#include "gl_uniform_buffer.h"

namespace gl
{
    gl_uniform_buffer::gl_uniform_buffer()
    {
        ::glGenBuffers( 1, &handle_ );
    }
    gl_uniform_buffer::~gl_uniform_buffer()
    {
        if (handle_ != 0)
        {
            ::glDeleteBuffers(1, &handle_);
        } 
    }
    GLuint gl_uniform_buffer::get_handle()const
    {
        return handle_;
    }
}
