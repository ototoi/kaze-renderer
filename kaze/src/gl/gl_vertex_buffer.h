#ifndef GL_VERTEX_BUFFER_H
#define GL_VERTEX_BUFFER_H

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/glew.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

namespace gl
{
    class gl_vertex_buffer
    {
    public:
        gl_vertex_buffer();
        ~gl_vertex_buffer();
        GLuint get_handle()const;
    private:
        GLuint handle_;
    };
}

#endif
