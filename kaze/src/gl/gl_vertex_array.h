#ifndef GL_VERTEX_ARRAY_H
#define GL_VERTEX_ARRAY_H

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/glew.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

namespace gl
{
    class gl_vertex_array
    {
    public:
        gl_vertex_array();
        ~gl_vertex_array();
        GLuint get_handle()const;
    private:
        GLuint handle_;
    };
}

#endif
