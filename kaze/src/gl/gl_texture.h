#ifndef GL_TEXTURE_H
#define GL_TEXTURE_H

#include <GL/glew.h>

#ifdef _WIN32
#include <windows.h>
#endif

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

namespace gl
{
    class gl_texture
    {
    public:
        gl_texture();
        virtual ~gl_texture() {}
        virtual GLuint get_handle() const;

    protected:
        GLuint handle_;
    };

    class gl_texture_1D : public gl_texture
    {
    public:
        gl_texture_1D();
        ~gl_texture_1D();

    public:
        void set_image(GLint internalformat, GLsizei width, GLenum format, GLenum type, const GLvoid* pixels, bool mipmap = false);
    };

    class gl_texture_2D : public gl_texture
    {
    public:
        gl_texture_2D();
        ~gl_texture_2D();
        void set_image(GLint internalformat, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid* pixels, bool mipmap = false);

    public:
        void bind() const;
        void unbind() const;
    };

    class gl_texture_2DArray : public gl_texture
    {
    public:
        gl_texture_2DArray();
        ~gl_texture_2DArray();
        void set_image(GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const GLvoid* pixels);

    public:
        void bind() const;
        void unbind() const;
    };

    class gl_texture_3D : public gl_texture
    {
    public:
        int dummy_;
    };
}

#endif
