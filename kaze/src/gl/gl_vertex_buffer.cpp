#include "gl_vertex_buffer.h"

namespace gl
{
    gl_vertex_buffer::gl_vertex_buffer()
    {
        handle_ = 0u;
        ::glGenBuffers(1, &handle_);
    }
    gl_vertex_buffer::~gl_vertex_buffer()
    {
        if(handle_)
        {
            ::glDeleteBuffers(1, &handle_);
        }
    }
    GLuint gl_vertex_buffer::get_handle()const
    {
        return handle_;
    }
}
