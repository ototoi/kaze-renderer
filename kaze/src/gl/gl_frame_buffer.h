#ifndef GL_FRAME_BUFFER_H
#define GL_FRAME_BUFFER_H

#include "gl_shader.h"
#include "gl_texture.h"
#include <memory>

namespace gl
{
    class gl_frame_buffer
    {
    public:
        gl_frame_buffer();
        ~gl_frame_buffer();

    public:
        GLuint get_handle() const;
        void bind() const;
        void unbind() const;

    public:
        void attach_color(GLuint handle, int n) const;
        void attach_depth(GLuint handle)const;
    public:
        GLenum check_status() const;

    private:
        GLuint handle_;
    };
}

#endif
