#include "gl_vertex_array.h"

namespace gl
{
    gl_vertex_array::gl_vertex_array()
    {
        handle_ = 0u;
        ::glGenVertexArrays(1, &handle_);
    }
    gl_vertex_array::~gl_vertex_array()
    {
        if(handle_)
        {
            ::glDeleteVertexArrays(1, &handle_);
        }
    }
    GLuint gl_vertex_array::get_handle()const
    {
        return handle_;
    }
}
