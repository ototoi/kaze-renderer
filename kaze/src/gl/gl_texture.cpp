#include "gl_texture.h"

namespace gl
{

    gl_texture::gl_texture()
    {
        handle_ = 0;
    }

    GLuint gl_texture::get_handle() const
    {
        return handle_;
    }

    //--------------------------------------------------------

    gl_texture_1D::gl_texture_1D()
    {
        handle_ = 0;
        glGenTextures(1, &handle_);
    }

    gl_texture_1D::~gl_texture_1D()
    {
        if (handle_ != 0) glDeleteTextures(1, &handle_);
    }

    void gl_texture_1D::set_image(GLint internalformat, GLsizei width, GLenum format, GLenum type, const GLvoid* pixels, bool mipmap)
    {
        if (handle_ == 0) return;
        bool bCompressed = false;

        glBindTexture(GL_TEXTURE_1D, handle_);

        if (!bCompressed)
        {
            glTexImage1D(GL_TEXTURE_1D, 0, internalformat, width, 0, format, type, pixels);
        }
        else
        { //Compress
            int imageSize = 0;
            glCompressedTexImage1D(GL_TEXTURE_1D, 0, format, width, 0, imageSize, pixels);
        }

        if (mipmap) glGenerateMipmap(GL_TEXTURE_1D);

        glBindTexture(GL_TEXTURE_1D, 0);
    }

    //--------------------------------------------------------

    gl_texture_2D::gl_texture_2D()
    {
        handle_ = 0;
        glGenTextures(1, &handle_);
    }

    gl_texture_2D::~gl_texture_2D()
    {
        if (handle_ != 0) glDeleteTextures(1, &handle_);
    }

    void gl_texture_2D::set_image(GLint internalformat, GLsizei width, GLsizei height, GLenum format, GLenum type, const GLvoid* pixels, bool mipmap)
    {
        if (handle_ == 0) return;
        bool bCompressed = false;

        this->bind();
        if (!bCompressed)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height, 0, format, type, pixels);
        }
        else
        { //Compress
            int imageSize = 0;
            glCompressedTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, imageSize, pixels);
        }

        if (mipmap) glGenerateMipmap(GL_TEXTURE_2D);

        this->unbind();
    }

    void gl_texture_2D::bind() const
    {
        ::glBindTexture(GL_TEXTURE_2D, handle_);
    }

    void gl_texture_2D::unbind() const
    {
        ::glBindTexture(GL_TEXTURE_2D, 0);
    }

    gl_texture_2DArray::gl_texture_2DArray()
    {
        handle_ = 0;
        glGenTextures(1, &handle_);
    }

    gl_texture_2DArray::~gl_texture_2DArray()
    {
        if (handle_ != 0) glDeleteTextures(1, &handle_);
    }

    void gl_texture_2DArray::set_image(GLint internalformat, GLsizei width, GLsizei height, GLsizei depth, GLint border, GLenum format, GLenum type, const GLvoid* pixels)
    {
        if (handle_ == 0) return;
        this->bind();
        ::glTexImage3D(GL_TEXTURE_2D_ARRAY, 0, internalformat, width, height, depth, border, format, type, pixels);
        this->unbind();
    }

    void gl_texture_2DArray::bind() const
    {
        ::glBindTexture(GL_TEXTURE_2D_ARRAY, handle_);
    }

    void gl_texture_2DArray::unbind() const
    {
        ::glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
    }
}