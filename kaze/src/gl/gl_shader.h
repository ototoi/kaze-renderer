#ifndef GL_SHADER_H
#define GL_SHADER_H

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/glew.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <string>

namespace gl
{
    class gl_shader
    {
    public:
        gl_shader(GLuint type = GL_VERTEX_SHADER);
        ~gl_shader();

    public:
        bool compile_from_file(const std::string& szFileName);
        bool compile_from_string(const std::string& strSource);

        GLuint get_handle() const;
        GLuint get_type() const;
        std::string get_log() const;
        bool is_compiled() const;
        bool is_valid() const;

    protected:
        GLuint handle_;
        GLuint type_;
        bool bCompiled_;
    };
}

#endif
