#include "gl_frame_buffer.h"

#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/glew.h>

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
//#include <OpenGL/gl3.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

namespace gl
{

    gl_frame_buffer::gl_frame_buffer()
    {
        ::glGenFramebuffers(1, &handle_);
    }

    gl_frame_buffer::~gl_frame_buffer()
    {
        if (handle_)
        {
            ::glDeleteFramebuffers(1, &handle_);
        }
    }

    GLuint gl_frame_buffer::get_handle() const
    {
        return handle_;
    }

    void gl_frame_buffer::bind() const
    {
        ::glBindFramebuffer(GL_FRAMEBUFFER, handle_);
    }

    void gl_frame_buffer::unbind() const
    {
        ::glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    void gl_frame_buffer::attach_color(GLuint handle, int n) const
    {
        ::glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + n, GL_TEXTURE_2D, handle, 0);
    }

    void gl_frame_buffer::attach_depth(GLuint handle)const
    {
        ::glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, handle, 0);
    }

    GLenum gl_frame_buffer::check_status() const
    {
        return ::glCheckFramebufferStatus(GL_FRAMEBUFFER);
    }
}
