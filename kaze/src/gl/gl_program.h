#ifndef GL_PROGRAM_H
#define GL_PROGRAM_H

#include "gl_shader.h"
#include "gl_texture.h"

#include <memory>
#include <vector>

namespace gl
{
    class gl_program
    {
    public:
        gl_program();
        ~gl_program();

    public:
        GLuint get_handle() const;

    public:
        void add_shader(GLuint handle) const;
        void add_shader(const std::shared_ptr<gl_shader>& sh) const;
        bool link() const;
        std::string get_log() const;

    public:
        GLint get_attrib_location(const char* szName) const;
        GLint get_uniform_location(const char* szName) const;

    public:
        void set_uniform(GLint location, GLint v0);
        void set_uniform(GLint location, GLfloat v0);
        void set_uniform(GLint location, GLuint count, const GLfloat* v);

    private:
        GLuint handle_;
    };
}

#endif
