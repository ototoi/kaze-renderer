#include "gl_program.h"

namespace gl
{

    gl_program::gl_program()
        : handle_(0)
    {
        handle_ = ::glCreateProgram();
    }

    gl_program::~gl_program()
    {
        if (handle_ != 0) ::glDeleteProgram(handle_);
    }

    GLuint gl_program::get_handle() const
    {
        return handle_;
    }

    void gl_program::add_shader(GLuint handle) const
    {
        ::glAttachShader(handle_, handle);
    }

    void gl_program::add_shader(const std::shared_ptr<gl_shader>& sh) const
    {
        this->add_shader(sh->get_handle());
    }

    bool gl_program::link() const
    {
        if (handle_ == 0)
        {
            return false;
        }

        ::glLinkProgram(handle_);
        GLint result = 0;
        ::glGetProgramiv(handle_, GL_LINK_STATUS, &result);
        if (result == GL_FALSE)
        {
            return false;
        }
        return true;
    }

    std::string gl_program::get_log() const
    {
        std::string str;
        if (handle_ != 0)
        {
            GLint len = 0;
            ::glGetShaderiv(handle_, GL_INFO_LOG_LENGTH, &len);
            if (len > 0)
            {
                std::vector<char> buffer(len + 1);
                GLsizei written = 0;
                ::glGetShaderInfoLog(handle_, len, &written, &buffer[0]);
                buffer[written] = 0;
                str = (const char*)(&buffer[0]);
            }
        }
        return str;
    }

    GLint gl_program::get_attrib_location(const char* szName) const
    {
        return ::glGetAttribLocation(handle_, szName);
    }

    GLint gl_program::get_uniform_location(const char* szName) const
    {
        return ::glGetUniformLocation(handle_, szName);
    }

    void gl_program::set_uniform(GLint location, GLint v)
    {
        ::glUniform1i(location, v);
    }

    void gl_program::set_uniform(GLint location, GLfloat v)
    {
        ::glUniform1f(location, v);
    }

    void gl_program::set_uniform(GLint location, GLuint count, const GLfloat* v)
    {
        ::glUniform1fv(location, count, v);
    }
}
