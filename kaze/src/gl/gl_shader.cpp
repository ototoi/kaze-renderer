#include "gl_shader.h"

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include <vector>
#include <string>
#include <assert.h>

#include <stdio.h>

namespace gl
{
    static void LoadShaderAsString(std::vector<GLchar>& bytes, const char* szFile)
    {
        FILE* fp = fopen(szFile, "rt");
        if (fp == 0) return;

        {
            fseek(fp, 0, SEEK_END);
            size_t sz = ftell(fp);
            bytes.reserve(sz + 1);
            fseek(fp, 0, SEEK_SET);
        }

        char buffer[1024] = {};
        while (1)
        {
            size_t szRead = fread(buffer, 1, 1024, fp);
            if (szRead == 0 || szRead == EOF) break;
            size_t szNow = bytes.size();
            bytes.resize(szNow + szRead);
            memcpy(&bytes[szNow], buffer, szRead);
        }
        fclose(fp);

        bytes.push_back(0);
    }

    static bool CheckShaderType(GLuint type)
    {
        switch (type)
        {
        case GL_VERTEX_SHADER:
        case GL_FRAGMENT_SHADER:
        case GL_GEOMETRY_SHADER:
        case GL_TESS_CONTROL_SHADER:
        case GL_TESS_EVALUATION_SHADER:
        case GL_COMPUTE_SHADER:
            return true;
        default:
            return false;
        }
        return false;
    }

    gl_shader::gl_shader(GLuint type)
        : handle_(0), type_(type), bCompiled_(false)
    {
        assert(CheckShaderType(type));
        handle_ = glCreateShader(type);
    }

    gl_shader::~gl_shader()
    {
        if (handle_ != 0)
        {
            glDeleteShader(handle_);
            handle_ = 0;
        }
    }

    bool gl_shader::compile_from_file(const std::string& strFileName)
    {
        std::vector<GLchar> bytes;
        LoadShaderAsString(bytes, strFileName.c_str());
        if(bytes.empty())return false;
        return compile_from_string(&bytes[0]);
    }

    bool gl_shader::compile_from_string(const std::string& strSource)
    {
        if (strSource.empty()) return false;

        if (handle_ == 0)
        {
            handle_ = glCreateShader(type_);
        }
        if (handle_ == 0)
        {
            return false;
        }

        GLuint handle = handle_;

        //Set Shader Source
        const GLchar* szCode = (const GLchar*)strSource.c_str();
        const GLchar* szCodeArray[] = {szCode};
        glShaderSource(handle, 1, szCodeArray, NULL);

        //Compile Shader
        glCompileShader(handle);

        //Check Status
        GLint result = 0;
        glGetShaderiv(handle, GL_COMPILE_STATUS, &result);
        if (result == GL_FALSE)
        {
            // サイズを取得
            GLint log_length;
            glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &log_length);

            // 文字列を取得
            const GLsizei max_length = 512;
            GLsizei length;
            GLchar log[max_length] = {};
            glGetShaderInfoLog(handle, max_length, &length, log);
            printf("error:%s\n", log);
            return false;
        }

        bCompiled_ = true;
        handle_ = handle;
        return true;
    }

    GLuint gl_shader::get_handle() const
    {
        return handle_;
    }

    GLuint gl_shader::get_type() const
    {
        return type_;
    }

    std::string gl_shader::get_log() const
    {
        std::string str;
        if (handle_ != 0)
        {
            GLint len = 0;
            glGetShaderiv(handle_, GL_INFO_LOG_LENGTH, &len);
            if (len > 0)
            {
                std::vector<char> buffer(len + 1);
                GLsizei written = 0;
                glGetShaderInfoLog(handle_, len, &written, &buffer[0]);
                buffer[written] = 0;
                str = (const char*)(&buffer[0]);
            }
        }
        return str;
    }

    bool gl_shader::is_compiled() const
    {
        return bCompiled_;
    }

    bool gl_shader::is_valid() const
    {
        return (handle_ != 0) && (bCompiled_);
    }
}
