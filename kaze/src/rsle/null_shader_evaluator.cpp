#include "null_shader_evaluator.h"

namespace rsle
{

    static vector3 diffuse_(const vector3& N, const shader_input* in)
    {
        color3 C(0, 0, 0);
        vector3 Cl = color3(1, 1, 1);
        vector3 E = in->E();
        vector3 P = in->P();
        vector3 L = E - P;
        if (dot(L, N) >= 0)
        {
            C += Cl * std::max<real>(0, dot(normalize(L), N));
        }
        return C;
    }

    void null_shader_evaluator::evaluate(shader_output* out, const shader_input* in) const
    {
        real Ka = 0;
        real Kd = 1.0;

        color3 Cs = in->Cs();
        color3 Os = in->Os();

        vector3 N = in->N();
        vector3 I = in->I();

        vector3 Nf = faceforward(normalize(N), I);
        color3 Oi = Os;
        color3 Ci = Os * Cs * (Ka * in->ambient() + Kd * diffuse_(Nf, in));

        out->Ci(Ci);
        out->Oi(Oi);
    }
}
