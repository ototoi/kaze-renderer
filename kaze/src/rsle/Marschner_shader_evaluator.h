#ifndef RSLE_MARSCHNER_SHADER_EVALUATOR_H
#define RSLE_MARSCHNER_SHADER_EVALUATOR_H

#include "shader_evaluator.h"
#include "types.h"

namespace rsle
{
    class Marschner_shader_evaluator : public shader_evaluator
    {
    public:
        Marschner_shader_evaluator();
        ~Marschner_shader_evaluator();

    public:
        virtual void evaluate(shader_output* out, const shader_input* in) const;

    public:
        void set(const char* key, real x);
        void set(const char* key, const color3& x);

    protected:
        real ior_;
        real eccentricity_;
        real shiftR_;
        real shiftTT_;
        real shiftTRT_;
        real widthR_;
        real widthTT_;
        real widthTRT_;
        real causticLimit_;
        real causticWidth_;
        real glintScale_;
        real causticFade_;
    };
}

#endif