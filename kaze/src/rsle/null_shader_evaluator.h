#ifndef RSLE_null_shader_evaluator_H
#define RSLE_null_shader_evaluator_H

#include "shader_evaluator.h"

namespace rsle
{
    class null_shader_evaluator : public shader_evaluator
    {
    public:
        virtual void evaluate(shader_output* out, const shader_input* in) const;
    };
}

#endif