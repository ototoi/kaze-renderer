#include "shader_evaluator.h"

#include "random.h"
#include "grid_base_noise_texture.h"

namespace rsle
{
    vector3 shader_input::ambient() const
    {
        return vector3(0, 0, 0);
    }

    vector3 shader_input::diffuse(const vector3& N) const
    {
        color3 C = color3(0, 0, 0);
        size_t sz = this->illuminance_size();
        for (size_t i = 0; i < sz; i++)
        {
            vector3 Cl = this->illuminance_Cl(i);
            vector3 L = this->illuminance_L(i);
            if (dot(L, N) >= 0)
            {
                C += Cl * std::max<real>(0, dot(normalize(L), N));
            }
        }
        return C;
    }

    vector3 shader_input::specular(const vector3& N, const vector3& V, real roughness) const
    {
#if 1
        color3 C = color3(0, 0, 0);
        size_t sz = this->illuminance_size();
        for (size_t i = 0; i < sz; i++)
        {
            vector3 Cl = this->illuminance_Cl(i);
            vector3 L = this->illuminance_L(i);
            //if(dot(L,N)>=0){
            C += Cl * specularbrdf(normalize(L), N, V, roughness);
            //}
        }
        return C;
#else
        color3 C = color3(0, 0, 0);
        vector3 R = reflect(-normalize(V), normalize(N));
        size_t sz = this->illuminance_size();
        for (size_t i = 0; i < sz; i++)
        {
            vector3 Cl = this->illuminance_Cl(i);
            vector3 L = this->illuminance_L(i);
            //if(dot(L,N)>=0){
            vector3 Ln = normalize(L);
            C += Cl * std::pow(std::max<real>(0.0, dot(R, Ln)), (real)8.0 / roughness);
            //}
        }
        return C;
#endif
    }

    vector3 shader_input::specularbrdf(const vector3& L, const vector3& N, const vector3& V, real roughness) const
    {
        vector3 H = normalize(L + V);
        real c = std::pow(std::max<real>(0, dot(N, H)), 8.0 / roughness);
        return color3(c, c, c);
    }

    vector3 shader_input::phong(const vector3& N, const vector3& V, real size) const
    {
        color3 C = color3(0, 0, 0);
        vector3 R = reflect(-normalize(V), normalize(N));
        size_t sz = this->illuminance_size();
        for (size_t i = 0; i < sz; i++)
        {
            vector3 Cl = this->illuminance_Cl(i);
            vector3 L = this->illuminance_L(i);
            //if(dot(L,N)>=0){
            vector3 Ln = normalize(L);
            C += Cl * pow(std::max(0.0, dot(R, Ln)), size);
            //}
        }
        return C;
    }

    real shader_input::texture_f1(const char* texturename, real u, real v) const
    {
        return this->get_f1(texturename);
    }

    vector2 shader_input::texture_f2(const char* texturename, real u, real v) const
    {
        return this->get_f2(texturename);
    }

    vector3 shader_input::texture_f3(const char* texturename, real u, real v) const
    {
        return this->get_f3(texturename);
    }
    //---------------------------------------------------------------------------------------
    vector3 shader_functions::faceforward(const vector3& N, const vector3& I)
    {
        if (dot(I, N) > 0)
        {
            return -N;
        }
        else
        {
            return N;
        }
    }
    vector3 shader_functions::reflect(const vector3& I, const vector3& N)
    {
        return I - 2 * dot(I, N) * N;
    }

    vector3 shader_functions::refract(const vector3& I, const vector3& N, real eta)
    {
        real IdotN = dot(I, N);
        real k = 1 - eta * eta * (1 - IdotN * IdotN);
        return k < 0 ? vector3(0, 0, 0) : eta * I - (eta * IdotN + sqrt(k)) * N;
    }
    //---------------------------------------------------------------------------------------

    /*
	vector3 shader_functions::cellnoise(real u, real v)
    {
		static grid_base_noise_texture tex;
		real val = tex.get_value(u,v,0);
        return vector3(val,val,val);
    }
	*/

    real shader_functions::step(real min, real value)
    {
        return (value < min) ? 0 : 1;
    }

    vector2 shader_functions::step(const vector2& min, const vector2& value)
    {
        return vector2(step(min[0], value[0]), step(min[1], value[1]));
    }
    vector3 shader_functions::step(const vector3& min, const vector3& value)
    {
        return vector3(
            step(min[0], value[0]),
            step(min[1], value[1]),
            step(min[2], value[2]));
    }
    vector2 shader_functions::step(real min, const vector2& value)
    {
        return step(vector2(min, min), value);
    }
    vector3 shader_functions::step(real min, const vector3& value)
    {
        return step(vector3(min, min, min), value);
    }

    real shader_functions::smoothstep(real min, real max, real value)
    {
        if (value <= min) return 0;
        if (max <= value) return 1;
        value = (value - min) / (max - min);
        return (value * value * (real(3.0) - (real(2.0) * value)));
    }
    vector2 shader_functions::smoothstep(const vector2& min, const vector2& max, const vector2& value)
    {
        return vector2(
            smoothstep(min[0], max[0], value[0]),
            smoothstep(min[1], max[1], value[1]));
    }
    vector3 shader_functions::smoothstep(const vector3& min, const vector3& max, const vector3& value)
    {
        return vector3(
            smoothstep(min[0], max[0], value[0]),
            smoothstep(min[1], max[1], value[1]),
            smoothstep(min[2], max[2], value[2]));
    }
    vector2 shader_functions::smoothstep(real min, real max, const vector2& value)
    {
        return smoothstep(vector2(min, min), vector2(max, max), value);
    }
    vector3 shader_functions::smoothstep(real min, real max, const vector3& value)
    {
        return smoothstep(vector3(min, min, min), vector3(max, max, max), value);
    }
    //---------------------------------------------------------------------------------------
    real shader_functions::spline(real value, const std::vector<real>& vals)
    {
        return 0;
    }
    real shader_functions::spline(const char* szBasis, real value, const std::vector<real>& vals)
    {
        return 0;
    }

    real shader_functions::random_f1()
    {
        static const real c = 1.0 / (real(std::numeric_limits<rand_t>::max()) + 1);
        return real(xor128()) * c;
    }
    vector2 shader_functions::random_f2()
    {
        return vector2(random_f1(), random_f1());
    }
    vector3 shader_functions::random_f3()
    {
        return vector3(random_f1(), random_f1(), random_f1());
    }

    //---------------------------------------------------------------------------------------

    real shader_functions::noise_f1(real u)
    {
        return hyper_texture::noise(u, 0, 0);
    }
    real shader_functions::noise_f1(real u, real v)
    {
        return hyper_texture::noise(u, v, 0);
    }
    real shader_functions::noise_f1(real u, real v, real w)
    {
        return hyper_texture::noise(u, v, w);
    }
    real shader_functions::noise_f1(real u, real v, real w, real t)
    {
        return hyper_texture::noise(u + t, v + t, w + t);
    }
    real shader_functions::noise_f1(const vector3& pt)
    {
        return noise_f1(pt[0], pt[1], pt[2]);
    }
    real shader_functions::noise_f1(const vector3& pt, real t)
    {
        return noise_f1(pt[0], pt[1], pt[2], t);
    }

    vector3 shader_functions::noise_f3(real u)
    {
        real x = noise_f1(u);
        return vector3(x, x, x);
    }
    vector3 shader_functions::noise_f3(real u, real v)
    {
        real x = noise_f1(u, v);
        return vector3(x, x, x);
    }
    vector3 shader_functions::noise_f3(real u, real v, real w)
    {
        real x = noise_f1(u, v, w);
        return vector3(x, x, x);
    }
    vector3 shader_functions::noise_f3(real u, real v, real w, real t)
    {
        real x = noise_f1(u, v, w, t);
        return vector3(x, x, x);
    }
    vector3 shader_functions::noise_f3(const vector3& pt)
    {
        return noise_f3(pt[0], pt[1], pt[2]);
    }
    vector3 shader_functions::noise_f3(const vector3& pt, real t)
    {
        return noise_f3(pt[0], pt[1], pt[2], t);
    }

    //---------------------------------------------------------------------------------------

    real shader_functions::cellnoise_f1(real u)
    {
        return hyper_texture::grid(u, 0, 0);
    }
    real shader_functions::cellnoise_f1(real u, real v)
    {
        return hyper_texture::grid(u, v, 0);
    }
    real shader_functions::cellnoise_f1(real u, real v, real w)
    {
        return hyper_texture::grid(u, v, w);
    }
    real shader_functions::cellnoise_f1(real u, real v, real w, real t)
    {
        return hyper_texture::grid(u + t, v + t, w + t);
    }
    real shader_functions::cellnoise_f1(const vector3& pt)
    {
        return cellnoise_f1(pt[0], pt[1], pt[2]);
    }
    real shader_functions::cellnoise_f1(const vector3& pt, real t)
    {
        return cellnoise_f1(pt[0], pt[1], pt[2], t);
    }

    vector3 shader_functions::cellnoise_f3(real u)
    {
        real x = cellnoise_f1(u);
        return vector3(x, x, x);
    }
    vector3 shader_functions::cellnoise_f3(real u, real v)
    {
        real x = cellnoise_f1(u, v);
        return vector3(x, x, x);
    }
    vector3 shader_functions::cellnoise_f3(real u, real v, real w)
    {
        real x = cellnoise_f1(u, v, w);
        return vector3(x, x, x);
    }
    vector3 shader_functions::cellnoise_f3(real u, real v, real w, real t)
    {
        real x = cellnoise_f1(u, v, w, t);
        return vector3(x, x, x);
    }
    vector3 shader_functions::cellnoise_f3(const vector3& pt)
    {
        return cellnoise_f3(pt[0], pt[1], pt[2]);
    }
    vector3 shader_functions::cellnoise_f3(const vector3& pt, real t)
    {
        return noise_f3(pt[0], pt[1], pt[2], t);
    }

    //---------------------------------------------------------------------------------------

    real shader_functions::clamp(real a, real t0, real t1)
    {
        return min(max(a, t0), t1);
    }
    vector2 shader_functions::clamp(const vector2& a, const vector2& t0, const vector2& t1)
    {
        return min(max(a, t0), t1);
    }
    vector3 shader_functions::clamp(const vector3& a, const vector3& t0, const vector3& t1)
    {
        return min(max(a, t0), t1);
    }

    real shader_functions::mix(real a, real b, real alpha)
    {
        return a * (1 - alpha) + b * alpha;
    }
    vector2 shader_functions::mix(const vector2& a, const vector2& b, real alpha)
    {
        return a * (1 - alpha) + b * alpha;
    }
    vector3 shader_functions::mix(const vector3& a, const vector3& b, real alpha)
    {
        return a * (1 - alpha) + b * alpha;
    }
}
