#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include "ksl_shader_evaluator.h"

#include "ksl_vector.h"
#include "ksl_shader_output.h"
#include "ksl_shader_input.h"

#include <mutex>
#include <map>

#ifdef __cplusplus
extern "C" {
#endif
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#ifdef __cplusplus
}
#endif

#ifndef lua_open
#define lua_open() luaL_newstate()
#endif

namespace rsle
{
    static const int STATE_SIZE = 8;

#ifdef _WIN32
    typedef DWORD thread_id_t;
    inline thread_id_t get_current_thread_id()
    {
        return ::GetCurrentThreadId();
    }
#endif

#if defined(__unix__) || defined(__APPLE__)
    typedef unsigned long thread_id_t;
    inline thread_id_t get_current_thread_id()
    {
        return (thread_id_t)pthread_self();
    }
#endif

//using FNV1-a
#define FNV1_32_INIT ((Fnv32_t)0x811c9dc5)
#define FNV1_32A_INIT FNV1_32_INIT
/*
 * 32 bit magic FNV-1a prime
 */
#define FNV_32_PRIME ((Fnv32_t)0x01000193)

    /*
    * 32 bit FNV-0 hash type
    */
    typedef uint32_t Fnv32_t;

    /*
    * fnv_32a_buf - perform a 32 bit Fowler/Noll/Vo FNV-1a hash on a buffer
    *
    * input:
    *	buf	- start of buffer to hash
    *	len	- length of buffer in octets
    *	hval	- previous hash value or 0 if first call
    *
    * returns:
    *	32 bit hash as a static hash type
    *
    * NOTE: To use the recommended 32 bit FNV-1a hash, use FNV1_32A_INIT as the
    * 	 hval arg on the first call to either fnv_32a_buf() or fnv_32a_str().
    */
    Fnv32_t fnv_32a_buf(void* buf, size_t len, Fnv32_t hval)
    {
        unsigned char* bp = (unsigned char*)buf; /* start of buffer */
        unsigned char* be = bp + len;            /* beyond end of buffer */

        /*
        * FNV-1a hash each octet in the buffer
        */
        while (bp < be)
        {

            /* xor the bottom with the current octet */
            hval ^= (Fnv32_t)*bp++;

/* multiply by the 32 bit FNV magic prime mod 2^32 */
#if defined(NO_FNV_GCC_OPTIMIZATION)
            hval *= FNV_32_PRIME;
#else
            hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
#endif
        }

        /* return our new hash value */
        return hval;
    }

    static uint32_t UniqueThreadID()
    {
        uint32_t val = get_current_thread_id();
        return fnv_32a_buf(&val, 4, FNV1_32A_INIT);
    }

    static int GetSlotNumber()
    {
        return UniqueThreadID() & (STATE_SIZE - 1);
    }

    static uint32_t GetFileID(const std::string& file)
    {
        return fnv_32a_buf((void*)file.c_str(), file.size(), FNV1_32A_INIT);
    }

    class ksl_shader_evaluator_imp : public shader_evaluator
    {
    public:
        ksl_shader_evaluator_imp(const std::string& strFile);
        ~ksl_shader_evaluator_imp();

    public:
        void evaluate(shader_output* out, const shader_input* in) const;

    public:
        const std::string& get_file_path()const{return strFile_;}
    protected:
        void evaluate(lua_State* L, shader_output* out, const shader_input* in) const;

    protected:
        lua_State* GetState(int i) const;

    protected:
        mutable lua_State* Ls_[STATE_SIZE];
        mutable std::mutex mtxs_[STATE_SIZE];
        std::string strFile_;
        uint32_t    file_id_;
    };

    lua_State* ksl_shader_evaluator_imp::GetState(int i) const
    {
        assert(i < STATE_SIZE);
        if (Ls_[i] == 0)
        {
            Ls_[i] = lua_open();
            luaL_openlibs(Ls_[i]);
            ksl_vector_open(Ls_[i]);
            ksl_shader_output_open(Ls_[i]);
            ksl_shader_input_open(Ls_[i]);
            if (luaL_dofile(Ls_[i], strFile_.c_str()) != 0)
            {
                lua_close(Ls_[i]);
                Ls_[i] = 0;
            }
        }
        return Ls_[i];
    }

    ksl_shader_evaluator_imp::ksl_shader_evaluator_imp(const std::string& strFile)
        : strFile_(strFile)
    {
        file_id_ = GetFileID(strFile_);
        for (int i = 0; i < STATE_SIZE; i++)
        {
            Ls_[i] = 0;
        }
    }

    ksl_shader_evaluator_imp::~ksl_shader_evaluator_imp()
    {
        for (int i = 0; i < STATE_SIZE; i++)
        {
            if (Ls_[i]) lua_close(Ls_[i]);
        }
    }

    void ksl_shader_evaluator_imp::evaluate(lua_State* L, shader_output* out, const shader_input* in) const
    {
        lua_getglobal(L, "surface");

        lua_getglobal(L, "shader_output");
        lua_pushlightuserdata(L, (void*)out);
        if (lua_pcall(L, 1, 1, 0) != 0)
        {
            printf("error : %s\n", lua_tostring(L, -1));
            lua_pop(L, lua_gettop(L));
            return;
        }

        lua_getglobal(L, "shader_input");
        lua_pushlightuserdata(L, (void*)in);
        if (lua_pcall(L, 1, 1, 0) != 0)
        {
            printf("error : %s\n", lua_tostring(L, -1));
            lua_pop(L, lua_gettop(L));
            return;
        }
        if ((lua_pcall(L, 2, 0, 0)) != 0)
        { //
            printf("error : %s\n", lua_tostring(L, -1));
            lua_pop(L, lua_gettop(L));
            return;
        }
    }

    void ksl_shader_evaluator_imp::evaluate(shader_output* out, const shader_input* in) const
    {
        int i = GetSlotNumber();
        std::lock_guard<std::mutex> lck(mtxs_[i]);
        lua_State* L = GetState(i);
        if (L)
        {
            evaluate(L, out, in);
        }
    }
    //-----------------------------------------------------------------

    class ksl_shader_evaluator_mgr
    {
    public:
        typedef std::map<std::string, std::pair<ksl_shader_evaluator_imp*, int> > map_type;
    public:
        ksl_shader_evaluator_mgr() {}
        ~ksl_shader_evaluator_mgr() {}
    public:
        ksl_shader_evaluator_imp* get(const std::string& strFile)
        {
            std::lock_guard<std::mutex> lck(mtx_);
            map_type::iterator it = map_.find(strFile);
            if (it != map_.end())
            {
                return it->second.first;
            }
            else
            {
                ksl_shader_evaluator_imp* imp = new ksl_shader_evaluator_imp(strFile);
                std::pair<ksl_shader_evaluator_imp*, int> pr = std::make_pair(imp, 1);
                map_.insert(map_type::value_type(strFile, pr));
                return imp;
            }
        }

        void unget(const std::string& strFile)
        {
            std::lock_guard<std::mutex> lck(mtx_);
            map_type::iterator it = map_.find(strFile);
            if (it != map_.end())
            {
                it->second.second--;
                if(it->second.second <= 0)
                {
                    delete it->second.first;
                    it->second.first = NULL;
                    map_.erase(it);
                }
            }
            else
            {
                assert(0);
            }
        }

    private:
        map_type map_;
        mutable std::mutex mtx_;
    };

    static ksl_shader_evaluator_mgr& GetMgr()
    {
        static ksl_shader_evaluator_mgr g_mgr;
        return g_mgr;
    }

    ksl_shader_evaluator::ksl_shader_evaluator(const char* szFile)
    {
        imp_ = GetMgr().get(szFile);
    }

    ksl_shader_evaluator::~ksl_shader_evaluator()
    {
        GetMgr().unget(imp_->get_file_path());
        imp_ = NULL;
    }

    void ksl_shader_evaluator::evaluate(shader_output* out, const shader_input* in) const
    {
        imp_->evaluate(out, in);
    }
}
