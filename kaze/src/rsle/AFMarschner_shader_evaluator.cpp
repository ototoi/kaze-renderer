#include "AFMarschner_shader_evaluator.h"
#include <cmath>

namespace rsle
{
    static const real PI = math_values<real>::pi();

    static real radians(real x)
    {
        return math_functions::radians(x);
    }

    /**
	 * Transform a vector by 3 basis vectors.
	 */
    static vector3 local_spherical(const vector3& v, const vector3& x, const vector3& y, const vector3& z)
    {
        real a = v[0] * x[0] + v[1] * y[0] + v[2] * z[0];
        real b = v[0] * x[1] + v[1] * y[1] + v[2] * z[1];
        real c = v[0] * x[2] + v[1] * y[2] + v[2] * z[2];

        return vector3(atan2(b, a), PI * 0.5 - acos(c), 0);
    }

    /**
	 * Unit-height zero-mean Gaussian function.
	 */
    static real gaussian(real x, real mu, real sigma)
    {
        return exp(-pow(x - mu, 2.0) * 0.5 / pow(sigma, 2.0));
    }

    /**
	 * Jitted vector.
	 */
    /*
	vector3 jitted(const vector3& vec, real a, real b, real c)
	{
		real amp = a * sin(2 * PI * b * v + c);
		vector3 Po = transform("object", P);
		vector3 jitted = noise(Po);
		return normalize(vec + normalize(jitted) * amp);
	}
	*/

    static color3 AFMarschner(
        real phi,
        real thetaD,
        real thetaH,

        real intensityR,
        const color3& colorR,
        real longitudinalShiftR,
        real longitudinalWidthR,

        real intensityTT,
        const color3& colorTT,
        real longitudinalShiftTT,
        real longitudinalWidthTT,
        real azimuthalWidthTT,

        real intensityTRT,
        const color3& colorTRT,
        real longitudinalShiftTRT,
        real longitudinalWidthTRT,

        real intensityG,
        real azimuthalShiftG,
        real azimuthalWidthG,

        color3& R,
        color3& TT,
        color3& TRT)
    {
        // R
        //
        real M_R = gaussian(thetaH, radians(longitudinalShiftR), radians(longitudinalWidthR));
        real N_R = cos(phi * 0.5);

        R = intensityR * colorR * M_R * N_R;

        // TT
        //
        real M_TT = gaussian(thetaH, radians(longitudinalShiftTT), radians(longitudinalWidthTT));
        real N_TT = gaussian(PI, fabs(phi), radians(azimuthalWidthTT));

        TT = intensityTT * colorTT * M_TT * N_TT;

        // TRT
        //
        real M_TRT = gaussian(thetaH, radians(longitudinalShiftTRT), radians(longitudinalWidthTRT));
        real N_TRGNG = cos(phi * 0.5);
        real N_G = intensityG * gaussian(radians(azimuthalShiftG), fabs(phi), radians(azimuthalWidthG));
        real N_TRT = N_TRGNG + N_G;

        TRT = intensityTRT * colorTRT * M_TRT * N_TRT;

        // Normalize.
        //
        real cosThetaD = cos(thetaD);
        real scale = 1.0 / pow(cosThetaD, 2);

        //TRT
        return (R + TT + TRT) * scale;
    }

    AFMarschner_shader_evaluator::AFMarschner_shader_evaluator()
    {
        intensityD_ = 1;
        //middleD_ = 0.5;
        //middleRangeD_ = 0.1;
        //colorRootD_ = color3(1, 1, 1);
        //colorTipD_ = color3(1, 1, 1);

        intensityR_ = 1;
        colorR_ = color3(1, 1, 1);
        longitudinalShiftR_ = -7.5;
        longitudinalWidthR_ = 7.5;

        intensityTT_ = 1;
        colorTT_ = color3(1, 1, 1);
        longitudinalShiftTT_ = 3.75;
        longitudinalWidthTT_ = 3.75;
        azimuthalWidthTT_ = 3;

        intensityTRT_ = 1;
        colorTRT_ = color3(1, 1, 1);
        longitudinalShiftTRT_ = 11.25;
        longitudinalWidthTRT_ = 15;

        intensityG_ = 1;
        azimuthalShiftG_ = 30;
        azimuthalWidthG_ = 10;

        attenuationFromRoot_ = 1.0;
    }

    AFMarschner_shader_evaluator::~AFMarschner_shader_evaluator()
    {
        ; //
    }

    void AFMarschner_shader_evaluator::evaluate(shader_output* out, const shader_input* in) const
    {
        //real middleD = middleD_;
        //real middleRangeD = middleRangeD_;
        //color3 colorRootD = colorRootD_;
        //color3 colorTipD  = colorTipD_;
        real intensityD = intensityD_;

        real intensityR = intensityR_;
        color3 colorR = colorR_;
        real longitudinalShiftR = longitudinalShiftR_;
        real longitudinalWidthR = longitudinalWidthR_;

        real intensityTT = intensityTT_;
        color3 colorTT = colorTT_;
        real longitudinalShiftTT = longitudinalShiftTT_;
        real longitudinalWidthTT = longitudinalWidthTT_;
        real azimuthalWidthTT = azimuthalWidthTT_;

        real intensityTRT = intensityTRT_;
        color3 colorTRT = colorTRT_;
        real longitudinalShiftTRT = longitudinalShiftTRT_;
        real longitudinalWidthTRT = longitudinalWidthTRT_;

        real intensityG = intensityG_;
        real azimuthalShiftG = azimuthalShiftG_;
        real azimuthalWidthG = azimuthalWidthG_;

        real attenuationFromRoot = attenuationFromRoot_;

        //
        vector3 dPdv = in->dPdv();
        vector3 N = in->N();
        vector3 I = in->I();

        color3 Cs = in->Cs();
        color3 Os = in->Os();

        vector2 uv = in->uv();
        real u = uv[0];
        real v = uv[1];

        // Calcuate mixed diffuse color over curve in [0,1].
        //
        //real mixingRatio = spline(v, 0, middleD, middleD + middleRangeD, 1);
        //color3 mixedcolorD = mix(colorRootD, colorTipD, mixingRatio);
        color3 mixedcolorD = Cs;

        // Prepare vectors.
        //
        vector3 T = normalize(dPdv);
        vector3 Nn = normalize(N);
        vector3 B = normalize(cross(Nn, T));
        vector3 Vn = -normalize(I);

        vector3 omegaO = local_spherical(Vn, B, Nn, T);

        color3 diffuseResult = color3(0, 0, 0);
        color3 scatteringResult = color3(0, 0, 0);

        for (size_t i = 0; i < in->illuminance_size(); i++)
        {
            vector3 L = in->illuminance_L(i);
            color3 Cl = in->illuminance_Cl(i);

            // Get \omega_o
            //
            vector3 Ln = normalize(L);
            vector3 omegaI = local_spherical(Ln, B, Nn, T);

            // phi is in [-PI, PI]
            //
            real phi = fabs(omegaO[0] - omegaI[0]);
            if (phi > PI)
                phi -= 2 * PI;

            real thetaD = fabs(omegaO[1] - omegaI[1]) * 0.5;
            real thetaH = (omegaO[1] + omegaI[1]) * 0.5;

            // Calculate the diffuse contribution.
            //
            real sinTL = sqrt(1.0 - pow(dot(T, Ln), 2.0));
            diffuseResult += Cl * intensityD * mixedcolorD * sinTL;

            // Get the scattering contribution.
            //
            color3 R = color3(0, 0, 0);
            color3 TT = color3(0, 0, 0);
            color3 TRT = color3(0, 0, 0);
            scatteringResult += Cl * AFMarschner(phi, thetaD, thetaH,
                                                 intensityR, colorR, longitudinalShiftR, longitudinalWidthR,
                                                 intensityTT, colorTT, longitudinalShiftTT, longitudinalWidthTT, azimuthalWidthTT,
                                                 intensityTRT, colorTRT, longitudinalShiftTRT, longitudinalWidthTRT,
                                                 intensityG, azimuthalShiftG, azimuthalWidthG,
                                                 R, TT, TRT);
        }

        color3 Ci = (diffuseResult + scatteringResult) * pow(v, attenuationFromRoot);
        color3 Oi = Os;

        out->Ci(Ci);
        out->Oi(Oi);
    }

#define SET_KEY(KEY)            \
    if (strcmp(key, #KEY) == 0) \
    {                           \
        KEY##_ = x;             \
        return;                 \
    }

    void AFMarschner_shader_evaluator::set(const char* key, real x)
    {
        SET_KEY(intensityD); //real intensityD_;
        //SET_KEY(middleD);				//real middleD_;
        //SET_KEY(middleRangeD);		//real middleRangeD_;
        //color3 colorRootD_;
        //color3 colorTipD_;
        SET_KEY(intensityR); //real intensityR_;
        //color3 colorR_;
        SET_KEY(longitudinalShiftR); //real longitudinalShiftR_;
        SET_KEY(longitudinalWidthR); //real longitudinalWidthR_;

        SET_KEY(intensityTT); //real intensityTT_;
        //color3 colorTT_;
        SET_KEY(longitudinalShiftTT); //real longitudinalShiftTT_;
        SET_KEY(longitudinalWidthTT); //real longitudinalWidthTT_;
        SET_KEY(azimuthalWidthTT);    //real azimuthalWidthTT_;

        SET_KEY(intensityTRT); //real intensityTRT_;
        //color3 colorTRT_;
        SET_KEY(longitudinalShiftTRT); //real longitudinalShiftTRT_;
        SET_KEY(longitudinalWidthTRT); //real longitudinalWidthTRT_;

        SET_KEY(intensityG);      //real intensityG_;
        SET_KEY(azimuthalShiftG); //real azimuthalShiftG_;
        SET_KEY(azimuthalWidthG); //real azimuthalWidthG_;
    }

    void AFMarschner_shader_evaluator::set(const char* key, const color3& x)
    {
        //SET_KEY(colorRootD);	//color3 colorRootD_;
        //SET_KEY(colorTipD);	//color3 colorTipD;
        SET_KEY(colorR);   //color3 colorR_;
        SET_KEY(colorTT);  //color3 colorTT_;
        SET_KEY(colorTRT); //color3 colorTRT_;
    }
}
