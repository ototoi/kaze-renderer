#ifndef RSLE_SHADER_EVALUATOR_H
#define RSLE_SHADER_EVALUATOR_H

#include "color.h"
#include "values.h"
#include <string>
#include <cmath>
#include <vector>

namespace rsle
{
    typedef kaze::real real;
    typedef kaze::color3 color3;
    typedef kaze::vector2 vector2;
    typedef kaze::vector3 vector3;
    using namespace kaze;

    namespace values = kaze::values;

    template<class T>
    class math_values
    {
    public:
        static inline T pi() { return T(3.14159265358979323846); }
    };

    class math_functions
    {
    public:
        template<class T> static inline T radians(T degrees) { return degrees * math_values<T>::pi() / T(180);  }
        template<class T> static inline T degrees(T radians) { return radians * T(180) / math_values<T>::pi(); }
        template<class T> static inline T sin(T a) { return std::sin(a); }
        template<class T> static inline T asin(T a) { return std::asin(a); }
        template<class T> static inline T cos(T a) { return std::cos(a); }
        template<class T> static inline T acos(T a) { return std::acos(a); }
        template<class T> static inline T tan(T a) { return std::tan(a); }
        template<class T> static inline T atan(T yoverx) { return std::atan(yoverx); }
        template<class T> static inline T atan(T y, T x) { return std::atan2(y, x); }
        template<class T> static inline T pow(T x, T y) { return std::pow(x, y); }
        template<class T> static inline T exp(T x) { return std::exp(x); }
        template<class T> static inline T sqrt(T x) { return std::sqrt(x); }
        template<class T> static inline T inversesqrt(T x) { return T(1) / std::sqrt(x); }
        template<class T> static inline T log(T x) { return std::log(x); }
        template<class T> static inline T log(T x, T base) { return std::log(x) / std::log(base); }
        template<class T> static inline T mod(T a, T b) { return std::fmod(a, b); }
        template<class T> static inline T abs(T x) { return std::fabs(x); }
        template<class T> static inline T sign(T x) { return (x < T(0)) ? T(-1) : ( (x > T(0)) ? T(1) : T(0) ); }
        template<class T> static inline T floor(T x) { return std::floor(x); }
        template<class T> static inline T ceil(T x) { return std::ceil(x); }
        template<class T> static inline T round(T x) { return std::floor(x + T(0.5)); }
        template<class T> static inline T fract(T x) { return x - std::floor(x); }
        template<class T> static inline T min(T a, T b) { return std::min<T>(a, b); }
        template<class T> static inline T max(T a, T b) { return std::max<T>(a, b); }
        template<class T> static inline T min(const std::vector<T>& v) { return *(std::min_element<T>(v.begin(), v.end())); }
        template<class T> static inline T max(const std::vector<T>& v) { return *(std::max_element<T>(v.begin(), v.end())); }
#define DEFFUNCS1(FUNC)                                                               \
    static vector2 FUNC(const vector2& x) { return vector2(FUNC(x[0]), FUNC(x[1])); } \
    static vector3 FUNC(const vector3& x) { return vector3(FUNC(x[0]), FUNC(x[1]), FUNC(x[2])); }

        DEFFUNCS1(radians)
        DEFFUNCS1(degrees)
        DEFFUNCS1(sin)
        DEFFUNCS1(asin)
        DEFFUNCS1(cos)
        DEFFUNCS1(acos)
        DEFFUNCS1(tan)
        DEFFUNCS1(atan)
        DEFFUNCS1(exp)
        DEFFUNCS1(sqrt)
        DEFFUNCS1(inversesqrt)
        DEFFUNCS1(log)
        DEFFUNCS1(abs)
        DEFFUNCS1(sign)
        DEFFUNCS1(floor)
        DEFFUNCS1(ceil)
        DEFFUNCS1(round)
        DEFFUNCS1(fract)

#undef DEFFUNCS1

#define DEFFUNCS2(FUNC)                                                                                             \
    static vector2 FUNC(const vector2& a, const vector2& b) { return vector2(FUNC(a[0], b[0]), FUNC(a[1], b[1])); } \
    static vector3 FUNC(const vector3& a, const vector3& b) { return vector3(FUNC(a[0], b[0]), FUNC(a[1], b[1]), FUNC(a[2], b[2])); }

        DEFFUNCS2(atan)
        DEFFUNCS2(log)
        DEFFUNCS2(mod)
        DEFFUNCS2(min)
        DEFFUNCS2(max)

#undef DEFFUNCS2
    
    };

    class shader_functions : public math_functions
    {
    public:
        static real clamp(real a, real t0, real t1);
        static vector2 clamp(const vector2& a, const vector2& t0, const vector2& t1);
        static vector3 clamp(const vector3& a, const vector3& t0, const vector3& t1);

        static real mix(real a, real b, real alpha);
        static vector2 mix(const vector2& a, const vector2& b, real alpha);
        static vector3 mix(const vector3& a, const vector3& b, real alpha);

        static real step(real min, real value);
        static vector2 step(const vector2& min, const vector2& value);
        static vector3 step(const vector3& min, const vector3& value);
        static vector2 step(real min, const vector2& value);
        static vector3 step(real min, const vector3& value);

        static real smoothstep(real min, real max, real value);
        static vector2 smoothstep(const vector2& min, const vector2& max, const vector2& value);
        static vector3 smoothstep(const vector3& min, const vector3& max, const vector3& value);
        static vector2 smoothstep(real min, real max, const vector2& value);
        static vector3 smoothstep(real min, real max, const vector3& value);
        //filterstep
        //spline
        static real spline(real value, const std::vector<real>& vals);
        static real spline(const char* szBasis, real value, const std::vector<real>& vals);

        //Du
        //Dv

        //random
        static real random_f1();
        static vector2 random_f2();
        static vector3 random_f3();

        //noise
        static real noise_f1(real u);
        static real noise_f1(real u, real v);
        static real noise_f1(real u, real v, real w);
        static real noise_f1(real u, real v, real w, real t);

        static real noise_f1(const vector3& pt);
        static real noise_f1(const vector3& pt, real t);

        static vector3 noise_f3(real v);
        static vector3 noise_f3(real u, real v);
        static vector3 noise_f3(real u, real v, real w);
        static vector3 noise_f3(real u, real v, real w, real t);

        static vector3 noise_f3(const vector3& pt);
        static vector3 noise_f3(const vector3& pt, real t);
        //pnoise

        //cellnoise
        static real cellnoise_f1(real u);
        static real cellnoise_f1(real u, real v);
        static real cellnoise_f1(real u, real v, real w);
        static real cellnoise_f1(real u, real v, real w, real t);

        static real cellnoise_f1(const vector3& pt);
        static real cellnoise_f1(const vector3& pt, real t);

        static vector3 cellnoise_f3(real v);
        static vector3 cellnoise_f3(real u, real v);
        static vector3 cellnoise_f3(real u, real v, real w);
        static vector3 cellnoise_f3(real u, real v, real w, real t);

        static vector3 cellnoise_f3(const vector3& pt);
        static vector3 cellnoise_f3(const vector3& pt, real t);

        static vector3 faceforward(const vector3& N, const vector3& I);
        static vector3 reflect(const vector3& I, const vector3& N);
        static vector3 refract(const vector3& I, const vector3& N, double eta);

        //fresnel( vector I, N; float eta; output float Kr, Kt; [output vector R, T] )
    };

    class shader_output
    {
    public:
        virtual ~shader_output() {}
        virtual void Ci(const color3& c) = 0;
        virtual void Oi(const color3& c) = 0;
        virtual void env(const color3& c) {}
        virtual void set_f3(const char* key, const vector3& v) {}
        virtual void set_f2(const char* key, const vector2& v) {}
        virtual void set_f1(const char* key, real f) {}
    };

    class shader_input : public shader_functions
    {
    public:
        virtual ~shader_input() {}

        virtual vector3 P() const = 0;
        virtual vector3 N() const = 0;
        virtual vector3 Ng() const { return this->N(); }
        virtual vector3 E() const = 0;
        virtual vector3 I() const = 0;
        virtual vector3 dPdu() const { return this->get_f3("dPdu"); }
        virtual vector3 dPdv() const { return this->get_f3("dPdv"); }
        virtual vector3 uvw() const { return this->get_f3("uvw"); }
        virtual vector2 uv() const { return this->get_f2("uv"); }
        virtual vector3 stw() const { return this->get_f3("stw"); }
        virtual vector2 st() const { return this->get_f2("st"); }
        virtual vector3 Cs() const { return this->get_f3("Cs"); }
        virtual vector3 Os() const { return this->get_f3("Os"); }
        virtual real u() const { return this->uv()[0]; }
        virtual real v() const { return this->uv()[1]; }
        virtual real s() const { return this->st()[0]; }
        virtual real t() const { return this->st()[1]; }
        virtual vector3 get_f3(const char* key) const { return get_f3(key, vector3(0, 0, 0)); }
        virtual vector2 get_f2(const char* key) const { return get_f2(key, vector2(0, 0)); }
        virtual real get_f1(const char* key) const { return get_f1(key, real(0)); }
        virtual vector3 get_f3(const char* key, const vector3& def) const { return def; }
        virtual vector2 get_f2(const char* key, const vector2& def) const { return def; }
        virtual real get_f1(const char* key, real def) const { return def; }
        virtual vector3 get_normal(const char* key) const { return get_f3(key); }
        virtual vector3 get_vector(const char* key) const { return get_f3(key); }
        virtual vector3 get_point(const char* key) const { return get_f3(key); }
        virtual vector3 get_color(const char* key) const { return get_f3(key); }
        virtual vector3 get_normal(const char* key, const vector3& def) const { return get_f3(key, def); }
        virtual vector3 get_vector(const char* key, const vector3& def) const { return get_f3(key, def); }
        virtual vector3 get_point(const char* key, const vector3& def) const { return get_f3(key, def); }
        virtual vector3 get_color(const char* key, const vector3& def) const { return get_f3(key, def); }

        virtual real get_float(const char* key) const { return get_f1(key); }
        virtual real get_float(const char* key, real def) const { return get_f1(key, def); }
        virtual std::string get_string(const char* key) const { return ""; }
        virtual std::string get_string(const char* key, const std::string& def) const { return ""; }
        virtual bool has_key(const char* key) const = 0;
        virtual size_t illuminance_size() const { return 0; }
        virtual vector3 illuminance_L(size_t i) const { return vector3(0, 0, 1); }
        virtual vector3 illuminance_Cl(size_t i) const { return vector3(0, 0, 0); }

        virtual vector3 ambient() const;
        virtual vector3 diffuse(const vector3& N) const;
        virtual vector3 specular(const vector3& N, const vector3& V, real roughness) const;
        virtual vector3 specularbrdf(const vector3& L, const vector3& N, const vector3& V, real roughness) const;
        virtual vector3 phong(const vector3& N, const vector3& V, real size) const;

        virtual vector3 indirectdiffuse(const vector3& P, const vector3& N, double samples) const { return vector3(0, 0, 1); }
        virtual real occlusion(const vector3& P, const vector3& N, double samples) const { return 0; }
        virtual real trace_f1(const vector3& P, const vector3& R) const { return 0; }
        virtual vector3 trace_f3(const vector3& P, const vector3& R) const { return vector3(0, 0, 1); }
        virtual vector3 transmission(const vector3& Psrc, const vector3& Pdst) const { return vector3(0, 0, 1); }

        virtual real texture_f1(const char* texturename) const { return texture_f1(texturename, s(), t()); }
        virtual real texture_f1(const char* texturename, real u, real v) const;
        virtual vector2 texture_f2(const char* texturename) const { return texture_f2(texturename, s(), t()); }
        virtual vector2 texture_f2(const char* texturename, real u, real v) const;
        virtual vector3 texture_f3(const char* texturename) const { return texture_f3(texturename, s(), t()); }
        virtual vector3 texture_f3(const char* texturename, real u, real v) const;

        virtual real environment_f1(const char* texturename, const vector3& V) const { return texture_f1(texturename, s(), t()); }
        virtual vector2 environment_f2(const char* texturename, const vector3& V) const { return texture_f2(texturename, s(), t()); }
        virtual vector3 environment_f3(const char* texturename, const vector3& V) const { return texture_f3(texturename, s(), t()); }

        virtual real shadow_f1(const char* texturename) const { return texture_f1(texturename, s(), t()); }
        virtual vector2 shadow_f2(const char* texturename) const { return texture_f2(texturename, s(), t()); }
        virtual vector3 shadow_f3(const char* texturename) const { return texture_f3(texturename, s(), t()); }

        //transform
        //vtransform
        //ntransform
        //deapth
        //calculatenormal

        //color trace( point P, point R )

        //
        //float atmosphere( string paramname; output type variable )
        //float displacement( string paramname; output type variable )
        //float lightsource( string paramname; output type variable )
        //float surface( string paramname; output type variable )
        //float incident( string paramname; output type variable )
        //float opposite( string paramname; output type variable )
        //float attribute ( string name; output type variable )
        //float option ( string name; output type variable )
        //float rendererinfo ( string name; output type variable )
        //string shadername ( )
        //string shadername ( string shadertype )
    };

    class shader_evaluator : public shader_functions
    {
    public:
        virtual ~shader_evaluator() {}
        virtual void evaluate(shader_output* out, const shader_input* in) const = 0;
    };
}

#endif
