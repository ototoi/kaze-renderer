#ifndef RSLE_KSL_VECTOR_H
#define RSLE_KSL_VECTOR_H

struct lua_State;

namespace rsle
{
    class ksl_vector
    {
    public:
        ksl_vector();
        ksl_vector(double x, double y);
        ksl_vector(double x, double y, double z);
        ksl_vector(double x, double y, double z, double w);
        double  operator[](int i) const { return e_[i]; }
        double& operator[](int i)       { return e_[i]; }
              double* ptr()     {return e_;}
        const double* ptr()const{return e_;}
        ksl_vector& operator+=(const ksl_vector& rhs);
        ksl_vector& operator-=(const ksl_vector& rhs);
        ksl_vector& operator*=(const ksl_vector& rhs);
        ksl_vector& operator/=(const ksl_vector& rhs);
        ksl_vector& operator*=(double rhs);
    private:
        double e_[4];
    };

    typedef ksl_vector ksl_vector2;
    typedef ksl_vector ksl_vector;
    typedef ksl_vector ksl_vector4;

    typedef ksl_vector ksl_vector;

    extern "C" int ksl_vector_open(lua_State* L);
}

#endif