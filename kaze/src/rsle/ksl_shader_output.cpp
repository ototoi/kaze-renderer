#include "ksl_shader_output.h"
#include "shader_evaluator.h"
#include "ksl_vector.h"

#ifdef __cplusplus
extern "C" {
#endif
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#ifdef __cplusplus
}
#endif

#if (LUA_VERSION_NUM == 502)
#undef luaL_register
#define luaL_register(L, n, f)      \
{                                   \
        if ((n) == NULL)            \
        {                           \
            luaL_setfuncs(L, f, 0); \
        }                           \
        else                        \
        {                           \
            luaL_newlib(L, f);      \
            lua_setglobal(L, n);    \
        }                           \
    }
#endif

#if (LUA_VERSION_NUM >= 502)
#define lua_objlen(L, i) lua_rawlen(L, i)
#endif

#define KLASS "shader_output"
#define KLASS_META "shader_output_mt"

namespace rsle
{

    static inline vector3 Convert(const ksl_vector& v)
    {
        return vector3(v[0], v[1], v[2]);
    }

    ksl_shader_output::ksl_shader_output(shader_output* ptr) : ptr_(ptr) {}

    void ksl_shader_output::Ci(const ksl_vector& p) { ptr_->Ci(Convert(p)); }
    void ksl_shader_output::Oi(const ksl_vector& p) { ptr_->Oi(Convert(p)); }
    void ksl_shader_output::set_f3(const char* key, const ksl_vector& p) { ptr_->set_f3(key, Convert(p)); }
    void ksl_shader_output::set_f1(const char* key, double f) { ptr_->set_f1(key, f); }
    //----------------------------------------------------------------

    static int ksl_shader_output_new(lua_State* L)
    {
        shader_output* s = (shader_output*)lua_topointer(L, -1);
        ksl_shader_output* v = (ksl_shader_output*)lua_newuserdata(L, sizeof(ksl_shader_output));
        v->set_ptr(s);
        luaL_getmetatable(L, KLASS_META);
        lua_setmetatable(L, -2);
        return 1;
    }

    static int ksl_shader_output_Ci(lua_State* L)
    {
        ksl_shader_output* self = (ksl_shader_output*)lua_touserdata(L, 1);
        const ksl_vector* v = (const ksl_vector*)lua_touserdata(L, 2);
        self->Ci(*v);
        return 0;
    }

    static int ksl_shader_output_Oi(lua_State* L)
    {
        ksl_shader_output* self = (ksl_shader_output*)lua_touserdata(L, 1);
        const ksl_vector* v = (const ksl_vector*)lua_touserdata(L, 2);
        self->Oi(*v);
        return 0;
    }

    static int ksl_shader_output_set_f1(lua_State* L)
    {
        ksl_shader_output* self = (ksl_shader_output*)lua_touserdata(L, 1);
        std::string key = lua_tostring(L, 2);
        double v = lua_tonumber(L, 3);
        self->set_f1(key.c_str(), v);
        return 0;
    }

    static int ksl_shader_output_set_f3(lua_State* L)
    {
        ksl_shader_output* self = (ksl_shader_output*)lua_touserdata(L, 1);
        std::string key = lua_tostring(L, 2);
        const ksl_vector* v = (const ksl_vector*)lua_touserdata(L, 3);
        self->set_f3(key.c_str(), *v);
        return 0;
    }

    //----------------------------------------------------------------

    static const luaL_Reg FUNCS_M[] =
        {
            {"Ci", ksl_shader_output_Ci},
            {"Oi", ksl_shader_output_Oi},
            {"set_f1", ksl_shader_output_set_f1},
            {"set_float", ksl_shader_output_set_f1},
            {"set_f3", ksl_shader_output_set_f3},
            {"set_point", ksl_shader_output_set_f3},
            {"set_vector", ksl_shader_output_set_f3},
            {"set_normal", ksl_shader_output_set_f3},
            {"set_color", ksl_shader_output_set_f3},
            {NULL, NULL}};

    static const luaL_Reg FUNCS_F[] =
        {
            {"Ci", ksl_shader_output_Ci},
            {"Oi", ksl_shader_output_Oi},
            {"set_f1", ksl_shader_output_set_f1},
            {"set_float", ksl_shader_output_set_f1},
            {"set_f3", ksl_shader_output_set_f3},
            {"set_point", ksl_shader_output_set_f3},
            {"set_vector", ksl_shader_output_set_f3},
            {"set_normal", ksl_shader_output_set_f3},
            {"set_color", ksl_shader_output_set_f3},
            {NULL, NULL}};

    int ksl_shader_output_open(lua_State* L)
    {
        luaL_newmetatable(L, KLASS_META);
        /*metatable.__index = metatable*/
        lua_pushvalue(L, -1); //clone metatable
        lua_setfield(L, -2, "__index");
        luaL_register(L, NULL, FUNCS_M);
        luaL_register(L, KLASS, FUNCS_F);

        lua_register(L, "shader_output", ksl_shader_output_new);

        lua_pop(L, lua_gettop(L));

        return 0;
    }
}
