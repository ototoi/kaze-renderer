#include "Marschner_shader_evaluator.h"
#include "values.h"
#include "newton_solver.h"
#include <cmath>

namespace rsle
{
    using namespace std;
    static const real PI = values::pi();
    static const real iPI = real(1.0) / PI;
    static const real PI3 = PI * PI * PI;

    static real radians(real x)
    {
        return values::radians(x);
    }

    /**
	 * Transform a vector by 3 basis vectors.
	 */
    static vector3 local_spherical(const vector3& v, const vector3& x, const vector3& y, const vector3& z)
    {
        real a = v[0] * x[0] + v[1] * y[0] + v[2] * z[0];
        real b = v[0] * x[1] + v[1] * y[1] + v[2] * z[1];
        real c = v[0] * x[2] + v[1] * y[2] + v[2] * z[2];

        return vector3(atan2(b, a), PI * 0.5 - acos(c), 0);
    }

    /**
	 * Unit-height zero-mean Gaussian function.
	 */
    static real gaussian(real x, real mu, real sigma)
    {
        return exp(-std::pow(x - mu, 2.0) * 0.5 / std::pow(sigma, 2.0));
    }

    static int solve(double root[], const double coeff[], int n, double x0, double x1)
    {
        static newton_solver ns;
        return ns.solve(root, coeff, n, x0, x1);
    }

    static int SolveCubic(real A, real B, real C, real D, real roots[4], real x0, real x1)
    {
        double dCoeff[4] = {A, B, C, D};
        double dRoot[4];
        int nCount = solve(dRoot, dCoeff, 3, x0, x1);
        for (int i = 0; i < nCount; i++)
        {
            roots[i] = (real)dRoot[i];
        }
        return nCount;
    }

    //Convert to Miller-Bravais index.
    static inline real pow2(real x)
    {
        return x * x;
    }

    static real BravaisIndex(real theta, real eta)
    {
        return sqrt(pow2(eta) - pow2(sin(theta))) / cos(theta); //
    }

    /// Evaluates the one dimensional gaussian function defined by a,b and c
    /// at position x. a is the height of the peak, b is the centre of the peak
    /// and c controls the width of the bell.
    static real Gaussian(real a, real b, real c, real x)
    {
        real o = x - b;
        return a * exp(-o * o / (2 * c * c));
    }

    static real Gaussian(real a, real b)
    {
        return exp(-(b * b) / (2 * a * a)) / (a * sqrt(2 * PI));
    }

    /// Computes the a, b and c parameters for a normalized gaussian pdf with the mean
    /// specifd by mu and a variance corresponding to sigma squared. This can then be
    /// evaluated using the Gaussian function above.
    static void GaussianPDF(real mu, real sigma, real& a, real& b, real& c)
    {
        a = 1 / (sigma * sqrt(2 * PI));
        b = mu;
        c = sigma;
    }

    static double clamp(double x, double a, double b)
    {
        if (x < a) return a;
        if (b < x) return b;
        return x;
    }

    static double smoothstep(double min, double max, double x)
    {
        if (x <= min) return 0;
        if (max <= x) return 1;
        x = (x - min) / (max - min);
        return (x * x * (double(3.0) - (double(2.0) * x)));
    }

    static double mod(double x, double m)
    {
        if (x < 0)
        {
            while (x < 0)
            {
                x += m;
            }
        }
        else
        {
            while (m <= x)
            {
                x -= m;
            }
        }
        return x;
    }

    // Computes reflectance fresnel with different index (eta) of iors for perpendicular and parallel polarized light.
    // Assumes the source media is vaccuum ( n = 1 ). If invert is non-zero, then assumes the target media is vaccuum.

    static real Fresnel(real angle, real n1, real n2)
    {
        real r = 1;
        real a = (n1 / n2) * sin(angle);
        a *= a;
        if (a <= 1)
        {
            real b = n2 * sqrt(1 - a);
            real c = n1 * cos(angle);
            r = (c - b) / (c + b);
            r *= r;
            r = std::min<real>(1, r);
        }
        return r;
    }

    static real MarschnerFresnel(real incidenceAngle, real iorPerp, real iorParal, bool invert)
    {
        real rPerp = 1;
        real rParal = 1;

        real angle = fabs(incidenceAngle);
        if (angle > PI / 2)
        {
            angle = PI - angle;
        }

        if (invert)
        {
            rPerp = Fresnel(angle, iorPerp, 1);
            rParal = Fresnel(angle, iorParal, 1);
        }
        else
        {
            rPerp = Fresnel(angle, 1, iorPerp);
            rParal = Fresnel(angle, 1, iorParal);
        }

        rPerp *= rPerp;
        rParal *= rParal;

        return 0.5 * (rPerp + rParal);
    }

    // computes a new ior index based on the hair eccentricity and the azimuth distance.
    static real MarschnerEccentricityRefraction(const real eccentricity, const real ior, const real averageAzimuth)
    {
        real n1 = 2 * (ior - 1) * eccentricity * eccentricity - ior + 2;
        real n2 = 2 * (ior - 1) / (eccentricity * eccentricity) - ior + 2;
        return ((n1 + n2) + cos(2 * averageAzimuth) * (n1 - n2)) / 2;
    }

    static real MarschnerExitAnglePolynomial(const real p, const real eta, const real h)
    {
        // use polynomial that approximates original equation.
        real gamma = asin(h);
        real c = asin(1 / eta);
        return (6 / PI * p * c - 2) * gamma - 8 / PI3 * p * c * gamma * gamma * gamma + p * PI;
    }

    static real MarschnerDExitAnglePolynomial(const real p, const real eta, const real h)
    {
        // computes the derivative of the polynomial relative to h.
        real gamma = asin(h);
        real c = asin(1 / eta);
        real dGamma = (6 / PI * p * c - 2) - 3 * 8 / PI3 * p * c * gamma * gamma;
        real denom = sqrt(1 - h * h);
        return dGamma / std::max<real>(1e-5, denom);
    }

    static real MarschnerDDExitAnglePolynomial(const real p, const real eta, const real h)
    {
        // computes the second derivative of the polynomial relative to h.
        real gamma = asin(h);
        real c = asin(1 / eta);
        real dGamma = -2 * 3 * 8 / PI * p * c * gamma;
        real denom = pow(1 - h * h, 3.0 / 2.0);
        return (dGamma * h) / std::max<real>(1e-5, denom);
    }

    static color3 MarschnerA0(const color3& absorption, const vector3& lightVec, real gammaI, real ior, real iorPerp, real iorParal)
    {
        real fresnel = Fresnel(gammaI, 1, ior);
        //real fresnel = MarschnerFresnel( gammaI, iorPerp, iorParal, false );
        return color3(fresnel, fresnel, fresnel);
    }

    static color3 MarschnerA1(const color3& absorption, const vector3& lightVec, real gammaI, real ior, real iorPerp, real iorParal)
    {
        real h = sin(gammaI);                                   // from [1] right before equation 3.
        real gammaT = asin(clamp(h / iorPerp, -1, 1));          // from [1] right before equation 3.
        real thetaT = acos((iorPerp / ior) * cos(lightVec[1])); // definition for equation 20 in [2].
        real cosTheta = cos(thetaT);
        real l = -2 * (1 + cos(2 * gammaT)) / cosTheta * 1; //TODO:replace robust function
        color3 segmentAbsorption = color3(exp(absorption[0] * l), exp(absorption[1] * l), exp(absorption[2] * l));
        segmentAbsorption[0] = std::max<real>(0, segmentAbsorption[0]);
        segmentAbsorption[1] = std::max<real>(0, segmentAbsorption[1]);
        segmentAbsorption[2] = std::max<real>(0, segmentAbsorption[2]);

        real fr = MarschnerFresnel(gammaI, iorPerp, iorParal, false);
        real ifr = MarschnerFresnel(gammaT, iorPerp, iorParal, true);
        real fresnel = (1 - fr) * (1 - ifr);
        return fresnel * segmentAbsorption;
    }

    static color3 MarschnerA2(const color3& absorption, const vector3& lightVec, real gammaI, real ior, real iorPerp, real iorParal)
    {
        real h = sin(gammaI);                                   // from [1] right before equation 3.
        real gammaT = asin(clamp(h / iorPerp, -1, 1));          // from [1] right before equation 3.
        real thetaT = acos((iorPerp / ior) * cos(lightVec[1])); // definition for equation 20 in [2].
        real cosTheta = cos(thetaT);
        real l = -2 * (1 + cos(2 * gammaT)) / cosTheta * 2; //TODO:replace robust function
        color3 segmentAbsorption = color3(exp(absorption[0] * l), exp(absorption[1] * l), exp(absorption[2] * l));
        segmentAbsorption[0] = std::max<real>(0, segmentAbsorption[0]);
        segmentAbsorption[1] = std::max<real>(0, segmentAbsorption[1]);
        segmentAbsorption[2] = std::max<real>(0, segmentAbsorption[2]);

        real fr = MarschnerFresnel(gammaI, iorPerp, iorParal, false);
        real ifr = MarschnerFresnel(gammaT, iorPerp, iorParal, true);
        real fresnel = (1 - fr) * ifr * (1 - ifr); //(1-fr)*pow(ifr,p-1)*(1-ifr);
        return fresnel * segmentAbsorption;
    }

    static color3 MarschnerA(const color3& absorption, const vector3& lightVec, int p, real gammaI, real ior, real iorPerp, real iorParal)
    {
        switch (p)
        { //0,1,2
        case 0:
            return MarschnerA0(absorption, lightVec, gammaI, ior, iorPerp, iorParal);
        case 1:
            return MarschnerA1(absorption, lightVec, gammaI, ior, iorPerp, iorParal);
        default:
            return MarschnerA2(absorption, lightVec, gammaI, ior, iorPerp, iorParal);
        }
    }

    static real MarschnerTargetAngle(int p, real relativeAzimuth)
    {
        real targetAngle = fabs(relativeAzimuth);

        // set right range to match polynomial representation of the real curve.
        if (p != 1)
        {
            // converts angles to range [-PI,PI]
            if (targetAngle > PI)
                targetAngle -= 2 * PI;

            // offset center
            targetAngle += p * PI;
        }
        return targetAngle;
    }

    static int MarschnerRoots(const real p, const real eta, const real targetAngle, real roots[])
    {
        real c = asin(1 / eta);
        int rootCount = SolveCubic(-8 / PI3 * p * c, 0, (6 / PI * p * c - 2), (p * PI - targetAngle), roots, -2 * PI, 2 * PI);
        return rootCount;
    }

    static color3 MarschnerNP(const color3& absorption, const vector3& lightVec, int p, real ior, real iorPerp, real iorParal, real targetAngle)
    {
        static const real denomMin = 1e-5;
        real roots[3] = {0, 0, 0};
        int rootCount = MarschnerRoots(p, iorPerp, targetAngle, roots); //

        color3 result = color3(0, 0, 0);
        for (int i = 0; i < rootCount; i++)
        {
            real gammaI = roots[i];
            if (fabs(gammaI) < PI / 2.0)
            {
                real h = sin(gammaI);
                color3 finalAbsorption = MarschnerA(absorption, lightVec, p, gammaI, ior, iorPerp, iorParal);
                real dexitAngle;
                dexitAngle = MarschnerDExitAnglePolynomial(p, iorPerp, h);
                real denom = std::max<real>(denomMin, 2.0 * fabs(dexitAngle));
                result += (finalAbsorption * real(1.0) / denom);
            }
        }
        return result;
    }

    static color3 MarschnerNTRT(const color3& absorption, const vector3& lightVec, const real ior, const real iorPerp, const real iorParal, const real targetAngle, const real causticLimit, const real causticWidth, const real glintScale, const real causticFade)
    {
        real dH, t, hc, Oc1, Oc2;
        if (iorPerp < 2)
        {
            real ddexitAngle;
            // compute roots of the polynomials derivative
            real c = asin(1 / iorPerp);
            real gammac = sqrt((6 * 2 / PI * c - 2) / (3 * 8 * 2 / PI3 * c));
            hc = fabs(sin(gammac));
            hc = clamp(hc, -1, +1);
            ddexitAngle = MarschnerDDExitAnglePolynomial(2, iorPerp, hc);
            dH = std::min<real>(causticLimit, 2 * sqrt(2 * causticWidth / fabs(ddexitAngle)));
            t = 1;
        }
        else
        {
            hc = 0;
            dH = causticLimit;
            t = 1 - smoothstep(2, 2 + causticFade, iorPerp);
        }

        Oc1 = MarschnerExitAnglePolynomial(2, iorPerp, hc);
        Oc2 = MarschnerExitAnglePolynomial(2, iorPerp, -hc);

        real a, b, c;
        GaussianPDF(0, causticWidth, a, b, c);
        real causticCenter = Gaussian(a, b, c, 0);
        real causticLeft = Gaussian(a, b, c, targetAngle - Oc1);
        real causticRight = Gaussian(a, b, c, targetAngle - Oc2);
        color3 glintAbsorption = MarschnerA(absorption, lightVec, 2, asin(hc), ior, iorPerp, iorParal);

        color3 result = MarschnerNP(absorption, lightVec, 2, ior, iorPerp, iorParal, targetAngle);
        result *= (1 - t * causticLeft / causticCenter);
        result *= (1 - t * causticRight / causticCenter);
        result += glintAbsorption * t * glintScale * dH * (causticLeft + causticRight);
        return result;
    }

    static real MarschnerM(real shift, real width, real normWidth, real x)
    {
        real a, b, c;
        real norm = 1.0 / (normWidth * sqrt(2 * PI));
        GaussianPDF(shift, width, a, b, c);
        return (Gaussian(a, b, c, x) / a) * norm;

        //return Gaussian(width, x-shift);
    }

    static color3 Marschner(
        real phi,
        real thetaD,
        real thetaH,
        real ior,
        const vector3& omegaI,
        const color3& absorption,
        const real eccentricity,
        const real shiftR, const real shiftTT, const real shiftTRT, //Angle :alpha
        const real widthR, const real widthTT, const real widthTRT, //Angle :beta
                                                                    // parameters for the caustic treatment
        const real causticLimit,
        const real causticWidth, //Angle
        const real glintScale,
        const real causticFade,
        // output factors R, TT and TRT
        color3& R,
        color3& TT,
        color3& TRT)
    {
        //convert to Bravais Index
        real iorPerp = BravaisIndex(thetaD, ior);
        real iorParal = (ior * ior) / iorPerp;

        // get ior indices modifd by the eccentricity to use in TRT
        real iorTRT = MarschnerEccentricityRefraction(eccentricity, ior, phi);
        real iorPerpTRT = BravaisIndex(thetaD, iorTRT);
        real iorParalTRT = (iorTRT * iorTRT) / iorPerpTRT;
        real averageTheta = thetaH;
        real relativeAzimuth = thetaD;

        static const real normWidth = values::radians(1);

        real MR = MarschnerM(shiftR, widthR, normWidth, averageTheta);
        real MTT = MarschnerM(shiftTT, widthTT, normWidth * 0.5, averageTheta);
        real MTRT = MarschnerM(shiftTRT, widthTRT, normWidth * 2.0, averageTheta);

        color3 NR = MarschnerNP(absorption, omegaI, 0, ior, iorPerp, iorParal, MarschnerTargetAngle(0, relativeAzimuth));
        R = MR * NR;

        color3 NTT = MarschnerNP(absorption, omegaI, 1, ior, iorPerp, iorParal, MarschnerTargetAngle(1, relativeAzimuth));
        TT = MTT * NTT;

        color3 NTRT = MarschnerNTRT(absorption, omegaI, iorTRT, iorPerpTRT, iorParalTRT, MarschnerTargetAngle(2, relativeAzimuth), causticLimit, causticWidth, glintScale, causticFade);
        TRT = MTRT * NTRT;

        // Normalize.
        //
        real cosThetaD = cos(thetaD);
        real scale = 1.0 / pow(cosThetaD, 2);

        //TRT
        return (R + TT + TRT); // * scale;
    }

    Marschner_shader_evaluator::Marschner_shader_evaluator()
    {
        ior_ = 1.55;         //
        eccentricity_ = 0.9; //0.85 to 1
        shiftR_ = -7.5;      //longitudinal shift: R lobe -10 to -5
        shiftTT_ = 3.75;     //
        shiftTRT_ = 11.25;   //
        widthR_ = 7.5;       // 5 to 10
        widthTT_ = 3.75;     //
        widthTRT_ = 15;      //

        causticLimit_ = 0.5;  //0.5
        causticWidth_ = 17.5; //10 to 25
        glintScale_ = 2.75;   //0.5 to 5
        causticFade_ = 0.3;   //0.2 ot 0.4
    }

    Marschner_shader_evaluator::~Marschner_shader_evaluator()
    {
    }

    void Marschner_shader_evaluator::evaluate(shader_output* out, const shader_input* in) const
    {
        real ior = ior_;
        real eccentricity = eccentricity_;
        real shiftR = shiftR_;
        real shiftTT = shiftTT_;
        real shiftTRT = shiftTRT_;
        real widthR = widthR_;
        real widthTT = widthTT_;
        real widthTRT = widthTRT_;

        real causticLimit = causticLimit_;
        real causticWidth = causticWidth_;
        real glintScale = glintScale_;
        real causticFade = causticFade_;

        real intensityD = 1.0;          //TODO
        real attenuationFromRoot = 1.0; //TODO

        //
        vector3 dPdv = in->dPdv();
        vector3 N = in->N();
        vector3 I = in->I();

        color3 Cs = in->Cs();
        color3 Os = in->Os();

        vector2 uv = in->uv();
        real u = uv[0];
        real v = uv[1];

        // Calcuate mixed diffuse color over curve in [0,1].
        //
        //real mixingRatio = spline(v, 0, middleD, middleD + middleRangeD, 1);
        //color3 mixedcolorD = mix(colorRootD, colorTipD, mixingRatio);
        color3 mixedcolorD = Cs;
        color3 absorption = vector3(0.1, 0.1, 0.1);

        // Prepare vectors.
        //
        vector3 T = normalize(dPdv);
        vector3 Nn = normalize(N);
        vector3 B = normalize(cross(Nn, T));
        vector3 Vn = -normalize(I);

        vector3 omegaO = local_spherical(Vn, B, Nn, T);

        color3 diffuseResult = color3(0, 0, 0);
        color3 scatteringResult = color3(0, 0, 0);

        for (size_t i = 0; i < in->illuminance_size(); i++)
        {
            vector3 L = in->illuminance_L(i);
            color3 Cl = in->illuminance_Cl(i);

            // Get \omega_o
            //
            vector3 Ln = normalize(L);
            vector3 omegaI = local_spherical(Ln, B, Nn, T);

            // phi is in [-PI, PI]
            //
            real phi = abs(omegaO[0] - omegaI[0]);
            if (phi > PI)
                phi -= 2 * PI;

            real thetaD = abs(omegaO[1] - omegaI[1]) * 0.5;
            real thetaH = (omegaO[1] + omegaI[1]) * 0.5;

            // Calculate the diffuse contribution.
            //
            real sinTL = sqrt(1.0 - pow(dot(T, Ln), 2.0));
            diffuseResult += Cl * intensityD * mixedcolorD * sinTL;

            // Get the scattering contribution.
            //
            color3 R = color3(0, 0, 0);
            color3 TT = color3(0, 0, 0);
            color3 TRT = color3(0, 0, 0);
            scatteringResult += Cl * Marschner(phi, thetaD, thetaH, ior,
                                               omegaI,
                                               absorption,
                                               eccentricity,
                                               shiftR, shiftTT, shiftTRT,
                                               widthR, widthTT, widthTRT,
                                               causticLimit,
                                               causticWidth,
                                               glintScale,
                                               causticFade,
                                               R, TT, TRT);
        }

        color3 Ci = (diffuseResult + scatteringResult) * pow(v, attenuationFromRoot);
        color3 Oi = Os;

        out->Ci(Ci);
        out->Oi(Oi);
    }

    void Marschner_shader_evaluator::set(const char* key, real x)
    {
        ; //
    }

    void Marschner_shader_evaluator::set(const char* key, const color3& x)
    {
        ; //
    }
}