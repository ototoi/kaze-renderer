#include "constant_shader_evaluator.h"

namespace rsle
{
    void constant_shader_evaluator::evaluate(shader_output* out, const shader_input* in) const
    {
        color3 Cs = in->Cs();
        color3 Os = in->Os();
        out->Ci(Cs * Os);
        out->Oi(Os);
    }
}
