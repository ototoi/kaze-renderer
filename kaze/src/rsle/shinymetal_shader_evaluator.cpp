#include "shinymetal_shader_evaluator.h"
#include <string>

/*
surface
shinymetal (float Ka = 1, Ks = 1, Kr = 1;
	    float roughness = .1;
	    string texturename = "";)
{
    vector V = normalize(I);
    normal Nf = faceforward (normalize(N),V);
    vector D = vtransform ("world", reflect (V, Nf));

    color env;
    if (texturename != "")
	env = Kr * color environment (texturename, D);
    else env = 0;

	Oi = Os;
	Ci = Oi * (Ka * ambient() +
		Ks * (ev + specular(Nf, -V, roughness)));

    Oi = Os;
    Ci = Os * Cs 
}
*/

namespace rsle
{

    void shinymetal_shader_evaluator::evaluate(shader_output* out, const shader_input* in) const
    {
        real Ka = in->get_float("Ka", 1.0);
        real Ks = in->get_float("Ks", 1.0);
        real Kr = in->get_float("Kr", 1.0);
        real roughness = in->get_float("roughness", 0.1);
        std::string texturename = in->get_string("texturename", "");

        color3 Cs = in->Cs();
        color3 Os = in->Os();

        vector3 N = in->N();
        vector3 I = in->I();

        vector3 Nf = faceforward(normalize(N), I);
        vector3 V = normalize(I);
        vector3 D = in->reflect(V, Nf);

        color3 env;
        if (texturename != "")
        {
            env = Kr * in->environment_f3(texturename.c_str(), D);
        }
        else
        {
            env = color3(0, 0, 0);
        }

        color3 Oi = Os;
        color3 Ci = Os * Cs * (Ka * in->ambient() + Ks * (env + in->specular(Nf, -V, roughness)));

        out->Ci(Ci);
        out->Oi(Oi);
    }
}
