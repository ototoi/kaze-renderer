//float radians(float degrees);
static int ksl_shader_input_radians_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->radians(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float degrees(float radians);
static int ksl_shader_input_degrees_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->degrees(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float sin(float a);
static int ksl_shader_input_sin_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->sin(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float asin(float a);
static int ksl_shader_input_asin_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->asin(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float cos(float a);
static int ksl_shader_input_cos_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->cos(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float acos(float a);
static int ksl_shader_input_acos_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->acos(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float tan(float a);
static int ksl_shader_input_tan_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->tan(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float atan(float yoverx);
static int ksl_shader_input_atan_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->atan(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float atan(float y, float x);
static int ksl_shader_input_atan_f1_f1f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       a2 = lua_tonumber(L, 3);
    const double       r1 = self->atan(a1, a2);
    lua_pushnumber(L, r1);
    return 1;
}

//float pow(float x, float y);
static int ksl_shader_input_pow_f1_f1f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       a2 = lua_tonumber(L, 3);
    const double       r1 = self->pow(a1, a2);
    lua_pushnumber(L, r1);
    return 1;
}

//float exp(float x);
static int ksl_shader_input_exp_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->exp(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float sqrt(float x);
static int ksl_shader_input_sqrt_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->sqrt(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float inversesqrt(float x);
static int ksl_shader_input_inversesqrt_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->inversesqrt(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float log(float x);
static int ksl_shader_input_log_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->log(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float log(float x, float base);
static int ksl_shader_input_log_f1_f1f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       a2 = lua_tonumber(L, 3);
    const double       r1 = self->log(a1, a2);
    lua_pushnumber(L, r1);
    return 1;
}

//float mod(float a, float b);
static int ksl_shader_input_mod_f1_f1f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       a2 = lua_tonumber(L, 3);
    const double       r1 = self->mod(a1, a2);
    lua_pushnumber(L, r1);
    return 1;
}

//float abs(float x);
static int ksl_shader_input_abs_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->abs(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float sign(float x);
static int ksl_shader_input_sign_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->sign(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float min(array<float> x);
static int ksl_shader_input_min_f1_fa(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    int sz = lua_rawlen(L, 2);
    std::vector<double> a1(sz);
    for(int i_=1;i_<=sz;i_++)
    {
        lua_pushinteger(L, i_);
        lua_gettable(L, 2);
        a1[i_-1]=lua_tonumber(L, -1);
        lua_pop(L, 1);
    }
    const double       r1 = self->min(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float min(float a, float b);
static int ksl_shader_input_min_f1_f1f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       a2 = lua_tonumber(L, 3);
    const double       r1 = self->min(a1, a2);
    lua_pushnumber(L, r1);
    return 1;
}

//vector min(vector a, vector b);
static int ksl_shader_input_min_f3_f3f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const ksl_vector   r1 = self->min(a1, a2);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//float max(float a, float b);
static int ksl_shader_input_max_f1_f1f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       a2 = lua_tonumber(L, 3);
    const double       r1 = self->max(a1, a2);
    lua_pushnumber(L, r1);
    return 1;
}

//vector max(vector a, vector b);
static int ksl_shader_input_max_f3_f3f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const ksl_vector   r1 = self->max(a1, a2);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//float clamp(float a, float min, float max);
static int ksl_shader_input_clamp_f1_f1f1f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       a2 = lua_tonumber(L, 3);
    const double       a3 = lua_tonumber(L, 4);
    const double       r1 = self->clamp(a1, a2, a3);
    lua_pushnumber(L, r1);
    return 1;
}

//vector clamp(vector a, vector min, vector max);
static int ksl_shader_input_clamp_f3_f3f3f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const ksl_vector&  a3 = *((const ksl_vector*)lua_touserdata(L, 4));
    const ksl_vector   r1 = self->clamp(a1, a2, a3);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//float mix(float x, float y, float alpha);
static int ksl_shader_input_mix_f1_f1f1f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       a2 = lua_tonumber(L, 3);
    const double       a3 = lua_tonumber(L, 4);
    const double       r1 = self->mix(a1, a2, a3);
    lua_pushnumber(L, r1);
    return 1;
}

//vector mix(vector x, vector y, float alpha);
static int ksl_shader_input_mix_f3_f3f3f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const double       a3 = lua_tonumber(L, 4);
    const ksl_vector   r1 = self->mix(a1, a2, a3);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//float floor(float x);
static int ksl_shader_input_floor_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->floor(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float ceil(float x);
static int ksl_shader_input_ceil_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->ceil(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float round(float x);
static int ksl_shader_input_round_f1_f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       r1 = self->round(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float step(float min, float value);
static int ksl_shader_input_step_f1_f1f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       a2 = lua_tonumber(L, 3);
    const double       r1 = self->step(a1, a2);
    lua_pushnumber(L, r1);
    return 1;
}

//float smoothstep(float min, float max, float value);
static int ksl_shader_input_smoothstep_f1_f1f1f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const double       a1 = lua_tonumber(L, 2);
    const double       a2 = lua_tonumber(L, 3);
    const double       a3 = lua_tonumber(L, 4);
    const double       r1 = self->smoothstep(a1, a2, a3);
    lua_pushnumber(L, r1);
    return 1;
}

//float xcomp(vector P);
static int ksl_shader_input_xcomp_f1_f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const double       r1 = self->xcomp(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float ycomp(vector P);
static int ksl_shader_input_ycomp_f1_f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const double       r1 = self->ycomp(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//float zcomp(vector P);
static int ksl_shader_input_zcomp_f1_f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const double       r1 = self->zcomp(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//vector reflect(vector I, vector N);
static int ksl_shader_input_reflect_f3_f3f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const ksl_vector   r1 = self->reflect(a1, a2);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector refract(vector I, vector N, float eta);
static int ksl_shader_input_refract_f3_f3f3f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const double       a3 = lua_tonumber(L, 4);
    const ksl_vector   r1 = self->refract(a1, a2, a3);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector transform(string tospace, vector p);
static int ksl_shader_input_transform_f3_sxf3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  a1 = lua_tostring(L, 2);
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const ksl_vector   r1 = self->transform(a1, a2);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector transform(string fromspace, string tospace, vector p);
static int ksl_shader_input_transform_f3_sxsxf3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  a1 = lua_tostring(L, 2);
    const std::string  a2 = lua_tostring(L, 3);
    const ksl_vector&  a3 = *((const ksl_vector*)lua_touserdata(L, 4));
    const ksl_vector   r1 = self->transform(a1, a2, a3);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector transform(matrix m, vector p);
static int ksl_shader_input_transform_f3_m4f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_matrix&  a1 = *((const ksl_matrix*)lua_touserdata(L, 2));
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const ksl_vector   r1 = self->transform(a1, a2);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector transform(string fromspace, matrix m, vector p);
static int ksl_shader_input_transform_f3_sxm4f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  a1 = lua_tostring(L, 2);
    const ksl_matrix&  a2 = *((const ksl_matrix*)lua_touserdata(L, 3));
    const ksl_vector&  a3 = *((const ksl_vector*)lua_touserdata(L, 4));
    const ksl_vector   r1 = self->transform(a1, a2, a3);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector vtransform(string tospace, vector v);
static int ksl_shader_input_vtransform_f3_sxf3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  a1 = lua_tostring(L, 2);
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const ksl_vector   r1 = self->vtransform(a1, a2);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector vtransform(string fromspace, string tospace, vector v);
static int ksl_shader_input_vtransform_f3_sxsxf3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  a1 = lua_tostring(L, 2);
    const std::string  a2 = lua_tostring(L, 3);
    const ksl_vector&  a3 = *((const ksl_vector*)lua_touserdata(L, 4));
    const ksl_vector   r1 = self->vtransform(a1, a2, a3);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector vtransform(matrix m, vector v);
static int ksl_shader_input_vtransform_f3_m4f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_matrix&  a1 = *((const ksl_matrix*)lua_touserdata(L, 2));
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const ksl_vector   r1 = self->vtransform(a1, a2);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector vtransform(string fromspace, matrix m, vector v);
static int ksl_shader_input_vtransform_f3_sxm4f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  a1 = lua_tostring(L, 2);
    const ksl_matrix&  a2 = *((const ksl_matrix*)lua_touserdata(L, 3));
    const ksl_vector&  a3 = *((const ksl_vector*)lua_touserdata(L, 4));
    const ksl_vector   r1 = self->vtransform(a1, a2, a3);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector ntransform(string tospace, vector n);
static int ksl_shader_input_ntransform_f3_sxf3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  a1 = lua_tostring(L, 2);
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const ksl_vector   r1 = self->ntransform(a1, a2);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector ntransform(string fromspace, string tospace, vector n);
static int ksl_shader_input_ntransform_f3_sxsxf3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  a1 = lua_tostring(L, 2);
    const std::string  a2 = lua_tostring(L, 3);
    const ksl_vector&  a3 = *((const ksl_vector*)lua_touserdata(L, 4));
    const ksl_vector   r1 = self->ntransform(a1, a2, a3);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector ntransform(matrix m, vector n);
static int ksl_shader_input_ntransform_f3_m4f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_matrix&  a1 = *((const ksl_matrix*)lua_touserdata(L, 2));
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const ksl_vector   r1 = self->ntransform(a1, a2);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector ntransform(string fromspace, matrix m, vector n);
static int ksl_shader_input_ntransform_f3_sxm4f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  a1 = lua_tostring(L, 2);
    const ksl_matrix&  a2 = *((const ksl_matrix*)lua_touserdata(L, 3));
    const ksl_vector&  a3 = *((const ksl_vector*)lua_touserdata(L, 4));
    const ksl_vector   r1 = self->ntransform(a1, a2, a3);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//float depth(vector P);
static int ksl_shader_input_depth_f1_f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const double       r1 = self->depth(a1);
    lua_pushnumber(L, r1);
    return 1;
}

//vector calculatenormal(vector P);
static int ksl_shader_input_calculatenormal_f3_f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const ksl_vector   r1 = self->calculatenormal(a1);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//float comp(vector c, float index);
static int ksl_shader_input_comp_f1_f3f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const double       a2 = lua_tonumber(L, 3);
    const double       r1 = self->comp(a1, a2);
    lua_pushnumber(L, r1);
    return 1;
}

//vector ctransform(string tospace, vector C);
static int ksl_shader_input_ctransform_f3_sxf3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  a1 = lua_tostring(L, 2);
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const ksl_vector   r1 = self->ctransform(a1, a2);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector ctransform(string fromspace, string tospace, vector C);
static int ksl_shader_input_ctransform_f3_sxsxf3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  a1 = lua_tostring(L, 2);
    const std::string  a2 = lua_tostring(L, 3);
    const ksl_vector&  a3 = *((const ksl_vector*)lua_touserdata(L, 4));
    const ksl_vector   r1 = self->ctransform(a1, a2, a3);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//string concat(array<string> s);
static int ksl_shader_input_concat_sx_sa(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    int sz = lua_rawlen(L, 2);
    std::vector<std::string> a1(sz);
    for(int i_=1;i_<=sz;i_++)
    {
        lua_pushinteger(L, i_);
        lua_gettable(L, 2);
        a1[i_-1]=lua_tostring(L, -1);
        lua_pop(L, 1);
    }
    const std::string  r1 = self->concat(a1);
    lua_pushstring(L, r1.c_str());
    return 1;
}

//float match(string pattern, string subject);
static int ksl_shader_input_match_f1_sxsx(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  a1 = lua_tostring(L, 2);
    const std::string  a2 = lua_tostring(L, 3);
    const double       r1 = self->match(a1, a2);
    lua_pushnumber(L, r1);
    return 1;
}

//vector ambient();
static int ksl_shader_input_ambient_f3_z0(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector   r1 = self->ambient();
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector diffuse(vector N);
static int ksl_shader_input_diffuse_f3_f3(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const ksl_vector   r1 = self->diffuse(a1);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector specular(vector N, vector V, float roughness);
static int ksl_shader_input_specular_f3_f3f3f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const double       a3 = lua_tonumber(L, 4);
    const ksl_vector   r1 = self->specular(a1, a2, a3);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector specularbrdf(vector L, vector N, vector V, float roughness);
static int ksl_shader_input_specularbrdf_f3_f3f3f3f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const ksl_vector&  a3 = *((const ksl_vector*)lua_touserdata(L, 4));
    const double       a4 = lua_tonumber(L, 5);
    const ksl_vector   r1 = self->specularbrdf(a1, a2, a3, a4);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//vector phong(vector N, vector V, float size);
static int ksl_shader_input_phong_f3_f3f3f1(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const ksl_vector&  a1 = *((const ksl_vector*)lua_touserdata(L, 2));
    const ksl_vector&  a2 = *((const ksl_vector*)lua_touserdata(L, 3));
    const double       a3 = lua_tonumber(L, 4);
    const ksl_vector   r1 = self->phong(a1, a2, a3);
    lua_getglobal(L, "vector_from_ptr");
    lua_pushlightuserdata(L, (void*)&r1);
    lua_pcall(L, 1, 1, 0);
    return 1;
}

//string shadername();
static int ksl_shader_input_shadername_sx_z0(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  r1 = self->shadername();
    lua_pushstring(L, r1.c_str());
    return 1;
}

//string shadername(string shadertype);
static int ksl_shader_input_shadername_sx_sx(lua_State* L)
{
    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
    const std::string  a1 = lua_tostring(L, 2);
    const std::string  r1 = self->shadername(a1);
    lua_pushstring(L, r1.c_str());
    return 1;
}

