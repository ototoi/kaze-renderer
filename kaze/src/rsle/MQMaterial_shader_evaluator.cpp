#include "MQMaterial_shader_evaluator.h"

namespace rsle
{

    static real get_diffuse(const shader_input* in)
    {
        real Diffuse = in->get_float("Diffuse", 1.0);

        vector3 N = in->N();
        vector3 I = in->I();

        vector3 Nf = in->faceforward(normalize(N), I);

        return Diffuse * in->diffuse(Nf)[0];
    }

    static vector3 reflect(const vector3& I, const vector3& N)
    {
        return I - 2 * dot(I, N) * N;
    }

    static color3 specularbrdf(const vector3& L, const vector3& N, const vector3& V, real power)
    {
        vector3 R = reflect(-normalize(V), normalize(N));
        //vector3 H = normalize(L+V);
        vector3 Ln = normalize(L);
        real c = pow(std::max(0.0, dot(R, Ln)), power);
        return color3(c, c, c);
    }

    static color3 get_specular(const shader_input* in)
    {
        real Specular = in->get_float("Specular", 1.0);
        real Power = in->get_float("Power", 50.0);

        vector3 N = in->N();
        vector3 I = in->I();

        vector3 Nf = in->faceforward(normalize(N), I);
        vector3 V = -normalize(I);

        color3 C = color3(0, 0, 0);
        size_t sz = in->illuminance_size();
        for (size_t i = 0; i < sz; i++)
        {
            color3 Cl = in->illuminance_Cl(i);
            vector3 L = in->illuminance_L(i);
            //if(dot(L,N)>=0){
            C += Cl * specularbrdf(normalize(L), Nf, V, Power);
            //}
        }
        return Specular * C;
    }

    static void eval_mq(shader_output* out, const shader_input* in)
    {
        color3 Color = in->get_f3("Color", vector3(1, 1, 1));
        real Alpha = in->get_float("Alpha", 1.0);
        //real Diffuse  = in->get_float("Diffuse",  1.0);
        real Ambient = in->get_float("Ambient", 1.0);
        real Emission = in->get_float("Emission", 1.0);
        //real Specular = in->get_float("Specular", 1.0);
        //real Power    = in->get_float("Power",    50.0);

        std::string TextureName = in->get_string("TextureName");
        std::string AlphaName = in->get_string("AlphaName");
        std::string BumpName = in->get_string("BumpName");

        if (TextureName != "")
        {
            Color = Color * in->texture_f3(TextureName.c_str());
        }
        if (AlphaName != "")
        {
            Alpha = Alpha * in->texture_f1(AlphaName.c_str());
        }

        real Diffuse = get_diffuse(in);
        color3 Specular = get_specular(in);

        color3 Oi = color3(Alpha, Alpha, Alpha);
        color3 Ci = (Diffuse + Emission) * Color + Ambient * in->ambient() + Specular;

        out->Ci(Ci * Oi);
        out->Oi(Oi);
    }

    /*
    static
    void eval_plastic(shader_output* out, const shader_input* in)
    {
        real Ka = in->get_float("Ka", 1.0);
		real Kd = in->get_float("Kd", 0.5);
		real Ks = in->get_float("Ks", 0.5);
		real roughness = in->get_float("roughness", 0.1);
		color3 specularcolor = in->get_color("specularcolor", color3(1,1,1));

		color3 Cs = in->Cs();
		color3 Os = in->Os();

		vector3 N = in->N();
		vector3 I = in->I();

		vector3 Nf = in->faceforward (normalize(N),I);

		color3 Oi = Os;
		color3 Ci = Os * ( Cs * (Ka*in->ambient() + Kd*in->diffuse(Nf)) +
		specularcolor * Ks * in->specular(Nf,-normalize(I),roughness));

		out->Ci(Ci);
		out->Oi(Oi);
    }
    */

    void MQMaterial_shader_evaluator::evaluate(shader_output* out, const shader_input* in) const
    {
        eval_mq(out, in);
    }
}