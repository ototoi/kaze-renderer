#ifndef RSLE_KSL_SHADER_EVALUATOR_H
#define RSLE_KSL_SHADER_EVALUATOR_H

#include "shader_evaluator.h"
#include "types.h"

namespace rsle
{
    class ksl_shader_evaluator_imp;
    class ksl_shader_evaluator : public shader_evaluator
    {
    public:
        ksl_shader_evaluator(const char* szFile);
        ~ksl_shader_evaluator();

    public:
        virtual void evaluate(shader_output* out, const shader_input* in) const;
    private:
        ksl_shader_evaluator_imp* imp_;
    };
}

#endif