#include "plastic_shader_evaluator.h"

#include <math.h>

/*
surface
plastic (float Ka = 1;
         float Kd = .5;
         float Ks = .5;
         float roughness = .1;
	 color specularcolor = 1;)
{
    normal Nf = faceforward (normalize(N),I);
    Oi = Os;
    Ci = Os * ( Cs * (Ka*ambient() + Kd*diffuse(Nf)) +
		specularcolor * Ks*specular(Nf,-normalize(I),roughness));
}
*/

namespace rsle
{
    void plastic_shader_evaluator::evaluate(shader_output* out, const shader_input* in) const
    {
        real Ka = in->get_float("Ka", 1.0);
        real Kd = in->get_float("Kd", 0.5);
        real Ks = in->get_float("Ks", 0.5);
        real roughness = in->get_float("roughness", 0.1);
        color3 specularcolor = in->get_color("specularcolor", color3(1, 1, 1));

        color3 Cs = in->Cs();
        color3 Os = in->Os();

        vector3 N = in->N();
        vector3 I = in->I();

        vector3 Nf = faceforward(normalize(N), I);

        color3 Oi = Os;
        color3 Ci = Os * (Cs * (Ka * in->ambient() + Kd * in->diffuse(Nf)) +
                          specularcolor * Ks * in->specular(Nf, -normalize(I), roughness));

        out->Ci(Ci);
        out->Oi(Oi);
    }
}
