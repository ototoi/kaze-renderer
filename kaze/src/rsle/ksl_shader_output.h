#ifndef RSLE_KSL_SHADER_OUTPUT_H
#define RSLE_KSL_SHADER_OUTPUT_H

#include "ksl_vector.h"
#include "shader_evaluator.h"

namespace rsle
{
    class shader_output;

    class ksl_shader_output
    {
    public:
        ksl_shader_output() : ptr_(NULL) {}
        ksl_shader_output(shader_output* ptr);

    public:
        void Ci(const ksl_vector& p);
        void Oi(const ksl_vector& p);
        void set_f3(const char* key, const ksl_vector& p);
        void set_f1(const char* key, double f);

    public:
        void set_ptr(shader_output* ptr) { ptr_ = ptr; }
    private:
        shader_output* ptr_;
    };

    typedef ksl_shader_output ksl_shader_output;

    extern "C" int ksl_shader_output_open(lua_State* L);
}

#endif
