#ifndef RSLE_CONSTANT_SHADER_EVALUATOR_H
#define RSLE_CONSTANT_SHADER_EVALUATOR_H

#include "shader_evaluator.h"

namespace rsle
{
    class constant_shader_evaluator : public shader_evaluator
    {
    public:
        virtual void evaluate(shader_output* out, const shader_input* in) const;
    };
}

#endif