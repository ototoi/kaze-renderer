#ifndef RSLE_KSL_SHADER_INPUT_H
#define RSLE_KSL_SHADER_INPUT_H

#include "ksl_vector.h"
#include "shader_evaluator.h"

namespace rsle
{

    class ksl_matrix4
    {
    public:
        int dummy;
    };

    typedef ksl_matrix4 ksl_matrix;

    class shader_input;

    class ksl_shader_input
    {
    public:
        ksl_shader_input() : ptr_(NULL) {}
        ksl_shader_input(const shader_input* ptr);

    public:
        ksl_vector P() const;
        ksl_vector N() const;
        ksl_vector Ng() const;
        ksl_vector E() const;
        ksl_vector I() const;
        ksl_vector dPdu() const;
        ksl_vector dPdv() const;
        ksl_vector uvw() const;
        ksl_vector stw() const;
        ksl_vector Cs() const;
        ksl_vector Os() const;

    public:
        double u() const;
        double v() const;
        double s() const;
        double t() const;

    public:
        double get_f1(const char* key) const;
        double get_f1(const char* key, double def) const;
        ksl_vector get_f3(const char* key) const;
        ksl_vector get_f3(const char* key, const ksl_vector& def) const;
        std::string get_string(const char* key) const;
        std::string get_string(const char* key, const std::string& def) const;

    public:
        static double random_f1();
        static ksl_vector random_f3();

    public:
        static double radians(double degrees);
        static double degrees(double radians);
        static double sin(double a);
        static double asin(double a);
        static double cos(double a);
        static double acos(double a);
        static double tan(double a);
        static double atan(double yoverx);
        static double atan(double y, double x);

    public:
        static double pow(double x, double y);
        static double exp(double x);
        static double sqrt(double x);
        static double inversesqrt(double x);
        static double log(double x);
        static double log(double x, double base);

    public:
        static double mod(double a, double b);
        static double abs(double x);
        static double sign(double x);
        static double floor(double x);
        static double ceil(double x);
        static double round(double x);
        static double fract(double x);

    public:
        static double min(double a, double b);
        static double max(double a, double b);
        static double clamp(double x, double a, double b);
        static double mix(double x, double y, double alpha);
        static double step(double min, double value);
        static double smoothstep(double min, double max, double value);

        static double min(const std::vector<double>& v){return 0;}//TODO
        static double max(const std::vector<double>& v){return 0;}//TODO

        static ksl_vector clamp(const ksl_vector& x, const ksl_vector& a, const ksl_vector& b);
        static ksl_vector mix  (const ksl_vector& x, const ksl_vector& y, double alpha){return mix(x,y,ksl_vector(alpha,alpha,alpha));}
        static ksl_vector mix  (const ksl_vector& x, const ksl_vector& y, const ksl_vector& alpha);
        static ksl_vector step(const ksl_vector& min, const ksl_vector& value);
        static ksl_vector smoothstep(const ksl_vector& min, const ksl_vector& max, const ksl_vector& value);

    public:
#define DEFFUNCS1(FUNC) \
    static ksl_vector FUNC(const ksl_vector& x) { return ksl_vector(FUNC(x[0]), FUNC(x[1]), FUNC(x[2])); }

        DEFFUNCS1(radians)
        DEFFUNCS1(degrees)
        DEFFUNCS1(sin)
        DEFFUNCS1(asin)
        DEFFUNCS1(cos)
        DEFFUNCS1(acos)
        DEFFUNCS1(tan)
        DEFFUNCS1(atan)
        DEFFUNCS1(exp)
        DEFFUNCS1(sqrt)
        DEFFUNCS1(inversesqrt)
        DEFFUNCS1(log)
        DEFFUNCS1(abs)
        DEFFUNCS1(sign)
        DEFFUNCS1(floor)
        DEFFUNCS1(ceil)
        DEFFUNCS1(round)
        DEFFUNCS1(fract)

#undef DEFFUNCS1

#define DEFFUNCS2(FUNC) \
    static ksl_vector FUNC(const ksl_vector& a, const ksl_vector& b) { return ksl_vector(FUNC(a[0], b[0]), FUNC(a[1], b[1]), FUNC(a[2], b[2])); }

        DEFFUNCS2(atan)
        DEFFUNCS2(log)
        DEFFUNCS2(mod)
        DEFFUNCS2(min)
        DEFFUNCS2(max)

#undef DEFFUNCS2

    public:
        static double comp (const ksl_vector& p, int index){return 0;}
        static double xcomp(const ksl_vector& p){return comp(p, 0);}
        static double ycomp(const ksl_vector& p){return comp(p, 1);}
        static double zcomp(const ksl_vector& p){return comp(p, 2);}

    public:
        static double noise_f1(double u);
        static double noise_f1(double u, double v);
        static double noise_f1(double u, double v, double w);
        static double noise_f1(double u, double v, double w, double t);
        static double noise_f1(const ksl_vector& pt);
        static double noise_f1(const ksl_vector& pt, double t);
        static ksl_vector noise_f3(double u);
        static ksl_vector noise_f3(double u, double v);
        static ksl_vector noise_f3(double u, double v, double w);
        static ksl_vector noise_f3(double u, double v, double w, double t);
        static ksl_vector noise_f3(const ksl_vector& pt);
        static ksl_vector noise_f3(const ksl_vector& pt, double t);

    public:
        static double pnoise_f1(double v, double period);
        static double pnoise_f1(double u, double v, double uperiod, double vperiod);
        static double pnoise_f1(double u, double v, double w, double uperiod, double vperiod, double wperiod);
        static double pnoise_f1(double u, double v, double w, double t, double uperiod, double vperiod, double wperiod, double tperiod);

    public:
        static double cellnoise_f1(double u);
        static double cellnoise_f1(double u, double v);
        static double cellnoise_f1(double u, double v, double w);
        static double cellnoise_f1(double u, double v, double w, double t);

    public:
        ksl_vector transform(const std::string& tospace, const ksl_vector& P) const;
        ksl_vector transform(const std::string& fromspace, const std::string& tospace, const ksl_vector& P) const;
        ksl_vector transform(const ksl_matrix4& matrix, const ksl_vector& P) const;
        ksl_vector transform(const std::string& fromspace, const ksl_matrix4& matrix, const ksl_vector& P) const;
        ksl_vector vtransform(const std::string& tospace, const ksl_vector& P) const;
        ksl_vector vtransform(const std::string& fromspace, const std::string& tospace, const ksl_vector& P) const;
        ksl_vector vtransform(const ksl_matrix4& matrix, const ksl_vector& P) const;
        ksl_vector vtransform(const std::string& fromspace, const ksl_matrix4& matrix, const ksl_vector& P) const;
        ksl_vector ntransform(const std::string& tospace, const ksl_vector& P) const;
        ksl_vector ntransform(const std::string& fromspace, const std::string& tospace, const ksl_vector& P) const;
        ksl_vector ntransform(const ksl_matrix4& matrix, const ksl_vector& P) const;
        ksl_vector ntransform(const std::string& fromspace, const ksl_matrix4& matrix, const ksl_vector& P) const;

    public:
        double depth(const ksl_vector& P) const;
        ksl_vector calculatenormal(const ksl_vector& P) const;

    public:
        ksl_vector ctransform(const std::string& tospace, const ksl_vector& C) const;
        ksl_vector ctransform(const std::string& fromspace, const std::string& tospace, const ksl_vector& C) const;

    public:
        ksl_vector faceforward(const ksl_vector& N, const ksl_vector& I) const;
        ksl_vector ambient() const;
        ksl_vector caustic(const ksl_vector& P, const ksl_vector& N) const;
        ksl_vector diffuse(const ksl_vector& N) const;
        ksl_vector specular(const ksl_vector& N, const ksl_vector& V, double roughness) const;
        ksl_vector specularbrdf(const ksl_vector& L, const ksl_vector& N, const ksl_vector& V, double roughness) const;
        ksl_vector phong(const ksl_vector& N, const ksl_vector& V, double size) const;
        ksl_vector reflect(const ksl_vector& I, const ksl_vector& N) const;
        ksl_vector refract(const ksl_vector& I, const ksl_vector& N, real eta) const;

    public:
        ksl_vector indirectdiffuse(const ksl_vector& P, const ksl_vector& N, double samples) const;
        double occlusion(const ksl_vector& P, const ksl_vector& N, double samples) const;

    public:
        double trace_f1(const ksl_vector& P, const ksl_vector& R) const;
        ksl_vector trace_f3(const ksl_vector& P, const ksl_vector& R) const;
        ksl_vector transmission(const ksl_vector& Psrc, const ksl_vector& Pdst) const;

    public:
        double texture_f1(const char* name) const;
        double texture_f1(const char* name, double s, double t) const;
        ksl_vector texture_f3(const char* name) const;
        ksl_vector texture_f3(const char* name, double s, double t) const;

    public:
        double environment_f1(const char* name) const;
        double environment_f1(const char* name, double s, double t) const;
        ksl_vector environment_f3(const char* name) const;
        ksl_vector environment_f3(const char* name, double s, double t) const;

    public:
        double shadow_f1(const char* name) const;
        double shadow_f1(const char* name, double s, double t) const;
        ksl_vector shadow_f3(const char* name) const;
        ksl_vector shadow_f3(const char* name, double s, double t) const;

    public:
        double bake3d(const char* filename, const char* displaychannels, const ksl_vector& P, const ksl_vector& N) const;
        double texture3d(const char* filename, const ksl_vector& P, const ksl_vector& N) const;
        ksl_vector photonmap(const char* filename, const ksl_vector& P, const ksl_vector& N) const;
    
    public:
        static std::string concat(const std::vector<std::string>& v){ return ""; }
        static int match (const std::string& pattern, const std::string& subject){ return -1; }
    
    public:
        std::string shadername() const;
        std::string shadername(const std::string& s) const{return "";}//TODO
    
    public:
        void set_ptr(const shader_input* ptr) { ptr_ = ptr; }
    private:
        const shader_input* ptr_;
    };

    typedef ksl_shader_input ksl_shader_input;

    extern "C" int ksl_shader_input_open(lua_State* L);
}

#endif
