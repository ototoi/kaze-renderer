#ifndef RSLE_MESURED_MERL_SHADER_EVALUATOR_H
#define RSLE_MESURED_MERL_SHADER_EVALUATOR_H

#include "shader_evaluator.h"

namespace rsle
{
    class mesured_MERL_shader_evaluator_imp;
    class mesured_MERL_shader_evaluator : public shader_evaluator
    {
    public:
        mesured_MERL_shader_evaluator(const char* szFile);
        ~mesured_MERL_shader_evaluator();

    public:
        virtual void evaluate(shader_output* out, const shader_input* in) const;

    private:
        mesured_MERL_shader_evaluator_imp* imp_;
    };
}

#endif