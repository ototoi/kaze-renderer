#include "ksl_shader_input.h"
#include "shader_evaluator.h"
#include "ksl_vector.h"

#ifdef __cplusplus
extern "C" {
#endif
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#ifdef __cplusplus
}
#endif

#if (LUA_VERSION_NUM == 502)
#undef luaL_register
#define luaL_register(L, n, f)      \
{                                   \
        if ((n) == NULL)            \
        {                           \
            luaL_setfuncs(L, f, 0); \
        }                           \
        else                        \
        {                           \
            luaL_newlib(L, f);      \
            lua_setglobal(L, n);    \
        }                           \
    }
#endif

#define KLASS "shader_input"
#define KLASS_META "shader_input_mt"

namespace rsle
{

    static inline ksl_vector Convert(const vector3& v)
    {
        return ksl_vector(v[0], v[1], v[2]);
    }

    static inline vector3 Convert(const ksl_vector& v)
    {
        return vector3(v[0], v[1], v[2]);
    }

    ksl_shader_input::ksl_shader_input(const shader_input* ptr) : ptr_(ptr) {}

    ksl_vector ksl_shader_input::P() const { return Convert(ptr_->P()); }
    ksl_vector ksl_shader_input::N() const { return Convert(ptr_->N()); }
    ksl_vector ksl_shader_input::Ng() const { return Convert(ptr_->Ng()); }
    ksl_vector ksl_shader_input::E() const { return Convert(ptr_->E()); }
    ksl_vector ksl_shader_input::I() const { return Convert(ptr_->I()); }
    ksl_vector ksl_shader_input::dPdu() const { return Convert(ptr_->dPdu()); }
    ksl_vector ksl_shader_input::dPdv() const { return Convert(ptr_->dPdv()); }
    ksl_vector ksl_shader_input::uvw() const { return Convert(ptr_->uvw()); }
    ksl_vector ksl_shader_input::stw() const { return Convert(ptr_->stw()); }
    ksl_vector ksl_shader_input::Cs() const { return Convert(ptr_->Cs()); }
    ksl_vector ksl_shader_input::Os() const { return Convert(ptr_->Os()); }

    double ksl_shader_input::random_f1() { return shader_functions::random_f1(); }
    double ksl_shader_input::min(double a, double b) { return shader_functions::min(a, b); }
    double ksl_shader_input::max(double a, double b) { return shader_functions::max(a, b); }
    double ksl_shader_input::clamp(double x, double a, double b) { return shader_functions::clamp(x, a, b); }
    double ksl_shader_input::mix(double x, double y, double alpha) { return shader_functions::mix(x, y, alpha); }
    double ksl_shader_input::step(double min, double value) { return shader_functions::step(min, value); }
    double ksl_shader_input::smoothstep(double min, double max, double value) { return shader_functions::smoothstep(min, max, value); }

    ksl_vector ksl_shader_input::random_f3() { return Convert(shader_functions::random_f3()); }
    ksl_vector ksl_shader_input::clamp(const ksl_vector& x, const ksl_vector& a, const ksl_vector& b) { return Convert(shader_functions::clamp(Convert(x), Convert(a), Convert(b))); }
    ksl_vector ksl_shader_input::step(const ksl_vector& min, const ksl_vector& value) { return Convert(shader_functions::step(Convert(min), Convert(value))); }
    ksl_vector ksl_shader_input::smoothstep(const ksl_vector& min, const ksl_vector& max, const ksl_vector& value) { return Convert(shader_functions::smoothstep(Convert(min), Convert(max), Convert(value))); }

    double ksl_shader_input::radians(double degrees) { return shader_functions::radians(degrees); }
    double ksl_shader_input::degrees(double radians) { return shader_functions::degrees(radians); }
    double ksl_shader_input::sin(double a) { return shader_functions::sin(a); }
    double ksl_shader_input::asin(double a) { return shader_functions::asin(a); }
    double ksl_shader_input::cos(double a) { return shader_functions::cos(a); }
    double ksl_shader_input::acos(double a) { return shader_functions::acos(a); }
    double ksl_shader_input::tan(double a) { return shader_functions::tan(a); }
    double ksl_shader_input::atan(double yoverx) { return shader_functions::atan(yoverx); }
    double ksl_shader_input::atan(double y, double x) { return shader_functions::atan(y, x); }

    double ksl_shader_input::pow(double x, double y) { return shader_functions::pow(x, y); }
    double ksl_shader_input::exp(double x) { return shader_functions::exp(x); }
    double ksl_shader_input::sqrt(double x) { return shader_functions::sqrt(x); }
    double ksl_shader_input::inversesqrt(double x) { return shader_functions::inversesqrt(x); }
    double ksl_shader_input::log(double x) { return shader_functions::log(x); }
    double ksl_shader_input::log(double x, double base) { return shader_functions::log(x, base); }

    double ksl_shader_input::mod(double a, double b) { return shader_functions::mod(a, b); }
    double ksl_shader_input::abs(double x) { return shader_functions::abs(x); }
    double ksl_shader_input::sign(double x) { return shader_functions::sign(x); }
    double ksl_shader_input::floor(double x) { return shader_functions::floor(x); }
    double ksl_shader_input::ceil(double x) { return shader_functions::ceil(x); }
    double ksl_shader_input::round(double x) { return shader_functions::round(x); }
    double ksl_shader_input::fract(double x) { return shader_functions::fract(x); }

    double ksl_shader_input::noise_f1(double u) { return shader_functions::noise_f1(u); }
    double ksl_shader_input::noise_f1(double u, double v) { return shader_functions::noise_f1(u, v); }
    double ksl_shader_input::noise_f1(double u, double v, double w) { return shader_functions::noise_f1(u, v, w); }
    double ksl_shader_input::noise_f1(double u, double v, double w, double t) { return shader_functions::noise_f1(u, v, w, t); }
    double ksl_shader_input::noise_f1(const ksl_vector& pt) { return shader_functions::noise_f1(Convert(pt)); }
    double ksl_shader_input::noise_f1(const ksl_vector& pt, double t) { return shader_functions::noise_f1(Convert(pt), t); }

    ksl_vector ksl_shader_input::noise_f3(double u) { return Convert(shader_functions::noise_f3(u)); }
    ksl_vector ksl_shader_input::noise_f3(double u, double v) { return Convert(shader_functions::noise_f3(u, v)); }
    ksl_vector ksl_shader_input::noise_f3(double u, double v, double w) { return Convert(shader_functions::noise_f3(u, v, w)); }
    ksl_vector ksl_shader_input::noise_f3(double u, double v, double w, double t) { return Convert(shader_functions::noise_f3(u, v, w, t)); }
    ksl_vector ksl_shader_input::noise_f3(const ksl_vector& pt) { return Convert(shader_functions::noise_f3(Convert(pt))); }
    ksl_vector ksl_shader_input::noise_f3(const ksl_vector& pt, double t) { return Convert(shader_functions::noise_f3(Convert(pt), t)); }

    double ksl_shader_input::pnoise_f1(double v, double period) { return 0; }
    double ksl_shader_input::pnoise_f1(double u, double v, double uperiod, double vperiod) { return 0; }
    double ksl_shader_input::pnoise_f1(double u, double v, double w, double uperiod, double vperiod, double wperiod) { return 0; }
    double ksl_shader_input::pnoise_f1(double u, double v, double w, double t, double uperiod, double vperiod, double wperiod, double tperiod) { return 0; }

    double ksl_shader_input::cellnoise_f1(double u) { return shader_functions::cellnoise_f1(u); }
    double ksl_shader_input::cellnoise_f1(double u, double v) { return shader_functions::cellnoise_f1(u, v); }
    double ksl_shader_input::cellnoise_f1(double u, double v, double w) { return shader_functions::cellnoise_f1(u, v, w); }
    double ksl_shader_input::cellnoise_f1(double u, double v, double w, double t) { return shader_functions::cellnoise_f1(u, v, w, t); }

    //----------------------------------------------------------------------------------------

    ksl_vector ksl_shader_input::transform(const std::string& tospace, const ksl_vector& P) const { return P; }
    ksl_vector ksl_shader_input::transform(const std::string& fromspace, const std::string& tospace, const ksl_vector& P) const { return P; }
    ksl_vector ksl_shader_input::transform(const ksl_matrix4& matrix, const ksl_vector& P) const { return P; }
    ksl_vector ksl_shader_input::transform(const std::string& fromspace, const ksl_matrix4& matrix, const ksl_vector& P) const { return P; }
    ksl_vector ksl_shader_input::vtransform(const std::string& tospace, const ksl_vector& P) const { return P; }
    ksl_vector ksl_shader_input::vtransform(const std::string& fromspace, const std::string& tospace, const ksl_vector& P) const { return P; }
    ksl_vector ksl_shader_input::vtransform(const ksl_matrix4& matrix, const ksl_vector& P) const { return P; }
    ksl_vector ksl_shader_input::vtransform(const std::string& fromspace, const ksl_matrix4& matrix, const ksl_vector& P) const { return P; }
    ksl_vector ksl_shader_input::ntransform(const std::string& tospace, const ksl_vector& P) const { return P; }
    ksl_vector ksl_shader_input::ntransform(const std::string& fromspace, const std::string& tospace, const ksl_vector& P) const { return P; }
    ksl_vector ksl_shader_input::ntransform(const ksl_matrix4& matrix, const ksl_vector& P) const { return P; }
    ksl_vector ksl_shader_input::ntransform(const std::string& fromspace, const ksl_matrix4& matrix, const ksl_vector& P) const { return P; }

    //----------------------------------------------------------------------------------------

    double ksl_shader_input::depth(const ksl_vector& P) const { return 0; }
    ksl_vector ksl_shader_input::calculatenormal(const ksl_vector& P) const { return P; }

    //----------------------------------------------------------------------------------------
    ksl_vector ksl_shader_input::ctransform(const std::string& tospace, const ksl_vector& C) const { return C; }
    ksl_vector ksl_shader_input::ctransform(const std::string& fromspace, const std::string& tospace, const ksl_vector& C) const { return C; }

    //----------------------------------------------------------------------------------------
    ksl_vector ksl_shader_input::faceforward(const ksl_vector& N, const ksl_vector& I) const
    {
        return Convert(ptr_->faceforward(Convert(N), Convert(I)));
    }
    ksl_vector ksl_shader_input::ambient() const
    {
        return Convert(ptr_->ambient());
    }
    ksl_vector ksl_shader_input::caustic(const ksl_vector& P, const ksl_vector& N) const
    {
        return ksl_vector(0, 0, 0); //todo
    }
    ksl_vector ksl_shader_input::diffuse(const ksl_vector& N) const
    {
        return Convert(ptr_->diffuse(Convert(N)));
    }
    ksl_vector ksl_shader_input::specular(const ksl_vector& N, const ksl_vector& V, double roughness) const
    {
        return Convert(ptr_->specular(Convert(N), Convert(V), roughness));
    }
    ksl_vector ksl_shader_input::specularbrdf(const ksl_vector& L, const ksl_vector& N, const ksl_vector& V, double roughness) const
    {
        return Convert(ptr_->specularbrdf(Convert(L), Convert(N), Convert(V), roughness));
    }
    ksl_vector ksl_shader_input::phong(const ksl_vector& N, const ksl_vector& V, double size) const
    {
        return Convert(ptr_->phong(Convert(N), Convert(V), size));
    }
    ksl_vector ksl_shader_input::reflect(const ksl_vector& I, const ksl_vector& N) const
    {
        return Convert(ptr_->reflect(Convert(I), Convert(N)));
    }
    ksl_vector ksl_shader_input::refract(const ksl_vector& I, const ksl_vector& N, double eta) const
    {
        return Convert(ptr_->refract(Convert(I), Convert(N), eta));
    }

    ksl_vector ksl_shader_input::indirectdiffuse(const ksl_vector& P, const ksl_vector& N, double samples) const
    {
        return Convert(ptr_->indirectdiffuse(Convert(P), Convert(N), samples));
    }
    double ksl_shader_input::occlusion(const ksl_vector& P, const ksl_vector& N, double samples) const
    {
        return ptr_->occlusion(Convert(P), Convert(N), samples);
    }

    double ksl_shader_input::trace_f1(const ksl_vector& P, const ksl_vector& R) const
    {
        return ptr_->trace_f1(Convert(P), Convert(R));
    }

    ksl_vector ksl_shader_input::trace_f3(const ksl_vector& P, const ksl_vector& R) const
    {
        return Convert(ptr_->trace_f3(Convert(P), Convert(R)));
    }

    ksl_vector ksl_shader_input::transmission(const ksl_vector& Psrc, const ksl_vector& Pdst) const
    {
        return Convert(ptr_->transmission(Convert(Psrc), Convert(Pdst)));
    }

    double ksl_shader_input::texture_f1(const char* name) const
    {
        return (double)ptr_->texture_f1(name);
    }
    double ksl_shader_input::texture_f1(const char* name, double s, double t) const
    {
        return (double)ptr_->texture_f1(name, s, t);
    }
    ksl_vector ksl_shader_input::texture_f3(const char* name) const
    {
        return Convert(ptr_->texture_f3(name));
    }
    ksl_vector ksl_shader_input::texture_f3(const char* name, double s, double t) const
    {
        return Convert(ptr_->texture_f3(name, s, t));
    }

    double ksl_shader_input::u() const { return (double)ptr_->u(); }
    double ksl_shader_input::v() const { return (double)ptr_->v(); }
    double ksl_shader_input::s() const { return (double)ptr_->s(); }
    double ksl_shader_input::t() const { return (double)ptr_->t(); }

    double ksl_shader_input::get_f1(const char* key) const { return (double)ptr_->get_f1(key); }
    double ksl_shader_input::get_f1(const char* key, double def) const { return (double)ptr_->get_f1(key, def); }
    ksl_vector ksl_shader_input::get_f3(const char* key) const { return Convert(ptr_->get_f3(key)); }
    ksl_vector ksl_shader_input::get_f3(const char* key, const ksl_vector& def) const { return Convert(ptr_->get_f3(key, Convert(def))); }
    std::string ksl_shader_input::get_string(const char* key) const { return ptr_->get_string(key); }
    std::string ksl_shader_input::get_string(const char* key, const std::string& def) const { return ptr_->get_string(key, def); }

    std::string ksl_shader_input::shadername() const
    {
        return "";
    }
    //----------------------------------------------------------------

    static int ksl_shader_input_new(lua_State* L)
    {
        const shader_input* s = (const shader_input*)lua_topointer(L, -1);
        ksl_shader_input* v = (ksl_shader_input*)lua_newuserdata(L, sizeof(ksl_shader_input));
        v->set_ptr(s);
        luaL_getmetatable(L, KLASS_META);
        lua_setmetatable(L, -2);
        return 1;
    }

    static int ksl_shader_input_P(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector v = self->P();
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    static int ksl_shader_input_N(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector v = self->N();
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_Ng(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector v = self->Ng();
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_E(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector v = self->E();
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    static int ksl_shader_input_I(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector v = self->I();
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    static int ksl_shader_input_dPdu(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector v = self->dPdu();
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    static int ksl_shader_input_dPdv(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector v = self->dPdv();
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    static int ksl_shader_input_uvw(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector v = self->uvw();
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    static int ksl_shader_input_stw(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector v = self->stw();
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    static int ksl_shader_input_Cs(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector v = self->Cs();
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    static int ksl_shader_input_Os(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector v = self->Os();
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    //----------------------------------------------------------------
    static int ksl_shader_input_faceforward(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector N = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector I = *((const ksl_vector*)lua_touserdata(L, 3));
        ksl_vector v = self->faceforward(N, I);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_ambient(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector v = self->ambient();
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_caustic(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector P = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector N = *((const ksl_vector*)lua_touserdata(L, 3));
        ksl_vector v = self->caustic(P, N);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_diffuse(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector N = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector v = self->diffuse(N);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_specular(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector N = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector V = *((const ksl_vector*)lua_touserdata(L, 3));
        double roughness = lua_tonumber(L, 4);
        ksl_vector v = self->specular(N, V, roughness);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_specularbrdf(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector L_ = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector N = *((const ksl_vector*)lua_touserdata(L, 3));
        ksl_vector V = *((const ksl_vector*)lua_touserdata(L, 4));
        double roughness = lua_tonumber(L, 5);

        ksl_vector v = self->specularbrdf(L_, N, V, roughness);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_phong(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector N = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector V = *((const ksl_vector*)lua_touserdata(L, 3));
        double size = lua_tonumber(L, 4);

        ksl_vector v = self->phong(N, V, size);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_reflect(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector I = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector N = *((const ksl_vector*)lua_touserdata(L, 3));
        ksl_vector v = self->reflect(I, N);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_refract(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector I = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector N = *((const ksl_vector*)lua_touserdata(L, 3));
        double eta = lua_tonumber(L, 4);
        ksl_vector v = self->refract(I, N, eta);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_indirectdiffuse(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector P = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector N = *((const ksl_vector*)lua_touserdata(L, 3));
        double samples = lua_tonumber(L, 4);
        ksl_vector v = self->indirectdiffuse(P, N, samples);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_occlusion(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector P = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector N = *((const ksl_vector*)lua_touserdata(L, 3));
        double samples = lua_tonumber(L, 4);
        double v = self->occlusion(P, N, samples);
        lua_pushnumber(L, v);
        return 1;
    }

    static int ksl_shader_input_trace_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector P = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector R = *((const ksl_vector*)lua_touserdata(L, 3));
        double v = self->trace_f1(P, R);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_trace_f3(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector P = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector R = *((const ksl_vector*)lua_touserdata(L, 3));
        ksl_vector v = self->trace_f3(P, R);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_transmission(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector P1 = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector P2 = *((const ksl_vector*)lua_touserdata(L, 3));
        ksl_vector v = self->transmission(P1, P2);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    //----------------------------------------------------------------

    static int ksl_shader_input_texture_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        std::string name = lua_tostring(L, 2);
        double v = 0;
        int n = lua_gettop(L);
        if (n >= 4)
        {
            double s = lua_tonumber(L, 3);
            double t = lua_tonumber(L, 4);
            v = self->texture_f1(name.c_str(), s, t);
        }
        else
        {
            v = self->texture_f1(name.c_str());
        }
        lua_pushnumber(L, v);
        return 1;
    }

    static int ksl_shader_input_texture_f3(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        std::string name = lua_tostring(L, 2);
        ksl_vector v;
        int n = lua_gettop(L);
        if (n >= 4)
        {
            double s = lua_tonumber(L, 3);
            double t = lua_tonumber(L, 4);
            v = self->texture_f3(name.c_str(), s, t);
        }
        else
        {
            v = self->texture_f3(name.c_str());
        }
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    //----------------------------------------------------------------
    static int ksl_shader_input_u(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double v = self->u();
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_v(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double v = self->v();
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_s(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double v = self->s();
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_t(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double v = self->t();
        lua_pushnumber(L, v);
        return 1;
    }

    //----------------------------------------------------------------
    static int ksl_shader_input_get_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        std::string name = lua_tostring(L, 2);
        double v;
        if (lua_gettop(L) >= 3)
        {
            double def = lua_tonumber(L, 3);
            v = self->get_f1(name.c_str(), def);
        }
        else
        {
            v = self->get_f1(name.c_str());
        }
        lua_pushnumber(L, v);
        return 1;
    }

    static int ksl_shader_input_get_f3(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        std::string name = lua_tostring(L, 2);
        ksl_vector v;
        if (lua_gettop(L) >= 3)
        {
            ksl_vector def = *((const ksl_vector*)lua_touserdata(L, 3));
            v = self->get_f3(name.c_str(), def);
        }
        else
        {
            v = self->get_f3(name.c_str());
        }
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }

    static int ksl_shader_input_get_string(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        std::string name = lua_tostring(L, 2);
        std::string v;
        if (lua_gettop(L) >= 3)
        {
            std::string def = lua_tostring(L, 3);
            v = self->get_string(name.c_str(), def);
        }
        else
        {
            v = self->get_string(name.c_str());
        }
        lua_pushstring(L, v.c_str());
        return 1;
    }
    //----------------------------------------------------------------
    static int ksl_shader_input_random_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double v = self->random_f1();
        lua_pushnumber(L, v);
        return 1;
    }

    static int ksl_shader_input_random_f3(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector v = self->random_f3();
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    //----------------------------------------------------------------
    static int ksl_shader_input_radians_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->radians(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_degrees_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->degrees(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_sin_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->sin(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_asin_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->asin(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_cos_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->cos(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_acos_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->acos(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_tan_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->tan(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_atan_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->atan(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_atan2_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double y = lua_tonumber(L, 2);
        double x = lua_tonumber(L, 3);
        double v = self->atan(y, x);
        lua_pushnumber(L, v);
        return 1;
    }
    //----------------------------------------------------------------
    static int ksl_shader_input_pow_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double y = lua_tonumber(L, 3);
        double v = self->pow(x, y);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_exp_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->exp(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_sqrt_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->sqrt(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_inversesqrt_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->inversesqrt(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_log_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->log(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_log_base_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double base = lua_tonumber(L, 3);
        double v = self->log(x, base);
        lua_pushnumber(L, v);
        return 1;
    }
    //----------------------------------------------------------------
    static int ksl_shader_input_mod_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double y = lua_tonumber(L, 3);
        double v = self->mod(x, y);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_abs_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->abs(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_sign_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->sign(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_floor_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->floor(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_ceil_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->ceil(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_round_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->round(x);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_fract_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double v = self->fract(x);
        lua_pushnumber(L, v);
        return 1;
    }
    //----------------------------------------------------------------
    static int ksl_shader_input_min_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double y = lua_tonumber(L, 3);
        double v = self->min(x, y);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_max_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double y = lua_tonumber(L, 3);
        double v = self->max(x, y);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_clamp_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x = lua_tonumber(L, 2);
        double a = lua_tonumber(L, 3);
        double b = lua_tonumber(L, 4);
        double v = self->clamp(x, a, b);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_step_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double min = lua_tonumber(L, 2);
        double val = lua_tonumber(L, 3);
        double v = self->step(min, val);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_smoothstep_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double min = lua_tonumber(L, 2);
        double max = lua_tonumber(L, 3);
        double val = lua_tonumber(L, 4);
        double v = self->smoothstep(min, max, val);
        lua_pushnumber(L, v);
        return 1;
    }

//----------------------------------------------------------------
#define DEFFUNCS1(FUNC)                                                               \
    static int ksl_shader_input_##FUNC##_f3(lua_State* L)                             \
    {                                                                                 \
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1); \
        ksl_vector x = *((const ksl_vector*)lua_touserdata(L, 2));                  \
        ksl_vector v = self->FUNC(x);                                                \
        lua_getglobal(L, "vector_from_ptr");                                         \
        lua_pushlightuserdata(L, &v);                                                 \
        lua_pcall(L, 1, 1, 0);                                                        \
        return 1;                                                                     \
    }

    DEFFUNCS1(radians)
    DEFFUNCS1(degrees)
    DEFFUNCS1(sin)
    DEFFUNCS1(asin)
    DEFFUNCS1(cos)
    DEFFUNCS1(acos)
    DEFFUNCS1(tan)
    DEFFUNCS1(atan)
    DEFFUNCS1(exp)
    DEFFUNCS1(sqrt)
    DEFFUNCS1(inversesqrt)
    DEFFUNCS1(log)
    DEFFUNCS1(abs)
    DEFFUNCS1(sign)
    DEFFUNCS1(floor)
    DEFFUNCS1(ceil)
    DEFFUNCS1(round)
    DEFFUNCS1(fract)

#undef DEFFUNCS1
    //----------------------------------------------------------------
    static int ksl_shader_input_noise_1_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x1 = lua_tonumber(L, 2);
        double v = self->noise_f1(x1);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_noise_2_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x1 = lua_tonumber(L, 2);
        double x2 = lua_tonumber(L, 3);
        double v = self->noise_f1(x1, x2);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_noise_3_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x1 = lua_tonumber(L, 2);
        double x2 = lua_tonumber(L, 3);
        double x3 = lua_tonumber(L, 4);
        double v = self->noise_f1(x1, x2, x3);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_noise_4_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x1 = lua_tonumber(L, 2);
        double x2 = lua_tonumber(L, 3);
        double x3 = lua_tonumber(L, 4);
        double x4 = lua_tonumber(L, 5);
        double v = self->noise_f1(x1, x2, x3, x4);
        lua_pushnumber(L, v);
        return 1;
    }
    //----------------------------------------------------------------
    static int ksl_shader_input_noise_1_f3(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x1 = lua_tonumber(L, 2);
        ksl_vector v = self->noise_f3(x1);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    static int ksl_shader_input_noise_2_f3(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x1 = lua_tonumber(L, 2);
        double x2 = lua_tonumber(L, 3);
        ksl_vector v = self->noise_f3(x1, x2);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    static int ksl_shader_input_noise_3_f3(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x1 = lua_tonumber(L, 2);
        double x2 = lua_tonumber(L, 3);
        double x3 = lua_tonumber(L, 4);
        ksl_vector v = self->noise_f3(x1, x2, x3);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    static int ksl_shader_input_noise_4_f3(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x1 = lua_tonumber(L, 2);
        double x2 = lua_tonumber(L, 3);
        double x3 = lua_tonumber(L, 4);
        double x4 = lua_tonumber(L, 5);
        ksl_vector v = self->noise_f3(x1, x2, x3, x4);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &v);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    //----------------------------------------------------------------
    static int ksl_shader_input_cellnoise_1_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x1 = lua_tonumber(L, 2);
        double v = self->cellnoise_f1(x1);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_cellnoise_2_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x1 = lua_tonumber(L, 2);
        double x2 = lua_tonumber(L, 3);
        double v = self->cellnoise_f1(x1, x2);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_cellnoise_3_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x1 = lua_tonumber(L, 2);
        double x2 = lua_tonumber(L, 3);
        double x3 = lua_tonumber(L, 4);
        double v = self->cellnoise_f1(x1, x2, x3);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_cellnoise_4_f1(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        double x1 = lua_tonumber(L, 2);
        double x2 = lua_tonumber(L, 3);
        double x3 = lua_tonumber(L, 4);
        double x4 = lua_tonumber(L, 5);
        double v = self->cellnoise_f1(x1, x2, x3, x4);
        lua_pushnumber(L, v);
        return 1;
    }
    //----------------------------------------------------------------
    static int ksl_shader_input_depth(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector P = *((const ksl_vector*)lua_touserdata(L, 2));
        double v = self->depth(P);
        lua_pushnumber(L, v);
        return 1;
    }
    static int ksl_shader_input_calculatenormal(lua_State* L)
    {
        const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);
        ksl_vector P = *((const ksl_vector*)lua_touserdata(L, 2));
        ksl_vector N = self->calculatenormal(P);
        lua_getglobal(L, "vector_from_ptr");
        lua_pushlightuserdata(L, &N);
        lua_pcall(L, 1, 1, 0);
        return 1;
    }
    //----------------------------------------------------------------
    #include "ksl_shader_input.inl"
    //----------------------------------------------------------------
    static const luaL_Reg FUNCS_M[] =
        {
            {"P", ksl_shader_input_P},
            {"N", ksl_shader_input_N},
            {"Ng", ksl_shader_input_Ng},
            {"E", ksl_shader_input_E},
            {"I", ksl_shader_input_I},
            {"dPdu", ksl_shader_input_dPdu},
            {"dPdv", ksl_shader_input_dPdv},
            {"uvw", ksl_shader_input_uvw},
            {"stw", ksl_shader_input_stw},
            {"Cs", ksl_shader_input_Cs},
            {"Os", ksl_shader_input_Os},

            {"faceforward", ksl_shader_input_faceforward},
            {"ambient", ksl_shader_input_ambient},
            {"caustic", ksl_shader_input_caustic},
            {"diffuse", ksl_shader_input_diffuse},
            {"specular", ksl_shader_input_specular},
            {"specularbrdf", ksl_shader_input_specularbrdf},
            {"phong", ksl_shader_input_phong},
            {"reflect", ksl_shader_input_reflect},
            {"refract", ksl_shader_input_refract},

            {"indirectdiffuse", ksl_shader_input_indirectdiffuse},
            {"occlusion", ksl_shader_input_occlusion},
            {"trace_f1", ksl_shader_input_trace_f1},
            {"trace_f3", ksl_shader_input_trace_f3},

            {"texture_f1", ksl_shader_input_texture_f1},
            {"texture_float", ksl_shader_input_texture_f1},
            {"texture_f3", ksl_shader_input_texture_f3},
            {"texture_point", ksl_shader_input_texture_f3},
            {"texture_normal", ksl_shader_input_texture_f3},
            {"texture_color", ksl_shader_input_texture_f3},

            {"u", ksl_shader_input_u},
            {"v", ksl_shader_input_v},
            {"s", ksl_shader_input_s},
            {"t", ksl_shader_input_t},

            {"get_f1", ksl_shader_input_get_f1},
            {"get_float", ksl_shader_input_get_f1},
            {"get_f3", ksl_shader_input_get_f3},
            {"get_point", ksl_shader_input_get_f3},
            {"get_normal", ksl_shader_input_get_f3},
            {"get_color", ksl_shader_input_get_f3},

            {"get_string", ksl_shader_input_get_string},

            {"random", ksl_shader_input_random_f1},
            {"random_f1", ksl_shader_input_random_f1},
            {"random_f3", ksl_shader_input_random_f3},

            {"radians", ksl_shader_input_radians_f1},
            {"degrees", ksl_shader_input_degrees_f1},
            {"sin", ksl_shader_input_sin_f1},
            {"asin", ksl_shader_input_asin_f1},
            {"cos", ksl_shader_input_cos_f1},
            {"acos", ksl_shader_input_acos_f1},
            {"tan", ksl_shader_input_tan_f1},
            {"atan", ksl_shader_input_atan_f1},
            {"atan2", ksl_shader_input_atan2_f1},

            {"pow", ksl_shader_input_pow_f1},
            {"exp", ksl_shader_input_exp_f1},
            {"sqrt", ksl_shader_input_sqrt_f1},
            {"inversesqrt", ksl_shader_input_inversesqrt_f1},
            {"log", ksl_shader_input_log_f1},
            {"log_base", ksl_shader_input_log_base_f1},

            {"mod", ksl_shader_input_mod_f1},
            {"abs", ksl_shader_input_abs_f1},
            {"sign", ksl_shader_input_sign_f1},
            {"floor", ksl_shader_input_floor_f1},
            {"ceil", ksl_shader_input_ceil_f1},
            {"round", ksl_shader_input_round_f1},
            {"fract", ksl_shader_input_fract_f1},

            {"min", ksl_shader_input_min_f1},
            {"max", ksl_shader_input_max_f1},
            {"clamp", ksl_shader_input_clamp_f1},
            {"step", ksl_shader_input_step_f1},
            {"smoothstep", ksl_shader_input_smoothstep_f1},

            {"radians_f3", ksl_shader_input_radians_f3},
            {"degrees_f3", ksl_shader_input_degrees_f3},
            {"sin_f3", ksl_shader_input_sin_f3},
            {"asin_f3", ksl_shader_input_asin_f3},
            {"cos_f3", ksl_shader_input_cos_f3},
            {"acos_f3", ksl_shader_input_acos_f3},
            {"tan_f3", ksl_shader_input_tan_f3},
            {"atan_f3", ksl_shader_input_atan_f3},
            //{ "atan2_f3",       ksl_shader_input_atan2_f3 },

            //{ "pow_f3",        ksl_shader_input_pow_f3 },
            {"exp_f3", ksl_shader_input_exp_f3},
            {"sqrt_f3", ksl_shader_input_sqrt_f3},
            {"inversesqrt_f3", ksl_shader_input_inversesqrt_f3},
            {"log_f3", ksl_shader_input_log_f3},
            //{ "log_base",   ksl_shader_input_log_base_f1 },

            //{ "mod_f3",        ksl_shader_input_mod_f3 },
            {"abs_f3", ksl_shader_input_abs_f3},
            {"sign_f3", ksl_shader_input_sign_f3},
            {"floor_f3", ksl_shader_input_floor_f3},
            {"ceil_f3", ksl_shader_input_ceil_f3},
            {"round_f3", ksl_shader_input_round_f3},
            {"fract_f3", ksl_shader_input_fract_f3},

            {"noise_1_f1", ksl_shader_input_noise_1_f1},
            {"noise_2_f1", ksl_shader_input_noise_2_f1},
            {"noise_3_f1", ksl_shader_input_noise_3_f1},
            {"noise_4_f1", ksl_shader_input_noise_4_f1},

            {"noise_1_f3", ksl_shader_input_noise_1_f3},
            {"noise_2_f3", ksl_shader_input_noise_2_f3},
            {"noise_3_f3", ksl_shader_input_noise_3_f3},
            {"noise_4_f3", ksl_shader_input_noise_4_f3},

            //pnoise_f1
            //pnoise_f3

            {"cellnoise_1_f1", ksl_shader_input_cellnoise_1_f1},
            {"cellnoise_2_f1", ksl_shader_input_cellnoise_2_f1},
            {"cellnoise_3_f1", ksl_shader_input_cellnoise_3_f1},
            {"cellnoise_4_f1", ksl_shader_input_cellnoise_4_f1},

            //cellnoise_f3

            {"depth", ksl_shader_input_depth},
            {"depth_f1", ksl_shader_input_depth},
            {"calculatenormal", ksl_shader_input_calculatenormal},
            {"calculatenormal_f3", ksl_shader_input_calculatenormal},

            {NULL, NULL}};

    static const luaL_Reg FUNCS_F[] =
        {
            {"P", ksl_shader_input_P},
            {"N", ksl_shader_input_N},
            {"Ng", ksl_shader_input_Ng},
            {"E", ksl_shader_input_E},
            {"I", ksl_shader_input_I},
            {"dPdu", ksl_shader_input_dPdu},
            {"dPdv", ksl_shader_input_dPdv},
            {"uvw", ksl_shader_input_uvw},
            {"stw", ksl_shader_input_stw},
            {"Cs", ksl_shader_input_Cs},
            {"Os", ksl_shader_input_Os},

            {"faceforward", ksl_shader_input_faceforward},
            {"ambient", ksl_shader_input_ambient},
            {"caustic", ksl_shader_input_caustic},
            {"diffuse", ksl_shader_input_diffuse},
            {"specular", ksl_shader_input_specular},
            {"specularbrdf", ksl_shader_input_specularbrdf},
            {"phong", ksl_shader_input_phong},
            {"reflect", ksl_shader_input_reflect},
            {"refract", ksl_shader_input_refract},

            {"indirectdiffuse", ksl_shader_input_indirectdiffuse},
            {"occlusion", ksl_shader_input_occlusion},
            {"trace_f1", ksl_shader_input_trace_f1},
            {"trace_f3", ksl_shader_input_trace_f3},

            {"texture_f1", ksl_shader_input_texture_f1},
            {"texture_float", ksl_shader_input_texture_f1},
            {"texture_f3", ksl_shader_input_texture_f3},
            {"texture_point", ksl_shader_input_texture_f3},
            {"texture_normal", ksl_shader_input_texture_f3},
            {"texture_color", ksl_shader_input_texture_f3},

            {"u", ksl_shader_input_u},
            {"v", ksl_shader_input_v},
            {"s", ksl_shader_input_s},
            {"t", ksl_shader_input_t},

            {"get_f1", ksl_shader_input_get_f1},
            {"get_float", ksl_shader_input_get_f1},
            {"get_f3", ksl_shader_input_get_f3},
            {"get_point", ksl_shader_input_get_f3},
            {"get_normal", ksl_shader_input_get_f3},
            {"get_color", ksl_shader_input_get_f3},

            {"get_string", ksl_shader_input_get_string},

            {"random", ksl_shader_input_random_f1},
            {"random_f1", ksl_shader_input_random_f1},
            {"random_f3", ksl_shader_input_random_f3},

            {"radians", ksl_shader_input_radians_f1},
            {"degrees", ksl_shader_input_degrees_f1},
            {"sin", ksl_shader_input_sin_f1},
            {"asin", ksl_shader_input_asin_f1},
            {"cos", ksl_shader_input_cos_f1},
            {"acos", ksl_shader_input_acos_f1},
            {"tan", ksl_shader_input_tan_f1},
            {"atan", ksl_shader_input_atan_f1},
            {"atan2", ksl_shader_input_atan2_f1},

            {"pow", ksl_shader_input_pow_f1},
            {"exp", ksl_shader_input_exp_f1},
            {"sqrt", ksl_shader_input_sqrt_f1},
            {"inversesqrt", ksl_shader_input_inversesqrt_f1},
            {"log", ksl_shader_input_log_f1},
            {"log_base", ksl_shader_input_log_base_f1},

            {"mod", ksl_shader_input_mod_f1},
            {"abs", ksl_shader_input_abs_f1},
            {"sign", ksl_shader_input_sign_f1},
            {"floor", ksl_shader_input_floor_f1},
            {"ceil", ksl_shader_input_ceil_f1},
            {"round", ksl_shader_input_round_f1},

            {"min", ksl_shader_input_min_f1},
            {"max", ksl_shader_input_max_f1},
            {"clamp", ksl_shader_input_clamp_f1},
            {"step", ksl_shader_input_step_f1},
            {"smoothstep", ksl_shader_input_smoothstep_f1},

            {"noise_1_f1", ksl_shader_input_noise_1_f1},
            {"noise_2_f1", ksl_shader_input_noise_2_f1},
            {"noise_3_f1", ksl_shader_input_noise_3_f1},
            {"noise_4_f1", ksl_shader_input_noise_4_f1},

            {"noise_1_f3", ksl_shader_input_noise_1_f3},
            {"noise_2_f3", ksl_shader_input_noise_2_f3},
            {"noise_3_f3", ksl_shader_input_noise_3_f3},
            {"noise_4_f3", ksl_shader_input_noise_4_f3},

            //pnoise_f1
            //pnoise_f3

            {"cellnoise_1_f1", ksl_shader_input_cellnoise_1_f1},
            {"cellnoise_2_f1", ksl_shader_input_cellnoise_2_f1},
            {"cellnoise_3_f1", ksl_shader_input_cellnoise_3_f1},
            {"cellnoise_4_f1", ksl_shader_input_cellnoise_4_f1},

            //cellnoise_f3

            {"depth", ksl_shader_input_depth},
            {"depth_f1", ksl_shader_input_depth},
            {"calculatenormal", ksl_shader_input_calculatenormal},
            {"calculatenormal_f3", ksl_shader_input_calculatenormal},

            {NULL, NULL}};

    int ksl_shader_input_open(lua_State* L)
    {
        luaL_newmetatable(L, KLASS_META);
        /*metatable.__index = metatable*/
        lua_pushvalue(L, -1); //clone metatable
        lua_setfield(L, -2, "__index");
        luaL_register(L, NULL, FUNCS_M);
        luaL_register(L, KLASS, FUNCS_F);

        lua_register(L, "shader_input", ksl_shader_input_new);

        lua_pop(L, lua_gettop(L));

        return 0;
    }
}
