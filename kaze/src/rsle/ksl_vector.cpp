#include "ksl_vector.h"

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#ifdef __cplusplus
}
#endif

#if (LUA_VERSION_NUM >= 502)
#undef luaL_register
#define luaL_register(L, n, f)      \
                                    \
{                                   \
        if ((n) == NULL)            \
        {                           \
            luaL_setfuncs(L, f, 0); \
        }                           \
        else                        \
        {                           \
            luaL_newlib(L, f);      \
            lua_setglobal(L, n);    \
        }                           \
}

#else
static void* luaL_testudata(lua_State* L, int ud, const char* tname)
{
    void* p = lua_touserdata(L, ud);
    if (p != NULL)
    { /* value is a userdata? */
        if (lua_getmetatable(L, ud))
        {                                 /* does it have a metatable? */
            luaL_getmetatable(L, tname);  /* get correct metatable */
            if (!lua_rawequal(L, -1, -2)) /* not the same? */
                p = NULL;                 /* value is a userdata with wrong metatable */
            lua_pop(L, 2);                /* remove both metatables */
            return p;
        }
    }
    return NULL; /* value is not a userdata with a metatable */
}
#endif

#define KLASS "ksl_vector"
#define KLASS_META "ksl_vector_mt"

using namespace rsle;

namespace rsle
{
    ksl_vector::ksl_vector()
    {
        e_[0] = 0;
        e_[1] = 0;
        e_[2] = 0;
        e_[3] = 0;
    }
    ksl_vector::ksl_vector(double x, double y)
    {
        e_[0] = x;
        e_[1] = y;
        e_[2] = 0;
        e_[3] = 0;
    }
    ksl_vector::ksl_vector(double x, double y, double z)
    {
        e_[0] = x;
        e_[1] = y;
        e_[2] = z;
        e_[3] = 0;
    }
    ksl_vector::ksl_vector(double x, double y, double z, double w)
    {
        e_[0] = x;
        e_[1] = y;
        e_[2] = z;
        e_[3] = w;
    }
    ksl_vector& ksl_vector::operator+=(const ksl_vector& rhs)
    {
        for(int i=0;i<4;i++){e_[i] += rhs.e_[i];}
        return *this;
    }

    ksl_vector& ksl_vector::operator-=(const ksl_vector& rhs)
    {
        for(int i=0;i<4;i++){e_[i] -= rhs.e_[i];}
        return *this;
    }
    ksl_vector& ksl_vector::operator*=(const ksl_vector& rhs)
    {
        for(int i=0;i<4;i++){e_[i] *= rhs.e_[i];}
        return *this;
    }
    ksl_vector& ksl_vector::operator/=(const ksl_vector& rhs)
    {
        for(int i=0;i<4;i++){e_[i] /= rhs.e_[i];}
        return *this;
    }
    ksl_vector& ksl_vector::operator*=(double rhs)
    {
        for(int i=0;i<4;i++){e_[i] *= rhs;}
        return *this;
    }


    int ksl_vector_new_ptr(lua_State* L)
    {
        const ksl_vector* p = (const ksl_vector*)lua_topointer(L, -1);
        ksl_vector* v = (ksl_vector*)lua_newuserdata(L, sizeof(ksl_vector));
        *v = *p;
        luaL_getmetatable(L, KLASS_META);
        lua_setmetatable(L, -2);
        return 1;
    }

    static int ksl_vector_new(lua_State* L)
    {
        int sz = lua_gettop(L);
        ksl_vector t;
        if (sz == 3)
        {
            t[0] = luaL_optnumber(L, 1, 0);
            t[1] = luaL_optnumber(L, 2, 0);
            t[2] = luaL_optnumber(L, 3, 0);
        }
        else if (sz == 1)
        {
            const ksl_vector* self = (const ksl_vector*)luaL_testudata(L, 1, KLASS_META);
            t = *self;
        }

        ksl_vector* v = (ksl_vector*)lua_newuserdata(L, sizeof(ksl_vector));
        *v = t;
        luaL_getmetatable(L, KLASS_META);
        lua_setmetatable(L, -2);
        return 1;
    }

    static int ksl_vector_geti(lua_State* L)
    {
        const ksl_vector* self = (const ksl_vector*)luaL_checkudata(L, 1, KLASS_META);
        int i = (int)lua_tonumber(L, 2);
        double ret = (double)self->operator[](i);
        lua_pushnumber(L, (lua_Number)ret);
        return 1;
    }

    static int ksl_vector_seti(lua_State* L)
    {
        ksl_vector* self = (ksl_vector*)luaL_checkudata(L, 1, KLASS_META);
        int i = (int)lua_tonumber(L, 2);
        double value = (double)lua_tonumber(L, 3);
        self->operator[](i) = value;
        return 0;
    }

    static int ksl_vector_get(lua_State* L)
    {
        const ksl_vector* self = (const ksl_vector*)luaL_checkudata(L, 1, KLASS_META);
        const ksl_vector& v = *self;
        const char* i = luaL_checkstring(L, 2);
        switch (*i)
        { /* lazy! */
        case '0':
        case 'x':
        case 'r':
            lua_pushnumber(L, v[0]);
            return 1;
        case '1':
        case 'y':
        case 'g':
            lua_pushnumber(L, v[1]);
            return 1;
        case '2':
        case 'z':
        case 'b':
            lua_pushnumber(L, v[2]);
            return 1;
        default:
            lua_pushnil(L);
            return 1;
        }
        return 1;
    }

    static int ksl_vector_set(lua_State* L)
    {
        ksl_vector* self = (ksl_vector*)luaL_checkudata(L, 1, KLASS_META);
        ksl_vector& v = *self;
        const char* i = luaL_checkstring(L, 2);
        double t = luaL_checknumber(L, 3);
        switch (*i)
        { /* lazy! */
        case '0':
        case 'x':
        case 'r':
            v[0] = t;
            break;
        case '1':
        case 'y':
        case 'g':
            v[1] = t;
            break;
        case '2':
        case 'z':
        case 'b':
            v[2] = t;
            break;
        default:
            break;
        }
        return 0;
    }

    static int ksl_vector_tostring(lua_State* L)
    {
        const ksl_vector* self = (const ksl_vector*)luaL_checkudata(L, 1, KLASS_META);
        char s[64];
        sprintf(s, "ksl_vector(%f,%f,%f)", (*self)[0], (*self)[1], (*self)[2]);
        lua_pushstring(L, s);
        return 1;
    }

    static int ksl_vector_add(lua_State* L)
    {
        const ksl_vector* a = (const ksl_vector*)lua_touserdata(L, 1);
        const ksl_vector* b = (const ksl_vector*)lua_touserdata(L, 2);
        ksl_vector* v = (ksl_vector*)lua_newuserdata(L, sizeof(ksl_vector));
        *v = *a;
        *v += *b;
        luaL_getmetatable(L, KLASS_META);
        lua_setmetatable(L, -2);
        return 1;
    }

    static int ksl_vector_sub(lua_State* L)
    {
        const ksl_vector* a = (const ksl_vector*)luaL_checkudata(L, 1, KLASS_META);
        const ksl_vector* b = (const ksl_vector*)luaL_checkudata(L, 2, KLASS_META);
        ksl_vector* v = (ksl_vector*)lua_newuserdata(L, sizeof(ksl_vector));
        *v = *a;
        *v -= *b;
        luaL_getmetatable(L, KLASS_META);
        lua_setmetatable(L, -2);
        return 1;
    }

    static int ksl_vector_mul(lua_State* L)
    {
        int nState = 0;
        if (luaL_testudata(L, 1, KLASS_META)) nState |= 1;
        if (luaL_testudata(L, 2, KLASS_META)) nState |= 2;

        double x = 0;
        double y = 0;
        double z = 0;

        switch (nState)
        {
        case 0:
            break;
        case 1:
        {
            const ksl_vector* a = (const ksl_vector*)lua_touserdata(L, 1);
            double b = lua_tonumber(L, 2);

            x = (*a)[0] * b;
            y = (*a)[1] * b;
            z = (*a)[2] * b;
        }
        break;
        case 2:
        {
            double a = lua_tonumber(L, 1);
            const ksl_vector* b = (const ksl_vector*)lua_touserdata(L, 2);

            x = a * (*b)[0];
            y = a * (*b)[1];
            z = a * (*b)[2];
        }
        break;
        case 3:
        {
            const ksl_vector* a = (const ksl_vector*)lua_touserdata(L, 1);
            const ksl_vector* b = (const ksl_vector*)lua_touserdata(L, 2);

            x = (*a)[0] * (*b)[0];
            y = (*a)[1] * (*b)[1];
            z = (*a)[2] * (*b)[2];
        }
        break;
        }

        ksl_vector* v = (ksl_vector*)lua_newuserdata(L, sizeof(ksl_vector));
        (*v)[0] = x;
        (*v)[1] = y;
        (*v)[2] = z;
        luaL_getmetatable(L, KLASS_META);
        lua_setmetatable(L, -2);
        return 1;
    }

    static int ksl_vector_normalize(lua_State* L)
    {
        const ksl_vector* a = (const ksl_vector*)luaL_checkudata(L, 1, KLASS_META);

        double x = (*a)[0];
        double y = (*a)[1];
        double z = (*a)[2];

        double l = x * x + y * y + z * z;
        l = 1.0 / sqrt(l);

        ksl_vector* v = (ksl_vector*)lua_newuserdata(L, sizeof(ksl_vector));
        (*v)[0] = x * l;
        (*v)[1] = y * l;
        (*v)[2] = z * l;
        luaL_getmetatable(L, KLASS_META);
        lua_setmetatable(L, -2);
        return 1;
    }

    static double dot(const ksl_vector& a, const ksl_vector& b)
    {
        return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
    }

    static ksl_vector cross(const ksl_vector& a, const ksl_vector& b)
    {
        return ksl_vector(
            a[1] * b[2] - a[2] * b[1], //xyzzy
            a[2] * b[0] - a[0] * b[2], //yzxxz
            a[0] * b[1] - a[1] * b[0]  //zxyyx
            );
    }

    static double length(const ksl_vector& a)
    {
        double x = a[0];
        double y = a[1];
        double z = a[2];

        double l = sqrt(x * x + y * y + z * z);

        return l;
    }

    static double xcomp(const ksl_vector& a)
    {
        return a[0];
    }

    static double ycomp(const ksl_vector& a)
    {
        return a[1];
    }

    static double zcomp(const ksl_vector& a)
    {
        return a[2];
    }

    static int ksl_vector_dot(lua_State* L)
    {
        const ksl_vector* a = (const ksl_vector*)luaL_checkudata(L, 1, KLASS_META);
        const ksl_vector* b = (const ksl_vector*)luaL_checkudata(L, 2, KLASS_META);

        double l = dot(*a, *b);

        lua_pushnumber(L, l);
        return 1;
    }

    static int ksl_vector_cross(lua_State* L)
    {
        const ksl_vector* a = (const ksl_vector*)luaL_checkudata(L, 1, KLASS_META);
        const ksl_vector* b = (const ksl_vector*)luaL_checkudata(L, 2, KLASS_META);
        ksl_vector r = cross(*a, *b);
        ksl_vector* v = (ksl_vector*)lua_newuserdata(L, sizeof(ksl_vector));
        *v = r;
        luaL_getmetatable(L, KLASS_META);
        lua_setmetatable(L, -2);
        return 1;
    }

    static int ksl_vector_length(lua_State* L)
    {
        const ksl_vector* a = (const ksl_vector*)luaL_checkudata(L, 1, KLASS_META);

        double l = length(*a);

        lua_pushnumber(L, l);
        return 1;
    }

    static int ksl_vector_xcomp(lua_State* L)
    {
        const ksl_vector* a = (const ksl_vector*)luaL_checkudata(L, 1, KLASS_META);

        double l = xcomp(*a);

        lua_pushnumber(L, l);
        return 1;
    }

    static int ksl_vector_ycomp(lua_State* L)
    {
        const ksl_vector* a = (const ksl_vector*)luaL_checkudata(L, 1, KLASS_META);

        double l = ycomp(*a);

        lua_pushnumber(L, l);
        return 1;
    }

    static int ksl_vector_zcomp(lua_State* L)
    {
        const ksl_vector* a = (const ksl_vector*)luaL_checkudata(L, 1, KLASS_META);

        double l = zcomp(*a);

        lua_pushnumber(L, l);
        return 1;
    }

    static int ksl_vector_unm(lua_State* L)
    {
        const ksl_vector* a = (const ksl_vector*)luaL_checkudata(L, 1, KLASS_META);

        ksl_vector* v = (ksl_vector*)lua_newuserdata(L, sizeof(ksl_vector));
        *v = *a;
        *v *= -1.0;
        luaL_getmetatable(L, KLASS_META);
        lua_setmetatable(L, -2);
        return 1;
    }

    static const luaL_Reg FUNCS_M[] =
        {
            {"__index", ksl_vector_get},
            {"__newindex", ksl_vector_set},
            {"__add", ksl_vector_add},
            {"__sub", ksl_vector_sub},
            {"__mul", ksl_vector_mul},
            {"__unm", ksl_vector_unm},
            {"new", ksl_vector_new},
            {"geti", ksl_vector_geti},
            {"geti", ksl_vector_geti},
            {"seti", ksl_vector_seti},
            {NULL, NULL}};

    static const luaL_Reg FUNCS_F[] =
        {
            {"new", ksl_vector_new},
            {"geti", ksl_vector_geti},
            {"seti", ksl_vector_seti},
            {NULL, NULL}};

    int ksl_vector_open(lua_State* L)
    {
        luaL_newmetatable(L, KLASS_META);
        /*metatable.__index = metatable*/
        lua_pushvalue(L, -1); //clone metatable
        lua_setfield(L, -2, "__index");
        luaL_register(L, NULL, FUNCS_M);
        luaL_register(L, KLASS, FUNCS_F);

        lua_register(L, "ksl_vector", ksl_vector_new);
        lua_register(L, "point", ksl_vector_new);
        lua_register(L, "vector", ksl_vector_new);
        lua_register(L, "normal", ksl_vector_new);
        lua_register(L, "color", ksl_vector_new);
        lua_register(L, "ksl_vector_from_ptr", ksl_vector_new_ptr);
        lua_register(L, "vector_from_ptr", ksl_vector_new_ptr);
        lua_register(L, "normalize", ksl_vector_normalize);
        lua_register(L, "dot", ksl_vector_dot);
        lua_register(L, "cross", ksl_vector_cross);
        lua_register(L, "length", ksl_vector_length);

        lua_register(L, "xcomp", ksl_vector_xcomp);
        lua_register(L, "ycomp", ksl_vector_ycomp);
        lua_register(L, "zcomp", ksl_vector_zcomp);

        lua_pop(L, lua_gettop(L));

        return 0;
    }
}
