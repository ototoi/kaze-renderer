#include "mesured_MERL_shader_evaluator.h"

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#define _USE_MATH_DEFINES
#include <math.h>

#include <cstdlib>
#include <string>
#include <fstream>
#include <string.h>
#include <map>

#define BRDF_SAMPLING_RES_THETA_H 90
#define BRDF_SAMPLING_RES_THETA_D 90
#define BRDF_SAMPLING_RES_PHI_D 360

namespace
{
    struct MERLBRDFData
    {
        int dims[3];
        float* brdfData;
    };

    MERLBRDFData* MERLBRDFLoad(const char* szFile)
    {
        FILE* fp = fopen(szFile, "rb");
        if (!fp) return NULL;

        int dims[3];
        if (fread(dims, sizeof(int), 3, fp) != 3)
        {
            fclose(fp);
            return NULL;
        }
        int numBRDFSamples = dims[0] * dims[1] * dims[2];
        if (numBRDFSamples != BRDF_SAMPLING_RES_THETA_H * BRDF_SAMPLING_RES_THETA_D * BRDF_SAMPLING_RES_PHI_D / 2)
        {
            fclose(fp);
            return NULL;
        }

        // read the data
        double* brdf = (double*)malloc(sizeof(double) * 3 * numBRDFSamples);
        if (fread(brdf, sizeof(double), 3 * numBRDFSamples, fp) != size_t(3 * numBRDFSamples))
        {
            fclose(fp);
            return NULL;
        }
        fclose(fp);

        MERLBRDFData* pData = (MERLBRDFData*)malloc(sizeof(MERLBRDFData));
        if (!pData)
        {
            free(brdf);
            return NULL;
        }

        float* fbrdf = (float*)malloc(sizeof(float) * 3 * numBRDFSamples);
        if (!fbrdf)
        {
            free(pData);
            return NULL;
        }

        for (int i = 0; i < numBRDFSamples; i++)
        {
            fbrdf[3 * i + 0] = (float)brdf[3 * i + 0];
            fbrdf[3 * i + 1] = (float)brdf[3 * i + 1];
            fbrdf[3 * i + 2] = (float)brdf[3 * i + 2];
        }
        free(brdf);

        pData->brdfData = fbrdf;
        memcpy(pData->dims, dims, sizeof(int) * 3);

        return pData;
    }

    void MERLBRDFClose(MERLBRDFData* pData)
    {
        if (pData)
        {
            if (pData->brdfData) free(pData->brdfData);
            free(pData);
        }
    }
}

namespace rsle
{
    namespace
    {

        inline int clamp(int x, int a, int b)
        {
            return std::min<int>(std::max<int>(a, x), b);
        }

        inline float fclamp(float x, float a, float b)
        {
            return std::min<float>(std::max<float>(a, x), b);
        }

        const float RED_SCALE = (float)(1.0 / 1500.0);
        const float GREEN_SCALE = (float)(1.15 / 1500.0);
        const float BLUE_SCALE = (float)(1.66 / 1500.0);

        // Lookup phi_diff index
        int phi_diff_index(float phi_diff)
        {
            if (phi_diff < 0.0)
                phi_diff += (float)M_PI;
            return clamp(int(phi_diff * (1.0 / M_PI * (BRDF_SAMPLING_RES_PHI_D / 2))), 0, BRDF_SAMPLING_RES_PHI_D / 2 - 1);
        }
        // Lookup theta_half index
        // This is a non-linear mapping!
        // In:  [0 .. pi/2]
        // Out: [0 .. 89]
        int theta_half_index(float theta_half)
        {
            if (theta_half <= 0.0) return 0;
            return clamp(int(sqrt(theta_half * (2.0 / M_PI)) * BRDF_SAMPLING_RES_THETA_H), 0, BRDF_SAMPLING_RES_THETA_H - 1);
        }
        // Lookup theta_diff index
        // In:  [0 .. pi/2]
        // Out: [0 .. 89]
        int theta_diff_index(float theta_diff)
        {
            return clamp(int(theta_diff * (2.0 / M_PI * BRDF_SAMPLING_RES_THETA_D)), 0, BRDF_SAMPLING_RES_THETA_D - 1);
        }

        float phi_diff_index_f(float phi_diff)
        {
            if (phi_diff < 0.0) phi_diff += (float)M_PI;
            return fclamp(phi_diff * (1.0f / M_PI * (BRDF_SAMPLING_RES_PHI_D / 2 - 1)), 0, BRDF_SAMPLING_RES_PHI_D / 2 - 1);
        }
        float theta_half_index_f(float theta_half)
        {
            if (theta_half <= 0.0) return 0;
            return fclamp(sqrt(theta_half * (2.0 / M_PI)) * (BRDF_SAMPLING_RES_THETA_H - 1), 0, BRDF_SAMPLING_RES_THETA_H - 1);
        }
        float theta_diff_index_f(float theta_diff)
        {
            return fclamp(theta_diff * (2.0 / M_PI * BRDF_SAMPLING_RES_THETA_D - 1), 0, BRDF_SAMPLING_RES_THETA_D - 1);
        }

        static vector3f BRDFSampling(const float measuredData[], int index)
        {
            int redIndex = index;
            int greenIndex = index + BRDF_SAMPLING_RES_THETA_H * BRDF_SAMPLING_RES_THETA_D * BRDF_SAMPLING_RES_PHI_D / 2;
            int blueIndex = index + BRDF_SAMPLING_RES_THETA_H * BRDF_SAMPLING_RES_THETA_D * BRDF_SAMPLING_RES_PHI_D;

            float r = measuredData[redIndex] * RED_SCALE;
            float g = measuredData[greenIndex] * GREEN_SCALE;
            float b = measuredData[blueIndex] * BLUE_SCALE;
            return vector3f(r, g, b);
        }

        static int GetIndex(int phi_idx, int tdiff_idx, int thalf_idx)
        {
            return phi_idx +
                   tdiff_idx * BRDF_SAMPLING_RES_PHI_D / 2 +
                   thalf_idx * BRDF_SAMPLING_RES_PHI_D / 2 *
                       BRDF_SAMPLING_RES_THETA_D;
        }

        static vector3f lerp(const vector3f& a, const vector3f& b, float t)
        {
            return a * (1 - t) + b * t;
        }

        vector3f BRDF(const float measuredData[], const vector3f& toLight, const vector3f& toViewer, const vector3f& normal, const vector3f& tangent, const vector3f& bitangent)
        {
            vector3f H = normalize(toLight + toViewer);
            float theta_H = acos(fclamp(dot(normal, H), 0.0, 1.0));
            float theta_diff = acos(fclamp(dot(H, toLight), 0.0, 1.0));
            float phi_diff = 0;

            if (theta_diff < 1e-3)
            {
                // phi_diff indeterminate, use phi_half instead
                phi_diff = atan2(fclamp(-dot(toLight, bitangent), -1.0f, 1.0f), fclamp(dot(toLight, tangent), -1.0f, 1.0f));
            }
            else if (theta_H > 1e-3)
            {
                // use Gram-Schmidt orthonormalization to find diff basis vectors
                vector3f u = -normalize(normal - dot(normal, H) * H);
                vector3f v = cross(H, u);
                phi_diff = atan2(fclamp(dot(toLight, v), -1.0f, 1.0f), fclamp(dot(toLight, u), -1.0f, 1.0f));
            }
            else
                theta_H = 0;
#if 0
			// Find index.
			// Note that phi_half is ignored, since isotropic BRDFs are assumed
			int ind = phi_diff_index(phi_diff) +
				theta_diff_index(theta_diff) * BRDF_SAMPLING_RES_PHI_D / 2 +
				theta_half_index(theta_H) * BRDF_SAMPLING_RES_PHI_D / 2 *
				BRDF_SAMPLING_RES_THETA_D;

			color3f c = BRDFSampling(measuredData, ind);
			
			return c;
#else
            int phi_idx[2];
            float phi_t = 0;
            {
                float f = phi_diff_index_f(phi_diff);
                int idx = (int)floor(f);
                phi_t = f - idx;
                phi_idx[0] = idx;
                phi_idx[1] = std::min<int>(idx + 1, BRDF_SAMPLING_RES_PHI_D / 2 - 1);
            }

            int tdiff_idx[2];
            float tdiff_t = 0;
            {
                float f = theta_diff_index_f(theta_diff);
                int idx = (int)floor(f);
                tdiff_t = f - idx;
                tdiff_idx[0] = idx;
                tdiff_idx[1] = std::min<int>(idx + 1, BRDF_SAMPLING_RES_THETA_D - 1);
            }

            int thalf_idx[2];
            float thalf_t = 0;
            {
                float f = theta_half_index_f(theta_H);
                int idx = (int)floor(f);
                thalf_t = f - idx;
                thalf_idx[0] = idx;
                thalf_idx[1] = std::min<int>(idx + 1, BRDF_SAMPLING_RES_THETA_H - 1);
            }

            int indices[] =
                {
                    GetIndex(phi_idx[0], tdiff_idx[0], thalf_idx[0]),
                    GetIndex(phi_idx[0], tdiff_idx[0], thalf_idx[1]),
                    GetIndex(phi_idx[0], tdiff_idx[1], thalf_idx[0]),
                    GetIndex(phi_idx[0], tdiff_idx[1], thalf_idx[1]),
                    GetIndex(phi_idx[1], tdiff_idx[0], thalf_idx[0]),
                    GetIndex(phi_idx[1], tdiff_idx[0], thalf_idx[1]),
                    GetIndex(phi_idx[1], tdiff_idx[1], thalf_idx[0]),
                    GetIndex(phi_idx[1], tdiff_idx[1], thalf_idx[1])};

            color3f cols[] =
                {
                    BRDFSampling(measuredData, indices[0]),
                    BRDFSampling(measuredData, indices[1]),
                    BRDFSampling(measuredData, indices[2]),
                    BRDFSampling(measuredData, indices[3]),
                    BRDFSampling(measuredData, indices[4]),
                    BRDFSampling(measuredData, indices[5]),
                    BRDFSampling(measuredData, indices[6]),
                    BRDFSampling(measuredData, indices[7])};

            return lerp(
                lerp(
                    lerp(cols[0], cols[1], thalf_t),
                    lerp(cols[2], cols[3], thalf_t),
                    tdiff_t),
                lerp(
                    lerp(cols[4], cols[5], thalf_t),
                    lerp(cols[6], cols[7], thalf_t),
                    tdiff_t),
                phi_t);
#endif
        }
    }
}

namespace rsle
{
    namespace
    {
        class MERLBRDFDataWrap
        {
        public:
            MERLBRDFDataWrap(const char* szFile) { pData_ = MERLBRDFLoad(szFile); }
            ~MERLBRDFDataWrap() { MERLBRDFClose(pData_); }
            MERLBRDFData* GetPtr() const { return pData_; }
        protected:
            MERLBRDFData* pData_;
        };
    }

    typedef std::map<std::string, std::shared_ptr<MERLBRDFDataWrap> > map_type;

    class mesured_MERL_shader_evaluator_imp : public shader_evaluator
    {
    public:
        mesured_MERL_shader_evaluator_imp(const char* szFile);
        ~mesured_MERL_shader_evaluator_imp();

    public:
        void evaluate(shader_output* out, const shader_input* in) const;

    private:
        static map_type& GetMap()
        {
            static map_type s_map;
            return s_map;
        }
        MERLBRDFData* pData_;
    };

    mesured_MERL_shader_evaluator_imp::mesured_MERL_shader_evaluator_imp(const char* szFile)
    {
        map_type& s_map = GetMap();

        map_type::iterator i;
        if ((i = s_map.find(szFile)) != s_map.end())
        {
            pData_ = i->second->GetPtr();
        }
        else
        {
            std::shared_ptr<MERLBRDFDataWrap> brdf(new MERLBRDFDataWrap(szFile));
            pData_ = brdf->GetPtr();
            s_map.insert(map_type::value_type(szFile, brdf));
        }
    }
    
    mesured_MERL_shader_evaluator_imp::~mesured_MERL_shader_evaluator_imp()
    {
        ;
    }

    static vector3 Convert(const vector3f& v)
    {
        return vector3(v[0], v[1], v[2]);
    }

    static vector3f Convert(const vector3& v)
    {
        return vector3f((float)v[0], (float)v[1], (float)v[2]);
    }

    static void GetIntensity(const MERLBRDFData* pData, shader_output* out, const shader_input* in)
    {
        real intensity_factor = 10.0;

        vector3f V = -Convert(in->I()); //To View
        vector3f N = Convert(in->N());
        vector3f T = Convert(in->dPdu());
        vector3f B = Convert(in->dPdv());
        if (dot(N, V) < 0)
        {
            N = -N;
            //T = -T;
            //B = -B;
        }

        vector3 c = vector3(0, 0, 0);
        for (int i = 0; i < (int)in->illuminance_size(); i++)
        {
            vector3f L = Convert(in->illuminance_L(i));
            vector3f Cl = Convert(in->illuminance_Cl(i));
            c += Convert(Cl * BRDF(pData->brdfData, L, V, N, T, B));
        }

        c *= intensity_factor;

        out->Ci(c);
    }

    void mesured_MERL_shader_evaluator_imp::evaluate(shader_output* out, const shader_input* in) const
    {
        if (pData_)
        {
            GetIntensity(pData_, out, in);
        }
    }

    //------------------------------------------------------------------------------------

    mesured_MERL_shader_evaluator::mesured_MERL_shader_evaluator(const char* szFile)
    {
        imp_ = new mesured_MERL_shader_evaluator_imp(szFile);
    }

    mesured_MERL_shader_evaluator::~mesured_MERL_shader_evaluator()
    {
        delete imp_;
    }

    void mesured_MERL_shader_evaluator::evaluate(shader_output* out, const shader_input* in) const
    {
        imp_->evaluate(out, in);
    }
}