#include "matte_shader_evaluator.h"

namespace rsle
{
    void matte_shader_evaluator::evaluate(shader_output* out, const shader_input* in) const
    {
        real Ka = in->get_float("Ka", 1.0);
        real Kd = in->get_float("Kd", 1.0);

        color3 Cs = in->Cs();
        color3 Os = in->Os();

        vector3 N = in->N();
        vector3 I = in->I();

        vector3 Nf = faceforward(normalize(N), I);
        color3 Oi = Os;
        color3 Ci = Os * Cs * (Ka * in->ambient() + Kd * in->diffuse(Nf));

        out->Ci(Ci);
        out->Oi(Oi);
    }
}
