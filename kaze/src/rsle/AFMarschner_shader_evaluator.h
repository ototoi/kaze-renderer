#ifndef RSLE_AFMARSCHNER_SHADER_EVALUATOR_H
#define RSLE_AFMARSCHNER_SHADER_EVALUATOR_H

#include "shader_evaluator.h"
#include "types.h"

namespace rsle
{
    class AFMarschner_shader_evaluator : public shader_evaluator
    {
    public:
        AFMarschner_shader_evaluator();
        ~AFMarschner_shader_evaluator();

    public:
        virtual void evaluate(shader_output* out, const shader_input* in) const;

    public:
        void set(const char* key, real x);
        void set(const char* key, const color3& x);

    protected:
        real intensityD_;
        //real middleD_;
        //real middleRangeD_;
        //color3 colorRootD_;
        //color3 colorTipD_;
        real intensityR_;
        color3 colorR_;
        real longitudinalShiftR_;
        real longitudinalWidthR_;

        real intensityTT_;
        color3 colorTT_;
        real longitudinalShiftTT_;
        real longitudinalWidthTT_;
        real azimuthalWidthTT_;

        real intensityTRT_;
        color3 colorTRT_;
        real longitudinalShiftTRT_;
        real longitudinalWidthTRT_;

        real intensityG_;
        real azimuthalShiftG_;
        real azimuthalWidthG_;

        real attenuationFromRoot_;
    };
}

#endif