#include "metal_shader_evaluator.h"

namespace rsle
{
    void metal_shader_evaluator::evaluate(shader_output* out, const shader_input* in) const
    {
        real Ka = in->get_float("Ka", 1.0);
        real Ks = in->get_float("Ks", 1.0);
        real roughness = in->get_float("roughness", 0.1);

        color3 Cs = in->Cs();
        color3 Os = in->Os();

        vector3 N = in->N();
        vector3 I = in->I();

        vector3 Nf = faceforward(normalize(N), I);
        /*vector3 Nf = normalize(N);*/
        vector3 V = -normalize(I);
        color3 Oi = Os;
        color3 Ci = Os * Cs * (Ka * in->ambient() + Ks * in->specular(Nf, V, roughness));

        out->Ci(Ci);
        out->Oi(Oi);
    }
}
