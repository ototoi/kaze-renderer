#ifndef MC_MC_BOUND_H
#define MC_MC_BOUND_H

#include "mc_types.h"

namespace mc
{
    struct mc_bound
    {
        vector3 min;
        vector3 max;
    };
}

#endif