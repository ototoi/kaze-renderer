#ifndef MC_MC_TYPES_H
#define MC_MC_TYPES_H

#include <tempest/vector.hpp>
#include <tempest/vector3.hpp>
#include <tempest/vector4.hpp>
#include <tempest/vector_functions.hpp>

#include <tempest/matrix.hpp>
#include <tempest/matrix4.hpp>
#include <tempest/matrix_functions.hpp>
#include <tempest/quaternion.hpp>
#include <tempest/matrix_generator.hpp>

#include <tempest/transform.hpp>

namespace mc
{
    typedef double real;
    typedef tempest::vector<real,3> vector3;
    typedef tempest::matrix<real,4,4> matrix4;
    typedef tempest::matrix_generator<matrix4> matrix_generator4;
    typedef matrix_generator4 mat4_gen;
}

#endif
