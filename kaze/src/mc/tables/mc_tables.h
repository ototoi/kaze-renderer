#ifndef MC_MC_TABLES_H
#define MC_MC_TABLES_H

namespace mc
{
    /*
     * Polygonising a scalar field
     * (http://local.wasp.uwa.edu.au/~pbourke/geometry/polygonise/)
     */
    extern const int edgeTable[256];
    extern const int triTable[256][16];
}

#endif