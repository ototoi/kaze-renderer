#include "mc_volume_evaluator.h"
//#include "values.h"
#include <algorithm>
#include <vector>

//#include "logger.h"

namespace mc
{
    typedef branch_mc_volume_evaluator::bound bound;

    static inline real clamp(real x)
    {
        return std::min<real>(std::max<real>(0, x), 1);
    }

    //
    //
    //
    //((3-x*x)*x*x-3)*x*x+1
    //3*xxxx - xxxxxx -3*xx + 1
    //-x^6 + 3x^4 - 3x^2 + 1

    //-6x^5 + 12x^3 -6x

    //6x^2 - 3x^4 -3

    static inline real geoff(real r2)
    {
        // 1 - 3*r2 + 3*r2*r2 - r2*r2*r2
        // 1-3*r2 - r2
        r2 = std::min<real>(r2, 1);
        return ((3 - r2) * r2 - 3) * r2 + 1;
    }

    static inline real dgeoff(real r2)
    {
        r2 = std::min<real>(r2, 1);
        return (6 - 3 * r2) * r2 - 3; //*2r
    }

    static inline real weight(real x)
    {
        return geoff(x * x);
    }

    static inline real weight2(real x2)
    {
        return clamp(geoff(x2));
    }

    static inline real dweight(real x)
    {
        return 2 * x * dgeoff(x);
    }

    static bound TransformBound(const bound& b, const matrix4& m)
    {
        vector3 min = b.min;
        vector3 max = b.max;
        vector3 points[] =
            {
                vector3(min[0], min[1], min[2]),
                vector3(min[0], min[1], max[2]),
                vector3(min[0], max[1], min[2]),
                vector3(min[0], max[1], max[2]),
                vector3(max[0], min[1], min[2]),
                vector3(max[0], min[1], max[2]),
                vector3(max[0], max[1], min[2]),
                vector3(max[0], max[1], max[2])};

        for (int i = 0; i < 8; i++)
        {
            points[i] = m * points[i];
        }
        vector3 cmin, cmax;
        cmin = cmax = points[0];
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                cmin[j] = std::min<real>(cmin[j], points[i][j]);
                cmax[j] = std::max<real>(cmax[j], points[i][j]);
            }
        }
        bound r;
        r.min = cmin;
        r.max = cmax;
        return r;
    }

    static inline vector3 MulNormal(const matrix4& m, const vector3& v)
    {
        return vector3(
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
    }

    static inline bool TestBound(const bound& b, const vector3& p)
    {
        if (p[0] < b.min[0] || b.max[0] < p[0]) return false;
        if (p[1] < b.min[1] || b.max[1] < p[1]) return false;
        if (p[2] < b.min[2] || b.max[2] < p[2]) return false;
        return true;
    }

    static inline bool IntersectBound(const bound& a, const bound& b)
    {
        if (a.max[0] < b.min[0] || b.max[0] < a.min[0]) return false;
        if (a.max[1] < b.min[1] || b.max[1] < a.min[1]) return false;
        if (a.max[2] < b.min[2] || b.max[2] < a.min[2]) return false;
        return true;
    }

    transformed_mc_volume_evaluator::transformed_mc_volume_evaluator(const std::shared_ptr<mc_volume_evaluator>& inter, const matrix4& l2w)
        : inter_(inter)
    {
        m_ = l2w;
        im_ = ~m_;
        b_ = TransformBound(inter_->get_bound(), m_);
    }

    bool transformed_mc_volume_evaluator::is_inside(const vector3& p) const
    {
        return inter_->is_inside(im_ * p);
    }
    real transformed_mc_volume_evaluator::get_value(const vector3& p) const
    {
        return inter_->get_value(im_ * p);
    }
    /*
    vector3 transformed_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        return MulNormal(im_, inter_->get_gradient(im_ * p));
        //return im_ * inter_->get_gradient(im_ * p);
    }
    */
    bound transformed_mc_volume_evaluator::get_bound() const
    {
        return b_;
    }

    void transformed_mc_volume_evaluator::get_bounds(std::vector<bound>& bounds) const
    {
        std::vector<bound> tmp;
        inter_->get_bounds(tmp);
        size_t sz = tmp.size();
        for(size_t i = 0;i<sz;i++)
        {
            bounds.push_back(TransformBound(tmp[i], m_));
        }
    }

    bool transformed_mc_volume_evaluator::test_bound(const bound& b) const
    {
        return IntersectBound(b_, b); //world2local
    }
    void transformed_mc_volume_evaluator::get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const
    {
        inter_->get_attribute(im_ * p, attr, value); //world2local
    }

    offset_mc_volume_evaluator::offset_mc_volume_evaluator(const std::shared_ptr<mc_volume_evaluator>& c)
        : c_(c)
    {
        k_ = 0.421875;
    }

    offset_mc_volume_evaluator::offset_mc_volume_evaluator(const std::shared_ptr<mc_volume_evaluator>& c, real k)
        : c_(c)
    {
        k_ = k;
    }

    offset_mc_volume_evaluator::~offset_mc_volume_evaluator()
    {
        ; //
    }
    bool offset_mc_volume_evaluator::is_inside(const vector3& p) const
    {
        return c_->is_inside(p);
    }
    real offset_mc_volume_evaluator::get_value(const vector3& p) const
    {
        return c_->get_value(p) - k_;
    }
    /*
    vector3 offset_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        return c_->get_gradient(p);
    }
    */
    bound offset_mc_volume_evaluator::get_bound() const
    {
        return c_->get_bound();
    }
    bool offset_mc_volume_evaluator::test_bound(const bound& b) const
    {
        return c_->test_bound(b);
    }
    void offset_mc_volume_evaluator::get_bounds(std::vector<bound>& bounds) const
    {
        return c_->get_bounds(bounds);
    }
    void offset_mc_volume_evaluator::get_attribute(const vector3& p, const mc_attribute_key& attr, std::vector<real>& value) const
    {
        c_->get_attribute(p, attr, value);
    }

    fma_mc_volume_evaluator::fma_mc_volume_evaluator(real a, const std::shared_ptr<mc_volume_evaluator>& b, real c, real min, real max)
        : a_(a), b_(b), c_(c), min_(min), max_(max)
    {
        ;
    }
    fma_mc_volume_evaluator::~fma_mc_volume_evaluator()
    {
    }
    bool fma_mc_volume_evaluator::is_inside(const vector3& p) const { return b_->is_inside(p); }
    real fma_mc_volume_evaluator::get_value(const vector3& p) const { return std::max(std::min(max_, a_ * b_->get_value(p) + c_), min_); }
    /*
    vector3 fma_mc_volume_evaluator::get_gradient(const vector3& p) const { return a_ * b_->get_gradient(p); }
    */
    bound fma_mc_volume_evaluator::get_bound() const { return b_->get_bound(); }
    void fma_mc_volume_evaluator::get_bounds(std::vector<bound>& bounds) const
    {
        return b_->get_bounds(bounds);
    }
    bool fma_mc_volume_evaluator::test_bound(const bound& b) const { return b_->test_bound(b); }
    void fma_mc_volume_evaluator::get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const { b_->get_attribute(p, attr, value); }

    //--------------------------------------------------------
    typedef std::map<mc_attribute_key, std::vector<real> > attr_map_type;
    bool leaf_mc_volume_evaluator::test_bound(const bound& b) const
    {
        return IntersectBound(b, this->get_bound());
    }
    void leaf_mc_volume_evaluator::set_attribute(const attribute& attr, const std::vector<real>& value)
    {
        attrs_.insert(attr_map_type::value_type(attr, value));
    }
    //--------------------------------------------------------
    static vector3 min_(const vector3& x, const vector3& y)
    {
        if (sqr_length(x) <= sqr_length(y))
            return x;
        else
            return y;
    }

    static vector3 max_(const vector3& x, const vector3& y)
    {
        if (sqr_length(x) >= sqr_length(y))
            return x;
        else
            return y;
    }

    struct Func
    {
        Func(){}
        virtual real operator()(real x, real y) const = 0;
        virtual vector3 operator()(const vector3& x, const vector3& y) const = 0;
    };

    struct AddFunc : public Func
    {
        AddFunc(){}
        real operator()(real x, real y) const { return x + y; }
        vector3 operator()(const vector3& x, const vector3& y) const { return x + y; }
    };

    struct SubFunc : public Func
    {
        real operator()(real x, real y) const { return x - y; }
        vector3 operator()(const vector3& x, const vector3& y) const { return x - y; }
    };

    struct MulFunc : public Func
    {
        real operator()(real x, real y) const { return x * y; }
        vector3 operator()(const vector3& x, const vector3& y) const { return x * y; }
    };

    struct MinFunc : public Func
    {
        real operator()(real x, real y) const { return std::min<real>(x, y); }
        vector3 operator()(const vector3& x, const vector3& y) const { return min_(x, y); }
    };

    struct MaxFunc : public Func
    {
        real operator()(real x, real y) const { return std::max<real>(x, y); }
        vector3 operator()(const vector3& x, const vector3& y) const { return max_(x, y); }
    };

    class blob_volume_bvh_node
    {
    public:
        virtual ~blob_volume_bvh_node() {}
        virtual void get(std::vector<const mc_volume_evaluator*>& ev, const vector3& p) const = 0;
        virtual bound get_bound() const = 0;
        virtual bool test_bound(const bound& b) const = 0;
        virtual real get_value(real x, const Func& f, const vector3& p) const = 0;
        //virtual vector3 get_gradient(vector3 x, const Func& f, const vector3& p) const = 0;
        virtual bool is_inside(const vector3& p) const = 0;
    };

    class leaf_blob_volume_bvh_node : public blob_volume_bvh_node
    {
    public:
        typedef const mc_volume_evaluator* PCTR;

    public:
        leaf_blob_volume_bvh_node(const std::vector<const mc_volume_evaluator*>& mv)
            : mv_(mv)
        {
            size_t sz = mv.size();
            const PCTR* m = &mv[0];
            bound b = m[0]->get_bound();
            for (size_t i = 1; i < sz; i++)
            {
                bound mb = m[i]->get_bound();
                for (int j = 0; j < 3; j++)
                {
                    b.min[j] = std::min<real>(b.min[j], mb.min[j]);
                    b.max[j] = std::max<real>(b.max[j], mb.max[j]);
                }
            }
            b_ = b;
        }
        void get(std::vector<const mc_volume_evaluator*>& ev, const vector3& p) const
        {
            if (TestBound(b_, p))
            {
                size_t sz = mv_.size();
                const PCTR* mv = &mv_[0];
                for (size_t i = 0; i < sz; i++)
                {
                    if (mv[i]->is_inside(p))
                    {
                        ev.push_back(mv[i]);
                    }
                }
            }
        }
        bound get_bound() const
        {
            return b_;
        }
        bool test_bound(const bound& b) const
        {
            if (IntersectBound(b_, b))
            {
                size_t sz = mv_.size();
                const PCTR* mv = &mv_[0];
                for (size_t i = 0; i < sz; i++)
                {
                    if (mv[i]->test_bound(b)) return true;
                }
            }
            return false;
        }
        real get_value(real x, const Func& f, const vector3& p) const
        {
            if (TestBound(b_, p))
            {
                size_t sz = mv_.size();
                const PCTR* mv = &mv_[0];
                for (size_t i = 0; i < sz; i++)
                {
                    if (mv[i]->is_inside(p))
                    {
                        x = f(x, mv[i]->get_value(p));
                    }
                }
            }
            return x;
        }
        /*
        vector3 get_gradient(vector3 x, const Func& f, const vector3& p) const
        {
            if (TestBound(b_, p))
            {
                size_t sz = mv_.size();
                const PCTR* mv = &mv_[0];
                for (size_t i = 0; i < sz; i++)
                {
                    if (mv[i]->is_inside(p))
                    {
                        x = f(x, mv[i]->get_gradient(p));
                    }
                }
            }
            return x;
        }
        */
        bool is_inside(const vector3& p) const
        {
            if (TestBound(b_, p))
            {
                size_t sz = mv_.size();
                const PCTR* mv = &mv_[0];
                for (size_t i = 0; i < sz; i++)
                {
                    if (mv[i]->is_inside(p)) return true;
                }
            }
            return false;
        }

    private:
        std::vector<const mc_volume_evaluator*> mv_;
        bound b_;
    };

    struct blob_sorter
    {
        blob_sorter(int plane) : plane_(plane) {}
        bool operator()(const mc_volume_evaluator* a, const mc_volume_evaluator* b)
        {
            int p = plane_;
            bound bnda = a->get_bound();
            bound bndb = b->get_bound();
            return (bnda.min[p] + bnda.max[p]) < (bndb.min[p] + bndb.max[p]);
        }

        int plane_;
    };

    class branch_blob_volume_bvh_node : public blob_volume_bvh_node
    {
    public:
        branch_blob_volume_bvh_node(std::vector<const mc_volume_evaluator*>& mv)
        {
            nodes_[0] = 0;
            nodes_[1] = 0;
            size_t sz = mv.size();
            bound b = mv[0]->get_bound();
            for (size_t i = 1; i < sz; i++)
            {
                bound mb = mv[i]->get_bound();
                for (int j = 0; j < 3; j++)
                {
                    b.min[j] = std::min<real>(b.min[j], mb.min[j]);
                    b.max[j] = std::max<real>(b.max[j], mb.max[j]);
                }
            }
            vector3 wid = b.max - b.min;
            int plane = 0;
            if (wid[plane] < wid[1]) plane = 1;
            if (wid[plane] < wid[2]) plane = 2;

            std::sort(mv.begin(), mv.end(), blob_sorter(plane));

            size_t lsz = sz >> 1;
            size_t rsz = sz - lsz;

            if (lsz)
            {
                std::vector<const mc_volume_evaluator*> c(lsz);
                memcpy(&c[0], &mv[0], sizeof(const mc_volume_evaluator*) * lsz);
                if (lsz <= 8)
                {
                    nodes_[0] = new leaf_blob_volume_bvh_node(c);
                }
                else
                {
                    nodes_[0] = new branch_blob_volume_bvh_node(c);
                }
            }

            if (rsz)
            {
                std::vector<const mc_volume_evaluator*> c(rsz);
                memcpy(&c[0], &mv[lsz], sizeof(const mc_volume_evaluator*) * rsz);
                if (rsz <= 8)
                {
                    nodes_[1] = new leaf_blob_volume_bvh_node(c);
                }
                else
                {
                    nodes_[1] = new branch_blob_volume_bvh_node(c);
                }
            }

            b_ = b;
        }
        ~branch_blob_volume_bvh_node()
        {
            if (nodes_[0]) delete nodes_[0];
            if (nodes_[1]) delete nodes_[1];
        }
        void get(std::vector<const mc_volume_evaluator*>& ev, const vector3& p) const
        {
            if (TestBound(b_, p))
            {
                if (nodes_[0]) nodes_[0]->get(ev, p);
                if (nodes_[1]) nodes_[1]->get(ev, p);
            }
        }
        bound get_bound() const
        {
            return b_;
        }
        bool test_bound(const bound& b) const
        {
            if (IntersectBound(b_, b))
            {
                if (nodes_[0] && nodes_[0]->test_bound(b)) return true;
                if (nodes_[1] && nodes_[1]->test_bound(b)) return true;
            }
            return false;
        }
        real get_value(real x, const Func& f, const vector3& p) const
        {
            if (TestBound(b_, p))
            {
                if (nodes_[0]) x = nodes_[0]->get_value(x, f, p);
                if (nodes_[1]) x = nodes_[1]->get_value(x, f, p);
            }
            return x;
        }
        /*
        vector3 get_gradient(vector3 x, const Func& f, const vector3& p) const
        {
            if (TestBound(b_, p))
            {
                if (nodes_[0]) x = nodes_[0]->get_gradient(x, f, p);
                if (nodes_[1]) x = nodes_[1]->get_gradient(x, f, p);
            }
            return x;
        }
        */
        bool is_inside(const vector3& p) const
        {
            if (TestBound(b_, p))
            {
                if (nodes_[0] && nodes_[0]->is_inside(p)) return true;
                if (nodes_[1] && nodes_[1]->is_inside(p)) return true;
            }
            return false;
        }

    private:
        blob_volume_bvh_node* nodes_[2];
        bound b_;
    };

    class blob_volume_bvh
    {
    public:
        blob_volume_bvh(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv)
        {
            size_t sz = mv.size();
            std::vector<const mc_volume_evaluator*> ptr;
            for (size_t i = 0; i < sz; i++)
            {
                ptr.push_back(mv[i].get());
            }
            if (sz <= 4)
            {
                root_ = new leaf_blob_volume_bvh_node(ptr);
            }
            else
            {
                root_ = new branch_blob_volume_bvh_node(ptr);
            }
        }
        ~blob_volume_bvh()
        {
            if (root_) delete root_;
        }
        void get(std::vector<const mc_volume_evaluator*>& ev, const vector3& p) const
        {
            if (root_) root_->get(ev, p);
        }
        bound get_bound() const
        {
            return root_->get_bound();
        }
        bool test_bound(const bound& b) const
        {
            return root_->test_bound(b);
        }
        real get_value(real x, const Func& f, const vector3& p) const
        {
            return root_->get_value(x, f, p);
        }
        /*
        vector3 get_gradient(const vector3& x, const Func& f, const vector3& p) const
        {
            return root_->get_gradient(x, f, p);
        }
        */
        bool is_inside(const vector3& p) const
        {
            return root_->is_inside(p);
        }

    protected:
        blob_volume_bvh_node* root_;
    };

    branch_mc_volume_evaluator::branch_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv)
        : mv_(mv), bvh_(0)
    {
        bvh_ = new blob_volume_bvh(mv);
    }

    branch_mc_volume_evaluator::~branch_mc_volume_evaluator()
    {
        if (bvh_) delete bvh_;
    }

    void branch_mc_volume_evaluator::get(std::vector<const mc_volume_evaluator*>& ev, const vector3& p) const
    {
        bvh_->get(ev, p);
    }

    bool branch_mc_volume_evaluator::is_inside(const vector3& p) const
    {
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (mv_[i]->is_inside(p)) return true;
        }
        return false;
    }

    bound branch_mc_volume_evaluator::get_bound() const
    {
        return bvh_->get_bound();
    }

    void branch_mc_volume_evaluator::get_bounds(std::vector<bound>& bounds) const
    {
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            mv_[i]->get_bounds(bounds);
        }
    }

    bool branch_mc_volume_evaluator::test_bound(const bound& b) const
    {
        return bvh_->test_bound(b);
    }

    typedef std::shared_ptr<mc_volume_evaluator> CPTR;
    //------------------------------------------------------
    add_mc_volume_evaluator::add_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv)
        : branch_mc_volume_evaluator(mv)
    {
    }

    add_mc_volume_evaluator::~add_mc_volume_evaluator()
    {
    }

    real add_mc_volume_evaluator::get_value(const vector3& p) const
    {
        static const AddFunc f;
        return bvh_->get_value(0, f, p);
    }

    /*
    vector3 add_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        static const AddFunc f;
        return bvh_->get_gradient(vector3(0, 0, 0), f, p);
    }
    */

    void add_mc_volume_evaluator::get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const
    {
        real w = 0;
        std::vector<real> tval(attr.count);
        std::vector<const mc_volume_evaluator*> ev;
        this->get(ev, p);
        size_t sz = ev.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (ev[i]->is_inside(p))
            {
                real l = ev[i]->get_value(p);
                w += l;
                std::vector<real> val;
                ev[i]->get_attribute(p, attr, val);
                for (int k = 0; k < attr.count; k++)
                {
                    tval[k] += l * val[k];
                }
            }
        }
        for (int k = 0; k < attr.count; k++)
        {
            tval[k] /= w;
        }
        value.swap(tval);
    }

    //------------------------------------------------------

    sub_mc_volume_evaluator::sub_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv)
        : branch_mc_volume_evaluator(mv)
    {
        ;
    }
    sub_mc_volume_evaluator::~sub_mc_volume_evaluator()
    {
        ;
    }

    real sub_mc_volume_evaluator::get_value(const vector3& p) const
    {
        if (mv_.size() == 2)
        {
            int nPat = 0;
            if (mv_[0]->is_inside(p)) nPat |= 1;
            if (mv_[1]->is_inside(p)) nPat |= 2;
            if (nPat == 0) return 0;
            if (nPat == 1) return mv_[0]->get_value(p);
            if (nPat == 2) return -mv_[1]->get_value(p);
            return mv_[0]->get_value(p) - mv_[1]->get_value(p);
        }
        if (mv_.size() != 0) return mv_[0]->get_value(p);
        return 0;
    }
    /*
    vector3 sub_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        if (mv_.size() == 2)
        {
            int nPat = 0;
            if (mv_[0]->is_inside(p)) nPat |= 1;
            if (mv_[1]->is_inside(p)) nPat |= 2;
            if (nPat == 0) return vector3(0, 0, 0);
            if (nPat == 1) return mv_[0]->get_gradient(p);
            if (nPat == 2) return -mv_[1]->get_gradient(p);
            return mv_[0]->get_gradient(p) - mv_[1]->get_gradient(p);
        }
        if (mv_.size() != 0) return mv_[0]->get_gradient(p);
        return vector3(0, 0, 0);
    }
    */
    void sub_mc_volume_evaluator::get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const
    {
        if (mv_.size() == 2)
        {
            std::vector<real> a, b;
            mv_[0]->get_attribute(p, attr, a);
            mv_[1]->get_attribute(p, attr, b);
            value.resize(attr.count);
            for (int k = 0; k < attr.count; k++)
            {
                value[k] = a[k] - b[k];
            }
        }
        else if (mv_.size() != 0)
        {
            mv_[0]->get_attribute(p, attr, value);
        }
    }

    //------------------------------------------------------

    div_mc_volume_evaluator::div_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv)
        : branch_mc_volume_evaluator(mv)
    {
        ;
    }
    div_mc_volume_evaluator::~div_mc_volume_evaluator()
    {
        ;
    }

    real div_mc_volume_evaluator::get_value(const vector3& p) const
    {
        if (mv_.size() == 2)
        {
            int nPat = 0;
            if (mv_[0]->is_inside(p)) nPat |= 1;
            if (mv_[1]->is_inside(p)) nPat |= 2;
            if (nPat == 0) return 0;
            if (nPat == 1) return mv_[0]->get_value(p);
            if (nPat == 2) return 0;
            return mv_[0]->get_value(p) / mv_[1]->get_value(p);
        }
        if (mv_.size() != 0) return mv_[0]->get_value(p);
        return 0;
    }

    static vector3 clamp_(const vector3& x)
    {
        return vector3(std::min<real>(x[0], 1), std::min<real>(x[1], 1), std::min<real>(x[2], 1));
    }
    /*
    vector3 div_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        if (mv_.size() == 2)
        {
            int nPat = 0;
            if (mv_[0]->is_inside(p)) nPat |= 1;
            if (mv_[1]->is_inside(p)) nPat |= 2;
            if (nPat == 0) return vector3(0, 0, 0);
            if (nPat == 1) return mv_[0]->get_gradient(p);
            if (nPat == 2) return vector3(0, 0, 0);
            return clamp_(mv_[0]->get_gradient(p) / mv_[1]->get_gradient(p));
        }
        if (mv_.size() != 0) return mv_[0]->get_gradient(p);
        return vector3(0, 0, 0);
    }
    */

    void div_mc_volume_evaluator::get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const
    {
        if (mv_.size() == 2)
        {
            std::vector<real> a, b;
            mv_[0]->get_attribute(p, attr, a);
            mv_[1]->get_attribute(p, attr, b);
            value.resize(attr.count);
            for (int k = 0; k < attr.count; k++)
            {
                value[k] = a[k] / b[k];
            }
        }
    }

    //------------------------------------------------------
    mul_mc_volume_evaluator::mul_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv)
        : branch_mc_volume_evaluator(mv)
    {
    }

    mul_mc_volume_evaluator::~mul_mc_volume_evaluator()
    {
    }

    real mul_mc_volume_evaluator::get_value(const vector3& p) const
    {
        static MulFunc f;
        if (bvh_->is_inside(p))
        {
            return bvh_->get_value(1, f, p);
        }
        return 0;
    }
    /*
    vector3 mul_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        static MulFunc f;
        if (bvh_->is_inside(p))
        {
            return bvh_->get_gradient(vector3(1, 1, 1), f, p);
        }
        return vector3(0, 0, 0);
    }
    */
    void mul_mc_volume_evaluator::get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const
    {
        std::vector<real> tval(attr.count);
        std::vector<const mc_volume_evaluator*> ev;
        this->get(ev, p);
        size_t sz = ev.size();
        if (sz)
        {
            for (int k = 0; k < attr.count; k++)
            {
                tval[k] = 1;
            }
            for (size_t i = 0; i < sz; i++)
            {
                std::vector<real> val;
                ev[i]->get_attribute(p, attr, val);
                for (int k = 0; k < attr.count; k++)
                {
                    tval[k] *= val[k];
                }
            }
        }
        value.swap(tval);
    }

    //------------------------------------------------------

    neg_mc_volume_evaluator::neg_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv)
        : branch_mc_volume_evaluator(mv)
    {
        ;
    }
    neg_mc_volume_evaluator::~neg_mc_volume_evaluator()
    {
        ;
    }

    real neg_mc_volume_evaluator::get_value(const vector3& p) const
    {
        return -mv_[0]->get_value(p);
    }
    /*
    vector3 neg_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        return -mv_[0]->get_gradient(p);
    }
    */

    void neg_mc_volume_evaluator::get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const
    {
        mv_[0]->get_attribute(p, attr, value);
        for (size_t i = 0; i < value.size(); i++)
        {
            value[i] *= -1;
        }
    }

    //------------------------------------------------------
    static real inv_(real x)
    {
        if (fabs(x) > 0) return clamp(1.0 / x);
        return 1;
    }
    inv_mc_volume_evaluator::inv_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv)
        : branch_mc_volume_evaluator(mv)
    {
        ;
    }
    inv_mc_volume_evaluator::~inv_mc_volume_evaluator()
    {
        ;
    }

    real inv_mc_volume_evaluator::get_value(const vector3& p) const
    {
        return inv_(mv_[0]->get_value(p));
    }
    /*
    vector3 inv_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        vector3 g = mv_[0]->get_gradient(p);
        return vector3(inv_(g[0]), inv_(g[1]), inv_(g[2]));
    }
    */

    void inv_mc_volume_evaluator::get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const
    {
        mv_[0]->get_attribute(p, attr, value);
        for (size_t i = 0; i < value.size(); i++)
        {
            value[i] = inv_(value[i]);
        }
    }

    //------------------------------------------------------
    max_mc_volume_evaluator::max_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv)
        : branch_mc_volume_evaluator(mv)
    {
    }

    max_mc_volume_evaluator::~max_mc_volume_evaluator()
    {
    }

    real max_mc_volume_evaluator::get_value(const vector3& p) const
    {
        static MaxFunc f;
        return bvh_->get_value(0, f, p);
    }
    /*
    vector3 max_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        if (bvh_->is_inside(p))
        {
            std::vector<const mc_volume_evaluator*> ev;
            this->get(ev, p);

            real rl = 0;
            vector3 rg = vector3(0, 0, 0);
            size_t sz = ev.size();
            for (size_t i = 0; i < sz; i++)
            {
                real l = ev[i]->get_value(p);
                if (rl < l)
                {
                    rl = l;
                    rg = ev[i]->get_gradient(p);
                }
            }
            return rg;
        }
        return vector3(0, 0, 0);
    }
    */
    void max_mc_volume_evaluator::get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const
    {
        real fl = -1;
        std::vector<real> tval(attr.count);
        std::vector<const mc_volume_evaluator*> ev;
        this->get(ev, p);
        size_t sz = ev.size();

        for (size_t i = 0; i < sz; i++)
        {
            if (ev[i]->is_inside(p))
            {
                real l = ev[i]->get_value(p);
                if (fl < l)
                {
                    fl = l;
                    std::vector<real> val;
                    ev[i]->get_attribute(p, attr, val);
                    tval.swap(val);
                }
            }
        }

        value.swap(tval);
    }

    //------------------------------------------------------
    min_mc_volume_evaluator::min_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv)
        : branch_mc_volume_evaluator(mv)
    {
    }

    min_mc_volume_evaluator::~min_mc_volume_evaluator()
    {
    }

    real min_mc_volume_evaluator::get_value(const vector3& p) const
    {
        static MinFunc f;
        if (bvh_->is_inside(p))
        {
            bvh_->get_value(1, f, p);
        }
        return 0;
    }
    /*
    vector3 min_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        if (bvh_->is_inside(p))
        {
            std::vector<const mc_volume_evaluator*> ev;
            this->get(ev, p);

            real rl = 1;
            vector3 rg = vector3(1, 1, 1);
            size_t sz = ev.size();
            for (size_t i = 0; i < sz; i++)
            {
                real l = ev[i]->get_value(p);
                if (rl > l)
                {
                    rl = l;
                    rg = ev[i]->get_gradient(p);
                }
            }
            return rg;
        }
        return vector3(0, 0, 0);
    }
    */
    void min_mc_volume_evaluator::get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const
    {
        real fl = 1;
        std::vector<real> tval(attr.count);
        std::vector<const mc_volume_evaluator*> ev;
        this->get(ev, p);
        size_t sz = ev.size();

        for (size_t i = 0; i < sz; i++)
        {
            if (ev[i]->is_inside(p))
            {
                real l = ev[i]->get_value(p);
                if (fl > l)
                {
                    std::vector<real> val;
                    ev[i]->get_attribute(p, attr, val);
                    tval.swap(val);
                }
            }
        }

        value.swap(tval);
    }

    //------------------------------------------------------

    sphere_mc_volume_evaluator::sphere_mc_volume_evaluator(const vector3& p, real r)
        : p_(p), r_(r)
    {
        ir2_ = 1.0 / (r * r);
    }
    sphere_mc_volume_evaluator::~sphere_mc_volume_evaluator()
    {
        ; //
    }

    real sphere_mc_volume_evaluator::get_value(const vector3& p) const
    {
        return weight2(clamp(sqr_length(p - p_) * ir2_));
    }

    static inline vector3 mul_normal(const matrix4& m, const vector3& v)
    {
        return vector3(
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
    }

    static inline matrix4 GetIM(const vector3& p, real r)
    {
        real ir = 1.0 / r;
        return mat4_gen::scaling(ir, ir, ir) * mat4_gen::translation(-p[0], -p[1], -p[2]);
    }
    /*
    vector3 sphere_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        matrix4 im = GetIM(p_, r_);
        vector3 lp = im * p;
        real l = length(lp);
        real w = dweight(l);
        return mul_normal(im, w * normalize(lp));
    }
    */

    bool sphere_mc_volume_evaluator::is_inside(const vector3& p) const
    {
        //return true;
        return (sqr_length(p - p_) * ir2_ <= 1);
    }

    bound sphere_mc_volume_evaluator::get_bound() const
    {
        bound b;
        b.min = p_ - vector3(r_, r_, r_);
        b.max = p_ + vector3(r_, r_, r_);
        return b;
    }

    void sphere_mc_volume_evaluator::get_bounds(std::vector<bound>& bounds) const
    {
        bound b = get_bound();
#if 0
        bounds.push_back(b);
#else
        static const int DIV = 4;
        vector3 min = b.min;
        vector3 max = b.max;
        vector3 wid = max - min;
        vector3 delta = wid * vector3(1.0/DIV,1.0/DIV,1.0/DIV);
        for(int k = 0;k < DIV; k++)
        {
            for(int j = 0;j < DIV; j++)
            {
                for(int i = 0;i < DIV; i++)
                {
                    bound bb;
                    bb.min = min + vector3(i,j,k) * delta;
                    bb.max = bb.min + delta;
                    bounds.push_back(bb);
                }
            }
        }
#endif
    }

    typedef std::map<mc_attribute_key, std::vector<real> > attr_map_type;

    void sphere_mc_volume_evaluator::get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const
    {
        real v = get_value(p);
        if (0 < v)
        {
            attr_map_type::const_iterator it = attrs_.find(attr);
            if (it != attrs_.end())
            {
                value = it->second;
                return;
            }
        }
        value.resize(attr.count);
        memset(&value[0], 0, sizeof(real) * attr.count);
    }

    ellipsoid_mc_volume_evaluator::ellipsoid_mc_volume_evaluator(const matrix4& m)
        : m_(m), im_(~m)
    {
        static const vector3 ep[] =
            {
                vector3(-1, -1, -1),
                vector3(-1, -1, +1),
                vector3(-1, +1, -1),
                vector3(-1, +1, +1),
                vector3(+1, -1, -1),
                vector3(+1, -1, +1),
                vector3(+1, +1, -1),
                vector3(+1, +1, +1)};
        vector3 pp = m_ * ep[0];
        vector3 min = pp;
        vector3 max = pp;
        for (int i = 1; i < 8; i++)
        {
            vector3 p = m_ * ep[i];
            for (int j = 0; j < 3; j++)
            {
                min[j] = std::min<real>(min[j], p[j]);
                max[j] = std::max<real>(max[j], p[j]);
            }
        }
        b_.min = min;
        b_.max = max;
    }
    ellipsoid_mc_volume_evaluator::~ellipsoid_mc_volume_evaluator()
    {
        ;
    }
    real ellipsoid_mc_volume_evaluator::get_value(const vector3& p) const
    { //m is l2w
        return weight2(clamp(sqr_length(im_ * p)));
    }
    /*
    vector3 ellipsoid_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        vector3 lp = im_ * p;
        real l = length(lp);
        real w = dweight(l);
        return mul_normal(im_, w * normalize(lp));
    }
    */
    bool ellipsoid_mc_volume_evaluator::is_inside(const vector3& p) const
    {
        if (!TestBound(b_, p)) return false;
        return (sqr_length(im_ * p) <= 1);
    }
    bound ellipsoid_mc_volume_evaluator::get_bound() const
    {
        return b_;
    }

    void ellipsoid_mc_volume_evaluator::get_bounds(std::vector<bound>& bounds) const
    {
        bounds.push_back(get_bound());
    }

    segment_mc_volume_evaluator::segment_mc_volume_evaluator(const vector3& p0, const vector3& p1, real r, const matrix4& m)
        : p0_(p0), p1_(p1), r_(r), m_(m)
    {
        im_ = ~m;
    }
    segment_mc_volume_evaluator::~segment_mc_volume_evaluator()
    {
    }
    static real line_rate(const vector3& a, const vector3& b, const vector3& c)
    {
        vector3 ab = b - a;
        vector3 ac = c - a;
        return dot(ac, ab) / dot(ab, ab);
    }

    real segment_mc_volume_evaluator::get_value(const vector3& p) const
    {
        vector3 lp = im_ * p;
        real t = clamp(line_rate(p0_, p1_, lp));
        vector3 c = p0_ * (1 - t) + p1_ * t;
        real l = length(c - lp);
        return weight(l / r_);
    }
    /*
    vector3 segment_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        vector3 lp = im_ * p;
        real t = clamp(line_rate(p0_, p1_, lp));
        vector3 c = p0_ * (1 - t) + p1_ * t;
        real l = length(c - lp);
        return dweight(l / r_) * normalize(lp - c);
    }
    */
    bool segment_mc_volume_evaluator::is_inside(const vector3& p) const
    {
        vector3 lp = im_ * p;
        real t = clamp(line_rate(p0_, p1_, lp));
        vector3 c = p0_ * (1 - t) + p1_ * t;
        real l = length(c - lp);
        if (l <= r_)
        {
            return true;
        }
        return false;
    }
    static vector3 gd(int i, real r_)
    {
        return vector3(
            (i & 1) ? +r_ : -r_,
            (i & 2) ? +r_ : -r_,
            (i & 4) ? +r_ : -r_);
    }
    bound segment_mc_volume_evaluator::get_bound() const
    {
        vector3 points[16];
        for (int i = 0; i < 8; i++)
        {
            points[i] = m_ * (p0_ + gd(i, r_));
            points[i + 8] = m_ * (p1_ + gd(i, r_));
        }

        vector3 min, max;
        min = max = points[0];
        for (int i = 1; i < 16; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                min[j] = std::min<real>(min[j], points[i][j]);
                max[j] = std::max<real>(max[j], points[i][j]);
            }
        }
        bound b;
        b.min = min;
        b.max = max;
        return b;
    }

    void segment_mc_volume_evaluator::get_bounds(std::vector<bound>& bounds) const
    {
        bounds.push_back(get_bound());
    }

    impl_segment_mc_volume_evaluator::impl_segment_mc_volume_evaluator(const vector3& p0, real r0, const vector3& p1, real r1)
        : p0_(p0), r0_(r0), p1_(p1), r1_(r1)
    {
        ;
    }
    impl_segment_mc_volume_evaluator::~impl_segment_mc_volume_evaluator()
    {
        ;
    }

    real impl_segment_mc_volume_evaluator::get_value(const vector3& p) const
    {
        real t = clamp(line_rate(p0_, p1_, p));
        vector3 c = p0_ * (1 - t) + p1_ * t;
        real r = r0_ * (1 - t) + r1_ * t;
        real l = length(c - p);
        real r2 = l / r;
        r2 *= r2;
        return geoff(r2);
    }
    /*
    vector3 impl_segment_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        real t = clamp(line_rate(p0_, p1_, p));
        vector3 c = p0_ * (1 - t) + p1_ * t;
        real r = r0_ * (1 - t) + r1_ * t;
        real l = length(c - p);
        real r2 = l / r;
        return 2.0 * dgeoff(r2 * r2) * normalize(p - c);
    }
    */
    bool impl_segment_mc_volume_evaluator::is_inside(const vector3& p) const
    {
        real t = clamp(line_rate(p0_, p1_, p));
        vector3 c = p0_ * (1 - t) + p1_ * t;
        real r = r0_ * (1 - t) + r1_ * t;
        real l = length(c - p);
        if (l <= r)
        {
            return true;
        }
        return false;
    }

    bound impl_segment_mc_volume_evaluator::get_bound() const
    {
        vector3 points[16];
        for (int i = 0; i < 8; i++)
        {
            points[i] = (p0_ + gd(i, r0_));
            points[i + 8] = (p1_ + gd(i, r1_));
        }

        vector3 min, max;
        min = max = points[0];
        for (int i = 1; i < 16; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                min[j] = std::min<real>(min[j], points[i][j]);
                max[j] = std::max<real>(max[j], points[i][j]);
            }
        }
        bound b;
        b.min = min;
        b.max = max;
        return b;
    }
    void impl_segment_mc_volume_evaluator::get_bounds(std::vector<bound>& bounds) const
    {
        bounds.push_back(get_bound());
    }

    void impl_segment_mc_volume_evaluator::get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const
    {
        real v = get_value(p);
        if (0 < v)
        {
            attr_map_type::const_iterator it = attrs_.find(attr);
            if (it != attrs_.end())
            {
                value = it->second;
                return;
            }
        }
        value.resize(attr.count);
        memset(&value[0], 0, sizeof(real) * attr.count);
    }

    void impl_segment_mc_volume_evaluator::set_attribute(const attribute& attr, const std::vector<real>& value)
    {
        attrs_[attr] = value;
    }

    impl_cube_mc_volume_evaluator::impl_cube_mc_volume_evaluator()
    {
    }
    impl_cube_mc_volume_evaluator::~impl_cube_mc_volume_evaluator()
    {
    }
    real impl_cube_mc_volume_evaluator::get_value(const vector3& p) const
    {
        real dist[3] = {};
        dist[0] = fabs(p[0]);
        dist[1] = fabs(p[1]);
        dist[2] = fabs(p[2]);
        if (dist[0] > 1) return 0;
        if (dist[1] > 1) return 0;
        if (dist[2] > 1) return 0;
        real l = std::max<real>(std::max<real>(dist[0], dist[1]), dist[2]);
        return geoff(l * l);
    }
    /*
    vector3 impl_cube_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        vector3 sq;
        vector3 grad;
        grad[0] = 0.;
        grad[1] = 0.;
        grad[2] = 0.;
        sq[0] = p[0] * p[0];
        sq[1] = p[1] * p[1];
        sq[2] = p[2] * p[2];
        if (sq[0] > sq[1])
        {
            if (sq[0] > sq[2])
                grad[0] = 2. * p[0] * dgeoff(sq[0]);
            else
                grad[2] = 2. * p[2] * dgeoff(sq[2]);
        }
        else if (sq[1] > sq[2])
            grad[1] = 2. * p[1] * dgeoff(sq[1]);
        else
            grad[2] = 2. * p[2] * dgeoff(sq[2]);

        return grad;
    }
    */
    bool impl_cube_mc_volume_evaluator::is_inside(const vector3& p) const
    {
        real dist[3] = {};
        dist[0] = fabs(p[0]);
        dist[1] = fabs(p[1]);
        dist[2] = fabs(p[2]);
        if (dist[0] > 1) return false;
        if (dist[1] > 1) return false;
        if (dist[2] > 1) return false;
        return true;
    }
    bound impl_cube_mc_volume_evaluator::get_bound() const
    {
        bound b;
        b.min = vector3(-1, -1, -1);
        b.max = vector3(+1, +1, +1);
        return b;
    }
    void impl_cube_mc_volume_evaluator::get_bounds(std::vector<bound>& bounds) const
    {
        bounds.push_back(get_bound());
    }

    //----------------------------------------------------
    bounded_mc_volume_evaluator::bounded_mc_volume_evaluator(const std::shared_ptr<mc_volume_evaluator> inter, const bound& b)
        : inter_(inter), b_(b)
    {
    }

    bool bounded_mc_volume_evaluator::is_inside(const vector3& p) const
    {
        if (TestBound(b_, p))
        {
            return inter_->is_inside(p);
        }
        return false;
    }
    real bounded_mc_volume_evaluator::get_value(const vector3& p) const
    {
        if (TestBound(b_, p))
        {
            return inter_->get_value(p);
        }
        return 0;
    }
    /*
    vector3 bounded_mc_volume_evaluator::get_gradient(const vector3& p) const
    {
        if (TestBound(b_, p))
        {
            return inter_->get_gradient(p);
        }
        return vector3(0, 0, 0);
    }
    */
    bound bounded_mc_volume_evaluator::get_bound() const
    {
        return b_;
    }
    void bounded_mc_volume_evaluator::get_bounds(std::vector<bound>& bounds) const
    {
        inter_->get_bounds(bounds);
    }
    bool bounded_mc_volume_evaluator::test_bound(const bound& b) const
    {
        if (IntersectBound(b_, b))
        {
            return inter_->test_bound(b);
        }
        return false;
    }
    void bounded_mc_volume_evaluator::get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const
    {
        if (TestBound(b_, p))
        {
            inter_->get_attribute(p, attr, value);
        }
    }
}
