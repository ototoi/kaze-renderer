#ifndef MC_MC_VOLUME_EVALUATOR_H
#define MC_MC_VOLUME_EVALUATOR_H

#include "mc_types.h"
#include "mc_attribute_key.h"
#include "mc_bound.h"
#include <map>
#include <vector>

namespace mc
{
    class mc_volume_evaluator
    {
    public:
        typedef mc_bound bound;
        typedef mc_attribute_key attribute;

    public:
        virtual ~mc_volume_evaluator() {}
        virtual bool is_inside(const vector3& p) const = 0;
        virtual real get_value(const vector3& p) const = 0;
        virtual bound get_bound() const = 0;
        virtual void get_bounds(std::vector<bound>& bounds) const = 0;
        virtual bool test_bound(const bound& b) const = 0;
        virtual void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const {}
    };

    class transformed_mc_volume_evaluator : public mc_volume_evaluator
    {
    public:
        transformed_mc_volume_evaluator(const std::shared_ptr<mc_volume_evaluator>& inter, const matrix4& l2w);

    public:
        virtual bool is_inside(const vector3& p) const;
        virtual real get_value(const vector3& p) const;
        virtual bound get_bound() const;
        virtual void get_bounds(std::vector<bound>& bounds) const;
        virtual bool test_bound(const bound& b) const;
        virtual void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;

    protected:
        std::shared_ptr<mc_volume_evaluator> inter_;
        matrix4 m_;
        matrix4 im_;
        bound b_;
    };

    class offset_mc_volume_evaluator : public mc_volume_evaluator
    {
    public:
        offset_mc_volume_evaluator(const std::shared_ptr<mc_volume_evaluator>& c);
        offset_mc_volume_evaluator(const std::shared_ptr<mc_volume_evaluator>& c, real k);
        ~offset_mc_volume_evaluator();
        virtual bool is_inside(const vector3& p) const;
        virtual real get_value(const vector3& p) const;
        virtual bound get_bound() const;
        virtual void get_bounds(std::vector<bound>& bounds) const;
        virtual bool test_bound(const bound& b) const;
        virtual void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;

    protected:
        std::shared_ptr<mc_volume_evaluator> c_;
        real k_;
    };

    class fma_mc_volume_evaluator : public mc_volume_evaluator
    {
    public:
        fma_mc_volume_evaluator(real a, const std::shared_ptr<mc_volume_evaluator>& b, real c, real min, real max);
        ~fma_mc_volume_evaluator();
        virtual bool is_inside(const vector3& p) const;
        virtual real get_value(const vector3& p) const;
        virtual bound get_bound() const;
        virtual void get_bounds(std::vector<bound>& bounds) const;
        virtual bool test_bound(const bound& b) const;
        virtual void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;

    protected:
        real a_;
        std::shared_ptr<mc_volume_evaluator> b_;
        real c_;
        real min_;
        real max_;
    };

    class leaf_mc_volume_evaluator : public mc_volume_evaluator
    {
    public:
        virtual bool test_bound(const bound& b) const;

    public:
        virtual void set_attribute(const attribute& attr, const std::vector<real>& value);

    protected:
        std::map<attribute, std::vector<real> > attrs_;
    };

    class blob_volume_bvh;
    class branch_mc_volume_evaluator : public mc_volume_evaluator
    {
    public:
        branch_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv);
        ~branch_mc_volume_evaluator();
        virtual bool is_inside(const vector3& p) const;
        virtual bound get_bound() const;
        virtual void get_bounds(std::vector<bound>& bounds) const;
        virtual bool test_bound(const bound& b) const;

    protected:
        void get(std::vector<const mc_volume_evaluator*>& ev, const vector3& p) const;

    protected:
        std::vector<std::shared_ptr<mc_volume_evaluator> > mv_;
        blob_volume_bvh* bvh_;
    };

    class add_mc_volume_evaluator : public branch_mc_volume_evaluator
    {
    public:
        add_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv);
        ~add_mc_volume_evaluator();

    public:
        real get_value(const vector3& p) const;
        vector3 get_gradient(const vector3& p) const;
        void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;
    };

    class mul_mc_volume_evaluator : public branch_mc_volume_evaluator
    {
    public:
        mul_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv);
        ~mul_mc_volume_evaluator();

    public:
        real get_value(const vector3& p) const;
        void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;
    };

    class sub_mc_volume_evaluator : public branch_mc_volume_evaluator
    {
    public:
        sub_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv);
        ~sub_mc_volume_evaluator();

    public:
        real get_value(const vector3& p) const;
        vector3 get_gradient(const vector3& p) const;
        void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;
    };

    class div_mc_volume_evaluator : public branch_mc_volume_evaluator
    {
    public:
        div_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv);
        ~div_mc_volume_evaluator();

    public:
        real get_value(const vector3& p) const;
        void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;
    };

    class neg_mc_volume_evaluator : public branch_mc_volume_evaluator
    {
    public:
        neg_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv);
        ~neg_mc_volume_evaluator();

    public:
        real get_value(const vector3& p) const;
        void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;
    };

    class inv_mc_volume_evaluator : public branch_mc_volume_evaluator
    {
    public:
        inv_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv);
        ~inv_mc_volume_evaluator();

    public:
        real get_value(const vector3& p) const;
        vector3 get_gradient(const vector3& p) const;
        void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;
    };

    class min_mc_volume_evaluator : public branch_mc_volume_evaluator
    {
    public:
        min_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv);
        ~min_mc_volume_evaluator();

    public:
        real get_value(const vector3& p) const;
        void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;
    };

    class max_mc_volume_evaluator : public branch_mc_volume_evaluator
    {
    public:
        max_mc_volume_evaluator(const std::vector<std::shared_ptr<mc_volume_evaluator> >& mv);
        ~max_mc_volume_evaluator();

    public:
        real get_value(const vector3& p) const;
        void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;
    };

    class sphere_mc_volume_evaluator : public leaf_mc_volume_evaluator
    {
    public:
        sphere_mc_volume_evaluator(const vector3& p, real radius);
        ~sphere_mc_volume_evaluator();

    public:
        real get_value(const vector3& p) const;
        bool is_inside(const vector3& p) const;
        bound get_bound() const;
        void get_bounds(std::vector<bound>& bounds) const;
        void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;

    protected:
        vector3 p_;
        real r_;
        real r2_;
        real ir2_;
    };

    class ellipsoid_mc_volume_evaluator : public leaf_mc_volume_evaluator
    {
    public:
        ellipsoid_mc_volume_evaluator(const matrix4& m);
        ~ellipsoid_mc_volume_evaluator();

    public:
        real get_value(const vector3& p) const;
        bool is_inside(const vector3& p) const;
        bound get_bound() const;
        void get_bounds(std::vector<bound>& bounds) const;
    protected:
        matrix4 m_;
        matrix4 im_;
        bound b_;
    };

    class segment_mc_volume_evaluator : public leaf_mc_volume_evaluator
    {
    public:
        segment_mc_volume_evaluator(const vector3& p0, const vector3& p1, real r, const matrix4& m);
        ~segment_mc_volume_evaluator();

    public:
        real get_value(const vector3& p) const;
        bool is_inside(const vector3& p) const;
        bound get_bound() const;
        void get_bounds(std::vector<bound>& bounds) const;
    protected:
        vector3 p0_;
        vector3 p1_;
        real r_;
        matrix4 m_;
        matrix4 im_;
    };

    /*
    class plane_mc_volume_evaluator :public leaf_mc_volume_evaluator
    {
    public:
        plane_mc_volume_evaluator();
        ~plane_mc_volume_evaluator();
    public:
        real get_value(const vector3& p)const;
        vector3 get_gradient(const vector3& p)const;
        bool is_inside(const vector3& p)const;
        bound  get_bound()const;
    protected:
        real f_[4];
        ;
    };
    */

    //
    class impl_segment_mc_volume_evaluator : public leaf_mc_volume_evaluator
    {
    public:
        impl_segment_mc_volume_evaluator(const vector3& p0, real r0, const vector3& p1, real r1);
        ~impl_segment_mc_volume_evaluator();
        real get_value(const vector3& p) const;
        bool is_inside(const vector3& p) const;
        bound get_bound() const;
        void get_bounds(std::vector<bound>& bounds) const;
        void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;

    public:
        void set_attribute(const attribute& attr, const std::vector<real>& value);

    protected:
        vector3 p0_;
        real r0_;
        vector3 p1_;
        real r1_;
        std::map<mc_attribute_key, std::vector<real> > attrs_;
    };

    class impl_cube_mc_volume_evaluator : public leaf_mc_volume_evaluator
    {
    public:
        impl_cube_mc_volume_evaluator();
        ~impl_cube_mc_volume_evaluator();
        real get_value(const vector3& p) const;
        bool is_inside(const vector3& p) const;
        bound get_bound() const;
        void get_bounds(std::vector<bound>& bounds) const;
    };

    class bounded_mc_volume_evaluator : public mc_volume_evaluator
    {
    public:
        bounded_mc_volume_evaluator(const std::shared_ptr<mc_volume_evaluator> inter, const bound& b);

    public:
        virtual bool is_inside(const vector3& p) const;
        virtual real get_value(const vector3& p) const;
        virtual bound get_bound() const;
        virtual void get_bounds(std::vector<bound>& bounds) const;
        virtual bool test_bound(const bound& b) const;
        virtual void get_attribute(const vector3& p, const attribute& attr, std::vector<real>& value) const;

    protected:
        std::shared_ptr<mc_volume_evaluator> inter_;
        bound b_;
    };
}


#endif
