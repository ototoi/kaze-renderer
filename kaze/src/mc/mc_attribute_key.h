#ifndef MC_MC_ATTRIBUTE_KEY_H
#define MC_MC_ATTRIBUTE_KEY_H

#include "mc_types.h"
#include <string>
#include <vector>

namespace mc
{
    struct mc_attribute_key
    {
        mc_attribute_key(const std::string& n, int c)
            :name(n), count(c)
        {}
        std::string name;
        int count;
    };
    inline bool operator<(const mc_attribute_key& a, const mc_attribute_key& b) { return a.name < b.name; }
}

#endif