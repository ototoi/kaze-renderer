#ifndef MC_MC_BLOB_NODE_H
#define MC_MC_BLOB_NODE_H

#include <string>
#include <vector>

namespace mc
{
    enum MC_BLOB_NODE_TYPE
    {
        MC_BLOB_NODE_UNKNOWN = -1,
        MC_BLOB_NODE_ADD,
        MC_BLOB_NODE_SUB,
        MC_BLOB_NODE_MUL,
        MC_BLOB_NODE_DIV,
        MC_BLOB_NODE_SPHERE,
    };

    class mc_blob_node
    {
    public:
        mc_blob_node() {}
        virtual ~mc_blob_node() {}
        virtual std::string type() const { return "Unknown"; }
        virtual int typeN() const { return MC_BLOB_NODE_UNKNOWN; }
        virtual bool is_leaf() const { return false; }
        virtual bool is_branch() const { return !is_leaf(); }
    public:
        std::vector<std::shared_ptr<mc_blob_node> >& get_children() { return cld_; }
        const std::vector<std::shared_ptr<mc_blob_node> >& get_children() const { return cld_; }
    protected:
        std::vector<std::shared_ptr<mc_blob_node> > cld_;
    };
}
