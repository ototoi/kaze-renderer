#ifndef MC_MC_MESH_H
#define MC_MC_MESH_H

#include "mc_types.h"
#include "mc_attribute_key.h"
#include <vector>
#include <map>

namespace mc
{
    struct mc_mesh
    {
    public:
        typedef mc_attribute_key attr_t;
        typedef std::map<attr_t, std::vector<real> > attr_map_t;
    public:
        static const mc_attribute_key& position_attr_key()
        { 
            static const mc_attribute_key key("position",3);
            return key;
        }
        static const mc_attribute_key& normal_attr_key()
        {
            static const mc_attribute_key key("normal",3);
            return key;
        }
    public:
        //------------
        void set_indices(const std::vector<size_t>& indices)
        {
            this->indices = indices;
        }
        std::vector<size_t>& get_indices() 
        {
            return indices; 
        }
        const std::vector<size_t>& get_indices() const
        {
            return indices; 
        }
        //------------
        void set_vertices(const std::vector<vector3>& value)
        {
            size_t sz = value.size();
            std::vector<real> fv(3*sz);
            for(size_t i = 0; i < sz; i++)
            {
                fv[3*i+0] = value[i][0];
                fv[3*i+1] = value[i][1];
                fv[3*i+2] = value[i][2];
            }
            set_attribute(position_attr_key(), fv);
        }
        std::vector<real>& get_vertices()
        {
            return get_attribute(position_attr_key()); 
        }
        const std::vector<real>& get_vertices() const
        {
            return get_attribute(position_attr_key());
        }
        //------------
        void set_normals(const std::vector<vector3>& value)
        {
            size_t sz = value.size();
            std::vector<real> fv(3*sz);
            for(size_t i = 0; i < sz; i++)
            {
                fv[3*i+0] = value[i][0];
                fv[3*i+1] = value[i][1];
                fv[3*i+2] = value[i][2];
            }
            set_attribute(normal_attr_key(), fv);
        }
        std::vector<real>& get_normals() 
        {
            return get_attribute(normal_attr_key());
        }
        const std::vector<real>& get_normals() const 
        {
            return get_attribute(normal_attr_key());
        }
        //------------
        void set_attribute(const attr_t& attr, const std::vector<real>& value)
        {
            attributes[attr] = value;
        }
        std::vector<real>& get_attribute(const attr_t& attr)
        {
            static std::vector<real> no_key;
            attr_map_t::iterator it = attributes.find(attr);
            if (it != attributes.end())
            {
                return it->second;
            }
            else
            {
                return no_key;
            }
        }
        const std::vector<real>& get_attribute(const attr_t& attr) const
        {
            static std::vector<real> no_key;
            attr_map_t::const_iterator it = attributes.find(attr);
            if (it != attributes.end())
            {
                return it->second;
            }
            else
            {
                return no_key;
            }
        }
    protected:
        std::vector<size_t> indices;
        std::map<attr_t, std::vector<real> > attributes;
    };

    typedef mc_mesh mesh;
}

#endif