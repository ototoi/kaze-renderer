#include "mc_node_to_evaluator.h"
#include <cassert>

namespace mc
{
    typedef mc_bound bound;

    template <class T, class X>
    static inline const T DCAST(X x)
    {
        return static_cast<T>(x);
    }

    /*
    static double root3(double v)
    {
        return ((v < 0) ? -1 : 1) * pow(fabs(v), 1.0 / 3.0);
    }

    static double GetCubicRoot(double coeff[4])
    {
        double A = coeff[1] / coeff[0];
        double B = coeff[2] / coeff[0];
        double C = coeff[3] / coeff[0];
        double Q = (3 * B - A * A) / 9;
        double R = (9 * A * B - 27 * C - 2 * A * A * A) / 54;
        double D = Q * Q * Q + R * R; // polynomial discriminant

        assert(D > 0);

        double sqrtD = sqrt(D);
        double S = root3(R + sqrtD);
        double T = root3(R - sqrtD);
        double x = (-A / 3 + (S + T)); // one real root
        return x;
    }
    */

    static double GetCubicRoot_(double t)
    {
        return 1 - pow(t, 1.0 / 3.0); // one real root
    }

    static double GetThresholdRoot(double t)
    {
        //1 - 3*r2 + 3*r2*r2 - r2*r2*r2
        //f(x) = -1*x^3 + 3*x^2 -3*x +1
        //f(x) = -1*_ + 3*_ -3*_ + 1-t
        //double coeff[4] = {-1, 3, -3, 1-t};
        t = std::min<real>(std::max<real>(0, t), 1);
        return sqrt(GetCubicRoot_(t));
    }

    static bool GetThreshold(real& threshold, const std::shared_ptr<mc_volume_node>& node)
    {
        int type = node->typeN();
        if (type == MC_VOLUME_OFFSET)
        {
            const offset_mc_volume_node* s = DCAST<const offset_mc_volume_node*>(node.get());
            threshold = s->get_threshold();
            return true;
        }
        else if (node->is_branch())
        {
            const std::vector<std::shared_ptr<mc_volume_node> >& c = node->get_children();
            for (size_t i = 0; i < c.size(); i++)
            {
                if (GetThreshold(threshold, c[i])) return true;
            }
        }
        return true;
    }

    static bool IntersectBound(const bound& a, const bound& b)
    {
        for (int i = 0; i < 3; i++)
        {
            if (a.max[i] < b.min[i] || b.max[i] < a.min[i]) return false;
        }
        return true;
    }

    static bool IntersectBound(const vector3& a, const vector3& b, const vector3& c, const vector3& d)
    {
        bound aa, bb;
        aa.min = a;
        aa.max = b;
        bb.min = c;
        bb.max = d;

        return IntersectBound(aa, bb);
    }

    static bool IsInside(const bound& a, const bound& b)
    {
        for (int i = 0; i < 3; i++)
        {
            if (!(b.min[i] <= a.min[i] && a.max[i] <= b.max[i])) return false;
        }
        return true;
    }

    static bool IsInside(const vector3& a, const vector3& b, const vector3& c, const vector3& d)
    {
        bound aa, bb;
        aa.min = a;
        aa.max = b;
        bb.min = c;
        bb.max = d;

        return IsInside(aa, bb);
    }

    static bound TransformBound(const bound& b, const matrix4& m)
    {
        vector3 min = b.min;
        vector3 max = b.max;
        vector3 points[] =
            {
                vector3(min[0], min[1], min[2]),
                vector3(min[0], min[1], max[2]),
                vector3(min[0], max[1], min[2]),
                vector3(min[0], max[1], max[2]),
                vector3(max[0], min[1], min[2]),
                vector3(max[0], min[1], max[2]),
                vector3(max[0], max[1], min[2]),
                vector3(max[0], max[1], max[2])};

        for (int i = 0; i < 8; i++)
        {
            points[i] = m * points[i];
        }
        vector3 cmin, cmax;
        cmin = cmax = points[0];
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                cmin[j] = std::min<real>(cmin[j], points[i][j]);
                cmax[j] = std::max<real>(cmax[j], points[i][j]);
            }
        }
        bound r;
        r.min = cmin;
        r.max = cmax;
        return r;
    }

    static vector3 GetNarestSidePoint(const vector3& min, const vector3& max, const vector3& p)
    {
        vector3 q = p;
        bool bInside = true;
        for (int i = 0; i < 3; i++)
        {
            real v = p[i];
            if (!(min[i] <= v && v <= max[i])) bInside = false;
        }
        if (bInside)
        {
            vector3 points[] = {
                vector3(min[0], p[1], p[2]),
                vector3(max[0], p[1], p[2]),
                vector3(p[0], min[1], p[2]),
                vector3(p[0], max[1], p[2]),
                vector3(p[0], p[1], min[2]),
                vector3(p[0], p[1], max[2])};

            int ii = 0;
            real lmin = length(points[0] - p);
            for (int i = 1; i < 6; i++)
            {
                real l = length(points[i] - p);
                if (l < lmin)
                {
                    l = lmin;
                    ii = i;
                }
            }
            return points[ii];
        }
        else
        {
            for (int i = 0; i < 3; i++)
            {
                real v = p[i];
                {
                    v = std::max<real>(v, min[i]);
                    v = std::min<real>(v, max[i]);
                }
                q[i] = v;
            }
        }

        return q;
    }

    static vector3 GetNarestPoint(const vector3& min, const vector3& max, const vector3& p)
    {
        vector3 q = p;
        for (int i = 0; i < 3; i++)
        {
            real v = p[i];
            {
                v = std::max<real>(v, min[i]);
                v = std::min<real>(v, max[i]);
            }
            q[i] = v;
        }
        return q;
    }

    static bool IsIntesectRange(const vector3& min, const vector3& max, const vector3& p, real r)
    {
        if (IsInside(p - vector3(r, r, r), p + vector3(r, r, r), min, max)) return true;

        vector3 points[] = {
            vector3(min[0], min[1], min[2]),
            vector3(min[0], min[1], max[2]),
            vector3(min[0], max[1], min[2]),
            vector3(min[0], max[1], max[2]),
            vector3(max[0], min[1], min[2]),
            vector3(max[0], min[1], max[2]),
            vector3(max[0], max[1], min[2]),
            vector3(max[0], max[1], max[2])
        };
        real lmin = length(GetNarestSidePoint(min, max, p) - p);
        real lmax = lmin;
        for (int i = 0; i < 8; i++)
        {
            real l = length(points[i] - p);
            lmin = std::min<real>(lmin, l);
            lmax = std::max<real>(lmax, l);
        }
        if (lmin * 0.9 <= r && r <= lmax + 1.1) return true;
        return false;
    }

    static bool IsIntesect(const vector3& min, const vector3& max, const vector3& p, real r)
    {
        real l = length(GetNarestPoint(min, max, p) - p);
        if (l <= r) return true;
        return false;
    }

    static const real FAR = std::numeric_limits<real>::max();

    static void GetMinMax(vector3& min, vector3& max, const std::shared_ptr<mc_volume_node>& node)
    {
        int type = node->typeN();
        if (type == MC_VOLUME_BVH)
        {
            const bvh_mc_volume_node* s = DCAST<const bvh_mc_volume_node*>(node.get());
            min = s->get_min();
            max = s->get_max();
            return;
        }

        if (node->is_branch())
        {
            vector3 cmin = +vector3(FAR, FAR, FAR);
            vector3 cmax = -vector3(FAR, FAR, FAR);
            const std::vector<std::shared_ptr<mc_volume_node> >& c = node->get_children();
            size_t sz = c.size();
            for (size_t i = 0; i < sz; i++)
            {
                vector3 smin, smax;
                GetMinMax(smin, smax, c[i]);
                for (int j = 0; j < 3; j++)
                {
                    cmin[j] = std::min<real>(cmin[j], smin[j]);
                    cmax[j] = std::max<real>(cmax[j], smax[j]);
                }
            }
            min = cmin;
            max = cmax;
        }
        else
        {
            switch (type)
            {
            case MC_VOLUME_SPHERE:
            {
                const sphere_mc_volume_node* s = DCAST<const sphere_mc_volume_node*>(node.get());
                vector3 p = s->get_position();
                real r = s->get_radius();
                min = p - vector3(r, r, r);
                max = p + vector3(r, r, r);
            }
            break;
            case MC_VOLUME_ELLIPSOID:
            {
                const ellipsoid_mc_volume_node* s = DCAST<const ellipsoid_mc_volume_node*>(node.get());
                matrix4 l2w = s->get_matrix();
                //matrix4 w2l = ~l2w;
                bound lb = {vector3(-1, -1, -1), vector3(+1, +1, +1)};
                bound wb = TransformBound(lb, l2w);
                min = wb.min;
                max = wb.max;
            }
            break;
            case MC_VOLUME_SEGMENT:
            {
                const segment_mc_volume_node* s = DCAST<const segment_mc_volume_node*>(node.get());
                vector3 p0 = s->get_position0();
                vector3 p1 = s->get_position1();
                real r = s->get_radius();
                matrix4 l2w = s->get_matrix();
                bound lb0 = {p0 - vector3(r, r, r), p0 + vector3(r, r, r)};
                bound wb0 = TransformBound(lb0, l2w);
                bound lb1 = {p1 - vector3(r, r, r), p1 + vector3(r, r, r)};
                bound wb1 = TransformBound(lb1, l2w);
                min = wb0.min;
                max = wb0.max;
                for (int i = 0; i < 3; i++)
                {
                    min[i] = std::min<real>(min[i], wb1.min[i]);
                    max[i] = std::max<real>(max[i], wb1.max[i]);
                }
            }
            break;
            case MC_VOLUME_IMPL_SEGMENT:
            {
                const impl_segment_mc_volume_node* s = DCAST<const impl_segment_mc_volume_node*>(node.get());
                vector3 p0 = s->get_position0();
                vector3 p1 = s->get_position1();
                real r0 = s->get_radius0();
                real r1 = s->get_radius1();
                real r = std::max<real>(r0, r1);
                bound wb0 = {p0 - vector3(r, r, r), p0 + vector3(r, r, r)};
                bound wb1 = {p1 - vector3(r, r, r), p1 + vector3(r, r, r)};
                min = wb0.min;
                max = wb0.max;
                for (int i = 0; i < 3; i++)
                {
                    min[i] = std::min<real>(min[i], wb1.min[i]);
                    max[i] = std::max<real>(max[i], wb1.max[i]);
                }
            }
            break;
            case MC_VOLUME_IMPL_CUBE:
            {
                const impl_cube_mc_volume_node* s = DCAST<const impl_cube_mc_volume_node*>(node.get());
                min = s->get_min();
                max = s->get_max();
            }
            break;
            case MC_VOLUME_BOUND:
            {
                const bound_mc_volume_node* s = DCAST<const bound_mc_volume_node*>(node.get());
                min = s->get_min();
                max = s->get_max();
            }
            break;
            }
        }
    }

    struct bound_node
    {
        ~bound_node()
        {
            if (nodes[0]) delete nodes[0];
            if (nodes[1]) delete nodes[1];
        }
        vector3 min;
        vector3 max;
        bound_node* nodes[2];
        const std::shared_ptr<mc_volume_node>* p_node;
    };

    struct BVHSorter
    {
        BVHSorter(int plane) : plane_(plane) {}
        bool operator()(const bound_node* a, const bound_node* b) const
        {
            int p = plane_;
            return (a->min[p] + a->max[p]) < (b->min[p] + b->max[p]);
        }
        int plane_;
    };

    static bound_node* CreateBVH(bound_node** nodes, int sz)
    {
        if (sz == 0) return 0;
        if (sz == 1)
        {
            return nodes[0];
        }
        else
        {
            vector3 cmin = +vector3(FAR, FAR, FAR);
            vector3 cmax = -vector3(FAR, FAR, FAR);
            for (size_t i = 0; i < sz; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    cmin[j] = std::min<real>(cmin[j], nodes[i]->min[j]);
                    cmax[j] = std::max<real>(cmax[j], nodes[i]->max[j]);
                }
            }
            vector3 wid = cmax - cmin;
            int plane = 0;
            if (wid[1] > wid[plane]) plane = 1;
            if (wid[2] > wid[plane]) plane = 2;
            std::sort(nodes, nodes + sz, BVHSorter(plane));
            int lsz = sz >> 1;
            int rsz = sz - lsz;
            bound_node* lnode = CreateBVH(nodes, lsz);
            bound_node* rnode = CreateBVH(nodes + lsz, rsz);
            bound_node* cnode = new bound_node();
            cnode->nodes[0] = lnode;
            cnode->nodes[1] = rnode;
            cnode->min = cmin;
            cnode->max = cmax;
            cnode->p_node = 0;

            return cnode;
        }
        return 0;
    }

    static std::shared_ptr<mc_volume_node> CreateBVH(const bound_node* n, int type)
    {
        if (!n) return std::shared_ptr<mc_volume_node>();
        if (!n->p_node)
        {
            std::vector<std::shared_ptr<mc_volume_node> > nodes(2);
            nodes[0] = CreateBVH(n->nodes[0], type);
            nodes[1] = CreateBVH(n->nodes[1], type);
            return std::shared_ptr<mc_volume_node>(new bvh_mc_volume_node(nodes, n->min, n->max, type));
        }
        else
        {
            return *n->p_node;
        }
    }

    static std::shared_ptr<mc_volume_node> CreateBVH(const std::vector<std::shared_ptr<mc_volume_node> >& c, const std::vector<bound>& b, int type)
    {
        size_t sz = c.size();
        std::vector<bound_node*> leafs(sz);
        for (size_t i = 0; i < sz; i++)
        {
            leafs[i] = new bound_node();
            leafs[i]->min = b[i].min;
            leafs[i]->max = b[i].max;
            leafs[i]->p_node = &c[i];
            leafs[i]->nodes[0] = 0;
            leafs[i]->nodes[1] = 0;
        }
        std::unique_ptr<bound_node> root(CreateBVH(&leafs[0], sz));
        return CreateBVH(root.get(), type);
    }

    static std::shared_ptr<mc_volume_node> AddBVH(std::shared_ptr<mc_volume_node>& node, vector3& min, vector3& max)
    {
        int type = node->typeN();
        if (node->is_branch())
        {
            std::vector<std::shared_ptr<mc_volume_node> >& c = node->get_children();
            size_t sz = c.size();
            std::vector<bound> b(sz);
            for (size_t i = 0; i < sz; i++)
            {
                c[i] = AddBVH(c[i], b[i].min, b[i].max);
            }

            if (sz >= 2 && (type == MC_VOLUME_ADD || type == MC_VOLUME_MAX))
            {
                std::shared_ptr<mc_volume_node> n = CreateBVH(c, b, type);
                GetMinMax(min, max, n);
                return n;
            }
            else
            {
                vector3 cmin = +vector3(FAR, FAR, FAR);
                vector3 cmax = -vector3(FAR, FAR, FAR);
                for (size_t i = 0; i < sz; i++)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        cmin[j] = std::min<real>(cmin[j], b[i].min[j]);
                        cmax[j] = std::max<real>(cmax[j], b[i].max[j]);
                    }
                }
                min = cmin;
                max = cmax;

                return node;
            }
        }
        else
        {
            GetMinMax(min, max, node);
            return node;
        }
    }

    //--------------------------------------------------------------------------------------------------------

    static std::shared_ptr<mc_volume_node> PullNode(const std::shared_ptr<mc_volume_node>& node, const vector3& min, const vector3& max)
    {
        int type = node->typeN();
        if (node->is_branch())
        {
            if (type == MC_VOLUME_BVH)
            {
                const bvh_mc_volume_node* s = DCAST<const bvh_mc_volume_node*>(node.get());
                vector3 smin = s->get_min();
                vector3 smax = s->get_max();

                if (IntersectBound(smin, smax, min, max))
                {
                    int i_type = s->internalTypeN();

                    const std::shared_ptr<mc_volume_node>& ln = s->get_left();
                    const std::shared_ptr<mc_volume_node>& rn = s->get_right();

                    std::vector<std::shared_ptr<mc_volume_node> > nodes;
                    if (ln.get())
                    {
                        std::shared_ptr<mc_volume_node> n = PullNode(ln, min, max);
                        if (n.get())
                        {
                            if (n->typeN() == i_type)
                            {
                                const std::vector<std::shared_ptr<mc_volume_node> >& cld = n->get_children();
                                for (size_t i = 0; i < cld.size(); i++)
                                {
                                    nodes.push_back(cld[i]);
                                }
                            }
                            else
                            {
                                nodes.push_back(n);
                            }
                        }
                    }

                    if (rn.get())
                    {
                        std::shared_ptr<mc_volume_node> n = PullNode(rn, min, max);
                        if (n.get())
                        {
                            if (n->typeN() == i_type)
                            {
                                const std::vector<std::shared_ptr<mc_volume_node> >& cld = n->get_children();
                                for (size_t i = 0; i < cld.size(); i++)
                                {
                                    nodes.push_back(cld[i]);
                                }
                            }
                            else
                            {
                                nodes.push_back(n);
                            }
                        }
                    }

                    if (nodes.empty()) return std::shared_ptr<mc_volume_node>();

                    if (i_type == MC_VOLUME_ADD)
                    {
                        return std::shared_ptr<mc_volume_node>(new add_mc_volume_node(nodes));
                    }
                    else if (i_type == MC_VOLUME_MAX)
                    {
                        return std::shared_ptr<mc_volume_node>(new max_mc_volume_node(nodes));
                    }
                }

                return std::shared_ptr<mc_volume_node>();
            }
            else
            {
                vector3 cmin = min;
                vector3 cmax = max;
                if (type == MC_VOLUME_TRANSFORM)
                {
                    const transform_mc_volume_node* s = DCAST<const transform_mc_volume_node*>(node.get());
                    //matrix4 l2w = s->get_matrix();
                    matrix4 w2l = s->get_inverse_matrix();
                    bound wb = {min, max};
                    bound lb = TransformBound(wb, w2l);
                    cmin = lb.min;
                    cmax = lb.max;
                }

                std::vector<std::shared_ptr<mc_volume_node> >& c = node->get_children();
                if (type == MC_VOLUME_SUB || type == MC_VOLUME_DIV)
                {
                    std::vector<std::shared_ptr<mc_volume_node> > cld;
                    if (c.size() == 2)
                    {
                        std::shared_ptr<mc_volume_node> cc0 = PullNode(c[0], cmin, cmax);
                        std::shared_ptr<mc_volume_node> cc1 = PullNode(c[1], cmin, cmax);
                        if (!cc0.get())
                        {
                            return std::shared_ptr<mc_volume_node>();
                        }
                        else if (!cc1.get())
                        {
                            return cc0;
                        }
                        if (cc0.get()) cld.push_back(cc0);
                        if (cc1.get()) cld.push_back(cc1);
                        std::shared_ptr<mc_volume_node> n = node->clone_nochildren();
                        n->get_children() = cld;
                        return n;
                    }
                    return std::shared_ptr<mc_volume_node>();
                }
                else
                {
                    std::vector<std::shared_ptr<mc_volume_node> > cld;
                    for (size_t i = 0; i < c.size(); i++)
                    {
                        std::shared_ptr<mc_volume_node> cc = PullNode(c[i], cmin, cmax);
                        if (cc.get())
                        {
                            cld.push_back(cc);
                        }
                    }
                    if (cld.empty())
                    {
                        return std::shared_ptr<mc_volume_node>();
                    }
                    else
                    {
                        std::shared_ptr<mc_volume_node> n = node->clone_nochildren();
                        n->get_children() = cld;
                        return n;
                    }
                }
            }
        }
        else
        {
            if (type == MC_VOLUME_SPHERE)
            {
                const sphere_mc_volume_node* s = DCAST<const sphere_mc_volume_node*>(node.get());
                vector3 p = s->get_position();
                real r = s->get_radius();
                vector3 smin = p - vector3(r, r, r);
                vector3 smax = p + vector3(r, r, r);
                if (IntersectBound(smin, smax, min, max))
                {
                    if (IsIntesect(min, max, p, r))
                    {
                        vector3 cmin = min;
                        vector3 cmax = max;
                        for (int ii = 0; ii < 3; ii++)
                        {
                            cmin[ii] = std::max<real>(cmin[ii], smin[ii]);
                            cmax[ii] = std::min<real>(cmax[ii], smax[ii]);
                        }
                        return std::shared_ptr<mc_volume_node>(new bound_mc_volume_node(cmin, cmax, node->clone()));
                    }
                }
                return std::shared_ptr<mc_volume_node>();
            }
            else if (type == MC_VOLUME_ELLIPSOID)
            {
                const ellipsoid_mc_volume_node* s = DCAST<const ellipsoid_mc_volume_node*>(node.get());
                matrix4 l2w = s->get_matrix();
                matrix4 w2l = s->get_inverse_matrix();
                bound lb = {vector3(-1, -1, -1), vector3(+1, +1, +1)};
                bound wb = TransformBound(lb, l2w);
                vector3 smin = wb.min;
                vector3 smax = wb.max;
                if (IntersectBound(smin, smax, min, max))
                {
                    bound wb = {min, max};
                    bound lb = TransformBound(wb, w2l);
                    if (IsIntesect(lb.min, lb.max, vector3(0, 0, 0), 1))
                    {
                        vector3 cmin = min;
                        vector3 cmax = max;
                        for (int ii = 0; ii < 3; ii++)
                        {
                            cmin[ii] = std::max<real>(cmin[ii], smin[ii]);
                            cmax[ii] = std::min<real>(cmax[ii], smax[ii]);
                        }
                        return std::shared_ptr<mc_volume_node>(new bound_mc_volume_node(cmin, cmax, node->clone()));
                    }
                }
                return std::shared_ptr<mc_volume_node>();
            }
            else
            {
                //const segment_mc_volume_node* s = DCAST<const segment_mc_volume_node*>(node.get());
                vector3 smin;
                vector3 smax;
                GetMinMax(smin, smax, node);
                if (IntersectBound(smin, smax, min, max))
                {
                    vector3 cmin = min;
                    vector3 cmax = max;
                    for (int ii = 0; ii < 3; ii++)
                    {
                        cmin[ii] = std::max<real>(cmin[ii], smin[ii]);
                        cmax[ii] = std::min<real>(cmax[ii], smax[ii]);
                    }
                    return std::shared_ptr<mc_volume_node>(new bound_mc_volume_node(cmin, cmax, node->clone()));
                }
                return std::shared_ptr<mc_volume_node>();
            }
            return node->clone();
        }
    }

    static bool IsEliminatable(int type, int sz)
    {
        if (type == MC_VOLUME_ADD)
        {
            if (sz == 1) return true;
            return false;
        }
        if (type == MC_VOLUME_MAX)
        {
            return true;
        }
        if (type == MC_VOLUME_MIN)
        {
            return true;
        }
        if (type == MC_VOLUME_MUL)
        {
            return true;
        }
        if (type == MC_VOLUME_SUB)
        {
            return false;
        }
        if (type == MC_VOLUME_DIV)
        {
            return false;
        }
        if (type == MC_VOLUME_NEG)
        {
            return false;
        }
        if (type == MC_VOLUME_INV)
        {
            return false;
        }
        return true;
    }

    /*
    static void ExpandMinmax(vector3& min, vector3& max, real a)
    {
        vector3 c = (min + max) * 0.5;
        vector3 w = (max - min) * 0.5;
        w *= a;
        min = c - w;
        max = c + w;
    }
    */

    static std::shared_ptr<mc_volume_node> EliminateNode_(std::shared_ptr<mc_volume_node>& node, real radius)
    {
        int type = node->typeN();
        if (node->is_branch())
        {
            /*
            matrix4 m = mat;
            if(type == MC_VOLUME_TRANSFORM){
                const transform_mc_volume_node* s = DCAST<const transform_mc_volume_node*>(node.get());
                m = s->get_matrix() * m;
            }
            */

            std::vector<std::shared_ptr<mc_volume_node> >& c = node->get_children();
            std::vector<std::shared_ptr<mc_volume_node> > cld;
            int sz = c.size();
            if (IsEliminatable(type, sz))
            {
                for (size_t i = 0; i < sz; i++)
                {
                    std::shared_ptr<mc_volume_node> cc = EliminateNode_(c[i], radius);
                    if (cc.get())
                    {
                        cld.push_back(cc);
                    }
                }

                if (cld.empty())
                {
                    return std::shared_ptr<mc_volume_node>();
                }
                else
                {
                    std::shared_ptr<mc_volume_node> n = node->clone_nochildren();
                    n->get_children() = cld;
                    return n;
                }
            }
            else
            {
                return node;
            }
        }
        else
        {
            if (type == MC_VOLUME_BOUND)
            {
                const bound_mc_volume_node* n = DCAST<const bound_mc_volume_node*>(node.get());
                int type_i = n->get_node()->typeN();
                if (type_i == MC_VOLUME_SPHERE)
                {
                    const sphere_mc_volume_node* s = DCAST<const sphere_mc_volume_node*>(n->get_node().get());
                    vector3 p = s->get_position();
                    real r = s->get_radius();
                    vector3 min = n->get_min();
                    vector3 max = n->get_max();
                    //ExpandMinmax(min, max, 1.5);
                    if (IsIntesectRange(min, max, p, r * radius))
                    {
                        return node;
                    }
                    else
                    {
                        return std::shared_ptr<mc_volume_node>();
                    }
                }
                else if (type_i == MC_VOLUME_ELLIPSOID)
                {
                    const ellipsoid_mc_volume_node* s = DCAST<const ellipsoid_mc_volume_node*>(n->get_node().get());
                    //matrix4 l2w = s->get_matrix();
                    matrix4 w2l = s->get_inverse_matrix();
                    vector3 min = n->get_min();
                    vector3 max = n->get_max();
                    bound wb = {min, max};
                    bound lb = TransformBound(wb, w2l);
                    min = lb.min;
                    max = lb.max;
                    //ExpandMinmax(min, max, 1.5);
                    if (IsIntesectRange(min, max, vector3(0, 0, 0), radius))
                    {
                        return node;
                    }
                    else
                    {
                        return std::shared_ptr<mc_volume_node>();
                    }
                }
            }

            return node;
        }
    }

    static std::shared_ptr<mc_volume_node> EliminateNode(std::shared_ptr<mc_volume_node>& node)
    {
        real th = 0.5;
        if (!GetThreshold(th, node)) return node;
        real radius = GetThresholdRoot(th);
        return EliminateNode_(node, radius);
    }

    static std::shared_ptr<mc_volume_node> PromoteNode(std::shared_ptr<mc_volume_node>& node)
    {
        int type = node->typeN();
        if (node->is_branch())
        {
            std::vector<std::shared_ptr<mc_volume_node> >& c = node->get_children();
            size_t sz = c.size();
            if (sz != 1)
            {
                std::vector<std::shared_ptr<mc_volume_node> > cld(sz);
                for (size_t i = 0; i < sz; i++)
                {
                    cld[i] = PromoteNode(c[i]);
                }
                node->get_children() = cld;
                return node;
            }
            else
            {
                if (type == MC_VOLUME_ADD || type == MC_VOLUME_MAX || type == MC_VOLUME_MIN)
                {
                    return PromoteNode(c[0]);
                }
                return node;
            }
        }
        else
        {
            return node;
        }
    }

    //------------------------------

    static std::shared_ptr<mc_volume_evaluator> ConvertNode(const std::shared_ptr<mc_volume_node>& node);

    static std::vector<std::shared_ptr<mc_volume_evaluator> > ConvertNodes(const std::vector<std::shared_ptr<mc_volume_node> >& c)
    {
        std::vector<std::shared_ptr<mc_volume_evaluator> > cld;
        for (size_t i = 0; i < c.size(); i++)
        {
            std::shared_ptr<mc_volume_evaluator> k = ConvertNode(c[i]);
            if (k.get())
            {
                cld.push_back(k);
            }
        }
        return cld;
    }

    static void SetAttributes(std::shared_ptr<mc_volume_evaluator>& e, const std::shared_ptr<mc_volume_node>& node)
    {
        leaf_mc_volume_evaluator* l = DCAST<leaf_mc_volume_evaluator*>(e.get());
        typedef mc_volume_node::attr_map_type attr_map_type;
        const attr_map_type& attrs = node->get_attributes();
        if (!attrs.empty())
        {
            for (attr_map_type::const_iterator it = attrs.begin(); it != attrs.end(); ++it)
            {
                l->set_attribute(it->first, it->second);
            }
        }
    }

    static std::shared_ptr<mc_volume_evaluator> ConvertNode(const std::shared_ptr<mc_volume_node>& node)
    {
        int type = node->typeN();
        switch (type)
        {
        case MC_VOLUME_OFFSET:
        {
            const offset_mc_volume_node* s = DCAST<const offset_mc_volume_node*>(node.get());
            real threshold = s->get_threshold();
            std::vector<std::shared_ptr<mc_volume_evaluator> > cld = ConvertNodes(node->get_children());
            if (!cld.empty())
            {
                return std::shared_ptr<mc_volume_evaluator>(new offset_mc_volume_evaluator(cld[0], threshold));
            }
        }
        break;
        case MC_VOLUME_TRANSFORM:
        {
            const transform_mc_volume_node* s = DCAST<const transform_mc_volume_node*>(node.get());
            matrix4 mat = s->get_matrix();
            std::vector<std::shared_ptr<mc_volume_evaluator> > cld = ConvertNodes(node->get_children());
            if (!cld.empty())
            {
                return std::shared_ptr<mc_volume_evaluator>(new transformed_mc_volume_evaluator(cld[0], mat));
            }
        }
        break;
        case MC_VOLUME_ADD:
        {
            std::vector<std::shared_ptr<mc_volume_evaluator> > cld = ConvertNodes(node->get_children());
            if (!cld.empty())
            {
                return std::shared_ptr<mc_volume_evaluator>(new add_mc_volume_evaluator(cld));
            }
        }
        break;
        case MC_VOLUME_MUL:
        {
            std::vector<std::shared_ptr<mc_volume_evaluator> > cld = ConvertNodes(node->get_children());
            if (!cld.empty())
            {
                return std::shared_ptr<mc_volume_evaluator>(new mul_mc_volume_evaluator(cld));
            }
        }
        break;
        case MC_VOLUME_MIN:
        {
            std::vector<std::shared_ptr<mc_volume_evaluator> > cld = ConvertNodes(node->get_children());
            if (!cld.empty())
            {
                return std::shared_ptr<mc_volume_evaluator>(new min_mc_volume_evaluator(cld));
            }
        }
        break;
        case MC_VOLUME_MAX:
        {
            std::vector<std::shared_ptr<mc_volume_evaluator> > cld = ConvertNodes(node->get_children());
            if (!cld.empty())
            {
                return std::shared_ptr<mc_volume_evaluator>(new max_mc_volume_evaluator(cld));
            }
        }
        break;
        case MC_VOLUME_SUB:
        {
            std::vector<std::shared_ptr<mc_volume_evaluator> > cld = ConvertNodes(node->get_children());
            if (!cld.empty())
            {
                return std::shared_ptr<mc_volume_evaluator>(new sub_mc_volume_evaluator(cld));
            }
        }
        break;
        case MC_VOLUME_DIV:
        {
            std::vector<std::shared_ptr<mc_volume_evaluator> > cld = ConvertNodes(node->get_children());
            if (!cld.empty())
            {
                return std::shared_ptr<mc_volume_evaluator>(new div_mc_volume_evaluator(cld));
            }
        }
        break;
        case MC_VOLUME_NEG:
        {
            std::vector<std::shared_ptr<mc_volume_evaluator> > cld = ConvertNodes(node->get_children());
            if (!cld.empty())
            {
                return std::shared_ptr<mc_volume_evaluator>(new neg_mc_volume_evaluator(cld));
            }
        }
        break;
        case MC_VOLUME_INV:
        {
            std::vector<std::shared_ptr<mc_volume_evaluator> > cld = ConvertNodes(node->get_children());
            if (!cld.empty())
            {
                return std::shared_ptr<mc_volume_evaluator>(new inv_mc_volume_evaluator(cld));
            }
        }
        break;
        case MC_VOLUME_FMA:
        {
            const fma_mc_volume_node* s = DCAST<const fma_mc_volume_node*>(node.get());
            real a = s->get_a();
            real c = s->get_c();
            real min = s->get_min();
            real max = s->get_max();
            std::vector<std::shared_ptr<mc_volume_evaluator> > cld = ConvertNodes(node->get_children());
            if (!cld.empty())
            {
                return std::shared_ptr<mc_volume_evaluator>(new fma_mc_volume_evaluator(a, cld[0], c, min, max));
            }
        }
        break;
        case MC_VOLUME_SPHERE:
        {
            const sphere_mc_volume_node* s = DCAST<const sphere_mc_volume_node*>(node.get());
            vector3 p = s->get_position();
            real r = s->get_radius();
            std::shared_ptr<mc_volume_evaluator> e = std::shared_ptr<mc_volume_evaluator>(new sphere_mc_volume_evaluator(p, r));
            SetAttributes(e, node);
            return e;
        }
        break;
        case MC_VOLUME_ELLIPSOID:
        {
            const ellipsoid_mc_volume_node* s = DCAST<const ellipsoid_mc_volume_node*>(node.get());
            matrix4 mat = s->get_matrix();
            std::shared_ptr<mc_volume_evaluator> e = std::shared_ptr<mc_volume_evaluator>(new ellipsoid_mc_volume_evaluator(mat));
            SetAttributes(e, node);
            return e;
        }
        break;
        case MC_VOLUME_SEGMENT:
        {
            const segment_mc_volume_node* s = DCAST<const segment_mc_volume_node*>(node.get());
            vector3 p0 = s->get_position0();
            vector3 p1 = s->get_position1();
            real r = s->get_radius();
            matrix4 mat = s->get_matrix();
            std::shared_ptr<mc_volume_evaluator> e = std::shared_ptr<mc_volume_evaluator>(new segment_mc_volume_evaluator(p0, p1, r, mat));
            SetAttributes(e, node);
            return e;
        }
        break;
        case MC_VOLUME_IMPL_SEGMENT:
        {
            const impl_segment_mc_volume_node* s = DCAST<const impl_segment_mc_volume_node*>(node.get());
            vector3 p0 = s->get_position0();
            vector3 p1 = s->get_position1();
            real r0 = s->get_radius0();
            real r1 = s->get_radius1();
            std::shared_ptr<mc_volume_evaluator> e = std::shared_ptr<mc_volume_evaluator>(new impl_segment_mc_volume_evaluator(p0, r0, p1, r1));
            SetAttributes(e, node);
            return e;
        }
        break;
        case MC_VOLUME_IMPL_CUBE:
        {
            //const impl_cube_mc_volume_node* s = DCAST<const impl_cube_mc_volume_node*>(node.get());
            std::shared_ptr<mc_volume_evaluator> e = std::shared_ptr<mc_volume_evaluator>(new impl_cube_mc_volume_evaluator());
            SetAttributes(e, node);
            return e;
        }
        break;
        case MC_VOLUME_BOUND:
        {
            const bound_mc_volume_node* s = DCAST<const bound_mc_volume_node*>(node.get());
            vector3 min = s->get_min();
            vector3 max = s->get_max();
            std::shared_ptr<mc_volume_evaluator> c = ConvertNode(s->get_node());
            if (c.get())
            {
                bound b = {min, max};
                return std::shared_ptr<mc_volume_evaluator>(new bounded_mc_volume_evaluator(c, b));
            }
            return std::shared_ptr<mc_volume_evaluator>();
        }
        break;
        }

        return std::shared_ptr<mc_volume_evaluator>();
    }

    std::shared_ptr<mc_volume_node> mc_node_add_bvh(const std::shared_ptr<mc_volume_node>& node)
    {
        std::shared_ptr<mc_volume_node> n = node->clone();
        vector3 min, max;
        return AddBVH(n, min, max);
    }

    std::shared_ptr<mc_volume_node> mc_node_remove_bvh(const std::shared_ptr<mc_volume_node>& node, const vector3& min, const vector3& max)
    {
        std::shared_ptr<mc_volume_node> n = node->clone();
        return PullNode(n, min, max);
    }

    std::shared_ptr<mc_volume_evaluator> mc_node_to_evaluator(const std::shared_ptr<mc_volume_node>& node)
    {
        return ConvertNode(node);
    }

    std::shared_ptr<mc_volume_evaluator> mc_node_to_evaluator(const std::shared_ptr<mc_volume_node>& node, const vector3& min, const vector3& max)
    {
        std::shared_ptr<mc_volume_node> n = PullNode(node, min, max);
        if (!n.get()) return std::shared_ptr<mc_volume_evaluator>();
        n = PromoteNode(n);
        if (!n.get()) return std::shared_ptr<mc_volume_evaluator>();
        n = EliminateNode(n);
        if (!n.get()) return std::shared_ptr<mc_volume_evaluator>();
        n = PromoteNode(n);
        if (!n.get()) return std::shared_ptr<mc_volume_evaluator>();
        return ConvertNode(n);
    }

    void mc_node_get_bounds(std::vector<mc_bound>& bounds, const std::shared_ptr<mc_volume_node>& node)
    {
        if(node->is_branch())
        {
            const std::vector<std::shared_ptr<mc_volume_node> >& c = node->get_children();
            size_t sz = c.size();
            for (size_t i = 0; i < sz; i++)
            {
                mc_node_get_bounds(bounds, c[i]);
            }
        }
        else
        {
            
        }
    }
}
