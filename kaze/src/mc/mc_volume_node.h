#ifndef MC_MC_VOLUME_NODE_H
#define MC_MC_VOLUME_NODE_H

#include "mc_types.h"
#include "mc_attribute_key.h"
#include <string>
#include <map>
#include <vector>

namespace mc
{
    enum
    {
        MC_VOLUME_UNKNOWN = -1,
        MC_VOLUME_ADD,
        MC_VOLUME_SUB,
        MC_VOLUME_MUL,
        MC_VOLUME_DIV,
        MC_VOLUME_MIN,
        MC_VOLUME_MAX,
        MC_VOLUME_NEG,
        MC_VOLUME_INV,
        MC_VOLUME_FMA,
        MC_VOLUME_TRANSFORM,
        MC_VOLUME_OFFSET,
        MC_VOLUME_SPHERE,
        MC_VOLUME_ELLIPSOID,
        MC_VOLUME_SEGMENT,
        MC_VOLUME_IMPL_SEGMENT = 1000,
        MC_VOLUME_IMPL_CUBE,
        MC_VOLUME_BOUND = 10000,
        MC_VOLUME_BVH
    };

    class mc_volume_node
    {
    public:
        mc_volume_node() {}
        mc_volume_node(const std::vector<std::shared_ptr<mc_volume_node> >& cld) : cld_(cld) {}
        virtual ~mc_volume_node() {}
        virtual std::string type() const { return "Unknown"; }
        virtual int typeN() const { return MC_VOLUME_UNKNOWN; }
        virtual bool is_leaf() const { return cld_.empty(); }
        virtual bool is_branch() const { return !is_leaf(); }
        virtual std::shared_ptr<mc_volume_node> clone() const = 0;
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return std::shared_ptr<mc_volume_node>(); }
    public:
        std::vector<std::shared_ptr<mc_volume_node> >& get_children() { return cld_; }
        const std::vector<std::shared_ptr<mc_volume_node> >& get_children() const { return cld_; }

        virtual std::vector<std::shared_ptr<mc_volume_node> > clone_children() const
        {
            if (cld_.empty()) return std::vector<std::shared_ptr<mc_volume_node> >();
            size_t sz = cld_.size();
            std::vector<std::shared_ptr<mc_volume_node> > cld(sz);
            for (size_t i = 0; i < sz; i++)
            {
                cld[i] = cld_[i]->clone();
            }
            return cld;
        }

    public:
        typedef std::map<mc_attribute_key, std::vector<real> > attr_map_type;
        void set_attribute(const mc_attribute_key& attr, const std::vector<real>& value) { attrs_[attr] = value; }
        attr_map_type& get_attributes() { return attrs_; }
        const attr_map_type& get_attributes() const { return attrs_; }
    protected:
        std::vector<std::shared_ptr<mc_volume_node> > cld_;
        std::map<mc_attribute_key, std::vector<real> > attrs_;
    };

    class offset_mc_volume_node : public mc_volume_node
    {
    public:
        offset_mc_volume_node(const std::vector<std::shared_ptr<mc_volume_node> >& cld, real threshold) : mc_volume_node(cld), threshold_(threshold) {}
        offset_mc_volume_node(real threshold) : threshold_(threshold) {}
        virtual std::string type() const { return "Offset"; }
        virtual int typeN() const { return MC_VOLUME_OFFSET; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new offset_mc_volume_node(clone_children(), threshold_)); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return std::shared_ptr<mc_volume_node>(new offset_mc_volume_node(threshold_)); }
    public:
        real get_threshold() const { return threshold_; }
    protected:
        real threshold_;
    };

    class transform_mc_volume_node : public mc_volume_node
    {
    protected:
        transform_mc_volume_node(const std::vector<std::shared_ptr<mc_volume_node> >& cld, const matrix4& mat, const matrix4& imat)
            : mc_volume_node(cld), mat_(mat), imat_(imat) {}
        transform_mc_volume_node(const matrix4& mat, const matrix4& imat)
            : mat_(mat), imat_(imat) {}
    public:
        transform_mc_volume_node(const std::shared_ptr<mc_volume_node>& c, const matrix4& mat)
            : mat_(mat)
        {
            cld_.push_back(c);
            imat_ = ~mat;
        }
        transform_mc_volume_node(const std::vector<std::shared_ptr<mc_volume_node> >& cld, const matrix4& mat)
            : mc_volume_node(cld), mat_(mat)
        {
            imat_ = ~mat;
        }
        transform_mc_volume_node(const matrix4& mat)
            : mat_(mat)
        {
            imat_ = ~mat;
        }
        virtual std::string type() const { return "Transform"; }
        virtual int typeN() const { return MC_VOLUME_TRANSFORM; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new transform_mc_volume_node(clone_children(), mat_, imat_)); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return std::shared_ptr<mc_volume_node>(new transform_mc_volume_node(mat_, imat_)); }
    public:
        matrix4 get_matrix() const { return mat_; }
        matrix4 get_inverse_matrix() const { return imat_; }
    protected:
        matrix4 mat_;
        matrix4 imat_;
    };

    class add_mc_volume_node : public mc_volume_node
    {
    public:
        add_mc_volume_node() {}
        add_mc_volume_node(const std::vector<std::shared_ptr<mc_volume_node> >& cld) : mc_volume_node(cld) {}
        virtual std::string type() const { return "Add"; }
        virtual int typeN() const { return MC_VOLUME_ADD; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new add_mc_volume_node(clone_children())); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return std::shared_ptr<mc_volume_node>(new add_mc_volume_node()); }
    };

    class sub_mc_volume_node : public mc_volume_node
    {
    public:
        sub_mc_volume_node() {}
        sub_mc_volume_node(const std::vector<std::shared_ptr<mc_volume_node> >& cld) : mc_volume_node(cld) {}
        virtual std::string type() const { return "Sub"; }
        virtual int typeN() const { return MC_VOLUME_SUB; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new sub_mc_volume_node(clone_children())); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return std::shared_ptr<mc_volume_node>(new sub_mc_volume_node()); }
    };

    class mul_mc_volume_node : public mc_volume_node
    {
    public:
        mul_mc_volume_node() {}
        mul_mc_volume_node(const std::vector<std::shared_ptr<mc_volume_node> >& cld) : mc_volume_node(cld) {}
        virtual std::string type() const { return "Mul"; }
        virtual int typeN() const { return MC_VOLUME_MUL; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new mul_mc_volume_node(clone_children())); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return std::shared_ptr<mc_volume_node>(new mul_mc_volume_node()); }
    };

    class div_mc_volume_node : public mc_volume_node
    {
    public:
        div_mc_volume_node() {}
        div_mc_volume_node(const std::vector<std::shared_ptr<mc_volume_node> >& cld) : mc_volume_node(cld) {}
        virtual std::string type() const { return "Div"; }
        virtual int typeN() const { return MC_VOLUME_DIV; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new div_mc_volume_node(clone_children())); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return std::shared_ptr<mc_volume_node>(new div_mc_volume_node()); }
    };

    class neg_mc_volume_node : public mc_volume_node
    {
    public:
        neg_mc_volume_node() {}
        neg_mc_volume_node(const std::vector<std::shared_ptr<mc_volume_node> >& cld) : mc_volume_node(cld) {}
        virtual std::string type() const { return "Neg"; }
        virtual int typeN() const { return MC_VOLUME_NEG; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new neg_mc_volume_node(clone_children())); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return std::shared_ptr<mc_volume_node>(new neg_mc_volume_node()); }
    };

    class inv_mc_volume_node : public mc_volume_node
    {
    public:
        inv_mc_volume_node() {}
        inv_mc_volume_node(const std::vector<std::shared_ptr<mc_volume_node> >& cld) : mc_volume_node(cld) {}
        virtual std::string type() const { return "Inv"; }
        virtual int typeN() const { return MC_VOLUME_INV; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new inv_mc_volume_node(clone_children())); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return std::shared_ptr<mc_volume_node>(new inv_mc_volume_node()); }
    };

    class min_mc_volume_node : public mc_volume_node
    {
    public:
        min_mc_volume_node() {}
        min_mc_volume_node(const std::vector<std::shared_ptr<mc_volume_node> >& cld) : mc_volume_node(cld) {}
        virtual std::string type() const { return "Min"; }
        virtual int typeN() const { return MC_VOLUME_MIN; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new min_mc_volume_node(clone_children())); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return std::shared_ptr<mc_volume_node>(new min_mc_volume_node()); }
    };

    class max_mc_volume_node : public mc_volume_node
    {
    public:
        max_mc_volume_node() {}
        max_mc_volume_node(const std::vector<std::shared_ptr<mc_volume_node> >& cld) : mc_volume_node(cld) {}
        virtual std::string type() const { return "Max"; }
        virtual int typeN() const { return MC_VOLUME_MAX; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new max_mc_volume_node(clone_children())); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return std::shared_ptr<mc_volume_node>(new max_mc_volume_node()); }
    };

    class fma_mc_volume_node : public mc_volume_node
    {
    public:
        fma_mc_volume_node()
            : a_(1), c_(0), min_(0), max_(1)
        {
        }
        fma_mc_volume_node(real a, real c, real min, real max)
            : a_(a), c_(c), min_(min), max_(max)
        {
        }
        fma_mc_volume_node(
            const std::vector<std::shared_ptr<mc_volume_node> >& cld, real a = 1.0, real c = 0.0, real min = 0.0, real max = 1.0) : mc_volume_node(cld), a_(a), c_(c), min_(min), max_(max)
        {
        }
        virtual std::string type() const { return "FMA"; }
        virtual int typeN() const { return MC_VOLUME_FMA; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new fma_mc_volume_node(clone_children(), a_, c_, min_, max_)); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return std::shared_ptr<mc_volume_node>(new fma_mc_volume_node(a_, c_, min_, max_)); }
    public:
        real get_a() const { return a_; }
        real get_c() const { return c_; }
        real get_min() const { return min_; }
        real get_max() const { return max_; }
    public:
        real get_mul() const { return get_a(); }
        real get_add() const { return get_c(); }
    protected:
        real a_;
        real c_;
        real min_;
        real max_;
    };

    class sphere_mc_volume_node : public mc_volume_node
    {
    public:
        sphere_mc_volume_node(const vector3& p, real r) : p_(p), r_(r) {}
        virtual std::string type() const { return "Sphere"; }
        virtual int typeN() const { return MC_VOLUME_SPHERE; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new sphere_mc_volume_node(p_, r_)); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return clone(); }
    public:
        vector3 get_position() const { return p_; }
        real get_radius() const { return r_; }
    protected:
        vector3 p_;
        real r_;
    };

    class ellipsoid_mc_volume_node : public mc_volume_node
    {
    protected:
        ellipsoid_mc_volume_node(const matrix4& mat, const matrix4& imat) : mat_(mat), imat_(imat) {}
    public:
        ellipsoid_mc_volume_node(const matrix4& mat) : mat_(mat)
        {
            imat_ = ~mat;
        }
        virtual std::string type() const { return "Ellipsoid"; }
        virtual int typeN() const { return MC_VOLUME_ELLIPSOID; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new ellipsoid_mc_volume_node(mat_, imat_)); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return clone(); }
    public:
        matrix4 get_matrix() const { return mat_; }
        matrix4 get_inverse_matrix() const { return imat_; }
    protected:
        matrix4 mat_;
        matrix4 imat_;
    };

    class segment_mc_volume_node : public mc_volume_node
    {
    public:
        segment_mc_volume_node(const vector3& p0, const vector3& p1, real r, const matrix4& mat) : p0_(p0), p1_(p1), r_(r), mat_(mat) {}
        virtual std::string type() const { return "Segment"; }
        virtual int typeN() const { return MC_VOLUME_SEGMENT; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new segment_mc_volume_node(p0_, p1_, r_, mat_)); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return clone(); }
    public:
        vector3 get_position0() const { return p0_; }
        vector3 get_position1() const { return p1_; }
        real get_radius() const { return r_; }
        matrix4 get_matrix() const { return mat_; }
    protected:
        vector3 p0_;
        vector3 p1_;
        real r_;
        matrix4 mat_;
    };

    class impl_segment_mc_volume_node : public mc_volume_node
    {
    public:
        impl_segment_mc_volume_node(const vector3& p0, real r0, const vector3& p1, real r1)
            : p0_(p0), r0_(r0), p1_(p1), r1_(r1)
        {
        }
        virtual std::string type() const { return "ImplSegment"; }
        virtual int typeN() const { return MC_VOLUME_IMPL_SEGMENT; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new impl_segment_mc_volume_node(p0_, r0_, p1_, r1_)); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return clone(); }
    public:
        vector3 get_position0() const { return p0_; }
        vector3 get_position1() const { return p1_; }
        real get_radius0() const { return r0_; }
        real get_radius1() const { return r1_; }
    protected:
        vector3 p0_;
        real r0_;
        vector3 p1_;
        real r1_;
    };

    class impl_cube_mc_volume_node : public mc_volume_node
    {
    public:
        impl_cube_mc_volume_node() {}
        virtual std::string type() const { return "ImplCube"; }
        virtual int typeN() const { return MC_VOLUME_IMPL_CUBE; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new impl_cube_mc_volume_node()); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return clone(); }
    public:
        vector3 get_min() const { return -vector3(1, 1, 1); }
        vector3 get_max() const { return +vector3(1, 1, 1); }
    };

    class bound_mc_volume_node : public mc_volume_node
    {
    public:
        bound_mc_volume_node(const vector3& min, const vector3& max, const std::shared_ptr<mc_volume_node>& node)
            : min_(min), max_(max), node_(node)
        {
        }
        virtual std::string type() const { return "Bound"; }
        virtual int typeN() const { return MC_VOLUME_BOUND; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new bound_mc_volume_node(min_, max_, node_->clone())); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return clone(); }
    public:
        const std::shared_ptr<mc_volume_node>& get_node() const { return node_; }
        vector3 get_min() const { return min_; }
        vector3 get_max() const { return max_; }
    protected:
        vector3 min_;
        vector3 max_;
        std::shared_ptr<mc_volume_node> node_;
    };

    class bvh_mc_volume_node : public mc_volume_node
    {
    public:
        bvh_mc_volume_node() {}
        bvh_mc_volume_node(const std::vector<std::shared_ptr<mc_volume_node> >& cld, const vector3& min, const vector3& max, int type)
            : mc_volume_node(cld), min_(min), max_(max), type_(type) {}
        virtual std::string type() const { return "BVH"; }
        virtual int typeN() const { return MC_VOLUME_BVH; }
        virtual std::shared_ptr<mc_volume_node> clone() const { return std::shared_ptr<mc_volume_node>(new bvh_mc_volume_node(clone_children(), min_, max_, type_)); }
        virtual std::shared_ptr<mc_volume_node> clone_nochildren() const { return std::shared_ptr<mc_volume_node>(new bvh_mc_volume_node()); }
    public:
        const std::shared_ptr<mc_volume_node>& get_left() const { return cld_[0]; }
        const std::shared_ptr<mc_volume_node>& get_right() const { return cld_[1]; }
        int internalTypeN() const { return type_; }
        vector3 get_min() const { return min_; }
        vector3 get_max() const { return max_; }
    protected:
        vector3 min_;
        vector3 max_;
        int type_;
    };
}

#endif