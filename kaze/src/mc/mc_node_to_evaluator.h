#ifndef MC_NODE_TO_EVALUATOR_H
#define MC_NODE_TO_EVALUATOR_H

#include "evaluator/mc_volume_evaluator.h"
#include "mc_volume_node.h"

namespace mc
{
    std::shared_ptr<mc_volume_node> mc_node_add_bvh   (const std::shared_ptr<mc_volume_node>& node);
    std::shared_ptr<mc_volume_node> mc_node_remove_bvh(const std::shared_ptr<mc_volume_node>& node, const vector3& min, const vector3& max);
    std::shared_ptr<mc_volume_evaluator> mc_node_to_evaluator(const std::shared_ptr<mc_volume_node>& node);
    std::shared_ptr<mc_volume_evaluator> mc_node_to_evaluator(const std::shared_ptr<mc_volume_node>& node, const vector3& min, const vector3& max);
    void mc_node_get_bounds(std::vector<mc_bound>& bounds, const std::shared_ptr<mc_volume_node>& node);
}

#endif
