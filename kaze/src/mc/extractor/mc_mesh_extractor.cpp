#include "mc_mesh_extractor.h"
#include "mc_tables.h"

#include <algorithm>

namespace mc
{
    namespace
    {
        typedef mc_volume_evaluator::bound bound;
        typedef mc_volume_evaluator::attribute attribute;

        struct TStat
        {
            size_t sample_time;
            size_t polygonize_time;
        };

#define GETVALUE(i) GetVertexValue(px[i], py[i], pz[i])
#define ADDVERTEX(i, j) AddVertex(px[i], py[i], pz[i], px[j], py[j], pz[j]);

        struct TGrid
        {
            vector3 min;
            vector3 max;
            int divs[3];
            int offset[3];
            vector3 delta;
            std::vector<real> values;

            std::vector<size_t> indices;
            std::vector<vector3> vertices;
            std::vector<vector3> normals;

            typedef std::pair<size_t, size_t> edge_t;
            typedef std::map<edge_t, size_t> edge_map_t;
            edge_map_t edge_map;

            void Set(const vector3& min_, const vector3& max_, const int divs_[3])
            {
                min = min_;
                max = max_;
                memcpy(divs, divs_, sizeof(int)*3);
                delta = vector3(1.0/divs[0],1.0/divs[1],1.0/divs[2]) * (max - min);
            }

            size_t GetVertexIndex(int i, int j, int k) const
            {
                i -= offset[0];
                j -= offset[1];
                k -= offset[2];

                int w = divs[0] + 1;
                int h = divs[1] + 1;
                int d = divs[2] + 1;
                return (k * w * h) + (j * w + i);
            }

            real GetVertexValue(int i, int j, int k) const
            {
                return values[GetVertexIndex(i, j, k)];
            }

            size_t RegistVertexIndices(int i0, int j0, int k0, int i1, int j1, int k1)
            {
                size_t v0 = GetVertexIndex(i0, j0, k0);
                size_t v1 = GetVertexIndex(i1, j1, k1);

                if (v0 > v1) std::swap(v0, v1);

                edge_t e = std::make_pair(v0, v1);
                edge_map_t::const_iterator it = edge_map.find(e);
                if (it != edge_map.end())
                {
                    return it->second;
                }
                else
                {
                    size_t sz = vertices.size();
                    edge_map.insert(edge_map_t::value_type(e, sz));
                    vertices.push_back(vector3(0, 0, 0));
                    return sz;
                }
            }

            vector3 GetVertexPosition(int i, int j, int k)
            {
                return min + vector3(i, j, k) * delta;
            }

            size_t AddVertex(int i0, int j0, int k0, int i1, int j1, int k1)
            {
                size_t idx = RegistVertexIndices(i0, j0, k0, i1, j1, k1);

                size_t v0 = GetVertexIndex(i0, j0, k0);
                size_t v1 = GetVertexIndex(i1, j1, k1);

                real val0 = values[v0];
                real val1 = values[v1];

                real t = fabs(val0 / (val1 - val0));

                vector3 p = (1 - t) * GetVertexPosition(i0, j0, k0) + t * GetVertexPosition(i1, j1, k1);
                vertices[idx] = p;

                return idx;
            }

            size_t AddFace(size_t i, size_t j, size_t k)
            {
                size_t sz = indices.size() / 3;
                indices.push_back(i);
                indices.push_back(j);
                indices.push_back(k);
                return sz;
            }

            void ProcessCell(int i, int j, int k)
            {
                int px[8] = {i, i + 1, i + 1, i, i, i + 1, i + 1, i};
                int py[8] = {j, j, j, j, j + 1, j + 1, j + 1, j + 1};
                int pz[8] = {k, k, k + 1, k + 1, k, k, k + 1, k + 1};

                /*
                int nCorner[8][3];
                for(int m = 0; m < 8; m++) {
                    nCorner[m][0] = px[m];
                    nCorner[m][1] = py[m];
                    nCorner[m][2] = pz[m];
                }
                */

                int cubetype = 0;
                if (GETVALUE(0) > 0) cubetype |= 1;
                if (GETVALUE(1) > 0) cubetype |= 2;
                if (GETVALUE(2) > 0) cubetype |= 4;
                if (GETVALUE(3) > 0) cubetype |= 8;
                if (GETVALUE(4) > 0) cubetype |= 16;
                if (GETVALUE(5) > 0) cubetype |= 32;
                if (GETVALUE(6) > 0) cubetype |= 64;
                if (GETVALUE(7) > 0) cubetype |= 128;

                if (cubetype == 0 || cubetype == 255)
                    return;

                size_t samples[12] = {}; //

                if (edgeTable[cubetype] & 1) samples[0] = ADDVERTEX(0, 1);
                if (edgeTable[cubetype] & 2) samples[1] = ADDVERTEX(1, 2);
                if (edgeTable[cubetype] & 4) samples[2] = ADDVERTEX(3, 2);
                if (edgeTable[cubetype] & 8) samples[3] = ADDVERTEX(0, 3);
                if (edgeTable[cubetype] & 16) samples[4] = ADDVERTEX(4, 5);
                if (edgeTable[cubetype] & 32) samples[5] = ADDVERTEX(5, 6);
                if (edgeTable[cubetype] & 64) samples[6] = ADDVERTEX(7, 6);
                if (edgeTable[cubetype] & 128) samples[7] = ADDVERTEX(4, 7);
                if (edgeTable[cubetype] & 256) samples[8] = ADDVERTEX(0, 4);
                if (edgeTable[cubetype] & 512) samples[9] = ADDVERTEX(1, 5);
                if (edgeTable[cubetype] & 1024) samples[10] = ADDVERTEX(2, 6);
                if (edgeTable[cubetype] & 2048) samples[11] = ADDVERTEX(3, 7);

                // connect samples by triangles
                for (int m = 0; triTable[cubetype][m] != -1; m += 3)
                {
                    AddFace(
                        samples[triTable[cubetype][m]],
                        samples[triTable[cubetype][m + 1]],
                        samples[triTable[cubetype][m + 2]]);
                }
            }
        };

        static
        bool get_active_bound(mc_bound& rb, const mc_volume_evaluator& e, const vector3& min, const vector3& max)
        {
            mc_bound eb = e.get_bound();
            vector3 emin = eb.min;
            vector3 emax = eb.max;
            for(int i = 0;i<3;i++)
            {
                emin[i] = std::max(emin[i], min[i]);
                emax[i] = std::min(emax[i], max[i]);
                if(emin[i] >= emax[i])return false;
            }
            rb.min = emin;
            rb.max = emax;
            return true;
        }

        struct index_range
        {
            int imin[3];
            int imax[3];
        };

        struct L
        {
            L(int p, int m):p_(p),m_(m){}
            bool operator()(const index_range& r)const
            {
                return r.imin[p_] < m_;
            }
            int p_;
            int m_;
        };

        struct R
        {
            R(int p, int m):p_(p),m_(m){}
            bool operator()(const index_range& r)const
            {
                return r.imax[p_] >= m_;
            }
            int p_;
            int m_;
        };

        typedef index_range* iterator;

        static bool IsAllContain(const int imin[3], const int imax[3], iterator a, iterator b)
        {
            for(iterator it = a; it != b; it++)
            {
                index_range rng = *it;
                for(int i = 0; i < 3; i++)
                {
                    if(imin[i] < rng.imin[i] || rng.imax[i] < imax[i])return false;
                }
            }
            return true;
        }

        static int stat0 = 0;
        static int stat1 = 0;
        static int stat2 = 0;
        static int stat3 = 0;

        static void CreateNode(
            std::vector<index_range>& ranges,
            const int imin[3], const int imax[3],
            iterator a, iterator b
        )
        {
            static const int MIN_GRID = 16;

            int iwid[3] = {};
            iwid[0] = imax[0] - imin[0];
            iwid[1] = imax[1] - imin[1];
            iwid[2] = imax[2] - imin[2];
            int sz0 = iwid[0]*iwid[1]*iwid[2];
            int sz1 = b - a;
            //print_log("%d:\n",sz1);
            if(sz1 <= 0)
            {
                stat0++;
            }
            else if(iwid[0]<=MIN_GRID && iwid[1]<=MIN_GRID && iwid[2]<=MIN_GRID)
            {
                stat1++;

                index_range rng;
                memcpy(rng.imin, imin, sizeof(int)*3);
                memcpy(rng.imax, imax, sizeof(int)*3);
                ranges.push_back(rng);
            }
            else if(IsAllContain(imin, imax, a, b))
            {
                stat2++;

                index_range rng;
                memcpy(rng.imin, imin, sizeof(int)*3);
                memcpy(rng.imax, imax, sizeof(int)*3);
                ranges.push_back(rng);
            }
            else if(sz1 <= 1)
            {
                stat3++;

                index_range rng = *a;
                for(int i = 0; i < 3; i++)
                {
                    rng.imin[i] = std::max(rng.imin[i], imin[i]);
                    rng.imax[i] = std::min(rng.imax[i], imax[i]);
                }
                ranges.push_back(rng);
            }
            else
            {
                int nPlane = 0;
                if(iwid[nPlane] < iwid[1])nPlane = 1;
                if(iwid[nPlane] < iwid[2])nPlane = 2;
                int delta = iwid[nPlane] >> 1;
                int mid = imin[nPlane] + delta;
                //print_log("mid:%d:\n",mid);
                int imid[3];
                {
                    memcpy(imid, imax, sizeof(int)*3);
                    imid[nPlane] = mid;
                    iterator c = std::partition(a, b, L(nPlane,mid));
                    //int ksz = c - a;
                    //print_log("L:%d:\n",ksz);
                    CreateNode(ranges, imin, imid, a, c);
                }
                {
                    memcpy(imid, imin, sizeof(int)*3);
                    imid[nPlane] = mid;
                    iterator c = std::partition(a, b, R(nPlane,mid));
                    //int ksz = c - a;
                    //print_log("R:%d:\n",ksz);
                    CreateNode(ranges, imid, imax, a, c);
                }
            }

        }

        static bool CreateMesh(TGrid& grid, TStat& stat, const mc_volume_evaluator& e, const vector3& min, const vector3& max, int dim[3])
        {
            /*
            grid.min = min;
            grid.max = max;
            grid.divs[0] = dim[0];
            grid.divs[1] = dim[1];
            grid.divs[2] = dim[2];
            */

            mc_bound rb;
            if(!get_active_bound(rb, e, min, max))return false;

            int w = dim[0] + 1;
            int h = dim[1] + 1;
            int d = dim[2] + 1;

            //print_log("%d,%d,%d:\n",w,h,d);

            int imin[3];
            int imax[3];
            for(int i = 0; i < 3; i++)
            {
                imin[i] = (int)floor((rb.min[i] - min[i])/(max[i] - min[i]) * (dim[i] + 1));
                imax[i] = (int)ceil ((rb.max[i] - min[i])/(max[i] - min[i]) * (dim[i] + 1));
                imin[i] = std::min(imin[i], dim[i]+1);
                imax[i] = std::min(imax[i], dim[i]+1);
                if(imin[i] >= imax[i])return false;
            }

            grid.Set(min, max, dim);

            for(int i = 0; i < 3; i++)
            {
                grid.offset[i] = imin[i];
                grid.divs[i] = (imax[i]-imin[i]-1);
            }


            {
            //    size_t sz0 = w*h*d;
            //    size_t sz1 = (imax[0]-imin[0])*(imax[1]-imin[1])*(imax[2]-imin[2]);
            //    print_log("%d/%d = %f\n", sz1, sz0, double(100*sz1)/sz0);
            }

            std::vector<index_range> iranges_m;
            {
                std::vector<mc_bound> bounds;
                e.get_bounds(bounds);

                std::vector<index_range> iranges;
                {
                    size_t sz = bounds.size();
                    for(size_t j =0;j<sz;j++)
                    {
                        vector3 rmin = bounds[j].min;
                        vector3 rmax = bounds[j].max;

                        index_range rng;
                        for(int i = 0; i < 3; i++)
                        {
                            rng.imin[i] = (int)floor((rmin[i] - min[i])/(max[i] - min[i]) * (dim[i] + 1));
                            rng.imax[i] = (int)ceil ((rmax[i] - min[i])/(max[i] - min[i]) * (dim[i] + 1));
                            rng.imin[i] = std::min(rng.imin[i], dim[i]+1);
                            rng.imax[i] = std::min(rng.imax[i], dim[i]+1);
                        }
                        iranges.push_back(rng);
                    }
                }

                CreateNode(iranges_m, imin, imax, &iranges[0], &iranges[0] + iranges.size());
/*
                print_log("%d\n", stat0);
                print_log("%d\n", stat1);
                print_log("%d\n", stat2);
                print_log("%d\n", stat3);
*/
                size_t sz0 = w*h*d;
                size_t sz1 = 0;
                {
                    size_t sz = iranges.size();
                    for(size_t j =0;j<sz;j++)
                    {
                        int x0 = iranges[j].imin[0];
                        int x1 = iranges[j].imax[0];
                        int y0 = iranges[j].imin[1];
                        int y1 = iranges[j].imax[1];
                        int z0 = iranges[j].imin[2];
                        int z1 = iranges[j].imax[2];

                        size_t sz_v = (x1-x0)*(y1-y0)*(z1-z0);
                        sz1 += sz_v;
                    }
                }
                size_t sz2 = 0;
                {
                    size_t sz = iranges_m.size();
                    for(size_t j =0;j<sz;j++)
                    {
                        int x0 = iranges_m[j].imin[0];
                        int x1 = iranges_m[j].imax[0];
                        int y0 = iranges_m[j].imin[1];
                        int y1 = iranges_m[j].imax[1];
                        int z0 = iranges_m[j].imin[2];
                        int z1 = iranges_m[j].imax[2];

                        size_t sz_v = (x1-x0)*(y1-y0)*(z1-z0);
                        sz2 += sz_v;
                    }
                }
                size_t sz3 = (imax[0]-imin[0])*(imax[1]-imin[1])*(imax[2]-imin[2]);
                //print_log("%d/%d = %f\n", sz1, sz0, double(100*sz1)/sz0);
                //print_log("%d/%d = %f\n", sz2, sz0, double(100*sz2)/sz0);
                //print_log("%d/%d = %f\n", sz3, sz0, double(100*sz3)/sz0);

            }


            bound b;
            b.min = min;
            b.max = max;
            if (e.test_bound(b))
            {
                //timer t;

                //t.start();
                {
                    size_t sz_v = (imax[0]-imin[0])*(imax[1]-imin[1])*(imax[2]-imin[2]);
                    std::vector<real> values(sz_v);
                    for (int i = 0; i < sz_v; i++)
                    {
                        values[i] = -1;
                    }
/*
                    //size_t index = 0;
                    for (int k = imin[2]; k < imax[2]; k++)
                    {
                        for (int j = imin[1]; j < imax[1]; j++)
                        {
                            for (int i = imin[0]; i < imax[0]; i++)
                            {
                                values[grid.GetVertexIndex(i, j, k)] = e.get_value(grid.GetVertexPosition(i, j, k));
                            }
                        }
                    }
*/
                    size_t sz = iranges_m.size();
                    //print_log("merged:%d\n", sz);
                    for(size_t p =0;p<sz;p++)
                    {
                        for (int k = iranges_m[p].imin[2]; k < iranges_m[p].imax[2]; k++)
                        {
                            for (int j = iranges_m[p].imin[1]; j < iranges_m[p].imax[1]; j++)
                            {
                                for (int i = iranges_m[p].imin[0]; i < iranges_m[p].imax[0]; i++)
                                {
                                    values[grid.GetVertexIndex(i, j, k)] = e.get_value(grid.GetVertexPosition(i, j, k));
                                }
                            }
                        }
                    }
                    grid.values.swap(values);
                }

                //t.end();
                //stat.sample_time = t.msec();

                //t.start();
                {

                    for (int k = imin[2]; k < imax[2] - 1; k++)
                    {
                        for (int j = imin[1]; j < imax[1] - 1; j++)
                        {
                            for (int i = imin[0]; i < imax[0] - 1; i++)
                            {
                                grid.ProcessCell(i, j, k);
                            }
                        }
                    }

                    size_t sz = iranges_m.size();
                    //print_log("merged:%d\n", sz);
                    /*
                    for(size_t p =0;p<sz;p++)
                    {
                        for (int k = iranges_m[p].imin[2]; k < iranges_m[p].imax[2]-1; k++)
                        {
                            for (int j = iranges_m[p].imin[1]; j < iranges_m[p].imax[1]-1; j++)
                            {
                                for (int i = iranges_m[p].imin[0]; i < iranges_m[p].imax[0]-1; i++)
                                {
                                    grid.ProcessCell(i, j, k);
                                }
                            }
                        }
                    }
                    */
                }
                //t.end();
                //stat.polygonize_time = t.msec();
                /*
                if (grid.vertices.size() != 0)
                {
                    size_t sz = grid.vertices.size();
                    grid.normals.resize(sz);
                    for (size_t i = 0; i < sz; i++)
                    {
                        vector3 grad = e.get_gradient(grid.vertices[i]);
                        if (grad.sqr_length() > 0){
                            grid.normals[i] = -normalize(grad);
                        }else{
                            grid.normals[i] = vector3(0,0,1);
                        }
                    }
                }
                */

                return true;
            }
            return true;
        }
    }

    mc_mesh_extractor::mc_mesh_extractor(const vector3& min, const vector3& max, int dim[3])
        : min_(min), max_(max)
    {
        dim_[0] = dim[0];
        dim_[1] = dim[1];
        dim_[2] = dim[2];
    }

    mc_mesh_extractor::~mc_mesh_extractor()
    {
    }

    bool mc_mesh_extractor::run(mc_mesh& mesh, const mc_volume_evaluator& e)
    {
        TGrid grid;
        TStat stat;
        //print_log("[%d, %d, %d]\n", dim_[0], dim_[1], dim_[2]);
        if (CreateMesh(grid, stat, e, min_, max_, dim_))
        {
            mesh.set_indices(grid.indices);
            mesh.set_vertices(grid.vertices);

            //print_log("sample time %ld ms\n", stat.sample_time);
            //print_log("polygonize time %ld ms\n", stat.polygonize_time);
            //mesh.normals.swap(grid.normals);
            return true;
        }
        return false;
    }
}

