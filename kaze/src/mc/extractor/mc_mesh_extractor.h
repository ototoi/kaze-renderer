#ifndef MC_MC_MESH_EXTRACTOR_H
#define MC_MC_MESH_EXTRACTOR_H

#include "mc_mesh.h"
#include "evaluator/mc_volume_evaluator.h"
#include <vector>
#include <map>

namespace mc
{
    class mc_mesh_extractor
    {
    public:
        mc_mesh_extractor(const vector3& min, const vector3& max, int dim[3]);
        ~mc_mesh_extractor();
        bool run(mc_mesh& mesh, const mc_volume_evaluator& ve);

    protected:
        vector3 min_;
        vector3 max_;
        int dim_[3];
    };
}

#endif