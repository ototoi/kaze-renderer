#include "ri_coordinate_system.h"

namespace ri
{
    ri_coordinate_system::ri_coordinate_system(RtToken name,
                                               const ri_transform& trns)
        : name_(name), trns_(trns) {}

    std::string ri_coordinate_system::get_name() const { return name_; }

    const ri_transform& ri_coordinate_system::get_transform() const
    {
        return trns_;
    }
}
