#ifndef RI_LIGHTSOURCE_H
#define RI_LIGHTSOURCE_H

#include <string>
#include <vector>
#include "ri.h"
#include "ri_shader.h"

namespace ri
{
    class ri_light_source : public ri_shader
    {
    public:
        ri_light_source(const ri_classifier* p_cls, RtToken name, RtInt n,
                        RtToken tokens[], RtPointer params[]);
        ~ri_light_source();

    public:
        virtual std::string type() const { return "LightSource"; }
        virtual void set_attributes(const ri_attributes& attr) { attr_ = attr; }
        virtual void set_transform(const ri_transform& trns) { trns_ = trns; }
        virtual const ri_attributes& get_attributes() const { return attr_; }
        virtual const ri_transform& get_transform() const { return trns_; }

    protected:
        ri_attributes attr_;
        ri_transform trns_;
    };

    class ri_geometry;
    class ri_group_geometry;

    class ri_area_light_source : public ri_light_source
    {
    public:
        ri_area_light_source(const ri_classifier* p_cls, RtToken name, RtInt n,
                             RtToken tokens[], RtPointer params[]);
        ~ri_area_light_source();

    public:
        virtual std::string type() const { return "AreaLightSource"; }

    public:
        ri_group_geometry* get_group_geometry() const;

    public:
        virtual void add_geometry(const ri_geometry* pGeo);
        virtual void get_geometry(std::vector<const ri_geometry*>& lv) const;

    protected:
        ri_group_geometry* pGeo_;
    };
}

#endif
