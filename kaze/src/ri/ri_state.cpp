#include "ri_state.h"

namespace ri
{

    enum
    {
        RI_STATE_IN_WORLD_MASK = 1,
        RI_STATE_IF_TRUE_MASK = 2,
        RI_STATE_NEVER_IF_TRUE_MASK = 4,
        RI_STATE_IN_RESOURCE_MASK = 8,
        RI_STATE_IN_ARCHIVE_MASK = 16,
        RI_STATE_IN_ARCHIVE_NEST_MASK = 32,
    };

    ri_state::ri_state() : nFlags_(0)
    {
        nFlags_ |= RI_STATE_IF_TRUE_MASK; //
        nFlags_ |= RI_STATE_NEVER_IF_TRUE_MASK; //
    }

    ri_state::ri_state(const ri_state& rhs) : nFlags_(rhs.nFlags_)
    {
        ; //
    }

    ri_state& ri_state::operator=(const ri_state& rhs)
    {
        nFlags_ = rhs.nFlags_;
        return *this;
    }

    bool ri_state::is_if_true() const
    {
        if (nFlags_ & RI_STATE_IF_TRUE_MASK && nFlags_ & RI_STATE_NEVER_IF_TRUE_MASK)
            return true;
        else
            return false;
    }

    void ri_state::set_if(bool b)
    {
        if (b)
        {
            nFlags_ |= RI_STATE_IF_TRUE_MASK;
        }
        else
        {
            nFlags_ &= ~RI_STATE_IF_TRUE_MASK;
        }
    }

     bool ri_state::is_never_if_true() const
     {
         if (nFlags_ & RI_STATE_NEVER_IF_TRUE_MASK)
            return true;
        else
            return false;
     }  

    void ri_state::set_never_if_true(bool b)
    {
        if (b)
        {
            nFlags_ |= RI_STATE_NEVER_IF_TRUE_MASK;
        }
        else
        {
            nFlags_ &= ~RI_STATE_NEVER_IF_TRUE_MASK;
        }
    }

    bool ri_state::is_in_world() const
    {
        if (nFlags_ & RI_STATE_IN_WORLD_MASK)
            return true;
        else
            return false;
    }

    void ri_state::set_in_world(bool b)
    {
        if (b)
        {
            nFlags_ |= RI_STATE_IN_WORLD_MASK;
        }
        else
        {
            nFlags_ &= ~RI_STATE_IN_WORLD_MASK;
        }
    }

    bool ri_state::is_in_resource() const
    {
        if (nFlags_ & RI_STATE_IN_RESOURCE_MASK)
            return true;
        else
            return false;
    }

    void ri_state::set_in_resource(bool b)
    {
        if (b)
        {
            nFlags_ |= RI_STATE_IN_RESOURCE_MASK;
        }
        else
        {
            nFlags_ &= ~RI_STATE_IN_RESOURCE_MASK;
        }
    }

    bool ri_state::is_in_archive() const
    {
        if (nFlags_ & RI_STATE_IN_ARCHIVE_MASK)
            return true;
        else
            return false;
    }

    void ri_state::set_in_archive(bool b)
    {
        if (b)
        {
            nFlags_ |= RI_STATE_IN_ARCHIVE_MASK;
        }
        else
        {
            nFlags_ &= ~RI_STATE_IN_ARCHIVE_MASK;
        }
    }

    bool ri_state::is_in_archive_nest() const
    {
        if (nFlags_ & RI_STATE_IN_ARCHIVE_NEST_MASK)
            return true;
        else
            return false;
    }
    void ri_state::set_in_archive_nest(bool b)
    {
        if (b)
        {
            nFlags_ |= RI_STATE_IN_ARCHIVE_NEST_MASK;
        }
        else
        {
            nFlags_ &= ~RI_STATE_IN_ARCHIVE_NEST_MASK;
        }
    }
}
