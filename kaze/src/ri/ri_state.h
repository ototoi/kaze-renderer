#ifndef RI_STATE_H
#define RI_STATE_H

namespace ri
{
    class ri_state
    {
    public:
        ri_state();
        ri_state(const ri_state& rhs);

    public:
        ri_state& operator=(const ri_state& rhs);

    public:
        bool is_if_true() const;
        void set_if(bool b);

        bool is_never_if_true() const;
        void set_never_if_true(bool b);
    public:
        bool is_in_world() const;
        void set_in_world(bool b);

    public:
        bool is_in_resource() const;
        void set_in_resource(bool b);

    public:
        bool is_in_archive() const;
        void set_in_archive(bool b);
        bool is_in_archive_nest() const;
        void set_in_archive_nest(bool b);

    protected:
        int nFlags_;
    };
}

#endif
