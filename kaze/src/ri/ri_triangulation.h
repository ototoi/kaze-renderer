#ifndef KAZE_TRIANGULATION_H
#define KAZE_TRIANGULATION_H

#include <vector>

namespace ri
{
    void ri_polygon_triangulate_indices(
        std::vector<int>& indices, std::vector<int>& tri2poly,
        std::vector<int>& fvindices, // for face varying
        int nverts, const float P[]);

    void ri_general_polygon_triangulate_indices(
        std::vector<int>& indices, std::vector<int>& tri2poly,
        std::vector<int>& fvindices, // for face varying
        int nloops, const int nverts[], const float P[]);

    void ri_points_polygons_triangulate_indices(
        std::vector<int>& indices, std::vector<int>& tri2poly,
        std::vector<int>& fvindices, // for face varying
        int npolys, const int nverts[], const int verts[], const float P[]);

    void ri_points_general_polygons_triangulate_indices(
        std::vector<int>& indices, std::vector<int>& tri2poly,
        std::vector<int>& fvindices, // for face varying
        int npolys, const int nloops[], const int nverts[], const int verts[],
        const float P[]);
}
#endif
