#ifndef RIB_ARRAY_H
#define RIB_ARRAY_H

#include <vector>
#include <string>
#include <utility>

typedef struct integer_array : public std::vector<int>
{
    ;
} integer_array;

typedef struct float_array : public std::vector<float>
{
    float_array() {}
    float_array(const integer_array& rhs)
    {
        size_t sz = rhs.size();
        resize(sz);
        for (size_t i = 0; i < sz; i++)
        {
            (*this)[i] = (float)rhs[i];
        }
    }
} float_array;

typedef struct string_array : public std::vector<char*>
{
    string_array() {}
    ~string_array()
    {
        size_t sz = this->size();
        for (size_t i = 0; i < sz; i++)
        {
            delete (*this)[i];
        }
    }
} string_array;

namespace detail
{
    template <class T>
    struct delete_value
    {
        static void func(T v) {}
    };
    template <class T>
    struct delete_value<T*>
    {
        static void func(T* v) { delete v; }
    };
}

struct value_conatainer
{
    typedef char* PCHAR;
    typedef integer_array* PIntArray;
    typedef float_array* PFloatArray;
    typedef string_array* PStringArray;

    virtual size_t size() const = 0;
    virtual ~value_conatainer() {}
    virtual const void* get_ptr() const = 0;
    static void* get_pointer(const int& v) { return (void*)&v; }
    static void* get_pointer(const float& v) { return (void*)&v; }
    static void* get_pointer(const PCHAR& v) { return (void*)&v; }
    static void* get_pointer(const PIntArray& v) { return (void*)(&((*v)[0])); }
    static void* get_pointer(const PFloatArray& v)
    {
        return (void*)(&((*v)[0]));
    }
    static void* get_pointer(const PStringArray& v)
    {
        return (void*)(&((*v)[0]));
    }

    static size_t get_size(const int& v) { return 1; }
    static size_t get_size(const float& v) { return 1; }
    static size_t get_size(const PCHAR& v) { return 1; }
    static size_t get_size(const PIntArray& v) { return v->size(); }
    static size_t get_size(const PFloatArray& v) { return v->size(); }
    static size_t get_size(const PStringArray& v) { return v->size(); }
};

template <class T>
struct base_value_container : public value_conatainer
{
    base_value_container(const T& rhs) : value(rhs) {}
    ~base_value_container() { detail::delete_value<T>::func(value); }
    const void* get_ptr() const { return get_pointer(value); }
    size_t size() const { return get_size(value); }
    T value;
};

typedef base_value_container<int> integer_value_container;
typedef base_value_container<float> float_value_container;
typedef base_value_container<char*> string_value_container;
typedef base_value_container<integer_array*> integer_array_value_container;
typedef base_value_container<float_array*> float_array_value_container;
typedef base_value_container<string_array*> string_array_value_container;

typedef struct token_value : public std::pair<std::string, value_conatainer*>
{
    typedef char* PCHAR;
    typedef std::pair<std::string, value_conatainer*> base_type;
    token_value(const std::string& key, int value)
        : base_type(key, new integer_value_container(value)) {}
    token_value(const std::string& key, float value)
        : base_type(key, new float_value_container(value)) {}
    token_value(const std::string& key, PCHAR value)
        : base_type(key, new string_value_container(value)) {}
    token_value(const std::string& key, integer_array* value)
        : base_type(key, new integer_array_value_container(value)) {}
    token_value(const std::string& key, float_array* value)
        : base_type(key, new float_array_value_container(value)) {}
    token_value(const std::string& key, string_array* value)
        : base_type(key, new string_array_value_container(value)) {}

    ~token_value()
    {
        if (this->second)
            delete this->second;
    }

} token_value;

typedef struct token_value_array : public std::vector<token_value*>
{
    typedef std::vector<token_value*> base_type;
    ~token_value_array()
    {
        size_t sz = this->size();
        for (size_t i = 0; i < sz; i++)
        {
            delete (*this)[i];
        }
    }
} token_value_array;

#endif
