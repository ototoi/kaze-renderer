#ifndef RI_RI_OPTIONS_H
#define RI_RI_OPTIONS_H

#include <vector>
#include "ri_parameters.h"

namespace ri
{
    class ri_camera_options;
    class ri_display;

    class ri_options
    {
    public:
        typedef const char* LPCSTR;

    public:
        ri_options();
        ~ri_options();
        ri_options(const ri_options& rhs);
        ri_options& operator=(const ri_options& rhs);
        void swap(ri_options& rhs);

    public:
        bool set(const char* key1, const char* key2, int val);
        bool set(const char* key1, const char* key2, float val);
        bool set(const char* key1, const char* key2, const char* val);
        bool set(const char* key1, const char* key2, const int* val, int n);
        bool set(const char* key1, const char* key2, const float* val, int n);
        bool set(const char* key1, const char* key2, const LPCSTR* val, int n);

    public:
        bool get(const char* key1, const char* key2, int& val) const;
        bool get(const char* key1, const char* key2, float& val) const;
        bool get(const char* key1, const char* key2, std::string& val) const;
        bool get(const char* key1, const char* key2, std::vector<int>& val) const;
        bool get(const char* key1, const char* key2, std::vector<float>& val) const;
        bool get(const char* key1, const char* key2,
                 std::vector<std::string>& val) const;

    public:
        void add_display(const ri_display*);
        size_t get_display_size() const;
        const ri_display* get_display_at(size_t i) const;

    public:
        void add_camera(const ri_camera_options*);
        size_t get_camera_size() const;
        const ri_camera_options* get_camera_at(size_t i) const;

    protected:
        bool set_path(const char* key1, const char* key2, const char* val);

    private:
        ri_parameters* map_;
        std::vector<const ri_display*> disps_;
        std::vector<const ri_camera_options*> cmrs_;
    };
}

#endif
