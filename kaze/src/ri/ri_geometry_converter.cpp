#include "ri_geometry_converter.h"
#include <memory>

namespace ri
{
    typedef ri_geometry_converter::map_type map_type;

    const ri_geometry*
    ri_geometry_converter::convert(const ri_geometry* pGeo) const
    {
        int nType = pGeo->typeN();
        switch (nType)
        {
        case RI_GEOMETRY_POLYGON:
        case RI_GEOMETRY_GENERAL_POLYGON:
        case RI_GEOMETRY_POINTS_POLYGONS:
        case RI_GEOMETRY_POINTS_GENERAL_POLYGONS:
        case RI_GEOMETRY_PATCH:
        case RI_GEOMETRY_PATCH_MESH:
        case RI_GEOMETRY_PROCEDURAL:
        case RI_GEOMETRY_CURVES:
            {
                map_type::iterator it = map_.find(pGeo);
                if (it != map_.end())
                {
                    pGeo = it->second.get();
                }
                else
                {
                    std::shared_ptr<ri_geometry> ap;
                    switch (nType)
                    {
                    case RI_GEOMETRY_POLYGON:
                        ap.reset(new ri_points_triangle_polygons_geometry(
                            dynamic_cast<const ri_polygon_geometry*>(pGeo)));
                        break;
                    case RI_GEOMETRY_GENERAL_POLYGON:
                        ap.reset(new ri_points_triangle_polygons_geometry(
                            dynamic_cast<const ri_general_polygon_geometry*>(pGeo)));
                        break;
                    case RI_GEOMETRY_POINTS_POLYGONS:
                        ap.reset(new ri_points_triangle_polygons_geometry(
                            dynamic_cast<const ri_points_polygons_geometry*>(pGeo)));
                        break;
                    case RI_GEOMETRY_POINTS_GENERAL_POLYGONS:
                        ap.reset(new ri_points_triangle_polygons_geometry(
                            dynamic_cast<const ri_points_general_polygons_geometry*>(pGeo)));
                        break;
                    case RI_GEOMETRY_PATCH:
                        ap.reset(new ri_bezier_patch_mesh_geometry(
                            dynamic_cast<const ri_patch_geometry*>(pGeo)));
                        break;
                    case RI_GEOMETRY_PATCH_MESH:
                        ap.reset(new ri_bezier_patch_mesh_geometry(
                            dynamic_cast<const ri_patch_mesh_geometry*>(pGeo)));
                        break;
                    case RI_GEOMETRY_PROCEDURAL:
                        ap.reset(new ri_procedural_evaluation_geometry(
                            dynamic_cast<const ri_procedural_geometry*>(pGeo)));
                        break;
                    case RI_GEOMETRY_CURVES:
                        ap.reset(new ri_bezier_curves_geometry(
                            dynamic_cast<const ri_curves_geometry*>(pGeo)));
                        break;
                    }
                    if (ap.get())
                    {
                        map_.insert(map_type::value_type(pGeo, ap));
                        pGeo = ap.get();
                    }
                    else
                    {
                        pGeo = NULL;
                    }
                }
            }
            return convert(pGeo);
        }
        return pGeo;
    }
}
