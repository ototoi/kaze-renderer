#include "ri_read_archive.h"
#include "ri_stdout_context.h"
#include "ri_frame_context.h"
#include "ri_composite_context.h"
#include "ri_rib_decoder.h"
#include "ri_path_resolver.h"

//#include "logger.h"

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#ifdef _WIN32
#include <windows.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>

#include <limits.h>
#include <stdlib.h>
#endif

#include <string.h>
#include <memory>

#ifndef HAVE_ZLIB
#ifdef _WIN32
#define HAVE_ZLIB 1
#else
#define HAVE_ZLIB 1
#endif
#endif

#ifdef HAVE_ZLIB
#include <zlib.h>
#endif

namespace
{
#ifdef _WIN32
    static bool IsDirectory(const char* path)
    {
        struct _stat64i32 st;
        int nRet = _stat64i32(path, &st);
        if (nRet != 0)
            return false;
        if (st.st_mode & _S_IFDIR)
            return true;
        return false;
    }

    static bool IsFile(const char* path)
    {
        struct _stat64i32 st;
        int nRet = _stat64i32(path, &st);
        if (nRet != 0)
            return false;
        if (st.st_mode & _S_IFREG)
            return true;
        return false;
    }

    static std::string GetDirectoryName_(const char* szPath)
    {
        std::string strPath(szPath);
        if (strPath.empty())
            return strPath;
        size_t sz = strPath.size();
        if (strPath[sz - 1] == '/' || strPath[sz - 1] == '\\')
        {
            strPath = strPath.substr(0, sz - 1);
        }
        return strPath;
    }

    static std::string GetFileToDirectoryName(const char* szPath)
    {
        char szDrive[_MAX_DRIVE];
        char szDir[_MAX_DIR];
        // char szFname[_MAX_FNAME];
        // char szExt[_MAX_EXT];

        _splitpath(szPath, szDrive, szDir, NULL, NULL);

        std::string sRet;
        sRet += szDrive;
        sRet += szDir;
        return sRet;
    }

    std::string GetFileToDirectoryName__(const char* szPath)
    {
        char szDrive[_MAX_DRIVE] = {};
        char szDir[_MAX_DIR] = {};
        char szFname[_MAX_FNAME] = {};
        char szExt[_MAX_EXT] = {};

        _splitpath(szPath, szDrive, szDir, szFname, szExt);

        size_t sz = strlen(szExt);
        if (sz != 0)
        {
            std::string sRet;
            sRet += szDrive;
            sRet += szDir;
            return GetDirectoryName_(sRet.c_str());
        }
        else
        {
            std::string sRet;
            sRet += szDrive;
            sRet += szDir;
            sRet += szFname;
            return sRet;
        }
    }

    static std::string GetFullPath(const char* szPath)
    {
        char szFullPath[_MAX_PATH];
        _fullpath(szFullPath, szPath, _MAX_PATH);
        return szFullPath;
    }

    static std::string GetDirectoryName(const char* szPath)
    {
        std::string strPath = GetDirectoryName_(szPath);
        if (IsDirectory(strPath.c_str()))
            return szPath;
        if (IsFile(strPath.c_str()))
        {
            strPath = GetFullPath(strPath.c_str());
            strPath = GetFileToDirectoryName(strPath.c_str());
            return strPath;
        }
        else
        {
            strPath = GetFullPath(strPath.c_str());
            return GetFileToDirectoryName__(strPath.c_str());
            return strPath;
        }
    }

    static std::string GetFileName(const char* path)
    {
        // char szFullPath[_MAX_PATH];
        // char szDrive[_MAX_DRIVE];
        // char szDir[_MAX_DIR];
        char szFname[_MAX_FNAME];
        char szExt[_MAX_EXT];

        _splitpath(path, NULL, NULL, szFname, szExt);
        std::string sRet;
        sRet += szFname;
        sRet += szExt;
        return sRet;
    }

    static std::string SetCurrentFolder(const char* szFilePath)
    {
        char szOldPath[_MAX_PATH] = {};
        ::GetCurrentDirectoryA(_MAX_PATH, szOldPath);
        char szOldFullPath[_MAX_PATH] = "";
        _fullpath(szOldFullPath, szOldPath, _MAX_PATH);

        std::string strDir = GetDirectoryName(szFilePath);

        ::SetCurrentDirectoryA(strDir.c_str());

        return szOldFullPath;
    }
#else
    static std::string get_dir(const char* szPath)
    {
        DIR* pDir = opendir(szPath);
        if (pDir)
        {
            closedir(pDir);
            return szPath;
        }
        else
        {
            std::string strPath(szPath);
            char buffer[512];
            strcpy(buffer, szPath);
            std::string::size_type i = strPath.find_last_of('/');
            if (i != std::string::npos)
            {
                buffer[i] = 0;
                return buffer;
            }
            else
            {
                return strPath;
            }
        }
    }
    static std::string SetCurrentFolder(const char* szFilePath)
    {
        char szOldPath[512] = "";
        getcwd(szOldPath, 512);
        char szOldFullPath[512] = "";
        realpath(szOldPath, szOldFullPath);

        // printf("old %s\n",szOldFullPath);

        char szFullPath[512];
        realpath(szFilePath, szFullPath);

        // printf("full %s\n",szFullPath);

        std::string strDir = get_dir(szFullPath);

        // printf("full dir %s\n",strDir.c_str());

        chdir(strDir.c_str());

        // char szPath[512];
        // getcwd(szPath, 512);
        // printf("current %s\n",szPath);

        return szOldFullPath;
    }

    static std::string GetFileName(const char* path)
    {
        std::string strPath(path);
        std::string::size_type i = strPath.find_last_of('/');
        if (i != std::string::npos)
        {
            return strPath.substr(i + 1);
        }
        else
        {
            return strPath;
        }
    }

#endif

    static bool CheckFile(FILE* fp)
    {
        if (!fp)
            return false;

        char buffer[16 + 1];
        ri::ri_rib_decoder dec(fp);
        int nRet = dec.read(buffer, 16);
#ifdef HAVE_ZLIB
        gzrewind((gzFile)fp);
#else
        rewind(fp);
#endif
        return nRet > 0;
    }
}

namespace ri
{
    /*
        std::string ri_set_current_folder(const char* szFile)
        {
            return SetCurrentFolder(szFile);
        }
         */
    int load_rib(FILE* in, ri_context* ctx);

    int ri_read_archive(FILE* in, ri_context* ctx) { return load_rib(in, ctx); }
    int ri_read_archive(const char* szFile, ri_context* ctx)
    {
        FILE* fp = NULL;
#ifdef HAVE_ZLIB
        fp = (FILE*)gzopen(szFile, "rb");
#else
        fp = fopen(szFile, "rt");
#endif

        if (!fp)
            return -1;

        int nRet = 0;
        if (CheckFile(fp))
        {
            nRet = ri_read_archive(fp, ctx);
        }

// SetCurrentFolder(strOldPath.c_str());

#ifdef HAVE_ZLIB
        if (fp)
            gzclose((gzFile)fp);
#else
        if (fp)
            fclose(fp);
#endif
        return nRet;
    }

    std::string ri_read_archive_set_path(const char* path)
    {
        ri_path_resolver::set_rib_path(path);
        return SetCurrentFolder(path);
    }

    std::string ri_read_archive_get_filename(const char* path)
    {
        return GetFileName(path);
    }
}

#ifdef UNIT_TEST

int main()
{
    using namespace kaze;
    using namespace ri;

    std::unique_ptr<ri_composite_context> ctx(new ri_composite_context());
    ctx->add(new ri_stdout_context());
    ctx->add(new ri_frame_context());

    int nRet = load_rib(stdin, ctx.get());

    return 0;
}

#endif
