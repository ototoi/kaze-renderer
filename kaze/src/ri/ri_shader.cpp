#include "ri_shader.h"

namespace ri
{
    static int GetSize(int nClass, RtInt vertex = 1, RtInt varying = 1,
                       RtInt uniform = 1, RtInt facevarying = 1)
    {
        switch (nClass)
        {
        case ri_classifier::VERTEX:
            return vertex;
        case ri_classifier::UNIFORM:
            return uniform;
        case ri_classifier::CONSTANT:
            return 1;
        case ri_classifier::VARYING:
            return varying;
        case ri_classifier::FACEVARYING:
            return facevarying;
        }
        return 1;
    }

    static void AddParams(ri_parameters* p_params, ri_classifier* p_cls,
                          RtToken token, RtPointer param, RtInt vertex = 1,
                          RtInt varying = 1, RtInt uniform = 1,
                          RtInt facevarying = 1)
    {
        typedef const char* PCTR;

        int nID = p_cls->def(token);
        if (nID < 0)
            return;
        const ri_classifier::param_data& data = (*p_cls)[nID];
        int nSz = GetSize(data.get_class(), vertex, varying, uniform, facevarying);
        switch (data.get_type())
        {
        case ri_classifier::INTEGER:
            p_params->set(data.get_name(), (const RtInt*)param, data.get_size() * nSz);
            break;
        case ri_classifier::STRING:
            p_params->set(data.get_name(), (const PCTR*)param, data.get_size() * nSz);
            break;
        case ri_classifier::FLOAT:
        case ri_classifier::COLOR:
        case ri_classifier::POINT:
        case ri_classifier::VECTOR:
        case ri_classifier::NORMAL:
        case ri_classifier::HPOINT:
        case ri_classifier::MATRIX:
            p_params->set(data.get_name(), (const RtFloat*)param,
                          data.get_size() * nSz);
            break;
        default:
            break;
        }
    }

    static void AddParams(ri_parameters* p_params, ri_classifier* p_cls,
                          RtInt count, RtToken tokens[], RtPointer params[],
                          RtInt vertex = 1, RtInt varying = 1, RtInt uniform = 1,
                          RtInt facevarying = 1)
    {
        if (count)
        {
            for (int i = 0; i < count; i++)
            {
                AddParams(p_params, p_cls, tokens[i], params[i], vertex, varying, uniform,
                          facevarying);
            }
        }
    }
    //--------------------------------------------------------------------------------------------------

    ri_shader::ri_shader(const ri_classifier* p_cls, RtToken name, RtInt n,
                         RtToken tokens[], RtPointer params[])
        : clsf_(0), name_(name)
    {
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params);
        }
    }

    ri_shader::~ri_shader() {}
}
