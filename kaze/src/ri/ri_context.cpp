#include "ri_context.h"

#ifdef _WIN32
#pragma warning(disable : 4786)
#pragma warning(disable : 4102)
#pragma warning(disable : 4996)
#pragma warning(disable : 4065)
#endif

#include <cstdlib>
#include <cassert>
#include <vector>

#include <stdio.h>
#include <string.h>

using namespace std;

namespace
{
    class param_helper
    {
    public:
        param_helper(va_list args)
        {
            RtToken t;
            RtPointer p;
            while ((t = va_arg(args, RtToken)) != RI_NULL)
            {
                m_tokens.push_back(t);
                p = va_arg(args, RtPointer);
                m_params.push_back(p);
            }
        }
        RtToken* tokens() { return &m_tokens[0]; }
        RtPointer* params() { return &m_params[0]; }
        int size() const { return (int)m_tokens.size(); }

    private:
        std::vector<RtToken> m_tokens;
        std::vector<RtPointer> m_params;
    };

    struct PTMap
    {
        void* ptr;
        RtToken name;
    };

    static void* TokenToPointer(const struct PTMap MAP[], RtToken name)
    {
        int i = 0;
        while (MAP[i].ptr)
        {
            if (strcmp(MAP[i].name, name) == 0)
                return MAP[i].ptr;
            i++;
        }
        return NULL;
    }

    static RtToken PointerToToken(const struct PTMap MAP[], void* ptr)
    {
        int i = 0;
        while (MAP[i].ptr)
        {
            if (MAP[i].ptr == ptr)
                return MAP[i].name;
            i++;
        }
        return NULL;
    }

    static const struct PTMap FILTERS[] = {
        {(void*)RiGaussianFilter, RI_GAUSSIAN},
        {(void*)RiBoxFilter, RI_BOX},
        {(void*)RiTriangleFilter, RI_TRIANGLE},
        {(void*)RiCatmullRomFilter, "catmullrom"},
        {(void*)RiCatmullRomFilter, "catmull-rom"},
        {(void*)RiSincFilter, RI_SINC},
        {NULL, RI_NULL}};

    static struct PTMap ERROR_HANDLERS[] = {{(void*)RiErrorIgnore, RI_IGNORE},
                                            {(void*)RiErrorPrint, RI_PRINT},
                                            {(void*)RiErrorAbort, RI_ABORT},
                                            {NULL, RI_NULL}};

    static const struct PTMap BASISES[] = {
        {(void*)RiBezierBasis, RI_BEZIER},
        {(void*)RiBSplineBasis, "bspline"}, //
        {(void*)RiBSplineBasis, "b-spline"},
        {(void*)RiCatmullRomBasis, "catmull-rom"}, //
        {(void*)RiCatmullRomBasis, "catmullrom"},
        {(void*)RiHermiteBasis, RI_HERMITE},
        {(void*)RiPowerBasis, RI_POWER},
        {NULL, RI_NULL},
    };

    static struct PTMap PROCEDURALS[] = {
        {(void*)RiProcDelayedReadArchive, "DelayedReadArchive"},
        {(void*)RiProcRunProgram, "RunProgram"},
        {(void*)RiProcDynamicLoad, "DynamicLoad"},
        {NULL, RI_NULL}};

    static struct PTMap PROCEDURALS2[] = {
        {(void*)RiProc2DelayedReadArchive, "DelayedReadArchive"},
        {(void*)RiProc2RunProgram, "RunProgram"},
        {(void*)RiProc2DynamicLoad, "DynamicLoad"},
        {NULL, RI_NULL}};

    static struct PTMap BOUND2[] = {{(void*)RiSimpleBound, "SimpleBound"},
                                    {(void*)RiDSOBound, "DSOBound"},
                                    {NULL, RI_NULL}};

    RtToken BasisToToken(RtBasis b)
    {
        const RtFloat* f = (const RtFloat*)b;
        int i = 0;
        while (BASISES[i].ptr)
        {
            const RtFloat* fp = (const RtFloat*)(BASISES[i].ptr);
            if (memcmp(fp, f, sizeof(RtFloat) * 16) == 0)
                return BASISES[i].name;
            i++;
        }
        return RI_NULL;
    }
}

#ifdef _WIN32
#define snprintf _snprintf
#endif

namespace ri
{
    ri_context::ri_context()
    {
        p_classifier_ = new ri_classifier();
        p_lightmap_ = new ri_mapper();
        p_objectmap_ = new ri_mapper();
    }
    ri_context::ri_context(const ri_classifier& cls)
    {
        p_classifier_ = new ri_classifier(cls);
        p_lightmap_ = new ri_mapper();
        p_objectmap_ = new ri_mapper();
    }
    ri_context::~ri_context()
    {
        delete p_classifier_;
        delete p_lightmap_;
        delete p_objectmap_;
    }

    RtVoid ri_context::RiVersion(float version)
    {
        char buffer[64];
        snprintf(buffer, 64, "%1.2f", version);
        this->RiVersion(buffer);
    }
    RtVoid ri_context::RiVersion(char* version)
    {
        this->RiVersion((float)atof(version));
    }

    RtToken ri_context::RiDeclare(char* name, char* declaration)
    {
        std::string a = name;
        std::string b = declaration;

        std::string d = b + " " + a;
        this->RiGetClassifier()->def(d.c_str());
        return name;
    }
    RtVoid ri_context::RiBegin(RtToken name) { assert(0); }
    RtVoid ri_context::RiEnd(void) { assert(0); }
    RtVoid ri_context::RiFrameBegin(RtInt frame) { assert(0); }
    RtVoid ri_context::RiFrameEnd(void) { assert(0); }
    RtVoid ri_context::RiWorldBegin(void) { assert(0); }
    RtVoid ri_context::RiWorldEnd(void) { assert(0); }
    RtVoid ri_context::RiFormat(RtInt xres, RtInt yres, RtFloat aspect)
    {
        assert(0);
    }
    RtVoid ri_context::RiFrameAspectRatio(RtFloat aspect) { assert(0); }
    RtVoid ri_context::RiScreenWindow(RtFloat left, RtFloat right, RtFloat bot,
                                      RtFloat top)
    {
        assert(0);
    }
    RtVoid ri_context::RiCropWindow(RtFloat xmin, RtFloat xmax, RtFloat ymin,
                                    RtFloat ymax)
    {
        assert(0);
    }
    RtVoid ri_context::RiProjection(RtToken name, va_list argptr)
    {
        param_helper param(argptr);
        this->RiProjectionV(name, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiProjectionV(RtToken name, RtInt n, RtToken tokens[],
                                     RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiClipping(RtFloat hither, RtFloat yon) { assert(0); }
    RtVoid ri_context::RiClippingPlane(RtFloat x, RtFloat y, RtFloat z, RtFloat nx,
                                       RtFloat ny, RtFloat nz)
    {
        assert(0);
    }
    RtVoid ri_context::RiShutter(RtFloat min, RtFloat max) { assert(0); }
    RtVoid ri_context::RiPixelVariance(RtFloat variation) { assert(0); }
    RtVoid ri_context::RiPixelSamples(RtFloat xsamples, RtFloat ysamples)
    {
        assert(0);
    }

    RtVoid ri_context::RiPixelSampleImager(RtToken name, va_list argptr)
    {
        param_helper param(argptr);
        this->RiPixelSampleImagerV(name, param.size(), param.tokens(),
                                   param.params());
    }
    RtVoid ri_context::RiPixelSampleImagerV(RtToken name, RtInt n, RtToken tokens[],
                                            RtPointer params[])
    {
        assert(0);
    }

    RtVoid ri_context::RiPixelFilter(RtToken name, RtFloat xwidth, RtFloat ywidth)
    {
        RtFilterFunc pf = (RtFilterFunc)TokenToPointer(FILTERS, name);
        if (pf)
        {
            this->RiPixelFilter(pf, xwidth, ywidth);
        }
    }
    RtVoid ri_context::RiPixelFilter(RtFilterFunc filterfunc, RtFloat xwidth,
                                     RtFloat ywidth)
    {
        RtToken tk = (RtToken)PointerToToken(FILTERS, (void*)filterfunc);
        if (tk)
        {
            this->RiPixelFilter(tk, xwidth, ywidth);
        }
    }

    RtVoid ri_context::RiExposure(RtFloat gain, RtFloat gamma) { assert(0); }
    RtVoid ri_context::RiImager(RtToken name, va_list argptr)
    {
        param_helper param(argptr);
        this->RiImagerV(name, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiImagerV(RtToken name, RtInt n, RtToken tokens[],
                                 RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiQuantize(RtToken type, RtInt one, RtInt min, RtInt max,
                                  RtFloat ampl)
    {
        assert(0);
    }
    RtVoid ri_context::RiDisplay(char* name, RtToken type, RtToken mode,
                                 va_list argptr)
    {
        param_helper param(argptr);
        this->RiDisplayV(name, type, mode, param.size(), param.tokens(),
                         param.params());
    }
    RtVoid ri_context::RiDisplayV(char* name, RtToken type, RtToken mode, RtInt n,
                                  RtToken tokens[], RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiDisplayChannel(RtToken channel, va_list argptr)
    {
        param_helper param(argptr);
        this->RiDisplayChannelV(channel, param.size(), param.tokens(),
                                param.params());
    }
    RtVoid ri_context::RiDisplayChannelV(RtToken channel, RtInt n, RtToken tokens[],
                                         RtPointer params[])
    {
        ; //
    }

    RtVoid ri_context::RiHider(RtToken type, va_list argptr)
    {
        param_helper param(argptr);
        this->RiHiderV(type, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiHiderV(RtToken type, RtInt n, RtToken tokens[],
                                RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiColorSamples(RtInt n, RtFloat nRGB[], RtFloat RGBn[])
    {
        assert(0);
    }
    RtVoid ri_context::RiRelativeDetail(RtFloat relativedetail) { assert(0); }
    RtVoid ri_context::RiOption(RtToken name, va_list argptr) { assert(0); }
    RtVoid ri_context::RiOptionV(RtToken name, RtInt n, RtToken tokens[],
                                 RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiAttributeBegin(void) { assert(0); }
    RtVoid ri_context::RiAttributeEnd(void) { assert(0); }
    RtVoid ri_context::RiColor(RtColor color) { assert(0); }
    RtVoid ri_context::RiOpacity(RtColor color) { assert(0); }
    RtVoid ri_context::RiTextureCoordinates(RtFloat s1, RtFloat t1, RtFloat s2,
                                            RtFloat t2, RtFloat s3, RtFloat t3,
                                            RtFloat s4, RtFloat t4)
    {
        assert(0);
    }
    RtLightHandle ri_context::RiLightSource(RtToken name, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiLightSourceV(name, param.size(), param.tokens(),
                                    param.params());
    }
    RtLightHandle ri_context::RiLightSourceV(RtToken name, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        return RI_NULL;
    }
    RtLightHandle ri_context::RiAreaLightSource(RtToken name, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiAreaLightSourceV(name, param.size(), param.tokens(),
                                        param.params());
    }
    RtLightHandle ri_context::RiAreaLightSourceV(RtToken name, RtInt n,
                                                 RtToken tokens[],
                                                 RtPointer params[])
    {
        return RI_NULL;
    }

    RtVoid ri_context::RiIlluminate(RtInt id, RtBoolean onoff)
    {
        RtLightHandle light = this->RiGetLightHandle(id);
        if (light)
        {
            this->RiIlluminate(light, onoff);
        }
    }
    RtVoid ri_context::RiIlluminate(RtToken name, RtBoolean onoff)
    {
        RtLightHandle light = this->RiGetLightHandle(name);
        if (light)
        {
            this->RiIlluminate(light, onoff);
        }
    }
    RtVoid ri_context::RiIlluminate(RtLightHandle light, RtBoolean onoff)
    {
        RtInt id = this->RiGetLightID(light);
        if (id >= 0)
        {
            this->RiIlluminate(id, onoff);
        }
        else
        {
            RtToken token = this->RiGetLightName(light);
            if (token)
            {
                this->RiIlluminate(token, onoff);
            }
        }
    }

    RtVoid ri_context::RiSurface(RtToken name, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiSurfaceV(name, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiSurfaceV(RtToken name, RtInt n, RtToken tokens[],
                                  RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiAtmosphere(RtToken name, va_list argptr) { assert(0); }
    RtVoid ri_context::RiAtmosphereV(RtToken name, RtInt n, RtToken tokens[],
                                     RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiInterior(RtToken name, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiInteriorV(name, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiInteriorV(RtToken name, RtInt n, RtToken tokens[],
                                   RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiExterior(RtToken name, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiExteriorV(name, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiExteriorV(RtToken name, RtInt n, RtToken tokens[],
                                   RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiShadingRate(RtFloat size) { assert(0); }
    RtVoid ri_context::RiShadingInterpolation(RtToken type) { assert(0); }
    RtVoid ri_context::RiMatte(RtBoolean onoff) { assert(0); }
    RtVoid ri_context::RiBound(RtBound bound) { assert(0); }
    RtVoid ri_context::RiDetail(RtBound bound) { assert(0); }
    RtVoid ri_context::RiDetailRange(RtFloat minvis, RtFloat lowtran,
                                     RtFloat uptran, RtFloat maxvis)
    {
        assert(0);
    }
    RtVoid ri_context::RiGeometricApproximation(RtToken type, RtFloat value)
    {
        assert(0);
    }
    RtVoid ri_context::RiOrientation(RtToken orientation) { assert(0); }
    RtVoid ri_context::RiReverseOrientation(void) { assert(0); }
    RtVoid ri_context::RiSides(RtInt sides) { assert(0); }
    RtVoid ri_context::RiIdentity(void) { assert(0); }
    RtVoid ri_context::RiTransform(RtMatrix transform) { assert(0); }
    RtVoid ri_context::RiConcatTransform(RtMatrix transform) { assert(0); }
    RtVoid ri_context::RiPerspective(RtFloat fov) { assert(0); }
    RtVoid ri_context::RiTranslate(RtFloat dx, RtFloat dy, RtFloat dz)
    {
        assert(0);
    }
    RtVoid ri_context::RiRotate(RtFloat angle, RtFloat dx, RtFloat dy, RtFloat dz)
    {
        assert(0);
    }
    RtVoid ri_context::RiScale(RtFloat sx, RtFloat sy, RtFloat sz) { assert(0); }
    RtVoid ri_context::RiSkew(RtFloat angle, RtFloat dx1, RtFloat dy1, RtFloat dz1,
                              RtFloat dx2, RtFloat dy2, RtFloat dz2)
    {
        assert(0);
    }
    RtVoid ri_context::RiDeformation(RtToken name, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiDeformationV(name, param.size(), param.tokens(),
                                    param.params());
    }
    RtVoid ri_context::RiDeformationV(RtToken name, RtInt n, RtToken tokens[],
                                      RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiDisplacement(RtToken name, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiDisplacementV(name, param.size(), param.tokens(),
                                     param.params());
    }
    RtVoid ri_context::RiDisplacementV(RtToken name, RtInt n, RtToken tokens[],
                                       RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiCoordinateSystem(RtToken space) { assert(0); }
    RtVoid ri_context::RiScopedCoordinateSystem(RtToken space) { assert(0); }
    RtVoid ri_context::RiCoordSysTransform(RtToken space) { assert(0); }
    RtPoint* ri_context::RiTransformPoints(RtToken fromspace, RtToken tospace,
                                           RtInt n, RtPoint points[])
    {
        return NULL;
    }
    RtVoid ri_context::RiTransformBegin(void) { assert(0); }
    RtVoid ri_context::RiTransformEnd(void) { assert(0); }

    RtVoid ri_context::RiResource(RtToken handle, RtToken type, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiResourceV(handle, type, param.size(), param.tokens(),
                                 param.params());
    }
    RtVoid ri_context::RiResourceV(RtToken handle, RtToken type, RtInt n,
                                   RtToken tokens[], RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiResourceBegin(void) { assert(0); }
    RtVoid ri_context::RiResourceEnd(void) { assert(0); }

    RtVoid ri_context::RiAttribute(RtToken name, va_list argptr) { assert(0); }
    RtVoid ri_context::RiAttributeV(RtToken name, RtInt n, RtToken tokens[],
                                    RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiPolygon(RtInt nverts, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiPolygonV(nverts, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiPolygonV(RtInt nverts, RtInt n, RtToken tokens[],
                                  RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiGeneralPolygon(RtInt nloops, RtInt nverts[],
                                        va_list argptr)
    {
        param_helper param(argptr);
        return this->RiGeneralPolygonV(nloops, nverts, param.size(), param.tokens(),
                                       param.params());
    }
    RtVoid ri_context::RiGeneralPolygonV(RtInt nloops, RtInt nverts[], RtInt n,
                                         RtToken tokens[], RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiPointsPolygons(RtInt npolys, RtInt nverts[], RtInt verts[],
                                        va_list argptr)
    {
        param_helper param(argptr);
        return this->RiPointsPolygonsV(npolys, nverts, verts, param.size(),
                                       param.tokens(), param.params());
    }
    RtVoid ri_context::RiPointsPolygonsV(RtInt npolys, RtInt nverts[],
                                         RtInt verts[], RtInt n, RtToken tokens[],
                                         RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiPointsGeneralPolygons(RtInt npolys, RtInt nloops[],
                                               RtInt nverts[], RtInt verts[],
                                               va_list argptr)
    {
        param_helper param(argptr);
        return this->RiPointsGeneralPolygonsV(npolys, nloops, verts, verts,
                                              param.size(), param.tokens(),
                                              param.params());
    }
    RtVoid ri_context::RiPointsGeneralPolygonsV(RtInt npolys, RtInt nloops[],
                                                RtInt nverts[], RtInt verts[],
                                                RtInt n, RtToken tokens[],
                                                RtPointer params[])
    {
        assert(0);
    }

    RtVoid ri_context::RiBasis(RtToken ubasis, RtInt ustep, RtToken vbasis,
                               RtInt vstep)
    {
        RtBasis ub = {};
        RtBasis vb = {};
        void* up = TokenToPointer(BASISES, ubasis);
        void* vp = TokenToPointer(BASISES, vbasis);
        if (up)
            memcpy(ub, up, sizeof(RtBasis));
        if (vp)
            memcpy(vb, vp, sizeof(RtBasis));
        this->RiBasis(ub, ustep, vb, vstep);
    }
    RtVoid ri_context::RiBasis(RtBasis ubasis, RtInt ustep, RtToken vbasis,
                               RtInt vstep) // basis token
    {
        // RtBasis ub = {};
        RtBasis vb = {};
        // void* up = TokenToPointer(BASISES, ubasis);
        void* vp = TokenToPointer(BASISES, vbasis);
        // if(up)memcpy(ub, up, sizeof(RtBasis));
        if (vp)
            memcpy(vb, vp, sizeof(RtBasis));
        this->RiBasis(ubasis, ustep, vb, vstep);
    }
    RtVoid ri_context::RiBasis(RtToken ubasis, RtInt ustep, RtBasis vbasis,
                               RtInt vstep) // token basis
    {
        RtBasis ub = {};
        // RtBasis vb = {};
        void* up = TokenToPointer(BASISES, ubasis);
        // void* vp = TokenToPointer(BASISES, vbasis);
        if (up)
            memcpy(ub, up, sizeof(RtBasis));
        // if(vp)memcpy(vb, vp, sizeof(RtBasis));
        this->RiBasis(ub, ustep, vbasis, vstep);
    }
    RtVoid ri_context::RiBasis(RtBasis ubasis, RtInt ustep, RtBasis vbasis,
                               RtInt vstep)
    {
        RtToken ub = (RtToken)BasisToToken(ubasis);
        RtToken vb = (RtToken)BasisToToken(vbasis);
        this->RiBasis(ub, ustep, vb, vstep);
    }

    RtVoid ri_context::RiPatch(RtToken type, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiPatchV(type, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiPatchV(RtToken type, RtInt n, RtToken tokens[],
                                RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiPatchMesh(RtToken type, RtInt nu, RtToken uwrap, RtInt nv,
                                   RtToken vwrap, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiPatchMeshV(type, nu, uwrap, nv, vwrap, param.size(),
                                  param.tokens(), param.params());
    }
    RtVoid ri_context::RiPatchMeshV(RtToken type, RtInt nu, RtToken uwrap, RtInt nv,
                                    RtToken vwrap, RtInt n, RtToken tokens[],
                                    RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiNuPatch(RtInt nu, RtInt uorder, RtFloat uknot[],
                                 RtFloat umin, RtFloat umax, RtInt nv, RtInt vorder,
                                 RtFloat vknot[], RtFloat vmin, RtFloat vmax,
                                 va_list argptr)
    {
        param_helper param(argptr);
        return this->RiNuPatchV(nu, uorder, uknot, umin, umax, nv, vorder, vknot,
                                vmin, vmax, param.size(), param.tokens(),
                                param.params());
    }
    RtVoid ri_context::RiNuPatchV(RtInt nu, RtInt uorder, RtFloat uknot[],
                                  RtFloat umin, RtFloat umax, RtInt nv,
                                  RtInt vorder, RtFloat vknot[], RtFloat vmin,
                                  RtFloat vmax, RtInt n, RtToken tokens[],
                                  RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiTrimCurve(RtInt nloops, RtInt ncurves[], RtInt order[],
                                   RtFloat knot[], RtFloat min[], RtFloat max[],
                                   RtInt n[], RtFloat u[], RtFloat v[],
                                   RtFloat w[])
    {
        assert(0);
    }
    RtVoid ri_context::RiSphere(RtFloat radius, RtFloat zmin, RtFloat zmax,
                                RtFloat tmax, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiSphereV(radius, zmin, zmax, tmax, param.size(), param.tokens(),
                               param.params());
    }
    RtVoid ri_context::RiSphereV(RtFloat radius, RtFloat zmin, RtFloat zmax,
                                 RtFloat tmax, RtInt n, RtToken tokens[],
                                 RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiCone(RtFloat height, RtFloat radius, RtFloat tmax,
                              va_list argptr)
    {
        param_helper param(argptr);
        return this->RiConeV(height, radius, tmax, param.size(), param.tokens(),
                             param.params());
    }
    RtVoid ri_context::RiConeV(RtFloat height, RtFloat radius, RtFloat tmax,
                               RtInt n, RtToken tokens[], RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiCylinder(RtFloat radius, RtFloat zmin, RtFloat zmax,
                                  RtFloat tmax, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiCylinderV(radius, zmin, zmax, tmax, param.size(),
                                 param.tokens(), param.params());
    }
    RtVoid ri_context::RiCylinderV(RtFloat radius, RtFloat zmin, RtFloat zmax,
                                   RtFloat tmax, RtInt n, RtToken tokens[],
                                   RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiHyperboloid(RtPoint point1, RtPoint point2, RtFloat tmax,
                                     va_list argptr)
    {
        param_helper param(argptr);
        return this->RiHyperboloidV(point1, point2, tmax, param.size(),
                                    param.tokens(), param.params());
    }
    RtVoid ri_context::RiHyperboloidV(RtPoint point1, RtPoint point2, RtFloat tmax,
                                      RtInt n, RtToken tokens[],
                                      RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiParaboloid(RtFloat rmax, RtFloat zmin, RtFloat zmax,
                                    RtFloat tmax, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiParaboloidV(rmax, zmin, zmax, tmax, param.size(),
                                   param.tokens(), param.params());
    }
    RtVoid ri_context::RiParaboloidV(RtFloat rmax, RtFloat zmin, RtFloat zmax,
                                     RtFloat tmax, RtInt n, RtToken tokens[],
                                     RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiDisk(RtFloat height, RtFloat radius, RtFloat tmax,
                              va_list argptr)
    {
        param_helper param(argptr);
        return this->RiDiskV(height, radius, tmax, param.size(), param.tokens(),
                             param.params());
    }
    RtVoid ri_context::RiDiskV(RtFloat height, RtFloat radius, RtFloat tmax,
                               RtInt n, RtToken tokens[], RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiTorus(RtFloat majrad, RtFloat minrad, RtFloat phimin,
                               RtFloat phimax, RtFloat tmax, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiTorusV(majrad, minrad, phimin, phimax, tmax, param.size(),
                              param.tokens(), param.params());
    }
    RtVoid ri_context::RiTorusV(RtFloat majrad, RtFloat minrad, RtFloat phimin,
                                RtFloat phimax, RtFloat tmax, RtInt n,
                                RtToken tokens[], RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiBlobby(RtInt nleaf, RtInt ncode, RtInt code[], RtInt nflt,
                                RtFloat flt[], RtInt nstr, RtToken str[],
                                va_list argptr)
    {
        param_helper param(argptr);
        return this->RiBlobbyV(nleaf, ncode, code, nflt, flt, nstr, str, param.size(),
                               param.tokens(), param.params());
    }
    RtVoid ri_context::RiBlobbyV(RtInt nleaf, RtInt ncode, RtInt code[], RtInt nflt,
                                 RtFloat flt[], RtInt nstr, RtToken str[], RtInt n,
                                 RtToken tokens[], RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiCurves(RtToken type, RtInt ncurves, RtInt nvertices[],
                                RtToken wrap, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiCurvesV(type, ncurves, nvertices, wrap, param.size(),
                               param.tokens(), param.params());
    }
    RtVoid ri_context::RiCurvesV(RtToken type, RtInt ncurves, RtInt nvertices[],
                                 RtToken wrap, RtInt n, RtToken tokens[],
                                 RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiPoints(RtInt nverts, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiPointsV(nverts, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiPointsV(RtInt nverts, RtInt n, RtToken tokens[],
                                 RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiSubdivisionMesh(RtToken mask, RtInt nf, RtInt nverts[],
                                         RtInt verts[], RtInt ntags, RtToken tags[],
                                         RtInt nargs[], RtInt intargs[],
                                         RtFloat floatargs[], va_list argptr)
    {
        param_helper param(argptr);
        return this->RiSubdivisionMeshV(mask, nf, nverts, verts, ntags, tags, nargs,
                                        intargs, floatargs, param.size(),
                                        param.tokens(), param.params());
    }
    RtVoid ri_context::RiSubdivisionMeshV(RtToken mask, RtInt nf, RtInt nverts[],
                                          RtInt verts[], RtInt ntags,
                                          RtToken tags[], RtInt nargs[],
                                          RtInt intargs[], RtFloat floatargs[],
                                          RtInt n, RtToken tokens[],
                                          RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiHierarchicalSubdivisionMesh(
        RtToken scheme, RtInt nfaces, RtInt nvertices[], RtInt vertices[],
        RtInt ntags, RtToken tags[], RtInt nargs[], RtInt intargs[],
        RtFloat floatargs[], RtString stringargs[], va_list argptr)
    {
        param_helper param(argptr);
        return this->RiHierarchicalSubdivisionMeshV(
            scheme, nfaces, nvertices, vertices, ntags, tags, nargs, intargs,
            floatargs, stringargs, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiHierarchicalSubdivisionMeshV(
        RtToken scheme, RtInt nfaces, RtInt nvertices[], RtInt vertices[],
        RtInt ntags, RtToken tags[], RtInt nargs[], RtInt intargs[],
        RtFloat floatargs[], RtString stringargs[], RtInt n, RtToken tokens[],
        RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiProcedural(RtPointer data, RtBound bound,
                                    RtVoid (*subdivfunc)(RtPointer, RtFloat),
                                    RtVoid (*freefunc)(RtPointer))
    {
        static const int PROCEDURALS_TOKEN_COUNT[] = {1, 2, 2};

        int nFind = -1;
        {
            int i = 0;
            while (PROCEDURALS[i].ptr)
            {
                if (subdivfunc == PROCEDURALS[i].ptr)
                {
                    nFind = i;
                    break;
                }
            }
        }
        if (nFind >= 0)
        {
            RtToken type = PROCEDURALS[nFind].name;
            if (type)
            {
                this->RiProcedural(type, PROCEDURALS_TOKEN_COUNT[nFind], (RtToken*)data,
                                   bound);
            }
        }
    }
    RtVoid ri_context::RiProcedural(RtToken type, RtInt n, RtToken tokens[],
                                    RtBound bound)
    {
        RtProcSubdivFunc pf = (RtProcSubdivFunc)TokenToPointer(PROCEDURALS, type);
        if (pf)
        {
            this->RiProcedural(tokens, bound, pf, NULL);
        }
    }
    RtVoid ri_context::RiProceduralV(RtToken type, RtInt n0, RtToken tokens0[],
                                     RtBound bound, RtInt n, RtToken tokens[],
                                     RtPointer params[])
    {
        return this->RiProcedural(type, n0, tokens0, bound);
    }
    RtVoid ri_context::RiGeometry(RtToken type, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiGeometryV(type, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiGeometryV(RtToken type, RtInt n, RtToken tokens[],
                                   RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiSolidBegin(RtToken operation) { assert(0); }
    RtVoid ri_context::RiSolidEnd(void) { assert(0); }
    RtObjectHandle ri_context::RiObjectBegin(void) { return NULL; }
    RtVoid ri_context::RiObjectEnd(void) { assert(0); }

    RtVoid ri_context::RiObjectInstance(RtInt id)
    {
        RtObjectHandle handle = this->RiGetObjectHandle(id);
        if (handle)
        {
            this->RiObjectInstance(handle);
        }
    }
    RtVoid ri_context::RiObjectInstance(RtToken name)
    {
        RtObjectHandle handle = this->RiGetObjectHandle(name);
        if (handle)
        {
            this->RiObjectInstance(handle);
        }
    }
    RtVoid ri_context::RiObjectInstance(RtObjectHandle handle)
    {
        RtInt id = this->RiGetObjectID(handle);
        if (id >= 0)
        {
            this->RiObjectInstance(id);
        }
        else
        {
            RtToken token = this->RiGetObjectName(handle);
            if (token)
            {
                this->RiObjectInstance(token);
            }
        }
    }

    RtVoid ri_context::RiMotionBegin(RtInt n, va_list argptr)
    {
        std::vector<RtFloat> times;
        RtFloat t;
        for (int i = 0; i < n; i++)
        {
            t = (RtFloat)va_arg(argptr, double);
            times.push_back(t);
        }
        return this->RiMotionBeginV(n, &times[0]);
    }
    RtVoid ri_context::RiMotionBeginV(RtInt n, RtFloat times[]) { assert(0); }
    RtVoid ri_context::RiMotionEnd(void) { assert(0); }

    RtVoid ri_context::RiMakeTexture(char* pic, char* tex, RtToken swrap,
                                     RtToken twrap, RtFilterFunc filterfunc,
                                     RtFloat swidth, RtFloat twidth,
                                     va_list argptr)
    {
        param_helper param(argptr);
        this->RiMakeTextureV(pic, tex, swrap, twrap, filterfunc, swidth, twidth,
                             param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiMakeTextureV(char* pic, char* tex, RtToken swrap,
                                      RtToken twrap, RtFilterFunc filterfunc,
                                      RtFloat swidth, RtFloat twidth, RtInt n,
                                      RtToken tokens[], RtPointer params[])
    {
        RtToken ff = RiGetFilterFunc(filterfunc);
        return this->RiMakeTextureV(pic, tex, swrap, twrap, ff, swidth,
                                    twidth, n, tokens, params);
    }
    RtVoid ri_context::RiMakeTextureV(char* pic, char* tex, RtToken swrap,
                                      RtToken twrap, RtToken filterfunc,
                                      RtFloat swidth, RtFloat twidth, RtInt n,
                                      RtToken tokens[], RtPointer params[])
    {
        RtFilterFunc ff = RiGetFilterFunc(filterfunc);
        return this->RiMakeTextureV(pic, tex, swrap, twrap, ff, swidth,
                                    twidth, n, tokens, params);
    }

    RtVoid ri_context::RiMakeBump(char* pic, char* tex, RtToken swrap,
                                  RtToken twrap, RtFilterFunc filterfunc,
                                  RtFloat swidth, RtFloat twidth, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiMakeBumpV(pic, tex, swrap, twrap, filterfunc, swidth, twidth,
                                 param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiMakeBumpV(char* pic, char* tex, RtToken swrap,
                                   RtToken twrap, RtFilterFunc filterfunc,
                                   RtFloat swidth, RtFloat twidth, RtInt n,
                                   RtToken tokens[], RtPointer params[])
    {
        RtToken ff = RiGetFilterFunc(filterfunc);
        return this->RiMakeBumpV(pic, tex, swrap, twrap, ff, swidth, twidth, n,
                                 tokens, params);
    }
    RtVoid ri_context::RiMakeBumpV(char* pic, char* tex, RtToken swrap,
                                   RtToken twrap, RtToken filterfunc,
                                   RtFloat swidth, RtFloat twidth, RtInt n,
                                   RtToken tokens[], RtPointer params[])
    {
        RtFilterFunc ff = RiGetFilterFunc(filterfunc);
        return this->RiMakeBumpV(pic, tex, swrap, twrap, ff, swidth, twidth, n,
                                 tokens, params);
    }

    RtVoid ri_context::RiMakeLatLongEnvironment(char* pic, char* tex,
                                                RtFilterFunc filterfunc,
                                                RtFloat swidth, RtFloat twidth,
                                                va_list argptr)
    {
        param_helper param(argptr);
        return this->RiMakeLatLongEnvironmentV(pic, tex, filterfunc, swidth, twidth,
                                               param.size(), param.tokens(),
                                               param.params());
    }
    RtVoid ri_context::RiMakeLatLongEnvironmentV(char* pic, char* tex,
                                                 RtFilterFunc filterfunc,
                                                 RtFloat swidth, RtFloat twidth,
                                                 RtInt n, RtToken tokens[],
                                                 RtPointer params[])
    {
        RtToken ff = RiGetFilterFunc(filterfunc);
        return this->RiMakeLatLongEnvironmentV(pic, tex, ff, swidth, twidth, n,
                                               tokens, params);
    }
    RtVoid ri_context::RiMakeLatLongEnvironmentV(char* pic, char* tex,
                                                 RtToken filterfunc, RtFloat swidth,
                                                 RtFloat twidth, RtInt n,
                                                 RtToken tokens[],
                                                 RtPointer params[])
    {
        RtFilterFunc ff = RiGetFilterFunc(filterfunc);
        return this->RiMakeLatLongEnvironmentV(pic, tex, ff, swidth, twidth, n,
                                               tokens, params);
    }

    RtVoid ri_context::RiMakeCubeFaceEnvironment(char* px, char* nx, char* py,
                                                 char* ny, char* pz, char* nz,
                                                 char* tex, RtFloat fov,
                                                 RtFilterFunc filterfunc,
                                                 RtFloat swidth, RtFloat twidth,
                                                 va_list argptr)
    {
        param_helper param(argptr);
        return this->RiMakeCubeFaceEnvironmentV(
            px, nx, py, ny, pz, nz, tex, fov, filterfunc, swidth, twidth,
            param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiMakeCubeFaceEnvironmentV(
        char* px, char* nx, char* py, char* ny, char* pz, char* nz, char* tex,
        RtFloat fov, RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth,
        RtInt n, RtToken tokens[], RtPointer params[])
    {
        RtToken ff = RiGetFilterFunc(filterfunc);
        return this->RiMakeCubeFaceEnvironmentV(px, nx, py, ny, pz, nz, tex, fov, ff,
                                                swidth, twidth, n, tokens, params);
    }
    RtVoid ri_context::RiMakeCubeFaceEnvironmentV(
        char* px, char* nx, char* py, char* ny, char* pz, char* nz, char* tex,
        RtFloat fov, RtToken filterfunc, RtFloat swidth, RtFloat twidth, RtInt n,
        RtToken tokens[], RtPointer params[])
    {
        RtFilterFunc ff = RiGetFilterFunc(filterfunc);
        return this->RiMakeCubeFaceEnvironmentV(px, nx, py, ny, pz, nz, tex, fov, ff,
                                                swidth, twidth, n, tokens, params);
    }

    RtVoid ri_context::RiMakeShadow(char* pic, char* tex, va_list argptr)
    {
        param_helper param(argptr);
        return this->RiMakeShadowV(pic, tex, param.size(), param.tokens(),
                                   param.params());
    }
    RtVoid ri_context::RiMakeShadowV(char* pic, char* tex, RtInt n,
                                     RtToken tokens[], RtPointer params[])
    {
        assert(0);
    }

    RtVoid ri_context::RiMakeBrickMap(int nptc, char** ptcnames, char* bkmname,
                                      va_list argptr)
    {
        param_helper param(argptr);
        return this->RiMakeBrickMapV(nptc, ptcnames, bkmname, param.size(),
                                     param.tokens(), param.params());
    }
    RtVoid ri_context::RiMakeBrickMapV(int nptc, char** ptcnames, char* bkmname,
                                       RtInt n, RtToken tokens[],
                                       RtPointer params[])
    {
        assert(0);
    }

    RtVoid ri_context::RiArchiveRecord(RtToken type, char* format, va_list argptr)
    {
        ;
    }
    RtVoid ri_context::RiReadArchive(RtToken name, RtArchiveCallback callback,
                                     va_list argptr)
    {
        param_helper param(argptr);
        return this->RiReadArchiveV(name, callback, param.size(), param.tokens(),
                                    param.params());
    }
    RtVoid ri_context::RiReadArchiveV(RtToken name, RtArchiveCallback callback,
                                      RtInt n, RtToken tokens[],
                                      RtPointer params[])
    {
        ; //
    }

    RtArchiveHandle ri_context::RiArchiveBegin(RtToken archivename,
                                               va_list argptr)
    {
        param_helper param(argptr);
        return this->RiArchiveBeginV(archivename, param.size(), param.tokens(),
                                     param.params());
    }
    RtArchiveHandle ri_context::RiArchiveBeginV(RtToken archivename, RtInt n,
                                                RtToken tokens[],
                                                RtPointer params[])
    {
        return RI_NULL;
    }
    RtVoid ri_context::RiArchiveEnd(void)
    {
        ; //
    }

    RtVoid ri_context::RiErrorHandler(RtToken name)
    {
        RtErrorHandler eh = (RtErrorHandler)TokenToPointer(ERROR_HANDLERS, name);
        if (eh)
        {
            this->RiErrorHandler(eh);
        }
    }
    RtVoid ri_context::RiErrorHandler(RtErrorHandler handler)
    {
        RtToken eh = (RtToken)PointerToToken(ERROR_HANDLERS, (void*)handler);
        if (eh)
        {
            this->RiErrorHandler(eh);
        }
    }

    //----------------------------------------------------------------

    RtVoid ri_context::RiDepthOfField(RtFloat fstop, RtFloat focallength,
                                      RtFloat focaldistance) {}

    RtVoid ri_context::RiMakeOcclusion(RtInt npics, RtString* picfile,
                                       RtString shadowfile, va_list argptr)
    {
        param_helper param(argptr);
        this->RiMakeOcclusionV(npics, picfile, shadowfile, param.size(),
                               param.tokens(), param.params());
    }
    RtVoid ri_context::RiMakeOcclusionV(RtInt npics, RtString* picfile,
                                        RtString shadowfile, RtInt count,
                                        RtToken tokens[], RtPointer params[])
    {
        assert(0);
    }

    RtVoid ri_context::RiShaderLayer(RtToken type, RtToken name, RtToken layername,
                                     va_list argptr)
    {
        param_helper param(argptr);
        this->RiShaderLayerV(type, name, layername, param.size(), param.tokens(),
                             param.params());
    }
    RtVoid ri_context::RiShaderLayerV(RtToken type, RtToken name, RtToken layername,
                                      RtInt count, RtToken tokens[],
                                      RtPointer values[])
    {
        assert(0);
    }
    RtVoid ri_context::RiConnectShaderLayers(RtToken type, RtToken layer1,
                                             RtToken variable1, RtToken layer2,
                                             RtToken variable2)
    {
        assert(0);
    }

    RtVoid ri_context::RiIfBegin(RtString condition) { assert(0); }
    RtVoid ri_context::RiIfEnd() { assert(0); }
    RtVoid ri_context::RiElse() { assert(0); }
    RtVoid ri_context::RiElseIf(RtString condition) { assert(0); }

    RtVoid ri_context::RiProcedural2(RtToken subdivide2func, RtToken boundfunc,
                                     RtInt n, RtToken tokens[],
                                     RtPointer params[])
    {
        RtProc2SubdivFunc psf =
            (RtProc2SubdivFunc)TokenToPointer(PROCEDURALS2, subdivide2func);
        RtProc2BoundFunc pbf = (RtProc2BoundFunc)TokenToPointer(BOUND2, boundfunc);
        if (psf && pbf)
        {
            this->RiProcedural2(psf, pbf, n, tokens, params);
        }
    }

    RtVoid ri_context::RiProcedural2(RtProc2SubdivFunc subdivide2func,
                                     RtProc2BoundFunc boundfunc, RtInt n,
                                     RtToken tokens[], RtPointer params[])
    {
        RtToken psf = (RtToken)PointerToToken(PROCEDURALS2, (void*)subdivide2func);
        RtToken pbf = (RtToken)PointerToToken(BOUND2, (void*)boundfunc);
        if (psf && pbf)
        {
            this->RiProcedural2(psf, pbf, n, tokens, params);
        }
    }
    RtVoid ri_context::RiSystem(RtString cmd) { assert(0); }

    RtVoid ri_context::RiVolume(RtPointer type, RtBound bounds, RtInt nvertices[])
    {
        assert(0);
    }
    RtVoid ri_context::RiVolumePixelSamples(RtFloat xsamples, RtFloat ysamples)
    {
        assert(0);
    }
    RtVoid ri_context::RiVPAtmosphere(RtToken shadername, va_list argptr)
    {
        param_helper param(argptr);
        this->RiVPAtmosphereV(shadername, param.size(), param.tokens(),
                              param.params());
    }
    RtVoid ri_context::RiVPAtmosphereV(RtToken shadername, RtInt n,
                                       RtToken tokens[], RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiVPInterior(RtToken shadername, va_list argptr)
    {
        param_helper param(argptr);
        this->RiVPInteriorV(shadername, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiVPInteriorV(RtToken shadername, RtInt n, RtToken tokens[],
                                     RtPointer params[])
    {
        assert(0);
    }
    RtVoid ri_context::RiVPSurface(RtToken shadername, va_list argptr)
    {
        param_helper param(argptr);
        this->RiVPSurfaceV(shadername, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiVPSurfaceV(RtToken shadername, RtInt n, RtToken tokens[],
                                    RtPointer params[])
    {
        assert(0);
    }

    RtVoid ri_context::RiInstancer(RtToken name, va_list argptr)
    {
        param_helper param(argptr);
        this->RiInstancerV(name, param.size(), param.tokens(), param.params());
    }
    RtVoid ri_context::RiInstancerV(RtToken name, RtInt n, RtToken tokens[],
                                    RtPointer params[])
    {
        (void)0;
    }

    RtVoid ri_context::RiProcedureV(RtToken name, RtBound bound, RtInt n,
                                    RtToken tokens[], RtPointer params[])
    {
        (void)0;
    }

    RtVoid ri_context::RiIntegratorV(RtToken name, RtToken handle, RtInt n,
                                     RtToken tokens[], RtPointer params[])
    {
        (void)0;
    }

    RtVoid ri_context::RiBxdfV(RtToken type, RtToken name, RtInt n,
                               RtToken tokens[], RtPointer params[])
    {
        (void)0;
    }

    RtVoid ri_context::RiPatternV(RtToken type, RtToken name, RtInt n,
                                  RtToken tokens[], RtPointer params[])
    {
        (void)0;
    }

    //----------------------------------------------------------------
    RtFilterFunc ri_context::RiGetFilterFunc(RtToken name)
    {
        return (RtFilterFunc)TokenToPointer(FILTERS, name);
    }
    RtToken ri_context::RiGetFilterFunc(RtFilterFunc func)
    {
        return (RtToken)PointerToToken(FILTERS, (void*)func);
    }

    RtLightHandle ri_context::RiGetLightHandle(RtInt id)
    {
        return p_lightmap_->get_ptr(id);
    }
    RtLightHandle ri_context::RiGetLightHandle(RtToken name)
    {
        return p_lightmap_->get_ptr(name);
    }
    RtToken ri_context::RiGetLightName(RtLightHandle light)
    {
        return const_cast<RtToken>(p_lightmap_->get_str(light));
    }
    RtInt ri_context::RiGetLightID(RtLightHandle light)
    {
        return p_lightmap_->get_int(light);
    }
    RtVoid ri_context::RiSetLightHandle(RtInt id, RtLightHandle light)
    {
        p_lightmap_->set(id, light);
    }
    RtVoid ri_context::RiSetLightHandle(RtToken name, RtLightHandle light)
    {
        p_lightmap_->set(name, light);
    }

    RtObjectHandle ri_context::RiGetObjectHandle(RtInt id)
    {
        return p_objectmap_->get_ptr(id);
    }
    RtObjectHandle ri_context::RiGetObjectHandle(RtToken name)
    {
        return p_objectmap_->get_ptr(name);
    }
    RtToken ri_context::RiGetObjectName(RtObjectHandle obj)
    {
        return const_cast<RtToken>(p_objectmap_->get_str(obj));
    }
    RtInt ri_context::RiGetObjectID(RtObjectHandle obj)
    {
        return p_objectmap_->get_int(obj);
    }
    RtVoid ri_context::RiSetObjectHandle(RtInt id, RtObjectHandle obj)
    {
        p_objectmap_->set(id, obj);
    }
    RtVoid ri_context::RiSetObjectHandle(RtToken name, RtObjectHandle obj)
    {
        p_objectmap_->set(name, obj);
    }

    ri_classifier* ri_context::RiGetClassifier() { return p_classifier_; }
    const ri_classifier* ri_context::RiGetClassifier() const
    {
        return p_classifier_;
    }
}
