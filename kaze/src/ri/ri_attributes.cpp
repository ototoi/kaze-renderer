#include "ri_attributes.h"
#include "ri_parameters.h"
#include "ri_light_source.h"
#include "ri_coordinate_system.h"
#include "ri_geometry.h"

#include "ri.h"

namespace ri
{
    typedef const char* LPCSTR;

    static std::string get_key(const char* key1, const char* key2)
    {
        std::string str = key1;
        str += ':';
        str += key2;
        return str;
    }

#define INIT_PARAM(KEY1, KEY2, VAL) para->set(get_key(KEY1, KEY2).c_str(), VAL)
#define INIT_PARAM_A(KEY1, KEY2, VAL, N) \
    para->set(get_key(KEY1, KEY2).c_str(), VAL, N)

    static RtFloat colWhite[] = {1.0f, 1.0f, 1.0f};
    static RtFloat texUnit[] = {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f};

    static void init_parameters(ri_parameters* para)
    {
        // Shading ri_attributes

        INIT_PARAM_A("Color", "color", colWhite, 3);
        INIT_PARAM_A("Opacity", "color", colWhite, 3);

        INIT_PARAM("TextureCoordinates", "s1", texUnit[0]);
        INIT_PARAM("TextureCoordinates", "t1", texUnit[1]);
        INIT_PARAM("TextureCoordinates", "s2", texUnit[2]);
        INIT_PARAM("TextureCoordinates", "t2", texUnit[3]);
        INIT_PARAM("TextureCoordinates", "s3", texUnit[4]);
        INIT_PARAM("TextureCoordinates", "t3", texUnit[5]);
        INIT_PARAM("TextureCoordinates", "s4", texUnit[6]);
        INIT_PARAM("TextureCoordinates", "t4", texUnit[7]);

        // Light
        // Surface
        INIT_PARAM("Surface", "name", "nil");
        // Displacement
        INIT_PARAM("Displacement", "name", "nil");
        // Atmosphere
        INIT_PARAM("Atmosphere", "name", "nil");
        // Interior
        INIT_PARAM("Interior", "name", "nil");
        // Exterior
        INIT_PARAM("Exterior", "name", "nil");

        INIT_PARAM("ShadingRate", "size", 1.0f);

        INIT_PARAM("ShadingInterpolation", "type", RI_CONSTANT); // RI_SMOOTH,RI_CONSTANT
        INIT_PARAM("ShadingInterpolation", "smooth_angle", 180.0f);

        INIT_PARAM("Matte", "onoff", 0); // false
        INIT_PARAM("Orientation", "orientation", RI_LH);

        //
        INIT_PARAM("Basis", "ubasis", RI_BEZIER);
        INIT_PARAM("Basis", "ustep", 3);
        INIT_PARAM("Basis", "vbasis", RI_BEZIER);
        INIT_PARAM("Basis", "vstep", 3);
    }

    bool ri_attributes::set(const char* key1, const char* key2, int val)
    {
        std::string str = get_key(key1, key2);
        return map_->set(str.c_str(), val);
    }
    bool ri_attributes::set(const char* key1, const char* key2, float val)
    {
        std::string str = get_key(key1, key2);
        return map_->set(str.c_str(), val);
    }
    bool ri_attributes::set(const char* key1, const char* key2, const char* val)
    {
        std::string str = get_key(key1, key2);
        return map_->set(str.c_str(), val);
    }
    bool ri_attributes::set(const char* key1, const char* key2, const int* val,
                            int n)
    {
        std::string str = get_key(key1, key2);
        return map_->set(str.c_str(), val, n);
    }
    bool ri_attributes::set(const char* key1, const char* key2, const float* val,
                            int n)
    {
        std::string str = get_key(key1, key2);
        return map_->set(str.c_str(), val, n);
    }
    bool ri_attributes::set(const char* key1, const char* key2, const LPCSTR* val,
                            int n)
    {
        std::string str = get_key(key1, key2);
        return map_->set(str.c_str(), val, n);
    }

    //------------------------------------------------------
    bool ri_attributes::set_surface(const ri_shader* s)
    {
        surface_.clear();
        if (s)
        {
            surface_.push_back(s);
            this->set("Surface", "name", s->get_name().c_str());
        }
        else
        {
            this->set("Surface", "name", "nil");
        }
        return true;
    }
    bool ri_attributes::del_surface()
    {
        surface_.clear();
        this->set("Surface", "name", "nil");
        return true;
    }
    bool ri_attributes::set_displacement(const ri_shader* s)
    {
        displacement_.clear();
        if (s)
        {
            displacement_.push_back(s);
            this->set("Displacement", "name", s->get_name().c_str());
        }
        else
        {
            this->set("Displacement", "name", "nil");
        }
        return true;
    }
    bool ri_attributes::del_displacement()
    {
        displacement_.clear();
        this->set("Displacement", "name", "nil");
        return true;
    }
    bool ri_attributes::set_atmosphere(const ri_shader* s)
    {
        atmosphere_.clear();
        if (s)
        {
            atmosphere_.push_back(s);
            this->set("Atmosphere", "name", s->get_name().c_str());
        }
        else
        {
            this->set("Atmosphere", "name", "nil");
        }
        return true;
    }
    bool ri_attributes::del_atmosphere()
    {
        atmosphere_.clear();
        this->set("Atmosphere", "name", "nil");
        return true;
    }
    bool ri_attributes::set_interior(const ri_shader* s)
    {
        interior_.clear();
        if (s)
        {
            interior_.push_back(s);
            this->set("Interior", "name", s->get_name().c_str());
        }
        else
        {
            this->set("Interior", "name", "nil");
        }
        return true;
    }
    bool ri_attributes::del_interior()
    {
        interior_.clear();
        this->set("Interior", "name", "nil");
        return true;
    }
    bool ri_attributes::set_exterior(const ri_shader* s)
    {
        exterior_.clear();
        if (s)
        {
            exterior_.push_back(s);
            this->set("Exterior", "name", s->get_name().c_str());
        }
        else
        {
            this->set("Exterior", "name", "nil");
        }
        return true;
    }
    bool ri_attributes::del_exterior()
    {
        exterior_.clear();
        this->set("Exterior", "name", "nil");
        return true;
    }

    //------------------------------------------------------

    const ri_shader* ri_attributes::get_suraface() const
    {
        if (!surface_.empty())
            return surface_.back();
        return NULL;
    }
    const ri_shader* ri_attributes::get_displacement() const
    {
        if (!displacement_.empty())
            return displacement_.back();
        return NULL;
    }
    const ri_shader* ri_attributes::get_atmosphere() const
    {
        if (!atmosphere_.empty())
            return atmosphere_.back();
        return NULL;
    }
    const ri_shader* ri_attributes::get_interior() const
    {
        if (!interior_.empty())
            return interior_.back();
        return NULL;
    }
    const ri_shader* ri_attributes::get_exterior() const
    {
        if (!exterior_.empty())
            return exterior_.back();
        return NULL;
    }

    //------------------------------------------------------

    bool ri_attributes::add_light_source(const ri_light_source* l)
    {
        lights_.insert(l);
        return true;
    }

    bool ri_attributes::del_light_source(const ri_light_source* l)
    {
        lights_.erase(l);
        return true;
    }

    bool ri_attributes::get_lightsource(
        std::vector<const ri_light_source*>& lv) const
    {
        typedef std::set<const ri_light_source*> LightSet;
        lv.reserve(lights_.size());
        for (LightSet::const_iterator i = lights_.begin(); i != lights_.end(); ++i)
        {
            lv.push_back(*i);
        }

        return true;
    }

    //------------------------------------------------------

    bool ri_attributes::add_trim_curve(const ri_trim_curve_geometry* c)
    {
        curves_.insert(c);
        return true;
    }

    bool ri_attributes::del_trim_curve(const ri_trim_curve_geometry* c)
    {
        curves_.erase(c);
        return true;
    }

    bool ri_attributes::get_trim_curve(
        std::vector<const ri_trim_curve_geometry*>& lv) const
    {
        typedef std::set<const ri_trim_curve_geometry*> CurveSet;
        lv.reserve(curves_.size());
        for (CurveSet::const_iterator i = curves_.begin(); i != curves_.end(); ++i)
        {
            lv.push_back(*i);
        }

        return true;
    }

    //------------------------------------------------------

    typedef std::map<std::string, const ri_coordinate_system*> coords_type;

    bool ri_attributes::add_coordinate_system(const ri_coordinate_system* c)
    {
        coords_[c->get_name()] = c;
        return true;
    }

    bool ri_attributes::del_coordinate_system(const ri_coordinate_system* c)
    {
        coords_.erase(c->get_name());
        return true;
    }

    bool ri_attributes::get_coordinate_system(
        std::vector<const ri_coordinate_system*>& cv) const
    {
        cv.reserve(coords_.size());
        for (coords_type::const_iterator i = coords_.begin(); i != coords_.end(); ++i)
        {
            cv.push_back(i->second);
        }
        return true;
    }
    const ri_coordinate_system*
    ri_attributes::get_coordinate_system(const char* key) const
    {
        typedef std::map<std::string, const ri_coordinate_system*> cmap;
        typedef cmap::const_iterator citer;
        citer it = coords_.find(key);
        if (it != coords_.end())
        {
            return it->second;
        }
        else
        {
            return NULL;
        }
    }

    //------------------------------------------------------

    bool ri_attributes::get(const char* key1, const char* key2, int& val) const
    {
        std::string str = get_key(key1, key2);
        const ri_parameters::value_type* pRet = map_->get(str.c_str());
        if (pRet)
        {
            if (pRet->nType != ri_parameters::TYPE_INTEGER)
                return false;
            if (pRet->integer_values.size() != 1)
                return false;
            val = pRet->integer_values[0];
            return true;
        }
        else
        {
            return false;
        }
    }
    bool ri_attributes::get(const char* key1, const char* key2, float& val) const
    {
        std::string str = get_key(key1, key2);
        const ri_parameters::value_type* pRet = map_->get(str.c_str());
        if (pRet)
        {
            if (pRet->nType != ri_parameters::TYPE_FLOAT)
                return false;
            if (pRet->float_values.size() != 1)
                return false;
            val = pRet->float_values[0];
            return true;
        }
        else
        {
            return false;
        }
    }
    bool ri_attributes::get(const char* key1, const char* key2,
                            std::string& val) const
    {
        std::string str = get_key(key1, key2);
        const ri_parameters::value_type* pRet = map_->get(str.c_str());
        if (pRet)
        {
            if (pRet->nType != ri_parameters::TYPE_STRING)
                return false;
            if (pRet->string_values.size() != 1)
                return false;
            val = pRet->string_values[0];
            return true;
        }
        else
        {
            return false;
        }
    }

    bool ri_attributes::get(const char* key1, const char* key2,
                            std::vector<int>& val) const
    {
        std::string str = get_key(key1, key2);
        const ri_parameters::value_type* pRet = map_->get(str.c_str());
        if (pRet)
        {
            if (pRet->nType != ri_parameters::TYPE_INTEGER)
                return false;
            val = pRet->integer_values;
            return true;
        }
        else
        {
            return false;
        }
    }
    bool ri_attributes::get(const char* key1, const char* key2,
                            std::vector<float>& val) const
    {
        std::string str = get_key(key1, key2);
        const ri_parameters::value_type* pRet = map_->get(str.c_str());
        if (pRet)
        {
            if (pRet->nType != ri_parameters::TYPE_FLOAT)
                return false;
            val = pRet->float_values;
            return true;
        }
        else
        {
            return false;
        }
    }
    bool ri_attributes::get(const char* key1, const char* key2,
                            std::vector<std::string>& val) const
    {
        std::string str = get_key(key1, key2);
        const ri_parameters::value_type* pRet = map_->get(str.c_str());
        if (pRet)
        {
            if (pRet->nType != ri_parameters::TYPE_STRING)
                return false;
            val = pRet->string_values;
            return true;
        }
        else
        {
            return false;
        }
    }

    ri_attributes::ri_attributes()
    {
        map_ = new ri_parameters();
        init_parameters(map_);
    }
    ri_attributes::ri_attributes(const ri_attributes& rhs)
    {
        map_ = new ri_parameters(*(rhs.map_));
        surface_ = rhs.surface_;
        displacement_ = rhs.displacement_;
        atmosphere_ = rhs.atmosphere_;
        interior_ = rhs.interior_;
        exterior_ = rhs.exterior_;
        curves_ = rhs.curves_;
        lights_ = rhs.lights_;
        coords_ = rhs.coords_;
        grps_ = rhs.grps_;
    }
    ri_attributes::~ri_attributes() { delete map_; }
    ri_attributes& ri_attributes::operator=(const ri_attributes& rhs)
    {
        ri_attributes tmp(rhs);
        this->swap(tmp);
        return *this;
    }
    void ri_attributes::swap(ri_attributes& rhs)
    {
        map_->swap(*rhs.map_);
        surface_.swap(rhs.surface_);
        displacement_.swap(rhs.displacement_);
        atmosphere_.swap(rhs.atmosphere_);
        interior_.swap(rhs.interior_);
        exterior_.swap(rhs.exterior_);
        curves_.swap(rhs.curves_);
        lights_.swap(rhs.lights_);
        coords_.swap(rhs.coords_);
        grps_.swap(rhs.grps_);
    }

    void ri_attributes::push_group(ri_group_geometry* grp) { grps_.push_back(grp); }

    void ri_attributes::pop_group()
    {
        if (!grps_.empty())
        {
            grps_.pop_back();
        }
    }

    ri_group_geometry* ri_attributes::top_group()
    {
        if (grps_.empty())
        {
            return NULL;
        }
        else
        {
            return grps_.back();
        }
    }

    void ri_attributes::clear_group() { grps_.clear(); }
}
