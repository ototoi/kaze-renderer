#ifndef RI_GEOMETRY_CONVERTER_H
#define RI_GEOMETRY_CONVERTER_H

#include "ri_geometry.h"
#include <memory>

namespace ri
{
    class ri_geometry_converter
    {
    public:
        typedef std::map<const ri_geometry*, std::shared_ptr<ri_geometry> > map_type;
    public:
        const ri_geometry* convert(const ri_geometry* pGeo) const;

    private:
        mutable map_type map_;
    };
}

#endif
