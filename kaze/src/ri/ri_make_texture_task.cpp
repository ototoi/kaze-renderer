#include "ri_make_texture_task.h"
#include "ri_path_resolver.h"
#include "ri_logger.h"

#ifdef _WIN32
#include <windows.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#else

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#endif

#include <errno.h>
#include <stdlib.h>

namespace ri
{
    using namespace std;

    static int GetSize(int nClass, RtInt vertex = 1, RtInt varying = 1,
                       RtInt uniform = 1, RtInt facevarying = 1)
    {
        switch (nClass)
        {
        case ri_classifier::VERTEX:
            return vertex;
        case ri_classifier::UNIFORM:
            return uniform;
        case ri_classifier::CONSTANT:
            return 1;
        case ri_classifier::VARYING:
            return varying;
        case ri_classifier::FACEVARYING:
            return facevarying;
        }
        return 1;
    }

    static void AddParams(ri_parameters* p_params, ri_classifier* p_cls,
                          RtToken token, RtPointer param, RtInt vertex = 1,
                          RtInt varying = 1, RtInt uniform = 1,
                          RtInt facevarying = 1)
    {
        typedef const char* PCTR;

        int nID = p_cls->def(token);
        if (nID < 0)
            return;
        const ri_classifier::param_data& data = (*p_cls)[nID];
        int nSz = GetSize(data.get_class(), vertex, varying, uniform, facevarying);
        switch (data.get_type())
        {
        case ri_classifier::INTEGER:
            p_params->set(data.get_name(), (const RtInt*)param, data.get_size() * nSz);
            break;
        case ri_classifier::STRING:
            p_params->set(data.get_name(), (const PCTR*)param, data.get_size() * nSz);
            break;
        case ri_classifier::FLOAT:
        case ri_classifier::COLOR:
        case ri_classifier::POINT:
        case ri_classifier::VECTOR:
        case ri_classifier::NORMAL:
        case ri_classifier::HPOINT:
        case ri_classifier::MATRIX:
            p_params->set(data.get_name(), (const RtFloat*)param,
                          data.get_size() * nSz);
            break;
        default:
            break;
        }
    }

    static void AddParams(ri_parameters* p_params, ri_classifier* p_cls,
                          RtInt count, RtToken tokens[], RtPointer params[],
                          RtInt vertex = 1, RtInt varying = 1, RtInt uniform = 1,
                          RtInt facevarying = 1)
    {
        if (count)
        {
            for (int i = 0; i < count; i++)
            {
                AddParams(p_params, p_cls, tokens[i], params[i], vertex, varying, uniform,
                          facevarying);
            }
        }
    }
//------------------------------------------------------

#ifdef _WIN32
    static int ExecuteProcess(const char* szCmd, const char* szArg)
    {
        PROCESS_INFORMATION pi;
        STARTUPINFOA si;

        ZeroMemory(&si, sizeof(si));
        si.cb = sizeof(si);

        si.cb = sizeof(STARTUPINFO);
        si.dwFlags = STARTF_USESHOWWINDOW;
        si.wShowWindow = SW_SHOWNORMAL;
        si.hStdOutput = GetStdHandle(STD_OUTPUT_HANDLE);
        si.hStdError = GetStdHandle(STD_ERROR_HANDLE);

#if 0
            int nWINDOW = CREATE_NEW_CONSOLE;
#else
        int nWINDOW = CREATE_NO_WINDOW;
#endif
        std::string strCmdLine;
        strCmdLine += szCmd;
        strCmdLine += " ";
        strCmdLine += szArg;

        std::vector<char> s_strCmdLine(strCmdLine.size() + 1);
        strcpy(&s_strCmdLine[0], strCmdLine.c_str());
        s_strCmdLine.back() = 0;

        BOOL bRet = ::CreateProcessA(NULL, (char*)(&s_strCmdLine[0]), NULL, NULL,
                                     TRUE, nWINDOW, NULL, NULL, &si, &pi);

        if (bRet)
        {
            DWORD nExitCode = 0;
            ::CloseHandle(pi.hThread);
            ::WaitForSingleObject(pi.hProcess, INFINITE);
            ::GetExitCodeProcess(pi.hProcess, &nExitCode);
            ::CloseHandle(pi.hProcess);
            return nExitCode;
        }

        if (!bRet)
            return -1;

        return 0;
    }
#else
    static int ExecuteProcess(const char* szCmd, const char* szArg)
    {
        std::string cmdLine;
        cmdLine += szCmd;
        cmdLine += " ";
        cmdLine += szArg;

        // printf("%s\n", cmdLine.c_str());
        int nRet = ::system(cmdLine.c_str());
        if (nRet == 127 || nRet == -1)
        {
            print_log("%s\n", strerror(errno));
            return -1;
        }
        return nRet;
    }
#endif

    static bool IsExistFile(const char* szFilePath)
    {
#ifdef _WIN32
        struct _stat s;
        int nRet = _stat(szFilePath, &s);
        return (nRet == 0);
#else
        struct stat s;
        int nRet = stat(szFilePath, &s);
        return (nRet == 0);
#endif
    }

    static size_t GetLastModifiedTime(const char* szFilePath)
    {
#ifdef _WIN32
        struct _stat s;
        int nRet = _stat(szFilePath, &s);
        return s.st_mtime;
#else
        struct stat s;
        int nRet = stat(szFilePath, &s);
        return s.st_mtime;
#endif
    }

    static std::string MakeFullPath(const char* szFilePath)
    {
#ifdef _WIN32
        char szFullPath[_MAX_PATH] = "";
        _fullpath(szFullPath, szFilePath, _MAX_PATH);
        return szFullPath;
#else
        char szFullPath[512];
        realpath(szFilePath, szFullPath);
        return szFullPath;
#endif
    }

    static std::string MakeFullPath(const std::vector<std::string>& strFolder,
                                    const char* szFilePath)
    {
        for (size_t i = 0; i < strFolder.size(); i++)
        {
            std::string path = strFolder[i] + szFilePath;
            path = MakeFullPath(path.c_str());
            if (IsExistFile(path.c_str()))
            {
                return path;
            }
        }
        return MakeFullPath(szFilePath);
    }

    static std::string ReplacePath(const char* szFullPath, const char* szOldName,
                                   const char* szNewName)
    {
        const char* szFind = strstr(szFullPath, szOldName);
        if (!szFind)
            return "";
        std::string strRet = std::string(szFullPath, szFind);
        strRet += szNewName;
        return strRet;
    }

    static bool TouchFile(const char* src)
    {
#ifdef _WIN32
        FILE* fp = fopen(src, "wb");
        if (fp)
            fclose(fp);
#else
        char buffer[1024] = {};
        snprintf(buffer, 1024, "touch \"%s\"", src);
        ::system(buffer);
#endif
        return true;
    }

    //------------------------------------------------------

    ri_make_texture_task::ri_make_texture_task(
        const ri_classifier* p_cls, const ri_options& opts, char* pic, char* tex,
        RtToken swrap, RtToken twrap, RtToken filterfunc, RtFloat swidth,
        RtFloat twidth, RtInt n, RtToken tokens[], RtPointer params[])
    {
        params_.set("pic", pic);
        params_.set("tex", tex);
        params_.set("swrap", swrap);
        params_.set("twrap", twrap);
        params_.set("filterfunc", filterfunc);
        params_.set("swidth", swidth);
        params_.set("twidth", twidth);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params);
        }

        ri_path_resolver pr(opts);
        std::vector<std::string> tex_path = pr.get_textures_paths();

        std::string strFullPic = MakeFullPath(tex_path, pic);
        std::string strFullTex = ReplacePath(strFullPic.c_str(), pic, tex);

        std::string strFullMrky = strFullTex + ".mrky";

        std::string strTEXCmp = pr.get_bin_path();
        strTEXCmp += "kzritex";

#ifdef _WIN32
        strTEXCmp += ".exe";
#endif
        strTEXCmp = MakeFullPath(strTEXCmp.c_str());

        // TouchFile(strFullMrky.c_str());

        params_.set("full_pic", strFullPic.c_str());
        params_.set("full_tex", strFullTex.c_str());
        params_.set("full_mrky", strFullMrky.c_str());
        params_.set("full_exe", strTEXCmp.c_str());
    }

    static std::string TOS(float x)
    {
        char buffer[64];
        sprintf(buffer, "%f", x);
        return buffer;
    }

    int ri_make_texture_task::run()
    {
        std::string strFullPic = (params_.get("full_pic"))->string_values[0];
        std::string strFullTex = (params_.get("full_tex"))->string_values[0];

        std::string strFullMrky = (params_.get("full_mrky"))->string_values[0];

        std::string swrap = (params_.get("swrap"))->string_values[0];
        std::string twrap = (params_.get("twrap"))->string_values[0];
        std::string filterfunc = (params_.get("filterfunc"))->string_values[0];
        float swidth = (params_.get("swidth"))->float_values[0];
        float twidth = (params_.get("twidth"))->float_values[0];

        std::string strTEXCmp = (params_.get("full_exe"))->string_values[0];

        if (!IsExistFile(strTEXCmp.c_str()))
            return 0;
        if (!IsExistFile(strFullPic.c_str()))
            return 0;
        size_t mcmp = GetLastModifiedTime(strTEXCmp.c_str());
        size_t mpic = GetLastModifiedTime(strFullPic.c_str());

        bool bForce = false;
        if (IsExistFile(strFullTex.c_str()))
        {
            size_t mtex = GetLastModifiedTime(strFullTex.c_str());
            if (mtex <= mcmp)
                bForce = true;
        }

        std::string strCmd;
        strCmd += "\"";
        strCmd += strTEXCmp;
        strCmd += "\"";

        std::string strArg;
        strArg += "\"";
        strArg += strFullPic;
        strArg += "\"";

        strArg += " ";

        strArg += "\"";
        strArg += strFullTex;
        strArg += "\"";

        strArg += " ";

        if (bForce)
        {
            strArg += "-force";
            strArg += " ";
        }

        strArg += "-smode \"";
        strArg += swrap;
        strArg += "\"";

        strArg += " ";

        strArg += "-tmode \"";
        strArg += twrap;
        strArg += "\"";

        strArg += " ";

        strArg += "-filter \"";
        strArg += filterfunc;
        strArg += "\"";

        strArg += " ";

        strArg += "-sfilterwidth \"";
        strArg += TOS(swidth);
        strArg += "\"";

        strArg += " ";

        strArg += "-tfilterwidth \"";
        strArg += TOS(twidth);
        strArg += "\"";

        // print_log("ri_make_texture_task:%s start.\n", strArg.c_str());
        int nRet = ExecuteProcess(strCmd.c_str(), strArg.c_str());
        // print_log("ri_make_texture_task:end.\n");

        // system::delete_file(strFullMrky.c_str());
        return 0;
    }

    //--------------------------------------------------------

    ri_make_lat_long_environment_task::ri_make_lat_long_environment_task(
        const ri_classifier* p_cls, const ri_options& opts, char* pic, char* tex,
        RtToken filterfunc, RtFloat swidth, RtFloat twidth, RtInt n,
        RtToken tokens[], RtPointer params[])
    {
        params_.set("pic", pic);
        params_.set("tex", tex);
        params_.set("filterfunc", filterfunc);
        params_.set("swidth", swidth);
        params_.set("twidth", twidth);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params);
        }

        ri_path_resolver pr(opts);
        std::vector<std::string> tex_path = pr.get_textures_paths();

        std::string strFullPic = MakeFullPath(tex_path, pic);
        std::string strFullTex = ReplacePath(strFullPic.c_str(), pic, tex);

        std::string strFullMrky = strFullTex + ".mrky";

        std::string strTEXCmp = pr.get_bin_path();
        if (1)
        {
            strTEXCmp += "kzritex";
        }
        else
        {
            strTEXCmp += "kzritexgl"; // gl
        }

#ifdef _WIN32
        strTEXCmp += ".exe";
#endif
        strTEXCmp = MakeFullPath(strTEXCmp.c_str());

        // TouchFile(strFullMrky.c_str());

        params_.set("full_pic", strFullPic.c_str());
        params_.set("full_tex", strFullTex.c_str());
        params_.set("full_mrky", strFullMrky.c_str());
        params_.set("full_exe", strTEXCmp.c_str());
    }

    int ri_make_lat_long_environment_task::run()
    {
        std::string strFullPic = (params_.get("full_pic"))->string_values[0];
        std::string strFullTex = (params_.get("full_tex"))->string_values[0];

        std::string strFullMrky = (params_.get("full_mrky"))->string_values[0];

        std::string filterfunc = (params_.get("filterfunc"))->string_values[0];
        float swidth = (params_.get("swidth"))->float_values[0];
        float twidth = (params_.get("twidth"))->float_values[0];

        std::string strTEXCmp = (params_.get("full_exe"))->string_values[0];

        if (!IsExistFile(strTEXCmp.c_str()))
            return 0;
        if (!IsExistFile(strFullPic.c_str()))
            return 0;
        size_t mcmp = GetLastModifiedTime(strTEXCmp.c_str());
        size_t mpic = GetLastModifiedTime(strFullPic.c_str());

        bool bForce = false;
        if (IsExistFile(strFullTex.c_str()))
        {
            size_t mtex = GetLastModifiedTime(strFullTex.c_str());
            if (mtex <= mcmp)
                bForce = true;
        }

        std::string strCmd;
        strCmd += "\"";
        strCmd += strTEXCmp;
        strCmd += "\"";

        std::string strArg;

        strArg += "-envlatl";
        strArg += " ";

        strArg += "\"";
        strArg += strFullPic;
        strArg += "\"";

        strArg += " ";

        strArg += "\"";
        strArg += strFullTex;
        strArg += "\"";

        strArg += " ";

        if (bForce)
        {
            strArg += "-force";
            strArg += " ";
        }

        strArg += "-filter \"";
        strArg += filterfunc;
        strArg += "\"";

        strArg += " ";

        strArg += "-sfilterwidth \"";
        strArg += TOS(swidth);
        strArg += "\"";

        strArg += " ";

        strArg += "-tfilterwidth \"";
        strArg += TOS(twidth);
        strArg += "\"";

        // print_log("ri_make_lat_long_environment_task:%s start.\n", strArg.c_str());
        int nRet = ExecuteProcess(strCmd.c_str(), strArg.c_str());

        return 0;
    }

    //--------------------------------------------------------

    ri_make_cube_face_environment_task::ri_make_cube_face_environment_task(
        const ri_classifier* p_cls, const ri_options& opts, char* px, char* nx,
        char* py, char* ny, char* pz, char* nz, char* tex, RtFloat fov,
        RtToken filterfunc, RtFloat swidth, RtFloat twidth, RtInt n,
        RtToken tokens[], RtPointer params[])
    {
        params_.set("px", px);
        params_.set("nx", nx);
        params_.set("py", py);
        params_.set("ny", ny);
        params_.set("pz", pz);
        params_.set("nz", nz);
        params_.set("tex", tex);
        params_.set("fov", fov);
        params_.set("filterfunc", filterfunc);
        params_.set("swidth", swidth);
        params_.set("twidth", twidth);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params);
        }

        ri_path_resolver pr(opts);
        std::vector<std::string> tex_path = pr.get_textures_paths();

        // std::string strFullPic = MakeFullPath(pic);
        std::string strFullPx = MakeFullPath(tex_path, px);
        std::string strFullPy = MakeFullPath(tex_path, py);
        std::string strFullPz = MakeFullPath(tex_path, pz);
        std::string strFullNx = MakeFullPath(tex_path, nx);
        std::string strFullNy = MakeFullPath(tex_path, ny);
        std::string strFullNz = MakeFullPath(tex_path, nz);

        std::string strFullTex = MakeFullPath(tex);

        std::string strFullMrky = strFullTex + ".mrky";

        std::string strTEXCmp = pr.get_bin_path();
        if (1)
        {
            strTEXCmp += "kzritex";
        }
        else
        {
            strTEXCmp += "kzritexgl"; // gl
        }

#ifdef _WIN32
        strTEXCmp += ".exe";
#endif
        strTEXCmp = MakeFullPath(strTEXCmp.c_str());

        params_.set("full_px", strFullPx.c_str());
        params_.set("full_py", strFullPy.c_str());
        params_.set("full_pz", strFullPz.c_str());
        params_.set("full_nx", strFullNx.c_str());
        params_.set("full_ny", strFullNy.c_str());
        params_.set("full_nz", strFullNz.c_str());
        params_.set("full_tex", strFullTex.c_str());
        params_.set("full_mrky", strFullMrky.c_str());
        params_.set("full_exe", strTEXCmp.c_str());
    }

    int ri_make_cube_face_environment_task::run()
    {
        std::string strFullPx = (params_.get("full_px"))->string_values[0];
        std::string strFullPy = (params_.get("full_py"))->string_values[0];
        std::string strFullPz = (params_.get("full_pz"))->string_values[0];
        std::string strFullNx = (params_.get("full_nx"))->string_values[0];
        std::string strFullNy = (params_.get("full_ny"))->string_values[0];
        std::string strFullNz = (params_.get("full_nz"))->string_values[0];

        std::string strFullTex = (params_.get("full_tex"))->string_values[0];

        std::string strTEXCmp = (params_.get("full_exe"))->string_values[0];

        float fov = (params_.get("fov"))->float_values[0];

        bool bForce = false;

        if (!IsExistFile(strTEXCmp.c_str()))
            return 0;

        if (!IsExistFile(strFullPx.c_str()))
            return 0;
        if (!IsExistFile(strFullPy.c_str()))
            return 0;
        if (!IsExistFile(strFullPz.c_str()))
            return 0;

        if (!IsExistFile(strFullNx.c_str()))
            return 0;
        if (!IsExistFile(strFullNy.c_str()))
            return 0;
        if (!IsExistFile(strFullNz.c_str()))
            return 0;

        size_t mcmp = GetLastModifiedTime(strTEXCmp.c_str());

        size_t mpics[6] = {};
        {
            std::string pics[] = {strFullPx, strFullPy, strFullPz,
                                  strFullNx, strFullNy, strFullNz};
            for (int i = 0; i < 6; i++)
            {
                mpics[i] = GetLastModifiedTime(pics[i].c_str());
                if (mcmp >= mpics[i])
                    bForce = true;
            }
        }

        if (IsExistFile(strFullTex.c_str()))
        {
            size_t mtex = GetLastModifiedTime(strFullTex.c_str());
            if (mtex <= mcmp)
                bForce = true;
            for (int i = 0; i < 6; i++)
            {
                if (mcmp <= mpics[i])
                    bForce = true;
            }
        }

        std::string strCmd;
        strCmd += "\"";
        strCmd += strTEXCmp;
        strCmd += "\"";

        std::string strArg;

        strArg += "-envcube";
        strArg += " ";

        //-------------------
        strArg += "\"";
        strArg += strFullPx;
        strArg += "\"";

        strArg += " ";

        strArg += "\"";
        strArg += strFullNx;
        strArg += "\"";

        strArg += " ";
        //-------------------
        //-------------------
        strArg += "\"";
        strArg += strFullPy;
        strArg += "\"";

        strArg += " ";

        strArg += "\"";
        strArg += strFullNy;
        strArg += "\"";

        strArg += " ";
        //-------------------
        //-------------------
        strArg += "\"";
        strArg += strFullPz;
        strArg += "\"";

        strArg += " ";

        strArg += "\"";
        strArg += strFullNz;
        strArg += "\"";

        strArg += " ";
        //-------------------
        strArg += "\"";
        strArg += strFullTex;
        strArg += "\"";

        strArg += " ";

        strArg += "-fov \"";
        strArg += TOS(fov);
        strArg += "\"";

        strArg += " ";

        if (bForce)
        {
            strArg += "-force";
            strArg += " ";
        }

        // print_log("ri_make_shadow_task:%s start.\n", strArg.c_str());
        int nRet = ExecuteProcess(strCmd.c_str(), strArg.c_str());
        // print_log("ri_make_shadow_task:end.\n");

        return 0;
    }
    //--------------------------------------------------------

    ri_make_shadow_task::ri_make_shadow_task(const ri_classifier* p_cls,
                                             const ri_options& opts,
                                             const char* pic, const char* tex,
                                             RtInt n, RtToken tokens[],
                                             RtPointer params[])
    {
        params_.set("pic", pic);
        params_.set("tex", tex);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params);
        }

        std::string strFullPic = MakeFullPath(pic);
        std::string strFullTex = MakeFullPath(tex);

        ri_path_resolver pr(opts);
        std::string strTEXCmp = pr.get_bin_path();
        if (1)
        {
            strTEXCmp += "kzritex";
        }
        else
        {
            strTEXCmp += "kzritexgl"; // gl
        }

#ifdef _WIN32
        strTEXCmp += ".exe";
#endif
        strTEXCmp = MakeFullPath(strTEXCmp.c_str());

        // TouchFile(strFullMrky.c_str());

        params_.set("full_pic", strFullPic.c_str());
        params_.set("full_tex", strFullTex.c_str());
        params_.set("full_exe", strTEXCmp.c_str());
    }

    int ri_make_shadow_task::run()
    {
        std::string strFullPic = (params_.get("full_pic"))->string_values[0];
        std::string strFullTex = (params_.get("full_tex"))->string_values[0];

        std::string strTEXCmp = (params_.get("full_exe"))->string_values[0];

        std::string strCmd;
        strCmd += "\"";
        strCmd += strTEXCmp;
        strCmd += "\"";

        std::string strArg;

        strArg += "-shadow";
        strArg += " ";

        strArg += "\"";
        strArg += strFullPic;
        strArg += "\"";

        strArg += " ";

        strArg += "\"";
        strArg += strFullTex;
        strArg += "\"";

        // print_log("ri_make_shadow_task:%s start.\n", strArg.c_str());
        if (!IsExistFile(strTEXCmp.c_str()))
            return 0;
        if (!IsExistFile(strFullPic.c_str()))
            return 0;
        if (IsExistFile(strFullTex.c_str()))
        {
            size_t mcmp = GetLastModifiedTime(strTEXCmp.c_str());
            size_t mpic = GetLastModifiedTime(strFullPic.c_str());
            size_t mtex = GetLastModifiedTime(strFullTex.c_str());
            if (mcmp < mtex && mtex > mpic)
                return 0;
        }
        int nRet = ExecuteProcess(strCmd.c_str(), strArg.c_str());
        // print_log("ri_make_shadow_task:end.\n");

        return 0;
    }

    //--------------------------------------------------------

    ri_make_brick_map_task::ri_make_brick_map_task(const ri_classifier* p_cls,
                                                   const ri_options& opts, int nptc,
                                                   char** ptcnames, char* bkmname,
                                                   RtInt n, RtToken tokens[],
                                                   RtPointer params[]) {}

    int ri_make_brick_map_task::run() { return 0; }
}
