#ifndef RI_BEZIER_H
#define RI_BEZIER_H

namespace ri
{
    void ri_patch_convert_to_bezier(float points[], int dim, const char* ubasis,
                                    const char* vbasis);
    void ri_curves_convert_to_bezier(float points[], int dim,
                                     const char* vbasis); // for curves
}

#endif
