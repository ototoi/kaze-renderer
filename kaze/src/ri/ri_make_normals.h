#ifndef RI_MAKE_NORMALS_H
#define RI_MAKE_NORMALS_H

#include "ri.h"
#include <vector>

namespace ri
{

    int ri_make_normals(std::vector<float>& N, int npolys, const int nverts[],
                        const int verts[], const float P[], float angle);
}

#endif
