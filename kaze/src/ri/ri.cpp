#include "ri.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <vector>
#include <set>
#include <cmath>

#define FUNC_BEGIN() \
    try              \
    {
   
#define FUNC_END() \
    }              \
    catch (...)    \
    {              \
    }

using namespace std;

#ifdef __cplusplus
extern "C" {
#endif


//---------------------------------------------------------
RtFloat RiGaussianFilter(RtFloat x, RtFloat y, RtFloat xwidth, RtFloat ywidth)
{
    x *= 2.0f / xwidth;
    y *= 2.0f / ywidth;
    return exp(-2.0f * (x * x + y * y));
}
RtFloat RiBoxFilter(RtFloat x, RtFloat y, RtFloat xwidth, RtFloat ywidth)
{
    return 1.0f;
}

RtFloat RiTriangleFilter(RtFloat x, RtFloat y, RtFloat xwidth, RtFloat ywidth)
{
    return ((1.0f - fabs(x)) / (xwidth * 0.5f)) *
           ((1.0f - fabs(y)) / (ywidth * 0.5f));
}
RtFloat RiCatmullRomFilter(RtFloat x, RtFloat y, RtFloat xwidth,
                           RtFloat ywidth)
{
    RtFloat r2 = x * x + y * y;
    RtFloat r = sqrt(r2);
    return (r >= 2.0f) ? 0.0f : (r < 1.0f)
                                    ? (3.0f * r * r2 - 5.0f * r2 + 2.0f)
                                    : (-r * r2 + 5.0f * r2 - 8.0f * r + 4.0f);
}
RtFloat RiSincFilter(RtFloat x, RtFloat y, RtFloat xwidth, RtFloat ywidth)
{
    RtFloat s, t;
    if (x > -0.001f && x < 0.001f)
        s = 1.0f;
    else
        s = sin(x) / x;

    if (y > -0.001f && y < 0.001f)
        t = 1.0f;
    else
        t = sin(y) / y;

    return s * t;
}

RtVoid RiErrorIgnore(RtInt code, RtInt severity, char* msg)
{
    ; //
}

RtVoid RiErrorPrint(RtInt code, RtInt severity, char* msg)
{
    ; //
}
RtVoid RiErrorAbort(RtInt code, RtInt severity, char* msg)
{
    ; //
}

#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4305)
#endif
RtBasis RiBezierBasis = {
    {-1, 3, -3, 1}, {3, -6, 3, 0}, {-3, 3, 0, 0}, {1, 0, 0, 0}};

RtBasis RiBSplineBasis = {{-1.0 / 6, 3.0 / 6, -3.0 / 6, 1.0 / 6},
                          {3.0 / 6, -6.0 / 6, 3.0 / 6, 0.0 / 6},
                          {-3.0 / 6, 0.0 / 6, 3.0 / 6, 0.0 / 6},
                          {1.0 / 6, 4.0 / 6, 1.0 / 6, 0.0 / 6}};

RtBasis RiCatmullRomBasis = {{-1.0 / 2, 3.0 / 2, -3.0 / 2, 1.0 / 2},
                             {2.0 / 2, -5.0 / 2, 4.0 / 2, -1.0 / 2},
                             {-1.0 / 2, 0.0 / 2, 1.0 / 2, 0.0 / 2},
                             {0.0 / 2, 2.0 / 2, 0.0 / 2, 0.0 / 2}};

RtBasis RiHermiteBasis = {
    {2, 1, -2, 1}, {-3, -2, 3, -1}, {0, 1, 0, 0}, {1, 0, 0, 0}};

RtBasis RiPowerBasis = {{1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};
#ifdef _MSC_VER
#pragma warning(pop)
#endif

//---------------------------------------------------------
RtVoid RiProcDelayedReadArchive(RtPointer data, RtFloat detail)
{ 
    ; 
}
RtVoid RiProcRunProgram(RtPointer data, RtFloat detail)
{ 
    ; 
}
RtVoid RiProcDynamicLoad(RtPointer data, RtFloat detail)
{
    ; //
}
//---------------------------------------------------------
RtVoid RiProc2DelayedReadArchive(RtContextHandle, RtFloat detail, RtInt n,
                                 RtToken tokens[], RtPointer params[])
{
    ;
}
RtVoid RiProc2RunProgram(RtContextHandle, RtFloat detail, RtInt n,
                         RtToken tokens[], RtPointer params[])
{
    ;
}
RtVoid RiProc2DynamicLoad(RtContextHandle, RtFloat detail, RtInt n,
                          RtToken tokens[], RtPointer params[])
{
    ;
}

RtVoid RiSimpleBound(RtContextHandle, RtFloat detail, RtInt n, RtToken tokens[],
                     RtPointer params[])
{
    ;
}

RtVoid RiDSOBound(RtContextHandle, RtFloat detail, RtInt n, RtToken tokens[],
                  RtPointer params[])
{
    ;
}
//--------------------------------
RtContextHandle RiGetContext(void)
{
    FUNC_BEGIN()
    // return ((RtContextHandle)GetCurrentContext());
    FUNC_END()
    return NULL;
}
RtVoid RiContext(RtContextHandle handle)
{
    FUNC_BEGIN()
    // SetCurrentContext((ri_context*)handle);
    FUNC_END()
}
//--------------------------------
RtToken RiDeclare(char* name, char* declaration)
{
    FUNC_BEGIN()
    return name; ////GetCurrentContext()->RiDeclare(name, declaration);
    FUNC_END()
    return RI_NULL;
}
RtVoid RiBegin(RtToken name)
{
    FUNC_BEGIN()
    // SetCurrentContext(GetCurrentContextFactory()->Create(name));
    // GetCurrentContext()->RiBegin(name);
    FUNC_END()
}
RtVoid RiEnd(void)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiEnd();
    // PopCurrentContext();
    FUNC_END()
}

RtVoid RiFrameBegin(RtInt frame)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiFrameBegin(frame);
    FUNC_END()
}
RtVoid RiFrameEnd(void)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiFrameEnd();
    FUNC_END()
}
RtVoid RiWorldBegin(void)
{
    FUNC_BEGIN()
    ////GetCurrentContext()->RiWorldBegin();
    FUNC_END()
}
RtVoid RiWorldEnd(void)
{
    FUNC_BEGIN()
    ////GetCurrentContext()->RiWorldEnd();
    FUNC_END()
}
//--------------------------------
RtVoid RiFormat(RtInt xres, RtInt yres, RtFloat aspect)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiFormat(xres, yres, aspect);
    FUNC_END()
}
RtVoid RiFrameAspectRatio(RtFloat aspect)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiFrameAspectRatio(aspect);
    FUNC_END()
}
RtVoid RiScreenWindow(RtFloat left, RtFloat right, RtFloat bot, RtFloat top)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiScreenWindow(left, right, bot, top);
    FUNC_END()
}
RtVoid RiCropWindow(RtFloat xmin, RtFloat xmax, RtFloat ymin, RtFloat ymax)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiCropWindow(xmin, xmax, ymin, ymax);
    FUNC_END()
}
RtVoid RiProjection(RtToken name, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, name );
    // GetCurrentContext()->RiProjection(name, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiProjectionV(RtToken name, RtInt n, RtToken tokens[],
                     RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiProjectionV(name, n, tokens, params);
    FUNC_END()
}
RtVoid RiClipping(RtFloat hither, RtFloat yon)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiClipping(hither, yon);
    FUNC_END()
}
RtVoid RiClippingPlane(RtFloat x, RtFloat y, RtFloat z, RtFloat nx, RtFloat ny,
                       RtFloat nz)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiClippingPlane(x, y, z, nx, ny, nz);
    FUNC_END()
}
RtVoid RiShutter(RtFloat min, RtFloat max)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiShutter(min, max);
    FUNC_END()
}
RtVoid RiPixelVariance(RtFloat variation)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiPixelVariance(variation);
    FUNC_END()
}
RtVoid RiPixelSamples(RtFloat xsamples, RtFloat ysamples)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiPixelSamples(xsamples, ysamples);
    FUNC_END()
}
RtVoid RiPixelFilter(RtFilterFunc filterfunc, RtFloat xwidth, RtFloat ywidth)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiPixelFilter(filterfunc, xwidth, ywidth);
    FUNC_END()
}
RtVoid RiExposure(RtFloat gain, RtFloat gamma)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiExposure(gain, gamma);
    FUNC_END()
}
RtVoid RiImager(RtToken name, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, name );
    // GetCurrentContext()->RiImager(name, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiImagerV(RtToken name, RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiImagerV(name, n, tokens, params);
    FUNC_END()
}
RtVoid RiQuantize(RtToken type, RtInt one, RtInt min, RtInt max, RtFloat ampl)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiQuantize(type, one, min, max, ampl);
    FUNC_END()
}
RtVoid RiDisplay(char* name, RtToken type, RtToken mode, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, mode );
    // GetCurrentContext()->RiDisplay(name, type, mode, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiDisplayV(char* name, RtToken type, RtToken mode, RtInt n,
                  RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiDisplayV(name, type, mode, n, tokens, params);
    FUNC_END()
}
RtVoid RiDisplayChannel(RtToken channel, ...)
{
    FUNC_BEGIN()
    //
    FUNC_END()
}
RtVoid RiDisplayChannelV(RtToken channel, RtInt n, RtToken tokens[],
                         RtPointer params[])
{
    FUNC_BEGIN()
    //
    FUNC_END()
}
RtVoid RiHider(RtToken type, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, type );
    // GetCurrentContext()->RiHider(type, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiHiderV(RtToken type, RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiHiderV(type, n, tokens, params);
    FUNC_END()
}
RtVoid RiColorSamples(RtInt n, RtFloat nRGB[], RtFloat RGBn[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiColorSamples(n, nRGB, RGBn);
    FUNC_END()
}
RtVoid RiRelativeDetail(RtFloat relativedetail)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiRelativeDetail(relativedetail);
    FUNC_END()
}
RtVoid RiOption(RtToken name, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, name );
    // GetCurrentContext()->RiOption(name, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiOptionV(RtToken name, RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiOptionV(name, n, tokens, params);
    FUNC_END()
}
RtVoid RiAttributeBegin(void)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiAttributeBegin();
    FUNC_END()
}
RtVoid RiAttributeEnd(void)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiAttributeEnd();
    FUNC_END()
}
RtVoid RiColor(RtColor color)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiColor(color);
    FUNC_END()
}
RtVoid RiOpacity(RtColor color)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiOpacity(color);
    FUNC_END()
}
RtVoid RiTextureCoordinates(RtFloat s1, RtFloat t1, RtFloat s2, RtFloat t2,
                            RtFloat s3, RtFloat t3, RtFloat s4, RtFloat t4)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiTextureCoordinates(s1, t1, s2, t2, s3, t3, s4, t4);
    FUNC_END()
}
RtLightHandle RiLightSource(RtToken name, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, name );
    // RtLightHandle h = //GetCurrentContext()->RiLightSource(name, list);
    // va_end(list);
    // return h;
    FUNC_END()
    return NULL;
}
RtLightHandle RiLightSourceV(RtToken name, RtInt n, RtToken tokens[],
                             RtPointer params[])
{
    FUNC_BEGIN()
    // return //GetCurrentContext()->RiLightSourceV(name, n, tokens, params);
    FUNC_END()
    return NULL;
}
RtLightHandle RiAreaLightSource(RtToken name, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, name );
    // RtLightHandle h = //GetCurrentContext()->RiAreaLightSource(name, list);
    // va_end(list);
    // return h;
    FUNC_END()
    return NULL;
}
RtLightHandle RiAreaLightSourceV(RtToken name, RtInt n, RtToken tokens[],
                                 RtPointer params[])
{
    FUNC_BEGIN()
    // return //GetCurrentContext()->RiAreaLightSourceV(name, n, tokens, params);
    FUNC_END()
    return NULL;
}
RtVoid RiIlluminate(RtLightHandle light, RtBoolean onoff)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiIlluminate(light, onoff);
    FUNC_END()
}
RtVoid RiSurface(RtToken name, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, name );
    // GetCurrentContext()->RiSurface(name, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiSurfaceV(RtToken name, RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiSurfaceV(name, n, tokens, params);
    FUNC_END()
}
RtVoid RiAtmosphere(RtToken name, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, name );
    // GetCurrentContext()->RiAtmosphere(name, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiAtmosphereV(RtToken name, RtInt n, RtToken tokens[],
                     RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiAtmosphereV(name, n, tokens, params);
    FUNC_END()
}
RtVoid RiInterior(RtToken name, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, name );
    // GetCurrentContext()->RiInterior(name, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiInteriorV(RtToken name, RtInt n, RtToken tokens[],
                   RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiInteriorV(name, n, tokens, params);
    FUNC_END()
}
RtVoid RiExterior(RtToken name, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, name );
    // GetCurrentContext()->RiExterior(name, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiExteriorV(RtToken name, RtInt n, RtToken tokens[],
                   RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiExteriorV(name, n, tokens, params);
    FUNC_END()
}
RtVoid RiShadingRate(RtFloat size)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiShadingRate(size);
    FUNC_END()
}
RtVoid RiShadingInterpolation(RtToken type)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiShadingInterpolation(type);
    FUNC_END()
}
RtVoid RiMatte(RtBoolean onoff)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiMatte(onoff);
    FUNC_END()
}
RtVoid RiBound(RtBound bound)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiBound(bound);
    FUNC_END()
}
RtVoid RiDetail(RtBound bound)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiDetail(bound);
    FUNC_END()
}
RtVoid RiDetailRange(RtFloat minvis, RtFloat lowtran, RtFloat uptran,
                     RtFloat maxvis)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiDetailRange(minvis, lowtran, uptran, maxvis);
    FUNC_END()
}
RtVoid RiGeometricApproximation(RtToken type, RtFloat value)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiGeometricApproximation(type, value);
    FUNC_END()
}
RtVoid RiOrientation(RtToken orientation)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiOrientation(orientation);
    FUNC_END()
}
RtVoid RiReverseOrientation(void)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiReverseOrientation();
    FUNC_END()
}
RtVoid RiSides(RtInt sides)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiSides(sides);
    FUNC_END()
}
RtVoid RiIdentity(void)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiIdentity();
    FUNC_END()
}
RtVoid RiTransform(RtMatrix transform)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiTransform(transform);
    FUNC_END()
}
RtVoid RiConcatTransform(RtMatrix transform)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiConcatTransform(transform);
    FUNC_END()
}
RtVoid RiPerspective(RtFloat fov)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiPerspective(fov);
    FUNC_END()
}
RtVoid RiTranslate(RtFloat dx, RtFloat dy, RtFloat dz)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiTranslate(dx, dy, dz);
    FUNC_END()
}
RtVoid RiRotate(RtFloat angle, RtFloat dx, RtFloat dy, RtFloat dz)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiRotate(angle, dx, dy, dz);
    FUNC_END()
}
RtVoid RiScale(RtFloat sx, RtFloat sy, RtFloat sz)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiScale(sx, sy, sz);
    FUNC_END()
}
RtVoid RiSkew(RtFloat angle, RtFloat dx1, RtFloat dy1, RtFloat dz1, RtFloat dx2,
              RtFloat dy2, RtFloat dz2)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiSkew(angle, dx1, dy1, dz1, dx2, dy2, dz2);
    FUNC_END()
}
RtVoid RiDeformation(RtToken name, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, name );
    // GetCurrentContext()->RiDeformation(name, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiDeformationV(RtToken name, RtInt n, RtToken tokens[],
                      RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiDeformationV(name, n, tokens, params);
    FUNC_END()
}
RtVoid RiDisplacement(RtToken name, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, name );
    // GetCurrentContext()->RiDisplacement(name, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiDisplacementV(RtToken name, RtInt n, RtToken tokens[],
                       RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiDisplacementV(name, n, tokens, params);
    FUNC_END()
}
RtVoid RiCoordinateSystem(RtToken space)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiCoordinateSystem(space);
    FUNC_END()
}
RtVoid RiScopedCoordinateSystem(RtToken space)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiCoordinateSystem(space);
    FUNC_END()
}
RtVoid RiCoordSysTransform(RtToken space)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiCoordSysTransform(space);
    FUNC_END()
}
RtPoint* RiTransformPoints(RtToken fromspace, RtToken tospace, RtInt n,
                           RtPoint points[])
{
    FUNC_BEGIN()
    // return //GetCurrentContext()->RiTransformPoints(fromspace, tospace, n,
    // points);
    FUNC_END()
    return NULL;
}
RtVoid RiTransformBegin(void)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiTransformBegin();
    FUNC_END()
}
RtVoid RiTransformEnd(void)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiTransformEnd();
    FUNC_END()
}

RtVoid RiResource(RtToken handle, RtToken type, ...)
{
    FUNC_BEGIN()
    //
    FUNC_END()
}
RtVoid RiResourceV(RtToken handle, RtToken type, RtInt n, RtPoint points[])
{
    FUNC_BEGIN()
    //
    FUNC_END()
}
RtVoid RiResourceBegin(void)
{
    FUNC_BEGIN()
    //
    FUNC_END()
}
RtVoid RiResourceEnd(void)
{
    FUNC_BEGIN()
    //
    FUNC_END()
}

RtVoid RiAttribute(RtToken name, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, name );
    // GetCurrentContext()->RiAttribute(name, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiAttributeV(RtToken name, RtInt n, RtToken tokens[],
                    RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiAttributeV(name, n, tokens, params);
    FUNC_END()
}
RtVoid RiPolygon(RtInt nverts, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, nverts );
    // GetCurrentContext()->RiPolygon(nverts, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiPolygonV(RtInt nverts, RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiPolygonV(nverts, n, tokens, params);
    FUNC_END()
}
RtVoid RiGeneralPolygon(RtInt nloops, RtInt nverts[], ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, nverts );
    // GetCurrentContext()->RiGeneralPolygon(nloops, nverts, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiGeneralPolygonV(RtInt nloops, RtInt nverts[], RtInt n,
                         RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiGeneralPolygonV(nloops, nverts, n, tokens, params);
    FUNC_END()
}
RtVoid RiPointsPolygons(RtInt npolys, RtInt nverts[], RtInt verts[], ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, verts );
    // GetCurrentContext()->RiPointsPolygons(npolys, nverts, verts, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiPointsPolygonsV(RtInt npolys, RtInt nverts[], RtInt verts[], RtInt n,
                         RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiPointsPolygonsV(npolys, nverts, verts, n, tokens,
    // params);
    FUNC_END()
}
RtVoid RiPointsGeneralPolygons(RtInt npolys, RtInt nloops[], RtInt nverts[],
                               RtInt verts[], ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, verts );
    // GetCurrentContext()->RiPointsGeneralPolygons(npolys, nloops, nverts, verts,
    // list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiPointsGeneralPolygonsV(RtInt npolys, RtInt nloops[], RtInt nverts[],
                                RtInt verts[], RtInt n, RtToken tokens[],
                                RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiPointsGeneralPolygonsV(npolys, nloops, nverts,
    // verts, n, tokens, params);
    FUNC_END()
}
RtVoid RiBasis(RtBasis ubasis, RtInt ustep, RtBasis vbasis, RtInt vstep)
{
    FUNC_BEGIN()

    FUNC_END()
}
RtVoid RiPatch(RtToken type, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, type );
    // GetCurrentContext()->RiPatch(type, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiPatchV(RtToken type, RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiPatchV(type, n, tokens, params);
    FUNC_END()
}
RtVoid RiPatchMesh(RtToken type, RtInt nu, RtToken uwrap, RtInt nv,
                   RtToken vwrap, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, vwrap );
    // GetCurrentContext()->RiPatchMesh(type, nu, uwrap, nv, vwrap, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiPatchMeshV(RtToken type, RtInt nu, RtToken uwrap, RtInt nv,
                    RtToken vwrap, RtInt n, RtToken tokens[],
                    RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiPatchMeshV(type, nu, uwrap, nv, vwrap, n, tokens,
    // params);
    FUNC_END()
}
RtVoid RiNuPatch(RtInt nu, RtInt uorder, RtFloat uknot[], RtFloat umin,
                 RtFloat umax, RtInt nv, RtInt vorder, RtFloat vknot[],
                 RtFloat vmin, RtFloat vmax, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, vmax );
    // GetCurrentContext()->RiNuPatch(nu, uorder, uknot, umin, umax, nv, vorder,
    // vknot, vmin, vmax, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiNuPatchV(RtInt nu, RtInt uorder, RtFloat uknot[], RtFloat umin,
                  RtFloat umax, RtInt nv, RtInt vorder, RtFloat vknot[],
                  RtFloat vmin, RtFloat vmax, RtInt n, RtToken tokens[],
                  RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiNuPatchV(nu, uorder, uknot, umin, umax, nv, vorder,
    // vknot, vmin, vmax, n, tokens, params);
    FUNC_END()
}
RtVoid RiTrimCurve(RtInt nloops, RtInt ncurves[], RtInt order[], RtFloat knot[],
                   RtFloat min[], RtFloat max[], RtInt n[], RtFloat u[],
                   RtFloat v[], RtFloat w[])
{
    FUNC_BEGIN()

    FUNC_END()
}
RtVoid RiSphere(RtFloat radius, RtFloat zmin, RtFloat zmax, RtFloat tmax, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, tmax );
    // GetCurrentContext()->RiSphere(radius, zmin, zmax, tmax, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiSphereV(RtFloat radius, RtFloat zmin, RtFloat zmax, RtFloat tmax,
                 RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiSphereV(radius, zmin, zmax, tmax, n, tokens,
    // params);
    FUNC_END()
}
RtVoid RiCone(RtFloat height, RtFloat radius, RtFloat tmax, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, tmax );
    // GetCurrentContext()->RiCone(height, radius, tmax, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiConeV(RtFloat height, RtFloat radius, RtFloat tmax, RtInt n,
               RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiConeV(height, radius, tmax, n, tokens, params);
    FUNC_END()
}
RtVoid RiCylinder(RtFloat radius, RtFloat zmin, RtFloat zmax, RtFloat tmax,
                  ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, tmax );
    // GetCurrentContext()->RiCylinder(radius, zmin, zmax, tmax, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiCylinderV(RtFloat radius, RtFloat zmin, RtFloat zmax, RtFloat tmax,
                   RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiCylinderV(radius, zmin, zmax, tmax, n, tokens,
    // params);
    FUNC_END()
}
RtVoid RiHyperboloid(RtPoint point1, RtPoint point2, RtFloat tmax, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, tmax );
    // GetCurrentContext()->RiHyperboloid(point1, point2, tmax, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiHyperboloidV(RtPoint point1, RtPoint point2, RtFloat tmax, RtInt n,
                      RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiHyperboloidV(point1, point2, tmax, n, tokens,
    // params);
    FUNC_END()
}
RtVoid RiParaboloid(RtFloat rmax, RtFloat zmin, RtFloat zmax, RtFloat tmax,
                    ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, tmax );
    // GetCurrentContext()->RiParaboloid(rmax, zmin, zmax, tmax, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiParaboloidV(RtFloat rmax, RtFloat zmin, RtFloat zmax, RtFloat tmax,
                     RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiParaboloidV(rmax, zmin, zmax, tmax, n, tokens,
    // params);
    FUNC_END()
}
RtVoid RiDisk(RtFloat height, RtFloat radius, RtFloat tmax, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, tmax );
    // GetCurrentContext()->RiDisk(height, radius, tmax, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiDiskV(RtFloat height, RtFloat radius, RtFloat tmax, RtInt n,
               RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiDiskV(height, radius, tmax, n, tokens, params);
    FUNC_END()
}
RtVoid RiTorus(RtFloat majrad, RtFloat minrad, RtFloat phimin, RtFloat phimax,
               RtFloat tmax, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, tmax );
    // GetCurrentContext()->RiTorus(majrad, minrad, phimin, phimax, tmax, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiTorusV(RtFloat majrad, RtFloat minrad, RtFloat phimin, RtFloat phimax,
                RtFloat tmax, RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiTorusV(majrad, minrad, phimin, phimax, tmax, n,
    // tokens, params);
    FUNC_END()
}
RtVoid RiBlobby(RtInt nleaf, RtInt ncode, RtInt code[], RtInt nflt,
                RtFloat flt[], RtInt nstr, RtToken str[], ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, str );
    // GetCurrentContext()->RiBlobby(nleaf, ncode, code, nflt, flt, nstr, str,
    // list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiBlobbyV(RtInt nleaf, RtInt ncode, RtInt code[], RtInt nflt,
                 RtFloat flt[], RtInt nstr, RtToken str[], RtInt n,
                 RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiBlobbyV(nleaf, ncode, code, nflt, flt, nstr, str, n,
    // tokens, params);
    FUNC_END()
}
RtVoid RiCurves(RtToken type, RtInt ncurves, RtInt nvertices[], RtToken wrap,
                ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, wrap );
    // GetCurrentContext()->RiCurves(type, ncurves, nvertices, wrap, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiCurvesV(RtToken type, RtInt ncurves, RtInt nvertices[], RtToken wrap,
                 RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiCurvesV(type, ncurves, nvertices, wrap, n, tokens,
    // params);
    FUNC_END()
}
RtVoid RiPoints(RtInt nverts, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, nverts );
    // GetCurrentContext()->RiPoints(nverts, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiPointsV(RtInt nverts, RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiPointsV(nverts, n, tokens, params);
    FUNC_END()
}
RtVoid RiSubdivisionMesh(RtToken mask, RtInt nf, RtInt nverts[], RtInt verts[],
                         RtInt ntags, RtToken tags[], RtInt numargs[],
                         RtInt intargs[], RtFloat floatargs[], ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, floatargs );
    // GetCurrentContext()->RiSubdivisionMesh(mask, nf, nverts, verts, ntags,
    // tags, numargs, intargs, floatargs, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiSubdivisionMeshV(RtToken mask, RtInt nf, RtInt nverts[], RtInt verts[],
                          RtInt ntags, RtToken tags[], RtInt nargs[],
                          RtInt intargs[], RtFloat floatargs[], RtInt n,
                          RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiSubdivisionMeshV(mask, nf, nverts, verts, ntags,
    // tags, nargs, intargs, floatargs, n, tokens, params);
    FUNC_END()
}
RtVoid RiHierarchicalSubdivisionMesh(RtToken scheme, RtInt nfaces,
                                     RtInt nvertices[], RtInt vertices[],
                                     RtInt ntags, RtToken tags[], RtInt nargs[],
                                     RtInt intargs[], RtFloat floatargs[],
                                     RtString stringargs[], ...)
{
    FUNC_BEGIN()
    //
    FUNC_END()
}
RtVoid RiHierarchicalSubdivisionMeshV(RtToken scheme, RtInt nfaces,
                                      RtInt nvertices[], RtInt vertices[],
                                      RtInt ntags, RtToken tags[],
                                      RtInt nargs[], RtInt intargs[],
                                      RtFloat floatargs[],
                                      RtString stringargs[], RtInt n,
                                      RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    //
    FUNC_END()
}
RtVoid RiProcedural(RtPointer data, RtBound bound,
                    RtVoid (*subdivfunc)(RtPointer, RtFloat),
                    RtVoid (*freefunc)(RtPointer))
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiProcedural(data, bound, subdivfunc, freefunc);
    FUNC_END()
}
RtVoid RiGeometry(RtToken type, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, type );
    // GetCurrentContext()->RiGeometry(type, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiGeometryV(RtToken type, RtInt n, RtToken tokens[],
                   RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiGeometryV(type, n, tokens, params);
    FUNC_END()
}
RtVoid RiSolidBegin(RtToken operation)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiSolidBegin(operation);
    FUNC_END()
}
RtVoid RiSolidEnd(void)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiSolidEnd();
    FUNC_END()
}
RtObjectHandle RiObjectBegin(void)
{
    FUNC_BEGIN()
    // return //GetCurrentContext()->RiObjectBegin();
    FUNC_END()
    return NULL;
}
RtVoid RiObjectEnd(void)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiObjectEnd();
    FUNC_END()
}
RtVoid RiObjectInstance(RtObjectHandle handle)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiObjectInstance(handle);
    FUNC_END()
}
RtVoid RiMotionBegin(RtInt n, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, n );
    // GetCurrentContext()->RiMotionBegin(n, list);
    // va_end(list);
    FUNC_END()
}
RtVoid RiMotionBeginV(RtInt n, RtFloat times[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiMotionBeginV(n, times);
    FUNC_END()
}
RtVoid RiMotionEnd(void)
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiMotionEnd();
    FUNC_END()
}
RtVoid RiMakeTexture(char* pic, char* tex, RtToken swrap, RtToken twrap,
                     RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth,
                     ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, twidth );
    // GetCurrentContext()->RiMakeTexture(pic, tex, swrap, twrap, filterfunc,
    // swidth, twidth, list);
    // va_end( list );
    FUNC_END()
}
RtVoid RiMakeTextureV(char* pic, char* tex, RtToken swrap, RtToken twrap,
                      RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth,
                      RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiMakeTextureV(pic, tex, swrap, twrap, filterfunc,
    // swidth, twidth, n, tokens, params);
    FUNC_END()
}
RtVoid RiMakeBump(char* pic, char* tex, RtToken swrap, RtToken twrap,
                  RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth,
                  ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, twidth );
    // GetCurrentContext()->RiMakeBump(pic, tex, swrap, twrap, filterfunc, swidth,
    // twidth, list);
    // va_end( list );
    FUNC_END()
}
RtVoid RiMakeBumpV(char* pic, char* tex, RtToken swrap, RtToken twrap,
                   RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth,
                   RtInt n, RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiMakeBumpV(pic, tex, swrap, twrap, filterfunc,
    // swidth, twidth, n, tokens, params);
    FUNC_END()
}
RtVoid RiMakeLatLongEnvironment(char* pic, char* tex, RtFilterFunc filterfunc,
                                RtFloat swidth, RtFloat twidth, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, twidth );
    // GetCurrentContext()->RiMakeLatLongEnvironment(pic, tex, filterfunc, swidth,
    // twidth, list);
    // va_end( list );
    FUNC_END()
}
RtVoid RiMakeLatLongEnvironmentV(char* pic, char* tex, RtFilterFunc filterfunc,
                                 RtFloat swidth, RtFloat twidth, RtInt n,
                                 RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiMakeLatLongEnvironmentV(pic, tex, filterfunc,
    // swidth, twidth, n, tokens, params);
    FUNC_END()
}
RtVoid RiMakeCubeFaceEnvironment(char* px, char* nx, char* py, char* ny,
                                 char* pz, char* nz, char* tex, RtFloat fov,
                                 RtFilterFunc filterfunc, RtFloat swidth,
                                 RtFloat ywidth, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, ywidth );
    // GetCurrentContext()->RiMakeCubeFaceEnvironment(px,nx,py,ny,pz,nz,tex,fov,filterfunc,swidth,ywidth,
    // list);
    // va_end( list );
    FUNC_END()
}
RtVoid RiMakeCubeFaceEnvironmentV(char* px, char* nx, char* py, char* ny,
                                  char* pz, char* nz, char* tex, RtFloat fov,
                                  RtFilterFunc filterfunc, RtFloat swidth,
                                  RtFloat ywidth, RtInt n, RtToken tokens[],
                                  RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiMakeCubeFaceEnvironmentV(px,nx,py,ny,pz,nz,tex,fov,filterfunc,swidth,ywidth,n,tokens,params);
    FUNC_END()
}
RtVoid RiMakeShadow(char* pic, char* tex, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, tex );
    // GetCurrentContext()->RiMakeShadow(pic, tex, list);
    // va_end( list );
    FUNC_END()
}
RtVoid RiMakeShadowV(char* pic, char* tex, RtInt n, RtToken tokens[],
                     RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiMakeShadowV(pic, tex, n, tokens, params);
    FUNC_END()
}
RtVoid RiMakeBrickMap(int nptc, char** ptcnames, char* bkmname, ...)
{
    FUNC_BEGIN()
    //
    FUNC_END()
}
RtVoid RiMakeBrickMapV(int nptc, char** ptcnames, char* bkmname, RtInt n,
                       RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    //
    FUNC_END()
}

RtVoid RiArchiveRecord(RtToken type, char* format, ...)
{
    ; //
}
// V
RtVoid RiReadArchive(RtToken name, RtArchiveCallback callback, ...)
{
    FUNC_BEGIN()
    // va_list list;
    // va_start( list, callback );
    // GetCurrentContext()->RiReadArchive(name, callback, list);
    // va_end( list );
    FUNC_END()
}
RtVoid RiReadArchiveV(RtToken name, RtArchiveCallback callback, RtInt n,
                      RtToken tokens[], RtPointer params[])
{
    FUNC_BEGIN()
    // GetCurrentContext()->RiReadArchiveV(name, callback, n, tokens, params);
    FUNC_END()
}

RtToken RI_FRAMEBUFFER = (RtToken)"framebuffer";
RtToken RI_FILE = (RtToken)"file";
RtToken RI_RGB = (RtToken)"rgb";
RtToken RI_RGBA = (RtToken)"rgba";
RtToken RI_RGBZ = (RtToken)"rgbz";
RtToken RI_RGBAZ = (RtToken)"rgbaz";
RtToken RI_A = (RtToken)"a";
RtToken RI_Z = (RtToken)"z";
RtToken RI_AZ = (RtToken)"az";
RtToken RI_MERGE = (RtToken)"merge";
RtToken RI_ORIGIN = (RtToken)"origin";
RtToken RI_PERSPECTIVE = (RtToken)"perspective";
RtToken RI_ORTHOGRAPHIC = (RtToken)"orthographic";
RtToken RI_HIDDEN = (RtToken)"hidden";
RtToken RI_PAINT = (RtToken)"paint";
RtToken RI_CONSTANT = (RtToken)"constant";
RtToken RI_SMOOTH = (RtToken)"smooth";
RtToken RI_FLATNESS = (RtToken)"flatness";
RtToken RI_FOV = (RtToken)"fov";
RtToken RI_AMBIENTLIGHT = (RtToken)"ambientlight";
RtToken RI_POINTLIGHT = (RtToken)"pointlight";
RtToken RI_DISTANTLIGHT = (RtToken)"distantlight";
RtToken RI_SPOTLIGHT = (RtToken)"spotlight";
RtToken RI_INTENSITY = (RtToken)"intensity";
RtToken RI_LIGHTCOLOR = (RtToken)"lightcolor";
RtToken RI_FROM = (RtToken)"from";
RtToken RI_TO = (RtToken)"to";
RtToken RI_CONEANGLE = (RtToken)"coneangle";
RtToken RI_CONEDELTAANGLE = (RtToken)"conedeltaangle";
RtToken RI_BEAMDISTRIBUTION = (RtToken)"beamdistribution";
RtToken RI_MATTE = (RtToken)"matte";
RtToken RI_METAL = (RtToken)"metal";
RtToken RI_SHINYMETAL = (RtToken)"shinymetal";
RtToken RI_PLASTIC = (RtToken)"plastic";
RtToken RI_PAINTEDPLASTIC = (RtToken)"paintedplastic";
RtToken RI_KA = (RtToken)"Ka";
RtToken RI_KD = (RtToken)"Kd";
RtToken RI_KS = (RtToken)"Ks";
RtToken RI_ROUGHNESS = (RtToken)"roughness";
RtToken RI_KR = (RtToken)"Kr";
RtToken RI_TEXTURENAME = (RtToken)"texturename";
RtToken RI_SPECULARCOLOR = (RtToken)"specularcolor";
RtToken RI_DEPTHCUE = (RtToken)"depthcue";
RtToken RI_FOG = (RtToken)"fog";
RtToken RI_BUMPY = (RtToken)"bumpy";
RtToken RI_MINDISTANCE = (RtToken)"mindistance";
RtToken RI_MAXDISTANCE = (RtToken)"maxdistance";
RtToken RI_BACKGROUND = (RtToken)"background";
RtToken RI_DISTANCE = (RtToken)"distance";
RtToken RI_AMPLITUDE = (RtToken)"amplitude";
RtToken RI_RASTER = (RtToken)"raster";
RtToken RI_SCREEN = (RtToken)"screen";
RtToken RI_CAMERA = (RtToken)"camera";
RtToken RI_WORLD = (RtToken)"world";
RtToken RI_OBJECT = (RtToken)"object";
RtToken RI_INSIDE = (RtToken)"inside";
RtToken RI_OUTSIDE = (RtToken)"outside";
RtToken RI_LH = (RtToken)"lh";
RtToken RI_RH = (RtToken)"rh";
RtToken RI_P = (RtToken)"P";
RtToken RI_PZ = (RtToken)"Pz";
RtToken RI_PW = (RtToken)"Pw";
RtToken RI_N = (RtToken)"N";
RtToken RI_NP = (RtToken)"Np";
RtToken RI_CS = (RtToken)"Cs";
RtToken RI_OS = (RtToken)"Os";
RtToken RI_S = (RtToken)"s";
RtToken RI_T = (RtToken)"t";
RtToken RI_ST = (RtToken)"st";
RtToken RI_BILINEAR = (RtToken)"bilinear";
RtToken RI_BICUBIC = (RtToken)"bicubic";
RtToken RI_LINEAR = (RtToken)"linear";
RtToken RI_CUBIC = (RtToken)"cubic";
RtToken RI_PRIMITIVE = (RtToken)"primitive";
RtToken RI_INTERSECTION = (RtToken)"intersection";
RtToken RI_UNION = (RtToken)"union";
RtToken RI_DIFFERENCE = (RtToken)"difference";
RtToken RI_WRAP = (RtToken)"wrap";
RtToken RI_NOWRAP = (RtToken)"nowrap";
RtToken RI_PERIODIC = (RtToken)"periodic";
RtToken RI_NONPERIODIC = (RtToken)"nonperiodic";
RtToken RI_CLAMP = (RtToken)"clamp";
RtToken RI_BLACK = (RtToken)"black";
RtToken RI_IGNORE = (RtToken)"ignore";
RtToken RI_PRINT = (RtToken)"print";
RtToken RI_ABORT = (RtToken)"abort";
RtToken RI_HANDLER = (RtToken)"handler";
RtToken RI_COMMENT = (RtToken)"comment";
RtToken RI_STRUCTURE = (RtToken)"structure";
RtToken RI_VERBATIM = (RtToken)"verbatim";
RtToken RI_IDENTIFIER = (RtToken)"identifier";
RtToken RI_NAME = (RtToken)"name";
RtToken RI_SHADINGGROUP = (RtToken)"shadinggroup";
RtToken RI_WIDTH = (RtToken)"width";
RtToken RI_CONSTANTWIDTH = (RtToken)"constantwidth";
RtToken RI_CURRENT = (RtToken)"current";
RtToken RI_SHADER = (RtToken)"shader";
RtToken RI_EYE = (RtToken)"eye";
RtToken RI_NDC = (RtToken)"ndc";

RtToken RI_SHADOWNAME = (RtToken)"shadowname";
RtToken RI_BLUR = (RtToken)"blur";
RtToken RI_BIQUADRATIC = (RtToken)"qudaratic";
RtToken RI_QUADRATIC = (RtToken)"biqudaratic";

RtToken RI_BEZIER = (RtToken)"bezier";
RtToken RI_BSPLINE = (RtToken)"bspline";
RtToken RI_CATMULLROM = (RtToken)"catmull-rom";
RtToken RI_HERMITE = (RtToken)"hermite";
RtToken RI_POWER = (RtToken)"power";

RtToken RI_GAUSSIAN = (RtToken)"gaussian";
RtToken RI_BOX = (RtToken)"box";
RtToken RI_TRIANGLE = (RtToken)"triangle";
// RtToken RI_CATMULLROM = (RtToken)"catmullrom";
RtToken RI_SINC = (RtToken)"sinc";

#ifdef __cplusplus
}
#endif
