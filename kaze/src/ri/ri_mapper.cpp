#include "ri_mapper.h"
#include <sstream>

namespace ri
{

    static std::string ToString(int id)
    {
        std::stringstream ss;
        ss << id;
        return ss.str();
    }

    static int ToInteger(const char* s)
    {
        if (s)
        {
            std::stringstream ss;
            ss << s;
            int i = 0;
            ss >> i;
            return i;
        }
        return -1;
    }

    void ri_mapper::set(int id, void* p)
    {
        std::string s = ToString(id);
        this->set(s.c_str(), p);
    }
    void ri_mapper::set(const char* name, void* p)
    {
        s2p_[name] = p;
        p2s_[p] = name;
    }
    const char* ri_mapper::get_str(void* p) const
    {
        std::map<void*, std::string>::const_iterator it = p2s_.find(p);
        if (it != p2s_.end())
        {
            return it->second.c_str();
        }
        else
        {
            return NULL;
        }
    }

    int ri_mapper::get_int(void* p) const
    {
        const char* s = get_str(p);
        if (s)
        {
            return ToInteger(s);
        }
        else
        {
            return -1;
        }
    }

    void* ri_mapper::get_ptr(int id) const
    {
        std::string s = ToString(id);
        return this->get_ptr(s.c_str());
    }

    void* ri_mapper::get_ptr(const char* name) const
    {
        std::map<std::string, void*>::const_iterator it = s2p_.find(name);
        if (it != s2p_.end())
        {
            return it->second;
        }
        else
        {
            return NULL;
        }
    }
}
