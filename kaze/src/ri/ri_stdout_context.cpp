#include "ri_stdout_context.h"

#include "ri_read_archive.h"

#include <assert.h>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif
#include <string>
#include <stdlib.h>
#include <string.h>

#if defined(_MSC_VER)
#if (_MSC_VER < 1400) // msc_ver < vc2005
#define SNPRINTF _snprintf
#else
#define SNPRINTF _snprintf_s
#endif
#else
#define SNPRINTF snprintf
#endif

#define PR(x) fprintf(fp_, "%s", x)
#define PI(x) fprintf(fp_, "%d", x)
#define PF(x) fprintf(fp_, "%f", x)
#define PS(x) fprintf(fp_, "\"%s\"", x)
#define S fprintf(fp_, " ")
#define N fprintf(fp_, "\n")

#define DUMMY_ 1

static std::string ArrayToString(const RtInt ar[], int n)
{
    char buffer[512];

    std::string sRet;
    sRet.reserve(n * 32);

    sRet += "[";
    for (int i = 0; i < n; i++)
    {
        SNPRINTF(buffer, sizeof(buffer), "%d", ar[i]);

        sRet += buffer;
        if (i != n - 1)
        {
            sRet += " ";
        }
    }
    sRet += "]";
    return sRet;
}

static std::string ArrayToString(const RtFloat ar[], int n)
{
    char buffer[512];

    std::string sRet;
    sRet.reserve(n * 32);

    sRet += "[";
    for (int i = 0; i < n; i++)
    {
        SNPRINTF(buffer, sizeof(buffer), "%f", ar[i]);

        sRet += buffer;
        if (i != n - 1)
        {
            sRet += " ";
        }
    }
    sRet += "]";
    return sRet;
}

static std::string ArrayToString(const RtToken ar[], int n)
{
    char buffer[512];

    std::string sRet;
    sRet.reserve(n * 32);

    sRet += "[";
    for (int i = 0; i < n; i++)
    {
        SNPRINTF(buffer, sizeof(buffer), "\"%s\"", ar[i]);

        sRet += buffer;
        if (i != n - 1)
        {
            sRet += " ";
        }
    }
    sRet += "]";
    return sRet;
}

static int SumArray(const RtInt ar[], int n)
{
    int nRet = 0;
    for (int i = 0; i < n; i++)
    {
        nRet += ar[i];
    }
    return nRet;
}

static std::string MatrixToString(RtMatrix m)
{
    RtFloat* f = (RtFloat*)m;
    return ArrayToString(f, 16);
}

static std::string ColorToString(RtColor c)
{
    RtFloat* f = (RtFloat*)c;
    return ArrayToString(f, 3);
}

static std::string BoundToString(RtBound b)
{
    RtFloat* f = (RtFloat*)b;
    return ArrayToString(f, 6);
}

struct PTMap
{
    void* ptr;
    RtToken name;
};

static RtToken PointerToToken(const struct PTMap FUNCS[], void* ptr)
{
    int i = 0;
    while (FUNCS[i].ptr)
    {
        if (FUNCS[i].ptr == ptr)
            return FUNCS[i].name;
        i++;
    }
    return NULL;
}

static struct PTMap FILTERS[] = {{(void*)RiGaussianFilter, RI_GAUSSIAN},
                                 {(void*)RiBoxFilter, RI_BOX},
                                 {(void*)RiTriangleFilter, RI_TRIANGLE},
                                 {(void*)RiCatmullRomFilter, RI_CATMULLROM},
                                 {(void*)RiSincFilter, RI_SINC},
                                 {NULL, RI_NULL}};

static struct PTMap ERROR_HANDLERS[] = {{(void*)RiErrorIgnore, RI_IGNORE},
                                        {(void*)RiErrorPrint, RI_PRINT},
                                        {(void*)RiErrorAbort, RI_ABORT},
                                        {NULL, RI_NULL}};

static const struct PTMap BASISES[] = {
    {(void*)RiBezierBasis, RI_BEZIER},
    {(void*)RiBSplineBasis, "bspline"}, //
    {(void*)RiBSplineBasis, "b-spline"},
    {(void*)RiCatmullRomBasis, "catmull-rom"}, //
    {(void*)RiCatmullRomBasis, "catmullrom"},
    {(void*)RiHermiteBasis, RI_HERMITE},
    {(void*)RiPowerBasis, RI_POWER},
    {NULL, RI_NULL},
};

static struct PTMap PROCEDURALS[] = {
    {(void*)RiProcDelayedReadArchive, "DelayedReadArchive"},
    {(void*)RiProcRunProgram, "RunProgram"},
    {(void*)RiProcDynamicLoad, "DynamicLoad"},
    {NULL, RI_NULL}};

static RtToken BasisToToken(RtBasis b)
{
    const RtFloat* f = (const RtFloat*)b;
    int i = 0;
    while (BASISES[i].ptr)
    {
        const RtFloat* fp = (const RtFloat*)(BASISES[i].ptr);
        if (memcmp(fp, f, sizeof(RtFloat) * 16) == 0)
            return BASISES[i].name;
        i++;
    }
    return RI_NULL;
}

static std::string BasisToString(RtBasis b)
{
    RtToken name = BasisToToken(b);
    if (name)
        return name;

    const RtFloat* f = (const RtFloat*)b;
    return ArrayToString(f, 16);
}

static std::string GetFilerFuncName(RtFilterFunc f)
{
    RtToken name = PointerToToken(FILTERS, (void*)f);
    if (name)
        return name;
    else
        return "unknown";
}

static std::string GetErrorHandlerName(RtErrorHandler f)
{
    RtToken name = PointerToToken(ERROR_HANDLERS, (void*)f);
    if (name)
        return name;
    else
        return "unknown";
}

//---------------------------------------------------------------------------------------

namespace ri
{
    RtVoid ri_stdout_context::RiVersion(char* version)
    {
        begin_function("RiVersion");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "version %s\n", version);
        }
        end_function("RiVersion");
    }

    RtToken ri_stdout_context::RiDeclare(char* name, char* declaration)
    {
        begin_function("RiDeclare");
        if (fp_)
        {
            std::string a = name;
            std::string b = declaration;

            std::string d = b + " " + a;
            this->RiGetClassifier()->def(d.c_str());

            print_tab();
            fprintf(fp_, "Declare \"%s\" \"%s\"\n", name, declaration);
        }
        end_function("RiDeclare");
        return name;
    }
    RtVoid ri_stdout_context::RiBegin(RtToken name)
    {
        begin_function("RiBegin");
        /*
          if(fp_){
              print_tab();
              fprintf(fp_,"Begin %s\n", name);
          }
          */
        // inc_tab();
        end_function("RiBegin");
    }
    RtVoid ri_stdout_context::RiEnd(void)
    {
        begin_function("RiEnd");
        // dec_tab();
        /*
          if(fp_){
              print_tab();
              fprintf(fp_,"End %s\n");
          }
          */

        if (fp_)
        {
            fflush(fp_);
        }
        end_function("RiEnd");
    }
    RtVoid ri_stdout_context::RiFrameBegin(RtInt frame)
    {
        begin_function("RiFrameBegin");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "FrameBegin %d\n", frame);
        }
        inc_tab();
        push();
        end_function("RiFrameBegin");
    }
    RtVoid ri_stdout_context::RiFrameEnd(void)
    {
        begin_function("RiFrameEnd");
        pop();
        dec_tab();
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "FrameEnd\n");
        }
        end_function("RiFrameEnd");
    }
    RtVoid ri_stdout_context::RiWorldBegin(void)
    {
        begin_function("RiWorldBegin");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "WorldBegin\n");
        }
        inc_tab();
        push();
        end_function("RiWorldBegin");
    }
    RtVoid ri_stdout_context::RiWorldEnd(void)
    {
        begin_function("RiWorldEnd");
        pop();
        dec_tab();
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "WorldEnd\n");
        }
        end_function("RiWorldEnd");
    }
    RtVoid ri_stdout_context::RiFormat(RtInt xres, RtInt yres, RtFloat aspect)
    {
        begin_function("RiFormat");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Format %d %d %f\n", xres, yres, aspect);
        }
        end_function("RiFormat");
    }
    RtVoid ri_stdout_context::RiFrameAspectRatio(RtFloat aspect)
    {
        begin_function("RiFrameAspectRatio");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "FrameAspectRatio %f\n", aspect);
        }
        end_function("RiFrameAspectRatio");
    }
    RtVoid ri_stdout_context::RiScreenWindow(RtFloat left, RtFloat right,
                                             RtFloat bot, RtFloat top)
    {
        begin_function("RiScreenWindow");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ScreenWindow %f %f %f %f\n", left, right, bot, top);
        }
        end_function("RiScreenWindow");
    }
    RtVoid ri_stdout_context::RiCropWindow(RtFloat xmin, RtFloat xmax, RtFloat ymin,
                                           RtFloat ymax)
    {
        begin_function("RiCropWindow");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "CropWindow %f %f %f %f\n", xmin, xmax, ymin, ymax);
        }
        end_function("RiCropWindow");
    }

    RtVoid ri_stdout_context::RiProjectionV(RtToken name, RtInt n, RtToken tokens[],
                                            RtPointer params[])
    {
        begin_function("RiProjection");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Projection \"%s\"", name);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiProjection");
    }

    RtVoid ri_stdout_context::RiClipping(RtFloat hither, RtFloat yon)
    {
        begin_function("RiClipping");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Clipping %f %f\n", hither, yon);
        }
        end_function("RiClipping");
    }
    RtVoid ri_stdout_context::RiClippingPlane(RtFloat x, RtFloat y, RtFloat z,
                                              RtFloat nx, RtFloat ny, RtFloat nz)
    {
        begin_function("RiClippingPlane");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ClippingPlane %f %f %f %f %f %f\n", x, y, z, nx, ny, nz);
        }
        end_function("RiClippingPlane");
    }
    RtVoid ri_stdout_context::RiShutter(RtFloat min, RtFloat max)
    {
        begin_function("RiShutter");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Shutter %f %f\n", min, max);
        }
        end_function("RiShutter");
    }
    RtVoid ri_stdout_context::RiPixelVariance(RtFloat variation)
    {
        begin_function("RiPixelVariance");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "PixelVariance %f\n", variation);
        }
        end_function("RiPixelVariance");
    }
    RtVoid ri_stdout_context::RiPixelSamples(RtFloat xsamples, RtFloat ysamples)
    {
        begin_function("RiPixelSamples");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "PixelSamples %f %f\n", xsamples, ysamples);
        }
        end_function("RiPixelSamples");
    }
    RtVoid ri_stdout_context::RiPixelSampleImagerV(RtToken name, RtInt n,
                                                   RtToken tokens[],
                                                   RtPointer params[])
    {
        begin_function("RiPixelSampleImager");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "PixelSample \"%s\"", name);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiPixelSampleImager");
    }
    RtVoid ri_stdout_context::RiPixelFilter(RtToken name, RtFloat xwidth,
                                            RtFloat ywidth)
    {
        begin_function("RiPixelFilter");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "PixelFilter \"%s\" %f %f\n", name, xwidth, ywidth);
        }
        end_function("RiPixelFilter");
    }
    RtVoid ri_stdout_context::RiPixelFilter(RtFilterFunc filterfunc, RtFloat xwidth,
                                            RtFloat ywidth)
    {
        begin_function("RiPixelFilter");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "PixelFilter \"%s\" %f %f\n",
                    GetFilerFuncName(filterfunc).c_str(), xwidth, ywidth);
        }
        end_function("RiPixelFilter");
    }
    RtVoid ri_stdout_context::RiExposure(RtFloat gain, RtFloat gamma)
    {
        begin_function("RiExposure");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Exposure %f %f\n", gain, gamma);
        }
        end_function("RiExposure");
    }

    RtVoid ri_stdout_context::RiImagerV(RtToken name, RtInt n, RtToken tokens[],
                                        RtPointer params[])
    {
        begin_function("RiImager");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Imager %s", name);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiImager");
    }
    RtVoid ri_stdout_context::RiQuantize(RtToken type, RtInt one, RtInt min,
                                         RtInt max, RtFloat ampl)
    {
        begin_function("RiQuantize");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Quantize \"%s\" %d %d %d %f\n", type, one, min, max, ampl);
        }
        end_function("RiQuantize");
    }

    RtVoid ri_stdout_context::RiDisplayV(char* name, RtToken type, RtToken mode,
                                         RtInt n, RtToken tokens[],
                                         RtPointer params[])
    {
        begin_function("RiDisplay");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Display \"%s\" \"%s\" \"%s\"", name, type, mode);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiDisplay");
    }

    RtVoid ri_stdout_context::RiDisplayChannelV(RtToken channel, RtInt n,
                                                RtToken tokens[],
                                                RtPointer params[])
    {
        begin_function("RiDisplayChannel");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "DisplayChannel \"%s\"", channel);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiDisplayChannel");
    }

    RtVoid ri_stdout_context::RiHiderV(RtToken type, RtInt n, RtToken tokens[],
                                       RtPointer params[])
    {
        begin_function("RiHider");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Hider \"%s\"", type);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiHider");
    }
    RtVoid ri_stdout_context::RiColorSamples(RtInt n, RtFloat nRGB[],
                                             RtFloat RGBn[])
    {
        begin_function("RiColorSamples");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ColorSamples %s %s\n", ArrayToString(nRGB, n * 3).c_str(),
                    ArrayToString(RGBn, n * 3).c_str());
        }
        end_function("RiColorSamples");
    }
    RtVoid ri_stdout_context::RiRelativeDetail(RtFloat relativedetail)
    {
        begin_function("RiRelativeDetail");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "RelativeDetail %f\n", relativedetail);
        }
        end_function("RiRelativeDetail");
    }

    RtVoid ri_stdout_context::RiOptionV(RtToken name, RtInt n, RtToken tokens[],
                                        RtPointer params[])
    {
        begin_function("RiOption");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Option \"%s\"", name);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiOption");
    }

    RtVoid ri_stdout_context::RiAttributeBegin(void)
    {
        begin_function("RiAttributeBegin");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "AttributeBegin\n");
        }
        inc_tab();
        push();
        end_function("RiAttributeBegin");
    }
    RtVoid ri_stdout_context::RiAttributeEnd(void)
    {
        begin_function("RiAttributeEnd");
        pop();
        dec_tab();
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "AttributeEnd\n");
        }
        end_function("RiAttributeEnd");
    }
    RtVoid ri_stdout_context::RiColor(RtColor color)
    {
        begin_function("RiColor");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Color %s\n", ColorToString(color).c_str());
        }
        end_function("RiColor");
    }
    RtVoid ri_stdout_context::RiOpacity(RtColor color)
    {
        begin_function("RiOpacity");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Opacity %s\n", ColorToString(color).c_str());
        }
        end_function("RiOpacity");
    }
    RtVoid ri_stdout_context::RiTextureCoordinates(RtFloat s1, RtFloat t1,
                                                   RtFloat s2, RtFloat t2,
                                                   RtFloat s3, RtFloat t3,
                                                   RtFloat s4, RtFloat t4)
    {
        begin_function("RiTextureCoordinates");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "TextureCoordinates %f %f %f %f %f %f %f %f\n", s1, t1, s2, t2,
                    s3, t3, s4, t4);
        }
        end_function("RiTextureCoordinates");
    }

    RtLightHandle ri_stdout_context::RiLightSourceV(RtToken name, RtInt n,
                                                    RtToken tokens[],
                                                    RtPointer params[])
    {
        begin_function("RiLightSource");
        LightSource* pLight = new LightSource();
        pLight->strLight = "LightSource";
        pLight->strName = name;
        pLight->strOther = convert_params(n, tokens, params);
        m_pLight_.push_back(pLight);
        end_function("RiLightSource");
        return pLight; //
    }

    RtLightHandle ri_stdout_context::RiAreaLightSourceV(RtToken name, RtInt n,
                                                        RtToken tokens[],
                                                        RtPointer params[])
    {
        begin_function("RiAreaLightSource");
        LightSource* pLight = new LightSource();
        pLight->strLight = "AreaLightSource";
        pLight->strName = name;
        pLight->strOther = convert_params(n, tokens, params);
        m_pLight_.push_back(pLight);
        end_function("RiAreaLightSource");
        return pLight; //
    }

    RtVoid ri_stdout_context::RiIlluminate(RtInt id, RtBoolean onoff)
    {
        begin_function("RiIlluminate");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Illuminate %d, %d\n", id, onoff ? 1 : 0);
        }
        end_function("RiIlluminate");
    }
    RtVoid ri_stdout_context::RiIlluminate(RtToken name, RtBoolean onoff)
    {
        begin_function("RiIlluminate");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Illuminate %s, %d\n", name, onoff ? 1 : 0);
        }
        end_function("RiIlluminate");
    }

    RtVoid ri_stdout_context::RiSurfaceV(RtToken name, RtInt n, RtToken tokens[],
                                         RtPointer params[])
    {
        begin_function("RiSurface");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Surface \"%s\"", name);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiSurface");
    }

    RtVoid ri_stdout_context::RiAtmosphereV(RtToken name, RtInt n, RtToken tokens[],
                                            RtPointer params[])
    {
        begin_function("RiAtmosphere");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Atmosphere \"%s\"", name);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiAtmosphere");
    }

    RtVoid ri_stdout_context::RiInteriorV(RtToken name, RtInt n, RtToken tokens[],
                                          RtPointer params[])
    {
        begin_function("RiInterior");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Interior \"%s\"", name);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiInterior");
    }

    RtVoid ri_stdout_context::RiExteriorV(RtToken name, RtInt n, RtToken tokens[],
                                          RtPointer params[])
    {
        begin_function("RiExterior");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Exterior \"%s\"", name);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiExterior");
    }

    RtVoid ri_stdout_context::RiShadingRate(RtFloat size)
    {
        begin_function("RiShadingRate");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ShadingRate %f\n", size);
        }
        end_function("RiShadingRate");
    }

    RtVoid ri_stdout_context::RiShadingInterpolation(RtToken type)
    {
        begin_function("RiShadingInterpolation");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ShadingInterpolation \"%s\"\n", type);
        }
        end_function("RiShadingInterpolation");
    }

    RtVoid ri_stdout_context::RiMatte(RtBoolean onoff)
    {
        begin_function("RiMatte");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Matte %d\n", onoff);
        }
        end_function("RiMatte");
    }

    RtVoid ri_stdout_context::RiBound(RtBound bound)
    {
        begin_function("RiBound");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Bound %s\n", BoundToString(bound).c_str());
        }
        end_function("RiBound");
    }

    RtVoid ri_stdout_context::RiDetail(RtBound bound)
    {
        begin_function("RiDetail");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Detail %s\n", BoundToString(bound).c_str());
        }
        end_function("RiDetail");
    }

    RtVoid ri_stdout_context::RiDetailRange(RtFloat minvis, RtFloat lowtran,
                                            RtFloat uptran, RtFloat maxvis)
    {
        begin_function("RiDetailRange");
        if (fp_)
        {
            print_tab();
            RtFloat ranges[] = {minvis, lowtran, uptran, maxvis};
            fprintf(fp_, "DetailRange %s\n", ArrayToString(ranges, 4).c_str());
        }
        end_function("RiDetailRange");
    }

    RtVoid ri_stdout_context::RiGeometricApproximation(RtToken type,
                                                       RtFloat value)
    {
        begin_function("RiGeometricApproximation");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "GeometricApproximation \"%s\" %f\n", type, value);
        }
        end_function("RiGeometricApproximation");
    }

    RtVoid ri_stdout_context::RiOrientation(RtToken orientation)
    {
        begin_function("RiOrientation");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Orientation \"%s\"\n", orientation);
        }
        end_function("RiOrientation");
    }

    RtVoid ri_stdout_context::RiReverseOrientation(void)
    {
        begin_function("RiReverseOrientation");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ReverseOrientation\n");
        }
        end_function("RiReverseOrientation");
    }

    RtVoid ri_stdout_context::RiSides(RtInt sides)
    {
        begin_function("RiSides");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Sides %d\n", sides);
        }
        end_function("RiSides");
    }

    RtVoid ri_stdout_context::RiIdentity(void)
    {
        begin_function("RiIdentity");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Identity\n");
        }
        end_function("RiIdentity");
    }

    RtVoid ri_stdout_context::RiTransform(RtMatrix transform)
    {
        begin_function("RiTransform");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Transform %s\n", MatrixToString(transform).c_str());
        }
        end_function("RiTransform");
    }

    RtVoid ri_stdout_context::RiConcatTransform(RtMatrix transform)
    {
        begin_function("RiConcatTransform");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ConcatTransform %s\n", MatrixToString(transform).c_str());
        }
        end_function("RiConcatTransform");
    }

    RtVoid ri_stdout_context::RiPerspective(RtFloat fov)
    {
        begin_function("RiPerspective");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Perspective %f\n", fov);
        }
        end_function("RiPerspective");
    }
    RtVoid ri_stdout_context::RiTranslate(RtFloat dx, RtFloat dy, RtFloat dz)
    {
        begin_function("RiTranslate");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Translate %f %f %f\n", dx, dy, dz);
        }
        end_function("RiTranslate");
    }
    RtVoid ri_stdout_context::RiRotate(RtFloat angle, RtFloat dx, RtFloat dy,
                                       RtFloat dz)
    {
        begin_function("RiRotate");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Rotate %f %f %f %f\n", angle, dx, dy, dz);
        }
        end_function("RiRotate");
    }

    RtVoid ri_stdout_context::RiScale(RtFloat sx, RtFloat sy, RtFloat sz)
    {
        begin_function("RiScale");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Scale %f %f %f\n", sx, sy, sz);
        }
        end_function("RiScale");
    }

    RtVoid ri_stdout_context::RiSkew(RtFloat angle, RtFloat dx1, RtFloat dy1,
                                     RtFloat dz1, RtFloat dx2, RtFloat dy2,
                                     RtFloat dz2)
    {
        begin_function("RiSkew");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Scale %f %f %f %f %f %f %f\n", angle, dx1, dy1, dz1, dx2, dy2,
                    dz2);
        }
        end_function("RiSkew");
    }

    RtVoid ri_stdout_context::RiDeformationV(RtToken name, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        begin_function("RiDeformation");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Deformation \"%s\"", name);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiDeformation");
    }

    RtVoid ri_stdout_context::RiDisplacementV(RtToken name, RtInt n,
                                              RtToken tokens[],
                                              RtPointer params[])
    {
        begin_function("RiDisplacement");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Displacement \"%s\"", name);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiDisplacement");
    }
    RtVoid ri_stdout_context::RiCoordinateSystem(RtToken space)
    {
        begin_function("RiCoordinateSystem");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "CoordinateSystem \"%s\"", space);
        }
        end_function("RiCoordinateSystem");
    }
    RtVoid ri_stdout_context::RiScopedCoordinateSystem(RtToken space)
    {
        begin_function("RiScopedCoordinateSystem");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ScopedCoordinateSystem \"%s\"", space);
        }
        end_function("RiScopedCoordinateSystem");
    }
    RtVoid ri_stdout_context::RiCoordSysTransform(RtToken space)
    {
        begin_function("RiCoordSysTransform");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "CoordSysTransform \"%s\"\n", space);
        }
        end_function("RiCoordSysTransform");
    }
    RtPoint* ri_stdout_context::RiTransformPoints(RtToken fromspace,
                                                  RtToken tospace, RtInt n,
                                                  RtPoint points[])
    {
        begin_function("RiTransformPoints");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "TransformPoints \"%s\" \"%s\"\n", fromspace, tospace); // TODO
        }
        end_function("RiTransformPoints");
        return NULL;
    }
    RtVoid ri_stdout_context::RiTransformBegin(void)
    {
        begin_function("RiTransformBegin");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "TransformBegin\n");
        }
        inc_tab();
        push();
        end_function("RiTransformBegin");
    }
    RtVoid ri_stdout_context::RiTransformEnd(void)
    {
        begin_function("RiTransformEnd");
        pop();
        dec_tab();
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "TransformEnd\n");
        }
        end_function("RiTransformEnd");
    }

    RtVoid ri_stdout_context::RiResourceV(RtToken handle, RtToken type, RtInt n,
                                          RtToken tokens[], RtPointer params[])
    {
        begin_function("RiResource");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Resource \"%s\"", handle);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiResource");
    }

    RtVoid ri_stdout_context::RiResourceBegin(void)
    {
        begin_function("RiResourceBegin");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ResourceBegin\n");
        }
        inc_tab();
        push();
        end_function("RiResourceBegin");
    }

    RtVoid ri_stdout_context::RiResourceEnd(void)
    {
        begin_function("RiResourceEnd");
        pop();
        dec_tab();
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ResourceEnd\n");
        }
        end_function("RiResourceEnd");
    }

    RtVoid ri_stdout_context::RiAttributeV(RtToken name, RtInt n, RtToken tokens[],
                                           RtPointer params[])
    {
        begin_function("RiAttribute");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Attribute \"%s\"", name);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiAttribute");
    }

    RtVoid ri_stdout_context::RiPolygonV(RtInt nverts, RtInt n, RtToken tokens[],
                                         RtPointer params[])
    {
        begin_function("RiPolygon");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Polygon");
            print_params(n, tokens, params, nverts, nverts, 1);
            fprintf(fp_, "\n");
        }
        end_function("RiPolygon");
    }

    RtVoid ri_stdout_context::RiGeneralPolygonV(RtInt nloops, RtInt nverts[],
                                                RtInt n, RtToken tokens[],
                                                RtPointer params[])
    {
        begin_function("RiGeneralPolygon");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "GeneralPolygon");
            fprintf(fp_, " %s", ArrayToString(nverts, nloops).c_str());
            int nPts = SumArray(nverts, nloops);
            print_params(n, tokens, params, nPts, nPts, 1);
            fprintf(fp_, "\n");
        }
        end_function("RiGeneralPolygon");
    }

    RtVoid ri_stdout_context::RiPointsPolygonsV(RtInt npolys, RtInt nverts[],
                                                RtInt verts[], RtInt n,
                                                RtToken tokens[],
                                                RtPointer params[])
    {
        begin_function("RiPointsPolygons");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "PointsPolygons");
            fprintf(fp_, " %s", ArrayToString(nverts, npolys).c_str());
            int nPts = SumArray(nverts, npolys);
            fprintf(fp_, " %s", ArrayToString(verts, nPts).c_str());

            int psz = 0;
            for (int i = 0; i < nPts; i++)
            {
                if (psz < verts[i])
                {
                    psz = verts[i];
                }
            }

            print_params(n, tokens, params, psz + 1, psz + 1, npolys, nPts);
            fprintf(fp_, "\n");
        }
        end_function("RiPointsPolygons");
    }

    RtVoid ri_stdout_context::RiPointsGeneralPolygonsV(RtInt npolys, RtInt nloops[],
                                                       RtInt nverts[],
                                                       RtInt verts[], RtInt n,
                                                       RtToken tokens[],
                                                       RtPointer params[])
    {
        begin_function("RiPointsGeneralPolygons");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "PointsGeneralPolygons");
            fprintf(fp_, " %s", ArrayToString(nloops, npolys).c_str());
            int nVts = SumArray(nloops, npolys);
            fprintf(fp_, " %s", ArrayToString(nverts, nVts).c_str());
            int nv = SumArray(nverts, nVts);
            fprintf(fp_, " %s", ArrayToString(verts, nv).c_str());

            int psz = 0;
            for (int i = 0; i < nv; i++)
            {
                if (psz < verts[i])
                {
                    psz = verts[i];
                }
            }

            print_params(n, tokens, params, psz + 1, psz + 1, npolys, nv);
            fprintf(fp_, "\n");
        }
        end_function("RiPointsGeneralPolygons");
    }

    RtVoid ri_stdout_context::RiBasis(RtToken ubasis, RtInt ustep, RtToken vbasis,
                                      RtInt vstep)
    {
        begin_function("RiBasis");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Basis \"%s\" %d \"%s\" %d\n", ubasis, ustep, vbasis, vstep);
        }
        m_steps_.back().ustep = ustep;
        m_steps_.back().vstep = vstep;
        end_function("RiBasis");
    }
    RtVoid ri_stdout_context::RiBasis(RtBasis ubasis, RtInt ustep, RtToken vbasis,
                                      RtInt vstep) // basis token
    {
        this->RiBasis((RtToken)(ArrayToString((const float*)ubasis, 16).c_str()),
                      ustep, vbasis, vstep);
    }
    RtVoid ri_stdout_context::RiBasis(RtToken ubasis, RtInt ustep, RtBasis vbasis,
                                      RtInt vstep) // token basis
    {
        this->RiBasis(ubasis, ustep,
                      (RtToken)(ArrayToString((const float*)vbasis, 16).c_str()),
                      vstep);
    }
    RtVoid ri_stdout_context::RiBasis(RtBasis ubasis, RtInt ustep, RtBasis vbasis,
                                      RtInt vstep)
    {
        this->RiBasis(
            (RtToken)(ArrayToString((const float*)ubasis, 16).c_str()), ustep,
            (RtToken)(ArrayToString((const float*)ubasis, 16).c_str()), vstep);
    }

    RtVoid ri_stdout_context::RiPatchV(RtToken type, RtInt n, RtToken tokens[],
                                       RtPointer params[])
    {
        begin_function("RiPatch");
        if (fp_)
        {
            print_tab();
            int nv = 0;
            if (strcmp(type, RI_BILINEAR) == 0)
            {
                nv = 2;
            }
            else if (strcmp(type, RI_BIQUADRATIC) == 0)
            {
                nv = 3;
            }
            else if (strcmp(type, RI_BICUBIC) == 0)
            {
                nv = 4;
            }
            if (nv)
            {
                fprintf(fp_, "Patch \"%s\"", type);
                print_params(n, tokens, params, nv * nv, 4);
                N;
            }
        }
        end_function("RiPatch");
    }

    struct T2I
    {
        const char* name;
        int num;
    };

    static T2I ORDERS[] = {{RI_BILINEAR, 2}, {RI_BIQUADRATIC, 3}, {RI_BICUBIC, 4}, {RI_LINEAR, 2}, {RI_QUADRATIC, 3}, {RI_CUBIC, 4}, {NULL, 0}};

    static int GetOrder(RtToken type)
    {
        int i = 0;
        while (ORDERS[i].name)
        {
            if (strcmp(ORDERS[i].name, type) == 0)
                return ORDERS[i].num;
            i++;
        }
        return 0;
    }

    RtVoid ri_stdout_context::RiPatchMeshV(RtToken type, RtInt nu, RtToken uwrap,
                                           RtInt nv, RtToken vwrap, RtInt n,
                                           RtToken tokens[], RtPointer params[])
    {
        begin_function("RiPatchMesh");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "PatchMesh \"%s\" %d \"%s\" %d \"%s\"", type, nu, uwrap, nv,
                    vwrap);
            int nOrder = GetOrder(type);
            int nupatch = 1;
            int nvpatch = 1;
            int nverts = 0;
            if (nOrder == 2)
            { // linear
                if (strcmp(uwrap, RI_PERIODIC) == 0)
                { // loop
                    nupatch = nu;
                }
                else
                { // open
                    nupatch = nu - 1;
                }

                if (strcmp(vwrap, RI_PERIODIC) == 0)
                { // loop
                    nvpatch = nv;
                }
                else
                { // open
                    nvpatch = nv - 1;
                }
                nverts = nu * nv;
            }
            else if (nOrder == 4)
            { // cubic
                RtInt nustep = m_steps_.back().ustep;
                RtInt nvstep = m_steps_.back().vstep;

                int nnu = 1;
                int nnv = 1;

                if (strcmp(uwrap, RI_PERIODIC) == 0)
                {
                    nupatch = nu / nustep;
                    nnu = nupatch;
                }
                else
                {
                    nupatch = (nu - 4) / nustep + 1;
                    nnu = nupatch + 1;
                }

                if (strcmp(vwrap, RI_PERIODIC) == 0)
                {
                    nvpatch = nv / nvstep;
                    nnv = nvpatch;
                }
                else
                {
                    nvpatch = (nv - 4) / nvstep + 1;
                    nnv = nvpatch + 1;
                }

                nverts = nnu * nnv;
            }

            if (nOrder)
            {
                print_params(n, tokens, params, nu * nv, nverts,
                             nupatch * nvpatch); // vertex varying uniform
            }
            fprintf(fp_, "\n");
        }
        end_function("RiPatchMesh");
    }

    RtVoid ri_stdout_context::RiNuPatchV(RtInt nu, RtInt uorder, RtFloat uknot[],
                                         RtFloat umin, RtFloat umax, RtInt nv,
                                         RtInt vorder, RtFloat vknot[],
                                         RtFloat vmin, RtFloat vmax, RtInt n,
                                         RtToken tokens[], RtPointer params[])
    {
        begin_function("RiNuPatch");
        if (fp_)
        {
            print_tab();
            PR("NuPatch");
            S;
            PI(nu);
            S;
            PI(uorder);
            S;
            PR(ArrayToString(uknot, nu + uorder).c_str());
            S;
            PF(umin);
            S;
            PF(umax);
            S;
            PI(nv);
            S;
            PI(vorder);
            S;
            PR(ArrayToString(vknot, nv + vorder).c_str());
            S;
            PF(vmin);
            S;
            PF(vmax);

            print_params(n, tokens, params, nu * nv,
                         (2 + nu - uorder) * (2 + nv - vorder),
                         (1 + nu - uorder) * (1 + nv - vorder));
            N;
        }
        end_function("RiNuPatch");
    }

    RtVoid ri_stdout_context::RiTrimCurve(RtInt nloops, RtInt ncurves[],
                                          RtInt order[], RtFloat knot[],
                                          RtFloat min[], RtFloat max[], RtInt n[],
                                          RtFloat u[], RtFloat v[], RtFloat w[])
    {
        begin_function("RiTrimCurve");
        if (fp_)
        {
            print_tab();
            int ttlc = SumArray(ncurves, nloops);
            int nbcoords = 0;
            int knotsize = 0;
            for (int i = 0; i < ttlc; i++)
            {
                nbcoords += n[i];
                knotsize += order[i] + n[i];
            }

            fprintf(fp_, "TrimCurve");
            fprintf(fp_, " %s", ArrayToString(ncurves, nloops).c_str());
            fprintf(fp_, " %s", ArrayToString(order, ttlc).c_str());
            fprintf(fp_, " %s", ArrayToString(knot, knotsize).c_str());
            fprintf(fp_, " %s", ArrayToString(min, ttlc).c_str());
            fprintf(fp_, " %s", ArrayToString(max, ttlc).c_str());
            fprintf(fp_, " %s", ArrayToString(n, ttlc).c_str());
            fprintf(fp_, " %s", ArrayToString(u, nbcoords).c_str());
            fprintf(fp_, " %s", ArrayToString(v, nbcoords).c_str());
            fprintf(fp_, " %s", ArrayToString(w, nbcoords).c_str());
            fprintf(fp_, "\n");
        }
        end_function("RiTrimCurve");
    }

    RtVoid ri_stdout_context::RiSphereV(RtFloat radius, RtFloat zmin, RtFloat zmax,
                                        RtFloat tmax, RtInt n, RtToken tokens[],
                                        RtPointer params[])
    {
        begin_function("RiSphere");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Sphere %f %f %f %f", radius, zmin, zmax, tmax);
            print_params(n, tokens, params, 4, 4);
            fprintf(fp_, "\n");
        }
        end_function("RiSphere");
    }

    RtVoid ri_stdout_context::RiConeV(RtFloat height, RtFloat radius, RtFloat tmax,
                                      RtInt n, RtToken tokens[],
                                      RtPointer params[])
    {
        begin_function("RiCone");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Cone %f %f %f", height, radius, tmax);
            print_params(n, tokens, params, 4, 4);
            fprintf(fp_, "\n");
        }
        end_function("RiCone");
    }

    RtVoid ri_stdout_context::RiCylinderV(RtFloat radius, RtFloat zmin,
                                          RtFloat zmax, RtFloat tmax, RtInt n,
                                          RtToken tokens[], RtPointer params[])
    {
        begin_function("RiCylinder");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Cylinder %f %f %f %f", radius, zmin, zmax, tmax);
            print_params(n, tokens, params, 4, 4);
            fprintf(fp_, "\n");
        }
        end_function("RiCylinder");
    }

    RtVoid ri_stdout_context::RiHyperboloidV(RtPoint point1, RtPoint point2,
                                             RtFloat tmax, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        begin_function("RiHyperboloid");
        if (fp_)
        {
            print_tab();
            PR("Hyperboloid");
            S;
            PF(point1[0]);
            S;
            PF(point1[1]);
            S;
            PF(point1[2]);
            S;
            PF(point2[0]);
            S;
            PF(point2[1]);
            S;
            PF(point2[2]);
            S;
            PF(tmax);
            print_params(n, tokens, params, 4, 4);
            N;
        }
        end_function("RiHyperboloid");
    }

    RtVoid ri_stdout_context::RiParaboloidV(RtFloat rmax, RtFloat zmin,
                                            RtFloat zmax, RtFloat tmax, RtInt n,
                                            RtToken tokens[], RtPointer params[])
    {
        begin_function("RiParaboloid");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Paraboloid %f %f %f %f", rmax, zmin, zmax, tmax);
            print_params(n, tokens, params, 4, 4);
            fprintf(fp_, "\n");
        }
        end_function("RiParaboloid");
    }

    RtVoid ri_stdout_context::RiDiskV(RtFloat height, RtFloat radius, RtFloat tmax,
                                      RtInt n, RtToken tokens[],
                                      RtPointer params[])
    {
        begin_function("RiDisk");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Disk %f %f %f", height, radius, tmax);
            print_params(n, tokens, params, 4, 4);
            fprintf(fp_, "\n");
        }
        end_function("RiDisk");
    }

    RtVoid ri_stdout_context::RiTorusV(RtFloat majrad, RtFloat minrad,
                                       RtFloat phimin, RtFloat phimax, RtFloat tmax,
                                       RtInt n, RtToken tokens[],
                                       RtPointer params[])
    {
        begin_function("RiTorus");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Torus %f %f %f %f %f", majrad, minrad, phimin, phimax, tmax);
            print_params(n, tokens, params, 4, 4);
            fprintf(fp_, "\n");
        }
        end_function("RiTorus");
    }

    RtVoid ri_stdout_context::RiBlobbyV(RtInt nleaf, RtInt ncode, RtInt code[],
                                        RtInt nflt, RtFloat flt[], RtInt nstr,
                                        RtToken str[], RtInt n, RtToken tokens[],
                                        RtPointer params[])
    {
        begin_function("RiBlobby");
        if (fp_)
        {
            print_tab();
            PR("Blobby");
            S;
            PR(ArrayToString(code, ncode).c_str());
            S;
            PR(ArrayToString(flt, nflt).c_str());
            S;
            PR(ArrayToString(str, nstr).c_str());
            print_params(n, tokens, params, nleaf, nleaf);
            N;
        }
        end_function("RiBlobby");
    }

    RtVoid ri_stdout_context::RiCurvesV(RtToken type, RtInt ncurves,
                                        RtInt nvertices[], RtToken wrap, RtInt n,
                                        RtToken tokens[], RtPointer params[])
    {
        begin_function("RiCurves");
        if (fp_)
        {
            print_tab();
            int vn = SumArray(nvertices, ncurves);
            int nOrder = GetOrder(type);
            int step = m_steps_.back().vstep;
            int nseg = 0;
            int ii = 0;
            if (nOrder == 2)
            { // linear
                if (strcmp(wrap, RI_PERIODIC) == 0)
                { // loop
                    nseg = vn;
                    ii = vn;
                }
                else if (strcmp(wrap, RI_NONPERIODIC) == 0)
                { // open
                    nseg = vn - ncurves;
                    ii = vn;
                }
            }
            else if (nOrder == 4)
            { // cubic
                if (strcmp(wrap, RI_PERIODIC) == 0)
                {
                    nseg = 0;
                    ii = 0;
                    for (int i = 0; i < ncurves; i++)
                    {
                        int ns = nvertices[i] / step;
                        nseg += ns;
                        ii += ns;
                    }
                }
                else if (strcmp(wrap, RI_NONPERIODIC) == 0)
                {
                    nseg = 0;
                    ii = 0;
                    for (int i = 0; i < ncurves; i++)
                    {
                        int ns = (nvertices[i] - 4) / step + 1;
                        nseg += ns;
                        ii += ns + 1;
                    }
                }
            }
            else
            {
                nOrder = 0;
            }

            fprintf(fp_, "Curves \"%s\" %s \"%s\"", type,
                    ArrayToString(nvertices, ncurves).c_str(), wrap); // TODO
            print_params(n, tokens, params, vn, ii, ncurves);         // TODO
            N;
        }
        end_function("RiCurves");
    }

    RtVoid ri_stdout_context::RiPointsV(RtInt nverts, RtInt n, RtToken tokens[],
                                        RtPointer params[])
    {
        begin_function("RiPoints");
        if (fp_)
        {
            print_tab();
            PR("Points");
            S;
            print_params(n, tokens, params, nverts, nverts); // vertex, varying
            N;
        }
        end_function("RiPoints");
    }

    RtVoid ri_stdout_context::RiSubdivisionMeshV(
        RtToken mask, RtInt nf, RtInt nverts[], RtInt verts[], RtInt ntags,
        RtToken tags[], RtInt nargs[], RtInt intargs[], RtFloat floatargs[],
        RtInt n, RtToken tokens[], RtPointer params[])
    {
        begin_function("RiSubdivisionMesh");
        if (fp_)
        {
            int nv = SumArray(nverts, nf);

            print_tab();
            PR("SubdivisionMesh");
            S;
            PS(mask);
            S;
            PR(ArrayToString(nverts, nf).c_str());
            S;
            PR(ArrayToString(verts, nv).c_str());
            S;
            PR(ArrayToString(tags, ntags).c_str());
            S;
            PR(ArrayToString(nargs, ntags * 2).c_str());
            S;

            RtInt isz = 0;
            RtInt fsz = 0;
            for (int i = 0; i < ntags * 2; i++)
            {
                if (i % 2 == 0)
                    isz += nargs[i];
                else
                    fsz += nargs[i];
            }
            PR(ArrayToString(intargs, isz).c_str());
            S;
            PR(ArrayToString(floatargs, fsz).c_str());
            S;
            RtInt psz = 0;
            for (int i = 0; i < nv; i++)
            {
                if (psz < verts[i])
                    psz = verts[i];
            }
            print_params(n, tokens, params, psz + 1, psz + 1, nf, nv);
            N;
        }
        end_function("RiSubdivisionMesh");
    }

    RtVoid ri_stdout_context::RiHierarchicalSubdivisionMeshV(
        RtToken scheme, RtInt nf, RtInt nverts[], RtInt verts[], RtInt ntags,
        RtToken tags[], RtInt nargs[], RtInt intargs[], RtFloat floatargs[],
        RtString stringargs[], RtInt n, RtToken tokens[], RtPointer params[])
    {
        begin_function("RiHierarchicalSubdivisionMesh");
        if (fp_)
        {
            int nv = SumArray(nverts, nf);

            print_tab();
            PR("HierarchicalSubdivisionMesh");
            S;
            PS(scheme);
            S;
            PR(ArrayToString(nverts, nf).c_str());
            S;
            PR(ArrayToString(verts, nv).c_str());
            S;
            PR(ArrayToString(tags, ntags).c_str());
            S;
            PR(ArrayToString(nargs, ntags * 3).c_str());
            S;

            RtInt isz = 0;
            RtInt fsz = 0;
            RtInt ssz = 0;
            for (int i = 0; i < ntags * 3; i++)
            {
                int k = i % 3;
                if (k == 0)
                {
                    isz += nargs[i];
                }
                else if (k == 1)
                {
                    fsz += nargs[i];
                }
                else
                {
                    ssz += nargs[i];
                }
            }
            PR(ArrayToString(intargs, isz).c_str());
            S;
            PR(ArrayToString(floatargs, fsz).c_str());
            S;
            PR(ArrayToString(stringargs, ssz).c_str());
            S;
            RtInt psz = 0;
            for (int i = 0; i < nv; i++)
            {
                if (psz < verts[i])
                    psz = verts[i];
            }
            print_params(n, tokens, params, psz + 1, psz + 1, nf, nv);
            N;
        }
        end_function("RiHierarchicalSubdivisionMesh");
    }

    RtVoid ri_stdout_context::RiProcedural(RtPointer data, RtBound bound,
                                           RtVoid (*subdivfunc)(RtPointer, RtFloat),
                                           RtVoid (*freefunc)(RtPointer))
    {
        begin_function("RiProcedural");
        if (fp_)
        {
            int nFind = -1;
            {
                int i = 0;
                while (PROCEDURALS[i].ptr)
                {
                    if (subdivfunc == PROCEDURALS[i].ptr)
                    {
                        nFind = i;
                        break;
                    }
                }
            }

            if (nFind >= 0)
            {
                if (PROCEDURALS[nFind].name)
                {
                    std::string name = PROCEDURALS[nFind].name;

                    print_tab();
                    fprintf(fp_, "Procedural");
                    S;
                    switch (nFind)
                    {
                    case 0:
                        PS(name.c_str());
                        S;
                        PR(ArrayToString(((RtString*)data), 1).c_str());
                        PR(ArrayToString(bound, 6).c_str());
                        N;
                        break;
                    case 1:
                    case 2:
                        PS(name.c_str());
                        S;
                        PR(ArrayToString(((RtString*)data), 2).c_str());
                        PR(ArrayToString(bound, 6).c_str());
                        N;
                        break;
                    }
                }
                else
                {
                    ; // error
                }
            }
        }
        end_function("RiProcedural");
    }

    RtVoid ri_stdout_context::RiProcedural(RtToken type, RtInt n, RtToken tokens[],
                                           RtBound bound)
    {
        this->RiProceduralV(type, n, tokens, bound, 0, NULL, NULL);
    }

    RtVoid ri_stdout_context::RiProceduralV(RtToken type, RtInt n0,
                                            RtToken tokens0[], RtBound bound,
                                            RtInt n, RtToken tokens[],
                                            RtPointer params[])
    {
        begin_function("RiProcedural");
        if (fp_)
        {
            std::string name = type;

            print_tab();
            fprintf(fp_, "Procedural");
            S;
            PS(name.c_str());
            S;
            PR(ArrayToString(tokens, n).c_str());
            PR(ArrayToString(bound, 6).c_str());
            print_params(n, tokens, params);
            N;
        }
        end_function("RiProcedural");
    }

    RtVoid ri_stdout_context::RiGeometryV(RtToken type, RtInt n, RtToken tokens[],
                                          RtPointer params[])
    {
        begin_function("RiGeometry");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Geometry \"%s\"", type);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiGeometry");
    }

    RtVoid ri_stdout_context::RiSolidBegin(RtToken operation)
    {
        begin_function("RiSolidBegin");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "SolidBegin \"%s\"\n", operation);
        }
        inc_tab();
        push();
        end_function("RiSolidBegin");
    }
    RtVoid ri_stdout_context::RiSolidEnd(void)
    {
        begin_function("RiSolidEnd");
        pop();
        dec_tab();
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "SolidEnd\n");
        }
        end_function("RiSolidEnd");
    }
    RtObjectHandle ri_stdout_context::RiObjectBegin(void)
    {
        begin_function("RiObjectBegin");
        // if(fp_){
        //    print_tab();
        //    fprintf(fp_,"ObjectBegin\n");
        //}
        // inc_tab();
        push();
        int* p = new int();
        *p = (int)m_pObj_.size();
        m_pObj_.push_back(p);
        end_function("RiObjectBegin");
        return p; //
    }
    RtVoid ri_stdout_context::RiObjectEnd(void)
    {
        begin_function("RiObjectEnd");
        pop();
        dec_tab();
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ObjectEnd\n");
        }
        end_function("RiObjectEnd");
    }

    RtVoid ri_stdout_context::RiObjectInstance(RtInt id)
    {
        begin_function("RiObjectInstance");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ObjectInstance %d\n", id);
        }
        end_function("RiObjectInstance");
    }
    RtVoid ri_stdout_context::RiObjectInstance(RtToken name)
    {
        begin_function("RiObjectInstance");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ObjectInstance %s\n", name);
        }
        end_function("RiObjectInstance");
    }

    RtVoid ri_stdout_context::RiMotionBeginV(RtInt n, RtFloat times[])
    {
        begin_function("RiMotionBegin");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "MotionBegin %s\n", ArrayToString(times, n).c_str());
        }
        inc_tab();
        push();
        end_function("RiMotionBegin");
    }
    RtVoid ri_stdout_context::RiMotionEnd(void)
    {
        begin_function("RiMotionEnd");
        pop();
        dec_tab();
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "MotionEnd\n");
        }
        end_function("RiMotionEnd");
    }

    RtVoid ri_stdout_context::RiMakeTextureV(char* pic, char* tex, RtToken swrap,
                                             RtToken twrap, RtToken filterfunc,
                                             RtFloat swidth, RtFloat twidth,
                                             RtInt n, RtToken tokens[],
                                             RtPointer params[])
    {
        begin_function("RiMakeTexture");
        if (fp_)
        {
            RtToken ff = filterfunc;
            print_tab();
            PR("MakeTexture");
            S;
            PS(pic);
            S;
            PS(tex);
            S;
            PS(swrap);
            S;
            PS(twrap);
            S;
            PS(ff);
            S;
            PF(swidth);
            S;
            PF(twidth);
            print_params(n, tokens, params);
            N;
        }
        end_function("RiMakeTexture");
    }

    RtVoid ri_stdout_context::RiMakeBumpV(char* pic, char* tex, RtToken swrap,
                                          RtToken twrap, RtToken filterfunc,
                                          RtFloat swidth, RtFloat twidth, RtInt n,
                                          RtToken tokens[], RtPointer params[])
    {
        begin_function("RiMakeBump");
        if (fp_)
        {
            RtToken ff = filterfunc;
            print_tab();
            PR("MakeBump");
            S;
            PS(pic);
            S;
            PS(tex);
            S;
            PS(swrap);
            S;
            PS(twrap);
            S;
            PS(ff);
            S;
            PF(swidth);
            S;
            PF(twidth);
            print_params(n, tokens, params);
            N;
        }
        end_function("RiMakeBump");
    }

    RtVoid ri_stdout_context::RiMakeLatLongEnvironmentV(
        char* pic, char* tex, RtToken filterfunc, RtFloat swidth, RtFloat twidth,
        RtInt n, RtToken tokens[], RtPointer params[])
    {
        begin_function("RiMakeLatLongEnvironment");
        if (fp_)
        {
            RtToken ff = filterfunc;
            print_tab();
            PR("MakeLatLongEnvironment");
            S;
            PS(pic);
            S;
            PS(tex);
            S;
            PS(ff);
            S;
            PF(swidth);
            S;
            PF(twidth);
            print_params(n, tokens, params);
            N;
        }
        end_function("RiMakeLatLongEnvironment");
    }

    RtVoid ri_stdout_context::RiMakeCubeFaceEnvironmentV(
        char* px, char* nx, char* py, char* ny, char* pz, char* nz, char* tex,
        RtFloat fov, RtToken filterfunc, RtFloat swidth, RtFloat twidth, RtInt n,
        RtToken tokens[], RtPointer params[])
    {
        begin_function("RiMakeCubeFaceEnvironment");
        if (fp_)
        {
            RtToken ff = filterfunc;
            print_tab();
            PR("MakeCubeFaceEnvironment");
            S;
            PS(px);
            S;
            PS(nx);
            S;
            PS(py);
            S;
            PS(ny);
            S;
            PS(pz);
            S;
            PS(nz);
            S;
            PS(tex);
            S;
            PF(fov);
            S;
            PS(ff);
            S;
            PF(swidth);
            S;
            PF(twidth);
            print_params(n, tokens, params);
            N;
        }
        end_function("RiMakeCubeFaceEnvironment");
    }

    RtVoid ri_stdout_context::RiMakeShadowV(char* pic, char* tex, RtInt n,
                                            RtToken tokens[], RtPointer params[])
    {
        begin_function("RiMakeShadow");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "MakeShadow \"%s\" \"%s\"", pic, tex);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiMakeShadow");
    }

    RtVoid ri_stdout_context::RiMakeBrickMapV(int nptc, char** ptcnames,
                                              char* bkmname, RtInt n,
                                              RtToken tokens[],
                                              RtPointer params[])
    {
        begin_function("RiMakeBrickMap");
        if (fp_)
        {
            print_tab();
            PR("MakeBrickMap");
            S;
            PR(ArrayToString(ptcnames, nptc).c_str());
            S;
            PS(bkmname);
            print_params(n, tokens, params);
            N;
        }
        end_function("RiMakeBrickMap");
    }

    RtVoid ri_stdout_context::RiArchiveRecord(RtToken type, char* format,
                                              va_list argptr)
    {
        begin_function("RiArchiveRecord");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ArchiveRecord \"%s\" \"%s\"\n", type, format); // TODO
        }
        end_function("RiArchiveRecord");
    }

    RtVoid ri_stdout_context::RiReadArchiveV(RtToken name,
                                             RtArchiveCallback callback, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        begin_function("RiReadArchive");
        // ri_context::RiReadArchiveV(name, callback, n, tokens, params);
        // printf("asdfghjkl\n");
        // load_rib(name, this);

        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ReadArchive \"%s\"", name);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }

        end_function("RiReadArchive");
    }
    RtArchiveHandle ri_stdout_context::RiArchiveBeginV(RtToken archivename, RtInt n,
                                                       RtToken tokens[],
                                                       RtPointer params[])
    {
        begin_function("RiArchiveBegin");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ArchiveBegin \"%s\"", archivename);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        inc_tab();
        push();
        int* p = new int();
        *p = (int)m_pObj_.size();
        m_pObj_.push_back(p);
        end_function("RiArchiveBegin");
        return p;
    }
    RtVoid ri_stdout_context::RiArchiveEnd(void)
    {
        begin_function("RiArchiveEnd");
        pop();
        dec_tab();
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ArchiveEnd");
            fprintf(fp_, "\n");
        }
        end_function("RiArchiveEnd");
    }

    RtVoid ri_stdout_context::RiErrorHandler(RtToken name)
    {
        begin_function("RiErrorHandler");
        if (fp_)
        {
            print_tab();
            if (name)
            {
                fprintf(fp_, "ErrorHandler \"%s\"\n", name);
            }
        }
        end_function("RiErrorHandler");
    }

    RtVoid ri_stdout_context::RiErrorHandler(RtErrorHandler handler)
    {
        begin_function("RiErrorHandler");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ErrorHandler \"%s\"\n", GetErrorHandlerName(handler).c_str());
        }
        end_function("RiErrorHandler");
    }

    RtVoid ri_stdout_context::RiDepthOfField(RtFloat fstop, RtFloat focallength,
                                             RtFloat focaldistance)
    {
        begin_function("RiDepthOfField");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "DepthOfField %f %f %f\n", fstop, focallength, focaldistance);
        }
        end_function("RiDepthOfField");
    }

    RtVoid ri_stdout_context::RiMakeOcclusionV(RtInt npics, RtString* picfile,
                                               RtString shadowfile, RtInt n,
                                               RtToken tokens[],
                                               RtPointer params[])
    {
        begin_function("RiMakeOcclusion");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "MakeOcclusion \"%s\" \"%s\"",
                    ArrayToString(picfile, npics).c_str(), shadowfile);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiMakeOcclusion");
    }

    RtVoid ri_stdout_context::RiShaderLayerV(RtToken type, RtToken name,
                                             RtToken layername, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        begin_function("RiShaderLayer");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ShaderLayer \"%s\" \"%s\" \"%s\"", type, name, layername);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiShaderLayer");
    }

    RtVoid ri_stdout_context::RiConnectShaderLayers(RtToken type, RtToken layer1,
                                                    RtToken variable1,
                                                    RtToken layer2,
                                                    RtToken variable2)
    {
        begin_function("RiConnectShaderLayers");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ConnectShaderLayers \"%s\" \"%s\" \"%s\" \"%s\" \"%s\"\n",
                    type, layer1, variable1, layer2, variable2);
        }
        end_function("RiConnectShaderLayers");
    }

    RtVoid ri_stdout_context::RiIfBegin(RtString condition)
    {
        begin_function("RiIfBegin");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "IfBegin \"%s\"\n", condition);
        }
        inc_tab();
        push();
        end_function("RiIfBegin");
    }
    RtVoid ri_stdout_context::RiIfEnd()
    {
        begin_function("RiIfEnd");
        pop();
        dec_tab();
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "IfEnd\n");
        }
        end_function("RiIfEnd");
    }
    RtVoid ri_stdout_context::RiElse()
    {
        begin_function("RiElse");
        pop();
        dec_tab();
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Else\n");
        }
        inc_tab();
        push();
        end_function("RiElse");
    }
    RtVoid ri_stdout_context::RiElseIf(RtString condition)
    {
        begin_function("RiElseIf");
        pop();
        dec_tab();
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ElseIf \"%s\"\n", condition);
        }
        inc_tab();
        push();
        end_function("RiElseIf");
    }

    RtVoid ri_stdout_context::RiSetLightHandle(RtInt id, RtLightHandle light)
    {
        begin_function("RiSetLightHandle");
        if (fp_)
        {
            LightSource* pLight = (LightSource*)light;
            print_tab();
            fprintf(fp_, "%s \"%s\" %d%s", pLight->strLight.c_str(),
                    pLight->strName.c_str(), id, pLight->strOther.c_str());
            fprintf(fp_, "\n");
        }
        ri_context::RiSetLightHandle(id, light);
        end_function("RiSetLightHandle");
    }

    RtVoid ri_stdout_context::RiSetLightHandle(RtToken name, RtLightHandle light)
    {
        begin_function("RiSetLightHandle");
        if (fp_)
        {
            LightSource* pLight = (LightSource*)light;
            print_tab();
            fprintf(fp_, "%s \"%s\" \"%s\"%s", pLight->strLight.c_str(),
                    pLight->strName.c_str(), name, pLight->strOther.c_str());
            fprintf(fp_, "\n");
        }

        ri_context::RiSetLightHandle(name, light);
        end_function("RiSetLightHandle");
    }

    RtVoid ri_stdout_context::RiSetObjectHandle(RtInt id, RtObjectHandle obj)
    {
        begin_function("RiSetObjectHandle");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ObjectBegin %d\n", id);
        }
        inc_tab();
        ri_context::RiSetObjectHandle(id, obj);
        end_function("RiSetObjectHandle");
    }

    RtVoid ri_stdout_context::RiSetObjectHandle(RtToken name, RtObjectHandle obj)
    {
        begin_function("RiSetObjectHandle");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "ObjectBegin %s\n", name);
        }
        inc_tab();
        ri_context::RiSetObjectHandle(name, obj);
        end_function("RiSetObjectHandle");
    }

    RtVoid ri_stdout_context::RiProcedural2(RtToken subdivide2func,
                                            RtToken boundfunc, RtInt n,
                                            RtToken tokens[], RtPointer params[])
    {
        begin_function("RiProcedural2");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Procedural2 \"%s\" \"%s\"", subdivide2func, boundfunc);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiProcedural2");
    }

    RtVoid ri_stdout_context::RiSystem(RtString cmd)
    {
        begin_function("RiSystem");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "System \"%s\"", cmd);
            fprintf(fp_, "\n");
        }
        end_function("RiSystem");
    }

    RtVoid ri_stdout_context::RiVolume(RtPointer type, RtBound bounds,
                                       RtInt nvertices[])
    {
        begin_function("RiVolume");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "Volume \"%s\" [%f %f %f %f %f %f] [%d %d %d]", type,
                    bounds[0], bounds[1], bounds[2], bounds[3], bounds[4], bounds[5],
                    nvertices[0], nvertices[1], nvertices[2]);
            fprintf(fp_, "\n");
        }
        end_function("RiVolume");
    }
    RtVoid ri_stdout_context::RiVolumePixelSamples(RtFloat xsamples,
                                                   RtFloat ysamples)
    {
        begin_function("RiVolumePixelSamples");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "VolumePixelSamples %f %f", xsamples, ysamples);
            fprintf(fp_, "\n");
        }
        end_function("RiVolumePixelSamples");
    }
    RtVoid ri_stdout_context::RiVPAtmosphereV(RtToken shadername, RtInt n,
                                              RtToken tokens[],
                                              RtPointer params[])
    {
        begin_function("RiVPAtmosphereV");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "VPAtmosphere %s", shadername);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiVPAtmosphereV");
    }
    RtVoid ri_stdout_context::RiVPInteriorV(RtToken shadername, RtInt n,
                                            RtToken tokens[], RtPointer params[])
    {
        begin_function("RiVPInteriorV");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "VPInterior %s", shadername);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiVPInteriorV");
    }
    RtVoid ri_stdout_context::RiVPSurfaceV(RtToken shadername, RtInt n,
                                           RtToken tokens[], RtPointer params[])
    {
        begin_function("RiVPSurfaceV");
        if (fp_)
        {
            print_tab();
            fprintf(fp_, "VPSurface %s", shadername);
            print_params(n, tokens, params);
            fprintf(fp_, "\n");
        }
        end_function("RiVPSurfaceV");
    }

    //-------------------------------------
    //
    //
    //
    //-------------------------------------

    ri_stdout_context::ri_stdout_context()
    {
        fp_ = stdout;
        bClose_ = false;
        nTab_ = 0;
        push(RI_BEZIERSTEP, RI_BEZIERSTEP);
    }
    ri_stdout_context::ri_stdout_context(const ri_classifier& cls)
        : ri_context(cls)
    {
        fp_ = stdout;
        bClose_ = false;
        nTab_ = 0;
        push(RI_BEZIERSTEP, RI_BEZIERSTEP);
    }
    ri_stdout_context::ri_stdout_context(const char* filename)
    {
        fp_ = fopen(filename, "wt");
        bClose_ = true;
        nTab_ = 0;
        push(RI_BEZIERSTEP, RI_BEZIERSTEP);
    }

    ri_stdout_context::ri_stdout_context(const ri_classifier& cls,
                                         const char* filename)
        : ri_context(cls)
    {
        fp_ = fopen(filename, "wt");
        bClose_ = true;
        nTab_ = 0;
        push(RI_BEZIERSTEP, RI_BEZIERSTEP);
    }
    ri_stdout_context::~ri_stdout_context()
    {
        if (bClose_)
        {
            fflush(fp_);
            fclose(fp_);
        }
        for (size_t i = 0; i < m_pObj_.size(); i++)
        {
            delete m_pObj_[i];
        }
        for (size_t i = 0; i < m_pLight_.size(); i++)
        {
            delete m_pLight_[i];
        }
    }

    void ri_stdout_context::print_tab()
    {
        if (fp_)
        {
            int n = nTab_;
            for (int i = 0; i < n * 4; i++)
            {
                fputc(' ', fp_);
            }
        }
    }

    void ri_stdout_context::push(int ustep, int vstep)
    {
        m_steps_.push_back(step(ustep, vstep));
    }

    void ri_stdout_context::push()
    {
        assert(!m_steps_.empty());
        m_steps_.push_back(m_steps_.back());
    }
    void ri_stdout_context::pop()
    {
        assert(!m_steps_.empty());
        if (m_steps_.size() >= 2)
        {
            m_steps_.pop_back();
        }
    }

    static int GetSize(int nClass, RtInt vertex = 1, RtInt varying = 1,
                       RtInt uniform = 1, RtInt facevarying = 1)
    {
        switch (nClass)
        {
        case ri_classifier::VERTEX:
            return vertex;
        case ri_classifier::UNIFORM:
            return uniform;
        case ri_classifier::CONSTANT:
            return 1;
        case ri_classifier::VARYING:
            return varying;
        case ri_classifier::FACEVARYING:
            return facevarying;
        }
        return 1;
    }

    static std::string ConvertToString(ri_classifier* p_cls, RtToken token,
                                       RtPointer val, RtInt vertex = 1,
                                       RtInt varying = 1, RtInt uniform = 1,
                                       RtInt facevarying = 1)
    {
        int nID = p_cls->def(token);
        if (nID < 0)
            return "";
        const ri_classifier::param_data& data = (*p_cls)[nID];
        int nSz = GetSize(data.get_class(), vertex, varying, uniform, facevarying);
        switch (data.get_type())
        {
        case ri_classifier::INTEGER:
            return ArrayToString((const RtInt*)val, data.get_size() * nSz);
        case ri_classifier::STRING:
            return ArrayToString((const RtString*)val, data.get_size() * nSz);
        case ri_classifier::FLOAT:
        case ri_classifier::COLOR:
        case ri_classifier::POINT:
        case ri_classifier::VECTOR:
        case ri_classifier::NORMAL:
        case ri_classifier::HPOINT:
        case ri_classifier::MATRIX:
            return ArrayToString((const RtFloat*)val, data.get_size() * nSz);
        default:
            break;
        }

        return "";
    }

    void ri_stdout_context::print_params(RtInt count, RtToken tokens[],
                                         RtPointer values[], RtInt vertex,
                                         RtInt varying, RtInt uniform,
                                         RtInt facevarying)
    {
        if (fp_)
        {
            if (count)
            {
                fprintf(fp_, " ");
                ri_temporary_classifier cls(this->RiGetClassifier());
                for (int i = 0; i < count; i++)
                {
                    std::string value = ConvertToString(&cls, tokens[i], values[i], vertex,
                                                        varying, uniform, facevarying);
                    if (value != "")
                    {
                        fprintf(fp_, "\"%s\" %s", tokens[i], value.c_str());
                        if (i != count - 1)
                            fprintf(fp_, " ");
                    }
                }
            }
        }
    }

    std::string ri_stdout_context::convert_params(RtInt count, RtToken tokens[],
                                                  RtPointer values[], RtInt vertex,
                                                  RtInt varying, RtInt uniform,
                                                  RtInt facevarying)
    {
        std::string strOut;
        if (count)
        {
            strOut += " ";
            ri_temporary_classifier cls(this->RiGetClassifier());
            for (int i = 0; i < count; i++)
            {
                std::string value = ConvertToString(&cls, tokens[i], values[i], vertex,
                                                    varying, uniform, facevarying);
                if (value != "")
                {
                    char buffer[1024];
                    SNPRINTF(buffer, 1024, "\"%s\" %s", tokens[i], value.c_str());
                    strOut += buffer;
                    if (i != count - 1)
                        strOut += " ";
                }
            }
        }
        return strOut;
    }

    void ri_stdout_context::inc_tab() { nTab_++; }
    void ri_stdout_context::dec_tab() { nTab_--; }

    void ri_stdout_context::begin_function(const char* szFunctionName) { ; }

    void ri_stdout_context::end_function(const char* szFunctionName) { ; }
}
