#ifndef RI_PATH_RESOLVER_H
#define RI_PATH_RESOLVER_H

#include <string>
#include "ri_options.h"

namespace ri
{
    class ri_path_resolver
    {
    public:
        ri_path_resolver();
        ri_path_resolver(const ri_options& opts);

    public:
        static std::string get_root_path();
        static std::string get_exec_path();
        static std::string get_bin_path();
        static std::string get_plugin_path();
        static std::string get_shader_path() { return get_shaders_path(); }
        static std::string get_shaders_path();
        static std::string get_textures_path();
        static std::string get_archives_path();

    public:
        static std::string get_rib_path();
        static void set_rib_path(const std::string& str);

    public:
        static std::string replace_options_path(const std::string& prev,
                                                const std::string& input);

    public:
        std::vector<std::string> get_shaders_paths();
        std::vector<std::string> get_textures_paths();
        std::vector<std::string> get_archives_paths();

    protected:
        ri_options opts_;
    };
}

#endif
