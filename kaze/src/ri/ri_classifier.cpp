#include "ri_classifier.h"
#include "ri.h"
#include <assert.h>
#include <string>
#include <vector>
#include <map>
#include <sstream>
#include <memory>
#include <string.h>
#include <stdlib.h>

namespace ri
{

    typedef ri_classifier::param_data param_data;

    struct TokenValue
    {
        const char* token;
        int value;
    };

    static const TokenValue TYPES[] = {
        {"int", ri_classifier::INTEGER},
        {"integer", ri_classifier::INTEGER},
        {"float", ri_classifier::FLOAT}, 
        {"string", ri_classifier::STRING}, 
        {"color", ri_classifier::COLOR}, 
        {"point", ri_classifier::POINT}, 
        {"vector", ri_classifier::VECTOR}, 
        {"normal", ri_classifier::NORMAL}, 
        {"hpoint", ri_classifier::HPOINT}, 
        {"mpoint", ri_classifier::MATRIX}, 
        {"matrix", ri_classifier::MATRIX}, 
        {NULL, -1}};

    static const TokenValue CLASSIES[] = {
        {"const", ri_classifier::CONSTANT},
        {"constant", ri_classifier::CONSTANT},
        {"uniform", ri_classifier::UNIFORM},
        {"varying", ri_classifier::VARYING},
        {"vertex", ri_classifier::VERTEX},
        {"facevarying", ri_classifier::FACEVARYING},
        {NULL, -1}};

    static std::vector<std::string> decomposite_stream(const std::string& str)
    {
        std::vector<std::string> sRet;
        std::stringstream ss(str);
        std::string l;
        while (ss)
        {
            l.clear();
            ss >> l;
            if (!l.empty())
            {
                sRet.push_back(l);
            }
        }
        return sRet;
    }

    static std::vector<std::string>
    decomposite_token(const std::vector<std::string>& tRet, const char* tok)
    {
        std::vector<std::string> sRet;
        for (size_t i = 0; i < tRet.size(); i++)
        {
            std::string s = tRet[i];
            std::string::size_type b1 = s.find(tok);
            if (b1 != std::string::npos)
            {
                std::string a = s.substr(0, b1);
                std::string b = s.substr(b1 + 1);
                if (!a.empty())
                    sRet.push_back(a);
                sRet.push_back(tok);
                if (!b.empty())
                    sRet.push_back(b);
            }
            else
            {
                sRet.push_back(s);
            }
        }
        return sRet;
    }

    static std::vector<std::string> decomposite(const std::string& str)
    {
        std::vector<std::string> sRet = decomposite_stream(str);
        sRet = decomposite_token(sRet, "[");
        sRet = decomposite_token(sRet, "]");
        return sRet;
    }

    static int find_type(const std::string& s)
    {
        int i = 0;
        while (TYPES[i].token)
        {
            if (s == TYPES[i].token)
            {
                return TYPES[i].value;
            }
            i++;
        }
        return -1;
    }

    static int find_type(const std::vector<std::string>& words, int& n)
    {
        n = 1;
        int nType = -1;
        size_t i = 0;
        for (i = 0; i < words.size(); i++)
        {
            nType = find_type(words[i]);
            if (nType >= 0)
            {
                break;
            }
        }
        if (nType < 0)
            return -1;

        if (i + 2 < words.size())
        {
            if (words[i + 1] == "[")
            {
                n = atoi(words[i + 2].c_str());
            }
        }

        return nType;
    }

    static int find_class(const std::string& s)
    {
        int i = 0;
        while (CLASSIES[i].token)
        {
            if (s == CLASSIES[i].token)
                return CLASSIES[i].value;
            i++;
        }
        return -1;
    }

    static int find_class(const std::vector<std::string>& words)
    {
        int sz = (int)words.size();
        if (sz == 0)
            return -1;
        for (int i = 0; i < sz; i++)
        {
            int nC = find_class(words[i]);
            if (nC >= 0)
                return nC;
        }
        return -1;
    }

    static int classify(const char* name, int& nClass, int& nType, int& nSize,
                        std::string& strKey)
    {
        std::vector<std::string> words = decomposite(name);
        if (words.empty())
            return -1;

        int sz = (int)words.size();
        if (sz == 0)
            return -1;
        strKey = words.back();

        {
            static ri_classifier cls;
            
            nType = -1;
            nClass = ri_classifier::UNIFORM;
            nSize = 1;

            int id = cls.find_raw(strKey.c_str());
            if(id >= 0)
            {
                nClass = cls[id].get_class();
                nType  = cls[id].get_type();
                nSize  = cls[id].get_size() / ri_classifier::get_size(nType);
            }
        }

        int nC = find_class(words);
        if (nC >= 0)
        {
            nClass = nC;
        }
        
        int nS = 1;
        int nT = find_type(words, nS);
        
        if (nT >= 0)
        {
            nType = nT;
            nSize = nS;
        }

        return nType;
    }

    ri_classifier::ri_classifier() { init(); }
    ri_classifier::ri_classifier(int noinit)
    {
        // init();
    }
    ri_classifier::ri_classifier(const ri_classifier& rhs) : data_(rhs.data_)
    {
        ; //
    }
    ri_classifier::~ri_classifier() { ; }
    size_t ri_classifier::size() const { return data_.size(); }
    const param_data& ri_classifier::operator[](size_t i) const { return data_[i]; }

    int ri_classifier::get_size(int type)
    {
        switch (type)
        {
        case INTEGER:
            return 1;
        case FLOAT:
            return 1;
        case STRING:
            return 1;
        case COLOR:
            return 3;
        case POINT:
            return 3;
        case VECTOR:
            return 3;
        case NORMAL:
            return 3;
        case HPOINT:
            return 4;
        case MATRIX:
            return 16;
        case BASIS:
            return 16; //
        case BOUND:
            return 6; //
        }
        return 1;
    }

    void ri_classifier::add(const param_data& param) { data_.push_back(param); }

    void ri_classifier::add(const char* name_, int class_, int type_, int size_)
    {
        if (size_ <= 0)
            size_ = get_size(type_);
        assert(size_ >= 1);
        data_.push_back(param_data(name_, class_, type_, size_));
    }

    void ri_classifier::add(const char* name_, int class_,
                            const std::string& type_)
    {
        int type = UNIFORM;
        int size = 1;
        std::vector<std::string> words = decomposite(type_);
        type = find_type(words, size);
        if (type >= 0)
        {
            data_.push_back(param_data(name_, class_, type, size));
        }
    }

    bool ri_classifier::check_param_data()
    {
        bool bRet = true;
    #if _DEBUG
        std::map<std::string, param_data> m;
        typedef std::map<std::string, param_data>::const_iterator const_iterator;
        typedef std::map<std::string, param_data>::value_type value_type;
        for (int i = 0; i < (int)data_.size(); i++)
        {
            const_iterator iter = m.find(data_[i].get_name());
            if(iter != m.end())
            {
                if(
                    iter->second.class_ != data_[i].class_ || 
                    iter->second.type_ != data_[i].type_ 
                )
                {
                    printf("parameter %s conflicted (%d)\n", data_[i].get_name(), i);
                    bRet = false;
                }
            }
            else
            {
                m.insert(value_type(data_[i].get_name(), data_[i]));
            }
        }
    #endif
        return bRet;
    }

    int ri_classifier::find_raw(const char* name) const
    {
        for (int i = 0; i < (int)data_.size(); i++)
        {
            if (strcmp(data_[i].get_name(), name) == 0)
                return i;
        }
        return -1;
    }

    int ri_classifier::def(const char* name)
    {
        int id = this->find_raw(name);
        if (id >= 0)
            return id;
        int nType = 0;
        int nClass = 0;
        int nSize = 1;
        std::string key;
        if (classify(name, nClass, nType, nSize, key) >= 0)
        {
            id = this->find_raw(key.c_str());
            if (id >= 0)
            {
                data_[id].name_ = key;
                data_[id].type_ = nType;
                data_[id].class_ = nClass;
                data_[id].size_ = nSize * get_size(nType);

                return id;
            }
            else
            {
                id = (int)this->size();
                this->add(key.c_str(), nClass, nType, nSize * get_size(nType));
                return id;
            }
        }
        return -1;
    }

    int ri_classifier::find(const char* name) const
    {
        int id = this->find_raw(name);
        if (id >= 0)
            return id;
        int nType = 0;
        int nClass = 0;
        int nSize = 1;
        std::string key;
        if (classify(name, nClass, nType, nSize, key) >= 0)
        {
            id = (int)this->find_raw(key.c_str());
            if (id >= 0)
            {
                if (data_[id].name_ != key)
                    return -1;
                if (data_[id].type_ != nType)
                    return -1;
                if (data_[id].class_ != nClass)
                    return -1;
                if (data_[id].size_ != nSize * get_size(nType))
                    return -1;

                return id;
            }
        }
        return -1;
    }

#define DECLARE_PARAM(NAME, CLASS, TYPE) \
    {                                    \
        this->add(NAME, CLASS, TYPE);    \
    }

    void ri_classifier::init()
    {
        DECLARE_PARAM(RI_KA, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_KD, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_KS, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_KR, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_ROUGHNESS, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_TEXTURENAME, UNIFORM, STRING);
        DECLARE_PARAM(RI_SPECULARCOLOR, UNIFORM, COLOR);
        DECLARE_PARAM(RI_INTENSITY, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_LIGHTCOLOR, UNIFORM, COLOR);
        DECLARE_PARAM(RI_FROM, UNIFORM, POINT);
        DECLARE_PARAM(RI_TO, UNIFORM, POINT);
        DECLARE_PARAM(RI_CONEANGLE, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_CONEDELTAANGLE, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_BEAMDISTRIBUTION, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_MINDISTANCE, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_MAXDISTANCE, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_DISTANCE, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_BACKGROUND, UNIFORM, COLOR);
        DECLARE_PARAM(RI_FOV, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_P, VERTEX, POINT);
        DECLARE_PARAM(RI_PZ, VERTEX, POINT);
        DECLARE_PARAM(RI_PW, VERTEX, HPOINT); // 4
        DECLARE_PARAM(RI_N, VARYING, NORMAL);
        DECLARE_PARAM(RI_NP, UNIFORM, NORMAL);
        DECLARE_PARAM(RI_CS, VARYING, COLOR);
        DECLARE_PARAM(RI_OS, VARYING, COLOR);
        DECLARE_PARAM(RI_S, VARYING, FLOAT);
        DECLARE_PARAM(RI_T, VARYING, FLOAT);
        DECLARE_PARAM(RI_ST, VARYING, "float[2]");

        DECLARE_PARAM(RI_AMPLITUDE, UNIFORM, FLOAT);
        DECLARE_PARAM(RI_WIDTH, VARYING, FLOAT);
        DECLARE_PARAM(RI_CONSTANTWIDTH, CONSTANT, FLOAT);

        DECLARE_PARAM("gridsize", UNIFORM, INTEGER);
        DECLARE_PARAM("texturememory", UNIFORM, INTEGER);
        DECLARE_PARAM("bucketsize", UNIFORM, "integer[2]");
        DECLARE_PARAM("eyesplits", UNIFORM, INTEGER);
        DECLARE_PARAM(RI_SHADER, UNIFORM, STRING);
        DECLARE_PARAM("archive", UNIFORM, STRING);
        DECLARE_PARAM("texture", UNIFORM, STRING);
        DECLARE_PARAM("display", UNIFORM, STRING);
        DECLARE_PARAM("auto_shadows", UNIFORM, STRING);
        DECLARE_PARAM("endofframe", UNIFORM, INTEGER);
        DECLARE_PARAM("sphere", UNIFORM, FLOAT);
        DECLARE_PARAM("coordinatesystem", UNIFORM, STRING);
        DECLARE_PARAM("shadows", UNIFORM, STRING);
        DECLARE_PARAM("shadowmapsize", UNIFORM, "integer[2]"); //
        DECLARE_PARAM("shadowangle", UNIFORM, FLOAT);
        DECLARE_PARAM("shadowmapname", UNIFORM, STRING);
        DECLARE_PARAM("shadow_shadingrate", UNIFORM, FLOAT);
        DECLARE_PARAM(RI_NAME, UNIFORM, STRING);
        DECLARE_PARAM("shadinggroup", UNIFORM, STRING);
        DECLARE_PARAM("sense", UNIFORM, STRING);
        DECLARE_PARAM("compression", UNIFORM, STRING);
        DECLARE_PARAM("quality", UNIFORM, INTEGER);
        DECLARE_PARAM("bias0", UNIFORM, FLOAT);
        DECLARE_PARAM("bias1", UNIFORM, FLOAT);
        DECLARE_PARAM("jitter", UNIFORM, INTEGER);
        DECLARE_PARAM("depthfilter", UNIFORM, STRING);

        DECLARE_PARAM("binary", UNIFORM, INTEGER);
        DECLARE_PARAM("procedural", UNIFORM, STRING);
        DECLARE_PARAM("quantize", UNIFORM, "float[4]");
        DECLARE_PARAM("dither", UNIFORM, FLOAT);
        DECLARE_PARAM("interpolateboundary", UNIFORM, INTEGER);
        DECLARE_PARAM("zthreshold", UNIFORM, COLOR);
        DECLARE_PARAM("pointtype", UNIFORM, STRING);

        // GeometricApproximation
        DECLARE_PARAM("edgelength", UNIFORM, FLOAT);

        // Lightsource
        DECLARE_PARAM("blur", UNIFORM, FLOAT);
        DECLARE_PARAM("falloff", UNIFORM, INTEGER);
        DECLARE_PARAM("shadowsamples", UNIFORM, INTEGER);
        DECLARE_PARAM("shadowattribute", UNIFORM, STRING);

        // cull
        DECLARE_PARAM("backfacing", UNIFORM, INTEGER);
        DECLARE_PARAM("hidden", UNIFORM, INTEGER);

        // derivatives
        DECLARE_PARAM("centered", UNIFORM, INTEGER);
        DECLARE_PARAM("extrapolate", UNIFORM, INTEGER);

        // dice
        //DECLARE_PARAM("binary", UNIFORM, INTEGER);
        DECLARE_PARAM("hair", UNIFORM, INTEGER);
        DECLARE_PARAM("rasterorient", UNIFORM, INTEGER);
        DECLARE_PARAM("strategy", UNIFORM, STRING);
        DECLARE_PARAM("referencecamera", UNIFORM, STRING);

        DECLARE_PARAM("pointfalloff", UNIFORM, INTEGER);
        DECLARE_PARAM("pointfalloffpower", UNIFORM, FLOAT);

        // sides
        DECLARE_PARAM("backfacetolerance", UNIFORM, FLOAT);
        DECLARE_PARAM("doubleshaded", UNIFORM, INTEGER);

        // shade
        DECLARE_PARAM("volumeintersectionstrategy", UNIFORM, STRING);
        DECLARE_PARAM("volumeintersectionpriority", UNIFORM, FLOAT);

        // stich
        DECLARE_PARAM("enable", UNIFORM, INTEGER);
        DECLARE_PARAM("traceenable", UNIFORM, INTEGER);
        DECLARE_PARAM("newgroup", UNIFORM, INTEGER);

        // trimcurve
        //DECLARE_PARAM("sense", UNIFORM, STRING);
        DECLARE_PARAM("rational", UNIFORM, INTEGER); //

        // procedural
        DECLARE_PARAM("attributes", UNIFORM, STRING);

        //
        // From Pixie
        //

        // See URL: http://www.renderpixie.com/pixiewiki/Documentation/Attributes

        // Dice
        DECLARE_PARAM("numprobes", UNIFORM, "integer[2]");
        DECLARE_PARAM("minsplits", UNIFORM, INTEGER);
        DECLARE_PARAM("boundexpand", UNIFORM, FLOAT);
        //DECLARE_PARAM("binary", UNIFORM, INTEGER);

        // Displacement
        // DECLARE_PARAM( "sphere", UNIFORM, FLOAT );
        // DECLARE_PARAM( "coordinatesystem", UNIFORM, STRING );

        // Shade
        DECLARE_PARAM("transmissionhitmode", UNIFORM, STRING);
        DECLARE_PARAM("diffusehitmode", UNIFORM, STRING);
        DECLARE_PARAM("specularhitmode", UNIFORM, STRING);
        DECLARE_PARAM("camerahitmode", UNIFORM, STRING);

        // Trace
        DECLARE_PARAM("displacements", UNIFORM, INTEGER);
        DECLARE_PARAM("bias", UNIFORM, FLOAT);
        DECLARE_PARAM("maxdiffusedepth", UNIFORM, INTEGER);
        DECLARE_PARAM("maxspeculardepth", UNIFORM, INTEGER);

        // Irradiance
        DECLARE_PARAM("handle", UNIFORM, STRING);
        DECLARE_PARAM("filemode", UNIFORM, STRING);
        DECLARE_PARAM("maxerror", UNIFORM, FLOAT);
        DECLARE_PARAM("maxpixeldist", UNIFORM, FLOAT);

        // Photon
        DECLARE_PARAM("globalmap", UNIFORM, STRING);
        DECLARE_PARAM("causticmap", UNIFORM, STRING);
        DECLARE_PARAM("shadingmodel", UNIFORM, STRING);
        DECLARE_PARAM("estimator", UNIFORM, INTEGER);

        // Visibility
        DECLARE_PARAM("camera", UNIFORM, INTEGER);
        DECLARE_PARAM("diffuse", UNIFORM, INTEGER);
        DECLARE_PARAM("specular", UNIFORM, INTEGER);
        DECLARE_PARAM("photon", UNIFORM, INTEGER);
        DECLARE_PARAM("transmission", UNIFORM, INTEGER);
        DECLARE_PARAM("trace", UNIFORM, INTEGER);
        DECLARE_PARAM("shadow", UNIFORM, INTEGER);
        DECLARE_PARAM("instancerbase", UNIFORM, INTEGER);

        // Options
        DECLARE_PARAM("bucketorder", UNIFORM, STRING);

        //
        // Form Air
        //

        //
        // Air Options
        //

        // Air bakemesh
        DECLARE_PARAM("bake", UNIFORM, STRING);
        DECLARE_PARAM("bakemesh", UNIFORM, STRING);
        DECLARE_PARAM("meshfile", UNIFORM, STRING);
        DECLARE_PARAM("bakemaps", UNIFORM, STRING);
        DECLARE_PARAM("border", UNIFORM, INTEGER);

        // Air Shading
        DECLARE_PARAM("shadingmultiplier", UNIFORM, FLOAT);
        DECLARE_PARAM("prmanspecular", UNIFORM, INTEGER);
        DECLARE_PARAM("algorithm", UNIFORM, STRING);
        DECLARE_PARAM("shadingcache", UNIFORM, INTEGER);
        //DECLARE_PARAM("texturememory", UNIFORM, INTEGER);
        DECLARE_PARAM("automapmemory", UNIFORM, INTEGER);

        // Air Sampling
        DECLARE_PARAM("edgemask", UNIFORM, INTEGER);
        DECLARE_PARAM("fields", UNIFORM, INTEGER);
        DECLARE_PARAM("prefilterclamptoalpha", UNIFORM, INTEGER);

        // Air Ray Tracing
        DECLARE_PARAM("max_raylevel", UNIFORM, INTEGER);
        DECLARE_PARAM("max_depth", UNIFORM, INTEGER);
        DECLARE_PARAM("minweight", UNIFORM, FLOAT);
        DECLARE_PARAM("reducesamples", UNIFORM, FLOAT);
        DECLARE_PARAM("optimizetracememory", UNIFORM, FLOAT);

        // Air Traced Reflections
        DECLARE_PARAM("background", UNIFORM, COLOR);
        DECLARE_PARAM("envname", UNIFORM, STRING);
        DECLARE_PARAM("envstrength", UNIFORM, FLOAT);
        DECLARE_PARAM("envsamples", UNIFORM, INTEGER);
        DECLARE_PARAM("envblur", UNIFORM, FLOAT);
        DECLARE_PARAM("envspace", UNIFORM, STRING);

        // Air Indirect Illumination
        DECLARE_PARAM("maxbounce", UNIFORM, INTEGER);
        DECLARE_PARAM("savefile", UNIFORM, STRING);
        DECLARE_PARAM("seedfile", UNIFORM, STRING);
        DECLARE_PARAM("prepass", UNIFORM, INTEGER);
        DECLARE_PARAM("prepassfactor", UNIFORM, FLOAT);
        DECLARE_PARAM("prepasschannels", UNIFORM, STRING);
        DECLARE_PARAM("prepassgroup", UNIFORM, STRING);
        // DECLARE_PARAM( "background", UNIFORM, COLOR );
        DECLARE_PARAM("background_spd", UNIFORM, STRING);
        // DECLARE_PARAM( "envname", UNIFORM, STRING );
        // DECLARE_PARAM( "envstrength", UNIFORM, FLOAT );
        // DECLARE_PARAM( "envsamples", UNIFORM, INTEGER );
        // DECLARE_PARAM( "envblur", UNIFORM, FLOAT );
        // DECLARE_PARAM( "envspace", UNIFORM, STRING );

        // Air Spectral Prefiltering and Chromatic Adaptation
        DECLARE_PARAM("dominantilluminant_spd", UNIFORM, STRING);
        DECLARE_PARAM("displayilluminant_spd", UNIFORM, STRING);
        DECLARE_PARAM("currentcolorspace", UNIFORM, STRING);

        // Air Occlusion
        // DECLARE_PARAM( "savefile", UNIFORM, STRING );
        // DECLARE_PARAM( "seedfile", UNIFORM, STRING );
        //DECLARE_PARAM("prepass", UNIFORM, INTEGER);
        //DECLARE_PARAM("prepassfactor", UNIFORM, FLOAT);

        // Air Caustics
        // DECLARE_PARAM( "savefile", UNIFORM, STRING );
        // DECLARE_PARAM( "seedfile", UNIFORM, STRING );

        // Air Subsurface Scattering
        DECLARE_PARAM("subsurfacetolerance", UNIFORM, FLOAT);
        DECLARE_PARAM("sssmultithread", UNIFORM, INTEGER);
        DECLARE_PARAM("sssautocache", UNIFORM, INTEGER);
        DECLARE_PARAM("sssinfo", UNIFORM, INTEGER);

        // Air Toon Rendering
        DECLARE_PARAM("maxinkwidth", UNIFORM, FLOAT);
        DECLARE_PARAM("fadethreshold", UNIFORM, FLOAT);
        DECLARE_PARAM("mininkwidth", UNIFORM, FLOAT);
        DECLARE_PARAM("inkwidthmultiplier", UNIFORM, FLOAT);
        DECLARE_PARAM("vectorexportfile", UNIFORM, STRING);

        // Air Network Cache
        // DECLARE_PARAM("cachedir", UNIFORM, STRING);
        // DECLARE_PARAM("cachesize", UNIFORM, INTEGER);
        DECLARE_PARAM("debug", UNIFORM, INTEGER);

        // Air Search Paths
        DECLARE_PARAM("dirmap", UNIFORM, STRING);
        DECLARE_PARAM("dirmapzone", UNIFORM, STRING);
        DECLARE_PARAM("dirmapdebug", UNIFORM, INTEGER);
        // DECLARE_PARAM("texture", UNIFORM, STRING);
        // DECLARE_PARAM("archive", UNIFORM, STRING);
        // DECLARE_PARAM("display", UNIFORM, STRING);
        // DECLARE_PARAM("procedural", UNIFORM, STRING);
        DECLARE_PARAM("spectrum", UNIFORM, STRING);
        DECLARE_PARAM("resource", UNIFORM, STRING);

        //
        // Air Attribures
        //

        // Air Light
        //DECLARE_PARAM("nsamples", UNIFORM, INTEGER);
        DECLARE_PARAM("maxdist", UNIFORM, FLOAT);
        DECLARE_PARAM("has_shadows", UNIFORM, INTEGER);
        DECLARE_PARAM("automapsize", UNIFORM, INTEGER);
        DECLARE_PARAM("automapscreenwindow", UNIFORM, "float[2]");

        // Air Visibility
        // DECLARE_PARAM( "camera", UNIFORM, INTEGER );
        // DECLARE_PARAM( "transmission", UNIFORM, INTEGER );
        // DECLARE_PARAM("transmissionhitmode", UNIFORM, STRING);
        // DECLARE_PARAM( "trace", UNIFORM, INTEGER );
        DECLARE_PARAM("indirect", UNIFORM, INTEGER);
        // DECLARE_PARAM("subset", UNIFORM, STRING);

        // Air Motion Blur
        DECLARE_PARAM("motionfactor", UNIFORM, FLOAT);
        DECLARE_PARAM("motionsamples", UNIFORM, INTEGER);
        DECLARE_PARAM("shutterscale", UNIFORM, FLOAT);
        DECLARE_PARAM("shutteroffset", UNIFORM, FLOAT);
        DECLARE_PARAM("adaptivemotionblur", UNIFORM, INTEGER);

        // Air Ray Tracing
        // DECLARE_PARAM("bias", UNIFORM, FLOAT);
        DECLARE_PARAM("motionblur", UNIFORM, INTEGER);
        // DECLARE_PARAM("displacements", UNIFORM, INTEGER);

        // Air Displacement
        DECLARE_PARAM("truedisplacement", UNIFORM, INTEGER);
        // DECLARE_PARAM( "sphere", UNIFORM, FLOAT );
        DECLARE_PARAM("normaldisplacement", UNIFORM, INTEGER);
        DECLARE_PARAM("triangledisplacement", UNIFORM, INTEGER);
        DECLARE_PARAM("smoothlevel", UNIFORM, INTEGER);
        DECLARE_PARAM("fastdisplacement", UNIFORM, INTEGER);

        // Indirect Illumination
        // DECLARE_PARAM("maxerror", UNIFORM, FLOAT);
        // DECLARE_PARAM("maxpixeldist", UNIFORM, FLOAT);
        // DECLARE_PARAM("nsamples", UNIFORM, INTEGER);
        DECLARE_PARAM("shading", UNIFORM, STRING);
        DECLARE_PARAM("strength", UNIFORM, FLOAT);
        DECLARE_PARAM("averagecolor", UNIFORM, INTEGER);
        DECLARE_PARAM("averagecolor_spd", UNIFORM, STRING);
        DECLARE_PARAM("maxhitdist", UNIFORM, FLOAT);
        DECLARE_PARAM("moving", UNIFORM, INTEGER);
        DECLARE_PARAM("reflective", UNIFORM, INTEGER);
        DECLARE_PARAM("adaptivesampling", UNIFORM, INTEGER);
        //DECLARE_PARAM("prepass", UNIFORM, INTEGER);

        // Air Caustics
        DECLARE_PARAM("ngather", UNIFORM, INTEGER);
        // DECLARE_PARAM("maxpixeldist", UNIFORM, FLOAT);
        // DECLARE_PARAM("specularcolor", UNIFORM, COLOR);
        DECLARE_PARAM("refractioncolor", UNIFORM, COLOR);
        DECLARE_PARAM("refractionindex", UNIFORM, FLOAT);
        DECLARE_PARAM("dispersion", UNIFORM, FLOAT);

        // Air Subsurface Scattering / Point Sets
        // DECLARE_PARAM("handle", UNIFORM, STRING);
        DECLARE_PARAM("cachemode", UNIFORM, STRING);

        // Air Geometric Approximation
        DECLARE_PARAM("divisionlevel", UNIFORM, INTEGER);
        DECLARE_PARAM("udivisions", UNIFORM, INTEGER);
        DECLARE_PARAM("vdivisions", UNIFORM, INTEGER);
        DECLARE_PARAM("trimborder", UNIFORM, FLOAT);
        DECLARE_PARAM("render", UNIFORM, STRING);
        DECLARE_PARAM("type", UNIFORM, STRING);
        DECLARE_PARAM("clustermax", UNIFORM, INTEGER);
        DECLARE_PARAM("flatnessspace", UNIFORM, STRING);
        // DECLARE_PARAM("edgelength", UNIFORM, FLOAT);

        // Air Toon Rendering
        DECLARE_PARAM("id", UNIFORM, INTEGER);
        DECLARE_PARAM("ink", UNIFORM, COLOR);
        DECLARE_PARAM("inkwidth", UNIFORM, FLOAT);
        // DECLARE_PARAM("zthreshold", UNIFORM, FLOAT);
        DECLARE_PARAM("silhouette", UNIFORM, FLOAT);
        DECLARE_PARAM("frontback", UNIFORM, FLOAT);
        DECLARE_PARAM("nakededges", UNIFORM, FLOAT);
        DECLARE_PARAM("faceangle", UNIFORM, FLOAT);

        // Air Geometry Export
        // DECLARE_PARAM("meshfile", UNIFORM, STRING);
        DECLARE_PARAM("makePref", UNIFORM, INTEGER);
        DECLARE_PARAM("makeNref", UNIFORM, INTEGER);
        DECLARE_PARAM("meshspace", UNIFORM, STRING);

        // Air Miscellaneous
        DECLARE_PARAM("nparallel", UNIFORM, INTEGER);
        DECLARE_PARAM("color_spd", UNIFORM, STRING);
        DECLARE_PARAM("runprogramthreads", UNIFORM, INTEGER);

        // Air Grouping
        DECLARE_PARAM("membership", UNIFORM, STRING);

        //
        // Air Shaders
        //

        // Lights
        // DECLARE_PARAM("intensity", UNIFORM, FLOAT);
        // DECLARE_PARAM("lightcolor", UNIFORM, COLOR);
        // DECLARE_PARAM("coneangle", UNIFORM, FLOAT);
        // DECLARE_PARAM("falloff", UNIFORM, INTEGER);
        DECLARE_PARAM("shadowname", UNIFORM, STRING);
        DECLARE_PARAM("shadowbias", UNIFORM, FLOAT);
        DECLARE_PARAM("shadowblur", UNIFORM, FLOAT);
        // DECLARE_PARAM( "shadowsamples", UNIFORM, INTEGER );
        DECLARE_PARAM("shadowcolor", UNIFORM, COLOR);
        DECLARE_PARAM("shadowgroups", UNIFORM, STRING);
        // DECLARE_PARAM("shadowattribute", UNIFORM, STRING);
        DECLARE_PARAM("__nondiffuse", UNIFORM, INTEGER);
        DECLARE_PARAM("__nonspecular", UNIFORM, INTEGER);
        DECLARE_PARAM("__foglight", UNIFORM, INTEGER);
        DECLARE_PARAM("__channel", UNIFORM, INTEGER);
        // Output only variables
        DECLARE_PARAM("__shadow", UNIFORM, COLOR);
        DECLARE_PARAM("__unshadowed_Cl", UNIFORM, COLOR);
        // Baked Shadows
        DECLARE_PARAM("bakemap", UNIFORM, STRING);
        DECLARE_PARAM("bakemode", UNIFORM, STRING);
        DECLARE_PARAM("bakemapblur", UNIFORM, FLOAT);
        DECLARE_PARAM("bakemapfilterwidth", UNIFORM, FLOAT);
        DECLARE_PARAM("motionspace", UNIFORM, STRING);
        // envlight
        DECLARE_PARAM("sampleenvironment", UNIFORM, INTEGER);
        DECLARE_PARAM("envmap", UNIFORM, STRING);
        // DECLARE_PARAM( "envspace", UNIFORM, STRING );
        // DECLARE_PARAM( "envsamples", UNIFORM, INTEGER );
        // DECLARE_PARAM( "envblur", UNIFORM, FLOAT );
        DECLARE_PARAM("mapname", UNIFORM, STRING);
        DECLARE_PARAM("mapblur", UNIFORM, FLOAT);
        DECLARE_PARAM("mapbias", UNIFORM, FLOAT);
        DECLARE_PARAM("maxsolidangle", UNIFORM, FLOAT);
        DECLARE_PARAM("__category", UNIFORM, STRING);
        // DECLARE_PARAM( "__nonspecular", UNIFORM, INTEGER );
        // DECLARE_PARAM( "__foglight", UNIFORM, INTEGER );
        DECLARE_PARAM("__indirectlight", UNIFORM, INTEGER);
        // DECLARE_PARAM( "__channel", UNIFORM, INTEGER );

        //
        DECLARE_PARAM("__bgchannel", UNIFORM, INTEGER);
        DECLARE_PARAM("__channels", UNIFORM, "float[30]"); // color[10]

        // massive_envlight
        DECLARE_PARAM("rotate_x", UNIFORM, FLOAT);
        DECLARE_PARAM("rotate_y", UNIFORM, FLOAT);
        DECLARE_PARAM("rotate_z", UNIFORM, FLOAT);

        // sunlight
        DECLARE_PARAM("date_month", UNIFORM, INTEGER);
        DECLARE_PARAM("date_day", UNIFORM, INTEGER);
        DECLARE_PARAM("date_hour", UNIFORM, INTEGER);
        DECLARE_PARAM("latitude_longitude", UNIFORM, FLOAT);
        DECLARE_PARAM("time_zone", UNIFORM, INTEGER);
        DECLARE_PARAM("animate_hour", UNIFORM, INTEGER);
        DECLARE_PARAM("animate_time_scale", UNIFORM, FLOAT);
        DECLARE_PARAM("coord_sys_y_is_up", UNIFORM, INTEGER);
        DECLARE_PARAM("coord_sys_north_angle", UNIFORM, FLOAT);
        DECLARE_PARAM("coord_sys_right_handed", UNIFORM, INTEGER);

        // particle
        DECLARE_PARAM("Diffuse", UNIFORM, FLOAT);
        DECLARE_PARAM("Incandescence", UNIFORM, FLOAT);
        DECLARE_PARAM("Ambient", UNIFORM, FLOAT);
        DECLARE_PARAM("OpacityMultiplier", UNIFORM, FLOAT);
        DECLARE_PARAM("PrimaryScatter", UNIFORM, FLOAT);
        DECLARE_PARAM("PrimaryScatterWeight", UNIFORM, FLOAT);
        DECLARE_PARAM("PrimaryScatterColor", UNIFORM, COLOR);
        DECLARE_PARAM("SecondScatter", UNIFORM, FLOAT);
        DECLARE_PARAM("SecondScatterColor", UNIFORM, COLOR);
        DECLARE_PARAM("AngleFalloff", UNIFORM, FLOAT);

        // VGrowGrass
        DECLARE_PARAM("Clumps", UNIFORM, FLOAT);
        DECLARE_PARAM("Density", UNIFORM, FLOAT);
        DECLARE_PARAM("LengthMax", UNIFORM, FLOAT);
        DECLARE_PARAM("LengthVaryFrequency", UNIFORM, FLOAT);
        DECLARE_PARAM("TiltMax", UNIFORM, FLOAT);
        DECLARE_PARAM("TiltVaryFrequency", UNIFORM, FLOAT);
        DECLARE_PARAM("Width", UNIFORM, FLOAT);
        DECLARE_PARAM("GrassColor", UNIFORM, COLOR);
        DECLARE_PARAM("GrassRootColor", UNIFORM, COLOR);
        DECLARE_PARAM("GrassColorVaryHSL", UNIFORM, FLOAT);

        // VInstanceTree
        DECLARE_PARAM("TreeArchive", UNIFORM, STRING);
        DECLARE_PARAM("TreeScale", UNIFORM, FLOAT);
        DECLARE_PARAM("TreeSeed", UNIFORM, FLOAT);
        DECLARE_PARAM("TreeRotateX", UNIFORM, FLOAT);
        DECLARE_PARAM("TreeQuality", UNIFORM, FLOAT);
        DECLARE_PARAM("LeafColorOverride", UNIFORM, INTEGER);
        DECLARE_PARAM("LeafVaryHSL", UNIFORM, COLOR);
        DECLARE_PARAM("LeafColor", UNIFORM, COLOR);

        // V3DArray
        DECLARE_PARAM("Archive", UNIFORM, STRING);
        DECLARE_PARAM("CountXYZ", UNIFORM, POINT);
        DECLARE_PARAM("OffsetX", UNIFORM, POINT);
        DECLARE_PARAM("OffsetY", UNIFORM, POINT);
        DECLARE_PARAM("OffsetZ", UNIFORM, POINT);
        DECLARE_PARAM("Origin", UNIFORM, POINT);

        //
        // 3Delight
        //
        // RiDisplay
        DECLARE_PARAM("driver", UNIFORM, STRING);
        DECLARE_PARAM("filter", UNIFORM, STRING);
        DECLARE_PARAM("filterwidth", UNIFORM, "float[2]");
        DECLARE_PARAM("associatealpha", UNIFORM, INTEGER);
        DECLARE_PARAM("matte", UNIFORM, INTEGER);
        DECLARE_PARAM("exclusive", UNIFORM, INTEGER);
        DECLARE_PARAM("subset", UNIFORM, STRING); //

        // RiHider
        // DECLARE_PARAM("depthfilter", UNIFORM, STRING);
        // DECLARE_PARAM("jitter", UNIFORM, INTEGER); //
        DECLARE_PARAM("samplemotion", UNIFORM, INTEGER);
        DECLARE_PARAM("extrememotiondof", UNIFORM, INTEGER);
        DECLARE_PARAM("maxvpdepth", UNIFORM, INTEGER);
        DECLARE_PARAM("midpointratio", UNIFORM, FLOAT);
        DECLARE_PARAM("progressive", UNIFORM, INTEGER);

        // Rendering Options
        DECLARE_PARAM("nthreads", UNIFORM, INTEGER);
        // DECLARE_PARAM("bucketorder", UNIFORM, STRING);         //
        DECLARE_PARAM("standardatmosphere", UNIFORM, INTEGER); //
        DECLARE_PARAM("volumeshadingrate", UNIFORM, FLOAT);
        DECLARE_PARAM("efficiency", UNIFORM, "float[2]");
        DECLARE_PARAM("offset", UNIFORM, FLOAT);
        DECLARE_PARAM("specularthreshold", UNIFORM, FLOAT);
        DECLARE_PARAM("emit", UNIFORM, INTEGER);
        DECLARE_PARAM("writemaps", UNIFORM, INTEGER);
        // DECLARE_PARAM("maxdiffusedepth", UNIFORM, INTEGER);
        // DECLARE_PARAM("maxspeculardepth", UNIFORM, INTEGER);

        // Quality vs. Performance Options
        // DECLARE_PARAM( "bucketsize", UNIFORM, "integer[2]" );
        // DECLARE_PARAM( "gridsize", UNIFORM, INTEGER );
        // DECLARE_PARAM("eyesplits", UNIFORM, INTEGER);
        DECLARE_PARAM("geommemory", UNIFORM, INTEGER);
        // DECLARE_PARAM("texturememory", UNIFORM, INTEGER);
        DECLARE_PARAM("othreshold", UNIFORM, COLOR);
        // DECLARE_PARAM("zthreshold", UNIFORM, COLOR);
        DECLARE_PARAM("volumegroupsize", UNIFORM, INTEGER);
        DECLARE_PARAM("sample", UNIFORM, INTEGER);
        DECLARE_PARAM("maxdepth", UNIFORM, INTEGER);
        DECLARE_PARAM("approximation", UNIFORM, FLOAT);

        // Search Paths Options
        DECLARE_PARAM("shader", UNIFORM, STRING);
        //DECLARE_PARAM("texture", UNIFORM, STRING);
        //DECLARE_PARAM("archive", UNIFORM, STRING);
        //DECLARE_PARAM("display", UNIFORM, STRING);
        //DECLARE_PARAM("procedural", UNIFORM, STRING);
        DECLARE_PARAM("spectrum", UNIFORM, STRING);
        DECLARE_PARAM("resource", UNIFORM, STRING);
        DECLARE_PARAM("dirmap", UNIFORM, STRING);
        DECLARE_PARAM("rslpluginmap", UNIFORM, STRING);

        // Statistics Options
        DECLARE_PARAM("endofframe", UNIFORM, INTEGER);

        // Message Options
        DECLARE_PARAM("mindispbound", UNIFORM, FLOAT);

        // Network Cache Options
        DECLARE_PARAM("cachedir", UNIFORM, STRING);
        DECLARE_PARAM("cachesize", UNIFORM, INTEGER);
        DECLARE_PARAM("minfreespace", UNIFORM, INTEGER);
        DECLARE_PARAM("writecache", UNIFORM, INTEGER);
        DECLARE_PARAM("includepaths", UNIFORM, STRING);
        DECLARE_PARAM("excludepaths", UNIFORM, STRING);

        // RIB Output Options
        DECLARE_PARAM("callprocedurals", UNIFORM, INTEGER);
        DECLARE_PARAM("compression", UNIFORM, STRING);
        DECLARE_PARAM("format", UNIFORM, STRING);
        DECLARE_PARAM("header", UNIFORM, STRING);
        DECLARE_PARAM("ident", UNIFORM, STRING);
        DECLARE_PARAM("pipe", UNIFORM, INTEGER);

        // Other Options
        DECLARE_PARAM("waitforlicense", UNIFORM, INTEGER);
        DECLARE_PARAM("holdlicense", UNIFORM, INTEGER);
        DECLARE_PARAM("server", UNIFORM, STRING);

        // Primitives Identification
        DECLARE_PARAM("tracesubset", UNIFORM, STRING);

        // Primitives Visibility and Ray Tracing
        DECLARE_PARAM("camera", UNIFORM, INTEGER);
        DECLARE_PARAM("trace", UNIFORM, INTEGER);
        DECLARE_PARAM("diffuse", UNIFORM, INTEGER);
        DECLARE_PARAM("specular", UNIFORM, INTEGER);
        DECLARE_PARAM("transmission", UNIFORM, INTEGER); // STRING->INTEGER
        DECLARE_PARAM("photon", UNIFORM, INTEGER);
        DECLARE_PARAM("subsurface", UNIFORM, STRING);
        DECLARE_PARAM("diffusehitmode", UNIFORM, STRING);
        DECLARE_PARAM("specularhitmode", UNIFORM, STRING);
        //DECLARE_PARAM("transmissionhitmode", UNIFORM, STRING);
        DECLARE_PARAM("photonhitmode", UNIFORM, STRING);
        DECLARE_PARAM("camerahitmode", UNIFORM, STRING);
        DECLARE_PARAM("viewdependent", UNIFORM, INTEGER);
        DECLARE_PARAM("hidden", UNIFORM, INTEGER);
        DECLARE_PARAM("backfacing", UNIFORM, INTEGER);
        DECLARE_PARAM("composite", UNIFORM, INTEGER);
        DECLARE_PARAM("samplemotion", UNIFORM, INTEGER);
        // DECLARE_PARAM("maxdiffusedepth", UNIFORM, INTEGER);
        // DECLARE_PARAM("maxspeculardepth", UNIFORM, INTEGER);
        DECLARE_PARAM("samples", UNIFORM, INTEGER);
        DECLARE_PARAM("samplingstrategy", UNIFORM, STRING);

        // Global Illumination Attributes
        DECLARE_PARAM("nsamples", UNIFORM, INTEGER);
        DECLARE_PARAM("shadingrate", UNIFORM, FLOAT);
        //　DECLARE_PARAM("maxerror", UNIFORM, FLOAT);
        DECLARE_PARAM("reflectance", UNIFORM, COLOR);
        DECLARE_PARAM("shadingmodel", UNIFORM, STRING);
        DECLARE_PARAM("causticmap", UNIFORM, STRING);
        DECLARE_PARAM("globalmap", UNIFORM, STRING);
        DECLARE_PARAM("estimator", UNIFORM, INTEGER);

        // Subsurface Light Transport
        DECLARE_PARAM("absorption", UNIFORM, COLOR);
        DECLARE_PARAM("scattering", UNIFORM, COLOR);
        DECLARE_PARAM("meanfreepath", UNIFORM, COLOR);
        DECLARE_PARAM("reflectance", UNIFORM, COLOR);
        DECLARE_PARAM("refractionindex", UNIFORM, FLOAT);
        DECLARE_PARAM("scale", UNIFORM, FLOAT);
        DECLARE_PARAM("shadingrate", UNIFORM, FLOAT);
        DECLARE_PARAM("referencecamera", UNIFORM, STRING);

        // Displacement
        DECLARE_PARAM("displacement", UNIFORM, FLOAT);
        DECLARE_PARAM("displacementshadingrate", UNIFORM, FLOAT);
        DECLARE_PARAM("inflategrids", UNIFORM, FLOAT);
        DECLARE_PARAM("concatenate", UNIFORM, INTEGER);
        DECLARE_PARAM("detail", UNIFORM, INTEGER);

        // Other Attributes
        DECLARE_PARAM("backfacetolerance", UNIFORM, FLOAT);
        DECLARE_PARAM("doubleshaded", UNIFORM, INTEGER);
        //DECLARE_PARAM("sense", UNIFORM, STRING);
        DECLARE_PARAM("rasterorient", UNIFORM, INTEGER);
        // DECLARE_PARAM("hair", UNIFORM, FLOAT);
        DECLARE_PARAM("hairumin", UNIFORM, FLOAT);
        DECLARE_PARAM("lodreferencecamera", UNIFORM, STRING);
        DECLARE_PARAM("referencecamera", UNIFORM, STRING);
        DECLARE_PARAM("centered", UNIFORM, INTEGER);
        DECLARE_PARAM("forcesecond", UNIFORM, INTEGER);
        DECLARE_PARAM("smooth", UNIFORM, INTEGER);
        DECLARE_PARAM("reentrant", UNIFORM, INTEGER);
        DECLARE_PARAM("motionsegment", UNIFORM, INTEGER);
        DECLARE_PARAM("smoothnormals", UNIFORM, INTEGER);

        //
        // kaze
        //
        DECLARE_PARAM("constantnormal", CONSTANT, NORMAL);
        DECLARE_PARAM("filename", UNIFORM, STRING);
        DECLARE_PARAM("xmlfilename", UNIFORM, STRING);
        DECLARE_PARAM("txtscale", UNIFORM, FLOAT);
        DECLARE_PARAM("direction", UNIFORM, VECTOR);
        DECLARE_PARAM("smoothangle", UNIFORM, FLOAT);
        DECLARE_PARAM("smooth_angle", UNIFORM, FLOAT);
        DECLARE_PARAM("Nf", FACEVARYING, NORMAL);

        //
        // PRMan
        //
        // Camera cameraname
        DECLARE_PARAM("depthoffield", UNIFORM, "float[3]");
        DECLARE_PARAM("extremeoffset", UNIFORM, INTEGER);
        DECLARE_PARAM("focusregion", UNIFORM, FLOAT);
        // DECLARE_PARAM("shutteropening", UNIFORM, "float[2]" );
        // DECLARE_PARAM("shutteropening", UNIFORM, "float[10]" );
        // Display name
        DECLARE_PARAM("merge", UNIFORM, INTEGER);
        DECLARE_PARAM("origin", UNIFORM, "int[2]");
        DECLARE_PARAM("resolution", UNIFORM, STRING);
        DECLARE_PARAM("resolutionunit", UNIFORM, "int[2]");
        // Hider hidden
        // DECLARE_PARAM("jitter", UNIFORM, INTEGER);
        DECLARE_PARAM("aperture", UNIFORM, "float[4]");
        DECLARE_PARAM("dofaspect", UNIFORM, FLOAT);
        DECLARE_PARAM("mpcache", UNIFORM, INTEGER);
        DECLARE_PARAM("mpmemory", UNIFORM, INTEGER);
        DECLARE_PARAM("mpcachedir", UNIFORM, STRING);
        DECLARE_PARAM("samplemotion", UNIFORM, INTEGER);
        DECLARE_PARAM("subpixel", UNIFORM, INTEGER);
        DECLARE_PARAM("extrememotiondof", UNIFORM, INTEGER);
        DECLARE_PARAM("maxvpdepth", UNIFORM, INTEGER);
        DECLARE_PARAM("sigma", UNIFORM, INTEGER);
        DECLARE_PARAM("sigmablur", UNIFORM, FLOAT);
        DECLARE_PARAM("pointfalloffpower", UNIFORM, FLOAT);
        // DECLARE_PARAM("depthfilter", UNIFORM, STRING);
        DECLARE_PARAM("mattefile", UNIFORM, STRING);
        // Hider raytrace
        DECLARE_PARAM("integrationmode", UNIFORM, STRING);
        DECLARE_PARAM("samplemode", UNIFORM, STRING);
        DECLARE_PARAM("minsamples", UNIFORM, INTEGER);
        DECLARE_PARAM("maxsamples", UNIFORM, INTEGER);
        DECLARE_PARAM("incremental", UNIFORM, INTEGER);
        // Hider depthmask
        DECLARE_PARAM("zfile", UNIFORM, STRING);
        DECLARE_PARAM("reversesign", UNIFORM, INTEGER);
        DECLARE_PARAM("depthbias", UNIFORM, FLOAT);
        // Hider null
        // Hider paint
        // Hider photon
        DECLARE_PARAM("emit", UNIFORM, INTEGER);
        // Hider zbuffer
        // Imager name
        // Option bucket
        DECLARE_PARAM("order", UNIFORM, STRING);
        DECLARE_PARAM("orderorigin", UNIFORM, INTEGER);
        // Option curve
        DECLARE_PARAM("orienttotransform", UNIFORM, INTEGER);
        // Option dice
        DECLARE_PARAM("maxhairlength", UNIFORM, INTEGER);
        // Option hair
        DECLARE_PARAM("minwidth", UNIFORM, INTEGER);
        // Option limits
        DECLARE_PARAM("bucketsize", UNIFORM, "int[2]");
        DECLARE_PARAM("brickmemory", UNIFORM, INTEGER);
        DECLARE_PARAM("deepshadowmemory", UNIFORM, INTEGER);
        DECLARE_PARAM("deepshadowtiles", UNIFORM, INTEGER);
        DECLARE_PARAM("geocachememory", UNIFORM, INTEGER);
        DECLARE_PARAM("gridsize", UNIFORM, INTEGER);
        DECLARE_PARAM("gridmemory", UNIFORM, INTEGER);
        DECLARE_PARAM("hemispheresamplememory", UNIFORM, INTEGER);
        DECLARE_PARAM("octreememory", UNIFORM, INTEGER);
        DECLARE_PARAM("pointmemory", UNIFORM, INTEGER);
        DECLARE_PARAM("proceduralmemory", UNIFORM, INTEGER);
        DECLARE_PARAM("radiositycachememory", UNIFORM, INTEGER);
        DECLARE_PARAM("rendermemory", UNIFORM, FLOAT);
        //DECLARE_PARAM("texturememory", UNIFORM, INTEGER);
        DECLARE_PARAM("ptexturememory", UNIFORM, INTEGER);
        DECLARE_PARAM("ptexturemaxfiles", UNIFORM, INTEGER);
        DECLARE_PARAM("threads", UNIFORM, INTEGER);
        DECLARE_PARAM("vpdepthshadingrate", UNIFORM, FLOAT);
        DECLARE_PARAM("vprelativeshadingrate", UNIFORM, FLOAT);
        DECLARE_PARAM("deepshadowerror", UNIFORM, FLOAT);
        DECLARE_PARAM("deepshadowdeptherror", UNIFORM, FLOAT);
        DECLARE_PARAM("deepshadowsimplifyerror", UNIFORM, FLOAT);
        DECLARE_PARAM("vpinteriorheuristic", UNIFORM, INTEGER);
        DECLARE_PARAM("vpvolumeintersections", UNIFORM, INTEGER);
        DECLARE_PARAM("othreshold", UNIFORM, COLOR);
        // DECLARE_PARAM("zthreshold", UNIFORM, COLOR);
        // Option photon
        DECLARE_PARAM("emit", UNIFORM, INTEGER);
        DECLARE_PARAM("lifetime", UNIFORM, STRING);
        // Option polygon
        DECLARE_PARAM("reducedmemory", UNIFORM, INTEGER);
        DECLARE_PARAM("nonplanar", UNIFORM, INTEGER);
        // Option render
        DECLARE_PARAM("rerenderbake", UNIFORM, INTEGER);
        DECLARE_PARAM("rerenderbakedbdir", UNIFORM, STRING);
        DECLARE_PARAM("rerenderbakedbname", UNIFORM, STRING);
        // Option rerender
        // Option Ri
        DECLARE_PARAM("Clipping", UNIFORM, "float[2]");
        DECLARE_PARAM("ClippingPlane", UNIFORM, "float[6]");
        DECLARE_PARAM("CropWindow", UNIFORM, "float[4]");
        DECLARE_PARAM("DepthOfField", UNIFORM, "float[3]");
        DECLARE_PARAM("Exposure", UNIFORM, "float[2]");
        DECLARE_PARAM("FormatResolution", UNIFORM, "int[2]");
        DECLARE_PARAM("FormatPixelAspectRatio", UNIFORM, FLOAT);
        DECLARE_PARAM("FrameAspectRatio", UNIFORM, FLOAT);
        DECLARE_PARAM("PixelFilterName", UNIFORM, STRING);
        DECLARE_PARAM("PixelFilterWidth", UNIFORM, "float[2]");
        DECLARE_PARAM("PixelSamples", UNIFORM, "float[2]");
        DECLARE_PARAM("PixelVariance", UNIFORM, FLOAT);
        DECLARE_PARAM("ProjectionName", UNIFORM, STRING);
        DECLARE_PARAM("ProjectionFOV", UNIFORM, FLOAT);
        DECLARE_PARAM("QuantizeType", UNIFORM, STRING);
        DECLARE_PARAM("QuantizeRange", UNIFORM, "int[3]");
        DECLARE_PARAM("QuantizeDither", UNIFORM, FLOAT);
        DECLARE_PARAM("RelativeDetail", UNIFORM, FLOAT);
        DECLARE_PARAM("ScreenWindow", UNIFORM, "float[4]");
        DECLARE_PARAM("Shutter", UNIFORM, "float[2]");
        DECLARE_PARAM("VolumePixelSamples", UNIFORM, "float[2]");
        // Option rib
        DECLARE_PARAM("format", UNIFORM, STRING);
        DECLARE_PARAM("asciistyle", UNIFORM, STRING);
        DECLARE_PARAM("compression", UNIFORM, STRING);
        DECLARE_PARAM("precision", UNIFORM, INTEGER);
        // Option ribparse
        // Option searchpath
        DECLARE_PARAM("shader", UNIFORM, STRING);
        //DECLARE_PARAM("texture", UNIFORM, STRING);
        //DECLARE_PARAM("display", UNIFORM, STRING);
        //DECLARE_PARAM("archive", UNIFORM, STRING);
        //DECLARE_PARAM("procedural", UNIFORM, STRING);
        DECLARE_PARAM("resource", UNIFORM, STRING);
        DECLARE_PARAM("servershader", UNIFORM, STRING);
        DECLARE_PARAM("servertexture", UNIFORM, STRING);
        DECLARE_PARAM("serverdisplay", UNIFORM, STRING);
        DECLARE_PARAM("serverarchive", UNIFORM, STRING);
        DECLARE_PARAM("serverresource", UNIFORM, STRING);
        DECLARE_PARAM("dirmap", UNIFORM, STRING);
        // Option shading
        DECLARE_PARAM("directlightingsamples", UNIFORM, INTEGER);
        DECLARE_PARAM("debug", UNIFORM, INTEGER);
        DECLARE_PARAM("derivsfollowdicing", UNIFORM, INTEGER);
        DECLARE_PARAM("checknans", UNIFORM, INTEGER);
        DECLARE_PARAM("defcache", UNIFORM, FLOAT);
        DECLARE_PARAM("objectcache", UNIFORM, FLOAT);
        // Option shadow
        // Option shutter
        DECLARE_PARAM("offset", UNIFORM, FLOAT);
        DECLARE_PARAM("clampmotion", UNIFORM, INTEGER);
        // Option statistics
        DECLARE_PARAM("endofframe", UNIFORM, INTEGER);
        DECLARE_PARAM("filename", UNIFORM, STRING);
        DECLARE_PARAM("xmlfilename", UNIFORM, STRING);
        DECLARE_PARAM("stylesheet", UNIFORM, STRING);
        DECLARE_PARAM("shaderprofile", UNIFORM, STRING);
        DECLARE_PARAM("displace_ratios", UNIFORM, "float[2]");
        DECLARE_PARAM("maxdispwarnings", UNIFORM, INTEGER);
        // Option texture
        DECLARE_PARAM("enable_gaussian", UNIFORM, FLOAT);
        DECLARE_PARAM("enable_lerp", UNIFORM, FLOAT);
        DECLARE_PARAM("texturefilter", UNIFORM, STRING);
        // Option trace
        DECLARE_PARAM("maxdepth", UNIFORM, INTEGER);
        DECLARE_PARAM("specularthreshold", UNIFORM, FLOAT);
        DECLARE_PARAM("decimationrate", UNIFORM, INTEGER);
        // Option user
        // PixelFilter filter

        // Attribute cull
        DECLARE_PARAM("backfacing", UNIFORM, INTEGER);
        DECLARE_PARAM("hidden", UNIFORM, INTEGER);
        // Attribute derivatives
        DECLARE_PARAM("centered", UNIFORM, INTEGER);
        DECLARE_PARAM("extrapolate", UNIFORM, INTEGER);
        // Attribute dice
        // DECLARE_PARAM("binary", UNIFORM, INTEGER);
        // DECLARE_PARAM("hair", UNIFORM, INTEGER);
        DECLARE_PARAM("offscreenstrategy", UNIFORM, STRING);
        DECLARE_PARAM("preservecv", UNIFORM, INTEGER);
        DECLARE_PARAM("rasterorient", UNIFORM, INTEGER);
        DECLARE_PARAM("referencecamera", UNIFORM, STRING);
        DECLARE_PARAM("roundcurve", UNIFORM, INTEGER);
        DECLARE_PARAM("strategy", UNIFORM, STRING);
        DECLARE_PARAM("minlength", UNIFORM, FLOAT);
        DECLARE_PARAM("minlengthspace", UNIFORM, STRING);
        // Attribute displacementbound
        DECLARE_PARAM("sphere", UNIFORM, FLOAT);
        DECLARE_PARAM("coordinatesystem", UNIFORM, STRING);
        // Attribute grouping
        // Attribute identifier
        // Attribute irradiance
        // DECLARE_PARAM("maxerror", UNIFORM, FLOAT);
        // DECLARE_PARAM("maxpixeldist", UNIFORM, FLOAT);
        // Attribute photon
        DECLARE_PARAM("estimator", UNIFORM, INTEGER);
        // DECLARE_PARAM("maxdiffusedepth", UNIFORM, INTEGER);
        // DECLARE_PARAM("maxspeculardepth", UNIFORM, INTEGER);
        DECLARE_PARAM("minstoredepth", UNIFORM, INTEGER);
        DECLARE_PARAM("causticmap", UNIFORM, STRING);
        DECLARE_PARAM("globalmap", UNIFORM, STRING);
        DECLARE_PARAM("shadingmodel", UNIFORM, STRING);
        // Attribute procedural
        DECLARE_PARAM("immediatesubivide", UNIFORM, INTEGER);
        DECLARE_PARAM("reentrant", UNIFORM, INTEGER);
        DECLARE_PARAM("unloadable", UNIFORM, INTEGER);
        DECLARE_PARAM("attributes", UNIFORM, STRING);
        // Attribute Ri
        DECLARE_PARAM("Color", UNIFORM, COLOR);
        DECLARE_PARAM("CoordinateSystem", UNIFORM, STRING);
        DECLARE_PARAM("Detail", UNIFORM, "float[6]");
        DECLARE_PARAM("DetailRange", UNIFORM, "float[4]");
        DECLARE_PARAM("GeometricApproximationFlatness", UNIFORM, FLOAT);
        DECLARE_PARAM("GeometricApproximationMotionFactor", UNIFORM, FLOAT);
        DECLARE_PARAM("GeometricApproximationFocusFactor", UNIFORM, FLOAT);
        DECLARE_PARAM("IlluminateHandle", UNIFORM, STRING);
        DECLARE_PARAM("IlluminateValue", UNIFORM, INTEGER);
        DECLARE_PARAM("Matte", UNIFORM, INTEGER);
        DECLARE_PARAM("Opacity", UNIFORM, COLOR);
        DECLARE_PARAM("Orientation", UNIFORM, STRING);
        DECLARE_PARAM("ReverseOrientation", UNIFORM, INTEGER);
        DECLARE_PARAM("ScopedCoordinateSystem", UNIFORM, STRING);
        DECLARE_PARAM("ShadingInterpolation", UNIFORM, STRING);
        DECLARE_PARAM("ShadingRate", UNIFORM, FLOAT);
        DECLARE_PARAM("Sides", UNIFORM, INTEGER);
        DECLARE_PARAM("TextureCoordinates", UNIFORM, "float[8]");
        // Attribute sides
        DECLARE_PARAM("backfacetolerance", UNIFORM, FLOAT);
        DECLARE_PARAM("doubleshaded", UNIFORM, INTEGER);
        // Attribute shade
        DECLARE_PARAM("shadingrate", UNIFORM, FLOAT);
        DECLARE_PARAM("strategy", UNIFORM, STRING);
        DECLARE_PARAM("volumeintersectionstrategy", UNIFORM, STRING);
        DECLARE_PARAM("volumeintersectionpriority", UNIFORM, FLOAT);
        DECLARE_PARAM("diffusehitmode", UNIFORM, STRING);
        DECLARE_PARAM("diffusehitcache", UNIFORM, STRING);
        DECLARE_PARAM("diffusehitcolorchannel", UNIFORM, STRING);
        DECLARE_PARAM("diffusehitopacitychannel", UNIFORM, STRING);
        DECLARE_PARAM("specularhitmode", UNIFORM, STRING);
        // DECLARE_PARAM("transmissionhitmode", UNIFORM, STRING);
        DECLARE_PARAM("transmissionhitcache", UNIFORM, STRING);
        DECLARE_PARAM("camerahitmode", UNIFORM, STRING);
        DECLARE_PARAM("relativeshadingrate", UNIFORM, FLOAT);
        DECLARE_PARAM("resetrelativeshadingrate", UNIFORM, FLOAT);
        DECLARE_PARAM("frequency", UNIFORM, STRING);
        // Attribute shadegroups
        DECLARE_PARAM("attributecombining", UNIFORM, STRING);
        DECLARE_PARAM("objectspacecombining", UNIFORM, STRING);
        // Attribute stitch
        DECLARE_PARAM("enable", UNIFORM, INTEGER);
        DECLARE_PARAM("traceenable", UNIFORM, INTEGER);
        DECLARE_PARAM("newgroup", UNIFORM, INTEGER);
        // Attribute stochastic
        DECLARE_PARAM("sigma", UNIFORM, INTEGER);
        DECLARE_PARAM("pointfalloff", UNIFORM, INTEGER);
        // Attribute trace
        // DECLARE_PARAM("bias", UNIFORM, FLOAT);
        // DECLARE_PARAM("decimationrate", UNIFORM, FLOAT);
        // DECLARE_PARAM("displacements", UNIFORM, INTEGER);
        DECLARE_PARAM("emissionbias", UNIFORM, FLOAT);
        DECLARE_PARAM("importancethreshold", UNIFORM, FLOAT);
        // DECLARE_PARAM("maxdiffusedepth", UNIFORM, INTEGER);
        // DECLARE_PARAM("maxspeculardepth", UNIFORM, INTEGER);
        DECLARE_PARAM("samplemotion", UNIFORM, INTEGER);
        // Attribute trimcurve
        // Attribute user
        // Attribute visibility
        DECLARE_PARAM("camera", UNIFORM, INTEGER);
        DECLARE_PARAM("diffuse", UNIFORM, INTEGER);
        DECLARE_PARAM("specular", UNIFORM, INTEGER);
        DECLARE_PARAM("transmission", UNIFORM, INTEGER);
        DECLARE_PARAM("photon", UNIFORM, INTEGER);
        DECLARE_PARAM("midpoint", UNIFORM, INTEGER);
        // Attribute volume
        DECLARE_PARAM("depthrelativeshadingrate", UNIFORM, FLOAT);
        DECLARE_PARAM("depthinterpolation", UNIFORM, STRING);
        DECLARE_PARAM("deptherror", UNIFORM, FLOAT);
        // DECLARE_PARAM("refinementstrategies", UNIFORM, STRING );
        // GeometricApproximation
        DECLARE_PARAM("motionfactor", UNIFORM, FLOAT);
        DECLARE_PARAM("focusfactor", UNIFORM, FLOAT);

        //
        // PR18
        //
        DECLARE_PARAM("__dsoname", UNIFORM, STRING);
        DECLARE_PARAM("__bound", UNIFORM, BOUND);
        DECLARE_PARAM("__bound0", UNIFORM, BOUND);
        DECLARE_PARAM("__bound1", UNIFORM, BOUND);
        DECLARE_PARAM("__immediatesubdivide", UNIFORM, INTEGER);
        DECLARE_PARAM("__savedattributes", UNIFORM, STRING);

        //
        // XRT
        //
        DECLARE_PARAM("__threshold", CONSTANT, FLOAT);
        DECLARE_PARAM("casts_shadows", UNIFORM, STRING);
        DECLARE_PARAM("steps", UNIFORM, INTEGER);
        DECLARE_PARAM("has_caustics", UNIFORM, INTEGER);
        DECLARE_PARAM("renderType", UNIFORM, STRING);
        // DECLARE_PARAM("background", UNIFORM, COLOR);

        //
        // Siren
        //
        DECLARE_PARAM("pathsamples", UNIFORM, INTEGER);
        DECLARE_PARAM("objectsamples", UNIFORM, INTEGER);

        // TEST
        DECLARE_PARAM("yscale", UNIFORM, FLOAT);
        DECLARE_PARAM("func", UNIFORM, FLOAT);
        //
        DECLARE_PARAM("shaders", UNIFORM, STRING);
        DECLARE_PARAM("textures", UNIFORM, STRING);
        DECLARE_PARAM("archives", UNIFORM, STRING);
    }

#undef DECLARE_PARAM

    ri_temporary_classifier::ri_temporary_classifier(const ri_classifier* parent)
        : ri_classifier(0) // noinit
    {
        parent_ = parent;
    }
    const param_data& ri_temporary_classifier::operator[](size_t id) const
    {
        if (id < parent_->size())
        {
            return (*parent_)[id];
        }
        else
        {
            return ri_classifier::operator[](id - parent_->size());
        }
    }

    int ri_temporary_classifier::find(const char* name) const
    {
        int id = ri_classifier::find(name);
        if (id >= 0)
            return id + (int)parent_->size();
        id = parent_->find(name);
        if (id >= 0)
            return id;
        return -1;
    }

    int ri_temporary_classifier::def(const char* name)
    {
        int id = parent_->find(name);
        if (id >= 0)
            return id;
        id = ri_classifier::def(name);
        if (id >= 0)
            return id + (int)parent_->size();
        return -1;
    }

    ri_saving_classifier::ri_saving_classifier(const ri_classifier* parent,
                                               ri_classifier* store)
        : ri_temporary_classifier(parent) // noinit
    {
        store_ = store;
    }
    const param_data& ri_saving_classifier::operator[](size_t id) const
    {
        return ri_temporary_classifier::operator[](id);
    }

    int ri_saving_classifier::find(const char* name) const
    {
        return ri_temporary_classifier::find(name);
    }

    int ri_saving_classifier::def(const char* name)
    {
        int id = ri_temporary_classifier::def(name);
        if (id >= 0)
        {
            store_->add(ri_temporary_classifier::operator[](id));
        }
        return id;
    }
}
