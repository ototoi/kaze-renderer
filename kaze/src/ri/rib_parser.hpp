/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton interface for Bison LALR(1) parsers in C++

   Copyright (C) 2002, 2003, 2004, 2005, 2006 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C++ LALR(1) parser skeleton written by Akim Demaille.  */

#ifndef PARSER_HEADER_H
# define PARSER_HEADER_H

#include <string>
#include <iostream>
#include "stack.hh"

namespace yy
{
  class position;
  class location;
}

/* First part of user declarations.  */
#line 5 "rib_parser.yy"

#ifdef _WIN32
#pragma warning(disable : 4786)
#pragma warning(disable : 4102)
#pragma warning(disable : 4996)
#pragma warning(disable : 4065)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define YYDEBUG 1
#define YYERROR_VERBOSE 1

#include <algorithm>
#include <iterator>
#include "ri.h"
#include "rib_array.h"
#include "ri_context.h"
#include "ri_rib_decoder.h"
#include "ri_rib_parse_param.h"





/* Line 303 of lalr1.cc.  */
#line 80 "rib_parser.hpp"

#include "location.hh"

/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 1
#endif

/* Enabling the token table.  */
#ifndef YYTOKEN_TABLE
# define YYTOKEN_TABLE 0
#endif

/* YYLLOC_DEFAULT -- Set CURRENT to span from RHS[1] to RHS[N].
   If N is 0, then set CURRENT to the empty location which ends
   the previous symbol: RHS[0] (always defined).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)		\
do {							\
  if (N)						\
    {							\
      (Current).begin = (Rhs)[1].begin;			\
      (Current).end   = (Rhs)[N].end;			\
    }							\
  else							\
    {							\
      (Current).begin = (Current).end = (Rhs)[0].end;	\
    }							\
} while (false)
#endif

namespace yy
{

  /// A Bison parser.
  class rib_parser
  {
  public:
    /// Symbol semantic values.
#ifndef YYSTYPE
    union semantic_type
#line 31 "rib_parser.yy"
{
	int   itype;
	float ftype;
	char  ctype;
	char* stype;
	struct integer_array* aitype;
	struct float_array*   aftype;
	struct string_array*  astype;
	struct token_value*   tvtype;
	struct token_value_array* atvtype;
}
/* Line 303 of lalr1.cc.  */
#line 144 "rib_parser.hpp"
	;
#else
    typedef YYSTYPE semantic_type;
#endif
    /// Symbol locations.
    typedef location location_type;
    /// Tokens.
    struct token
    {
      /* Tokens.  */
   enum yytokentype {
     EOF_TOKEN = 258,
     UNKNOWN_TOKEN = 259,
     INVALID_VALUE = 260,
     FLOAT_TOKEN = 261,
     INTEGER_TOKEN = 262,
     STRING_TOKEN = 263,
     COMMENT = 264,
     TOKEN_ARCHIVEBEGIN = 265,
     TOKEN_ARCHIVEEND = 266,
     TOKEN_AREALIGHTSOURCE = 267,
     TOKEN_ATMOSPHERE = 268,
     TOKEN_ATTRIBUTE = 269,
     TOKEN_ATTRIBUTEBEGIN = 270,
     TOKEN_ATTRIBUTEEND = 271,
     TOKEN_BASIS = 272,
     TOKEN_BLOBBY = 273,
     TOKEN_BOUND = 274,
     TOKEN_CLIPPING = 275,
     TOKEN_COLOR = 276,
     TOKEN_COLORSAMPLES = 277,
     TOKEN_CONCATTRANSFORM = 278,
     TOKEN_CONE = 279,
     TOKEN_COORDINATESYSTEM = 280,
     TOKEN_COORDSYSTRANSFORM = 281,
     TOKEN_CROPWINDOW = 282,
     TOKEN_CURVES = 283,
     TOKEN_CYLINDER = 284,
     TOKEN_DECLARE = 285,
     TOKEN_DEFORMATION = 286,
     TOKEN_DEPTHOFFIELD = 287,
     TOKEN_DETAIL = 288,
     TOKEN_DETAILRANGE = 289,
     TOKEN_DISK = 290,
     TOKEN_DISPLACEMENT = 291,
     TOKEN_DISPLAYCHANNEL = 292,
     TOKEN_DISPLAY = 293,
     TOKEN_ELSE = 294,
     TOKEN_ELSEIF = 295,
     TOKEN_ERRORABORT = 296,
     TOKEN_ERRORHANDLER = 297,
     TOKEN_ERRORIGNORE = 298,
     TOKEN_ERRORPRINT = 299,
     TOKEN_EXPOSURE = 300,
     TOKEN_EXTERIOR = 301,
     TOKEN_FORMAT = 302,
     TOKEN_FRAMEASPECTRATIO = 303,
     TOKEN_FRAMEBEGIN = 304,
     TOKEN_FRAMEEND = 305,
     TOKEN_GENERALPOLYGON = 306,
     TOKEN_GEOMETRICAPPROXIMATION = 307,
     TOKEN_GEOMETRY = 308,
     TOKEN_HIERARCHICALSUBDIVISIONMESH = 309,
     TOKEN_HIDER = 310,
     TOKEN_HYPERBOLOID = 311,
     TOKEN_IDENTITY = 312,
     TOKEN_IFBEGIN = 313,
     TOKEN_IFEND = 314,
     TOKEN_ILLUMINATE = 315,
     TOKEN_IMAGER = 316,
     TOKEN_INTERIOR = 317,
     TOKEN_LIGHTSOURCE = 318,
     TOKEN_MAKEOCCLUSION = 319,
     TOKEN_MAKEBRICKMAP = 320,
     TOKEN_MAKEBUMP = 321,
     TOKEN_MAKECUBEFACEENVIRONMENT = 322,
     TOKEN_MAKELATLONGENVIRONMENT = 323,
     TOKEN_MAKESHADOW = 324,
     TOKEN_MAKETEXTURE = 325,
     TOKEN_MATTE = 326,
     TOKEN_MOTIONBEGIN = 327,
     TOKEN_MOTIONEND = 328,
     TOKEN_NUPATCH = 329,
     TOKEN_OBJECTBEGIN = 330,
     TOKEN_OBJECTEND = 331,
     TOKEN_OBJECTINSTANCE = 332,
     TOKEN_OPACITY = 333,
     TOKEN_OPTION = 334,
     TOKEN_ORIENTATION = 335,
     TOKEN_PARABOLOID = 336,
     TOKEN_PATCH = 337,
     TOKEN_PATCHMESH = 338,
     TOKEN_PERSPECTIVE = 339,
     TOKEN_PIXELFILTER = 340,
     TOKEN_PIXELSAMPLES = 341,
     TOKEN_PIXELVARIANCE = 342,
     TOKEN_POINTS = 343,
     TOKEN_POINTSGENERALPOLYGONS = 344,
     TOKEN_POINTSPOLYGONS = 345,
     TOKEN_POLYGON = 346,
     TOKEN_PROCEDURAL = 347,
     TOKEN_PROCEDURE = 348,
     TOKEN_PROJECTION = 349,
     TOKEN_QUANTIZE = 350,
     TOKEN_READARCHIVE = 351,
     TOKEN_RELATIVEDETAIL = 352,
     TOKEN_RESOURCE = 353,
     TOKEN_RESOURCEBEGIN = 354,
     TOKEN_RESOURCEEND = 355,
     TOKEN_REVERSEORIENTATION = 356,
     TOKEN_ROTATE = 357,
     TOKEN_SCALE = 358,
     TOKEN_SCOPEDCOORDINATESYSTEM = 359,
     TOKEN_SCREENWINDOW = 360,
     TOKEN_SHADINGINTERPOLATION = 361,
     TOKEN_SHADINGRATE = 362,
     TOKEN_SHUTTER = 363,
     TOKEN_SIDES = 364,
     TOKEN_SKEW = 365,
     TOKEN_SOLIDBEGIN = 366,
     TOKEN_SOLIDEND = 367,
     TOKEN_SPHERE = 368,
     TOKEN_SUBDIVISIONMESH = 369,
     TOKEN_SURFACE = 370,
     TOKEN_TEXTURECOORDINATES = 371,
     TOKEN_TORUS = 372,
     TOKEN_TRANSFORM = 373,
     TOKEN_TRANSFORMBEGIN = 374,
     TOKEN_TRANSFORMEND = 375,
     TOKEN_TRANSFORMPOINTS = 376,
     TOKEN_TRANSLATE = 377,
     TOKEN_TRIMCURVE = 378,
     TOKEN_VERSION = 379,
     TOKEN_WORLDBEGIN = 380,
     TOKEN_WORLDEND = 381,
     TOKEN_SHADERLAYER = 382,
     TOKEN_CONNECTSHADERLAYERS = 383,
     TOKEN_PROCEDURAL2 = 384,
     TOKEN_SYSTEM = 385,
     TOKEN_VOLUME = 386,
     TOKEN_VPATMOSPHERE = 387,
     TOKEN_VPINTERIOR = 388,
     TOKEN_VPSURFACE = 389,
     TOKEN_INTEGRATOR = 390,
     TOKEN_BXDF = 391,
     TOKEN_PATTERN = 392
   };

    };
    /// Token type.
    typedef token::yytokentype token_type;

    /// Build a parser object.
    rib_parser (void * scanner_yyarg);
    virtual ~rib_parser ();

    /// Parse.
    /// \returns  0 iff parsing succeeded.
    virtual int parse ();

    /// The current debugging stream.
    std::ostream& debug_stream () const;
    /// Set the current debugging stream.
    void set_debug_stream (std::ostream &);

    /// Type for debugging levels.
    typedef int debug_level_type;
    /// The current debugging level.
    debug_level_type debug_level () const;
    /// Set the current debugging level.
    void set_debug_level (debug_level_type l);

  private:
    /// Report a syntax error.
    /// \param loc    where the syntax error is found.
    /// \param msg    a description of the syntax error.
    virtual void error (const location_type& loc, const std::string& msg);

    /// Generate an error message.
    /// \param state   the state where the error occurred.
    /// \param tok     the look-ahead token.
    virtual std::string yysyntax_error_ (int yystate, int tok);

#if YYDEBUG
    /// \brief Report a symbol value on the debug stream.
    /// \param yytype       The token type.
    /// \param yyvaluep     Its semantic value.
    /// \param yylocationp  Its location.
    virtual void yy_symbol_value_print_ (int yytype,
					 const semantic_type* yyvaluep,
					 const location_type* yylocationp);
    /// \brief Report a symbol on the debug stream.
    /// \param yytype       The token type.
    /// \param yyvaluep     Its semantic value.
    /// \param yylocationp  Its location.
    virtual void yy_symbol_print_ (int yytype,
				   const semantic_type* yyvaluep,
				   const location_type* yylocationp);
#endif /* ! YYDEBUG */


    /// State numbers.
    typedef int state_type;
    /// State stack type.
    typedef stack<state_type>    state_stack_type;
    /// Semantic value stack type.
    typedef stack<semantic_type> semantic_stack_type;
    /// location stack type.
    typedef stack<location_type> location_stack_type;

    /// The state stack.
    state_stack_type yystate_stack_;
    /// The semantic value stack.
    semantic_stack_type yysemantic_stack_;
    /// The location stack.
    location_stack_type yylocation_stack_;

    /// Internal symbol numbers.
    typedef unsigned char token_number_type;
    /* Tables.  */
    /// For a state, the index in \a yytable_ of its portion.
    static const short int yypact_[];
    static const short int yypact_ninf_;

    /// For a state, default rule to reduce.
    /// Unless\a  yytable_ specifies something else to do.
    /// Zero means the default is an error.
    static const unsigned short int yydefact_[];

    static const short int yypgoto_[];
    static const short int yydefgoto_[];

    /// What to do in a state.
    /// \a yytable_[yypact_[s]]: what to do in state \a s.
    /// - if positive, shift that token.
    /// - if negative, reduce the rule which number is the opposite.
    /// - if zero, do what YYDEFACT says.
    static const short int yytable_[];
    static const short int yytable_ninf_;

    static const short int yycheck_[];

    /// For a state, its accessing symbol.
    static const unsigned short int yystos_[];

    /// For a rule, its LHS.
    static const unsigned short int yyr1_[];
    /// For a rule, its RHS length.
    static const unsigned char yyr2_[];

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
    /// For a symbol, its name in clear.
    static const char* const yytname_[];
#endif

#if YYERROR_VERBOSE
    /// Convert the symbol name \a n to a form suitable for a diagnostic.
    virtual std::string yytnamerr_ (const char *n);
#endif

#if YYDEBUG
    /// A type to store symbol numbers and -1.
    typedef short int rhs_number_type;
    /// A `-1'-separated list of the rules' RHS.
    static const rhs_number_type yyrhs_[];
    /// For each rule, the index of the first RHS symbol in \a yyrhs_.
    static const unsigned short int yyprhs_[];
    /// For each rule, its source line number.
    static const unsigned short int yyrline_[];
    /// For each scanner token number, its symbol number.
    static const unsigned short int yytoken_number_[];
    /// Report on the debug stream that the rule \a r is going to be reduced.
    virtual void yy_reduce_print_ (int r);
    /// Print the state stack on the debug stream.
    virtual void yystack_print_ ();
#endif

    /// Convert a scanner token number \a t to a symbol number.
    token_number_type yytranslate_ (int t);

    /// \brief Reclaim the memory associated to a symbol.
    /// \param yymsg        Why this token is reclaimed.
    /// \param yytype       The symbol type.
    /// \param yyvaluep     Its semantic value.
    /// \param yylocationp  Its location.
    inline void yydestruct_ (const char* yymsg,
			     int yytype,
			     semantic_type* yyvaluep,
			     location_type* yylocationp);

    /// Pop \a n symbols the three stacks.
    inline void yypop_ (unsigned int n = 1);

    /* Constants.  */
    static const int yyeof_;
    /* LAST_ -- Last index in TABLE_.  */
    static const int yylast_;
    static const int yynnts_;
    static const int yyempty_;
    static const int yyfinal_;
    static const int yyterror_;
    static const int yyerrcode_;
    static const int yyntokens_;
    static const unsigned int yyuser_token_number_max_;
    static const token_number_type yyundef_token_;

    /* Debugging.  */
    int yydebug_;
    std::ostream* yycdebug_;


    /* User arguments.  */
    void * scanner;
  };
}


#endif /* ! defined PARSER_HEADER_H */
