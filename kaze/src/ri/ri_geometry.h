#ifndef RI_GEOMETRY_H
#define RI_GEOMETRY_H

#include <string>
#include "ri.h"
#include "ri_object.h"
#include "ri_options.h"
#include "ri_attributes.h"
#include "ri_transform.h"
#include "ri_parameters.h"
#include "ri_classifier.h"

namespace ri
{
    enum
    {
        RI_GEOMETRY_UNKNOWN = 1000,
        RI_GEOMETRY_POLYGON,
        RI_GEOMETRY_GENERAL_POLYGON,
        RI_GEOMETRY_POINTS_POLYGONS,
        RI_GEOMETRY_POINTS_GENERAL_POLYGONS,
        RI_GEOMETRY_PATCH,
        RI_GEOMETRY_PATCH_MESH,
        RI_GEOMETRY_NU_PATCH,
        RI_GEOMETRY_TRIM_CURVE,
        RI_GEOMETRY_SPHERE,
        RI_GEOMETRY_CONE,
        RI_GEOMETRY_CYLINDER,
        RI_GEOMETRY_HYPERBOLOID,
        RI_GEOMETRY_PARABOLOID,
        RI_GEOMETRY_DISK,
        RI_GEOMETRY_TORUS,
        RI_GEOMETRY_BLOBBY,
        RI_GEOMETRY_CURVES,
        RI_GEOMETRY_POINTS,
        RI_GEOMETRY_SUBDIVISION_MESH,
        RI_GEOMETRY_PROCEDURAL,
        RI_GEOMETRY_GROUP,
        RI_GEOMETRY_SOLID,
        RI_GEOMETRY_OBJECT_INSTANCE,
        RI_GEOMETRY_BUNNY,
        RI_GEOMETRY_TEAPOT,
        RI_GEOMETRY_MOTION,
        /***********************************/
        RI_GEOMETRY_BEZIER_PATCH_MESH,
        RI_GEOMETRY_POINTS_TRIANGLE_POLYGONS,
        RI_GEOMETRY_RROCEDURAL_EVALUATION,
        RI_GEOMETRY_BEZIER_CURVES,
        RI_GEOMETRY_FILE_MESH,
        RI_GEOMETRY_BLOBBY_MESH,
        RI_GEOMETRY_STRUCTURE_SYNTH
    };

    class ri_geometry : public ri_object
    {
    public:
        ri_geometry() : clsf_(0) {}
        ri_geometry(const ri_attributes& attr, const ri_transform& trns,
                    const ri_classifier& clsf)
            : attr_(attr), trns_(trns), clsf_(clsf) {}
        virtual ~ri_geometry() {}
        virtual std::string type() const = 0;
        virtual int typeN() const { return RI_GEOMETRY_UNKNOWN; }
        virtual void set_attributes(const ri_attributes& attr) { attr_ = attr; }
        virtual void set_transform(const ri_transform& trns) { trns_ = trns; }
        virtual const ri_attributes& get_attributes() const { return attr_; }
        virtual const ri_transform& get_transform() const { return trns_; }
        virtual const ri_classifier& get_classifier() const { return clsf_; }
        virtual const ri_parameters& get_parameters() const { return params_; }

    public:
        virtual const ri_parameters::value_type*
        get_parameter(const char* key) const
        {
            return params_.get(key);
        }

    protected:
        ri_attributes attr_;
        ri_transform trns_;
        ri_classifier clsf_;
        ri_parameters params_;
    };

    /***********************
 * Polygon
 ***********************/

    class ri_base_polygons_geometry : public ri_geometry
    {
    public:
        virtual const float* get_P() const;
        virtual const float* get_N() const;
        virtual const float* get_Np() const;
        virtual const float* get_Cs() const;
        virtual const float* get_Os() const;
    };

    class ri_polygon_geometry : public ri_base_polygons_geometry
    {
    public:
        ri_polygon_geometry(const ri_classifier* p_cls, RtInt nverts, RtInt n,
                            RtToken tokens[], RtPointer params[]);
        virtual std::string type() const { return "Polygon"; }
        virtual int typeN() const { return RI_GEOMETRY_POLYGON; }

    public:
        const float* get_P() const;
        const float* get_Cs() const;
        const float* get_Os() const;
        int get_nverts() const;
    };

    class ri_general_polygon_geometry : public ri_base_polygons_geometry
    {
    public:
        ri_general_polygon_geometry(const ri_classifier* p_cls, RtInt nloops,
                                    RtInt nverts[], RtInt n, RtToken tokens[],
                                    RtPointer params[]);
        virtual std::string type() const { return "GeneralPolygon"; }
        virtual int typeN() const { return RI_GEOMETRY_GENERAL_POLYGON; }

    public:
        int get_nloops() const;
        const int* get_nverts() const;
        const float* get_P() const;
        const float* get_Cs() const;
        const float* get_Os() const;
    };

    class ri_points_polygons_geometry : public ri_base_polygons_geometry
    {
    public:
        ri_points_polygons_geometry(const ri_classifier* p_cls, RtInt npolys,
                                    RtInt nverts[], RtInt verts[], RtInt n,
                                    RtToken tokens[], RtPointer params[]);
        ri_points_polygons_geometry(const ri_classifier* p_cls, RtInt nverts, RtInt n,
                                    RtToken tokens[],
                                    RtPointer params[]); // for one Polygon;

        virtual std::string type() const { return "PointsPolygons"; }
        virtual int typeN() const { return RI_GEOMETRY_POINTS_POLYGONS; }
    public:
        int get_npolys() const;
        const int* get_nverts() const;
        const int* get_verts() const;
        const float* get_P() const;
        const float* get_Cs() const;
        const float* get_Os() const;
    public:
        void make_normals();
    };

    class ri_points_general_polygons_geometry : public ri_base_polygons_geometry
    {
    public:
        ri_points_general_polygons_geometry(const ri_classifier* p_cls, RtInt npolys,
                                            RtInt nloops[], RtInt nverts[],
                                            RtInt verts[], RtInt n, RtToken tokens[],
                                            RtPointer params[]);
        virtual std::string type() const { return "PointsGeneralPolygons"; }
        virtual int typeN() const { return RI_GEOMETRY_POINTS_GENERAL_POLYGONS; }

    public:
        int get_npolys() const;
        const int* get_nloops() const;
        const int* get_nverts() const;
        const int* get_verts() const;
        const float* get_P() const;
        const float* get_Cs() const;
        const float* get_Os() const;
    };

    /***********************
 * Patch
 ***********************/
    class ri_base_patch_geometry : public ri_geometry
    {
    public:
        ri_base_patch_geometry() {}
        ri_base_patch_geometry(const ri_attributes& attr, const ri_transform& trns,
                               const ri_classifier& clsf)
            : ri_geometry(attr, trns, clsf) {}
        virtual const float* get_P() const = 0;
        virtual const float* get_Pw() const = 0;
        virtual bool is_rational() const = 0;
    };

    class ri_patch_geometry : public ri_base_patch_geometry
    {
    public:
        ri_patch_geometry(const ri_classifier* p_cls, RtToken type, RtInt n,
                          RtToken tokens[], RtPointer params[]);
        virtual std::string type() const { return "Patch"; }
        virtual int typeN() const { return RI_GEOMETRY_PATCH; }

    public:
        const char* get_type() const;
        const float* get_P() const;
        const float* get_Cs() const;
        const float* get_Os() const;

    public:
        const float* get_Pw() const;

    public:
        std::string get_ubasis() const;
        std::string get_vbasis() const;

    public:
        bool is_linear() const;
        bool is_cubic() const;
        bool is_rational() const;
    };

    class ri_patch_mesh_geometry : public ri_base_patch_geometry
    {
    public:
        ri_patch_mesh_geometry(const ri_classifier* p_cls, RtInt ustep, RtInt vstep,
                               RtToken type, RtInt nu, RtToken uwrap, RtInt nv,
                               RtToken vwrap, RtInt n, RtToken tokens[],
                               RtPointer params[]);
        virtual std::string type() const { return "PatchMesh"; }
        virtual int typeN() const { return RI_GEOMETRY_PATCH_MESH; }

    public:
        const char* get_type() const;
        int get_nu() const;
        int get_nv() const;
        const char* get_uwrap() const;
        const char* get_vwrap() const;

    public:
        const float* get_P() const;
        const float* get_Cs() const;
        const float* get_Os() const;

    public:
        const float* get_Pw() const;

    public:
        std::string get_ubasis() const;
        std::string get_vbasis() const;
        int get_ustep() const;
        int get_vstep() const;

    public:
        bool is_linear() const;
        bool is_cubic() const;
        bool is_rational() const;
    };

    class ri_nu_patch_geometry : public ri_base_patch_geometry
    {
    public:
        ri_nu_patch_geometry(const ri_classifier* p_cls, RtInt nu, RtInt uorder,
                             RtFloat uknot[], RtFloat umin, RtFloat umax, RtInt nv,
                             RtInt vorder, RtFloat vknot[], RtFloat vmin,
                             RtFloat vmax, RtInt n, RtToken tokens[],
                             RtPointer params[]);
        virtual std::string type() const { return "NuPatch"; }
        virtual int typeN() const { return RI_GEOMETRY_NU_PATCH; }

    public:
        int get_nu() const;
        int get_nv() const;
        int get_uorder() const;
        int get_vorder() const;
        const std::vector<float>& get_uknot() const;
        const std::vector<float>& get_vknot() const;
        float get_umin() const;
        float get_umax() const;
        float get_vmin() const;
        float get_vmax() const;

    public:
        const float* get_Pw() const;
        const float* get_Cs() const;
        const float* get_Os() const;

    public:
        const float* get_P() const;

    public:
        bool is_rational() const;
        bool is_noweight_cp() const;
    };

    class ri_trim_curve_geometry : public ri_geometry
    {
    public:
        ri_trim_curve_geometry(const ri_classifier* p_cls, RtInt nloops,
                               RtInt ncurves[], RtInt order[], RtFloat knot[],
                               RtFloat min[], RtFloat max[], RtInt n[], RtFloat u[],
                               RtFloat v[], RtFloat w[]);
        virtual std::string type() const { return "TrimCurve"; }
        virtual int typeN() const { return RI_GEOMETRY_TRIM_CURVE; }

    public:
        int get_nloops() const;
        const int* get_ncurves() const;
        const int* get_order() const;
        const float* get_knot() const;
        const float* get_min() const;
        const float* get_max() const;
        const int* get_n() const;
        const float* get_u() const;
        const float* get_v() const;
        const float* get_w() const;

    public:
        bool is_outer() const;
        bool is_inner() const;
        bool is_rational() const;
        bool is_noweight_cp() const;
    };

    /***********************
 * Primitive
 ***********************/
    class ri_sphere_geometry : public ri_geometry
    {
    public:
        ri_sphere_geometry(const ri_classifier* p_cls, RtFloat radius, RtFloat zmin,
                           RtFloat zmax, RtFloat tmax, RtInt n, RtToken tokens[],
                           RtPointer params[]);
        virtual std::string type() const { return "Sphere"; }
        virtual int typeN() const { return RI_GEOMETRY_SPHERE; }

    public:
        float get_radius() const;
        float get_zmin() const;
        float get_zmax() const;
        float get_tmax() const;
    };

    class ri_cone_geometry : public ri_geometry
    {
    public:
        ri_cone_geometry(const ri_classifier* p_cls, RtFloat height, RtFloat radius,
                         RtFloat tmax, RtInt n, RtToken tokens[], RtPointer params[]);
        virtual std::string type() const { return "Cone"; }
        virtual int typeN() const { return RI_GEOMETRY_CONE; }

    public:
        float get_height() const;
        float get_radius() const;
        float get_tmax() const;
    };

    class ri_cylinder_geometry : public ri_geometry
    {
    public:
        ri_cylinder_geometry(const ri_classifier* p_cls, RtFloat radius, RtFloat zmin,
                             RtFloat zmax, RtFloat tmax, RtInt n, RtToken tokens[],
                             RtPointer params[]);
        virtual std::string type() const { return "Cylinder"; }
        virtual int typeN() const { return RI_GEOMETRY_CYLINDER; }

    public:
        float get_radius() const;
        float get_zmin() const;
        float get_zmax() const;
        float get_tmax() const;
    };

    class ri_hyperboloid_geometry : public ri_geometry
    {
    public:
        ri_hyperboloid_geometry(const ri_classifier* p_cls, RtPoint point1,
                                RtPoint point2, RtFloat tmax, RtInt n,
                                RtToken tokens[], RtPointer params[]);
        virtual std::string type() const { return "Hyperboloid"; }
        virtual int typeN() const { return RI_GEOMETRY_HYPERBOLOID; }

    public:
        const float* get_point1() const;
        const float* get_point2() const;
        float get_tmax() const;
    };

    class ri_paraboloid_geometry : public ri_geometry
    {
    public:
        ri_paraboloid_geometry(const ri_classifier* p_cls, RtFloat rmax, RtFloat zmin,
                               RtFloat zmax, RtFloat tmax, RtInt n, RtToken tokens[],
                               RtPointer params[]);
        virtual std::string type() const { return "Paraboloid"; }
        virtual int typeN() const { return RI_GEOMETRY_PARABOLOID; }

    public:
        float get_rmax() const;
        float get_zmin() const;
        float get_zmax() const;
        float get_tmax() const;
    };

    class ri_disk_geometry : public ri_geometry
    {
    public:
        ri_disk_geometry(const ri_classifier* p_cls, RtFloat height, RtFloat radius,
                         RtFloat tmax, RtInt n, RtToken tokens[], RtPointer params[]);
        virtual std::string type() const { return "Disk"; }
        virtual int typeN() const { return RI_GEOMETRY_DISK; }

    public:
        float get_height() const;
        float get_radius() const;
        float get_tmax() const;
    };

    class ri_torus_geometry : public ri_geometry
    {
    public:
        ri_torus_geometry(const ri_classifier* p_cls, RtFloat majrad, RtFloat minrad,
                          RtFloat phimin, RtFloat phimax, RtFloat tmax, RtInt n,
                          RtToken tokens[], RtPointer params[]);
        virtual std::string type() const { return "Torus"; }
        virtual int typeN() const { return RI_GEOMETRY_TORUS; }

    public:
        float get_majrad() const;
        float get_minrad() const;
        float get_phimin() const;
        float get_phimax() const;
        float get_tmax() const;
    };

    class ri_blobby_geometry : public ri_geometry
    {
    public:
        ri_blobby_geometry(const ri_classifier* p_cls, RtInt nleaf, RtInt ncode,
                           RtInt code[], RtInt nflt, RtFloat flt[], RtInt nstr,
                           RtToken str[], RtInt n, RtToken tokens[],
                           RtPointer params[]);
        virtual std::string type() const { return "Blobby"; }
        virtual int typeN() const { return RI_GEOMETRY_BLOBBY; }

    public:
        int get_nleaf() const;
        int get_ncode() const;
        const int* get_code() const;
        int get_nflt() const;
        const float* get_flt() const;
        int get_nstr() const;
        const std::string* get_str() const;
        float get_threthold() const;
    };

    /***********************
 * Simple
 ***********************/
    class ri_curves_geometry : public ri_geometry
    {
    public:
        ri_curves_geometry(const ri_classifier* p_cls, RtInt vstep, RtToken type,
                           RtInt ncurves, RtInt nvertices[], RtToken wrap, RtInt n,
                           RtToken tokens[], RtPointer params[]);
        virtual std::string type() const { return "Curves"; }
        virtual int typeN() const { return RI_GEOMETRY_CURVES; }

    public:
        int get_ncurves() const;
        const int* get_nvertices() const;
        const char* get_type() const;
        const char* get_wrap() const;

    public:
        const float* get_P() const;
        const float* get_N() const;
        const float* get_width() const;
        const float* get_Cs() const;
        const float* get_Os() const;

        const float* get_Pw() const;

        const float* get_Nv() const;
        const float* get_widthv() const;
        const float* get_Csv() const;
        const float* get_Osv() const;

        float get_constantwidth() const;
        const float* get_constantnormal() const;

        std::string get_vbasis() const;
        int get_vstep() const;

    public:
        std::string get_basis() const { return get_vbasis(); }
        int get_step() const { return get_vstep(); }

    public:
        bool is_linear() const;
        bool is_cubic() const;
        bool is_vertex(const char* key) const;
    };

    class ri_points_geometry : public ri_geometry
    {
    public:
        ri_points_geometry(const ri_classifier* p_cls, RtInt nverts, RtInt n,
                           RtToken tokens[], RtPointer params[]);
        virtual std::string type() const { return "Points"; }
        virtual int typeN() const { return RI_GEOMETRY_POINTS; }

    public:
        const float* get_P() const;
        const float* get_width() const;
        const float* get_Cs() const;
        const float* get_Os() const;
        float get_constantwidth() const;
        const float* get_N() const;
        int get_nverts() const;

    public:
        const float* get_constantnormal() const;
        const char* get_type() const;
    };

    /***********************
     * SubdivisionMesh
     ***********************/
    class ri_subdivision_mesh_geometry : public ri_base_polygons_geometry
    {
    public:
        ri_subdivision_mesh_geometry(const ri_classifier* p_cls, RtToken mask,
                                     RtInt npolys, RtInt nverts[], RtInt verts[],
                                     RtInt ntags, RtToken tags[], RtInt nargs[],
                                     RtInt intargs[], RtFloat floatargs[], RtInt n,
                                     RtToken tokens[], RtPointer params[]);
        virtual std::string type() const { return "SubdivisionMesh"; }
        virtual int typeN() const { return RI_GEOMETRY_SUBDIVISION_MESH; }

    public:
        int get_npolys() const;
        int get_nf() const;
        const int* get_nverts() const;
        const int* get_verts() const;
        int get_ntags() const;
        const std::string* get_tags() const;
        const int* get_nargs() const;
        const int* get_intargs() const;
        const float* get_floatargs() const;
        const float* get_P() const;
        const float* get_N() const;
        const float* get_Cs() const;
        const float* get_Os() const;

    public:
        const float* get_st() const;
        const float* get_stf() const;
        bool is_facevarying(const char* key) const;
    };

    /***********************
     * Procedural
     ***********************/
    class ri_procedural_geometry : public ri_geometry
    {
    public:
        ri_procedural_geometry();
        ri_procedural_geometry(const ri_classifier* p_cls, RtToken type, RtInt n,
                               RtToken data[], RtBound bound);
        virtual std::string type() const { return "Procedural"; }
        virtual int typeN() const { return RI_GEOMETRY_PROCEDURAL; }
        virtual void set_options(const ri_options& opts) { opts_ = opts; }
        virtual const ri_options& get_options() const { return opts_; }
        virtual void set_attributes(const ri_attributes& attr);

    public:
        const char* get_type() const;
        int get_n() const;
        const char* get_data_at(int i) const;
        const float* get_bound() const;

    protected:
        ri_options opts_;
    };

    /***********************
     * Geometry
     ***********************/
    class ri_teapot_geometry : public ri_geometry
    {
    public:
        ri_teapot_geometry();
        virtual std::string type() const { return "Teapot"; }
        virtual int typeN() const { return RI_GEOMETRY_TEAPOT; }
    };

    class ri_bunny_geometry : public ri_geometry
    {
    public:
        ri_bunny_geometry();
        virtual std::string type() const { return "Bunny"; }
        virtual int typeN() const { return RI_GEOMETRY_BUNNY; }
    };

    /***********************
     * Group
     ***********************/
    class ri_group_geometry : public ri_geometry
    {
    public:
        ri_group_geometry() {}
        virtual std::string type() const { return "Group"; }
        virtual int typeN() const { return RI_GEOMETRY_GROUP; }
        virtual void add(const ri_geometry* geo)
        {
            if (geo != this)
            {
                children_.push_back(geo);
            }
        }

    public:
        size_t get_size() const { return children_.size(); }
        const ri_geometry* get_at(size_t i) const { return children_[i]; }

    protected:
        std::vector<const ri_geometry*> children_;
    };

    class ri_solid_geometry : public ri_group_geometry
    {
    public:
        explicit ri_solid_geometry(RtToken operation);
        virtual std::string type() const { return "Solid"; }
        virtual int typeN() const { return RI_GEOMETRY_SOLID; }

    public:
        const char* get_operation() const;
    };

    class ri_object_instance_geometry : public ri_geometry
    {
    public:
        explicit ri_object_instance_geometry(const ri_geometry* geo) : geo_(geo) {}
        virtual std::string type() const { return "ObjectInstance"; }
        virtual int typeN() const { return RI_GEOMETRY_OBJECT_INSTANCE; }

    public:
        const ri_geometry* get_geometry() const { return geo_; }

    protected:
        const ri_geometry* geo_;
    };

    /***********************
     * Motion
     ***********************/

    class ri_motion_geometry : public ri_geometry
    {
    public:
        ri_motion_geometry(int n, const float times[],
                           const ri_geometry* const geos[]);
        virtual std::string type() const { return "Motion"; }
        virtual int typeN() const { return RI_GEOMETRY_MOTION; }

    public:
        int get_n() const;
        float get_time_at(size_t i) const;
        const ri_geometry* get_geometry_at(size_t i) const;

    public:
        const ri_geometry* get_at(size_t i) const { return get_geometry_at(i); }
        const float* get_times() const { return &times_[0]; }

    protected:
        std::vector<float> times_;
        std::vector<const ri_geometry*> geos_;
    };

    /***********************
     * Internal
     ***********************/
    class ri_bezier_patch_mesh_geometry : public ri_base_patch_geometry
    {
    public:
        explicit ri_bezier_patch_mesh_geometry(const ri_patch_geometry* pGeo);
        explicit ri_bezier_patch_mesh_geometry(const ri_patch_mesh_geometry* pGeo);

        virtual std::string type() const { return "BezierPatchMesh"; }
        virtual int typeN() const { return RI_GEOMETRY_BEZIER_PATCH_MESH; }
        virtual const ri_attributes& get_attributes() const
        {
            return pGeo_->get_attributes();
        }
        virtual const ri_transform& get_transform() const
        {
            return pGeo_->get_transform();
        }
        virtual const ri_classifier& get_classifier() const
        {
            return pGeo_->get_classifier();
        }

        virtual const ri_parameters::value_type* get_parameter(const char* key) const;
        virtual const ri_geometry* get_ptr() const { return pGeo_; }

    public:
        int get_nu() const;
        int get_nv() const;
        int get_uorder() const;
        int get_vorder() const;

    public:
        const float* get_P() const;
        const float* get_Pw() const;
        bool is_rational() const;

    protected:
        const ri_base_patch_geometry* pGeo_;
    };

    class ri_points_triangle_polygons_geometry : public ri_base_polygons_geometry
    {
    public:
        explicit ri_points_triangle_polygons_geometry(const ri_polygon_geometry* pGeo);
        explicit ri_points_triangle_polygons_geometry(const ri_general_polygon_geometry* pGeo);
        explicit ri_points_triangle_polygons_geometry(const ri_points_polygons_geometry* pGeo);
        explicit ri_points_triangle_polygons_geometry(
            const ri_points_general_polygons_geometry* pGeo);
        explicit ri_points_triangle_polygons_geometry(
            const ri_subdivision_mesh_geometry* pGeo);

        virtual std::string type() const { return "TrianglePolygons"; }
        virtual int typeN() const { return RI_GEOMETRY_POINTS_TRIANGLE_POLYGONS; }
        virtual const ri_attributes& get_attributes() const
        {
            return pGeo_->get_attributes();
        }
        virtual const ri_transform& get_transform() const
        {
            return pGeo_->get_transform();
        }
        virtual const ri_classifier& get_classifier() const
        {
            return pGeo_->get_classifier();
        }

        virtual const ri_parameters::value_type* get_parameter(const char* key) const;
        virtual const ri_geometry* get_ptr() const { return pGeo_; }

    public:
        int get_nfaces() const;
        int get_nverts() const;

        const int* get_verts() const;

        const float* get_P() const;
        const float* get_N() const;
        const float* get_Np() const;
        const float* get_Nf() const;

        const float* get_Cs() const;
        const float* get_Os() const;

        const float* get_st() const;
        const float* get_stf() const;

    protected:
        bool is_facevarying(const char* key) const;

    private:
        const ri_base_polygons_geometry* pGeo_;
        std::vector<int> indices_;
        int nverts_;
    };

    class ri_context;

    class ri_procedural_evaluation_geometry : public ri_group_geometry
    {
    public:
        explicit ri_procedural_evaluation_geometry(const ri_procedural_geometry* pGeo);
        ~ri_procedural_evaluation_geometry();

    public:
        virtual std::string type() const { return "ProceduralEvaluation"; }
        virtual int typeN() const { return RI_GEOMETRY_RROCEDURAL_EVALUATION; }

    protected:
        ri_context* ctx_;
    };

    class ri_bezier_curves_geometry : public ri_geometry
    {
    public:
        explicit ri_bezier_curves_geometry(const ri_curves_geometry* pGeo);

    public:
        virtual std::string type() const { return "BezierCurves"; }
        virtual int typeN() const { return RI_GEOMETRY_BEZIER_CURVES; }
        virtual const ri_attributes& get_attributes() const
        {
            return pGeo_->get_attributes();
        }
        virtual const ri_transform& get_transform() const
        {
            return pGeo_->get_transform();
        }
        virtual const ri_classifier& get_classifier() const
        {
            return pGeo_->get_classifier();
        }

        virtual const ri_parameters::value_type* get_parameter(const char* key) const;

    public:
        int get_ncurves() const { return pGeo_->get_ncurves(); }
        int get_order() const;
        int get_vorder() const { return this->get_order(); }
        const int* get_nvertices() const;

    public:
        const float* get_P() const;
        const float* get_N() const;
        const float* get_width() const;
        const float* get_Cs() const;
        const float* get_Os() const;

        const float* get_Nv() const;
        const float* get_widthv() const;
        const float* get_Csv() const;
        const float* get_Osv() const;

        float get_constantwidth() const { return pGeo_->get_constantwidth(); }
        const float* get_constantnormal() const { return pGeo_->get_constantnormal(); }

    protected:
        bool is_vertex(const char* key) const;
        bool is_periodic() const;
        bool is_bezier() const;
        bool is_original() const;

    protected:
        const ri_curves_geometry* pGeo_;
    };

    class ri_file_mesh_geometry : public ri_geometry
    {
    public:
        ri_file_mesh_geometry(const ri_classifier* p_cls, RtToken type, RtInt n,
                              RtToken tokens[], RtPointer params[]);

        virtual std::string type() const { return "FileMesh"; }
        virtual int typeN() const { return RI_GEOMETRY_FILE_MESH; }

    public:
        std::string get_type() const;
        std::string get_path() const;
        std::string get_full_path() const;
    };

    class ri_blobby_mesh_geometry : public ri_geometry
    {
    public:
        ri_blobby_mesh_geometry(const ri_blobby_geometry* pGeo,
                                const std::vector<int>& indices,
                                const std::vector<float>& vertices,
                                const ri_classifier& cls,
                                const ri_parameters& params);

        virtual std::string type() const { return "BlobbyMesh"; }
        virtual int typeN() const { return RI_GEOMETRY_BLOBBY_MESH; }

        virtual const ri_attributes& get_attributes() const
        {
            return pGeo_->get_attributes();
        }
        virtual const ri_transform& get_transform() const
        {
            return pGeo_->get_transform();
        }
        virtual const ri_classifier& get_classifier() const { return clsf_; }
        virtual const ri_parameters& get_parameters() const { return params_; }

        virtual const ri_geometry* get_ptr() const { return pGeo_; }

    public:
        int get_nfaces() const;
        int get_nverts() const;

        const int* get_verts() const;

    private:
        const ri_blobby_geometry* pGeo_;
        const std::vector<int> indices_;
        const std::vector<float> vertices_;
    };

    class ri_structure_synth_geometry : public ri_geometry
    {
    public:
        ri_structure_synth_geometry(const ri_classifier* p_cls, RtInt n,
                                    RtToken tokens[], RtPointer params[]);

        virtual std::string type() const { return "StructureSynth"; }
        virtual int typeN() const { return RI_GEOMETRY_STRUCTURE_SYNTH; }

    public:
        std::string get_path() const;
        std::string get_full_path() const;
    };
}

#endif
