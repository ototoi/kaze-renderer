#include "ri_frame_task.h"

namespace ri
{
    void ri_frame_task::add_geometry(ri_geometry* geo) { geos_.push_back(geo); }

    size_t ri_frame_task::get_geometry_size() const { return geos_.size(); }

    ri_geometry* ri_frame_task::get_geomoetry_at(size_t i) const
    {
        if (i < geos_.size())
        {
            return geos_[i];
        }
        else
        {
            return NULL;
        }
    }

    int ri_frame_task::run()
    {
        return 0;
    }
}
