#ifndef RI_COORDINATE_SYSTEM_H
#define RI_COORDINATE_SYSTEM_H

#include "ri.h"
#include "ri_object.h"
#include "ri_transform.h"
#include <string>

namespace ri
{
    class ri_coordinate_system : public ri_object
    {
    public:
        ri_coordinate_system(RtToken name, const ri_transform& trns);
        std::string get_name() const;
        std::string get_space() const { return get_name(); }
        const ri_transform& get_transform() const;

    protected:
        std::string name_;
        ri_transform trns_;
    };
}

#endif
