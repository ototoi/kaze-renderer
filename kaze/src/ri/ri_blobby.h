#ifndef RI_BLOBBY_H
#define RI_BLOBBY_H

#include <string.h>
#include <string>
#include <cassert>
#include "ri.h"
#include "ri_object.h"
#include "ri_options.h"
#include "ri_attributes.h"
#include "ri_transform.h"
#include "ri_parameters.h"
#include "ri_classifier.h"

namespace ri
{
    enum
    {
        RI_BLOBBY_UNKNOWN = -1,
        RI_BLOBBY_ADD = 0,
        RI_BLOBBY_MUL = 1,
        RI_BLOBBY_MAX = 2,
        RI_BLOBBY_MIN = 3,
        RI_BLOBBY_SUB = 4,
        RI_BLOBBY_DIV = 5,
        RI_BLOBBY_NEG = 6,
        RI_BLOBBY_IDENTITY = 7,
        RI_BLOBBY_CONSTANT = 1000,
        RI_BLOBBY_ELLIPSOID = 1001,
        RI_BLOBBY_SEGMENT = 1002,
        RI_BLOBBY_PLANE = 1003,
        RI_BLOBBY_IMPLICIT = 1004,

        RI_BLOBBY_AIR = 9000,
        //-----------------------
        RI_BLOBBY_COMPOSITE = 10000,
        RI_BLOBBY_VALUE = 10001,
        RI_BLOBBY_SPHERE = 10002,
        RI_BLOBBY_POINTS = 10003,
        RI_BLOBBY_FMA = 10004,
        RI_BLOBBY_INV = 10005
    };

    class ri_blobby : public ri_object
    {
    public:
        ri_blobby() : index_(0) {}
        virtual ~ri_blobby() {}
        virtual std::string type() const { return "Unknown"; }
        virtual int typeN() const { return RI_BLOBBY_UNKNOWN; }
        virtual size_t get_index() const { return index_; }
        virtual size_t get_count() const { return 1; }
        virtual void set_index(size_t index) { index_ = index; }
        virtual bool is_leaf() const { return false; }
        virtual bool is_branch() const { return !is_leaf(); }

    protected:
        size_t index_;
    };

    class ri_base_branch_blobby : public ri_blobby
    {
    public:
        ri_base_branch_blobby(const std::vector<std::shared_ptr<ri_blobby> >& cld)
            : cld_(cld) {}
        ri_base_branch_blobby(const std::shared_ptr<ri_blobby>& cld)
        {
            cld_.push_back(cld);
        }

    public:
        std::vector<std::shared_ptr<ri_blobby> >& get_children() { return cld_; }
        const std::vector<std::shared_ptr<ri_blobby> >& get_children() const { return cld_; }

    protected:
        std::vector<std::shared_ptr<ri_blobby> > cld_;
    };

    typedef ri_base_branch_blobby base_composite_blobby;

    class ri_base_leaf_blobby : public ri_blobby
    {
    public:
        ri_base_leaf_blobby() : clsf_(0) {}

    public:
        virtual bool is_leaf() const { return true; }
        virtual ri_classifier& get_classifier() { return clsf_; }
        virtual ri_parameters& get_parameters() { return params_; }
        virtual const ri_classifier& get_classifier() const { return clsf_; }
        virtual const ri_parameters& get_parameters() const { return params_; }

    protected:
        ri_classifier clsf_;
        ri_parameters params_;
    };

    //----------------------------------------------------------------------------------

    template <int CODE>
    class ri_binope_blobby : public ri_base_branch_blobby
    {
    public:
        ri_binope_blobby(const std::vector<std::shared_ptr<ri_blobby> >& cld)
            : ri_base_branch_blobby(cld) {}

    public:
        virtual std::string type() const { return Code2Str(CODE); }
        virtual int typeN() const { return CODE; }

    protected:
        static std::string Code2Str(int code)
        {
            switch (code)
            {
            case RI_BLOBBY_ADD:
                return "Add";
            case RI_BLOBBY_MUL:
                return "Mul";
            case RI_BLOBBY_MAX:
                return "Max";
            case RI_BLOBBY_MIN:
                return "Min";
            case RI_BLOBBY_SUB:
                return "Sub";
            case RI_BLOBBY_DIV:
                return "Div";
            }
            assert(0);
            return "";
        }
    };

    typedef ri_binope_blobby<RI_BLOBBY_ADD> ri_add_blobby;
    typedef ri_binope_blobby<RI_BLOBBY_MUL> ri_mul_blobby;
    typedef ri_binope_blobby<RI_BLOBBY_MAX> ri_max_blobby;
    typedef ri_binope_blobby<RI_BLOBBY_MIN> ri_min_blobby;
    typedef ri_binope_blobby<RI_BLOBBY_SUB> ri_sub_blobby;
    typedef ri_binope_blobby<RI_BLOBBY_DIV> ri_div_blobby;

    template <int CODE>
    class ri_unaope_blobby : public ri_base_branch_blobby
    {
    public:
        ri_unaope_blobby(const std::shared_ptr<ri_blobby>& cld)
            : ri_base_branch_blobby(cld) {}

    public:
        virtual std::string type() const { return Code2Str(CODE); }
        virtual int typeN() const { return CODE; }

    public:
        const std::vector<std::shared_ptr<ri_blobby> >& get_children() const { return cld_; }

    protected:
        static std::string Code2Str(int code)
        {
            switch (code)
            {
            case RI_BLOBBY_NEG:
                return "Neg";
            case RI_BLOBBY_INV:
                return "Inv";
            }
            assert(0);
            return "";
        }
    };

    typedef ri_unaope_blobby<RI_BLOBBY_NEG> ri_neg_blobby;
    typedef ri_unaope_blobby<RI_BLOBBY_INV> ri_inv_blobby;

    class ri_composite_blobby : public ri_base_branch_blobby
    {
    public:
        ri_composite_blobby(const std::vector<std::shared_ptr<ri_blobby> >& cld)
            : ri_base_branch_blobby(cld) {}

    public:
        virtual std::string type() const { return "Composite"; }
        virtual int typeN() const { return RI_BLOBBY_COMPOSITE; }
    };

    // Fused Multiply Add
    // x = A * B +C
    class ri_fma_blobby : public ri_base_branch_blobby
    {
    public:
        ri_fma_blobby(float a, const std::shared_ptr<ri_blobby>& b, float c, float min = 0,
                      float max = 1)
            : ri_base_branch_blobby(b), mul_(a), add_(c) {}

    public:
        virtual std::string type() const { return "FMA"; }
        virtual int typeN() const { return RI_BLOBBY_FMA; }

    public:
        float get_mul() const { return mul_; }
        float get_add() const { return add_; }
        float get_min() const { return min_; }
        float get_max() const { return max_; }

    protected:
        float mul_;
        float add_;
        float min_;
        float max_;
    };

    //-------------------------------------------------

    class ri_identity_blobby : public ri_base_leaf_blobby
    {
    public:
        virtual std::string type() const { return "Identity"; }
        virtual int typeN() const { return RI_BLOBBY_IDENTITY; }
    };

    class ri_constant_blobby : public ri_base_leaf_blobby
    {
    public:
        ri_constant_blobby(float value) : value_(value) {}

    public:
        virtual std::string type() const { return "Constant"; }
        virtual int typeN() const { return RI_BLOBBY_CONSTANT; }

        float get_value() const { return value_; }

    protected:
        float value_;
    };

    class ri_value_blobby : public ri_base_leaf_blobby
    {
    public:
        ri_value_blobby(float value) : value_(value) {}

    public:
        virtual std::string type() const { return "Value"; }
        virtual int typeN() const { return RI_BLOBBY_VALUE; }

        float get_value() const { return value_; }

    protected:
        float value_;
    };

    class ri_air_blobby : public ri_base_leaf_blobby
    {
    public:
        ri_air_blobby(int subcode) : subcode_(subcode)
        {
            static const float INITMAT[] = {1, 0, 0, 0, 0, 1, 0, 0,
                                            0, 0, 1, 0, 0, 0, 0, 1};
            memcpy(mat_, INITMAT, sizeof(float) * 16);
        }

    public:
        virtual std::string type() const { return "Air"; }
        virtual int typeN() const { return RI_BLOBBY_AIR; }

        void set_name(const std::string& name) { name_ = name; }
        std::string get_name() const { return name_; }

        void set_mat(const float mat[]) { memcpy(mat_, mat, sizeof(float) * 16); }
        const float* get_mat() const { return mat_; }

        void set_int(const std::vector<int>& i) { i_ = i; }
        const std::vector<int>& get_int() const { return i_; }

        void set_float(const std::vector<float>& f) { f_ = f; }
        const std::vector<float>& get_float() const { return f_; }

        void set_string(const std::vector<std::string>& s) { s_ = s; }
        const std::vector<std::string>& get_string() const { return s_; }

    protected:
        int subcode_;
        std::string name_;
        float mat_[16];
        std::vector<int> i_;
        std::vector<float> f_;
        std::vector<std::string> s_;
    };

    class ri_ellipsoid_blobby : public ri_base_leaf_blobby
    {
    public:
        ri_ellipsoid_blobby(const float mat[16])
        {
            memcpy(mat_, mat, sizeof(float) * 16);
        }

    public:
        virtual std::string type() const { return "Ellipsoid"; }
        virtual int typeN() const { return RI_BLOBBY_ELLIPSOID; }

        const float* get_mat() const { return mat_; }

    protected:
        float mat_[16];
    };

    class ri_segment_blobby : public ri_base_leaf_blobby
    {
    public:
        ri_segment_blobby(const float f[23]) { memcpy(f_, f, sizeof(float) * 23); }
        virtual std::string type() const { return "Segment"; }
        virtual int typeN() const { return RI_BLOBBY_SEGMENT; }
        size_t get_count() const { return 2; }

    public:
        const float* get_position0() const { return f_ + 0; }
        const float* get_position1() const { return f_ + 3; }
        float get_radius() const { return f_[6]; }
        const float* get_mat() const { return f_ + 7; }

    protected:
        float f_[23];
    };

    class ri_plane_blobby : public ri_base_leaf_blobby
    {
    public:
        ri_plane_blobby(const std::string& name, const float f[4]) : name_(name)
        {
            memcpy(f_, f, sizeof(float) * 4);
        }
        virtual std::string type() const { return "Plane"; }
        virtual int typeN() const { return RI_BLOBBY_PLANE; }

        std::string get_name() const { return name_; }

    protected:
        std::string name_;
        float f_[4];
    };

    class ri_implicit_field_blobby : public ri_base_leaf_blobby
    {
    public:
        ri_implicit_field_blobby(const std::string& name, const std::vector<float>& f,
                                 const std::vector<std::string>& s)
            : name_(name), f_(f), s_(s) {}

        virtual std::string type() const { return "ImplicitField"; }
        virtual int typeN() const { return RI_BLOBBY_IMPLICIT; }

        std::string get_name() const { return name_; }
        const std::vector<float>& get_floats() const { return f_; }
        const std::vector<std::string>& get_strings() const { return s_; }

    protected:
        std::string name_;
        std::vector<float> f_;
        std::vector<std::string> s_;
    };

    class ri_sphere_blobby : public ri_base_leaf_blobby
    {
    public:
        ri_sphere_blobby(const float P[3], float R)
        {
            memcpy(P_, P, sizeof(float) * 3);
            R_ = R;
        }

    public:
        virtual std::string type() const { return "Sphere"; }
        virtual int typeN() const { return RI_BLOBBY_SPHERE; }

        const float* get_position() const { return P_; }
        float get_radius() const { return R_; }

    protected:
        float P_[3];
        float R_;
    };

    class ri_points_blobby : public ri_base_leaf_blobby
    {
    public:
        ri_points_blobby() {}

    public:
        virtual std::string type() const { return "Points"; }
        virtual int typeN() const { return RI_BLOBBY_POINTS; }

    protected:
        int dummy_;
    };
}

#endif
