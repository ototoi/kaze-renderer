%skeleton "lalr1.cc"
%define "parser_class_name" "rib_parser"
%defines

%{
#ifdef _WIN32
#pragma warning(disable : 4786)
#pragma warning(disable : 4102)
#pragma warning(disable : 4996)
#pragma warning(disable : 4065)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define YYDEBUG 1
#define YYERROR_VERBOSE 1

#include <algorithm>
#include <iterator>
#include "ri.h"
#include "rib_array.h"
#include "ri_context.h"
#include "ri_rib_decoder.h"
#include "ri_rib_parse_param.h"



%}

%union{
	int   itype;
	float ftype;
	char  ctype;
	char* stype;
	struct integer_array* aitype;
	struct float_array*   aftype;
	struct string_array*  astype;
	struct token_value*   tvtype;
	struct token_value_array* atvtype;
}

%pure-parser

// The parsing context.
%parse-param { void * scanner }
%lex-param   { void * scanner }

%locations
// %debug
%error-verbose

%{

#define yyerror(m) error(yylloc,m)

using namespace kaze::ri;

#define GET_CTX() (((ri_rib_parse_param*)(scanner))->ctx)
#define GET_CLS() (GET_CTX()->RiGetClassifier())
#define REQ_PARAM() {((ri_rib_parse_param*)(scanner))->fParams=true;} 
#define SET_LIGHTMAP(Key, Handle) (GET_CTX()->RiSetLightHandle(Key,Handle));
#define SET_OBJECTMAP(Key, Handle) (GET_CTX()->RiSetObjectHandle(Key,Handle));

static
int expand_token_value(std::vector<RtToken>& tokens, std::vector<void*>& params, const token_value_array& atv){
	int n = (int)atv.size();
	if(n){
		tokens.resize(n);
		params.resize(n);
	}else{
		tokens.resize(1);
		params.resize(1);
	}
	for(int i = 0;i<n;i++){
		tokens[i] = (char*)(atv[i]->first.c_str());
	    params[i] = (void*)(atv[i]->second->get_ptr());
    }
    return n;
}

static 
void release_value(const char* v){delete[] v;}
static
void release_value(integer_array* v){delete v;}
static
void release_value(float_array* v){delete v;}
static
void release_value(string_array* v){delete v;}
//static
//void release_value(token_value* v){delete v;}
static
void release_value(token_value_array* v){delete v;}

static
void convet_to_RtMatrix(float_array* v, RtMatrix m){
	float_array& rv = *v;
	for(int j=0;j<4;j++){
		for(int i=0;i<4;i++){
			m[j][i] = (RtFloat)rv[4*j+i];
		}
	}
}

static
int FindParam(RtToken name, const RtToken tokens[], int n)
{
	for(int i=0;i<n;i++){
		if(strcmp(name, tokens[i])==0)return i;
	}
	return -1;
}

#define	YY_DECL											\
	int						\
	yylex(yy::rib_parser::semantic_type* yylval,		\
		 yy::rib_parser::location_type* yylloc,		\
		 void* scanner)

YY_DECL;

%}

/*tokens*/

%token <ctype> EOF_TOKEN
%token <ctype> UNKNOWN_TOKEN INVALID_VALUE
%token <ftype> FLOAT_TOKEN
%token <itype> INTEGER_TOKEN
%token <stype> STRING_TOKEN
%token <stype> COMMENT

%type <itype> integer
%type <ftype> float
%type <stype> string version_string
%type <aftype> scalar_array scalar_array_or_empty
%type <aftype> floats float_array /*float_array_or_empty*/
%type <aitype> integers integer_array integer_array_or_empty
%type <astype> strings string_array string_array_or_empty
%type <tvtype> token_value
%type <atvtype> token_value_array
%type <atvtype> opt_token_value_array

/*
%token <ctype> TOKEN_ARRAY_START
%token <ctype> TOKEN_ARRAY_END
*/

/*
%token <ctype> EOL_TOKEN EOF_TOKEN CHAR_TOKEN 
%token <ctype> TOKEN_BESSELFILTER
%token <ctype> TOKEN_BOXFILTER
%token <ctype> TOKEN_CATMULLROMFILTER
%token <ctype> TOKEN_DISKFILTER
%token <ctype> TOKEN_GAUSSIANFILTER
%token <ctype> TOKEN_SINCFILTER
%token <ctype> TOKEN_TRIANGLEFILTER
*/

%token <ctype> TOKEN_ARCHIVEBEGIN
%token <ctype> TOKEN_ARCHIVEEND
%token <ctype> TOKEN_AREALIGHTSOURCE
%token <ctype> TOKEN_ATMOSPHERE
%token <ctype> TOKEN_ATTRIBUTE
%token <ctype> TOKEN_ATTRIBUTEBEGIN
%token <ctype> TOKEN_ATTRIBUTEEND
%token <ctype> TOKEN_BASIS

%token <ctype> TOKEN_BLOBBY
%token <ctype> TOKEN_BOUND

%token <ctype> TOKEN_CLIPPING
%token <ctype> TOKEN_COLOR
%token <ctype> TOKEN_COLORSAMPLES
%token <ctype> TOKEN_CONCATTRANSFORM
%token <ctype> TOKEN_CONE
%token <ctype> TOKEN_COORDINATESYSTEM
%token <ctype> TOKEN_COORDSYSTRANSFORM
%token <ctype> TOKEN_CROPWINDOW
%token <ctype> TOKEN_CURVES
%token <ctype> TOKEN_CYLINDER
%token <ctype> TOKEN_DECLARE
%token <ctype> TOKEN_DEFORMATION
%token <ctype> TOKEN_DEPTHOFFIELD
%token <ctype> TOKEN_DETAIL
%token <ctype> TOKEN_DETAILRANGE
%token <ctype> TOKEN_DISK
%token <ctype> TOKEN_DISPLACEMENT
%token <ctype> TOKEN_DISPLAYCHANNEL
%token <ctype> TOKEN_DISPLAY
%token <ctype> TOKEN_ELSE
%token <ctype> TOKEN_ELSEIF
%token <ctype> TOKEN_ERRORABORT
%token <ctype> TOKEN_ERRORHANDLER
%token <ctype> TOKEN_ERRORIGNORE
%token <ctype> TOKEN_ERRORPRINT
%token <ctype> TOKEN_EXPOSURE
%token <ctype> TOKEN_EXTERIOR
%token <ctype> TOKEN_FORMAT
%token <ctype> TOKEN_FRAMEASPECTRATIO
%token <ctype> TOKEN_FRAMEBEGIN
%token <ctype> TOKEN_FRAMEEND
%token <ctype> TOKEN_GENERALPOLYGON
%token <ctype> TOKEN_GEOMETRICAPPROXIMATION
%token <ctype> TOKEN_GEOMETRY
%token <ctype> TOKEN_HIERARCHICALSUBDIVISIONMESH
%token <ctype> TOKEN_HIDER
%token <ctype> TOKEN_HYPERBOLOID
%token <ctype> TOKEN_IDENTITY
%token <ctype> TOKEN_IFBEGIN
%token <ctype> TOKEN_IFEND
%token <ctype> TOKEN_ILLUMINATE
%token <ctype> TOKEN_IMAGER
%token <ctype> TOKEN_INTERIOR
%token <ctype> TOKEN_LIGHTSOURCE
%token <ctype> TOKEN_MAKEOCCLUSION
%token <ctype> TOKEN_MAKEBRICKMAP
%token <ctype> TOKEN_MAKEBUMP
%token <ctype> TOKEN_MAKECUBEFACEENVIRONMENT
%token <ctype> TOKEN_MAKELATLONGENVIRONMENT
%token <ctype> TOKEN_MAKESHADOW
%token <ctype> TOKEN_MAKETEXTURE
%token <ctype> TOKEN_MATTE
%token <ctype> TOKEN_MOTIONBEGIN
%token <ctype> TOKEN_MOTIONEND
%token <ctype> TOKEN_NUPATCH
%token <ctype> TOKEN_OBJECTBEGIN
%token <ctype> TOKEN_OBJECTEND
%token <ctype> TOKEN_OBJECTINSTANCE
%token <ctype> TOKEN_OPACITY
%token <ctype> TOKEN_OPTION
%token <ctype> TOKEN_ORIENTATION
%token <ctype> TOKEN_PARABOLOID
%token <ctype> TOKEN_PATCH
%token <ctype> TOKEN_PATCHMESH
%token <ctype> TOKEN_PERSPECTIVE
%token <ctype> TOKEN_PIXELFILTER
%token <ctype> TOKEN_PIXELSAMPLES
%token <ctype> TOKEN_PIXELVARIANCE
%token <ctype> TOKEN_POINTS
%token <ctype> TOKEN_POINTSGENERALPOLYGONS
%token <ctype> TOKEN_POINTSPOLYGONS
%token <ctype> TOKEN_POLYGON
%token <ctype> TOKEN_PROCEDURAL
%token <ctype> TOKEN_PROCEDURE
%token <ctype> TOKEN_PROJECTION
%token <ctype> TOKEN_QUANTIZE
%token <ctype> TOKEN_READARCHIVE
%token <ctype> TOKEN_RELATIVEDETAIL
%token <ctype> TOKEN_RESOURCE
%token <ctype> TOKEN_RESOURCEBEGIN
%token <ctype> TOKEN_RESOURCEEND
%token <ctype> TOKEN_REVERSEORIENTATION
%token <ctype> TOKEN_ROTATE
%token <ctype> TOKEN_SCALE
%token <ctype> TOKEN_SCOPEDCOORDINATESYSTEM
%token <ctype> TOKEN_SCREENWINDOW
%token <ctype> TOKEN_SHADINGINTERPOLATION
%token <ctype> TOKEN_SHADINGRATE
%token <ctype> TOKEN_SHUTTER
%token <ctype> TOKEN_SIDES

%token <ctype> TOKEN_SKEW
%token <ctype> TOKEN_SOLIDBEGIN
%token <ctype> TOKEN_SOLIDEND
%token <ctype> TOKEN_SPHERE
%token <ctype> TOKEN_SUBDIVISIONMESH
%token <ctype> TOKEN_SURFACE
%token <ctype> TOKEN_TEXTURECOORDINATES
%token <ctype> TOKEN_TORUS
%token <ctype> TOKEN_TRANSFORM
%token <ctype> TOKEN_TRANSFORMBEGIN
%token <ctype> TOKEN_TRANSFORMEND
%token <ctype> TOKEN_TRANSFORMPOINTS
%token <ctype> TOKEN_TRANSLATE

%token <ctype> TOKEN_TRIMCURVE
%token <ctype> TOKEN_VERSION
%token <ctype> TOKEN_WORLDBEGIN
%token <ctype> TOKEN_WORLDEND
%token <ctype> TOKEN_SHADERLAYER
%token <ctype> TOKEN_CONNECTSHADERLAYERS

%token <ctype> TOKEN_PROCEDURAL2
%token <ctype> TOKEN_SYSTEM
%token <ctype> TOKEN_VOLUME
%token <ctype> TOKEN_VPATMOSPHERE
%token <ctype> TOKEN_VPINTERIOR
%token <ctype> TOKEN_VPSURFACE

%token <ctype> TOKEN_INTEGRATOR
%token <ctype> TOKEN_BXDF
%token <ctype> TOKEN_PATTERN

/*expression*/
%type <ctype> archivebegin
%type <ctype> archiveend
%type <ctype> arealightsource
%type <ctype> atmosphere
%type <ctype> attribute
%type <ctype> attributebegin
%type <ctype> attributeend
%type <ctype> basis
%type <ctype> blobby
%type <ctype> bound
%type <ctype> clipping
%type <ctype> color
%type <ctype> colorsamples
%type <ctype> concattransform
%type <ctype> cone
%type <ctype> coordinatesystem
%type <ctype> coordsystransform
%type <ctype> cropwindow
%type <ctype> curves
%type <ctype> cylinder
%type <ctype> declare
%type <ctype> deformation
%type <ctype> depthoffield
%type <ctype> detail
%type <ctype> detailrange
%type <ctype> disk
%type <ctype> displacement
%type <ctype> displaychannel
%type <ctype> display
%type <ctype> else
%type <ctype> elseif
%type <ctype> errorabort
%type <ctype> errorhandler
%type <ctype> errorignore
%type <ctype> errorprint
%type <ctype> exposure
%type <ctype> exterior
%type <ctype> format
%type <ctype> frameaspectratio
%type <ctype> framebegin
%type <ctype> frameend
%type <ctype> generalpolygon
%type <ctype> geometricapproximation
%type <ctype> geometry
%type <ctype> hierarchicalsubdivisionmesh
%type <ctype> hider
%type <ctype> hyperboloid
%type <ctype> identity
%type <ctype> ifbegin
%type <ctype> ifend
%type <ctype> illuminate
%type <ctype> imager
%type <ctype> interior
%type <ctype> lightsource
%type <ctype> makeocclusion
%type <ctype> makebrickmap
%type <ctype> makebump
%type <ctype> makecubefaceenvironment
%type <ctype> makelatlongenvironment
%type <ctype> makeshadow
%type <ctype> maketexture
%type <ctype> matte
%type <ctype> motionbegin
%type <ctype> motionend
%type <ctype> nupatch
%type <ctype> objectbegin
%type <ctype> objectend
%type <ctype> objectinstance
%type <ctype> opacity
%type <ctype> option
%type <ctype> orientation
%type <ctype> procedural
%type <ctype> procedure
%type <ctype> paraboloid
%type <ctype> patch
%type <ctype> patchmesh
%type <ctype> perspective
%type <ctype> pixelfilter
%type <ctype> pixelsamples
%type <ctype> pixelvariance
%type <ctype> points
%type <ctype> pointsgeneralpolygons
%type <ctype> pointspolygons
%type <ctype> polygon
%type <ctype> projection
%type <ctype> quantize
%type <ctype> readarchive
%type <ctype> relativedetail
%type <ctype> resource
%type <ctype> resourcebegin
%type <ctype> resourceend
%type <ctype> reverseorientation
%type <ctype> rotate
%type <ctype> scale
%type <ctype> scopedcoordinatesystem
%type <ctype> screenwindow
%type <ctype> shadinginterpolation
%type <ctype> shadingrate
%type <ctype> shutter
%type <ctype> sides
%type <ctype> skew
%type <ctype> solidbegin
%type <ctype> solidend
%type <ctype> sphere
%type <ctype> subdivisionmesh
%type <ctype> surface
%type <ctype> texturecoordinates
%type <ctype> torus
%type <ctype> transform
%type <ctype> transformbegin
%type <ctype> transformend
%type <ctype> transformpoints
%type <ctype> translate
%type <ctype> trimcurve
%type <ctype> version
%type <ctype> worldbegin
%type <ctype> worldend
%type <ctype> shaderlayer
%type <ctype> connectshaderlayers
%type <ctype> procedural2
%type <ctype> system
%type <ctype> volume
%type <ctype> vpatmosphere
%type <ctype> vpinterior
%type <ctype> vpsurface

%type <ctype> arraystart
%type <ctype> arrayend

%type <ctype> integrator
%type <ctype> bxdf
%type <ctype> pattern

%%

file : requests
			{ ; }
	| requests EOF_TOKEN
			{ ; }
	;

requests : request
	|	requests request
	;

request : complete_request
			{;}
	|	COMMENT
			{
				release_value($1);
			}
	;

complete_request
	:	version version_string
			{  
				GET_CTX()->RiVersion($2);
				release_value($2);
			}
	|	version float
			{
				GET_CTX()->RiVersion($2);
			}
	|	declare string string
			{
				GET_CTX()->RiDeclare($2, $3);
				release_value($2);
				release_value($3);	
			}
	|	framebegin integer
			{
				GET_CTX()->RiFrameBegin($2);
			}
	|	frameend
			{
				GET_CTX()->RiFrameEnd();
			}
	|	worldbegin
			{
				GET_CTX()->RiWorldBegin();
			}
	|	worldend
			{
				GET_CTX()->RiWorldEnd();
			}
	|	format integer integer float
			{
				GET_CTX()->RiFormat($2, $3, $4);
			}
	|	frameaspectratio float
			{
				GET_CTX()->RiFrameAspectRatio($2);
			}
	|	screenwindow float float float float
			{
				GET_CTX()->RiScreenWindow($2, $3, $4, $5);
			}
	|	cropwindow float float float float
			{
				GET_CTX()->RiCropWindow($2, $3, $4, $5);
			}
	|	projection string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiProjectionV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
	|	clipping float float
			{
				GET_CTX()->RiClipping($2, $3);	
			}
	|	depthoffield
			{
				GET_CTX()->RiDepthOfField(0, 0, 0);
			}
	|	depthoffield float float float
			{
				GET_CTX()->RiDepthOfField($2, $3, $4);
			}
	|	shutter float float
			{
				GET_CTX()->RiShutter($2, $3);
			}
	|	pixelvariance float
			{
				GET_CTX()->RiPixelVariance($2);
			}
	|	pixelsamples float float
			{
				GET_CTX()->RiPixelSamples($2, $3);
			}
	|	pixelfilter string float float
			{
				GET_CTX()->RiPixelFilter($2, $3, $4);
				release_value($2);
			}
	|	else 
			{
				GET_CTX()->RiElse();	
			}
	|	elseif string
			{
				GET_CTX()->RiElseIf($2);
				release_value($2);	
			}
	|	exposure float float
			{
				GET_CTX()->RiExposure($2, $3);
			}
	|	imager string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiImagerV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
	|	quantize string integer integer integer float
			{
				GET_CTX()->RiQuantize($2, $3, $4, $5, $6);
				release_value($2);
			}
	|	displaychannel string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiDisplayChannelV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
	|	display string string string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$5);
				GET_CTX()->RiDisplayV($2, $3, $4, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
				release_value($5);
			}
	|	hider string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiHiderV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
	|	colorsamples scalar_array scalar_array
			{
				int n = (int)($2->size());
				GET_CTX()->RiColorSamples(n, &((*$2)[0]), &((*$3)[0]));
				release_value($2);
				release_value($3);
			}
	|	relativedetail float
			{
				GET_CTX()->RiRelativeDetail($2);	
			}
	|	option string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiOptionV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);	
			}
	|	attributebegin
			{
				GET_CTX()->RiAttributeBegin();	
			}
	|	attributeend
			{
				GET_CTX()->RiAttributeEnd();
			}
	|	color scalar_array
			{
				RtColor c;
				c[0] = (*$2)[0];
				c[1] = (*$2)[1];
				c[2] = (*$2)[2];
				GET_CTX()->RiColor(c);
				release_value($2);
			}
	|	color float float float
			{
				RtColor c;
				c[0] = $2;
				c[1] = $3;
				c[2] = $4;
				GET_CTX()->RiColor(c);
			}
	|	opacity scalar_array
			{
				RtColor c;
				c[0] = (*$2)[0];
				c[1] = (*$2)[1];
				c[2] = (*$2)[2];
				GET_CTX()->RiOpacity(c);
				release_value($2);
			}
	|	opacity float float float
			{
				RtColor c;
				c[0] = $2;
				c[1] = $3;
				c[2] = $4;
				GET_CTX()->RiOpacity(c);
			}
	|	texturecoordinates float float float float float float float float
			{
				GET_CTX()->RiTextureCoordinates($2, $3, $4, $5, $6, $7, $8, $9);
			}
	|	texturecoordinates scalar_array
			{
				float_array& afv = *$2;
				GET_CTX()->RiTextureCoordinates(afv[0], afv[1], afv[2], afv[3], afv[4], afv[5], afv[6], afv[7]);
				release_value($2);
			}
	|	lightsource string integer opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				RtLightHandle handle = GET_CTX()->RiLightSourceV($2, n, &tokens[0], &params[0]);
				SET_LIGHTMAP($3, handle);
				release_value($2);
				//release_value($3);
				release_value($4);
			}
	|	lightsource string string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				RtLightHandle handle = GET_CTX()->RiLightSourceV($2, n, &tokens[0], &params[0]);
				SET_LIGHTMAP($3, handle);
				release_value($2);
				release_value($3);
				release_value($4);	
			}
	|	arealightsource string integer opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				RtLightHandle handle = GET_CTX()->RiAreaLightSourceV($2, n, &tokens[0], &params[0]);
				SET_LIGHTMAP($3, handle);
				release_value($2);
				//release_value($3);
				release_value($4);	
			}
	|	arealightsource string string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				RtLightHandle handle = GET_CTX()->RiAreaLightSourceV($2, n, &tokens[0], &params[0]);
				SET_LIGHTMAP($3, handle);
				release_value($2);
				release_value($3);
				release_value($4);	
			}
	|	ifbegin string
			{
				GET_CTX()->RiIfBegin($2);
				release_value($2);	
			}
	|	ifend 
			{
				GET_CTX()->RiIfEnd();	
			}
	|	illuminate integer integer
			{
				GET_CTX()->RiIlluminate($2,$3);
			}
	|	illuminate string integer
			{
				GET_CTX()->RiIlluminate($2,$3);
				release_value($2);
			}
	|	surface string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiSurfaceV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
	|	atmosphere string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiAtmosphereV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);	
			}
	|	interior string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiInteriorV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);		
			}
	|	exterior string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiExteriorV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
	|	shadingrate float
			{
				GET_CTX()->RiShadingRate($2);
			}
	|	shadinginterpolation string
			{
				GET_CTX()->RiShadingInterpolation($2);
				release_value($2);
			}
	|	matte integer
			{
				GET_CTX()->RiMatte($2);	
			}
	|	bound float float float float float float
			{
				RtBound b = {$2, $3, $4, $5, $6, $7};
				GET_CTX()->RiBound(b);
			}
	|	bound scalar_array
			{
				float_array& afv = *$2;
				RtBound b = {afv[0], afv[1], afv[2], afv[3], afv[4], afv[5]};
				GET_CTX()->RiBound(b);
				release_value($2);
			}
	|	detail float float float float float float
			{
				RtBound b = {$2, $3, $4, $5, $6, $7};
				GET_CTX()->RiDetail(b);
			}
	|	detail scalar_array
			{
				float_array& afv = *$2;
				RtBound b = {afv[0], afv[1], afv[2], afv[3], afv[4], afv[5]};
				GET_CTX()->RiDetail(b);
				release_value($2);
			}
	|	detailrange float float float float
			{
				GET_CTX()->RiDetailRange($2, $3, $4, $5);
			}
	|	detailrange scalar_array
			{
				float_array& afv = *$2;
				GET_CTX()->RiDetailRange(afv[0], afv[1], afv[2], afv[3]);
				release_value($2);
			}
	|	geometricapproximation string float
			{
				GET_CTX()->RiGeometricApproximation($2, $3);
				release_value($2);
			}
	|	geometricapproximation string scalar_array
			{
				GET_CTX()->RiGeometricApproximation($2, (*$3)[0]);
				release_value($2);
				release_value($3);
			}
	|	orientation string
			{
				GET_CTX()->RiOrientation($2);
				release_value($2);
			}
	|	reverseorientation
			{
				GET_CTX()->RiReverseOrientation();
			}
	|	sides integer
			{
				GET_CTX()->RiSides($2);
			}
	|	identity
			{
				GET_CTX()->RiIdentity();
			}
	|	transform scalar_array
			{
				RtMatrix tmp;
				convet_to_RtMatrix($2,tmp);
				GET_CTX()->RiTransform(tmp);
				release_value($2);
			}
	|	concattransform scalar_array
			{
				RtMatrix tmp;
				convet_to_RtMatrix($2,tmp);
				GET_CTX()->RiConcatTransform(tmp);
				release_value($2);
			}
	|	perspective float
			{
				GET_CTX()->RiPerspective($2);
			}
	|	translate float float float
			{
				GET_CTX()->RiTranslate($2, $3, $4);
			}
	|	rotate float float float float
			{
				GET_CTX()->RiRotate($2, $3, $4, $5);
			}
	|	scale float float float
			{
				GET_CTX()->RiScale($2, $3, $4);
			}
	|	skew float float float float float float float
			{
				GET_CTX()->RiSkew($2, $3, $4, $5, $6, $7, $8);
			}
	|	skew scalar_array
			{
				float_array& afv = *$2;
				GET_CTX()->RiSkew(afv[0],afv[1],afv[2],afv[3],afv[4],afv[5],afv[6]);
				release_value($2);
			}
	|	deformation string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiDeformationV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);		
			}
	|	displacement string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiDisplacementV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);	
			}
	|	coordinatesystem string
			{
				GET_CTX()->RiCoordinateSystem($2);
				release_value($2);
			}
	|	scopedcoordinatesystem string
			{
				GET_CTX()->RiScopedCoordinateSystem($2);
				release_value($2);
			}
	|	coordsystransform string
			{
				GET_CTX()->RiCoordSysTransform($2);
				release_value($2);
			}
	|	transformpoints
			{
				;//GET_CTX()->RiTransformPoints();
			}
	|	transformbegin
			{
				GET_CTX()->RiTransformBegin();
			}
	|	transformend
			{
				GET_CTX()->RiTransformEnd();
			}
	|   resource string string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				GET_CTX()->RiResourceV($2, $3, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
			}
	|	resourcebegin
			{
				GET_CTX()->RiResourceBegin();
			}
	|	resourceend
			{
				GET_CTX()->RiResourceEnd();
			}
	|	attribute string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiAttributeV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
	|	polygon token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$2);
				int nFind = FindParam(RI_P,&tokens[0],n);//TODO:move to context...
				if(nFind>=0){
					int nv = (((*$2)[nFind])->second->size())/3;//TODO
					GET_CTX()->RiPolygonV(nv, n, &tokens[0], &params[0]);
				}
				release_value($2);
			}
	|	generalpolygon integer_array token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiGeneralPolygonV($2->size(), &(*$2)[0], n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
    |   curves string integer_array string opt_token_value_array
            {
            	//RiCurves (RtToken type, RtInt ncurves, RtInt nvertices[], RtToken wrap, ...);
            	std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$5);
				int nCurves = (int)($3->size());
				GET_CTX()->RiCurvesV($2, nCurves, &((*$3)[0]), $4 , n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
				release_value($5);	
            }
    |   blobby integer integer_array float_array string_array_or_empty opt_token_value_array
            {
            	//RiBlobby(RtInt nleaf, RtInt ncode, RtInt code[], RtInt nflt, RtFloat flt[], RtInt nstr, RtToken str[], …);
            	std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$6);
				int ncode = (int)($3->size());
				int nflt  = (int)($4->size());
				int nstr  = (int)($5->size());
				
				RtToken sDummy = "";
				RtToken* array5 = &sDummy;
				if($5->size()){
					array5 = &(*$5)[0];
				}
				
				
				GET_CTX()->RiBlobbyV($2, ncode, &((*$3)[0]), nflt, &((*$4)[0]), nstr, array5, n, &tokens[0], &params[0]);
				release_value($3);
				release_value($4);
				release_value($5);
				release_value($6);
            }
    |   readarchive string 
	    	{
	    		GET_CTX()->RiReadArchiveV($2,NULL,0,NULL,NULL);
	    		release_value($2);
	    	}
	|	archivebegin string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiArchiveBeginV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
	|	archiveend
			{
				GET_CTX()->RiArchiveEnd();
			}
	|   procedural string string_array scalar_array opt_token_value_array
            {
				if($4->size() == 6){
					std::vector<RtToken> tokens;
					std::vector<void*> params;
					int n = expand_token_value(tokens,params,*$5);

					GET_CTX()->RiProceduralV($2, $3->size(), &(*$3)[0], &(*$4)[0], n, &tokens[0], &params[0]);
				}
				release_value($2);
				release_value($3);
				release_value($4);
				release_value($5);
            }
	|   procedure string scalar_array opt_token_value_array
            {
				if($3->size() == 6){
					std::vector<RtToken> tokens;
					std::vector<void*> params;
					int n = expand_token_value(tokens,params,*$4);

					GET_CTX()->RiProcedureV($2, &(*$3)[0], n, &tokens[0], &params[0]);
				}
				release_value($2);
				release_value($3);
				release_value($4);
            }
	|   points opt_token_value_array
            {
            	std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$2);
				int nFind = FindParam(RI_P,&tokens[0],n);//TODO:move to context...
				if(nFind>=0){
					int nv = (((*$2)[nFind])->second->size())/3;//TODO
					GET_CTX()->RiPointsV(nv, n, &tokens[0], &params[0]);
				}
				release_value($2);	
            }
	|	pointspolygons integer_array integer_array token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				GET_CTX()->RiPointsPolygonsV($2->size(), &(*$2)[0], &(*$3)[0], n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
			}
	|	pointsgeneralpolygons integer_array integer_array integer_array token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$5);
				GET_CTX()->RiPointsGeneralPolygonsV($2->size(), &(*$2)[0], &(*$3)[0], &(*$4)[0], n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
				release_value($5);
			}
	|	basis string integer string integer
			{
				GET_CTX()->RiBasis($2, $3, $4, $5);
				release_value($2);
				release_value($4);
			}
	|	basis string integer scalar_array integer
			{
				RtBasis vb = {};
				if($4->size()==16){
					memcpy(vb, &((*$4)[0]), sizeof(RtBasis));
					GET_CTX()->RiBasis($2, $3, vb, $5);
				}else{
					yyerror("Unexpected basis size");	
				}
				release_value($2);
				release_value($4);
			}
	|	basis scalar_array integer string integer
			{
				RtBasis ub = {};
				if($2->size()==16){
					memcpy(ub, &((*$2)[0]), sizeof(RtBasis));
					GET_CTX()->RiBasis(ub, $3, $4, $5);
				}else{
					yyerror("Unexpected basis size");	
				}
				release_value($2);
				release_value($4);
			}
	|	basis scalar_array integer scalar_array integer
			{
				RtBasis ub = {};
				RtBasis vb = {};
				if($2->size()==16 && $4->size()==16){
					memcpy(ub, &((*$2)[0]), sizeof(RtBasis));
					memcpy(vb, &((*$4)[0]), sizeof(RtBasis));
					GET_CTX()->RiBasis(ub, $3, vb, $5);
				}else{
					yyerror("Unexpected basis size");	
				}
				release_value($2);
				release_value($4);	
			}
	|	patch string token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiPatchV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
	|	patchmesh string integer string integer string token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$7);
				GET_CTX()->RiPatchMeshV($2, $3, $4, $5, $6, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($4);
				release_value($6);
				release_value($7);	
			}
	|	nupatch integer integer scalar_array float float integer integer scalar_array float float token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$12);
				GET_CTX()->RiNuPatchV($2, $3, &(*$4)[0], $5, $6, $7, $8, &(*$9)[0], $10, $11, n, &tokens[0], &params[0]);
				release_value($4);
				release_value($9);
				release_value($12);
			}
	|	trimcurve integer_array integer_array scalar_array scalar_array scalar_array integer_array scalar_array scalar_array scalar_array
			{
				GET_CTX()->RiTrimCurve($2->size(), &(*$2)[0], &(*$3)[0], &(*$4)[0], &(*$5)[0], &(*$6)[0], &(*$7)[0], &(*$8)[0], &(*$9)[0], &(*$10)[0]);
				release_value($2);
				release_value($3);
				release_value($4);
				release_value($5);
				release_value($6);
				release_value($7);
				release_value($8);
				release_value($9);
				release_value($10);
			}
	|	sphere float float float float opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$6);
				GET_CTX()->RiSphereV($2, $3, $4, $5, n, &tokens[0], &params[0]);
				release_value($6);
			}
	|	cone float float float opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$5);
				GET_CTX()->RiConeV($2, $3, $4, n, &tokens[0], &params[0]);
				release_value($5);
			}
	|	cylinder float float float float opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$6);
				GET_CTX()->RiCylinderV($2, $3, $4, $5, n, &tokens[0], &params[0]);
				release_value($6);	
			}
	|	hyperboloid scalar_array opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				
				float_array& afv = *$2;
				RtPoint p1 = {afv[0], afv[1], afv[2]};
				RtPoint p2 = {afv[3], afv[4], afv[5]};
				RtFloat tmax = afv[6];
				
				GET_CTX()->RiHyperboloidV(p1, p2, tmax, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
	|	hyperboloid float float float float float float float opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$9);
				
				RtPoint p1 = {$2, $3, $4};
				RtPoint p2 = {$5, $6, $7};
				RtFloat tmax = $8;
				
				GET_CTX()->RiHyperboloidV(p1, p2, tmax, n, &tokens[0], &params[0]);
				
				release_value($9);
			}
	|	paraboloid float float float float opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$6);
				GET_CTX()->RiParaboloidV($2, $3, $4, $5, n, &tokens[0], &params[0]);
				release_value($6);
			}
	|	disk float float float opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$5);
				GET_CTX()->RiDiskV($2, $3, $4, n, &tokens[0], &params[0]);
				release_value($5);	
			}
	|	torus float float float float float opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$7);
				GET_CTX()->RiTorusV($2, $3, $4, $5, $6, n, &tokens[0], &params[0]);
				release_value($7);	
			}
	|	geometry string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiGeometryV($2, n, &tokens[0], &params[0]);
				release_value($2);	
				release_value($3);	
			}
	|	solidbegin string
			{
				GET_CTX()->RiSolidBegin($2);
				release_value($2);	
			}
	|	solidend
			{
				GET_CTX()->RiSolidEnd();	
			}
	|	objectbegin integer
			{
				RtObjectHandle handle = GET_CTX()->RiObjectBegin();	
				SET_OBJECTMAP($2, handle);
			}
	|	objectbegin string
			{
				RtObjectHandle handle = GET_CTX()->RiObjectBegin();
				SET_OBJECTMAP($2, handle);
				release_value($2);	
			}
	|	objectend
			{
				GET_CTX()->RiObjectEnd();	
			}
	|	objectinstance integer
			{
				GET_CTX()->RiObjectInstance($2);
			}
	|	objectinstance string
			{
				GET_CTX()->RiObjectInstance($2);
				release_value($2);
			}
	|	motionbegin scalar_array
			{
				GET_CTX()->RiMotionBeginV($2->size(), &((*$2)[0]));
				release_value($2);	
			}
	|	motionend
			{
				GET_CTX()->RiMotionEnd();	
			}
	|	maketexture	string string string string string float float opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$9);
				
				//RtFilterFunc func = GET_CTX()->RiGetFilterFunc($6);
				GET_CTX()->RiMakeTextureV($2, $3, $4, $5, $6, $7, $8, n, &tokens[0], &params[0]);
				
				release_value($2);
				release_value($3);
				release_value($4);
				release_value($5);
				release_value($6);
				//release_value($7);
				//release_value($8);
				release_value($9);
			}
	|	makebump string string string string string float float opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$9);
				//RtFilterFunc func = GET_CTX()->RiGetFilterFunc($6);
				GET_CTX()->RiMakeBumpV($2, $3, $4, $5, $6, $7, $8, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
				release_value($5);
				release_value($6);
				//release_value($7);
				//release_value($8);
				release_value($9);
			}
	|	makelatlongenvironment string string string float float opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$7);
				//RtFilterFunc func = GET_CTX()->RiGetFilterFunc($4);
				GET_CTX()->RiMakeLatLongEnvironmentV($2, $3, $4, $5, $6, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
				//release_value($5);
				//release_value($6);
				release_value($7);
			}
	|	makecubefaceenvironment	string string string string string string string float string float float opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$13);
				//RtFilterFunc func = GET_CTX()->RiGetFilterFunc($10);
				GET_CTX()->RiMakeCubeFaceEnvironmentV($2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
				release_value($5);
				release_value($6);
				release_value($7);
				release_value($8);
				//release_value($9);
				release_value($10);
				//release_value($11);
				//release_value($12);
				release_value($13);
			}
	|	makeshadow string string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				GET_CTX()->RiMakeShadowV($2, $3, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
			}
	|	makeocclusion string_array string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				GET_CTX()->RiMakeOcclusionV($2->size(), &(*$2)[0], $3, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
			}
	|	makebrickmap string string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				RtToken ptcs[] = {$2};
				GET_CTX()->RiMakeBrickMapV(1, ptcs, $3, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
			}
	|	makebrickmap string_array string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				GET_CTX()->RiMakeBrickMapV($2->size(), &(*$2)[0], $3, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
			}
	|	errorhandler string
			{
				GET_CTX()->RiErrorHandler($2);
				release_value($2);
			}
	|	errorignore
			{;}
	|	errorprint
			{;}
	|	errorabort
			{;}
	|	subdivisionmesh string integer_array integer_array string_array_or_empty integer_array_or_empty integer_array_or_empty scalar_array_or_empty token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$9);
				
				RtToken sDummy = "";
				RtInt iDummy = 0;
				RtFloat fDummy = 0.0f;
				
				RtToken* array5 = &sDummy;
				if($5->size()){
					array5 = &(*$5)[0];
				}
				RtInt* array6 = &iDummy;
				if($6->size()){
					array6 = &(*$6)[0];
				}
				RtInt* array7 = &iDummy;
				if($7->size()){
					array7 = &(*$7)[0];
				}
				RtFloat* array8 = &fDummy;
				if($8->size()){
					array8 = &(*$8)[0];
				}
				
				
				GET_CTX()->RiSubdivisionMeshV($2, $3->size(), &(*$3)[0], &(*$4)[0], $5->size(), array5, array6, array7, array8, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
				release_value($5);
				release_value($6);
				release_value($7);
				release_value($8);
				release_value($9);			
			}
	|	subdivisionmesh string integer_array integer_array token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$5);
				RtToken sDummy = "";
				RtInt iDummy = 0;
				RtFloat fDummy = 0.0f;
				GET_CTX()->RiSubdivisionMeshV($2, $3->size(), &(*$3)[0], &(*$4)[0], 0, &sDummy, &iDummy, &iDummy, &fDummy, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
				release_value($5);	
			}
	|   hierarchicalsubdivisionmesh string integer_array integer_array string_array_or_empty integer_array_or_empty integer_array_or_empty scalar_array_or_empty string_array_or_empty token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$10);
				
				RtToken sDummy = "";
				RtInt iDummy = 0;
				RtFloat fDummy = 0.0f;
				
				RtToken* array5 = &sDummy;
				if($5->size()){
					array5 = &(*$5)[0];
				}
				RtInt* array6 = &iDummy;
				if($6->size()){
					array6 = &(*$6)[0];
				}
				RtInt* array7 = &iDummy;
				if($7->size()){
					array7 = &(*$7)[0];
				}
				RtFloat* array8 = &fDummy;
				if($8->size()){
					array8 = &(*$8)[0];
				}
				RtString* array9 = &sDummy;
				if($9->size()){
					array9 = &(*$9)[0];
				}
				
				GET_CTX()->RiHierarchicalSubdivisionMeshV($2, $3->size(), &(*$3)[0], &(*$4)[0], $5->size(), array5, array6, array7, array8, array9, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
				release_value($5);
				release_value($6);
				release_value($7);
				release_value($8);
				release_value($9);
				release_value($10);
			}
	|	shaderlayer string string string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$5);
				GET_CTX()->RiShaderLayerV($2, $3, $4, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
				release_value($5);
			}
	|	connectshaderlayers string string string string string	
			{
				GET_CTX()->RiConnectShaderLayers($2, $3, $4, $5, $6);
				release_value($2);
				release_value($3);
				release_value($4);
				release_value($5);
				release_value($6);
			}
	|	procedural2 string string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				GET_CTX()->RiProcedural2($2, $3, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
			}
	|	system string
			{
				GET_CTX()->RiSystem($2);
				release_value($2);
			}
	|	volume string scalar_array integer_array
			{
				float_array& afv = *$3;
				RtBound b = {afv[0], afv[1], afv[2], afv[3], afv[4], afv[5]};
				GET_CTX()->RiVolume($2, b, &(*$4)[0]);
				release_value($2);
				release_value($3);
				release_value($4);
			}
	|	vpatmosphere string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiVPAtmosphereV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
	|	vpinterior string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiVPInteriorV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
	|	vpsurface string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$3);
				GET_CTX()->RiVPSurfaceV($2, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
			}
	|	integrator string string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				GET_CTX()->RiIntegratorV($2, $3, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
			}
	|	bxdf string string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				GET_CTX()->RiBxdfV($2, $3, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
			}
	|	pattern string string opt_token_value_array
			{
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*$4);
				GET_CTX()->RiPatternV($2, $3, n, &tokens[0], &params[0]);
				release_value($2);
				release_value($3);
				release_value($4);
			}
	|	UNKNOWN_TOKEN
			{
				;	
			}
	|	error
			{;}
	|	INVALID_VALUE
			{
				// A value has been found when a request is expected, quietly eat it...
			}
	;
	
archivebegin : TOKEN_ARCHIVEBEGIN	{ REQ_PARAM(); };
archiveend : TOKEN_ARCHIVEEND	{ REQ_PARAM(); };
arealightsource : TOKEN_AREALIGHTSOURCE	{ REQ_PARAM(); };
atmosphere : TOKEN_ATMOSPHERE	{ REQ_PARAM(); };
attribute : TOKEN_ATTRIBUTE	{ REQ_PARAM(); };
attributebegin : TOKEN_ATTRIBUTEBEGIN	{ REQ_PARAM(); };
attributeend : TOKEN_ATTRIBUTEEND	{ REQ_PARAM(); };
basis : TOKEN_BASIS	{ REQ_PARAM(); };
blobby : TOKEN_BLOBBY	{ REQ_PARAM(); };
bound : TOKEN_BOUND	{ REQ_PARAM(); };
clipping : TOKEN_CLIPPING	{ REQ_PARAM(); };
color : TOKEN_COLOR	{ REQ_PARAM(); };
colorsamples : TOKEN_COLORSAMPLES	{ REQ_PARAM(); };
concattransform : TOKEN_CONCATTRANSFORM	{ REQ_PARAM(); };
cone : TOKEN_CONE	{ REQ_PARAM(); };
coordinatesystem : TOKEN_COORDINATESYSTEM	{ REQ_PARAM(); };
coordsystransform : TOKEN_COORDSYSTRANSFORM	{ REQ_PARAM(); };
cropwindow : TOKEN_CROPWINDOW	{ REQ_PARAM(); };
cylinder : TOKEN_CYLINDER	{ REQ_PARAM(); };
declare : TOKEN_DECLARE	{ REQ_PARAM(); };
deformation : TOKEN_DEFORMATION	{ REQ_PARAM(); };
depthoffield : TOKEN_DEPTHOFFIELD	{ REQ_PARAM(); };
detail : TOKEN_DETAIL	{ REQ_PARAM(); };
detailrange : TOKEN_DETAILRANGE	{ REQ_PARAM(); };
disk : TOKEN_DISK	{ REQ_PARAM(); };
displacement : TOKEN_DISPLACEMENT	{ REQ_PARAM(); };
displaychannel : TOKEN_DISPLAYCHANNEL	{ REQ_PARAM(); };
display : TOKEN_DISPLAY	{ REQ_PARAM(); };
else   : TOKEN_ELSE	{  REQ_PARAM();};
elseif : TOKEN_ELSEIF	{  REQ_PARAM();};
errorabort : TOKEN_ERRORABORT	{ REQ_PARAM(); };
errorhandler : TOKEN_ERRORHANDLER	{ REQ_PARAM(); };
errorignore : TOKEN_ERRORIGNORE	{ REQ_PARAM(); };
errorprint : TOKEN_ERRORPRINT	{ REQ_PARAM(); };
exposure : TOKEN_EXPOSURE	{ REQ_PARAM(); };
exterior : TOKEN_EXTERIOR	{ REQ_PARAM(); };
format : TOKEN_FORMAT	{ REQ_PARAM(); };
frameaspectratio : TOKEN_FRAMEASPECTRATIO	{ REQ_PARAM(); };
framebegin : TOKEN_FRAMEBEGIN	{ REQ_PARAM(); };
frameend : TOKEN_FRAMEEND	{ REQ_PARAM(); };
generalpolygon : TOKEN_GENERALPOLYGON	{ REQ_PARAM(); };
geometricapproximation : TOKEN_GEOMETRICAPPROXIMATION	{ REQ_PARAM(); };
geometry : TOKEN_GEOMETRY	{ REQ_PARAM(); };
hierarchicalsubdivisionmesh : TOKEN_HIERARCHICALSUBDIVISIONMESH	{ REQ_PARAM(); };
hider : TOKEN_HIDER	{ REQ_PARAM(); };
hyperboloid : TOKEN_HYPERBOLOID	{ REQ_PARAM(); };
identity : TOKEN_IDENTITY	{ REQ_PARAM(); };
ifbegin : TOKEN_IFBEGIN	{ REQ_PARAM(); };
ifend : TOKEN_IFEND	{ REQ_PARAM(); };
illuminate : TOKEN_ILLUMINATE	{ REQ_PARAM(); };
imager : TOKEN_IMAGER	{ REQ_PARAM(); };
interior : TOKEN_INTERIOR	{ REQ_PARAM(); };
lightsource : TOKEN_LIGHTSOURCE	{ REQ_PARAM(); };
makeocclusion : TOKEN_MAKEOCCLUSION	{ REQ_PARAM(); };
makebrickmap : TOKEN_MAKEBRICKMAP	{ REQ_PARAM(); };
makebump : TOKEN_MAKEBUMP	{ REQ_PARAM(); };
makecubefaceenvironment : TOKEN_MAKECUBEFACEENVIRONMENT	{ REQ_PARAM(); };
makelatlongenvironment : TOKEN_MAKELATLONGENVIRONMENT	{ REQ_PARAM(); };
makeshadow : TOKEN_MAKESHADOW	{ REQ_PARAM(); };
maketexture : TOKEN_MAKETEXTURE	{ REQ_PARAM(); };
matte : TOKEN_MATTE	{ REQ_PARAM(); };
motionbegin : TOKEN_MOTIONBEGIN	{ REQ_PARAM(); };
motionend : TOKEN_MOTIONEND	{ REQ_PARAM(); };
nupatch : TOKEN_NUPATCH	{ REQ_PARAM(); };
objectbegin : TOKEN_OBJECTBEGIN	{ REQ_PARAM(); };
objectend : TOKEN_OBJECTEND	{ REQ_PARAM(); };
objectinstance : TOKEN_OBJECTINSTANCE	{ REQ_PARAM(); };
opacity : TOKEN_OPACITY	{ REQ_PARAM(); };
option : TOKEN_OPTION	{ REQ_PARAM(); };
orientation : TOKEN_ORIENTATION	{ REQ_PARAM(); };
paraboloid : TOKEN_PARABOLOID	{ REQ_PARAM(); };
patch : TOKEN_PATCH	{ REQ_PARAM(); };
patchmesh : TOKEN_PATCHMESH	{ REQ_PARAM(); };
perspective : TOKEN_PERSPECTIVE	{ REQ_PARAM(); };
pixelfilter : TOKEN_PIXELFILTER	{ REQ_PARAM(); };
pixelsamples : TOKEN_PIXELSAMPLES	{ REQ_PARAM(); };
pixelvariance : TOKEN_PIXELVARIANCE	{ REQ_PARAM(); };
curves: TOKEN_CURVES	{ REQ_PARAM(); };
procedural: TOKEN_PROCEDURAL	{ REQ_PARAM(); };
procedure: TOKEN_PROCEDURE { REQ_PARAM(); };
points: TOKEN_POINTS	{ REQ_PARAM(); };
pointsgeneralpolygons : TOKEN_POINTSGENERALPOLYGONS	{ REQ_PARAM(); };
pointspolygons : TOKEN_POINTSPOLYGONS	{ REQ_PARAM(); };
polygon : TOKEN_POLYGON	{ REQ_PARAM(); };
projection : TOKEN_PROJECTION	{ REQ_PARAM(); };
quantize : TOKEN_QUANTIZE	{ REQ_PARAM(); };
readarchive: TOKEN_READARCHIVE	{ REQ_PARAM(); };
relativedetail : TOKEN_RELATIVEDETAIL	{ REQ_PARAM(); };
resource : TOKEN_RESOURCE	{ REQ_PARAM(); };
resourcebegin : TOKEN_RESOURCEBEGIN	{ REQ_PARAM(); };
resourceend : TOKEN_RESOURCEEND	{ REQ_PARAM(); };
reverseorientation : TOKEN_REVERSEORIENTATION	{ REQ_PARAM(); };
rotate : TOKEN_ROTATE	{ REQ_PARAM(); };
scale : TOKEN_SCALE	{ REQ_PARAM(); };
scopedcoordinatesystem : TOKEN_SCOPEDCOORDINATESYSTEM	{ REQ_PARAM(); };
screenwindow : TOKEN_SCREENWINDOW	{ REQ_PARAM(); };
shadinginterpolation : TOKEN_SHADINGINTERPOLATION	{ REQ_PARAM(); };
shadingrate : TOKEN_SHADINGRATE	{ REQ_PARAM(); };
shutter : TOKEN_SHUTTER	{ REQ_PARAM(); };
sides : TOKEN_SIDES	{ REQ_PARAM(); };
skew : TOKEN_SKEW	{ REQ_PARAM(); };
solidbegin : TOKEN_SOLIDBEGIN	{ REQ_PARAM(); };
solidend : TOKEN_SOLIDEND	{ REQ_PARAM(); };
sphere : TOKEN_SPHERE	{ REQ_PARAM(); };
subdivisionmesh : TOKEN_SUBDIVISIONMESH	{ REQ_PARAM(); };
surface : TOKEN_SURFACE	{ REQ_PARAM(); };
texturecoordinates : TOKEN_TEXTURECOORDINATES	{ REQ_PARAM(); };
torus : TOKEN_TORUS	{ REQ_PARAM(); };
transform : TOKEN_TRANSFORM	{ REQ_PARAM(); };
transformbegin : TOKEN_TRANSFORMBEGIN	{ REQ_PARAM(); };
transformend : TOKEN_TRANSFORMEND	{ REQ_PARAM(); };
transformpoints : TOKEN_TRANSFORMPOINTS	{ REQ_PARAM(); };
translate : TOKEN_TRANSLATE	{ REQ_PARAM(); };
trimcurve : TOKEN_TRIMCURVE	{ REQ_PARAM(); };
version : TOKEN_VERSION	{ REQ_PARAM(); };
worldbegin : TOKEN_WORLDBEGIN	{ REQ_PARAM(); };
worldend : TOKEN_WORLDEND	{ REQ_PARAM(); };
shaderlayer : TOKEN_SHADERLAYER	{ REQ_PARAM(); };
connectshaderlayers : TOKEN_CONNECTSHADERLAYERS	{ REQ_PARAM(); };
procedural2 : TOKEN_PROCEDURAL2 { REQ_PARAM(); }
system : TOKEN_SYSTEM { REQ_PARAM(); }
volume : TOKEN_VOLUME { REQ_PARAM(); }
vpatmosphere : TOKEN_VPATMOSPHERE { REQ_PARAM(); }
vpinterior : TOKEN_VPINTERIOR { REQ_PARAM(); }
vpsurface : TOKEN_VPSURFACE { REQ_PARAM(); }
integrator : TOKEN_INTEGRATOR { REQ_PARAM(); }
bxdf : TOKEN_BXDF { REQ_PARAM(); }
pattern : TOKEN_PATTERN { REQ_PARAM(); }

arraystart : '['
			{}
	|	arraystart COMMENT
			{
				release_value($2);
			}
	;
			
arrayend : ']'
			{}
	|	arrayend COMMENT
			{
				release_value($2);
			}
	;

integer : INTEGER_TOKEN
			{
				$$ = $1;
			}
	|	integer COMMENT
			{
				$$ = $1;
				release_value($2);
			}
	;

integers : INTEGER_TOKEN
			{
				$$ = new integer_array();
				$$->push_back($1);
			}
	|	integers integer
			{
				$$ = $1;
				$$->push_back($2);
			}
	;

integer_array : arraystart integers arrayend
			{
				$$ = $2;
			}
	| integer_array COMMENT
			{
				$$ = $1;
				release_value($2);
			}
	;

integer_array_or_empty : integer_array
	|	arraystart arrayend
			{
				$$ = new integer_array();
			}
	;

float : FLOAT_TOKEN
			{
				$$ = $1;
			}
	|	integer
			{ 
				$$ = static_cast<float>($1); 
			}
	|	float COMMENT
			{
				$$ = $1;
				release_value($2);
			}
	;
	;

floats :	FLOAT_TOKEN
			{
				$$ = new float_array();
				$$->push_back($1);
			}
	|	floats FLOAT_TOKEN
			{
				$$ = $1;
				$$->push_back($2);
			}
	|	floats INTEGER_TOKEN
			{
				$$ = $1;
				$$->push_back(static_cast<float>($2));
			}
	|	integers FLOAT_TOKEN
			{
				$$ = new float_array(*$1);
				$$->push_back($2);
				release_value($1);
			}
	;

float_array : arraystart floats arrayend
			{
				$$ = $2;
			}
	| arraystart integers arrayend
			{
				$$ = new float_array(*$2);
				release_value($2);
			}
	| float_array COMMENT
			{
				$$ = $1;
				release_value($2);
			}
	;
/*
float_array_or_empty : float_array
	|	arraystart arrayend
			{
				$$ = new float_array();
			}
	;
*/	
	
string : STRING_TOKEN
			{
				$$ = $1;
			}
	|	string COMMENT
			{
				$$ = $1;
				release_value($2);
			}
	;
	
version_string : STRING_TOKEN
			{
				$$ = $1;
			}
	|	version_string COMMENT
			{
				$$ = $1;
				release_value($2);
			}
	;

strings : STRING_TOKEN
			{
				$$ = new string_array();
				$$->push_back($1);
			}
	|	strings STRING_TOKEN
			{
				$$ = $1;
				$$->push_back($2);
			}
	;

string_array : arraystart strings arrayend
			{
				$$ = $2;
			}
	|	string_array COMMENT
			{
				$$ = $1;
				release_value($2);
			}
	;

string_array_or_empty : string_array
	|	arraystart arrayend
			{
				$$ = new string_array();
			}
	;

scalar_array : float_array
	|	integer_array
			{
				$$ = new float_array(*$1);
				release_value($1);
			}
	;

scalar_array_or_empty : scalar_array
	|	arraystart arrayend
			{
				$$ = new float_array();
			}
	;
	
token_value : string INTEGER_TOKEN
			{
				ri_temporary_classifier cls(GET_CLS());
				int id = cls.def($1);
				if(id>=0){
					int nType = cls[id].get_type();
					switch(nType){
					case ri_classifier::INTEGER:
						$$ = new token_value($1, $2);
						break;
					case ri_classifier::FLOAT:
					case ri_classifier::POINT:
					case ri_classifier::COLOR:
					case ri_classifier::HPOINT:
					case ri_classifier::NORMAL:
					case ri_classifier::VECTOR:
					case ri_classifier::MATRIX:
						$$ = new token_value($1, (float)$2);
						break;
					default:
						yyerror("mispatch parameter");
						break;
					}
				}else{
					$$ = new token_value($1, $2);
				}
				release_value($1);
			}
	|	string integer_array
			{
				ri_temporary_classifier cls(GET_CLS());
				int id = cls.def($1);
				if(id>=0){
					int nType = cls[id].get_type();
					switch(nType){
						case ri_classifier::INTEGER:
							$$ = new token_value($1, $2);
							break;
						case ri_classifier::FLOAT:
						case ri_classifier::POINT:
						case ri_classifier::COLOR:
						case ri_classifier::HPOINT:
						case ri_classifier::NORMAL:
						case ri_classifier::VECTOR:
						case ri_classifier::MATRIX:
							$$ = new token_value($1, new float_array(*$2));
							release_value($2);
							break;
						default:
							release_value($2);
							yyerror("mispatch parameter5");
					}
				}else{
					release_value($2);
					yyerror($1);
					yyerror("mispatch parameter6");
				}
				release_value($1);
			}
	|	string FLOAT_TOKEN
			{
				ri_temporary_classifier cls(GET_CLS());
				int id = cls.def($1);
				if(id>=0){
					$$ = new token_value($1, $2);
				}else{
					yyerror("mispatch parameter1");
				}
				release_value($1);
			}
	|	string float_array
			{
				ri_temporary_classifier cls(GET_CLS());
				int id = cls.def($1);
				if(id>=0){
					$$ = new token_value($1, $2);
				}else{
					release_value($2);
					yyerror("mispatch parameter2");
				}
				release_value($1);
			}
	|	string STRING_TOKEN
			{
				ri_temporary_classifier cls(GET_CLS());
				int id = cls.def($1);
				if(id>=0){
					$$ = new token_value($1, $2);
				}else{
					release_value($2);
					yyerror("mispatch parameter3");
				}
				release_value($1);
			}
	|	string string_array
			{
				ri_temporary_classifier cls(GET_CLS());
				int id = cls.def($1);
				if(id>=0){
					$$ = new token_value($1, $2);
				}else{
					release_value($2);
					yyerror("mispatch parameter4");
				}
				release_value($1);
			}
	;

token_value_array
	:	token_value
			{
				$$ = new token_value_array();
				$$->push_back($1);
			}
	|	error token_value
			{
				$$ = new token_value_array();
				$$->push_back($2);
			}
	|	token_value_array token_value
			{
				$$ = $1;
				$$->push_back($2);
			}
	|	token_value_array error
	;
	
opt_token_value_array : /* Empty */
			{
				$$ = new token_value_array();
			}
	|	error
			{
				$$ = new token_value_array();
			}
	|	token_value_array
	;
	
%%

void yy::rib_parser::error(const yy::rib_parser::location_type&, const std::string& m)
{
	fprintf(stderr, "parser error %s\n", m.c_str());
}

extern int yylex_init(void** p);
extern int yylex_destroy(void* p);


namespace kaze{
namespace ri{
	int load_rib(FILE* in, ri_context* ctx)
	{	
		ri_rib_decoder dec(in);

		void* scanner;
		yylex_init(&scanner);
		
		ri_rib_parse_param* param = (ri_rib_parse_param*)(scanner);
		param->ctx = ctx;
		param->fRequest = false;
		param->fParams = false;
		param->fSkipFrame = false;
		param->line_number = 1;
		param->dec = &dec;

		yy::rib_parser parser(scanner);

		int nRet = parser.parse();

		yylex_destroy(scanner);

		return nRet;
	}
}
}
