#ifndef RI_MAKE_TEXTURE_TASK_H
#define RI_MAKE_TEXTURE_TASK_H

#include "ri.h"
#include "ri_object.h"
#include "ri_task.h"
#include "ri_classifier.h"
#include "ri_parameters.h"
#include "ri_options.h"

namespace ri
{
    class ri_make_texture_task : public ri_task
    {
    public:
        ri_make_texture_task(const ri_classifier* p_cls, const ri_options& opts,
                             char* pic, char* tex, RtToken swrap, RtToken twrap,
                             RtToken filterfunc, RtFloat swidth, RtFloat twidth,
                             RtInt n, RtToken tokens[], RtPointer params[]);

    public:
        std::string type() const { return "MakeTexture"; }
        int run();

    protected:
        ri_classifier clsf_;
        ri_parameters params_;
    };

    class ri_make_lat_long_environment_task : public ri_task
    {
    public:
        ri_make_lat_long_environment_task(const ri_classifier* p_cls,
                                          const ri_options& opts, char* pic,
                                          char* tex, RtToken filterfunc,
                                          RtFloat swidth, RtFloat twidth, RtInt n,
                                          RtToken tokens[], RtPointer params[]);

    public:
        std::string type() const { return "MakeLatLongEnvironment"; }
        int run();

    protected:
        ri_classifier clsf_;
        ri_parameters params_;
    };

    class ri_make_cube_face_environment_task : public ri_task
    {
    public:
        ri_make_cube_face_environment_task(const ri_classifier* p_cls,
                                           const ri_options& opts, char* px, char* nx,
                                           char* py, char* ny, char* pz, char* nz,
                                           char* tex, RtFloat fov, RtToken filterfunc,
                                           RtFloat swidth, RtFloat twidth, RtInt n,
                                           RtToken tokens[], RtPointer params[]);

    public:
        std::string type() const { return "MakeCubeFaceEnvironment"; }
        int run();

    protected:
        ri_classifier clsf_;
        ri_parameters params_;
    };

    class ri_make_shadow_task : public ri_task
    {
    public:
        ri_make_shadow_task(const ri_classifier* p_cls, const ri_options& opts,
                            const char* pic, const char* tex, RtInt n,
                            RtToken tokens[], RtPointer params[]);

    public:
        std::string type() const { return "MakeShadow"; }
        int run();

    protected:
        ri_classifier clsf_;
        ri_parameters params_;
    };

    class ri_make_brick_map_task : public ri_task
    {
    public:
        ri_make_brick_map_task(const ri_classifier* p_cls, const ri_options& opts,
                               int nptc, char** ptcnames, char* bkmname, RtInt n,
                               RtToken tokens[], RtPointer params[]);

    public:
        std::string type() const { return "MakeBrickMap"; }
        int run();

    protected:
        ri_classifier clsf_;
        ri_parameters params_;
    };
}
#endif
