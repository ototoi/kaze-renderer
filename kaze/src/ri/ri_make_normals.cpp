#define _USE_MATH_DEFINES
#include <math.h>

#include "ri_make_normals.h"
#include "ri_types.h"

namespace ri
{
    namespace
    {
        static int SumArray(const RtInt ar[], int n)
        {
            int nRet = 0;
            for (int i = 0; i < n; i++)
            {
                nRet += ar[i];
            }
            return nRet;
        }

        struct fce
        {
            int vertex_count;
            size_t vert[4];
            size_t next_face[4];
            vector3f normal;

            vector3f nf[4];
        };

        struct vtx
        {
            vector3f p;
            size_t face;
        };

        static const size_t EMPTY_MASK = (size_t)-1;

        bool calc_normals_facevarying(std::vector<fce>& fl, std::vector<vtx>& vl,
                                      float angle)
        {
            float angle_ = angle / 180.0f * (float)M_PI;
            float limit_cos = std::cos(angle_);

            size_t fsz = fl.size();
            size_t vsz = vl.size();

            for (size_t i = 0; i < fsz; i++)
            {
                fl[i].nf[0] = fl[i].normal;
                fl[i].nf[1] = fl[i].normal;
                fl[i].nf[2] = fl[i].normal;
                fl[i].nf[3] = fl[i].normal;
            }

            for (size_t i = 0; i < fsz; i++)
            {
                for (int j = 0; j < fl[i].vertex_count; j++)
                {
                    size_t vidx = fl[i].vert[j];

                    if (vl[vidx].face == EMPTY_MASK)
                    {
                        vl[vidx].face = i;
                    }
                    else
                    {
                        size_t fidx = vl[vidx].face;

                        int k = 0;
                        while (1)
                        {
                            bool bFind = false;
                            for (k = 0; k < fl[fidx].vertex_count; k++)
                            {
                                if (fl[fidx].vert[k] == vidx)
                                {
                                    bFind = true;
                                    break;
                                }
                            }
                            assert(bFind);
                            if (fl[fidx].next_face[k] == EMPTY_MASK)
                                break;
                            fidx = fl[fidx].next_face[k];
                        }

                        fl[fidx].next_face[k] = i;
                    }
                }
            }

            std::vector<size_t> vface_indices;
            for (size_t i = 0; i < fsz; i++)
            {
                vector3f iN = fl[i].normal;
                bool bLeng = false;
                if (iN.sqr_length() >= 0)
                {
                    iN = normalize(iN);
                    bLeng = true;
                }
                for (int j = 0; j < fl[i].vertex_count; j++)
                {
                    size_t vidx = fl[i].vert[j];

                    vface_indices.clear();
                    if (vl[vidx].face != EMPTY_MASK)
                    {
                        size_t fidx = vl[vidx].face;

                        int k = 0;
                        while (1)
                        {
                            vface_indices.push_back(fidx);

                            bool bFind = false;
                            for (k = 0; k < fl[fidx].vertex_count; k++)
                            {
                                if (fl[fidx].vert[k] == vidx)
                                {
                                    bFind = true;
                                    break;
                                }
                            }
                            if (!bFind)
                                break;
                            if (fl[fidx].next_face[k] == EMPTY_MASK)
                                break;
                            fidx = fl[fidx].next_face[k];
                        };
                    }

                    for (size_t l = 0; l < vface_indices.size(); l++)
                    {
                        if (vface_indices[l] != i)
                        {
                            vector3f lN = fl[vface_indices[l]].normal;
                            if (bLeng)
                            {
                                float d = dot(iN, normalize(lN));
                                if (d < 0)
                                {
                                    iN *= -1;
                                    d *= -1;
                                }
                                if (d > limit_cos)
                                {
                                    fl[i].nf[j] += lN;
                                }
                            }
                            else
                            {
                                fl[i].nf[j] += lN;
                            }
                        }
                    }
                }
            }

            for (size_t i = 0; i < fsz; i++)
            {
                for (int j = 0; j < fl[i].vertex_count; j++)
                {
                    if (fl[i].nf[j].sqr_length() <= 0)
                        continue;
                    fl[i].nf[j] = normalize(fl[i].nf[j]);
                }
            }

            return true;
        }
    }

    // for RiPointsPorygons
    int ri_make_normals(std::vector<float>& N, int npolys, const int nverts[],
                        const int verts[], const float P[], float angle)
    {
        static const int WRAP4[] = {0, 1, 2, 3, 0, 1, 2, 3};

        int nPts = SumArray(nverts, npolys);
        int psz = 0;
        for (int i = 0; i < nPts; i++)
        {
            if (psz < verts[i])
            {
                psz = verts[i];
            }
        }

        size_t fsz = npolys;
        size_t vsz = psz + 1;

        std::vector<vtx> vl(vsz);
        for (size_t i = 0; i < vsz; i++)
        {
            vl[i].face = EMPTY_MASK;
            vl[i].p = vector3f(P + 3 * i);
        }

        std::vector<fce> fl(fsz);
        size_t findex = 0;
        for (size_t i = 0; i < fsz; i++)
        {
            int vertex_count = nverts[i];
            if (vertex_count != 3 && vertex_count != 4)
                return -1;
            fl[i].vertex_count = vertex_count;
            for (int j = 0; j < vertex_count; j++)
            {
                fl[i].vert[j] = verts[findex + j];
                fl[i].next_face[j] = EMPTY_MASK;
            }

            if (vertex_count == 3)
            {
                vector3f p0 = vl[fl[i].vert[0]].p;
                vector3f p1 = vl[fl[i].vert[1]].p;
                vector3f p2 = vl[fl[i].vert[2]].p;

                vector3f e1 = p1 - p0;
                vector3f e2 = p2 - p0;
                fl[i].normal = cross(e1, e2);
            }
            else
            {
                vector3f n(0, 0, 0);
                for (int j = 0; j < vertex_count; j++)
                {
                    vector3f p0 = vl[fl[i].vert[WRAP4[j + 0]]].p;
                    vector3f p1 = vl[fl[i].vert[WRAP4[j + 1]]].p;
                    vector3f p2 = vl[fl[i].vert[WRAP4[j + 2]]].p;

                    vector3f e1 = p1 - p0;
                    vector3f e2 = p2 - p0;
                    n += cross(e1, e2);
                }
                fl[i].normal = n * ((vertex_count - 2.0f) / vertex_count);
            }
            findex += vertex_count;
        }

        size_t total_vertex = findex;

        if (!calc_normals_facevarying(fl, vl, angle))
            return -1;

        N.resize(total_vertex * 3);
        findex = 0;
        for (size_t i = 0; i < fsz; i++)
        {
            int vertex_count = fl[i].vertex_count;
            for (int j = 0; j < vertex_count; j++)
            {
                vector3f n = fl[i].nf[j];
                size_t idx = findex + j;
                N[3 * idx + 0] = n[0];
                N[3 * idx + 1] = n[1];
                N[3 * idx + 2] = n[2];
            }
            findex += vertex_count;
        }
        return 0;
    }
}
