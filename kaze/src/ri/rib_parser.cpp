/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison LALR(1) parsers in C++

   Copyright (C) 2002, 2003, 2004, 2005, 2006 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


#include "rib_parser.hpp"

/* User implementation prologue.  */
#line 53 "rib_parser.yy"


#define yyerror(m) error(yylloc,m)

using namespace ri;

#define GET_CTX() (((ri_rib_parse_param*)(scanner))->ctx)
#define GET_CLS() (GET_CTX()->RiGetClassifier())
#define REQ_PARAM() {((ri_rib_parse_param*)(scanner))->fParams=true;}
#define SET_LIGHTMAP(Key, Handle) (GET_CTX()->RiSetLightHandle(Key,Handle));
#define SET_OBJECTMAP(Key, Handle) (GET_CTX()->RiSetObjectHandle(Key,Handle));

static
int expand_token_value(std::vector<RtToken>& tokens, std::vector<void*>& params, const token_value_array& atv){
	int n = (int)atv.size();
	if(n){
		tokens.resize(n);
		params.resize(n);
	}else{
		tokens.resize(1);
		params.resize(1);
	}
	for(int i = 0;i<n;i++){
		tokens[i] = (char*)(atv[i]->first.c_str());
	    params[i] = (void*)(atv[i]->second->get_ptr());
    }
    return n;
}

static
void release_value(const char* v){delete[] v;}
static
void release_value(integer_array* v){delete v;}
static
void release_value(float_array* v){delete v;}
static
void release_value(string_array* v){delete v;}
//static
//void release_value(token_value* v){delete v;}
static
void release_value(token_value_array* v){delete v;}

static
void convet_to_RtMatrix(float_array* v, RtMatrix m){
	float_array& rv = *v;
	for(int j=0;j<4;j++){
		for(int i=0;i<4;i++){
			m[j][i] = (RtFloat)rv[4*j+i];
		}
	}
}

static
int FindParam(RtToken name, const RtToken tokens[], int n)
{
	for(int i=0;i<n;i++){
		if(strcmp(name, tokens[i])==0)return i;
	}
	return -1;
}

#define	YY_DECL											\
	int						\
	yylex(yy::rib_parser::semantic_type* yylval,		\
		 yy::rib_parser::location_type* yylloc,		\
		 void* scanner)

YY_DECL;



/* Line 317 of lalr1.cc.  */
#line 113 "rib_parser.cpp"

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* FIXME: INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#define YYUSE(e) ((void) (e))

/* A pseudo ostream that takes yydebug_ into account.  */
# define YYCDEBUG							\
  for (bool yydebugcond_ = yydebug_; yydebugcond_; yydebugcond_ = false)	\
    (*yycdebug_)

/* Enable debugging if requested.  */
#if YYDEBUG

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)	\
do {							\
  if (yydebug_)						\
    {							\
      *yycdebug_ << Title << ' ';			\
      yy_symbol_print_ ((Type), (Value), (Location));	\
      *yycdebug_ << std::endl;				\
    }							\
} while (false)

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug_)				\
    yy_reduce_print_ (Rule);		\
} while (false)

# define YY_STACK_PRINT()		\
do {					\
  if (yydebug_)				\
    yystack_print_ ();			\
} while (false)

#else /* !YYDEBUG */

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_REDUCE_PRINT(Rule)
# define YY_STACK_PRINT()

#endif /* !YYDEBUG */

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab

namespace yy
{
#if YYERROR_VERBOSE

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
  rib_parser::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr = "";
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              /* Fall through.  */
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }

#endif

  /// Build a parser object.
  rib_parser::rib_parser (void * scanner_yyarg)
    : yydebug_ (false),
      yycdebug_ (&std::cerr),
      scanner (scanner_yyarg)
  {
  }

  rib_parser::~rib_parser ()
  {
  }

#if YYDEBUG
  /*--------------------------------.
  | Print this symbol on YYOUTPUT.  |
  `--------------------------------*/

  inline void
  rib_parser::yy_symbol_value_print_ (int yytype,
			   const semantic_type* yyvaluep, const location_type* yylocationp)
  {
    YYUSE (yylocationp);
    YYUSE (yyvaluep);
    switch (yytype)
      {
         default:
	  break;
      }
  }


  void
  rib_parser::yy_symbol_print_ (int yytype,
			   const semantic_type* yyvaluep, const location_type* yylocationp)
  {
    *yycdebug_ << (yytype < yyntokens_ ? "token" : "nterm")
	       << ' ' << yytname_[yytype] << " ("
	       << *yylocationp << ": ";
    yy_symbol_value_print_ (yytype, yyvaluep, yylocationp);
    *yycdebug_ << ')';
  }
#endif /* ! YYDEBUG */

  void
  rib_parser::yydestruct_ (const char* yymsg,
			   int yytype, semantic_type* yyvaluep, location_type* yylocationp)
  {
    YYUSE (yylocationp);
    YYUSE (yymsg);
    YYUSE (yyvaluep);

    YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

    switch (yytype)
      {
  
	default:
	  break;
      }
  }

  void
  rib_parser::yypop_ (unsigned int n)
  {
    yystate_stack_.pop (n);
    yysemantic_stack_.pop (n);
    yylocation_stack_.pop (n);
  }

  std::ostream&
  rib_parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  rib_parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  rib_parser::debug_level_type
  rib_parser::debug_level () const
  {
    return yydebug_;
  }

  void
  rib_parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }


  int
  rib_parser::parse ()
  {
    /// Look-ahead and look-ahead in internal form.
    int yychar = yyempty_;
    int yytoken = 0;

    /* State.  */
    int yyn;
    int yylen = 0;
    int yystate = 0;

    /* Error handling.  */
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// Semantic value of the look-ahead.
    semantic_type yylval;
    /// Location of the look-ahead.
    location_type yylloc;
    /// The locations where the error started and ended.
    location yyerror_range[2];

    /// $$.
    semantic_type yyval;
    /// @$.
    location_type yyloc;

    int yyresult;

    YYCDEBUG << "Starting parse" << std::endl;


    /* Initialize the stacks.  The initial state will be pushed in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystate_stack_ = state_stack_type (0);
    yysemantic_stack_ = semantic_stack_type (0);
    yylocation_stack_ = location_stack_type (0);
    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yylloc);

    /* New state.  */
  yynewstate:
    yystate_stack_.push (yystate);
    YYCDEBUG << "Entering state " << yystate << std::endl;
    goto yybackup;

    /* Backup.  */
  yybackup:

    /* Try to take a decision without look-ahead.  */
    yyn = yypact_[yystate];
    if (yyn == yypact_ninf_)
      goto yydefault;

    /* Read a look-ahead token.  */
    if (yychar == yyempty_)
      {
	YYCDEBUG << "Reading a token: ";
	yychar = yylex (&yylval, &yylloc, scanner);
      }


    /* Convert token to internal form.  */
    if (yychar <= yyeof_)
      {
	yychar = yytoken = yyeof_;
	YYCDEBUG << "Now at end of input." << std::endl;
      }
    else
      {
	yytoken = yytranslate_ (yychar);
	YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
      }

    /* If the proper action on seeing token YYTOKEN is to reduce or to
       detect an error, take that action.  */
    yyn += yytoken;
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yytoken)
      goto yydefault;

    /* Reduce or error.  */
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
	if (yyn == 0 || yyn == yytable_ninf_)
	goto yyerrlab;
	yyn = -yyn;
	goto yyreduce;
      }

    /* Accept?  */
    if (yyn == yyfinal_)
      goto yyacceptlab;

    /* Shift the look-ahead token.  */
    YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

    /* Discard the token being shifted unless it is eof.  */
    if (yychar != yyeof_)
      yychar = yyempty_;

    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yylloc);

    /* Count tokens shifted since error; after three, turn off error
       status.  */
    if (yyerrstatus_)
      --yyerrstatus_;

    yystate = yyn;
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystate];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    /* If YYLEN is nonzero, implement the default value of the action:
       `$$ = $1'.  Otherwise, use the top of the stack.

       Otherwise, the following line sets YYVAL to garbage.
       This behavior is undocumented and Bison
       users should not rely upon it.  */
    if (yylen)
      yyval = yysemantic_stack_[yylen - 1];
    else
      yyval = yysemantic_stack_[0];

    {
      slice<location_type, location_stack_type> slice (yylocation_stack_, yylen);
      YYLLOC_DEFAULT (yyloc, slice, yylen);
    }
    YY_REDUCE_PRINT (yyn);
    switch (yyn)
      {
	  case 2:
#line 432 "rib_parser.yy"
    { ; ;}
    break;

  case 3:
#line 434 "rib_parser.yy"
    { ; ;}
    break;

  case 6:
#line 442 "rib_parser.yy"
    {;;}
    break;

  case 7:
#line 444 "rib_parser.yy"
    {
				release_value((yysemantic_stack_[(1) - (1)].stype));
			;}
    break;

  case 8:
#line 451 "rib_parser.yy"
    {
				GET_CTX()->RiVersion((yysemantic_stack_[(2) - (2)].stype));
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 9:
#line 456 "rib_parser.yy"
    {
				GET_CTX()->RiVersion((yysemantic_stack_[(2) - (2)].ftype));
			;}
    break;

  case 10:
#line 460 "rib_parser.yy"
    {
				GET_CTX()->RiDeclare((yysemantic_stack_[(3) - (2)].stype), (yysemantic_stack_[(3) - (3)].stype));
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].stype));
			;}
    break;

  case 11:
#line 466 "rib_parser.yy"
    {
				GET_CTX()->RiFrameBegin((yysemantic_stack_[(2) - (2)].itype));
			;}
    break;

  case 12:
#line 470 "rib_parser.yy"
    {
				GET_CTX()->RiFrameEnd();
			;}
    break;

  case 13:
#line 474 "rib_parser.yy"
    {
				GET_CTX()->RiWorldBegin();
			;}
    break;

  case 14:
#line 478 "rib_parser.yy"
    {
				GET_CTX()->RiWorldEnd();
			;}
    break;

  case 15:
#line 482 "rib_parser.yy"
    {
				GET_CTX()->RiFormat((yysemantic_stack_[(4) - (2)].itype), (yysemantic_stack_[(4) - (3)].itype), (yysemantic_stack_[(4) - (4)].ftype));
			;}
    break;

  case 16:
#line 486 "rib_parser.yy"
    {
				GET_CTX()->RiFrameAspectRatio((yysemantic_stack_[(2) - (2)].ftype));
			;}
    break;

  case 17:
#line 490 "rib_parser.yy"
    {
				GET_CTX()->RiScreenWindow((yysemantic_stack_[(5) - (2)].ftype), (yysemantic_stack_[(5) - (3)].ftype), (yysemantic_stack_[(5) - (4)].ftype), (yysemantic_stack_[(5) - (5)].ftype));
			;}
    break;

  case 18:
#line 494 "rib_parser.yy"
    {
				GET_CTX()->RiCropWindow((yysemantic_stack_[(5) - (2)].ftype), (yysemantic_stack_[(5) - (3)].ftype), (yysemantic_stack_[(5) - (4)].ftype), (yysemantic_stack_[(5) - (5)].ftype));
			;}
    break;

  case 19:
#line 498 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiProjectionV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 20:
#line 507 "rib_parser.yy"
    {
				GET_CTX()->RiClipping((yysemantic_stack_[(3) - (2)].ftype), (yysemantic_stack_[(3) - (3)].ftype));
			;}
    break;

  case 21:
#line 511 "rib_parser.yy"
    {
				GET_CTX()->RiDepthOfField(0, 0, 0);
			;}
    break;

  case 22:
#line 515 "rib_parser.yy"
    {
				GET_CTX()->RiDepthOfField((yysemantic_stack_[(4) - (2)].ftype), (yysemantic_stack_[(4) - (3)].ftype), (yysemantic_stack_[(4) - (4)].ftype));
			;}
    break;

  case 23:
#line 519 "rib_parser.yy"
    {
				GET_CTX()->RiShutter((yysemantic_stack_[(3) - (2)].ftype), (yysemantic_stack_[(3) - (3)].ftype));
			;}
    break;

  case 24:
#line 523 "rib_parser.yy"
    {
				GET_CTX()->RiPixelVariance((yysemantic_stack_[(2) - (2)].ftype));
			;}
    break;

  case 25:
#line 527 "rib_parser.yy"
    {
				GET_CTX()->RiPixelSamples((yysemantic_stack_[(3) - (2)].ftype), (yysemantic_stack_[(3) - (3)].ftype));
			;}
    break;

  case 26:
#line 531 "rib_parser.yy"
    {
				GET_CTX()->RiPixelFilter((yysemantic_stack_[(4) - (2)].stype), (yysemantic_stack_[(4) - (3)].ftype), (yysemantic_stack_[(4) - (4)].ftype));
				release_value((yysemantic_stack_[(4) - (2)].stype));
			;}
    break;

  case 27:
#line 536 "rib_parser.yy"
    {
				GET_CTX()->RiElse();
			;}
    break;

  case 28:
#line 540 "rib_parser.yy"
    {
				GET_CTX()->RiElseIf((yysemantic_stack_[(2) - (2)].stype));
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 29:
#line 545 "rib_parser.yy"
    {
				GET_CTX()->RiExposure((yysemantic_stack_[(3) - (2)].ftype), (yysemantic_stack_[(3) - (3)].ftype));
			;}
    break;

  case 30:
#line 549 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiImagerV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 31:
#line 558 "rib_parser.yy"
    {
				GET_CTX()->RiQuantize((yysemantic_stack_[(6) - (2)].stype), (yysemantic_stack_[(6) - (3)].itype), (yysemantic_stack_[(6) - (4)].itype), (yysemantic_stack_[(6) - (5)].itype), (yysemantic_stack_[(6) - (6)].ftype));
				release_value((yysemantic_stack_[(6) - (2)].stype));
			;}
    break;

  case 32:
#line 563 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiDisplayChannelV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 33:
#line 572 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(5) - (5)].atvtype));
				GET_CTX()->RiDisplayV((yysemantic_stack_[(5) - (2)].stype), (yysemantic_stack_[(5) - (3)].stype), (yysemantic_stack_[(5) - (4)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(5) - (2)].stype));
				release_value((yysemantic_stack_[(5) - (3)].stype));
				release_value((yysemantic_stack_[(5) - (4)].stype));
				release_value((yysemantic_stack_[(5) - (5)].atvtype));
			;}
    break;

  case 34:
#line 583 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiHiderV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 35:
#line 592 "rib_parser.yy"
    {
				int n = (int)((yysemantic_stack_[(3) - (2)].aftype)->size());
				GET_CTX()->RiColorSamples(n, &((*(yysemantic_stack_[(3) - (2)].aftype))[0]), &((*(yysemantic_stack_[(3) - (3)].aftype))[0]));
				release_value((yysemantic_stack_[(3) - (2)].aftype));
				release_value((yysemantic_stack_[(3) - (3)].aftype));
			;}
    break;

  case 36:
#line 599 "rib_parser.yy"
    {
				GET_CTX()->RiRelativeDetail((yysemantic_stack_[(2) - (2)].ftype));
			;}
    break;

  case 37:
#line 603 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiOptionV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 38:
#line 612 "rib_parser.yy"
    {
				GET_CTX()->RiAttributeBegin();
			;}
    break;

  case 39:
#line 616 "rib_parser.yy"
    {
				GET_CTX()->RiAttributeEnd();
			;}
    break;

  case 40:
#line 620 "rib_parser.yy"
    {
				RtColor c;
				c[0] = (*(yysemantic_stack_[(2) - (2)].aftype))[0];
				c[1] = (*(yysemantic_stack_[(2) - (2)].aftype))[1];
				c[2] = (*(yysemantic_stack_[(2) - (2)].aftype))[2];
				GET_CTX()->RiColor(c);
				release_value((yysemantic_stack_[(2) - (2)].aftype));
			;}
    break;

  case 41:
#line 629 "rib_parser.yy"
    {
				RtColor c;
				c[0] = (yysemantic_stack_[(4) - (2)].ftype);
				c[1] = (yysemantic_stack_[(4) - (3)].ftype);
				c[2] = (yysemantic_stack_[(4) - (4)].ftype);
				GET_CTX()->RiColor(c);
			;}
    break;

  case 42:
#line 637 "rib_parser.yy"
    {
				RtColor c;
				c[0] = (*(yysemantic_stack_[(2) - (2)].aftype))[0];
				c[1] = (*(yysemantic_stack_[(2) - (2)].aftype))[1];
				c[2] = (*(yysemantic_stack_[(2) - (2)].aftype))[2];
				GET_CTX()->RiOpacity(c);
				release_value((yysemantic_stack_[(2) - (2)].aftype));
			;}
    break;

  case 43:
#line 646 "rib_parser.yy"
    {
				RtColor c;
				c[0] = (yysemantic_stack_[(4) - (2)].ftype);
				c[1] = (yysemantic_stack_[(4) - (3)].ftype);
				c[2] = (yysemantic_stack_[(4) - (4)].ftype);
				GET_CTX()->RiOpacity(c);
			;}
    break;

  case 44:
#line 654 "rib_parser.yy"
    {
				GET_CTX()->RiTextureCoordinates((yysemantic_stack_[(9) - (2)].ftype), (yysemantic_stack_[(9) - (3)].ftype), (yysemantic_stack_[(9) - (4)].ftype), (yysemantic_stack_[(9) - (5)].ftype), (yysemantic_stack_[(9) - (6)].ftype), (yysemantic_stack_[(9) - (7)].ftype), (yysemantic_stack_[(9) - (8)].ftype), (yysemantic_stack_[(9) - (9)].ftype));
			;}
    break;

  case 45:
#line 658 "rib_parser.yy"
    {
				float_array& afv = *(yysemantic_stack_[(2) - (2)].aftype);
				GET_CTX()->RiTextureCoordinates(afv[0], afv[1], afv[2], afv[3], afv[4], afv[5], afv[6], afv[7]);
				release_value((yysemantic_stack_[(2) - (2)].aftype));
			;}
    break;

  case 46:
#line 664 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				RtLightHandle handle = GET_CTX()->RiLightSourceV((yysemantic_stack_[(4) - (2)].stype), n, &tokens[0], &params[0]);
				SET_LIGHTMAP((yysemantic_stack_[(4) - (3)].itype), handle);
				release_value((yysemantic_stack_[(4) - (2)].stype));
				//release_value($3);
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 47:
#line 675 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				RtLightHandle handle = GET_CTX()->RiLightSourceV((yysemantic_stack_[(4) - (2)].stype), n, &tokens[0], &params[0]);
				SET_LIGHTMAP((yysemantic_stack_[(4) - (3)].stype), handle);
				release_value((yysemantic_stack_[(4) - (2)].stype));
				release_value((yysemantic_stack_[(4) - (3)].stype));
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 48:
#line 686 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				RtLightHandle handle = GET_CTX()->RiAreaLightSourceV((yysemantic_stack_[(4) - (2)].stype), n, &tokens[0], &params[0]);
				SET_LIGHTMAP((yysemantic_stack_[(4) - (3)].itype), handle);
				release_value((yysemantic_stack_[(4) - (2)].stype));
				//release_value($3);
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 49:
#line 697 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				RtLightHandle handle = GET_CTX()->RiAreaLightSourceV((yysemantic_stack_[(4) - (2)].stype), n, &tokens[0], &params[0]);
				SET_LIGHTMAP((yysemantic_stack_[(4) - (3)].stype), handle);
				release_value((yysemantic_stack_[(4) - (2)].stype));
				release_value((yysemantic_stack_[(4) - (3)].stype));
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 50:
#line 708 "rib_parser.yy"
    {
				GET_CTX()->RiIfBegin((yysemantic_stack_[(2) - (2)].stype));
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 51:
#line 713 "rib_parser.yy"
    {
				GET_CTX()->RiIfEnd();
			;}
    break;

  case 52:
#line 717 "rib_parser.yy"
    {
				GET_CTX()->RiIlluminate((yysemantic_stack_[(3) - (2)].itype),(yysemantic_stack_[(3) - (3)].itype));
			;}
    break;

  case 53:
#line 721 "rib_parser.yy"
    {
				GET_CTX()->RiIlluminate((yysemantic_stack_[(3) - (2)].stype),(yysemantic_stack_[(3) - (3)].itype));
				release_value((yysemantic_stack_[(3) - (2)].stype));
			;}
    break;

  case 54:
#line 726 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiSurfaceV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 55:
#line 735 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiAtmosphereV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 56:
#line 744 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiInteriorV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 57:
#line 753 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiExteriorV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 58:
#line 762 "rib_parser.yy"
    {
				GET_CTX()->RiShadingRate((yysemantic_stack_[(2) - (2)].ftype));
			;}
    break;

  case 59:
#line 766 "rib_parser.yy"
    {
				GET_CTX()->RiShadingInterpolation((yysemantic_stack_[(2) - (2)].stype));
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 60:
#line 771 "rib_parser.yy"
    {
				GET_CTX()->RiMatte((yysemantic_stack_[(2) - (2)].itype));
			;}
    break;

  case 61:
#line 775 "rib_parser.yy"
    {
				RtBound b = {(yysemantic_stack_[(7) - (2)].ftype), (yysemantic_stack_[(7) - (3)].ftype), (yysemantic_stack_[(7) - (4)].ftype), (yysemantic_stack_[(7) - (5)].ftype), (yysemantic_stack_[(7) - (6)].ftype), (yysemantic_stack_[(7) - (7)].ftype)};
				GET_CTX()->RiBound(b);
			;}
    break;

  case 62:
#line 780 "rib_parser.yy"
    {
				float_array& afv = *(yysemantic_stack_[(2) - (2)].aftype);
				RtBound b = {afv[0], afv[1], afv[2], afv[3], afv[4], afv[5]};
				GET_CTX()->RiBound(b);
				release_value((yysemantic_stack_[(2) - (2)].aftype));
			;}
    break;

  case 63:
#line 787 "rib_parser.yy"
    {
				RtBound b = {(yysemantic_stack_[(7) - (2)].ftype), (yysemantic_stack_[(7) - (3)].ftype), (yysemantic_stack_[(7) - (4)].ftype), (yysemantic_stack_[(7) - (5)].ftype), (yysemantic_stack_[(7) - (6)].ftype), (yysemantic_stack_[(7) - (7)].ftype)};
				GET_CTX()->RiDetail(b);
			;}
    break;

  case 64:
#line 792 "rib_parser.yy"
    {
				float_array& afv = *(yysemantic_stack_[(2) - (2)].aftype);
				RtBound b = {afv[0], afv[1], afv[2], afv[3], afv[4], afv[5]};
				GET_CTX()->RiDetail(b);
				release_value((yysemantic_stack_[(2) - (2)].aftype));
			;}
    break;

  case 65:
#line 799 "rib_parser.yy"
    {
				GET_CTX()->RiDetailRange((yysemantic_stack_[(5) - (2)].ftype), (yysemantic_stack_[(5) - (3)].ftype), (yysemantic_stack_[(5) - (4)].ftype), (yysemantic_stack_[(5) - (5)].ftype));
			;}
    break;

  case 66:
#line 803 "rib_parser.yy"
    {
				float_array& afv = *(yysemantic_stack_[(2) - (2)].aftype);
				GET_CTX()->RiDetailRange(afv[0], afv[1], afv[2], afv[3]);
				release_value((yysemantic_stack_[(2) - (2)].aftype));
			;}
    break;

  case 67:
#line 809 "rib_parser.yy"
    {
				GET_CTX()->RiGeometricApproximation((yysemantic_stack_[(3) - (2)].stype), (yysemantic_stack_[(3) - (3)].ftype));
				release_value((yysemantic_stack_[(3) - (2)].stype));
			;}
    break;

  case 68:
#line 814 "rib_parser.yy"
    {
				GET_CTX()->RiGeometricApproximation((yysemantic_stack_[(3) - (2)].stype), (*(yysemantic_stack_[(3) - (3)].aftype))[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].aftype));
			;}
    break;

  case 69:
#line 820 "rib_parser.yy"
    {
				GET_CTX()->RiOrientation((yysemantic_stack_[(2) - (2)].stype));
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 70:
#line 825 "rib_parser.yy"
    {
				GET_CTX()->RiReverseOrientation();
			;}
    break;

  case 71:
#line 829 "rib_parser.yy"
    {
				GET_CTX()->RiSides((yysemantic_stack_[(2) - (2)].itype));
			;}
    break;

  case 72:
#line 833 "rib_parser.yy"
    {
				GET_CTX()->RiIdentity();
			;}
    break;

  case 73:
#line 837 "rib_parser.yy"
    {
				RtMatrix tmp;
				convet_to_RtMatrix((yysemantic_stack_[(2) - (2)].aftype),tmp);
				GET_CTX()->RiTransform(tmp);
				release_value((yysemantic_stack_[(2) - (2)].aftype));
			;}
    break;

  case 74:
#line 844 "rib_parser.yy"
    {
				RtMatrix tmp;
				convet_to_RtMatrix((yysemantic_stack_[(2) - (2)].aftype),tmp);
				GET_CTX()->RiConcatTransform(tmp);
				release_value((yysemantic_stack_[(2) - (2)].aftype));
			;}
    break;

  case 75:
#line 851 "rib_parser.yy"
    {
				GET_CTX()->RiPerspective((yysemantic_stack_[(2) - (2)].ftype));
			;}
    break;

  case 76:
#line 855 "rib_parser.yy"
    {
				GET_CTX()->RiTranslate((yysemantic_stack_[(4) - (2)].ftype), (yysemantic_stack_[(4) - (3)].ftype), (yysemantic_stack_[(4) - (4)].ftype));
			;}
    break;

  case 77:
#line 859 "rib_parser.yy"
    {
				GET_CTX()->RiRotate((yysemantic_stack_[(5) - (2)].ftype), (yysemantic_stack_[(5) - (3)].ftype), (yysemantic_stack_[(5) - (4)].ftype), (yysemantic_stack_[(5) - (5)].ftype));
			;}
    break;

  case 78:
#line 863 "rib_parser.yy"
    {
				GET_CTX()->RiScale((yysemantic_stack_[(4) - (2)].ftype), (yysemantic_stack_[(4) - (3)].ftype), (yysemantic_stack_[(4) - (4)].ftype));
			;}
    break;

  case 79:
#line 867 "rib_parser.yy"
    {
				GET_CTX()->RiSkew((yysemantic_stack_[(8) - (2)].ftype), (yysemantic_stack_[(8) - (3)].ftype), (yysemantic_stack_[(8) - (4)].ftype), (yysemantic_stack_[(8) - (5)].ftype), (yysemantic_stack_[(8) - (6)].ftype), (yysemantic_stack_[(8) - (7)].ftype), (yysemantic_stack_[(8) - (8)].ftype));
			;}
    break;

  case 80:
#line 871 "rib_parser.yy"
    {
				float_array& afv = *(yysemantic_stack_[(2) - (2)].aftype);
				GET_CTX()->RiSkew(afv[0],afv[1],afv[2],afv[3],afv[4],afv[5],afv[6]);
				release_value((yysemantic_stack_[(2) - (2)].aftype));
			;}
    break;

  case 81:
#line 877 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiDeformationV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 82:
#line 886 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiDisplacementV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 83:
#line 895 "rib_parser.yy"
    {
				GET_CTX()->RiCoordinateSystem((yysemantic_stack_[(2) - (2)].stype));
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 84:
#line 900 "rib_parser.yy"
    {
				GET_CTX()->RiScopedCoordinateSystem((yysemantic_stack_[(2) - (2)].stype));
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 85:
#line 905 "rib_parser.yy"
    {
				GET_CTX()->RiCoordSysTransform((yysemantic_stack_[(2) - (2)].stype));
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 86:
#line 910 "rib_parser.yy"
    {
				;//GET_CTX()->RiTransformPoints();
			;}
    break;

  case 87:
#line 914 "rib_parser.yy"
    {
				GET_CTX()->RiTransformBegin();
			;}
    break;

  case 88:
#line 918 "rib_parser.yy"
    {
				GET_CTX()->RiTransformEnd();
			;}
    break;

  case 89:
#line 922 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				GET_CTX()->RiResourceV((yysemantic_stack_[(4) - (2)].stype), (yysemantic_stack_[(4) - (3)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(4) - (2)].stype));
				release_value((yysemantic_stack_[(4) - (3)].stype));
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 90:
#line 932 "rib_parser.yy"
    {
				GET_CTX()->RiResourceBegin();
			;}
    break;

  case 91:
#line 936 "rib_parser.yy"
    {
				GET_CTX()->RiResourceEnd();
			;}
    break;

  case 92:
#line 940 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiAttributeV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 93:
#line 949 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(2) - (2)].atvtype));
				int nFind = FindParam(RI_P,&tokens[0],n);//TODO:move to context...
				if(nFind>=0){
					int nv = (((*(yysemantic_stack_[(2) - (2)].atvtype))[nFind])->second->size())/3;//TODO
					GET_CTX()->RiPolygonV(nv, n, &tokens[0], &params[0]);
				}
				release_value((yysemantic_stack_[(2) - (2)].atvtype));
			;}
    break;

  case 94:
#line 961 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiGeneralPolygonV((yysemantic_stack_[(3) - (2)].aitype)->size(), &(*(yysemantic_stack_[(3) - (2)].aitype))[0], n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].aitype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 95:
#line 970 "rib_parser.yy"
    {
            	//RiCurves (RtToken type, RtInt ncurves, RtInt nvertices[], RtToken wrap, ...);
            	std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(5) - (5)].atvtype));
				int nCurves = (int)((yysemantic_stack_[(5) - (3)].aitype)->size());
				GET_CTX()->RiCurvesV((yysemantic_stack_[(5) - (2)].stype), nCurves, &((*(yysemantic_stack_[(5) - (3)].aitype))[0]), (yysemantic_stack_[(5) - (4)].stype) , n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(5) - (2)].stype));
				release_value((yysemantic_stack_[(5) - (3)].aitype));
				release_value((yysemantic_stack_[(5) - (4)].stype));
				release_value((yysemantic_stack_[(5) - (5)].atvtype));
            ;}
    break;

  case 96:
#line 983 "rib_parser.yy"
    {
            	//RiBlobby(RtInt nleaf, RtInt ncode, RtInt code[], RtInt nflt, RtFloat flt[], RtInt nstr, RtToken str[], …);
            	std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(6) - (6)].atvtype));
				int ncode = (int)((yysemantic_stack_[(6) - (3)].aitype)->size());
				int nflt  = (int)((yysemantic_stack_[(6) - (4)].aftype)->size());
				int nstr  = (int)((yysemantic_stack_[(6) - (5)].astype)->size());

				RtToken sDummy = "";
				RtToken* array5 = &sDummy;
				if((yysemantic_stack_[(6) - (5)].astype)->size()){
					array5 = &(*(yysemantic_stack_[(6) - (5)].astype))[0];
				}


				GET_CTX()->RiBlobbyV((yysemantic_stack_[(6) - (2)].itype), ncode, &((*(yysemantic_stack_[(6) - (3)].aitype))[0]), nflt, &((*(yysemantic_stack_[(6) - (4)].aftype))[0]), nstr, array5, n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(6) - (3)].aitype));
				release_value((yysemantic_stack_[(6) - (4)].aftype));
				release_value((yysemantic_stack_[(6) - (5)].astype));
				release_value((yysemantic_stack_[(6) - (6)].atvtype));
            ;}
    break;

  case 97:
#line 1006 "rib_parser.yy"
    {
	    		GET_CTX()->RiReadArchiveV((yysemantic_stack_[(2) - (2)].stype),NULL,0,NULL,NULL);
	    		release_value((yysemantic_stack_[(2) - (2)].stype));
	    	;}
    break;

  case 98:
#line 1011 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiArchiveBeginV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 99:
#line 1020 "rib_parser.yy"
    {
				GET_CTX()->RiArchiveEnd();
			;}
    break;

  case 100:
#line 1024 "rib_parser.yy"
    {
				if((yysemantic_stack_[(5) - (4)].aftype)->size() == 6){
					std::vector<RtToken> tokens;
					std::vector<void*> params;
					int n = expand_token_value(tokens,params,*(yysemantic_stack_[(5) - (5)].atvtype));

					GET_CTX()->RiProceduralV((yysemantic_stack_[(5) - (2)].stype), (yysemantic_stack_[(5) - (3)].astype)->size(), &(*(yysemantic_stack_[(5) - (3)].astype))[0], &(*(yysemantic_stack_[(5) - (4)].aftype))[0], n, &tokens[0], &params[0]);
				}
				release_value((yysemantic_stack_[(5) - (2)].stype));
				release_value((yysemantic_stack_[(5) - (3)].astype));
				release_value((yysemantic_stack_[(5) - (4)].aftype));
				release_value((yysemantic_stack_[(5) - (5)].atvtype));
            ;}
    break;

  case 101:
#line 1038 "rib_parser.yy"
    {
				if((yysemantic_stack_[(4) - (3)].aftype)->size() == 6){
					std::vector<RtToken> tokens;
					std::vector<void*> params;
					int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));

					GET_CTX()->RiProcedureV((yysemantic_stack_[(4) - (2)].stype), &(*(yysemantic_stack_[(4) - (3)].aftype))[0], n, &tokens[0], &params[0]);
				}
				release_value((yysemantic_stack_[(4) - (2)].stype));
				release_value((yysemantic_stack_[(4) - (3)].aftype));
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
            ;}
    break;

  case 102:
#line 1051 "rib_parser.yy"
    {
            	std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(2) - (2)].atvtype));
				int nFind = FindParam(RI_P,&tokens[0],n);//TODO:move to context...
				if(nFind>=0){
					int nv = (((*(yysemantic_stack_[(2) - (2)].atvtype))[nFind])->second->size())/3;//TODO
					GET_CTX()->RiPointsV(nv, n, &tokens[0], &params[0]);
				}
				release_value((yysemantic_stack_[(2) - (2)].atvtype));
            ;}
    break;

  case 103:
#line 1063 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				GET_CTX()->RiPointsPolygonsV((yysemantic_stack_[(4) - (2)].aitype)->size(), &(*(yysemantic_stack_[(4) - (2)].aitype))[0], &(*(yysemantic_stack_[(4) - (3)].aitype))[0], n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(4) - (2)].aitype));
				release_value((yysemantic_stack_[(4) - (3)].aitype));
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 104:
#line 1073 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(5) - (5)].atvtype));
				GET_CTX()->RiPointsGeneralPolygonsV((yysemantic_stack_[(5) - (2)].aitype)->size(), &(*(yysemantic_stack_[(5) - (2)].aitype))[0], &(*(yysemantic_stack_[(5) - (3)].aitype))[0], &(*(yysemantic_stack_[(5) - (4)].aitype))[0], n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(5) - (2)].aitype));
				release_value((yysemantic_stack_[(5) - (3)].aitype));
				release_value((yysemantic_stack_[(5) - (4)].aitype));
				release_value((yysemantic_stack_[(5) - (5)].atvtype));
			;}
    break;

  case 105:
#line 1084 "rib_parser.yy"
    {
				GET_CTX()->RiBasis((yysemantic_stack_[(5) - (2)].stype), (yysemantic_stack_[(5) - (3)].itype), (yysemantic_stack_[(5) - (4)].stype), (yysemantic_stack_[(5) - (5)].itype));
				release_value((yysemantic_stack_[(5) - (2)].stype));
				release_value((yysemantic_stack_[(5) - (4)].stype));
			;}
    break;

  case 106:
#line 1090 "rib_parser.yy"
    {
				RtBasis vb = {};
				if((yysemantic_stack_[(5) - (4)].aftype)->size()==16){
					memcpy(vb, &((*(yysemantic_stack_[(5) - (4)].aftype))[0]), sizeof(RtBasis));
					GET_CTX()->RiBasis((yysemantic_stack_[(5) - (2)].stype), (yysemantic_stack_[(5) - (3)].itype), vb, (yysemantic_stack_[(5) - (5)].itype));
				}else{
					yyerror("Unexpected basis size");
				}
				release_value((yysemantic_stack_[(5) - (2)].stype));
				release_value((yysemantic_stack_[(5) - (4)].aftype));
			;}
    break;

  case 107:
#line 1102 "rib_parser.yy"
    {
				RtBasis ub = {};
				if((yysemantic_stack_[(5) - (2)].aftype)->size()==16){
					memcpy(ub, &((*(yysemantic_stack_[(5) - (2)].aftype))[0]), sizeof(RtBasis));
					GET_CTX()->RiBasis(ub, (yysemantic_stack_[(5) - (3)].itype), (yysemantic_stack_[(5) - (4)].stype), (yysemantic_stack_[(5) - (5)].itype));
				}else{
					yyerror("Unexpected basis size");
				}
				release_value((yysemantic_stack_[(5) - (2)].aftype));
				release_value((yysemantic_stack_[(5) - (4)].stype));
			;}
    break;

  case 108:
#line 1114 "rib_parser.yy"
    {
				RtBasis ub = {};
				RtBasis vb = {};
				if((yysemantic_stack_[(5) - (2)].aftype)->size()==16 && (yysemantic_stack_[(5) - (4)].aftype)->size()==16){
					memcpy(ub, &((*(yysemantic_stack_[(5) - (2)].aftype))[0]), sizeof(RtBasis));
					memcpy(vb, &((*(yysemantic_stack_[(5) - (4)].aftype))[0]), sizeof(RtBasis));
					GET_CTX()->RiBasis(ub, (yysemantic_stack_[(5) - (3)].itype), vb, (yysemantic_stack_[(5) - (5)].itype));
				}else{
					yyerror("Unexpected basis size");
				}
				release_value((yysemantic_stack_[(5) - (2)].aftype));
				release_value((yysemantic_stack_[(5) - (4)].aftype));
			;}
    break;

  case 109:
#line 1128 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiPatchV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 110:
#line 1137 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(7) - (7)].atvtype));
				GET_CTX()->RiPatchMeshV((yysemantic_stack_[(7) - (2)].stype), (yysemantic_stack_[(7) - (3)].itype), (yysemantic_stack_[(7) - (4)].stype), (yysemantic_stack_[(7) - (5)].itype), (yysemantic_stack_[(7) - (6)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(7) - (2)].stype));
				release_value((yysemantic_stack_[(7) - (4)].stype));
				release_value((yysemantic_stack_[(7) - (6)].stype));
				release_value((yysemantic_stack_[(7) - (7)].atvtype));
			;}
    break;

  case 111:
#line 1148 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(12) - (12)].atvtype));
				GET_CTX()->RiNuPatchV((yysemantic_stack_[(12) - (2)].itype), (yysemantic_stack_[(12) - (3)].itype), &(*(yysemantic_stack_[(12) - (4)].aftype))[0], (yysemantic_stack_[(12) - (5)].ftype), (yysemantic_stack_[(12) - (6)].ftype), (yysemantic_stack_[(12) - (7)].itype), (yysemantic_stack_[(12) - (8)].itype), &(*(yysemantic_stack_[(12) - (9)].aftype))[0], (yysemantic_stack_[(12) - (10)].ftype), (yysemantic_stack_[(12) - (11)].ftype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(12) - (4)].aftype));
				release_value((yysemantic_stack_[(12) - (9)].aftype));
				release_value((yysemantic_stack_[(12) - (12)].atvtype));
			;}
    break;

  case 112:
#line 1158 "rib_parser.yy"
    {
				GET_CTX()->RiTrimCurve((yysemantic_stack_[(10) - (2)].aitype)->size(), &(*(yysemantic_stack_[(10) - (2)].aitype))[0], &(*(yysemantic_stack_[(10) - (3)].aitype))[0], &(*(yysemantic_stack_[(10) - (4)].aftype))[0], &(*(yysemantic_stack_[(10) - (5)].aftype))[0], &(*(yysemantic_stack_[(10) - (6)].aftype))[0], &(*(yysemantic_stack_[(10) - (7)].aitype))[0], &(*(yysemantic_stack_[(10) - (8)].aftype))[0], &(*(yysemantic_stack_[(10) - (9)].aftype))[0], &(*(yysemantic_stack_[(10) - (10)].aftype))[0]);
				release_value((yysemantic_stack_[(10) - (2)].aitype));
				release_value((yysemantic_stack_[(10) - (3)].aitype));
				release_value((yysemantic_stack_[(10) - (4)].aftype));
				release_value((yysemantic_stack_[(10) - (5)].aftype));
				release_value((yysemantic_stack_[(10) - (6)].aftype));
				release_value((yysemantic_stack_[(10) - (7)].aitype));
				release_value((yysemantic_stack_[(10) - (8)].aftype));
				release_value((yysemantic_stack_[(10) - (9)].aftype));
				release_value((yysemantic_stack_[(10) - (10)].aftype));
			;}
    break;

  case 113:
#line 1171 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(6) - (6)].atvtype));
				GET_CTX()->RiSphereV((yysemantic_stack_[(6) - (2)].ftype), (yysemantic_stack_[(6) - (3)].ftype), (yysemantic_stack_[(6) - (4)].ftype), (yysemantic_stack_[(6) - (5)].ftype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(6) - (6)].atvtype));
			;}
    break;

  case 114:
#line 1179 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(5) - (5)].atvtype));
				GET_CTX()->RiConeV((yysemantic_stack_[(5) - (2)].ftype), (yysemantic_stack_[(5) - (3)].ftype), (yysemantic_stack_[(5) - (4)].ftype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(5) - (5)].atvtype));
			;}
    break;

  case 115:
#line 1187 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(6) - (6)].atvtype));
				GET_CTX()->RiCylinderV((yysemantic_stack_[(6) - (2)].ftype), (yysemantic_stack_[(6) - (3)].ftype), (yysemantic_stack_[(6) - (4)].ftype), (yysemantic_stack_[(6) - (5)].ftype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(6) - (6)].atvtype));
			;}
    break;

  case 116:
#line 1195 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));

				float_array& afv = *(yysemantic_stack_[(3) - (2)].aftype);
				RtPoint p1 = {afv[0], afv[1], afv[2]};
				RtPoint p2 = {afv[3], afv[4], afv[5]};
				RtFloat tmax = afv[6];

				GET_CTX()->RiHyperboloidV(p1, p2, tmax, n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].aftype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 117:
#line 1210 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(9) - (9)].atvtype));

				RtPoint p1 = {(yysemantic_stack_[(9) - (2)].ftype), (yysemantic_stack_[(9) - (3)].ftype), (yysemantic_stack_[(9) - (4)].ftype)};
				RtPoint p2 = {(yysemantic_stack_[(9) - (5)].ftype), (yysemantic_stack_[(9) - (6)].ftype), (yysemantic_stack_[(9) - (7)].ftype)};
				RtFloat tmax = (yysemantic_stack_[(9) - (8)].ftype);

				GET_CTX()->RiHyperboloidV(p1, p2, tmax, n, &tokens[0], &params[0]);

				release_value((yysemantic_stack_[(9) - (9)].atvtype));
			;}
    break;

  case 118:
#line 1224 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(6) - (6)].atvtype));
				GET_CTX()->RiParaboloidV((yysemantic_stack_[(6) - (2)].ftype), (yysemantic_stack_[(6) - (3)].ftype), (yysemantic_stack_[(6) - (4)].ftype), (yysemantic_stack_[(6) - (5)].ftype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(6) - (6)].atvtype));
			;}
    break;

  case 119:
#line 1232 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(5) - (5)].atvtype));
				GET_CTX()->RiDiskV((yysemantic_stack_[(5) - (2)].ftype), (yysemantic_stack_[(5) - (3)].ftype), (yysemantic_stack_[(5) - (4)].ftype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(5) - (5)].atvtype));
			;}
    break;

  case 120:
#line 1240 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(7) - (7)].atvtype));
				GET_CTX()->RiTorusV((yysemantic_stack_[(7) - (2)].ftype), (yysemantic_stack_[(7) - (3)].ftype), (yysemantic_stack_[(7) - (4)].ftype), (yysemantic_stack_[(7) - (5)].ftype), (yysemantic_stack_[(7) - (6)].ftype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(7) - (7)].atvtype));
			;}
    break;

  case 121:
#line 1248 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiGeometryV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 122:
#line 1257 "rib_parser.yy"
    {
				GET_CTX()->RiSolidBegin((yysemantic_stack_[(2) - (2)].stype));
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 123:
#line 1262 "rib_parser.yy"
    {
				GET_CTX()->RiSolidEnd();
			;}
    break;

  case 124:
#line 1266 "rib_parser.yy"
    {
				RtObjectHandle handle = GET_CTX()->RiObjectBegin();
				SET_OBJECTMAP((yysemantic_stack_[(2) - (2)].itype), handle);
			;}
    break;

  case 125:
#line 1271 "rib_parser.yy"
    {
				RtObjectHandle handle = GET_CTX()->RiObjectBegin();
				SET_OBJECTMAP((yysemantic_stack_[(2) - (2)].stype), handle);
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 126:
#line 1277 "rib_parser.yy"
    {
				GET_CTX()->RiObjectEnd();
			;}
    break;

  case 127:
#line 1281 "rib_parser.yy"
    {
				GET_CTX()->RiObjectInstance((yysemantic_stack_[(2) - (2)].itype));
			;}
    break;

  case 128:
#line 1285 "rib_parser.yy"
    {
				GET_CTX()->RiObjectInstance((yysemantic_stack_[(2) - (2)].stype));
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 129:
#line 1290 "rib_parser.yy"
    {
				GET_CTX()->RiMotionBeginV((yysemantic_stack_[(2) - (2)].aftype)->size(), &((*(yysemantic_stack_[(2) - (2)].aftype))[0]));
				release_value((yysemantic_stack_[(2) - (2)].aftype));
			;}
    break;

  case 130:
#line 1295 "rib_parser.yy"
    {
				GET_CTX()->RiMotionEnd();
			;}
    break;

  case 131:
#line 1299 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(9) - (9)].atvtype));

				//RtFilterFunc func = GET_CTX()->RiGetFilterFunc($6);
				GET_CTX()->RiMakeTextureV((yysemantic_stack_[(9) - (2)].stype), (yysemantic_stack_[(9) - (3)].stype), (yysemantic_stack_[(9) - (4)].stype), (yysemantic_stack_[(9) - (5)].stype), (yysemantic_stack_[(9) - (6)].stype), (yysemantic_stack_[(9) - (7)].ftype), (yysemantic_stack_[(9) - (8)].ftype), n, &tokens[0], &params[0]);

				release_value((yysemantic_stack_[(9) - (2)].stype));
				release_value((yysemantic_stack_[(9) - (3)].stype));
				release_value((yysemantic_stack_[(9) - (4)].stype));
				release_value((yysemantic_stack_[(9) - (5)].stype));
				release_value((yysemantic_stack_[(9) - (6)].stype));
				//release_value($7);
				//release_value($8);
				release_value((yysemantic_stack_[(9) - (9)].atvtype));
			;}
    break;

  case 132:
#line 1317 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(9) - (9)].atvtype));
				//RtFilterFunc func = GET_CTX()->RiGetFilterFunc($6);
				GET_CTX()->RiMakeBumpV((yysemantic_stack_[(9) - (2)].stype), (yysemantic_stack_[(9) - (3)].stype), (yysemantic_stack_[(9) - (4)].stype), (yysemantic_stack_[(9) - (5)].stype), (yysemantic_stack_[(9) - (6)].stype), (yysemantic_stack_[(9) - (7)].ftype), (yysemantic_stack_[(9) - (8)].ftype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(9) - (2)].stype));
				release_value((yysemantic_stack_[(9) - (3)].stype));
				release_value((yysemantic_stack_[(9) - (4)].stype));
				release_value((yysemantic_stack_[(9) - (5)].stype));
				release_value((yysemantic_stack_[(9) - (6)].stype));
				//release_value($7);
				//release_value($8);
				release_value((yysemantic_stack_[(9) - (9)].atvtype));
			;}
    break;

  case 133:
#line 1333 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(7) - (7)].atvtype));
				//RtFilterFunc func = GET_CTX()->RiGetFilterFunc($4);
				GET_CTX()->RiMakeLatLongEnvironmentV((yysemantic_stack_[(7) - (2)].stype), (yysemantic_stack_[(7) - (3)].stype), (yysemantic_stack_[(7) - (4)].stype), (yysemantic_stack_[(7) - (5)].ftype), (yysemantic_stack_[(7) - (6)].ftype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(7) - (2)].stype));
				release_value((yysemantic_stack_[(7) - (3)].stype));
				release_value((yysemantic_stack_[(7) - (4)].stype));
				//release_value($5);
				//release_value($6);
				release_value((yysemantic_stack_[(7) - (7)].atvtype));
			;}
    break;

  case 134:
#line 1347 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(13) - (13)].atvtype));
				//RtFilterFunc func = GET_CTX()->RiGetFilterFunc($10);
				GET_CTX()->RiMakeCubeFaceEnvironmentV((yysemantic_stack_[(13) - (2)].stype), (yysemantic_stack_[(13) - (3)].stype), (yysemantic_stack_[(13) - (4)].stype), (yysemantic_stack_[(13) - (5)].stype), (yysemantic_stack_[(13) - (6)].stype), (yysemantic_stack_[(13) - (7)].stype), (yysemantic_stack_[(13) - (8)].stype), (yysemantic_stack_[(13) - (9)].ftype), (yysemantic_stack_[(13) - (10)].stype), (yysemantic_stack_[(13) - (11)].ftype), (yysemantic_stack_[(13) - (12)].ftype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(13) - (2)].stype));
				release_value((yysemantic_stack_[(13) - (3)].stype));
				release_value((yysemantic_stack_[(13) - (4)].stype));
				release_value((yysemantic_stack_[(13) - (5)].stype));
				release_value((yysemantic_stack_[(13) - (6)].stype));
				release_value((yysemantic_stack_[(13) - (7)].stype));
				release_value((yysemantic_stack_[(13) - (8)].stype));
				//release_value($9);
				release_value((yysemantic_stack_[(13) - (10)].stype));
				//release_value($11);
				//release_value($12);
				release_value((yysemantic_stack_[(13) - (13)].atvtype));
			;}
    break;

  case 135:
#line 1367 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				GET_CTX()->RiMakeShadowV((yysemantic_stack_[(4) - (2)].stype), (yysemantic_stack_[(4) - (3)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(4) - (2)].stype));
				release_value((yysemantic_stack_[(4) - (3)].stype));
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 136:
#line 1377 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				GET_CTX()->RiMakeOcclusionV((yysemantic_stack_[(4) - (2)].astype)->size(), &(*(yysemantic_stack_[(4) - (2)].astype))[0], (yysemantic_stack_[(4) - (3)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(4) - (2)].astype));
				release_value((yysemantic_stack_[(4) - (3)].stype));
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 137:
#line 1387 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				RtToken ptcs[] = {(yysemantic_stack_[(4) - (2)].stype)};
				GET_CTX()->RiMakeBrickMapV(1, ptcs, (yysemantic_stack_[(4) - (3)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(4) - (2)].stype));
				release_value((yysemantic_stack_[(4) - (3)].stype));
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 138:
#line 1398 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				GET_CTX()->RiMakeBrickMapV((yysemantic_stack_[(4) - (2)].astype)->size(), &(*(yysemantic_stack_[(4) - (2)].astype))[0], (yysemantic_stack_[(4) - (3)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(4) - (2)].astype));
				release_value((yysemantic_stack_[(4) - (3)].stype));
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 139:
#line 1408 "rib_parser.yy"
    {
				GET_CTX()->RiErrorHandler((yysemantic_stack_[(2) - (2)].stype));
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 140:
#line 1413 "rib_parser.yy"
    {;;}
    break;

  case 141:
#line 1415 "rib_parser.yy"
    {;;}
    break;

  case 142:
#line 1417 "rib_parser.yy"
    {;;}
    break;

  case 143:
#line 1419 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(9) - (9)].atvtype));

				RtToken sDummy = "";
				RtInt iDummy = 0;
				RtFloat fDummy = 0.0f;

				RtToken* array5 = &sDummy;
				if((yysemantic_stack_[(9) - (5)].astype)->size()){
					array5 = &(*(yysemantic_stack_[(9) - (5)].astype))[0];
				}
				RtInt* array6 = &iDummy;
				if((yysemantic_stack_[(9) - (6)].aitype)->size()){
					array6 = &(*(yysemantic_stack_[(9) - (6)].aitype))[0];
				}
				RtInt* array7 = &iDummy;
				if((yysemantic_stack_[(9) - (7)].aitype)->size()){
					array7 = &(*(yysemantic_stack_[(9) - (7)].aitype))[0];
				}
				RtFloat* array8 = &fDummy;
				if((yysemantic_stack_[(9) - (8)].aftype)->size()){
					array8 = &(*(yysemantic_stack_[(9) - (8)].aftype))[0];
				}


				GET_CTX()->RiSubdivisionMeshV((yysemantic_stack_[(9) - (2)].stype), (yysemantic_stack_[(9) - (3)].aitype)->size(), &(*(yysemantic_stack_[(9) - (3)].aitype))[0], &(*(yysemantic_stack_[(9) - (4)].aitype))[0], (yysemantic_stack_[(9) - (5)].astype)->size(), array5, array6, array7, array8, n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(9) - (2)].stype));
				release_value((yysemantic_stack_[(9) - (3)].aitype));
				release_value((yysemantic_stack_[(9) - (4)].aitype));
				release_value((yysemantic_stack_[(9) - (5)].astype));
				release_value((yysemantic_stack_[(9) - (6)].aitype));
				release_value((yysemantic_stack_[(9) - (7)].aitype));
				release_value((yysemantic_stack_[(9) - (8)].aftype));
				release_value((yysemantic_stack_[(9) - (9)].atvtype));
			;}
    break;

  case 144:
#line 1457 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(5) - (5)].atvtype));
				RtToken sDummy = "";
				RtInt iDummy = 0;
				RtFloat fDummy = 0.0f;
				GET_CTX()->RiSubdivisionMeshV((yysemantic_stack_[(5) - (2)].stype), (yysemantic_stack_[(5) - (3)].aitype)->size(), &(*(yysemantic_stack_[(5) - (3)].aitype))[0], &(*(yysemantic_stack_[(5) - (4)].aitype))[0], 0, &sDummy, &iDummy, &iDummy, &fDummy, n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(5) - (2)].stype));
				release_value((yysemantic_stack_[(5) - (3)].aitype));
				release_value((yysemantic_stack_[(5) - (4)].aitype));
				release_value((yysemantic_stack_[(5) - (5)].atvtype));
			;}
    break;

  case 145:
#line 1471 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(10) - (10)].atvtype));

				RtToken sDummy = "";
				RtInt iDummy = 0;
				RtFloat fDummy = 0.0f;

				RtToken* array5 = &sDummy;
				if((yysemantic_stack_[(10) - (5)].astype)->size()){
					array5 = &(*(yysemantic_stack_[(10) - (5)].astype))[0];
				}
				RtInt* array6 = &iDummy;
				if((yysemantic_stack_[(10) - (6)].aitype)->size()){
					array6 = &(*(yysemantic_stack_[(10) - (6)].aitype))[0];
				}
				RtInt* array7 = &iDummy;
				if((yysemantic_stack_[(10) - (7)].aitype)->size()){
					array7 = &(*(yysemantic_stack_[(10) - (7)].aitype))[0];
				}
				RtFloat* array8 = &fDummy;
				if((yysemantic_stack_[(10) - (8)].aftype)->size()){
					array8 = &(*(yysemantic_stack_[(10) - (8)].aftype))[0];
				}
				RtString* array9 = &sDummy;
				if((yysemantic_stack_[(10) - (9)].astype)->size()){
					array9 = &(*(yysemantic_stack_[(10) - (9)].astype))[0];
				}

				GET_CTX()->RiHierarchicalSubdivisionMeshV((yysemantic_stack_[(10) - (2)].stype), (yysemantic_stack_[(10) - (3)].aitype)->size(), &(*(yysemantic_stack_[(10) - (3)].aitype))[0], &(*(yysemantic_stack_[(10) - (4)].aitype))[0], (yysemantic_stack_[(10) - (5)].astype)->size(), array5, array6, array7, array8, array9, n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(10) - (2)].stype));
				release_value((yysemantic_stack_[(10) - (3)].aitype));
				release_value((yysemantic_stack_[(10) - (4)].aitype));
				release_value((yysemantic_stack_[(10) - (5)].astype));
				release_value((yysemantic_stack_[(10) - (6)].aitype));
				release_value((yysemantic_stack_[(10) - (7)].aitype));
				release_value((yysemantic_stack_[(10) - (8)].aftype));
				release_value((yysemantic_stack_[(10) - (9)].astype));
				release_value((yysemantic_stack_[(10) - (10)].atvtype));
			;}
    break;

  case 146:
#line 1513 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(5) - (5)].atvtype));
				GET_CTX()->RiShaderLayerV((yysemantic_stack_[(5) - (2)].stype), (yysemantic_stack_[(5) - (3)].stype), (yysemantic_stack_[(5) - (4)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(5) - (2)].stype));
				release_value((yysemantic_stack_[(5) - (3)].stype));
				release_value((yysemantic_stack_[(5) - (4)].stype));
				release_value((yysemantic_stack_[(5) - (5)].atvtype));
			;}
    break;

  case 147:
#line 1524 "rib_parser.yy"
    {
				GET_CTX()->RiConnectShaderLayers((yysemantic_stack_[(6) - (2)].stype), (yysemantic_stack_[(6) - (3)].stype), (yysemantic_stack_[(6) - (4)].stype), (yysemantic_stack_[(6) - (5)].stype), (yysemantic_stack_[(6) - (6)].stype));
				release_value((yysemantic_stack_[(6) - (2)].stype));
				release_value((yysemantic_stack_[(6) - (3)].stype));
				release_value((yysemantic_stack_[(6) - (4)].stype));
				release_value((yysemantic_stack_[(6) - (5)].stype));
				release_value((yysemantic_stack_[(6) - (6)].stype));
			;}
    break;

  case 148:
#line 1533 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				GET_CTX()->RiProcedural2((yysemantic_stack_[(4) - (2)].stype), (yysemantic_stack_[(4) - (3)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(4) - (2)].stype));
				release_value((yysemantic_stack_[(4) - (3)].stype));
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 149:
#line 1543 "rib_parser.yy"
    {
				GET_CTX()->RiSystem((yysemantic_stack_[(2) - (2)].stype));
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 150:
#line 1548 "rib_parser.yy"
    {
				float_array& afv = *(yysemantic_stack_[(4) - (3)].aftype);
				RtBound b = {afv[0], afv[1], afv[2], afv[3], afv[4], afv[5]};
				GET_CTX()->RiVolume((yysemantic_stack_[(4) - (2)].stype), b, &(*(yysemantic_stack_[(4) - (4)].aitype))[0]);
				release_value((yysemantic_stack_[(4) - (2)].stype));
				release_value((yysemantic_stack_[(4) - (3)].aftype));
				release_value((yysemantic_stack_[(4) - (4)].aitype));
			;}
    break;

  case 151:
#line 1557 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiVPAtmosphereV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 152:
#line 1566 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiVPInteriorV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 153:
#line 1575 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(3) - (3)].atvtype));
				GET_CTX()->RiVPSurfaceV((yysemantic_stack_[(3) - (2)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(3) - (2)].stype));
				release_value((yysemantic_stack_[(3) - (3)].atvtype));
			;}
    break;

  case 154:
#line 1584 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				GET_CTX()->RiIntegratorV((yysemantic_stack_[(4) - (2)].stype), (yysemantic_stack_[(4) - (3)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(4) - (2)].stype));
				release_value((yysemantic_stack_[(4) - (3)].stype));
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 155:
#line 1594 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				GET_CTX()->RiBxdfV((yysemantic_stack_[(4) - (2)].stype), (yysemantic_stack_[(4) - (3)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(4) - (2)].stype));
				release_value((yysemantic_stack_[(4) - (3)].stype));
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 156:
#line 1604 "rib_parser.yy"
    {
				std::vector<RtToken> tokens;
				std::vector<void*> params;
				int n = expand_token_value(tokens,params,*(yysemantic_stack_[(4) - (4)].atvtype));
				GET_CTX()->RiPatternV((yysemantic_stack_[(4) - (2)].stype), (yysemantic_stack_[(4) - (3)].stype), n, &tokens[0], &params[0]);
				release_value((yysemantic_stack_[(4) - (2)].stype));
				release_value((yysemantic_stack_[(4) - (3)].stype));
				release_value((yysemantic_stack_[(4) - (4)].atvtype));
			;}
    break;

  case 157:
#line 1614 "rib_parser.yy"
    {
				;
			;}
    break;

  case 158:
#line 1618 "rib_parser.yy"
    {;;}
    break;

  case 159:
#line 1620 "rib_parser.yy"
    {
				// A values.has been found when a request is expected, quietly eat it...
			;}
    break;

  case 160:
#line 1625 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 161:
#line 1626 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 162:
#line 1627 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 163:
#line 1628 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 164:
#line 1629 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 165:
#line 1630 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 166:
#line 1631 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 167:
#line 1632 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 168:
#line 1633 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 169:
#line 1634 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 170:
#line 1635 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 171:
#line 1636 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 172:
#line 1637 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 173:
#line 1638 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 174:
#line 1639 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 175:
#line 1640 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 176:
#line 1641 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 177:
#line 1642 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 178:
#line 1643 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 179:
#line 1644 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 180:
#line 1645 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 181:
#line 1646 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 182:
#line 1647 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 183:
#line 1648 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 184:
#line 1649 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 185:
#line 1650 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 186:
#line 1651 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 187:
#line 1652 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 188:
#line 1653 "rib_parser.yy"
    {  REQ_PARAM();;}
    break;

  case 189:
#line 1654 "rib_parser.yy"
    {  REQ_PARAM();;}
    break;

  case 190:
#line 1655 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 191:
#line 1656 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 192:
#line 1657 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 193:
#line 1658 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 194:
#line 1659 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 195:
#line 1660 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 196:
#line 1661 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 197:
#line 1662 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 198:
#line 1663 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 199:
#line 1664 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 200:
#line 1665 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 201:
#line 1666 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 202:
#line 1667 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 203:
#line 1668 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 204:
#line 1669 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 205:
#line 1670 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 206:
#line 1671 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 207:
#line 1672 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 208:
#line 1673 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 209:
#line 1674 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 210:
#line 1675 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 211:
#line 1676 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 212:
#line 1677 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 213:
#line 1678 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 214:
#line 1679 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 215:
#line 1680 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 216:
#line 1681 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 217:
#line 1682 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 218:
#line 1683 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 219:
#line 1684 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 220:
#line 1685 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 221:
#line 1686 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 222:
#line 1687 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 223:
#line 1688 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 224:
#line 1689 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 225:
#line 1690 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 226:
#line 1691 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 227:
#line 1692 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 228:
#line 1693 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 229:
#line 1694 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 230:
#line 1695 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 231:
#line 1696 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 232:
#line 1697 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 233:
#line 1698 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 234:
#line 1699 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 235:
#line 1700 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 236:
#line 1701 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 237:
#line 1702 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 238:
#line 1703 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 239:
#line 1704 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 240:
#line 1705 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 241:
#line 1706 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 242:
#line 1707 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 243:
#line 1708 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 244:
#line 1709 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 245:
#line 1710 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 246:
#line 1711 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 247:
#line 1712 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 248:
#line 1713 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 249:
#line 1714 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 250:
#line 1715 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 251:
#line 1716 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 252:
#line 1717 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 253:
#line 1718 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 254:
#line 1719 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 255:
#line 1720 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 256:
#line 1721 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 257:
#line 1722 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 258:
#line 1723 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 259:
#line 1724 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 260:
#line 1725 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 261:
#line 1726 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 262:
#line 1727 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 263:
#line 1728 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 264:
#line 1729 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 265:
#line 1730 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 266:
#line 1731 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 267:
#line 1732 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 268:
#line 1733 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 269:
#line 1734 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 270:
#line 1735 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 271:
#line 1736 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 272:
#line 1737 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 273:
#line 1738 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 274:
#line 1739 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 275:
#line 1740 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 276:
#line 1741 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 277:
#line 1742 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 278:
#line 1743 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 279:
#line 1744 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 280:
#line 1745 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 281:
#line 1746 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 282:
#line 1747 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 283:
#line 1748 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 284:
#line 1749 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 285:
#line 1750 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 286:
#line 1751 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 287:
#line 1752 "rib_parser.yy"
    { REQ_PARAM(); ;}
    break;

  case 288:
#line 1755 "rib_parser.yy"
    {;}
    break;

  case 289:
#line 1757 "rib_parser.yy"
    {
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 290:
#line 1763 "rib_parser.yy"
    {;}
    break;

  case 291:
#line 1765 "rib_parser.yy"
    {
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 292:
#line 1771 "rib_parser.yy"
    {
				(yyval.itype) = (yysemantic_stack_[(1) - (1)].itype);
			;}
    break;

  case 293:
#line 1775 "rib_parser.yy"
    {
				(yyval.itype) = (yysemantic_stack_[(2) - (1)].itype);
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 294:
#line 1782 "rib_parser.yy"
    {
				(yyval.aitype) = new integer_array();
				(yyval.aitype)->push_back((yysemantic_stack_[(1) - (1)].itype));
			;}
    break;

  case 295:
#line 1787 "rib_parser.yy"
    {
				(yyval.aitype) = (yysemantic_stack_[(2) - (1)].aitype);
				(yyval.aitype)->push_back((yysemantic_stack_[(2) - (2)].itype));
			;}
    break;

  case 296:
#line 1794 "rib_parser.yy"
    {
				(yyval.aitype) = (yysemantic_stack_[(3) - (2)].aitype);
			;}
    break;

  case 297:
#line 1798 "rib_parser.yy"
    {
				(yyval.aitype) = (yysemantic_stack_[(2) - (1)].aitype);
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 299:
#line 1806 "rib_parser.yy"
    {
				(yyval.aitype) = new integer_array();
			;}
    break;

  case 300:
#line 1812 "rib_parser.yy"
    {
				(yyval.ftype) = (yysemantic_stack_[(1) - (1)].ftype);
			;}
    break;

  case 301:
#line 1816 "rib_parser.yy"
    {
				(yyval.ftype) = static_cast<float>((yysemantic_stack_[(1) - (1)].itype));
			;}
    break;

  case 302:
#line 1820 "rib_parser.yy"
    {
				(yyval.ftype) = (yysemantic_stack_[(2) - (1)].ftype);
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 303:
#line 1828 "rib_parser.yy"
    {
				(yyval.aftype) = new float_array();
				(yyval.aftype)->push_back((yysemantic_stack_[(1) - (1)].ftype));
			;}
    break;

  case 304:
#line 1833 "rib_parser.yy"
    {
				(yyval.aftype) = (yysemantic_stack_[(2) - (1)].aftype);
				(yyval.aftype)->push_back((yysemantic_stack_[(2) - (2)].ftype));
			;}
    break;

  case 305:
#line 1838 "rib_parser.yy"
    {
				(yyval.aftype) = (yysemantic_stack_[(2) - (1)].aftype);
				(yyval.aftype)->push_back(static_cast<float>((yysemantic_stack_[(2) - (2)].itype)));
			;}
    break;

  case 306:
#line 1843 "rib_parser.yy"
    {
				(yyval.aftype) = new float_array(*(yysemantic_stack_[(2) - (1)].aitype));
				(yyval.aftype)->push_back((yysemantic_stack_[(2) - (2)].ftype));
				release_value((yysemantic_stack_[(2) - (1)].aitype));
			;}
    break;

  case 307:
#line 1851 "rib_parser.yy"
    {
				(yyval.aftype) = (yysemantic_stack_[(3) - (2)].aftype);
			;}
    break;

  case 308:
#line 1855 "rib_parser.yy"
    {
				(yyval.aftype) = new float_array(*(yysemantic_stack_[(3) - (2)].aitype));
				release_value((yysemantic_stack_[(3) - (2)].aitype));
			;}
    break;

  case 309:
#line 1860 "rib_parser.yy"
    {
				(yyval.aftype) = (yysemantic_stack_[(2) - (1)].aftype);
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 310:
#line 1875 "rib_parser.yy"
    {
				(yyval.stype) = (yysemantic_stack_[(1) - (1)].stype);
			;}
    break;

  case 311:
#line 1879 "rib_parser.yy"
    {
				(yyval.stype) = (yysemantic_stack_[(2) - (1)].stype);
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 312:
#line 1886 "rib_parser.yy"
    {
				(yyval.stype) = (yysemantic_stack_[(1) - (1)].stype);
			;}
    break;

  case 313:
#line 1890 "rib_parser.yy"
    {
				(yyval.stype) = (yysemantic_stack_[(2) - (1)].stype);
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 314:
#line 1897 "rib_parser.yy"
    {
				(yyval.astype) = new string_array();
				(yyval.astype)->push_back((yysemantic_stack_[(1) - (1)].stype));
			;}
    break;

  case 315:
#line 1902 "rib_parser.yy"
    {
				(yyval.astype) = (yysemantic_stack_[(2) - (1)].astype);
				(yyval.astype)->push_back((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 316:
#line 1909 "rib_parser.yy"
    {
				(yyval.astype) = (yysemantic_stack_[(3) - (2)].astype);
			;}
    break;

  case 317:
#line 1913 "rib_parser.yy"
    {
				(yyval.astype) = (yysemantic_stack_[(2) - (1)].astype);
				release_value((yysemantic_stack_[(2) - (2)].stype));
			;}
    break;

  case 319:
#line 1921 "rib_parser.yy"
    {
				(yyval.astype) = new string_array();
			;}
    break;

  case 321:
#line 1928 "rib_parser.yy"
    {
				(yyval.aftype) = new float_array(*(yysemantic_stack_[(1) - (1)].aitype));
				release_value((yysemantic_stack_[(1) - (1)].aitype));
			;}
    break;

  case 323:
#line 1936 "rib_parser.yy"
    {
				(yyval.aftype) = new float_array();
			;}
    break;

  case 324:
#line 1942 "rib_parser.yy"
    {
				ri_temporary_classifier cls(GET_CLS());
				int id = cls.def((yysemantic_stack_[(2) - (1)].stype));
				if(id>=0){
					int nType = cls[id].get_type();
					switch(nType){
					case ri_classifier::INTEGER:
						(yyval.tvtype) = new token_value((yysemantic_stack_[(2) - (1)].stype), (yysemantic_stack_[(2) - (2)].itype));
						break;
					case ri_classifier::FLOAT:
					case ri_classifier::POINT:
					case ri_classifier::COLOR:
					case ri_classifier::HPOINT:
					case ri_classifier::NORMAL:
					case ri_classifier::VECTOR:
					case ri_classifier::MATRIX:
						(yyval.tvtype) = new token_value((yysemantic_stack_[(2) - (1)].stype), (float)(yysemantic_stack_[(2) - (2)].itype));
						break;
					default:
						yyerror("mispatch parameter");
						break;
					}
				}else{
					(yyval.tvtype) = new token_value((yysemantic_stack_[(2) - (1)].stype), (yysemantic_stack_[(2) - (2)].itype));
				}
				release_value((yysemantic_stack_[(2) - (1)].stype));
			;}
    break;

  case 325:
#line 1970 "rib_parser.yy"
    {
				ri_temporary_classifier cls(GET_CLS());
				int id = cls.def((yysemantic_stack_[(2) - (1)].stype));
				if(id>=0){
					int nType = cls[id].get_type();
					switch(nType){
						case ri_classifier::INTEGER:
							(yyval.tvtype) = new token_value((yysemantic_stack_[(2) - (1)].stype), (yysemantic_stack_[(2) - (2)].aitype));
							break;
						case ri_classifier::FLOAT:
						case ri_classifier::POINT:
						case ri_classifier::COLOR:
						case ri_classifier::HPOINT:
						case ri_classifier::NORMAL:
						case ri_classifier::VECTOR:
						case ri_classifier::MATRIX:
							(yyval.tvtype) = new token_value((yysemantic_stack_[(2) - (1)].stype), new float_array(*(yysemantic_stack_[(2) - (2)].aitype)));
							release_value((yysemantic_stack_[(2) - (2)].aitype));
							break;
						default:
							release_value((yysemantic_stack_[(2) - (2)].aitype));
							yyerror("mispatch parameter5");
					}
				}else{
					release_value((yysemantic_stack_[(2) - (2)].aitype));
					yyerror((yysemantic_stack_[(2) - (1)].stype));
					yyerror("mispatch parameter6");
				}
				release_value((yysemantic_stack_[(2) - (1)].stype));
			;}
    break;

  case 326:
#line 2001 "rib_parser.yy"
    {
				ri_temporary_classifier cls(GET_CLS());
				int id = cls.def((yysemantic_stack_[(2) - (1)].stype));
				if(id>=0){
					(yyval.tvtype) = new token_value((yysemantic_stack_[(2) - (1)].stype), (yysemantic_stack_[(2) - (2)].ftype));
				}else{
					yyerror("mispatch parameter1");
				}
				release_value((yysemantic_stack_[(2) - (1)].stype));
			;}
    break;

  case 327:
#line 2012 "rib_parser.yy"
    {
				ri_temporary_classifier cls(GET_CLS());
				int id = cls.def((yysemantic_stack_[(2) - (1)].stype));
				if(id>=0){
					(yyval.tvtype) = new token_value((yysemantic_stack_[(2) - (1)].stype), (yysemantic_stack_[(2) - (2)].aftype));
				}else{
					release_value((yysemantic_stack_[(2) - (2)].aftype));
					yyerror("mispatch parameter2");
				}
				release_value((yysemantic_stack_[(2) - (1)].stype));
			;}
    break;

  case 328:
#line 2024 "rib_parser.yy"
    {
				ri_temporary_classifier cls(GET_CLS());
				int id = cls.def((yysemantic_stack_[(2) - (1)].stype));
				if(id>=0){
					(yyval.tvtype) = new token_value((yysemantic_stack_[(2) - (1)].stype), (yysemantic_stack_[(2) - (2)].stype));
				}else{
					release_value((yysemantic_stack_[(2) - (2)].stype));
					yyerror("mispatch parameter3");
				}
				release_value((yysemantic_stack_[(2) - (1)].stype));
			;}
    break;

  case 329:
#line 2036 "rib_parser.yy"
    {
				ri_temporary_classifier cls(GET_CLS());
				int id = cls.def((yysemantic_stack_[(2) - (1)].stype));
				if(id>=0){
					(yyval.tvtype) = new token_value((yysemantic_stack_[(2) - (1)].stype), (yysemantic_stack_[(2) - (2)].astype));
				}else{
					release_value((yysemantic_stack_[(2) - (2)].astype));
					yyerror("mispatch parameter4");
				}
				release_value((yysemantic_stack_[(2) - (1)].stype));
			;}
    break;

  case 330:
#line 2051 "rib_parser.yy"
    {
				(yyval.atvtype) = new token_value_array();
				(yyval.atvtype)->push_back((yysemantic_stack_[(1) - (1)].tvtype));
			;}
    break;

  case 331:
#line 2056 "rib_parser.yy"
    {
				(yyval.atvtype) = new token_value_array();
				(yyval.atvtype)->push_back((yysemantic_stack_[(2) - (2)].tvtype));
			;}
    break;

  case 332:
#line 2061 "rib_parser.yy"
    {
				(yyval.atvtype) = (yysemantic_stack_[(2) - (1)].atvtype);
				(yyval.atvtype)->push_back((yysemantic_stack_[(2) - (2)].tvtype));
			;}
    break;

  case 334:
#line 2069 "rib_parser.yy"
    {
				(yyval.atvtype) = new token_value_array();
			;}
    break;

  case 335:
#line 2073 "rib_parser.yy"
    {
				(yyval.atvtype) = new token_value_array();
			;}
    break;


    /* Line 675 of lalr1.cc.  */
#line 3150 "rib_parser.cpp"
	default: break;
      }
    YY_SYMBOL_PRINT ("-> $$ =", yyr1_[yyn], &yyval, &yyloc);

    yypop_ (yylen);
    yylen = 0;
    YY_STACK_PRINT ();

    yysemantic_stack_.push (yyval);
    yylocation_stack_.push (yyloc);

    /* Shift the result of the reduction.  */
    yyn = yyr1_[yyn];
    yystate = yypgoto_[yyn - yyntokens_] + yystate_stack_[0];
    if (0 <= yystate && yystate <= yylast_
	&& yycheck_[yystate] == yystate_stack_[0])
      yystate = yytable_[yystate];
    else
      yystate = yydefgoto_[yyn - yyntokens_];
    goto yynewstate;

  /*------------------------------------.
  | yyerrlab -- here on detecting error |
  `------------------------------------*/
  yyerrlab:
    /* If not already recovering from an error, report this error.  */
    if (!yyerrstatus_)
      {
	++yynerrs_;
	error (yylloc, yysyntax_error_ (yystate, yytoken));
      }

    yyerror_range[0] = yylloc;
    if (yyerrstatus_ == 3)
      {
	/* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

	if (yychar <= yyeof_)
	  {
	  /* Return failure if at end of input.  */
	  if (yychar == yyeof_)
	    YYABORT;
	  }
	else
	  {
	    yydestruct_ ("Error: discarding", yytoken, &yylval, &yylloc);
	    yychar = yyempty_;
	  }
      }

    /* Else will try to reuse look-ahead token after shifting the error
       token.  */
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;

    yyerror_range[0] = yylocation_stack_[yylen - 1];
    /* Do not reclaim the symbols of the rule which action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    yystate = yystate_stack_[0];
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;	/* Each real token shifted decrements this.  */

    for (;;)
      {
	yyn = yypact_[yystate];
	if (yyn != yypact_ninf_)
	{
	  yyn += yyterror_;
	  if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
	    {
	      yyn = yytable_[yyn];
	      if (0 < yyn)
		break;
	    }
	}

	/* Pop the current state because it cannot handle the error token.  */
	if (yystate_stack_.height () == 1)
	YYABORT;

	yyerror_range[0] = yylocation_stack_[0];
	yydestruct_ ("Error: popping",
		     yystos_[yystate],
		     &yysemantic_stack_[0], &yylocation_stack_[0]);
	yypop_ ();
	yystate = yystate_stack_[0];
	YY_STACK_PRINT ();
      }

    if (yyn == yyfinal_)
      goto yyacceptlab;

    yyerror_range[1] = yylloc;
    // Using YYLLOC is tempting, but would change the location of
    // the look-ahead.  YYLOC is available though.
    YYLLOC_DEFAULT (yyloc, (yyerror_range - 1), 2);
    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yyloc);

    /* Shift the error token.  */
    YY_SYMBOL_PRINT ("Shifting", yystos_[yyn],
		   &yysemantic_stack_[0], &yylocation_stack_[0]);

    yystate = yyn;
    goto yynewstate;

    /* Accept.  */
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    /* Abort.  */
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (yychar != yyeof_ && yychar != yyempty_)
      yydestruct_ ("Cleanup: discarding lookahead", yytoken, &yylval, &yylloc);

    /* Do not reclaim the symbols of the rule which action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (yystate_stack_.height () != 1)
      {
	yydestruct_ ("Cleanup: popping",
		   yystos_[yystate_stack_[0]],
		   &yysemantic_stack_[0],
		   &yylocation_stack_[0]);
	yypop_ ();
      }

    return yyresult;
  }

  // Generate an error message.
  std::string
  rib_parser::yysyntax_error_ (int yystate, int tok)
  {
    std::string res;
    YYUSE (yystate);
#if YYERROR_VERBOSE
    int yyn = yypact_[yystate];
    if (yypact_ninf_ < yyn && yyn <= yylast_)
      {
	/* Start YYX at -YYN if negative to avoid negative indexes in
	   YYCHECK.  */
	int yyxbegin = yyn < 0 ? -yyn : 0;

	/* Stay within bounds of both yycheck and yytname.  */
	int yychecklim = yylast_ - yyn + 1;
	int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
	int count = 0;
	for (int x = yyxbegin; x < yyxend; ++x)
	  if (yycheck_[x + yyn] == x && x != yyterror_)
	    ++count;

	// FIXME: This method of building the message is not compatible
	// with internationalization.  It should work like yacc.c does it.
	// That is, first build a string that looks like this:
	// "syntax error, unexpected %s or %s or %s"
	// Then, invoke YY_ on this string.
	// Finally, use the string as a format to output
	// yytname_[tok], etc.
	// Until this gets fixed, this message appears in English only.
	res = "syntax error, unexpected ";
	res += yytnamerr_ (yytname_[tok]);
	if (count < 5)
	  {
	    count = 0;
	    for (int x = yyxbegin; x < yyxend; ++x)
	      if (yycheck_[x + yyn] == x && x != yyterror_)
		{
		  res += (!count++) ? ", expecting " : " or ";
		  res += yytnamerr_ (yytname_[x]);
		}
	  }
      }
    else
#endif
      res = YY_("syntax error");
    return res;
  }


  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
  const short int rib_parser::yypact_ninf_ = -558;
  const short int
  rib_parser::yypact_[] =
  {
      3745,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,    51,  3608,  -558,  -558,    81,  -558,    81,
      81,    81,  -558,  -558,    33,    96,    37,    88,    37,   -80,
     -80,    88,    81,    81,    88,    88,    81,    81,    88,    37,
      37,    88,    81,    81,    81,  -558,    81,  -558,    81,  -558,
    -558,    88,    81,    96,    88,    96,  -558,   -80,    81,    81,
      81,    81,    37,  -558,    81,  -558,    66,    81,    81,    81,
     -80,    33,    81,    81,    81,    81,    81,    96,   -80,  -558,
      96,    66,  -558,    66,    37,    81,    81,    88,    81,    81,
      88,    81,    88,    88,    81,    81,    81,  1538,   -80,   -80,
      20,    81,    81,    81,    88,    81,  -558,  -558,  -558,    88,
      88,    81,    88,    81,    88,    88,    96,    37,    81,  -558,
      88,    81,    81,    37,    88,   -80,  -558,  -558,  -558,    88,
     -80,    99,  -558,  -558,    81,    81,    81,    81,    81,    81,
      81,    81,    81,    81,    81,  -558,  -558,  -558,  -558,  1676,
     120,  1676,  1676,  -558,   104,   103,   108,    71,    96,  -558,
      36,  -558,   112,   109,  -558,   109,   109,  -558,   -80,  -558,
     109,   125,   125,   109,   109,   136,  1676,   109,   109,  -558,
     109,  -558,   109,  1676,  1676,   136,   125,   125,   109,  1676,
      77,   128,   112,   123,    67,    31,  1676,    40,  1676,   109,
    1538,   125,    77,    71,  1676,  1676,   120,   148,   151,   136,
     151,   136,   136,   136,   136,   136,   112,  -558,    77,   112,
     125,   112,   125,   109,  -558,  1676,   125,   109,    91,    71,
     128,   113,   109,   128,    40,    40,    40,    81,    25,  -558,
    1814,  -558,    50,    50,    81,  1952,  1676,    71,   125,   128,
     136,   109,   109,   125,   109,   125,   128,   109,   112,   109,
    -558,   125,   109,    40,  1676,   109,  -558,   109,  -558,   109,
      50,  -558,   128,   131,   136,   136,   136,   125,    40,  1676,
    1676,  1676,   136,   136,   136,  -558,  -558,  2090,  1676,  -558,
    -558,  -558,  -558,  -558,    19,    23,  -558,  -558,    47,    47,
    -558,    50,  -558,   109,   128,   109,  -558,   109,   109,   109,
     125,  -558,   109,   109,   109,   109,  -558,  -558,   136,   128,
    -558,   117,    -4,  2228,   128,  -558,  -558,    50,  -558,   109,
    -558,   112,   112,  -558,  -558,  2090,  1676,  -558,     2,  -558,
    1676,  1676,  1676,   136,   136,   136,  1676,   136,    36,   109,
    -558,   109,  2366,   156,   109,   128,   158,    52,  1538,  -558,
    -558,  -558,  -558,    63,   103,   108,   133,  -558,  -558,    50,
      67,  -558,    77,  1676,   109,   109,   109,   128,   109,   109,
      50,  -558,   109,   109,   109,    50,  -558,   136,   136,  1676,
     -80,  -558,  -558,  -558,  1676,  1676,  1676,  -558,  -558,  -558,
    -558,   170,   112,  -558,  -558,   170,    71,    96,    71,    96,
     104,    53,   109,   128,  2504,   109,   109,   128,   109,   109,
    2504,  1676,   128,   170,    50,   109,  -558,  -558,  -558,   170,
    -558,  -558,  -558,   136,   136,   113,  -558,   136,    88,   128,
     109,    71,   128,  1676,  1538,  -558,    67,  2642,    77,  -558,
     109,   128,   109,   109,   109,    14,   109,   109,   128,   -80,
    1676,   136,  -558,   103,  -558,  -558,  -558,  -558,   112,   112,
     112,   112,    19,    45,   133,  1538,   109,  -558,   128,  2504,
     109,   128,  -558,  -558,   -80,   109,   136,   136,   109,   136,
     109,  2504,   156,  -558,  -558,  2780,   117,   128,   128,   109,
    2504,   -80,  2918,   109,   109,   -80,  -558,   136,   170,   170,
    -558,   109,  -558,   109,    41,   103,   -80,   109,   113,   136,
    2504,   113,   124,  -558,    91,   128,   109,  -558,   -80,   109,
    2504,   -80,   125,   128,   128,   170,   -80,   109,   109,   136,
    -558,   109,    77,  3056,   109,   -80,   109,  -558,    50,    10,
    -558,   -80,  2504,  2504,   113,  2504,    36,   128,    20,   109,
     -80,   170,    20,  -558,  -558,   168,  -558,    88,  3194,   128,
     -80,  3332,   113,   109,  -558,   109,   100,  2504,  3470,  -558
  };

  /* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
     doesn't specify something else to do.  Zero means the default is an
     error.  */
  const unsigned short int
  rib_parser::yydefact_[] =
  {
         0,   158,   157,   159,     7,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   237,   178,   179,   180,   181,   182,   183,
     184,   185,   186,   187,   188,   189,   190,   191,   192,   193,
     194,   195,   196,   197,   198,   199,   200,   201,   202,   203,
     204,   205,   206,   207,   208,   209,   210,   211,   212,   213,
     214,   215,   216,   217,   218,   219,   220,   221,   222,   223,
     224,   225,   226,   227,   228,   229,   230,   231,   232,   233,
     234,   235,   236,   240,   241,   242,   243,   238,   239,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,     0,     0,     4,     6,     0,    99,     0,
       0,     0,    38,    39,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,    21,     0,
       0,     0,     0,     0,     0,    27,     0,   142,     0,   140,
     141,     0,     0,     0,     0,     0,    12,     0,     0,     0,
       0,     0,     0,    72,     0,    51,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   130,
       0,     0,   126,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    90,    91,    70,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   123,
       0,     0,     0,     0,     0,     0,    87,    88,    86,     0,
       0,     0,    13,    14,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     1,     3,     5,   310,     0,
       0,     0,     0,   288,     0,   321,   320,     0,     0,   292,
       0,   300,   301,     0,    62,     0,     0,    40,     0,    74,
       0,    83,    85,     0,     0,     0,     0,     0,     0,    64,
       0,    66,     0,     0,     0,     0,    28,   139,     0,     0,
       0,    16,    11,     0,     0,     0,     0,     0,     0,     0,
       0,    50,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    60,   129,     0,   124,
     125,   127,   128,     0,    42,     0,    69,     0,     0,     0,
      75,     0,     0,    24,     0,     0,     0,   335,     0,   330,
       0,   102,     0,     0,     0,     0,     0,     0,    97,    36,
       0,     0,     0,    84,     0,    59,    58,     0,    71,     0,
      80,   122,     0,     0,     0,     0,    45,     0,    73,     0,
       0,   312,     9,     8,     0,     0,     0,   149,     0,     0,
       0,     0,     0,     0,     0,   311,    98,     0,     0,    55,
      92,   303,   294,   289,     0,     0,   297,   309,     0,     0,
     293,     0,   302,     0,    20,     0,    35,     0,     0,     0,
      10,    81,     0,     0,     0,     0,    82,    32,     0,    29,
      57,     0,     0,     0,    67,    68,   121,     0,    34,     0,
     116,    52,    53,    30,    56,     0,     0,   314,     0,   317,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      37,     0,     0,     0,     0,    25,     0,     0,     0,   331,
     326,   324,   328,     0,   325,   327,   329,   333,   332,     0,
       0,    19,     0,     0,     0,     0,     0,    23,     0,     0,
       0,    54,     0,     0,     0,     0,   313,     0,     0,     0,
       0,   151,   152,   153,     0,     0,     0,    48,    49,   306,
     290,   296,   295,   304,   305,   307,     0,     0,     0,     0,
       0,     0,     0,    41,     0,     0,     0,    22,     0,     0,
       0,     0,    15,   296,     0,     0,    46,    47,   315,   316,
     136,   137,   138,     0,     0,     0,   135,     0,     0,    43,
       0,     0,    26,     0,     0,   101,     0,     0,     0,    89,
       0,    78,     0,     0,     0,     0,     0,     0,    76,     0,
       0,     0,   148,   150,   154,   155,   156,   291,   105,   106,
     107,   108,     0,     0,   318,     0,     0,   114,    18,     0,
       0,    65,   119,    33,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    95,   100,     0,     0,    77,    17,     0,
       0,     0,     0,     0,     0,     0,   146,     0,   308,   319,
      96,     0,   115,     0,     0,   298,     0,     0,     0,     0,
       0,     0,     0,   118,     0,    31,     0,   113,     0,     0,
       0,     0,   147,    61,    63,   299,     0,     0,     0,     0,
     133,     0,     0,     0,     0,     0,     0,   120,     0,     0,
     322,     0,     0,     0,     0,     0,     0,    79,     0,     0,
       0,   323,     0,   117,   132,     0,   131,     0,     0,    44,
       0,     0,     0,     0,   112,     0,     0,     0,     0,   134
  };

  /* YYPGOTO[NTERM-NUM].  */
  const short int
  rib_parser::yypgoto_[] =
  {
      -558,  -558,  -558,    48,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,  -558,
    -558,  -558,  -558,   527,  -406,   569,  -311,   570,  -557,  -147,
    -558,  -340,   397,  -558,  -558,  -185,  -477,   537,  -484,  -318,
    -200,   848
  };

  /* YYDEFGOTO[NTERM-NUM].  */
  const short int
  rib_parser::yydefgoto_[] =
  {
        -1,   133,   134,   135,   136,   137,   138,   139,   140,   141,
     142,   143,   144,   145,   146,   147,   148,   149,   150,   151,
     152,   153,   154,   155,   156,   157,   158,   159,   160,   161,
     162,   163,   164,   165,   166,   167,   168,   169,   170,   171,
     172,   173,   174,   175,   176,   177,   178,   179,   180,   181,
     182,   183,   184,   185,   186,   187,   188,   189,   190,   191,
     192,   193,   194,   195,   196,   197,   198,   199,   200,   201,
     202,   203,   204,   205,   206,   207,   208,   209,   210,   211,
     212,   213,   214,   215,   216,   217,   218,   219,   220,   221,
     222,   223,   224,   225,   226,   227,   228,   229,   230,   231,
     232,   233,   234,   235,   236,   237,   238,   239,   240,   241,
     242,   243,   244,   245,   246,   247,   248,   249,   250,   251,
     252,   253,   254,   255,   256,   257,   258,   259,   260,   261,
     262,   263,   264,   274,   521,   282,   414,   275,   636,   283,
     415,   276,   358,   393,   458,   594,   595,   670,   671,   359,
     360,   361
  };

  /* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule which
     number is the opposite.  If zero, do what YYDEFACT says.  */
  const short int rib_parser::yytable_ninf_ = -337;
  const short int
  rib_parser::yytable_[] =
  {
       285,   286,   442,   279,   290,   328,   330,   293,   294,   525,
     548,   297,   298,   300,   302,   364,   411,   412,   485,   413,
     365,   364,   268,   416,   308,   519,   279,   311,   268,   523,
     524,   480,   481,   482,   405,   319,   543,   281,   279,   479,
     405,   268,   488,   281,   279,   420,   479,   488,   412,   405,
     413,   265,   549,   457,   413,   268,   420,   343,   273,   416,
     347,   459,   417,   350,   648,   352,   353,   604,   364,   411,
     412,   457,   413,   279,   268,   268,   416,   369,   279,   656,
     405,   531,   371,   372,   279,   374,   420,   376,   377,   268,
     379,   665,   364,   382,   281,   279,   385,   387,   621,   268,
     405,   364,   389,   279,   392,   281,   279,   391,   268,   422,
     411,   412,   416,   413,   443,   281,   279,   417,   422,   281,
     279,   420,   405,   281,   279,   488,   420,   279,   268,   405,
     412,   279,   413,   422,   405,   520,   423,   422,   424,   425,
     506,   520,   459,   427,   268,   405,   428,   429,   472,   520,
     432,   433,   273,   434,   488,   435,   457,   413,   520,   268,
     459,   439,   520,   273,   268,   420,   268,   416,   444,   273,
     477,   273,   449,   486,   273,   273,   268,   422,   273,   587,
     520,   678,   267,     0,   520,   273,   628,   629,   273,     0,
     273,   273,     0,     0,   682,     0,   469,     0,     0,     0,
     471,     0,     0,     0,   474,   475,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   592,
       0,     0,     0,     0,   494,   495,     0,   496,   655,     0,
     497,     0,   498,     0,     0,   499,     0,     0,   502,     0,
     503,     0,   504,     0,     0,     0,     0,     0,     0,   488,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   681,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   532,     0,   533,     0,
     534,   535,   536,     0,     0,   537,   538,   539,   540,     0,
     567,     0,     0,     0,   542,     0,     0,   488,     0,     0,
       0,     0,   545,     0,   488,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   559,   442,   560,     0,     0,   562,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   488,     0,   570,   571,   572,
       0,   573,   574,     0,     0,   576,   577,   578,     0,     0,
       0,     0,     0,     0,     0,     0,   615,     0,     0,     0,
     488,     0,     0,   488,     0,   622,     0,     0,     0,     0,
     488,     0,     0,     0,     0,   596,     0,     0,   598,   599,
       0,   600,   601,     0,     0,     0,     0,     0,   605,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   608,     0,
       0,   610,     0,   611,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   617,     0,   618,   619,   620,     0,   623,
     624,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   663,     0,     0,     0,     0,   631,
       0,     0,     0,   633,     0,     0,     0,     0,   637,     0,
       0,   640,     0,   642,     0,     0,     0,     0,     0,   645,
       0,     0,   646,     0,     0,     0,   649,   650,   688,     0,
       0,     0,   691,     0,   653,     0,   654,     0,     0,     0,
     657,   658,     0,     0,   661,     0,   698,     0,     0,   664,
       0,     0,   666,     0,     0,     0,     0,     0,     0,     0,
     672,   673,     0,     0,   675,     0,     0,   677,     0,   679,
       0,     0,     0,     0,     0,     0,     0,   685,     0,     0,
       0,     0,   689,     0,   269,     0,   270,   271,   272,     0,
     693,   277,     0,     0,     0,   695,   696,     0,   697,   291,
     292,     0,     0,   295,   296,     0,     0,     0,     0,   303,
     304,   305,     0,   306,     0,   307,     0,     0,     0,   309,
       0,     0,     0,     0,     0,   315,   316,   317,   318,     0,
       0,   321,     0,   323,   324,   325,   326,     0,   329,   331,
     332,   333,   334,   335,     0,     0,     0,     0,   340,     0,
     342,     0,   345,   346,     0,   348,   349,     0,   351,     0,
       0,   354,   355,   356,     0,     0,     0,     0,   366,   367,
     368,     0,   370,     0,     0,     0,     0,     0,   373,     0,
     375,     0,     0,     0,     0,   381,     0,     0,   383,   384,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   394,   395,   396,   397,   398,   399,   400,   401,   402,
     403,   404,     0,     0,     0,     0,     0,   408,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   278,     0,   284,     0,   287,   288,   289,     0,     0,
       0,     0,   430,     0,     0,     0,   299,   301,     0,     0,
       0,     0,   438,     0,   313,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   280,     0,     0,   327,   327,   320,
       0,     0,     0,   456,     0,   460,   461,   462,   463,   464,
     465,   466,   467,     0,     0,   337,     0,     0,     0,     0,
       0,   344,   310,     0,   312,   313,   313,   314,     0,     0,
       0,     0,     0,     0,     0,   322,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   336,   493,     0,   338,
     339,     0,   341,     0,   380,     0,     0,   313,     0,     0,
     386,     0,   388,     0,     0,     0,     0,     0,   362,   363,
       0,   507,   508,   509,     0,     0,     0,     0,     0,   514,
     515,   516,     0,     0,     0,   378,     0,   313,     0,     0,
       0,     0,     0,     0,     0,   526,   528,     0,     0,     0,
     390,     0,     0,     0,     0,   426,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   541,     0,     0,     0,   407,
       0,     0,     0,     0,   313,     0,   418,   419,     0,     0,
     421,     0,   445,     0,     0,     0,     0,     0,     0,     0,
     553,   554,   555,     0,   557,     0,     0,     0,     0,     0,
     561,     0,     0,   563,     0,     0,     0,     0,     0,   441,
       0,   313,   327,     0,     0,   483,     0,   447,     0,   313,
     313,   451,   452,   478,     0,   455,     0,     0,     0,     0,
       0,     0,     0,     0,   580,   581,     0,   468,     0,     0,
     313,     0,     0,     0,     0,     0,     0,   313,   473,     0,
       0,     0,     0,     0,   476,     0,     0,     0,   484,     0,
       0,     0,   489,   490,     0,   510,   492,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   530,     0,
     606,   607,     0,   500,   609,   527,   529,     0,     0,     0,
     505,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,   313,     0,     0,     0,   627,     0,
       0,     0,     0,   522,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   638,   639,   558,   641,     0,     0,   644,
       0,   522,     0,     0,   564,     0,   313,   544,     0,     0,
       0,     0,     0,     0,   652,     0,     0,   313,     0,     0,
       0,     0,     0,     0,     0,     0,   659,   313,     0,     0,
       0,     0,   579,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   674,     0,   593,   566,
       0,   568,     0,     0,     0,     0,     0,     0,     0,     0,
     575,   593,     0,     0,     0,     0,     0,     0,     0,     0,
     583,     0,   692,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   588,   589,   590,   591,     0,
       0,     0,   593,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   625,   406,     0,   409,
     410,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     612,   634,     0,     0,     0,     0,     0,   616,     0,     0,
       0,     0,     0,     0,   431,     0,     0,     0,   634,     0,
       0,   436,   437,     0,     0,     0,     0,   440,     0,     0,
       0,   522,   651,   634,   446,     0,   448,     0,   450,     0,
       0,     0,   453,   454,   635,   634,     0,     0,   313,     0,
       0,     0,     0,   669,     0,     0,     0,     0,     0,     0,
       0,   635,   669,   470,     0,     0,     0,     0,   593,     0,
       0,     0,     0,     0,     0,   680,   635,     0,     0,     0,
       0,   662,     0,   687,   491,     0,     0,   690,   635,     0,
       0,   668,     0,     0,     0,     0,     0,   694,     0,     0,
       0,   676,   501,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   511,   512,   513,
       0,     0,     0,     0,     0,   517,   518,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   546,   547,     0,     0,     0,   550,   551,
     552,     0,     0,     0,   556,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,   565,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   569,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   582,     0,     0,
       0,     0,   584,   585,   586,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,   597,     0,     0,     0,     0,     0,   602,   603,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,   613,   614,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   626,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   630,     0,     0,     0,   632,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   643,
       0,     0,     0,     0,     0,     0,     0,     0,   647,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   660,     0,
       0,     0,     0,     0,     0,     0,     0,     0,   667,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     683,   684,     0,   686,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,  -334,   357,
       0,  -334,  -334,  -334,     0,   699,   268,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,   357,     0,  -334,
    -334,  -334,     0,     0,   268,   405,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -336,   487,     0,  -336,  -336,  -336,
       0,     0,   268,  -336,  -336,  -336,  -336,  -336,  -336,  -336,
    -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,
    -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,
    -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,
    -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,
    -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,
    -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,
    -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,
    -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,
    -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,
    -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,
    -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,
    -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,  -336,
    -336,  -336,   -93,   487,     0,   -93,   -93,   -93,     0,     0,
     268,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,
     -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,
     -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,
     -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,
     -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,
     -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,
     -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,
     -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,
     -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,
     -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,
     -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,
     -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,
     -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,   -93,
    -334,   357,     0,  -334,  -334,  -334,     0,     0,   268,   420,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,   -94,   487,
       0,   -94,   -94,   -94,     0,     0,   268,   -94,   -94,   -94,
     -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,
     -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,
     -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,
     -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,
     -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,
     -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,
     -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,
     -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,
     -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,
     -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,
     -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,
     -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,   -94,
     -94,   -94,   -94,   -94,   -94,   -94,  -109,   487,     0,  -109,
    -109,  -109,     0,     0,   268,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,  -109,
    -109,  -109,  -109,  -109,  -334,   357,     0,  -334,  -334,  -334,
       0,     0,   268,   422,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,  -334,
    -334,  -334,  -103,   487,     0,  -103,  -103,  -103,     0,     0,
     268,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,  -103,
    -104,   487,     0,  -104,  -104,  -104,     0,     0,   268,  -104,
    -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,
    -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,
    -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,
    -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,
    -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,
    -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,
    -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,
    -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,
    -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,
    -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,
    -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,
    -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,
    -104,  -104,  -104,  -104,  -104,  -104,  -104,  -104,  -144,   487,
       0,  -144,  -144,  -144,     0,     0,   268,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,  -144,
    -144,  -144,  -144,  -144,  -144,  -144,  -110,   487,     0,  -110,
    -110,  -110,     0,     0,   268,  -110,  -110,  -110,  -110,  -110,
    -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,
    -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,
    -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,
    -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,
    -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,
    -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,
    -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,
    -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,
    -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,
    -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,
    -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,
    -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,  -110,
    -110,  -110,  -110,  -110,  -143,   487,     0,  -143,  -143,  -143,
       0,     0,   268,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,  -143,
    -143,  -143,  -145,   487,     0,  -145,  -145,  -145,     0,     0,
     268,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,
    -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,
    -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,
    -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,
    -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,
    -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,
    -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,
    -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,
    -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,
    -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,
    -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,
    -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,
    -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,  -145,
    -111,   487,     0,  -111,  -111,  -111,     0,     0,   268,  -111,
    -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,
    -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,
    -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,
    -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,
    -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,
    -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,
    -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,
    -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,
    -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,
    -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,
    -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,
    -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,
    -111,  -111,  -111,  -111,  -111,  -111,  -111,  -111,    -2,     1,
       0,   266,     2,     3,     0,     0,     0,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    71,    72,    73,    74,    75,    76,
      77,    78,    79,    80,    81,    82,    83,    84,    85,    86,
      87,    88,    89,    90,    91,    92,    93,    94,    95,    96,
      97,    98,    99,   100,   101,   102,   103,   104,   105,   106,
     107,   108,   109,   110,   111,   112,   113,   114,   115,   116,
     117,   118,   119,   120,   121,   122,   123,   124,   125,   126,
     127,   128,   129,   130,   131,   132,     1,     0,     0,     2,
       3,     0,     0,     0,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132
  };

  /* YYCHECK.  */
  const short int
  rib_parser::yycheck_[] =
  {
       147,   148,   313,     7,   151,   190,   191,   154,   155,   415,
       8,   158,   159,   160,   161,     1,     6,     7,   358,     9,
     220,     1,     8,     9,   171,     6,     7,   174,     8,     6,
       7,     6,     7,     8,     9,   182,   442,     6,     7,   357,
       9,     8,   360,     6,     7,     9,   364,   365,     7,     9,
       9,     0,   458,     8,     9,     8,     9,   204,   138,     9,
     207,     9,     9,   210,   621,   212,   213,   544,     1,     6,
       7,     8,     9,     7,     8,     8,     9,   224,     7,   636,
       9,   421,   229,   230,     7,   232,     9,   234,   235,     8,
     237,   648,     1,   240,     6,     7,   243,   244,   575,     8,
       9,     1,   249,     7,   251,     6,     7,     8,     8,     9,
       6,     7,     9,     9,   314,     6,     7,     9,     9,     6,
       7,     9,     9,     6,     7,   443,     9,     7,     8,     9,
       7,     7,     9,     9,     9,   139,   283,     9,   285,   286,
       9,   139,     9,   290,     8,     9,   293,   294,   348,   139,
     297,   298,   138,   300,   472,   302,     8,     9,   139,     8,
       9,   308,   139,   138,     8,     9,     8,     9,   315,   138,
     355,   138,   319,   358,   138,   138,     8,     9,   138,     9,
     139,   665,   134,    -1,   139,   138,   592,   593,   138,    -1,
     138,   138,    -1,    -1,   671,    -1,   343,    -1,    -1,    -1,
     347,    -1,    -1,    -1,   351,   352,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   530,
      -1,    -1,    -1,    -1,   371,   372,    -1,   374,   634,    -1,
     377,    -1,   379,    -1,    -1,   382,    -1,    -1,   385,    -1,
     387,    -1,   389,    -1,    -1,    -1,    -1,    -1,    -1,   567,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   669,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   423,    -1,   425,    -1,
     427,   428,   429,    -1,    -1,   432,   433,   434,   435,    -1,
     490,    -1,    -1,    -1,   441,    -1,    -1,   615,    -1,    -1,
      -1,    -1,   449,    -1,   622,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   469,   634,   471,    -1,    -1,   474,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   663,    -1,   494,   495,   496,
      -1,   498,   499,    -1,    -1,   502,   503,   504,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   566,    -1,    -1,    -1,
     688,    -1,    -1,   691,    -1,   575,    -1,    -1,    -1,    -1,
     698,    -1,    -1,    -1,    -1,   532,    -1,    -1,   535,   536,
      -1,   538,   539,    -1,    -1,    -1,    -1,    -1,   545,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   555,    -1,
      -1,   558,    -1,   560,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   570,    -1,   572,   573,   574,    -1,   576,
     577,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   644,    -1,    -1,    -1,    -1,   596,
      -1,    -1,    -1,   600,    -1,    -1,    -1,    -1,   605,    -1,
      -1,   608,    -1,   610,    -1,    -1,    -1,    -1,    -1,   616,
      -1,    -1,   619,    -1,    -1,    -1,   623,   624,   678,    -1,
      -1,    -1,   682,    -1,   631,    -1,   633,    -1,    -1,    -1,
     637,   638,    -1,    -1,   641,    -1,   696,    -1,    -1,   646,
      -1,    -1,   649,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     657,   658,    -1,    -1,   661,    -1,    -1,   664,    -1,   666,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   674,    -1,    -1,
      -1,    -1,   679,    -1,   137,    -1,   139,   140,   141,    -1,
     687,   144,    -1,    -1,    -1,   692,   693,    -1,   695,   152,
     153,    -1,    -1,   156,   157,    -1,    -1,    -1,    -1,   162,
     163,   164,    -1,   166,    -1,   168,    -1,    -1,    -1,   172,
      -1,    -1,    -1,    -1,    -1,   178,   179,   180,   181,    -1,
      -1,   184,    -1,   186,   187,   188,   189,    -1,   191,   192,
     193,   194,   195,   196,    -1,    -1,    -1,    -1,   201,    -1,
     203,    -1,   205,   206,    -1,   208,   209,    -1,   211,    -1,
      -1,   214,   215,   216,    -1,    -1,    -1,    -1,   221,   222,
     223,    -1,   225,    -1,    -1,    -1,    -1,    -1,   231,    -1,
     233,    -1,    -1,    -1,    -1,   238,    -1,    -1,   241,   242,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   254,   255,   256,   257,   258,   259,   260,   261,   262,
     263,   264,    -1,    -1,    -1,    -1,    -1,   270,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   144,    -1,   146,    -1,   148,   149,   150,    -1,    -1,
      -1,    -1,   295,    -1,    -1,    -1,   159,   160,    -1,    -1,
      -1,    -1,   305,    -1,   177,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   145,    -1,    -1,   190,   191,   182,
      -1,    -1,    -1,   326,    -1,   328,   329,   330,   331,   332,
     333,   334,   335,    -1,    -1,   198,    -1,    -1,    -1,    -1,
      -1,   204,   173,    -1,   175,   218,   219,   177,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   186,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   197,   370,    -1,   200,
     201,    -1,   203,    -1,   237,    -1,    -1,   250,    -1,    -1,
     243,    -1,   245,    -1,    -1,    -1,    -1,    -1,   218,   219,
      -1,   394,   395,   396,    -1,    -1,    -1,    -1,    -1,   402,
     403,   404,    -1,    -1,    -1,   236,    -1,   280,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   418,   419,    -1,    -1,    -1,
     250,    -1,    -1,    -1,    -1,   288,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   438,    -1,    -1,    -1,   270,
      -1,    -1,    -1,    -1,   317,    -1,   277,   278,    -1,    -1,
     280,    -1,   315,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     463,   464,   465,    -1,   467,    -1,    -1,    -1,    -1,    -1,
     473,    -1,    -1,   476,    -1,    -1,    -1,    -1,    -1,   310,
      -1,   354,   355,    -1,    -1,   358,    -1,   317,    -1,   362,
     363,   322,   323,   356,    -1,   326,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   507,   508,    -1,   338,    -1,    -1,
     383,    -1,    -1,    -1,    -1,    -1,    -1,   390,   349,    -1,
      -1,    -1,    -1,    -1,   354,    -1,    -1,    -1,   358,    -1,
      -1,    -1,   362,   363,    -1,   398,   367,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   421,    -1,
     553,   554,    -1,   383,   557,   418,   419,    -1,    -1,    -1,
     390,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,   447,    -1,    -1,    -1,   581,    -1,
      -1,    -1,    -1,   414,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   606,   607,   468,   609,    -1,    -1,   612,
      -1,   442,    -1,    -1,   477,    -1,   489,   447,    -1,    -1,
      -1,    -1,    -1,    -1,   627,    -1,    -1,   500,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   639,   510,    -1,    -1,
      -1,    -1,   505,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   659,    -1,   531,   489,
      -1,   492,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     500,   544,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     510,    -1,   685,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   526,   527,   528,   529,    -1,
      -1,    -1,   575,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   579,   269,    -1,   271,
     272,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     561,   604,    -1,    -1,    -1,    -1,    -1,   568,    -1,    -1,
      -1,    -1,    -1,    -1,   296,    -1,    -1,    -1,   621,    -1,
      -1,   303,   304,    -1,    -1,    -1,    -1,   309,    -1,    -1,
      -1,   592,   625,   636,   316,    -1,   318,    -1,   320,    -1,
      -1,    -1,   324,   325,   604,   648,    -1,    -1,   651,    -1,
      -1,    -1,    -1,   656,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   621,   665,   345,    -1,    -1,    -1,    -1,   671,    -1,
      -1,    -1,    -1,    -1,    -1,   668,   636,    -1,    -1,    -1,
      -1,   642,    -1,   676,   366,    -1,    -1,   680,   648,    -1,
      -1,   651,    -1,    -1,    -1,    -1,    -1,   690,    -1,    -1,
      -1,   662,   384,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   399,   400,   401,
      -1,    -1,    -1,    -1,    -1,   407,   408,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   455,   456,    -1,    -1,    -1,   460,   461,
     462,    -1,    -1,    -1,   466,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,   478,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   493,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,   509,    -1,    -1,
      -1,    -1,   514,   515,   516,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,   534,    -1,    -1,    -1,    -1,    -1,   540,   541,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,   563,   564,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   580,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,   595,    -1,    -1,    -1,   599,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   611,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   620,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   640,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,   650,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     672,   673,    -1,   675,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,     0,     1,
      -1,     3,     4,     5,    -1,   697,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,     0,     1,    -1,     3,
       4,     5,    -1,    -1,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,     0,     1,    -1,     3,     4,     5,
      -1,    -1,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,     0,     1,    -1,     3,     4,     5,    -1,    -1,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
       0,     1,    -1,     3,     4,     5,    -1,    -1,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,     0,     1,
      -1,     3,     4,     5,    -1,    -1,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,     0,     1,    -1,     3,
       4,     5,    -1,    -1,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,     0,     1,    -1,     3,     4,     5,
      -1,    -1,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,     0,     1,    -1,     3,     4,     5,    -1,    -1,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
       0,     1,    -1,     3,     4,     5,    -1,    -1,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,     0,     1,
      -1,     3,     4,     5,    -1,    -1,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,     0,     1,    -1,     3,
       4,     5,    -1,    -1,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    17,    18,    19,    20,    21,    22,    23,
      24,    25,    26,    27,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,    41,    42,    43,
      44,    45,    46,    47,    48,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    71,    72,    73,
      74,    75,    76,    77,    78,    79,    80,    81,    82,    83,
      84,    85,    86,    87,    88,    89,    90,    91,    92,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     104,   105,   106,   107,   108,   109,   110,   111,   112,   113,
     114,   115,   116,   117,   118,   119,   120,   121,   122,   123,
     124,   125,   126,   127,   128,   129,   130,   131,   132,   133,
     134,   135,   136,   137,     0,     1,    -1,     3,     4,     5,
      -1,    -1,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    17,    18,    19,    20,    21,    22,    23,    24,    25,
      26,    27,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    38,    39,    40,    41,    42,    43,    44,    45,
      46,    47,    48,    49,    50,    51,    52,    53,    54,    55,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
      66,    67,    68,    69,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,    87,    88,    89,    90,    91,    92,    93,    94,    95,
      96,    97,    98,    99,   100,   101,   102,   103,   104,   105,
     106,   107,   108,   109,   110,   111,   112,   113,   114,   115,
     116,   117,   118,   119,   120,   121,   122,   123,   124,   125,
     126,   127,   128,   129,   130,   131,   132,   133,   134,   135,
     136,   137,     0,     1,    -1,     3,     4,     5,    -1,    -1,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    17,
      18,    19,    20,    21,    22,    23,    24,    25,    26,    27,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,    41,    42,    43,    44,    45,    46,    47,
      48,    49,    50,    51,    52,    53,    54,    55,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    71,    72,    73,    74,    75,    76,    77,
      78,    79,    80,    81,    82,    83,    84,    85,    86,    87,
      88,    89,    90,    91,    92,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,   104,   105,   106,   107,
     108,   109,   110,   111,   112,   113,   114,   115,   116,   117,
     118,   119,   120,   121,   122,   123,   124,   125,   126,   127,
     128,   129,   130,   131,   132,   133,   134,   135,   136,   137,
       0,     1,    -1,     3,     4,     5,    -1,    -1,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    38,    39,
      40,    41,    42,    43,    44,    45,    46,    47,    48,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    71,    72,    73,    74,    75,    76,    77,    78,    79,
      80,    81,    82,    83,    84,    85,    86,    87,    88,    89,
      90,    91,    92,    93,    94,    95,    96,    97,    98,    99,
     100,   101,   102,   103,   104,   105,   106,   107,   108,   109,
     110,   111,   112,   113,   114,   115,   116,   117,   118,   119,
     120,   121,   122,   123,   124,   125,   126,   127,   128,   129,
     130,   131,   132,   133,   134,   135,   136,   137,     0,     1,
      -1,     3,     4,     5,    -1,    -1,    -1,     9,    10,    11,
      12,    13,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    38,    39,    40,    41,
      42,    43,    44,    45,    46,    47,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,    73,    74,    75,    76,    77,    78,    79,    80,    81,
      82,    83,    84,    85,    86,    87,    88,    89,    90,    91,
      92,    93,    94,    95,    96,    97,    98,    99,   100,   101,
     102,   103,   104,   105,   106,   107,   108,   109,   110,   111,
     112,   113,   114,   115,   116,   117,   118,   119,   120,   121,
     122,   123,   124,   125,   126,   127,   128,   129,   130,   131,
     132,   133,   134,   135,   136,   137,     1,    -1,    -1,     4,
       5,    -1,    -1,    -1,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137
  };

  /* STOS_[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
  const unsigned short int
  rib_parser::yystos_[] =
  {
         0,     1,     4,     5,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   141,   142,   143,   144,   145,   146,   147,
     148,   149,   150,   151,   152,   153,   154,   155,   156,   157,
     158,   159,   160,   161,   162,   163,   164,   165,   166,   167,
     168,   169,   170,   171,   172,   173,   174,   175,   176,   177,
     178,   179,   180,   181,   182,   183,   184,   185,   186,   187,
     188,   189,   190,   191,   192,   193,   194,   195,   196,   197,
     198,   199,   200,   201,   202,   203,   204,   205,   206,   207,
     208,   209,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   221,   222,   223,   224,   225,   226,   227,
     228,   229,   230,   231,   232,   233,   234,   235,   236,   237,
     238,   239,   240,   241,   242,   243,   244,   245,   246,   247,
     248,   249,   250,   251,   252,   253,   254,   255,   256,   257,
     258,   259,   260,   261,   262,   263,   264,   265,   266,   267,
     268,   269,   270,   271,   272,     0,     3,   143,     8,   282,
     282,   282,   282,   138,   273,   277,   281,   282,   287,     7,
     275,     6,   275,   279,   287,   279,   279,   287,   287,   287,
     279,   282,   282,   279,   279,   282,   282,   279,   279,   287,
     279,   287,   279,   282,   282,   282,   282,   282,   279,   282,
     275,   279,   275,   273,   277,   282,   282,   282,   282,   279,
     287,   282,   275,   282,   282,   282,   282,   273,   285,   282,
     285,   282,   282,   282,   282,   282,   275,   287,   275,   275,
     282,   275,   282,   279,   287,   282,   282,   279,   282,   282,
     279,   282,   279,   279,   282,   282,   282,     1,   282,   289,
     290,   291,   277,   277,     1,   290,   282,   282,   282,   279,
     282,   279,   279,   282,   279,   282,   279,   279,   275,   279,
     287,   282,   279,   282,   282,   279,   287,   279,   287,   279,
     277,     8,   279,   283,   282,   282,   282,   282,   282,   282,
     282,   282,   282,   282,   282,     9,   291,   275,   282,   291,
     291,     6,     7,     9,   276,   280,     9,     9,   275,   275,
       9,   277,     9,   279,   279,   279,   287,   279,   279,   279,
     282,   291,   279,   279,   279,   279,   291,   291,   282,   279,
     291,   275,   276,   290,   279,   287,   291,   277,   291,   279,
     291,   275,   275,   291,   291,   275,   282,     8,   284,     9,
     282,   282,   282,   282,   282,   282,   282,   282,   275,   279,
     291,   279,   290,   275,   279,   279,   277,   285,   287,   289,
       6,     7,     8,   273,   277,   281,   285,     1,   289,   277,
     277,   291,   275,   282,   279,   279,   279,   279,   279,   279,
     277,   291,   279,   279,   279,   277,     9,   282,   282,   282,
     287,   291,   291,   291,   282,   282,   282,   291,   291,     6,
     139,   274,   275,     6,     7,   274,   282,   287,   282,   287,
     273,   281,   279,   279,   279,   279,   279,   279,   279,   279,
     279,   282,   279,   274,   277,   279,   291,   291,     8,   274,
     291,   291,   291,   282,   282,   282,   291,   282,   287,   279,
     279,   282,   279,   282,   287,   291,   277,   290,   275,   291,
     279,   279,   279,   279,   279,   277,   279,   279,   279,   287,
     282,   282,   291,   277,   291,   291,   291,     9,   275,   275,
     275,   275,   276,   273,   285,   286,   279,   291,   279,   279,
     279,   279,   291,   291,   286,   279,   282,   282,   279,   282,
     279,   279,   275,   291,   291,   290,   275,   279,   279,   279,
     279,   286,   290,   279,   279,   287,   291,   282,   274,   274,
     291,   279,   291,   279,   273,   277,   278,   279,   282,   282,
     279,   282,   279,   291,   282,   279,   279,   291,   278,   279,
     279,   287,   282,   279,   279,   274,   278,   279,   279,   282,
     291,   279,   275,   290,   279,   278,   279,   291,   277,   273,
     287,   288,   279,   279,   282,   279,   275,   279,   288,   279,
     287,   274,   286,   291,   291,   279,   291,   287,   290,   279,
     287,   290,   282,   279,   287,   279,   279,   279,   290,   291
  };

#if YYDEBUG
  /* TOKEN_NUMBER_[YYLEX-NUM] -- Internal symbol number corresponding
     to YYLEX-NUM.  */
  const unsigned short int
  rib_parser::yytoken_number_[] =
  {
         0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,    91,    93
  };
#endif

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
  const unsigned short int
  rib_parser::yyr1_[] =
  {
         0,   140,   141,   141,   142,   142,   143,   143,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     144,   144,   144,   144,   144,   144,   144,   144,   144,   144,
     145,   146,   147,   148,   149,   150,   151,   152,   153,   154,
     155,   156,   157,   158,   159,   160,   161,   162,   163,   164,
     165,   166,   167,   168,   169,   170,   171,   172,   173,   174,
     175,   176,   177,   178,   179,   180,   181,   182,   183,   184,
     185,   186,   187,   188,   189,   190,   191,   192,   193,   194,
     195,   196,   197,   198,   199,   200,   201,   202,   203,   204,
     205,   206,   207,   208,   209,   210,   211,   212,   213,   214,
     215,   216,   217,   218,   219,   220,   221,   222,   223,   224,
     225,   226,   227,   228,   229,   230,   231,   232,   233,   234,
     235,   236,   237,   238,   239,   240,   241,   242,   243,   244,
     245,   246,   247,   248,   249,   250,   251,   252,   253,   254,
     255,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   273,
     274,   274,   275,   275,   276,   276,   277,   277,   278,   278,
     279,   279,   279,   280,   280,   280,   280,   281,   281,   281,
     282,   282,   283,   283,   284,   284,   285,   285,   286,   286,
     287,   287,   288,   288,   289,   289,   289,   289,   289,   289,
     290,   290,   290,   290,   291,   291,   291
  };

  /* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
  const unsigned char
  rib_parser::yyr2_[] =
  {
         0,     2,     1,     2,     1,     2,     1,     1,     2,     2,
       3,     2,     1,     1,     1,     4,     2,     5,     5,     3,
       3,     1,     4,     3,     2,     3,     4,     1,     2,     3,
       3,     6,     3,     5,     3,     3,     2,     3,     1,     1,
       2,     4,     2,     4,     9,     2,     4,     4,     4,     4,
       2,     1,     3,     3,     3,     3,     3,     3,     2,     2,
       2,     7,     2,     7,     2,     5,     2,     3,     3,     2,
       1,     2,     1,     2,     2,     2,     4,     5,     4,     8,
       2,     3,     3,     2,     2,     2,     1,     1,     1,     4,
       1,     1,     3,     2,     3,     5,     6,     2,     3,     1,
       5,     4,     2,     4,     5,     5,     5,     5,     5,     3,
       7,    12,    10,     6,     5,     6,     3,     9,     6,     5,
       7,     3,     2,     1,     2,     2,     1,     2,     2,     2,
       1,     9,     9,     7,    13,     4,     4,     4,     4,     2,
       1,     1,     1,     9,     5,    10,     5,     6,     4,     2,
       4,     3,     3,     3,     4,     4,     4,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     2,
       1,     2,     1,     2,     1,     2,     3,     2,     1,     2,
       1,     1,     2,     1,     2,     2,     2,     3,     3,     2,
       1,     2,     1,     2,     1,     2,     3,     2,     1,     2,
       1,     1,     1,     2,     2,     2,     2,     2,     2,     2,
       1,     2,     2,     2,     0,     1,     1
  };

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
  /* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
     First, the terminals, then, starting at \a yyntokens_, nonterminals.  */
  const char*
  const rib_parser::yytname_[] =
  {
    "$end", "error", "$undefined", "EOF_TOKEN", "UNKNOWN_TOKEN",
  "INVALID_VALUE", "FLOAT_TOKEN", "INTEGER_TOKEN", "STRING_TOKEN",
  "COMMENT", "TOKEN_ARCHIVEBEGIN", "TOKEN_ARCHIVEEND",
  "TOKEN_AREALIGHTSOURCE", "TOKEN_ATMOSPHERE", "TOKEN_ATTRIBUTE",
  "TOKEN_ATTRIBUTEBEGIN", "TOKEN_ATTRIBUTEEND", "TOKEN_BASIS",
  "TOKEN_BLOBBY", "TOKEN_BOUND", "TOKEN_CLIPPING", "TOKEN_COLOR",
  "TOKEN_COLORSAMPLES", "TOKEN_CONCATTRANSFORM", "TOKEN_CONE",
  "TOKEN_COORDINATESYSTEM", "TOKEN_COORDSYSTRANSFORM", "TOKEN_CROPWINDOW",
  "TOKEN_CURVES", "TOKEN_CYLINDER", "TOKEN_DECLARE", "TOKEN_DEFORMATION",
  "TOKEN_DEPTHOFFIELD", "TOKEN_DETAIL", "TOKEN_DETAILRANGE", "TOKEN_DISK",
  "TOKEN_DISPLACEMENT", "TOKEN_DISPLAYCHANNEL", "TOKEN_DISPLAY",
  "TOKEN_ELSE", "TOKEN_ELSEIF", "TOKEN_ERRORABORT", "TOKEN_ERRORHANDLER",
  "TOKEN_ERRORIGNORE", "TOKEN_ERRORPRINT", "TOKEN_EXPOSURE",
  "TOKEN_EXTERIOR", "TOKEN_FORMAT", "TOKEN_FRAMEASPECTRATIO",
  "TOKEN_FRAMEBEGIN", "TOKEN_FRAMEEND", "TOKEN_GENERALPOLYGON",
  "TOKEN_GEOMETRICAPPROXIMATION", "TOKEN_GEOMETRY",
  "TOKEN_HIERARCHICALSUBDIVISIONMESH", "TOKEN_HIDER", "TOKEN_HYPERBOLOID",
  "TOKEN_IDENTITY", "TOKEN_IFBEGIN", "TOKEN_IFEND", "TOKEN_ILLUMINATE",
  "TOKEN_IMAGER", "TOKEN_INTERIOR", "TOKEN_LIGHTSOURCE",
  "TOKEN_MAKEOCCLUSION", "TOKEN_MAKEBRICKMAP", "TOKEN_MAKEBUMP",
  "TOKEN_MAKECUBEFACEENVIRONMENT", "TOKEN_MAKELATLONGENVIRONMENT",
  "TOKEN_MAKESHADOW", "TOKEN_MAKETEXTURE", "TOKEN_MATTE",
  "TOKEN_MOTIONBEGIN", "TOKEN_MOTIONEND", "TOKEN_NUPATCH",
  "TOKEN_OBJECTBEGIN", "TOKEN_OBJECTEND", "TOKEN_OBJECTINSTANCE",
  "TOKEN_OPACITY", "TOKEN_OPTION", "TOKEN_ORIENTATION", "TOKEN_PARABOLOID",
  "TOKEN_PATCH", "TOKEN_PATCHMESH", "TOKEN_PERSPECTIVE",
  "TOKEN_PIXELFILTER", "TOKEN_PIXELSAMPLES", "TOKEN_PIXELVARIANCE",
  "TOKEN_POINTS", "TOKEN_POINTSGENERALPOLYGONS", "TOKEN_POINTSPOLYGONS",
  "TOKEN_POLYGON", "TOKEN_PROCEDURAL", "TOKEN_PROCEDURE",
  "TOKEN_PROJECTION", "TOKEN_QUANTIZE", "TOKEN_READARCHIVE",
  "TOKEN_RELATIVEDETAIL", "TOKEN_RESOURCE", "TOKEN_RESOURCEBEGIN",
  "TOKEN_RESOURCEEND", "TOKEN_REVERSEORIENTATION", "TOKEN_ROTATE",
  "TOKEN_SCALE", "TOKEN_SCOPEDCOORDINATESYSTEM", "TOKEN_SCREENWINDOW",
  "TOKEN_SHADINGINTERPOLATION", "TOKEN_SHADINGRATE", "TOKEN_SHUTTER",
  "TOKEN_SIDES", "TOKEN_SKEW", "TOKEN_SOLIDBEGIN", "TOKEN_SOLIDEND",
  "TOKEN_SPHERE", "TOKEN_SUBDIVISIONMESH", "TOKEN_SURFACE",
  "TOKEN_TEXTURECOORDINATES", "TOKEN_TORUS", "TOKEN_TRANSFORM",
  "TOKEN_TRANSFORMBEGIN", "TOKEN_TRANSFORMEND", "TOKEN_TRANSFORMPOINTS",
  "TOKEN_TRANSLATE", "TOKEN_TRIMCURVE", "TOKEN_VERSION",
  "TOKEN_WORLDBEGIN", "TOKEN_WORLDEND", "TOKEN_SHADERLAYER",
  "TOKEN_CONNECTSHADERLAYERS", "TOKEN_PROCEDURAL2", "TOKEN_SYSTEM",
  "TOKEN_VOLUME", "TOKEN_VPATMOSPHERE", "TOKEN_VPINTERIOR",
  "TOKEN_VPSURFACE", "TOKEN_INTEGRATOR", "TOKEN_BXDF", "TOKEN_PATTERN",
  "'['", "']'", "$accept", "file", "requests", "request",
  "complete_request", "archivebegin", "archiveend", "arealightsource",
  "atmosphere", "attribute", "attributebegin", "attributeend", "basis",
  "blobby", "bound", "clipping", "color", "colorsamples",
  "concattransform", "cone", "coordinatesystem", "coordsystransform",
  "cropwindow", "cylinder", "declare", "deformation", "depthoffield",
  "detail", "detailrange", "disk", "displacement", "displaychannel",
  "display", "else", "elseif", "errorabort", "errorhandler", "errorignore",
  "errorprint", "exposure", "exterior", "format", "frameaspectratio",
  "framebegin", "frameend", "generalpolygon", "geometricapproximation",
  "geometry", "hierarchicalsubdivisionmesh", "hider", "hyperboloid",
  "identity", "ifbegin", "ifend", "illuminate", "imager", "interior",
  "lightsource", "makeocclusion", "makebrickmap", "makebump",
  "makecubefaceenvironment", "makelatlongenvironment", "makeshadow",
  "maketexture", "matte", "motionbegin", "motionend", "nupatch",
  "objectbegin", "objectend", "objectinstance", "opacity", "option",
  "orientation", "paraboloid", "patch", "patchmesh", "perspective",
  "pixelfilter", "pixelsamples", "pixelvariance", "curves", "procedural",
  "procedure", "points", "pointsgeneralpolygons", "pointspolygons",
  "polygon", "projection", "quantize", "readarchive", "relativedetail",
  "resource", "resourcebegin", "resourceend", "reverseorientation",
  "rotate", "scale", "scopedcoordinatesystem", "screenwindow",
  "shadinginterpolation", "shadingrate", "shutter", "sides", "skew",
  "solidbegin", "solidend", "sphere", "subdivisionmesh", "surface",
  "texturecoordinates", "torus", "transform", "transformbegin",
  "transformend", "transformpoints", "translate", "trimcurve", "version",
  "worldbegin", "worldend", "shaderlayer", "connectshaderlayers",
  "procedural2", "system", "volume", "vpatmosphere", "vpinterior",
  "vpsurface", "integrator", "bxdf", "pattern", "arraystart", "arrayend",
  "integer", "integers", "integer_array", "integer_array_or_empty",
  "float", "floats", "float_array", "string", "version_string", "strings",
  "string_array", "string_array_or_empty", "scalar_array",
  "scalar_array_or_empty", "token_value", "token_value_array",
  "opt_token_value_array", 0
  };
#endif

#if YYDEBUG
  /* YYRHS -- A `-1'-separated list of the rules' RHS.  */
  const rib_parser::rhs_number_type
  rib_parser::yyrhs_[] =
  {
       141,     0,    -1,   142,    -1,   142,     3,    -1,   143,    -1,
     142,   143,    -1,   144,    -1,     9,    -1,   259,   283,    -1,
     259,   279,    -1,   164,   282,   282,    -1,   183,   275,    -1,
     184,    -1,   260,    -1,   261,    -1,   181,   275,   275,   279,
      -1,   182,   279,    -1,   240,   279,   279,   279,   279,    -1,
     162,   279,   279,   279,   279,    -1,   229,   282,   291,    -1,
     155,   279,   279,    -1,   166,    -1,   166,   279,   279,   279,
      -1,   243,   279,   279,    -1,   221,   279,    -1,   220,   279,
     279,    -1,   219,   282,   279,   279,    -1,   173,    -1,   174,
     282,    -1,   179,   279,   279,    -1,   195,   282,   291,    -1,
     230,   282,   275,   275,   275,   279,    -1,   171,   282,   291,
      -1,   172,   282,   282,   282,   291,    -1,   189,   282,   291,
      -1,   157,   287,   287,    -1,   232,   279,    -1,   213,   282,
     291,    -1,   150,    -1,   151,    -1,   156,   287,    -1,   156,
     279,   279,   279,    -1,   212,   287,    -1,   212,   279,   279,
     279,    -1,   251,   279,   279,   279,   279,   279,   279,   279,
     279,    -1,   251,   287,    -1,   197,   282,   275,   291,    -1,
     197,   282,   282,   291,    -1,   147,   282,   275,   291,    -1,
     147,   282,   282,   291,    -1,   192,   282,    -1,   193,    -1,
     194,   275,   275,    -1,   194,   282,   275,    -1,   250,   282,
     291,    -1,   148,   282,   291,    -1,   196,   282,   291,    -1,
     180,   282,   291,    -1,   242,   279,    -1,   241,   282,    -1,
     205,   275,    -1,   154,   279,   279,   279,   279,   279,   279,
      -1,   154,   287,    -1,   167,   279,   279,   279,   279,   279,
     279,    -1,   167,   287,    -1,   168,   279,   279,   279,   279,
      -1,   168,   287,    -1,   186,   282,   279,    -1,   186,   282,
     287,    -1,   214,   282,    -1,   236,    -1,   244,   275,    -1,
     191,    -1,   253,   287,    -1,   158,   287,    -1,   218,   279,
      -1,   257,   279,   279,   279,    -1,   237,   279,   279,   279,
     279,    -1,   238,   279,   279,   279,    -1,   245,   279,   279,
     279,   279,   279,   279,   279,    -1,   245,   287,    -1,   165,
     282,   291,    -1,   170,   282,   291,    -1,   160,   282,    -1,
     239,   282,    -1,   161,   282,    -1,   256,    -1,   254,    -1,
     255,    -1,   233,   282,   282,   291,    -1,   234,    -1,   235,
      -1,   149,   282,   291,    -1,   228,   290,    -1,   185,   277,
     290,    -1,   222,   282,   277,   282,   291,    -1,   153,   275,
     277,   281,   286,   291,    -1,   231,   282,    -1,   145,   282,
     291,    -1,   146,    -1,   223,   282,   285,   287,   291,    -1,
     224,   282,   287,   291,    -1,   225,   291,    -1,   227,   277,
     277,   290,    -1,   226,   277,   277,   277,   290,    -1,   152,
     282,   275,   282,   275,    -1,   152,   282,   275,   287,   275,
      -1,   152,   287,   275,   282,   275,    -1,   152,   287,   275,
     287,   275,    -1,   216,   282,   290,    -1,   217,   282,   275,
     282,   275,   282,   290,    -1,   208,   275,   275,   287,   279,
     279,   275,   275,   287,   279,   279,   290,    -1,   258,   277,
     277,   287,   287,   287,   277,   287,   287,   287,    -1,   248,
     279,   279,   279,   279,   291,    -1,   159,   279,   279,   279,
     291,    -1,   163,   279,   279,   279,   279,   291,    -1,   190,
     287,   291,    -1,   190,   279,   279,   279,   279,   279,   279,
     279,   291,    -1,   215,   279,   279,   279,   279,   291,    -1,
     169,   279,   279,   279,   291,    -1,   252,   279,   279,   279,
     279,   279,   291,    -1,   187,   282,   291,    -1,   246,   282,
      -1,   247,    -1,   209,   275,    -1,   209,   282,    -1,   210,
      -1,   211,   275,    -1,   211,   282,    -1,   206,   287,    -1,
     207,    -1,   204,   282,   282,   282,   282,   282,   279,   279,
     291,    -1,   200,   282,   282,   282,   282,   282,   279,   279,
     291,    -1,   202,   282,   282,   282,   279,   279,   291,    -1,
     201,   282,   282,   282,   282,   282,   282,   282,   279,   282,
     279,   279,   291,    -1,   203,   282,   282,   291,    -1,   198,
     285,   282,   291,    -1,   199,   282,   282,   291,    -1,   199,
     285,   282,   291,    -1,   176,   282,    -1,   177,    -1,   178,
      -1,   175,    -1,   249,   282,   277,   277,   286,   278,   278,
     288,   290,    -1,   249,   282,   277,   277,   290,    -1,   188,
     282,   277,   277,   286,   278,   278,   288,   286,   290,    -1,
     262,   282,   282,   282,   291,    -1,   263,   282,   282,   282,
     282,   282,    -1,   264,   282,   282,   291,    -1,   265,   282,
      -1,   266,   282,   287,   277,    -1,   267,   282,   291,    -1,
     268,   282,   291,    -1,   269,   282,   291,    -1,   270,   282,
     282,   291,    -1,   271,   282,   282,   291,    -1,   272,   282,
     282,   291,    -1,     4,    -1,     1,    -1,     5,    -1,    10,
      -1,    11,    -1,    12,    -1,    13,    -1,    14,    -1,    15,
      -1,    16,    -1,    17,    -1,    18,    -1,    19,    -1,    20,
      -1,    21,    -1,    22,    -1,    23,    -1,    24,    -1,    25,
      -1,    26,    -1,    27,    -1,    29,    -1,    30,    -1,    31,
      -1,    32,    -1,    33,    -1,    34,    -1,    35,    -1,    36,
      -1,    37,    -1,    38,    -1,    39,    -1,    40,    -1,    41,
      -1,    42,    -1,    43,    -1,    44,    -1,    45,    -1,    46,
      -1,    47,    -1,    48,    -1,    49,    -1,    50,    -1,    51,
      -1,    52,    -1,    53,    -1,    54,    -1,    55,    -1,    56,
      -1,    57,    -1,    58,    -1,    59,    -1,    60,    -1,    61,
      -1,    62,    -1,    63,    -1,    64,    -1,    65,    -1,    66,
      -1,    67,    -1,    68,    -1,    69,    -1,    70,    -1,    71,
      -1,    72,    -1,    73,    -1,    74,    -1,    75,    -1,    76,
      -1,    77,    -1,    78,    -1,    79,    -1,    80,    -1,    81,
      -1,    82,    -1,    83,    -1,    84,    -1,    85,    -1,    86,
      -1,    87,    -1,    28,    -1,    92,    -1,    93,    -1,    88,
      -1,    89,    -1,    90,    -1,    91,    -1,    94,    -1,    95,
      -1,    96,    -1,    97,    -1,    98,    -1,    99,    -1,   100,
      -1,   101,    -1,   102,    -1,   103,    -1,   104,    -1,   105,
      -1,   106,    -1,   107,    -1,   108,    -1,   109,    -1,   110,
      -1,   111,    -1,   112,    -1,   113,    -1,   114,    -1,   115,
      -1,   116,    -1,   117,    -1,   118,    -1,   119,    -1,   120,
      -1,   121,    -1,   122,    -1,   123,    -1,   124,    -1,   125,
      -1,   126,    -1,   127,    -1,   128,    -1,   129,    -1,   130,
      -1,   131,    -1,   132,    -1,   133,    -1,   134,    -1,   135,
      -1,   136,    -1,   137,    -1,   138,    -1,   273,     9,    -1,
     139,    -1,   274,     9,    -1,     7,    -1,   275,     9,    -1,
       7,    -1,   276,   275,    -1,   273,   276,   274,    -1,   277,
       9,    -1,   277,    -1,   273,   274,    -1,     6,    -1,   275,
      -1,   279,     9,    -1,     6,    -1,   280,     6,    -1,   280,
       7,    -1,   276,     6,    -1,   273,   280,   274,    -1,   273,
     276,   274,    -1,   281,     9,    -1,     8,    -1,   282,     9,
      -1,     8,    -1,   283,     9,    -1,     8,    -1,   284,     8,
      -1,   273,   284,   274,    -1,   285,     9,    -1,   285,    -1,
     273,   274,    -1,   281,    -1,   277,    -1,   287,    -1,   273,
     274,    -1,   282,     7,    -1,   282,   277,    -1,   282,     6,
      -1,   282,   281,    -1,   282,     8,    -1,   282,   285,    -1,
     289,    -1,     1,   289,    -1,   290,   289,    -1,   290,     1,
      -1,    -1,     1,    -1,   290,    -1
  };

  /* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
     YYRHS.  */
  const unsigned short int
  rib_parser::yyprhs_[] =
  {
         0,     0,     3,     5,     8,    10,    13,    15,    17,    20,
      23,    27,    30,    32,    34,    36,    41,    44,    50,    56,
      60,    64,    66,    71,    75,    78,    82,    87,    89,    92,
      96,   100,   107,   111,   117,   121,   125,   128,   132,   134,
     136,   139,   144,   147,   152,   162,   165,   170,   175,   180,
     185,   188,   190,   194,   198,   202,   206,   210,   214,   217,
     220,   223,   231,   234,   242,   245,   251,   254,   258,   262,
     265,   267,   270,   272,   275,   278,   281,   286,   292,   297,
     306,   309,   313,   317,   320,   323,   326,   328,   330,   332,
     337,   339,   341,   345,   348,   352,   358,   365,   368,   372,
     374,   380,   385,   388,   393,   399,   405,   411,   417,   423,
     427,   435,   448,   459,   466,   472,   479,   483,   493,   500,
     506,   514,   518,   521,   523,   526,   529,   531,   534,   537,
     540,   542,   552,   562,   570,   584,   589,   594,   599,   604,
     607,   609,   611,   613,   623,   629,   640,   646,   653,   658,
     661,   666,   670,   674,   678,   683,   688,   693,   695,   697,
     699,   701,   703,   705,   707,   709,   711,   713,   715,   717,
     719,   721,   723,   725,   727,   729,   731,   733,   735,   737,
     739,   741,   743,   745,   747,   749,   751,   753,   755,   757,
     759,   761,   763,   765,   767,   769,   771,   773,   775,   777,
     779,   781,   783,   785,   787,   789,   791,   793,   795,   797,
     799,   801,   803,   805,   807,   809,   811,   813,   815,   817,
     819,   821,   823,   825,   827,   829,   831,   833,   835,   837,
     839,   841,   843,   845,   847,   849,   851,   853,   855,   857,
     859,   861,   863,   865,   867,   869,   871,   873,   875,   877,
     879,   881,   883,   885,   887,   889,   891,   893,   895,   897,
     899,   901,   903,   905,   907,   909,   911,   913,   915,   917,
     919,   921,   923,   925,   927,   929,   931,   933,   935,   937,
     939,   941,   943,   945,   947,   949,   951,   953,   955,   957,
     960,   962,   965,   967,   970,   972,   975,   979,   982,   984,
     987,   989,   991,   994,   996,   999,  1002,  1005,  1009,  1013,
    1016,  1018,  1021,  1023,  1026,  1028,  1031,  1035,  1038,  1040,
    1043,  1045,  1047,  1049,  1052,  1055,  1058,  1061,  1064,  1067,
    1070,  1072,  1075,  1078,  1081,  1082,  1084
  };

  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
  const unsigned short int
  rib_parser::yyrline_[] =
  {
         0,   431,   431,   433,   437,   438,   441,   443,   450,   455,
     459,   465,   469,   473,   477,   481,   485,   489,   493,   497,
     506,   510,   514,   518,   522,   526,   530,   535,   539,   544,
     548,   557,   562,   571,   582,   591,   598,   602,   611,   615,
     619,   628,   636,   645,   653,   657,   663,   674,   685,   696,
     707,   712,   716,   720,   725,   734,   743,   752,   761,   765,
     770,   774,   779,   786,   791,   798,   802,   808,   813,   819,
     824,   828,   832,   836,   843,   850,   854,   858,   862,   866,
     870,   876,   885,   894,   899,   904,   909,   913,   917,   921,
     931,   935,   939,   948,   960,   969,   982,  1005,  1010,  1019,
    1023,  1037,  1050,  1062,  1072,  1083,  1089,  1101,  1113,  1127,
    1136,  1147,  1157,  1170,  1178,  1186,  1194,  1209,  1223,  1231,
    1239,  1247,  1256,  1261,  1265,  1270,  1276,  1280,  1284,  1289,
    1294,  1298,  1316,  1332,  1346,  1366,  1376,  1386,  1397,  1407,
    1412,  1414,  1416,  1418,  1456,  1470,  1512,  1523,  1532,  1542,
    1547,  1556,  1565,  1574,  1583,  1593,  1603,  1613,  1617,  1619,
    1625,  1626,  1627,  1628,  1629,  1630,  1631,  1632,  1633,  1634,
    1635,  1636,  1637,  1638,  1639,  1640,  1641,  1642,  1643,  1644,
    1645,  1646,  1647,  1648,  1649,  1650,  1651,  1652,  1653,  1654,
    1655,  1656,  1657,  1658,  1659,  1660,  1661,  1662,  1663,  1664,
    1665,  1666,  1667,  1668,  1669,  1670,  1671,  1672,  1673,  1674,
    1675,  1676,  1677,  1678,  1679,  1680,  1681,  1682,  1683,  1684,
    1685,  1686,  1687,  1688,  1689,  1690,  1691,  1692,  1693,  1694,
    1695,  1696,  1697,  1698,  1699,  1700,  1701,  1702,  1703,  1704,
    1705,  1706,  1707,  1708,  1709,  1710,  1711,  1712,  1713,  1714,
    1715,  1716,  1717,  1718,  1719,  1720,  1721,  1722,  1723,  1724,
    1725,  1726,  1727,  1728,  1729,  1730,  1731,  1732,  1733,  1734,
    1735,  1736,  1737,  1738,  1739,  1740,  1741,  1742,  1743,  1744,
    1745,  1746,  1747,  1748,  1749,  1750,  1751,  1752,  1754,  1756,
    1762,  1764,  1770,  1774,  1781,  1786,  1793,  1797,  1804,  1805,
    1811,  1815,  1819,  1827,  1832,  1837,  1842,  1850,  1854,  1859,
    1874,  1878,  1885,  1889,  1896,  1901,  1908,  1912,  1919,  1920,
    1926,  1927,  1934,  1935,  1941,  1969,  2000,  2011,  2023,  2035,
    2050,  2055,  2060,  2065,  2069,  2072,  2076
  };

  // Print the state stack on the debug stream.
  void
  rib_parser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (state_stack_type::const_iterator i = yystate_stack_.begin ();
	 i != yystate_stack_.end (); ++i)
      *yycdebug_ << ' ' << *i;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  rib_parser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    /* Print the symbols being reduced, and their result.  */
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
	       << " (line " << yylno << "), ";
    /* The symbols being reduced.  */
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
		       yyrhs_[yyprhs_[yyrule] + yyi],
		       &(yysemantic_stack_[(yynrhs) - (yyi + 1)]),
		       &(yylocation_stack_[(yynrhs) - (yyi + 1)]));
  }
#endif // YYDEBUG

  /* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
  rib_parser::token_number_type
  rib_parser::yytranslate_ (int t)
  {
    static
    const token_number_type
    translate_table[] =
    {
           0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,   138,     2,   139,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137
    };
    if ((unsigned int) t <= yyuser_token_number_max_)
      return translate_table[t];
    else
      return yyundef_token_;
  }

  const int rib_parser::yyeof_ = 0;
  const int rib_parser::yylast_ = 3882;
  const int rib_parser::yynnts_ = 152;
  const int rib_parser::yyempty_ = -2;
  const int rib_parser::yyfinal_ = 265;
  const int rib_parser::yyterror_ = 1;
  const int rib_parser::yyerrcode_ = 256;
  const int rib_parser::yyntokens_ = 140;

  const unsigned int rib_parser::yyuser_token_number_max_ = 392;
  const rib_parser::token_number_type rib_parser::yyundef_token_ = 2;

} // namespace yy

#line 2079 "rib_parser.yy"


void yy::rib_parser::error(const yy::rib_parser::location_type&, const std::string& m)
{
	fprintf(stderr, "parser error %s\n", m.c_str());
}

extern int yylex_init(void** p);
extern int yylex_destroy(void* p);


namespace ri
{
	int load_rib(FILE* in, ri_context* ctx)
	{
		ri_rib_decoder dec(in);

		void* scanner;
		yylex_init(&scanner);

		ri_rib_parse_param* param = (ri_rib_parse_param*)(scanner);
		param->ctx = ctx;
		param->fRequest = false;
		param->fParams = false;
		param->fSkipFrame = false;
		param->line_number = 1;
		param->dec = &dec;

		yy::rib_parser parser(scanner);

		int nRet = parser.parse();

		yylex_destroy(scanner);

		return nRet;
	}
}

