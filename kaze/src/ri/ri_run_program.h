#ifndef RI_RUN_PROGRAM_H
#define RI_RUN_PROGRAM_H

namespace ri
{
    class ri_context;
    int ri_run_program(ri_context* ctx, const char* program, float area, const char* arg);
}

#endif
