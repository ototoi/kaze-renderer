#include "ri_sequence_task.h"

namespace ri
{
    void ri_sequence_task::add_task(const std::shared_ptr<ri_task>& task)
    {
        tasks_.push_back(task);
    }

    int ri_sequence_task::run()
    {
        int nRet = 0;
        for (int i = 0; i < tasks_.size(); i++)
        {
            nRet = tasks_[i]->run();
            if (nRet != 0)
                return nRet;
        }
        return nRet;
    }
}
