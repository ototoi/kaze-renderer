#include "ri_config.h"
#include "ri_path_resolver.h"
#include "ri_env.h"

#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>

#include <string.h>

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif

namespace ri
{
    ri_config_reader::ri_config_reader(const char* szFilePath)
    {
        std::ifstream ifs(szFilePath);
        if (!ifs)
            return;

        std::string strLine;
        while (std::getline(ifs, strLine))
        {
            std::stringstream ss(strLine);
            std::string key, val;
            ss >> key;
            ss >> val;
            if (!key.empty() && !val.empty())
            {
                if (key[0] != '#')
                {
                    map_[key] = ri_env::parse(val.c_str());
                }
            }
        }
    }

    bool ri_config_reader::get(const char* key, std::string& val) const
    {
        const_iterator iter = map_.find(key);
        if (iter != map_.end())
        {
            val = iter->second;
            return true;
        }
        if (ri_env::get(key, val))
        {
            map_[key] = val;
        }
        return false;
    }

    static bool IsExistFile(const char* szFilePath)
    {
#ifdef _WIN32
        struct _stat s;
        int nRet = _stat(szFilePath, &s);
        return (nRet == 0);
#else
        struct stat s;
        int nRet = stat(szFilePath, &s);
        return (nRet == 0);
#endif
    }

    static std::string get_config_path()
    {
        std::string strEtc = ri_path_resolver::get_root_path() + "etc/";
        std::string strHome;
        ri_env::get("HOME", strHome);
        std::string strFile = "rendermn.ini";
        std::vector<std::string> paths;
        paths.push_back(strHome + strFile);
        paths.push_back(strEtc + strFile);
        for (size_t i = 0; i < paths.size(); i++)
        {
            if (IsExistFile(paths[i].c_str()))
            {
                return paths[i];
            }
        }
        return "";
    }

    static const ri_config_reader& instance()
    {
        static ri_config_reader c(get_config_path().c_str());
        return c;
    }

    bool ri_config::get(const char* key, std::string& val)
    {
        if (instance().get(key, val))
            return true;

        return false;
    }

    std::string ri_config::parse(const char* strVal)
    {
        static const char* strBegin = "${";
        static const char* strEnd = "}";

        std::string val = strVal;

        bool bContinue = false;
        do
        {
            bContinue = false;
            const char* strFindBegin = strstr(val.c_str(), strBegin);
            if (strFindBegin)
            {
                const char* strFindEnd = strstr(strFindBegin + 2, strEnd);
                if (strFindEnd)
                {
                    std::string strA, strB, strC;

                    {
                        int n = strFindBegin - val.c_str();
                        if (n > 0)
                        {
                            strA.assign(val.c_str(), n);
                        }
                    }
                    {
                        int nKey = strFindEnd - (strFindBegin + 2);
                        nKey = std::min(nKey, 255);
                        if (nKey)
                        {
                            char szKey[256] = {};
                            memcpy(szKey, strFindBegin + 2, sizeof(char) * nKey);
                            if (!ri_config::get(szKey, strB))
                            {
                                strB = "";
                            }
                        }
                    }
                    {
                        strC = strFindEnd + 1;
                    }

                    val = strA + strB + strC;
                    bContinue = true;
                }
            }
        } while (bContinue);

        return val; // TODO
    }
}
