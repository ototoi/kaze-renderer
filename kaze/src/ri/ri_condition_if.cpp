#include "ri_condition_if.h"
#include "ri_config.h"

#include <string>
#include <vector>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <cassert>

#include <string.h>
#include <memory>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#pragma warning(disable : 4800)
#endif

namespace ri
{
    namespace
    {
        enum
        {
            TYPE_BOOL,
            TYPE_INT,
            TYPE_FLOAT,
            TYPE_ARRAY,
            TYPE_STRING
        };

        int GetTypePromotion(int a, int b) { return (a >= b) ? a : b; }

        enum
        {
            TOKEN_EOF = -1,
            TOKEN_BAD = 0,
            TOKEN_INT,
            TOKEN_FLOAT,
            TOKEN_STRING,
            TOKEN_VARIABLE,
            TOKEN_ADD,
            TOKEN_SUB,
            TOKEN_MUL,
            TOKEN_DIV,
            TOKEN_AND,
            TOKEN_OR,
            TOKEN_EQ,
            TOKEN_NE,
            TOKEN_LE,
            TOKEN_GE,
            TOKEN_LS,
            TOKEN_GT,
            TOKEN_NOT,
            TOKEN_LB,
            TOKEN_RB,
            TOKEN_LP,
            TOKEN_RP,
            TOKEN_DEFINED,
            TOKEN_CONCAT,
        };

        struct s2k
        {
            const char* str;
            int kind;
        } SYMBOL2KIND[] = {
            {"&&", TOKEN_AND},
            {"||", TOKEN_OR},
            {"==", TOKEN_EQ},
            {"!=", TOKEN_NE},
            {"<=", TOKEN_LE},
            {">=", TOKEN_GE},
            {"<", TOKEN_LS},
            {">", TOKEN_GT},
            {"!", TOKEN_NOT},
            {"[", TOKEN_LB},
            {"]", TOKEN_RB},
            {"(", TOKEN_LP},
            {")", TOKEN_RP},
            {"+", TOKEN_ADD},
            {"-", TOKEN_SUB},
            {"*", TOKEN_MUL},
            {"/", TOKEN_DIV},
            {"defined", TOKEN_DEFINED},
            {"concat", TOKEN_CONCAT},
            {NULL, 0},
        };

        static int GetTokenKindSymbol(const char* str)
        {
            int i = 0;
            while (SYMBOL2KIND[i].str)
            {
                if (strcmp(SYMBOL2KIND[i].str, str) == 0)
                    return SYMBOL2KIND[i].kind;
                i++;
            }
            return -1;
        }

        static bool IsFloat(const std::string& str)
        {
            const char* s = str.c_str();
            const char* i = s;

            if (str.size() < 2)
                return false;
            if (str[str.size() - 1] == '.')
                return false;

            char c = *i;
            if (c == '.')
            {
                i++;
                if (!(*i))
                    return false;
                while (*i)
                {
                    c = *i;
                    if (!isdigit(c))
                    {
                        return false;
                    }
                    i++;
                }
                return true;
            }
            else
            {
                bool bUsedDot = false;
                if (!(('0' <= c) && (c <= '9')))
                    return false; // 1-9
                i++;
                while (*i)
                {
                    c = *i;
                    if (!isdigit(c))
                    {
                        if (!bUsedDot)
                        {
                            if (c != '.')
                                return false;
                            bUsedDot = true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    i++;
                }
                return true;
            }
        }

        static bool IsInt(const std::string& str)
        {
            const char* s = str.c_str();
            const char* i = s;

            char c = *i;

            int l = (int)strlen(s);
            if (l == 1)
            {
                if (c == '0')
                    return true;
            }

            if (!(('1' <= c) && (c <= '9')))
                return false; // 1-9
            i++;
            while (*i)
            {
                c = *i;
                if (!isdigit(c))
                    return false;
                i++;
            }
            return true;
        }

        static bool IsString(const std::string& str)
        {
            size_t sz = str.size();
            if (sz < 2)
                return false;
            if (str[0] == '\'' && str[sz - 1] == '\'')
                return true;
            return false;
        }

        static int GetTokenKind(const char* s)
        {
            int nRet = GetTokenKindSymbol(s);
            if (nRet > 0)
                return nRet;
            std::string str = s;
            assert(str.size() > 0);
            if (s[0] == '$')
                return TOKEN_VARIABLE;
            if (IsInt(s))
                return TOKEN_INT;
            if (IsFloat(s))
                return TOKEN_FLOAT;
            if (IsString(s))
                return TOKEN_STRING;
            return TOKEN_BAD;
        }

        struct Token
        {
            int kind;
            std::string value;
        };

        static bool StringsToToken(std::vector<Token>& tokens,
                                   const std::vector<std::string>& strings)
        {
            size_t sz = strings.size();
            for (size_t i = 0; i < sz; i++)
            {
                int nKind = GetTokenKind(strings[i].c_str());
                if (nKind <= 0)
                    return false;
                Token tok;
                tok.kind = nKind;
                tok.value = strings[i];
            }
            return true;
        }

        static bool IsEvaluatableCondition(const char* condition)
        {
            if (strstr(condition, "$") == 0 || strstr(condition, ":") == 0)
                return false;
            return true;
        }

        static void SplitToStrings(std::vector<std::string>& tokens, const char* str,
                                   const char* token)
        {
            const char* c = strstr(str, token);
            size_t len = strlen(token);
            if (c != 0)
            {
                if (c != str)
                {
                    size_t l = c - str;
                    std::string s(str, str + l);
                    tokens.push_back(s);
                }
                tokens.push_back(token);
                SplitToStrings(tokens, c + len, token);
            }
            else if (len != 0)
            {
                tokens.push_back(str);
            }
        }

        static std::string TrimSpace(const std::string& str)
        {
            const char* s = str.c_str();
            std::string out;
            while (*s)
            {
                if (!isspace(*s))
                {
                    out += *s;
                }
                s++;
            }
            return out;
        }

        static void SplitToStrings(std::vector<std::string>& tokens, const char* str)
        {
            std::vector<std::string> tmp_in;
            std::vector<std::string> tmp_out;

            tmp_in.push_back(str);

            int k = 0;
            while (SYMBOL2KIND[k].str)
            {
                for (size_t i = 0; i < tmp_in.size(); i++)
                {
                    SplitToStrings(tmp_out, tmp_in[i].c_str(), SYMBOL2KIND[k].str);
                }
                tmp_out.swap(tmp_in);
                tmp_out.clear();
                k++;
            }
            //-------------------------------------------------------------
            tokens.clear();
            for (size_t i = 0; i < tmp_in.size(); i++)
            {
                std::string s = TrimSpace(tmp_in[i]);
                if (!s.empty())
                {
                    tokens.push_back(s);
                }
            }
        }

        static void SplitToTokens(std::vector<std::string>& tokens, const char* str)
        {
            size_t l = strlen(str);
            if (l == 0)
                return;

            const char* c1 = strstr(str, "'");
            if (c1)
            {
                const char* c2 = strstr(c1 + 1, "'");
                if (c2)
                {
                    std::string tmp1(str, c1);
                    SplitToTokens(tokens, tmp1.c_str());
                    std::string tmp2(c1, c2 + 1);
                    tokens.push_back(tmp2);
                    SplitToTokens(tokens, c2 + 1);
                }
                else
                {
                    std::string tmp1(str, c1);
                    SplitToTokens(tokens, tmp1.c_str());
                    std::string tmp2(c1);
                    tmp2 += "'";
                    tokens.push_back(tmp2);
                }
            }
            else
            {
                SplitToStrings(tokens, str);
            }
        }

        //-----------------------------------------------------------------------------------------------------

        class cond
        {
        public:
            virtual ~cond() {}
            virtual int type() const { return TYPE_BOOL; }
            virtual bool to_b() const { return false; }
            virtual int to_i() const { return 0; }
            virtual float to_f() const { return 0.0; }
            virtual std::vector<float> to_a() const
            {
                std::vector<float> tmp;
                return tmp;
            }
            virtual std::string to_s() const { return ""; }
        };

#define RETURN_BOOL(OP)                  \
    switch (nType)                       \
    {                                    \
    case TYPE_BOOL:                      \
        return a_->to_b() OP b_->to_b(); \
    case TYPE_INT:                       \
        return a_->to_i() OP b_->to_i(); \
    case TYPE_FLOAT:                     \
        return a_->to_f() OP b_->to_f(); \
    case TYPE_ARRAY:                     \
        return a_->to_a() OP b_->to_a(); \
    default:                             \
        return a_->to_s() OP b_->to_s(); \
    }

        class eq_cond : public cond
        {
        public:
            eq_cond(cond* a, cond* b) : a_(a), b_(b) {}
            ~eq_cond()
            {
                delete a_;
                delete b_;
            }
            virtual int type() const { return TYPE_BOOL; }
            virtual bool to_b() const
            {
                int nType = GetTypePromotion(a_->type(), b_->type());
                RETURN_BOOL(== );
            }
            virtual int to_i() const { return (to_b()) ? 1 : 0; }
            virtual float to_f() const { return (to_b()) ? 1.0f : 0.0f; }
            virtual std::string to_s() const { return (to_b()) ? "1" : ""; }

        protected:
            cond* a_;
            cond* b_;
        };

        class not_cond : public cond
        {
        public:
            not_cond(cond* a) : a_(a) {}
            ~not_cond() { delete a_; }
            virtual int type() const { return TYPE_BOOL; }
            virtual bool to_b() const { return !a_->to_b(); }
            virtual int to_i() const { return (to_b()) ? 1 : 0; }
            virtual float to_f() const { return (to_b()) ? 1.0f : 0.0f; }
            virtual std::string to_s() const { return (to_b()) ? "1" : ""; }

        protected:
            cond* a_;
        };

        class ne_cond : public eq_cond
        {
        public:
            ne_cond(cond* a, cond* b) : eq_cond(a, b) {}
            virtual bool to_b() const { return !eq_cond::to_b(); }
        };
        class ge_cond : public eq_cond
        {
        public:
            ge_cond(cond* a, cond* b) : eq_cond(a, b) {}
            virtual bool to_b() const
            {
                int nType = GetTypePromotion(a_->type(), b_->type());
                RETURN_BOOL(>= );
            }
        };
        class gt_cond : public eq_cond
        {
        public:
            gt_cond(cond* a, cond* b) : eq_cond(a, b) {}
            virtual bool to_b() const
            {
                int nType = GetTypePromotion(a_->type(), b_->type());
                RETURN_BOOL(> );
            }
        };
        class le_cond : public eq_cond
        {
        public:
            le_cond(cond* a, cond* b) : eq_cond(a, b) {}
            virtual bool to_b() const
            {
                int nType = GetTypePromotion(a_->type(), b_->type());
                RETURN_BOOL(<= );
            }
        };
        class ls_cond : public eq_cond
        {
        public:
            ls_cond(cond* a, cond* b) : eq_cond(a, b) {}
            virtual bool to_b() const
            {
                int nType = GetTypePromotion(a_->type(), b_->type());
                RETURN_BOOL(< );
            }
        };

        class and_cond : public eq_cond
        {
        public:
            and_cond(cond* a, cond* b) : eq_cond(a, b) {}
            virtual bool to_b() const { return a_->to_b() && b_->to_b(); }
        };

        class or_cond : public eq_cond
        {
        public:
            or_cond(cond* a, cond* b) : eq_cond(a, b) {}
            virtual bool to_b() const { return a_->to_b() || b_->to_b(); }
        };

        class bool_cond : public cond
        {
        public:
            bool_cond(bool b) : b_(b) {}
            virtual int type() const { return TYPE_BOOL; }
            virtual bool to_b() const { return b_; }
            virtual int to_i() const { return (to_b()) ? 1 : 0; }
            virtual float to_f() const { return (to_b()) ? 1.0f : 0.0f; }
            virtual std::string to_s() const { return (to_b()) ? "true" : "false"; }

        private:
            bool b_;
        };

        class int_cond : public cond
        {
        public:
            int_cond(int i) : i_(i) {}
            virtual int type() const { return TYPE_INT; }
            virtual bool to_b() const { return (bool)i_; }
            virtual int to_i() const { return i_; }
            virtual float to_f() const { return (float)i_; }
            virtual std::string to_s() const
            {
                std::stringstream ss;
                ss << i_;
                return ss.str();
            }

        private:
            int i_;
        };

        class float_cond : public cond
        {
        public:
            float_cond(float f) : f_(f) {}
            virtual int type() const { return TYPE_FLOAT; }
            virtual bool to_b() const { return (bool)f_; }
            virtual int to_i() const { return (int)f_; }
            virtual float to_f() const { return f_; }
            virtual std::string to_s() const
            {
                std::stringstream ss;
                ss << f_;
                return ss.str();
            }

        private:
            float f_;
        };

        class array_cond : public cond
        {
        public:
            array_cond(const std::vector<float>& f) : f_(f) {}
            virtual int type() const { return TYPE_ARRAY; }
            virtual bool to_b() const { return (bool)to_i(); }
            virtual int to_i() const { return (int)f_.size(); }
            virtual float to_f() const { return (float)f_.size(); }
            virtual std::vector<float> to_a() const { return f_; }
            virtual std::string to_s() const
            {
                std::stringstream ss;
                ss << "[";
                for (size_t i = 0; i < f_.size(); i++)
                {
                    ss << f_[i];
                    if (i != f_.size())
                    {
                        ss << " ";
                    }
                }
                ss << "]";
                return ss.str();
            }

        private:
            std::vector<float> f_;
        };

        class string_cond : public cond
        {
        public:
            string_cond(const std::string& str)
            {
                std::string tmp = str;
                size_t s, e;
                if ((s = tmp.find_first_of('\'')) != std::string::npos)
                {
                    tmp = tmp.substr(s + 1);
                }
                if ((e = tmp.find_last_of('\'')) != std::string::npos)
                {
                    tmp = tmp.substr(0, e);
                }

                s_ = tmp;
            }
            virtual int type() const { return TYPE_STRING; }
            virtual bool to_b() const { return (s_ == "") ? false : true; }
            virtual int to_i() const { return (to_b()) ? 1 : 0; }
            virtual float to_f() const { return (to_b()) ? 1.0f : 0.0f; }
            virtual std::string to_s() const { return s_; }

        private:
            std::string s_;
        };

        class paren_cond : public cond
        {
        public:
            paren_cond(cond* c) : c_(c) {}
            ~paren_cond() { delete c_; }
            virtual int type() const { return c_->type(); }
            virtual bool to_b() const { return c_->to_b(); }
            virtual int to_i() const { return c_->to_i(); }
            virtual float to_f() const { return c_->to_f(); }
            virtual std::string to_s() const { return c_->to_s(); }

        private:
            cond* c_;
        };

        static std::vector<std::string> split(const std::string& str, char delim)
        {
            std::vector<std::string> res;
            size_t current = 0, found;
            while ((found = str.find_first_of(delim, current)) != std::string::npos)
            {
                res.push_back(std::string(str, current, found - current));
                current = found + 1;
            }
            res.push_back(std::string(str, current, str.size() - current));
            return res;
        }

        static int ToType(const ri_classifier::param_data& data)
        {
            int nType = data.get_type();
            switch (nType)
            {
            case ri_classifier::INTEGER:
                return TYPE_INT;
            case ri_classifier::FLOAT:
            {
                if (data.get_size() == 1)
                    return TYPE_FLOAT;
                else
                    return TYPE_ARRAY;
            }
            case ri_classifier::COLOR:
            case ri_classifier::POINT:
            case ri_classifier::VECTOR:
            case ri_classifier::NORMAL:
            case ri_classifier::HPOINT:
            case ri_classifier::MATRIX:
            case ri_classifier::BASIS:
                return TYPE_ARRAY;
            case ri_classifier::STRING:
                return TYPE_STRING;
            }
            return TYPE_STRING;
        }

        class val_cond : public cond
        {
        public:
            val_cond(const std::string& s, const ri_classifier& cls,
                     const ri_options& opts, const ri_attributes& attr)
                : cls_(cls), opts_(opts), attr_(attr)
            {
                tokens_ = split(s, ':');
            }
            virtual int type() const
            {
                std::vector<std::string> tokens = tokens_;
                if (tokens.size() == 3)
                {
                    std::string key = tokens[0] + ":" + tokens[1] + ":" + tokens[2];
                    int id = cls_.find(key.c_str());
                    if (id >= 0)
                    {
                        return ToType(cls_[id]);
                    }
                }
                if (tokens.size() == 2)
                {
                    std::string k = tokens[0].substr(1);
                    std::string key = "$Attribute:" + k + ":" + tokens[1];
                    int id = cls_.find(key.c_str());
                    if (id >= 0)
                    {
                        return ToType(cls_[id]);
                    }
                }
                if (tokens.size() == 1)
                {
                    std::string str = ri_config::parse(tokens[0].c_str());
                    if (str != tokens[0])
                    {
                        if (IsFloat(str))
                            return TYPE_FLOAT;
                        if (IsInt(str))
                            return TYPE_INT;
                        return TYPE_STRING;
                    }
                }
                return TYPE_STRING;
            }
            virtual bool to_b() const { return (bool)to_i(); }
            virtual int to_i() const
            {
                int f;
                std::vector<std::string> tokens = tokens_;
                if (tokens.size() == 3)
                {
                    if (tokens[0] == "$Option")
                    {
                        if (opts_.get(tokens[1].c_str(), tokens[2].c_str(), f))
                            return f;
                    }
                    else if (tokens[0] == "$Attribute")
                    {
                        if (attr_.get(tokens[1].c_str(), tokens[2].c_str(), f))
                            return f;
                    }
                }
                else if (tokens.size() == 2)
                {
                    std::string key = tokens[0].substr(1);
                    if (attr_.get(key.c_str(), tokens[1].c_str(), f))
                        return f;
                }
                else if (tokens.size() == 1)
                {
                    std::string str = ri_config::parse(tokens[0].c_str());
                    if (str != tokens[0])
                    {
                        return atoi(str.c_str());
                    }
                }
                return 0;
            }
            virtual float to_f() const
            {
                float f;
                std::vector<std::string> tokens = tokens_;
                if (tokens.size() == 3)
                {
                    if (tokens[0] == "$Option")
                    {
                        if (opts_.get(tokens[1].c_str(), tokens[2].c_str(), f))
                            return f;
                    }
                    else if (tokens[0] == "$Attribute")
                    {
                        if (attr_.get(tokens[1].c_str(), tokens[2].c_str(), f))
                            return f;
                    }
                }
                else if (tokens.size() == 2)
                {
                    std::string key = tokens[0].substr(1);
                    if (attr_.get(key.c_str(), tokens[1].c_str(), f))
                        return f;
                }
                else if (tokens.size() == 1)
                {
                    std::string str = ri_config::parse(tokens[0].c_str());
                    if (str != tokens[0])
                    {
                        return (float)atof(str.c_str());
                    }
                }
                return 0;
            }
            virtual std::string to_s() const
            {
                std::string s;
                std::vector<std::string> tokens = tokens_;
                if (tokens.size() == 3)
                {
                    if (tokens[0] == "$Option")
                    {
                        if (opts_.get(tokens[1].c_str(), tokens[2].c_str(), s))
                        {
                            return s;
                        }
                    }
                    else if (tokens[0] == "$Attribute")
                    {
                        if (attr_.get(tokens[1].c_str(), tokens[2].c_str(), s))
                        {
                            return s;
                        }
                    }
                }
                else if (tokens.size() == 2)
                {
                    std::string key = tokens[0].substr(1);
                    if (attr_.get(key.c_str(), tokens[1].c_str(), s))
                        return s;
                }
                else if (tokens.size() == 1)
                {
                    std::string str = ri_config::parse(tokens[0].c_str());
                    return str;
                }
                return "";
            }

        private:
            std::vector<std::string> tokens_;
            const ri_classifier& cls_;
            const ri_options& opts_;
            const ri_attributes& attr_;
        };

        class defined_cond : public cond
        {
        public:
            defined_cond(const std::string& s, const ri_classifier& cls,
                         const ri_options& opts, const ri_attributes& attr)
                : cls_(cls), opts_(opts), attr_(attr)
            {
                tokens_ = split(s, ':');
            }
            virtual int type() const { return TYPE_BOOL; }
            virtual bool to_b() const
            {
                std::vector<std::string> tokens = tokens_;
                if (tokens.size() == 3)
                {
                    std::string key = tokens[0] + ":" + tokens[1] + ":" + tokens[2];
                    int id = cls_.find(key.c_str());
                    if (id >= 0)
                    {
                        return true;
                    }
                }
                if (tokens.size() == 2)
                {
                    std::string k = tokens[0].substr(1);
                    std::string key = "$Attribute:" + k + ":" + tokens[1];
                    int id = cls_.find(key.c_str());
                    if (id >= 0)
                    {
                        return true;
                    }
                }
                return false;
            }
            virtual int to_i() const { return (to_b()) ? 1 : 0; }
            virtual float to_f() const { return (to_b()) ? 1.0f : 0.0f; }
            virtual std::string to_s() const { return (to_b()) ? "1" : "0"; }

        private:
            std::vector<std::string> tokens_;
            const ri_classifier& cls_;
            const ri_options& opts_;
            const ri_attributes& attr_;
        };

        class mul_cond : public cond
        {
        public:
            mul_cond(cond* a, cond* b) : a_(a), b_(b) {}
            ~mul_cond()
            {
                delete a_;
                delete b_;
            }
            virtual int type() const { return TYPE_FLOAT; }
            virtual bool to_b() const { return (bool)to_i(); }
            virtual int to_i() const { return (int)to_f(); }
            virtual float to_f() const { return a_->to_f() * b_->to_f(); }
            virtual std::string to_s() const { return ""; }

        protected:
            cond* a_;
            cond* b_;
        };

        class div_cond : public cond
        {
        public:
            div_cond(cond* a, cond* b) : a_(a), b_(b) {}
            ~div_cond()
            {
                delete a_;
                delete b_;
            }
            virtual int type() const { return TYPE_FLOAT; }
            virtual bool to_b() const { return (bool)to_i(); }
            virtual int to_i() const { return (int)to_f(); }
            virtual float to_f() const { return a_->to_f() / b_->to_f(); }
            virtual std::string to_s() const { return ""; }

        protected:
            cond* a_;
            cond* b_;
        };

        class add_cond : public cond
        {
        public:
            add_cond(cond* a, cond* b) : a_(a), b_(b) {}
            ~add_cond()
            {
                delete a_;
                delete b_;
            }
            virtual int type() const { return TYPE_FLOAT; }
            virtual bool to_b() const { return (bool)to_i(); }
            virtual int to_i() const { return (int)to_f(); }
            virtual float to_f() const { return a_->to_f() + b_->to_f(); }
            virtual std::string to_s() const { return ""; }

        protected:
            cond* a_;
            cond* b_;
        };

        class sub_cond : public cond
        {
        public:
            sub_cond(cond* a, cond* b) : a_(a), b_(b) {}
            ~sub_cond()
            {
                delete a_;
                delete b_;
            }
            virtual int type() const { return TYPE_FLOAT; }
            virtual bool to_b() const { return (bool)to_i(); }
            virtual int to_i() const { return (int)to_f(); }
            virtual float to_f() const { return a_->to_f() - b_->to_f(); }
            virtual std::string to_s() const { return ""; }

        protected:
            cond* a_;
            cond* b_;
        };

        //------------------------------------------------------

        static void PrintTokens(std::vector<std::string>& tokens)
        {
            size_t sz = tokens.size();
            std::cout << "[[";
            for (size_t i = 0; i < sz; i++)
            {
                std::cout << tokens[i] << ",";
            }
            std::cout << "]]" << std::endl;
        }

//------------------------------------------------------
#if 0

#else
        class ConditionCompiler
        {
        public:
            ConditionCompiler(const char* str, const ri_classifier& cls,
                              const ri_options& opts, const ri_attributes& attr);
            ~ConditionCompiler();
            cond* Compile();
            void reset() { cur_ = 0; }

        protected:
            Token GetToken();
            void UngetToken();

        protected:
            cond* ToRelation();
            cond* ToExpression();
            cond* ToTerm();
            cond* ToPrimary();
            cond* ToInt();
            cond* ToFloat();
            cond* ToArray();
            cond* ToString();
            cond* ToVariable();
            cond* ToFunction();

        protected:
            int cur_;
            const ri_classifier& cls_;
            const ri_options& opts_;
            const ri_attributes& attr_;
            std::vector<Token> tokens_;
        };

        cond* ConditionCompiler::Compile() { return ToRelation(); }

        Token ConditionCompiler::GetToken()
        {
            if (0 <= cur_ && cur_ < tokens_.size())
            {
                Token tmp = tokens_[cur_];
                cur_++;
                return tmp;
            }
            else
            {
                Token tmp;
                tmp.kind = TOKEN_EOF;
                return tmp;
            }
        }

        void ConditionCompiler::UngetToken() { cur_--; }

        cond* ConditionCompiler::ToRelation()
        {
            Token tmp = GetToken();
            if (tmp.kind == TOKEN_LP)
            {
                std::unique_ptr<cond> ak(ToRelation());
                if (!ak.get())
                    return 0;
                tmp = GetToken();
                if (tmp.kind != TOKEN_RP)
                {
                    return NULL;
                }
                return ak.release();
            }
            else if (tmp.kind == TOKEN_NOT)
            {
                std::unique_ptr<cond> ak(ToRelation());
                if (!ak.get())
                    return 0;
                return new not_cond(ak.release());
            }
            else
            {
                UngetToken();

                std::unique_ptr<cond> lp;
                lp.reset(ToExpression());
                tmp = GetToken();
                if (tmp.kind == TOKEN_EQ)
                {
                    std::unique_ptr<cond> rp(ToRelation());
                    if (!rp.get())
                        return 0;
                    return new eq_cond(lp.release(), rp.release());
                }
                else if (tmp.kind == TOKEN_NE)
                {
                    std::unique_ptr<cond> rp(ToRelation());
                    if (!rp.get())
                        return 0;
                    return new ne_cond(lp.release(), rp.release());
                }
                else if (tmp.kind == TOKEN_LE)
                {
                    std::unique_ptr<cond> rp(ToRelation());
                    if (!rp.get())
                        return 0;
                    return new le_cond(lp.release(), rp.release());
                }
                else if (tmp.kind == TOKEN_GE)
                {
                    std::unique_ptr<cond> rp(ToRelation());
                    if (!rp.get())
                        return 0;
                    return new ge_cond(lp.release(), rp.release());
                }
                else if (tmp.kind == TOKEN_GT)
                {
                    std::unique_ptr<cond> rp(ToRelation());
                    if (!rp.get())
                        return 0;
                    return new gt_cond(lp.release(), rp.release());
                }
                else if (tmp.kind == TOKEN_LS)
                {
                    std::unique_ptr<cond> rp(ToRelation());
                    if (!rp.get())
                        return 0;
                    return new ls_cond(lp.release(), rp.release());
                }
                else if (tmp.kind == TOKEN_AND)
                {
                    std::unique_ptr<cond> rp(ToRelation());
                    if (!rp.get())
                        return 0;
                    return new and_cond(lp.release(), rp.release());
                }
                else if (tmp.kind == TOKEN_OR)
                {
                    std::unique_ptr<cond> rp(ToRelation());
                    if (!rp.get())
                        return 0;
                    return new or_cond(lp.release(), rp.release());
                }
                else
                {
                    UngetToken();
                    return lp.release();
                }
            }
        }

        cond* ConditionCompiler::ToExpression()
        {
            Token tmp = GetToken();
            if (tmp.kind == TOKEN_LP)
            {
                std::unique_ptr<cond> ak(ToExpression());
                if (!ak.get())
                    return 0;
                tmp = GetToken();
                if (tmp.kind != TOKEN_RP)
                {
                    return NULL;
                }
                return ak.release();
            }
            else
            {
                UngetToken();

                std::unique_ptr<cond> lp;
                lp.reset(ToTerm());
                tmp = GetToken();
                if (tmp.kind == TOKEN_ADD)
                {
                    std::unique_ptr<cond> rp(ToExpression());
                    if (!rp.get())
                        return 0;
                    return new add_cond(lp.release(), rp.release());
                }
                else if (tmp.kind == TOKEN_SUB)
                {
                    std::unique_ptr<cond> rp(ToExpression());
                    if (!rp.get())
                        return 0;
                    return new sub_cond(lp.release(), rp.release());
                }
                else
                {
                    UngetToken();
                    return lp.release();
                }
            }
        }

        cond* ConditionCompiler::ToTerm()
        {
            Token tmp = GetToken();
            if (tmp.kind == TOKEN_LP)
            {
                std::unique_ptr<cond> ak(ToTerm());
                if (!ak.get())
                    return 0;
                tmp = GetToken();
                if (tmp.kind != TOKEN_RP)
                {
                    return NULL;
                }
                return ak.release();
            }
            else
            {
                UngetToken();

                std::unique_ptr<cond> lp;
                lp.reset(ToPrimary());
                //cond* c = lp.get();
                tmp = GetToken();
                if (tmp.kind == TOKEN_MUL)
                {
                    std::unique_ptr<cond> rp(ToTerm());
                    if (!rp.get())
                        return 0;
                    return new mul_cond(lp.release(), rp.release());
                }
                else if (tmp.kind == TOKEN_DIV)
                {
                    std::unique_ptr<cond> rp(ToTerm());
                    if (!rp.get())
                        return 0;
                    return new div_cond(lp.release(), rp.release());
                }
                else
                {
                    UngetToken();
                    return lp.release();
                }
            }
        }

        cond* ConditionCompiler::ToPrimary()
        {
            Token tmp = GetToken();
            if (tmp.kind == TOKEN_LP)
            {
                std::unique_ptr<cond> ak(ToPrimary());
                if (!ak.get())
                    return 0;
                tmp = GetToken();
                if (tmp.kind != TOKEN_RP)
                {
                    return NULL;
                }
                return ak.release();
            }
            else
            {
                UngetToken();

                cond* c = 0;
                if (c = ToInt())
                {
                    return c;
                }
                if (c = ToFloat())
                {
                    return c;
                }
                if (c = ToArray())
                {
                    return c;
                }
                if (c = ToString())
                {
                    return c;
                }
                if (c = ToVariable())
                {
                    return c;
                }
                return 0;
            }
        }

        cond* ConditionCompiler::ToInt()
        {
            Token tmp = GetToken();
            if (tmp.kind == TOKEN_INT)
            {
                return new int_cond(atoi(tmp.value.c_str()));
            }
            else
            {
                UngetToken();
                return NULL;
            }
        }

        cond* ConditionCompiler::ToFloat()
        {
            Token tmp = GetToken();
            if (tmp.kind == TOKEN_FLOAT)
            {
                return new float_cond((float)atof(tmp.value.c_str()));
            }
            else
            {
                UngetToken();
                return NULL;
            }
        }

        cond* ConditionCompiler::ToArray()
        {
            Token tmp = GetToken();
            if (tmp.kind == TOKEN_LB)
            {
                std::vector<float> fs;
                int count = 1;
                do
                {
                    tmp = GetToken();
                    count++;
                    if (tmp.kind == TOKEN_INT || tmp.kind == TOKEN_FLOAT)
                    {
                        float f = (float)atof(tmp.value.c_str());
                        fs.push_back(f);
                    }
                    else if (tmp.kind == TOKEN_RB)
                    {
                        return new array_cond(fs);
                    }
                    else
                    {
                        break;
                    }
                } while (1);
                for (int i = 0; i < count; i++)
                {
                    UngetToken();
                }
                return NULL;
            }
            else
            {
                UngetToken();
                return NULL;
            }
        }

        cond* ConditionCompiler::ToString()
        {
            Token tmp = GetToken();
            if (tmp.kind == TOKEN_STRING)
            {
                return new string_cond(tmp.value);
            }
            else
            {
                UngetToken();
                return NULL;
            }
        }
        cond* ConditionCompiler::ToVariable()
        {
            Token tmp = GetToken();
            if (tmp.kind == TOKEN_VARIABLE)
            {
                return new val_cond(tmp.value, cls_, opts_, attr_);
            }
            else
            {
                UngetToken();
                return NULL;
            }
        }
        cond* ConditionCompiler::ToFunction()
        {
            Token tmp = GetToken();
            if (tmp.kind == TOKEN_DEFINED)
            {
                tmp = GetToken();
                if (tmp.kind != TOKEN_LP)
                {
                    UngetToken();
                    UngetToken();
                    return NULL;
                }
                tmp = GetToken();
                if (tmp.kind != TOKEN_VARIABLE)
                {
                    UngetToken();
                    UngetToken();
                    UngetToken();
                    return NULL;
                }
                std::string valname = tmp.value;
                tmp = GetToken();
                if (tmp.kind != TOKEN_VARIABLE)
                {
                    UngetToken();
                    UngetToken();
                    UngetToken();
                    UngetToken();
                    return NULL;
                }

                return new defined_cond(valname, cls_, opts_, attr_);
            }
            else if (tmp.kind == TOKEN_CONCAT)
            {
                UngetToken();
                return NULL;
            }
            else
            {
                UngetToken();
                return NULL;
            }
        }

        ConditionCompiler::ConditionCompiler(const char* str, const ri_classifier& cls,
                                             const ri_options& opts,
                                             const ri_attributes& attr)
            : cur_(0), cls_(cls), opts_(opts), attr_(attr)
        {
            //if (!IsEvaluatableCondition(str))
            //    return;
            std::vector<std::string> tokens;
            SplitToTokens(tokens, str);
            for (size_t i = 0; i < tokens.size(); i++)
            {
                std::string s = tokens[i];
                Token tmp;
                tmp.kind = GetTokenKind(s.c_str());
                tmp.value = s;
                tokens_.push_back(tmp);
            }
        }

        ConditionCompiler::~ConditionCompiler()
        {
            ; //
        }
#endif
    }

    ri_condition_if::ri_condition_if(const ri_classifier& cls,
                                     const ri_options& opts,
                                     const ri_attributes& attr, const char* str)
    {
        ConditionCompiler cc(str, cls, opts, attr);
        std::unique_ptr<cond> c(cc.Compile());
        if(c.get())
        {
            this->is_valid_ = true;
            this->result_ = c->to_b();
        }
        else
        {
            this->is_valid_ = false;
            this->result_ = false;
        }
    }
    ri_condition_if::~ri_condition_if()
    {
        //
    }
    bool ri_condition_if::is_valid() const
    {
        return this->is_valid_;
    }
    bool ri_condition_if::evaluate() const
    {
        return this->result_;
    }
}
