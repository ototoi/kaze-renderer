#ifndef RI_RIB_DECODER_H
#define RI_RIB_DECODER_H

#include <cstdio>
#include <string>
#include <vector>
#include <map>

namespace ri
{
    class ri_rib_decoder
    {
    public:
        ri_rib_decoder(FILE* fp);
        ~ri_rib_decoder();
        int read(char* buffer, unsigned int size);

    public:
        static std::string string_filter(const char* buffer);

    protected:
        std::string next_token();

    private:
        FILE* fp_;
        bool bBinary_;
        std::vector<unsigned char> buff_;
        std::string requests_[256];
        std::map<int, std::string> strings_;
    };
}

#endif
