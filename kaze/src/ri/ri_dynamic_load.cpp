#include "ri_dynamic_load.h"
#include "ri.h"

#include "ri_read_archive.h"
#include "ri_context.h"
#include "ri_logger.h"

#include "ri_file_util.h"

#include <string.h>
//#include <fstream>
#include <sstream>

#ifdef _WIN32
#include "shlwapi.h"
#pragma comment(lib, "shlwapi.lib")
#else
#include <stdlib.h>
#include <errno.h>
#endif

#include <math.h>
#include <string.h>
#include <stdlib.h>

namespace
{
    typedef RtPointer ConvertParameters(RtString paramstr);
    typedef RtVoid Subdivide(RtPointer data, RtFloat detail);
    typedef RtVoid Free(RtPointer data);

    static
    RtPointer ConvertParameters_Dummy(RtString paramstr)
    {
        float* pf = (float*)malloc(sizeof(float));
        *pf = atof(paramstr);
        return (RtPointer)pf;
    }

    static
    RtVoid Subdivide_Dummy(RtPointer data, RtFloat detail)
    {
        ;//
    }

    static
    RtVoid Free_Dummy(RtPointer data)
    {
       free(data);
    }

    struct DynamicLoadFunctions
    {
        ConvertParameters* pConvertParameters;
        Subdivide* pSubdivide;
        Free* pFree;
    };


}

namespace ri
{   
    int ri_dynamic_load(ri_context* ctx, const char* program, float area, const char* arg)
    {
        int nRet = 0;
        DynamicLoadFunctions dlf;
        dlf.pConvertParameters = ConvertParameters_Dummy;
        dlf.pSubdivide         = Subdivide_Dummy;
        dlf.pFree              = Free_Dummy;
        
        //load from dll

        //mtx.lock();
        ////
        ::RiContext((RtContextHandle)ctx);
        RtPointer pData = dlf.pConvertParameters((RtString)arg);
        if(pData == NULL)return -1;
        dlf.pSubdivide(pData, area);
        dlf.pFree(pData);
        ////
        //mtx.unlock();

        return nRet;
    }
}
