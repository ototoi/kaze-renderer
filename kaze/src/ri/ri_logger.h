#ifndef RI_LOGGER_H
#define RI_LOGGER_H

#include <vector>
#include <string>

namespace ri
{
    typedef int logtype_t;

    class logger
    {
    public:
        virtual void print(const char* str) = 0;

        virtual ~logger() {}
    };

    class multi_logger : public logger
    {
    public:
        ~multi_logger();
        void add(logger* lgr);

    public:
        void print(const char* cstr);

    private:
        std::vector<logger*> lgrlist_;
    };

    //-----------------------------------------------------

    logger* get_logger();
    logger* get_logger_debug();
    logger* get_logger_progress();

    void set_logger(const char* key, logger* lgr);
    void set_logger_debug(const char* key, logger* lgr);
    void set_logger_progress(const char* key, logger* lgr);

    void print_log(const char* str, ...);
    void print_debug(const char* str, ...);
    void print_progress(const char* str, ...);

    class logger_set : public logger
    {
        struct LnK
        {
            std::string key;
            logger* p_logger;
        };

    public:
        ~logger_set();
        void add(const char* key, logger* lgr);
        void erase(const char* key);

    public:
        void print(const char* cstr);

    private:
        std::vector<LnK> lgrlist_;
    };
}

#endif
