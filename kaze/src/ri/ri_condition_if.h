#ifndef RI_CONDITION_IF_H
#define RI_CONDITION_IF_H

#include "ri_classifier.h"
#include "ri_options.h"
#include "ri_attributes.h"
#include <string>

namespace ri
{
    class ri_condition_if
    {
    public:
        ri_condition_if(const ri_classifier& cls, const ri_options& opts,
                        const ri_attributes& attr, const char* str);
        ~ri_condition_if();
        bool is_valid() const;
        bool evaluate() const;
    protected:
        bool is_valid_;
        bool result_;
    };
}

#endif
