#ifndef RI_RIB_PARSE_PARAM_H
#define RI_RIB_PARSE_PARAM_H

namespace ri
{

    typedef struct ri_rib_parse_param
    {
        ri_context* ctx;
        ri_rib_decoder* dec;
        bool fRequest;
        bool fParams;
        bool fSkipFrame;
        int line_number;

        void scan_begin();
        void scan_end();
    } ri_rib_parse_param;

    typedef ri_rib_parse_param rib_parse_param;
}

#endif
