#include "ri_evaluate_blobby.h"
#include "ri_object.h"
#include "ri_types.h"
#include "ri_logger.h"

#include <string>
#include <vector>
#include <stack>
#include <cmath>
#include <memory>

#include <stdio.h>

#ifdef _WIN32
#define snprintf _snprintf
#endif

namespace ri
{
    using namespace std;

    static std::shared_ptr<ri_blobby>
    CreateBlobby(int code, const std::vector<std::shared_ptr<ri_blobby> >& children)
    {
        switch (code)
        {
        case RI_BLOBBY_ADD:
            return std::shared_ptr<ri_blobby>(new ri_add_blobby(children));
        case RI_BLOBBY_MUL:
            return std::shared_ptr<ri_blobby>(new ri_mul_blobby(children));
        case RI_BLOBBY_MIN:
            return std::shared_ptr<ri_blobby>(new ri_min_blobby(children));
        case RI_BLOBBY_MAX:
            return std::shared_ptr<ri_blobby>(new ri_max_blobby(children));
        case RI_BLOBBY_SUB:
            return std::shared_ptr<ri_blobby>(new ri_sub_blobby(children));
        case RI_BLOBBY_DIV:
            return std::shared_ptr<ri_blobby>(new ri_div_blobby(children));
        case RI_BLOBBY_NEG:
            return std::shared_ptr<ri_blobby>(new ri_neg_blobby(children[0]));
        }
        return std::shared_ptr<ri_blobby>();
    }

    static std::shared_ptr<ri_blobby> EvalBlobbyCodes(int nleaf, int ncode,
                                                const int code[], int nflt,
                                                const float flt[], int nstr,
                                                const std::string str[])
    {
        std::vector<std::shared_ptr<ri_blobby> > stack;
        size_t lidx = 0; // for laef count
        int pc = 0;
        while (pc < ncode)
        {
            int c = code[pc];
            switch (c)
            {
            case RI_BLOBBY_ADD:
            case RI_BLOBBY_MUL:
            case RI_BLOBBY_MAX:
            case RI_BLOBBY_MIN:
            {
                int count = code[pc + 1];
                std::vector<std::shared_ptr<ri_blobby> > children;
                for (int i = 0; i < count; ++i)
                {
                    int idx = code[pc + 2 + i];
                    if (0 <= idx && idx < stack.size())
                    {
                        children.push_back(stack[idx]);
                    }
                }

                stack.push_back(CreateBlobby(c, children));

                pc += count + 2;
            }
            break;
            case RI_BLOBBY_SUB:
            case RI_BLOBBY_DIV:
            {
                int count = 2;
                std::vector<std::shared_ptr<ri_blobby> > children;
                for (int i = 0; i < count; ++i)
                {
                    int idx = code[pc + 1 + i];
                    if (0 <= idx && idx < stack.size())
                    {
                        children.push_back(stack[idx]);
                    }
                }

                stack.push_back(CreateBlobby(c, children));

                pc += 3;
            }
            break;
            case RI_BLOBBY_NEG:
            {
                int count = 1;
                std::vector<std::shared_ptr<ri_blobby> > children;
                for (int i = 0; i < count; ++i)
                {
                    int idx = code[pc + 1 + i];
                    if (0 <= idx && idx < stack.size())
                    {
                        children.push_back(stack[idx]);
                    }
                }

                stack.push_back(CreateBlobby(c, children));

                pc += 2;
            }
            break;
            case RI_BLOBBY_AIR:
            {
                int subcode = code[pc + 1];
                std::shared_ptr<ri_blobby> ap(new ri_air_blobby(subcode));
                ri_air_blobby* b = (ri_air_blobby*)ap.get();
                if (1 <= subcode)
                {
                    b->set_name(str[code[pc + 2]]);
                }
                if (2 <= subcode)
                {
                    float m[16];
                    memcpy(m, flt + code[pc + 3], sizeof(float) * 16);
                    b->set_mat(m);
                }
                if (4 <= subcode)
                {
                    int n = code[pc + 4];
                    if (n)
                    {
                        int idx = code[pc + 5];
                        std::vector<float> f(n);
                        memcpy(&f[0], flt + idx, sizeof(float) * n);
                        b->set_float(f);
                    }
                }
                if (6 <= subcode)
                {
                    int n = code[pc + 6];
                    if (n)
                    {
                        int idx = code[pc + 7];
                        std::vector<std::string> s(n);
                        for (int i = 0; i < n; i++)
                        {
                            s[i] = str[idx + i];
                        }
                        b->set_string(s);
                    }
                }
                if (7 <= subcode)
                {
                    int n = code[pc + 8];
                    if (n)
                    {
                        std::vector<int> ii(n);
                        for (int i = 0; i < n; i++)
                        {
                            ii[i] = code[pc + 9 + i];
                        }
                        b->set_int(ii);
                    }
                }

                ap->set_index(lidx);
                lidx++;
                stack.push_back(ap);

                pc += 2 + subcode;
            }
            break;
            case RI_BLOBBY_IDENTITY:
            {
                std::shared_ptr<ri_blobby> ap(new ri_identity_blobby());
                ap->set_index(lidx);
                lidx++;
                stack.push_back(ap);
                pc++;
            }
            break;
            case RI_BLOBBY_CONSTANT:
            {
                int index = code[pc + 1];
                float result = flt[index];
                std::shared_ptr<ri_blobby> ap(new ri_constant_blobby(result));
                ap->set_index(lidx);
                lidx++;
                stack.push_back(ap);
                pc += 2;
            }
            break;
            case RI_BLOBBY_ELLIPSOID:
            {
                int index = code[pc + 1];
                float mat[16];
                memcpy(mat, flt + index, sizeof(float) * 16);
                std::shared_ptr<ri_blobby> ap(new ri_ellipsoid_blobby(mat));
                ap->set_index(lidx);
                lidx++;
                stack.push_back(ap);
                pc += 2;
            }
            break;
            case RI_BLOBBY_SEGMENT:
            {
                int index = code[pc + 1];
                float f[23]; // 23
                memcpy(f, flt + index, sizeof(float) * 23);
                std::shared_ptr<ri_blobby> ap(new ri_segment_blobby(f));
                ap->set_index(lidx);
                lidx += 2;
                stack.push_back(ap);
                pc += 2;
            }
            break;
            case RI_BLOBBY_PLANE:
            {
                int index_s = code[pc + 1];
                int index_p = code[pc + 2];
                float f[4]; // 4
                std::string name = str[index_s];
                memcpy(f, flt + index_p, sizeof(float) * 4);
                std::shared_ptr<ri_blobby> ap(new ri_plane_blobby(name, f));
                ap->set_index(lidx);
                lidx++;
                stack.push_back(ap);
                pc += 3;
            }
            break;
            case RI_BLOBBY_IMPLICIT:
            {
                int index_name = code[pc + 1];
                int size_f = code[pc + 2];
                int index_f = code[pc + 3];

                int size_s = code[pc + 4];
                int index_s = code[pc + 5];

                std::vector<float> f;
                std::vector<std::string> s;
                std::string name = str[index_name];

                if (size_f)
                {
                    f.resize(size_f);
                    for (int i = 0; i < size_f; i++)
                    {
                        f[i] = flt[index_f + i];
                    }
                }

                if (size_s)
                {
                    s.resize(size_s);
                    for (int i = 0; i < size_s; i++)
                    {
                        s[i] = str[index_s + i];
                    }
                }

                std::shared_ptr<ri_blobby> ap(
                    new ri_implicit_field_blobby(name, f, s)); // dummy
                ap->set_index(lidx);

                lidx += 1;
                stack.push_back(ap);
                pc += 6;
            }
            break;
            default:
            {
                pc++;
                print_log("unknown blobby instruction code %d.\n", c);
            }
            break;
            }
        }

        for (int i = (int)stack.size() - 1; i >= 0; i--)
        {
            if (!stack[i]->is_leaf())
            {
                return stack[i];
            }
        }
        return std::shared_ptr<ri_blobby>();
    }

    std::shared_ptr<ri_blobby> ri_evaluate_blobby(int nleaf, int ncode, const int code[],
                                            int nflt, const float flt[], int nstr,
                                            const std::string str[])
    {
        return EvalBlobbyCodes(nleaf, ncode, code, nflt, flt, nstr, str);
    }

    static void put_tab(int tabs)
    {
        for (int i = 0; i < tabs; i++)
            print_log("\t");
    }
#define TAB(n) put_tab(n)

    void ri_print_blobby(const std::shared_ptr<ri_blobby>& blobby, int tab = 0)
    {
        int nCode = blobby->typeN();
        switch (nCode)
        {
        case RI_BLOBBY_ADD:
        case RI_BLOBBY_MUL:
        case RI_BLOBBY_MAX:
        case RI_BLOBBY_MIN:
        case RI_BLOBBY_DIV:
        case RI_BLOBBY_SUB:
        case RI_BLOBBY_NEG:
        case RI_BLOBBY_FMA:
        case RI_BLOBBY_COMPOSITE:
        {
            const base_composite_blobby* b =
                dynamic_cast<const base_composite_blobby*>(blobby.get());
            const std::vector<std::shared_ptr<ri_blobby> >& cld = b->get_children();
            TAB(tab);
            print_log("%s {\n", b->type().c_str());
            for (size_t i = 0; i < cld.size(); i++)
            {
                ri_print_blobby(cld[i], tab + 1);
            }
            TAB(tab);
            print_log("}\n");
        }
        break;
        case RI_BLOBBY_AIR:
        {
            TAB(tab);
            print_log("%s:%d\n", blobby->type().c_str(), blobby->get_index());
        }
        break;
        case RI_BLOBBY_ELLIPSOID:
        case RI_BLOBBY_SEGMENT:
        case RI_BLOBBY_IMPLICIT:
        case RI_BLOBBY_PLANE:
        case RI_BLOBBY_SPHERE:
        case RI_BLOBBY_POINTS:
        {
            TAB(tab);
            print_log("%s:%d\n", blobby->type().c_str(), blobby->get_index());
        }
        break;
        case RI_BLOBBY_IDENTITY:
        {
            const ri_identity_blobby* b =
                dynamic_cast<const ri_identity_blobby*>(blobby.get());

            TAB(tab);
            print_log("%s(%f):%d\n", blobby->type().c_str(), 1.0f, blobby->get_index());
        }
        break;
        case RI_BLOBBY_CONSTANT:
        {
            const ri_constant_blobby* b =
                dynamic_cast<const ri_constant_blobby*>(blobby.get());

            TAB(tab);
            print_log("%s(%f):%d\n", blobby->type().c_str(), b->get_value(),
                      blobby->get_index());
        }
        break;
        case RI_BLOBBY_VALUE:
        {
            const ri_value_blobby* b =
                dynamic_cast<const ri_value_blobby*>(blobby.get());

            TAB(tab);
            print_log("%s(%f):%d\n", blobby->type().c_str(), b->get_value(),
                      blobby->get_index());
        }
        break;
        }
    }

    struct uniform_sphere_info
    {
        float r;
        float t[3];
    };

    static matrix4 ConvertMatrix(const double mat[16])
    {
        matrix4 tmp(mat);
        tmp.transpose();
        return tmp;
    }

    static bool IsNearEqual(double a, double b)
    {
        return fabs(b - a) <= std::numeric_limits<double>::epsilon();
    }

    static bool IsUniform(uniform_sphere_info* info, const float m[])
    {
        double dm[16];
        for (int i = 0; i < 16; i++)
            dm[i] = (double)m[i];
        matrix4 M = ConvertMatrix(dm);
        vector3 t = vector3(M[0][3], M[1][3], M[2][3]);
        matrix4 T = mat4_gen::translation(-t[0], -t[1], -t[2]);
        matrix4 M2 = T * M;
        if (!IsNearEqual(M2[3][0], 0.0))
            return false;
        if (!IsNearEqual(M2[3][1], 0.0))
            return false;
        if (!IsNearEqual(M2[3][2], 0.0))
            return false;
        if (!IsNearEqual(M2[0][3], 0.0))
            return false;
        if (!IsNearEqual(M2[1][3], 0.0))
            return false;
        if (!IsNearEqual(M2[2][3], 0.0))
            return false;
        if (!IsNearEqual(M2[3][3], 1.0))
            return false;

        vector3 sV[6] = {vector3(M2[0][0], M2[0][1], M2[0][2]), // yoko
                         vector3(M2[1][0], M2[1][1], M2[1][2]),
                         vector3(M2[2][0], M2[2][1], M2[2][2]),
                         vector3(M2[0][0], M2[1][0], M2[2][0]), // tate
                         vector3(M2[0][1], M2[1][1], M2[2][1]),
                         vector3(M2[0][2], M2[1][2], M2[2][2])};

        double sg2 = 0;
        double sE = 0;
        double ss[6] = {};
        for (int i = 0; i < 6; i++)
        {
            ss[i] = sqrt(dot(sV[i], sV[i]));
            sE += ss[i];
        }
        sE /= 6;
        for (int i = 0; i < 6; i++)
        {
            double f = sE - ss[i];
            sg2 += f * f;
        }
        sg2 /= 6;
        if (!IsNearEqual(sg2, 0.0))
            return false;

        double s = sE;
        if (IsNearEqual(s, 0.0))
            return false;

        matrix4 iS = mat4_gen::scaling(1.0 / s, 1.0 / s, 1.0 / s);
        matrix4 R = iS * M2;
        matrix4 tR = transpose(R);
        matrix4 iR = ~R;
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (!IsNearEqual(tR[i][j], iR[i][j]))
                    return false;
            }
        }

        info->r = (float)s;
        info->t[0] = (float)t[0];
        info->t[1] = (float)t[1];
        info->t[2] = (float)t[2];

        return true;
    }

    static std::shared_ptr<ri_blobby>
    OptimizeBlobbySphereE(const std::shared_ptr<ri_blobby>& blobby)
    {
        const ri_ellipsoid_blobby* eb =
            dynamic_cast<const ri_ellipsoid_blobby*>(blobby.get());

        uniform_sphere_info info;
        bool bUniform = IsUniform(&info, eb->get_mat());

        if (bUniform)
        {
            // print_log("find uniform leaf: r=%f,t=[%f,%f,%f] \n", info.r, info.t[0],
            // info.t[1], info.t[2]);
            std::shared_ptr<ri_blobby> pRet(new ri_sphere_blobby(info.t, info.r));
            pRet->set_index(blobby->get_index());
            return pRet;
        }
        else
        {
            // print_log("no uniform leaf\n");
            return blobby;
        }
    }

    static std::shared_ptr<ri_blobby>
    OptimizeBlobbySphere(const std::shared_ptr<ri_blobby>& blobby)
    {
        if (blobby->is_branch())
        {
            ri_base_branch_blobby* b =
                dynamic_cast<ri_base_branch_blobby*>(blobby.get());
            std::vector<std::shared_ptr<ri_blobby> >& cld = b->get_children();
            for (size_t i = 0; i < cld.size(); i++)
            {
                cld[i] = OptimizeBlobbySphere(cld[i]);
            }
            return blobby;
        }
        else
        {
            int nCode = blobby->typeN();
            if (nCode == RI_BLOBBY_ELLIPSOID)
                return OptimizeBlobbySphereE(blobby);
            else
                return blobby;
        }
    }

    static std::shared_ptr<ri_blobby>
    OptimizeBlobbyValueConvert(const std::shared_ptr<ri_blobby>& blobby)
    {
        if (blobby->is_branch())
        {
            ri_base_branch_blobby* b =
                dynamic_cast<ri_base_branch_blobby*>(blobby.get());
            std::vector<std::shared_ptr<ri_blobby> >& cld = b->get_children();
            for (size_t i = 0; i < cld.size(); i++)
            {
                cld[i] = OptimizeBlobbyValueConvert(cld[i]);
            }
            return blobby;
        }
        else
        {
            int nCode = blobby->typeN();
            if (nCode == RI_BLOBBY_IDENTITY)
            {
                return std::shared_ptr<ri_blobby>(new ri_value_blobby(1.0f));
            }
            else if (nCode == RI_BLOBBY_CONSTANT)
            {
                ri_constant_blobby* b = dynamic_cast<ri_constant_blobby*>(blobby.get());
                return std::shared_ptr<ri_blobby>(new ri_value_blobby(b->get_value()));
            }
            else
                return blobby;
        }
    }

    static bool IsValue(const std::vector<std::shared_ptr<ri_blobby> >& cld)
    {
        for (size_t i = 0; i < cld.size(); i++)
        {
            if (cld[i]->typeN() != RI_BLOBBY_VALUE)
                return false;
        }
        return true;
    }

    static float GetFoldingValue(int nCode,
                                 const std::vector<std::shared_ptr<ri_blobby> >& cld)
    {
        std::vector<float> values(cld.size());
        for (size_t i = 0; i < cld.size(); i++)
        {
            values[i] =
                dynamic_cast<const ri_value_blobby*>(cld[i].get())->get_value();
        }

        switch (nCode)
        {
        case RI_BLOBBY_ADD:
        {
            float v = values[0];
            for (size_t i = 1; i < values.size(); i++)
                v += values[i];
            return v;
        }
        break;
        case RI_BLOBBY_MUL:
        {
            float v = values[0];
            for (size_t i = 1; i < values.size(); i++)
                v *= values[i];
            return v;
        }
        break;
        case RI_BLOBBY_MAX:
        {
            float v = values[0];
            for (size_t i = 1; i < values.size(); i++)
                v = std::max<float>(v, values[i]);
            return v;
        }
        break;
        case RI_BLOBBY_MIN:
        {
            float v = values[0];
            for (size_t i = 1; i < values.size(); i++)
                v = std::min<float>(v, values[i]);
            return v;
        }
        break;
        case RI_BLOBBY_SUB:
        {
            float v = values[0];
            for (size_t i = 1; i < values.size(); i++)
                v -= values[i];
            return v;
        }
        break;
        case RI_BLOBBY_DIV:
        {
            float v = values[0];
            for (size_t i = 1; i < values.size(); i++)
                v /= values[i];
            return v;
        }
        break;
        case RI_BLOBBY_NEG:
        {
            float v = values[0];
            for (size_t i = 1; i < values.size(); i++)
                v += values[i];
            return -v;
        }
        break;
        }
        return 1.0f;
    }

    static bool IsEqual(const std::vector<std::shared_ptr<ri_blobby> >& a,
                        const std::vector<std::shared_ptr<ri_blobby> >& b)
    {
        if (a.size() == b.size())
        {
            for (size_t i = 0; i < a.size(); i++)
            {
                if (a[i].get() != b[i].get())
                    return false;
            }
            return true;
        }
        return false;
    }

    static std::shared_ptr<ri_blobby>
    OptimizeBlobbyValueFolding(const std::shared_ptr<ri_blobby>& blobby)
    {
        if (blobby->is_branch())
        {
            ri_base_branch_blobby* b =
                dynamic_cast<ri_base_branch_blobby*>(blobby.get());
            const std::vector<std::shared_ptr<ri_blobby> >& cld = b->get_children();
            std::vector<std::shared_ptr<ri_blobby> > cld2;
            for (size_t i = 0; i < cld.size(); i++)
            {
                std::shared_ptr<ri_blobby> c = OptimizeBlobbyValueFolding(cld[i]);
                if (c.get())
                    cld2.push_back(c);
            }
            if (IsValue(cld2))
            {
                float v = GetFoldingValue(blobby->typeN(), cld2);
                return std::shared_ptr<ri_blobby>(new ri_value_blobby(v));
            }
            if (!IsEqual(cld, cld2))
            {
                int nCode = blobby->typeN();
                switch (nCode)
                {
                case RI_BLOBBY_ADD:
                    return std::shared_ptr<ri_blobby>(new ri_add_blobby(cld2));
                case RI_BLOBBY_MUL:
                    return std::shared_ptr<ri_blobby>(new ri_mul_blobby(cld2));
                case RI_BLOBBY_MAX:
                    return std::shared_ptr<ri_blobby>(new ri_max_blobby(cld2));
                case RI_BLOBBY_MIN:
                    return std::shared_ptr<ri_blobby>(new ri_min_blobby(cld2));
                case RI_BLOBBY_SUB:
                    return std::shared_ptr<ri_blobby>(new ri_sub_blobby(cld2));
                case RI_BLOBBY_DIV:
                    return std::shared_ptr<ri_blobby>(new ri_div_blobby(cld2));
                case RI_BLOBBY_NEG:
                    return std::shared_ptr<ri_blobby>(new ri_neg_blobby(cld2[0]));
                }
            }
            return blobby;
        }
        else
        {
            return blobby;
        }
    }

    static std::shared_ptr<ri_blobby> OptimizeBlobbyFMAHelper(
        int nCode, const std::vector<std::shared_ptr<ri_blobby> >& other_children)
    {
        // int nCode = blobby->typeN();
        if (other_children.size() == 1)
            return other_children[0];
        // assert(nCode == RI_BLOBBY_ADD || nCode == RI_BLOBBY_MULL);
        if (nCode == RI_BLOBBY_ADD)
            return std::shared_ptr<ri_blobby>(new ri_add_blobby(other_children));
        if (nCode == RI_BLOBBY_MUL)
            return std::shared_ptr<ri_blobby>(new ri_mul_blobby(other_children));
        if (nCode == RI_BLOBBY_MIN)
            return std::shared_ptr<ri_blobby>(new ri_min_blobby(other_children));
        if (nCode == RI_BLOBBY_MAX)
            return std::shared_ptr<ri_blobby>(new ri_max_blobby(other_children));
        return std::shared_ptr<ri_blobby>();
    }

    static std::shared_ptr<ri_blobby>
    OptimizeBlobbyFMA(const std::shared_ptr<ri_blobby>& blobby)
    {
        if (blobby->is_branch())
        {
            int nCode = blobby->typeN();
            ri_base_branch_blobby* b =
                dynamic_cast<ri_base_branch_blobby*>(blobby.get());
            const std::vector<std::shared_ptr<ri_blobby> >& cld = b->get_children();
            std::vector<std::shared_ptr<ri_blobby> > cld2;
            for (size_t i = 0; i < cld.size(); i++)
            {
                std::shared_ptr<ri_blobby> c = OptimizeBlobbyFMA(cld[i]);
                if (c.get())
                    cld2.push_back(c);
            }
            std::vector<std::shared_ptr<ri_blobby> > value_children;
            std::vector<std::shared_ptr<ri_blobby> > other_children;
            for (size_t i = 0; i < cld2.size(); i++)
            {
                if (cld[i]->typeN() == RI_BLOBBY_VALUE)
                {
                    value_children.push_back(cld2[i]);
                }
                else
                {
                    other_children.push_back(cld2[i]);
                }
            }
            if (other_children.empty())
                return std::shared_ptr<ri_blobby>();
            if (!value_children.empty())
            {
                switch (nCode)
                {
                case RI_BLOBBY_ADD:
                {
                    float val = 0.0f;
                    for (int i = 0; i < value_children.size(); i++)
                    {
                        val += dynamic_cast<ri_value_blobby*>(value_children[i].get())
                                   ->get_value();
                    }
                    return std::shared_ptr<ri_blobby>(new ri_fma_blobby(
                        1.0f, OptimizeBlobbyFMAHelper(nCode, other_children), val));
                }
                case RI_BLOBBY_MUL:
                {
                    float val = 1.0f;
                    for (int i = 0; i < value_children.size(); i++)
                    {
                        val *= dynamic_cast<ri_value_blobby*>(value_children[i].get())
                                   ->get_value();
                    }
                    return std::shared_ptr<ri_blobby>(new ri_fma_blobby(
                        val, OptimizeBlobbyFMAHelper(nCode, other_children), 0.0f));
                }
                case RI_BLOBBY_SUB:
                {
                    float val = dynamic_cast<ri_value_blobby*>(value_children[0].get())
                                    ->get_value();
                    if (cld[0].get() == value_children[0].get())
                    {
                        return std::shared_ptr<ri_blobby>(
                            new ri_fma_blobby(-1.0f, other_children[0], val));
                    }
                    else
                    {
                        return std::shared_ptr<ri_blobby>(
                            new ri_fma_blobby(+1.0f, other_children[0], -val));
                    }
                }
                case RI_BLOBBY_DIV:
                {
                    float val = dynamic_cast<ri_value_blobby*>(value_children[0].get())
                                    ->get_value();
                    if (cld[0].get() == value_children[0].get())
                    {
                        return std::shared_ptr<ri_blobby>(new ri_fma_blobby(
                            val, std::shared_ptr<ri_blobby>(new ri_inv_blobby(other_children[0])),
                            0.f));
                    }
                    else
                    {
                        return std::shared_ptr<ri_blobby>(
                            new ri_fma_blobby(1.0f / val, other_children[0], 0.f));
                    }
                }
                case RI_BLOBBY_MIN:
                {
                    float val = dynamic_cast<ri_value_blobby*>(value_children[0].get())
                                    ->get_value();
                    return std::shared_ptr<ri_blobby>(new ri_fma_blobby(
                        +1.0f, OptimizeBlobbyFMAHelper(nCode, other_children), val, 1.0f));
                }
                case RI_BLOBBY_MAX:
                {
                    float val = dynamic_cast<ri_value_blobby*>(value_children[0].get())
                                    ->get_value();
                    return std::shared_ptr<ri_blobby>(new ri_fma_blobby(
                        +1.0f, OptimizeBlobbyFMAHelper(nCode, other_children), 0.0f, val));
                }
                }
            }
            else
            {
                if (nCode == RI_BLOBBY_NEG)
                {
                    return std::shared_ptr<ri_blobby>(
                        new ri_fma_blobby(-1.0f, other_children[0], 0.0f));
                }
            }
        }

        return blobby;
    }

    static std::shared_ptr<ri_blobby>
    OptimizeBlobbyFMAFolding(const std::shared_ptr<ri_blobby>& blobby)
    {
        if (blobby->is_branch())
        {
            ri_base_branch_blobby* b =
                dynamic_cast<ri_base_branch_blobby*>(blobby.get());
            const std::vector<std::shared_ptr<ri_blobby> >& cld = b->get_children();
            std::vector<std::shared_ptr<ri_blobby> > cld2;
            for (size_t i = 0; i < cld.size(); i++)
            {
                std::shared_ptr<ri_blobby> c = OptimizeBlobbyFMAFolding(cld[i]);
                if (c.get())
                    cld2.push_back(c);
            }
            if (blobby->typeN() == RI_BLOBBY_FMA)
            {
                ri_fma_blobby* b = dynamic_cast<ri_fma_blobby*>(blobby.get());
                if (cld2[0]->typeN() == RI_BLOBBY_FMA)
                {
                    ri_fma_blobby* cb = dynamic_cast<ri_fma_blobby*>(cld2[0].get());

                    float a = b->get_mul() * cb->get_mul();
                    float c = b->get_add() + b->get_mul() * cb->get_add();
                    float min = std::max<float>(b->get_min(), b->get_min());
                    float max = std::min<float>(b->get_max(), b->get_max());
                    return std::shared_ptr<ri_blobby>(
                        new ri_fma_blobby(a, cb->get_children()[0], c, min, max));
                }
                else
                {
                    if (!IsEqual(cld, cld2))
                    {
                        float a = b->get_mul();
                        float c = b->get_add();
                        float min = b->get_min();
                        float max = b->get_max();
                        return std::shared_ptr<ri_blobby>(
                            new ri_fma_blobby(a, cld2[0], c, min, max));
                    }
                }
            }
        }
        return blobby;
    }

    static std::shared_ptr<ri_blobby>
    OptimizeBlobbyPoints(const std::shared_ptr<ri_blobby>& blobby)
    {
        if (blobby->is_branch())
        {
            ri_base_branch_blobby* b =
                dynamic_cast<ri_base_branch_blobby*>(blobby.get());
            {
                std::vector<std::shared_ptr<ri_blobby> >& cld = b->get_children();
                size_t sz = cld.size();
                if (sz >= 2)
                {
                    bool bSphere = true;
                    for (size_t i = 0; i < sz; i++)
                    {
                        if (cld[i]->typeN() != RI_BLOBBY_SPHERE)
                        {
                            bSphere = false;
                            break;
                        }
                    }
                    if (bSphere)
                    {
                        std::vector<size_t> indices(sz);
                        std::vector<float> P(sz * 3);
                        std::vector<float> R(sz);

                        for (size_t i = 0; i < sz; i++)
                        {
                            const ri_sphere_blobby* s =
                                dynamic_cast<const ri_sphere_blobby*>(cld[i].get());
                            indices[i] = s->get_index();
                            const float* p = s->get_position();

                            P[3 * i + 0] = p[0];
                            P[3 * i + 1] = p[1];
                            P[3 * i + 2] = p[2];

                            R[i] = s->get_radius();
                        }

                        std::vector<std::shared_ptr<ri_blobby> > v;
                        v.push_back(std::shared_ptr<ri_blobby>(new ri_points_blobby()));
                        cld = v;
                    }
                }
            }
            {
                std::vector<std::shared_ptr<ri_blobby> >& cld = b->get_children();
                size_t sz = cld.size();
                for (size_t i = 0; i < sz; i++)
                {
                    cld[i] = OptimizeBlobbyPoints(cld[i]);
                }
            }
            return blobby;
        }
        else
        {
            return blobby;
        }
    }

    static std::shared_ptr<ri_blobby> OptimizeBlobby(const std::shared_ptr<ri_blobby>& blobby)
    {
        std::shared_ptr<ri_blobby> ret = blobby;
        ret = OptimizeBlobbyValueConvert(ret);
        ret = OptimizeBlobbyValueFolding(ret);
        ret = OptimizeBlobbySphere(ret);
        ret = OptimizeBlobbyFMA(ret);
        ret = OptimizeBlobbyFMAFolding(ret);

        // ret = OptimizeBlobbyPoints(ret);
        return ret;
    }

    std::shared_ptr<ri_blobby> ri_optimize_blobby(const std::shared_ptr<ri_blobby>& blobby)
    {
        // ri_print_blobby(blobby);
        std::shared_ptr<ri_blobby> b = OptimizeBlobby(blobby);
        // ri_print_blobby(b);
        return b;
    }

    //-------------------------------------------------------------------------

    static void GetLeafBlobby(std::vector<ri_blobby*>& leafs,
                              const std::shared_ptr<ri_blobby>& blobby)
    {
        if (blobby->is_branch())
        {
            ri_base_branch_blobby* b =
                dynamic_cast<ri_base_branch_blobby*>(blobby.get());
            const std::vector<std::shared_ptr<ri_blobby> >& cld = b->get_children();
            for (size_t i = 0; i < cld.size(); i++)
            {
                GetLeafBlobby(leafs, cld[i]);
            }
        }
        else if (blobby->is_leaf())
        {
            leafs.push_back(blobby.get());
        }
    }

    static bool IsFloat(const ri_classifier::param_data& data)
    {
        int nType = data.get_type();
        switch (nType)
        {
        case ri_classifier::FLOAT:
        case ri_classifier::COLOR:
        case ri_classifier::POINT:
        case ri_classifier::VECTOR:
        case ri_classifier::NORMAL:
        case ri_classifier::HPOINT:
        case ri_classifier::MATRIX:
        case ri_classifier::BASIS:
        case ri_classifier::BOUND:
            return true;
        }
        return false;
    }

    static bool IsVertex(const ri_classifier::param_data& data)
    {
        return data.get_class() == ri_classifier::VERTEX;
    }

    static bool IsVarying(const ri_classifier::param_data& data)
    {
        return data.get_class() == ri_classifier::VARYING;
    }

    static bool IsUniform(const ri_classifier::param_data& data)
    {
        return data.get_class() == ri_classifier::UNIFORM;
    }

    std::shared_ptr<ri_blobby> ri_attributes_blobby(const std::shared_ptr<ri_blobby>& blobby,
                                              const ri_classifier& cls,
                                              const ri_parameters& params)
    {
        std::vector<ri_blobby*> leafs;
        GetLeafBlobby(leafs, blobby);
        for (size_t i = 0; i < leafs.size(); i++)
        {
            for (size_t j = 0; j < cls.size(); j++)
            {
                ri_base_leaf_blobby* b = dynamic_cast<ri_base_leaf_blobby*>(leafs[i]);
                ri_classifier& bcls = b->get_classifier();
                ri_parameters& bparams = b->get_parameters();

                const ri_classifier::param_data& data = cls[j];
                const ri_parameters::value_type* pv = params.get(data.get_name());
                if (IsFloat(data) && pv)
                {
                    if (IsVertex(data) || IsVarying(data))
                    {
                        size_t offset = data.get_size() * b->get_index();
                        size_t count = data.get_size() * b->get_count();

                        if (offset + count <= pv->float_values.size())
                        {
                            bcls.add(data);
                            bparams.set(data.get_name(), &pv->float_values[0] + offset, count);
                        }
                    }
                    else if (IsUniform(data))
                    {
                        bcls.add(data);
                        bparams.set(data.get_name(), &pv->float_values[0], data.get_size());
                    }
                }
            }
        }

        return blobby;
    }
}
