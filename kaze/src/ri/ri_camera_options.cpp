#include "ri_camera_options.h"
#include "ri.h"
#include "ri_display.h"

#include <math.h>
#include <memory>
#include <algorithm>

namespace ri
{

    ri_camera_options::ri_camera_options(const ri_options& opts,
                                         const ri_transform& trns)
        : opts_(opts), trns_(trns) {}

    ri_transform ri_camera_options::get_world_to_camera() const { return trns_; }

    ri_transform ri_camera_options::get_camera_to_world() const
    {
        ri_transform t = trns_;
        t.invert();
        return t;
    }

    bool ri_camera_options::is_motion() const { return trns_.is_motion(); }
    size_t ri_camera_options::get_motion_size() const { return trns_.get_size(); }
    float ri_camera_options::get_time_at(size_t i) const
    {
        return trns_.get_time_at(i);
    }
    ri_transform ri_camera_options::get_world_to_camera_at(size_t i) const
    {
        return ri_transform(trns_.get_mat_at(i));
    }
    ri_transform ri_camera_options::get_camera_to_world_at(size_t i) const
    {
        ri_transform t(trns_.get_mat_at(i));
        t.invert();
        return t;
    }

    std::string ri_camera_options::get_display_name() const
    {
        size_t sz = opts_.get_display_size();
        if (sz)
        {
            for (size_t i = 0; i < sz; i++)
            {
                const ri_display* disp = opts_.get_display_at(i);
                //std::string type = disp->get_type();
                return disp->get_name();
            }
        }
        else
        {
            std::string name;
            if (opts_.get("Display", "name", name))
            {
                return name;
            }
        }
        return "";
    }

    int ri_camera_options::get_camera_type() const
    {
        std::string name;
        if (opts_.get("Projection", "name", name))
        {
            if (name == RI_ORTHOGRAPHIC)
            {
                return RI_CAMERA_ORTHOGRAPHIC;
            }
            else if (name == RI_PERSPECTIVE)
            {
                return RI_CAMERA_PERSPECTIVE;
            }
        }
        return RI_CAMERA_ORTHOGRAPHIC;
    }

    float ri_camera_options::get_fov() const
    {
        float fov = 0;
        if (opts_.get("Projection", "fov", fov))
        {
            return fov;
        }
        return fov;
    }

    float ri_camera_options::get_frame_aspect_ratio() const
    {
        float aspect = 1.0;
        if (opts_.get("FrameAspectRatio", "aspect", aspect))
        {
            return aspect;
        }
        else
        {
            int xres = 0;
            int yres = 0;
            float paspect = 1.0f;
            this->get_format(xres, yres, paspect);
            aspect = paspect * float(xres) / float(yres);
            return fabs(aspect);
        }
    }

    void ri_camera_options::get_format(int& xres, int& yres, float& aspect) const
    {
        opts_.get("Format", "xres", xres);
        opts_.get("Format", "yres", yres);
        opts_.get("Format", "aspect", aspect);
    }

    bool ri_camera_options::get_screen_window_raw(float& left, float& right,
                                                  float& bottom, float& top) const
    {
        bool bDefined = true;
        if (!opts_.get("ScreenWindow", "left", left))
            bDefined = false;
        if (!opts_.get("ScreenWindow", "right", right))
            bDefined = false;
        if (!opts_.get("ScreenWindow", "bottom", bottom))
            bDefined = false;
        if (!opts_.get("ScreenWindow", "top", top))
            bDefined = false;
        return bDefined;
    }

    void ri_camera_options::get_screen_window(float& left, float& right,
                                              float& bottom, float& top) const
    {
        if (!get_screen_window_raw(left, right, bottom, top))
        {
            float aspect = get_frame_aspect_ratio();
            left = -aspect;
            right = +aspect;
            bottom = -1.0f;
            top = +1.0f;
        }
    }

    void ri_camera_options::get_fullscreen_window(float& left, float& right,
                                                  float& bottom, float& top) const
    {
        // if(!get_screen_window_raw(left, right, bottom, top)){
        float aspect = get_frame_aspect_ratio();
        left = -aspect;
        right = +aspect;
        bottom = -1.0f;
        top = +1.0f;
        //}
    }

    void ri_camera_options::get_crop_window(float& xmin, float& xmax, float& ymin,
                                            float& ymax) const
    {
        opts_.get("CropWindow", "left", xmin);
        opts_.get("CropWindow", "right", xmax);
        opts_.get("CropWindow", "bottom", ymin);
        opts_.get("CropWindow", "top", ymax);
    }

    void ri_camera_options::get_clipping(float& near, float& far) const
    {
        opts_.get("Clipping", "near", near);
        opts_.get("Clipping", "far", far);
    }
    void ri_camera_options::get_pixel_samples(int& xsamples, int& ysamples) const
    {
        float fxsamples = 1;
        float fysamples = 1;
        opts_.get("PixelSamples", "xsamples", fxsamples);
        opts_.get("PixelSamples", "ysamples", fysamples);
        xsamples = std::max<int>(1, (int)fxsamples);
        ysamples = std::max<int>(1, (int)fysamples);
    }

    bool ri_camera_options::get_depth_of_field(float& fstop, float& focallength,
                                               float& focaldistance) const
    {
        bool bDefined = true;
        if (!opts_.get("DepthOfField", "fstop", fstop))
            bDefined = false;
        if (!opts_.get("DepthOfField", "focallength", focallength))
            bDefined = false;
        if (!opts_.get("DepthOfField", "focaldistance", focaldistance))
            bDefined = false;
        return bDefined;
    }
}
