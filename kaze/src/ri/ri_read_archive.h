#ifndef RI_READ_ARCHIVE_H
#define RI_READ_ARCHIVE_H

#include <stdio.h>
#include <string>

namespace ri
{
    class ri_context;
    int ri_read_archive(FILE* in, ri_context* ctx);
    int ri_read_archive(const char* szFile, ri_context* ctx);
    std::string ri_read_archive_set_path(const char* path);
    std::string ri_read_archive_get_filename(const char* path);
}

#endif
