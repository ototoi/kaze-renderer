#ifndef RI_SEQUENCE_TASK_H
#define RI_SEQUENCE_TASK_H

#include "ri_task.h"
#include <vector>

namespace ri
{
    class ri_sequence_task : public ri_task
    {
    public:
        virtual std::string type() const { return "Sequence"; }
        virtual int run();

    public:
        virtual void add_task(const std::shared_ptr<ri_task>& task);

    protected:
        std::vector<std::shared_ptr<ri_task> > tasks_;
    };
}

#endif
