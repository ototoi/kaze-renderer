#include "ri_run_program.h"
#include "ri_read_archive.h"
#include "ri_context.h"
#include "ri_logger.h"

#include "ri_file_util.h"

#include <string.h>
//#include <fstream>
#include <sstream>

#ifdef _WIN32
#include "shlwapi.h"
#pragma comment(lib, "shlwapi.lib")
#else
#include <stdlib.h>
#include <errno.h>
#endif

namespace ri
{
    static std::string GetTmpPath()
    {
        char buffer[512];
        get_temppath_nodelete(buffer, 512, "RAT"); //
        //_snprintf(buffer,512, "E:/kaze/kaze/src/ri/RIBs/Procedurals/RAT_%d.txt",
        // rand());
        return buffer;
    }

    static std::string CreateArgText(const char* arg)
    {
        std::string path = GetTmpPath();
        FILE* fp = fopen(path.c_str(), "wt");
        if (!fp)
            return "";
        fprintf(fp, "%s", arg);
        fclose(fp);
        /*
          std::ofstream ofs(path.c_str());
          if(!ofs)return path;
          ofs << arg;
          ofs.flush();
          */
        return path;
    }

    static bool CheckOutputFile(const char* file)
    {
        FILE* fp = fopen(file, "rt");
        if (!fp)
            return false;
        char buffer[512] = {};
        while (fgets(buffer, 512, fp))
        {
            if (strlen(buffer) != 0)
            {
                fclose(fp);
                return true;
            }
        }
        fclose(fp);
        /*
          std::ifstream ifs(file);
          if(!ifs)return false;

          std::string s;
          while(std::getline(ifs, s)){
              if(!s.empty())return true;
          }
          */
        return false;
    }

#ifdef _WIN32
    static std::string GetExecutable(const char* pszExtension)
    {
        DWORD dwOut;
        HRESULT hr;
        char pszFile[MAX_PATH + 10];
        std::string strExt;

        std::string strCommand = "";
        if (pszExtension == NULL)
            return false;

        strExt = pszExtension;
        /*
          if(strExt.Left(1) != _T("."))
              strExt = _T(".") + strExt;
          if(strExt.GetLength() == 1)
              return  "";
          */

        dwOut = MAX_PATH + 10;
        hr = ::AssocQueryStringA(ASSOCF_NOTRUNCATE, ASSOCSTR_EXECUTABLE,
                                 strExt.c_str(), "open", pszFile, &dwOut);
        if (FAILED(hr) || dwOut == 0)
            return "";

        return pszFile;
    }

    static void SplitPath(std::vector<std::string>& pathes,
                          const std::string& path)
    {
        std::stringstream ss(path);
        while (1)
        {
            std::string s;
            ss >> s;
            if (s.empty())
                break;
            if (ss.bad())
                break;
            pathes.push_back(s);
        }
    }

    static std::string JoinPath(const std::vector<std::string>& pathes)
    {
        std::string sRet;
        size_t sz = pathes.size();
        for (size_t i = 0; i < sz; i++)
        {
            sRet += pathes[i];
            if (i != sz - 1)
                sRet += " ";
        }
        return sRet;
    }

    static std::string GetExecutablePath(const char* program)
    {
        std::vector<std::string> pathes;
        SplitPath(pathes, program);

        int nFind = -1;
        std::string exe = "";
        size_t sz = pathes.size();
        for (size_t i = 1; i < sz; i++)
        {
            char ext[_MAX_EXT] = {0};
            _splitpath(pathes[i].c_str(), NULL, NULL, NULL, ext);
            if (strlen(ext) >= 2)
            {
                exe = "\"" + GetExecutable(ext) + "\"";
                nFind = (int)i;
            }
        }

        if (nFind >= 0)
        {
            if (nFind >= 1)
            {
                pathes[0] = exe;
            }
        }

        return JoinPath(pathes);
    }

    static int ExecuteProcess(const char* path, const char* input,
                              const char* output)
    {
        PROCESS_INFORMATION pi;
        STARTUPINFOA si;

        ZeroMemory(&si, sizeof(si));
        si.cb = sizeof(si);

        HANDLE hFileIn = NULL;
        HANDLE hStdInput = NULL;
        ;

        if (input)
        {
            hFileIn =
                CreateFileA((char*)input, GENERIC_READ, 0, NULL, OPEN_ALWAYS,
                            FILE_ATTRIBUTE_READONLY | FILE_ATTRIBUTE_TEMPORARY, NULL);
            DuplicateHandle(GetCurrentProcess(), hFileIn, GetCurrentProcess(),
                            &hStdInput, 0, TRUE, DUPLICATE_SAME_ACCESS);
        }

        HANDLE hFileOut = NULL;
        HANDLE hStdOutput = NULL;

        if (output)
        {
            hFileOut = CreateFileA((char*)output, GENERIC_WRITE, 0, NULL,
                                   CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY, NULL);
            DuplicateHandle(GetCurrentProcess(), hFileOut, GetCurrentProcess(),
                            &hStdOutput, 0, TRUE, DUPLICATE_SAME_ACCESS);
        }

        si.dwFlags = STARTF_USESTDHANDLES;
        si.hStdInput = hStdInput;
        si.hStdOutput = hStdOutput;

#if 0
            int nWINDOW = CREATE_NEW_CONSOLE;
#else
        int nWINDOW = CREATE_NO_WINDOW;
#endif

        BOOL bRet = ::CreateProcessA(NULL, (char*)path, NULL, NULL, TRUE, nWINDOW,
                                     NULL, NULL, &si, &pi);

        if (bRet)
        {
            ::CloseHandle(pi.hThread);
            ::WaitForSingleObject(pi.hProcess, INFINITE);
            ::CloseHandle(pi.hProcess);
        }

        if (hFileIn)
        {
            ::CloseHandle(hFileIn);
            ::CloseHandle(hStdInput);
        }
        if (hFileOut)
        {
            ::CloseHandle(hFileOut);
            ::CloseHandle(hStdOutput);
        }

        if (!bRet)
            return -1;

        return 0;
    }

    static int RunProgram1st(const char* program, const char* infile,
                             const char* outfile)
    {
        int nRet = ExecuteProcess(program, infile, outfile);
        return nRet;
    }

    static int RunProgram2nd(const char* program, const char* infile,
                             const char* outfile)
    {
        std::string executable = GetExecutablePath(program);
        int nRet = ExecuteProcess(executable.c_str(), infile, outfile);
        return nRet;
    }

    static int RunProgram(ri_context* ctx, const char* program, const char* arg)
    {
        int nRet = 0;

        std::string infile = CreateArgText(arg);
        std::string outfile = GetTmpPath();
        if ((nRet = RunProgram1st(program, infile.c_str(), outfile.c_str())) != 0)
        {
            delete_file(outfile.c_str());
            // delete_file(infile.c_str());
            outfile = GetTmpPath();
            // infile  = CreateArgText(arg);
            nRet = RunProgram2nd(program, infile.c_str(), outfile.c_str());
        }

        if (nRet == 0 && !CheckOutputFile(outfile.c_str()))
        {
            nRet = -1;
        }

        if (nRet == 0)
        {
            nRet = ri_read_archive(outfile.c_str(), ctx);
        }

        delete_file(infile.c_str());
        delete_file(outfile.c_str());
        return nRet;
    }
#else
    static int ExecuteProcess(const char* path, const char* input,
                              const char* output)
    {
        std::string cmdLine;
        cmdLine += path;
        cmdLine += " < ";
        cmdLine += "\"";
        cmdLine += input;
        cmdLine += "\"";
        cmdLine += " > ";
        cmdLine += "\"";
        cmdLine += output;
        cmdLine += "\"";

        // printf("%s\n", cmdLine.c_str());
        int nRet = ::system(cmdLine.c_str());
        if (nRet == 127 || nRet == -1)
        {
            print_log("%s¥n", strerror(errno));
            return -1;
        }
        return nRet;
    }

    static FILE* ExecuteProcess(const char* path, const char* input)
    {
        std::string cmdLine;
        cmdLine += path;
        cmdLine += " < ";
        cmdLine += "\"";
        cmdLine += input;
        cmdLine += "\"";
        // cmdLine += " > ";
        // cmdLine += "\"";
        // cmdLine += output;
        // cmdLine += "\"";
        FILE* fp = popen(cmdLine.c_str(), "r");
        if (fp)
            fflush(fp);
        return fp;
    }

    static FILE* RunProgram(const char* program, const char* infile)
    {
        return ExecuteProcess(program, infile);
    }

    static void CloseProgram(FILE* fp) { pclose(fp); }

    static int RunProgram(const char* program, const char* infile, const char* outfile)
    {
        return ExecuteProcess(program, infile, outfile);
    }

    bool CheckOutputFile(FILE* fp)
    {
        if (feof(fp))
            return false;
        fseek(fp, 0L, SEEK_END);
        int sz = ftell(fp);
        fseek(fp, 0L, SEEK_SET);
        return (sz != 0);
        /*
          char buffer[255] = {};
          fgets(buffer,255,fp);
          std::string s = buffer;
          bool bRet = true;
          if(s.empty())bRet = false;
          fseek(fp,0L,SEEK_SET);
          return bRet;
          */
    }

    static int RunProgram(ri_context* ctx, const char* program, const char* arg)
    {
        int nRet = 0;

        std::string infile = CreateArgText(arg);
#if 1
        std::string outfile = GetTmpPath();
        nRet = RunProgram(program, infile.c_str(), outfile.c_str());

        if (nRet == 0 && !CheckOutputFile(outfile.c_str()))
        {
            nRet = -1;
        }

        if (nRet == 0)
        {
            nRet = ri_read_archive(outfile.c_str(), ctx);
        }
        delete_file(infile.c_str());
        delete_file(outfile.c_str());
#else
        FILE* fp = RunProgram(program, infile.c_str());

        if (fp == NULL)
        {
            nRet = -1;
        }

        if (!CheckOutputFile(fp))
        {
            CloseProgram(fp);
            nRet = -1;
        }

        if (nRet == 0)
        {
            nRet = ri_read_archive(fp, ctx);
        }

        if (fp)
        {
            CloseProgram(fp);
        }

        delete_file(infile.c_str());
#endif

        return nRet;
    }
#endif


    static std::string ToString(float f)
    {
        char buffer[64];
        sprintf(buffer, "%.6f", f);
        return buffer;
    }

    int ri_run_program(ri_context* ctx, const char* program, float area, const char* arg)
    {
        std::string s = ToString(area) + " " + arg;
        return RunProgram(ctx, program, s.c_str());
    }
}
