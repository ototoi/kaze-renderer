#include "ri_transform.h"
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <vector>

#define _USE_MATH_DEFINES
#include <math.h>

#define PI M_PI

#define MAT(m, i, j) (m[4 * i + j])

namespace ri
{
    static const float IDENTITY[] = {1, 0, 0, 0, 0, 1, 0, 0,
                                     0, 0, 1, 0, 0, 0, 0, 1};

    static void mul_matrix(float* out, const float* lhs, const float* rhs)
    {
        out[0] =
            lhs[0] * rhs[0] + lhs[1] * rhs[4] + lhs[2] * rhs[8] + lhs[3] * rhs[12];
        out[1] =
            lhs[0] * rhs[1] + lhs[1] * rhs[5] + lhs[2] * rhs[9] + lhs[3] * rhs[13];
        out[2] =
            lhs[0] * rhs[2] + lhs[1] * rhs[6] + lhs[2] * rhs[10] + lhs[3] * rhs[14];
        out[3] =
            lhs[0] * rhs[3] + lhs[1] * rhs[7] + lhs[2] * rhs[11] + lhs[3] * rhs[15];

        out[4] =
            lhs[4] * rhs[0] + lhs[5] * rhs[4] + lhs[6] * rhs[8] + lhs[7] * rhs[12];
        out[5] =
            lhs[4] * rhs[1] + lhs[5] * rhs[5] + lhs[6] * rhs[9] + lhs[7] * rhs[13];
        out[6] =
            lhs[4] * rhs[2] + lhs[5] * rhs[6] + lhs[6] * rhs[10] + lhs[7] * rhs[14];
        out[7] =
            lhs[4] * rhs[3] + lhs[5] * rhs[7] + lhs[6] * rhs[11] + lhs[7] * rhs[15];

        out[8] =
            lhs[8] * rhs[0] + lhs[9] * rhs[4] + lhs[10] * rhs[8] + lhs[11] * rhs[12];
        out[9] =
            lhs[8] * rhs[1] + lhs[9] * rhs[5] + lhs[10] * rhs[9] + lhs[11] * rhs[13];
        out[10] =
            lhs[8] * rhs[2] + lhs[9] * rhs[6] + lhs[10] * rhs[10] + lhs[11] * rhs[14];
        out[11] =
            lhs[8] * rhs[3] + lhs[9] * rhs[7] + lhs[10] * rhs[11] + lhs[11] * rhs[15];

        out[12] = lhs[12] * rhs[0] + lhs[13] * rhs[4] + lhs[14] * rhs[8] +
                  lhs[15] * rhs[12];
        out[13] = lhs[12] * rhs[1] + lhs[13] * rhs[5] + lhs[14] * rhs[9] +
                  lhs[15] * rhs[13];
        out[14] = lhs[12] * rhs[2] + lhs[13] * rhs[6] + lhs[14] * rhs[10] +
                  lhs[15] * rhs[14];
        out[15] = lhs[12] * rhs[3] + lhs[13] * rhs[7] + lhs[14] * rhs[11] +
                  lhs[15] * rhs[15];
    }

    static bool normalize(float& x, float& y, float& z)
    {
        float l = x * x + y * y + z * z;
        if (l)
        {
            l = 1.0f / sqrt(l);
            x *= l;
            y *= l;
            z *= l;

            return true;
        }
        else
        {
            return false;
        }
    }

    static void cross(float& x, float& y, float& z, float x0, float y0, float z0,
                      float x1, float y1, float z1)
    {
        x = y0 * z1 - z0 * y1; // xyzzy
        y = z0 * x1 - x0 * z1; // yzxxz
        z = x0 * y1 - y0 * x1; // zxyyx
    }

    static void cross_r(float& x, float& y, float& z, float x0, float y0, float z0,
                        float x1, float y1, float z1)
    {
        cross(x, y, z, x1, y1, z1, x0, y0, z0);
    }

    static float radians(float x) { return (float)PI / 180.0f * x; }

    inline float det33(float m00, float m01, float m02, float m10, float m11,
                       float m12, float m20, float m21, float m22)
    {
        return (m00 * m11 * m22 - m11 * m20 * m02 + m01 * m12 * m20 -
                m10 * m22 * m01 + m02 * m10 * m21 - m12 * m21 * m00);
    }

    /*********************************************************************/
    class ri_transform_base
    {
    public:
        virtual ~ri_transform_base() {}

    public:
        virtual void set(const float mat[4 * 4]) = 0;
        virtual void concat(const float mat[4 * 4]) = 0;
        virtual const float* get() const = 0;
        virtual void identity() = 0;
        virtual void transpose() = 0;
        virtual bool invert() = 0;
        virtual bool is_motion() const = 0;

    public:
        virtual void translate(float x, float y, float z) = 0;
        virtual void scale(float x, float y, float z) = 0;
        virtual void rotate(float angle, float dx, float dy, float dz) = 0;
        virtual void skew(float angle, float dx1, float dy1, float dz1, float dx2,
                          float dy2, float dz2) = 0;
        virtual void perspective(float fov) = 0;

    public:
        virtual void print_() = 0; // for debug
        virtual float det() const = 0;

    public:
        virtual ri_transform_base* clone() = 0;
    };

    class ri_transform_matrix : public ri_transform_base
    {
    public:
        ri_transform_matrix();
        explicit ri_transform_matrix(const float mat[4 * 4]);
        ri_transform_matrix(const ri_transform_matrix& rhs);
        ~ri_transform_matrix();

    public:
        void set(const float mat[4 * 4]);
        void concat(const float mat[4 * 4]);
        const float* get() const;
        void identity();
        void transpose();
        bool invert();
        bool is_motion() const;
        bool is_invert() const;

    public:
        void translate(float x, float y, float z);
        void scale(float x, float y, float z);
        void rotate(float angle, float dx, float dy, float dz);
        void skew(float angle, float dx1, float dy1, float dz1, float dx2, float dy2,
                  float dz2);
        void perspective(float fov);

    public:
        void print_(); // for debug
        float det() const;

    public:
        ri_transform_base* clone();

    private:
        float mat_[4 * 4];
    };

    /*********************************************************************/

    ri_transform_matrix::ri_transform_matrix() { identity(); }
    ri_transform_matrix::ri_transform_matrix(const float mat[4 * 4])
    {
        memcpy(mat_, mat, sizeof(float) * 16);
    }
    ri_transform_matrix::ri_transform_matrix(const ri_transform_matrix& rhs)
    {
        memcpy(mat_, rhs.mat_, sizeof(float) * 16);
    }
    ri_transform_matrix::~ri_transform_matrix() { ; }

    void ri_transform_matrix::set(const float mat[4 * 4])
    {
        ri_transform_matrix tt(mat);
        if (tt.is_invert())
        {
            memcpy(mat_, mat, sizeof(float) * 16);
        }
    }
    void ri_transform_matrix::concat(const float mat[4 * 4])
    {
        ri_transform_matrix tt(mat);
        if (tt.is_invert())
        {
            float tmp[16];
            mul_matrix(tmp, mat, mat_);
            memcpy(mat_, tmp, sizeof(float) * 16);
        }
    }
    const float* ri_transform_matrix::get() const { return mat_; }

    void ri_transform_matrix::identity()
    {
        memcpy(mat_, IDENTITY, sizeof(float) * 16);
    }

    void ri_transform_matrix::transpose()
    {
        float m00 = MAT(mat_, 0, 0);
        float m01 = MAT(mat_, 0, 1);
        float m02 = MAT(mat_, 0, 2);
        float m03 = MAT(mat_, 0, 3);

        float m10 = MAT(mat_, 1, 0);
        float m11 = MAT(mat_, 1, 1);
        float m12 = MAT(mat_, 1, 2);
        float m13 = MAT(mat_, 1, 3);

        float m20 = MAT(mat_, 2, 0);
        float m21 = MAT(mat_, 2, 1);
        float m22 = MAT(mat_, 2, 2);
        float m23 = MAT(mat_, 2, 3);

        float m30 = MAT(mat_, 3, 0);
        float m31 = MAT(mat_, 3, 1);
        float m32 = MAT(mat_, 3, 2);
        float m33 = MAT(mat_, 3, 3);

        float tmp[] = {m00, m10, m20, m30, m01, m11, m21, m31,
                       m02, m12, m22, m32, m03, m13, m23, m33};
        this->set(tmp);
    }

    float ri_transform_matrix::det() const
    {
        float m00 = MAT(mat_, 0, 0);
        float m01 = MAT(mat_, 0, 1);
        float m02 = MAT(mat_, 0, 2);
        float m03 = MAT(mat_, 0, 3);

        float m10 = MAT(mat_, 1, 0);
        float m11 = MAT(mat_, 1, 1);
        float m12 = MAT(mat_, 1, 2);
        float m13 = MAT(mat_, 1, 3);

        float m20 = MAT(mat_, 2, 0);
        float m21 = MAT(mat_, 2, 1);
        float m22 = MAT(mat_, 2, 2);
        float m23 = MAT(mat_, 2, 3);

        float m30 = MAT(mat_, 3, 0);
        float m31 = MAT(mat_, 3, 1);
        float m32 = MAT(mat_, 3, 2);
        float m33 = MAT(mat_, 3, 3);

        return (+m00 * det33(m11, m12, m13, m21, m22, m23, m31, m32, m33) -
                m10 * det33(m01, m02, m03, m21, m22, m23, m31, m32, m33) +
                m20 * det33(m01, m02, m03, m11, m12, m13, m31, m32, m33) -
                m30 * det33(m01, m02, m03, m11, m12, m13, m21, m22, m23));
    }

    bool ri_transform_matrix::is_invert() const
    {
        float det = this->det();
        if (!det)
            return false;
        else
            return true;
    }

    bool ri_transform_matrix::invert()
    {
        float det = this->det();
        if (!det)
            return false;
        det = 1.0f / det;

        float m00 = MAT(mat_, 0, 0);
        float m01 = MAT(mat_, 0, 1);
        float m02 = MAT(mat_, 0, 2);
        float m03 = MAT(mat_, 0, 3);

        float m10 = MAT(mat_, 1, 0);
        float m11 = MAT(mat_, 1, 1);
        float m12 = MAT(mat_, 1, 2);
        float m13 = MAT(mat_, 1, 3);

        float m20 = MAT(mat_, 2, 0);
        float m21 = MAT(mat_, 2, 1);
        float m22 = MAT(mat_, 2, 2);
        float m23 = MAT(mat_, 2, 3);

        float m30 = MAT(mat_, 3, 0);
        float m31 = MAT(mat_, 3, 1);
        float m32 = MAT(mat_, 3, 2);
        float m33 = MAT(mat_, 3, 3);

        float tmp[16] = {+det33(m11, m12, m13, m21, m22, m23, m31, m32, m33) * det,
                         -det33(m01, m02, m03, m21, m22, m23, m31, m32, m33) * det,
                         +det33(m01, m02, m03, m11, m12, m13, m31, m32, m33) * det,
                         -det33(m01, m02, m03, m11, m12, m13, m21, m22, m23) * det,

                         -det33(m10, m12, m13, m20, m22, m23, m30, m32, m33) * det,
                         +det33(m00, m02, m03, m20, m22, m23, m30, m32, m33) * det,
                         -det33(m00, m02, m03, m10, m12, m13, m30, m32, m33) * det,
                         +det33(m00, m02, m03, m10, m12, m13, m20, m22, m23) * det,

                         +det33(m10, m11, m13, m20, m21, m23, m30, m31, m33) * det,
                         -det33(m00, m01, m03, m20, m21, m23, m30, m31, m33) * det,
                         +det33(m00, m01, m03, m10, m11, m13, m30, m31, m33) * det,
                         -det33(m00, m01, m03, m10, m11, m13, m20, m21, m23) * det,

                         -det33(m10, m11, m12, m20, m21, m22, m30, m31, m32) * det,
                         +det33(m00, m01, m02, m20, m21, m22, m30, m31, m32) * det,
                         -det33(m00, m01, m02, m10, m11, m12, m30, m31, m32) * det,
                         +det33(m00, m01, m02, m10, m11, m12, m20, m21, m22) * det};

        this->set(tmp);

        return true;
    }

    bool ri_transform_matrix::is_motion() const { return false; }

    void ri_transform_matrix::translate(float x, float y, float z)
    {
        float tmp[16] = {
            1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, x, y, z, 1,
        };
        concat(tmp);
    }
    void ri_transform_matrix::scale(float x, float y, float z)
    {
        float tmp[16] = {
            x, 0, 0, 0, 0, y, 0, 0, 0, 0, z, 0, 0, 0, 0, 1,
        };
        concat(tmp);
    }

    //
    //
    // left hand...
    void ri_transform_matrix::rotate(float angle, float dx, float dy, float dz)
    {
        if (!normalize(dx, dy, dz))
            return;
        float rad = radians(angle);
        float s = sin(rad);
        float c = cos(rad);
        float t = 1.0f - c;

        float x = dx;
        float y = dy;
        float z = dz;

        float m00 = t * x * x + c;
        float m11 = t * y * y + c;
        float m22 = t * z * z + c;
        float m33 = 1.0f;

        float txy = t * x * y;
        float sz = s * z;

        float txz = t * x * z;
        float sy = s * y;

        float tyz = t * y * z;
        float sx = s * x;

        float tmp[16] = {0};

        MAT(tmp, 0, 0) = m00; // 00
        MAT(tmp, 1, 1) = m11; // 11
        MAT(tmp, 2, 2) = m22; // 22
        MAT(tmp, 3, 3) = m33; // 33

        MAT(tmp, 0, 1) = txy + sz; // 01
        MAT(tmp, 1, 0) = txy - sz; // 10

        MAT(tmp, 0, 2) = txz - sy; // 02
        MAT(tmp, 2, 0) = txz + sy; // 20

        MAT(tmp, 1, 2) = tyz + sx; // 12
        MAT(tmp, 2, 1) = tyz - sx; // 21

        concat(tmp);
    }

    void ri_transform_matrix::skew(float angle, float dx1, float dy1, float dz1,
                                   float dx2, float dy2, float dz2)
    {
        float rad = radians(angle);

        if (!normalize(dx1, dy1, dz1))
            return;
        if (!normalize(dx2, dy2, dz2))
            return;

        float d1d2 = dx1 * dx2 + dy1 * dy2 + dz1 * dz2;
        float ax = (float)acos(d1d2);
        if (ax <= rad || rad <= (ax - PI))
        {
            identity();
        }
        else
        {
            float rx[3]; // X
            cross(rx[0], rx[1], rx[2], dx1, dy1, dz1, dx2, dy2, dz2);
            if (!normalize(rx[0], rx[1], rx[2]))
                return;

            float ox[3]; // Y
            cross(ox[0], ox[1], ox[2], dx2, dy2, dz2, rx[0], rx[1], rx[2]);
            float dx[3] = {dx2, dy2, dz2};

            float par = d1d2;                          // cos
            float perp = (float)sqrt(1.0 - par * par); // sin
            float s = (float)(tan(rad + acos(perp)) * perp - par);

            float rot[16] = {rx[0], ox[0], dx[0], 0, rx[1], ox[1], dx[1], 0,
                             rx[2], ox[2], dx[2], 0, 0, 0, 0, 1};

            float trot[16] = {rx[0], rx[1], rx[2], 0, ox[0], ox[1], ox[2], 0,
                              dx[0], dx[1], dx[2], 0, 0, 0, 0, 1};

            float skw[16] = {1, 0, 0, 0, 0, 1, s, 0, 0, 0, 1, 0, 0, 0, 0, 1};

            // v' = v * Rot * Skw * tRot;
            float tmp1[16];
            float tmp2[16];
            mul_matrix(tmp1, rot, skw);
            mul_matrix(tmp2, tmp1, trot);

            concat(tmp2);
        }
    }
    void ri_transform_matrix::perspective(float fov)
    { /**/
    }

    void ri_transform_matrix::print_()
    {
        printf("[\n");
        printf("  %5.5f, %5.5f, %5.5f, %5.5f\n", mat_[0], mat_[1], mat_[2], mat_[3]);
        printf("  %5.5f, %5.5f, %5.5f, %5.5f\n", mat_[4], mat_[5], mat_[6], mat_[7]);
        printf("  %5.5f, %5.5f, %5.5f, %5.5f\n", mat_[8], mat_[9], mat_[10],
               mat_[11]);
        printf("  %5.5f, %5.5f, %5.5f, %5.5f\n", mat_[12], mat_[13], mat_[14],
               mat_[15]);
        printf("]\n");
    }

    ri_transform_base* ri_transform_matrix::clone()
    {
        return new ri_transform_matrix(*this);
    }

    //-----------------------------------------------------------------------------------
    class ri_transform_motion : public ri_transform_base
    {
    public:
        struct sequence_t
        {
            float t;
            ri_transform_matrix mat;
        };

    public:
        ri_transform_motion();
        ri_transform_motion(const ri_transform_motion& rhs);
        ri_transform_motion(int n, const float times[], const float* mats[]);
        ~ri_transform_motion();

    public:
        void set(const float mat[4 * 4]);
        void concat(const float mat[4 * 4]);
        const float* get() const;
        void identity();
        void transpose();
        bool invert();
        bool is_motion() const;

    public:
        void translate(float x, float y, float z);
        void scale(float x, float y, float z);
        void rotate(float angle, float dx, float dy, float dz);
        void skew(float angle, float dx1, float dy1, float dz1, float dx2, float dy2,
                  float dz2);
        void perspective(float fov);

    public:
        void print_(); // for debug
        float det() const;

    public:
        ri_transform_base* clone();
        const float* get_mat_at(size_t i) const;
        float get_time_at(size_t i) const;
        size_t get_size() const;

    private:
        std::vector<sequence_t> seq_;
    };

    ri_transform_motion::ri_transform_motion()
    {
        sequence_t s0, s1;
        s0.t = 0;
        s0.mat.identity();
        s1.t = 1;
        s1.mat.identity();
        seq_.push_back(s0);
        seq_.push_back(s1);
    }

    ri_transform_motion::ri_transform_motion(const ri_transform_motion& rhs)
        : seq_(rhs.seq_)
    {
        ;
    }

    ri_transform_motion::ri_transform_motion(int n, const float times[],
                                             const float* mats[])
    {
        for (int i = 0; i < n; i++)
        {
            sequence_t s;
            s.t = times[i];
            s.mat.set(mats[i]);
            seq_.push_back(s);
        }
    }

    ri_transform_motion::~ri_transform_motion() { ; }

    void ri_transform_motion::set(const float mat[4 * 4])
    {
        size_t sz = seq_.size();
        for (size_t i = 0; i < sz; i++)
        {
            seq_[i].mat.set(mat);
        }
    }

    void ri_transform_motion::concat(const float mat[4 * 4])
    {
        size_t sz = seq_.size();
        for (size_t i = 0; i < sz; i++)
        {
            seq_[i].mat.concat(mat);
        }
    }

    const float* ri_transform_motion::get() const { return seq_[0].mat.get(); }

    void ri_transform_motion::identity()
    {
        size_t sz = seq_.size();
        for (size_t i = 0; i < sz; i++)
        {
            seq_[i].mat.identity();
        }
    }

    void ri_transform_motion::transpose()
    {
        size_t sz = seq_.size();
        for (size_t i = 0; i < sz; i++)
        {
            seq_[i].mat.transpose();
        }
    }

    bool ri_transform_motion::invert()
    {
        bool bRet = true;
        size_t sz = seq_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (!seq_[i].mat.invert())
            {
                bRet = false;
            }
        }
        return bRet;
    }

    bool ri_transform_motion::is_motion() const { return true; }

    void ri_transform_motion::translate(float x, float y, float z)
    {
        size_t sz = seq_.size();
        for (size_t i = 0; i < sz; i++)
        {
            seq_[i].mat.translate(x, y, z);
        }
    }

    void ri_transform_motion::scale(float x, float y, float z)
    {
        size_t sz = seq_.size();
        for (size_t i = 0; i < sz; i++)
        {
            seq_[i].mat.scale(x, y, z);
        }
    }

    void ri_transform_motion::rotate(float angle, float dx, float dy, float dz)
    {
        size_t sz = seq_.size();
        for (size_t i = 0; i < sz; i++)
        {
            seq_[i].mat.rotate(angle, dx, dy, dz);
        }
    }

    void ri_transform_motion::skew(float angle, float dx1, float dy1, float dz1,
                                   float dx2, float dy2, float dz2)
    {
        size_t sz = seq_.size();
        for (size_t i = 0; i < sz; i++)
        {
            seq_[i].mat.skew(angle, dx1, dy1, dz1, dx2, dy2, dz2);
        }
    }

    void ri_transform_motion::perspective(float fov)
    {
        size_t sz = seq_.size();
        for (size_t i = 0; i < sz; i++)
        {
            seq_[i].mat.perspective(fov);
        }
    }

    void ri_transform_motion::print_() { seq_[0].mat.print_(); }

    float ri_transform_motion::det() const { return seq_[0].mat.det(); }

    ri_transform_base* ri_transform_motion::clone()
    {
        return new ri_transform_motion(*this);
    }

    const float* ri_transform_motion::get_mat_at(size_t i) const
    {
        return seq_[i].mat.get();
    }

    float ri_transform_motion::get_time_at(size_t i) const { return seq_[i].t; }

    size_t ri_transform_motion::get_size() const { return (int)seq_.size(); }

    //-----------------------------------------------------------------------------------
    ri_transform::ri_transform() { base_ = new ri_transform_matrix(); }
    ri_transform::ri_transform(const float mat[4 * 4])
    {
        base_ = new ri_transform_matrix();
        base_->set(mat);
    }
    ri_transform::ri_transform(const ri_transform& rhs)
    {
        base_ = rhs.base_->clone();
    }
    ri_transform::ri_transform(int n, const float times[], const float* mats[])
    {
        base_ = new ri_transform_motion(n, times, mats);
    }
    ri_transform::ri_transform(int n, const float times[],
                               const ri_transform mats[])
    {
        std::vector<const float*> fmats(n);
        for (int i = 0; i < n; i++)
            fmats[i] = mats[i].get();
        base_ = new ri_transform_motion(n, times, &fmats[0]);
    }
    ri_transform::~ri_transform() { delete base_; }
    ri_transform& ri_transform::operator=(const ri_transform& rhs)
    {
        if(&rhs != this)
        {
            delete base_;
            base_ = rhs.base_->clone();
        }
        return *this;
    }
    void ri_transform::set(const float mat[4 * 4]) { base_->set(mat); }

    void ri_transform::concat(const float mat[4 * 4]) { base_->concat(mat); }

    const float* ri_transform::get() const { return base_->get(); }

    const float* ri_transform::get_mat_at(size_t i) const
    {
        if (!base_->is_motion())
        {
            return base_->get();
        }
        else
        {
            return static_cast<const ri_transform_motion*>(base_)->get_mat_at(i);
        }
    }

    float ri_transform::get_time_at(size_t i) const
    {
        if (!base_->is_motion())
        {
            return 0.0f;
        }
        else
        {
            return static_cast<const ri_transform_motion*>(base_)->get_time_at(i);
        }
    }

    size_t ri_transform::get_size() const
    {
        if (!base_->is_motion())
        {
            return 1;
        }
        else
        {
            return static_cast<const ri_transform_motion*>(base_)->get_size();
        }
    }

    void ri_transform::identity() { base_->identity(); }
    void ri_transform::transpose() { base_->transpose(); }
    bool ri_transform::invert() { return base_->invert(); }
    bool ri_transform::is_motion() const { return base_->is_motion(); }
    void ri_transform::translate(float x, float y, float z)
    {
        base_->translate(x, y, z);
    }
    void ri_transform::scale(float x, float y, float z) { base_->scale(x, y, z); }
    void ri_transform::rotate(float angle, float dx, float dy, float dz)
    {
        base_->rotate(angle, dx, dy, dz);
    }
    void ri_transform::skew(float angle, float dx1, float dy1, float dz1, float dx2,
                            float dy2, float dz2)
    {
        base_->skew(angle, dx1, dy1, dz1, dx2, dy2, dz2);
    }
    void ri_transform::perspective(float fov) { base_->perspective(fov); }

    void ri_transform::print_() { base_->print_(); }
    float ri_transform::det() const { return base_->det(); }
}
