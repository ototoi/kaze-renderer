#include "ri_triangulation.h"
#include "ri_types.h"
#include <vector>

namespace ri
{
    enum
    {
        ORIENTATION_CW = 0,
        ORIENTATION_CCW,
        ORIENTATION_UNKNOWN
    };

    static float det(float a, float b, float c, float d) { return a * d - b * c; }

    static int calc_determinant(const vector2f& a, const vector2f& b,
                                const vector2f& c)
    {
        vector2f ab = b - a;
        vector2f ac = c - a;
        float DET = det(ab[0], ac[0], ab[1], ac[1]); // ad-bc

        if (DET > 0)
        {
            return ORIENTATION_CCW;
        }
        else if (DET < 0)
        {
            return ORIENTATION_CW;
        }
        else
        {
            return ORIENTATION_UNKNOWN;
        }
    }

    // calc angle B
    static float calc_angle(const vector2f& a, const vector2f& b,
                            const vector2f& c)
    {
        int nDet = calc_determinant(a, b, c);
        if (nDet == ORIENTATION_UNKNOWN)
            return 0;

        vector2f ba = (a - b);
        ba = normalize(ba);
        vector2f bc = (c - b);
        bc = normalize(bc);

        float cc = -dot(ba, bc);
        float cos_m = acos(cc);
        if (nDet == ORIENTATION_CCW)
        {
            return cos_m;
        }
        else
        {
            return -cos_m;
        }
    }
/*
    static void CreateVertices(std::vector<vector3f>& verts, int nverts,
                               const float P[])
    {
        verts.resize(nverts);
        for (int i = 0; i < nverts; i++)
        {
            verts[i] = vector3f(P + 3 * i);
        }
    }
*/
    static vector3f CreateMainNormal(const std::vector<vector3f>& verts)
    {
        vector3f n(0, 0, 0);
        int vsz = (int)verts.size();
        for (int i = 0; i < vsz; i++)
        {
            vector3f e1 = verts[(i + 1) % vsz] - verts[i];
            vector3f e2 = verts[(i + 2) % vsz] - verts[i];
            vector3f nn = cross(e1, e2);
            n += nn;
        }
        return normalize(n);
    }

    static matrix3f CreateRotationMatrix(const std::vector<vector3f>& verts)
    {
        vector3f z = CreateMainNormal(verts);
        int nPlane = 0;
        if (fabs(z[1]) < fabs(z[nPlane]))
            nPlane = 1;
        if (fabs(z[2]) < fabs(z[nPlane]))
            nPlane = 2;
        vector3f xx = vector3f(0, 0, 0);
        xx[nPlane] = 1;
        vector3f y = normalize(cross(z, xx));
        vector3f x = cross(y, z);
        return matrix3f(x[0], x[1], x[2], y[0], y[1], y[2], z[0], z[1], z[2]);
    }

    static void CreateFlattenVertex(std::vector<vector3f>& flatten,
                                    const std::vector<vector3f>& verts)
    {
        matrix3f mat = CreateRotationMatrix(verts);
        flatten.resize(verts.size());
        for (size_t i = 0; i < verts.size(); i++)
        {
            flatten[i] = mat * verts[i];
        }
    }

    static void CreateIndices(std::vector<int>& out,
                              const std::vector<vector3f>& positions)
    {
        std::vector<vector3f> flatten;
        CreateFlattenVertex(flatten, positions);
        int sz = (int)flatten.size();
        std::vector<vector2f> pos(sz);
        std::vector<int> idx(sz);
        for (int i = 0; i < sz; i++)
        {
            pos[i] = vector2f(flatten[i][0], flatten[i][1]);
            idx[i] = i;
        }

        while (idx.size() > 3)
        {
            int nFind = 0;
            float ang = 8;
            int ksz = (int)idx.size();
            for (int i = 0; i < ksz; i++)
            {
                vector2f p0 = pos[(i + ksz - 1) % ksz];
                vector2f p1 = pos[i];
                vector2f p2 = pos[(i + ksz + 1) % ksz];

                float a = calc_angle(p0, p1, p2);
                if (a < ang)
                {
                    ang = a;
                    nFind = i;
                }
            }
            int i0 = idx[(nFind + ksz - 1) % ksz];
            int i1 = idx[nFind];
            int i2 = idx[(nFind + ksz + 1) % ksz];

            out.push_back(i0);
            out.push_back(i1);
            out.push_back(i2);

            idx.erase(idx.begin() + nFind);
            pos.erase(pos.begin() + nFind);
        }
        out.push_back(idx[0]);
        out.push_back(idx[1]);
        out.push_back(idx[2]);
    }

    void ri_polygon_triangulate_indices(
        std::vector<int>& indices, std::vector<int>& tri2poly,
        std::vector<int>& fvindices, // for face varying
        int nverts, const float P[])
    {
        if (nverts >= 3)
        {
            if (nverts == 3)
            {
                indices.resize(nverts);
                for (int i = 0; i < nverts; i++)
                {
                    indices[i] = i;
                }
            }
            else
            {
                indices.reserve(nverts * 3);
                std::vector<vector3f> positions(nverts);
                for (int i = 0; i < nverts; i++)
                {
                    positions[i] = vector3f(P[3 * i + 0], P[3 * i + 1], P[3 * i + 2]);
                }
                CreateIndices(indices, positions);
            }
            tri2poly.resize(nverts - 2);
            memset(&tri2poly[0], 0, sizeof(int) * (nverts - 2));
        }
        fvindices = indices;
    }

    void ri_general_polygon_triangulate_indices(
        std::vector<int>& indices, std::vector<int>& tri2poly,
        std::vector<int>& fvindices, // for face varying
        int nloops, const int nverts[], const float P[])
    {
#if 0
            int k = 0;
            {
                int nV = nverts[0];
                for (int j=0; j<nV-2; j++)
                {
                    int s = k+j;
                    indices.push_back(k);
                    indices.push_back(s+1);
                    indices.push_back(s+2);

                    tri2poly.push_back(0);//
                }
                k+=nV;
            }
#else
        int nV = nverts[0];
        if (nV >= 3)
        {
            if (nV == 3)
            {
                for (int i = 0; i < nV; i++)
                {
                    indices.push_back(i);
                }
            }
            else
            {
                std::vector<vector3f> positions(nV);
                for (int i = 0; i < nV; i++)
                {
                    positions[i] = vector3f(P[3 * i + 0], P[3 * i + 1], P[3 * i + 2]);
                }
                CreateIndices(indices, positions);
            }

            for (int j = 0; j < nV - 2; j++)
            {
                tri2poly.push_back(0); //
            }
        }
#endif
        fvindices = indices;
    }

    void ri_points_polygons_triangulate_indices(
        std::vector<int>& indices, std::vector<int>& tri2poly,
        std::vector<int>& fvindices, // for face varying
        int npolys, const int nverts[], const int verts[], const float P[])
    {
#if 0
            int k = 0;
            indices.reserve(npolys*3*2);
            for (int i=0; i<npolys; i++) {
                int nV = nverts[i];
                for (int j=0; j<nV-2; j++) {
                    int s = k+j;
                    indices.push_back(verts[k]);
                    indices.push_back(verts[s+1]);
                    indices.push_back(verts[s+2]);

                    fvindices.push_back(k);
                    fvindices.push_back(s+1);
                    fvindices.push_back(s+2);

                    tri2poly.push_back(i);
                }
                k+=nV;
            }
#else
        int k = 0;
        indices.reserve(npolys * 3 * 2);
        for (int i = 0; i < npolys; i++)
        {
            int nV = nverts[i];
            if (nV >= 3)
            {
                if (nV == 3)
                {
                    for (int j = 0; j < 3; j++)
                    {
                        indices.push_back(verts[k + j]);
                        fvindices.push_back(k + j);
                    }
                }
                else
                {
                    std::vector<vector3f> positions(nV);
                    for (int j = 0; j < nV; j++)
                    {
                        positions[j] = vector3f(P + 3 * verts[k + j]);
                    }
                    {
                        std::vector<int> idx;
                        CreateIndices(idx, positions);
                        for (int j = 0; j < idx.size(); j++)
                        {
                            indices.push_back(verts[k + idx[j]]);
                            fvindices.push_back(k + idx[j]);
                        }
                    }
                }
                for (int j = 0; j < nV - 2; j++)
                {
                    tri2poly.push_back(i); //
                }
            }
            k += nV;
        }
#endif
    }

    void ri_points_general_polygons_triangulate_indices(
        std::vector<int>& indices, std::vector<int>& tri2poly,
        std::vector<int>& fvindices, // for face varying
        int npolys, const int nloops[], const int nverts[], const int verts[],
        const float P[])
    {
        int k = 0;
        int v = 0;
        for (int i = 0; i < npolys; i++)
        {
            int nloop = nloops[i];
            {
                int nV = nverts[v];
                if (nV >= 3)
                {
#if 0
                        for (int j=0; j<nV-2; j++)
                        {
                            int s = k+j;
                            indices.push_back(verts[k]);
                            indices.push_back(verts[s+1]);
                            indices.push_back(verts[s+2]);

                            fvindices.push_back(k);
                            fvindices.push_back(s+1);
                            fvindices.push_back(s+2);
                        }
#else
                    if (nV == 3)
                    {
                        for (int j = 0; j < 3; j++)
                        {
                            indices.push_back(verts[k + j]);
                            fvindices.push_back(k + j);
                        }
                    }
                    else
                    {
                        std::vector<vector3f> positions(nV);
                        for (int j = 0; j < nV; j++)
                        {
                            positions[j] = vector3f(P + 3 * verts[k + j]);
                        }
                        {
                            std::vector<int> idx;
                            CreateIndices(idx, positions);
                            for (int j = 0; j < idx.size(); j++)
                            {
                                indices.push_back(verts[k + idx[j]]);
                                fvindices.push_back(k + idx[j]);
                            }
                        }
                    }
#endif
                    for (int j = 0; j < nV - 2; j++)
                    {
                        tri2poly.push_back(i); //
                    }
                }
                k += nV;
            }
            for (int j = 1; j < nloop; j++)
            {
                int nV = nverts[v];
                {
                    ;
                }
                k += nV;
            }
            v += nloop;
        }
    }
}
