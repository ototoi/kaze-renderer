#ifndef RI_CLASSIFIER_H
#define RI_CLASSIFIER_H

#include <vector>
#include <string>

namespace ri
{
    class ri_classifier
    {
    public:
        struct param_data
        {
            std::string name_;
            int class_;
            int type_;
            int size_;
            param_data(const char* n, int c, int t, int s)
                : name_(n), class_(c), type_(t), size_(s) {}
            const char* get_name() const { return name_.c_str(); }
            int get_class() const { return class_; }
            int get_type() const { return type_; }
            int get_size() const { return size_; }
            void set_name(const char* name) { name_ = name; }
        };
        enum ENUM_CLASS
        {
            CONSTANT,
            UNIFORM,
            VARYING,
            VERTEX,
            FACEVARYING
        };
        enum ENUM_TYPE
        {
            INTEGER,
            FLOAT,
            STRING,
            COLOR,
            POINT,
            VECTOR,
            NORMAL,
            HPOINT,
            MATRIX,
            BASIS,
            BOUND
        };

    public:
        static int get_size(int type);

    public:
        ri_classifier();
        explicit ri_classifier(int noinit);
        ri_classifier(const ri_classifier& rhs);
        virtual ~ri_classifier();
        size_t size() const;
        virtual const param_data& operator[](size_t i) const;
        virtual int find(const char* name) const;
        virtual int def(const char* name);

    public:
        void add(const param_data& param);
        bool check_param_data();
        virtual int find_raw(const char* name) const;
    protected:
        void add(const char* name_, int class_, int type_, int size_ = 0); /*add*/
        void add(const char* name_, int class_, const std::string& type_);
        void init();
    private:
        std::vector<param_data> data_;
    };

    class ri_temporary_classifier : public ri_classifier
    {
    public:
        explicit ri_temporary_classifier(const ri_classifier* parent_);
        virtual const param_data& operator[](size_t i) const;
        virtual int find(const char* name) const;
        virtual int def(const char* name);

    private:
        const ri_classifier* parent_;
    };

    class ri_saving_classifier : public ri_temporary_classifier
    {
    public:
        ri_saving_classifier(const ri_classifier* parent, ri_classifier* store);
        virtual const param_data& operator[](size_t i) const;
        virtual int find(const char* name) const;
        virtual int def(const char* name);

    private:
        ri_classifier* store_;
    };
}
#endif
