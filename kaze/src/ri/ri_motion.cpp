#include "ri_motion.h"

namespace ri
{
    ri_motion::ri_motion(int n, const float times[], const ri_transform& trans)
        : i_(0), j_(0), bTransform_(false)
    {
        times_.assign(times, times + n);
        transes_.resize(n);
        for (int i = 0; i < n; i++)
        {
            transes_[i].set(trans.get());
        }
    }

    ri_motion::~ri_motion()
    {
        ; //
    }

    bool ri_motion::is_transform() const { return bTransform_; }

    bool ri_motion::is_geometry() const { return !is_transform(); }

    bool ri_motion::is_completed() const
    {
        if (is_transform())
        {
            return i_ >= get_n();
        }
        else
        {
            return j_ >= get_n();
        }
    }

    void ri_motion::identity()
    {
        size_t i = i_++;
        if (i < transes_.size())
        {
            transes_[i].identity();
            bTransform_ = true;
        }
    }

    void ri_motion::set(const float mat[4 * 4])
    {
        size_t i = i_++;
        if (i < transes_.size())
        {
            transes_[i].set(mat);
            bTransform_ = true;
        }
    }

    void ri_motion::concat(const float mat[4 * 4])
    {
        size_t i = i_++;
        if (i < transes_.size())
        {
            transes_[i].concat(mat);
            bTransform_ = true;
        }
    }

    void ri_motion::translate(float x, float y, float z)
    {
        size_t i = i_++;
        if (i < transes_.size())
        {
            transes_[i].translate(x, y, z);
            bTransform_ = true;
        }
    }

    void ri_motion::scale(float x, float y, float z)
    {
        size_t i = i_++;
        if (i < transes_.size())
        {
            transes_[i].scale(x, y, z);
            bTransform_ = true;
        }
    }

    void ri_motion::rotate(float angle, float dx, float dy, float dz)
    {
        size_t i = i_++;
        if (i < transes_.size())
        {
            transes_[i].rotate(angle, dx, dy, dz);
            bTransform_ = true;
        }
    }

    void ri_motion::skew(float angle, float dx1, float dy1, float dz1, float dx2,
                         float dy2, float dz2)
    {
        size_t i = i_++;
        if (i < transes_.size())
        {
            transes_[i].skew(angle, dx1, dy1, dz1, dx2, dy2, dz2);
            bTransform_ = true;
        }
    }

    void ri_motion::perspective(float fov)
    {
        size_t i = i_++;
        if (i < transes_.size())
        {
            transes_[i].perspective(fov);
            bTransform_ = true;
        }
    }

    void ri_motion::add_geometry(const ri_geometry* geo)
    {
        j_++;
        geos_.push_back(geo);
    }

    int ri_motion::get_n() const { return (int)times_.size(); }

    float ri_motion::get_time_at(size_t i) const { return times_[i]; }

    ri_transform ri_motion::get_transform_at(size_t i) const { return transes_[i]; }

    const ri_geometry* ri_motion::get_geometry_at(size_t i) const
    {
        return geos_[i];
    }
}
