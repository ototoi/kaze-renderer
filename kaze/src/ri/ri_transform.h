#ifndef RI_RI_TRANSFORM_H
#define RI_RI_TRANSFORM_H

#include <vector>

namespace ri
{
    class ri_transform_base;

    class ri_transform
    {
    public:
        ri_transform();
        ri_transform(const float mat[4 * 4]);
        ri_transform(const ri_transform& rhs);
        ri_transform(int n, const float times[], const float* mats[]);
        ri_transform(int n, const float times[], const ri_transform mats[]);
        ~ri_transform();
        ri_transform& operator=(const ri_transform& rhs);
        void set(const float mat[4 * 4]);
        void concat(const float mat[4 * 4]);
        const float* get() const;
        const float* get_mat_at(size_t i) const;
        float get_time_at(size_t i) const;
        size_t get_size() const;

    public:
        void identity();
        void transpose();
        bool invert();
        bool is_motion() const;

    public:
        void translate(float x, float y, float z);
        void scale(float x, float y, float z);
        void rotate(float angle, float dx, float dy, float dz);
        void skew(float angle, float dx1, float dy1, float dz1, float dx2, float dy2,
                  float dz2);
        void perspective(float fov);

    public:
        void print_(); // for debug
        float det() const;

    private:
        ri_transform_base* base_;
    };
}

#endif
