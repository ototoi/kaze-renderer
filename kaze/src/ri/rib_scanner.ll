%option noyywrap nounput batch
%option never-interactive
%option noyy_scan_buffer
%option noyy_scan_bytes
%option noyy_scan_string
%option 8bit reentrant
%option nounistd

%{

#ifdef _WIN32
#pragma warning(disable : 4018)
#pragma warning(disable : 4102)
#pragma warning(disable : 4244)
#pragma warning(disable : 4267)
#pragma warning(disable : 4786)
#pragma warning(disable : 4996)

extern "C" int isatty(int);

#endif

#include <stdio.h>
#include <string>
#include <string.h>
#include <memory>
#ifdef HAVE_ZLIB
#include <zlib.h>
#endif

#include "rib_parser.hpp"
#include "ri_context.h"
#include "ri_rib_decoder.h"
#include "ri_rib_parse_param.h"

#define YY_EXTRA_TYPE ri::ri_rib_parse_param


#define PARAM() ((ri::ri_rib_parse_param*)(yyscanner))

#define	YY_DECL											\
	int							\
	yylex(yy::rib_parser::semantic_type* yylval,		\
	     yy::rib_parser::location_type* yyloc,          \
		 void* yyscanner)


#undef YY_INPUT
#define YY_INPUT(buf, retval, maxlen)	if ( (retval = PARAM()->dec->read(buf,maxlen)) < 0) 			\
											YY_FATAL_ERROR( "input in flex scanner failed" );


typedef yy::rib_parser::token token;


%}



delim				[ \t\\]
whitespace		{delim}+
letter				[A-Za-z]
digit					[0-9]
float				[+\-]?{digit}*(\.{digit}*)?([eE][+\-]?{digit}+)?
integer			[+\-]?{digit}+
string				\"[^"]*\"
version_string		{digit}+(\.{digit}+)*
eol					\r\n|\r|\n
comment_		##.*
comment			#.*


%s params
%x incl skip_frame

/* %option lex-compat */
%option noreject

%%
	using namespace ri;

	bool fRequest   = PARAM()->fRequest;
	bool fParams    = PARAM()->fParams;
	bool fSkipFrame = PARAM()->fSkipFrame;

	if(fRequest)
	{
		BEGIN(INITIAL);
		fRequest=false;
	}

	if(fParams)
	{
		BEGIN(params);
		fParams=false;
	}

	if(fSkipFrame)
	{
		BEGIN(skip_frame);
		fSkipFrame=false;
	}

	PARAM()->fRequest   = fRequest;
	PARAM()->fParams    = fParams;
	PARAM()->fSkipFrame = fSkipFrame;


{comment_}			{
						yylval->stype = new char[strlen(yytext)+1]; strcpy(yylval->stype, yytext);
						return token::COMMENT;
					}
<*>{eol}			{ PARAM()->line_number++; }
<*>{whitespace}		{  }
<*>{comment}	    {  }

<skip_frame>FrameEnd	{BEGIN(INITIAL);}
<skip_frame>.

<params>{integer}		{ yylval->itype = atoi(yytext); return token::INTEGER_TOKEN; }
<params>{float}			{ yylval->ftype = (float)atof(yytext); return token::FLOAT_TOKEN; }
<params>{string}		{ std::string temp = ri_rib_decoder::string_filter(yytext); yylval->stype = new char[temp.size()+1];yylval->stype[temp.size()]=0; strcpy(yylval->stype, temp.c_str()); return token::STRING_TOKEN; }
<params>{version_string} { std::string temp(yytext); yylval->stype = new char[temp.size()-1]; strcpy(yylval->stype, temp.c_str()); return token::STRING_TOKEN; }

<INITIAL>{integer}		{ return token::INVALID_VALUE; }
<INITIAL>{float}		{ return token::INVALID_VALUE; }
<INITIAL>{string}		{ return token::INVALID_VALUE; }

%{
//BesselFilter			{ return token::TOKEN_BESSELFILTER; }
//BoxFilter			{ return token::TOKEN_BOXFILTER; }
//CatmullRomFilter		{ return token::TOKEN_CATMULLROMFILTER; }
//DiskFilter			{ return token::TOKEN_DISKFILTER; }
//GaussianFilter			{ return token::TOKEN_GAUSSIANFILTER; }
//SincFilter			{ return token::TOKEN_SINCFILTER; }
//TriangleFilter			{ return token::TOKEN_TRIANGLEFILTER; }
%}

ArchiveBegin		{ return token::TOKEN_ARCHIVEBEGIN; }
ArchiveEnd			{ return token::TOKEN_ARCHIVEEND; }
AreaLightSource			{ return token::TOKEN_AREALIGHTSOURCE; }
Atmosphere			{ return token::TOKEN_ATMOSPHERE; }
Attribute			{ return token::TOKEN_ATTRIBUTE; }
AttributeBegin			{ return token::TOKEN_ATTRIBUTEBEGIN; }
AttributeEnd			{ return token::TOKEN_ATTRIBUTEEND; }
Basis				{ return token::TOKEN_BASIS; }
Blobby      			{ return token::TOKEN_BLOBBY; }
Bound				{ return token::TOKEN_BOUND; }
Clipping			{ return token::TOKEN_CLIPPING; }
Color				{ return token::TOKEN_COLOR; }
ColorSamples			{ return token::TOKEN_COLORSAMPLES; }
ConcatTransform			{ return token::TOKEN_CONCATTRANSFORM; }
Cone				{ return token::TOKEN_CONE; }
CoordSysTransform		{ return token::TOKEN_COORDSYSTRANSFORM; }
CoordinateSystem		{ return token::TOKEN_COORDINATESYSTEM; }
CropWindow			{ return token::TOKEN_CROPWINDOW; }
Curves      			{ return token::TOKEN_CURVES; }
Cylinder			{ return token::TOKEN_CYLINDER; }
Declare				{ return token::TOKEN_DECLARE; }
Deformation			{ return token::TOKEN_DEFORMATION; }
DepthOfField			{ return token::TOKEN_DEPTHOFFIELD; }
Detail				{ return token::TOKEN_DETAIL; }
DetailRange			{ return token::TOKEN_DETAILRANGE; }
Disk				{ return token::TOKEN_DISK; }
Displacement			{ return token::TOKEN_DISPLACEMENT; }
DisplayChannel		{ return token::TOKEN_DISPLAYCHANNEL; }
Display				{ return token::TOKEN_DISPLAY; }
Else				{ return token::TOKEN_ELSE; }
ElseIf				{ return token::TOKEN_ELSEIF; }
ErrorHandler			{ return token::TOKEN_ERRORHANDLER; }
Exposure			{ return token::TOKEN_EXPOSURE; }
Exterior			{ return token::TOKEN_EXTERIOR; }
Format				{ return token::TOKEN_FORMAT; }
FrameAspectRatio		{ return token::TOKEN_FRAMEASPECTRATIO; }
FrameBegin			{ return token::TOKEN_FRAMEBEGIN; }
FrameEnd			{ return token::TOKEN_FRAMEEND; }
GeneralPolygon			{ return token::TOKEN_GENERALPOLYGON; }
GeometricApproximation		{ return token::TOKEN_GEOMETRICAPPROXIMATION; }
Geometry			{ return token::TOKEN_GEOMETRY; }
HierarchicalSubdivisionMesh	{return token::TOKEN_HIERARCHICALSUBDIVISIONMESH; }
Hider				{ return token::TOKEN_HIDER; }
Hyperboloid			{ return token::TOKEN_HYPERBOLOID; }
Identity			{ return token::TOKEN_IDENTITY; }
Illuminate			{ return token::TOKEN_ILLUMINATE; }
Imager				{ return token::TOKEN_IMAGER; }
Interior			{ return token::TOKEN_INTERIOR; }
IfBegin				{ return token::TOKEN_IFBEGIN; }
IfEnd				{ return token::TOKEN_IFEND; }
LightSource			{ return token::TOKEN_LIGHTSOURCE; }
MakeBrickMap		{ return token::TOKEN_MAKEBRICKMAP; }
MakeBump			{ return token::TOKEN_MAKEBUMP; }
MakeCubeFaceEnvironment 	{ return token::TOKEN_MAKECUBEFACEENVIRONMENT; }
MakeLatLongEnvironment		{ return token::TOKEN_MAKELATLONGENVIRONMENT; }
MakeOcclusion			{ return token::TOKEN_MAKEOCCLUSION; }
MakeShadow			{ return token::TOKEN_MAKESHADOW; }
MakeTexture			{ return token::TOKEN_MAKETEXTURE; }
Matte				{ return token::TOKEN_MATTE; }
MotionBegin			{ return token::TOKEN_MOTIONBEGIN; }
MotionEnd			{ return token::TOKEN_MOTIONEND; }
NuPatch				{ return token::TOKEN_NUPATCH; }
ObjectBegin			{ return token::TOKEN_OBJECTBEGIN; }
ObjectEnd			{ return token::TOKEN_OBJECTEND; }
ObjectInstance			{ return token::TOKEN_OBJECTINSTANCE; }
Opacity				{ return token::TOKEN_OPACITY; }
Option				{ return token::TOKEN_OPTION; }
Orientation			{ return token::TOKEN_ORIENTATION; }
Paraboloid			{ return token::TOKEN_PARABOLOID; }
Patch				{ return token::TOKEN_PATCH; }
PatchMesh			{ return token::TOKEN_PATCHMESH; }
Perspective			{ return token::TOKEN_PERSPECTIVE; }
PixelFilter			{ return token::TOKEN_PIXELFILTER; }
PixelSamples			{ return token::TOKEN_PIXELSAMPLES; }
PixelVariance			{ return token::TOKEN_PIXELVARIANCE; }
Points      			{ return token::TOKEN_POINTS; }
PointsGeneralPolygons		{ return token::TOKEN_POINTSGENERALPOLYGONS; }
PointsPolygons			{ return token::TOKEN_POINTSPOLYGONS; }
Polygon				{ return token::TOKEN_POLYGON; }
Procedural			{ return token::TOKEN_PROCEDURAL; }
Procedure			{ return token::TOKEN_PROCEDURE; }
Projection			{ return token::TOKEN_PROJECTION; }
Quantize			{ return token::TOKEN_QUANTIZE; }
ReadArchive			{ return token::TOKEN_READARCHIVE; }
RelativeDetail			{ return token::TOKEN_RELATIVEDETAIL; }
Resource			{ return token::TOKEN_RESOURCE; }
ResourceBegin		{ return token::TOKEN_RESOURCEBEGIN; }
ResourceEnd			{ return token::TOKEN_RESOURCEEND; }
ReverseOrientation		{ return token::TOKEN_REVERSEORIENTATION; }
Rotate				{ return token::TOKEN_ROTATE; }
Scale				{ return token::TOKEN_SCALE; }
ScopedCoordinateSystem { return token::TOKEN_SCOPEDCOORDINATESYSTEM; }
ScreenWindow			{ return token::TOKEN_SCREENWINDOW; }
ShadingInterpolation		{ return token::TOKEN_SHADINGINTERPOLATION; }
ShadingRate			{ return token::TOKEN_SHADINGRATE; }
Shutter				{ return token::TOKEN_SHUTTER; }
Sides				{ return token::TOKEN_SIDES; }
Skew				{ return token::TOKEN_SKEW; }
SolidBegin			{ return token::TOKEN_SOLIDBEGIN; }
SolidEnd			{ return token::TOKEN_SOLIDEND; }
Sphere				{ return token::TOKEN_SPHERE; }
SubdivisionMesh			{ return token::TOKEN_SUBDIVISIONMESH; }
Surface				{ return token::TOKEN_SURFACE; }
TextureCoordinates		{ return token::TOKEN_TEXTURECOORDINATES; }
Torus				{ return token::TOKEN_TORUS; }
Transform			{ return token::TOKEN_TRANSFORM; }
TransformBegin			{ return token::TOKEN_TRANSFORMBEGIN; }
TransformEnd			{ return token::TOKEN_TRANSFORMEND; }
TransformPoints			{ return token::TOKEN_TRANSFORMPOINTS; }
Translate			{ return token::TOKEN_TRANSLATE; }
TrimCurve			{ return token::TOKEN_TRIMCURVE; }
WorldBegin			{ return token::TOKEN_WORLDBEGIN; }
WorldEnd			{ return token::TOKEN_WORLDEND; }
ShaderLayer			{ return token::TOKEN_SHADERLAYER; }
ConnectShaderLayers { return token::TOKEN_CONNECTSHADERLAYERS; }
Version				{ return token::TOKEN_VERSION; }
version				{ return token::TOKEN_VERSION; }
Procedural2			{ return token::TOKEN_PROCEDURAL2; }
System              { return token::TOKEN_SYSTEM; }
Volume              { return token::TOKEN_VOLUME; }
VPAtmosphere        { return token::TOKEN_VPATMOSPHERE; }
VPInterior          { return token::TOKEN_VPINTERIOR; }
VPSurface           { return token::TOKEN_VPSURFACE; }
Integrator			{ return token::TOKEN_INTEGRATOR; }
Bxdf				{ return token::TOKEN_BXDF; }
Pattern				{ return token::TOKEN_PATTERN; }





{letter}({letter}|{digit})*	{ return token::UNKNOWN_TOKEN; }
.				{ return yytext[0]; }

<<EOF>>			{ yyterminate(); }




%%

namespace ri
{

void ri_rib_parse_param::scan_begin()
{

}

void ri_rib_parse_param::scan_end()
{
	;//
}

}
