#ifndef RI_EVALUATE_BLOBBY_H
#define RI_EVALUATE_BLOBBY_H

#include <cstring>
#include <string>
#include <vector>
#include "ri_blobby.h"

namespace ri
{
    std::shared_ptr<ri_blobby> ri_evaluate_blobby(int nleaf, int ncode, const int code[],
                                            int nflt, const float flt[], int nstr,
                                            const std::string str[]);
    std::shared_ptr<ri_blobby> ri_optimize_blobby(const std::shared_ptr<ri_blobby>& blobby);
    std::shared_ptr<ri_blobby> ri_attributes_blobby(const std::shared_ptr<ri_blobby>& blobby,
                                              const ri_classifier& cls,
                                              const ri_parameters& params);
}
#endif
