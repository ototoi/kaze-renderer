#include "ri_path_resolver.h"
#include <string>

#ifdef _WIN32
#include <windows.h>
#else
#include <libgen.h>
#include <stdlib.h>
#endif

#ifdef __APPLE__
#include <mach-o/dyld.h>
#endif

#if defined(__linux__) || defined(FREEBSD)
#include <unistd.h>
#endif

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

namespace ri
{

    static std::string ri_get_full_path(const std::string& path)
    {
#if defined(_WIN32)
        char szFullPath[_MAX_PATH];
        _fullpath(szFullPath, path.c_str(), _MAX_PATH);
        return szFullPath;
#else
        char szFullPath[1024];
        realpath(path.c_str(), szFullPath);
        return szFullPath;
#endif
    }

    static std::string ri_get_exec_path_()
    {
#if defined(_WIN32)
        char buffer[_MAX_PATH];
        ::GetModuleFileNameA(NULL, buffer, _MAX_PATH);
        return buffer;
#elif defined(__APPLE__)
        char path[1024] = {};
        uint32_t size = sizeof(path);
        _NSGetExecutablePath(path, &size);
        return ri_get_full_path(path);
#elif defined(__linux__)
        char path[1024] = {};
        size_t size = sizeof(path);
        readlink("/proc/self/exe", path, size);
        return path;
#elif defined(FREEBSD)
        char path[1024] = {};
        size_t size = sizeof(path);
        readlink("/proc/curproc/file", path, size);
        return path;
#else
        return "";
#endif
    }

    static std::string dirname_(const std::string& path)
    {
        return path.substr(0, path.find_last_of('/'));
    }

    static std::string ri_get_bin_path_()
    {
#ifdef _WIN32
        static std::string strExec = ri_get_exec_path_();
        char szDrive[_MAX_DRIVE];
        char szDir[_MAX_DIR];
        _splitpath(strExec.c_str(), szDrive, szDir, NULL, NULL);
        std::string strRet1;
        strRet1 += szDrive;
        strRet1 += szDir;
        return ri_get_full_path(strRet1);
#else
        static std::string strExec = ri_get_exec_path_();
        static std::string strDir = dirname_(strExec);
        std::string strRet1;
        strRet1 = strDir;
        strRet1 += "/";
        return ri_get_full_path(strRet1) + "/";
#endif
    }

    static std::string ri_get_xxx_path_(const char* szFolder)
    {
#ifdef _WIN32
        static std::string strExec = ri_get_exec_path_();
        char szDrive[_MAX_DRIVE];
        char szDir[_MAX_DIR];
        _splitpath(strExec.c_str(), szDrive, szDir, NULL, NULL);
        std::string strRet1;
        strRet1 += szDrive;
        strRet1 += szDir;
        strRet1 += szFolder;
        return ri_get_full_path(strRet1);
#else
        static std::string strExec = ri_get_exec_path_();
        static std::string strDir = dirname_(strExec);
        std::string strRet1;
        strRet1 = strDir;
        strRet1 += "/";
        strRet1 += szFolder;
        return ri_get_full_path(strRet1) + "/";
#endif
    }

    static std::string ri_get_root_path_()
    {
#ifdef _WIN32
        static std::string strExec = ri_get_exec_path_();
        char szDrive[_MAX_DRIVE];
        char szDir[_MAX_DIR];
        _splitpath(strExec.c_str(), szDrive, szDir, NULL, NULL);
        std::string strRet1;
        strRet1 += szDrive;
        strRet1 += szDir;
        strRet1 += "../";
        return ri_get_full_path(strRet1);
#else
        static std::string strExec = ri_get_exec_path_();
        static std::string strDir = dirname_(strExec);
        std::string strRet1;
        strRet1 = strDir;
        strRet1 += "/";
        strRet1 += "../";
        return ri_get_full_path(strRet1) + "/";
#endif
    }

    //----------------------------------------------

    ri_path_resolver::ri_path_resolver() {}
    ri_path_resolver::ri_path_resolver(const ri_options& opts) : opts_(opts) {}

    std::string ri_path_resolver::get_root_path()
    {
        static std::string strRet = ri_get_root_path_();
        return strRet;
    }

    std::string ri_path_resolver::get_exec_path()
    {
        static std::string strRet = ri_get_exec_path_();
        return strRet;
    }

    std::string ri_path_resolver::get_bin_path()
    {
        static std::string strRet = ri_get_bin_path_();
        return strRet;
    }

    std::string ri_path_resolver::get_plugin_path()
    {
        static std::string strRet = ri_get_xxx_path_("../plugin/");
        return strRet;
    }

    std::string ri_path_resolver::get_shaders_path()
    {
        static std::string strRet = ri_get_xxx_path_("../shaders/");
        return strRet;
    }

    std::string ri_path_resolver::get_textures_path()
    {
        static std::string strRet = ri_get_xxx_path_("../textures/");
        return strRet;
    }
    std::string ri_path_resolver::get_archives_path()
    {
        static std::string strRet = ri_get_xxx_path_("../archives/");
        return strRet;
    }

    //------------------------------------------
    static std::string rib_path_;
    std::string ri_path_resolver::get_rib_path() { return rib_path_; }
    void ri_path_resolver::set_rib_path(const std::string& str)
    {
        rib_path_ = ri_get_full_path(str);
    }

    static std::vector<std::string> split_path(const std::string& src,
                                               const std::string& delim)
    {
        std::vector<std::string> dest;

        std::string::size_type start = 0;
        while (true)
        {
            std::string::size_type end = src.find(delim, start);
            if (end != std::string::npos)
            {
                dest.push_back(src.substr(start, end - start));
            }
            else
            {
                dest.push_back(src.substr(start, src.length() - start));
                break;
            }
            start = end + delim.length();
        }
#ifdef _WIN32
        { // windows absolute path here.
            std::vector<std::string> d;
            int i = 0;
            while (i < dest.size())
            {
                int sz = (int)dest[i].size();
                if (sz == 0)
                {
                    i++;
                    continue;
                }
                if (sz == 1)
                {
                    char c = dest[i][0];
                    if (('a' <= c && c <= 'z') || ('A' <= c && c <= 'Z'))
                    {
                        if (i + 1 < dest.size())
                        {
                            std::string str = dest[i] + ":" + dest[i + 1];
                            d.push_back(str);
                        }
                        i += 2;
                    }
                    else
                    {
                        d.push_back(dest[i]);
                        i++;
                    }
                }
                else
                {
                    d.push_back(dest[i]);
                    i++;
                }
            }
            dest.swap(d);
        }
#endif
        return dest;
    }

    static std::string kill_space(const std::string& str)
    {
        std::string tmp = str;
        size_t s, e;
        if ((s = tmp.find_first_not_of(" ")) == std::string::npos)
            return "";
        tmp = tmp.substr(s);
        if ((e = tmp.find_last_not_of(" ")) == std::string::npos)
            return tmp;
        tmp = tmp.substr(0, e + 1);
        return tmp;
    }

    static std::string replace_default(const std::string& path,
                                       const std::string& default_path)
    {
        std::string str = path;
        std::string::size_type start = 0;
        while ((start = str.find("@\\\\")) != std::string::npos)
        {
            str = str.substr(0, start) + default_path + str.substr(start + 3);
        }
        while ((start = str.find("@/")) != std::string::npos)
        {
            str = str.substr(0, start) + default_path + str.substr(start + 2);
        }
        while ((start = str.find("@\\")) != std::string::npos)
        {
            str = str.substr(0, start) + default_path + str.substr(start + 2);
        }

        while ((start = str.find("@")) != std::string::npos)
        {
            str = str.substr(0, start) + default_path + str.substr(start + 1);
        }

        return str;
    }

    static std::string add_last(const std::string& path)
    {
        std::string str = path;
        char last = str[str.size() - 1];
        if (last != '/' && last != '\\')
            str += '/';
        return str;
    }

    static std::vector<std::string>
    expand_paths(const std::vector<std::string>& paths,
                 const std::string& default_path)
    {
        std::vector<std::string> sRet;
        for (size_t i = 0; i < paths.size(); i++)
        {
            std::string path = paths[i];
            if (path.empty())
                continue;
            if (path.find("&") != std::string::npos)
                continue;
            path = kill_space(path);
            path = replace_default(path, default_path);
            path = add_last(path);
            path = ri_get_full_path(path);
#ifndef _WIN32
            path += "/";
#endif
            sRet.push_back(path);
        }
        return sRet;
    }

    std::string ri_path_resolver::replace_options_path(const std::string& prev,
                                                       const std::string& path)
    {
        std::string str = path;
        std::string::size_type start = 0;
        while ((start = str.find("&")) != std::string::npos)
        {
            str = str.substr(0, start) + prev + str.substr(start + 1);
        }

        return str;
    }

    std::vector<std::string> ri_path_resolver::get_shaders_paths()
    {
        std::string shaderpath;
        opts_.get("searchpath", "shader", shaderpath);
        std::vector<std::string> sRet = split_path(shaderpath, ":");
        sRet = expand_paths(sRet, ri_path_resolver::get_shaders_path());
        return sRet;
    }

    std::vector<std::string> ri_path_resolver::get_textures_paths()
    {
        std::string shaderpath;
        opts_.get("searchpath", "texture", shaderpath);
        std::vector<std::string> sRet = split_path(shaderpath, ":");
        sRet = expand_paths(sRet, ri_path_resolver::get_textures_path());
        return sRet;
    }

    std::vector<std::string> ri_path_resolver::get_archives_paths()
    {
        std::string shaderpath;
        opts_.get("searchpath", "archive", shaderpath);
        std::vector<std::string> sRet = split_path(shaderpath, ":");
        sRet = expand_paths(sRet, ri_path_resolver::get_archives_path());
        return sRet;
    }
}
