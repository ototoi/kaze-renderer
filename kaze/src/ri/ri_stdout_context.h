#ifndef RI_STDOUT_CONTEXT_H
#define RI_STDOUT_CONTEXT_H

#include "ri_context.h"
#include <fstream>
#include <vector>

namespace ri
{
    class ri_stdout_context : public ri_context
    {
    public:
        virtual RtVoid RiVersion(char* version);

        virtual RtToken RiDeclare(char* name, char* declaration);
        virtual RtVoid RiBegin(RtToken name);
        virtual RtVoid RiEnd(void);
        virtual RtVoid RiFrameBegin(RtInt frame);
        virtual RtVoid RiFrameEnd(void);
        virtual RtVoid RiWorldBegin(void);
        virtual RtVoid RiWorldEnd(void);
        virtual RtVoid RiFormat(RtInt xres, RtInt yres, RtFloat aspect);
        virtual RtVoid RiFrameAspectRatio(RtFloat aspect);
        virtual RtVoid RiScreenWindow(RtFloat left, RtFloat right, RtFloat bot,
                                      RtFloat top);
        virtual RtVoid RiCropWindow(RtFloat xmin, RtFloat xmax, RtFloat ymin,
                                    RtFloat ymax);

        virtual RtVoid RiProjectionV(RtToken name, RtInt n, RtToken tokens[],
                                     RtPointer params[]);
        virtual RtVoid RiClipping(RtFloat hither, RtFloat yon);
        virtual RtVoid RiClippingPlane(RtFloat x, RtFloat y, RtFloat z, RtFloat nx,
                                       RtFloat ny, RtFloat nz);
        virtual RtVoid RiShutter(RtFloat min, RtFloat max);
        virtual RtVoid RiPixelVariance(RtFloat variation);
        virtual RtVoid RiPixelSamples(RtFloat xsamples, RtFloat ysamples);
        virtual RtVoid RiPixelSampleImagerV(RtToken name, RtInt n, RtToken tokens[],
                                            RtPointer params[]);
        virtual RtVoid RiPixelFilter(RtToken name, RtFloat xwidth, RtFloat ywidth);
        virtual RtVoid RiPixelFilter(RtFilterFunc filterfunc, RtFloat xwidth,
                                     RtFloat ywidth);
        virtual RtVoid RiExposure(RtFloat gain, RtFloat gamma);

        virtual RtVoid RiImagerV(RtToken name, RtInt n, RtToken tokens[],
                                 RtPointer params[]);
        virtual RtVoid RiQuantize(RtToken type, RtInt one, RtInt min, RtInt max,
                                  RtFloat ampl);

        virtual RtVoid RiDisplayV(char* name, RtToken type, RtToken mode, RtInt n,
                                  RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiDisplayChannelV(RtToken channel, RtInt n, RtToken tokens[],
                                         RtPointer params[]);

        virtual RtVoid RiHiderV(RtToken type, RtInt n, RtToken tokens[],
                                RtPointer params[]);
        virtual RtVoid RiColorSamples(RtInt n, RtFloat nRGB[], RtFloat RGBn[]);
        virtual RtVoid RiRelativeDetail(RtFloat relativedetail);

        virtual RtVoid RiOptionV(RtToken name, RtInt n, RtToken tokens[],
                                 RtPointer params[]);
        virtual RtVoid RiAttributeBegin(void);
        virtual RtVoid RiAttributeEnd(void);
        virtual RtVoid RiColor(RtColor color);
        virtual RtVoid RiOpacity(RtColor color);
        virtual RtVoid RiTextureCoordinates(RtFloat s1, RtFloat t1, RtFloat s2,
                                            RtFloat t2, RtFloat s3, RtFloat t3,
                                            RtFloat s4, RtFloat t4);

        virtual RtLightHandle RiLightSourceV(RtToken name, RtInt n, RtToken tokens[],
                                             RtPointer params[]);

        virtual RtLightHandle RiAreaLightSourceV(RtToken name, RtInt n,
                                                 RtToken tokens[],
                                                 RtPointer params[]);

        virtual RtVoid RiIlluminate(RtInt id, RtBoolean onoff);
        virtual RtVoid RiIlluminate(RtToken name, RtBoolean onoff);
        // virtual RtVoid RiIlluminate(RtLightHandle light, RtBoolean onoff);

        virtual RtVoid RiSurfaceV(RtToken name, RtInt n, RtToken tokens[],
                                  RtPointer params[]);

        virtual RtVoid RiAtmosphereV(RtToken name, RtInt n, RtToken tokens[],
                                     RtPointer params[]);

        virtual RtVoid RiInteriorV(RtToken name, RtInt n, RtToken tokens[],
                                   RtPointer params[]);

        virtual RtVoid RiExteriorV(RtToken name, RtInt n, RtToken tokens[],
                                   RtPointer params[]);
        virtual RtVoid RiShadingRate(RtFloat size);
        virtual RtVoid RiShadingInterpolation(RtToken type);
        virtual RtVoid RiMatte(RtBoolean onoff);
        virtual RtVoid RiBound(RtBound bound);
        virtual RtVoid RiDetail(RtBound bound);
        virtual RtVoid RiDetailRange(RtFloat minvis, RtFloat lowtran, RtFloat uptran,
                                     RtFloat maxvis);
        virtual RtVoid RiGeometricApproximation(RtToken type, RtFloat value);
        virtual RtVoid RiOrientation(RtToken orientation);
        virtual RtVoid RiReverseOrientation(void);
        virtual RtVoid RiSides(RtInt sides);
        virtual RtVoid RiIdentity(void);
        virtual RtVoid RiTransform(RtMatrix transform);
        virtual RtVoid RiConcatTransform(RtMatrix transform);
        virtual RtVoid RiPerspective(RtFloat fov);
        virtual RtVoid RiTranslate(RtFloat dx, RtFloat dy, RtFloat dz);
        virtual RtVoid RiRotate(RtFloat angle, RtFloat dx, RtFloat dy, RtFloat dz);
        virtual RtVoid RiScale(RtFloat sx, RtFloat sy, RtFloat sz);
        virtual RtVoid RiSkew(RtFloat angle, RtFloat dx1, RtFloat dy1, RtFloat dz1,
                              RtFloat dx2, RtFloat dy2, RtFloat dz2);

        virtual RtVoid RiDeformationV(RtToken name, RtInt n, RtToken tokens[],
                                      RtPointer params[]);

        virtual RtVoid RiDisplacementV(RtToken name, RtInt n, RtToken tokens[],
                                       RtPointer params[]);
        virtual RtVoid RiCoordinateSystem(RtToken space);
        virtual RtVoid RiScopedCoordinateSystem(RtToken space);
        virtual RtVoid RiCoordSysTransform(RtToken space);
        virtual RtPoint* RiTransformPoints(RtToken fromspace, RtToken tospace,
                                           RtInt n, RtPoint points[]);
        virtual RtVoid RiTransformBegin(void);
        virtual RtVoid RiTransformEnd(void);

        virtual RtVoid RiResourceV(RtToken handle, RtToken type, RtInt n,
                                   RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiResourceBegin(void);
        virtual RtVoid RiResourceEnd(void);

        virtual RtVoid RiAttributeV(RtToken name, RtInt n, RtToken tokens[],
                                    RtPointer params[]);

        virtual RtVoid RiPolygonV(RtInt nverts, RtInt n, RtToken tokens[],
                                  RtPointer params[]);

        virtual RtVoid RiGeneralPolygonV(RtInt nloops, RtInt nverts[], RtInt n,
                                         RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiPointsPolygonsV(RtInt npolys, RtInt nverts[], RtInt verts[],
                                         RtInt n, RtToken tokens[],
                                         RtPointer params[]);

        virtual RtVoid RiPointsGeneralPolygonsV(RtInt npolys, RtInt nloops[],
                                                RtInt nverts[], RtInt verts[],
                                                RtInt n, RtToken tokens[],
                                                RtPointer params[]);

        virtual RtVoid RiBasis(RtToken ubasis, RtInt ustep, RtToken vbasis,
                               RtInt vstep); // token token
        virtual RtVoid RiBasis(RtBasis ubasis, RtInt ustep, RtToken vbasis,
                               RtInt vstep); // basis token
        virtual RtVoid RiBasis(RtToken ubasis, RtInt ustep, RtBasis vbasis,
                               RtInt vstep); // token basis
        virtual RtVoid RiBasis(RtBasis ubasis, RtInt ustep, RtBasis vbasis,
                               RtInt vstep); // basis basis

        virtual RtVoid RiPatchV(RtToken type, RtInt n, RtToken tokens[],
                                RtPointer params[]);

        virtual RtVoid RiPatchMeshV(RtToken type, RtInt nu, RtToken uwrap, RtInt nv,
                                    RtToken vwrap, RtInt n, RtToken tokens[],
                                    RtPointer params[]);

        virtual RtVoid RiNuPatchV(RtInt nu, RtInt uorder, RtFloat uknot[],
                                  RtFloat umin, RtFloat umax, RtInt nv, RtInt vorder,
                                  RtFloat vknot[], RtFloat vmin, RtFloat vmax,
                                  RtInt n, RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiTrimCurve(RtInt nloops, RtInt ncurves[], RtInt order[],
                                   RtFloat knot[], RtFloat min[], RtFloat max[],
                                   RtInt n[], RtFloat u[], RtFloat v[], RtFloat w[]);

        virtual RtVoid RiSphereV(RtFloat radius, RtFloat zmin, RtFloat zmax,
                                 RtFloat tmax, RtInt n, RtToken tokens[],
                                 RtPointer params[]);

        virtual RtVoid RiConeV(RtFloat height, RtFloat radius, RtFloat tmax, RtInt n,
                               RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiCylinderV(RtFloat radius, RtFloat zmin, RtFloat zmax,
                                   RtFloat tmax, RtInt n, RtToken tokens[],
                                   RtPointer params[]);

        virtual RtVoid RiHyperboloidV(RtPoint point1, RtPoint point2, RtFloat tmax,
                                      RtInt n, RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiParaboloidV(RtFloat rmax, RtFloat zmin, RtFloat zmax,
                                     RtFloat tmax, RtInt n, RtToken tokens[],
                                     RtPointer params[]);

        virtual RtVoid RiDiskV(RtFloat height, RtFloat radius, RtFloat tmax, RtInt n,
                               RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiTorusV(RtFloat majrad, RtFloat minrad, RtFloat phimin,
                                RtFloat phimax, RtFloat tmax, RtInt n,
                                RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiBlobbyV(RtInt nleaf, RtInt ncode, RtInt code[], RtInt nflt,
                                 RtFloat flt[], RtInt nstr, RtToken str[], RtInt n,
                                 RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiCurvesV(RtToken type, RtInt ncurves, RtInt nvertices[],
                                 RtToken wrap, RtInt n, RtToken tokens[],
                                 RtPointer params[]);

        virtual RtVoid RiPointsV(RtInt nverts, RtInt n, RtToken tokens[],
                                 RtPointer params[]);

        virtual RtVoid RiSubdivisionMeshV(RtToken mask, RtInt nf, RtInt nverts[],
                                          RtInt verts[], RtInt ntags, RtToken tags[],
                                          RtInt nargs[], RtInt intargs[],
                                          RtFloat floatargs[], RtInt n,
                                          RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiHierarchicalSubdivisionMeshV(
            RtToken scheme, RtInt nf, RtInt nverts[], RtInt verts[], RtInt ntags,
            RtToken tags[], RtInt nargs[], RtInt intargs[], RtFloat floatargs[],
            RtString stringargs[], RtInt n, RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiProcedural(RtPointer data, RtBound bound,
                                    RtVoid (*subdivfunc)(RtPointer, RtFloat),
                                    RtVoid (*freefunc)(RtPointer));
        virtual RtVoid RiProcedural(RtToken type, RtInt n, RtToken tokens[],
                                    RtBound bound); // extend
        virtual RtVoid RiProceduralV(RtToken type, RtInt n0, RtToken tokens0[],
                                     RtBound bound, RtInt n, RtToken tokens[],
                                     RtPointer params[]);

        virtual RtVoid RiGeometryV(RtToken type, RtInt n, RtToken tokens[],
                                   RtPointer params[]);
        virtual RtVoid RiSolidBegin(RtToken operation);
        virtual RtVoid RiSolidEnd(void);
        virtual RtObjectHandle RiObjectBegin(void);
        virtual RtVoid RiObjectEnd(void);

        virtual RtVoid RiObjectInstance(RtInt id);
        virtual RtVoid RiObjectInstance(RtToken name);
        // virtual RtVoid RiObjectInstance(RtObjectHandle handle);

        virtual RtVoid RiMotionBeginV(RtInt n, RtFloat times[]);
        virtual RtVoid RiMotionEnd(void);

        virtual RtVoid RiMakeTextureV(char* pic, char* tex, RtToken swrap,
                                      RtToken twrap, RtToken filterfunc,
                                      RtFloat swidth, RtFloat twidth, RtInt n,
                                      RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiMakeBumpV(char* pic, char* tex, RtToken swrap, RtToken twrap,
                                   RtToken filterfunc, RtFloat swidth, RtFloat twidth,
                                   RtInt n, RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiMakeLatLongEnvironmentV(char* pic, char* tex,
                                                 RtToken filterfunc, RtFloat swidth,
                                                 RtFloat twidth, RtInt n,
                                                 RtToken tokens[],
                                                 RtPointer params[]);

        virtual RtVoid
        RiMakeCubeFaceEnvironmentV(char* px, char* nx, char* py, char* ny, char* pz,
                                   char* nz, char* tex, RtFloat fov,
                                   RtToken filterfunc, RtFloat swidth, RtFloat twidth,
                                   RtInt n, RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiMakeShadowV(char* pic, char* tex, RtInt n, RtToken tokens[],
                                     RtPointer params[]);

        virtual RtVoid RiMakeBrickMapV(int nptc, char** ptcnames, char* bkmname,
                                       RtInt n, RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiArchiveRecord(RtToken type, char* format, va_list argptr);

        virtual RtVoid RiReadArchiveV(RtToken name, RtArchiveCallback callback,
                                      RtInt n, RtToken tokens[], RtPointer params[]);
        // virtual RtArchiveHandle RiArchiveBegin ( RtToken archivename, va_list
        // argptr);
        virtual RtArchiveHandle RiArchiveBeginV(RtToken archivename, RtInt n,
                                                RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiArchiveEnd(void);

        virtual RtVoid RiErrorHandler(RtToken name);
        virtual RtVoid RiErrorHandler(RtErrorHandler handler);

        virtual RtVoid RiDepthOfField(RtFloat fstop, RtFloat focallength,
                                      RtFloat focaldistance);

        virtual RtVoid RiMakeOcclusionV(RtInt npics, RtString* picfile,
                                        RtString shadowfile, RtInt count,
                                        RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiShaderLayerV(RtToken type, RtToken name, RtToken layername,
                                      RtInt count, RtToken tokens[],
                                      RtPointer values[]);
        virtual RtVoid RiConnectShaderLayers(RtToken type, RtToken layer1,
                                             RtToken variable1, RtToken layer2,
                                             RtToken variable2);

        virtual RtVoid RiIfBegin(RtString condition);
        virtual RtVoid RiIfEnd();
        virtual RtVoid RiElse();
        virtual RtVoid RiElseIf(RtString condition);

        virtual RtVoid RiProcedural2(RtToken subdivide2func, RtToken boundfunc,
                                     RtInt n, RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiSystem(RtString cmd);
        virtual RtVoid RiVolume(RtPointer type, RtBound bounds, RtInt nvertices[]);
        virtual RtVoid RiVolumePixelSamples(RtFloat xsamples, RtFloat ysamples);
        virtual RtVoid RiVPAtmosphereV(RtToken shadername, RtInt n, RtToken tokens[],
                                       RtPointer params[]);
        virtual RtVoid RiVPInteriorV(RtToken shadername, RtInt n, RtToken tokens[],
                                     RtPointer params[]);
        virtual RtVoid RiVPSurfaceV(RtToken shadername, RtInt n, RtToken tokens[],
                                    RtPointer params[]);

    public:
        virtual RtVoid RiSetLightHandle(RtInt id, RtLightHandle light);
        virtual RtVoid RiSetLightHandle(RtToken name, RtLightHandle light);
        virtual RtVoid RiSetObjectHandle(RtInt id, RtObjectHandle obj);
        virtual RtVoid RiSetObjectHandle(RtToken name, RtObjectHandle obj);

    public:
        ri_stdout_context();
        ri_stdout_context(const ri_classifier& cls);
        ri_stdout_context(const char* filename);
        ri_stdout_context(const ri_classifier& cls, const char* filename);
        ~ri_stdout_context();

    protected:
        void inc_tab();
        void dec_tab();
        void print_tab();
        void print_params(RtInt count, RtToken tokens[], RtPointer values[],
                          RtInt vertex = 1, RtInt varying = 1, RtInt uniform = 1,
                          RtInt facevarying = 1);
        std::string convert_params(RtInt count, RtToken tokens[], RtPointer values[],
                                   RtInt vertex = 1, RtInt varying = 1,
                                   RtInt uniform = 1, RtInt facevarying = 1);
        virtual void begin_function(const char* szFunctionName);
        virtual void end_function(const char* szFunctionName);

    private:
        struct step
        {
            step() {}
            step(int u, int v) : ustep(u), vstep(v) {}
            int ustep;
            int vstep;
        };
        void push(int ustep, int vstep);
        void push();
        void pop();

    protected:
        struct LightSource
        {
            std::string strLight;
            std::string strName;
            std::string strOther;
        };

    private:
        FILE* fp_;
        bool bClose_;
        int nTab_;
        std::vector<int*> m_pObj_;
        std::vector<LightSource*> m_pLight_;
        std::vector<step> m_steps_;
    };
}

#endif
