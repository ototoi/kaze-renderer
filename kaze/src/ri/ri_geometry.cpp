#include "ri_geometry.h"
#include "ri_classifier.h"
#include "ri_triangulation.h"
#include "ri_context.h"
#include "ri_frame_context.h"
#include "ri_frame_task.h"

#include "ri_path_resolver.h"
#include "ri_read_archive.h"
#include "ri_run_program.h"
#include "ri_dynamic_load.h"
#include "ri_evaluate_blobby.h"
#include "ri_convert_bezier.h"
#include "ri_make_normals.h"

#include <assert.h>

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>

#include <limits.h>
#include <stdlib.h>
#endif

#include <memory>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

namespace ri
{

    static int SumArray(const RtInt ar[], int n)
    {
        int nRet = 0;
        for (int i = 0; i < n; i++)
        {
            nRet += ar[i];
        }
        return nRet;
    }

    static int GetSize(int nClass, RtInt vertex = 1, RtInt varying = 1,
                       RtInt uniform = 1, RtInt facevarying = 1)
    {
        switch (nClass)
        {
        case ri_classifier::VERTEX:
            return vertex;
        case ri_classifier::UNIFORM:
            return uniform;
        case ri_classifier::CONSTANT:
            return 1;
        case ri_classifier::VARYING:
            return varying;
        case ri_classifier::FACEVARYING:
            return facevarying;
        }
        return 1;
    }

    static void AddParams(ri_parameters* p_params, ri_classifier* p_cls,
                          RtToken token, RtPointer param, RtInt vertex = 1,
                          RtInt varying = 1, RtInt uniform = 1,
                          RtInt facevarying = 1)
    {
        typedef const char* PCTR;

        int nID = p_cls->def(token);
        if (nID < 0)
            return;
        const ri_classifier::param_data& data = (*p_cls)[nID];
        int nSz = GetSize(data.get_class(), vertex, varying, uniform, facevarying);
        switch (data.get_type())
        {
        case ri_classifier::INTEGER:
            p_params->set(data.get_name(), (const RtInt*)param, data.get_size() * nSz);
            break;
        case ri_classifier::STRING:
            p_params->set(data.get_name(), (const PCTR*)param, data.get_size() * nSz);
            break;
        case ri_classifier::FLOAT:
        case ri_classifier::COLOR:
        case ri_classifier::POINT:
        case ri_classifier::VECTOR:
        case ri_classifier::NORMAL:
        case ri_classifier::HPOINT:
        case ri_classifier::MATRIX:
            p_params->set(data.get_name(), (const RtFloat*)param,
                          data.get_size() * nSz);
            break;
        default:
            break;
        }
    }

    static void AddParams(ri_parameters* p_params, ri_classifier* p_cls,
                          RtInt count, RtToken tokens[], RtPointer params[],
                          RtInt vertex = 1, RtInt varying = 1, RtInt uniform = 1,
                          RtInt facevarying = 1)
    {
        if (count)
        {
            for (int i = 0; i < count; i++)
            {
                AddParams(p_params, p_cls, tokens[i], params[i], vertex, varying, uniform,
                          facevarying);
            }
        }
    }

    static bool IsUniform(int nClass)
    {
        if (nClass == ri_classifier::UNIFORM)
        {
            return true;
        }
        return false;
    }

    static bool IsConstant(int nClass)
    {
        if (nClass == ri_classifier::CONSTANT)
        {
            return true;
        }
        return false;
    }

    static bool IsVarying(int nClass)
    {
        if (nClass == ri_classifier::VARYING)
        {
            return true;
        }
        return false;
    }

    static bool IsFaceVarying(int nClass)
    {
        if (nClass == ri_classifier::FACEVARYING)
        {
            return true;
        }
        return false;
    }

    static bool IsVertex(int nClass)
    {
        if (nClass == ri_classifier::VERTEX)
        {
            return true;
        }
        return false;
    }

    static bool IsInteger(int nType) { return nType == ri_classifier::INTEGER; }

    static bool IsString(int nType) { return nType == ri_classifier::STRING; }

    static bool IsLikeFloat(int nType)
    {
        switch (nType)
        {
        case ri_classifier::FLOAT:
        case ri_classifier::COLOR:
        case ri_classifier::POINT:
        case ri_classifier::VECTOR:
        case ri_classifier::NORMAL:
        case ri_classifier::HPOINT:
        case ri_classifier::MATRIX:
            return true;
        default:
            return false;
        }
        return false;
    }
    //------------------------------------------------------

    const float* ri_base_polygons_geometry::get_P() const
    {
        const ri_parameters::value_type* pv = params_.get("P");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_base_polygons_geometry::get_N() const
    {
        const ri_parameters::value_type* pv = params_.get("N");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_base_polygons_geometry::get_Np() const
    {
        const ri_parameters::value_type* pv = params_.get("Np");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_base_polygons_geometry::get_Cs() const
    {
        const ri_parameters::value_type* pv = params_.get("Cs");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_base_polygons_geometry::get_Os() const
    {
        const ri_parameters::value_type* pv = params_.get("Os");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    //------------------------------------------------------
    // RiPolygon
    ri_polygon_geometry::ri_polygon_geometry(const ri_classifier* p_cls,
                                             RtInt nverts, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        params_.set("nverts", nverts);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, nverts, nverts, 1, 1);
        }
    }
    const float* ri_polygon_geometry::get_P() const
    {
        const ri_parameters::value_type* pv = params_.get("P");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_polygon_geometry::get_Cs() const
    {
        const ri_parameters::value_type* pv = params_.get("Cs");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_polygon_geometry::get_Os() const
    {
        const ri_parameters::value_type* pv = params_.get("Os");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    int ri_polygon_geometry::get_nverts() const
    {
        return (params_.get("nverts"))->integer_values[0];
    }

    //------------------------------------------------------
    // RiGeneralPolygon
    ri_general_polygon_geometry::ri_general_polygon_geometry(
        const ri_classifier* p_cls, RtInt nloops, RtInt nverts[], RtInt n,
        RtToken tokens[], RtPointer params[])
    {
        int nPts = SumArray(nverts, nloops);

        params_.set("nloops", nloops);
        params_.set("nverts", nverts, nloops);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, nPts, nPts, 1, 1);
        }
    }
    const float* ri_general_polygon_geometry::get_P() const
    {
        const ri_parameters::value_type* pv = params_.get("P");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_general_polygon_geometry::get_Cs() const
    {
        const ri_parameters::value_type* pv = params_.get("Cs");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_general_polygon_geometry::get_Os() const
    {
        const ri_parameters::value_type* pv = params_.get("Os");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const int* ri_general_polygon_geometry::get_nverts() const
    {
        return &((params_.get("nverts"))->integer_values[0]);
    }
    int ri_general_polygon_geometry::get_nloops() const
    {
        return (params_.get("nloops"))->integer_values[0];
    }
    //------------------------------------------------------
    // RiPointsPolygons
    ri_points_polygons_geometry::ri_points_polygons_geometry(
        const ri_classifier* p_cls, RtInt npolys, RtInt nverts[], RtInt verts[],
        RtInt n, RtToken tokens[], RtPointer params[])
    {
        int nPts = SumArray(nverts, npolys);
        int psz = 0;
        for (int i = 0; i < nPts; i++)
        {
            if (psz < verts[i])
            {
                psz = verts[i];
            }
        }

        params_.set("npolys", npolys);
        params_.set("nverts", nverts, npolys);
        params_.set("verts", verts, nPts);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, psz + 1, psz + 1, npolys,
                      nPts);
        }
    }
    ri_points_polygons_geometry::ri_points_polygons_geometry(
        const ri_classifier* p_cls, RtInt nverts, RtInt n, RtToken tokens[],
        RtPointer params[])
    {
        int npolys = 1;
        RtInt nverts_[] = {nverts};
        std::vector<RtInt> verts_(nverts);
        for (int i = 0; i < nverts; i++)
            verts_[i] = i;

        int nPts = nverts;
        int psz = 0;
        for (int i = 0; i < nPts; i++)
        {
            if (psz < verts_[i])
            {
                psz = verts_[i];
            }
        }

        params_.set("npolys", npolys);
        params_.set("nverts", nverts_, npolys);
        params_.set("verts", &verts_[0], nverts);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, psz + 1, psz + 1, npolys,
                      nPts);
        }
    }

    const float* ri_points_polygons_geometry::get_P() const
    {
        const ri_parameters::value_type* pv = params_.get("P");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_points_polygons_geometry::get_Cs() const
    {
        const ri_parameters::value_type* pv = params_.get("Cs");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_points_polygons_geometry::get_Os() const
    {
        const ri_parameters::value_type* pv = params_.get("Os");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    int ri_points_polygons_geometry::get_npolys() const
    {
        return (params_.get("npolys"))->integer_values[0];
    }
    const int* ri_points_polygons_geometry::get_nverts() const
    {
        return &((params_.get("nverts"))->integer_values[0]);
    }
    const int* ri_points_polygons_geometry::get_verts() const
    {
        return &((params_.get("verts"))->integer_values[0]);
    }

    void ri_points_polygons_geometry::make_normals()
    {
        int npolys = this->get_npolys();
        const int* nverts = this->get_nverts();
        const int* verts = this->get_verts();
        const float* P = this->get_P();

        if (params_.get("N") || params_.get("Nf") || params_.get("Np"))
        {
            ; //
        }
        else if (P)
        {
            std::string type;
            if (get_attributes().get("ShadingInterpolation", "type", type))
            {
                float angle = 30.0f;
                if (type == RI_CONSTANT)
                {
                    angle = 0.0f;
                }
                else
                {
                    float t = angle;
                    if (get_attributes().get("ShadingInterpolation", "smoothangle", t))
                    {
                        if (0.0f <= t && t <= 360.0f)
                        {
                            angle = t;
                        }
                    }
                }

                std::vector<float> N;
                if (0 == ri_make_normals(N, npolys, nverts, verts, P, angle))
                {
                    clsf_.def("facevarying normal N");
                    params_.set("N", &N[0], (int)N.size());
                }
            }
        }
    }
    
    //------------------------------------------------------
    // RiPointsGeneralPolygons
    ri_points_general_polygons_geometry::ri_points_general_polygons_geometry(
        const ri_classifier* p_cls, RtInt npolys, RtInt nloops[], RtInt nverts[],
        RtInt verts[], RtInt n, RtToken tokens[], RtPointer params[])
    {
        int nVts = SumArray(nloops, npolys);
        int nv = SumArray(nverts, nVts);
        int psz = 0;
        for (int i = 0; i < nv; i++)
        {
            if (psz < verts[i])
            {
                psz = verts[i];
            }
        }

        params_.set("npolys", npolys);
        params_.set("nloops", nloops, npolys);
        params_.set("nverts", nverts, nVts);
        params_.set("verts", verts, nv);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, psz + 1, psz + 1, npolys, nv);
        }
    }
    const float* ri_points_general_polygons_geometry::get_P() const
    {
        const ri_parameters::value_type* pv = params_.get("P");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_points_general_polygons_geometry::get_Cs() const
    {
        const ri_parameters::value_type* pv = params_.get("Cs");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_points_general_polygons_geometry::get_Os() const
    {
        const ri_parameters::value_type* pv = params_.get("Os");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    int ri_points_general_polygons_geometry::get_npolys() const
    {
        return (params_.get("npolys"))->integer_values[0];
    }
    const int* ri_points_general_polygons_geometry::get_nloops() const
    {
        return &((params_.get("nloops"))->integer_values[0]);
    }
    const int* ri_points_general_polygons_geometry::get_nverts() const
    {
        return &((params_.get("nverts"))->integer_values[0]);
    }
    const int* ri_points_general_polygons_geometry::get_verts() const
    {
        return &((params_.get("verts"))->integer_values[0]);
    }
    //------------------------------------------------------
    struct T2I
    {
        const char* name;
        int num;
    };

    static T2I ORDERS[] = {{RI_BILINEAR, 2}, {RI_BIQUADRATIC, 3}, {RI_BICUBIC, 4}, {RI_LINEAR, 2}, {RI_QUADRATIC, 3}, {RI_CUBIC, 4}, {NULL, 0}};

    static int GetOrder(const char* type)
    {
        int i = 0;
        while (ORDERS[i].name)
        {
            if (strcmp(ORDERS[i].name, type) == 0)
                return ORDERS[i].num;
            i++;
        }
        return 0;
    }

    //------------------------------------------------------
    // RiPatch
    ri_patch_geometry::ri_patch_geometry(const ri_classifier* p_cls, RtToken type,
                                         RtInt n, RtToken tokens[],
                                         RtPointer params[])
    {
        int nv = GetOrder(type);
        if (nv)
        {
            params_.set("type", type);
            if (n)
            {
                ri_saving_classifier cls(p_cls, &(this->clsf_));
                AddParams(&params_, &cls, n, tokens, params, nv * nv,
                          4); // vertex varying uniform facevarying
            }
        }
    }
    const char* ri_patch_geometry::get_type() const
    {
        return (params_.get("type"))->string_values[0].c_str();
    }
    const float* ri_patch_geometry::get_P() const
    {
        const ri_parameters::value_type* pv = params_.get("P");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_patch_geometry::get_Cs() const
    {
        const ri_parameters::value_type* pv = params_.get("Cs");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_patch_geometry::get_Os() const
    {
        const ri_parameters::value_type* pv = params_.get("Os");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_patch_geometry::get_Pw() const
    {
        const ri_parameters::value_type* pv = params_.get("Pw");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    std::string ri_patch_geometry::get_ubasis() const
    {
        std::string str;
        attr_.get("Basis", "ubasis", str);
        return str;
    }
    std::string ri_patch_geometry::get_vbasis() const
    {
        std::string str;
        attr_.get("Basis", "vbasis", str);
        return str;
    }

    bool ri_patch_geometry::is_linear() const
    {
        int nOrder = GetOrder((RtToken) this->get_type());
        return nOrder == 2;
    }

    bool ri_patch_geometry::is_cubic() const
    {
        int nOrder = GetOrder((RtToken) this->get_type());
        return nOrder == 4;
    }

    bool ri_patch_geometry::is_rational() const
    {
        if (this->get_P())
            return false;
        if (this->get_Pw())
            return true;
        return false;
    }

    //------------------------------------------------------
    // RiPatchMesh
    ri_patch_mesh_geometry::ri_patch_mesh_geometry(
        const ri_classifier* p_cls, RtInt ustep, RtInt vstep, RtToken type,
        RtInt nu, RtToken uwrap, RtInt nv, RtToken vwrap, RtInt n, RtToken tokens[],
        RtPointer params[])
    {
        int nOrder = GetOrder(type);
        int nupatch = 1;
        int nvpatch = 1;
        int nverts = 0;
        if (nOrder == 2)
        { // linear
            ustep = 1;
            vstep = 1;
            if (strcmp(uwrap, RI_PERIODIC) == 0)
            { // loop
                nupatch = nu;
            }
            else if (strcmp(uwrap, RI_NONPERIODIC) == 0)
            { // open
                nupatch = nu - 1;
            }

            if (strcmp(vwrap, RI_PERIODIC) == 0)
            { // loop
                nvpatch = nv;
            }
            else if (strcmp(vwrap, RI_NONPERIODIC) == 0)
            { // open
                nvpatch = nv - 1;
            }
            nverts = nu * nv;
        }
        else if (nOrder == 4)
        { // cubic
            RtInt nustep = ustep;
            RtInt nvstep = vstep;

            int nnu = 1;
            int nnv = 1;

            if (strcmp(uwrap, RI_PERIODIC) == 0)
            {
                nupatch = nu / nustep;
                nnu = nupatch;
            }
            else if (strcmp(uwrap, RI_NONPERIODIC) == 0)
            {
                nupatch = (nu - 4) / nustep + 1;
                nnu = nupatch + 1;
            }

            if (strcmp(vwrap, RI_PERIODIC) == 0)
            {
                nvpatch = nv / nvstep;
                nnv = nvpatch;
            }
            else if (strcmp(vwrap, RI_NONPERIODIC) == 0)
            {
                nvpatch = (nv - 4) / nvstep + 1;
                nnv = nvpatch + 1;
            }

            nverts = nnu * nnv;
        }

        params_.set("type", type);
        params_.set("nu", nu);
        params_.set("nv", nv);
        params_.set("uwrap", uwrap);
        params_.set("vwrap", vwrap);

        params_.set("ustep", ustep);
        params_.set("vstep", vstep);

        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, nu * nv, nverts,
                      nupatch * nvpatch);
        }
    }
    const char* ri_patch_mesh_geometry::get_type() const
    {
        return (params_.get("type"))->string_values[0].c_str();
    }
    int ri_patch_mesh_geometry::get_nu() const
    {
        return (params_.get("nu"))->integer_values[0];
    }
    int ri_patch_mesh_geometry::get_nv() const
    {
        return (params_.get("nv"))->integer_values[0];
    }
    const char* ri_patch_mesh_geometry::get_uwrap() const
    {
        return (params_.get("uwrap"))->string_values[0].c_str();
    }
    const char* ri_patch_mesh_geometry::get_vwrap() const
    {
        return (params_.get("vwrap"))->string_values[0].c_str();
    }

    const float* ri_patch_mesh_geometry::get_P() const
    {
        const ri_parameters::value_type* pv = params_.get("P");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_patch_mesh_geometry::get_Cs() const
    {
        const ri_parameters::value_type* pv = params_.get("Cs");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_patch_mesh_geometry::get_Os() const
    {
        const ri_parameters::value_type* pv = params_.get("Os");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_patch_mesh_geometry::get_Pw() const
    {
        const ri_parameters::value_type* pv = params_.get("Pw");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    std::string ri_patch_mesh_geometry::get_ubasis() const
    {
        std::string str;
        attr_.get("Basis", "ubasis", str);
        return str;
    }
    std::string ri_patch_mesh_geometry::get_vbasis() const
    {
        std::string str;
        attr_.get("Basis", "vbasis", str);
        return str;
    }
    int ri_patch_mesh_geometry::get_ustep() const
    {
        return (params_.get("ustep"))->integer_values[0];
    }
    int ri_patch_mesh_geometry::get_vstep() const
    {
        return (params_.get("vstep"))->integer_values[0];
    }

    bool ri_patch_mesh_geometry::is_linear() const
    {
        int nOrder = GetOrder((RtToken) this->get_type());
        return nOrder == 2;
    }

    bool ri_patch_mesh_geometry::is_cubic() const
    {
        int nOrder = GetOrder((RtToken) this->get_type());
        return nOrder == 4;
    }

    bool ri_patch_mesh_geometry::is_rational() const
    {
        if (this->get_P())
            return false;
        if (this->get_Pw())
            return true;
        return false;
    }
    //------------------------------------------------------
    // RiNuPatch
    ri_nu_patch_geometry::ri_nu_patch_geometry(
        const ri_classifier* p_cls, RtInt nu, RtInt uorder, RtFloat uknot[],
        RtFloat umin, RtFloat umax, RtInt nv, RtInt vorder, RtFloat vknot[],
        RtFloat vmin, RtFloat vmax, RtInt n, RtToken tokens[], RtPointer params[])
    {
        params_.set("nu", nu);
        params_.set("uorder", uorder);
        params_.set("uknot", uknot, nu + uorder);
        params_.set("umin", umin);
        params_.set("umax", umax);
        params_.set("nv", nv);
        params_.set("vorder", vorder);
        params_.set("vknot", vknot, nv + vorder);
        params_.set("vmin", vmin);
        params_.set("vmax", vmax);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, nu * nv,
                      (2 + nu - uorder) * (2 + nv - vorder),
                      (1 + nu - uorder) * (1 + nv - vorder));
        }
    }
    int ri_nu_patch_geometry::get_nu() const
    {
        return (params_.get("nu"))->integer_values[0];
    }
    int ri_nu_patch_geometry::get_nv() const
    {
        return (params_.get("nv"))->integer_values[0];
    }
    int ri_nu_patch_geometry::get_uorder() const
    {
        return (params_.get("uorder"))->integer_values[0];
    }
    int ri_nu_patch_geometry::get_vorder() const
    {
        return (params_.get("vorder"))->integer_values[0];
    }
    const std::vector<float>& ri_nu_patch_geometry::get_uknot() const
    {
        return (params_.get("uknot"))->float_values;
    }
    const std::vector<float>& ri_nu_patch_geometry::get_vknot() const
    {
        return (params_.get("vknot"))->float_values;
    }
    float ri_nu_patch_geometry::get_umin() const
    {
        return (params_.get("umin"))->float_values[0];
    }
    float ri_nu_patch_geometry::get_umax() const
    {
        return (params_.get("umax"))->float_values[0];
    }
    float ri_nu_patch_geometry::get_vmin() const
    {
        return (params_.get("vmin"))->float_values[0];
    }
    float ri_nu_patch_geometry::get_vmax() const
    {
        return (params_.get("vmax"))->float_values[0];
    }
    const float* ri_nu_patch_geometry::get_Pw() const
    {
        const ri_parameters::value_type* pv = params_.get("Pw");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_nu_patch_geometry::get_Cs() const
    {
        const ri_parameters::value_type* pv = params_.get("Cs");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_nu_patch_geometry::get_Os() const
    {
        const ri_parameters::value_type* pv = params_.get("Os");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_nu_patch_geometry::get_P() const
    {
        const ri_parameters::value_type* pv = params_.get("P");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    bool ri_nu_patch_geometry::is_rational() const
    {
        if (this->get_P())
            return false;
        if (this->get_Pw())
            return true;
        return false;
    }

    bool ri_nu_patch_geometry::is_noweight_cp() const
    {
        int nNurbs = 0;
        if (this->attr_.get("nu_patch", "noweight", nNurbs))
        {
            if (nNurbs)
                return true;
        }
        return false;
    }
    //------------------------------------------------------
    // RiTrimCurve
    ri_trim_curve_geometry::ri_trim_curve_geometry(const ri_classifier* p_cls,
                                                   RtInt nloops, RtInt ncurves[],
                                                   RtInt order[], RtFloat knot[],
                                                   RtFloat min[], RtFloat max[],
                                                   RtInt n[], RtFloat u[],
                                                   RtFloat v[], RtFloat w[])
    {
        int ttlc = SumArray(ncurves, nloops);
        int nbcoords = 0;
        int knotsize = 0;
        for (int i = 0; i < ttlc; i++)
        {
            nbcoords += n[i];
            knotsize += order[i] + n[i];
        }

        params_.set("nloops", nloops);
        params_.set("ncurves", ncurves, nloops);
        params_.set("order", order, ttlc);
        params_.set("knot", knot, knotsize);
        params_.set("min", min, ttlc);
        params_.set("max", max, ttlc);
        params_.set("n", n, ttlc);
        params_.set("u", u, nbcoords);
        params_.set("v", v, nbcoords);
        params_.set("w", w, nbcoords);
    }

    int ri_trim_curve_geometry::get_nloops() const
    {
        return (params_.get("nloops"))->integer_values[0];
    }
    const int* ri_trim_curve_geometry::get_ncurves() const
    {
        return &((params_.get("ncurves"))->integer_values[0]);
    }
    const int* ri_trim_curve_geometry::get_order() const
    {
        return &((params_.get("order"))->integer_values[0]);
    }
    const float* ri_trim_curve_geometry::get_knot() const
    {
        return &((params_.get("knot"))->float_values[0]);
    }
    const float* ri_trim_curve_geometry::get_min() const
    {
        return &((params_.get("min"))->float_values[0]);
    }
    const float* ri_trim_curve_geometry::get_max() const
    {
        return &((params_.get("max"))->float_values[0]);
    }
    const int* ri_trim_curve_geometry::get_n() const
    {
        return &((params_.get("n"))->integer_values[0]);
    }
    const float* ri_trim_curve_geometry::get_u() const
    {
        return &((params_.get("u"))->float_values[0]);
    }
    const float* ri_trim_curve_geometry::get_v() const
    {
        return &((params_.get("v"))->float_values[0]);
    }
    const float* ri_trim_curve_geometry::get_w() const
    {
        return &((params_.get("w"))->float_values[0]);
    }

    bool ri_trim_curve_geometry::is_outer() const { return !is_inner(); }

    bool ri_trim_curve_geometry::is_inner() const
    {
        //"trimcurve", "sense"
        std::string sense;
        if (this->attr_.get("trimcurve", "sense", sense))
        {
            if (sense == "outside")
                return true;
        }
        return false;
    }

    bool ri_trim_curve_geometry::is_rational() const
    {
        int nNurbs = 0;
        if (this->attr_.get("trimcurve", "rational", nNurbs))
        {
            if (nNurbs)
                return true;
        }
        return false;
    }

    bool ri_trim_curve_geometry::is_noweight_cp() const
    {
        int nNurbs = 0;
        if (this->attr_.get("trimcurve", "noweight", nNurbs))
        {
            if (nNurbs)
                return true;
        }
        return false;
    }

    //------------------------------------------------------
    // RiSphere
    ri_sphere_geometry::ri_sphere_geometry(const ri_classifier* p_cls,
                                           RtFloat radius, RtFloat zmin,
                                           RtFloat zmax, RtFloat tmax, RtInt n,
                                           RtToken tokens[], RtPointer params[])
    {
        params_.set("radius", radius);
        params_.set("zmin", zmin);
        params_.set("zmax", zmax);
        params_.set("tmax", tmax);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, 4, 4);
        }
    }
    float ri_sphere_geometry::get_radius() const
    {
        return (params_.get("radius"))->float_values[0];
    }
    float ri_sphere_geometry::get_zmin() const
    {
        return (params_.get("zmin"))->float_values[0];
    }
    float ri_sphere_geometry::get_zmax() const
    {
        return (params_.get("zmax"))->float_values[0];
    }
    float ri_sphere_geometry::get_tmax() const
    {
        return (params_.get("tmax"))->float_values[0];
    }
    //------------------------------------------------------
    // RiCone
    ri_cone_geometry::ri_cone_geometry(const ri_classifier* p_cls, RtFloat height,
                                       RtFloat radius, RtFloat tmax, RtInt n,
                                       RtToken tokens[], RtPointer params[])
    {
        params_.set("height", height);
        params_.set("radius", radius);
        params_.set("tmax", tmax);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, 4, 4);
        }
    }
    float ri_cone_geometry::get_height() const
    {
        return (params_.get("height"))->float_values[0];
    }
    float ri_cone_geometry::get_radius() const
    {
        return (params_.get("radius"))->float_values[0];
    }
    float ri_cone_geometry::get_tmax() const
    {
        return (params_.get("tmax"))->float_values[0];
    }
    //------------------------------------------------------
    // RiCylinder
    ri_cylinder_geometry::ri_cylinder_geometry(const ri_classifier* p_cls,
                                               RtFloat radius, RtFloat zmin,
                                               RtFloat zmax, RtFloat tmax, RtInt n,
                                               RtToken tokens[],
                                               RtPointer params[])
    {
        params_.set("radius", radius);
        params_.set("zmin", zmin);
        params_.set("zmax", zmax);
        params_.set("tmax", tmax);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, 4, 4);
        }
    }
    float ri_cylinder_geometry::get_radius() const
    {
        return (params_.get("radius"))->float_values[0];
    }
    float ri_cylinder_geometry::get_zmin() const
    {
        return (params_.get("zmin"))->float_values[0];
    }
    float ri_cylinder_geometry::get_zmax() const
    {
        return (params_.get("zmax"))->float_values[0];
    }
    float ri_cylinder_geometry::get_tmax() const
    {
        return (params_.get("tmax"))->float_values[0];
    }
    //------------------------------------------------------
    // RiHyperboloid
    ri_hyperboloid_geometry::ri_hyperboloid_geometry(const ri_classifier* p_cls,
                                                     RtPoint point1, RtPoint point2,
                                                     RtFloat tmax, RtInt n,
                                                     RtToken tokens[],
                                                     RtPointer params[])
    {
        params_.set("point1", point1, 3);
        params_.set("point2", point2, 3);
        params_.set("tmax", tmax);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, 4, 4);
        }
    }
    const float* ri_hyperboloid_geometry::get_point1() const
    {
        return &((params_.get("point1"))->float_values[0]);
    }
    const float* ri_hyperboloid_geometry::get_point2() const
    {
        return &((params_.get("point2"))->float_values[0]);
    }
    float ri_hyperboloid_geometry::get_tmax() const
    {
        return (params_.get("tmax"))->float_values[0];
    }
    //------------------------------------------------------
    // RiParaboloid
    ri_paraboloid_geometry::ri_paraboloid_geometry(const ri_classifier* p_cls,
                                                   RtFloat rmax, RtFloat zmin,
                                                   RtFloat zmax, RtFloat tmax,
                                                   RtInt n, RtToken tokens[],
                                                   RtPointer params[])
    {
        params_.set("rmax", rmax);
        params_.set("zmin", zmin);
        params_.set("zmax", zmax);
        params_.set("tmax", tmax);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, 4, 4);
        }
    }
    float ri_paraboloid_geometry::get_rmax() const
    {
        return (params_.get("rmax"))->float_values[0];
    }
    float ri_paraboloid_geometry::get_zmin() const
    {
        return (params_.get("zmin"))->float_values[0];
    }
    float ri_paraboloid_geometry::get_zmax() const
    {
        return (params_.get("zmax"))->float_values[0];
    }
    float ri_paraboloid_geometry::get_tmax() const
    {
        return (params_.get("tmax"))->float_values[0];
    }
    //------------------------------------------------------
    // RiDisk
    ri_disk_geometry::ri_disk_geometry(const ri_classifier* p_cls, RtFloat height,
                                       RtFloat radius, RtFloat tmax, RtInt n,
                                       RtToken tokens[], RtPointer params[])
    {
        params_.set("height", height);
        params_.set("radius", radius);
        params_.set("tmax", tmax);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, 4, 4);
        }
    }
    float ri_disk_geometry::get_height() const
    {
        return (params_.get("height"))->float_values[0];
    }
    float ri_disk_geometry::get_radius() const
    {
        return (params_.get("radius"))->float_values[0];
    }
    float ri_disk_geometry::get_tmax() const
    {
        return (params_.get("tmax"))->float_values[0];
    }
    //------------------------------------------------------
    // RiTorus
    ri_torus_geometry::ri_torus_geometry(const ri_classifier* p_cls, RtFloat majrad,
                                         RtFloat minrad, RtFloat phimin,
                                         RtFloat phimax, RtFloat tmax, RtInt n,
                                         RtToken tokens[], RtPointer params[])
    {
        params_.set("majrad", majrad);
        params_.set("minrad", minrad);
        params_.set("phimin", phimin);
        params_.set("phimax", phimax);
        params_.set("tmax", tmax);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, 4, 4);
        }
    }
    float ri_torus_geometry::get_majrad() const
    {
        return (params_.get("majrad"))->float_values[0];
    }
    float ri_torus_geometry::get_minrad() const
    {
        return (params_.get("minrad"))->float_values[0];
    }
    float ri_torus_geometry::get_phimin() const
    {
        return (params_.get("phimin"))->float_values[0];
    }
    float ri_torus_geometry::get_phimax() const
    {
        return (params_.get("phimax"))->float_values[0];
    }
    float ri_torus_geometry::get_tmax() const
    {
        return (params_.get("tmax"))->float_values[0];
    }
    //------------------------------------------------------
    // RiBlobby
    ri_blobby_geometry::ri_blobby_geometry(const ri_classifier* p_cls, RtInt nleaf,
                                           RtInt ncode, RtInt code[], RtInt nflt,
                                           RtFloat flt[], RtInt nstr, RtToken str[],
                                           RtInt n, RtToken tokens[],
                                           RtPointer params[])
    {
        static const float DEFAULT_THRETHOLD = 0.421875f; // aka. F(0.5) = 0.421875
        params_.set("nleaf", nleaf);
        params_.set("ncode", ncode);
        params_.set("code", code, ncode);
        params_.set("nflt", nflt);
        params_.set("flt", flt, nflt);
        params_.set("nstr", nstr);
        params_.set("str", str, nstr);
        params_.set("__threthold", DEFAULT_THRETHOLD);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, nleaf, nleaf);
        }
    }

    int ri_blobby_geometry::get_nleaf() const
    {
        return (params_.get("nleaf"))->integer_values[0];
    }
    int ri_blobby_geometry::get_ncode() const
    {
        return (params_.get("ncode"))->integer_values[0];
    }
    const int* ri_blobby_geometry::get_code() const
    {
        return &((params_.get("code"))->integer_values[0]);
    }
    int ri_blobby_geometry::get_nflt() const
    {
        return (params_.get("nflt"))->integer_values[0];
    }
    const float* ri_blobby_geometry::get_flt() const
    {
        return &((params_.get("flt"))->float_values[0]);
    }
    int ri_blobby_geometry::get_nstr() const
    {
        return (params_.get("nstr"))->integer_values[0];
    }
    const std::string* ri_blobby_geometry::get_str() const
    {
        if (this->get_nstr())
        {
            return &((params_.get("str"))->string_values[0]);
        }
        else
        {
            static std::string dummy;
            return &dummy;
        }
    }
    float ri_blobby_geometry::get_threthold() const
    {
        return (params_.get("__threthold"))->float_values[0];
    }

    //------------------------------------------------------
    // RiCurves
    ri_curves_geometry::ri_curves_geometry(const ri_classifier* p_cls, RtInt step,
                                           RtToken type, RtInt ncurves,
                                           RtInt nvertices[], RtToken wrap, RtInt n,
                                           RtToken tokens[], RtPointer params[])
    {
        int vn = SumArray(nvertices, ncurves);

        int nOrder = GetOrder(type);
        int nseg = 0;
        int ii = 0;
        if (nOrder == 2)
        { // linear
            if (strcmp(wrap, RI_PERIODIC) == 0)
            { // loop
                nseg = vn;
                ii = vn;
            }
            else if (strcmp(wrap, RI_NONPERIODIC) == 0)
            { // open
                nseg = vn - ncurves;
                ii = vn;
            }
        }
        else if (nOrder == 4)
        { // cubic
            if (strcmp(wrap, RI_PERIODIC) == 0)
            {
                nseg = 0;
                ii = 0;
                for (int i = 0; i < ncurves; i++)
                {
                    int ns = nvertices[i] / step;
                    nseg += ns;
                    ii += ns;
                }
            }
            else if (strcmp(wrap, RI_NONPERIODIC) == 0)
            {
                nseg = 0;
                ii = 0;
                for (int i = 0; i < ncurves; i++)
                {
                    int ns = (nvertices[i] - 4) / step + 1;
                    nseg += ns;
                    ii += ns + 1;
                }
            }
        }
        else
        {
            nOrder = 0;
        }

        params_.set("type", type);
        params_.set("ncurves", ncurves);
        params_.set("nvertices", nvertices, ncurves);
        params_.set("wrap", wrap);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, vn, ii,
                      ncurves); // vertex varying uniform facevarying
        }
    }

    int ri_curves_geometry::get_ncurves() const
    {
        return (params_.get("ncurves"))->integer_values[0];
    }

    const int* ri_curves_geometry::get_nvertices() const
    {
        return &((params_.get("nvertices"))->integer_values[0]);
    }

    const char* ri_curves_geometry::get_type() const
    {
        return (params_.get("type"))->string_values[0].c_str();
    }

    const char* ri_curves_geometry::get_wrap() const
    {
        return (params_.get("wrap"))->string_values[0].c_str();
    }

    const float* ri_curves_geometry::get_P() const
    {
        const ri_parameters::value_type* pv = params_.get("P");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    const float* ri_curves_geometry::get_N() const
    {
        const ri_parameters::value_type* pv = params_.get("N");
        if (pv)
        {
            if (is_vertex("N"))
                return NULL;
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    const float* ri_curves_geometry::get_Pw() const
    {
        const ri_parameters::value_type* pv = params_.get("Pw");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    const float* ri_curves_geometry::get_width() const
    {
        const ri_parameters::value_type* pv = params_.get("width");
        if (pv)
        {
            if (is_vertex("width"))
                return NULL;
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_curves_geometry::get_Cs() const
    {
        const ri_parameters::value_type* pv = params_.get("Cs");
        if (pv)
        {
            if (is_vertex("Cs"))
                return NULL;
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_curves_geometry::get_Os() const
    {
        const ri_parameters::value_type* pv = params_.get("Os");
        if (pv)
        {
            if (is_vertex("Os"))
                return NULL;
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    
    const float* ri_curves_geometry::get_Nv() const
    {
        const ri_parameters::value_type* pv = params_.get("N");
        if (pv)
        {
            if (!is_vertex("N"))
                return NULL;
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_curves_geometry::get_widthv() const
    {
        const ri_parameters::value_type* pv = params_.get("width");
        if (pv)
        {
            if (!is_vertex("width"))
                return NULL;
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_curves_geometry::get_Csv() const
    {
        const ri_parameters::value_type* pv = params_.get("Cs");
        if (pv)
        {
            if (!is_vertex("Cs"))
                return NULL;
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_curves_geometry::get_Osv() const
    {
        const ri_parameters::value_type* pv = params_.get("Os");
        if (pv)
        {
            if (!is_vertex("Os"))
                return NULL;
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    float ri_curves_geometry::get_constantwidth() const
    {
        const ri_parameters::value_type* pv = params_.get("constantwidth");
        if (pv)
        {
            return pv->float_values[0];
        }
        return 1.0f;
    }

    const float* ri_curves_geometry::get_constantnormal() const
    {
        const ri_parameters::value_type* pv = params_.get("constantnormal");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    std::string ri_curves_geometry::get_vbasis() const
    {
        std::string str;
        attr_.get("Basis", "vbasis", str);
        return str;
    }
    int ri_curves_geometry::get_vstep() const
    {
        int nStep = 0;
        attr_.get("Basis", "vstep", nStep);
        return nStep;
    }

    bool ri_curves_geometry::is_vertex(const char* key) const
    {
        int id = get_classifier().find(key);
        if (id >= 0)
        {
            const ri_classifier::param_data& data = get_classifier()[id];
            if (IsVertex(data.get_class()))
                return true;
        }
        return false;
    }

    bool ri_curves_geometry::is_linear() const
    {
        int nOrder = GetOrder((RtToken) this->get_type());
        return nOrder == 2;
    }

    bool ri_curves_geometry::is_cubic() const
    {
        int nOrder = GetOrder((RtToken) this->get_type());
        return nOrder == 4;
    }

    //------------------------------------------------------
    // RiPoints
    ri_points_geometry::ri_points_geometry(const ri_classifier* p_cls, RtInt nverts,
                                           RtInt n, RtToken tokens[],
                                           RtPointer params[])
    {
        params_.set("nverts", nverts);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, nverts,
                      nverts); // vertex, varying
        }
    }
    const float* ri_points_geometry::get_P() const
    {
        const ri_parameters::value_type* pv = params_.get("P");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_points_geometry::get_width() const
    {
        const ri_parameters::value_type* pv = params_.get("width");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_points_geometry::get_Cs() const
    {
        const ri_parameters::value_type* pv = params_.get("Cs");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_points_geometry::get_Os() const
    {
        const ri_parameters::value_type* pv = params_.get("Os");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    float ri_points_geometry::get_constantwidth() const
    {
        const ri_parameters::value_type* pv = params_.get("constantwidth");
        if (pv)
        {
            return pv->float_values[0];
        }
        return 1.0f;
    }
    int ri_points_geometry::get_nverts() const
    {
        return (params_.get("nverts"))->integer_values[0];
    }

    const float* ri_points_geometry::get_N() const
    {
        const ri_parameters::value_type* pv = params_.get("N");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    const float* ri_points_geometry::get_constantnormal() const
    {
        const ri_parameters::value_type* pv = params_.get("constantnormal");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    const char* ri_points_geometry::get_type() const
    {
        const ri_parameters::value_type* pv = params_.get("type");
        if (pv)
        {
            return pv->string_values[0].c_str();
        }
        return NULL;
    }

    //------------------------------------------------------
    // RiSubdivisionMesh
    ri_subdivision_mesh_geometry::ri_subdivision_mesh_geometry(
        const ri_classifier* p_cls, RtToken mask, RtInt nf, RtInt nverts[],
        RtInt verts[], RtInt ntags, RtToken tags[], RtInt nargs[], RtInt intargs[],
        RtFloat floatargs[], RtInt n, RtToken tokens[], RtPointer params[])
    {
        int nv = SumArray(nverts, nf);

        RtInt isz = 0;
        RtInt fsz = 0;
        for (int i = 0; i < ntags * 2; i++)
        {
            if (i % 2 == 0)
                isz += nargs[i];
            else
                fsz += nargs[i];
        }

        RtInt psz = 0;
        for (int i = 0; i < nv; i++)
        {
            if (psz < verts[i])
                psz = verts[i];
        }

        params_.set("mask", mask);
        params_.set("nf", nf);
        params_.set("npolys", nf);
        params_.set("nverts", nverts, nf);
        params_.set("verts", verts, nv);
        params_.set("ntags", ntags);

        params_.set("tags", tags, ntags);
        params_.set("nargs", nargs, ntags * 2);
        params_.set("intargs", intargs, isz);
        params_.set("floatargs", floatargs, fsz);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params, psz + 1, psz + 1, nf, nv);
        }
    }

    const float* ri_subdivision_mesh_geometry::get_P() const
    {
        const ri_parameters::value_type* pv = params_.get("P");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    const float* ri_subdivision_mesh_geometry::get_N() const
    {
        const ri_parameters::value_type* pv = params_.get("N");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    const float* ri_subdivision_mesh_geometry::get_Cs() const
    {
        const ri_parameters::value_type* pv = params_.get("Cs");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_subdivision_mesh_geometry::get_Os() const
    {
        const ri_parameters::value_type* pv = params_.get("Os");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    bool ri_subdivision_mesh_geometry::is_facevarying(const char* key) const
    {
        int id = this->get_classifier().find(key);
        if (id >= 0)
        {
            const ri_classifier::param_data& data = this->get_classifier()[id];
            if (IsFaceVarying(data.get_class()))
                return true;
        }
        return false;
    }

    const float* ri_subdivision_mesh_geometry::get_st() const
    {
        if (!is_facevarying("st"))
        {
            const ri_parameters::value_type* pv = params_.get("st");
            if (pv)
            {
                return &(pv->float_values[0]);
            }
        }
        return NULL;
    }

    const float* ri_subdivision_mesh_geometry::get_stf() const
    {
        if (is_facevarying("st"))
        {
            const ri_parameters::value_type* pv = params_.get("st");
            if (pv)
            {
                return &(pv->float_values[0]);
            }
        }
        return NULL;
    }

    int ri_subdivision_mesh_geometry::get_npolys() const
    {
        return (params_.get("npolys"))->integer_values[0];
    }
    int ri_subdivision_mesh_geometry::get_nf() const { return this->get_npolys(); }
    const int* ri_subdivision_mesh_geometry::get_nverts() const
    {
        return &((params_.get("nverts"))->integer_values[0]);
    }
    const int* ri_subdivision_mesh_geometry::get_verts() const
    {
        return &((params_.get("verts"))->integer_values[0]);
    }
    int ri_subdivision_mesh_geometry::get_ntags() const
    {
        return (params_.get("ntags"))->integer_values[0];
    }
    const std::string* ri_subdivision_mesh_geometry::get_tags() const
    {
        const ri_parameters::value_type* pv = params_.get("tags");
        if (pv && !pv->string_values.empty())
        {
            return &(pv->string_values[0]);
        }
        return NULL;
    }
    const int* ri_subdivision_mesh_geometry::get_nargs() const
    {
        const ri_parameters::value_type* pv = params_.get("nargs");
        if (pv && !pv->integer_values.empty())
        {
            return &(pv->integer_values[0]);
        }
        return NULL;
    }
    const int* ri_subdivision_mesh_geometry::get_intargs() const
    {
        const ri_parameters::value_type* pv = params_.get("intargs");
        if (pv && !pv->integer_values.empty())
        {
            return &(pv->integer_values[0]);
        }
        return NULL;
    }
    const float* ri_subdivision_mesh_geometry::get_floatargs() const
    {
        const ri_parameters::value_type* pv = params_.get("floatargs");
        if (pv && !pv->float_values.empty())
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    //------------------------------------------------------
    // RiProcedural

    ri_procedural_geometry::ri_procedural_geometry() {}

    ri_procedural_geometry::ri_procedural_geometry(const ri_classifier* p_cls,
                                                   RtToken type, RtInt n,
                                                   RtToken data[], RtBound bound)
    {
        if (n)
        {
            std::vector<const char*> tmp(n);
            for (int i = 0; i < n; i++)
            {
                tmp[i] = (const char*)data[i];
            }
            params_.set("data", &tmp[0], n);
        }
        else
        {
            const char* szDummy = "";
            params_.set("data", &szDummy, 0);
        }

        params_.set("type", type);
        params_.set("bound", bound, 6);
    }

    void ri_procedural_geometry::set_attributes(const ri_attributes& attr)
    {
        attr_ = attr;
        attr_.clear_group();
    }

    const char* ri_procedural_geometry::get_type() const
    {
        return (params_.get("type"))->string_values[0].c_str();
    }

    int ri_procedural_geometry::get_n() const
    {
        const ri_parameters::value_type* pv = params_.get("data");
        if (pv)
        {
            return (int)(pv->string_values.size());
        }
        return 0;
    }

    const char* ri_procedural_geometry::get_data_at(int i) const
    {
        int n = this->get_n();
        if (i < 0 || i >= n)
            return NULL;
        return (params_.get("data"))->string_values[i].c_str();
    }

    const float* ri_procedural_geometry::get_bound() const
    {
        const ri_parameters::value_type* pv = params_.get("bound");
        if (pv && !pv->float_values.empty())
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }

    //------------------------------------------------------
    // RiGeometry name is "teapot"
    ri_teapot_geometry::ri_teapot_geometry()
    {
        // assert(0);
    }

    //------------------------------------------------------
    // RiGeometry name is "bunny"
    ri_bunny_geometry::ri_bunny_geometry()
    {
        // assert(0);
    }

    //------------------------------------------------------
    // RiSolid
    ri_solid_geometry::ri_solid_geometry(RtToken operation)
    {
        params_.set("type", operation);
        params_.set("operation", operation);
    }

    const char* ri_solid_geometry::get_operation() const
    {
        return (params_.get("operation"))->string_values[0].c_str();
    }

    //------------------------------------------------------
    // RiMotion
    ri_motion_geometry::ri_motion_geometry(int n, const float times[],
                                           const ri_geometry* const geos[])
    {
        times_.assign(times, times + n);
        geos_.assign(geos, geos + n);
    }

    int ri_motion_geometry::get_n() const { return (int)times_.size(); }
    float ri_motion_geometry::get_time_at(size_t i) const { return times_[i]; }

    const ri_geometry* ri_motion_geometry::get_geometry_at(size_t i) const
    {
        return geos_[i];
    }

    //------------------------------------------------------
    // Internal

    static void
    BezierPatchExpandData(ri_parameters* out, int nu, int uorder, const char* uwrap,
                          int ustep, const char* ubasis, int nv, int vorder,
                          const char* vwrap, int vstep, const char* vbasis,
                          const ri_classifier* p_cls, const ri_parameters* in)
    {
        int nPu = (nu - 1) / (uorder - 1);
        int nPv = (nv - 1) / (vorder - 1);

        int ku = nu;
        int kv = nv;

        bool bUPeriodic = false;
        bool bVPeriodic = false;

        if (strcmp(uwrap, RI_PERIODIC) == 0)
        {
            nPu = nu / ustep;
            bUPeriodic = true;
        }
        else if (strcmp(uwrap, RI_NONPERIODIC) == 0)
        {
            nPu = (nu - uorder) / ustep + 1;
        }

        if (strcmp(vwrap, RI_PERIODIC) == 0)
        {
            nPv = nv / vstep;
            bVPeriodic = true;
        }
        else if (strcmp(vwrap, RI_NONPERIODIC) == 0)
        {
            nPv = (nv - vorder) / vstep + 1;
        }

        ku = (uorder - 1) * nPu + 1;
        kv = (vorder - 1) * nPv + 1;

        size_t sz = p_cls->size();
        for (size_t i = 0; i < sz; i++)
        {
            const ri_classifier::param_data& data = (*p_cls)[i];
            if (IsVertex(data.get_class()))
            {
                int dsz = data.get_size();
                if (dsz > 4)
                    continue;
                //
                switch (data.get_type())
                {
                case ri_classifier::FLOAT:
                case ri_classifier::COLOR:
                case ri_classifier::POINT:
                case ri_classifier::VECTOR:
                case ri_classifier::NORMAL:
                case ri_classifier::HPOINT:
                    // case ri_classifier::MATRIX:
                    {
                        const float* p = &(in->get(data.get_name())->float_values[0]);
                        std::vector<float> tmp(ku * kv * dsz);
                        for (int jj = 0; jj < nPv; jj++)
                        {
                            for (int ii = 0; ii < nPu; ii++)
                            {
                                int i0 = ii * ustep;
                                int j0 = jj * vstep;

                                std::vector<float> tpat(uorder * vorder * dsz);
                                for (int j = 0; j < vorder; j++)
                                {
                                    for (int i = 0; i < uorder; i++)
                                    {
                                        int ix = i0 + i;
                                        int jx = j0 + j;
                                        ix = ix % nu;
                                        jx = jx % nv;
                                        int index = jx * nu + ix;
                                        memcpy(&tpat[((j * uorder) + i) * dsz], &p[index * dsz],
                                               sizeof(float) * dsz);
                                    }
                                }
                                if (uorder == 4 && vorder == 4)
                                {
                                    ri_patch_convert_to_bezier(&tpat[0], dsz, ubasis, vbasis);
                                }
                                for (int j = 0; j < vorder; j++)
                                {
                                    for (int i = 0; i < uorder; i++)
                                    {
                                        int ix = ii * (uorder - 1) + i;
                                        int jx = jj * (vorder - 1) + j;
                                        int index = jx * ku + ix;
                                        memcpy(&tmp[index * dsz], &tpat[(j * uorder + i) * dsz],
                                               sizeof(float) * dsz);
                                    }
                                }
                            }
                        }
                        out->set(data.get_name(), (const float*)&tmp[0], (int)tmp.size());
                    }
                    break;
                }
            }
            else if (IsVarying(data.get_class()))
            {
                if (!bUPeriodic && !bVPeriodic)
                    continue;
                int dsz = data.get_size();
                if (IsLikeFloat(data.get_type()))
                {
                    const float* p = &(in->get(data.get_name())->float_values[0]);
                    std::vector<float> tmp((nPu + 1) * (nPv + 1) * dsz);

                    int kPu = nPu;
                    int kPv = nPv;
                    if (bUPeriodic)
                        kPu--;
                    if (bVPeriodic)
                        kPv--;
                    for (int jj = 0; jj < nPv + 1; jj++)
                    {
                        for (int ii = 0; ii < nPu + 1; ii++)
                        {
                            int ix = ii % (kPu + 1);
                            int jx = jj % (kPv + 1);

                            memcpy(&tmp[(jj * (nPu + 1) + ii) * dsz],
                                   &p[(jx * (kPu + 1) + ix) * dsz], sizeof(float) * dsz);
                        }
                    }

                    out->set(data.get_name(), (const float*)&tmp[0], (int)tmp.size());
                }
                else if (IsInteger(data.get_type()))
                {
                    const int* p = &(in->get(data.get_name())->integer_values[0]);
                    std::vector<int> tmp((nPu + 1) * (nPv + 1) * dsz);

                    int kPu = nPu;
                    int kPv = nPv;
                    if (bUPeriodic)
                        kPu--;
                    if (bVPeriodic)
                        kPv--;
                    for (int jj = 0; jj < nPv + 1; jj++)
                    {
                        for (int ii = 0; ii < nPu + 1; ii++)
                        {
                            int ix = ii % (kPu + 1);
                            int jx = jj % (kPv + 1);

                            memcpy(&tmp[(jj * (nPu + 1) + ii) * dsz],
                                   &p[(jx * (kPu + 1) + ix) * dsz], sizeof(int) * dsz);
                        }
                    }

                    out->set(data.get_name(), (const int*)&tmp[0], (int)tmp.size());
                }
            }
        }
    }

    // bezier basis
    // non periodic
    ri_bezier_patch_mesh_geometry::ri_bezier_patch_mesh_geometry(
        const ri_patch_geometry* pGeo)
        : pGeo_(pGeo)
    {
        int nOrder = GetOrder(pGeo->get_type());
        int nPu = 1;
        int nPv = 1;
        int uorder = nOrder;
        int vorder = nOrder;
        int nu = (uorder - 1) * nPu + 1;
        int nv = (vorder - 1) * nPv + 1;
        params_.set("nu", nu);
        params_.set("nv", nv);
        params_.set("uorder", uorder);
        params_.set("vorder", vorder);

        std::string ubasis = pGeo->get_ubasis();
        std::string vbasis = pGeo->get_vbasis();

        BezierPatchExpandData(&params_, nu, uorder, RI_NONPERIODIC, uorder - 1,
                              ubasis.c_str(), nv, vorder, RI_NONPERIODIC, vorder - 1,
                              vbasis.c_str(), &pGeo->get_classifier(),
                              &(pGeo->get_parameters()));
    }

    ri_bezier_patch_mesh_geometry::ri_bezier_patch_mesh_geometry(
        const ri_patch_mesh_geometry* pGeo)
        : pGeo_(pGeo)
    {
        int nOrder = GetOrder(pGeo->get_type());

        int nu = pGeo->get_nu();
        int nv = pGeo->get_nv();
        int uorder = nOrder;
        int vorder = nOrder;

        const char* uwrap = pGeo->get_uwrap();
        const char* vwrap = pGeo->get_vwrap();
        int ustep = pGeo->get_ustep();
        int vstep = pGeo->get_vstep();
        std::string ubasis = pGeo->get_ubasis();
        std::string vbasis = pGeo->get_vbasis();

        int nPu = (nu - 1) / (uorder - 1);
        int nPv = (nv - 1) / (vorder - 1);

        int ku = nu;
        int kv = nv;

        if (uorder != 4)
            ustep = uorder - 1;
        if (vorder != 4)
            vstep = vorder - 1;

        if (strcmp(uwrap, RI_PERIODIC) == 0)
        {
            nPu = nu / ustep;
        }
        else if (strcmp(uwrap, RI_NONPERIODIC) == 0)
        {
            nPu = (nu - uorder) / ustep + 1;
        }

        if (strcmp(vwrap, RI_PERIODIC) == 0)
        {
            nPv = nv / vstep;
        }
        else if (strcmp(vwrap, RI_NONPERIODIC) == 0)
        {
            nPv = (nv - vorder) / vstep + 1;
        }

        ku = (uorder - 1) * nPu + 1;
        kv = (vorder - 1) * nPv + 1;

        params_.set("nu", ku);
        params_.set("nv", kv);
        params_.set("uorder", uorder);
        params_.set("vorder", vorder);

        BezierPatchExpandData(&params_, nu, uorder, uwrap, ustep, ubasis.c_str(), nv,
                              vorder, vwrap, vstep, vbasis.c_str(),
                              &pGeo->get_classifier(), &(pGeo->get_parameters()));
    }

    int ri_bezier_patch_mesh_geometry::get_nu() const
    {
        return (params_.get("nu"))->integer_values[0];
    }

    int ri_bezier_patch_mesh_geometry::get_nv() const
    {
        return (params_.get("nv"))->integer_values[0];
    }

    int ri_bezier_patch_mesh_geometry::get_uorder() const
    {
        return (params_.get("uorder"))->integer_values[0];
    }

    int ri_bezier_patch_mesh_geometry::get_vorder() const
    {
        return (params_.get("vorder"))->integer_values[0];
    }

    const ri_parameters::value_type*
    ri_bezier_patch_mesh_geometry::get_parameter(const char* key) const
    {
        const ri_parameters::value_type* pv = params_.get(key);
        if (pv)
            return pv;
        return pGeo_->get_parameter(key);
    }

    const float* ri_bezier_patch_mesh_geometry::get_P() const
    {
        const ri_parameters::value_type* pv = this->get_parameter("P");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_bezier_patch_mesh_geometry::get_Pw() const
    {
        const ri_parameters::value_type* pv = this->get_parameter("Pw");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    bool ri_bezier_patch_mesh_geometry::is_rational() const
    {
        return pGeo_->is_rational();
    }

    //------------------------------------------------------
    static void ExpandUniformData(ri_parameters* out,
                                  const std::vector<int>& tri2poly,
                                  const ri_classifier* p_cls,
                                  const ri_parameters* in)
    {
        size_t fsz = tri2poly.size();
        size_t sz = p_cls->size();
        for (size_t i = 0; i < sz; i++)
        {
            const ri_classifier::param_data& data = (*p_cls)[i];
            if (IsUniform(data.get_class()))
            {
                switch (data.get_type())
                {
                case ri_classifier::INTEGER:
                {
                    int dsz = data.get_size();
                    std::vector<int> tmp(fsz * dsz);
                    const int* p = &(in->get(data.get_name())->integer_values[0]);
                    for (size_t j = 0; j < fsz; j++)
                    {
                        int idx = tri2poly[j];
                        for (int k = 0; k < dsz; k++)
                        {
                            tmp[j * dsz + k] = p[idx * dsz + k];
                        }
                    }
                    out->set(data.get_name(), (const int*)&tmp[0], (int)tmp.size());
                }
                break;
                case ri_classifier::STRING:
                {
                    int dsz = data.get_size();
                    std::vector<std::string> tmp(fsz * dsz);
                    const std::string* p = &(in->get(data.get_name())->string_values[0]);
                    for (size_t j = 0; j < fsz; j++)
                    {
                        int idx = tri2poly[j];
                        for (int k = 0; k < dsz; k++)
                        {
                            tmp[j * dsz + k] = p[idx * dsz + k];
                        }
                    }
                    out->set(data.get_name(), (const std::string*)&tmp[0],
                             (int)tmp.size());
                }
                break;
                case ri_classifier::FLOAT:
                case ri_classifier::COLOR:
                case ri_classifier::POINT:
                case ri_classifier::VECTOR:
                case ri_classifier::NORMAL:
                case ri_classifier::HPOINT:
                case ri_classifier::MATRIX:
                {
                    int dsz = data.get_size();
                    std::vector<float> tmp(fsz * dsz);
                    const float* p = &(in->get(data.get_name())->float_values[0]);
                    for (size_t j = 0; j < fsz; j++)
                    {
                        int idx = tri2poly[j];
                        for (int k = 0; k < dsz; k++)
                        {
                            tmp[j * dsz + k] = p[idx * dsz + k];
                        }
                    }
                    out->set(data.get_name(), (const float*)&tmp[0], (int)tmp.size());
                }
                break;
                default:
                    break;
                }
            }
        }
    }

    static void
    ExpandFaceVaryingData(ri_parameters* out,
                          const std::vector<int>& fvindices, // for face varying
                          const ri_classifier* p_cls, const ri_parameters* in)
    {
        size_t isz = fvindices.size();
        size_t sz = p_cls->size();
        for (size_t i = 0; i < sz; i++)
        {
            const ri_classifier::param_data& data = (*p_cls)[i];
            if (IsFaceVarying(data.get_class()))
            {
                switch (data.get_type())
                {
                case ri_classifier::INTEGER:
                {
                    int dsz = data.get_size();
                    std::vector<int> tmp(isz * dsz);
                    const int* p = &(in->get(data.get_name())->integer_values[0]);
                    for (size_t j = 0; j < isz; j++)
                    {
                        int idx = fvindices[j]; //
                        for (int k = 0; k < dsz; k++)
                        {
                            tmp[j * dsz + k] = p[idx * dsz + k];
                        }
                    }
                    out->set(data.get_name(), (const int*)&tmp[0], (int)tmp.size());
                }
                break;
                case ri_classifier::STRING:
                {
                    int dsz = data.get_size();
                    std::vector<std::string> tmp(isz * dsz);
                    const std::string* p = &(in->get(data.get_name())->string_values[0]);
                    for (size_t j = 0; j < isz; j++)
                    {
                        int idx = fvindices[j];
                        for (int k = 0; k < dsz; k++)
                        {
                            tmp[j * dsz + k] = p[idx * dsz + k];
                        }
                    }
                    out->set(data.get_name(), (const std::string*)&tmp[0],
                             (int)tmp.size());
                }
                break;
                case ri_classifier::FLOAT:
                case ri_classifier::COLOR:
                case ri_classifier::POINT:
                case ri_classifier::VECTOR:
                case ri_classifier::NORMAL:
                case ri_classifier::HPOINT:
                case ri_classifier::MATRIX:
                {
                    int dsz = data.get_size();
                    std::vector<float> tmp(isz * dsz);
                    const float* p = &(in->get(data.get_name())->float_values[0]);
                    for (size_t j = 0; j < isz; j++)
                    {
                        int idx = fvindices[j];
                        for (int k = 0; k < dsz; k++)
                        {
                            tmp[j * dsz + k] = p[idx * dsz + k];
                        }
                    }
                    out->set(data.get_name(), (const float*)&tmp[0], (int)tmp.size());
                }
                break;
                default:
                    break;
                }
            }
        }
    }

    ri_points_triangle_polygons_geometry::ri_points_triangle_polygons_geometry(
        const ri_polygon_geometry* pGeo)
        : pGeo_(pGeo)
    {
        std::vector<int> tri2poly;
        std::vector<int> fvindices;
        ri_polygon_triangulate_indices(indices_, tri2poly, fvindices,
                                       pGeo->get_nverts(), pGeo->get_P());
        ExpandUniformData(&params_, tri2poly, &pGeo->get_classifier(),
                          &(pGeo->get_parameters()));
        ExpandFaceVaryingData(&params_, fvindices, &pGeo->get_classifier(),
                              &(pGeo->get_parameters()));

        nverts_ = pGeo->get_nverts();
    }

    ri_points_triangle_polygons_geometry::ri_points_triangle_polygons_geometry(
        const ri_general_polygon_geometry* pGeo)
        : pGeo_(pGeo)
    {
        std::vector<int> tri2poly;
        std::vector<int> fvindices;
        ri_general_polygon_triangulate_indices(indices_, tri2poly, fvindices,
                                               pGeo->get_nloops(), pGeo->get_nverts(),
                                               pGeo->get_P());
        ExpandUniformData(&params_, tri2poly, &pGeo->get_classifier(),
                          &(pGeo->get_parameters()));
        ExpandFaceVaryingData(&params_, fvindices, &pGeo->get_classifier(),
                              &(pGeo->get_parameters()));

        nverts_ = SumArray(pGeo->get_nverts(), pGeo->get_nloops());
    }

    ri_points_triangle_polygons_geometry::ri_points_triangle_polygons_geometry(
        const ri_points_polygons_geometry* pGeo)
        : pGeo_(pGeo)
    {
        std::vector<int> tri2poly;
        std::vector<int> fvindices;
        ri_points_polygons_triangulate_indices(indices_, tri2poly, fvindices,
                                               pGeo->get_npolys(), pGeo->get_nverts(),
                                               pGeo->get_verts(), pGeo->get_P());
        ExpandUniformData(&params_, tri2poly, &pGeo->get_classifier(),
                          &(pGeo->get_parameters()));
        ExpandFaceVaryingData(&params_, fvindices, &pGeo->get_classifier(),
                              &(pGeo->get_parameters()));

        int nPts = SumArray(pGeo->get_nverts(), pGeo->get_npolys());
        const int* verts = pGeo->get_verts();
        int psz = 0;
        for (int i = 0; i < nPts; i++)
        {
            if (psz < verts[i])
            {
                psz = verts[i];
            }
        }
        nverts_ = psz + 1;
    }

    ri_points_triangle_polygons_geometry::ri_points_triangle_polygons_geometry(
        const ri_points_general_polygons_geometry* pGeo)
        : pGeo_(pGeo)
    {
        std::vector<int> tri2poly;
        std::vector<int> fvindices;
        ri_points_general_polygons_triangulate_indices(
            indices_, tri2poly, fvindices, pGeo->get_npolys(), pGeo->get_nloops(),
            pGeo->get_nverts(), pGeo->get_verts(), pGeo->get_P());
        ExpandUniformData(&params_, tri2poly, &pGeo->get_classifier(),
                          &(pGeo->get_parameters()));
        ExpandFaceVaryingData(&params_, fvindices, &pGeo->get_classifier(),
                              &(pGeo->get_parameters()));

        int nVts = SumArray(pGeo->get_nloops(), pGeo->get_npolys());
        int nv = SumArray(pGeo->get_nverts(), nVts);
        const int* verts = pGeo->get_verts();
        int psz = 0;
        for (int i = 0; i < nv; i++)
        {
            if (psz < verts[i])
            {
                psz = verts[i];
            }
        }
        nverts_ = psz + 1;
    }

    ri_points_triangle_polygons_geometry::ri_points_triangle_polygons_geometry(
        const ri_subdivision_mesh_geometry* pGeo)
        : pGeo_(pGeo)
    {
        // As ri_points_polygons_geometry
        std::vector<int> tri2poly;
        std::vector<int> fvindices;
        ri_points_polygons_triangulate_indices(indices_, tri2poly, fvindices,
                                               pGeo->get_npolys(), pGeo->get_nverts(),
                                               pGeo->get_verts(), pGeo->get_P());
        ExpandUniformData(&params_, tri2poly, &pGeo->get_classifier(),
                          &(pGeo->get_parameters()));
        ExpandFaceVaryingData(&params_, fvindices, &pGeo->get_classifier(),
                              &(pGeo->get_parameters()));

        int nPts = SumArray(pGeo->get_nverts(), pGeo->get_npolys());
        const int* verts = pGeo->get_verts();
        int psz = 0;
        for (int i = 0; i < nPts; i++)
        {
            if (psz < verts[i])
            {
                psz = verts[i];
            }
        }
        nverts_ = psz + 1;
    }

    const ri_parameters::value_type*
    ri_points_triangle_polygons_geometry::get_parameter(const char* key) const
    {
        const ri_parameters::value_type* pv = params_.get(key);
        if (pv)
            return pv;
        return pGeo_->get_parameter(key);
    }

    int ri_points_triangle_polygons_geometry::get_nfaces() const
    {
        return (int)indices_.size() / 3;
    }
    int ri_points_triangle_polygons_geometry::get_nverts() const { return nverts_; }
    const float* ri_points_triangle_polygons_geometry::get_P() const
    {
        return pGeo_->get_P();
    }
    const float* ri_points_triangle_polygons_geometry::get_N() const
    {
        if (is_facevarying("N"))
            return NULL;
        const ri_parameters::value_type* pv = this->get_parameter("N");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_points_triangle_polygons_geometry::get_Np() const
    {
        const ri_parameters::value_type* pv = this->get_parameter("Np");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_points_triangle_polygons_geometry::get_Nf() const
    {
        if (is_facevarying("N"))
        {
            const ri_parameters::value_type* pv = this->get_parameter("N");
            if (pv)
            {
                return &(pv->float_values[0]);
            }
        }
        return NULL;
    }
    const float* ri_points_triangle_polygons_geometry::get_st() const
    {
        if (is_facevarying("st"))
            return NULL;
        const ri_parameters::value_type* pv = this->get_parameter("st");
        if (pv)
        {
            return &(pv->float_values[0]);
        }
        return NULL;
    }
    const float* ri_points_triangle_polygons_geometry::get_stf() const
    {
        if (is_facevarying("st"))
        {
            const ri_parameters::value_type* pv = this->get_parameter("st");
            if (pv)
            {
                return &(pv->float_values[0]);
            }
        }
        return NULL;
    }

    bool ri_points_triangle_polygons_geometry::is_facevarying(
        const char* key) const
    {
        int id = pGeo_->get_classifier().find(key);
        if (id >= 0)
        {
            const ri_classifier::param_data& data = pGeo_->get_classifier()[id];
            if (IsFaceVarying(data.get_class()))
                return true;
        }
        return false;
    }

    const float* ri_points_triangle_polygons_geometry::get_Cs() const
    {
        if (is_facevarying("Cs"))
            return NULL;
        return pGeo_->get_Cs();
    }
    const float* ri_points_triangle_polygons_geometry::get_Os() const
    {
        if (is_facevarying("Os"))
            return NULL;
        return pGeo_->get_Os();
    }
    const int* ri_points_triangle_polygons_geometry::get_verts() const
    {
        if (!indices_.empty())
        {
            return &indices_[0];
        }
        else
        {
            return NULL;
        }
    }
    //------------------------------------------------------
    //
    static bool IsExistFile(const char* szFilePath)
    {
#ifdef _WIN32
        struct _stat s;
        int nRet = _stat(szFilePath, &s);
        return (nRet == 0);
#else
        struct stat s;
        int nRet = stat(szFilePath, &s);
        return (nRet == 0);
#endif
    }

    static float GetArea(const ri_procedural_geometry* pGeo)
    {
        const float* bound = pGeo->get_bound();
        ;//
        return 640 * 480;
    }

    ri_procedural_evaluation_geometry::ri_procedural_evaluation_geometry(
        const ri_procedural_geometry* pGeo)
        : ctx_(NULL)
    {
        ri_attributes attr = pGeo->get_attributes();
        attr.clear_group();
        std::unique_ptr<ri_frame_context> ctx(
            new ri_frame_context(pGeo->get_options(), attr, pGeo->get_transform()));

        //
        std::string type = pGeo->get_type();
        if (type == "DelayedReadArchive")
        {
            const char* filename = pGeo->get_data_at(0);
            if (!filename)
                return;

            bool bReaded = false;
            ri_path_resolver pr(pGeo->get_options());
            std::vector<std::string> archive_paths = pr.get_archives_paths();
            for (size_t i = 0; i < archive_paths.size(); i++)
            {
                std::string path = archive_paths[i] + filename;

                if (IsExistFile(path.c_str()))
                {
                    int nRet = ri_read_archive(path.c_str(), ctx.get());
                    if (nRet != 0)
                        return;
                    bReaded = true;
                    break;
                }
            }
            // if(!bReaded){
            //  print_log("Cant read!\n");
            //}
        }
        else if (type == "RunProgram")
        {
            const char* programname = pGeo->get_data_at(0);
            const char* argument = pGeo->get_data_at(1);
            if (!(programname && argument))
                return;
            float area = GetArea(pGeo);
            int nRet = ri_run_program(ctx.get(), programname, area, argument);
            if (nRet != 0)
                return;
        }
        else if (type == "DynamicLoad")
        {
            const char* programname = pGeo->get_data_at(0);
            const char* argument = pGeo->get_data_at(1);
            if (!(programname && argument))
                return;
            float area = GetArea(pGeo);
            int nRet = ri_dynamic_load(ctx.get(), programname, area, argument);
            if (nRet != 0)
                return;
        }
        else
        {
            return;
        }

        size_t sz = ctx->get_task_size();
        for (size_t i = 0; i < sz; i++)
        {
            ri_task* tsk = ctx->get_task_at(i);
            if (tsk->type() == "Frame")
            {
                ri_frame_task* frm = dynamic_cast<ri_frame_task*>(tsk);
                size_t gsz = frm->get_geometry_size();
                for (size_t j = 0; j < gsz; j++)
                {
                    this->add(frm->get_geomoetry_at(j));
                }
            }
            else
            {
                tsk->run();
            }
        }
        ctx_ = ctx.release();
    }

    ri_procedural_evaluation_geometry::~ri_procedural_evaluation_geometry()
    {
        if (ctx_)
            delete ctx_;
    }

    //-----------------------------------------------------------
    static void ExpandVertexDataLinear(std::vector<float>& out, int ncurves,
                                       const int nvertices[], const char* wrap,
                                       int ndim, const float* P)
    {
        bool is_periodic = false;
        if (strcmp(wrap, RI_PERIODIC) == 0)
        {
            is_periodic = true;
        }

        if (!is_periodic)
        {
            int total = 0;
            for (int i = 0; i < ncurves; i++)
            {
                int nv = nvertices[i];
                total += nv * ndim;
            }
            out.resize(total);
            memcpy(&out[0], P, sizeof(float) * total);
        }
        else
        {
            int total = 0;
            for (int i = 0; i < ncurves; i++)
            {
                int nv = nvertices[i];
                total += (nv + 1) * ndim;
            }
            out.reserve(total);
            int offset_v = 0;
            for (int i = 0; i < ncurves; i++)
            {
                int nv = nvertices[i];
                for (int j = 0; j < nv + 1; j++)
                {
                    int jj = j % nv;
                    for (int k = 0; k < ndim; k++)
                    {
                        out.push_back(P[(offset_v + jj) * ndim + k]);
                    }
                }
                offset_v += nv;
            }
        }
    }

    static void ExpandVertexDataCubic(std::vector<float>& out, int ncurves,
                                      const int nvertices[], const char* wrap,
                                      int step, const char* basis, int ndim,
                                      const float* P)
    {
        bool is_periodic = false;
        if (strcmp(wrap, RI_PERIODIC) == 0)
        {
            is_periodic = true;
        }

        if (!is_periodic)
        {
            int total = 0;
            for (int i = 0; i < ncurves; i++)
            {
                int nv = nvertices[i];
                int ns = (nv - 4) / step + 1;
                int nt = ns * 3 + 1;
                total += nt * ndim;
            }
            out.reserve(total);
            int offset_v = 0;
            for (int i = 0; i < ncurves; i++)
            {
                int nv = nvertices[i];
                int ns = (nv - 4) / step + 1;
                int nt = ns * 3 + 1;
                std::vector<float> pOut(nt * ndim);

                for (int j = 0; j < ns; j++)
                {
                    std::vector<float> points(4 * ndim);
                    memcpy(&points[0], P + ndim * (offset_v + step * j),
                           sizeof(float) * 4 * ndim);
                    ri_curves_convert_to_bezier(&points[0], ndim, basis);
                    memcpy(&pOut[3 * j * ndim], &points[0], sizeof(float) * 4 * ndim);
                }

                for (int j = 0; j < pOut.size(); j++)
                {
                    out.push_back(pOut[j]);
                }
                offset_v += nv;
            }
        }
        else
        {
            int total = 0;
            for (int i = 0; i < ncurves; i++)
            {
                int nv = nvertices[i];
                int ns = nv / step;
                int nt = ns * 3 + 1;
                total += nt * ndim;
            }
            out.reserve(total);
            int offset_v = 0;
            for (int i = 0; i < ncurves; i++)
            {
                int nv = nvertices[i];
                int ns = nv / step;
                int nt = ns * 3 + 1;
                std::vector<float> pOut(nt * ndim);

                for (int j = 0; j < ns; j++)
                {
                    std::vector<float> points(4 * ndim);
                    if (j != ns - 1)
                    {
                        memcpy(&points[0], P + ndim * (offset_v + step * j),
                               sizeof(float) * 4 * ndim);
                    }
                    else
                    {
                        memcpy(&points[0], P + ndim * (offset_v + step * j),
                               sizeof(float) * 3 * ndim);
                        memcpy(&points[3 * ndim], P + ndim * (offset_v + step * 0),
                               sizeof(float) * 1 * ndim);
                    }
                    ri_curves_convert_to_bezier(&points[0], ndim, basis);
                    memcpy(&pOut[3 * j * ndim], &points[0], sizeof(float) * 4 * ndim);
                }

                for (int j = 0; j < pOut.size(); j++)
                {
                    out.push_back(pOut[j]);
                }
                offset_v += nv;
            }
        }
    }

    static void ExpandVertexData(std::vector<float>& out, int ncurves,
                                 const int nvertices[], int order, const char* wrap,
                                 int step, const char* basis, int ndim,
                                 const float* P)
    {
        if (order == 2)
        {
            ExpandVertexDataLinear(out, ncurves, nvertices, wrap, ndim, P);
        }
        else if (order == 4)
        {
            ExpandVertexDataCubic(out, ncurves, nvertices, wrap, step, basis, ndim, P);
        }
    }

    static void ExpandVaryingDataLinear(std::vector<float>& out, int ncurves,
                                        const int nvertices[], const char* wrap,
                                        int ndim, const float* P)
    {
        ExpandVertexDataLinear(out, ncurves, nvertices, wrap, ndim, P);
    }

    static void ExpandVaryingDataCubic(std::vector<float>& out, int ncurves,
                                       const int nvertices[], const char* wrap,
                                       int step, const char* basis, int ndim,
                                       const float* P)
    {
        bool is_periodic = false;
        if (strcmp(wrap, RI_PERIODIC) == 0)
        {
            is_periodic = true;
        }

        if (!is_periodic)
        {
            int total = 0;
            for (int i = 0; i < ncurves; i++)
            {
                int nv = nvertices[i];
                int ns = (nv - 4) / step + 1;
                int nt = ns + 1;
                total += nt * ndim;
            }
            out.resize(total);
            memcpy(&out[0], P, sizeof(float) * total);
        }
        else
        {
            int total = 0;
            for (int i = 0; i < ncurves; i++)
            {
                int nv = nvertices[i];
                int ns = nv / step;
                int nt = ns + 1;
                total += nt * ndim;
            }
            out.reserve(total);

            int offset_v = 0;
            for (int i = 0; i < ncurves; i++)
            {
                int nv = nvertices[i];
                int ns = nv / step;
                int nt = ns + 1;

                for (int j = 0; j < nt; j++)
                {
                    int jj = j % ns;
                    for (int k = 0; k < ndim; k++)
                    {
                        out.push_back(P[(offset_v + jj) * ndim + k]);
                    }
                }
                offset_v += ns;
            }
        }
    }

    static void ExpandVaryingData(std::vector<float>& out, int ncurves,
                                  const int nvertices[], int order,
                                  const char* wrap, int step, const char* basis,
                                  int ndim, const float* P)
    {
        if (order == 2)
        {
            ExpandVaryingDataLinear(out, ncurves, nvertices, wrap, ndim, P);
        }
        else if (order == 4)
        {
            ExpandVaryingDataCubic(out, ncurves, nvertices, wrap, step, basis, ndim, P);
        }
    }

    static void BezierCurvesExpandData(ri_parameters* out, int ncurves,
                                       const int nvertices[], int vorder,
                                       const char* vwrap, int vstep,
                                       const char* vbasis,
                                       const ri_classifier* p_cls,
                                       const ri_parameters* in)
    {
        size_t sz = p_cls->size();
        for (size_t i = 0; i < sz; i++)
        {
            const ri_classifier::param_data& data = (*p_cls)[i];
            if (IsVertex(data.get_class()))
            {
                int dsz = data.get_size();
                if (dsz > 4)
                    continue;

                if (IsLikeFloat(data.get_type()))
                {
                    const float* p = &(in->get(data.get_name())->float_values[0]);
                    std::vector<float> tmp;
                    ExpandVertexData(tmp, ncurves, nvertices, vorder, vwrap, vstep, vbasis,
                                     data.get_size(), p);
                    out->set(data.get_name(), &tmp[0], (int)tmp.size());
                }
            }
            else if (IsVarying(data.get_class()))
            {
                int dsz = data.get_size();
                if (dsz > 4)
                    continue;

                if (IsLikeFloat(data.get_type()))
                {
                    const float* p = &(in->get(data.get_name())->float_values[0]);
                    std::vector<float> tmp;
                    ExpandVaryingData(tmp, ncurves, nvertices, vorder, vwrap, vstep, vbasis,
                                      data.get_size(), p);
                    out->set(data.get_name(), &tmp[0], (int)tmp.size());
                }
            }
            else if (IsUniform(data.get_class()))
            {
                out->set(data.get_name(), *(in->get(data.get_name())));
            }
            else if (IsConstant(data.get_class()))
            {
                out->set(data.get_name(), *(in->get(data.get_name())));
            }
        }

        { // check to convert vertices...
            bool is_periodic = false;
            if (strcmp(vwrap, RI_PERIODIC) == 0)
            {
                is_periodic = true;
            }

            bool is_degree_is_step =
                (vorder != 4) || ((vorder - 1) == vstep); // degree is step

            std::vector<int> tvertices;
            if (is_degree_is_step)
            { //
                tvertices.resize(ncurves);
                if (is_periodic)
                {
                    for (int i = 0; i < ncurves; i++)
                    {
                        int nv = nvertices[i];
                        tvertices[i] = nv + 1;
                    }
                    nvertices = &tvertices[0];
                }
            }
            else
            {
                // cubic
                assert(vorder == 4);
                tvertices.resize(ncurves);
                if (is_periodic)
                {
                    for (int i = 0; i < ncurves; i++)
                    {
                        int nv = nvertices[i];
                        int ns = nv / vstep;
                        tvertices[i] = ns * (vorder - 1) + 1;
                    }
                    nvertices = &tvertices[0];
                }
                else
                {
                    for (int i = 0; i < ncurves; i++)
                    {
                        int nv = nvertices[i];
                        int ns = (nv - 4) / vstep + 1;
                        tvertices[i] = ns * (vorder - 1) + 1;
                    }
                    nvertices = &tvertices[0];
                }
            }

            out->set("nvertices", nvertices, ncurves);
        }
    }

    ri_bezier_curves_geometry::ri_bezier_curves_geometry(
        const ri_curves_geometry* pGeo)
        : pGeo_(pGeo)
    {
        if (!is_original())
        {
            // To Convert NonPeriodic Bezier Curves.
            int nOrder = GetOrder(pGeo->get_type());
            int ncurves = pGeo->get_ncurves();
            const int* nvertices = pGeo->get_nvertices();
            int vorder = nOrder;
            const char* vwrap = pGeo->get_wrap();
            int vstep = pGeo->get_vstep();
            std::string vbasis = pGeo->get_vbasis();
            BezierCurvesExpandData(&params_, ncurves, nvertices, vorder, vwrap, vstep,
                                   vbasis.c_str(), &(pGeo->get_classifier()),
                                   &(pGeo->get_parameters()));
        }
    }

    const int* ri_bezier_curves_geometry::get_nvertices() const
    {
        if (is_original())
        {
            return pGeo_->get_nvertices();
        }
        else
        {
            return &((params_.get("nvertices"))->integer_values[0]);
        }
    }

    const float* ri_bezier_curves_geometry::get_P() const
    {
        if (is_original())
        {
            return pGeo_->get_P();
        }
        else
        {
            const ri_parameters::value_type* pv = params_.get("P");
            if (pv)
            {
                return &(pv->float_values[0]);
            }
            return NULL;
        }
    }
    const float* ri_bezier_curves_geometry::get_N() const
    {
        if (is_original())
        {
            return pGeo_->get_N();
        }
        else
        {
            if (is_vertex("N"))
                return NULL;
            const ri_parameters::value_type* pv = params_.get("N");
            if (pv)
            {
                return &(pv->float_values[0]);
            }
            return NULL;
        }
    }
    const float* ri_bezier_curves_geometry::get_width() const
    {
        if (is_original())
        {
            return pGeo_->get_width();
        }
        else
        {
            if (is_vertex("width"))
                return NULL;
            const ri_parameters::value_type* pv = params_.get("width");
            if (pv)
            {
                return &(pv->float_values[0]);
            }
            return NULL;
        }
    }
    const float* ri_bezier_curves_geometry::get_Cs() const
    {
        if (is_original())
        {
            return pGeo_->get_Cs();
        }
        else
        {
            if (is_vertex("Cs"))
                return NULL;
            const ri_parameters::value_type* pv = params_.get("Cs");
            if (pv)
            {
                return &(pv->float_values[0]);
            }
            return NULL;
        }
    }
    const float* ri_bezier_curves_geometry::get_Os() const
    {
        if (is_original())
        {
            return pGeo_->get_Os();
        }
        else
        {
            if (is_vertex("Os"))
                return NULL;
            const ri_parameters::value_type* pv = params_.get("Os");
            if (pv)
            {
                return &(pv->float_values[0]);
            }
            return NULL;
        }
    }
    const float* ri_bezier_curves_geometry::get_Nv() const
    {
        if (is_original())
        {
            return pGeo_->get_Nv();
        }
        else
        {
            if (!is_vertex("N"))
                return NULL;
            const ri_parameters::value_type* pv = params_.get("N");
            if (pv)
            {
                return &(pv->float_values[0]);
            }
            return NULL;
        }
    }
    const float* ri_bezier_curves_geometry::get_widthv() const
    {
        if (is_original())
        {
            return pGeo_->get_widthv();
        }
        else
        {
            if (!is_vertex("width"))
                return NULL;
            const ri_parameters::value_type* pv = params_.get("width");
            if (pv)
            {
                return &(pv->float_values[0]);
            }
            return NULL;
        }
    }
    const float* ri_bezier_curves_geometry::get_Csv() const
    {
        if (is_original())
        {
            return pGeo_->get_Csv();
        }
        else
        {
            if (!is_vertex("Cs"))
                return NULL;
            const ri_parameters::value_type* pv = params_.get("Cs");
            if (pv)
            {
                return &(pv->float_values[0]);
            }
            return NULL;
        }
    }
    const float* ri_bezier_curves_geometry::get_Osv() const
    {
        if (is_original())
        {
            return pGeo_->get_Osv();
        }
        else
        {
            if (!is_vertex("Os"))
                return NULL;
            const ri_parameters::value_type* pv = params_.get("Os");
            if (pv)
            {
                return &(pv->float_values[0]);
            }
            return NULL;
        }
    }

    int ri_bezier_curves_geometry::get_order() const
    {
        return GetOrder(pGeo_->get_type());
    }

    bool ri_bezier_curves_geometry::is_periodic() const
    {
        if (strcmp(RI_PERIODIC, pGeo_->get_wrap()) == 0)
            return true;
        return false;
    }
    bool ri_bezier_curves_geometry::is_bezier() const
    {
        int order = get_order();
        if (order == 2)
            return true;
        std::string basis = pGeo_->get_vbasis();
        if (strcmp(RI_BEZIER, basis.c_str()) == 0)
            return true;
        return false;
    }
    bool ri_bezier_curves_geometry::is_original() const
    {
        return (!is_periodic() && is_bezier());
    }

    bool ri_bezier_curves_geometry::is_vertex(const char* key) const
    {
        return pGeo_->is_vertex(key);
    }

    const ri_parameters::value_type*
    ri_bezier_curves_geometry::get_parameter(const char* key) const
    {
        const ri_parameters::value_type* pv = params_.get(key);
        if (pv)
            return pv;
        return pGeo_->get_parameter(key);
    }

    //------------------------------------------------------------

    static std::string MakeFullPath(const char* szFilePath)
    {
#ifdef _WIN32
        char szFullPath[_MAX_PATH] = "";
        _fullpath(szFullPath, szFilePath, _MAX_PATH);
        return szFullPath;
#else
        char szFullPath[512];
        realpath(szFilePath, szFullPath);
        return szFullPath;
#endif
    }

    ri_file_mesh_geometry::ri_file_mesh_geometry(const ri_classifier* p_cls,
                                                 RtToken type, RtInt n,
                                                 RtToken tokens[],
                                                 RtPointer params[])
    {
        params_.set("type", type);
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params);
        }
        std::string path = this->get_path();
        if (!path.empty())
        {
            params_.set("full_path", MakeFullPath(path.c_str()));
        }
    }

    std::string ri_file_mesh_geometry::get_type() const
    {
        return params_.get("type")->string_values[0];
    }
    std::string ri_file_mesh_geometry::get_path() const
    {
        const char* PATHs[] = {"path", "file", "file_path", "filepath", NULL};
        int i = 0;
        while (PATHs[i])
        {
            const ri_parameters::value_type* pv = params_.get(PATHs[i]);
            if (pv && pv->string_values.size())
            {
                return pv->string_values[0];
            }
            i++;
        }
        return "";
    }
    std::string ri_file_mesh_geometry::get_full_path() const
    {
        const ri_parameters::value_type* pv = params_.get("full_path");
        if (pv)
            return pv->string_values[0];
        return "";
    }

    ri_structure_synth_geometry::ri_structure_synth_geometry(
        const ri_classifier* p_cls, RtInt n, RtToken tokens[], RtPointer params[])
    {
        if (n)
        {
            ri_saving_classifier cls(p_cls, &(this->clsf_));
            AddParams(&params_, &cls, n, tokens, params);
        }
        std::string path = this->get_path();
        if (!path.empty())
        {
            params_.set("full_path", MakeFullPath(path.c_str()));
        }
    }
    std::string ri_structure_synth_geometry::get_path() const
    {
        const char* PATHs[] = {"path", "file", "file_path", "filepath", NULL};
        int i = 0;
        while (PATHs[i])
        {
            const ri_parameters::value_type* pv = params_.get(PATHs[i]);
            if (pv && pv->string_values.size())
            {
                return pv->string_values[0];
            }
            i++;
        }
        return "";
    }
    std::string ri_structure_synth_geometry::get_full_path() const
    {
        const ri_parameters::value_type* pv = params_.get("full_path");
        if (pv)
            return pv->string_values[0];
        return "";
    }

    ri_blobby_mesh_geometry::ri_blobby_mesh_geometry(
        const ri_blobby_geometry* pGeo, const std::vector<int>& indices,
        const std::vector<float>& vertices, const ri_classifier& cls,
        const ri_parameters& params)
        : pGeo_(pGeo), indices_(indices), vertices_(vertices)
    {
        clsf_ = cls;
        params_ = params;
    }

    int ri_blobby_mesh_geometry::get_nfaces() const
    {
        return (int)(indices_.size() / 3);
    }
    int ri_blobby_mesh_geometry::get_nverts() const
    {
        return (int)(vertices_.size() / 3);
    }

    const int* ri_blobby_mesh_geometry::get_verts() const { return &indices_[0]; }
}
