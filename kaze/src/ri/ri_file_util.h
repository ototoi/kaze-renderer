#ifndef RI_FILE_UTIL_H
#define RI_FILE_UTIL_H

#ifdef _WIN32
#include <windows.h>
#endif

#if defined(__unix__) || defined(__APPLE__)
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#endif

#include <vector>
#include <string>
#include <cstdlib>

namespace ri
{
    enum
    {
        FILE_OPEN_READ = 0x1,
        FILE_OPEN_WRITE = 0x2,
        FILE_OPEN_CREATE = 0x4,
        FILE_OPEN_APPEND = 0x8,
        FILE_OPEN_TRUNCATE = 0x10, //write & append

        FILE_OPEN_BINARY = 0x20,
        FILE_OPEN_EXCL = 0x40,

        FILE_OPEN_LARGEFILE = 0x4000,

        FILE_OPEN_NOSHARE = 0x8000
    };

    enum
    {
        FILE_MODE_DEFAULT = 0x0666,
        FILE_MODE_TEMP = 0x0666,

        FILE_MODE_UREAD = 0x0400,
        FILE_MODE_UWRITE = 0x0200,
        FILE_MODE_UEXECUTE = 0x0100,

        FILE_MODE_GREAD = 0x0040,
        FILE_MODE_GWRITE = 0x0020,
        FILE_MODE_GEXECUTE = 0x0010,

        FILE_MODE_WREAD = 0x0004,
        FILE_MODE_WWRITE = 0x0002,
        FILE_MODE_WEXECUTE = 0x0001
    };

#ifdef _WIN32

    typedef HANDLE file_handle; //�n���h��
    typedef DWORD file_mode_t;

    inline file_handle invalid_file_handle() { return INVALID_HANDLE_VALUE; }

    inline bool open_file(file_handle* fh, const char* filepath, file_mode_t flag, file_mode_t mode = FILE_MODE_DEFAULT)
    {
        file_handle tfh;

        DWORD oflags = 0;
        DWORD createflags = 0;
        DWORD attributes = FILE_ATTRIBUTE_NORMAL;
        DWORD sharemode = FILE_SHARE_READ | FILE_SHARE_WRITE;

        if (flag & FILE_OPEN_READ)
        {
            oflags |= GENERIC_READ;
        }
        if (flag & FILE_OPEN_WRITE)
        {
            oflags |= GENERIC_WRITE;
        }

        if (flag & FILE_OPEN_CREATE)
        {
            if (flag & FILE_OPEN_EXCL)
            {
                /* only create new if file does not already exist */
                createflags = CREATE_NEW;
            }
            else if (flag & FILE_OPEN_TRUNCATE)
            {
                /* truncate existing file or create new */
                createflags = CREATE_ALWAYS;
            }
            else
            {
                /* open existing but create if necessary */
                createflags = OPEN_ALWAYS;
            }
        }
        else if (flag & FILE_OPEN_TRUNCATE)
        {
            /* only truncate if file already exists */
            createflags = TRUNCATE_EXISTING;
        }
        else
        {
            /* only open if file already exists */
            createflags = OPEN_EXISTING;
        }

        if ((flag & FILE_OPEN_EXCL) && !(flag & FILE_OPEN_CREATE))
        {
            return false;
        }

#if (_WIN32_WINNT >= 0x0400)
        sharemode |= FILE_SHARE_DELETE;
#endif

        if (flag & FILE_OPEN_NOSHARE)
        {
            sharemode = 0;
        }

        tfh = ::CreateFileA(filepath, oflags, sharemode, NULL, createflags, attributes, NULL);

        if (tfh == INVALID_HANDLE_VALUE)
        {
            return false;
        }
        else
        {
            *fh = tfh;
        }

        if (flag & FILE_OPEN_APPEND)
        {
            ::SetFilePointer(tfh, 0, NULL, FILE_END);
        }

        return true;
    }

    template <bool b64bit = true>
    struct file_util__
    {
        static BOOL seek_offset(file_handle h, std::size_t offset)
        {
            LARGE_INTEGER li;
            li.QuadPart = offset;
            return ::SetFilePointer(h, li.LowPart, &li.HighPart, FILE_BEGIN);
        }
    };
    template <>
    struct file_util__<false>
    {
        static BOOL seek_offset(file_handle h, std::size_t offset)
        {
            return ::SetFilePointer(h, (LONG)offset, NULL, FILE_BEGIN);
        }
    };

    inline void seek_offset(file_handle h, std::size_t offset)
    {
        file_util__<sizeof(std::size_t) == 8>::seek_offset(h, offset);
    }

    inline bool close_file(file_handle h)
    {
        if (::CloseHandle(h))
            return true;
        else
            return false;
    }

    inline bool flush_file(file_handle h)
    {
        if (::FlushFileBuffers(h))
            return true;
        else
            return false;
    }

    inline bool remove_file(const char* filepath)
    {
        if (::DeleteFileA(filepath))
            return true;
        else
            return false;
    }

    inline bool delete_file(const char* filepath)
    {
        return remove_file(filepath);
    }

    inline size_t maxpath() { return (size_t)MAX_PATH; }

#endif

#if defined(__unix__) || defined(__APPLE__)

    typedef int file_handle;
    typedef int file_mode_t;

    inline file_handle invalid_file_handle() { return (-1); }

    inline file_mode_t parms2mode(file_mode_t perms)
    {
        int mode = 0;

        if (perms & FILE_MODE_UREAD)
            mode |= S_IRUSR;
        if (perms & FILE_MODE_UWRITE)
            mode |= S_IWUSR;
        if (perms & FILE_MODE_UEXECUTE)
            mode |= S_IXUSR;

        if (perms & FILE_MODE_GREAD)
            mode |= S_IRGRP;
        if (perms & FILE_MODE_GWRITE)
            mode |= S_IWGRP;
        if (perms & FILE_MODE_GEXECUTE)
            mode |= S_IXGRP;

        if (perms & FILE_MODE_WREAD)
            mode |= S_IROTH;
        if (perms & FILE_MODE_WWRITE)
            mode |= S_IWOTH;
        if (perms & FILE_MODE_WEXECUTE)
            mode |= S_IXOTH;

        return mode;
    }

    inline bool open_file(file_handle* fh, const char* filepath, file_mode_t flag, file_mode_t mode = FILE_MODE_DEFAULT)
    {
        file_handle tfh;

        int oflags = 0;

        if ((flag & FILE_OPEN_READ) && (flag & FILE_OPEN_WRITE))
        {
            oflags = O_RDWR;
        }
        else if (flag & FILE_OPEN_READ)
        {
            oflags = O_RDONLY;
        }
        else if (flag & FILE_OPEN_WRITE)
        {
            oflags = O_WRONLY;
        }
        else
        {
            return false;
        }

        if (flag & FILE_OPEN_CREATE)
        {
            oflags |= O_CREAT;
            if (flag & FILE_OPEN_EXCL)
            {
                oflags |= O_EXCL;
            }
        }
        if ((flag & FILE_OPEN_EXCL) && !(flag & FILE_OPEN_CREATE))
        {
            return false;
        }

        if (flag & FILE_OPEN_APPEND)
        {
            oflags |= O_APPEND;
        }
        if (flag & FILE_OPEN_TRUNCATE)
        {
            oflags |= O_TRUNC;
        }

#ifdef O_BINARY
        if (flag & FILE_OPEN_BINARY)
        {
            oflags |= O_BINARY;
        }
#endif

#if defined(_LARGEFILE64_SOURCE)
        oflags |= O_LARGEFILE;
#elif defined(O_LARGEFILE)
        if (flag & FILE_OPEN_LARGEFILE)
        {
            oflags |= O_LARGEFILE;
        }
#endif

        tfh = open(filepath, oflags, parms2mode(mode));

        if (tfh < 0)
        {
            return false;
        }
        else
        {
            *fh = tfh;
            return true;
        }
    }

    template <class T>
    struct seek_selecter
    {
        static inline T seek(int fd, T offset, int origin) { return lseek(fd, (off_t)offset, origin); }
    };
#ifndef __APPLE__
    template <>
    struct seek_selecter<unsigned long long>
    {
        static unsigned long long seek(int fd, unsigned long long offset, int origin) { return lseek64(fd, offset, origin); }
    };
#endif

    inline void seek_offset(file_handle h, std::size_t offset)
    {
        seek_selecter<std::size_t>::seek(h, offset, SEEK_SET);
    }

    inline bool close_file(file_handle h)
    {
        return (0 == ::close(h));
    }

    inline bool flush_file(file_handle h)
    {
        return (0 == ::fsync(h));
    }

    inline bool remove_file(const char* filepath)
    {
        return (0 == ::unlink(filepath));
    }

    inline bool delete_file(const char* filepath)
    {
        return remove_file(filepath);
    }

    inline size_t maxpath() { return (size_t)1024; }
#endif

#ifdef _WIN32

    inline int get_tempdir(char* buffer, size_t size)
    {
        DWORD nsz = ::GetTempPathA((DWORD)size, buffer);
        if (nsz == 0) return -1;
        return nsz;
    }

    inline int get_tempfile(char* buffer, size_t size, const char* tmpdir, const char* prefix)
    {

        if (std::strlen(tmpdir) + 15 > size) return -1;

        if (0 == ::GetTempFileNameA(tmpdir, prefix, 0, buffer)) return -1;

        DeleteFileA(buffer);

        return (int)(std::strlen(buffer) + 1);
    }

    inline int get_tempfile_nodelete(char* buffer, size_t size, const char* tmpdir, const char* prefix)
    {

        if (std::strlen(tmpdir) + 15 > size) return -1;

        if (0 == ::GetTempFileNameA(tmpdir, prefix, 0, buffer)) return -1;

        //DeleteFileA(buffer);

        return (int)(std::strlen(buffer) + 1);
    }

#endif

#if defined(__unix__) | __APPLE__

    static bool test_tempdir(const char* temp_dir)
    {
        std::string tmp(temp_dir);
        tmp += std::string("/kz_tmp.XXXXXX");

        std::vector<char> buffer(tmp.size() + 1);

        std::strcpy(&buffer[0], tmp.c_str());

        int fd = mkstemp(&buffer[0]);

        if (fd == -1) return false;

        unlink(&buffer[0]);
        close(fd);
        return true;
    }

    inline int get_tempdir(char* buffer, size_t size)
    {
        static const char* try_envs[] = {"TMP", "TEMP", "TMPDIR"};
        static const char* try_dirs[] = {"/tmp", "/usr/tmp", "/var/tmp"};

        const char* cp = NULL;

        for (std::size_t i = 0; i < (sizeof(try_envs) / sizeof(const char*)); i++)
        {
            cp = getenv(try_envs[i]);
            if (cp != NULL) break;
        }

        if (cp == NULL)
        {
            for (std::size_t i = 0; i < (sizeof(try_dirs) / sizeof(const char*)); i++)
            {
                if (test_tempdir(try_dirs[i]))
                {
                    cp = try_dirs[i];
                    break;
                }
            }
        }

        if (cp == NULL) return -1;

        size_t sz = std::strlen(cp) + 1;
        if (size < sz) return -1;
        std::strcpy(buffer, cp);
        return sz;
    }

    inline int get_tempfile(char* buffer, size_t size, const char* tmpdir, const char* prefix)
    {
        std::string temp(tmpdir);

        char lc = temp[temp.size() - 1];
        if (lc != '/')
        {
            if (lc != '\\')
            {
                temp.push_back('/');
            }
        }

        temp += std::string(prefix);
        temp += std::string("XXXXXX");

        size_t sz = temp.size() + 1;

        if (sz > size) return -1;

        std::strcpy(buffer, temp.c_str());

        int fd = mkstemp(buffer);

        if (fd == -1) return -1;

        close(fd);
        unlink(buffer);
        return sz;
    }

    inline int get_tempfile_nodelete(char* buffer, size_t size, const char* tmpdir, const char* prefix)
    {
        std::string temp(tmpdir);

        char lc = temp[temp.size() - 1];
        if (lc != '/')
        {
            if (lc != '\\')
            {
                temp.push_back('/');
            }
        }

        temp += std::string(prefix);
        temp += std::string("XXXXXX");

        size_t sz = temp.size() + 1;

        if (sz > size) return -1;

        std::strcpy(buffer, temp.c_str());

        int fd = mkstemp(buffer);

        if (fd == -1) return -1;

        close(fd);
        //unlink(buffer);
        return sz;
    }

#endif

    inline int get_temppath(char* buffer, size_t size, const char* prefix)
    {
        int sz;
        if (0 > get_tempdir(buffer, size)) return -1;
        std::string tmp(buffer);
        if (0 > (sz = get_tempfile(buffer, size, tmp.c_str(), prefix))) return -1;
        return sz;
    }

    inline int get_temppath_nodelete(char* buffer, size_t size, const char* prefix)
    {
        int sz;
        if (0 > get_tempdir(buffer, size)) return -1;
        std::string tmp(buffer);
        if (0 > (sz = get_tempfile_nodelete(buffer, size, tmp.c_str(), prefix))) return -1;
        return sz;
    }
}

#endif
