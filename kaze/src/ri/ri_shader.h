#ifndef RI_SHADER_H
#define RI_SHADER_H

#include "ri.h"
#include "ri_object.h"
#include "ri_options.h"
#include "ri_attributes.h"
#include "ri_classifier.h"
#include "ri_transform.h"
#include "ri_parameters.h"

namespace ri
{

    class ri_shader : public ri_object
    {
    public:
        ri_shader(const ri_classifier* p_cls, RtToken name, RtInt n, RtToken tokens[],
                  RtPointer params[]);
        ~ri_shader();

    public:
        virtual std::string get_name() const { return name_; }
        virtual std::string type() const { return "Shader"; }
        virtual const ri_classifier& get_classifier() const { return clsf_; }
        virtual const ri_parameters& get_parameters() const { return params_; }
        virtual const ri_options& get_options() const { return opts_; }
        virtual const ri_attributes& get_attributes() const { return attr_; }
        virtual const ri_transform& get_transform() const { return trns_; }
        virtual void set_options(const ri_options& opts) { opts_ = opts; }
        virtual void set_attributes(const ri_attributes& attr) { attr_ = attr; }
        virtual void set_transform(const ri_transform& trns) { trns_ = trns; }

    public:
        virtual const ri_parameters::value_type*
        get_parameter(const char* key) const
        {
            return params_.get(key);
        }

    protected:
        std::string name_;
        ri_classifier clsf_;
        ri_parameters params_;
        ri_options opts_;
        ri_attributes attr_;
        ri_transform trns_;
    };
}

#endif
