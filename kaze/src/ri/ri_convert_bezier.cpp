#include "ri_convert_bezier.h"
#include "ri_types.h"

namespace ri
{
    static const char* RI_BILINEAR = "bilinear";
    static const char* RI_BIQUADRATIC = "biqudaratic";
    static const char* RI_BICUBIC = "bicubic";

    static const char* RI_LINEAR = "linear";
    static const char* RI_QUADRATIC = "qudaratic";
    static const char* RI_CUBIC = "cubic";

    static const char* RI_PERIODIC = "periodic";

    static const char* RI_BEZIER = "bezier";
    static const char* RI_BSPLINE = "bspline";
    static const char* RI_CATMULLROM = "catmull-rom";
    static const char* RI_HERMITE = "hermite";
    static const char* RI_POWER = "power";

    static const float RiBezierBasis[4][4] = {
        {-1, 3, -3, 1}, {3, -6, 3, 0}, {-3, 3, 0, 0}, {1, 0, 0, 0}};

    static const float RiBSplineBasis[4][4] = {
        {-1.0f / 6.0f, 3.0f / 6.0f, -3.0f / 6.0f, 1.0f / 6.0f},
        {3.0f / 6.0f, -6.0f / 6.0f, 3.0f / 6.0f, 0.0f / 6.0f},
        {-3.0f / 6.0f, 0.0f / 6.0f, 3.0f / 6.0f, 0.0f / 6.0f},
        {1.0f / 6.0f, 4.0f / 6.0f, 1.0f / 6.0f, 0.0f / 6.0f}};

    static const float RiCatmullRomBasis[4][4] = {
        {-1.0f / 2.0f, 3.0f / 2.0f, -3.0f / 2.0f, 1.0f / 2.0f},
        {2.0f / 2.0f, -5.0f / 2.0f, 4.0f / 2.0f, -1.0f / 2.0f},
        {-1.0f / 2.0f, 0.0f / 2.0f, 1.0f / 2.0f, 0.0f / 2.0f},
        {0.0f / 2.0f, 2.0f / 2.0f, 0.0f / 2.0f, 0.0f / 2.0f}};

    static const float RiHermiteBasis[4][4] = {
        {2, 1, -2, 1}, {-3, -2, 3, -1}, {0, 1, 0, 0}, {1, 0, 0, 0}};

    static const float RiPowerBasis[4][4] = {
        {1, 0, 0, 0}, {0, 1, 0, 0}, {0, 0, 1, 0}, {0, 0, 0, 1}};

    struct TypeNum
    {
        const char* type;
        int num;
    };

    static TypeNum BASIS2NUM[] = {{RI_BEZIER, 0}, // 0
                                  {"bspline", 1},
                                  {"b-spline", 1}, // 1
                                  {"catmull-rom", 2},
                                  {"catmullrom", 2}, // 2
                                  {RI_HERMITE, 3},   // 3
                                  {RI_POWER, 4},     // 4
                                  {NULL, -1}};

    static int GetBasisType(const char* basis)
    {
        int i = 0;
        while (BASIS2NUM[i].type)
        {
            if (strcmp(basis, BASIS2NUM[i].type) == 0)
                return BASIS2NUM[i].num;
            i++;
        }
        return -1;
    }

    static bool GetPeriodic(const char* wrap)
    {
        if (strcmp(RI_PERIODIC, wrap) == 0)
            return true;
        return false;
    }

    static void GetMatrix(float mat[4][4], const char* szMatrix)
    {
        float* matf = (float*)mat;
        const char* szFind = strstr(szMatrix, "[");
        if (!szFind)
        {
            memcpy(matf, (float*)RiBezierBasis, sizeof(float) * 16);
        }
        else
        {
            std::stringstream ss(szFind + 1);
            for (int i = 0; i < 16; i++)
            {
                std::string s;
                ss >> s;
                matf[i] = (float)atof(s.c_str());
            }
        }
    }

    static matrix4f GetBezierConvertMatrix(const float mat[4][4])
    {
        // P = [t^3 t^2 t 1]*Mb*Pp
        // Q = [t^3 t^2 t 1]*Mq*Pq
        // P == Q
        // Mb*Pp = Mq*Pq
        // Pp= ~Bez * Mq * Pq
        // Pp = ~Bez*Mq*Ptm

        // P = [t^3 t^2 t 1]*Mb*~Mb*Mq*Pq
        matrix4f Mb((const float*)RiBezierBasis);
        matrix4f Mq((const float*)mat);
        matrix4f Inv = ~Mb * Mq;
        return Inv;
    }

    template <class T>
    static T mul(const float e[4], const T P[4])
    {
        return e[0] * P[0] + e[1] * P[1] + e[2] * P[2] + e[3] * P[3];
    }

    template <class T>
    static void CovertToBezier(T points[16], const char* ubasis,
                               const char* vbasis)
    {
        int nUBasis = GetBasisType(ubasis);
        int nVBasis = GetBasisType(vbasis);

        if (nUBasis)
        {
            float mat[4][4] = {};
            switch (nUBasis)
            {
            case 0: // RI_BEZIER
                memcpy(mat, RiBezierBasis, sizeof(float) * 16);
                break;
            case 1: // RI_BSPLINE
                memcpy(mat, RiBSplineBasis, sizeof(float) * 16);
                break;
            case 2: // RI_CATMULLROM
                memcpy(mat, RiCatmullRomBasis, sizeof(float) * 16);
                break;
            case 3: // RI_HERMITE
                memcpy(mat, RiHermiteBasis, sizeof(float) * 16);
                break;
            case 4: // RI_POWER
                memcpy(mat, RiPowerBasis, sizeof(float) * 16);
                break;
            default: // user define
                GetMatrix(mat, ubasis);
                break;
            }
            matrix4f Inv = GetBezierConvertMatrix(mat);
            for (int v = 0; v < 4; v++)
            {
                T tmp1[4];
                T tmp2[4];
                for (int u = 0; u < 4; u++)
                {
                    tmp1[u] = points[4 * v + u];
                }
                for (int i = 0; i < 4; i++)
                {
                    float e[] = {Inv[i][0], Inv[i][1], Inv[i][2], Inv[i][3]};
                    tmp2[i] = mul(e, tmp1);
                }
                for (int u = 0; u < 4; u++)
                {
                    points[4 * v + u] = tmp2[u];
                }
            }
        }

        if (nVBasis)
        {
            float mat[4][4] = {};
            switch (nVBasis)
            {
            case 0: // RI_BEZIER
                memcpy(mat, RiBezierBasis, sizeof(float) * 16);
                break;
            case 1: // RI_BSPLINE
                memcpy(mat, RiBSplineBasis, sizeof(float) * 16);
                break;
            case 2: // RI_CATMULLROM
                memcpy(mat, RiCatmullRomBasis, sizeof(float) * 16);
                break;
            case 3: // RI_HERMITE
                memcpy(mat, RiHermiteBasis, sizeof(float) * 16);
                break;
            case 4: // RI_POWER
                memcpy(mat, RiPowerBasis, sizeof(float) * 16);
                break;
            default: // user define
                GetMatrix(mat, vbasis);
                break;
            }

            matrix4f Inv = GetBezierConvertMatrix(mat);
            for (int u = 0; u < 4; u++)
            {
                T tmp1[4];
                T tmp2[4];
                for (int v = 0; v < 4; v++)
                {
                    tmp1[v] = points[4 * v + u];
                }
                for (int i = 0; i < 4; i++)
                {
                    float e[] = {Inv[i][0], Inv[i][1], Inv[i][2], Inv[i][3]};
                    tmp2[i] = mul(e, tmp1);
                }
                for (int v = 0; v < 4; v++)
                {
                    points[4 * v + u] = tmp2[v];
                }
            }
        }
    }

    template <class T>
    static void CovertToBezier(T points[4], const char* vbasis)
    {
        int nVBasis = GetBasisType(vbasis);

        if (nVBasis)
        {
            float mat[4][4] = {};
            switch (nVBasis)
            {
            case 0: // RI_BEZIER
                memcpy(mat, RiBezierBasis, sizeof(float) * 16);
                break;
            case 1: // RI_BSPLINE
                memcpy(mat, RiBSplineBasis, sizeof(float) * 16);
                break;
            case 2: // RI_CATMULLROM
                memcpy(mat, RiCatmullRomBasis, sizeof(float) * 16);
                break;
            case 3: // RI_HERMITE
                memcpy(mat, RiHermiteBasis, sizeof(float) * 16);
                break;
            case 4: // RI_POWER
                memcpy(mat, RiPowerBasis, sizeof(float) * 16);
                break;
            default: // user define
                GetMatrix(mat, vbasis);
                break;
            }

            matrix4f Inv = GetBezierConvertMatrix(mat);
            {
                T tmp1[4];
                T tmp2[4];
                for (int v = 0; v < 4; v++)
                {
                    tmp1[v] = points[v];
                }
                for (int i = 0; i < 4; i++)
                {
                    float e[] = {Inv[i][0], Inv[i][1], Inv[i][2], Inv[i][3]};
                    tmp2[i] = mul(e, tmp1);
                }
                for (int v = 0; v < 4; v++)
                {
                    points[v] = tmp2[v];
                }
            }
        }
    }

    static matrix4f GetToBezierMatrix(const char* basis)
    {
        int nBasis = GetBasisType(basis);

        float mat[4][4] = {};
        switch (nBasis)
        {
        case 0: // RI_BEZIER
            memcpy(mat, RiBezierBasis, sizeof(float) * 16);
            break;
        case 1: // RI_BSPLINE
            memcpy(mat, RiBSplineBasis, sizeof(float) * 16);
            break;
        case 2: // RI_CATMULLROM
            memcpy(mat, RiCatmullRomBasis, sizeof(float) * 16);
            break;
        case 3: // RI_HERMITE
            memcpy(mat, RiHermiteBasis, sizeof(float) * 16);
            break;
        case 4: // RI_POWER
            memcpy(mat, RiPowerBasis, sizeof(float) * 16);
            break;
        default: // user define
            GetMatrix(mat, basis);
            break;
        }
        matrix4f Inv = GetBezierConvertMatrix(mat);

        return Inv;
    }

    template <class T>
    static void CovertToBezier(float points[], int dim, const char* ubasis,
                               const char* vbasis)
    {
        T tmp[16];
        for (int i = 0; i < 16; i++)
        {
            for (int d = 0; d < dim; d++)
            {
                tmp[i][d] = points[dim * i + d];
            }
        }
        CovertToBezier(tmp, ubasis, vbasis);
        for (int i = 0; i < 16; i++)
        {
            for (int d = 0; d < dim; d++)
            {
                points[dim * i + d] = tmp[i][d];
            }
        }
    }

    template <class T>
    static void CovertToBezier(float points[], int dim, const char* vbasis)
    {
        T tmp[4];
        for (int i = 0; i < 4; i++)
        {
            for (int d = 0; d < dim; d++)
            {
                tmp[i][d] = points[dim * i + d];
            }
        }
        CovertToBezier(tmp, vbasis);
        for (int i = 0; i < 4; i++)
        {
            for (int d = 0; d < dim; d++)
            {
                points[dim * i + d] = tmp[i][d];
            }
        }
    }

    void ri_patch_convert_to_bezier(float points[], int dim, const char* ubasis,
                                    const char* vbasis)
    {
        switch (dim)
        {
        case 1:
            CovertToBezier(points, ubasis, vbasis);
            break;
        case 2:
            CovertToBezier<vector2f>(points, 2, ubasis, vbasis);
            break;
        case 3:
            CovertToBezier<vector3f>(points, 3, ubasis, vbasis);
            break;
        case 4:
            CovertToBezier<vector4f>(points, 4, ubasis, vbasis);
            break;
        }
    }

    void ri_curves_convert_to_bezier(float points[], int dim, const char* vbasis)
    {
        switch (dim)
        {
        case 1:
            CovertToBezier(points, vbasis);
            break;
        case 2:
            CovertToBezier<vector2f>(points, 2, vbasis);
            break;
        case 3:
            CovertToBezier<vector3f>(points, 3, vbasis);
            break;
        case 4:
            CovertToBezier<vector4f>(points, 4, vbasis);
            break;
        }
    }
}
