#ifndef RI_TASK_H
#define RI_TASK_H

#include <string>

namespace ri
{
    class ri_task
    {
    public:
        virtual ~ri_task() {}
        virtual std::string type() const = 0;
        virtual int run() { return 0; }
    };
}

#endif
