#include "ri_light_source.h"
#include "ri_geometry.h"

namespace ri
{

    ri_light_source::ri_light_source(const ri_classifier* p_cls, RtToken name,
                                     RtInt n, RtToken tokens[], RtPointer params[])
        : ri_shader(p_cls, name, n, tokens, params)
    {
        ; //
    }

    ri_light_source::~ri_light_source() {}

    ri_area_light_source::ri_area_light_source(const ri_classifier* p_cls,
                                               RtToken name, RtInt n,
                                               RtToken tokens[], RtPointer params[])
        : ri_light_source(p_cls, name, n, tokens, params)
    {
        pGeo_ = new ri_group_geometry();
    }

    ri_area_light_source::~ri_area_light_source() { delete pGeo_; }

    ri_group_geometry* ri_area_light_source::get_group_geometry() const
    {
        return pGeo_;
    }

    void ri_area_light_source::add_geometry(const ri_geometry* pGeo)
    {
        pGeo_->add(pGeo);
    }

    void ri_area_light_source::get_geometry(
        std::vector<const ri_geometry*>& lv) const
    {
        size_t sz = pGeo_->get_size();
        for (size_t i = 0; i < sz; i++)
        {
            lv.push_back(pGeo_->get_at(i));
        }
    }
}
