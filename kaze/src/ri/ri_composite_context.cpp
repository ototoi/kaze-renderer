#include "ri_composite_context.h"

namespace ri
{
    enum
    {
        OBJECT,
        LIGHT,
        ARCHIVE
    };

    RtVoid ri_composite_context::RiVersion(char* version)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiVersion(version);
        }
    }

    RtToken ri_composite_context::RiDeclare(char* name, char* declaration)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiDeclare(name, declaration);
        }
        return ri_context::RiDeclare(name, declaration);
    }
    RtVoid ri_composite_context::RiBegin(RtToken name)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiBegin(name);
        }
    }
    RtVoid ri_composite_context::RiEnd(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiEnd();
        }
    }
    RtVoid ri_composite_context::RiFrameBegin(RtInt frame)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiFrameBegin(frame);
        }
    }
    RtVoid ri_composite_context::RiFrameEnd(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiFrameEnd();
        }
    }
    RtVoid ri_composite_context::RiWorldBegin(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiWorldBegin();
        }
    }
    RtVoid ri_composite_context::RiWorldEnd(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiWorldEnd();
        }
    }
    RtVoid ri_composite_context::RiFormat(RtInt xres, RtInt yres, RtFloat aspect)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiFormat(xres, yres, aspect);
        }
    }
    RtVoid ri_composite_context::RiFrameAspectRatio(RtFloat aspect)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiFrameAspectRatio(aspect);
        }
    }
    RtVoid ri_composite_context::RiScreenWindow(RtFloat left, RtFloat right,
                                                RtFloat bot, RtFloat top)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiScreenWindow(left, right, bot, top);
        }
    }
    RtVoid ri_composite_context::RiCropWindow(RtFloat xmin, RtFloat xmax,
                                              RtFloat ymin, RtFloat ymax)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiCropWindow(xmin, xmax, ymin, ymax);
        }
    }

    RtVoid ri_composite_context::RiProjectionV(RtToken name, RtInt n,
                                               RtToken tokens[],
                                               RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiProjectionV(name, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiClipping(RtFloat hither, RtFloat yon)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiClipping(hither, yon);
        }
    }
    RtVoid ri_composite_context::RiClippingPlane(RtFloat x, RtFloat y, RtFloat z,
                                                 RtFloat nx, RtFloat ny,
                                                 RtFloat nz)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiClippingPlane(x, y, z, nx, ny, nz);
        }
    }
    RtVoid ri_composite_context::RiShutter(RtFloat min, RtFloat max)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiShutter(min, max);
        }
    }
    RtVoid ri_composite_context::RiPixelVariance(RtFloat variation)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiPixelVariance(variation);
        }
    }
    RtVoid ri_composite_context::RiPixelSamples(RtFloat xsamples,
                                                RtFloat ysamples)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiPixelSamples(xsamples, ysamples);
        }
    }
    RtVoid ri_composite_context::RiPixelSampleImagerV(RtToken name, RtInt n,
                                                      RtToken tokens[],
                                                      RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiPixelSampleImagerV(name, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiPixelFilter(RtToken name, RtFloat xwidth,
                                               RtFloat ywidth)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiPixelFilter(name, xwidth, ywidth);
        }
    }
    RtVoid ri_composite_context::RiPixelFilter(RtFilterFunc filterfunc,
                                               RtFloat xwidth, RtFloat ywidth)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiPixelFilter(filterfunc, xwidth, ywidth);
        }
    }
    RtVoid ri_composite_context::RiExposure(RtFloat gain, RtFloat gamma)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiExposure(gain, gamma);
        }
    }

    RtVoid ri_composite_context::RiImagerV(RtToken name, RtInt n, RtToken tokens[],
                                           RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiImagerV(name, n, tokens, params);
        }
    }
    RtVoid ri_composite_context::RiQuantize(RtToken type, RtInt one, RtInt min,
                                            RtInt max, RtFloat ampl)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiQuantize(type, one, min, max, ampl);
        }
    }

    RtVoid ri_composite_context::RiDisplayV(char* name, RtToken type, RtToken mode,
                                            RtInt n, RtToken tokens[],
                                            RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiDisplayV(name, type, mode, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiDisplayChannelV(RtToken channel, RtInt n,
                                                   RtToken tokens[],
                                                   RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiDisplayChannelV(channel, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiHiderV(RtToken type, RtInt n, RtToken tokens[],
                                          RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiHiderV(type, n, tokens, params);
        }
    }
    RtVoid ri_composite_context::RiColorSamples(RtInt n, RtFloat nRGB[],
                                                RtFloat RGBn[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiColorSamples(n, nRGB, RGBn);
        }
    }
    RtVoid ri_composite_context::RiRelativeDetail(RtFloat relativedetail)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiRelativeDetail(relativedetail);
        }
    }

    RtVoid ri_composite_context::RiOptionV(RtToken name, RtInt n, RtToken tokens[],
                                           RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiOptionV(name, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiAttributeBegin(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiAttributeBegin();
        }
    }
    RtVoid ri_composite_context::RiAttributeEnd(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiAttributeEnd();
        }
    }
    RtVoid ri_composite_context::RiColor(RtColor color)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiColor(color);
        }
    }
    RtVoid ri_composite_context::RiOpacity(RtColor color)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiOpacity(color);
        }
    }
    RtVoid ri_composite_context::RiTextureCoordinates(RtFloat s1, RtFloat t1,
                                                      RtFloat s2, RtFloat t2,
                                                      RtFloat s3, RtFloat t3,
                                                      RtFloat s4, RtFloat t4)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiTextureCoordinates(s1, t1, s2, t2, s3, t3, s4, t4);
        }
    }

    RtLightHandle ri_composite_context::RiLightSourceV(RtToken name, RtInt n,
                                                       RtToken tokens[],
                                                       RtPointer params[])
    {
        ObjectType* pLight = new ObjectType();
        pLight->type = LIGHT;
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            pLight->mv.push_back(
                (void*)(ctx_[i]->RiLightSourceV(name, n, tokens, params)));
        }
        obj_.push_back(pLight);

        return pLight;
    }

    RtLightHandle ri_composite_context::RiAreaLightSourceV(RtToken name, RtInt n,
                                                           RtToken tokens[],
                                                           RtPointer params[])
    {
        ObjectType* pLight = new ObjectType();
        pLight->type = LIGHT;

        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            pLight->mv.push_back(
                (void*)(ctx_[i]->RiAreaLightSourceV(name, n, tokens, params)));
        }
        obj_.push_back(pLight);

        return pLight;
    }

    RtVoid ri_composite_context::RiIlluminate(RtInt id, RtBoolean onoff)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiIlluminate(id, onoff);
        }
    }
    RtVoid ri_composite_context::RiIlluminate(RtToken name, RtBoolean onoff)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiIlluminate(name, onoff);
        }
    }

    RtVoid ri_composite_context::RiSurfaceV(RtToken name, RtInt n, RtToken tokens[],
                                            RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiSurfaceV(name, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiAtmosphereV(RtToken name, RtInt n,
                                               RtToken tokens[],
                                               RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiAtmosphereV(name, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiInteriorV(RtToken name, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiInteriorV(name, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiExteriorV(RtToken name, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiExteriorV(name, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiShadingRate(RtFloat size)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiShadingRate(size);
        }
    }

    RtVoid ri_composite_context::RiShadingInterpolation(RtToken type)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiShadingInterpolation(type);
        }
    }

    RtVoid ri_composite_context::RiMatte(RtBoolean onoff)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiMatte(onoff);
        }
    }

    RtVoid ri_composite_context::RiBound(RtBound bound)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiBound(bound);
        }
    }

    RtVoid ri_composite_context::RiDetail(RtBound bound)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiDetail(bound);
        }
    }

    RtVoid ri_composite_context::RiDetailRange(RtFloat minvis, RtFloat lowtran,
                                               RtFloat uptran, RtFloat maxvis)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiDetailRange(minvis, lowtran, uptran, maxvis);
        }
    }

    RtVoid ri_composite_context::RiGeometricApproximation(RtToken type,
                                                          RtFloat value)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiGeometricApproximation(type, value);
        }
    }

    RtVoid ri_composite_context::RiOrientation(RtToken orientation)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiOrientation(orientation);
        }
    }

    RtVoid ri_composite_context::RiReverseOrientation(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiReverseOrientation();
        }
    }

    RtVoid ri_composite_context::RiSides(RtInt sides)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiSides(sides);
        }
    }

    RtVoid ri_composite_context::RiIdentity(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiIdentity();
        }
    }

    RtVoid ri_composite_context::RiTransform(RtMatrix transform)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiTransform(transform);
        }
    }

    RtVoid ri_composite_context::RiConcatTransform(RtMatrix transform)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiConcatTransform(transform);
        }
    }

    RtVoid ri_composite_context::RiPerspective(RtFloat fov)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiPerspective(fov);
        }
    }
    RtVoid ri_composite_context::RiTranslate(RtFloat dx, RtFloat dy, RtFloat dz)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiTranslate(dx, dy, dz);
        }
    }
    RtVoid ri_composite_context::RiRotate(RtFloat angle, RtFloat dx, RtFloat dy,
                                          RtFloat dz)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiRotate(angle, dx, dy, dz);
        }
    }

    RtVoid ri_composite_context::RiScale(RtFloat sx, RtFloat sy, RtFloat sz)
    {
        size_t siz = ctx_.size();
        for (size_t i = 0; i < siz; i++)
        {
            ctx_[i]->RiScale(sx, sy, sz);
        }
    }

    RtVoid ri_composite_context::RiSkew(RtFloat angle, RtFloat dx1, RtFloat dy1,
                                        RtFloat dz1, RtFloat dx2, RtFloat dy2,
                                        RtFloat dz2)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiSkew(angle, dx1, dy1, dz1, dx2, dy2, dz2);
        }
    }

    RtVoid ri_composite_context::RiDeformationV(RtToken name, RtInt n,
                                                RtToken tokens[],
                                                RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiDeformationV(name, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiDisplacementV(RtToken name, RtInt n,
                                                 RtToken tokens[],
                                                 RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiDisplacementV(name, n, tokens, params);
        }
    }
    RtVoid ri_composite_context::RiCoordinateSystem(RtToken space)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiCoordinateSystem(space);
        }
    }
    RtVoid ri_composite_context::RiScopedCoordinateSystem(RtToken space)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiScopedCoordinateSystem(space);
        }
    }
    RtVoid ri_composite_context::RiCoordSysTransform(RtToken space)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiCoordSysTransform(space);
        }
    }
    RtPoint* ri_composite_context::RiTransformPoints(RtToken fromspace,
                                                     RtToken tospace, RtInt n,
                                                     RtPoint points[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiTransformPoints(fromspace, tospace, n, points);
        }
        return points;
    }
    RtVoid ri_composite_context::RiTransformBegin(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiTransformBegin();
        }
    }
    RtVoid ri_composite_context::RiTransformEnd(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiTransformEnd();
        }
    }

    RtVoid ri_composite_context::RiResourceV(RtToken handle, RtToken type, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiResourceV(handle, type, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiResourceBegin(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiResourceBegin();
        }
    }

    RtVoid ri_composite_context::RiResourceEnd(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiResourceEnd();
        }
    }

    RtVoid ri_composite_context::RiAttributeV(RtToken name, RtInt n,
                                              RtToken tokens[],
                                              RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiAttributeV(name, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiPolygonV(RtInt nverts, RtInt n, RtToken tokens[],
                                            RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiPolygonV(nverts, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiGeneralPolygonV(RtInt nloops, RtInt nverts[],
                                                   RtInt n, RtToken tokens[],
                                                   RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiGeneralPolygonV(nloops, nverts, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiPointsPolygonsV(RtInt npolys, RtInt nverts[],
                                                   RtInt verts[], RtInt n,
                                                   RtToken tokens[],
                                                   RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiPointsPolygonsV(npolys, nverts, verts, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiPointsGeneralPolygonsV(
        RtInt npolys, RtInt nloops[], RtInt nverts[], RtInt verts[], RtInt n,
        RtToken tokens[], RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiPointsGeneralPolygonsV(npolys, nloops, nverts, verts, n, tokens,
                                              params);
        }
    }

    RtVoid ri_composite_context::RiBasis(RtToken ubasis, RtInt ustep,
                                         RtToken vbasis, RtInt vstep)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiBasis(ubasis, ustep, vbasis, vstep);
        }
    }
    RtVoid ri_composite_context::RiBasis(RtBasis ubasis, RtInt ustep,
                                         RtToken vbasis, RtInt vstep) // basis token
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiBasis(ubasis, ustep, vbasis, vstep);
        }
    }
    RtVoid ri_composite_context::RiBasis(RtToken ubasis, RtInt ustep,
                                         RtBasis vbasis, RtInt vstep) // token basis
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiBasis(ubasis, ustep, vbasis, vstep);
        }
    }
    RtVoid ri_composite_context::RiBasis(RtBasis ubasis, RtInt ustep,
                                         RtBasis vbasis, RtInt vstep)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiBasis(ubasis, ustep, vbasis, vstep);
        }
    }

    RtVoid ri_composite_context::RiPatchV(RtToken type, RtInt n, RtToken tokens[],
                                          RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiPatchV(type, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiPatchMeshV(RtToken type, RtInt nu, RtToken uwrap,
                                              RtInt nv, RtToken vwrap, RtInt n,
                                              RtToken tokens[],
                                              RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiPatchMeshV(type, nu, uwrap, nv, vwrap, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiNuPatchV(RtInt nu, RtInt uorder, RtFloat uknot[],
                                            RtFloat umin, RtFloat umax, RtInt nv,
                                            RtInt vorder, RtFloat vknot[],
                                            RtFloat vmin, RtFloat vmax, RtInt n,
                                            RtToken tokens[], RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiNuPatchV(nu, uorder, uknot, umin, umax, nv, vorder, vknot, vmin,
                                vmax, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiTrimCurve(RtInt nloops, RtInt ncurves[],
                                             RtInt order[], RtFloat knot[],
                                             RtFloat min[], RtFloat max[],
                                             RtInt n[], RtFloat u[], RtFloat v[],
                                             RtFloat w[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiTrimCurve(nloops, ncurves, order, knot, min, max, n, u, v, w);
        }
    }

    RtVoid ri_composite_context::RiSphereV(RtFloat radius, RtFloat zmin,
                                           RtFloat zmax, RtFloat tmax, RtInt n,
                                           RtToken tokens[], RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiSphereV(radius, zmin, zmax, tmax, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiConeV(RtFloat height, RtFloat radius,
                                         RtFloat tmax, RtInt n, RtToken tokens[],
                                         RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiConeV(height, radius, tmax, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiCylinderV(RtFloat radius, RtFloat zmin,
                                             RtFloat zmax, RtFloat tmax, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiCylinderV(radius, zmin, zmax, tmax, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiHyperboloidV(RtPoint point1, RtPoint point2,
                                                RtFloat tmax, RtInt n,
                                                RtToken tokens[],
                                                RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiHyperboloidV(point1, point2, tmax, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiParaboloidV(RtFloat rmax, RtFloat zmin,
                                               RtFloat zmax, RtFloat tmax, RtInt n,
                                               RtToken tokens[],
                                               RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiParaboloidV(rmax, zmin, zmax, tmax, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiDiskV(RtFloat height, RtFloat radius,
                                         RtFloat tmax, RtInt n, RtToken tokens[],
                                         RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiDiskV(height, radius, tmax, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiTorusV(RtFloat majrad, RtFloat minrad,
                                          RtFloat phimin, RtFloat phimax,
                                          RtFloat tmax, RtInt n, RtToken tokens[],
                                          RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiTorusV(majrad, minrad, phimin, phimax, tmax, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiBlobbyV(RtInt nleaf, RtInt ncode, RtInt code[],
                                           RtInt nflt, RtFloat flt[], RtInt nstr,
                                           RtToken str[], RtInt n, RtToken tokens[],
                                           RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiBlobbyV(nleaf, ncode, code, nflt, flt, nstr, str, n, tokens,
                               params);
        }
    }

    RtVoid ri_composite_context::RiCurvesV(RtToken type, RtInt ncurves,
                                           RtInt nvertices[], RtToken wrap, RtInt n,
                                           RtToken tokens[], RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiCurvesV(type, ncurves, nvertices, wrap, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiPointsV(RtInt nverts, RtInt n, RtToken tokens[],
                                           RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiPointsV(nverts, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiSubdivisionMeshV(
        RtToken mask, RtInt nf, RtInt nverts[], RtInt verts[], RtInt ntags,
        RtToken tags[], RtInt nargs[], RtInt intargs[], RtFloat floatargs[],
        RtInt n, RtToken tokens[], RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiSubdivisionMeshV(mask, nf, nverts, verts, ntags, tags, nargs,
                                        intargs, floatargs, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiHierarchicalSubdivisionMeshV(
        RtToken scheme, RtInt nf, RtInt nverts[], RtInt verts[], RtInt ntags,
        RtToken tags[], RtInt nargs[], RtInt intargs[], RtFloat floatargs[],
        RtString stringargs[], RtInt n, RtToken tokens[], RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiHierarchicalSubdivisionMeshV(scheme, nf, nverts, verts, ntags,
                                                    tags, nargs, intargs, floatargs,
                                                    stringargs, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiProcedural(RtPointer data, RtBound bound,
                                              RtVoid (*subdivfunc)(RtPointer,
                                                                   RtFloat),
                                              RtVoid (*freefunc)(RtPointer))
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiProcedural(data, bound, subdivfunc, freefunc);
        }
    }

    RtVoid ri_composite_context::RiProcedural(RtToken type, RtInt n,
                                              RtToken tokens[], RtBound bound)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiProcedural(type, n, tokens, bound);
        }
    }
    RtVoid ri_composite_context::RiProceduralV(RtToken type, RtInt n0,
                                               RtToken tokens0[], RtBound bound,
                                               RtInt n, RtToken tokens[],
                                               RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiProceduralV(type, n0, tokens0, bound, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiGeometryV(RtToken type, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiGeometryV(type, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiSolidBegin(RtToken operation)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiSolidBegin(operation);
        }
    }
    RtVoid ri_composite_context::RiSolidEnd(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiSolidEnd();
        }
    }
    RtObjectHandle ri_composite_context::RiObjectBegin(void)
    {
        ObjectType* pObj = new ObjectType();
        pObj->type = OBJECT;
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            pObj->mv.push_back((void*)(ctx_[i]->RiObjectBegin()));
        }
        obj_.push_back(pObj);
        return (void*)pObj;
    }
    RtVoid ri_composite_context::RiObjectEnd(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiObjectEnd();
        }
    }

    RtVoid ri_composite_context::RiObjectInstance(RtInt id)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiObjectInstance(id);
        }
    }
    RtVoid ri_composite_context::RiObjectInstance(RtToken name)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiObjectInstance(name);
        }
    }

    RtVoid ri_composite_context::RiMotionBeginV(RtInt n, RtFloat times[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiMotionBeginV(n, times);
        }
    }
    RtVoid ri_composite_context::RiMotionEnd(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiMotionEnd();
        }
    }

    RtVoid ri_composite_context::RiMakeTextureV(char* pic, char* tex, RtToken swrap,
                                                RtToken twrap, RtToken filterfunc,
                                                RtFloat swidth, RtFloat twidth,
                                                RtInt n, RtToken tokens[],
                                                RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiMakeTextureV(pic, tex, swrap, twrap, filterfunc, swidth, twidth,
                                    n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiMakeBumpV(char* pic, char* tex, RtToken swrap,
                                             RtToken twrap, RtToken filterfunc,
                                             RtFloat swidth, RtFloat twidth,
                                             RtInt n, RtToken tokens[],
                                             RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiMakeBumpV(pic, tex, swrap, twrap, filterfunc, swidth, twidth, n,
                                 tokens, params);
        }
    }

    RtVoid ri_composite_context::RiMakeLatLongEnvironmentV(
        char* pic, char* tex, RtToken filterfunc, RtFloat swidth, RtFloat twidth,
        RtInt n, RtToken tokens[], RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiMakeLatLongEnvironmentV(pic, tex, filterfunc, swidth, twidth, n,
                                               tokens, params);
        }
    }

    RtVoid ri_composite_context::RiMakeCubeFaceEnvironmentV(
        char* px, char* nx, char* py, char* ny, char* pz, char* nz, char* tex,
        RtFloat fov, RtToken filterfunc, RtFloat swidth, RtFloat twidth, RtInt n,
        RtToken tokens[], RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiMakeCubeFaceEnvironmentV(px, nx, py, ny, pz, nz, tex, fov,
                                                filterfunc, swidth, twidth, n, tokens,
                                                params);
        }
    }

    RtVoid ri_composite_context::RiMakeShadowV(char* pic, char* tex, RtInt n,
                                               RtToken tokens[],
                                               RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiMakeShadowV(pic, tex, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiMakeBrickMapV(int nptc, char** ptcnames,
                                                 char* bkmname, RtInt n,
                                                 RtToken tokens[],
                                                 RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiMakeBrickMapV(nptc, ptcnames, bkmname, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiArchiveRecord(RtToken type, char* format,
                                                 va_list argptr)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiArchiveRecord(type, format, argptr);
        }
    }

    RtVoid ri_composite_context::RiReadArchiveV(RtToken name,
                                                RtArchiveCallback callback, RtInt n,
                                                RtToken tokens[],
                                                RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiReadArchiveV(name, callback, n, tokens, params);
        }
    }

    RtArchiveHandle ri_composite_context::RiArchiveBeginV(RtToken archivename,
                                                          RtInt n, RtToken tokens[],
                                                          RtPointer params[])
    {
        ObjectType* pHandle = new ObjectType();
        pHandle->type = ARCHIVE;
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            pHandle->mv.push_back(
                (void*)(ctx_[i]->RiArchiveBeginV(archivename, n, tokens, params)));
        }
        obj_.push_back(pHandle);

        return pHandle;
    }
    RtVoid ri_composite_context::RiArchiveEnd(void)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiArchiveEnd();
        }
    }

    RtVoid ri_composite_context::RiErrorHandler(RtToken name)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiErrorHandler(name);
        }
    }

    RtVoid ri_composite_context::RiErrorHandler(RtErrorHandler handler)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiErrorHandler(handler);
        }
    }

    RtVoid ri_composite_context::RiDepthOfField(RtFloat fstop, RtFloat focallength,
                                                RtFloat focaldistance)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiDepthOfField(fstop, focallength, focaldistance);
        }
    }

    RtVoid ri_composite_context::RiMakeOcclusionV(RtInt npics, RtString* picfile,
                                                  RtString shadowfile, RtInt n,
                                                  RtToken tokens[],
                                                  RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiMakeOcclusionV(npics, picfile, shadowfile, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiShaderLayerV(RtToken type, RtToken name,
                                                RtToken layername, RtInt n,
                                                RtToken tokens[],
                                                RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiShaderLayerV(type, name, layername, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiConnectShaderLayers(RtToken type, RtToken layer1,
                                                       RtToken variable1,
                                                       RtToken layer2,
                                                       RtToken variable2)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiConnectShaderLayers(type, layer1, variable1, layer2, variable2);
        }
    }

    RtVoid ri_composite_context::RiIfBegin(RtString condition)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiIfBegin(condition);
        }
    }
    RtVoid ri_composite_context::RiIfEnd()
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiIfEnd();
        }
    }
    RtVoid ri_composite_context::RiElse()
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiElse();
        }
    }
    RtVoid ri_composite_context::RiElseIf(RtString condition)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiElseIf(condition);
        }
    }

    RtVoid ri_composite_context::RiProcedural2(RtToken subdivide2func,
                                               RtToken boundfunc, RtInt n,
                                               RtToken tokens[],
                                               RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiProcedural2(subdivide2func, boundfunc, n, tokens, params);
        }
    }

    RtVoid ri_composite_context::RiSystem(RtString cmd)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiSystem(cmd);
        }
    }

    RtVoid ri_composite_context::RiVolume(RtPointer type, RtBound bounds,
                                          RtInt nvertices[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiVolume(type, bounds, nvertices);
        }
    }

    RtVoid ri_composite_context::RiVolumePixelSamples(RtFloat xsamples,
                                                      RtFloat ysamples)
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiVolumePixelSamples(xsamples, ysamples);
        }
    }

    RtVoid ri_composite_context::RiVPAtmosphereV(RtToken shadername, RtInt n,
                                                 RtToken tokens[],
                                                 RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiVPAtmosphereV(shadername, n, tokens, params);
        }
    }
    RtVoid ri_composite_context::RiVPInteriorV(RtToken shadername, RtInt n,
                                               RtToken tokens[],
                                               RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiVPInteriorV(shadername, n, tokens, params);
        }
    }
    RtVoid ri_composite_context::RiVPSurfaceV(RtToken shadername, RtInt n,
                                              RtToken tokens[],
                                              RtPointer params[])
    {
        size_t sz = ctx_.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiVPSurfaceV(shadername, n, tokens, params);
        }
    }
    //-----------------------------------------------------------------------------
    // RtLightHandle ri_composite_context::RiGetLightHandle(RtInt id);
    // RtLightHandle ri_composite_context::RiGetLightHandle(RtToken name);
    // RtToken       ri_composite_context::RiGetLightName(RtLightHandle light);
    // RtInt         ri_composite_context::RiGetLightID  (RtLightHandle light);
    RtVoid ri_composite_context::RiSetLightHandle(RtInt id, RtLightHandle light)
    {
        ri_context::RiSetLightHandle(id, light);
        ObjectType* pObj = (ObjectType*)light;
        size_t sz = pObj->mv.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiSetLightHandle(id, pObj->mv[i]);
        }
    }
    RtVoid ri_composite_context::RiSetLightHandle(RtToken name,
                                                  RtLightHandle light)
    {
        ri_context::RiSetLightHandle(name, light);
        ObjectType* pObj = (ObjectType*)light;
        size_t sz = pObj->mv.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiSetLightHandle(name, pObj->mv[i]);
        }
    }

    // RtObjectHandle RiGetObjectHandle(RtInt id);
    // RtObjectHandle RiGetObjectHandle(RtToken name);
    // RtToken        ri_composite_context::RiGetObjectName(RtObjectHandle obj);
    // RtInt          ri_composite_context::RiGetObjectID  (RtObjectHandle obj);
    RtVoid ri_composite_context::RiSetObjectHandle(RtInt id, RtObjectHandle obj)
    {
        ri_context::RiSetObjectHandle(id, obj);
        ObjectType* pObj = (ObjectType*)obj;
        size_t sz = pObj->mv.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiSetObjectHandle(id, pObj->mv[i]);
        }
    }
    RtVoid ri_composite_context::RiSetObjectHandle(RtToken name,
                                                   RtObjectHandle obj)
    {
        ri_context::RiSetObjectHandle(name, obj);
        ObjectType* pObj = (ObjectType*)obj;
        size_t sz = pObj->mv.size();
        for (size_t i = 0; i < sz; i++)
        {
            ctx_[i]->RiSetObjectHandle(name, pObj->mv[i]);
        }
    }
    //-----------------------------------------------------------------------------
    ri_composite_context::ri_composite_context() {}
    ri_composite_context::ri_composite_context(const std::vector<ri_context*>& ctx)
        : ctx_(ctx) {}
    void ri_composite_context::add(ri_context* ctx) { ctx_.push_back(ctx); }
    ri_context* ri_composite_context::get_at(size_t i) { return ctx_[i]; }

    ri_composite_context::~ri_composite_context()
    {
        for (size_t i = 0; i < ctx_.size(); i++)
        {
            delete ctx_[i];
        }
        for (size_t i = 0; i < obj_.size(); i++)
        {
            delete obj_[i];
        }
    }
}
