#ifndef RI_ENV_H
#define RI_ENV_H

#include <map>
#include <string>

namespace ri
{

    class ri_env
    {
    public:
        static bool get(const char* key1, std::string& val);
        static std::string parse(const char* str);
    };
}

#endif
