#ifndef RI_ATTRIBUTES_H
#define RI_ATTRIBUTES_H

#include <set>
#include <map>
#include <string>
#include <vector>

#include "ri_parameters.h"

namespace ri
{

    class ri_shader;
    class ri_light_source;
    class ri_trim_curve_geometry;
    class ri_coordinate_system;
    class ri_geometry;
    class ri_group_geometry;
    class ri_attributes
    {
    public:
        typedef const char* LPCSTR;

    public:
        ri_attributes();
        ri_attributes(const ri_attributes& rhs);
        ~ri_attributes();
        ri_attributes& operator=(const ri_attributes& rhs);
        void swap(ri_attributes& rhs);

    public:
        bool set(const char* key1, const char* key2, int val);
        bool set(const char* key1, const char* key2, float val);
        bool set(const char* key1, const char* key2, const char* val);
        bool set(const char* key1, const char* key2, const int* val, int n);
        bool set(const char* key1, const char* key2, const float* val, int n);
        bool set(const char* key1, const char* key2, const LPCSTR* val, int n);

    public:
        bool get(const char* key1, const char* key2, int& val) const;
        bool get(const char* key1, const char* key2, float& val) const;
        bool get(const char* key1, const char* key2, std::string& val) const;
        bool get(const char* key1, const char* key2, std::vector<int>& val) const;
        bool get(const char* key1, const char* key2, std::vector<float>& val) const;
        bool get(const char* key1, const char* key2,
                 std::vector<std::string>& val) const;

    public:
        bool set_surface(const ri_shader* s);
        bool del_surface();
        bool set_displacement(const ri_shader* s);
        bool del_displacement();
        bool set_atmosphere(const ri_shader* s);
        bool del_atmosphere();
        bool set_interior(const ri_shader* s);
        bool del_interior();
        bool set_exterior(const ri_shader* s);
        bool del_exterior();

    public:
        const ri_shader* get_suraface() const;
        const ri_shader* get_displacement() const;
        const ri_shader* get_atmosphere() const;
        const ri_shader* get_interior() const;
        const ri_shader* get_exterior() const;

    public:
        bool add_light_source(const ri_light_source* l);
        bool del_light_source(const ri_light_source* l);
        bool get_lightsource(std::vector<const ri_light_source*>& lv) const;

    public:
        bool add_trim_curve(const ri_trim_curve_geometry* c);
        bool del_trim_curve(const ri_trim_curve_geometry* c);
        bool get_trim_curve(std::vector<const ri_trim_curve_geometry*>& lv) const;

    public:
        bool add_coordinate_system(const ri_coordinate_system* c);
        bool del_coordinate_system(const ri_coordinate_system* c);
        bool
        get_coordinate_system(std::vector<const ri_coordinate_system*>& cv) const;
        const ri_coordinate_system* get_coordinate_system(const char* key) const;

    public:
        void push_group(ri_group_geometry* grp);
        void pop_group();
        ri_group_geometry* top_group();
        void clear_group();

    private:
        ri_parameters* map_;
        std::vector<const ri_shader*> surface_;
        std::vector<const ri_shader*> displacement_;
        std::vector<const ri_shader*> atmosphere_;
        std::vector<const ri_shader*> interior_;
        std::vector<const ri_shader*> exterior_;
        std::set<const ri_light_source*> lights_;
        std::set<const ri_trim_curve_geometry*> curves_;
        std::map<std::string, const ri_coordinate_system*> coords_;
        std::vector<ri_group_geometry*> grps_;
    };
}

#endif
