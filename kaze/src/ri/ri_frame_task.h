#ifndef RI_FRAME_TASK_H
#define RI_FRAME_TASK_H

#include "ri_task.h"
#include "ri_options.h"
#include <vector>
#include <string>

namespace ri
{
    class ri_object;
    class ri_geometry;
    class ri_options;

    class ri_frame_task : public ri_task
    {
    public:
        virtual std::string type() const { return "Frame"; }

    public:
        virtual void set_options(const ri_options& opts) { opts_ = opts; }
        virtual const ri_options& get_options() const { return opts_; }

    public:
        void add_geometry(ri_geometry* geo);
        size_t get_geometry_size() const;
        ri_geometry* get_geomoetry_at(size_t i) const;

    public:
        virtual int run();

    protected:
        std::vector<ri_geometry*> geos_;
        ri_options opts_;
    };
}

#endif
