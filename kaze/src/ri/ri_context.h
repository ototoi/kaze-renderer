#ifndef RI_CONTEXT_H
#define RI_CONTEXT_H

#include "ri.h"
#include "ri_classifier.h"
#include "ri_mapper.h"
#include <stdarg.h>

namespace ri
{
    class ri_context
    {
    public:
        ri_context();
        ri_context(const ri_classifier& cls);
        virtual ~ri_context();

    public:
        // virtual RtFloat RiGaussianFilter(RtFloat x, RtFloat y, RtFloat xwidth,
        // RtFloat ywidth);
        // virtual RtFloat RiBoxFilter(RtFloat x, RtFloat y, RtFloat xwidth, RtFloat
        // ywidth);
        // virtual RtFloat RiTriangleFilter(RtFloat x, RtFloat y, RtFloat xwidth,
        // RtFloat ywidth);
        // virtual RtFloat RiCatmullRomFilter(RtFloat x, RtFloat y, RtFloat xwidth,
        // RtFloat ywidth);
        // virtual RtFloat RiSincFilter(RtFloat x, RtFloat y, RtFloat xwidth, RtFloat
        // ywidth);
        // virtual RtVoid RiErrorIgnore(RtInt code, RtInt severity, char *msg);
        // virtual RtVoid RiErrorPrint(RtInt code, RtInt severity, char *msg);
        // virtual RtVoid RiErrorAbort(RtInt code, RtInt severity, char *msg);
        // virtual RtVoid RiProcDelayedReadArchive(RtPointer data, RtFloat detail);
        // virtual RtVoid RiProcRunProgram(RtPointer data, RtFloat detail);
        // virtual RtVoid RiProcDynamicLoad(RtPointer data, RtFloat detail);
        // virtual RtContextHandle RiGetContext(void);
        // virtual RtVoid RiContext(RtContextHandle);
        virtual RtVoid RiVersion(float version);
        virtual RtVoid RiVersion(char* version);
        //
        virtual RtToken RiDeclare(char* name, char* declaration);
        virtual RtVoid RiBegin(RtToken name);
        virtual RtVoid RiEnd(void);
        virtual RtVoid RiFrameBegin(RtInt frame);
        virtual RtVoid RiFrameEnd(void);
        virtual RtVoid RiWorldBegin(void);
        virtual RtVoid RiWorldEnd(void);
        virtual RtVoid RiFormat(RtInt xres, RtInt yres, RtFloat aspect);
        virtual RtVoid RiFrameAspectRatio(RtFloat aspect);
        virtual RtVoid RiScreenWindow(RtFloat left, RtFloat right, RtFloat bot,
                                      RtFloat top);
        virtual RtVoid RiCropWindow(RtFloat xmin, RtFloat xmax, RtFloat ymin,
                                    RtFloat ymax);
        virtual RtVoid RiProjection(RtToken name, va_list argptr);
        virtual RtVoid RiProjectionV(RtToken name, RtInt n, RtToken tokens[],
                                     RtPointer params[]);
        virtual RtVoid RiClipping(RtFloat hither, RtFloat yon);
        virtual RtVoid RiClippingPlane(RtFloat x, RtFloat y, RtFloat z, RtFloat nx,
                                       RtFloat ny, RtFloat nz);
        virtual RtVoid RiShutter(RtFloat min, RtFloat max);
        virtual RtVoid RiPixelVariance(RtFloat variation);
        virtual RtVoid RiPixelSamples(RtFloat xsamples, RtFloat ysamples);

        virtual RtVoid RiPixelSampleImager(RtToken name, va_list argptr);
        virtual RtVoid RiPixelSampleImagerV(RtToken name, RtInt n, RtToken tokens[],
                                            RtPointer params[]);

        virtual RtVoid RiPixelFilter(RtToken name, RtFloat xwidth,
                                     RtFloat ywidth); /*extension*/
        virtual RtVoid RiPixelFilter(RtFilterFunc filterfunc, RtFloat xwidth,
                                     RtFloat ywidth);

        virtual RtVoid RiExposure(RtFloat gain, RtFloat gamma);
        virtual RtVoid RiImager(RtToken name, va_list argptr);
        virtual RtVoid RiImagerV(RtToken name, RtInt n, RtToken tokens[],
                                 RtPointer params[]);
        virtual RtVoid RiQuantize(RtToken type, RtInt one, RtInt min, RtInt max,
                                  RtFloat ampl);
        virtual RtVoid RiDisplay(char* name, RtToken type, RtToken mode,
                                 va_list argptr);
        virtual RtVoid RiDisplayV(char* name, RtToken type, RtToken mode, RtInt n,
                                  RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiDisplayChannel(RtToken channel, va_list argptr);
        virtual RtVoid RiDisplayChannelV(RtToken channel, RtInt n, RtToken tokens[],
                                         RtPointer params[]);
        virtual RtVoid RiHider(RtToken type, va_list argptr);
        virtual RtVoid RiHiderV(RtToken type, RtInt n, RtToken tokens[],
                                RtPointer params[]);
        virtual RtVoid RiColorSamples(RtInt n, RtFloat nRGB[], RtFloat RGBn[]);
        virtual RtVoid RiRelativeDetail(RtFloat relativedetail);
        virtual RtVoid RiOption(RtToken name, va_list argptr);
        virtual RtVoid RiOptionV(RtToken name, RtInt n, RtToken tokens[],
                                 RtPointer params[]);
        virtual RtVoid RiAttributeBegin(void);
        virtual RtVoid RiAttributeEnd(void);
        virtual RtVoid RiColor(RtColor color);
        virtual RtVoid RiOpacity(RtColor color);
        virtual RtVoid RiTextureCoordinates(RtFloat s1, RtFloat t1, RtFloat s2,
                                            RtFloat t2, RtFloat s3, RtFloat t3,
                                            RtFloat s4, RtFloat t4);
        virtual RtLightHandle RiLightSource(RtToken name, va_list argptr);
        virtual RtLightHandle RiLightSourceV(RtToken name, RtInt n, RtToken tokens[],
                                             RtPointer params[]);
        virtual RtLightHandle RiAreaLightSource(RtToken name, va_list argptr);
        virtual RtLightHandle RiAreaLightSourceV(RtToken name, RtInt n,
                                                 RtToken tokens[],
                                                 RtPointer params[]);

        virtual RtVoid RiIlluminate(RtInt id, RtBoolean onoff);
        virtual RtVoid RiIlluminate(RtToken name, RtBoolean onoff);
        virtual RtVoid RiIlluminate(RtLightHandle light, RtBoolean onoff);

        virtual RtVoid RiSurface(RtToken name, va_list argptr);
        virtual RtVoid RiSurfaceV(RtToken name, RtInt n, RtToken tokens[],
                                  RtPointer params[]);
        virtual RtVoid RiAtmosphere(RtToken name, va_list argptr);
        virtual RtVoid RiAtmosphereV(RtToken name, RtInt n, RtToken tokens[],
                                     RtPointer params[]);
        virtual RtVoid RiInterior(RtToken name, va_list argptr);
        virtual RtVoid RiInteriorV(RtToken name, RtInt n, RtToken tokens[],
                                   RtPointer params[]);
        virtual RtVoid RiExterior(RtToken name, va_list argptr);
        virtual RtVoid RiExteriorV(RtToken name, RtInt n, RtToken tokens[],
                                   RtPointer params[]);
        virtual RtVoid RiShadingRate(RtFloat size);
        virtual RtVoid RiShadingInterpolation(RtToken type);
        virtual RtVoid RiMatte(RtBoolean onoff);
        virtual RtVoid RiBound(RtBound bound);
        virtual RtVoid RiDetail(RtBound bound);
        virtual RtVoid RiDetailRange(RtFloat minvis, RtFloat lowtran, RtFloat uptran,
                                     RtFloat maxvis);
        virtual RtVoid RiGeometricApproximation(RtToken type, RtFloat value);
        virtual RtVoid RiOrientation(RtToken orientation);
        virtual RtVoid RiReverseOrientation(void);
        virtual RtVoid RiSides(RtInt sides);
        virtual RtVoid RiIdentity(void);
        virtual RtVoid RiTransform(RtMatrix transform);
        virtual RtVoid RiConcatTransform(RtMatrix transform);
        virtual RtVoid RiPerspective(RtFloat fov);
        virtual RtVoid RiTranslate(RtFloat dx, RtFloat dy, RtFloat dz);
        virtual RtVoid RiRotate(RtFloat angle, RtFloat dx, RtFloat dy, RtFloat dz);
        virtual RtVoid RiScale(RtFloat sx, RtFloat sy, RtFloat sz);
        virtual RtVoid RiSkew(RtFloat angle, RtFloat dx1, RtFloat dy1, RtFloat dz1,
                              RtFloat dx2, RtFloat dy2, RtFloat dz2);
        virtual RtVoid RiDeformation(RtToken name, va_list argptr);
        virtual RtVoid RiDeformationV(RtToken name, RtInt n, RtToken tokens[],
                                      RtPointer params[]);
        virtual RtVoid RiDisplacement(RtToken name, va_list argptr);
        virtual RtVoid RiDisplacementV(RtToken name, RtInt n, RtToken tokens[],
                                       RtPointer params[]);
        virtual RtVoid RiCoordinateSystem(RtToken space);
        virtual RtVoid RiScopedCoordinateSystem(RtToken space);
        virtual RtVoid RiCoordSysTransform(RtToken space);
        virtual RtPoint* RiTransformPoints(RtToken fromspace, RtToken tospace,
                                           RtInt n, RtPoint points[]);
        virtual RtVoid RiTransformBegin(void);
        virtual RtVoid RiTransformEnd(void);

        virtual RtVoid RiResource(RtToken handle, RtToken type, va_list argptr);
        virtual RtVoid RiResourceV(RtToken handle, RtToken type, RtInt n,
                                   RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiResourceBegin(void);
        virtual RtVoid RiResourceEnd(void);

        virtual RtVoid RiAttribute(RtToken name, va_list argptr);
        virtual RtVoid RiAttributeV(RtToken name, RtInt n, RtToken tokens[],
                                    RtPointer params[]);
        virtual RtVoid RiPolygon(RtInt nverts, va_list argptr);
        virtual RtVoid RiPolygonV(RtInt nverts, RtInt n, RtToken tokens[],
                                  RtPointer params[]);
        virtual RtVoid RiGeneralPolygon(RtInt nloops, RtInt nverts[], va_list argptr);
        virtual RtVoid RiGeneralPolygonV(RtInt nloops, RtInt nverts[], RtInt n,
                                         RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiPointsPolygons(RtInt npolys, RtInt nverts[], RtInt verts[],
                                        va_list argptr);
        virtual RtVoid RiPointsPolygonsV(RtInt npolys, RtInt nverts[], RtInt verts[],
                                         RtInt n, RtToken tokens[],
                                         RtPointer params[]);
        virtual RtVoid RiPointsGeneralPolygons(RtInt npolys, RtInt nloops[],
                                               RtInt nverts[], RtInt verts[],
                                               va_list argptr);
        virtual RtVoid RiPointsGeneralPolygonsV(RtInt npolys, RtInt nloops[],
                                                RtInt nverts[], RtInt verts[],
                                                RtInt n, RtToken tokens[],
                                                RtPointer params[]);

        virtual RtVoid RiBasis(RtToken ubasis, RtInt ustep, RtToken vbasis,
                               RtInt vstep); // token token
        virtual RtVoid RiBasis(RtBasis ubasis, RtInt ustep, RtToken vbasis,
                               RtInt vstep); // basis token
        virtual RtVoid RiBasis(RtToken ubasis, RtInt ustep, RtBasis vbasis,
                               RtInt vstep); // token basis
        virtual RtVoid RiBasis(RtBasis ubasis, RtInt ustep, RtBasis vbasis,
                               RtInt vstep); // basis basis

        virtual RtVoid RiPatch(RtToken type, va_list argptr);
        virtual RtVoid RiPatchV(RtToken type, RtInt n, RtToken tokens[],
                                RtPointer params[]);
        virtual RtVoid RiPatchMesh(RtToken type, RtInt nu, RtToken uwrap, RtInt nv,
                                   RtToken vwrap, va_list argptr);
        virtual RtVoid RiPatchMeshV(RtToken type, RtInt nu, RtToken uwrap, RtInt nv,
                                    RtToken vwrap, RtInt n, RtToken tokens[],
                                    RtPointer params[]);
        virtual RtVoid RiNuPatch(RtInt nu, RtInt uorder, RtFloat uknot[],
                                 RtFloat umin, RtFloat umax, RtInt nv, RtInt vorder,
                                 RtFloat vknot[], RtFloat vmin, RtFloat vmax,
                                 va_list argptr);
        virtual RtVoid RiNuPatchV(RtInt nu, RtInt uorder, RtFloat uknot[],
                                  RtFloat umin, RtFloat umax, RtInt nv, RtInt vorder,
                                  RtFloat vknot[], RtFloat vmin, RtFloat vmax,
                                  RtInt n, RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiTrimCurve(RtInt nloops, RtInt ncurves[], RtInt order[],
                                   RtFloat knot[], RtFloat min[], RtFloat max[],
                                   RtInt n[], RtFloat u[], RtFloat v[], RtFloat w[]);
        virtual RtVoid RiSphere(RtFloat radius, RtFloat zmin, RtFloat zmax,
                                RtFloat tmax, va_list argptr);
        virtual RtVoid RiSphereV(RtFloat radius, RtFloat zmin, RtFloat zmax,
                                 RtFloat tmax, RtInt n, RtToken tokens[],
                                 RtPointer params[]);
        virtual RtVoid RiCone(RtFloat height, RtFloat radius, RtFloat tmax,
                              va_list argptr);
        virtual RtVoid RiConeV(RtFloat height, RtFloat radius, RtFloat tmax, RtInt n,
                               RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiCylinder(RtFloat radius, RtFloat zmin, RtFloat zmax,
                                  RtFloat tmax, va_list argptr);
        virtual RtVoid RiCylinderV(RtFloat radius, RtFloat zmin, RtFloat zmax,
                                   RtFloat tmax, RtInt n, RtToken tokens[],
                                   RtPointer params[]);
        virtual RtVoid RiHyperboloid(RtPoint point1, RtPoint point2, RtFloat tmax,
                                     va_list argptr);
        virtual RtVoid RiHyperboloidV(RtPoint point1, RtPoint point2, RtFloat tmax,
                                      RtInt n, RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiParaboloid(RtFloat rmax, RtFloat zmin, RtFloat zmax,
                                    RtFloat tmax, va_list argptr);
        virtual RtVoid RiParaboloidV(RtFloat rmax, RtFloat zmin, RtFloat zmax,
                                     RtFloat tmax, RtInt n, RtToken tokens[],
                                     RtPointer params[]);
        virtual RtVoid RiDisk(RtFloat height, RtFloat radius, RtFloat tmax,
                              va_list argptr);
        virtual RtVoid RiDiskV(RtFloat height, RtFloat radius, RtFloat tmax, RtInt n,
                               RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiTorus(RtFloat majrad, RtFloat minrad, RtFloat phimin,
                               RtFloat phimax, RtFloat tmax, va_list argptr);
        virtual RtVoid RiTorusV(RtFloat majrad, RtFloat minrad, RtFloat phimin,
                                RtFloat phimax, RtFloat tmax, RtInt n,
                                RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiBlobby(RtInt nleaf, RtInt ncode, RtInt code[], RtInt nflt,
                                RtFloat flt[], RtInt nstr, RtToken str[],
                                va_list argptr);
        virtual RtVoid RiBlobbyV(RtInt nleaf, RtInt ncode, RtInt code[], RtInt nflt,
                                 RtFloat flt[], RtInt nstr, RtToken str[], RtInt n,
                                 RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiCurves(RtToken type, RtInt ncurves, RtInt nvertices[],
                                RtToken wrap, va_list argptr);
        virtual RtVoid RiCurvesV(RtToken type, RtInt ncurves, RtInt nvertices[],
                                 RtToken wrap, RtInt n, RtToken tokens[],
                                 RtPointer params[]);
        virtual RtVoid RiPoints(RtInt nverts, va_list argptr);
        virtual RtVoid RiPointsV(RtInt nverts, RtInt n, RtToken tokens[],
                                 RtPointer params[]);
        virtual RtVoid RiSubdivisionMesh(RtToken mask, RtInt nf, RtInt nverts[],
                                         RtInt verts[], RtInt ntags, RtToken tags[],
                                         RtInt numargs[], RtInt intargs[],
                                         RtFloat floatargs[], va_list argptr);
        virtual RtVoid RiSubdivisionMeshV(RtToken mask, RtInt nf, RtInt nverts[],
                                          RtInt verts[], RtInt ntags, RtToken tags[],
                                          RtInt nargs[], RtInt intargs[],
                                          RtFloat floatargs[], RtInt n,
                                          RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiHierarchicalSubdivisionMesh(
            RtToken scheme, RtInt nfaces, RtInt nvertices[], RtInt vertices[],
            RtInt ntags, RtToken tags[], RtInt nargs[], RtInt intargs[],
            RtFloat floatargs[], RtString stringargs[], va_list argptr);
        virtual RtVoid RiHierarchicalSubdivisionMeshV(
            RtToken scheme, RtInt nfaces, RtInt nvertices[], RtInt vertices[],
            RtInt ntags, RtToken tags[], RtInt nargs[], RtInt intargs[],
            RtFloat floatargs[], RtString stringargs[], RtInt n, RtToken tokens[],
            RtPointer params[]);

        virtual RtVoid RiProcedural(RtPointer data, RtBound bound,
                                    RtVoid (*subdivfunc)(RtPointer, RtFloat),
                                    RtVoid (*freefunc)(RtPointer));
        virtual RtVoid RiProcedural(RtToken type, RtInt n, RtToken tokens[],
                                    RtBound bound);
        virtual RtVoid RiProceduralV(RtToken type, RtInt n0, RtToken tokens0[],
                                     RtBound bound, RtInt n, RtToken tokens[],
                                     RtPointer params[]);

        virtual RtVoid RiGeometry(RtToken type, va_list argptr);
        virtual RtVoid RiGeometryV(RtToken type, RtInt n, RtToken tokens[],
                                   RtPointer params[]);
        virtual RtVoid RiSolidBegin(RtToken operation);
        virtual RtVoid RiSolidEnd(void);
        virtual RtObjectHandle RiObjectBegin(void);
        virtual RtVoid RiObjectEnd(void);

        virtual RtVoid RiObjectInstance(RtInt id);
        virtual RtVoid RiObjectInstance(RtToken name);
        virtual RtVoid RiObjectInstance(RtObjectHandle handle);

        virtual RtVoid RiMotionBegin(RtInt n, va_list argptr);
        virtual RtVoid RiMotionBeginV(RtInt n, RtFloat times[]);
        virtual RtVoid RiMotionEnd(void);

        virtual RtVoid RiMakeTexture(char* pic, char* tex, RtToken swrap,
                                     RtToken twrap, RtFilterFunc filterfunc,
                                     RtFloat swidth, RtFloat twidth, va_list argptr);
        virtual RtVoid RiMakeTextureV(char* pic, char* tex, RtToken swrap,
                                      RtToken twrap, RtFilterFunc filterfunc,
                                      RtFloat swidth, RtFloat twidth, RtInt n,
                                      RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiMakeTextureV(char* pic, char* tex, RtToken swrap,
                                      RtToken twrap, RtToken filterfunc,
                                      RtFloat swidth, RtFloat twidth, RtInt n,
                                      RtToken tokens[], RtPointer params[]); /*token*/

        virtual RtVoid RiMakeBump(char* pic, char* tex, RtToken swrap, RtToken twrap,
                                  RtFilterFunc filterfunc, RtFloat swidth,
                                  RtFloat twidth, va_list argptr);
        virtual RtVoid RiMakeBumpV(char* pic, char* tex, RtToken swrap, RtToken twrap,
                                   RtFilterFunc filterfunc, RtFloat swidth,
                                   RtFloat twidth, RtInt n, RtToken tokens[],
                                   RtPointer params[]);
        virtual RtVoid RiMakeBumpV(char* pic, char* tex, RtToken swrap, RtToken twrap,
                                   RtToken filterfunc, RtFloat swidth, RtFloat twidth,
                                   RtInt n, RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiMakeLatLongEnvironment(char* pic, char* tex,
                                                RtFilterFunc filterfunc,
                                                RtFloat swidth, RtFloat twidth,
                                                va_list argptr);
        virtual RtVoid RiMakeLatLongEnvironmentV(char* pic, char* tex,
                                                 RtFilterFunc filterfunc,
                                                 RtFloat swidth, RtFloat twidth,
                                                 RtInt n, RtToken tokens[],
                                                 RtPointer params[]);
        virtual RtVoid RiMakeLatLongEnvironmentV(char* pic, char* tex,
                                                 RtToken filterfunc, RtFloat swidth,
                                                 RtFloat twidth, RtInt n,
                                                 RtToken tokens[],
                                                 RtPointer params[]);

        virtual RtVoid RiMakeCubeFaceEnvironment(char* px, char* nx, char* py,
                                                 char* ny, char* pz, char* nz,
                                                 char* tex, RtFloat fov,
                                                 RtFilterFunc filterfunc,
                                                 RtFloat swidth, RtFloat twidth,
                                                 va_list argptr);
        virtual RtVoid RiMakeCubeFaceEnvironmentV(
            char* px, char* nx, char* py, char* ny, char* pz, char* nz, char* tex,
            RtFloat fov, RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth,
            RtInt n, RtToken tokens[], RtPointer params[]);
        virtual RtVoid
        RiMakeCubeFaceEnvironmentV(char* px, char* nx, char* py, char* ny, char* pz,
                                   char* nz, char* tex, RtFloat fov,
                                   RtToken filterfunc, RtFloat swidth, RtFloat twidth,
                                   RtInt n, RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiMakeShadow(char* pic, char* tex, va_list argptr);
        virtual RtVoid RiMakeShadowV(char* pic, char* tex, RtInt n, RtToken tokens[],
                                     RtPointer params[]);

        virtual RtVoid RiMakeBrickMap(int nptc, char** ptcnames, char* bkmname,
                                      va_list argptr);
        virtual RtVoid RiMakeBrickMapV(int nptc, char** ptcnames, char* bkmname,
                                       RtInt n, RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiArchiveRecord(RtToken type, char* format, va_list argptr);
        virtual RtVoid RiReadArchive(RtToken name, RtArchiveCallback callback,
                                     va_list argptr);
        virtual RtVoid RiReadArchiveV(RtToken name, RtArchiveCallback callback,
                                      RtInt n, RtToken tokens[], RtPointer params[]);
        virtual RtArchiveHandle RiArchiveBegin(RtToken archivename, va_list argptr);
        virtual RtArchiveHandle RiArchiveBeginV(RtToken archivename, RtInt n,
                                                RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiArchiveEnd(void);

        virtual RtVoid RiErrorHandler(RtToken name);
        virtual RtVoid RiErrorHandler(RtErrorHandler handler); // for output

        virtual RtVoid RiDepthOfField(RtFloat fstop, RtFloat focallength,
                                      RtFloat focaldistance);
        virtual RtVoid RiMakeOcclusion(RtInt npics, RtString* picfile,
                                       RtString shadowfile, va_list argptr);
        virtual RtVoid RiMakeOcclusionV(RtInt npics, RtString* picfile,
                                        RtString shadowfile, RtInt count,
                                        RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiShaderLayer(RtToken type, RtToken name, RtToken layername,
                                     va_list argptr);
        virtual RtVoid RiShaderLayerV(RtToken type, RtToken name, RtToken layername,
                                      RtInt count, RtToken tokens[],
                                      RtPointer values[]);
        virtual RtVoid RiConnectShaderLayers(RtToken type, RtToken layer1,
                                             RtToken variable1, RtToken layer2,
                                             RtToken variable2);

        virtual RtVoid RiIfBegin(RtString condition);
        virtual RtVoid RiIfEnd();
        virtual RtVoid RiElse();
        virtual RtVoid RiElseIf(RtString condition);

        virtual RtVoid RiProcedural2(RtToken subdivide2func, RtToken boundfunc,
                                     RtInt n, RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiProcedural2(RtProc2SubdivFunc subdivide2func,
                                     RtProc2BoundFunc boundfunc, RtInt n,
                                     RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiSystem(RtString cmd); // TODO

        virtual RtVoid RiVolume(RtPointer type, RtBound bounds, RtInt nvertices[]);
        virtual RtVoid RiVolumePixelSamples(RtFloat xsamples, RtFloat ysamples);
        virtual RtVoid RiVPAtmosphere(RtToken shadername, va_list argptr);
        virtual RtVoid RiVPAtmosphereV(RtToken shadername, RtInt n, RtToken tokens[],
                                       RtPointer params[]);
        virtual RtVoid RiVPInterior(RtToken shadername, va_list argptr);
        virtual RtVoid RiVPInteriorV(RtToken shadername, RtInt n, RtToken tokens[],
                                     RtPointer params[]);
        virtual RtVoid RiVPSurface(RtToken shadername, va_list argptr);
        virtual RtVoid RiVPSurfaceV(RtToken shadername, RtInt n, RtToken tokens[],
                                    RtPointer params[]);

        virtual RtVoid RiInstancer(RtToken name, va_list argptr);
        virtual RtVoid RiInstancerV(RtToken name, RtInt n, RtToken tokens[],
                                    RtPointer params[]);

        virtual RtVoid RiProcedureV(RtToken name, RtBound bound, RtInt n,
                                    RtToken tokens[], RtPointer params[]);

        virtual RtVoid RiIntegratorV(RtToken name, RtToken handle, RtInt n,
                                     RtToken tokens[], RtPointer params[]);
        virtual RtVoid RiBxdfV(RtToken type, RtToken name, RtInt n, RtToken tokens[],
                               RtPointer params[]);
        virtual RtVoid RiPatternV(RtToken type, RtToken name, RtInt n,
                                  RtToken tokens[], RtPointer params[]);

    public:
        virtual RtFilterFunc RiGetFilterFunc(RtToken name);
        virtual RtToken RiGetFilterFunc(RtFilterFunc func);

        virtual RtLightHandle RiGetLightHandle(RtInt id);
        virtual RtLightHandle RiGetLightHandle(RtToken name);
        virtual RtToken RiGetLightName(RtLightHandle light);
        virtual RtInt RiGetLightID(RtLightHandle light);
        virtual RtVoid RiSetLightHandle(RtInt id, RtLightHandle light);
        virtual RtVoid RiSetLightHandle(RtToken name, RtLightHandle light);

        virtual RtObjectHandle RiGetObjectHandle(RtInt id);
        virtual RtObjectHandle RiGetObjectHandle(RtToken name);
        virtual RtToken RiGetObjectName(RtObjectHandle obj);
        virtual RtInt RiGetObjectID(RtObjectHandle obj);
        virtual RtVoid RiSetObjectHandle(RtInt id, RtObjectHandle obj);
        virtual RtVoid RiSetObjectHandle(RtToken name, RtObjectHandle obj);

    public:
        virtual ri_classifier* RiGetClassifier();
        virtual const ri_classifier* RiGetClassifier() const;

    protected:
        ri_classifier* p_classifier_;
        ri_mapper* p_lightmap_;
        ri_mapper* p_objectmap_;
    };
}

#endif
