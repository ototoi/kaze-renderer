#ifndef RI_CAMERA_OPTIONS_H
#define RI_CAMERA_OPTIONS_H

#include "ri_options.h"
#include "ri_object.h"
#include "ri_transform.h"

namespace ri
{

    class ri_camera_options : public ri_object
    {
    public:
        enum
        {
            RI_CAMERA_PERSPECTIVE,
            RI_CAMERA_ORTHOGRAPHIC
        };

    public:
        ri_camera_options(const ri_options& opts,
                          const ri_transform& trns = ri_transform());
        int get_camera_type() const;

    public:
        ri_transform get_world_to_camera() const;
        ri_transform get_camera_to_world() const;

    public:
        bool is_motion() const;
        size_t get_motion_size() const;
        float get_time_at(size_t i) const;
        ri_transform get_world_to_camera_at(size_t i) const;
        ri_transform get_camera_to_world_at(size_t i) const;

    public:
        std::string get_display_name() const;

    public:
        float get_fov() const;

    public:
        float get_frame_aspect_ratio() const;
        void get_format(int& xres, int& yres, float& aspect) const;
        void get_screen_window(float& left, float& right, float& bottom,
                               float& top) const;
        void get_crop_window(float& xmin, float& xmax, float& ymin,
                             float& ymax) const;
        void get_clipping(float& near, float& far) const;
        void get_pixel_samples(int& xsamples, int& ysamples) const;

    public:
        bool get_depth_of_field(float& fstop, float& focallength,
                                float& focaldistance) const;

    public:
        size_t get_display_size() const { return opts_.get_display_size(); }
        const ri_display* get_display_at(size_t i) const
        {
            return opts_.get_display_at(i);
        }

    public:
        void get_fullscreen_window(float& left, float& right, float& bottom,
                                   float& top) const;

    protected:
        bool get_screen_window_raw(float& left, float& right, float& bottom,
                                   float& top) const;

    protected:
        ri_options opts_;
        ri_transform trns_;
    };
    
}

#endif
