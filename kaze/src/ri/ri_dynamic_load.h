#ifndef RI_DYNAMIC_LOAD_H
#define RI_DYNAMIC_LOAD_H

namespace ri
{
    class ri_context;
    int ri_dynamic_load(ri_context* ctx, const char* program, float area, const char* arg);
}

#endif