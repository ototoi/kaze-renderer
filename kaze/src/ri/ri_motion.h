#ifndef RI_MOTION_H
#define RI_MOTION_H

#include "ri.h"
#include "ri_object.h"
#include "ri_transform.h"
#include <vector>

namespace ri
{
    class ri_geometry;

    class ri_motion : public ri_object
    {
    public:
        ri_motion(int n, const float times[],
                  const ri_transform& trans = ri_transform());
        ~ri_motion();

    public:
        bool is_transform() const;
        bool is_geometry() const;
        bool is_completed() const;

    public:
        void identity();
        void set(const float mat[4 * 4]);
        void concat(const float mat[4 * 4]);
        void translate(float x, float y, float z);
        void scale(float x, float y, float z);
        void rotate(float angle, float dx, float dy, float dz);
        void skew(float angle, float dx1, float dy1, float dz1, float dx2, float dy2,
                  float dz2);
        void perspective(float fov);

    public:
        int get_n() const;
        float get_time_at(size_t) const;
        ri_transform get_transform_at(size_t i) const;
        const ri_geometry* get_geometry_at(size_t i) const;

    public:
        void add_geometry(const ri_geometry* geo);

    protected:
        int i_;
        int j_;
        bool bTransform_;
        std::vector<float> times_;
        std::vector<ri_transform> transes_;
        std::vector<const ri_geometry*> geos_;
    };
}

#endif
