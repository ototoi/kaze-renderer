#include "ri_rib_decoder.h"
#include <algorithm>

#ifdef _MSC_VER
//'_splitpath': This function or variable may be unsafe.
#pragma warning(disable : 4996)
#endif

#define HAVE_ZLIB

#ifdef HAVE_ZLIB
#include <zlib.h>
#endif

#ifdef __GNUC__
#include <stdint.h>
#elif defined(_MSC_VER)
#if _MSC_VER >= 1600
#include <stdint.h>
#else
typedef char int8_t;
typedef short int16_t;
typedef __int32 int32_t;
typedef __int64 int64_t;

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;
#endif
#endif

namespace ri
{

    static bool CheckBinary(const char* buffer, unsigned int sz)
    {
        bool bComment = false;
        bool bLineComment = false;
        for (unsigned int i = 0; i < sz; i++)
        {
            unsigned char c = buffer[i];
            if (!bLineComment)
            {
                if (!bComment)
                {
                    if (c == '\"')
                    {
                        bComment = true;
                    }
                    else
                    {
                        if (0200 <= c && c < 0377)
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    if (c == '\"')
                    {
                        bComment = false;
                    }
                }
            }
            else
            {
                if (c == '\r' || c == '\n')
                {
                    bLineComment = false;
                }
            }
        }
        return false;
    }

    static int ReadRaw(FILE* fp, char* buffer, unsigned int size)
    {
#ifdef HAVE_ZLIB
        return gzread((gzFile)fp, buffer, size);
#else
        return fread(buffer, 1, (size_t)size, fp);
#endif
    }

    static int GetChar(FILE* fp)
    {
#ifdef HAVE_ZLIB
        return gzgetc((gzFile)fp);
#else
        return getc(fp);
#endif
    }

    std::string ri_rib_decoder::string_filter(const char* buffer)
    {
        std::string temp(buffer);
        std::string s = temp.substr(1, temp.size() - 2);
        return s;
    }

    ri_rib_decoder::ri_rib_decoder(FILE* fp)
    {
        fp_ = fp;
        bBinary_ = false;
        char buffer[128];
        int nRet = ReadRaw(fp, buffer, 128);
        if (nRet >= 0)
        {
            bBinary_ = CheckBinary(buffer, nRet);
        }
#ifdef HAVE_ZLIB
        gzrewind((gzFile)fp);
#else
        rewind(fp);
#endif
    }

    ri_rib_decoder::~ri_rib_decoder()
    {
        ; // Don't release
    }

    static int ToInteger(FILE* fp, int sz)
    {
        int result = 0;
        switch (sz)
        {
        case 4:
            result = (unsigned char)(GetChar(fp)) << 24;
        case 3:
            result += (unsigned char)(GetChar(fp)) << 16;
        case 2:
            result += (unsigned char)(GetChar(fp)) << 8;
        case 1:
            result += (unsigned char)(GetChar(fp));
        }
        return result;
    }

    static std::string ToString(int i)
    {
        char buffer[64];
        sprintf(buffer, "%d", i);
        return buffer;
    }

    static std::string ToString(float f)
    {
        char buffer[64];
        sprintf(buffer, "%.6f", f);
        return buffer;
    }

    static std::string ToString(double f)
    {
        char buffer[64];
        sprintf(buffer, "%.6lf", f);
        return buffer;
    }

    static std::string ToFixedPoint(FILE* fp, int n, int r)
    {
        int mag = ToInteger(fp, n - r);
        int frac = ToInteger(fp, std::min<int>(r, n));
        double f = mag + static_cast<double>(frac) / (1 << (8 * r));
        return ToString(f);
    }

    static std::string ToString(FILE* fp, int n)
    {
        std::string str = "";
        for (int i = 0; i < n; i++)
        {
            unsigned char uc = GetChar(fp);
            str += (char)(uc);
        }
        return str;
    }

    static float ToFloat32(FILE* fp)
    {
        // union to avoid illegal type-punning
        union FloatInt32
        {
            float f;
            unsigned int i;
        } conv;
        conv.i = ToInteger(fp, 4);
        return conv.f;
    }

    static double ToFloat64(FILE* fp)
    {
        // union to avoid illegal type-punning
        union FloatInt64
        {
            double d;
            uint64_t i;
        } conv;
        conv.i = static_cast<uint64_t>(ToInteger(fp, 4)) << 32;
        conv.i += ToInteger(fp, 4);
        return conv.d;
    }

    std::string ri_rib_decoder::next_token()
    {
        std::string strRet = "";
        int ci = 0;
        bool inString = false;
        while ((ci = GetChar(fp_)) != EOF)
        {
            unsigned char c = ci;
            if (c <= 0x7f)
            {
                switch (c)
                {
                case '"':
                {
                    if (!inString)
                    {
                        inString = true;
                    }
                    else
                    {
                        return strRet;
                    }
                }
                break;
                default:
                    strRet.push_back((char)c);
                }
            }
            else if (0200 <= c && c < 0377)
            {
                switch (c)
                {
                case 0220:
                case 0221:
                case 0222:
                case 0223:
                case 0224:
                case 0225:
                case 0226:
                case 0227:
                case 0230:
                case 0231:
                case 0232:
                case 0233:
                case 0234:
                case 0235:
                case 0236:
                case 0237:
                { // strings of no more than 15 characters
                    return ToString(fp_, c - 0220);
                }
                break;
                case 0240:
                case 0241:
                case 0242:
                case 0243:
                { // strings longer than 15 characters
                    int nlength = ToInteger(fp_, c - 0240 + 1);
                    return ToString(fp_, nlength);
                }
                break;
                default:
                {
                    strRet.push_back((char)c);
                }
                }
            }
            else
            {
                strRet.push_back((char)c);
            }
        }
        return strRet;
    }

    int ri_rib_decoder::read(char* buffer, unsigned int size)
    {
        if (!bBinary_)
        {
            return ReadRaw(fp_, buffer, size);
        }
        else
        {
#ifdef _DEBUG
            memset(buffer, 0, size);
#endif
            int ci = 0;
            unsigned int i = 0;
            if (!buff_.empty())
            {
                int sz = std::min<int>(size, (int)buff_.size());
                for (int j = 0; j < sz; j++)
                {
                    buffer[i + j] = buff_[j];
                }
                buff_.erase(buff_.begin(), buff_.begin() + sz);
                i += sz;
            }
            while (i < size && ((ci = GetChar(fp_)) != EOF))
            {
                unsigned char c = ci;
                if (c <= 0x7f)
                {
                    switch (c)
                    {
                    case '[':
                    {
                        std::string s;
                        s += ' ';
                        s += c;
                        // s += ' ';
                        int sz = (int)s.size();
                        int rsz = std::min<int>(sz, size - i);
                        for (int j = 0; j < rsz; j++)
                        {
                            buffer[i + j] = s[j];
                        }
                        for (int j = rsz; j < sz; j++)
                        {
                            buff_.push_back(s[j]);
                        }
                        i += rsz;
                    }
                    break;
                    case ']':
                    {
                        std::string s;
                        s += ' ';
                        s += c;
                        s += ' ';
                        int sz = (int)s.size();
                        int rsz = std::min<int>(sz, size - i);
                        for (int j = 0; j < rsz; j++)
                        {
                            buffer[i + j] = s[j];
                        }
                        for (int j = rsz; j < sz; j++)
                        {
                            buff_.push_back(s[j]);
                        }
                        i += rsz;
                    }
                    break;
                    default:
                    {
                        buffer[i] = (char)c;
                        i++;
                    }
                    }
                }
                else if (0200 <= c && c < 0377)
                {
                    switch (c)
                    {
                    case 0200:
                    case 0201:
                    case 0202:
                    case 0203:
                    { // integers
                        int n = ToInteger(fp_, c - 0200 + 1);
                        std::string strn = " " + ToString(n);
                        int sz = (int)strn.size();
                        int rsz = std::min<int>(sz, size - i);
                        for (int j = 0; j < rsz; j++)
                        {
                            buffer[i + j] = strn[j];
                        }
                        for (int j = rsz; j < sz; j++)
                        {
                            buff_.push_back(strn[j]);
                        }
                        i += rsz;
                    }
                    break;
                    case 0204:
                    case 0205:
                    case 0206:
                    case 0207:
                    case 0210:
                    case 0211:
                    case 0212:
                    case 0213:
                    case 0214:
                    case 0215:
                    case 0216:
                    case 0217:
                    { // fixed-point numbers
                        std::string f = ToFixedPoint(fp_, ((c - 0200) & 0x03) + 1,
                                                     ((c - 0200) >> 2) & 0x03);
                        std::string strn = " " + f;
                        int sz = (int)strn.size();
                        int rsz = std::min<int>(sz, size - i);
                        for (int j = 0; j < rsz; j++)
                        {
                            buffer[i + j] = strn[j];
                        }
                        for (int j = rsz; j < sz; j++)
                        {
                            buff_.push_back(strn[j]);
                        }
                        i += rsz;
                    }
                    break;
                    case 0220:
                    case 0221:
                    case 0222:
                    case 0223:
                    case 0224:
                    case 0225:
                    case 0226:
                    case 0227:
                    case 0230:
                    case 0231:
                    case 0232:
                    case 0233:
                    case 0234:
                    case 0235:
                    case 0236:
                    case 0237:
                    { // strings of no more than 15 characters
                        std::string s = ToString(fp_, c - 0220);
                        std::string strn = " \"" + s + "\"";
                        int sz = (int)strn.size();
                        int rsz = std::min<int>(sz, size - i);
                        for (int j = 0; j < rsz; j++)
                        {
                            buffer[i + j] = strn[j];
                        }
                        for (int j = rsz; j < sz; j++)
                        {
                            buff_.push_back(strn[j]);
                        }
                        i += rsz;
                    }
                    break;
                    case 0240:
                    case 0241:
                    case 0242:
                    case 0243:
                    { // strings longer than 15 characters
                        int nlength = ToInteger(fp_, c - 0240 + 1);
                        std::string s = ToString(fp_, nlength);
                        std::string strn = " " + s;
                        int sz = (int)strn.size();
                        int rsz = std::min<int>(sz, size - i);
                        for (int j = 0; j < rsz; j++)
                        {
                            buffer[i + j] = strn[j];
                        }
                        for (int j = rsz; j < sz; j++)
                        {
                            buff_.push_back(strn[j]);
                        }
                        i += rsz;
                    }
                    break;
                    case 0244:
                    { // single precision IEEE floating point value
                        float f = ToFloat32(fp_);
                        std::string strn = " " + ToString(f);
                        int sz = (int)strn.size();
                        int rsz = std::min<int>(sz, size - i);
                        for (int j = 0; j < rsz; j++)
                        {
                            buffer[i + j] = strn[j];
                        }
                        for (int j = rsz; j < sz; j++)
                        {
                            buff_.push_back(strn[j]);
                        }
                        i += rsz;
                    }
                    break;
                    case 0245:
                    { // double precision IEEE floating point value
                        double f = ToFloat64(fp_);
                        std::string strn = " " + ToString(f);
                        int sz = (int)strn.size();
                        int rsz = std::min<int>(sz, size - i);
                        for (int j = 0; j < rsz; j++)
                        {
                            buffer[i + j] = strn[j];
                        }
                        for (int j = rsz; j < sz; j++)
                        {
                            buff_.push_back(strn[j]);
                        }
                        i += rsz;
                    }
                    break;
                    case 0246:
                    {                                      // RI request
                        unsigned char code = GetChar(fp_); // code
                        std::string s = requests_[code];
                        std::string strn = "\n" + s;
                        int sz = (int)strn.size();
                        int rsz = std::min<int>(sz, size - i);
                        for (int j = 0; j < rsz; j++)
                        {
                            buffer[i + j] = strn[j];
                        }
                        for (int j = rsz; j < sz; j++)
                        {
                            buff_.push_back(strn[j]);
                        }
                        i += rsz;
                    }
                    break;
                    // 0247-0307
                    //
                    case 0310:
                    case 0311:
                    case 0312:
                    case 0313:
                    { // single precision array
                        int nlength = ToInteger(fp_, c - 0310 + 1);
                        std::vector<float> floats(nlength);
                        // ReadRaw(fp_, (char*)&floats[0], sizeof(float)*nlength);
                        for (int j = 0; j < nlength; j++)
                        {
                            floats[j] = ToFloat32(fp_);
                        }
                        std::string strn = " [ ";
                        for (int j = 0; j < nlength; j++)
                        {
                            strn += ToString(floats[j]);
                            if (j != nlength - 1)
                                strn += " ";
                        }
                        strn += " ] ";

                        int sz = (int)strn.size();
                        int rsz = std::min<int>(sz, size - i);
                        for (int j = 0; j < rsz; j++)
                        {
                            buffer[i + j] = strn[j];
                        }
                        for (int j = rsz; j < sz; j++)
                        {
                            buff_.push_back(strn[j]);
                        }
                        i += rsz;
                    }
                    break;
                    case 0314:
                    {                                      // define encoded request
                        unsigned char code = GetChar(fp_); // code
                        std::string s = next_token();
                        requests_[code] = s;
                    }
                    break;
                    case 0315:
                    case 0316:
                    { // define encoded string token
                        int code = ToInteger(fp_, c - 0315 + 1);
                        std::string s = next_token();
                        strings_[code] = s;
                    }
                    break;
                    case 0317:
                    case 0320:
                    { // interpolate defined string
                        int code = ToInteger(fp_, c - 0317 + 1);
                        std::string s = strings_[code];
                        std::string strn = " \"" + s + "\"";
                        int sz = (int)strn.size();
                        int rsz = std::min<int>(sz, size - i);
                        for (int j = 0; j < rsz; j++)
                        {
                            buffer[i + j] = strn[j];
                        }
                        for (int j = rsz; j < sz; j++)
                        {
                            buff_.push_back(strn[j]);
                        }
                        i += rsz;
                    }
                    break;
                    default:
                    {     // nothing (reserved)
                        ; //
                        putc('@', stdin);
                    }
                    break;
                    }
                }
                else
                {
                    buffer[i] = (char)c;
                    i++;
                }

                /*
              const char* cc = strstr(buffer, "vis");
              {
                  if(cc){
                      int dummy = 0;
                  }
              }
              */
            }
            return i;
        }
    }
}
