#include "ri_options.h"
#include "ri.h"
#include "ri_display.h"
#include "ri_camera_options.h"
#include "ri_config.h"
#include "ri_env.h"

#include <stdlib.h>
#include <string.h>
#include <memory>

namespace ri
{
    typedef const char* LPCSTR;

    static std::string get_key(const char* key1, const char* key2)
    {
        std::string str = key1;
        str += ':';
        str += key2;
        return str;
    }

#define INIT_PARAM(KEY1, KEY2, VAL) para->set(get_key(KEY1, KEY2).c_str(), VAL)
#define INIT_PARAM_A(KEY1, KEY2, VAL, N) \
    para->set(get_key(KEY1, KEY2).c_str(), VAL, N)

    static void init_parameters(ri_parameters* para)
    {
        // Camera ri_options
        //-----------------------------------------------------------
        INIT_PARAM("Format", "xres", 640);
        INIT_PARAM("Format", "yres", 480);
        INIT_PARAM("Format", "aspect", 1.0f); // pixel aspcet

        // INIT_PARAM("FrameAspectRatio","aspect", 4.0f/3.0f);

        // INIT_PARAM("ScreenWindow","left",  -4.0f/3.0f);
        // INIT_PARAM("ScreenWindow","right", +4.0f/3.0f);
        // INIT_PARAM("ScreenWindow","top",   -1.0f);
        // INIT_PARAM("ScreenWindow","bottom",+1.0f);

        INIT_PARAM("CropWindow", "xmin", 0.0f);
        INIT_PARAM("CropWindow", "xmax", 1.0f);
        INIT_PARAM("CropWindow", "ymin", 0.0f);
        INIT_PARAM("CropWindow", "ymax", 1.0f);

        INIT_PARAM("Projection", "name", RI_ORTHOGRAPHIC);
        INIT_PARAM("Projection", "fov", 90.0f);

        INIT_PARAM("Clipping", "near", (RtFloat)RI_EPSILON);
        INIT_PARAM("Clipping", "far", (RtFloat)RI_INFINITY);

        INIT_PARAM("ClippingPlane", "x", 0.0f);
        INIT_PARAM("ClippingPlane", "y", 0.0f);
        INIT_PARAM("ClippingPlane", "z", 0.0f);
        INIT_PARAM("ClippingPlane", "nx", 0.0f);
        INIT_PARAM("ClippingPlane", "ny", 0.0f);
        INIT_PARAM("ClippingPlane", "nz", 1.0f);

        INIT_PARAM("Shutter", "min", 0.0f);
        INIT_PARAM("Shutter", "max", 0.0f);

        // INIT_PARAM("DepthOfField","fstop", (RtFloat)RI_INFINITY);
        // INIT_PARAM("DepthOfField","focallength", (RtFloat)RI_INFINITY);
        // INIT_PARAM("DepthOfField","focaldistance", (RtFloat)RI_INFINITY);

        // Display ri_options
        //-----------------------------------------------------------
        INIT_PARAM("PixelVariance", "variation", 1.0f);

        INIT_PARAM("PixelFilter", "name", "catmull-rom");
        INIT_PARAM("PixelFilter", "xwidth", 2.0f);
        INIT_PARAM("PixelFilter", "ywidth", 2.0f);

        INIT_PARAM("PixelSamples", "xsamples", 1.0f);
        INIT_PARAM("PixelSamples", "ysamples", 1.0f);

        INIT_PARAM("Explosure", "gain", 1.0f);
        INIT_PARAM("Explosure", "gamma", 1.0f); // 2.2

        INIT_PARAM("Imager", "name", "null");

        INIT_PARAM("Quantize", "one", 255);
        INIT_PARAM("Quantize", "min", 0);
        INIT_PARAM("Quantize", "max", 255);
        INIT_PARAM("Quantize", "ampl", 0.5f);

        INIT_PARAM("Display", "name", "");
        INIT_PARAM("Display", "type", "frame-buffer");
        INIT_PARAM("Display", "mode", "bmp");

        // Additional options
        //-----------------------------------------------------------
        INIT_PARAM("Hider", "type", "hidden");
        INIT_PARAM("Hider", "subpixel", 1);
        INIT_PARAM("Hider", "mpcache", 1);
        INIT_PARAM("Hider", "maxvpdepth", -1);
        {
            float values[] = {2, 0, 0, 0}; // nsides, angle, roundness,
            para->set(get_key("Hider", "aperture").c_str(), values, 4);
        }

        INIT_PARAM("Hider", "jitter", 1);
        INIT_PARAM("Hider", "dofaspect", 1.0f);
        INIT_PARAM("Hider", "extrememotiondof", 1);
        INIT_PARAM("Hider", "sigma", 0);
        INIT_PARAM("Hider", "sigmablur", 1.0f);

        INIT_PARAM("Hider", "samplemode", "fixed");
        INIT_PARAM("Hider", "minsamples", 1);
        INIT_PARAM("Hider", "maxsamples", 256);
        INIT_PARAM("Hider", "incremental", 1);
        INIT_PARAM("Hider", "integrationmode", "path"); // path
        //-----------------------------------------------------------

        {
            static const float nRGB[] = {1, 1, 1};
            para->set(get_key("ColorSamples", "nRGB").c_str(), nRGB, 3);
            para->set(get_key("ColorSamples", "RGBn").c_str(), nRGB, 3);
        }

        INIT_PARAM("RelativeDetail", "relativedetail", 0.0f);

        //-----------------------------------------------------------
        INIT_PARAM("dice", "maxhairlength", -1);

        //-----------------------------------------------------------
        INIT_PARAM("bucket", "order", "auto");

        //-----------------------------------------------------------
        INIT_PARAM("shading", "directlightingsamples", 16);
        INIT_PARAM("shading", "debug", 0);
        INIT_PARAM("shading", "derivsfollowdicing", 0);
        INIT_PARAM("shading", "checknans", 0);
        INIT_PARAM("shading", "defcache", 0.0f);
        INIT_PARAM("shading", "objectcache", 1.5f);
        //-----------------------------------------------------------
        INIT_PARAM("shadow", "bias", 1e-3f);
        // INIT_PARAM("shadow", "depthfilter", "max");

        INIT_PARAM("limits", "geocachememory", 204800);
        INIT_PARAM("limits", "hemispheresamplememory", 10240);
        INIT_PARAM("limits", "radiositycachememory", 102400);

        //-----------------------------------------------------------
        INIT_PARAM("curve", "orienttotransform", -1);
        //-----------------------------------------------------------
        std::string str;
        {
            static const char* names[] = {
                "shader", "texture", "archive", "procedural",
                "display", "resource", "servershader", "servertexture",
                "serverarchive", "serverdisplay", "serversorce", NULL};
            int i = 0;
            while (names[i])
            {
                std::string confname = std::string("/") + names[i] + std::string("path");
                if (ri_config::get(confname.c_str(), str))
                {
                    INIT_PARAM("searchpath", names[i], str.c_str());
                }
                else
                {
                    INIT_PARAM("searchpath", names[i], ".:@");
                }
                i++;
            }
        }

        if (ri_config::get("/prman/hider/mpcachedir", str))
        {
            INIT_PARAM("Hider", "mpcachedir", str.c_str());
        }
        if (ri_config::get("/prman/ptexturememory ", str))
        {
            INIT_PARAM("limits", "ptexturememory", atoi(str.c_str()));
        }
        if (ri_config::get("/prman/ptexturemaxfiles ", str))
        {
            INIT_PARAM("limits", "ptexturemaxfiles", atoi(str.c_str()));
        }
        if (ri_config::get("/prman/pointmemory ", str))
        {
            INIT_PARAM("limits", "pointmemory", atoi(str.c_str()));
        }
        if (ri_config::get("/prman/octreememory ", str))
        {
            INIT_PARAM("limits", "octreememory", atoi(str.c_str()));
        }
        if (ri_config::get("prman/polygon/nonplanar ", str))
        {
            INIT_PARAM("polygon", "nonplanar", atoi(str.c_str()));
        }
        if (ri_env::get("RIPRECISION", str))
        {
            INIT_PARAM("rib", "precision", atoi(str.c_str()));
        }
        else
        {
            INIT_PARAM("rib", "precision", 6);
        }
    }

    static const char* szNeedEvaluateKey[] = {
        "searchpath", NULL,
    };

    static bool NeedEvaluateKey(const char* szKey)
    {
        int i = 0;
        while (szNeedEvaluateKey[i])
        {
            if (strcmp(szKey, szNeedEvaluateKey[i]) == 0)
                return true;
            i++;
        }
        return false;
    }

    bool ri_options::set(const char* key1, const char* key2, int val)
    {
        std::string str = get_key(key1, key2);
        return map_->set(str.c_str(), val);
    }
    bool ri_options::set(const char* key1, const char* key2, float val)
    {
        std::string str = get_key(key1, key2);
        return map_->set(str.c_str(), val);
    }
    bool ri_options::set(const char* key1, const char* key2, const char* val)
    {
        if (!NeedEvaluateKey(key1))
        {
            std::string str = get_key(key1, key2);
            return map_->set(str.c_str(), val);
        }
        else
        {
            if (strcmp(key1, "searchpath") == 0)
            {
                return set_path(key1, key2, val);
            }
            return true;
        }
    }

    static std::string replace_options_path(const std::string& prev,
                                            const std::string& path)
    {
        std::string str = path;
        std::string::size_type start = 0;
        while ((start = str.find("&")) != std::string::npos)
        {
            str = str.substr(0, start) + prev + str.substr(start + 1);
        }

        return str;
    }

    bool ri_options::set_path(const char* key1, const char* key2, const char* val)
    {
        std::string str = get_key(key1, key2);
        std::string prev;
        if (!this->get(key1, key2, prev))
            return false;
        std::string path = replace_options_path(prev, val);
        return map_->set(str.c_str(), path.c_str());
    }

    bool ri_options::set(const char* key1, const char* key2, const int* val,
                         int n)
    {
        std::string str = get_key(key1, key2);
        return map_->set(str.c_str(), val, n);
    }
    bool ri_options::set(const char* key1, const char* key2, const float* val,
                         int n)
    {
        std::string str = get_key(key1, key2);
        return map_->set(str.c_str(), val, n);
    }
    bool ri_options::set(const char* key1, const char* key2, const LPCSTR* val,
                         int n)
    {
        if (n == 1)
        {
            return this->set(key1, key2, val[0]);
        }
        else
        {
            std::string str = get_key(key1, key2);
            return map_->set(str.c_str(), val, n);
        }
    }

    ri_options::ri_options()
    {
        map_ = new ri_parameters();
        init_parameters(map_);
    }
    ri_options::ri_options(const ri_options& rhs)
    {
        map_ = new ri_parameters(*(rhs.map_));
        cmrs_ = rhs.cmrs_;
        disps_ = rhs.disps_;
    }
    ri_options::~ri_options() { delete map_; }
    ri_options& ri_options::operator=(const ri_options& rhs)
    {
        ri_options tmp(rhs);
        tmp.swap(*this);
        return *this;
    }
    void ri_options::swap(ri_options& rhs)
    {
        map_->swap(*(rhs.map_));
        cmrs_.swap(rhs.cmrs_);
        disps_.swap(rhs.disps_);
    }

    bool ri_options::get(const char* key1, const char* key2, int& val) const
    {
        std::string str = get_key(key1, key2);
        const ri_parameters::value_type* pRet = map_->get(str.c_str());
        if (pRet)
        {
            if (pRet->nType != ri_parameters::TYPE_INTEGER)
                return false;
            if (pRet->integer_values.size() != 1)
                return false;
            val = pRet->integer_values[0];
            return true;
        }
        else
        {
            return false;
        }
    }
    bool ri_options::get(const char* key1, const char* key2, float& val) const
    {
        std::string str = get_key(key1, key2);
        const ri_parameters::value_type* pRet = map_->get(str.c_str());
        if (pRet)
        {
            if (pRet->nType != ri_parameters::TYPE_FLOAT)
                return false;
            if (pRet->float_values.size() != 1)
                return false;
            val = pRet->float_values[0];
            return true;
        }
        else
        {
            return false;
        }
    }
    bool ri_options::get(const char* key1, const char* key2,
                         std::string& val) const
    {
        std::string str = get_key(key1, key2);
        const ri_parameters::value_type* pRet = map_->get(str.c_str());
        if (pRet)
        {
            if (pRet->nType != ri_parameters::TYPE_STRING)
                return false;
            if (pRet->string_values.size() != 1)
                return false;
            val = pRet->string_values[0];
            return true;
        }
        else
        {
            return false;
        }
    }

    bool ri_options::get(const char* key1, const char* key2,
                         std::vector<int>& val) const
    {
        std::string str = get_key(key1, key2);
        const ri_parameters::value_type* pRet = map_->get(str.c_str());
        if (pRet)
        {
            if (pRet->nType != ri_parameters::TYPE_INTEGER)
                return false;
            val = pRet->integer_values;
            return true;
        }
        else
        {
            return false;
        }
    }
    bool ri_options::get(const char* key1, const char* key2,
                         std::vector<float>& val) const
    {
        std::string str = get_key(key1, key2);
        const ri_parameters::value_type* pRet = map_->get(str.c_str());
        if (pRet)
        {
            if (pRet->nType != ri_parameters::TYPE_FLOAT)
                return false;
            val = pRet->float_values;
            return true;
        }
        else
        {
            return false;
        }
    }
    bool ri_options::get(const char* key1, const char* key2,
                         std::vector<std::string>& val) const
    {
        std::string str = get_key(key1, key2);
        const ri_parameters::value_type* pRet = map_->get(str.c_str());
        if (pRet)
        {
            if (pRet->nType != ri_parameters::TYPE_STRING)
                return false;
            val = pRet->string_values;
            return true;
        }
        else
        {
            return false;
        }
    }

    void ri_options::add_display(const ri_display* disp) { disps_.push_back(disp); }

    size_t ri_options::get_display_size() const { return disps_.size(); }

    const ri_display* ri_options::get_display_at(size_t i) const
    {
        return disps_[i];
    }

    void ri_options::add_camera(const ri_camera_options* cmr)
    {
        cmrs_.push_back(cmr);
    }

    size_t ri_options::get_camera_size() const { return cmrs_.size(); }

    const ri_camera_options* ri_options::get_camera_at(size_t i) const
    {
        return cmrs_[i];
    }
}
