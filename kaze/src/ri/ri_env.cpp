#include "ri_env.h"
#include "ri_path_resolver.h"

#include <map>
#include <string>
#include <algorithm>

#include <stdio.h>
#include <stdlib.h>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

namespace ri
{
    static std::string get_env(const char* str)
    {
        const char* s = getenv(str);
        if (s)
            return s;
        return "";
    }

    class ri_env_map_
    {
    public:
        typedef std::map<std::string, std::string> map_type;
        typedef map_type::const_iterator const_iterator;

    public:
        bool get(const char* key, std::string& val) const
        {
            const_iterator iter = map_.find(key);
            if (iter != map_.end())
            {
                val = iter->second;
                return true;
            }
            else
            {
                std::string vv = get_env(key);
                if (vv != "")
                {
                    map_[key] = val;
                    val = vv;
                    return true;
                }
            }
            return false;
        }

    public:
        static ri_env_map_& get_instance()
        {
            static ri_env_map_ e;
            return e;
        };

    protected:
        mutable std::map<std::string, std::string> map_;
    };

    /*
        RMANTREE
            This specifies the root of the file tree that contains the RenderMan
   Toolkit release. All programs in the RenderMan Toolkit check this variable to
   search for executables, shell scripts, data files, etc. If this variable is
   undefined, the software tries to fall back to the installed location of the
   running binary, typically in /opt/pixar/.... If the release is installed at
   some other location, this variable must be set to point at its new location.
        RICOMPRESSION
            This controls whether the output of the RIB library is in
   compressed. Setting RICOMPRESSION to gzip causes gzip compression to be
   applied to the output. The default is to not use gzip compression.
        RIFORMAT
            This controls whether the output of the RIB library is in binary or
   ASCII. Setting RIFORMAT to binary causes binary output and setting it to
   ascii causes ASCII output. If RIFORMAT is undefined, the output is in ASCII.
        RISERVER
            This specifies the name of the file to which the RIB library will
   write its output. If RISERVER is undefined, and no name is specified to
   RiBegin (see PRMan Options: RIB Output), the output is written to standard
   output.
        RMAN_DUMP_DEFAULTS
            If set, PRMan will display the default configuration files while
   they are read from rendermn.ini. This is a useful debugging tool.
        RMANFB
            This overrides the name of the default framebuffer device. By
   default, RiDisplay "framebuffer" will use either the x11 or windows device
   driver. This variable can be used to force the default framebuffer device to
   be a machine-specific or site-specific device driver, instead.
        */

    bool ri_env::get(const char* key, std::string& val)
    {
        ri_env_map_& e = ri_env_map_::get_instance();

        std::string strKey = key;
#ifdef _WIN32
        if (strKey == "HOME")
        {
            strKey = "USERPROFILE";
        }
#endif

        if (e.get(strKey.c_str(), val))
        {
            return true;
        }

        if (strKey == "RMANTREE")
        {
            val = ri_path_resolver::get_root_path();
            return true;
        }
        else if (strKey == "RICOMPRESSION")
        {
            val = "";
            return true;
        }
        else if (strKey == "RIFORMAT")
        {
            val = "ascii";
            return true;
        }
        else if (strKey == "RMANFB")
        {
            val = "framebuffer";
            return true;
        }

        return false;
    }

    std::string ri_env::parse(const char* strVal)
    {
        static const char* strBegin = "${";
        static const char* strEnd = "}";

        std::string val = strVal;

        bool bContinue = false;
        do
        {
            bContinue = false;
            const char* strFindBegin = strstr(val.c_str(), strBegin);
            if (strFindBegin)
            {
                const char* strFindEnd = strstr(strFindBegin + 2, strEnd);
                if (strFindEnd)
                {
                    std::string strA, strB, strC;

                    {
                        int n = strFindBegin - val.c_str();
                        if (n > 0)
                        {
                            strA.assign(val.c_str(), n);
                        }
                    }
                    {
                        int nKey = strFindEnd - (strFindBegin + 2);
                        nKey = std::min(nKey, 255);
                        if (nKey)
                        {
                            char szKey[256] = {};
                            memcpy(szKey, strFindBegin + 2, sizeof(char) * nKey);
                            if (!ri_env::get(szKey, strB))
                            {
                                strB = "";
                            }
                        }
                    }
                    {
                        strC = strFindEnd + 1;
                    }

                    val = strA + strB + strC;
                    bContinue = true;
                }
            }
        } while (bContinue);

        return val; // TODO
    }
}
