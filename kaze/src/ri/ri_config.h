#ifndef RI_CONFIG_H
#define RI_CONFIG_H

#include <map>
#include <string>

namespace ri
{
    class ri_config_reader
    {
    public:
        typedef std::map<std::string, std::string> map_type;
        typedef map_type::const_iterator const_iterator;
        ri_config_reader(const char* szFilePath);
        bool get(const char* key1, std::string& val) const;

    private:
        mutable map_type map_;
    };

    class ri_config
    {
    public:
        static bool get(const char* key, std::string& val);
        static std::string parse(const char* strVal);
    };
}

#endif
