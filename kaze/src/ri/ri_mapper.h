#ifndef RI_MAPPER_H
#define RI_MAPPER_H

#include <map>
#include <string>

namespace ri
{
    class ri_mapper
    {
    public:
        void set(int id, void* p);
        void set(const char* name, void* p);
        const char* get_str(void* p) const;
        int get_int(void* p) const;
        void* get_ptr(int id) const;
        void* get_ptr(const char* name) const;

    private:
        std::map<void*, std::string> p2s_;
        std::map<std::string, void*> s2p_;
    };
}

#endif
