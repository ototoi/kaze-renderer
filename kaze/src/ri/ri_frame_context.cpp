#include "ri_frame_context.h"

#include "ri_object.h"
#include "ri_attributes.h"
#include "ri_options.h"
#include "ri_transform.h"
#include "ri_state.h"
#include "ri_light_source.h"
#include "ri_geometry.h"
#include "ri_task.h"
#include "ri_frame_task.h"
#include "ri_make_texture_task.h"
#include "ri_camera_options.h"
#include "ri_display.h"
#include "ri_coordinate_system.h"
#include "ri_motion.h"
#include "ri_condition_if.h"
#include "ri_stdout_context.h"

#include "ri_logger.h"
#include "ri_path_resolver.h"
#include "ri_read_archive.h"

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif
#include <string>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <memory>

#if defined(_MSC_VER)
#if (_MSC_VER < 1400) // msc_ver < vc2005
#define SNPRINTF _snprintf
#else
#define SNPRINTF _snprintf_s
#endif
#else
#define SNPRINTF snprintf
#endif

#if 1
#define PRINT(STRING)
#else
static void PRINT(const char* szString) { printf("%s\n", szString); }
#endif

struct BasisStepMap
{
    RtToken basis;
    RtInt step;
};

static struct BasisStepMap BASISSTEPS[] = {
    {RI_BEZIER, 3}, 
    {RI_BSPLINE, 1}, 
    {RI_CATMULLROM, 1}, 
    {RI_HERMITE, 2}, 
    {RI_POWER, 4}, 
    {RI_NULL, 0}};

static RtInt GetBasisStep(RtToken basis)
{
    int i = 0;
    while (BASISSTEPS[i].basis)
    {
        if (strcmp(BASISSTEPS[i].basis, basis) == 0)
            return BASISSTEPS[i].step;
        i++;
    }
    return 0;
}

struct PTMap
{
    void* ptr;
    const char* name;
};

static const char* PointerToToken(const struct PTMap FUNCS[], void* ptr)
{
    int i = 0;
    while (FUNCS[i].ptr)
    {
        if (FUNCS[i].ptr == ptr)
            return FUNCS[i].name;
        i++;
    }
    return NULL;
}

static struct PTMap FILTERS[] = {{(void*)RiGaussianFilter, RI_GAUSSIAN},
                                 {(void*)RiBoxFilter, RI_BOX},
                                 {(void*)RiTriangleFilter, RI_TRIANGLE},
                                 {(void*)RiCatmullRomFilter, RI_CATMULLROM},
                                 {(void*)RiSincFilter, RI_SINC},
                                 {NULL, RI_NULL}};
/*
static struct PTMap ERROR_HANDLERS[] = {{(void*)RiErrorIgnore, RI_IGNORE},
                                        {(void*)RiErrorPrint, RI_PRINT},
                                        {(void*)RiErrorAbort, RI_ABORT},
                                        {NULL, RI_NULL}};
*/
static const struct PTMap BASISES[] = {
    {(void*)RiBezierBasis, RI_BEZIER},
    {(void*)RiBSplineBasis, "bspline"}, //
    {(void*)RiBSplineBasis, "b-spline"},
    {(void*)RiCatmullRomBasis, "catmull-rom"}, //
    {(void*)RiCatmullRomBasis, "catmullrom"},
    {(void*)RiHermiteBasis, RI_HERMITE},
    {(void*)RiPowerBasis, RI_POWER},
    {NULL, RI_NULL},
};
/*
static struct PTMap PROCEDURALS[] = {
    {(void*)RiProcDelayedReadArchive, "DelayedReadArchive"},
    {(void*)RiProcRunProgram, "RunProgram"},
    {(void*)RiProcDynamicLoad, "DynamicLoad"},
    {NULL, RI_NULL}};
*/
static const char* GetFilerFuncName(RtFilterFunc f)
{
    const char* name = PointerToToken(FILTERS, (void*)f);
    if (name)
        return name;
    else
        return "unknown";
}

static const char* BasisToToken(RtBasis b)
{
    const RtFloat* f = (const RtFloat*)b;
    int i = 0;
    while (BASISES[i].ptr)
    {
        const RtFloat* fp = (const RtFloat*)(BASISES[i].ptr);
        if (memcmp(fp, f, sizeof(RtFloat) * 16) == 0)
            return BASISES[i].name;
        i++;
    }
    return RI_NULL;
}

static std::string ArrayToString(const RtFloat ar[])
{
    char buffer[512];

    std::string sRet;
    sRet.reserve(16 * 32);

    sRet += "[";
    for (int i = 0; i < 16; i++)
    {
        SNPRINTF(buffer, sizeof(buffer), "%.8f", ar[i]);

        sRet += buffer;
        if (i != 15)
        {
            sRet += " ";
        }
    }
    sRet += "]";
    return sRet;
}

static std::string BasisToString(RtBasis b)
{
    const char* name = BasisToToken(b);
    if (name)
        return name;

    const RtFloat* f = (const RtFloat*)b;
    return ArrayToString(f);
}

#define CLS() (this->RiGetClassifier())
#define OPT() (this->top_options())
#define ATTR() (this->top_attributes())
#define TRNS() (this->top_transform())
#define STATE() (this->top_state())
#define MOTION() (this->top_motion())

#define IFTRUE() (this->is_if_true())

#define ARCH() (this->top_archive())

#define DUMMY_ 1

namespace ri
{
    ri_frame_context::ri_frame_context()
    {
        opts_.push_back(new ri_options());
        attr_.push_back(new ri_attributes());
        trns_.push_back(new ri_transform());
        states_.push_back(new ri_state());
    }

    ri_frame_context::ri_frame_context(const ri_options& opt,
                                       const ri_attributes& attr,
                                       const ri_transform& trans)
    {
        opts_.push_back(new ri_options(opt));
        attr_.push_back(new ri_attributes(attr));
        trns_.push_back(new ri_transform(trans));
        states_.push_back(new ri_state());
    }

    ri_frame_context::~ri_frame_context()
    {
        {
            size_t sz = objs_.size();
            for (size_t i = 0; i < sz; i++)
            {
                delete objs_[i];
            }
        }
        {
            size_t sz = attr_.size();
            for (size_t i = 0; i < sz; i++)
            {
                delete attr_[i];
            }
        }
        {
            size_t sz = opts_.size();
            for (size_t i = 0; i < sz; i++)
            {
                delete opts_[i];
            }
        }
        {
            size_t sz = trns_.size();
            for (size_t i = 0; i < sz; i++)
            {
                delete trns_[i];
            }
        }
        {
            size_t sz = states_.size();
            for (size_t i = 0; i < sz; i++)
            {
                delete states_[i];
            }
        }
        {
            size_t sz = task_.size();
            for (size_t i = 0; i < sz; i++)
            {
                delete task_[i];
            }
        }
        {
            size_t sz = arch_.size();
            for (size_t i = 0; i < sz; i++)
            {
                delete arch_[i];
            }
        }
        PRINT("~ri_frame_context");
    }

    RtVoid ri_frame_context::RiVersion(float version)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiVersion(version);
            return;
        }
    }
    RtVoid ri_frame_context::RiVersion(char* version)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiVersion(version);
            return;
        }
    }
    //
    RtToken ri_frame_context::RiDeclare(char* name, char* declaration)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiDeclare(name, declaration);
            return name;
        }
        if (!IFTRUE())
            return name;
        return ri_context::RiDeclare(name, declaration);
    }
    RtVoid ri_frame_context::RiBegin(RtToken name)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiBegin(name);
            return;
        }
        if (!IFTRUE())
            return;
    }
    RtVoid ri_frame_context::RiEnd(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiEnd();
            return;
        }
        if (!IFTRUE())
            return;
    }
    RtVoid ri_frame_context::RiFrameBegin(RtInt frame)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiFrameBegin(frame);
            return;
        }
        if (!IFTRUE())
            return;

        this->push_frame();
        this->push_options();
        this->push_attributes();
        this->push_transform();
        this->push_state();
    }
    RtVoid ri_frame_context::RiFrameEnd(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiFrameEnd();
            return;
        }
        if (!IFTRUE())
            return;

        this->pop_state();
        this->pop_transform();
        this->pop_attributes();
        this->pop_options();
        this->pop_frame();
    }
    RtVoid ri_frame_context::RiWorldBegin(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiWorldBegin();
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_camera_options> ap(new ri_camera_options(*OPT(), *TRNS()));

        this->push_options();
        this->push_attributes();
        this->push_transform();
        this->push_state();
        STATE()
            ->set_in_world(true);

        ATTR()
            ->clear_group();
        TRNS()
            ->identity();
        OPT()
            ->add_camera(ap.get());

        objs_.push_back(ap.release());
    }
    RtVoid ri_frame_context::RiWorldEnd(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiWorldEnd();
            return;
        }
        if (!IFTRUE())
            return;

        this->top_frame()->set_options(*OPT());

        this->pop_state();
        this->pop_transform();
        this->pop_attributes();
        this->pop_options();
    }

    //------------------------------------------------------------------------------------------------
    // options
    static int GetSize(int nClass, RtInt vertex = 1, RtInt varying = 1,
                       RtInt uniform = 1, RtInt facevarying = 1)
    {
        switch (nClass)
        {
        case ri_classifier::VERTEX:
            return vertex;
        case ri_classifier::UNIFORM:
            return uniform;
        case ri_classifier::CONSTANT:
            return 1;
        case ri_classifier::VARYING:
            return varying;
        case ri_classifier::FACEVARYING:
            return facevarying;
        }
        return 1;
    }

    static void SetOptions(ri_classifier* p_cls, ri_options* p_opt,
                           const char* szCategory, RtToken token, RtPointer val,
                           RtInt vertex = 1, RtInt varying = 1, RtInt uniform = 1,
                           RtInt facevarying = 1)
    {
        int nID = p_cls->def(token);
        if (nID < 0)
            return;
        const ri_classifier::param_data& data = (*p_cls)[nID];
        int nSz = GetSize(data.get_class(), vertex, varying, uniform, facevarying);
        switch (data.get_type())
        {
        case ri_classifier::INTEGER:
            p_opt->set(szCategory, data.get_name(), (const RtInt*)val,
                       data.get_size() * nSz);
            break;
        case ri_classifier::STRING:
            p_opt->set(szCategory, data.get_name(), (const RtString*)val,
                       data.get_size() * nSz);
            break;
        case ri_classifier::FLOAT:
        case ri_classifier::COLOR:
        case ri_classifier::POINT:
        case ri_classifier::VECTOR:
        case ri_classifier::NORMAL:
        case ri_classifier::HPOINT:
        case ri_classifier::MATRIX:
            p_opt->set(szCategory, data.get_name(), (const RtFloat*)val,
                       data.get_size() * nSz);
            break;
        default:
            break;
        }
    }

    RtVoid ri_frame_context::RiFormat(RtInt xres, RtInt yres, RtFloat aspect)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiFormat(xres, yres, aspect);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("Format", "xres", xres);
        OPT()
            ->set("Format", "yres", yres);
        OPT()
            ->set("Format", "aspect", aspect);
    }
    RtVoid ri_frame_context::RiFrameAspectRatio(RtFloat aspect)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiFrameAspectRatio(aspect);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("FrameAspectRatio", "aspect", aspect);
    }
    RtVoid ri_frame_context::RiScreenWindow(RtFloat left, RtFloat right,
                                            RtFloat bottom, RtFloat top)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiScreenWindow(left, right, bottom, top);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("ScreenWindow", "left", left);
        OPT()
            ->set("ScreenWindow", "right", right);
        OPT()
            ->set("ScreenWindow", "bottom", bottom);
        OPT()
            ->set("ScreenWindow", "top", top);
    }
    RtVoid ri_frame_context::RiCropWindow(RtFloat xmin, RtFloat xmax, RtFloat ymin,
                                          RtFloat ymax)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiCropWindow(xmin, xmax, ymin, ymax);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("CropWindow", "xmin", xmin);
        OPT()
            ->set("CropWindow", "xmax", xmax);
        OPT()
            ->set("CropWindow", "ymin", ymin);
        OPT()
            ->set("CropWindow", "ymax", ymax);
    }
    RtVoid ri_frame_context::RiProjectionV(RtToken name, RtInt n, RtToken tokens[],
                                           RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiProjectionV(name, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("Projection", "name", name);
        ri_temporary_classifier cls(this->RiGetClassifier());
        for (int i = 0; i < n; i++)
        {
            SetOptions(&cls, OPT(), "Projection", tokens[i], params[i]);
        }
    }
    RtVoid ri_frame_context::RiClipping(RtFloat hither, RtFloat yon)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiClipping(hither, yon);
            return;
        }
        if (!IFTRUE())
            return;

        RtFloat near = hither;
        RtFloat far = yon;

        OPT()
            ->set("Clipping", "hither", hither);
        OPT()
            ->set("Clipping", "yon", yon);
        OPT()
            ->set("Clipping", "near", near);
        OPT()
            ->set("Clipping", "far", far);
    }
    RtVoid ri_frame_context::RiClippingPlane(RtFloat x, RtFloat y, RtFloat z,
                                             RtFloat nx, RtFloat ny, RtFloat nz)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiClippingPlane(x, y, z, nx, ny, nz);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("ClippingPlane", "x", x);
        OPT()
            ->set("ClippingPlane", "y", y);
        OPT()
            ->set("ClippingPlane", "z", z);
        OPT()
            ->set("ClippingPlane", "nx", nx);
        OPT()
            ->set("ClippingPlane", "ny", ny);
        OPT()
            ->set("ClippingPlane", "nz", nz);
    }
    RtVoid ri_frame_context::RiShutter(RtFloat min, RtFloat max)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiShutter(min, max);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("Shutter", "min", min);
        OPT()
            ->set("Shutter", "max", max);
    }
    RtVoid ri_frame_context::RiPixelVariance(RtFloat variation)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiPixelVariance(variation);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("PixelVariance", "variation", variation);
    }
    RtVoid ri_frame_context::RiPixelSamples(RtFloat xsamples, RtFloat ysamples)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiPixelSamples(xsamples, ysamples);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("PixelSamples", "xsamples", (int)xsamples);
        OPT()
            ->set("PixelSamples", "ysamples", (int)ysamples);
        OPT()
            ->set("PixelSamples", "xsamples", xsamples);
        OPT()
            ->set("PixelSamples", "ysamples", ysamples);
    }
    RtVoid ri_frame_context::RiPixelSampleImagerV(RtToken name, RtInt n,
                                                  RtToken tokens[],
                                                  RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiPixelSampleImagerV(name, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("PixelSampleImager", "name", name);
        ri_temporary_classifier cls(this->RiGetClassifier());
        for (int i = 0; i < n; i++)
        {
            SetOptions(&cls, OPT(), "PixelSampleImager", tokens[i], params[i]);
        }
    }
    RtVoid ri_frame_context::RiPixelFilter(RtToken name, RtFloat xwidth,
                                           RtFloat ywidth) /*extension*/
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiPixelFilter(name, xwidth, ywidth);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("PixelFilter", "name", name);
        OPT()
            ->set("PixelFilter", "xwidth", xwidth);
        OPT()
            ->set("PixelFilter", "ywidth", ywidth);
    }
    RtVoid ri_frame_context::RiPixelFilter(RtFilterFunc filterfunc, RtFloat xwidth,
                                           RtFloat ywidth)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiPixelFilter(filterfunc, xwidth, ywidth);
            return;
        }
        if (!IFTRUE())
            return;

        this->RiPixelFilter((RtToken)(GetFilerFuncName(filterfunc)), xwidth, ywidth);
    }
    RtVoid ri_frame_context::RiExposure(RtFloat gain, RtFloat gamma)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiExposure(gain, gamma);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("Exposure", "gain", gain);
        OPT()
            ->set("Exposure", "gamma", gamma);
    }
    RtVoid ri_frame_context::RiImagerV(RtToken name, RtInt n, RtToken tokens[],
                                       RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiImagerV(name, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("Imager", "name", name);
        ri_temporary_classifier cls(this->RiGetClassifier());
        for (int i = 0; i < n; i++)
        {
            SetOptions(&cls, OPT(), "Imager", tokens[i], params[i]);
        }
    }
    RtVoid ri_frame_context::RiQuantize(RtToken type, RtInt one, RtInt min,
                                        RtInt max, RtFloat ampl)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiQuantize(type, one, min, max, ampl);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("Quantize", "type", type);
        OPT()
            ->set("Quantize", "one", one);
        OPT()
            ->set("Quantize", "min", min);
        OPT()
            ->set("Quantize", "max", max);
        OPT()
            ->set("Quantize", "ampl", ampl);
    }
    RtVoid ri_frame_context::RiDisplayV(char* name, RtToken type, RtToken mode,
                                        RtInt n, RtToken tokens[],
                                        RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiDisplayV(name, type, mode, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_display> ap(new ri_display(this->RiGetClassifier(), name,
                                                    type, mode, n, tokens, params));
        OPT()
            ->add_display(ap.get());
        objs_.push_back(ap.release());
    }

    RtVoid ri_frame_context::RiDisplayChannelV(RtToken channel, RtInt n,
                                               RtToken tokens[],
                                               RtPointer params[])
    {
        if (!IFTRUE())
            return;
    }

    RtVoid ri_frame_context::RiHiderV(RtToken type, RtInt n, RtToken tokens[],
                                      RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiHiderV(type, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("Hider", "type", type);
        ri_temporary_classifier cls(this->RiGetClassifier());
        for (int i = 0; i < n; i++)
        {
            SetOptions(&cls, OPT(), "Hider", tokens[i], params[i]);
        }
    }

    RtVoid ri_frame_context::RiColorSamples(RtInt n, RtFloat nRGB[],
                                            RtFloat RGBn[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiColorSamples(n, nRGB, RGBn);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("ColorSamples", "nRGB", nRGB, n);
        OPT()
            ->set("ColorSamples", "RGBn", RGBn, n);
    }
    RtVoid ri_frame_context::RiRelativeDetail(RtFloat relativedetail)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiRelativeDetail(relativedetail);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("RelativeDetail", "relativedetail", relativedetail);
    }
    RtVoid ri_frame_context::RiOptionV(RtToken name, RtInt n, RtToken tokens[],
                                       RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiOptionV(name, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        ri_temporary_classifier cls(this->RiGetClassifier());
        for (int i = 0; i < n; i++)
        {
            SetOptions(&cls, OPT(), name, tokens[i], params[i]);
        }
        for (int i = 0; i < n; i++)
        {
            int id = cls.find(tokens[i]);
            if (id >= 0)
            {
                ri_classifier::param_data data = cls[id];
                std::string key = data.get_name();
                std::string fullkey =
                    std::string("$Option") + ":" + std::string(name) + ":" + key;
                data.set_name(fullkey.c_str());
                this->RiGetClassifier()->add(data);
            }
        }
    }
    //------------------------------------------------------------------------------------------------
    static void SetAttributes(ri_classifier* p_cls, ri_attributes* p_atr,
                              const char* szCategory, RtToken token, RtPointer val,
                              RtInt vertex = 1, RtInt varying = 1,
                              RtInt uniform = 1, RtInt facevarying = 1)
    {
        int nID = p_cls->def(token);
        if (nID < 0)
            return;
        const ri_classifier::param_data& data = (*p_cls)[nID];
        int nSz = GetSize(data.get_class(), vertex, varying, uniform, facevarying);
        switch (data.get_type())
        {
        case ri_classifier::INTEGER:
            p_atr->set(szCategory, data.get_name(), (const RtInt*)val,
                       data.get_size() * nSz);
            break;
        case ri_classifier::STRING:
            p_atr->set(szCategory, data.get_name(), (const RtString*)val,
                       data.get_size() * nSz);
            break;
        case ri_classifier::FLOAT:
        case ri_classifier::COLOR:
        case ri_classifier::POINT:
        case ri_classifier::VECTOR:
        case ri_classifier::NORMAL:
        case ri_classifier::HPOINT:
        case ri_classifier::MATRIX:
            p_atr->set(szCategory, data.get_name(), (const RtFloat*)val,
                       data.get_size() * nSz);
            break;
        default:
            break;
        }
    }

    RtVoid ri_frame_context::RiAttributeBegin(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiAttributeBegin();
            return;
        }
        if (!IFTRUE())
            return;

        this->push_attributes();
        this->push_transform();
    }
    RtVoid ri_frame_context::RiAttributeEnd(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiAttributeEnd();
            return;
        }
        if (!IFTRUE())
            return;

        this->pop_transform();
        this->pop_attributes();
    }
    RtVoid ri_frame_context::RiColor(RtColor color)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiColor(color);
            return;
        }
        if (!IFTRUE())
            return;

        ATTR()
            ->set("Color", "color", color, 3);
    }
    RtVoid ri_frame_context::RiOpacity(RtColor color)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiColor(color);
            return;
        }
        if (!IFTRUE())
            return;

        ATTR()
            ->set("Opacity", "color", color, 3);
    }
    RtVoid ri_frame_context::RiTextureCoordinates(RtFloat s1, RtFloat t1,
                                                  RtFloat s2, RtFloat t2,
                                                  RtFloat s3, RtFloat t3,
                                                  RtFloat s4, RtFloat t4)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiTextureCoordinates(s1, t1, s2, t2, s3, t3, s4, t4);
            return;
        }
        if (!IFTRUE())
            return;

        ATTR()
            ->set("TextureCoordinates", "s1", s1);
        ATTR()
            ->set("TextureCoordinates", "t1", t1);
        ATTR()
            ->set("TextureCoordinates", "s2", s2);
        ATTR()
            ->set("TextureCoordinates", "t2", t2);
        ATTR()
            ->set("TextureCoordinates", "s3", s3);
        ATTR()
            ->set("TextureCoordinates", "t3", t3);
        ATTR()
            ->set("TextureCoordinates", "s4", s4);
        ATTR()
            ->set("TextureCoordinates", "t4", t4);
    }

    RtLightHandle ri_frame_context::RiLightSourceV(RtToken name, RtInt n,
                                                   RtToken tokens[],
                                                   RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            return ctx->RiLightSourceV(name, n, tokens, params);
        }
        if (!IFTRUE())
            return NULL;

        std::unique_ptr<ri_light_source> ap(
            new ri_light_source(this->RiGetClassifier(), name, n, tokens, params));
        ri_light_source* l = ap.get();
        l->set_attributes(*ATTR());
        l->set_transform(*TRNS());
        ATTR()
            ->add_light_source(l);
        this->RiSetLightHandle(name, l);
        objs_.push_back(ap.release());
        return l;
    }
    RtLightHandle ri_frame_context::RiAreaLightSourceV(RtToken name, RtInt n,
                                                       RtToken tokens[],
                                                       RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            return ctx->RiAreaLightSourceV(name, n, tokens, params);
        }
        if (!IFTRUE())
            return NULL;

        std::unique_ptr<ri_area_light_source> ap(new ri_area_light_source(
            this->RiGetClassifier(), name, n, tokens, params));
        ri_area_light_source* l = ap.get();
        l->set_attributes(*ATTR());
        l->set_transform(*TRNS());
        ATTR()
            ->add_light_source(l);
        this->RiSetLightHandle(name, l);
        objs_.push_back(ap.release());
        ATTR()
            ->push_group(l->get_group_geometry());
        return l;
    }

    RtVoid ri_frame_context::RiIlluminate(RtInt id, RtBoolean onoff)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiIlluminate(id, onoff);
            return;
        }
        if (!IFTRUE())
            return;

        ri_light_source* l = (ri_light_source*)this->RiGetLightHandle(id);
        if (!l)
            return;
        if (onoff)
        {
            ATTR()
                ->add_light_source(l);
        }
        else
        {
            ATTR()
                ->del_light_source(l);
        }
    }
    RtVoid ri_frame_context::RiIlluminate(RtToken name, RtBoolean onoff)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiIlluminate(name, onoff);
            return;
        }
        if (!IFTRUE())
            return;

        ri_light_source* l = (ri_light_source*)this->RiGetLightHandle(name);
        if (!l)
            return;
        if (onoff)
        {
            ATTR()
                ->add_light_source(l);
        }
        else
        {
            ATTR()
                ->del_light_source(l);
        }
    }
    RtVoid ri_frame_context::RiIlluminate(RtLightHandle light, RtBoolean onoff)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            return ctx->RiIlluminate(light, onoff);
        }
        if (!IFTRUE())
            return;

        ri_light_source* l = (ri_light_source*)light;
        if (!l)
            return;
        if (onoff)
        {
            ATTR()
                ->add_light_source(l);
        }
        else
        {
            ATTR()
                ->del_light_source(l);
        }
    }

    static bool IsNullShader(RtToken name)
    {
        if (!name)
            return true;
        if (strcmp(name, "null") == 0)
            return true;
        if (strcmp(name, "nil") == 0)
            return true;
        return false;
    }

    RtVoid ri_frame_context::RiSurfaceV(RtToken name, RtInt n, RtToken tokens[],
                                        RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiSurfaceV(name, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        if (IsNullShader(name))
        {
            ATTR()
                ->del_surface();
        }
        else
        {
            std::unique_ptr<ri_shader> ap(
                new ri_shader(this->RiGetClassifier(), name, n, tokens, params));
            ri_shader* s = ap.get();
            s->set_attributes(*ATTR());
            s->set_transform(*TRNS());
            s->set_options(*OPT());
            ATTR()
                ->set_surface(s);
            objs_.push_back(ap.release());
        }
    }
    RtVoid ri_frame_context::RiAtmosphereV(RtToken name, RtInt n, RtToken tokens[],
                                           RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiAtmosphereV(name, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        if (IsNullShader(name))
        {
            ATTR()
                ->del_atmosphere();
        }
        else
        {
            std::unique_ptr<ri_shader> ap(
                new ri_shader(this->RiGetClassifier(), name, n, tokens, params));
            ri_shader* s = ap.get();
            s->set_attributes(*ATTR());
            s->set_transform(*TRNS());
            s->set_options(*OPT());
            ATTR()
                ->set_atmosphere(s);
            objs_.push_back(ap.release());
        }
    }
    RtVoid ri_frame_context::RiInteriorV(RtToken name, RtInt n, RtToken tokens[],
                                         RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiInteriorV(name, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        if (IsNullShader(name))
        {
            ATTR()
                ->del_interior();
        }
        else
        {
            std::unique_ptr<ri_shader> ap(
                new ri_shader(this->RiGetClassifier(), name, n, tokens, params));
            ri_shader* s = ap.get();
            s->set_attributes(*ATTR());
            s->set_transform(*TRNS());
            s->set_options(*OPT());
            ATTR()
                ->set_interior(s);
            objs_.push_back(ap.release());
        }
    }
    RtVoid ri_frame_context::RiExteriorV(RtToken name, RtInt n, RtToken tokens[],
                                         RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiExteriorV(name, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        if (IsNullShader(name))
        {
            ATTR()
                ->del_exterior();
        }
        else
        {
            std::unique_ptr<ri_shader> ap(
                new ri_shader(this->RiGetClassifier(), name, n, tokens, params));
            ri_shader* s = ap.get();
            s->set_attributes(*ATTR());
            s->set_transform(*TRNS());
            s->set_options(*OPT());
            ATTR()
                ->set_exterior(s);
            objs_.push_back(ap.release());
        }
    }
    RtVoid ri_frame_context::RiDisplacementV(RtToken name, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiDisplacementV(name, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        if (IsNullShader(name))
        {
            ATTR()
                ->del_displacement();
        }
        else
        {
            std::unique_ptr<ri_shader> ap(
                new ri_shader(this->RiGetClassifier(), name, n, tokens, params));
            ri_shader* s = ap.get();
            s->set_attributes(*ATTR());
            s->set_transform(*TRNS());
            s->set_options(*OPT());
            ATTR()
                ->set_displacement(s);
            objs_.push_back(ap.release());
        }
    }

    RtVoid ri_frame_context::RiShadingRate(RtFloat size)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiShadingRate(size);
            return;
        }
        if (!IFTRUE())
            return;

        ATTR()
            ->set("ShadingRate", "size", size);
    }
    RtVoid ri_frame_context::RiShadingInterpolation(RtToken type)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiShadingInterpolation(type);
            return;
        }
        if (!IFTRUE())
            return;

        ATTR()
            ->set("ShadingInterpolation", "type", type);
    }
    RtVoid ri_frame_context::RiMatte(RtBoolean onoff)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiMatte(onoff);
            return;
        }
        if (!IFTRUE())
            return;

        ATTR()
            ->set("Matte", "onoff", onoff);
    }
    RtVoid ri_frame_context::RiBound(RtBound bound)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiBound(bound);
            return;
        }
        if (!IFTRUE())
            return;

        ATTR()
            ->set("Bound", "bound", bound, 6);
    }
    RtVoid ri_frame_context::RiDetail(RtBound bound)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiDetail(bound);
            return;
        }
        if (!IFTRUE())
            return;

        ATTR()
            ->set("Detail", "bound", bound, 6);
    }
    RtVoid ri_frame_context::RiDetailRange(RtFloat minvis, RtFloat lowtran,
                                           RtFloat uptran, RtFloat maxvis)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiDetailRange(minvis, lowtran, uptran, maxvis);
            return;
        }
        if (!IFTRUE())
            return;

        ATTR()
            ->set("DetailRange", "minvis", minvis);
        ATTR()
            ->set("DetailRange", "lowtran", lowtran);
        ATTR()
            ->set("DetailRange", "uptran", uptran);
        ATTR()
            ->set("DetailRange", "maxvis", maxvis);
    }
    RtVoid ri_frame_context::RiGeometricApproximation(RtToken type, RtFloat value)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiGeometricApproximation(type, value);
            return;
        }
        if (!IFTRUE())
            return;

        ATTR()
            ->set("GeometricApproximation", "type", type);
        ATTR()
            ->set("GeometricApproximation", "value", value);
    }
    RtVoid ri_frame_context::RiOrientation(RtToken orientation)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiOrientation(orientation);
            return;
        }
        if (!IFTRUE())
            return;

        ATTR()
            ->set("Orientation", "orientation", orientation);
    }
    RtVoid ri_frame_context::RiReverseOrientation(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiReverseOrientation();
            return;
        }
        if (!IFTRUE())
            return;

        std::string orientation;
        if (ATTR()->get("Orientation", "orientation", orientation))
        {
            if (orientation == RI_LH)
            {
                ATTR()
                    ->set("Orientation", "orientation", RI_RH);
            }
            else if (orientation == RI_RH)
            {
                ATTR()
                    ->set("Orientation", "orientation", RI_LH);
            }
            else if (orientation == RI_INSIDE)
            {
                ATTR()
                    ->set("Orientation", "orientation", RI_OUTSIDE);
            }
            else if (orientation == RI_OUTSIDE)
            {
                ATTR()
                    ->set("Orientation", "orientation", RI_INSIDE);
            }
        }
    }
    RtVoid ri_frame_context::RiSides(RtInt sides)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiSides(sides);
            return;
        }
        if (!IFTRUE())
            return;

        ATTR()
            ->set("Sides", "sides", sides);
    }
    RtVoid ri_frame_context::RiIdentity(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiIdentity();
            return;
        }
        if (!IFTRUE())
            return;

        ri_motion* mtn = MOTION();
        if (mtn)
        {
            mtn->identity();
        }
        else
        {
            TRNS()
                ->identity();
        }
    }
    RtVoid ri_frame_context::RiTransform(RtMatrix transform)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiTransform(transform);
            return;
        }
        if (!IFTRUE())
            return;

        float tmp[16];
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                tmp[4 * i + j] = transform[i][j];
            }
        }

        ri_motion* mtn = MOTION();
        if (mtn)
        {
            mtn->set(tmp);
        }
        else
        {
            TRNS()
                ->set(tmp);
        }
    }
    RtVoid ri_frame_context::RiConcatTransform(RtMatrix transform)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiConcatTransform(transform);
            return;
        }
        if (!IFTRUE())
            return;

        float tmp[16];
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                tmp[4 * i + j] = transform[i][j];
            }
        }

        ri_motion* mtn = MOTION();
        if (mtn)
        {
            mtn->concat(tmp);
        }
        else
        {
            TRNS()
                ->concat(tmp);
        }
    }
    RtVoid ri_frame_context::RiPerspective(RtFloat fov)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiPerspective(fov);
            return;
        }
        if (!IFTRUE())
            return;

        ri_motion* mtn = MOTION();
        if (mtn)
        {
            mtn->perspective(fov);
        }
        else
        {
            TRNS()
                ->perspective(fov);
        }
    }
    RtVoid ri_frame_context::RiTranslate(RtFloat dx, RtFloat dy, RtFloat dz)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiTranslate(dx, dy, dz);
            return;
        }
        if (!IFTRUE())
            return;

        ri_motion* mtn = MOTION();
        if (mtn)
        {
            mtn->translate(dx, dy, dz);
        }
        else
        {
            TRNS()
                ->translate(dx, dy, dz);
        }
    }
    RtVoid ri_frame_context::RiRotate(RtFloat angle, RtFloat dx, RtFloat dy,
                                      RtFloat dz)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiRotate(angle, dx, dy, dz);
            return;
        }
        if (!IFTRUE())
            return;

        ri_motion* mtn = MOTION();
        if (mtn)
        {
            mtn->rotate(angle, dx, dy, dz);
        }
        else
        {
            TRNS()
                ->rotate(angle, dx, dy, dz);
        }
    }
    RtVoid ri_frame_context::RiScale(RtFloat sx, RtFloat sy, RtFloat sz)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiScale(sx, sy, sz);
            return;
        }
        if (!IFTRUE())
            return;

        ri_motion* mtn = MOTION();
        if (mtn)
        {
            mtn->scale(sx, sy, sz);
        }
        else
        {
            TRNS()
                ->scale(sx, sy, sz);
        }
    }
    RtVoid ri_frame_context::RiSkew(RtFloat angle, RtFloat dx1, RtFloat dy1,
                                    RtFloat dz1, RtFloat dx2, RtFloat dy2,
                                    RtFloat dz2)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiSkew(angle, dx1, dy1, dz1, dx2, dy2, dz2);
            return;
        }
        if (!IFTRUE())
            return;

        ri_motion* mtn = MOTION();
        if (mtn)
        {
            mtn->skew(angle, dx1, dy1, dz1, dx2, dy2, dz2);
        }
        else
        {
            TRNS()
                ->skew(angle, dx1, dy1, dz1, dx2, dy2, dz2);
        }
    }
    RtVoid ri_frame_context::RiDeformationV(RtToken name, RtInt n, RtToken tokens[],
                                            RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiDeformationV(name, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;
        // assert(0);
    }

    RtVoid ri_frame_context::RiCoordinateSystem(RtToken space)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiCoordinateSystem(space);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_coordinate_system> ap(
            new ri_coordinate_system(space, *TRNS()));
        ri_coordinate_system* c = ap.get();
        ATTR()
            ->add_coordinate_system(c);
        coords_[space] = c;
        objs_.push_back(ap.release());
    }
    RtVoid ri_frame_context::RiScopedCoordinateSystem(RtToken space)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiScopedCoordinateSystem(space);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_coordinate_system> ap(
            new ri_coordinate_system(space, *TRNS()));
        ri_coordinate_system* c = ap.get();
        ATTR()
            ->add_coordinate_system(c);
        objs_.push_back(ap.release());
    }

    RtVoid ri_frame_context::RiCoordSysTransform(RtToken space)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiCoordSysTransform(space);
            return;
        }
        if (!IFTRUE())
            return;

        const ri_coordinate_system* c = ATTR()->get_coordinate_system(space);
        if (!c)
        {
            typedef std::map<std::string, const ri_coordinate_system*> coord_map;
            typedef coord_map::const_iterator const_iterator;
            const_iterator it = coords_.find(space);
            if (it != coords_.end())
            {
                c = it->second;
            }
        }

        if (c)
        {
            ri_transform t = c->get_transform();
            TRNS()
                ->set(t.get());
        }
    }

    RtPoint* ri_frame_context::RiTransformPoints(RtToken fromspace, RtToken tospace,
                                                 RtInt n, RtPoint points[])
    {
        return NULL;
    }

    RtVoid ri_frame_context::RiTransformBegin(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiTransformBegin();
            return;
        }
        if (!IFTRUE())
            return;

        this->push_transform();
    }
    RtVoid ri_frame_context::RiTransformEnd(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiTransformEnd();
            return;
        }
        if (!IFTRUE())
            return;

        this->pop_transform();
    }

    RtVoid ri_frame_context::RiResourceV(RtToken handle, RtToken type, RtInt n,
                                         RtToken tokens[], RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiResourceV(handle, type, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;
    }

    RtVoid ri_frame_context::RiResourceBegin(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiResourceBegin();
            return;
        }
        if (!IFTRUE())
            return;
        this->push_state();
        STATE()
            ->set_in_resource(true);
    }

    RtVoid ri_frame_context::RiResourceEnd(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiResourceEnd();
            return;
        }
        if (!IFTRUE())
            return;
        this->pop_state();
    }

    RtVoid ri_frame_context::RiAttributeV(RtToken name, RtInt n, RtToken tokens[],
                                          RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiAttributeV(name, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        ri_temporary_classifier cls(this->RiGetClassifier());
        for (int i = 0; i < n; i++)
        {
            SetAttributes(&cls, ATTR(), name, tokens[i], params[i]);
        }
        for (int i = 0; i < n; i++)
        {
            int id = cls.find(tokens[i]);
            if (id >= 0)
            {
                ri_classifier::param_data data = cls[id];
                std::string key = data.get_name();
                std::string fullkey =
                    std::string("$Attribute") + ":" + std::string(name) + ":" + key;
                data.set_name(fullkey.c_str());
                this->RiGetClassifier()->add(data);
            }
        }
    }
    //------------------------------------------------------------------------------------------------
    // RtVoid ri_frame_context::RiPolygon(RtInt nverts, va_list argptr){/**/}
    RtVoid ri_frame_context::RiPolygonV(RtInt nverts, RtInt n, RtToken tokens[],
                                        RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiPolygonV(nverts, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_polygon_geometry> pObj(new ri_polygon_geometry(
            this->RiGetClassifier(), nverts, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }
    // RtVoid ri_frame_context::RiGeneralPolygon (RtInt nloops, RtInt nverts[],
    // va_list argptr){/**/}
    RtVoid ri_frame_context::RiGeneralPolygonV(RtInt nloops, RtInt nverts[],
                                               RtInt n, RtToken tokens[],
                                               RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiGeneralPolygonV(nloops, nverts, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_general_polygon_geometry> pObj(
            new ri_general_polygon_geometry(this->RiGetClassifier(), nloops, nverts,
                                            n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }
    // RtVoid ri_frame_context::RiPointsPolygons (RtInt npolys, RtInt nverts[],
    // RtInt verts[], va_list argptr){/**/}
    RtVoid ri_frame_context::RiPointsPolygonsV(RtInt npolys, RtInt nverts[],
                                               RtInt verts[], RtInt n,
                                               RtToken tokens[],
                                               RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiPointsPolygonsV(npolys, nverts, verts, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_points_polygons_geometry> pObj(
            new ri_points_polygons_geometry(this->RiGetClassifier(), npolys, nverts,
                                            verts, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        pObj->make_normals();
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }
    // RtVoid ri_frame_context::RiPointsGeneralPolygons (RtInt npolys, RtInt
    // nloops[], RtInt nverts[], RtInt verts[], va_list argptr){/**/}
    RtVoid ri_frame_context::RiPointsGeneralPolygonsV(RtInt npolys, RtInt nloops[],
                                                      RtInt nverts[], RtInt verts[],
                                                      RtInt n, RtToken tokens[],
                                                      RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiPointsGeneralPolygonsV(npolys, nloops, nverts, verts, n, tokens,
                                          params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_points_general_polygons_geometry> pObj(
            new ri_points_general_polygons_geometry(this->RiGetClassifier(), npolys,
                                                    nloops, nverts, verts, n, tokens,
                                                    params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }

    static int GetBasisStepAt(RtToken basis, RtInt step)
    {
        int nStep = GetBasisStep(basis);
        if (nStep)
            return nStep;
        return step;
    }

    struct TokenToken
    {
        const char* from;
        const char* to;
    };

    static const TokenToken BASISMAP[] = {
        {RI_BEZIER, RI_BEZIER}, 
        {"bspline", RI_BSPLINE},
        {"b-spline", RI_BSPLINE}, 
        {"catmull-rom", RI_CATMULLROM}, 
        {"catmullrom", RI_CATMULLROM}, 
        {RI_HERMITE, RI_HERMITE}, 
        {RI_POWER, RI_POWER}, 
        {NULL, NULL}};

    static const char* GetBasisName(RtToken name)
    {
        int i = 0;
        while (BASISMAP[i].from)
        {
            if (strcmp(BASISMAP[i].from, name) == 0)
                return BASISMAP[i].to;
            i++;
        }
        return name;
    }

    RtVoid ri_frame_context::RiBasis(RtToken ubasis, RtInt ustep, RtToken vbasis,
                                     RtInt vstep)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiBasis(ubasis, ustep, vbasis, vstep);
            return;
        }
        if (!IFTRUE())
            return;

        std::string ub = GetBasisName(ubasis);
        std::string vb = GetBasisName(vbasis);

        ATTR()
            ->set("Basis", "ubasis", ub.c_str());
        ATTR()
            ->set("Basis", "vbasis", vb.c_str());
        ATTR()
            ->set("Basis", "ustep", GetBasisStepAt((RtToken)ub.c_str(), ustep));
        ATTR()
            ->set("Basis", "vstep", GetBasisStepAt((RtToken)vb.c_str(), vstep));
    }
    RtVoid ri_frame_context::RiBasis(RtBasis ubasis, RtInt ustep, RtToken vbasis,
                                     RtInt vstep) // basis token
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiBasis(ubasis, ustep, vbasis, vstep);
            return;
        }
        if (!IFTRUE())
            return;

        std::string uname = BasisToString(ubasis);
        // std::string vname = BasisToString(vbasis);
        // std::string uname = ArrayToString((const float*)ubasis);
        this->RiBasis((RtToken)(uname.c_str()), ustep, vbasis, vstep);
    }
    RtVoid ri_frame_context::RiBasis(RtToken ubasis, RtInt ustep, RtBasis vbasis,
                                     RtInt vstep) // token basis
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiBasis(ubasis, ustep, vbasis, vstep);
            return;
        }
        if (!IFTRUE())
            return;

        // std::string uname = BasisToString(ubasis);
        std::string vname = BasisToString(vbasis);
        // std::string vname = ArrayToString((const float*)vbasis);

        this->RiBasis(ubasis, ustep, (RtToken)(vname.c_str()), vstep);
    }
    RtVoid ri_frame_context::RiBasis(RtBasis ubasis, RtInt ustep, RtBasis vbasis,
                                     RtInt vstep)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiBasis(ubasis, ustep, vbasis, vstep);
            return;
        }
        if (!IFTRUE())
            return;

        std::string uname = BasisToString(ubasis);
        std::string vname = BasisToString(vbasis);
        // std::string uname = ArrayToString((const float*)ubasis);
        // std::string vname = ArrayToString((const float*)vbasis);
        this->RiBasis((RtToken)(uname.c_str()), ustep, (RtToken)(vname.c_str()),
                      vstep);
    }

    static const TokenToken PATCHTYPEMAP[] = {{RI_BILINEAR, RI_BILINEAR},
                                              {RI_BIQUADRATIC, RI_BIQUADRATIC},
                                              {RI_BICUBIC, RI_BICUBIC},
                                              {RI_LINEAR, RI_BILINEAR},
                                              {RI_QUADRATIC, RI_BIQUADRATIC},
                                              {RI_CUBIC, RI_BICUBIC},
                                              {NULL, NULL}};

    static const char* GetPatchType(RtToken type)
    {
        int i = 0;
        while (PATCHTYPEMAP[i].from)
        {
            if (strcmp(PATCHTYPEMAP[i].from, type) == 0)
                return PATCHTYPEMAP[i].to;
            i++;
        }
        return NULL;
    }

    // RtVoid ri_frame_context::RiPatch (RtToken type, va_list argptr){/**/}
    RtVoid ri_frame_context::RiPatchV(RtToken type, RtInt n, RtToken tokens[],
                                      RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiPatchV(type, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        type = (RtToken)GetPatchType(type);
        if (!type)
            return;

        std::unique_ptr<ri_patch_geometry> pObj(
            new ri_patch_geometry(this->RiGetClassifier(), type, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }
    // RtVoid ri_frame_context::RiPatchMesh (RtToken type, RtInt nu, RtToken uwrap,
    // RtInt nv, RtToken vwrap, va_list argptr){/**/}
    RtVoid ri_frame_context::RiPatchMeshV(RtToken type, RtInt nu, RtToken uwrap,
                                          RtInt nv, RtToken vwrap, RtInt n,
                                          RtToken tokens[], RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiPatchMeshV(type, nu, uwrap, nv, vwrap, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        int ustep = 0;
        int vstep = 0;
        if (!ATTR()->get("Basis", "ustep", ustep))
            return;
        if (!ATTR()->get("Basis", "vstep", vstep))
            return;
        type = (RtToken)GetPatchType(type);
        if (!type)
            return;

        std::unique_ptr<ri_patch_mesh_geometry> pObj(
            new ri_patch_mesh_geometry(this->RiGetClassifier(), ustep, vstep, type,
                                       nu, uwrap, nv, vwrap, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }
    // RtVoid ri_frame_context::RiNuPatch (RtInt nu, RtInt uorder, RtFloat uknot[],
    // RtFloat umin, RtFloat umax, RtInt nv, RtInt vorder, RtFloat vknot[], RtFloat
    // vmin, RtFloat vmax, va_list argptr){/**/}
    RtVoid ri_frame_context::RiNuPatchV(RtInt nu, RtInt uorder, RtFloat uknot[],
                                        RtFloat umin, RtFloat umax, RtInt nv,
                                        RtInt vorder, RtFloat vknot[], RtFloat vmin,
                                        RtFloat vmax, RtInt n, RtToken tokens[],
                                        RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiNuPatchV(nu, uorder, uknot, umin, umax, nv, vorder, vknot, vmin,
                            vmax, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_nu_patch_geometry> pObj(new ri_nu_patch_geometry(
            this->RiGetClassifier(), nu, uorder, uknot, umin, umax, nv, vorder, vknot,
            vmin, vmax, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }
    RtVoid ri_frame_context::RiTrimCurve(RtInt nloops, RtInt ncurves[],
                                         RtInt order[], RtFloat knot[],
                                         RtFloat min[], RtFloat max[], RtInt n[],
                                         RtFloat u[], RtFloat v[], RtFloat w[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiTrimCurve(nloops, ncurves, order, knot, min, max, n, u, v, w);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_trim_curve_geometry> pObj(
            new ri_trim_curve_geometry(this->RiGetClassifier(), nloops, ncurves,
                                       order, knot, min, max, n, u, v, w));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());

        ATTR()
            ->add_trim_curve(pObj.get());
        objs_.push_back(pObj.release());
    }
    // RtVoid ri_frame_context::RiSphere (RtFloat radius, RtFloat zmin, RtFloat
    // zmax, RtFloat tmax, va_list argptr){/**/}
    RtVoid ri_frame_context::RiSphereV(RtFloat radius, RtFloat zmin, RtFloat zmax,
                                       RtFloat tmax, RtInt n, RtToken tokens[],
                                       RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiSphereV(radius, zmin, zmax, tmax, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_sphere_geometry> pObj(new ri_sphere_geometry(
            this->RiGetClassifier(), radius, zmin, zmax, tmax, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }
    // RtVoid ri_frame_context::RiCone (RtFloat height, RtFloat radius, RtFloat
    // tmax, va_list argptr){/**/}
    RtVoid ri_frame_context::RiConeV(RtFloat height, RtFloat radius, RtFloat tmax,
                                     RtInt n, RtToken tokens[],
                                     RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiConeV(height, radius, tmax, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_cone_geometry> pObj(new ri_cone_geometry(
            this->RiGetClassifier(), height, radius, tmax, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }
    // RtVoid ri_frame_context::RiCylinder (RtFloat radius,RtFloat zmin,RtFloat
    // zmax,RtFloat tmax, va_list argptr){/**/}
    RtVoid ri_frame_context::RiCylinderV(RtFloat radius, RtFloat zmin, RtFloat zmax,
                                         RtFloat tmax, RtInt n, RtToken tokens[],
                                         RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiCylinderV(radius, zmin, zmax, tmax, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_cylinder_geometry> pObj(new ri_cylinder_geometry(
            this->RiGetClassifier(), radius, zmin, zmax, tmax, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }
    // RtVoid ri_frame_context::RiHyperboloid (RtPoint point1, RtPoint point2,
    // RtFloat tmax, va_list argptr){/**/}
    RtVoid ri_frame_context::RiHyperboloidV(RtPoint point1, RtPoint point2,
                                            RtFloat tmax, RtInt n, RtToken tokens[],
                                            RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiHyperboloidV(point1, point2, tmax, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_hyperboloid_geometry> pObj(new ri_hyperboloid_geometry(
            this->RiGetClassifier(), point1, point2, tmax, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }
    // RtVoid ri_frame_context::RiParaboloid (RtFloat rmax,RtFloat zmin,RtFloat
    // zmax,RtFloat tmax, va_list argptr){/**/}
    RtVoid ri_frame_context::RiParaboloidV(RtFloat rmax, RtFloat zmin, RtFloat zmax,
                                           RtFloat tmax, RtInt n, RtToken tokens[],
                                           RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiParaboloidV(rmax, zmin, zmax, tmax, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_paraboloid_geometry> pObj(new ri_paraboloid_geometry(
            this->RiGetClassifier(), rmax, zmin, zmax, tmax, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }
    // RtVoid ri_frame_context::RiDisk (RtFloat height, RtFloat radius, RtFloat
    // tmax, va_list argptr){/**/}
    RtVoid ri_frame_context::RiDiskV(RtFloat height, RtFloat radius, RtFloat tmax,
                                     RtInt n, RtToken tokens[],
                                     RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiDiskV(height, radius, tmax, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_disk_geometry> pObj(new ri_disk_geometry(
            this->RiGetClassifier(), height, radius, tmax, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }
    // RtVoid ri_frame_context::RiTorus (RtFloat majrad, RtFloat minrad, RtFloat
    // phimin, RtFloat phimax, RtFloat tmax, va_list argptr){/**/}
    RtVoid ri_frame_context::RiTorusV(RtFloat majrad, RtFloat minrad,
                                      RtFloat phimin, RtFloat phimax, RtFloat tmax,
                                      RtInt n, RtToken tokens[],
                                      RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiTorusV(majrad, minrad, phimin, phimax, tmax, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_torus_geometry> pObj(
            new ri_torus_geometry(this->RiGetClassifier(), majrad, minrad, phimin,
                                  phimax, tmax, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }
    // RtVoid ri_frame_context::RiBlobby (RtInt nleaf, RtInt ncode, RtInt code[],
    // RtInt nflt, RtFloat flt[], RtInt nstr, RtToken str[], va_list argptr){/**/}
    RtVoid ri_frame_context::RiBlobbyV(RtInt nleaf, RtInt ncode, RtInt code[],
                                       RtInt nflt, RtFloat flt[], RtInt nstr,
                                       RtToken str[], RtInt n, RtToken tokens[],
                                       RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiBlobbyV(nleaf, ncode, code, nflt, flt, nstr, str, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_blobby_geometry> pObj(
            new ri_blobby_geometry(this->RiGetClassifier(), nleaf, ncode, code, nflt,
                                   flt, nstr, str, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }

    static const TokenToken CURVETYPEMAP[] = {{RI_BILINEAR, RI_LINEAR},
                                              {RI_BIQUADRATIC, RI_QUADRATIC},
                                              {RI_BICUBIC, RI_CUBIC},
                                              {RI_LINEAR, RI_LINEAR},
                                              {RI_QUADRATIC, RI_QUADRATIC},
                                              {RI_CUBIC, RI_CUBIC},
                                              {NULL, NULL}};

    static const char* GetCurveType(RtToken type)
    {
        int i = 0;
        while (CURVETYPEMAP[i].from)
        {
            if (strcmp(CURVETYPEMAP[i].from, type) == 0)
                return CURVETYPEMAP[i].to;
            i++;
        }
        return NULL;
    }

    // RtVoid ri_frame_context::RiCurves (RtToken type, RtInt ncurves, RtInt
    // nvertices[], RtToken wrap, va_list argptr){/**/}
    RtVoid ri_frame_context::RiCurvesV(RtToken type, RtInt ncurves,
                                       RtInt nvertices[], RtToken wrap, RtInt n,
                                       RtToken tokens[], RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiCurvesV(type, ncurves, nvertices, wrap, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        // int ustep = 0;
        int vstep = 0;
        // if(!ATTR()->get("Basis", "ustep", ustep))return;
        if (!ATTR()->get("Basis", "vstep", vstep))
            return;
        type = (RtToken)GetCurveType(type);
        if (!type)
            return;

        std::unique_ptr<ri_curves_geometry> pObj(
            new ri_curves_geometry(this->RiGetClassifier(), vstep, type, ncurves,
                                   nvertices, wrap, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }

    // RtVoid ri_frame_context::RiPoints (RtInt nverts,va_list argptr){/**/}
    RtVoid ri_frame_context::RiPointsV(RtInt nverts, RtInt n, RtToken tokens[],
                                       RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiPointsV(nverts, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_points_geometry> pObj(new ri_points_geometry(
            this->RiGetClassifier(), nverts, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }

    // RtVoid ri_frame_context::RiSubdivisionMesh(RtToken mask, RtInt nf, RtInt
    // nverts[], RtInt verts[], RtInt ntags, RtToken tags[], RtInt numargs[], RtInt
    // intargs[], RtFloat floatargs[], va_list argptr){/**/}
    RtVoid ri_frame_context::RiSubdivisionMeshV(
        RtToken mask, RtInt nf, RtInt nverts[], RtInt verts[], RtInt ntags,
        RtToken tags[], RtInt nargs[], RtInt intargs[], RtFloat floatargs[],
        RtInt n, RtToken tokens[], RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiSubdivisionMeshV(mask, nf, nverts, verts, ntags, tags, nargs,
                                    intargs, floatargs, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_subdivision_mesh_geometry> pObj(
            new ri_subdivision_mesh_geometry(this->RiGetClassifier(), mask, nf,
                                             nverts, verts, ntags, tags, nargs,
                                             intargs, floatargs, n, tokens, params));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }

    RtVoid ri_frame_context::RiHierarchicalSubdivisionMeshV(
        RtToken scheme, RtInt nf, RtInt nverts[], RtInt verts[], RtInt ntags,
        RtToken tags[], RtInt nargs[], RtInt intargs[], RtFloat floatargs[],
        RtString stringargs[], RtInt n, RtToken tokens[], RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiHierarchicalSubdivisionMeshV(scheme, nf, nverts, verts, ntags, tags,
                                                nargs, intargs, floatargs, stringargs,
                                                n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        /*
          std::unique_ptr<ri_subdivision_mesh_geometry> pObj(new
     ri_subdivision_mesh_geometry(this->RiGetClassifier(), mask, nf, nverts,
     verts, ntags, tags, nargs, intargs, floatargs, n, tokens, params));
          pObj->set_attributes(*ATTR());
          pObj->set_transform(*TRNS());
          //add World.
          this->add_geometry(pObj.get());
          objs_.push_back(pObj.release());
          */
    }

    RtVoid ri_frame_context::RiProcedural(RtPointer data, RtBound bound,
                                          RtVoid (*subdivfunc)(RtPointer, RtFloat),
                                          RtVoid (*freefunc)(RtPointer))
    {
        ri_context::RiProcedural(data, bound, subdivfunc, freefunc);
    }

    RtVoid ri_frame_context::RiProcedural(RtToken type, RtInt n, RtToken tokens[],
                                          RtBound bound)
    {
        this->RiProceduralV(type, n, tokens, bound, 0, NULL, NULL);
    }
    RtVoid ri_frame_context::RiProceduralV(RtToken type, RtInt n0,
                                           RtToken tokens0[], RtBound bound,
                                           RtInt n, RtToken tokens[],
                                           RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiProceduralV(type, n0, tokens0, bound, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        ri_attributes attr = *ATTR();
        attr.clear_group();
        std::unique_ptr<ri_procedural_geometry> pObj(new ri_procedural_geometry(
            this->RiGetClassifier(), type, n0, tokens0, bound));
        pObj->set_options(*OPT());
        pObj->set_attributes(attr);
        pObj->set_transform(*TRNS());
        // add World.
        this->add_geometry(pObj.get());
        objs_.push_back(pObj.release());
    }

    // RtVoid ri_frame_context::RiGeometry (RtToken type, va_list argptr){/**/}
    RtVoid ri_frame_context::RiGeometryV(RtToken type, RtInt n, RtToken tokens[],
                                         RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiGeometryV(type, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_geometry> pObj;

        if (strcmp("bunny", type) == 0)
        {
            pObj.reset(new ri_bunny_geometry());
        }
        else if (strcmp("teapot", type) == 0)
        {
            pObj.reset(new ri_teapot_geometry());
        }
        else if ((strcmp("file", type) == 0) || (strcmp("mesh", type) == 0) ||
                 (strcmp("ply", type) == 0) || (strcmp("obj", type) == 0))
        {
            pObj.reset(new ri_file_mesh_geometry(this->RiGetClassifier(), type, n,
                                                 tokens, params));
        }
        else if (strcmp("StructureSynth", type) == 0 ||
                 strcmp("structure_synth", type) == 0)
        {
            pObj.reset(new ri_structure_synth_geometry(this->RiGetClassifier(), n,
                                                       tokens, params));
        }

        if (pObj.get())
        {
            pObj->set_attributes(*ATTR());
            pObj->set_transform(*TRNS());
            // add World.
            this->add_geometry(pObj.get());
            objs_.push_back(pObj.release());
        }
    }

    RtVoid ri_frame_context::RiSolidBegin(RtToken operation)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiSolidBegin(operation);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_solid_geometry> pObj(new ri_solid_geometry(operation));
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        this->push_group(pObj.get());
        this->push_attributes();
        this->push_transform();
        objs_.push_back(pObj.release());
    }

    static ri_solid_geometry* GetSolid(ri_group_geometry* pGroup)
    {
        std::string strType = pGroup->type();
        if (strType == "Solid")
        {
            return dynamic_cast<ri_solid_geometry*>(pGroup);
        }
        else
        {
            return NULL;
        }
    }
    RtVoid ri_frame_context::RiSolidEnd(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiSolidEnd();
            return;
        }
        if (!IFTRUE())
            return;

        ri_group_geometry* pGroup = this->top_group();

        if (pGroup)
        {
            this->pop_transform();
            this->pop_attributes();
            this->pop_group();

            ri_solid_geometry* pSolid = GetSolid(pGroup);
            if (pSolid)
            {
                this->add_geometry(pSolid);
            }
        }
    }

    RtObjectHandle ri_frame_context::RiObjectBegin(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            return ctx->RiObjectBegin();
        }
        if (!IFTRUE())
            return NULL;

        std::unique_ptr<ri_group_geometry> pObj(new ri_group_geometry());
        pObj->set_attributes(*ATTR());
        pObj->set_transform(*TRNS());
        this->push_group(pObj.get());
        this->push_attributes();
        this->push_transform();

        TRNS()
            ->identity();

        ri_group_geometry* pGroup = pObj.get();
        objs_.push_back(pObj.release());
        return pGroup;
    }

    RtVoid ri_frame_context::RiObjectEnd(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiObjectEnd();
            return;
        }
        if (!IFTRUE())
            return;

        ri_group_geometry* pGroup = this->top_group();
        if (pGroup)
        {
            this->pop_transform();
            this->pop_attributes();
            this->pop_group();
            // this->add_geometry(pGroup);
        }
    }

    static ri_geometry* GetGeometry(RtObjectHandle handle)
    {
        ri_geometry* pGeo = static_cast<ri_geometry*>(handle);
        std::string strType = pGeo->type();
        if (!strType.empty())
        {
            return pGeo;
        }
        else
        {
            return NULL;
        }
    }

    RtVoid ri_frame_context::RiObjectInstance(RtInt id)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiObjectInstance(id);
            return;
        }
        if (!IFTRUE())
            return;

        RtObjectHandle handle = RiGetObjectHandle(id);
        if (handle)
        {
            this->RiObjectInstance(handle);
        }
    }

    RtVoid ri_frame_context::RiObjectInstance(RtToken name)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiObjectInstance(name);
            return;
        }
        if (!IFTRUE())
            return;

        RtObjectHandle handle = RiGetObjectHandle(name);
        if (handle)
        {
            this->RiObjectInstance(handle);
        }
    }
    RtVoid ri_frame_context::RiObjectInstance(RtObjectHandle handle)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiObjectInstance(handle);
            return;
        }
        if (!IFTRUE())
            return;

        if (handle)
        {
            ri_geometry* pGeo = GetGeometry(handle);
            if (pGeo)
            {
                std::unique_ptr<ri_object_instance_geometry> pObj(
                    new ri_object_instance_geometry(pGeo));
                pObj->set_attributes(*ATTR());
                pObj->set_transform(*TRNS());
                this->add_geometry(pObj.get());
                objs_.push_back(pObj.release());
            }
        }
    }

    //------------------------------------------------------------------------------------------------

    RtVoid ri_frame_context::RiMotionBeginV(RtInt n, RtFloat times[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiMotionBeginV(n, times);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_motion> ap(new ri_motion(n, times, *TRNS()));

        this->push_attributes();
        this->push_transform();
        this->push_motion(ap.get());

        objs_.push_back(ap.release());
    }

    RtVoid ri_frame_context::RiMotionEnd(void)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiMotionEnd();
            return;
        }
        if (!IFTRUE())
            return;

        ri_motion* mtn = MOTION();

        ri_transform trns = *TRNS();
        ri_attributes attr = *ATTR();

        this->pop_motion();
        this->pop_transform();
        this->pop_attributes();

        if (mtn && mtn->is_completed())
        {
            if (mtn->is_transform())
            {
                int n = mtn->get_n();
                std::vector<float> times(n);
                std::vector<ri_transform> transes(n);
                for (int i = 0; i < n; i++)
                {
                    times[i] = mtn->get_time_at(i);
                    transes[i] = mtn->get_transform_at(i);
                }
                *(TRNS()) = ri_transform(n, &times[0], &transes[0]);
            }
            else
            {
                int n = mtn->get_n();
                std::vector<float> times(n);
                std::vector<const ri_geometry*> geos(n);
                for (int i = 0; i < n; i++)
                {
                    times[i] = mtn->get_time_at(i);
                    geos[i] = mtn->get_geometry_at(i);
                }
                std::unique_ptr<ri_motion_geometry> pObj(
                    new ri_motion_geometry(n, &times[0], &geos[0]));
                pObj->set_attributes(attr);
                pObj->set_transform(trns);
                this->add_geometry(pObj.get());
                objs_.push_back(pObj.release());
            }
        }
    }

    //------------------------------------------------------------------------------------------------

    // RtVoid ri_frame_context::RiMakeTexture(char *pic, char *tex, RtToken swrap,
    // RtToken twrap, RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth,
    // va_list argptr){/**/}
    RtVoid ri_frame_context::RiMakeTextureV(char* pic, char* tex, RtToken swrap,
                                            RtToken twrap, RtToken filterfunc,
                                            RtFloat swidth, RtFloat twidth, RtInt n,
                                            RtToken tokens[], RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiMakeTextureV(pic, tex, swrap, twrap, filterfunc, swidth, twidth, n,
                                tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_make_texture_task> ap(new ri_make_texture_task(
            this->RiGetClassifier(), *OPT(), pic, tex, swrap, twrap, filterfunc,
            swidth, twidth, n, tokens, params));
        task_.push_back(ap.release());
    }

    // RtVoid ri_frame_context::RiMakeBump (char *pic, char *tex, RtToken swrap,
    // RtToken twrap, RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth,
    // va_list argptr){/**/}
    RtVoid ri_frame_context::RiMakeBumpV(char* pic, char* tex, RtToken swrap,
                                         RtToken twrap, RtToken filterfunc,
                                         RtFloat swidth, RtFloat twidth, RtInt n,
                                         RtToken tokens[], RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiMakeBumpV(pic, tex, swrap, twrap, filterfunc, swidth, twidth, n,
                             tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        /**/
    }

    // RtVoid ri_frame_context::RiMakeLatLongEnvironment (char *pic, char *tex,
    // RtFilterFunc filterfunc, RtFloat swidth, RtFloat twidth, va_list
    // argptr){/**/}
    RtVoid ri_frame_context::RiMakeLatLongEnvironmentV(
        char* pic, char* tex, RtToken filterfunc, RtFloat swidth, RtFloat twidth,
        RtInt n, RtToken tokens[], RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiMakeLatLongEnvironmentV(pic, tex, filterfunc, swidth, twidth, n,
                                           tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_make_lat_long_environment_task> ap(
            new ri_make_lat_long_environment_task(this->RiGetClassifier(), *OPT(),
                                                  pic, tex, filterfunc, swidth,
                                                  twidth, n, tokens, params));
        task_.push_back(ap.release());
    }

    // RtVoid ri_frame_context::RiMakeCubeFaceEnvironment (char *px, char *nx, char
    // *py, char *ny, char *pz, char *nz, char *tex, RtFloat fov, RtFilterFunc
    // filterfunc, RtFloat swidth, RtFloat twidth, va_list argptr){/**/}
    RtVoid ri_frame_context::RiMakeCubeFaceEnvironmentV(
        char* px, char* nx, char* py, char* ny, char* pz, char* nz, char* tex,
        RtFloat fov, RtToken filterfunc, RtFloat swidth, RtFloat twidth, RtInt n,
        RtToken tokens[], RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiMakeCubeFaceEnvironmentV(px, nx, py, ny, pz, nz, tex, fov,
                                            filterfunc, swidth, twidth, n, tokens,
                                            params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_make_cube_face_environment_task> ap(
            new ri_make_cube_face_environment_task(
                this->RiGetClassifier(), *OPT(), px, nx, py, ny, pz, nz, tex, fov,
                filterfunc, swidth, twidth, n, tokens, params));
        task_.push_back(ap.release());
    }
    // RtVoid ri_frame_context::RiMakeShadow (char *pic, char *tex, va_list
    // argptr){/**/}
    RtVoid ri_frame_context::RiMakeShadowV(char* pic, char* tex, RtInt n,
                                           RtToken tokens[], RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiMakeShadowV(pic, tex, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_make_shadow_task> ap(new ri_make_shadow_task(
            this->RiGetClassifier(), *OPT(), pic, tex, n, tokens, params));
        task_.push_back(ap.release());
    }
    //
    RtVoid ri_frame_context::RiMakeBrickMapV(int nptc, char** ptcnames,
                                             char* bkmname, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiMakeBrickMapV(nptc, ptcnames, bkmname, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        std::unique_ptr<ri_make_brick_map_task> ap(
            new ri_make_brick_map_task(this->RiGetClassifier(), *OPT(), nptc,
                                       ptcnames, bkmname, n, tokens, params));
        task_.push_back(ap.release());
    }
    //----------------------------------------------------------------------------------------

    RtVoid ri_frame_context::RiArchiveRecord(RtToken type, char* format,
                                             va_list argptr)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiArchiveRecord(type, format, argptr);
            return;
        }
        if (!IFTRUE())
            return;

        /**/
    }

    static bool IsExistFile(const char* szFilePath)
    {
#ifdef _WIN32
        struct _stat s;
        int nRet = _stat(szFilePath, &s);
        return (nRet == 0);
#else
        struct stat s;
        int nRet = stat(szFilePath, &s);
        return (nRet == 0);
#endif
    }

    // RtVoid ri_frame_context::RiReadArchive (RtToken name, RtArchiveCallback
    // callback, va_list argptr){/**/}
    RtVoid ri_frame_context::RiReadArchiveV(RtToken name,
                                            RtArchiveCallback callback, RtInt n,
                                            RtToken tokens[], RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiReadArchiveV(name, callback, n, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        bool bReaded = false;

        // get_archives_paths
        ri_path_resolver pr(*OPT());
        std::vector<std::string> archive_paths = pr.get_archives_paths();
        for (size_t i = 0; i < archive_paths.size(); i++)
        {
            std::string path = archive_paths[i] + name;

            if (IsExistFile(path.c_str()))
            {
                ri_read_archive(path.c_str(), this);
                bReaded = true;
                break;
            }
        }
        if (!bReaded)
        {
            print_log("Cant read!\n");
        }
    }

    RtArchiveHandle ri_frame_context::RiArchiveBeginV(RtToken archivename, RtInt n,
                                                      RtToken tokens[],
                                                      RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            this->push_state();
            STATE()
                ->set_in_archive_nest(true);
            return ctx->RiArchiveBeginV(archivename, n, tokens, params);
        }
        if (!IFTRUE())
            return NULL;

        ri_context* p = NULL;
        try
        {
            std::unique_ptr<ri_context> ctx(new ri_stdout_context(
                *(this->RiGetClassifier()), (const char*)archivename));
            p = ctx.get();
            this->push_archive(ctx.release());
        }
        catch (...)
        {
            std::unique_ptr<ri_context> ctx(new ri_stdout_context());
            p = ctx.get();
            this->push_archive(ctx.release());
        }

        this->push_state();
        STATE()
            ->set_in_archive(true);
        return (RtArchiveHandle)p;
    }

    RtVoid ri_frame_context::RiArchiveEnd(void)
    {
        if (STATE()->is_in_archive_nest())
        {
            ri_context* ctx = ARCH();
            if (ctx)
            {
                ctx->RiArchiveEnd();
            }
            this->pop_state();
            return;
        }

        if (!IFTRUE())
            return;

        this->pop_archive();
        this->pop_state();
    }

    RtVoid ri_frame_context::RiErrorHandler(RtToken name)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiErrorHandler(name);
            return;
        }
        if (!IFTRUE())
            return;
        /**/
    }
    RtVoid ri_frame_context::RiErrorHandler(RtErrorHandler handler)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiErrorHandler(handler);
            return;
        }
        if (!IFTRUE())
            return;
        /**/
    } // for output

    RtVoid ri_frame_context::RiDepthOfField(RtFloat fstop, RtFloat focallength,
                                            RtFloat focaldistance)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiDepthOfField(fstop, focallength, focaldistance);
            return;
        }
        if (!IFTRUE())
            return;

        OPT()
            ->set("DepthOfField", "fstop", fstop);
        OPT()
            ->set("DepthOfField", "focallength", focallength);
        OPT()
            ->set("DepthOfField", "focaldistance", focaldistance);
    }
    // RtVoid ri_frame_context::RiMakeOcclusion (RtInt npics, RtString *picfile,
    // RtString shadowfile, va_list argptr){/**/}
    RtVoid ri_frame_context::RiMakeOcclusionV(RtInt npics, RtString* picfile,
                                              RtString shadowfile, RtInt count,
                                              RtToken tokens[],
                                              RtPointer params[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiMakeOcclusionV(npics, picfile, shadowfile, count, tokens, params);
            return;
        }
        if (!IFTRUE())
            return;

        /**/
    }

    // RtVoid  ri_frame_context::RiShaderLayer ( RtToken type, RtToken name, RtToken
    // layername, va_list argptr){/**/}
    RtVoid ri_frame_context::RiShaderLayerV(RtToken type, RtToken name,
                                            RtToken layername, RtInt count,
                                            RtToken tokens[], RtPointer values[])
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiShaderLayerV(type, name, layername, count, tokens, values);
            return;
        }
        if (!IFTRUE())
            return;
        /**/
    }
    RtVoid ri_frame_context::RiConnectShaderLayers(RtToken type, RtToken layer1,
                                                   RtToken variable1,
                                                   RtToken layer2,
                                                   RtToken variable2)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiConnectShaderLayers(type, layer1, variable1, layer2, variable2);
            return;
        }
        if (!IFTRUE())
            return;

        /**/
    }

    //------------------------------------------------------------------------------------------------

    bool ri_frame_context::evaluate_condition(RtString condition) const
    {
        ri_condition_if cnd(*CLS(), *OPT(), *ATTR(), condition);

        return cnd.evaluate(); // true
    }

    RtVoid ri_frame_context::RiIfBegin(RtString condition)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiIfBegin(condition);
            return;
        }
        bool bCondition = STATE()->is_if_true();
        this->push_state();
        if (bCondition)
        {
            bool bRet = evaluate_condition(condition);
            if (bRet)
            {
                STATE()
                    ->set_if(true);
            }
            else
            {
                STATE()
                    ->set_if(false);
            }
        }
    }
    RtVoid ri_frame_context::RiIfEnd()
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiIfEnd();
            return;
        }

        this->pop_state();
    }
    RtVoid ri_frame_context::RiElse()
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiElse();
            return;
        }
        if (STATE()->is_if_true())
        { // false
            this->pop_state();
            this->push_state();
            STATE()
                ->set_never_if_true(false);
            STATE()
                ->set_if(false);
        }
        else
        { // true
            bool bNeverTrue = STATE()->is_never_if_true();
            this->pop_state();
            this->push_state();
            STATE()
                ->set_never_if_true(bNeverTrue);
            STATE()
                ->set_if(true);
        }
    }
    RtVoid ri_frame_context::RiElseIf(RtString condition)
    {
        ri_context* ctx = ARCH();
        if (ctx)
        {
            ctx->RiElseIf(condition);
            return;
        }
        if (STATE()->is_if_true())
        { // false
            this->pop_state();
            this->push_state();
            STATE()
                ->set_never_if_true(false);
            STATE()
                ->set_if(false);
        }
        else
        { // true
            bool bNeverTrue = STATE()
                                ->is_never_if_true();
            this->pop_state();
            this->push_state();
            STATE()
                ->set_never_if_true(bNeverTrue);
            bool bRet = evaluate_condition(condition);
            if (bRet)
            {
                STATE()
                    ->set_if(true);
            }
            else
            {
                STATE()
                    ->set_if(false);
            }
        }
    }

    RtVoid ri_frame_context::RiProcedural2(RtToken subdivide2func,
                                           RtToken boundfunc, RtInt n,
                                           RtToken tokens[], RtPointer params[]) {}

    RtVoid ri_frame_context::RiSystem(RtString cmd) { ; }

    RtVoid ri_frame_context::RiVolume(RtPointer type, RtBound bounds,
                                      RtInt nvertices[])
    {
        ;
    }

    RtVoid ri_frame_context::RiVolumePixelSamples(RtFloat xsamples,
                                                  RtFloat ysamples)
    {
        OPT()
            ->set("VolumePixelSamples", "xsamples", xsamples);
        OPT()
            ->set("VolumePixelSamples", "ysamples", ysamples);
    }

    RtVoid ri_frame_context::RiVPAtmosphereV(RtToken shadername, RtInt n,
                                             RtToken tokens[], RtPointer params[])
    {
        ;
    }
    RtVoid ri_frame_context::RiVPInteriorV(RtToken shadername, RtInt n,
                                           RtToken tokens[], RtPointer params[])
    {
        ;
    }
    RtVoid ri_frame_context::RiVPSurfaceV(RtToken shadername, RtInt n,
                                          RtToken tokens[], RtPointer params[])
    {
        ;
    }

    RtFilterFunc ri_frame_context::RiGetFilterFunc(RtToken name)
    {
        return ri_context::RiGetFilterFunc(name);
    }
    RtToken ri_frame_context::RiGetFilterFunc(RtFilterFunc func)
    {
        return ri_context::RiGetFilterFunc(func);
    }

    RtLightHandle ri_frame_context::RiGetLightHandle(RtInt id)
    {
        return ri_context::RiGetLightHandle(id);
    }
    RtLightHandle ri_frame_context::RiGetLightHandle(RtToken name)
    {
        return ri_context::RiGetLightHandle(name);
    }
    RtToken ri_frame_context::RiGetLightName(RtLightHandle light)
    {
        return ri_context::RiGetLightName(light);
    }
    RtInt ri_frame_context::RiGetLightID(RtLightHandle light)
    {
        return ri_context::RiGetLightID(light);
    }
    RtVoid ri_frame_context::RiSetLightHandle(RtInt id, RtLightHandle light)
    {
        ri_context::RiSetLightHandle(id, light);
    }
    RtVoid ri_frame_context::RiSetLightHandle(RtToken name, RtLightHandle light)
    {
        ri_context::RiSetLightHandle(name, light);
    }

    RtObjectHandle ri_frame_context::RiGetObjectHandle(RtInt id)
    {
        return ri_context::RiGetObjectHandle(id);
    }
    RtObjectHandle ri_frame_context::RiGetObjectHandle(RtToken name)
    {
        return ri_context::RiGetObjectHandle(name);
    }
    RtToken ri_frame_context::RiGetObjectName(RtObjectHandle obj)
    {
        return ri_context::RiGetObjectName(obj);
    }
    RtInt ri_frame_context::RiGetObjectID(RtObjectHandle obj)
    {
        return ri_context::RiGetObjectID(obj);
    }
    RtVoid ri_frame_context::RiSetObjectHandle(RtInt id, RtObjectHandle obj)
    {
        ri_context::RiSetObjectHandle(id, obj);
    }
    RtVoid ri_frame_context::RiSetObjectHandle(RtToken name, RtObjectHandle obj)
    {
        ri_context::RiSetObjectHandle(name, obj);
    }

    //------------------------------------------------------------------------------------------------

    void ri_frame_context::push_attributes()
    {
        attr_.push_back(new ri_attributes(*(attr_.back())));
    }
    void ri_frame_context::pop_attributes()
    {
        delete attr_.back();
        attr_.pop_back();
    }
    ri_attributes* ri_frame_context::top_attributes()
    { 
        return attr_.back(); 
    }
    const ri_attributes* ri_frame_context::top_attributes() const
    {
        return attr_.back();
    }

    void ri_frame_context::push_options()
    {
        opts_.push_back(new ri_options(*(opts_.back())));
    }
    void ri_frame_context::pop_options()
    {
        delete opts_.back();
        opts_.pop_back();
    }
    ri_options* ri_frame_context::top_options() { return opts_.back(); }
    const ri_options* ri_frame_context::top_options() const { return opts_.back(); }

    void ri_frame_context::push_transform()
    {
        trns_.push_back(new ri_transform(*(trns_.back())));
    }
    void ri_frame_context::pop_transform()
    {
        delete trns_.back();
        trns_.pop_back();
    }
    ri_transform* ri_frame_context::top_transform() { return trns_.back(); }

    void ri_frame_context::push_state()
    {
        states_.push_back(new ri_state(*(states_.back())));
    }
    void ri_frame_context::pop_state()
    {
        delete states_.back();
        states_.pop_back();
    }
    ri_state* ri_frame_context::top_state()
    { 
        return states_.back(); 
    }

    bool ri_frame_context::is_if_true() const
    {
        size_t sz = states_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (!states_[i]->is_if_true())
                return false;
        }
        return true;
    }

    bool ri_frame_context::is_if_true_parent() const
    {
        int sz = (int)states_.size() - 1;
        for (int i = 0; i < sz; i++)
        {
            if (!states_[i]->is_if_true())
                return false;
        }
        return true;
    }

    void ri_frame_context::push_frame()
    {
        std::unique_ptr<ri_frame_task> ap(new ri_frame_task());
        ap->set_options(*OPT());
        frms_.push_back(ap.get());
        task_.push_back(ap.release());
    }
    void ri_frame_context::pop_frame() { frms_.pop_back(); }
    ri_frame_task* ri_frame_context::top_frame()
    {
        if (frms_.empty())
        {
            push_frame();
        }
        return frms_.back();
    }

    void ri_frame_context::add_geometry(ri_geometry* geo)
    {
        {
            ri_motion* pMotion = MOTION();
            if (pMotion)
            {
                pMotion->add_geometry(geo);
                return;
            }
        }
        { // Area Light
            ri_group_geometry* pGroup = ATTR()->top_group();
            if (pGroup)
            {
                pGroup->add(geo);
                return;
            }
        }
        { // Group, Solid,
            ri_group_geometry* pGroup = this->top_group();
            if (pGroup)
            {
                pGroup->add(geo);
                return;
            }
        }
        this->top_frame()->add_geometry(geo);
    }

    void ri_frame_context::push_group(ri_group_geometry* grp)
    {
        grps_.push_back(grp);
    }

    void ri_frame_context::pop_group()
    {
        if (!grps_.empty())
        {
            grps_.pop_back();
        }
    }

    ri_group_geometry* ri_frame_context::top_group()
    {
        if (grps_.empty())
        {
            return NULL;
        }
        else
        {
            return grps_.back();
        }
    }

    void ri_frame_context::push_motion(ri_motion* mtn) { mtns_.push_back(mtn); }

    void ri_frame_context::pop_motion()
    {
        if (!mtns_.empty())
        {
            mtns_.pop_back();
        }
    }

    ri_motion* ri_frame_context::top_motion()
    {
        if (mtns_.empty())
        {
            return NULL;
        }
        else
        {
            return mtns_.back();
        }
    }

    void ri_frame_context::push_archive(ri_context* ctx) { arch_.push_back(ctx); }

    void ri_frame_context::pop_archive()
    {
        if (!arch_.empty())
        {
            ri_context* ctx = arch_.back();
            arch_.pop_back();
            delete ctx;
        }
    }

    ri_context* ri_frame_context::top_archive()
    {
        if (arch_.empty())
        {
            return NULL;
        }
        else
        {
            return arch_.back();
        }
    }

    //------------------------------------
    size_t ri_frame_context::get_task_size() const { return task_.size(); }

    ri_task* ri_frame_context::get_task_at(size_t i) const { return task_[i]; }
}
