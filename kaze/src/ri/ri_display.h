#ifndef RI_DISPLAY_H
#define RI_DISPLAY_H

#include "ri.h"
#include "ri_object.h"
#include "ri_attributes.h"
#include "ri_classifier.h"
#include "ri_transform.h"
#include "ri_parameters.h"

namespace ri
{

    class ri_display : public ri_object
    {
    public:
        ri_display(const ri_classifier* p_cls, RtToken name, RtToken type,
                   RtToken mode, RtInt n, RtToken tokens[], RtPointer params[]);
        ~ri_display();

    public:
        virtual std::string get_name() const { return name_; }
        virtual std::string get_type() const { return type_; }
        virtual std::string get_mode() const { return mode_; }
        virtual std::string type() const { return "Display"; }
        virtual const ri_classifier& get_classifier() const { return clsf_; }
        virtual const ri_parameters& get_parameters() const { return params_; }

    public:
        virtual const ri_parameters::value_type*
        get_parameter(const char* key) const
        {
            return params_.get(key);
        }

    protected:
        std::string name_;
        std::string type_;
        std::string mode_;
        ri_classifier clsf_;
        ri_parameters params_;
    };
}

#endif
