/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_COLOR_H
#define LSD_COLOR_H

#include <vector>
#include "Vector.h"


namespace lsd
{
    class Color
    {
    public:
        Color()
        {
            e_[0] = 0;
            e_[1] = 0;
            e_[2] = 0;
        }
        Color(float r, float g, float b)
        {
            e_[0] = r;
            e_[1] = g;
            e_[2] = b;
        }
        explicit Color(const std::vector<float>& v)
        {
            e_[0] = v[0];
            e_[1] = v[1];
            e_[2] = v[2];
        }
        float& operator[](int i)     {return e_[i];}
        float  operator[](int i)const{return e_[i];}

        operator Vector()const{return Vector(e_[0], e_[1], e_[2]);}
    public:
        static Color White(){return Color(1,1,1);}
    private:
        float e_[3];
    };
}

#endif