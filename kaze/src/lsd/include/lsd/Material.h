/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_MATERIAL_H
#define LSD_MATERIAL_H

#include <memory>
#include "Resource.h"
#include "Parameter.h"
#include "Texture.h"

namespace lsd
{
    class MaterialImp;
    class Material : public Resource
    {
    public:
        typedef std::shared_ptr<Material> Ptr;
    public:
        explicit Material(MaterialImp* imp);
        virtual ~Material();
        MaterialImp* GetImp()const;
        Resource::IDType GetID()const;
        const std::vector<Texture::Ptr>&     GetTextureList()const;
    private:
        MaterialImp* imp_;
    };

    class MaterialBuilderImp;
    class MaterialBuilder
    {
    public:
        MaterialBuilder();
        ~MaterialBuilder();
    public:
        Material::Ptr Build(const Parameter& param)const;
        Material::Ptr BuildDiffuseMaterial(const Parameter& param)const;
    private:
        MaterialBuilderImp* imp_;
    };
}

#endif
