/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_HITINFO_H
#define LSD_HITINFO_H

#include <memory>

namespace lsd
{
    class HitInfoImp;
    class HitInfo
    {
    public:
        HitInfo(HitInfoImp* imp);
        ~HitInfo();
        HitInfoImp* GetImp()const;
    private:
        HitInfoImp* imp_;
    };
}

#endif