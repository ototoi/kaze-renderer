/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_PARAMETER_H
#define LSD_PARAMETER_H

#include <string>
#include <memory>
#include <map>
#include <string>

#include "Vector.h"

namespace lsd
{
    class ParameterValueImp;
    class ParameterValue
    {
    public:
        ParameterValue(const ParameterValue& rhs);
        ParameterValue(int val);
        ParameterValue(float val);
        ParameterValue(const Vector& val);
        ParameterValue(const std::string& val);
        ~ParameterValue();
    public:
        ParameterValue& operator=(const ParameterValue& rhs);
    public:
        int     GetAsInt    ()const;
        float   GetAsFloat  ()const;
        Vector GetAsVector()const;
        std::string GetAsString()const;
    private:
        ParameterValue(ParameterValueImp* imp);
    private:
        ParameterValueImp* imp_;
    };

    class Parameter
    {
    public:
        void Set(const std::string& key, int val);
        void Set(const std::string& key, float val);
        void Set(const std::string& key, double val);
        void Set(const std::string& key, const std::string& val);
        void Set(const std::string& key, const Vector& val);
        const ParameterValue* Get(const std::string& key)const;
        int GetAsInt(const std::string& key, int def = 0)const;
        float GetAsFloat(const std::string& key, float def = 0)const;
        std::string GetAsString(const std::string& key, const std::string& def = "")const;
        Vector GetAsVector(const std::string& key, const Vector& def = Vector())const;
        void Clear();
    protected:
        std::map<std::string, std::shared_ptr<ParameterValue> > vals_;
    };

}

#endif
