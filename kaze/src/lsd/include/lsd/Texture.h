/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_TEXTURE_H
#define LSD_TEXTURE_H

#include <memory>
#include "Resource.h"
#include "Parameter.h"

namespace lsd
{
    class TextureImp;
    class Texture : public Resource
    {
    public:
        typedef std::shared_ptr<Texture> Ptr;
    public:
        explicit Texture(TextureImp* imp);
        virtual ~Texture();
        Resource::IDType GetID()const;
    private:
        TextureImp* imp_;
    };

    class TextureBuilder
    {
    public:
        Texture::Ptr Build(const Parameter& param)const;
    };
}

#endif