/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_RAY_H
#define LSD_RAY_H

#include "Vector.h"
#include "Resource.h"

namespace lsd
{
    class Ray : public Resource
    {
    public:
        Ray(const Vector& org, const Vector& dir, Resource::IDType id = Resource::IDType(0))
            :org_(org), dir_(dir)
        {
            id_ = id;
        }
        const Vector& GetOrigin()const
        {
            return org_;
        }
        const Vector& GetDirection()const
        {
            return dir_;
        }
        Resource::IDType GetID()const{ return id_; }
    protected:
        Vector org_;
        Vector dir_;
        Resource::IDType id_;
    };
}

#endif