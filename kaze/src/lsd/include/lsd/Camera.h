/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_CAMERA_H
#define LSD_CAMERA_H

#include <memory>
#include "Vector.h"
#include "Resource.h"
#include "Parameter.h"

namespace lsd
{
    class CameraImp;
    class Camera : public Resource
    {
    public:
        typedef std::shared_ptr<Camera> Ptr;
        friend class CameraBuilder;
    public:
        explicit Camera(CameraImp* imp);
        virtual ~Camera();
        CameraImp* GetImp()const;
        Resource::IDType GetID()const;
    private:
        CameraImp* imp_;
    };

    class CameraBuilderImp;
    class CameraBuilder
    {
    public:
        CameraBuilder();
        ~CameraBuilder();
    public:
        Camera::Ptr Build(const Parameter& param)const;
    private:
        CameraBuilderImp* imp_;
    };
}

#endif