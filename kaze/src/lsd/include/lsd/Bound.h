/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_BOUND_H
#define LSD_BOUND_H

#include "Vector.h"

namespace lsd
{
    class Bound
    {
    public:
        Bound(const Vector& min, const Vector& max)
            :min_(min), max_(max)
        {}
        const Vector& GetMin()const{return min_;}
        const Vector& GetMax()const{return max_;}
    protected:
        Vector min_;
        Vector max_;
    };
}

#endif