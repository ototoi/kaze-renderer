/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_RENDERER_H
#define LSD_RENDERER_H

#include <memory>
#include "Scene.h"

namespace lsd
{
    class RendererImp;
    class Renderer
    {
    public:
        typedef std::shared_ptr<Renderer> Ptr;
    public:
        explicit Renderer(RendererImp* imp);
        ~Renderer();
    public:
        bool StartRender();
        bool StopRender();
        bool IsRunning();
        int GetFrameNumber();
    private:
        RendererImp* imp_;
    };

    class RendererBuilderImp;
    class RendererBuilder
    {
    public:
        RendererBuilder();
        ~RendererBuilder();
        Renderer::Ptr Build(const Parameter& param)const;
    };
}

#endif