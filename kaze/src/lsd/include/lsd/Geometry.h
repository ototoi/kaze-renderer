/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_GEOMETRY_H
#define LSD_GEOMETRY_H

#include <memory>
#include "Resource.h"
#include "Ray.h"
#include "Bound.h"
#include "HitInfo.h"
#include "Material.h"
#include "Parameter.h"

namespace lsd
{
    class GeometryImp;
    class Geometry : public Resource
    {
    public:
        typedef std::shared_ptr<Geometry> Ptr;
    public:
        explicit Geometry(GeometryImp* imp);
        virtual ~Geometry();
        GeometryImp* GetImp()const;
        Resource::IDType GetID()const;
        void SetMaterial(const Material::Ptr& mat);
        Material::Ptr GetMaterial()const;
    private:
        GeometryImp* imp_;
    };

    class GeometryBuilderImp;
    class GeometryBuilder
    {
    public:
        GeometryBuilder();
        ~GeometryBuilder();
    public:
        Geometry::Ptr Build      (const Parameter& param)const;
        Geometry::Ptr LoadMesh   (const Parameter& param)const;    //for mesh
        Geometry::Ptr BuildSphere(const Parameter& param)const;
        Geometry::Ptr BuildBox   (const Parameter& param)const;
        Geometry::Ptr BuildPlane (const Parameter& param)const;
    private:
        GeometryBuilderImp* imp_;
    };

}

#endif