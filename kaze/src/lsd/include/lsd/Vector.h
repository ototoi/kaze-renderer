/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_VECTOR_H
#define LSD_VECTOR_H

#include <vector>
#include <ostream>

namespace lsd
{
    class Vector
    {
    public:
        Vector()
        {
            e_[0] = 0;
            e_[1] = 0;
            e_[2] = 0;
        }
        Vector(float x, float y, float z)
        {
            e_[0] = x;
            e_[1] = y;
            e_[2] = z;
        }
        explicit Vector(const std::vector<float>& v)
        {
            e_[0] = v[0];
            e_[1] = v[1];
            e_[2] = v[2];
        }
        float& operator[](int i)     {return e_[i];}
        float  operator[](int i)const{return e_[i];}
    private:
        float e_[3];
    };

    template <typename CharT, class Traits>
    std::basic_ostream<CharT, Traits>& operator<<(std::basic_ostream<CharT, Traits>& os, const Vector& rhs)
    {

        std::basic_ostringstream<CharT, Traits> s;
        s.flags(os.flags());
        s.imbue(os.getloc());
        s.precision(os.precision());
        s << "(";
        for (std::size_t i = 0; i < 3 - 1; ++i)
        {
            s << rhs[i] << ",";
        }
        s << rhs[3 - 1] << ")";
        return os << s.str();
    }
}

#endif