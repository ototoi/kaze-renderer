/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_RESOURCE_H
#define LSD_RESOURCE_H

#include <memory>
#include <cstdint>

namespace lsd
{
    class Resource
    {
    public:
        typedef uint64_t IDType;
    public:
        Resource(){}
        virtual ~Resource(){}
        virtual Resource::IDType GetID()const = 0;
    };
}

#endif