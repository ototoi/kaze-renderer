/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_LIGHT_H
#define LSD_LIGHT_H

#include <memory>
#include "Resource.h"
#include "Parameter.h"

namespace lsd
{
    class LightImp;
    class Light : public Resource
    {
    public:
        typedef std::shared_ptr<Light> Ptr;
    public:
        explicit Light(LightImp* imp);
        virtual ~Light();
        LightImp* GetImp()const;
        Resource::IDType GetID()const;
    private:
        LightImp* imp_;
    };

    class LightBuilderImp;
    class LightBuilder
    {
    public:
        LightBuilder();
        ~LightBuilder();
    public:
        Light::Ptr Build(const Parameter& param)const;
    private:
        LightBuilderImp* imp_;
    };
}

#endif