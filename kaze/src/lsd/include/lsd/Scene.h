/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_SCENE_H
#define LSD_SCENE_H

#include <memory>

#include "Resource.h"
#include "Geometry.h"
#include "Light.h"
#include "Material.h"
#include "Texture.h"
#include "Camera.h"
#include "Parameter.h"
#include "Bound.h"

namespace lsd
{
    class SceneImp;
    class Scene : public Resource
    {
    public:
        typedef std::shared_ptr<Scene> Ptr;
    public:
        Scene(SceneImp* imp);
        virtual ~Scene();
        const std::vector<Geometry::Ptr>&    GetGeometryList()const;
        const std::vector<Light::Ptr>&       GetLightList()const;
        const std::vector<Material::Ptr>&    GetMaterialList()const;
        const std::vector<Texture::Ptr>&     GetTextureList()const;
        const std::vector<Camera::Ptr>&      GetCameraList()const;
        Resource::IDType GetID()const;
    protected:
        SceneImp* imp_;
    };

    class SceneBuilderImp;
    class SceneBuilder
    {
    public:
        SceneBuilder();
        ~SceneBuilder();
    public:
        void AddGeometry(const Geometry::Ptr& geometry);
        void AddLight   (const Light::Ptr&    light);
        void AddMaterial(const Material::Ptr& material);
        void AddTexture (const Texture::Ptr&  texture);
        void AddCamera  (const Camera::Ptr&   camera);
        Scene::Ptr Build(const Parameter& param)const;
    private:
        SceneBuilderImp* imp_;
    };
}

#endif