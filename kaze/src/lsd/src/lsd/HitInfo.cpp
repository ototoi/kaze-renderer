#include <lsd/HitInfo.h>

namespace lsd
{
    class HitInfoImp
    {
    public:

        int dummy_;
    };
    
    HitInfo::HitInfo(HitInfoImp* imp)
    {
        imp_ = imp;
    }

    HitInfo::~HitInfo()
    {
        delete imp_;
    }

    HitInfoImp* HitInfo::GetImp()const
    {
        return imp_;
    }
}

