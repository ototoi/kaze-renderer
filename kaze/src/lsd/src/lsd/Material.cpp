#include <lsd/Material.h>
#include "./imp/MaterialImp.h"

namespace lsd
{
    Material::Material(MaterialImp* imp)
    {
        imp_ = imp;
    }

    Material::~Material()
    {
        delete imp_;
    }

    MaterialImp* Material::GetImp()const
    {
        return imp_;
    }

    Resource::IDType Material::GetID()const
    {
        return imp_->GetID();
    }

    MaterialBuilder::MaterialBuilder()
    {
        imp_ = new MaterialBuilderImp();
    }
    
    MaterialBuilder::~MaterialBuilder()
    {
        delete imp_;
    }

    Material::Ptr MaterialBuilder::Build(const Parameter& param)const
    {
        return imp_->Build(param);
    }
}

