#include <lsd/Camera.h>
#include "./imp/CameraImp.h"

namespace lsd
{   
    Camera::Camera(CameraImp* imp)
    {
        imp_ = imp;
    }

    Camera::~Camera()
    {
        delete imp_;
    }

    CameraImp* Camera::GetImp()const
    {
        return imp_;
    }

    Resource::IDType Camera::GetID()const
    {
        return imp_->GetID();
    }

    CameraBuilder::CameraBuilder()
    {
        imp_ = new CameraBuilderImp();
    }
    
    CameraBuilder::~CameraBuilder()
    {
        delete imp_;
    }

    Camera::Ptr CameraBuilder::Build(const Parameter& param)const
    {
        return imp_->Build(param);
    }

}

