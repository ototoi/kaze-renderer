#include <lsd/Scene.h>
#include "./imp/SceneImp.h"

namespace lsd
{
    Scene::Scene(SceneImp* imp)
    {
        imp_ = imp;
    }

    Scene::~Scene()
    {
        delete imp_;
    }

    const std::vector<Geometry::Ptr>&    Scene::GetGeometryList()const
    {
        return imp_->GetGeometryList();
    }

    const std::vector<Light::Ptr>&       Scene::GetLightList()const
    {
        return imp_->GetLightList();
    }

    const std::vector<Material::Ptr>&    Scene::GetMaterialList()const
    {
        return imp_->GetMaterialList();
    }

    const std::vector<Texture::Ptr>&     Scene::GetTextureList()const
    {
        return imp_->GetTextureList();
    }

    const std::vector<Camera::Ptr>&      Scene::GetCameraList()const
    {
        return imp_->GetCameraList();
    }

    Resource::IDType Scene::GetID()const
    {
        return imp_->GetID();
    }

    SceneBuilder::SceneBuilder()
    {
        imp_ = new SceneBuilderImp();
    }

    SceneBuilder::~SceneBuilder()
    {
        delete imp_;
    }

    void SceneBuilder::AddGeometry(const Geometry::Ptr& geometry)
    {
        imp_->AddGeometry(geometry);
    }

    void SceneBuilder::AddLight   (const Light::Ptr&    light)
    {
        imp_->AddLight(light);
    }

    void SceneBuilder::AddMaterial(const Material::Ptr& material)
    {
        imp_->AddMaterial(material);
    }

    void SceneBuilder::AddTexture (const Texture::Ptr&  texture)
    {
        imp_->AddTexture(texture);
    }

    void SceneBuilder::AddCamera  (const Camera::Ptr&   camera)
    {
        imp_->AddCamera(camera);
    }

    Scene::Ptr SceneBuilder::Build(const Parameter& param)const
    {
        return imp_->Build(param);
    }

}
