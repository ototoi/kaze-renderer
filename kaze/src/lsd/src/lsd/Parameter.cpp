#include <lsd/Parameter.h>
#include <vector>

namespace lsd
{
    class ParameterValueImp
    {
    public:
        ParameterValueImp(int val)
        {
            ivals.push_back(val);
        }
        ParameterValueImp(float val)
        {
            fvals.push_back(val);
        }
        ParameterValueImp(const Vector& val)
        {
            fvals.push_back(val[0]);
            fvals.push_back(val[1]);
            fvals.push_back(val[2]);
        }
        ParameterValueImp(const std::string& val)
        {
            svals.push_back(val);
        }
        int     GetAsInt    ()const
        {
            if(ivals.size())return ivals[0];
            return 0;
        }
        float   GetAsFloat  ()const
        {
            if(fvals.size())return fvals[0];
            return 0.0f;
        }
        Vector GetAsVector()const
        {
            if(fvals.size() >= 3)
            {
                return Vector(fvals[0], fvals[1], fvals[2]);
            }
            return Vector();
        }
        std::string GetAsString()const
        {
            if(svals.size())return svals[0];
            return std::string("");
        }

        ParameterValueImp* Clone()const{ return new ParameterValueImp(ivals, fvals, svals); }
    public:
        bool Equal(const ParameterValueImp& other)
        {
            return ivals == other.ivals && fvals == other.fvals && svals == other.svals;
        }
    protected:
        ParameterValueImp(
            const std::vector<int>& ivals, const std::vector<float>& fvals, const std::vector<std::string>& svals
            )
            :ivals(ivals), fvals(fvals), svals(svals)
        {
            
        }
    protected:
        std::vector<int> ivals;
        std::vector<float> fvals;
        std::vector<std::string> svals;
    };

    ParameterValue::ParameterValue(const ParameterValue& rhs)
    {
        imp_ = rhs.imp_->Clone();
    }

    ParameterValue::ParameterValue(int val)
    {
        imp_ = new ParameterValueImp(val);
    }

    ParameterValue::ParameterValue(float val)
    {
        imp_ = new ParameterValueImp(val);
    }

    ParameterValue::ParameterValue(const Vector& val)
    {
        imp_ = new ParameterValueImp(val);
    }

    ParameterValue::ParameterValue(const std::string& val)
    {
        imp_ = new ParameterValueImp(val);
    }

    ParameterValue::~ParameterValue()
    {
        delete imp_;
    }

    ParameterValue::ParameterValue(ParameterValueImp* imp)
    {
        imp_ = imp;
    }

    ParameterValue& ParameterValue::operator=(const ParameterValue& rhs)
    {
        if(this != &rhs)
        {
            ParameterValueImp* n = rhs.imp_->Clone();
            std::swap(imp_, n);
            delete n; 
        }
        return *this;
    }

    int     ParameterValue::GetAsInt    ()const
    {
        return imp_->GetAsInt();
    }

    float   ParameterValue::GetAsFloat  ()const
    {
        return imp_->GetAsFloat();
    }

    Vector ParameterValue::GetAsVector()const
    {
        return imp_->GetAsVector();
    }

    std::string ParameterValue::GetAsString()const
    {
        return imp_->GetAsString();
    }

    void Parameter::Set(const std::string& key, int val)
    {
        std::shared_ptr<ParameterValue> sv(new ParameterValue(val));
        vals_.insert( std::make_pair(key, sv) );
    }

    void Parameter::Set(const std::string& key, float val)
    {
        std::shared_ptr<ParameterValue> sv(new ParameterValue(val));
        vals_.insert( std::make_pair(key, sv) );
    }

    void Parameter::Set(const std::string& key, double val)
    {
        std::shared_ptr<ParameterValue> sv(new ParameterValue((float)val));
        vals_.insert( std::make_pair(key, sv) );
    }

    void Parameter::Set(const std::string& key, const std::string& val)
    {
        std::shared_ptr<ParameterValue> sv(new ParameterValue(val));
        vals_.insert( std::make_pair(key, sv) );
    }

    void Parameter::Set(const std::string& key, const Vector& val)
    {
        std::shared_ptr<ParameterValue> sv(new ParameterValue(val));
        vals_.insert( std::make_pair(key, sv) );
    }

    const ParameterValue* Parameter::Get(const std::string& key)const
    {
        typedef std::map<std::string, std::shared_ptr<ParameterValue> > MapType;
        MapType::const_iterator it = vals_.find(key);
        if(it != vals_.end())
        {
            return it->second.get();
        }
        return NULL;
    }

    int Parameter::GetAsInt(const std::string& key, int def)const
    {
        const ParameterValue* val = this->Get(key);
        if(val)return val->GetAsInt();
        else   return def;
    }

    float Parameter::GetAsFloat(const std::string& key, float def)const
    {
        const ParameterValue* val = this->Get(key);
        if(val)return val->GetAsFloat();
        else   return def;
    }

    std::string Parameter::GetAsString(const std::string& key, const std::string& def)const
    {
        const ParameterValue* val = this->Get(key);
        if(val)return val->GetAsString();
        else   return def;
    }

    Vector Parameter::GetAsVector(const std::string& key, const Vector& def)const
    {
        const ParameterValue* val = this->Get(key);
        if(val)return val->GetAsVector();
        else   return def;
    }

    void Parameter::Clear()
    {
        vals_.clear();
    }
}
