#include <lsd/Geometry.h>
#include "./imp/GeometryImp.h"

namespace lsd
{   
    Geometry::Geometry(GeometryImp* imp)
    {
        imp_ = imp;
    }

    Geometry::~Geometry()
    {
        delete imp_;
    }

    GeometryImp* Geometry::GetImp()const
    {
        return imp_;
    }

    void Geometry::SetMaterial(const Material::Ptr& mat)
    {
        imp_->SetMaterial(mat);
    }

    Material::Ptr Geometry::GetMaterial()const
    {
        return imp_->GetMaterial();
    }

    Resource::IDType Geometry::GetID()const
    {
        return imp_->GetID();
    }

    GeometryBuilder::GeometryBuilder()
    {
        imp_ = new GeometryBuilderImp();
    }
    
    GeometryBuilder::~GeometryBuilder()
    {
        delete imp_;
    }

    Geometry::Ptr GeometryBuilder::Build      (const Parameter& param)const
    {
        return imp_->Build(param);
    }

    Geometry::Ptr GeometryBuilder::LoadMesh   (const Parameter& param)const
    {
        return this->Build(param);
    }

    Geometry::Ptr GeometryBuilder::BuildSphere(const Parameter& param)const
    {
        return this->Build(param);
    }

    Geometry::Ptr GeometryBuilder::BuildBox   (const Parameter& param)const
    {
        return this->Build(param);
    }

    Geometry::Ptr GeometryBuilder::BuildPlane (const Parameter& param)const
    {
        return this->Build(param);
    }
}

