#include "MaterialImp.h"

namespace lsd
{
    MaterialImp::MaterialImp()
    {
        ;//
    }

    Material::Ptr MaterialBuilderImp::Build(const Parameter& param)const
    {
        return Material::Ptr( new Material( new MaterialImp() ) ) ;
    }
}

