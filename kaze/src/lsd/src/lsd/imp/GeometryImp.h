/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_GEOMETRY_IMP_H
#define LSD_GEOMETRY_IMP_H

#include <lsd/Resource.h>
#include <lsd/Parameter.h>
#include <lsd/Geometry.h>
#include <lsd/Material.h>

#include "ResourceImp.h"

namespace lsd
{
    class GeometryImp : public ResourceImpBase<Geometry>
    {
    public:
        GeometryImp();
        virtual ~GeometryImp(){}
        void SetMaterial(const Material::Ptr& mat);
        Material::Ptr GetMaterial()const;
    private:
        Material::Ptr mat_;
    };

    class GeometryBuilderImp
    {
    public:
        Geometry::Ptr Build(const Parameter& param)const;
    };
}

#endif
