/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_SCENE_IMP_H
#define LSD_SCENE_IMP_H

#include <lsd/Resource.h>
#include <lsd/Parameter.h>
#include <lsd/Scene.h>

#include "ResourceImp.h"

namespace lsd
{
    class SceneImp : public ResourceImpBase<Scene>
    {
    public:
        SceneImp();
        ~SceneImp();
        const std::vector<Geometry::Ptr>&    GetGeometryList()const;
        const std::vector<Light::Ptr>&       GetLightList()const;
        const std::vector<Material::Ptr>&    GetMaterialList()const;
        const std::vector<Texture::Ptr>&     GetTextureList()const;
        const std::vector<Camera::Ptr>&      GetCameraList()const;
    protected:
        std::vector<Geometry::Ptr> geoList_;
        std::vector<Light::Ptr> lightList_;
        std::vector<Material::Ptr> matList_;
        std::vector<Texture::Ptr> texList_;
        std::vector<Camera::Ptr> camList_;
    };

    class SceneBuilderImp
    {
    public:
        Scene::Ptr Build(const Parameter& param)const;
        void AddGeometry(const Geometry::Ptr& geometry);
        void AddLight   (const Light::Ptr&    light);
        void AddMaterial(const Material::Ptr& material);
        void AddTexture (const Texture::Ptr&  texture);
        void AddCamera  (const Camera::Ptr&   camera);
    private:
        std::vector<Geometry::Ptr> geoList_;
        std::vector<Light::Ptr> lightList_;
        std::vector<Material::Ptr> matList_;
        std::vector<Texture::Ptr> texList_;
        std::vector<Camera::Ptr> camList_;
    };
}

#endif
