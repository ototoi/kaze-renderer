#include "CameraImp.h"

namespace lsd
{
    CameraImp::CameraImp()
    {
        ;//
    }

    CameraImp::~CameraImp()
    {

    }

    Camera::Ptr CameraBuilderImp::Build(const Parameter& param)const
    {
        return Camera::Ptr( new Camera( new CameraImp() ) ) ;
    }
}

