/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_MATERIAL_IMP_H
#define LSD_MATERIAL_IMP_H

#include <lsd/Resource.h>
#include <lsd/Parameter.h>
#include <lsd/Material.h>

#include "ResourceImp.h"

namespace lsd
{
    class MaterialImp : public ResourceImpBase<Material>
    {
    public:
        MaterialImp();
        virtual ~MaterialImp(){}
    };

    class MaterialBuilderImp
    {
    public:
        Material::Ptr Build(const Parameter& param)const;
    };
}

#endif
