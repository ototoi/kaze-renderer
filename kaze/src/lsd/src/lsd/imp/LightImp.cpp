#include "LightImp.h"

namespace lsd
{
    LightImp::LightImp()
    {
        ;
    }

    Light::Ptr LightBuilderImp::Build(const Parameter& param)const
    {
        return Light::Ptr( new Light( new LightImp() ) ) ;
    }
}

