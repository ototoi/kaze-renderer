/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_CAMERA_IMP_H
#define LSD_CAMERA_IMP_H

#include <lsd/Resource.h>
#include <lsd/Parameter.h>
#include <lsd/Camera.h>

#include "ResourceImp.h"

namespace lsd
{
    class CameraImp : public ResourceImpBase<Camera>
    {
    public:
        CameraImp();
        virtual ~CameraImp();
    };

    class PersectiveCameraImp : public CameraImp
    {

    };

    class CameraBuilderImp
    {
    public:
        Camera::Ptr Build(const Parameter& param)const;
    };
}

#endif
