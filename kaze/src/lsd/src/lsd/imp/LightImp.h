/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_LIGHT_IMP_H
#define LSD_LIGHT_IMP_H

#include <lsd/Resource.h>
#include <lsd/Parameter.h>
#include <lsd/Light.h>

#include "ResourceImp.h"

namespace lsd
{
    class LightImp : public ResourceImpBase<Light>
    {
    public:
        LightImp();
        virtual ~LightImp(){}
    };

    class LightBuilderImp
    {
    public:
        Light::Ptr Build(const Parameter& param)const;
    };
}

#endif
