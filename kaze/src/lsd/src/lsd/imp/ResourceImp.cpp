#include "ResourceImp.h"
#include <cstdint>
#include <atomic>

namespace lsd
{
    static std::atomic<uint32_t> camID_;
    static std::atomic<uint32_t> geoD_;
    static std::atomic<uint32_t> lightID_;
    static std::atomic<uint32_t> matID_;
    static std::atomic<uint32_t> texID_;
    static std::atomic<uint32_t> sceneID_;

    Resource::IDType ResourceIDGenerator::GetID(ResourceTag<Camera>)
    {
        return camID_++ +1000;
    }

    Resource::IDType ResourceIDGenerator::GetID(ResourceTag<Geometry>)
    {
        return geoD_++ +1000;
    }

    Resource::IDType ResourceIDGenerator::GetID(ResourceTag<Light>)
    {
        return lightID_++ +1000;
    }

    Resource::IDType ResourceIDGenerator::GetID(ResourceTag<Material>)
    {
        return matID_++ +1000;
    }

    Resource::IDType ResourceIDGenerator::GetID(ResourceTag<Texture>)
    {
        return texID_++ +1000;
    }

    Resource::IDType ResourceIDGenerator::GetID(ResourceTag<Scene>)
    {
        return sceneID_++ +1000;
    }
}

