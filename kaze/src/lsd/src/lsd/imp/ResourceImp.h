/*
 * Light Style Scene Description
 */
#pragma once

#ifndef LSD_RESOURCE_IMP_H
#define LSD_RESOURCE_IMP_H

#include <lsd/Resource.h>
#include <lsd/Parameter.h>
#include <lsd/Camera.h>
#include <lsd/Geometry.h>
#include <lsd/Light.h>
#include <lsd/Material.h>
#include <lsd/Texture.h>
#include <lsd/Scene.h>

namespace lsd
{
    template< class Target_ >
    class ResourceTag
    {
    public:
        typedef Target_ Target;
    };

    class ResourceIDGenerator
    {
    public:
        static Resource::IDType GetID(ResourceTag<Camera>);
        static Resource::IDType GetID(ResourceTag<Geometry>);
        static Resource::IDType GetID(ResourceTag<Light>);
        static Resource::IDType GetID(ResourceTag<Material>);
        static Resource::IDType GetID(ResourceTag<Texture>);
        static Resource::IDType GetID(ResourceTag<Scene>);
    };

    template< class Target >
    class ResourceImpBase
    {
    public:
        ResourceImpBase()
        {
            nID_ = ResourceIDGenerator::GetID( ResourceTag<Target>() );
        }
        Resource::IDType GetID()const
        {
            return nID_;
        }
    protected:
        Resource::IDType nID_;
    };
}

#endif
