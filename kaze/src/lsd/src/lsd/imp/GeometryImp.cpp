#include "GeometryImp.h"


namespace lsd
{
    GeometryImp::GeometryImp()
    {
        
    }

    void GeometryImp::SetMaterial(const Material::Ptr& mat)
    {
        this->mat_ = mat;
    }
    
    Material::Ptr GeometryImp::GetMaterial()const
    {
        return this->mat_;
    }

    Geometry::Ptr GeometryBuilderImp::Build(const Parameter& param)const
    {
        return Geometry::Ptr( new Geometry( new GeometryImp() ) ) ;
    }
}

