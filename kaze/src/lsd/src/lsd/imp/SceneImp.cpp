#include "SceneImp.h"

namespace lsd
{
    SceneImp::SceneImp()
    {

    }

    SceneImp::~SceneImp()
    {

    }

    const std::vector<Geometry::Ptr>&    SceneImp::GetGeometryList()const
    {
        return geoList_;
    }

    const std::vector<Light::Ptr>&       SceneImp::GetLightList()const
    {
        return lightList_;
    }

    const std::vector<Material::Ptr>&    SceneImp::GetMaterialList()const
    {
        return matList_;
    }

    const std::vector<Texture::Ptr>&     SceneImp::GetTextureList()const
    {
        return texList_;
    }

    const std::vector<Camera::Ptr>&      SceneImp::GetCameraList()const
    {
        return camList_;
    }

    Scene::Ptr SceneBuilderImp::Build(const Parameter& param)const
    {
        return Scene::Ptr( new Scene( new SceneImp() ) );
    }

    void SceneBuilderImp::AddGeometry(const Geometry::Ptr& geometry)
    {

    }

    void SceneBuilderImp::AddLight   (const Light::Ptr&    light)
    {

    }

    void SceneBuilderImp::AddMaterial(const Material::Ptr& material)
    {

    }

    void SceneBuilderImp::AddTexture (const Texture::Ptr&  texture)
    {

    }

    void SceneBuilderImp::AddCamera  (const Camera::Ptr&   camera)
    {

    }

}