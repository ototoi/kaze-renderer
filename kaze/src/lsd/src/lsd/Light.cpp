#include <lsd/Light.h>
#include "./imp/LightImp.h"

namespace lsd
{
    Light::Light(LightImp* imp)
    {
        imp_ = imp;
    }

    Light::~Light()
    {
        delete imp_;
    }

    LightImp* Light::GetImp()const
    {
        return imp_;
    }

    Resource::IDType Light::GetID()const
    {
        return imp_->GetID();
    }

    LightBuilder::LightBuilder()
    {
        imp_ = new LightBuilderImp();
    }
    
    LightBuilder::~LightBuilder()
    {
        delete imp_;
    }

    Light::Ptr LightBuilder::Build(const Parameter& param)const
    {
        return imp_->Build(param);
    }
}

