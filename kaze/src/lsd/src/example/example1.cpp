#include <lsd/lsd.h>
#include <iostream>
#include <sstream>

int main()
{
    using namespace std;
    using namespace lsd;

    GeometryBuilder geoBuilder;
    LightBuilder    lightBuilder;
    MaterialBuilder matBuilder;
    CameraBuilder   camBuilder;
    SceneBuilder    sceneBuilder;

    Parameter param;
    param.Clear();

    Geometry::Ptr geometry = geoBuilder.Build(param);

    param.Set("shape", "sphere");
    param.Set("center", Vector(0,0,6));
    param.Set("radius", 0.5);
    Light::Ptr    light    = lightBuilder.Build(param);
    Light::Ptr    light2   = lightBuilder.Build(param);
    param.Clear();

    param.Set("shader", "light");
    param.Set("color", Color::White());
    param.Set("intensity", 1.0);
    Material::Ptr material = matBuilder.Build(param);
    param.Clear();
    //light->SetMaterial(material);

    param.Set("type", "perspective");
    param.Set("from", Vector(3, 1.75, 1));
    param.Set("to",  Vector(0.75, 0.5, 0));
    param.Set("up",  Vector(0, 0, 1));
    param.Set("fov", 30.0);
    Camera::Ptr   camera   = camBuilder.Build(param);
    
    cout << param.GetAsString("type") << endl;
    cout << param.GetAsVector("from") << endl;
    cout << param.GetAsVector("to") << endl;
    cout << param.GetAsVector("up") << endl;
    cout << param.GetAsFloat("fov") << endl;
    param.Clear();

	//sampler := NewSampler(4, 4)

    sceneBuilder.AddGeometry(geometry);
    sceneBuilder.AddLight   (light);
    sceneBuilder.AddMaterial(material);
    sceneBuilder.AddCamera  (camera);

    Scene::Ptr scene = sceneBuilder.Build(param);
    param.Clear();
	//renderer := NewRenderer(&scene, &camera, sampler, 960, 540)

    cout << "geometry id = " << geometry->GetID() << "\n";
    cout << "light id = " << light->GetID() << "\n";
    cout << "light id = " << light2->GetID() << "\n";
    cout << "material id = " << material->GetID() << "\n";
    cout << "camera id = " << camera->GetID() << "\n";
    cout << "scene id = " << scene->GetID() << "\n";


    return 0;
}
