#ifndef OSD_SUBDIVIDE_MESH_H
#define OSD_SUBDIVIDE_MESH_H

#include <vector>
#include <string>

namespace osd
{
    struct subdivide_mesh_tag
    {
        std::string name;
        std::vector<int> intargs;
        std::vector<float> floatargs;
        std::vector<std::string> stringargs;
    };

    struct subdivide_mesh_cage
    {
        std::vector<int> facenums;
        std::vector<int> indices;
        std::vector<float> vertices;
        std::vector<std::string> vvNames;
        std::vector<float> vvData;
        std::vector<std::string> fvNames;
        std::vector<float> fvData;
        std::vector<subdivide_mesh_tag> tags;
    };

    int subdivide_mesh_catmark(const subdivide_mesh_cage* in, subdivide_mesh_cage* out, int level);
}

#endif
