#include "osd_subdivide_mesh.h"
/*
#include <osdutil/adaptiveEvaluator.h>
#include <osdutil/uniformEvaluator.h>
#include <osdutil/topology.h>

#include <osd/error.h>
*/
#include <stdio.h>
#include <iostream>

namespace osd
{
    /*
    using namespace OpenSubdiv;

    static
    int subdivide_mesh_catmark_(const subdivide_mesh_cage* in, subdivide_mesh_cage* out, int level, std::string *errorMessage)
    {
        if(level == 0)
        {
            *out = *in;
            return 0;
        }
        OsdUtilSubdivTopology topology;
        const std::vector<float>& pointPositions = in->vertices;
        
        if (!topology.Initialize(
            in->vertices.size()/3,
            &in->facenums[0], in->facenums.size(),
            &in->indices[0], in->indices.size(),
            level,
            errorMessage
        )) {
            return false;
        }

        for(int i=0;i<in->tags.size();i++)
        {
            OsdUtilTagData::TagType tagType;
            if(OsdUtilTagData::TagTypeFromString(&tagType, in->tags[i].name))
            {
                std::string name = in->tags[i].name;
                std::vector<int> intargs = in->tags[i].intargs;
                std::vector<float> floatargs = in->tags[i].floatargs;

                if(name == "interpolateboundary")
                {
                    if(intargs.empty())
                    {
                        intargs.push_back(1);
                    }
                }

                topology.tagData.tags.push_back(tagType);
                topology.tagData.numArgs.push_back(intargs.size());
                topology.tagData.numArgs.push_back(floatargs.size());
                topology.tagData.numArgs.push_back(0); 
                for (int i=0; i<intargs.size(); ++i) {
                    topology.tagData.intArgs.push_back(intargs[i]);
                }
                for (int i=0; i<floatargs.size(); ++i) {
                    topology.tagData.floatArgs.push_back(floatargs[i]);
                }
            }
        }

        //topology.refinementLevel = level;

        // Push the vertex varying datas ...
        if(in->vvNames.size())
        {
            topology.vvNames = in->vvNames;
        }

        // Push the face varying datas ...
        if(in->fvNames.size())
        {
            topology.fvNames = in->fvNames;
            topology.fvData  = in->fvData;
        }

        OsdUtilUniformEvaluator uniformEvaluator;

        // Create uniformEvaluator
        if (!uniformEvaluator.Initialize(topology, errorMessage)) {
            //std::cout << "Initialize failed with " << *errorMessage << "\n";        
            return -1;
        }

        // Push the vertex data
        uniformEvaluator.SetCoarsePositions(pointPositions, errorMessage);

        // Push the vertex varying datas ...
        if(in->vvNames.size())
        {
            topology.vvNames = in->vvNames;
            uniformEvaluator.SetCoarseVVData(in->vvData, errorMessage);
        }

        // Refine with one thread
        if (!uniformEvaluator.Refine(1, errorMessage)) {
            //std::cout << "Refine failed with " << *errorMessage << "\n";
            return -1;
        }

        OsdUtilSubdivTopology refinedTopology;
        //refinedTopology.fvNames = topology.fvNames;
        //refinedTopology.fvData  = topology.fvData;
        {
            const float *positions = NULL;
            if (!uniformEvaluator.GetRefinedTopology(
                    &refinedTopology, &positions, errorMessage)) {
                //std::cout << "GetRefinedTopology failed with " << *errorMessage <<"\n";
                return -1;
            }
            if(positions != NULL)
            {
                out->facenums = refinedTopology.nverts;
                out->indices  = refinedTopology.indices;
                out->vertices.resize(refinedTopology.numVertices*3);
                memcpy(&out->vertices[0], positions, sizeof(float)*refinedTopology.numVertices*3);
            }
        }

        if(in->vvNames.size())
        {
            float *vvData = NULL;
            int numFloats = 0;
            if (!uniformEvaluator.GetRefinedVVData(
                    &vvData, &numFloats, NULL, errorMessage)) {
                //std::cout << "GetRefinedVVData failed with " << *errorMessage <<"\n";
                return -1;
            }
            if(vvData != NULL)
            {
                out->vvNames = in->vvNames;
                out->vvData.resize(numFloats);
                memcpy(&out->vvData[0], vvData+0, sizeof(float)*numFloats);
            }
        }
        
        if(in->fvNames.size())
        {
            std::vector<float> fvData;
            uniformEvaluator.GetRefinedFVData(level, in->fvNames, &fvData);
            if(fvData.size())
            {
                out->fvNames = in->fvNames;
                out->fvData = fvData;
            }
        }
        return 0;
    }
    */
    int subdivide_mesh_catmark(const subdivide_mesh_cage* in, subdivide_mesh_cage* out, int level)
    {
        return 0;
        //std::string errorMessage;
        //return subdivide_mesh_catmark_(in, out, level, &errorMessage);
    }
}
