#ifndef KAZE_RANDOM_SAMPLER_HPP
#define KAZE_RANDOM_SAMPLER_HPP

#include "types.h"
#include "sampler.hpp"
#include "random.h"

namespace kaze
{

    class random2d_sampler : public sampler<vector2>
    {
    public:
        random2d_sampler();
        ~random2d_sampler();
        vector2 get() const;

    protected:
        real r() const;

    protected:
        mutable rand_generator* g_;
    };
}

#endif