#ifndef KAZE_SAMPLER_HPP
#define KAZE_SAMPLER_HPP

namespace kaze
{

    template <class T>
    class sampler
    {
    public:
        virtual ~sampler() {}
        virtual T get() const = 0;
    };
}

#endif