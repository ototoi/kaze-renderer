#include "halton_sampler.h"
#include "QMC.h"

namespace kaze
{

    static vector2 GetHalton(int i, unsigned int base0, unsigned int base1)
    {
        double x = ri_vdC(i, base0);
        double y = ri_S(i, base1);
        return vector2(x, y);
    }

    halton_sampler::halton_sampler()
    {
        base0_ = 12345;
        base1_ = 34567;
        i_ = 0;
    }

    halton_sampler::halton_sampler(unsigned int base0, unsigned int base1)
    {
        base0_ = base0;
        base1_ = base1;
        i_ = 0;
    }

    vector2 halton_sampler::get() const
    {
        return GetHalton(i_++, base0_, base1_);
    }
}