#include "random_sampler.h"
#include "QMC.h"
#include <limits>

namespace kaze
{

    random2d_sampler::random2d_sampler()
    {
        g_ = new xor128_generator();
    }

    random2d_sampler::~random2d_sampler()
    {
        delete g_;
    }

    vector2 random2d_sampler::get() const
    {
        return vector2(r(), r());
    }

    real random2d_sampler::r() const
    {
        return (g_->generate()) / std::numeric_limits<rand_t>::max();
    }
}
