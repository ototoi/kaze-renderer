#ifndef KAZE_AGREE_RANGE_LIST_H
#define KAZE_AGREE_RANGE_LIST_H

#include <list>

namespace kaze
{

    struct agree_range_entry
    {
        double min, max;
    };

    class agree_range_list
    {
    public:
        int size() const;
        const agree_range_entry& operator[](int i) const;
        bool empty() const;

    public:
        agree_range_list(double min, double max);
        ~agree_range_list();

        void reset(double min, double max);
        void sub(double min, double max);

    protected:
        void sub_internal(double min, double max);
        void sub_internal(double min, double max, std::list<agree_range_entry>::iterator a, std::list<agree_range_entry>::iterator b);

    private:
        std::list<agree_range_entry> l_;
    };
}

#endif
