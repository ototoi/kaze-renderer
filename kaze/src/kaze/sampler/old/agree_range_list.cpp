#include "agree_range_list.h"

#include "values.h"

#include <list>

namespace kaze
{
    typedef std::list<agree_range_entry>::iterator iterator;

    int agree_range_list::size() const
    {
        return (int)l_.size();
    }
    const agree_range_entry& agree_range_list::operator[](int i) const
    {
        std::list<agree_range_entry>::const_iterator it = l_.begin();
        //for(int p = 0;p<i;p++)it++;
        std::advance(it, i);
        return *it;
        //return  *(std::advance(l_.begin(),i));
    }

    bool agree_range_list::empty() const
    {
        return l_.empty();
    }

    agree_range_list::agree_range_list(double min, double max)
    {
        agree_range_entry tmp;
        tmp.min = min;
        tmp.max = max;
        l_.push_back(tmp);
    }

    agree_range_list::~agree_range_list()
    {
        //
    }

    void agree_range_list::reset(double min, double max)
    {
        l_.clear();
        agree_range_entry tmp;
        tmp.min = min;
        tmp.max = max;
        l_.push_back(tmp);
    }

    void agree_range_list::sub(double min, double max)
    {
        static const double PI2 = (double)(values::pi() * 2);

        if (!l_.empty())
        {
            if (min > PI2)
            {
                sub(min - PI2, max - PI2);
            }
            else if (max < 0)
            {
                sub(min + PI2, max + PI2);
            }
            else if (min < 0)
            {
                sub(0, max);
                sub(min + PI2, PI2);
            }
            else if (max > PI2)
            {
                sub(min, PI2);
                sub(0, max - PI2);
            }
            else
            {
                sub_internal(min, max);
            }
        }
    }

    void agree_range_list::sub_internal(double min, double max)
    {
        iterator a;
        iterator b;

        iterator i = l_.begin();
        for (; i != l_.end(); ++i)
        {
            if (min < i->max) break;
        }
        a = i;
        for (; i != l_.end(); ++i)
        {
            if (max < i->min) break;
        }
        b = i;
        //if(a != b){
        sub_internal(min, max, a, b);
        //}
    }

    void agree_range_list::sub_internal(double min, double max, iterator a, iterator b)
    {
        iterator i = a;
        while (i != b)
        {
            if (min <= i->min)
            { //min<i->min
                if (max < i->max)
                {
                    i->min = max;
                    i++;
                }
                else
                {
                    iterator kill = i;
                    i++;
                    l_.erase(kill);
                }
            }
            else
            { //i->min<min
                if (max < i->max)
                { //split

                    agree_range_entry tmp0;
                    tmp0.min = i->min;
                    tmp0.max = min;

                    //agree_range_entry tmp1;
                    //tmp1.min = max;
                    //tmp1.max = i->max;
                    l_.insert(i, tmp0);

                    i->min = max;

                    i++;
                }
                else
                { //i->min < min < i->max <= max
                    i->max = min;

                    i++;
                }
            }
        }
    }
}