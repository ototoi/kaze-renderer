#ifndef KAZE_LDS_SHADOWMAP_SAMPLER_H
#define KAZE_LDS_SHADOWMAP_SAMPLER_H

#include "sampler.hpp"
#include "oval_sampler_base.hpp"

namespace kaze
{

    class halton_sampler : public sampler<real>,
                           public sampler<vector2>
    {
    public:
        halton_sampler();
        void get(aggregator<real>* agr, int n) const;
        void get(aggregator<vector2>* agr, int n) const;

    public:
        vector2 get2(int i) const;
        vector2 get2() const;
        static halton_sampler& get_instace();

    protected:
        unsigned int base0_;
        unsigned int base1_;
        mutable int i_;
    };

    class hammersley_sampler : public sampler<vector2>
    {
    public:
        void get(aggregator<vector2>* agr, int n) const;

    protected:
        void get_norm(aggregator<vector2>* agr, int n) const;
        void get_perm(aggregator<vector2>* agr, int n) const;
    };
}

#endif