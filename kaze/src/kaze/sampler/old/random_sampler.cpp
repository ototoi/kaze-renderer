#include "random_sampler.h"
#include "random.h"
#include "values.h"

#include <cmath>

#ifdef _MSC_VER
#pragma warning(disable : 4305)
#endif

namespace kaze
{

    static real get01()
    {
        //return real(rand())/RAND_MAX;
        return real(xor128()) / std::numeric_limits<rand_t>::max();
    }

    static real get_point1()
    {
        return get01();
    }

    static vector2 get_point2()
    {
        real x, y;
        x = get01(); //0..+1
        y = get01(); //0..+1
        return vector2(x, y);
    }

    static vector3 get_point3()
    {
        real x, y, z;
        x = get01(); //0..+1
        y = get01(); //0..+1
        z = get01(); //0..+1
        return vector3(x, y, z);
    }

    //-------------------------------------------------------------

    void random_sampler::get(aggregator<real>* agr, int n) const
    {
        for (int i = 0; i < n; i++)
        {
            agr->add(get_point1());
        }
    }

    void random_sampler::get(aggregator<vector2>* agr, int n) const
    {
        for (int i = 0; i < n; i++)
        {
            agr->add(get_point2());
        }
    }

    void random_sampler::get(aggregator<vector3>* agr, int n) const
    {
        for (int i = 0; i < n; i++)
        {
            agr->add(get_point3());
        }
    }
}
