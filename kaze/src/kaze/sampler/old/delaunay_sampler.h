#ifndef KAZE_DELAUNAY_SAMPLER_H
#define KAZE_DELAUNAY_SAMPLER_H

#include "sampler.hpp"

namespace kaze
{

    class delaunay_sampler : public sampler<vector2>
    {
    public:
        delaunay_sampler() {}
        void get(aggregator<vector2>* agr, int n) const;
        void get_qtree(aggregator<vector2>* agr, int n) const;
        void get_heap(aggregator<vector2>* agr, int n) const;
        void get_randomize(aggregator<vector2>* agr, int n) const;
    };
}

#endif