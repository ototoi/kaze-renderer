#include "delaunay_sampler.h"
#include "toroidal_delaunay_sample_grid.h"
#include "toroidal_qtree_delaunay_sample_grid.h"
#include "toroidal_heap_delaunay_sample_grid.h"

#include "random.h"
#include <vector>

namespace kaze
{

    static inline vector2 convert(const vector2f& v)
    {
        return vector2(v[0], v[1]);
    }
    static inline vector2f convert(const vector2& v)
    {
        return vector2f((float)v[0], (float)v[1]);
    }
    static double rand01()
    {
        double x = ((double)xor128()) / (std::numeric_limits<rand_t>::max() - 1);
        return x;
    }
    static vector2 rand_v()
    {
        return vector2(rand01(), rand01());
    }

    void delaunay_sampler::get(aggregator<vector2>* agr, int n) const
    {
        get_heap(agr, n);
    }

    void delaunay_sampler::get_qtree(aggregator<vector2>* agr, int n) const
    {
        toroidal_qtree_delaunay_sample_grid grid;
        grid.reserve(n);
        for (int i = 0; i < n; i++)
        {
            agr->add(convert(grid.get()));
        }
    }

    void delaunay_sampler::get_heap(aggregator<vector2>* agr, int n) const
    {
        toroidal_heap_delaunay_sample_grid grid;
        grid.reserve(n);
        for (int i = 0; i < n; i++)
        {
            agr->add(convert(grid.get()));
        }
    }

#define N 4
    void delaunay_sampler::get_randomize(aggregator<vector2>* agr, int n) const
    {
        //
        assert(0);
    }
}
