#ifndef KAZE_TOROIDAL_DELAUNAY_SAMPLE_GRID_H
#define KAZE_TOROIDAL_DELAUNAY_SAMPLE_GRID_H

#include "toroidal_heap_delaunay_sample_grid.h"

namespace kaze
{
    typedef toroidal_heap_delaunay_sample_grid toroidal_delaunay_sample_grid;
}

#endif
