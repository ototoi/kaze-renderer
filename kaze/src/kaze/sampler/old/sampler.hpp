#ifndef KAZE_SAMPLER_HPP
#define KAZE_SAMPLER_HPP

#include "types.h"
#include "aggregator.hpp"

namespace kaze
{

    template <class T>
    class sampler
    {
    public:
        virtual ~sampler() {}
    public:
        virtual void get(aggregator<T>* agr, int n) const
        {
            for (int i = 0; i < n; i++)
                agr->add(this->get());
        }
        virtual T get() const { return T(); }
    };
}

#endif