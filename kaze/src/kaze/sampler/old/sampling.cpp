#include "sampling.h"
#include "random.h"
#include <vector>

#include "values.h"

namespace kaze
{

    static real rand01()
    {
        static const real c = 1.0 / (real(std::numeric_limits<rand_t>::max()) + 1);
        return real(xor128()) * c;
    }

    static void random_permute(int n, double* x)
    {
        int i, j;
        double tmp;

        for (i = 1; i < n; i++)
        {
            j = (int)(rand01() * (double)(i + 1));

            assert(j >= 0);
            assert(j < n);

            /* swap index i and j. */
            tmp = x[j];
            x[j] = x[i];
            x[i] = tmp;
        }
    }

    void NLOOK_shuffle(vector2* v, size_t n, int div)
    {
        static const real EPSILON = values::epsilon() * 1000;

        std::vector<std::vector<vector2> > grid_y(div);
        std::vector<std::vector<vector2> > grid_x(div);

        std::vector<real> vline;

        //Y DIVIDE
        //----------------------------------
        for (size_t i = 0; i < n; i++)
        {
            real f = v[i][1];
            int iy = (int)floor(f * div * (1 - EPSILON));
            assert(iy < div);
            grid_y[iy].push_back(v[i]);
        }

        for (int y = 0; y < div; y++)
        {
            size_t sz = grid_y[y].size();
            vline.resize(sz);
            for (size_t i = 0; i < sz; i++)
            {
                vline[i] = grid_y[y][i][0];
            }

            random_permute((int)sz, &(vline[0]));

            for (size_t i = 0; i < sz; i++)
            {
                grid_y[y][i][0] = vline[i];
            }
        }

        //X DIVIDE
        //----------------------------------
        for (int y = 0; y < div; y++)
        {
            size_t sz = grid_y[y].size();
            for (size_t i = 0; i < sz; i++)
            {
                real f = grid_y[y][i][0];
                int ix = (int)floor(f * div * (1 - EPSILON));
                grid_x[ix].push_back(grid_y[y][i]);
            }
        }

        for (int x = 0; x < div; x++)
        {
            size_t sz = grid_x[x].size();
            vline.resize(sz);
            for (size_t i = 0; i < sz; i++)
            {
                vline[i] = grid_x[x][i][1];
            }

            random_permute((int)sz, &(vline[0]));

            for (size_t i = 0; i < sz; i++)
            {
                grid_x[x][i][1] = vline[i];
            }
        }

        //-----------------------------------
        int k = 0;
        for (int x = 0; x < div; x++)
        {
            size_t sz = grid_x[x].size();
            for (size_t i = 0; i < sz; i++)
            {
                v[k] = grid_x[x][i];
                k++;
            }
        }
    }
}
