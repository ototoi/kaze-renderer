#ifndef KAZE_RANDOM_SAMPLER_H
#define KAZE_RANDOM_SAMPLER_H

#include "sampler.hpp"

namespace kaze
{

    class random_sampler : public sampler<real>,
                           public sampler<vector2>,
                           public sampler<vector3>
    {
    public:
        void get(aggregator<real>* agr, int n) const;
        void get(aggregator<vector2>* agr, int n) const;
        void get(aggregator<vector3>* agr, int n) const;
    };
}

#endif