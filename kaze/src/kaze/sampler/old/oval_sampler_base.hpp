#ifndef KAZE_OVAL_SAMPLER_BASE_HPP
#define KAZE_OVAL_SAMPLER_BASE_HPP

#include "types.h"
#include "values.h"
#include "aggregator.hpp"

namespace kaze
{

    template <class BASE_SAMPLER>
    class oval_sampler_base : public BASE_SAMPLER
    {
    public:
        class oval_filter2 : public aggregator<vector2>
        {
        public:
            oval_filter2(aggregator<vector2>* agr) : agr_(agr) {}

            void add(const vector2& rhs)
            {
                real x = rhs[0];
                real y = rhs[1];
                if (x * x + y * y <= real(1))
                {
                    agr_->add(rhs);
                }
            }

        private:
            aggregator<vector2>* agr_;
        };

        static int to_oval(int nSamples) { return int(ceil(nSamples * values::pi() * 1.0 / 4)); }

        oval_sampler_base(int nSamples) : BASE_SAMPLER(to_oval(nSamples)) {}
    public:
        void get(aggregator<vector2>* agr) const
        {
            oval_filter2 sf(agr);
            BASE_SAMPLER::get(&sf);
        }
    };
}

#endif
