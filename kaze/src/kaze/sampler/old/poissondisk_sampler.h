#ifndef KAZE_POISSONDISK_SAMPLER_H
#define KAZE_POISSONDISK_SAMPLER_H

#include "sampler.hpp"
#include <vector>

namespace kaze
{

    class poissondisk_sampler_imp;

    class poissondisk_sampler : public sampler<vector2>
    {
    public:
        poissondisk_sampler();
        ~poissondisk_sampler();
        void get(aggregator<vector2>* agr, int n) const;

    private:
        poissondisk_sampler_imp* imp_;
    };
}

#endif