#include "toroidal_qtree_delaunay_sample_grid.h"

//   References:
//     http://kanamori.cs.tsukuba.ac.jp/projects/delaunay_blue_noise/
//     Yoshihiro Kanamori, Zoltan Szego, Tomoyuki Nishita: "Deterministic Blue Noise Sampling by Solving Largest Empty Circle Problems," Journal of IIEEJ, Vol.40, No.1, 2011-1.

#include <float.h>
#include <assert.h>
#include <algorithm>
#include <memory>

namespace kaze
{
    namespace
    {
        struct point
        {
            point() {}
            explicit point(const vector2f* p_, const vector2f& o_ = vector2f(0, 0)) : p(p_), o(o_) {}
            point& operator=(const vector2f* p_)
            {
                p = p_;
                o = vector2f(0, 0);
                return *this;
            }
            vector2f evaluate() const { return *p + o; }

            const vector2f* p;
            vector2f o;
        };
        struct tri
        {
            float area; //
            point points[3];
            struct tri* neighbour[3];
        };
    }

    struct toroidal_qtree_delaunay_sample_grid::qtree
    {
        tri* pMax;
        bool bLeaf;
        void* children[4];
    };

    typedef toroidal_qtree_delaunay_sample_grid::qtree qtree;
    typedef toroidal_qtree_delaunay_sample_grid::weight_mapper weight_mapper;

    static const int TRAP[] = {0, 1, 2, 0, 1, 2};

    static inline float tri_area(const vector2f& a, const vector2f& b, const vector2f& c)
    {
        return (a[0] - c[0]) * (b[1] - c[1]) - (a[1] - c[1]) * (b[0] - c[0]); //
    }

    static inline float circum_radius(const vector2f& a, const vector2f& b, const vector2f& c)
    {
        float A = length(b - c);
        float B = length(c - a);
        float C = length(a - b);

        return (A * B * C) / sqrt((A + B + C) * (-A + B + C) * (A - B + C) * (A + B - C));
    }

    static bool is_incircle(const vector2f& pa, const vector2f& pb, const vector2f& pc, const vector2f& pd)
    {
        float adx = pa[0] - pd[0];
        float ady = pa[1] - pd[1];
        float bdx = pb[0] - pd[0];
        float bdy = pb[1] - pd[1];

        float adxbdy = adx * bdy;
        float bdxady = bdx * ady;
        float oabd = adxbdy - bdxady;

        if (oabd <= 0)
        {
            return false;
        }

        float cdx = pc[0] - pd[0];
        float cdy = pc[1] - pd[1];

        float cdxady = cdx * ady;
        float adxcdy = adx * cdy;
        float ocad = cdxady - adxcdy;

        if (ocad <= 0)
        {
            return false;
        }

        float bdxcdy = bdx * cdy;
        float cdxbdy = cdx * bdy;

        float alift = adx * adx + ady * ady;
        float blift = bdx * bdx + bdy * bdy;
        float clift = cdx * cdx + cdy * cdy;

        float det = alift * (bdxcdy - cdxbdy) + blift * ocad + clift * oabd;

        return det > 0;
    }

    namespace
    {
        struct tri_sorter
        {
            bool operator()(tri* a, tri* b) const
            {
                return a->area > b->area;
            }
        };

        struct area_mapper : public toroidal_qtree_delaunay_sample_grid::weight_mapper
        {
            float get(const vector2f& a, const vector2f& b, const vector2f& c) const
            {
                return tri_area(a, b, c);
            }
        };

        struct circum_mapper : public toroidal_qtree_delaunay_sample_grid::weight_mapper
        {
            float get(const vector2f& a, const vector2f& b, const vector2f& c) const
            {
                return circum_radius(a, b, c);
            }
        };
    }

    static tri* clone_tri(tri* t)
    {
        tri* n = new tri();
        memcpy(n, t, sizeof(tri));
        return n;
    }

    static float area_tri(tri* t)
    {
        assert(t);
        assert(t->points[0].p);
        assert(t->points[1].p);
        assert(t->points[2].p);

        vector2f a = t->points[0].evaluate();
        vector2f b = t->points[1].evaluate();
        vector2f c = t->points[2].evaluate();

        float fRet = tri_area(a, b, c);
        assert(fRet > 0);
        return fRet;
    }

    static vector2f center_tri(tri* t)
    {
        assert(t);
        assert(t->points[0].p);
        assert(t->points[1].p);
        assert(t->points[2].p);

        vector2f a = t->points[0].evaluate();
        vector2f b = t->points[1].evaluate();
        vector2f c = t->points[2].evaluate();

        return (a + b + c) * (1.0f / 3.0f);
    }

    static void replace_tri(tri* a, tri* b, tri* c) //b->c
    {
        if (a)
        {
            for (int i = 0; i < 3; i++)
            {
                if (a->neighbour[i] == b)
                {
                    a->neighbour[i] = c;
                    break;
                }
            }
        }
    }

    static size_t get_bytes_qtree(qtree* q)
    {
        size_t sz = 0;
        if (!q) return 0;
        sz += sizeof(qtree);
        if (q->bLeaf)
        { //leaf
            for (int i = 0; i < 4; i++)
            {
                if (q->children[i]) sz += sizeof(tri);
            }
        }
        else
        {
            for (int i = 0; i < 4; i++)
            {
                if (q->children[i]) sz += get_bytes_qtree((qtree*)q->children[i]);
            }
        }
        return sz;
    }

    static qtree* branch_qtree()
    {
        qtree* pRet = new qtree();
        memset(pRet, 0, sizeof(qtree));
        pRet->bLeaf = false;
        return pRet;
    }

    static qtree* leaf_qtree()
    {
        qtree* pRet = new qtree();
        memset(pRet, 0, sizeof(qtree));
        pRet->bLeaf = true;
        return pRet;
    }

    static bool is_intri(tri* t, const vector2f& p, const vector2f& offset)
    {
        vector2f a = t->points[0].evaluate();
        vector2f b = t->points[1].evaluate();
        vector2f c = t->points[2].evaluate();

        a -= offset;
        b -= offset;
        c -= offset;

        float area0 = tri_area(a, b, p);
        float area1 = tri_area(b, c, p);
        float area2 = tri_area(c, a, p);

        if (area0 <= 0 && area1 <= 0 && area2 <= 0) return true;
        if (area0 >= 0 && area1 >= 0 && area2 >= 0) return true;

        return false;
    }

    static bool is_intri(tri* t, const vector2f& p)
    {
        if (is_intri(t, p, vector2f(0, 0))) return true;

        if (is_intri(t, p, vector2f(-1, -1))) return true;
        if (is_intri(t, p, vector2f(-1, 0))) return true;
        if (is_intri(t, p, vector2f(-1, +1))) return true;

        if (is_intri(t, p, vector2f(0, -1))) return true;
        //if(is_intri(t,p,vector2f( 0, 0)))return true;
        if (is_intri(t, p, vector2f(0, +1))) return true;

        if (is_intri(t, p, vector2f(+1, -1))) return true;
        if (is_intri(t, p, vector2f(+1, 0))) return true;
        if (is_intri(t, p, vector2f(+1, +1))) return true;

        return false;
    }

    static tri* find_tri_qtree(qtree* q, float x0, float x1, float y0, float y1, float cx, float cy)
    {
        if (!q) return NULL;
        if (q->bLeaf)
        { //leaf
            vector2f p(cx, cy);
            for (int i = 0; i < 4; i++)
            {
                tri* t = (tri*)q->children[i];
                if (t)
                {
                    if (is_intri(t, p))
                    {
                        return t;
                    }
                }
            }
            return NULL;
        }
        else
        {
            float xm = (x0 + x1) * 0.5f;
            float ym = (y0 + y1) * 0.5f;
            float cx0 = x0;
            float cx1 = x1;
            float cy0 = y0;
            float cy1 = y1;

            int nFlag = 0;
            if (cx < xm)
            {
                cx1 = xm;
            }
            else
            {
                cx0 = xm;
                nFlag |= 1;
            }
            if (cy < ym)
            {
                cy1 = ym;
            }
            else
            {
                cy0 = ym;
                nFlag |= 2;
            }
            tri* t = find_tri_qtree((qtree*)q->children[nFlag], cx0, cx1, cy0, cy1, cx, cy);
            if (t) return t;
            for (int i = 0; i < 4; i++)
            {
                if (i != nFlag)
                {
                    if (i & 1)
                    {
                        cx0 = xm;
                        cx1 = x1;
                    }
                    else
                    {
                        cx0 = x0;
                        cx1 = xm;
                    }
                    if (i & 2)
                    {
                        cy0 = ym;
                        cy1 = y1;
                    }
                    else
                    {
                        cy0 = y0;
                        cy1 = ym;
                    }
                    t = find_tri_qtree((qtree*)q->children[i], cx0, cx1, cy0, cy1, cx, cy);
                    if (t) return t;
                }
            }
            return NULL;
        }
    }

    static void remove_qtree(qtree* q, tri* t, float x0, float x1, float y0, float y1, float cx, float cy)
    {
        if (q->bLeaf)
        { //leaf
            q->pMax = NULL;
            for (int i = 0; i < 4; i++)
            {
                if (((tri*)q->children[i]) == t)
                {
                    q->children[i] = NULL;
                }
                else
                {
                    if (q->children[i])
                    {
                        if (q->pMax)
                        {
                            if (q->pMax->area < ((tri*)q->children[i])->area)
                            {
                                q->pMax = (tri*)q->children[i];
                            }
                        }
                        else
                        {
                            q->pMax = (tri*)q->children[i];
                        }
                    }
                }
            }
        }
        else
        {
            float xm = (x0 + x1) * 0.5f;
            float ym = (y0 + y1) * 0.5f;

            int nFlag = 0;
            if (cx < xm)
            {
                x1 = xm;
            }
            else
            {
                x0 = xm;
                nFlag |= 1;
            }
            if (cy < ym)
            {
                y1 = ym;
            }
            else
            {
                y0 = ym;
                nFlag |= 2;
            }
            if (q->children[nFlag])
            {
                remove_qtree((qtree*)q->children[nFlag], t, x0, x1, y0, y1, cx, cy);
            }
            q->pMax = NULL;
            for (int i = 0; i < 4; i++)
            {
                if (q->children[i])
                {
                    if (q->pMax)
                    {
                        qtree* tmp = (qtree*)q->children[i];
                        if (tmp->pMax)
                        {
                            if (q->pMax->area < tmp->pMax->area)
                            {
                                q->pMax = tmp->pMax;
                            }
                        }
                    }
                    else
                    {
                        q->pMax = ((qtree*)q->children[i])->pMax;
                    }
                }
            }
        }
    }

    static void insert_qtree(qtree* q, tri* t, float x0, float x1, float y0, float y1, float cx, float cy)
    {
        if (q->pMax)
        {
            if (q->pMax->area < t->area)
            {
                q->pMax = t;
            }
        }
        else
        {
            q->pMax = t;
        }

        if (q->bLeaf)
        { //leaf
            float xm = (x0 + x1) * 0.5f;
            float ym = (y0 + y1) * 0.5f;

            int nFlag = 0;
            if (cx < xm)
            {
                //x1 = xm;
            }
            else
            {
                //x0 = xm;
                nFlag |= 1;
            }
            if (cy < ym)
            {
                //y1 = ym;
            }
            else
            {
                //y0 = ym;
                nFlag |= 2;
            }
            if (q->children[nFlag])
            {
                q->bLeaf = false;
                for (int i = 0; i < 4; i++)
                {
                    if (!q->children[i]) continue;

                    float xx0 = x0;
                    float xx1 = x1;
                    float yy0 = y0;
                    float yy1 = y1;
                    if (i & 1)
                    {
                        xx0 = xm;
                    }
                    else
                    {
                        xx1 = xm;
                    }
                    if (i & 2)
                    {
                        yy0 = ym;
                    }
                    else
                    {
                        yy1 = ym;
                    }

                    vector2f cv = center_tri((tri*)q->children[i]);
                    float ccx = cv[0] - floor(cv[0]);
                    float ccy = cv[1] - floor(cv[1]);

                    qtree* tmp = leaf_qtree();
                    insert_qtree(tmp, (tri*)q->children[i], xx0, xx1, yy0, yy1, ccx, ccy);
                    if (nFlag == i)
                    {
                        insert_qtree(tmp, t, xx0, xx1, yy0, yy1, cx, cy);
                    }

                    q->children[i] = tmp;
                }
            }
            else
            {
                q->children[nFlag] = t;
            }
        }
        else
        {
            float xm = (x0 + x1) * 0.5f;
            float ym = (y0 + y1) * 0.5f;

            int nFlag = 0;
            if (cx < xm)
            {
                x1 = xm;
            }
            else
            {
                x0 = xm;
                nFlag |= 1;
            }
            if (cy < ym)
            {
                y1 = ym;
            }
            else
            {
                y0 = ym;
                nFlag |= 2;
            }
            if (!q->children[nFlag])
            {
                q->children[nFlag] = leaf_qtree();
            }
            insert_qtree((qtree*)q->children[nFlag], t, x0, x1, y0, y1, cx, cy);
        }
    }

    static tri* find_tri_qtree(qtree* q, const vector2f& cv)
    {
        float cx = cv[0] - floor(cv[0]);
        float cy = cv[1] - floor(cv[1]);
        return find_tri_qtree(q, 0, 1, 0, 1, cx, cy);
    }

    static void remove_qtree(qtree* q, tri* t)
    {
        vector2f cv = center_tri(t);
        float cx = cv[0] - floor(cv[0]);
        float cy = cv[1] - floor(cv[1]);
        remove_qtree(q, t, 0, 1, 0, 1, cx, cy);
    }

    static void insert_qtree(qtree* q, tri* t)
    {
        vector2f cv = center_tri(t);
        float cx = cv[0] - floor(cv[0]);
        float cy = cv[1] - floor(cv[1]);
        insert_qtree(q, t, 0, 1, 0, 1, cx, cy);
    }

    void destroy_qtree(qtree* q)
    {
        if (q->bLeaf)
        {
            for (int i = 0; i < 4; i++)
            {
                if (q->children[i])
                {
                    delete ((tri*)q->children[i]);
                }
            }
        }
        else
        {
            for (int i = 0; i < 4; i++)
            {
                if (q->children[i])
                {
                    destroy_qtree((qtree*)q->children[i]);
                }
            }
        }
        delete q;
    }

    static bool test_legalize(const vector2f& a, const vector2f& b, const vector2f& c, const vector2f& d)
    {
        if (tri_area(c, a, d) <= 0)
        {
            return false;
        }
        if (tri_area(a, b, d) <= 0)
        {
            return false;
        }

        if (is_incircle(a, b, c, d))
        {
            return true;
        }

        return false;
    }

    static float get_weight(tri* t, const weight_mapper& wm)
    {
        vector2f a = t->points[0].evaluate();
        vector2f b = t->points[1].evaluate();
        vector2f c = t->points[2].evaluate();
        return wm.get(a, b, c);
    }

    static void legalize_tri(qtree* q, tri* t, int i, const weight_mapper& wm)
    {
        assert(t);
        tri* s = t->neighbour[i];
        if (!s) return;

        const vector2f* a = t->points[TRAP[i]].p;
        const vector2f* b = t->points[TRAP[i + 1]].p;
        const vector2f* c = t->points[TRAP[i + 2]].p;

        int j = 0;
        if (s->points[2].p == b)
        {
            j = 0;
        }
        else if (s->points[0].p == b)
        {
            j = 1;
        }
        else
        {
            j = 2;
        }

        const vector2f* d = s->points[j].p;

        vector2f pa = t->points[TRAP[i]].evaluate();
        vector2f pb = t->points[TRAP[i + 1]].evaluate();
        vector2f pc = t->points[TRAP[i + 2]].evaluate();

        vector2f pb2 = s->points[TRAP[j + 2]].evaluate();
        vector2f offset = pb - pb2;
        vector2f pd = s->points[j].evaluate() + offset;

        if (test_legalize(pa, pb, pc, pd))
        {
            remove_qtree(q, t);
            remove_qtree(q, s);

            tri* t2 = t->neighbour[TRAP[i + 2]];
            tri* s2 = s->neighbour[TRAP[j + 2]];

            //FLIP
            t->points[TRAP[i + 1]] = point(d, s->points[j].o + offset);
            s->points[TRAP[j + 1]] = point(a, t->points[i].o - offset);

            t->neighbour[TRAP[i + 2]] = s;
            s->neighbour[TRAP[j + 2]] = t;

            t->neighbour[TRAP[i + 0]] = s2;
            s->neighbour[TRAP[j + 0]] = t2;

            replace_tri(t2, t, s); //
            replace_tri(s2, s, t); //

            //area
            t->area = get_weight(t, wm);
            s->area = get_weight(t, wm);

            insert_qtree(q, t);
            insert_qtree(q, s);

            //call
            legalize_tri(q, t, TRAP[i + 0], wm);
            legalize_tri(q, s, TRAP[j + 1], wm);
        }
    }

    //-----------------------------------------------------------------------------
    tri* init_tri(const point& a, const point& b, const point& c)
    {
        tri* t = new tri();
        memset(t, 0, sizeof(tri));
        t->points[0] = a;
        t->points[1] = b;
        t->points[2] = c;
        return t;
    }

    static void create_initial_tri(std::vector<vector2f*>& points, std::vector<tri*>& tmp, const weight_mapper& wm)
    {
        points.push_back(new vector2f(0.0f, 0.0f)); //0
        points.push_back(new vector2f(0.5f, 0.0f)); //1
        points.push_back(new vector2f(0.0f, 0.5f)); //2
        points.push_back(new vector2f(0.5f, 0.5f)); //3

        point kp[9];
        kp[0] = point(points[0], vector2f(0, 0));
        kp[1] = point(points[1], vector2f(0, 0));
        kp[2] = point(points[0], vector2f(1, 0)); //
        kp[3] = point(points[2], vector2f(0, 0));
        kp[4] = point(points[3], vector2f(0, 0));
        kp[5] = point(points[2], vector2f(1, 0)); //
        kp[6] = point(points[0], vector2f(0, 1));
        kp[7] = point(points[1], vector2f(0, 1));
        kp[8] = point(points[0], vector2f(1, 1)); //

        tmp.push_back(init_tri(kp[0], kp[1], kp[4])); // ]
        tmp.push_back(init_tri(kp[0], kp[4], kp[3])); //[
        tmp.push_back(init_tri(kp[1], kp[2], kp[5])); //   ]
        tmp.push_back(init_tri(kp[1], kp[5], kp[4])); //  [
        tmp.push_back(init_tri(kp[3], kp[4], kp[7])); // ]
        tmp.push_back(init_tri(kp[3], kp[7], kp[6])); //[
        tmp.push_back(init_tri(kp[4], kp[5], kp[8])); //   ]
        tmp.push_back(init_tri(kp[4], kp[8], kp[7])); //  [

        //----------------------------
        tmp[0]->neighbour[0] = tmp[3];
        tmp[0]->neighbour[1] = tmp[1];
        tmp[0]->neighbour[2] = tmp[5];
        //----------------------------
        tmp[1]->neighbour[0] = tmp[4];
        tmp[1]->neighbour[1] = tmp[2];
        tmp[1]->neighbour[2] = tmp[0];
        //----------------------------
        tmp[2]->neighbour[0] = tmp[1];
        tmp[2]->neighbour[1] = tmp[3];
        tmp[2]->neighbour[2] = tmp[7];
        //----------------------------
        tmp[3]->neighbour[0] = tmp[6];
        tmp[3]->neighbour[1] = tmp[0];
        tmp[3]->neighbour[2] = tmp[2];
        //----------------------------
        tmp[4]->neighbour[0] = tmp[7];
        tmp[4]->neighbour[1] = tmp[5];
        tmp[4]->neighbour[2] = tmp[1];
        //----------------------------
        tmp[5]->neighbour[0] = tmp[0];
        tmp[5]->neighbour[1] = tmp[6];
        tmp[5]->neighbour[2] = tmp[4];
        //----------------------------
        tmp[6]->neighbour[0] = tmp[5];
        tmp[6]->neighbour[1] = tmp[7];
        tmp[6]->neighbour[2] = tmp[3];
        //----------------------------
        tmp[7]->neighbour[0] = tmp[2];
        tmp[7]->neighbour[1] = tmp[4];
        tmp[7]->neighbour[2] = tmp[6];
        //----------------------------
        for (int i = 0; i < 8; i++)
        {
            tmp[i]->area = get_weight(tmp[i], wm);
        }
    }

    static vector2f insert_point(qtree* q, std::vector<vector2f*>& points, tri* t, const vector2f& p, const weight_mapper& wm)
    {
        vector2f pp = p;
        float offx = floor(pp[0]);
        float offy = floor(pp[1]);

        pp[0] -= offx;
        pp[1] -= offy;

        vector2f* np = new vector2f(pp);
        points.push_back(np);

        remove_qtree(q, t);

        std::unique_ptr<tri> tt(t);

        tri* t0 = clone_tri(t);
        tri* t1 = clone_tri(t);
        tri* t2 = clone_tri(t);

        t0->points[0] = point(np, vector2f(offx, offy));
        t1->points[1] = point(np, vector2f(offx, offy));
        t2->points[2] = point(np, vector2f(offx, offy));

        t0->neighbour[1] = t1;
        t0->neighbour[2] = t2;

        t1->neighbour[2] = t2;
        t1->neighbour[0] = t0;

        t2->neighbour[0] = t0;
        t2->neighbour[1] = t1;

        replace_tri(t->neighbour[0], t, t0);
        replace_tri(t->neighbour[1], t, t1);
        replace_tri(t->neighbour[2], t, t2);

        t0->area = get_weight(t0, wm);
        t1->area = get_weight(t1, wm);
        t2->area = get_weight(t2, wm);

        insert_qtree(q, t0);
        insert_qtree(q, t1);
        insert_qtree(q, t2);

        legalize_tri(q, t0, 0, wm);
        legalize_tri(q, t1, 1, wm);
        legalize_tri(q, t2, 2, wm);

        return pp; //return toroidal point
    }

    toroidal_qtree_delaunay_sample_grid::toroidal_qtree_delaunay_sample_grid()
        : wm_(new area_mapper())
    {
        std::vector<tri*> tmp;
        create_initial_tri(points_, tmp, *(wm_.get()));

        tree_ = branch_qtree();
        for (size_t i = 0; i < tmp.size(); i++)
        {
            insert_qtree(tree_, tmp[i]);
        }
    }

    toroidal_qtree_delaunay_sample_grid::toroidal_qtree_delaunay_sample_grid(const auto_count_ptr<weight_mapper>& wm)
        : wm_(wm)
    {
        std::vector<tri*> tmp;
        create_initial_tri(points_, tmp, *(wm_.get()));

        tree_ = branch_qtree();
        for (size_t i = 0; i < tmp.size(); i++)
        {
            insert_qtree(tree_, tmp[i]);
        }
    }

    toroidal_qtree_delaunay_sample_grid::~toroidal_qtree_delaunay_sample_grid()
    {
        destroy_qtree(tree_);
        size_t sz = points_.size();
        for (size_t i = 0; i < sz; i++)
        {
            delete points_[i];
        }
    }

    vector2f toroidal_qtree_delaunay_sample_grid::get()
    {
        tri* t = tree_->pMax; //get max area triangle
        assert(t);
        vector2f p = center_tri(t); //get center of triangle
        return insert_point(tree_, points_, t, p, *(wm_.get()));
    }

    void toroidal_qtree_delaunay_sample_grid::reserve(size_t n)
    {
        points_.reserve(n * 3);
    }

    size_t toroidal_qtree_delaunay_sample_grid::get_bytes() const
    {
        size_t sz = 0;
        sz += points_.capacity() * sizeof(void*);
        sz += points_.size() * sizeof(vector2f);
        sz += get_bytes_qtree(tree_);
        sz += sizeof(toroidal_qtree_delaunay_sample_grid);
        return sz;
    }
}
