#include "polar_delaunay_sample_grid.h"

//   References:
//     http://kanamori.cs.tsukuba.ac.jp/projects/delaunay_blue_noise/
//     Yoshihiro Kanamori, Zoltan Szego, Tomoyuki Nishita: "Deterministic Blue Noise Sampling by Solving Largest Empty Circle Problems," Journal of IIEEJ, Vol.40, No.1, 2011-1.

#include <float.h>
#include <assert.h>
#include <algorithm>
#include <memory>
#include <cmath>

#include "values.h"

namespace kaze
{
    namespace
    {
        struct point
        {
            point() {}
            explicit point(const vector2f* p_, const vector2f& o_ = vector2f(0, 0)) : p(p_), o(o_) {}
            point& operator=(const vector2f* p_)
            {
                p = p_;
                o = vector2f(0, 0);
                return *this;
            }
            vector2f evaluate() const { return *p + o; }

            const vector2f* p;
            vector2f o;
        };
        struct tri
        {
            float area; //
            point points[3];
            unsigned int id;
            struct tri* neighbour[3];
        };
    }

    struct polar_delaunay_sample_grid::heap
    {
        size_t size() const { return vec_.size(); }
        unsigned int get_id() const { return (unsigned int)pos_.size(); }

        size_t get_bytes() const
        {
            size_t sz = 0;
            sz += vec_.capacity() * sizeof(tri*);
            sz += vec_.size() * sizeof(tri);
            sz += pos_.capacity() * sizeof(int);
            sz += sizeof(heap);
            return sz;
        }

        heap()
        {
        }
        ~heap()
        {
            size_t sz = vec_.size();
            for (size_t i = 0; i < sz; i++)
            {
                if (vec_[i]) delete vec_[i];
            }
        }
        void reserve(size_t sz)
        {
            pos_.reserve(sz);
        }

        void clear()
        {
            vec_.clear();
            pos_.clear();
        }
        void insert(tri* val)
        {
            unsigned int id = val->id;
            vec_.push_back(val);
            int p = (int)vec_.size() - 1;
            if ((int)pos_.size() <= id) pos_.resize(id + 1);
            pos_[id] = p;
            heapup(p);
        }
        tri* top()
        {
            return vec_[0];
        }

        void pop()
        {
            del(0);
        }

        void remove(tri* val)
        {
            unsigned int id = val->id;
            int p = pos_[id];
            // check if p is the last element?
            if (p == vec_.size() - 1)
                vec_.pop_back();
            else
                del(p);
            //unused_.push_back(id);//
        }

        void del(int p)
        {
            swapvalues(p, (int)vec_.size() - 1);
            //HEAP_DEBUG("after del.swap");
            vec_.pop_back();
            // check the new node
            if (p > 0)
            {
                int parent = (p - 1) / 2;
                if (vec_[parent]->area < vec_[p]->area) heapup(p);
            }
            heapdown(p);
        }

        void heapup(int p)
        {
            while (p > 0)
            {
                int parent = (p - 1) / 2;
                if (vec_[p]->area < vec_[parent]->area) break;
                swapvalues(parent, p);
                p = parent;
            }
        }

        void heapdown(int p)
        {
            int next1, next2, swap;
            int s = (int)vec_.size();
            while (p < s)
            {
                next1 = p * 2 + 1;
                next2 = next1 + 1;

                if (next2 < s)
                {
                    if (vec_[next1]->area < vec_[p]->area && vec_[next2]->area < vec_[p]->area)
                        break; // all is well
                    else
                    {
                        swap = (vec_[next2]->area < vec_[next1]->area) ? next1 : next2;
                    }
                }
                else if (next1 < s)
                {
                    if (vec_[p]->area < vec_[next1]->area)
                        swap = next1;
                    else
                        break;
                }
                else
                    break;

                swapvalues(p, swap);
                p = swap;
            }
        }

        void swapvalues(int pos1, int pos2)
        {
            if (pos1 == pos2) return;
            unsigned int id1 = vec_[pos1]->id;
            unsigned int id2 = vec_[pos2]->id;
            std::swap(vec_[pos1], vec_[pos2]);
            std::swap(pos_[id1], pos_[id2]);
        }
        //--------------
        std::vector<tri*> vec_;
        std::vector<int> pos_;
    };

    typedef polar_delaunay_sample_grid::heap heap;
    typedef polar_delaunay_sample_grid::weight_mapper weight_mapper;

    static const int TRAP[] = {0, 1, 2, 0, 1, 2};

    static inline float tri_area(const vector2f& a, const vector2f& b, const vector2f& c)
    {
        return (a[0] - c[0]) * (b[1] - c[1]) - (a[1] - c[1]) * (b[0] - c[0]); //
    }

    static inline float circum_radius(const vector2f& a, const vector2f& b, const vector2f& c)
    {
        float A = length(b - c);
        float B = length(c - a);
        float C = length(a - b);

        return (A * B * C) / sqrt((A + B + C) * (-A + B + C) * (+A - B + C) * (+A + B - C));
    }

    static inline vector2f circum_center(const vector2f& a, const vector2f& b, const vector2f& c)
    {
        //float A = length(b-c);
        //float B = length(c-a);
        //float C = length(a-b);
        //float A2=A*A;
        //float B2=B*B;
        //float C2=C*C;
        float A2 = sqr_length(b - c);
        float B2 = sqr_length(c - a);
        float C2 = sqr_length(a - b);

        float U = A2 * (-A2 + B2 + C2);
        float V = B2 * (+A2 - B2 + C2);
        float W = C2 * (+A2 + B2 - C2);

        return (U * a + V * b + W * c) / (U + V + W);
    }

    static bool is_incircle(const vector2f& pa, const vector2f& pb, const vector2f& pc, const vector2f& pd)
    {
        float adx = pa[0] - pd[0];
        float ady = pa[1] - pd[1];
        float bdx = pb[0] - pd[0];
        float bdy = pb[1] - pd[1];

        float adxbdy = adx * bdy;
        float bdxady = bdx * ady;
        float oabd = adxbdy - bdxady;

        if (oabd <= 0)
        {
            return false;
        }

        float cdx = pc[0] - pd[0];
        float cdy = pc[1] - pd[1];

        float cdxady = cdx * ady;
        float adxcdy = adx * cdy;
        float ocad = cdxady - adxcdy;

        if (ocad <= 0)
        {
            return false;
        }

        float bdxcdy = bdx * cdy;
        float cdxbdy = cdx * bdy;

        float alift = adx * adx + ady * ady;
        float blift = bdx * bdx + bdy * bdy;
        float clift = cdx * cdx + cdy * cdy;

        float det = alift * (bdxcdy - cdxbdy) + blift * ocad + clift * oabd;

        return det > 0;
    }

    namespace
    {
        struct tri_sorter
        {
            bool operator()(tri* a, tri* b) const
            {
                return a->area > b->area;
            }
        };

        struct area_mapper : public polar_delaunay_sample_grid::weight_mapper
        {
            float get(const vector2f& a, const vector2f& b, const vector2f& c) const
            {
                static const float fPI = (float)values::pi();
                //0..2,0..1
                float theta = fPI * (a[1] + b[1] + c[1]) * (1.0f / 3.0f);

                return tri_area(a, b, c) * sin(theta);
            }
        };

        struct circum_mapper : public polar_delaunay_sample_grid::weight_mapper
        {
            float get(const vector2f& a, const vector2f& b, const vector2f& c) const
            {
                static const float fPI = (float)values::pi();

                vector2f cc = circum_center(a, b, c);
                //0..2,0..1
                float theta = fPI * cc[1];

                return circum_radius(a, b, c) * sin(theta);
            }
        };
    }

    //----------------

    static tri* clone_tri(tri* t)
    {
        tri* n = new tri();
        memcpy(n, t, sizeof(tri));
        return n;
    }

    static float area_tri(tri* t)
    {
        assert(t);
        assert(t->points[0].p);
        assert(t->points[1].p);
        assert(t->points[2].p);

        vector2f a = t->points[0].evaluate();
        vector2f b = t->points[1].evaluate();
        vector2f c = t->points[2].evaluate();

        float fRet = tri_area(a, b, c);
        assert(fRet > 0);
        return fRet;
    }

    static vector2f center_tri(tri* t)
    {
        assert(t);
        assert(t->points[0].p);
        assert(t->points[1].p);
        assert(t->points[2].p);

        vector2f a = t->points[0].evaluate();
        vector2f b = t->points[1].evaluate();
        vector2f c = t->points[2].evaluate();

        return (a + b + c) * (1.0f / 3.0f);
    }

    static void replace_tri(tri* a, tri* b, tri* c) //b->c
    {
        if (a)
        {
            for (int i = 0; i < 3; i++)
            {
                if (a->neighbour[i] == b)
                {
                    a->neighbour[i] = c;
                    break;
                }
            }
        }
    }

    static bool is_intri(tri* t, const vector2f& p)
    {
        vector2f a = t->points[0].evaluate();
        vector2f b = t->points[1].evaluate();
        vector2f c = t->points[2].evaluate();

        float area0 = tri_area(a, b, p);
        float area1 = tri_area(b, c, p);
        float area2 = tri_area(c, a, p);

        if (area0 <= 0 && area1 <= 0 && area2 <= 0) return true;
        if (area0 >= 0 && area1 >= 0 && area2 >= 0) return true;

        return false;
    }

    static bool test_legalize(const vector2f& a, const vector2f& b, const vector2f& c, const vector2f& d)
    {
        if (tri_area(c, a, d) <= 0)
        {
            return false;
        }
        if (tri_area(a, b, d) <= 0)
        {
            return false;
        }

        if (is_incircle(a, b, c, d))
        {
            return true;
        }

        return false;
    }

    static float get_weight(tri* t, const weight_mapper& wm)
    {
        vector2f a = t->points[0].evaluate();
        vector2f b = t->points[1].evaluate();
        vector2f c = t->points[2].evaluate();
        return wm.get(a, b, c);
    }

    static void legalize_tri(heap* h, tri* t, int i, const weight_mapper& wm)
    {
        assert(t);
        tri* s = t->neighbour[i];
        if (!s) return;

        const vector2f* a = t->points[TRAP[i]].p;
        const vector2f* b = t->points[TRAP[i + 1]].p;
        const vector2f* c = t->points[TRAP[i + 2]].p;

        int j = 0;
        if (s->points[2].p == b)
        {
            j = 0;
        }
        else if (s->points[0].p == b)
        {
            j = 1;
        }
        else
        {
            j = 2;
        }

        const vector2f* d = s->points[j].p;

        vector2f pa = t->points[TRAP[i]].evaluate();
        vector2f pb = t->points[TRAP[i + 1]].evaluate();
        vector2f pc = t->points[TRAP[i + 2]].evaluate();

        vector2f pb2 = s->points[TRAP[j + 2]].evaluate();
        vector2f offset = pb - pb2;
        vector2f pd = s->points[j].evaluate() + offset;

        if (test_legalize(pa, pb, pc, pd))
        {
            h->remove(t);
            h->remove(s);

            tri* t2 = t->neighbour[TRAP[i + 2]];
            tri* s2 = s->neighbour[TRAP[j + 2]];

            //FLIP
            t->points[TRAP[i + 1]] = point(d, s->points[j].o + offset);
            s->points[TRAP[j + 1]] = point(a, t->points[i].o - offset);

            t->neighbour[TRAP[i + 2]] = s;
            s->neighbour[TRAP[j + 2]] = t;

            t->neighbour[TRAP[i + 0]] = s2;
            s->neighbour[TRAP[j + 0]] = t2;

            replace_tri(t2, t, s); //
            replace_tri(s2, s, t); //

            //area
            t->area = get_weight(t, wm);
            s->area = get_weight(s, wm);

            h->insert(t);
            h->insert(s);

            //call
            legalize_tri(h, t, TRAP[i + 0], wm);
            legalize_tri(h, s, TRAP[j + 1], wm);
        }
    }

    //-----------------------------------------------------------------------------
    tri* init_tri(const point& a, const point& b, const point& c)
    {
        tri* t = new tri();
        memset(t, 0, sizeof(tri));
        t->points[0] = a;
        t->points[1] = b;
        t->points[2] = c;
        return t;
    }

    static void create_initial_tri(std::vector<vector2f*>& points, std::vector<tri*>& tmp, const weight_mapper& wm)
    {
        points.push_back(new vector2f(0.0f, 0.0f));
        points.push_back(new vector2f(1.0f, 0.0f)); //1
        points.push_back(new vector2f(0.0f, 1.0f));
        points.push_back(new vector2f(1.0f, 1.0f)); //3

        point kp[6];
        kp[0] = point(points[0], vector2f(0, 0));
        kp[1] = point(points[1], vector2f(0, 0));
        kp[2] = point(points[0], vector2f(2, 0)); //
        kp[3] = point(points[2], vector2f(0, 0));
        kp[4] = point(points[3], vector2f(0, 0));
        kp[5] = point(points[2], vector2f(2, 0)); //

        tmp.push_back(init_tri(kp[0], kp[1], kp[4])); // ]
        tmp.push_back(init_tri(kp[0], kp[4], kp[3])); //[
        tmp.push_back(init_tri(kp[1], kp[2], kp[5])); //   ]
        tmp.push_back(init_tri(kp[1], kp[5], kp[4])); //  [

        //----------------------------
        tmp[0]->neighbour[0] = tmp[3];
        tmp[0]->neighbour[1] = tmp[1];
        tmp[0]->neighbour[2] = NULL;
        //----------------------------
        tmp[1]->neighbour[0] = NULL;
        tmp[1]->neighbour[1] = NULL;
        tmp[1]->neighbour[2] = tmp[0];
        //----------------------------
        tmp[2]->neighbour[0] = NULL;
        tmp[2]->neighbour[1] = tmp[3];
        tmp[2]->neighbour[2] = NULL;
        //----------------------------
        tmp[3]->neighbour[0] = NULL;
        tmp[3]->neighbour[1] = tmp[0];
        tmp[3]->neighbour[2] = tmp[2];
        //----------------------------
        for (int i = 0; i < 4; i++)
        {
            tmp[i]->area = get_weight(tmp[i], wm);
        }
    }

    static vector2f insert_point(heap* h, std::vector<vector2f*>& points, tri* t, const vector2f& p, const weight_mapper& wm)
    {
        assert(t);
        h->remove(t);

        vector2f pp = p;
        float offx = 2.0f * floor(pp[0] * 0.5f); //0..2
        float offy = 0;                          //floor(pp[1]);

        pp[0] -= offx;
        //pp[1] -= offy;

        vector2f* np = new vector2f(pp);
        points.push_back(np);

        std::unique_ptr<tri> tt(t);

        tri* t0 = clone_tri(t);
        tri* t1 = clone_tri(t);
        tri* t2 = clone_tri(t);

        t0->points[0] = point(np, vector2f(offx, offy));
        t1->points[1] = point(np, vector2f(offx, offy));
        t2->points[2] = point(np, vector2f(offx, offy));

        t0->neighbour[1] = t1;
        t0->neighbour[2] = t2;

        t1->neighbour[2] = t2;
        t1->neighbour[0] = t0;

        t2->neighbour[0] = t0;
        t2->neighbour[1] = t1;

        replace_tri(t->neighbour[0], t, t0);
        replace_tri(t->neighbour[1], t, t1);
        replace_tri(t->neighbour[2], t, t2);

        t0->area = get_weight(t0, wm);
        t1->area = get_weight(t1, wm);
        t2->area = get_weight(t2, wm);

        unsigned int newid = h->get_id();
        t0->id = newid + 0;
        t1->id = newid + 1;
        t2->id = newid + 2;

        h->insert(t0);
        h->insert(t1);
        h->insert(t2);

        legalize_tri(h, t0, 0, wm);
        legalize_tri(h, t1, 1, wm);
        legalize_tri(h, t2, 2, wm);

        //return pp;//return toroidal point
        return vector2f(0.5f * pp[0], pp[1]); //[0..1],[0..1]
    }

    polar_delaunay_sample_grid::polar_delaunay_sample_grid()
        : wm_(new area_mapper())
    {
        std::vector<tri*> tmp;
        create_initial_tri(points_, tmp, *(wm_.get()));
        heap_ = new heap();
        heap_->reserve(8 * 8);
        for (size_t i = 0; i < tmp.size(); i++)
        {
            tmp[i]->id = (unsigned int)i;
            heap_->insert(tmp[i]);
        }
    }

    polar_delaunay_sample_grid::polar_delaunay_sample_grid(const auto_count_ptr<weight_mapper>& wm)
        : wm_(wm)
    {
        std::vector<tri*> tmp;
        create_initial_tri(points_, tmp, *(wm_.get()));
        heap_ = new heap();
        heap_->reserve(8 * 8);
        for (size_t i = 0; i < tmp.size(); i++)
        {
            tmp[i]->id = (unsigned int)i;
            heap_->insert(tmp[i]);
        }
    }

    polar_delaunay_sample_grid::~polar_delaunay_sample_grid()
    {
        delete heap_;
        size_t sz = points_.size();
        for (size_t i = 0; i < sz; i++)
        {
            delete points_[i];
        }
    }

    vector2f polar_delaunay_sample_grid::get()
    {
        tri* t = heap_->top(); //get max area triangle
        assert(t);
        vector2f p = center_tri(t); //get center of triangle
        return insert_point(heap_, points_, t, p, *(wm_.get()));
    }

    void polar_delaunay_sample_grid::reserve(size_t n)
    {
        points_.reserve(n * 3);
        heap_->reserve(n);
    }

    size_t polar_delaunay_sample_grid::get_bytes() const
    {
        size_t sz = 0;
        sz += points_.capacity() * sizeof(void*);
        sz += points_.size() * sizeof(vector2f);
        sz += heap_->get_bytes();
        sz += sizeof(polar_delaunay_sample_grid);
        return sz;
    }
}
