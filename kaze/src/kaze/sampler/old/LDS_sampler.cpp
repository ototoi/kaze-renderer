#include "LDS_sampler.h"

#include "QMC.h"
#include "random.h"
#include "sampling.h"
#include "values.h"

#include "logger.h"

#include <vector>

#ifdef _WIN32
#include <windows.h>
#endif

namespace kaze
{

    halton_sampler::halton_sampler()
    {
        base0_ = xor128();
        base1_ = xor128();
        i_ = 0;
    }
    vector2 halton_sampler::get2() const
    {
#ifdef _WIN32
        return get2(::InterlockedExchangeAdd((LPLONG)&i_, 1));
#else
        return get2(i_++);
#endif
    }

    vector2 halton_sampler::get2(int i) const
    {
        unsigned int base0 = base0_;
        unsigned int base1 = base1_;
        double x = ri_vdC(i, base0);
        double y = ri_S(i, base1);
        return vector2(x, y);
    }

    void halton_sampler::get(aggregator<real>* agr, int n) const
    {
        unsigned int base0 = base0_;
        for (int i = 0; i < n; i++)
        {
            agr->add(ri_vdC(i, base0));
        }
    }

    void halton_sampler::get(aggregator<vector2>* agr, int n) const
    {

        for (int i = 0; i < n; i++)
        {
            agr->add(get2());
        }
    }

    halton_sampler& halton_sampler::get_instace()
    {
        static halton_sampler smp_;
        return smp_;
    }

    void hammersley_sampler::get(aggregator<vector2>* agr, int n) const
    {
        this->get_norm(agr, n);
    }

    void hammersley_sampler::get_norm(aggregator<vector2>* agr, int n) const
    {
        std::vector<double> v(n * 2);

        unsigned int base = xor128();
        hammersley2_r(&v[0], n, base);

        real shift = (real)(0.5 / n);

        for (int i = 0; i < n; i++)
        {
            real x = real((v[2 * i] + shift));
            real y = real((v[2 * i + 1] + shift));

            agr->add(vector2(x, y));
        }
    }

    void hammersley_sampler::get_perm(aggregator<vector2>* agr, int n) const
    {
        std::vector<double> v(n * 2);

        //unsigned int base = xor128();

        hammersley2_s(&v[0], n, 11); //2,3,4,5->5,   7,  11,  13
        //hammersley2(&v[0],n);

        real shift = (real)(1.0 / (2 * n));

        for (int i = 0; i < n * 2; i += 2)
        {
            real x = real(v[i] + shift);
            real y = real(v[i + 1] + shift);

            agr->add(vector2(x, y));
        }
    }
}