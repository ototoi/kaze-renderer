/*
 implementation form 

 A Spatial Data Structure for Fast Poisson-Disk Sample Generation
	http://www.cs.virginia.edu/~gfx/pubs/antimony/

 */

#include "poissondisk_sampler.h"
#include "random.h"

#include "values.h"

#include "agree_range_list.h"

#include <math.h>
#include <string.h>

#define kMaxPointsPerCell 4 * 2

namespace kaze
{

    typedef agree_range_list range_list;
    typedef agree_range_entry range_entry;

    static rand_t rand_int() { return xor128(); }

    static double rand01()
    {
        double x = ((double)xor128()) / std::numeric_limits<rand_t>::max();
        return x;
    }

    /*
	static int rand_int(){return rand();}

	static double rand01(){
		return ((double)rand())/RAND_MAX;
	}
*/
    static double rand11()
    {
        return 2 * rand01() - 1;
    }

    static int get_grid(double x, int nGrid)
    {
        double xx = (x + 1) * 0.5;
        return (int)floor(xx * nGrid);
    }

    //0/4->2, 0/6->3
    static int GridFromRadius(double rad)
    {
        double diam = 2 * rad;
        int nRet = (int)ceil(1.1 / diam);
        if (nRet < 2)
        {
            nRet = 2;
        }
        else
        {
            int i = 2;
            while (i < nRet)
            {
                i <<= 1;
            }
            nRet = i;
        }

        return nRet;
    }

    static real dist2(const vector2& a, const vector2& b)
    {
        vector2 c = b - a;
        return c[0] * c[0] + c[1] * c[1];
    }

    static real dist2(real a, real b)
    {
        return a * a + b * b;
    }

    static vector2 rand_point()
    {
        real x = rand11();
        real y = rand11();
        return vector2(x, y);
    }

    static bool is_area(const vector2& v)
    {
        real x = v[0];
        real y = v[1];
        return ((-1 <= x && x <= 1) && (-1 <= y && y <= 1));
    }

    static vector2 repeat_place(const vector2& v)
    {
#if 0		
		real x = (v[0]+1)*0.5;//0...1
		real y = (v[1]+1)*0.5;

		real ix = floor(x);
		real iy = floor(y);

		x = x - ix;//0...1
		y = y - iy;//

		x = 2*x-1;//-1...1
		y = 2*y-1;
#else
        real x = v[0];
        real y = v[1];
        if (x < -1)
            x += 2;
        else if (x > 1)
            x -= 2;

        if (y < -1)
            y += 2;
        else if (y > 1)
            y -= 2;
#endif
        return vector2(x, y);
    }
    namespace
    {
        class vector_aggregator : public aggregator<vector2>
        {
        public:
            vector_aggregator(std::vector<vector2>* v) : v_(v) {}
            void add(const vector2& rhs)
            {
                v_->push_back(rhs);
            }
            size_t size() const { return v_->size(); }
            const vector2& operator[](size_t i) const { return (*v_)[i]; }
        private:
            std::vector<vector2>* v_;
        };
    }

    namespace
    {

        //-----------------------------------------------

        class grid_cell
        {
        public:
            grid_cell() 
                : sz(0) 
            {
                memset(ar_, 0, sizeof(int)*kMaxPointsPerCell);
            }
            int size() const { return sz; }
            void push_back(int i)
            {
#if _DEBUG
                if (sz >= kMaxPointsPerCell)
                {
                    return;
                }
#endif

                ar_[sz] = i;
                sz++;
                //
                assert(sz <= kMaxPointsPerCell);
                //
            }
            int& operator[](size_t i) { return ar_[i]; }
            int operator[](size_t i) const { return ar_[i]; }
            void clear() { sz = 0; }
        private:
            int ar_[kMaxPointsPerCell];
            int sz;
        };

        typedef grid_cell cell_type;

        class index_grid
        {
        public:
            index_grid(int nGridNum)
            {
                int nGridTotalNum = nGridNum * nGridNum;
                m_aGridArray = new grid_cell[nGridTotalNum];
                m_nGridNum = nGridNum;
                m_dCellWidth = 2.0 / nGridNum;
            }
            ~index_grid()
            {
                delete[] m_aGridArray;
            }

            cell_type& get_grid(int x, int y) { return m_aGridArray[x * m_nGridNum + y]; }
            const cell_type& get_grid(int x, int y) const { return m_aGridArray[x * m_nGridNum + y]; }
            void add(int x, int y, int idx) { get_grid(x, y).push_back(idx); }
            int get_grid_size() { return m_nGridNum; }
            double get_cell_size() const { return m_dCellWidth; }
        private:
            int m_nGridNum;
            double m_dCellWidth;
            grid_cell* m_aGridArray;
        };

        class sample_data
        {
        public:
            sample_data(int nGrid)
            {
                ig_ = new index_grid(nGrid);
            }
            void add(const vector2& v)
            {
                int idx = (int)points_.size();
                int nGrid = ig_->get_grid_size();
                int ix = get_grid(v[0], nGrid);
                int iy = get_grid(v[1], nGrid);
                ig_->add(ix, iy, idx); //grid
                points_.push_back(v);  //point
            }
            const vector2& get(size_t idx) { return points_[idx]; }
            size_t size() const { return points_.size(); }

            void find(int index, range_list& rl, double distance)
            {
                double distanceSqrd = distance * distance;
                double idistance = 1 / distance;

                int nGrid = ig_->get_grid_size();
                int nGrid2 = nGrid >> 1;
                double gridCellSize = ig_->get_cell_size();

                int N = std::min((int)ceil(nGrid2 * distance), nGrid2);

                vector2& v = points_[index];

                double x = v[0];
                double y = v[1];

                int gx = get_grid(x, nGrid);
                int gy = get_grid(y, nGrid);

                int xSide = (x - (-1 + gx * gridCellSize)) > gridCellSize * .5;
                int ySide = (y - (-1 + gy * gridCellSize)) > gridCellSize * .5;
                int iy = 1;
                for (int j = -N; j <= N; j++)
                {
                    switch (j)
                    {
                    case 0:
                        iy = ySide;
                        break;
                    case 1:
                        iy = 0;
                        break;
                    default:;
                    }

                    int ix = 1;
                    for (int i = -N; i <= N; i++)
                    {
                        switch (i)
                        {
                        case 0:
                            ix = xSide;
                            break;
                        case 1:
                            ix = 0;
                            break;
                        default:;
                        }

                        // offset to closest cell point
                        double dx = x - (-1 + (gx + i + ix) * gridCellSize);
                        double dy = y - (-1 + (gy + j + iy) * gridCellSize);

                        if (dist2(dx, dy) < distanceSqrd)
                        {
                            int cx = (gx + i + nGrid) & (nGrid - 1);
                            int cy = (gy + j + nGrid) & (nGrid - 1);
                            //int cx = (gx+i+nGrid)%(nGrid);
                            //int cy = (gy+j+nGrid)%(nGrid);

                            const cell_type& cell = ig_->get_grid(cx, cy);

                            size_t sz = cell.size();
                            for (size_t k = 0; k < sz; k++)
                            {
                                int idx = cell[k];
                                if (idx != index)
                                {
                                    vector2 t = repeat_place(points_[idx] - v);
                                    double distSqrd = t.sqr_length();
                                    if (distSqrd < distanceSqrd)
                                    {
                                        double dist = sqrt(distSqrd);
                                        double angle = atan2(t[1], t[0]);
                                        double theta = acos(dist * idistance);

                                        rl.sub(angle - theta, angle + theta);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            ~sample_data()
            {
                delete ig_;
            }

        private:
            index_grid* ig_;
            std::vector<vector2> points_;
        };
    }
    //--------------------------------------------------------

    class poissondisk_sampler_imp
    {
    public:
        poissondisk_sampler_imp();
        ~poissondisk_sampler_imp();
        void get(aggregator<vector2>* agr, int n) const;

    protected:
        void get(sample_data& samples, double radius) const;
    };

    poissondisk_sampler_imp::poissondisk_sampler_imp() {}

    poissondisk_sampler_imp::~poissondisk_sampler_imp() {}

    void poissondisk_sampler_imp::get(sample_data& samples, double radius) const
    {
        static const double LRANGE = (double)values::pi() / 3.0;
        static const double PI2 = (double)values::pi() * 2;

        double r2 = 2 * radius;
        double r4 = 2 * r2;

        range_list ranges(0, 0);
        std::vector<int> olds;
        olds.reserve(10);

        samples.add(rand_point());
        olds.push_back(0);

        int num = 0;
        while (!olds.empty())
        {
            int c = rand_int() % (int)olds.size();
            int index = olds[c];
            vector2 cur = samples.get(index);
            //
            olds[c] = olds[olds.size() - 1];
            olds.pop_back();
            //

            ranges.reset(0, PI2);
            samples.find(index, ranges, r4);
            int isz;
            while (!ranges.empty())
            {
                isz = ranges.size();

                const agree_range_entry& re = ranges[rand_int() % isz];
                double angle = re.min + (re.max - re.min) * rand01();

                real xx = cos(angle) * r2;
                real yy = sin(angle) * r2;

                samples.add(repeat_place(cur + vector2(xx, yy)));
                olds.push_back((int)samples.size() - 1);
                ranges.sub(angle - LRANGE, angle + LRANGE);
            }
            //if(num++ >100)break;
        }
    }

    void poissondisk_sampler_imp::get(aggregator<vector2>* agr, int n) const
    {
        real dn = sqrt((double)n);
        real radius = (real)1.0 / dn;
        sample_data samples(GridFromRadius(radius));
        //------------------------------------------------------------
        this->get(samples, radius);
        //------------------------------------------------------------
        size_t sz = samples.size();
        for (size_t i = 0; i < sz; i++)
        {
            agr->add(samples.get(i));
        }
    }

    //--------------------------------------------------------

    poissondisk_sampler::poissondisk_sampler()
    {
        imp_ = new poissondisk_sampler_imp();
    }

    poissondisk_sampler::~poissondisk_sampler()
    {
        delete imp_;
    }

    void poissondisk_sampler::get(aggregator<vector2>* agr, int n) const
    {
        imp_->get(agr, n);
    }
}
