#ifndef KAZE_SAMPLING_H
#define KAZE_SAMPLING_H

#include "types.h"

namespace kaze
{

    void NLOOK_shuffle(vector2* v, size_t n, int div);
}

#endif