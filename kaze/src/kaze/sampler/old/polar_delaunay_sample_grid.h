#ifndef POLAR_DELAUNAY_SAMPLE_GRID_H
#define POLAR_DELAUNAY_SAMPLE_GRID_H

#include "types.h"
#include "count_ptr.hpp"
#include <vector>

namespace kaze
{

    class polar_delaunay_sample_grid
    {
    public:
        struct weight_mapper
        {
            virtual ~weight_mapper() {}
            virtual float get(const vector2f& a, const vector2f& b, const vector2f& c) const = 0;
        };
        struct heap;

    public:
        //
        polar_delaunay_sample_grid();
        polar_delaunay_sample_grid(const auto_count_ptr<weight_mapper>& wm);
        ~polar_delaunay_sample_grid();
        vector2f get();

    public:
        void reserve(size_t n);
        size_t get_bytes() const;

    protected:
        std::vector<vector2f*> points_;
        heap* heap_;
        std::shared_ptr<weight_mapper> wm_;
    };
}

#endif