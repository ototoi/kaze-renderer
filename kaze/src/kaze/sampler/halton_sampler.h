#ifndef KAZE_HALTON_SAMPLER_HPP
#define KAZE_HALTON_SAMPLER_HPP

#include "types.h"
#include "sampler.hpp"

namespace kaze
{

    class halton_sampler : public sampler<vector2>
    {
    public:
        halton_sampler();
        halton_sampler(unsigned int base0, unsigned int base1);
        vector2 get() const;

    protected:
        unsigned int base0_;
        unsigned int base1_;
        mutable int i_;
    };
}

#endif