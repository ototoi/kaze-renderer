#ifndef KAZE_TRIMMED_MULTI_BEZIER_PATCH_HPP
#define KAZE_TRIMMED_MULTI_BEZIER_PATCH_HPP

#include "multi_bezier_patch.hpp"
#include "multi_bezier_curve.hpp"

#include <vector>

namespace kaze
{

    template <class T, class U>
    class trimmed_multi_bezier_patch : public multi_bezier_patch<T>
    {
    public:
        typedef std::vector<multi_bezier_curve<U> > loop_type;
        typedef trimmed_multi_bezier_patch<T, U> this_type;
        typedef multi_bezier_patch<T> base_type;

    public:
        trimmed_multi_bezier_patch() {}

        trimmed_multi_bezier_patch(
            const std::vector<real>& knot_u,
            const std::vector<real>& knot_v,
            const std::vector<bezier_patch<T> >& patches) : multi_bezier_patch<T>(knot_u, knot_v, patches) {}

        trimmed_multi_bezier_patch(
            const std::vector<real>& knot_u,
            const std::vector<real>& knot_v,
            const std::vector<bezier_patch<T> >& patches,
            const std::vector<loop_type>& outer_loops,
            const std::vector<loop_type>& inner_loops) : multi_bezier_patch<T>(knot_u, knot_v, patches), outer_loops_(outer_loops), inner_loops_(inner_loops) {}

        trimmed_multi_bezier_patch(
            const multi_bezier_patch<T>& patch,
            const std::vector<loop_type>& outer_loops,
            const std::vector<loop_type>& inner_loops) : multi_bezier_patch<T>(patch), outer_loops_(outer_loops), inner_loops_(inner_loops) {}
    public:
        this_type& operator=(const trimmed_multi_bezier_patch<T, U>& rhs)
        {
            base_type::operator=(rhs);
            outer_loops_ = rhs.outer_loops_;
            inner_loops_ = rhs.inner_loops_;
            return *this;
        }
        void swap(trimmed_multi_bezier_patch<T, U>& rhs)
        {
            base_type::swap(rhs);
            outer_loops_.swap(rhs.outer_loops_);
            inner_loops_.swap(rhs.inner_loops_);
        }
        bool equal(const this_type& rhs) const
        {
            if (!base_type::equal(rhs)) return false;
            if (outer_loops_ != rhs.outer_loops_) return false;
            if (inner_loops_ != rhs.inner_loops_) return false;
            return true;
        }

    public:
        void set_outer_loops(const std::vector<loop_type>& loops)
        {
            outer_loops_ = loops;
        }
        const std::vector<loop_type>& get_outer_loops() const
        {
            return outer_loops_;
        }

        int get_outer_loop_size() const { return (int)outer_loops_.size(); }
        loop_type& get_outer_loop_at(int i)
        {
            return outer_loops_[i];
        }

        const loop_type& get_outer_loop_at(int i) const
        {
            return outer_loops_[i];
        }

        void set_inner_loops(const std::vector<loop_type>& loops)
        {
            inner_loops_ = loops;
        }
        const std::vector<loop_type>& get_inner_loops() const
        {
            return inner_loops_;
        }

        int get_inner_loop_size() const { return (int)inner_loops_.size(); }
        loop_type& get_inner_loop_at(int i)
        {
            return inner_loops_[i];
        }

        const loop_type& get_inner_loop_at(int i) const
        {
            return inner_loops_[i];
        }

    protected:
        std::vector<loop_type> outer_loops_;
        std::vector<loop_type> inner_loops_;
    };

    template <class T, class U>
    inline bool operator==(const trimmed_multi_bezier_patch<T, U>& lhs, const trimmed_multi_bezier_patch<T, U>& rhs)
    {
        return lhs.equal(rhs);
    }

    template <class T, class U>
    inline bool operator!=(const trimmed_multi_bezier_patch<T, U>& lhs, const trimmed_multi_bezier_patch<T, U>& rhs)
    {
        return !lhs.equal(rhs);
    }
}

#endif