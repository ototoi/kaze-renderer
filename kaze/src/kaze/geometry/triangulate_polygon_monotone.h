#ifndef KAZE_TRIANGULATE_POLYGON_EARCLIP_H
#define KAZE_TRIANGULATE_POLYGON_EARCLIP_H

#include "../core/values.h"
#include <vector>
#include <utility>

namespace kaze
{

    bool split_monotone_polygon(std::vector<std::vector<vector2> >& loops, const std::vector<vector2>& loop);
    void get_monotone_diagonal(std::vector<std::pair<int, int> >& D, const std::vector<vector2>& loop);
    void get_monotone_vertex_type(std::vector<int>& type, const std::vector<vector2>& loop);
    bool triangulate_monotone_polygon(std::vector<vector2>& tri, const std::vector<vector2>& loop);
    bool triangulate_polygon_monotone(std::vector<vector2>& tri, const std::vector<vector2>& loop);
}

#endif
