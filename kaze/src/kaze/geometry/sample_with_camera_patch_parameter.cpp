#include "sample_with_camera_patch_parameter.h"

namespace kaze
{

    template <class PATCH>
    static void get_points(vector3 points[4], const PATCH& patch, real u0, real u1, real v0, real v1)
    {
        points[0] = patch.evaluate(u0, v0);
        points[1] = patch.evaluate(u1, v0);
        points[2] = patch.evaluate(u0, v1);
        points[3] = patch.evaluate(u1, v1);
    }

    static void get_minmax(vector3& min, vector3& max, vector3 points[4])
    {
        min = max = points[0];
        for (int i = 1; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (min[j] > points[i][j]) min[j] = points[i][j];
                if (max[j] < points[i][j]) max[j] = points[i][j];
            }
        }
    }

    static vector3 transform_point(const matrix4& mat, const vector3& v)
    {
        vector4 vv = mat * vector4(v[0], v[1], v[2], 1);
        return vector3(vv[0] / vv[3], vv[1] / vv[3], vv[2] / vv[3]);
    }

    static int check_xy(vector3 points[4], int width, int height)
    {
        int nRet = 0;
        vector3 du0 = (points[0] - points[1]);
        vector3 du1 = (points[2] - points[3]);
        if (
            fabs(du0[0]) * width > 0.5 ||
            fabs(du0[1]) * height > 0.5 ||
            fabs(du1[0]) * width > 0.5 ||
            fabs(du1[1]) * height > 0.5)
        {
            nRet |= 1;
        }

        vector3 dv0 = (points[0] - points[2]);
        vector3 dv1 = (points[1] - points[3]);
        if (
            fabs(dv0[0]) * width > 0.5 ||
            fabs(dv0[1]) * height > 0.5 ||
            fabs(dv1[0]) * width > 0.5 ||
            fabs(dv1[1]) * height > 0.5)
        {
            nRet |= 2;
        }

        return nRet;
    }

    template <class PATCH>
    static void sample_patch_parameter_(std::vector<vector2>& out, const PATCH& patch, const matrix4& mat, int width, int height, real u0, real u1, real v0, real v1, int nFlag, int level)
    {
        vector3 points[4];
        get_points(points, patch, u0, u1, v0, v1);
        for (int i = 0; i < 4; i++)
        {
            points[i] = transform_point(mat, points[i]);
        }
        vector3 min, max;
        get_minmax(min, max, points);

        if (max[0] < 0) return;
        if (max[1] < 0) return;
        if (min[0] > 1) return;
        if (min[1] > 1) return;

        int nSub = check_xy(points, width, height);
        if (level <= 0) nSub = 0;

        switch (nSub)
        {
        case 0:
        {
            out.push_back(vector2(u0, v0));                   //LU
            if (nFlag & 0x1) out.push_back(vector2(u1, v0));  //RU
            if (nFlag & 0x2) out.push_back(vector2(u0, v1));  //LB
            if (nFlag == 0x3) out.push_back(vector2(u1, v1)); //RB
        }
        break;
        case 1: //U
        {
            real um = (u0 + u1) * 0.5;
            sample_patch_parameter_(out, patch, mat, width, height, u0, um, v0, v1, nFlag & 0x2, level - 1);
            sample_patch_parameter_(out, patch, mat, width, height, um, u1, v0, v1, nFlag & 0x3, level - 1);
        }
        break;
        case 2: //V
        {
            real vm = (v0 + v1) * 0.5;
            sample_patch_parameter_(out, patch, mat, width, height, u0, u1, v0, vm, nFlag & 0x1, level - 1);
            sample_patch_parameter_(out, patch, mat, width, height, u0, u1, vm, v1, nFlag & 0x3, level - 1);
        }
        break;
        case 3:
        {
            real um = (u0 + u1) * 0.5;
            real vm = (v0 + v1) * 0.5;
            sample_patch_parameter_(out, patch, mat, width, height, u0, um, v0, vm, nFlag & 0x0, level - 1);
            sample_patch_parameter_(out, patch, mat, width, height, um, u1, v0, vm, nFlag & 0x1, level - 1);
            sample_patch_parameter_(out, patch, mat, width, height, u0, um, vm, v1, nFlag & 0x2, level - 1);
            sample_patch_parameter_(out, patch, mat, width, height, um, u1, vm, v1, nFlag & 0x3, level - 1);
        }
        break;
        }
    }

    void sample_patch_parameter(std::vector<vector2>& out, const bezier_patch<vector3>& patch, const matrix4& mat, int width, int height)
    {
        static const real knots[] = {0, 1};
        std::vector<real> u_params = std::vector<real>(knots, knots + 2);
        std::vector<real> v_params = std::vector<real>(knots, knots + 2);
        int usz = (int)u_params.size();
        int vsz = (int)v_params.size();
        for (int v = 0; v < vsz - 1; v++)
        {
            int nFlag = 0;
            if (v == vsz - 2) nFlag = 2;
            for (int u = 0; u < usz - 1; u++)
            {
                if (u == usz - 2) nFlag |= 1;
                sample_patch_parameter_(out, patch, mat, width, height, u_params[u], u_params[u + 1], v_params[v], v_params[v + 1], nFlag, 8);
            }
        }
    }

    void sample_patch_parameter(std::vector<vector2>& out, const multi_bezier_patch<vector3>& patch, const matrix4& mat, int width, int height)
    {
        std::vector<real> u_params = patch.get_knots_u();
        std::vector<real> v_params = patch.get_knots_v();
        int usz = (int)u_params.size();
        int vsz = (int)v_params.size();
        for (int v = 0; v < vsz - 1; v++)
        {
            int nFlag = 0;
            if (v == vsz - 2) nFlag = 2;
            for (int u = 0; u < usz - 1; u++)
            {
                if (u == usz - 2) nFlag |= 1;
                sample_patch_parameter_(out, patch, mat, width, height, u_params[u], u_params[u + 1], v_params[v], v_params[v + 1], nFlag, 8);
            }
        }
    }
}
