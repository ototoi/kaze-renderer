#include "triangulate_polygon_delaunay.h"

#include <vector>
#include <map>
#include <algorithm>
#include <iostream>
#include "values.h"
#include "logger.h"
#include "delaunay_mesh.h"

namespace kaze
{
    namespace
    {
        enum EDGE_TYPE
        {
            EDGE_DIAGONAL,        //free edge.
            EDGE_GLOBAL,          //edge is contained by global triangleangle.
            EDGE_CONSTRAINED,     //edge is contained by loop.
            EDGE_CONSTRAINED_REV, //edge is contained by loop.
            EDGE_NODE,
        };

        static const int TRAP[] = {0, 1, 2, 0, 1, 2};

        static inline real tri_area(const vector2& a, const vector2& b, const vector2& c)
        {
            return (a[0] - c[0]) * (b[1] - c[1]) - (a[1] - c[1]) * (b[0] - c[0]);
        }

        struct triangle;
        struct edge;

        struct point
        {
            point(const vector2& p_) : p(p_), p_next(NULL), p_edge(NULL), p_tri(NULL) {}
            vector2 p;
            union
            {
                point* p_next;
                size_t index;
            };
            edge* p_edge;
            triangle* p_tri;
        };

        struct triangle
        {
            triangle()
            {
                points[0] = NULL;
                points[1] = NULL;
                points[2] = NULL;
                neighbor[0] = NULL;
                neighbor[1] = NULL;
                neighbor[2] = NULL;
                edge_type[0] = EDGE_DIAGONAL;
                edge_type[1] = EDGE_DIAGONAL;
                edge_type[2] = EDGE_DIAGONAL;
                inners = NULL;
            }
            triangle(point* a, point* b, point* c)
            {
                points[0] = a;
                points[1] = b;
                points[2] = c;
                neighbor[0] = NULL;
                neighbor[1] = NULL;
                neighbor[2] = NULL;
                edge_type[0] = EDGE_DIAGONAL;
                edge_type[1] = EDGE_DIAGONAL;
                edge_type[2] = EDGE_DIAGONAL;
                inners = NULL;
            }

            void get_uvw(real uvw[3], const vector2& p)
            {
                (uvw[0] = tri_area(points[1]->p, points[2]->p, p));
                (uvw[1] = tri_area(points[2]->p, points[0]->p, p));
                (uvw[2] = tri_area(points[0]->p, points[1]->p, p));
            }

            bool test(const vector2& p) const
            {
                if ((tri_area(points[1]->p, points[2]->p, p)) < 0)
                {
                    return false;
                }
                if ((tri_area(points[2]->p, points[0]->p, p)) < 0)
                {
                    return false;
                }
                if ((tri_area(points[0]->p, points[1]->p, p)) < 0)
                {
                    return false;
                }
                return true;
            }

            triangle* clone() const
            {
                triangle* ptriangle = new triangle();
                memcpy(ptriangle, this, sizeof(triangle));
                return ptriangle;
            }

            void neighbor_swap(triangle* a, triangle* b)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (neighbor[i] == a)
                    {
                        neighbor[i] = b;
                        break;
                    }
                }
            }

            int index_of(const point* p) const
            {
                if (this->points[0] == p) return 0;
                if (this->points[1] == p) return 1;
                if (this->points[2] == p) return 2;
                return -1;
            }

            bool check_neighbor() const
            {
                if (neighbor[0])
                {
                    //assert(neighbor[0] != neighbor[1]);
                    if (neighbor[0] == neighbor[1])
                    {
                        return false;
                    }
                    if (neighbor[0] == neighbor[2])
                    {
                        return false;
                    }

                    int i = neighbor[0]->index_of(points[1]);
                    int j = neighbor[0]->index_of(points[2]);
                    if (i < 0 || j < 0) return false;
                    if (TRAP[j + 1] != i) return false;
                }
                if (neighbor[1])
                {
                    //assert(neighbor[1] != neighbor[2]);
                    if (neighbor[1] == neighbor[2])
                    {
                        return false;
                    }
                    if (neighbor[1] == neighbor[0])
                    {
                        return false;
                    }

                    int i = neighbor[1]->index_of(points[2]);
                    int j = neighbor[1]->index_of(points[0]);
                    if (i < 0 || j < 0) return false;
                    if (TRAP[j + 1] != i) return false;
                }
                if (neighbor[2])
                {
                    //assert(neighbor[2] != neighbor[0]);
                    if (neighbor[2] == neighbor[0])
                    {
                        return false;
                    }
                    if (neighbor[2] == neighbor[1])
                    {
                        return false;
                    }

                    int i = neighbor[2]->index_of(points[0]);
                    int j = neighbor[2]->index_of(points[1]);
                    if (i < 0 || j < 0) return false;
                    if (TRAP[j + 1] != i) return false;
                }
                return true;
            }

            bool check_area() const
            {
                if (tri_area(points[0]->p, points[1]->p, points[2]->p) < 0)
                {
                    return false;
                } //==0 is OK
                return true;
            }

            bool check() const
            {
                return check_neighbor() && check_area();
            }

            point* points[3];
            triangle* neighbor[3];
            EDGE_TYPE edge_type[3];
            union
            {
                point* inners;
                size_t index;
            };
        };

        struct edge
        {
            edge(point* a, point* b, int g, size_t i)
            {
                points[0] = a;
                points[1] = b;
                group = g;
                index = i;
            }
            point* points[2];
            int group; //
            size_t index;
        };

        template <class T>
        class inner_list
        {
        public:
            inner_list() : first_(NULL), last_(NULL) {}
            T* get_first() const { return first_; }
            void add(T* e)
            {
                if (!first_)
                {
                    first_ = last_ = e;
                    e->p_next = NULL;
                }
                else
                {
                    last_->p_next = e;
                    e->p_next = NULL;
                    last_ = e;
                }
            }

        private:
            T* first_;
            T* last_;
        };

        typedef inner_list<point> point_list;
    }

    static void get_minmax(vector2& min, vector2& max, const std::vector<vector2>& v)
    {
        min = max = v[0];
        size_t sz = v.size();
        for (size_t i = 0; i < sz; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                if (min[j] > v[i][j])
                {
                    min[j] = v[i][j];
                }
                if (max[j] < v[i][j])
                {
                    max[j] = v[i][j];
                }
            }
        }
    }

    //if A-D is Constrained -> bConstrained==true
    static bool is_incircle(const vector2& pa, const vector2& pb, const vector2& pc, const vector2& pd)
    {
        double adx = pa[0] - pd[0];
        double ady = pa[1] - pd[1];
        double bdx = pb[0] - pd[0];
        double bdy = pb[1] - pd[1];

        double adxbdy = adx * bdy;
        double bdxady = bdx * ady;
        double oabd = adxbdy - bdxady;

        if (oabd <= 0)
        {
            return false;
        }

        double cdx = pc[0] - pd[0];
        double cdy = pc[1] - pd[1];

        double cdxady = cdx * ady;
        double adxcdy = adx * cdy;
        double ocad = cdxady - adxcdy;

        if (ocad <= 0)
        {
            return false;
        }

        double bdxcdy = bdx * cdy;
        double cdxbdy = cdx * bdy;

        double alift = adx * adx + ady * ady;
        double blift = bdx * bdx + bdy * bdy;
        double clift = cdx * cdx + cdy * cdy;

        double det = alift * (bdxcdy - cdxbdy) + blift * ocad + clift * oabd;

        return det > 0;
    }

    static bool test_legalize(const point* a, const point* b, const point* c, const point* d)
    {
        if (tri_area(c->p, a->p, d->p) <= 0)
        {
            return false;
        }
        if (tri_area(a->p, b->p, d->p) <= 0)
        {
            return false;
        }

        if (is_incircle(a->p, b->p, c->p, d->p))
        {
            return true;
        }

        /*
        if (bConstarainAD) {//edge A->D is Constrain
            return true;
        }
        */

        return false;
    }

    static void legalize_edge(triangle* t, int i, std::vector<triangle*>& tris)
    {
        assert(t);
        if (!t->neighbor[i])
        {
            return;
        }
        if (t->edge_type[i] == EDGE_CONSTRAINED || t->edge_type[i] == EDGE_CONSTRAINED_REV)
        {
            return;
        }

        triangle* s = t->neighbor[i];

        assert(t != s);
        assert(t->check());
        assert(s->check());

        {
            point* a = t->points[TRAP[i]];
            point* b = t->points[TRAP[i + 1]];
            point* c = t->points[TRAP[i + 2]];

            int j = 0;
            if (s->points[2] == b)
            {
                j = 0;
            }
            else if (s->points[0] == b)
            {
                j = 1;
            }
            else
            {
                j = 2;
            }

            assert(s->points[TRAP[j + 2]] == b);
            assert(s->points[TRAP[j + 1]] == c);

            point* d = s->points[j];

            assert(a != d);

            //
            if (test_legalize(a, b, c, d))
            {
                triangle* t0 = t->clone();
                triangle* t1 = s->clone();

                triangle* P = t->neighbor[TRAP[i + 1]];
                triangle* Q = t->neighbor[TRAP[i + 2]];
                triangle* R = s->neighbor[TRAP[j + 1]];
                triangle* S = s->neighbor[TRAP[j + 2]];

                assert(t->check());
                assert(s->check());

                assert(!(t->neighbor[TRAP[i + 1]] == s->neighbor[TRAP[j + 1]]));
                assert(!(t->neighbor[TRAP[i + 2]] == s->neighbor[TRAP[j + 1]]));
                assert(!(t->neighbor[TRAP[i + 1]] == s->neighbor[TRAP[j + 2]]));
                assert(!(t->neighbor[TRAP[i + 2]] == s->neighbor[TRAP[j + 2]]));

                EDGE_TYPE typeQ = t->edge_type[TRAP[i + 2]];
                EDGE_TYPE typeS = s->edge_type[TRAP[j + 2]];

                t0->points[TRAP[i + 1]] = d;
                t0->neighbor[TRAP[i + 2]] = t1;
                t0->edge_type[TRAP[i + 2]] = EDGE_DIAGONAL;
                t0->neighbor[TRAP[i + 0]] = S;
                t0->edge_type[TRAP[i + 0]] = typeS;

                t1->points[TRAP[j + 1]] = a;
                t1->neighbor[TRAP[j + 2]] = t0;
                t1->edge_type[TRAP[j + 2]] = EDGE_DIAGONAL;
                t1->neighbor[TRAP[j + 0]] = Q;
                t1->edge_type[TRAP[j + 0]] = typeQ;

                {
                    point_list el0;
                    point_list el1;
                    {
                        point* c = t->inners;
                        while (c)
                        {
                            point* next = c->p_next;
                            if (t0->test(c->p))
                            {
                                el0.add(c);
                                c->p_tri = t0;
                            }
                            else if (t1->test(c->p))
                            {
                                el1.add(c);
                                c->p_tri = t1;
                            }
                            else
                            {
                                assert(0);
                            }
                            c = next;
                        }
                    }
                    {
                        point* c = s->inners;
                        while (c)
                        {
                            point* next = c->p_next;
                            if (t0->test(c->p))
                            {
                                el0.add(c);
                                c->p_tri = t0;
                            }
                            else if (t1->test(c->p))
                            {
                                el1.add(c);
                                c->p_tri = t1;
                            }
                            else
                            {
                                assert(0);
                            }
                            c = next;
                        }
                    }
                    t0->inners = el0.get_first();
                    t1->inners = el1.get_first();

                    t->inners = NULL;
                    s->inners = NULL;
                }

                //assert(t0->check_inners());
                //assert(t1->check_inners());

                if (P)
                {
                    P->neighbor_swap(t, t0);
                }
                if (Q)
                {
                    Q->neighbor_swap(t, t1);
                }
                if (R)
                {
                    R->neighbor_swap(s, t1);
                }
                if (S)
                {
                    S->neighbor_swap(s, t0);
                }

                assert(t->check());
                assert(s->check());
                assert(t0->check());
                assert(t1->check());

                assert(t0->neighbor[TRAP[i + 2]] == t1);
                assert(t1->neighbor[TRAP[j + 2]] == t0);

                assert(t0 != t0->neighbor[TRAP[i + 0]]);
                assert(t0 != t0->neighbor[TRAP[i + 1]]);
                assert(t0 != t0->neighbor[TRAP[i + 2]]);
                assert(t1 != t1->neighbor[TRAP[j + 0]]);
                assert(t1 != t1->neighbor[TRAP[j + 1]]);
                assert(t1 != t1->neighbor[TRAP[j + 2]]);

                assert(t0->neighbor[TRAP[i + 2]] == t1);
                assert(t1->neighbor[TRAP[j + 2]] == t0);

                assert(t0->points[TRAP[i + 0]] == t1->points[TRAP[j + 1]]);
                assert(t0->points[TRAP[i + 1]] == t1->points[TRAP[j + 0]]);
                assert(t0->points[TRAP[i + 2]] != t1->points[TRAP[j + 2]]);

                t->edge_type[0] = EDGE_NODE;
                t->edge_type[1] = EDGE_NODE;
                t->edge_type[2] = EDGE_NODE;
                t->neighbor[0] = NULL;
                t->neighbor[1] = NULL;
                t->neighbor[2] = NULL;

                s->edge_type[0] = EDGE_NODE;
                s->edge_type[1] = EDGE_NODE;
                s->edge_type[2] = EDGE_NODE;
                s->neighbor[0] = NULL;
                s->neighbor[1] = NULL;
                s->neighbor[2] = NULL;

                tris.push_back(t0);
                tris.push_back(t1);

                //------------------
                a->p_tri = t1;
                b->p_tri = t1;
                c->p_tri = t0;
                d->p_tri = t0;
                //------------------

                legalize_edge(t0, TRAP[i + 0], tris);
                legalize_edge(t1, TRAP[j + 1], tris);
            }
        }
    }

    static void create_triangle1(triangle* t, point* p, std::vector<triangle*>& tris)
    {
        assert(t);

        triangle* t0 = t->clone();
        triangle* t1 = t->clone();
        triangle* t2 = t->clone();

        point* p0 = t->points[0];
        point* p1 = t->points[1];
        point* p2 = t->points[2];

        t0->points[0] = p;
        t1->points[1] = p;
        t2->points[2] = p;

        {
            point_list el0;
            point_list el1;
            point_list el2;
            {
                point* c = t->inners;
                while (c)
                {
                    point* next = c->p_next;
                    if (c == p)
                    {                     //kill self
                        c->p_next = NULL; //
                        c->p_tri = NULL;
                    }
                    else if (t0->test(c->p))
                    {
                        el0.add(c);
                        c->p_tri = t0;
                    }
                    else if (t1->test(c->p))
                    {
                        el1.add(c);
                        c->p_tri = t1;
                    }
                    else if (t2->test(c->p))
                    {
                        el2.add(c);
                        c->p_tri = t2;
                    }
                    c = next;
                }
            }
            t0->inners = el0.get_first();
            t1->inners = el1.get_first();
            t2->inners = el2.get_first();

            t->inners = NULL;
        }

        //assert(t0->check_inners());
        //assert(t1->check_inners());
        //assert(t2->check_inners());

        t0->edge_type[1] = EDGE_DIAGONAL;
        t0->edge_type[2] = EDGE_DIAGONAL;
        t1->edge_type[2] = EDGE_DIAGONAL;
        t1->edge_type[0] = EDGE_DIAGONAL;
        t2->edge_type[0] = EDGE_DIAGONAL;
        t2->edge_type[1] = EDGE_DIAGONAL;

        triangle* P = t->neighbor[0];
        triangle* Q = t->neighbor[1];
        triangle* R = t->neighbor[2];

        t0->neighbor[1] = t1;
        t0->neighbor[2] = t2;
        t1->neighbor[2] = t2;
        t1->neighbor[0] = t0;
        t2->neighbor[0] = t0;
        t2->neighbor[1] = t1;

        assert(t0 != t0->neighbor[0]);
        assert(t0 != t0->neighbor[1]);
        assert(t0 != t0->neighbor[2]);
        assert(t1 != t1->neighbor[0]);
        assert(t1 != t1->neighbor[1]);
        assert(t1 != t1->neighbor[2]);
        assert(t2 != t2->neighbor[0]);
        assert(t2 != t2->neighbor[1]);
        assert(t2 != t2->neighbor[2]);

        //------------------
        if (P)
        {
            P->neighbor_swap(t, t0);
        }

        if (Q)
        {
            Q->neighbor_swap(t, t1);
        }

        if (R)
        {
            R->neighbor_swap(t, t2);
        }

        assert(t0->check());
        assert(t1->check());
        assert(t2->check());

        //------------------
        t->edge_type[0] = EDGE_NODE;
        t->edge_type[1] = EDGE_NODE;
        t->edge_type[2] = EDGE_NODE;
        t->neighbor[0] = NULL;
        t->neighbor[1] = NULL;
        t->neighbor[2] = NULL;
        //------------------

        //------------------
        p0->p_tri = t2;
        p1->p_tri = t0;
        p2->p_tri = t1;
        p->p_tri = t0;
        //------------------

        tris.push_back(t0);
        tris.push_back(t1);
        tris.push_back(t2);

        legalize_edge(t0, 0, tris);
        legalize_edge(t1, 1, tris);
        legalize_edge(t2, 2, tris);
    }

    static void create_triangle2(int i, triangle* t, point* p, std::vector<triangle*>& tris)
    {
        assert(t);
        assert(t->neighbor[i]);

        triangle* s = t->neighbor[i];

        point* a = t->points[TRAP[i + 0]];
        point* b = t->points[TRAP[i + 1]];
        point* c = t->points[TRAP[i + 2]];

        int j = 0;
        if (s->points[2] == b)
        {
            j = 0;
        }
        else if (s->points[0] == b)
        {
            j = 1;
        }
        else
        {
            j = 2;
        }

        assert(s->points[TRAP[j + 2]] == b);
        assert(s->points[TRAP[j + 1]] == c);

        point* d = s->points[TRAP[j + 0]];

        triangle* t0 = t->clone();
        triangle* t1 = t->clone();

        triangle* s0 = s->clone();
        triangle* s1 = s->clone();

        t0->points[TRAP[i + 1]] = p;
        t1->points[TRAP[i + 2]] = p;

        t0->edge_type[TRAP[i + 0]] = EDGE_DIAGONAL;
        t0->edge_type[TRAP[i + 2]] = EDGE_DIAGONAL;
        t1->edge_type[TRAP[i + 1]] = EDGE_DIAGONAL;
        t1->edge_type[TRAP[i + 0]] = EDGE_DIAGONAL;

        s0->points[TRAP[j + 1]] = p;
        s1->points[TRAP[j + 2]] = p;

        s0->edge_type[TRAP[j + 0]] = EDGE_DIAGONAL;
        s0->edge_type[TRAP[j + 2]] = EDGE_DIAGONAL;
        s1->edge_type[TRAP[j + 1]] = EDGE_DIAGONAL;
        s1->edge_type[TRAP[j + 0]] = EDGE_DIAGONAL;

        assert(t->check());
        assert(s->check());

        {
            point_list elt0;
            point_list elt1;
            point_list els0;
            point_list els1;
            {
                point* c = t->inners;
                while (c)
                {
                    point* next = c->p_next;
                    if (c == p)
                    {
                        c->p_next = NULL; //
                        c->p_tri = NULL;
                    }
                    else if (t0->test(c->p))
                    {
                        elt0.add(c);
                        c->p_tri = t0;
                    }
                    else if (t1->test(c->p))
                    {
                        elt1.add(c);
                        c->p_tri = t1;
                    }
                    else if (s0->test(c->p))
                    {
                        els0.add(c);
                        c->p_tri = s0;
                    }
                    else if (s1->test(c->p))
                    {
                        els1.add(c);
                        c->p_tri = s1;
                    }
                    else
                    {
                        assert(0);
                    }
                    c = next;
                }
            }
            {
                point* c = s->inners;
                while (c)
                {
                    point* next = c->p_next;
                    if (c == p)
                    {
                        c->p_next = NULL; //
                        c->p_tri = NULL;
                    }
                    else if (s0->test(c->p))
                    {
                        els0.add(c);
                        c->p_tri = s0;
                    }
                    else if (s1->test(c->p))
                    {
                        els1.add(c);
                        c->p_tri = s1;
                    }
                    else if (t0->test(c->p))
                    {
                        elt0.add(c);
                        c->p_tri = t0;
                    }
                    else if (t1->test(c->p))
                    {
                        elt1.add(c);
                        c->p_tri = t1;
                    }
                    else
                    {
                        assert(0);
                    }
                    c = next;
                }
            }
            t0->inners = elt0.get_first();
            t1->inners = elt1.get_first();
            s0->inners = els0.get_first();
            s1->inners = els1.get_first();

            t->inners = NULL;
            s->inners = NULL;
        }

        //assert(t0->check_inners());
        //assert(t1->check_inners());
        //assert(s0->check_inners());
        //assert(s1->check_inners());

        triangle* P = t->neighbor[TRAP[i + 1]];
        triangle* Q = t->neighbor[TRAP[i + 2]];
        triangle* R = s->neighbor[TRAP[j + 1]];
        triangle* S = s->neighbor[TRAP[j + 2]];

        t0->neighbor[TRAP[i + 2]] = t1;
        t1->neighbor[TRAP[i + 1]] = t0;

        s0->neighbor[TRAP[j + 2]] = s1;
        s1->neighbor[TRAP[j + 1]] = s0;

        t0->neighbor[TRAP[i + 0]] = s1;
        s1->neighbor[TRAP[j + 0]] = t0;

        t1->neighbor[TRAP[i + 0]] = s0;
        s0->neighbor[TRAP[j + 0]] = t1;

        assert(t0 != t0->neighbor[0]);
        assert(t0 != t0->neighbor[1]);
        assert(t0 != t0->neighbor[2]);
        assert(t1 != t1->neighbor[0]);
        assert(t1 != t1->neighbor[1]);
        assert(t1 != t1->neighbor[2]);
        assert(s0 != s0->neighbor[0]);
        assert(s0 != s0->neighbor[1]);
        assert(s0 != s0->neighbor[2]);
        assert(s1 != s1->neighbor[0]);
        assert(s1 != s1->neighbor[1]);
        assert(s1 != s1->neighbor[2]);

        if (P)
        {
            P->neighbor_swap(t, t0);
        }
        if (Q)
        {
            Q->neighbor_swap(t, t1);
        }

        if (R)
        {
            R->neighbor_swap(s, s0);
        }
        if (S)
        {
            S->neighbor_swap(s, s1);
        }

        assert(t0->check());
        assert(t1->check());
        assert(s0->check());
        assert(s1->check());

        //------------------

        t->edge_type[0] = EDGE_NODE;
        t->edge_type[1] = EDGE_NODE;
        t->edge_type[2] = EDGE_NODE;
        t->neighbor[0] = NULL;
        t->neighbor[1] = NULL;
        t->neighbor[2] = NULL;

        //------------------

        s->edge_type[0] = EDGE_NODE;
        s->edge_type[1] = EDGE_NODE;
        s->edge_type[2] = EDGE_NODE;
        s->neighbor[0] = NULL;
        s->neighbor[1] = NULL;
        s->neighbor[2] = NULL;

        //------------------
        a->p_tri = t1;
        b->p_tri = s0;
        c->p_tri = t0;
        d->p_tri = s1;
        p->p_tri = t0;
        //------------------

        tris.push_back(t0);
        tris.push_back(t1);
        tris.push_back(s0);
        tris.push_back(s1);

        legalize_edge(t0, TRAP[i + 1], tris);
        legalize_edge(t1, TRAP[i + 2], tris);
        legalize_edge(s0, TRAP[j + 1], tris);
        legalize_edge(s1, TRAP[j + 2], tris);
    }

    static void create_triangle3(triangle* t, point* p, std::vector<triangle*>& tris)
    {
        { //kill point
            point_list el;
            {
                point* c = t->inners;
                while (c)
                {
                    point* next = c->p_next;
                    if (c == p)
                    {                     //kill self
                        c->p_next = NULL; //
                        c->p_tri = NULL;
                    }
                    else
                    {
                        el.add(c);
                        c->p_tri = t;
                    }
                    c = next;
                }
            }
            t->inners = el.get_first();
            p->p_tri = NULL;
        }
    }

    static void create_triangle(triangle* t, point* p, std::vector<triangle*>& tris)
    {
        static const real EPSILON = values::epsilon() * 1024;
        if (!t) return;

        assert(t);
        assert(p);
        real uvw[3];
        t->get_uvw(uvw, p->p);

        int nZeroCount = 0;
        int i = -1;
        for (int j = 0; j < 3; j++)
        {
            assert(uvw[j] >= 0);
            if (fabs(uvw[j]) <= EPSILON)
            {
                i = j;
                nZeroCount++;
            }
        }

        assert(t != t->neighbor[0]);
        assert(t != t->neighbor[1]);
        assert(t != t->neighbor[2]);

        assert(t->neighbor[0] == NULL || t->neighbor[0] != t->neighbor[1]);
        assert(t->neighbor[1] == NULL || t->neighbor[1] != t->neighbor[2]);
        assert(t->neighbor[2] == NULL || t->neighbor[2] != t->neighbor[0]);

        switch (nZeroCount)
        {
        case 0:
            create_triangle1(t, p, tris);
            break;
        case 1:
            create_triangle2(i, t, p, tris);
            break;
        case 2: //fall through
        case 3:
            create_triangle3(t, p, tris);
            break;
        }
    }

    //---------------------------------------------------------------------------------------------

    static int index_of(triangle* t, point* p)
    {
        if (t->points[0] == p) return 0;
        if (t->points[1] == p) return 1;
        if (t->points[2] == p) return 2;
        return -1;
    }

    static triangle* get_left(triangle* t, point* p)
    {
        int i = index_of(t, p);
        assert(i >= 0);
        return t->neighbor[TRAP[i + 1]];
    }
    static triangle* get_right(triangle* t, point* p)
    {
        int i = index_of(t, p);
        assert(i >= 0);
        return t->neighbor[TRAP[i + 2]];
    }

    static void replace_point_triangle(point* p, triangle* t_ignore)
    {
        triangle* t = p->p_tri;
        triangle* t_first = t;

        assert(t);

        if (t != t_ignore) return;

        while (t = get_left(t, p))
        {
            if (t == t_first) return;
            if (t != t_ignore && t->edge_type[0] != EDGE_NODE)
            {
                p->p_tri = t;
                return;
            }
        }
        t = t_first;
        while (t = get_right(t, p))
        {
            //if(t == t_first)return NULL;
            if (t != t_ignore && t->edge_type[0] != EDGE_NODE)
            {
                p->p_tri = t;
                return;
            }
        }
    }

    static bool test_line_line(const vector2& a, const vector2& b, const vector2& c, const vector2& d)
    {
        real a1 = tri_area(a, b, d);
        real a2 = tri_area(a, b, c);
        real a3 = tri_area(c, d, a);
        real a4 = tri_area(c, d, b);

        if (a1 * a2 < 0)
        {
            if (a3 * a4 < 0)
            {
                return true;
            }
        }

        return false;
    }

    static void print_line(const vector2& p, const vector2& a, const vector2& b, const vector2& c, const vector2& d)
    {
        std::cout << p << "," << a << "," << b << std::endl;
        std::cout << c << "," << d << std::endl;
    }

    namespace
    {
        struct bound_edge_set
        {
            std::vector<point*> points;
            std::vector<EDGE_TYPE> edge_types;
            std::vector<triangle*> neighbors;
        };

        struct index_angle
        {
            size_t idx;
            double angle;
        };

        struct index_set
        {
            size_t indices[3];
        };

        enum
        {
            ORIENTATION_CW = 0,
            ORIENTATION_CCW,
            ORIENTATION_UNKNOWN
        };
    }

    static double det(double a, double b, double c, double d)
    {
        return a * d - b * c;
    }

    static int calc_determinant(const vector2& a, const vector2& b, const vector2& c)
    {
        vector2 ab = b - a;
        vector2 ac = c - a;
        double Determ = det(ab[0], ac[0], ab[1], ac[1]); //ad-bc

        if (Determ > 0)
        {
            return ORIENTATION_CCW;
        }
        else if (Determ < 0)
        {
            return ORIENTATION_CW;
        }
        else
        {
            return ORIENTATION_UNKNOWN;
        }
    }

    //calc angle B
    static double calc_angle(const vector2& a, const vector2& b, const vector2& c, const vector2& shift)
    {
        static const double PI2 = 2 * values::pi();
        vector2 os(0, 0);
        if (a == c)
        {
            os = shift * 0.000001;
        }

        int nDet = calc_determinant(a - os, b, c + os); //bit shift
        if (nDet == ORIENTATION_UNKNOWN) return 0;

        vector2 ba = normalize(a - b);
        vector2 bc = normalize(c - b);

        double cc = dot(ba, bc);
        double cos_m = acos(cc);
        if (nDet == ORIENTATION_CW)
        {
            return cos_m;
        }
        else
        {
            return PI2 - cos_m;
        }
    }

    static bool check_triangles(const std::vector<index_set>& indices, const bound_edge_set& bes)
    {
        size_t tsz = indices.size();
        for (size_t i = 0; i < tsz; i++)
        {
            point* a = bes.points[indices[i].indices[0]];
            point* b = bes.points[indices[i].indices[1]];
            point* c = bes.points[indices[i].indices[2]];
            if (calc_determinant(a->p, b->p, c->p) == ORIENTATION_CW)
            {
                for (size_t k = 0; k < bes.points.size(); k++)
                {
                    std::cout << bes.points[k]->p << std::endl;
                }
                return false;
            }
        }
        return true;
    }

    static void trianglulate_bound(std::vector<index_set>& indices, const bound_edge_set& bes)
    {
        typedef std::vector<index_set> IDX_VECTOR;
        typedef std::vector<index_angle> ANGLE_VECTOR;
        typedef ANGLE_VECTOR::iterator ANGLE_ITER;

        static const double PI = values::pi();

        //calculate angles
        std::vector<index_angle> angles;
        angles.resize(bes.points.size());

        point* a = bes.points.front();
        point* b = bes.points.back();

        vector2 NAB = normalize(b->p - a->p); //ab
        angles[0].idx = 0;
        angles[0].angle = 0;
        for (size_t i = 1; i < bes.points.size() - 1; i++)
        {
            point* prev = bes.points[i - 1];
            point* crnt = bes.points[i];
            point* next = bes.points[i + 1];
            angles[i].idx = i;
            angles[i].angle = calc_angle(prev->p, crnt->p, next->p, NAB);
        }
        angles[bes.points.size() - 1].idx = bes.points.size() - 1;
        angles[bes.points.size() - 1].angle = 0;

        indices.clear();
        while (!angles.empty())
        {
            size_t asz = angles.size();
            if (asz != 3)
            {
                int k = -1;
                for (int j = 1; j < asz - 1; j++)
                {
                    double ang = angles[j].angle;
                    if (ang < PI)
                    {
                        k = j;
                        break;
                    }
                }
                if (k < 0)
                {
                    for (int j = 1; j < asz - 1; j++)
                    {
                        double ang = angles[j].angle;
                        if (ang <= PI)
                        {
                            k = j;
                            break;
                        }
                    }
                }
                assert(k > 0);
                index_set tmp;
                tmp.indices[0] = angles[k].idx;
                tmp.indices[1] = angles[k - 1].idx;
                tmp.indices[2] = angles[k + 1].idx;
                indices.push_back(tmp);
                //
                if (k - 1 != 0)
                {
                    point* prev = bes.points[angles[k - 2].idx];
                    point* crnt = bes.points[angles[k - 1].idx];
                    point* next = bes.points[angles[k + 1].idx];
                    angles[k - 1].angle = calc_angle(prev->p, crnt->p, next->p, NAB);
                }
                if (k + 1 != asz - 1)
                {
                    point* prev = bes.points[angles[k - 1].idx];
                    point* crnt = bes.points[angles[k + 1].idx];
                    point* next = bes.points[angles[k + 2].idx];
                    angles[k + 1].angle = calc_angle(prev->p, crnt->p, next->p, NAB);
                }
                ANGLE_ITER it = angles.begin();
                std::advance(it, k);
                angles.erase(it);
            }
            else
            {
                index_set tmp;
                tmp.indices[0] = angles[1].idx;
                tmp.indices[1] = angles[0].idx;
                tmp.indices[2] = angles[2].idx;
                indices.push_back(tmp);
                angles.clear();
            }
        }
    }

    static void connectNighbor(triangle* t, int n, const std::vector<triangle*>& tris)
    {
        point* a = t->points[TRAP[n + 1]];
        point* b = t->points[TRAP[n + 2]];

        size_t sz = tris.size();
        for (size_t i = 0; i < sz; i++)
        {
            triangle* s = tris[i];
            if (s->points[0] == b && s->points[1] == a)
            {
                s->neighbor[2] = t;
                t->neighbor[n] = s;
                break;
            }
            if (s->points[1] == b && s->points[2] == a)
            {
                s->neighbor[0] = t;
                t->neighbor[n] = s;
                break;
            }
            if (s->points[2] == b && s->points[0] == a)
            {
                s->neighbor[1] = t;
                t->neighbor[n] = s;
                break;
            }
        }
    }

    static void replace_edge(std::vector<triangle*>& trs, point* a, point* b, std::vector<triangle*>& tris)
    {
        size_t sz = trs.size();
        assert(sz >= 2);
        {
            //make bound
            bound_edge_set eL;
            bound_edge_set eR;
            triangle* t = trs[0];
            triangle* t_next = trs[1];
            int inf_idx = index_of(t, a);
            int nxt_idx = TRAP[index_of(t_next, t->points[TRAP[inf_idx + 1]]) + 1];
            eL.points.push_back(a);
            eR.points.push_back(a);
            eL.neighbors.push_back(t->neighbor[TRAP[inf_idx + 1]]);
            eR.neighbors.push_back(t->neighbor[TRAP[inf_idx + 2]]);
            eL.edge_types.push_back(t->edge_type[TRAP[inf_idx + 1]]);
            eR.edge_types.push_back(t->edge_type[TRAP[inf_idx + 2]]);

            for (size_t i = 1; i < sz - 1; i++)
            {
                t = trs[i];
                t_next = trs[i + 1];
                inf_idx = nxt_idx;
                if (test_line_line(t->points[TRAP[inf_idx]]->p, t->points[TRAP[inf_idx + 1]]->p, a->p, b->p))
                {
                    eR.points.push_back(t->points[TRAP[inf_idx + 2]]);
                    eR.neighbors.push_back(t->neighbor[TRAP[inf_idx + 1]]);
                    eR.edge_types.push_back(t->edge_type[TRAP[inf_idx + 1]]);
                    nxt_idx = TRAP[index_of(t_next, t->points[TRAP[inf_idx]]) + 1];
                }
                else
                {
                    eL.points.push_back(t->points[TRAP[inf_idx + 1]]);
                    eL.neighbors.push_back(t->neighbor[TRAP[inf_idx + 2]]);
                    eL.edge_types.push_back(t->edge_type[TRAP[inf_idx + 2]]);
                    nxt_idx = TRAP[index_of(t_next, t->points[TRAP[inf_idx + 2]]) + 1];
                }
            }
            {
                t = trs[sz - 1];
                inf_idx = nxt_idx;
                assert(inf_idx == index_of(t, b));
                eL.points.push_back(t->points[TRAP[inf_idx + 1]]);
                eR.points.push_back(t->points[TRAP[inf_idx + 2]]);
                eL.neighbors.push_back(t->neighbor[TRAP[inf_idx + 2]]);
                eR.neighbors.push_back(t->neighbor[TRAP[inf_idx + 1]]);
                eL.edge_types.push_back(t->edge_type[TRAP[inf_idx + 2]]);
                eR.edge_types.push_back(t->edge_type[TRAP[inf_idx + 1]]);
            }
            eL.points.push_back(b);
            eR.points.push_back(b);

            std::reverse(eR.points.begin(), eR.points.end());
            std::reverse(eR.neighbors.begin(), eR.neighbors.end());
            std::reverse(eR.edge_types.begin(), eR.edge_types.end());

            assert(eL.points.size() >= 3);
            assert(eR.points.size() >= 3);
            assert(eL.points.size() == eL.neighbors.size() + 1);
            assert(eR.points.size() == eR.neighbors.size() + 1);

            std::vector<index_set> indicesL;
            trianglulate_bound(indicesL, eL);
            std::vector<index_set> indicesR;
            trianglulate_bound(indicesR, eR);
//print_log("tszL:%d¥n",indicesL.size());
//print_log("tszR:%d¥n",indicesR.size());

#ifdef _DEBUG
            if (!check_triangles(indicesL, eL))
            {
                print_log("L dark¥n");
            }
            if (!check_triangles(indicesR, eR))
            {
                print_log("R dark¥n");
            }
#endif
            std::map<point*, std::vector<triangle*> > mapPToT;
            std::vector<triangle*> triL;
            std::vector<triangle*> triR;
            size_t lsz = indicesL.size();
            for (size_t i = 0; i < lsz; i++)
            {
                size_t i0 = indicesL[i].indices[0];
                size_t i1 = indicesL[i].indices[1];
                size_t i2 = indicesL[i].indices[2];
                triangle* t = new triangle(eL.points[i0], eL.points[i1], eL.points[i2]);
                mapPToT[t->points[0]].push_back(t);
                mapPToT[t->points[1]].push_back(t);
                mapPToT[t->points[2]].push_back(t);

                triL.push_back(t);
            }

            size_t rsz = indicesR.size();
            for (size_t i = 0; i < rsz; i++)
            {
                size_t i0 = indicesR[i].indices[0];
                size_t i1 = indicesR[i].indices[1];
                size_t i2 = indicesR[i].indices[2];
                triangle* t = new triangle(eR.points[i0], eR.points[i1], eR.points[i2]);
                mapPToT[t->points[0]].push_back(t);
                mapPToT[t->points[1]].push_back(t);
                mapPToT[t->points[2]].push_back(t);

                triR.push_back(t);
            }

            //-------------------------------------
            //connect to neighbor
            for (size_t i = 0; i < lsz; i++)
            {
                triangle* t = triL[i];
                connectNighbor(t, 0, mapPToT[t->points[1]]);
                connectNighbor(t, 1, mapPToT[t->points[2]]);
                connectNighbor(t, 2, mapPToT[t->points[0]]);
            }
            for (size_t i = 0; i < rsz; i++)
            {
                triangle* t = triR[i];
                connectNighbor(t, 0, mapPToT[t->points[1]]);
                connectNighbor(t, 1, mapPToT[t->points[2]]);
                connectNighbor(t, 2, mapPToT[t->points[0]]);
            }
            //-------------------------------------
            //connect to bound
            for (size_t i = 0; i < lsz; i++)
            {
                size_t i0 = indicesL[i].indices[0];
                size_t i1 = indicesL[i].indices[1];
                size_t i2 = indicesL[i].indices[2];
                triangle* t = triL[i];
                if (i2 + 1 == i1)
                {
                    t->edge_type[0] = eL.edge_types[i2];
                    if (!t->neighbor[0])
                    {
                        triangle* s = eL.neighbors[i2];
                        if (s)
                        {
                            t->neighbor[0] = s;
                            int j = index_of(s, t->points[1]);
                            assert(j >= 0);
                            s->neighbor[TRAP[j + 1]] = t;
                        }
                    }
                }
                if (i0 + 1 == i2)
                {
                    t->edge_type[1] = eL.edge_types[i0];
                    if (!t->neighbor[1])
                    {
                        triangle* s = eL.neighbors[i0];
                        if (s)
                        {
                            t->neighbor[1] = s;
                            int j = index_of(s, t->points[2]);
                            assert(j >= 0);
                            s->neighbor[TRAP[j + 1]] = t;
                        }
                    }
                }
                if (i1 + 1 == i0)
                {
                    t->edge_type[2] = eL.edge_types[i1];
                    if (!t->neighbor[2])
                    {
                        triangle* s = eL.neighbors[i1];
                        if (s)
                        {
                            t->neighbor[2] = s;
                            int j = index_of(s, t->points[0]);
                            assert(j >= 0);
                            s->neighbor[TRAP[j + 1]] = t;
                        }
                    }
                }
            }

            for (size_t i = 0; i < rsz; i++)
            {
                size_t i0 = indicesR[i].indices[0];
                size_t i1 = indicesR[i].indices[1];
                size_t i2 = indicesR[i].indices[2];
                triangle* t = triR[i];
                if (i2 + 1 == i1)
                {
                    t->edge_type[0] = eR.edge_types[i2];
                    if (!t->neighbor[0])
                    {
                        triangle* s = eR.neighbors[i2];
                        if (s)
                        {
                            t->neighbor[0] = s;
                            int j = index_of(s, t->points[1]);
                            assert(j >= 0);
                            s->neighbor[TRAP[j + 1]] = t;
                        }
                    }
                }
                if (i0 + 1 == i2)
                {
                    t->edge_type[1] = eR.edge_types[i0];
                    if (!t->neighbor[1])
                    {
                        triangle* s = eR.neighbors[i0];
                        if (s)
                        {
                            t->neighbor[1] = s;
                            int j = index_of(s, t->points[2]);
                            assert(j >= 0);
                            s->neighbor[TRAP[j + 1]] = t;
                        }
                    }
                }
                if (i1 + 1 == i0)
                {
                    t->edge_type[2] = eR.edge_types[i1];
                    if (!t->neighbor[2])
                    {
                        triangle* s = eR.neighbors[i1];
                        if (s)
                        {
                            t->neighbor[2] = s;
                            int j = index_of(s, t->points[0]);
                            assert(j >= 0);
                            s->neighbor[TRAP[j + 1]] = t;
                        }
                    }
                }
            }
            //-----------------------------------------
            {
                triangle* tL = triL.back();
                triangle* tR = tL->neighbor[0];
                assert(tL);
                assert(tR);
                assert(tL->points[1] == a);
                assert(tL->points[2] == b);
                assert(tR->points[1] == b);
                assert(tR->points[2] == a);
                //if(!bInner){//outer
                tL->edge_type[0] = EDGE_CONSTRAINED;
                tR->edge_type[0] = EDGE_CONSTRAINED_REV;
                //}else{
                //    tL->edge_type[0] = EDGE_CONSTRAINED_REV;
                //    tR->edge_type[0] = EDGE_CONSTRAINED;
                //}
            }
            //-----------------------------------------
            for (size_t i = 0; i < lsz; i++)
            {
                triangle* t = triL[i];
                t->points[0]->p_tri = t;
                t->points[1]->p_tri = t;
                t->points[2]->p_tri = t;
            }
            for (size_t i = 0; i < rsz; i++)
            {
                triangle* t = triR[i];
                t->points[0]->p_tri = t;
                t->points[1]->p_tri = t;
                t->points[2]->p_tri = t;
            }
            //-----------------------------------------
            {
                std::vector<triangle*> tmpTrs;
#if 1
                for (size_t i = 0; i < lsz; i++)
                {
                    triangle* t = triL[i];
                    for (int j = 0; j < 3; j++)
                    {
                        if (t->edge_type[j] != EDGE_NODE)
                        {
                            legalize_edge(t, j, tmpTrs);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                for (size_t i = 0; i < rsz; i++)
                {
                    triangle* t = triR[i];
                    for (int j = 0; j < 3; j++)
                    {
                        if (t->edge_type[j] != EDGE_NODE)
                        {
                            legalize_edge(t, j, tmpTrs);
                        }
                        else
                        {
                            break;
                        }
                    }
                }
#endif
                //-----------------------------------------
                for (size_t i = 0; i < lsz; i++)
                {
                    triangle* t = triL[i];
                    if (t->edge_type[0] != EDGE_NODE)
                    {
                        tris.push_back(t);
                    }
                    else
                    {
                        delete t;
                    }
                }
                for (size_t i = 0; i < rsz; i++)
                {
                    triangle* t = triR[i];
                    if (t->edge_type[0] != EDGE_NODE)
                    {
                        tris.push_back(t);
                    }
                    else
                    {
                        delete t;
                    }
                }
                for (size_t i = 0; i < tmpTrs.size(); i++)
                {
                    triangle* t = tmpTrs[i];
                    if (t->edge_type[0] != EDGE_NODE)
                    {
                        tris.push_back(t);
                    }
                    else
                    {
                        delete t;
                    }
                }
                //-----------------------------------------
            }
        }
        //kill old faces.
        for (size_t i = 0; i < sz; i++)
        {
            triangle* t = trs[i];
            assert(t);
            t->edge_type[0] = EDGE_NODE;
            t->edge_type[1] = EDGE_NODE;
            t->edge_type[2] = EDGE_NODE;
            t->neighbor[0] = NULL;
            t->neighbor[1] = NULL;
            t->neighbor[2] = NULL;
        }
    }

    static triangle* get_edge_contain_triangle(point* a, point* b)
    {
        triangle* t = a->p_tri;
        triangle* t_first = t;
        assert(t);

        int j = index_of(t, b);
        if (j >= 0) return t;

        while (t = get_left(t, a))
        {
            if (t == t_first) return NULL;
            j = index_of(t, b);
            if (j >= 0) return t;
        }
        t = t_first;
        while (t = get_right(t, a))
        {
            //if(t == t_first)return NULL;
            j = index_of(t, b);
            if (j >= 0) return t;
        }

        return NULL;
    }

    static triangle* get_edge_influence_triangle(point* a, point* b)
    {
        triangle* t = a->p_tri;
        triangle* t_first = t;
        assert(t);
        //assert(t->edge_type[0] != EDGE_NODE);

        int i = index_of(t, a);
        int j = index_of(t, b);
        assert(i >= 0);
        assert(j < 0);
        //print_line(t->points[TRAP[i+0]]->p, t->points[TRAP[i+1]]->p,t->points[TRAP[i+2]]->p, a->p, b->p);

        if (test_line_line(t->points[TRAP[i + 1]]->p, t->points[TRAP[i + 2]]->p, a->p, b->p))
        {
            return t;
        }

        assert(!t->test(b->p));

        while (t = get_left(t, a))
        {
            if (t == t_first) return NULL;
            assert(!t->test(b->p));

            i = index_of(t, a);
            //j = index_of(t, b);
            assert(i >= 0);
            //assert(j<0);
            //print_line(t->points[TRAP[i+0]]->p, t->points[TRAP[i+1]]->p,t->points[TRAP[i+2]]->p, a->p, b->p);

            if (test_line_line(t->points[TRAP[i + 1]]->p, t->points[TRAP[i + 2]]->p, a->p, b->p))
            {
                return t;
            }
        }
        t = t_first;
        while (t = get_right(t, a))
        {
            //if(t == t_first)return NULL;
            assert(!t->test(b->p));

            i = index_of(t, a);
            //j = index_of(t, b);
            assert(i >= 0);
            //assert(j<0);
            //print_line(t->points[TRAP[i+0]]->p, t->points[TRAP[i+1]]->p,t->points[TRAP[i+2]]->p, a->p, b->p);

            if (test_line_line(t->points[TRAP[i + 1]]->p, t->points[TRAP[i + 2]]->p, a->p, b->p))
            {
                return t;
            }
        }
        assert(0);
        return NULL;
    }

    static int get_next_edge_influence_triangle(triangle* t, int i, point* a, point* b) //i==influence edge
    {
        int j = index_of(t, b);
        if (j >= 0) return -1;
        if (test_line_line(t->points[TRAP[i + 2]]->p, t->points[TRAP[i + 0]]->p, a->p, b->p))
        {
            return TRAP[i + 1];
        }
        if (test_line_line(t->points[TRAP[i + 0]]->p, t->points[TRAP[i + 1]]->p, a->p, b->p))
        {
            return TRAP[i + 2];
        }
        return -1;
    }

    static void insert_edge(edge* e, std::vector<triangle*>& tris)
    {
        assert(e);
        assert(e->points[0]);
        assert(e->points[1]);
        point* a = e->points[0];
        point* b = e->points[1];
        if (!a->p_tri || !b->p_tri) return;

        triangle* t = get_edge_contain_triangle(a, b);
        if (t)
        {
            int i = index_of(t, a);
            int j = index_of(t, b);
            //---------------------
            assert(i >= 0);
            assert(j >= 0);
            assert(i != j);
            //---------------------
            if (TRAP[i + 1] == j)
            {
                t->edge_type[TRAP[i + 2]] = EDGE_CONSTRAINED;
                triangle* t2 = t->neighbor[TRAP[i + 2]];
                if (t2)
                {
                    int i2 = index_of(t2, a);
                    int j2 = index_of(t2, b);
                    assert(i2 >= 0);
                    assert(j2 >= 0);
                    assert(TRAP[j2 + 1] == i2);
                    assert(t2->neighbor[TRAP[j2 + 2]] == t);
                    t2->edge_type[TRAP[j2 + 2]] = EDGE_CONSTRAINED_REV;
                }
            }
            else
            {
                assert(TRAP[i + 2] == j);
                t->edge_type[TRAP[i + 1]] = EDGE_CONSTRAINED_REV;
                triangle* t2 = t->neighbor[TRAP[i + 1]];
                if (t2)
                {
                    int i2 = index_of(t2, a);
                    int j2 = index_of(t2, b);
                    assert(i2 >= 0);
                    assert(j2 >= 0);
                    assert(TRAP[i2 + 1] == j2);
                    assert(t2->neighbor[TRAP[i2 + 2]] == t);
                    t2->edge_type[TRAP[i2 + 2]] = EDGE_CONSTRAINED;
                }
            }
        }
        else
        {
            //Find influence edge & Repace Triangle.

            std::vector<triangle*> trs; //influence triangles
            t = get_edge_influence_triangle(a, b);
            assert(t);
            int i = index_of(t, a);
            assert(i >= 0);
            //point* c = t->points[TRAP[i+1]];
            point* d = t->points[TRAP[i + 2]];
            //assert(c);
            assert(d);

            trs.push_back(t);
            t = t->neighbor[i];
            assert(t);

            int j = index_of(t, d);
            assert(j >= 0);
            j = TRAP[j + 2]; //influence edge
            while (t)
            {
                trs.push_back(t);
                int k = get_next_edge_influence_triangle(t, j, a, b);
                if (k < 0) break;

                triangle* t_next = t->neighbor[k];
                assert(t_next);

                if (k == TRAP[j + 1])
                { //right
                    j = index_of(t_next, t->points[TRAP[j + 2]]);
                    assert(j >= 0);
                    j = TRAP[j + 1];
                }
                else
                { //left
                    assert(k == TRAP[j + 2]);
                    j = index_of(t_next, t->points[TRAP[j + 0]]);
                    assert(j >= 0);
                    j = TRAP[j + 1];
                }
                t = t_next;
            }

            assert(trs.size() >= 2);
            assert(index_of(trs.front(), a) >= 0);
            assert(index_of(trs.back(), b) >= 0);

            replace_edge(trs, a, b, tris);
        }
    }

    static bool test_bound(const vector2& min, const vector2& max, const vector2& p)
    {
        for (int i = 0; i < 2; i++)
        {
            if (p[i] < min[i]) return false;
            if (p[i] > max[i]) return false;
        }
        return true;
    }

    namespace
    {
        struct point_sorter
        {
            bool operator()(point* a, point* b) const
            {
                return a->p_edge->index < b->p_edge->index;
            }
        };
    }
    static bool check_edge(point* a, point* b, point* c)
    {

        if (!a->p_edge) return true;
        if (!b->p_edge) return true;
        if (!c->p_edge) return true;

        int ga = a->p_edge->group;
        int gb = b->p_edge->group;
        int gc = c->p_edge->group;

        if (ga != gb || gb != gc || gc != ga) return true;

        point* points[3] = {a, b, c};
        std::sort(points, points + 3, point_sorter());
        if (calc_determinant(points[0]->p, points[1]->p, points[2]->p) != ORIENTATION_CCW) return false;
        return true;
    }

    static size_t get_total_points(const std::vector<std::vector<vector2> >& loops)
    {
        size_t nRet = 0;
        for (size_t i = 0; i < loops.size(); i++)
        {
            nRet += loops[i].size();
        }
        return nRet;
    }

    static bool triangulate_polygon_delaunay(
        delaunay_mesh& out,
        const std::vector<vector2>& loop,
        const std::vector<std::vector<vector2> >& inner_loops,
        const std::vector<vector2>& samples)
    {
        size_t osz = loop.size();
        size_t isz = get_total_points(inner_loops);
        size_t ssz = samples.size();

        size_t esz = osz + isz;
        size_t psz = osz + isz + ssz;
        //--------------------------
        std::vector<point*> points;
        points.reserve(psz);
        std::vector<edge*> edges;
        edges.reserve(esz);
        //--------------------------

        //--------------------------
        //make_outer loop
        {
            std::vector<point*> opoints(osz);
            for (size_t i = 0; i < osz; i++)
            {
                opoints[i] = new point(loop[i]);
            }
            std::vector<edge*> oedges(osz);
            for (size_t i = 0; i < osz - 1; i++)
            {
                oedges[i] = new edge(opoints[i], opoints[i + 1], 0, i);
                opoints[i]->p_edge = oedges[i];
            }
            oedges[osz - 1] = new edge(opoints[osz - 1], opoints[0], 0, osz - 1);
            opoints[osz - 1]->p_edge = oedges[osz - 1];

            //--------------------------
            for (size_t i = 0; i < osz; i++)
            {
                points.push_back(opoints[i]);
                edges.push_back(oedges[i]);
            }
            //--------------------------
        }
        //--------------------------
        {
            std::vector<std::vector<point*> > ipoints(inner_loops.size());
            for (size_t i = 0; i < inner_loops.size(); i++)
            {
                for (size_t j = 0; j < inner_loops[i].size(); j++)
                {
                    ipoints[i].push_back(new point(inner_loops[i][j]));
                }
            }
            std::vector<std::vector<edge*> > iedges(inner_loops.size());
            for (int i = 0; i < inner_loops.size(); i++)
            {
                size_t ilpsz = inner_loops[i].size();
                for (size_t j = 0; j < ilpsz - 1; j++)
                {
                    iedges[i].push_back(new edge(ipoints[i][j], ipoints[i][j + 1], i + 1, j));
                    ipoints[i][j]->p_edge = iedges[i][j];
                }
                iedges[i].push_back(new edge(ipoints[i][ilpsz - 1], ipoints[i][0], i + 1, ilpsz - 1));
                ipoints[i][ilpsz - 1]->p_edge = iedges[i][ilpsz - 1];
            }

            //--------------------------
            for (size_t i = 0; i < inner_loops.size(); i++)
            {
                size_t ilpsz = inner_loops[i].size();
                for (size_t j = 0; j < ilpsz; j++)
                {
                    points.push_back(ipoints[i][j]);
                    edges.push_back(iedges[i][j]);
                }
            }
            //--------------------------
        }
        //--------------------------
        {
            for (size_t i = 0; i < ssz; i++)
            {
                points.push_back(new point(samples[i]));
            }
        }
        //--------------------------

        vector2 min;
        vector2 max;
        get_minmax(min, max, loop);

        vector2 wid = max - min;
        vector2 cnt = (min + max) * 0.5;
        real M = std::max<real>(wid[0], wid[1]) * 0.6;
        vector2 global_tmp[3] =
            {
                vector2(3 * M, 0) + cnt,
                vector2(0, 3 * M) + cnt,
                vector2(-3 * M, -3 * M) + cnt};
        std::vector<point*> global_points(3);
        for (int i = 0; i < 3; i++)
        {
            global_points[i] = new point(global_tmp[i]);
        }
        //--------------------------
        std::vector<triangle*> triangles;
        triangles.reserve(psz * 3);
        triangle* pGlobalTri = new triangle(global_points[0], global_points[1], global_points[2]);
        pGlobalTri->edge_type[0] = EDGE_GLOBAL;
        pGlobalTri->edge_type[1] = EDGE_GLOBAL;
        pGlobalTri->edge_type[2] = EDGE_GLOBAL;
        triangles.push_back(pGlobalTri);
        //--------------------------
        //Create chain
        for (size_t i = 0; i < psz - 1; i++)
        {
            points[i]->p_next = points[i + 1];
        }
        //--------------------------
        for (size_t i = 0; i < psz; i++)
        {
            points[i]->p_tri = pGlobalTri;
        }
        pGlobalTri->inners = points[0];
        //--------------------------
        std::vector<size_t> point_order(psz);
        for (size_t i = 0; i < psz; i++)
        {
            point_order[i] = i;
        }
        std::random_shuffle(point_order.begin(), point_order.end());
        //--------------------------
        //insert points
        for (size_t i = 0; i < psz; i++)
        {
            size_t k = point_order[i];
            triangle* pTri = points[k]->p_tri;
            create_triangle(pTri, points[k], triangles);
        }
        //--------------------------
        //insert edges
        for (size_t i = 0; i < esz; i++)
        {
            insert_edge(edges[i], triangles); //BUGS
        }
        //--------------------------
        for (size_t i = 0; i < psz; i++)
        {
            points[i]->index = 0;
        }

        std::vector<triangle*> lived_triangles;
        lived_triangles.reserve(psz * 3);
        size_t tsz = triangles.size();
        for (size_t i = 0; i < tsz; i++)
        {
            triangle* pTri = triangles[i];
            if (pTri->edge_type[0] != EDGE_NODE)
            {
                if (pTri->edge_type[0] != EDGE_GLOBAL && pTri->edge_type[1] != EDGE_GLOBAL && pTri->edge_type[2] != EDGE_GLOBAL)
                {
                    if (test_bound(min, max, pTri->points[0]->p) && test_bound(min, max, pTri->points[1]->p) && test_bound(min, max, pTri->points[2]->p))
                    {
                        if (pTri->edge_type[0] != EDGE_CONSTRAINED_REV && pTri->edge_type[1] != EDGE_CONSTRAINED_REV && pTri->edge_type[2] != EDGE_CONSTRAINED_REV)
                        {
                            if (check_edge(pTri->points[0], pTri->points[1], pTri->points[2]))
                            {
                                pTri->points[0]->index = 1;
                                pTri->points[1]->index = 1;
                                pTri->points[2]->index = 1;
                                lived_triangles.push_back(pTri);
                            }
                        }
                    }
                }
            }
            pTri->index = (size_t)-1;
        }
        std::vector<point*> lived_points;
        lived_points.reserve(psz);
        for (size_t i = 0; i < psz; i++)
        {
            if (points[i]->index)
            {
                points[i]->index = lived_points.size();
            }
            lived_points.push_back(points[i]);
        }
        size_t ltsz = lived_triangles.size();
        size_t lpsz = lived_points.size();

        for (size_t i = 0; i < ltsz; i++)
        {
            lived_triangles[i]->index = i;
        }
        //--------------------------
        std::vector<vector2> tpoints(lpsz);
        std::vector<delaunay_triangle> ttriangles(ltsz);
        for (size_t i = 0; i < ltsz; i++)
        {
            triangle* pTri = lived_triangles[i];
            ttriangles[i].points[0] = pTri->points[0]->index;
            ttriangles[i].points[1] = pTri->points[1]->index;
            ttriangles[i].points[2] = pTri->points[2]->index;
            ttriangles[i].neighbor[0] = (pTri->neighbor[0]) ? pTri->neighbor[0]->index : (size_t)-1;
            ttriangles[i].neighbor[1] = (pTri->neighbor[1]) ? pTri->neighbor[1]->index : (size_t)-1;
            ttriangles[i].neighbor[2] = (pTri->neighbor[2]) ? pTri->neighbor[2]->index : (size_t)-1;
            ttriangles[i].edge_type[0] = pTri->edge_type[0];
            ttriangles[i].edge_type[1] = pTri->edge_type[1];
            ttriangles[i].edge_type[2] = pTri->edge_type[2];
        }
        for (size_t i = 0; i < lpsz; i++)
        {
            tpoints[i] = lived_points[i]->p;
        }
        //--------------------------

        out.points.swap(tpoints);
        out.trinagles.swap(ttriangles);

        //--------------------------
        for (size_t i = 0; i < psz; i++)
        {
            delete points[i];
        }
        for (size_t i = 0; i < esz; i++)
        {
            delete edges[i];
        }
        for (int i = 0; i < 3; i++)
        {
            delete global_points[i];
        }
        for (size_t i = 0; i < tsz; i++)
        {
            delete triangles[i];
        }
        //--------------------------
        return true;
    }

    bool triangulate_polygon_delaunay(
        std::vector<vector2>& out,
        const std::vector<vector2>& outer_loop)
    {
        std::vector<std::vector<vector2> > inner_loops;
        std::vector<vector2> pins;

        delaunay_mesh m;
        if (!triangulate_polygon_delaunay(m, outer_loop, inner_loops, pins)) return false;

        size_t tsz = m.trinagles.size();
        out.resize(tsz * 3);
        for (size_t i = 0; i < tsz; i++)
        {
            size_t i0 = m.trinagles[i].points[0];
            size_t i1 = m.trinagles[i].points[1];
            size_t i2 = m.trinagles[i].points[2];
            out[3 * i + 0] = m.points[i0];
            out[3 * i + 1] = m.points[i1];
            out[3 * i + 2] = m.points[i2];
        }

        return true;
    }

    bool triangulate_polygon_delaunay(
        std::vector<vector2>& out,
        const std::vector<vector2>& outer_loop,
        const std::vector<vector2>& pins)
    {
        std::vector<std::vector<vector2> > inner_loops;
        //std::vector<vector2> pins;

        delaunay_mesh m;
        if (!triangulate_polygon_delaunay(m, outer_loop, inner_loops, pins)) return false;

        size_t tsz = m.trinagles.size();
        out.resize(tsz * 3);
        for (size_t i = 0; i < tsz; i++)
        {
            size_t i0 = m.trinagles[i].points[0];
            size_t i1 = m.trinagles[i].points[1];
            size_t i2 = m.trinagles[i].points[2];
            out[3 * i + 0] = m.points[i0];
            out[3 * i + 1] = m.points[i1];
            out[3 * i + 2] = m.points[i2];
        }

        return true;
    }

    bool triangulate_polygon_delaunay(
        std::vector<vector2>& out,
        const std::vector<vector2>& outer_loop,
        const std::vector<std::vector<vector2> >& inner_loops,
        const std::vector<vector2>& pins)
    {
        //std::vector< std::vector<vector2> > inner_loops;
        //std::vector<vector2> pins;

        delaunay_mesh m;
        if (!triangulate_polygon_delaunay(m, outer_loop, inner_loops, pins)) return false;

        size_t tsz = m.trinagles.size();
        out.resize(tsz * 3);
        for (size_t i = 0; i < tsz; i++)
        {
            size_t i0 = m.trinagles[i].points[0];
            size_t i1 = m.trinagles[i].points[1];
            size_t i2 = m.trinagles[i].points[2];
            out[3 * i + 0] = m.points[i0];
            out[3 * i + 1] = m.points[i1];
            out[3 * i + 2] = m.points[i2];
        }

        return true;
    }
}
