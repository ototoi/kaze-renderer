#include "triangulate_polygon_earclip.h"
#include "values.h"

#include <cmath>

namespace kaze
{

    enum
    {
        ORIENTATION_CW = 0,
        ORIENTATION_CCW,
        ORIENTATION_UNKNOWN
    };

    static double det(double a, double b, double c, double d)
    {
        return a * d - b * c;
    }

    static int calc_determinant(const vector2& a, const vector2& b, const vector2& c)
    {
        vector2 ab = b - a;
        vector2 ac = c - a;
        double Determ = det(ab[0], ac[0], ab[1], ac[1]); //ad-bc

        if (Determ > 0)
        {
            return ORIENTATION_CCW;
        }
        else if (Determ < 0)
        {
            return ORIENTATION_CW;
        }
        else
        {
            return ORIENTATION_UNKNOWN;
        }
    }

    //calc angle B
    static double calc_angle(const vector2& a, const vector2& b, const vector2& c)
    {
        int nDet = calc_determinant(a, b, c);
        if (nDet == ORIENTATION_UNKNOWN) return 0;

        vector2 ba = (a - b);
        ba = normalize(ba);
        vector2 bc = (c - b);
        bc = normalize(bc);

        double cc = -dot(ba, bc);
        double cos_m = acos(cc);
        if (nDet == ORIENTATION_CCW)
        {
            return cos_m;
        }
        else
        {
            return -cos_m;
        }
    }

    static bool is_regular(int i1, int i2, int i3, const std::vector<vector2>& v)
    {
        int t1 = calc_determinant(v[i1], v[i2], v[i3]);
        return t1 == ORIENTATION_CCW; //
    }

    static bool is_outside(int i1, int i2, int i3, const std::vector<int>& iList, const std::vector<vector2>& v)
    {
        int sz = (int)iList.size();
        for (int i = 0; i < sz; i++)
        {
            int iN = iList[i];

            // Ignore the vertices which make up the specified triangle
            if ((iN == i1) || (iN == i2) || (iN == i3))
                continue;

            if (is_regular(i2, i1, iN, v) || is_regular(i1, i3, iN, v) || is_regular(i3, i2, iN, v))
            {
                continue;
            }
            else
            {
                // If it is coincident with one of the vertices, then presume it is inside.
                if (
                    (v[iN] == v[i1]) ||
                    (v[iN] == v[i2]) ||
                    (v[iN] == v[i3]))
                {
                    continue;
                }
                else
                {
                    return false;
                }
            }
        }
        return true;
    }

    static double get_cos(int i1, int i2, int i3, const std::vector<vector2>& v)
    {
        vector2 l1 = v[i1] - v[i2];
        vector2 l3 = v[i3] - v[i2];
#if 0
    real ll1 = l1.length();
    real ll3 = l3.length();
    assert(ll1>0);
    assert(ll3>0);

    if(ll1<=0)return 0;
    if(ll3<=0)return 0;

    return dot(l1,l3)/(ll1*ll3);
#else
        //return calc_angle(v[i1],v[i1],v[i1]);
        return fabs(det(l1[0], l3[0], l1[1], l3[1])); //ad-bc
//return dot(l1,l3);
#endif
    }

    static bool triangulate_indices(std::vector<int>& iv, const std::vector<vector2>& lv)
    {
        int sz = (int)lv.size();
        std::vector<int> indices(sz);
        for (int i = 0; i < sz; i++)
            indices[i] = i;

        int csz = sz;
        while (csz >= 3)
        {
            int nFind = -1;
            double dCos = -values::far();
            //find cut position
            for (int i = 0; i < csz; i++)
            {
                int iCurr = i;
                int iPrev = iCurr - 1;
                int iNext = iCurr + 1;

                if (iCurr == 0)
                {
                    iPrev = csz - 1;
                }
                else if (iCurr == csz - 1)
                {
                    iNext = 0;
                }

                bool bRegular = is_regular(indices[iPrev], indices[iCurr], indices[iNext], lv);
                bool bOutSide = is_outside(indices[iPrev], indices[iCurr], indices[iNext], indices, lv);
                if (bRegular && bOutSide)
                { //  bRegular && bOutSide
                    nFind = iCurr;
                    break;
                    /*
          double dd = get_cos( indices[ iPrev ], indices[ iCurr ], indices[ iNext ], lv);//TODO:use cache.
          if(dCos<dd){
            nFind = iCurr;
            dCos = dd;
          }
          */
                }
            }
            //find cut position
            if (nFind < 0)
            {
                return true;
            }
            else
            { //find
                int iCurr = nFind;
                int iPrev = iCurr - 1;
                int iNext = iCurr + 1;

                if (iCurr == 0)
                {
                    iPrev = csz - 1;
                }
                else if (iCurr == csz - 1)
                {
                    iNext = 0;
                }

                iv.push_back(indices[iPrev]);
                iv.push_back(indices[iCurr]);
                iv.push_back(indices[iNext]);

                indices.erase(indices.begin() + iCurr);
                csz = (int)indices.size();
            }
        } //csz>3
        return true;
    }

    static bool triangulate_internal(std::vector<vector2>& tv, const std::vector<vector2>& lv)
    {
        std::vector<int> indices;
        if (triangulate_indices(indices, lv))
        {
            size_t isz = indices.size();
            tv.reserve(tv.size() + isz);
            for (size_t i = 0; i < isz; i++)
            {
                tv.push_back(lv[indices[i]]);
            }
            return true;
        }
        return false;
    }

    bool triangulate_polygon_earclip(std::vector<vector2>& tri, const std::vector<vector2>& loop)
    {
        return triangulate_internal(tri, loop);
    }
}