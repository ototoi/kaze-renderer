#ifndef KAZE_sample_patch_parameter_H
#define KAZE_sample_patch_parameter_H

#include "bezier_curve.hpp"
#include "multi_bezier_curve.hpp"
#include "bezier_patch.hpp"
#include "multi_bezier_patch.hpp"

namespace kaze
{
#define DEF_SAMPLE_PARAM(CURVE) \
    void sample_patch_parameter(std::vector<real>& out, const CURVE& curve, const std::vector<real>& params, real distance);

    DEF_SAMPLE_PARAM(bezier_curve<vector2>)
    DEF_SAMPLE_PARAM(bezier_curve<vector3>)
    DEF_SAMPLE_PARAM(multi_bezier_curve<vector2>)
    DEF_SAMPLE_PARAM(multi_bezier_curve<vector3>)

#undef DEF_SAMPLE_PARAM

#define DEF_SAMPLE_PARAM(CURVE, PATCH) \
    void sample_patch_parameter(std::vector<real>& out, const CURVE& curve, const PATCH& patch, const std::vector<real>& params, real distance);

    DEF_SAMPLE_PARAM(bezier_curve<vector2>, bezier_patch<vector2>)
    DEF_SAMPLE_PARAM(bezier_curve<vector2>, bezier_patch<vector3>)
    DEF_SAMPLE_PARAM(multi_bezier_curve<vector2>, multi_bezier_patch<vector2>)
    DEF_SAMPLE_PARAM(multi_bezier_curve<vector2>, multi_bezier_patch<vector3>)

#undef DEF_SAMPLE_PARAM

#define DEF_SAMPLE_PARAM(CURVE, PATCH) \
    void sample_patch_parameter(std::vector<real>& out, const CURVE& curve, const PATCH& patch, const std::vector<real>& params, real distance, real angle);

    DEF_SAMPLE_PARAM(bezier_curve<vector2>, bezier_patch<vector3>)
    DEF_SAMPLE_PARAM(multi_bezier_curve<vector2>, multi_bezier_patch<vector3>)

#undef DEF_SAMPLE_PARAM

#define DEF_SAMPLE_PARAM(PATCH) \
    void sample_patch_parameter(std::vector<vector2>& out, const PATCH& patch, real distance, real angle);

    DEF_SAMPLE_PARAM(bezier_patch<vector3>)
    DEF_SAMPLE_PARAM(multi_bezier_patch<vector3>)

#undef DEF_SAMPLE_PARAM

    std::vector<real> get_valid_knots_v(const multi_bezier_patch<vector3>& patch, real distance);
    std::vector<real> get_valid_knots_u(const multi_bezier_patch<vector3>& patch, real distance);
    std::vector<real> get_valid_knots(const multi_bezier_curve<vector2>& curve, const multi_bezier_patch<vector3>& patch, real distance);

    /*knotted -> not knotted*/
    void reverse_knots_transform(std::vector<vector2>& points, const std::vector<real>& uknots, const std::vector<real>& vknots, int nu, int nv);
    /*not knotted -> knotted*/
    void knots_transform(std::vector<vector2>& points, const std::vector<real>& uknots, const std::vector<real>& vknots, int nu, int nv);
}

#endif