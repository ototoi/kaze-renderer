#include "bspline_to_bezier.h"

namespace kaze
{

    template <class T>
    static void bspline_to_bezier_(std::vector<bezier_curve<T> >& vecBez, std::vector<real>& knots, const bspline_curve<T>& bsp)
    {
        bspline_curve<T> tmp(bsp);
        tmp.to_bezier_type();

        assert(tmp.is_bezier_type());

        int nOrder = tmp.order();
        int nCp = tmp.get_cp_size();
        int nSegment = (nCp - 1) / (nOrder - 1);
        int nKnot = nSegment + 1;
        vecBez.reserve(nSegment);
        knots.reserve(nKnot);
        for (int i = 0; i < nSegment; i++)
        {
            std::vector<T> cps(nOrder);
            for (int k = 0; k < nOrder; k++)
            {
                cps[k] = tmp.get_cp_at(i * (nOrder - 1) + k);
            }
            vecBez.push_back(bezier_curve<T>(cps));
        }
        for (int i = 0; i < nKnot; i++)
        {
            knots.push_back(tmp.get_knot_at(1 + (nOrder - 1) * i));
        }
    }

    template <class T>
    static void bspline_to_bezier_(std::vector<bezier_patch<T> >& vecBez, std::vector<real>& uknots, std::vector<real>& vknots, const bspline_patch<T>& bsp)
    {
        bspline_patch<T> tmp(bsp);
        tmp.to_bezier_type();

        assert(tmp.is_bezier_type());

        int ou = tmp.order_u();
        int ov = tmp.order_v();
        int nu = tmp.get_nu();
        int nv = tmp.get_nv();
        int nPu = (nu - 1) / (ou - 1);
        int nPv = (nv - 1) / (ov - 1);
        int nKnots_u = nPu + 1;
        int nKnots_v = nPv + 1;
        vecBez.reserve(nPu * nPv);
        uknots.reserve(nKnots_u);
        vknots.reserve(nKnots_v);

        for (int j = 0; j < nPv; j++)
        {
            for (int i = 0; i < nPu; i++)
            {
                std::vector<T> cps(ou * ov); //order
                for (int l = 0; l < ov; l++)
                {
                    for (int k = 0; k < ou; k++)
                    {
                        int ui = i * (ou - 1) + k;
                        int vi = j * (ov - 1) + l;
                        cps[l * ou + k] = tmp.get_cp_at(ui, vi);
                    }
                }
                vecBez.push_back(bezier_patch<T>(ou, ov, cps));
            }
        }

        for (int i = 0; i < nKnots_u; i++)
        {
            uknots.push_back(tmp.get_knot_at_u(1 + (ou - 1) * i));
        }
        for (int i = 0; i < nKnots_v; i++)
        {
            vknots.push_back(tmp.get_knot_at_v(1 + (ov - 1) * i));
        }
    }

    ////////////////////////////////////////////////////////////////////////////////////////

    template <class T>
    static void bspline_to_bezier_(multi_bezier_curve<T>& bzr, const bspline_curve<T>& bsp)
    {
        std::vector<bezier_curve<T> > vecBez;
        std::vector<real> knots;
        bspline_to_bezier_(vecBez, knots, bsp);
        multi_bezier_curve<T> tmp(vecBez, knots);
        bzr.swap(tmp);

        assert(bzr.order() == bsp.order());
    }

    template <class T>
    static void bspline_to_bezier_(multi_bezier_patch<T>& bzr, const bspline_patch<T>& bsp)
    {
        std::vector<bezier_patch<T> > vecBez;
        std::vector<real> uknots;
        std::vector<real> vknots;
        bspline_to_bezier_(vecBez, uknots, vknots, bsp);
        multi_bezier_patch<T> tmp(vecBez, uknots, vknots);
        bzr.swap(tmp);
    }

#define DEC_BSP_TO_BZR(TYPE)                                                              \
    void bspline_to_bezier(multi_bezier_curve<TYPE>& bzr, const bspline_curve<TYPE>& bsp) \
    {                                                                                     \
        bspline_to_bezier_(bzr, bsp);                                                     \
    }                                                                                     \
    void bspline_to_bezier(multi_bezier_patch<TYPE>& bzr, const bspline_patch<TYPE>& bsp) \
    {                                                                                     \
        bspline_to_bezier_(bzr, bsp);                                                     \
    }

    DEC_BSP_TO_BZR(real)
    DEC_BSP_TO_BZR(vector2)
    DEC_BSP_TO_BZR(vector3)

#undef DEC_BSP_TO_BZR
}
