#ifndef KAZE_INTERSECT_LINES_H
#define KAZE_INTERSECT_LINES_H

#include "../core/types.h"
#include <vector>

namespace kaze
{

    void intersect_loop(std::vector<vector2>& out, const std::vector<vector2>& v);
    void intersect_loop(std::vector<vector2>& inout);

    void intersect_loop(std::vector<std::vector<vector2> >& out, const std::vector<std::vector<vector2> >& loops);
    void intersect_loop(std::vector<std::vector<vector2> >& inout);

    void intersect_lines(std::vector<std::pair<vector2, vector2> >& out, const std::vector<std::pair<vector2, vector2> >& v);
    void intersect_lines(std::vector<std::pair<vector2, vector2> >& inout);
}

#endif