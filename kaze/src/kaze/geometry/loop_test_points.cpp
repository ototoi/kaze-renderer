#include "loop_test_points.h"

#include <vector>
#include <utility>
#include <cmath>
#include <algorithm>
#include <set>

#include "values.h"

namespace kaze
{
    namespace
    {
        static const real EPSILON = values::epsilon() * 4096;

        enum EVENT_TYPE
        {
            EDGE_START,
            EDGE_END,
            POINT
        };

        static double get_intersection(const vector2& a, const vector2& b, double v)
        {
            double k = (v - a[1]) / (b[1] - a[1]);
            //assert(0<=k&&k<=1);
            double u = (1 - k) * a[0] + k * b[0];

            return u;
        }

        struct edge
        {
            edge()
            {
                p[0] = NULL;
                p[1] = NULL;
            }
            edge(const vector2* a, const vector2* b)
            {
                p[0] = a;
                p[1] = b;
            }

            const vector2* p[2];

            real sweep(real y) const
            {
                return get_intersection(*p[0], *p[1], y);
            }
        };

        struct edge_event
        {
            EVENT_TYPE type;
            vector2 p;
            size_t index;
            edge* e;
        };

        struct event_sorter
        {
            bool operator()(const edge_event& a, const edge_event& b)
            {
                if (a.p[1] != b.p[1])
                {
                    return a.p[1] > b.p[1];
                }
                else
                {
                    if (a.p[0] != b.p[0])
                    {
                        return a.p[0] < b.p[0];
                    }
                    else
                    {
                        return (int)a.type < (int)b.type;
                    }
                }
            }
        };

        typedef std::set<edge*> stage_set;
        typedef stage_set::iterator stage_iter;

        bool y_check(edge* e, real y)
        {
            real ys[2];
            ys[0] = (*e->p[0])[1];
            ys[1] = (*e->p[1])[1];
            if (ys[0] > ys[1]) std::swap(ys[0], ys[1]);
            return (ys[0] <= y && y <= ys[1]);
        }

        static void get_minmax(vector2& min, vector2& max, const std::vector<vector2>& a)
        {
            size_t asz = a.size();
            min = a[0];
            max = a[0];
            for (size_t i = 0; i < asz; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (min[j] > a[i][j]) min[j] = a[i][j];
                    if (max[j] < a[i][j]) max[j] = a[i][j];
                }
            }
        }

        static bool test_bound(const vector2& min, const vector2& max, const vector2& p)
        {
            if (min[0] < p[0] && p[0] < max[0])
            {
                if (min[1] < p[1] && p[1] < max[1])
                {
                    return true;
                }
            }
            return false;
        }

        bool loop_inner_points_innernal(std::vector<vector2>& out, const std::vector<vector2>& loop, const std::vector<vector2>& points)
        {
            if (points.empty()) return true;

            vector2 min, max;
            get_minmax(min, max, loop);

            std::vector<edge> edges;
            size_t lsz = loop.size();
            edges.reserve(lsz);
            for (size_t i = 0; i < lsz; i++)
            {
                edge e(&loop[i], &loop[(i == lsz - 1) ? 0 : i + 1]);
                edges.push_back(e);
            }

            size_t psz = points.size();

            //size_t hID = 0;
            std::vector<edge_event> events;
            events.reserve(lsz * 2 + psz);
            for (size_t i = 0; i < lsz; i++)
            {
                edge* e = &edges[i];
                vector2 p0 = *(e->p[0]);
                vector2 p1 = *(e->p[1]);
                real dY = std::fabs(p0[1] - p1[1]);
                real dX = std::fabs(p0[0] - p1[0]);

                if (dY <= EPSILON || (dX >= EPSILON && dY / dX <= EPSILON))
                {
                    //
                }
                else
                {
                    if (p0[1] < p1[1]) std::swap(p0, p1);

                    edge_event ev0;
                    ev0.type = EDGE_START;
                    ev0.e = e;
                    ev0.p = p0;

                    edge_event ev1;
                    ev1.type = EDGE_END;
                    ev1.e = e;
                    ev1.p = p1;

                    events.push_back(ev0);
                    events.push_back(ev1);
                }
            }
            for (size_t i = 0; i < psz; i++)
            {
                if (test_bound(min, max, points[i]))
                {
                    edge_event ev;
                    ev.type = POINT;
                    ev.e = NULL;
                    ev.p = points[i];
                    ev.index = i;
                    events.push_back(ev);
                }
            }
            //
            std::sort(events.begin(), events.end(), event_sorter());
            //
            std::vector<bool> bchecks(psz, false);

            std::vector<real> ys;

            stage_set stg;
            size_t evsz = events.size();
            for (size_t i = 0; i < evsz; i++)
            {
                edge_event& ev = events[i];
                switch (ev.type)
                {
                case EDGE_START:
                {
                    stg.insert(ev.e);
                }
                break;
                case EDGE_END:
                {
                    stg.erase(ev.e);
                }
                break;
                case POINT:
                {
                    if (!stg.empty())
                    {
                        ys.clear();
                        for (stage_iter iter = stg.begin(); iter != stg.end(); iter++)
                        {
                            edge* e = *iter;
                            if (y_check(e, ev.p[1]))
                            {
                                ys.push_back(e->sweep(ev.p[1]));
                            }
                        }
                        if (!ys.empty())
                        {
                            std::sort(ys.begin(), ys.end());
                            int ysz = (int)ys.size();
                            real x = ev.p[0];
                            if (ysz >= 2)
                            {
                                for (int j = 0; j < ysz - 1; j += 2)
                                {
                                    if (ys[j] + EPSILON < x && x < ys[j + 1] - EPSILON)
                                    {
                                        bchecks[ev.index] = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
                }
            }

            out.reserve(psz);
            for (size_t i = 0; i < psz; i++)
            {
                if (bchecks[i])
                {
                    out.push_back(points[i]);
                }
            }

            return true;
        }

        bool loop_outer_points_innernal(std::vector<vector2>& out, const std::vector<vector2>& loop, const std::vector<vector2>& points)
        {
            if (points.empty()) return true;

            vector2 min, max;
            get_minmax(min, max, loop);

            std::vector<edge> edges;
            size_t lsz = loop.size();
            edges.reserve(lsz);
            for (size_t i = 0; i < lsz; i++)
            {
                edge e(&loop[i], &loop[(i == lsz - 1) ? 0 : i + 1]);
                edges.push_back(e);
            }

            size_t psz = points.size();

            //size_t hID = 0;
            std::vector<edge_event> events;
            events.reserve(lsz * 2 + psz);
            for (size_t i = 0; i < lsz; i++)
            {
                edge* e = &edges[i];
                vector2 p0 = *(e->p[0]);
                vector2 p1 = *(e->p[1]);
                real dY = std::fabs(p0[1] - p1[1]);
                real dX = std::fabs(p0[0] - p1[0]);

                if (dY <= EPSILON || (dX >= EPSILON && dY / dX <= EPSILON))
                {
                    //
                }
                else
                {
                    if (p0[1] < p1[1]) std::swap(p0, p1);

                    edge_event ev0;
                    ev0.type = EDGE_START;
                    ev0.e = e;
                    ev0.p = p0;

                    edge_event ev1;
                    ev1.type = EDGE_END;
                    ev1.e = e;
                    ev1.p = p1;

                    events.push_back(ev0);
                    events.push_back(ev1);
                }
            }
            for (size_t i = 0; i < psz; i++)
            {
                if (test_bound(min, max, points[i]))
                {
                    edge_event ev;
                    ev.type = POINT;
                    ev.e = NULL;
                    ev.p = points[i];
                    ev.index = i;
                    events.push_back(ev);
                }
            }
            //
            std::sort(events.begin(), events.end(), event_sorter());
            //
            std::vector<bool> bchecks(psz, true); //true

            std::vector<real> ys;

            stage_set stg;
            size_t evsz = events.size();
            for (size_t i = 0; i < evsz; i++)
            {
                edge_event& ev = events[i];
                switch (ev.type)
                {
                case EDGE_START:
                {
                    stg.insert(ev.e);
                }
                break;
                case EDGE_END:
                {
                    stg.erase(ev.e);
                }
                break;
                case POINT:
                {
                    if (!stg.empty())
                    {
                        ys.clear();
                        for (stage_iter iter = stg.begin(); iter != stg.end(); iter++)
                        {
                            edge* e = *iter;
                            if (y_check(e, ev.p[1]))
                            {
                                ys.push_back(e->sweep(ev.p[1]));
                            }
                        }
                        if (!ys.empty())
                        {
                            std::sort(ys.begin(), ys.end());
                            int ysz = (int)ys.size();
                            real x = ev.p[0];
                            if (ysz >= 2)
                            {
                                for (int j = 0; j < ysz - 1; j += 2)
                                {
                                    if (ys[j] - EPSILON <= x && x <= ys[j + 1] + EPSILON)
                                    {
                                        bchecks[ev.index] = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
                }
            }

            out.reserve(psz);
            for (size_t i = 0; i < psz; i++)
            {
                if (bchecks[i])
                {
                    out.push_back(points[i]);
                }
            }

            return true;
        }

        //------------------------------------------------------------------------------------

        struct kdt
        {
            vector2 p;
            char plane;
        };

        struct kdt_sorter
        {
            kdt_sorter(int pl) : plane(pl) {}

            bool operator()(const kdt& a, const kdt& b) const
            {
                return a.p[plane] < b.p[plane];
            }
            int plane;
        };

        void sort_kdtree(std::vector<kdt>& v, size_t a, size_t b, vector2& min, vector2& max)
        {
            size_t sz = b - a;
            if (sz <= 1) return;

            vector2 wid = max - min;

            int plane = (wid[0] < wid[1]) ? 1 : 0;

            std::sort(v.begin() + a, v.begin() + b, kdt_sorter(plane));

            size_t c = a + ((b - a) >> 1);
            v[c].plane = plane;

            if (sz <= 3) return;

            real k = v[c].p[plane];

            {
                vector2 cmin = min;
                vector2 cmax = max;
                cmax[plane] = k;
                sort_kdtree(v, a, c, cmin, cmax);
            }
            {
                vector2 cmin = min;
                cmin[plane] = k;
                vector2 cmax = max;
                sort_kdtree(v, c + 1, b, cmin, cmax);
            }
        }

        bool find_kdtree(const std::vector<kdt>& v, size_t a, size_t b, const vector2& p, real dist2)
        {
            size_t sz = b - a;
            if (!sz) return false;
            size_t c = a + ((b - a) >> 1);

            vector2 dif = v[c].p - p;
            if (dif.sqr_length() <= dist2) return true;

            int plane = v[c].plane;

            if (p[plane] < v[c].p[plane])
            {
                if (find_kdtree(v, a, c, p, dist2)) return true;
                real d2 = v[c].p[plane] - p[plane];
                d2 *= d2;
                if (d2 <= dist2)
                {
                    if (find_kdtree(v, c + 1, b, p, dist2)) return true;
                }
            }
            else
            {
                if (find_kdtree(v, c + 1, b, p, dist2)) return true;
                real d2 = v[c].p[plane] - p[plane];
                d2 *= d2;
                if (d2 <= dist2)
                {
                    if (find_kdtree(v, a, c, p, dist2)) return true;
                }
            }

            return false;
        }

        bool find_kdtree(const std::vector<kdt>& v, const vector2& p, real dist2)
        {
            size_t sz = v.size();
            if (!sz) return false;
            if (find_kdtree(v, 0, sz, p, dist2)) return true;
            return false;
        }

        bool check_unique_points(const std::vector<vector2>& a, const std::vector<vector2>& b, real dist2)
        {
            size_t asz = a.size();
            std::vector<kdt> tmp(asz);
            for (size_t i = 0; i < asz; i++)
            {
                tmp[i].p = a[i];
                tmp[i].plane = 0;
            }
            vector2 min = a[0];
            vector2 max = a[0];
            for (size_t i = 0; i < asz; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (min[j] > a[i][j]) min[j] = a[i][j];
                    if (max[j] < a[i][j]) max[j] = a[i][j];
                }
            }
            sort_kdtree(tmp, 0, asz, min, max);

            size_t bsz = b.size();
            for (size_t i = 0; i < bsz; i++)
            {
                if (find_kdtree(tmp, b[i], dist2)) return false;
            }

            return true;
        }

        bool make_unique_points(std::vector<vector2>& out, const std::vector<vector2>& a, const std::vector<vector2>& b, real dist)
        {
            size_t asz = a.size();
            std::vector<kdt> tmp(asz);
            for (size_t i = 0; i < asz; i++)
            {
                tmp[i].p = a[i];
                tmp[i].plane = 0;
            }
            vector2 min = a[0];
            vector2 max = a[0];
            for (size_t i = 0; i < asz; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (min[j] > a[i][j]) min[j] = a[i][j];
                    if (max[j] < a[i][j]) max[j] = a[i][j];
                }
            }
            sort_kdtree(tmp, 0, asz, min, max);

            size_t bsz = b.size();
            out.reserve(bsz);
            for (size_t i = 0; i < bsz; i++)
            {
                if (!find_kdtree(tmp, b[i], dist))
                {
                    out.push_back(b[i]);
                }
            }

            return true;
        }

        bool test_loop_inner_points_internal(const std::vector<vector2>& loop, const std::vector<vector2>& points)
        {
            if (points.empty()) return false;

            vector2 min, max;
            get_minmax(min, max, loop);

            std::vector<edge> edges;
            size_t lsz = loop.size();
            edges.reserve(lsz);
            for (size_t i = 0; i < lsz; i++)
            {
                edge e(&loop[i], &loop[(i == lsz - 1) ? 0 : i + 1]);
                edges.push_back(e);
            }

            size_t psz = points.size();

            //size_t hID = 0;
            std::vector<edge_event> events;
            events.reserve(lsz * 2 + psz);
            for (size_t i = 0; i < lsz; i++)
            {
                edge* e = &edges[i];
                vector2 p0 = *(e->p[0]);
                vector2 p1 = *(e->p[1]);
                real dY = std::fabs(p0[1] - p1[1]);
                real dX = std::fabs(p0[0] - p1[0]);

                if (dY <= EPSILON || (dX >= EPSILON && dY / dX <= EPSILON))
                {
                    //
                }
                else
                {
                    if (p0[1] < p1[1]) std::swap(p0, p1);

                    edge_event ev0;
                    ev0.type = EDGE_START;
                    ev0.e = e;
                    ev0.p = p0;

                    edge_event ev1;
                    ev1.type = EDGE_END;
                    ev1.e = e;
                    ev1.p = p1;

                    events.push_back(ev0);
                    events.push_back(ev1);
                }
            }
            for (size_t i = 0; i < psz; i++)
            {
                if (test_bound(min, max, points[i]))
                {
                    edge_event ev;
                    ev.type = POINT;
                    ev.e = NULL;
                    ev.p = points[i];
                    ev.index = i;
                    events.push_back(ev);
                }
            }
            //
            std::sort(events.begin(), events.end(), event_sorter());
            //
            std::vector<bool> bchecks(psz, false);

            std::vector<real> ys;

            stage_set stg;
            size_t evsz = events.size();
            for (size_t i = 0; i < evsz; i++)
            {
                edge_event& ev = events[i];
                switch (ev.type)
                {
                case EDGE_START:
                {
                    stg.insert(ev.e);
                }
                break;
                case EDGE_END:
                {
                    stg.erase(ev.e);
                }
                break;
                case POINT:
                {
                    if (!stg.empty())
                    {
                        ys.clear();
                        for (stage_iter iter = stg.begin(); iter != stg.end(); iter++)
                        {
                            edge* e = *iter;
                            if (y_check(e, ev.p[1]))
                            {
                                ys.push_back(e->sweep(ev.p[1]));
                            }
                        }
                        if (!ys.empty())
                        {
                            std::sort(ys.begin(), ys.end());
                            int ysz = (int)ys.size();
                            real x = ev.p[0];
                            if (ysz >= 2)
                            {
                                for (int j = 0; j < ysz - 1; j += 2)
                                {
                                    if (ys[j] + EPSILON < x && x < ys[j + 1] - EPSILON)
                                    {
                                        bchecks[ev.index] = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
                }
            }

            //out.reserve(psz);
            for (size_t i = 0; i < psz; i++)
            {
                if (!bchecks[i])
                {
                    return false;
                }
            }

            return true;
        }

        bool test_loop_outer_points_internal(const std::vector<vector2>& loop, const std::vector<vector2>& points)
        {
            if (points.empty()) return false;

            vector2 min, max;
            get_minmax(min, max, loop);

            std::vector<edge> edges;
            size_t lsz = loop.size();
            edges.reserve(lsz);
            for (size_t i = 0; i < lsz; i++)
            {
                edge e(&loop[i], &loop[(i == lsz - 1) ? 0 : i + 1]);
                edges.push_back(e);
            }

            size_t psz = points.size();

            //size_t hID = 0;
            std::vector<edge_event> events;
            events.reserve(lsz * 2 + psz);
            for (size_t i = 0; i < lsz; i++)
            {
                edge* e = &edges[i];
                vector2 p0 = *(e->p[0]);
                vector2 p1 = *(e->p[1]);
                real dY = std::fabs(p0[1] - p1[1]);
                real dX = std::fabs(p0[0] - p1[0]);

                if (dY <= EPSILON || (dX >= EPSILON && dY / dX <= EPSILON))
                {
                    //
                }
                else
                {
                    if (p0[1] < p1[1]) std::swap(p0, p1);

                    edge_event ev0;
                    ev0.type = EDGE_START;
                    ev0.e = e;
                    ev0.p = p0;

                    edge_event ev1;
                    ev1.type = EDGE_END;
                    ev1.e = e;
                    ev1.p = p1;

                    events.push_back(ev0);
                    events.push_back(ev1);
                }
            }
            for (size_t i = 0; i < psz; i++)
            {
                if (test_bound(min, max, points[i]))
                {
                    edge_event ev;
                    ev.type = POINT;
                    ev.e = NULL;
                    ev.p = points[i];
                    ev.index = i;
                    events.push_back(ev);
                }
            }
            //
            std::sort(events.begin(), events.end(), event_sorter());
            //
            std::vector<bool> bchecks(psz, true); //true

            std::vector<real> ys;

            stage_set stg;
            size_t evsz = events.size();
            for (size_t i = 0; i < evsz; i++)
            {
                edge_event& ev = events[i];
                switch (ev.type)
                {
                case EDGE_START:
                {
                    stg.insert(ev.e);
                }
                break;
                case EDGE_END:
                {
                    stg.erase(ev.e);
                }
                break;
                case POINT:
                {
                    if (!stg.empty())
                    {
                        ys.clear();
                        for (stage_iter iter = stg.begin(); iter != stg.end(); iter++)
                        {
                            edge* e = *iter;
                            if (y_check(e, ev.p[1]))
                            {
                                ys.push_back(e->sweep(ev.p[1]));
                            }
                        }
                        if (!ys.empty())
                        {
                            std::sort(ys.begin(), ys.end());
                            int ysz = (int)ys.size();
                            real x = ev.p[0];
                            if (ysz >= 2)
                            {
                                for (int j = 0; j < ysz - 1; j += 2)
                                {
                                    if (ys[j] - EPSILON <= x && x <= ys[j + 1] + EPSILON)
                                    {
                                        bchecks[ev.index] = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                break;
                }
            }

            for (size_t i = 0; i < psz; i++)
            {
                if (!bchecks[i])
                {
                    return false;
                }
            }

            return true;
        }
    }

    //------------------------------------------------------------------------------------

    bool loop_inner_points(std::vector<vector2>& out, const std::vector<vector2>& loop, const std::vector<vector2>& points)
    {
        if (points.empty())
        {
            out.clear();
            return true;
        }
        if (loop.size() < 3) return false;
        if (loop.back() == loop.front())
        {
            std::vector<vector2> tmp(loop);
            tmp.pop_back();
            if (tmp.size() < 3) return false;
            return loop_inner_points_innernal(out, tmp, points);
        }
        else
        {
            return loop_inner_points_innernal(out, loop, points);
        }
    }

    bool loop_outer_points(std::vector<vector2>& out, const std::vector<vector2>& loop, const std::vector<vector2>& points)
    {
        if (points.empty())
        {
            out.clear();
            return true;
        }
        if (loop.size() < 3) return false;
        if (loop.back() == loop.front())
        {
            std::vector<vector2> tmp(loop);
            tmp.pop_back();
            if (tmp.size() < 3) return false;
            return loop_outer_points_innernal(out, tmp, points);
        }
        else
        {
            return loop_outer_points_innernal(out, loop, points);
        }
    }

    bool test_loop_inner_points(const std::vector<vector2>& loop, const std::vector<vector2>& points)
    {
        if (points.empty()) return false;
        if (loop.size() < 3) return false;
        if (loop.back() == loop.front())
        {
            std::vector<vector2> tmp(loop);
            tmp.pop_back();
            if (tmp.size() < 3) return false;
            return test_loop_inner_points_internal(tmp, points);
        }
        else
        {
            return test_loop_inner_points_internal(loop, points);
        }

        return true;
    }

    bool test_loop_outer_points(const std::vector<vector2>& loop, const std::vector<vector2>& points)
    {
        if (points.empty()) return false;
        if (loop.size() < 3) return false;
        if (loop.back() == loop.front())
        {
            std::vector<vector2> tmp(loop);
            tmp.pop_back();
            if (tmp.size() < 3) return false;
            return test_loop_outer_points_internal(tmp, points);
        }
        else
        {
            return test_loop_outer_points_internal(loop, points);
        }

        return true;
    }
}
