#include "triangle_quad_tree.h"

namespace kaze
{
    typedef triangle_quad_tree::tri_struct tri_struct;
    typedef triangle_quad_tree::quad_tree_node quad_tree_node;

    //----------------------------------------------------------------
    typedef const tri_struct* PCTRI;
    typedef std::vector<PCTRI>::iterator citer;

    void get_minmax(vector2& min, vector2& max, const std::vector<vector2>& v)
    {
        min = max = v[0];
        for (size_t i = 0; i < v.size(); i++)
        {
            for (int k = 0; k < 2; k++)
            {
                if (v[i][k] < min[k]) min[k] = v[i][k];
                if (v[i][k] > max[k]) max[k] = v[i][k];
            }
        }
    }

    void get_minmax(vector2& min, vector2& max, citer a, citer b)
    {
        min = max = (*a)->c[0];
        for (citer i = a; i != b; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                vector2 p = (*i)->c[j];
                for (int k = 0; k < 2; k++)
                {
                    if (min[k] > p[k]) min[k] = p[k];
                    if (max[k] < p[k]) max[k] = p[k];
                }
            }
        }
    }

    static inline real tri_area(const vector2& a, const vector2& b, const vector2& c)
    {
        return (a[0] - c[0]) * (b[1] - c[1]) - (a[1] - c[1]) * (b[0] - c[0]);
    }

    static inline bool test_tri(const tri_struct& tri, const vector2& p)
    {
        real areas[3];
        areas[0] = tri_area(tri.c[0], tri.c[1], p);
        areas[1] = tri_area(tri.c[1], tri.c[2], p);
        areas[2] = tri_area(tri.c[2], tri.c[0], p);

        int nSign = 0;
        if (areas[0] < 0) nSign++;
        if (areas[1] < 0) nSign++;
        if (areas[2] < 0) nSign++;

        return (nSign == 0) || (nSign == 3);
    }

    struct less_
    {
        less_(int plane) : plane_(plane) {}
        bool operator()(PCTRI a, PCTRI b) const
        {
            int plane = plane_;
            real ax = (a->c[0][plane] + a->c[1][plane] + a->c[2][plane]);
            real bx = (b->c[0][plane] + b->c[1][plane] + b->c[2][plane]);
            return ax < bx;
        }
        int plane_;
    };

    class leaf_quad_tree_node : public quad_tree_node
    {
    public:
        leaf_quad_tree_node(citer a, citer b) : ptr_(*a) {}
        bool test(const vector2& p) const
        {
            return test_tri(*ptr_, p);
        }

    private:
        PCTRI ptr_;
    };

    class branch_quad_tree_node : public quad_tree_node
    {
    public:
        branch_quad_tree_node(citer a, citer b)
        {
            nodes_[0] = 0;
            nodes_[1] = 0;

            size_t sz = b - a;
            assert(sz);

            vector2 min, max;
            get_minmax(min, max, a, b);
            vector2 wid = max - min;
            int plane = (wid[0] > wid[1]) ? 0 : 1;
            std::sort(a, b, less_(plane));
            size_t m = sz >> 1;
            citer c = a + m;
            size_t csz = 0;

            csz = m;
            if (csz)
            {
                if (csz == 1)
                {
                    nodes_[0] = new leaf_quad_tree_node(a, c);
                }
                else
                {
                    nodes_[0] = new branch_quad_tree_node(a, c);
                }
            }

            csz = b - c;
            if (csz)
            {
                if (csz == 1)
                {
                    nodes_[1] = new leaf_quad_tree_node(c, b);
                }
                else
                {
                    nodes_[1] = new branch_quad_tree_node(c, b);
                }
            }

            min_ = min;
            max_ = max;
        }

        bool test(const vector2& p) const
        {
            if (min_[0] <= p[0] && p[0] <= max_[0] && min_[1] <= p[1] && p[1] <= max_[1])
            {
                if (nodes_[0])
                {
                    if (nodes_[0]->test(p)) return true;
                }
                if (nodes_[1])
                {
                    if (nodes_[1]->test(p)) return true;
                }
            }
            return false;
        }

    private:
        quad_tree_node* nodes_[2];
        vector2 min_;
        vector2 max_;
    };

    class quad_tree
    {
    public:
        quad_tree(const std::vector<tri_struct>& tri)
        {
            std::vector<PCTRI> ptri(tri.size());
            for (size_t i = 0; i < tri.size(); i++)
            {
                ptri[i] = &tri[i];
            }
            node_ = new branch_quad_tree_node(ptri.begin(), ptri.end());
        }
        ~quad_tree()
        {
            delete node_;
        }
        bool test(const vector2& p) const
        {
            return node_->test(p);
        }

    public:
        quad_tree_node* node_;
    };

    triangle_quad_tree::triangle_quad_tree(const std::vector<vector2>& v)
    {
        size_t sz = v.size() / 3;
        mv_.resize(sz);
        for (size_t i = 0; i < sz; i++)
        {
            mv_[i].c[0] = v[3 * i + 0];
            mv_[i].c[1] = v[3 * i + 1];
            mv_[i].c[2] = v[3 * i + 2];
        }

        std::vector<PCTRI> ptri(sz);
        for (size_t i = 0; i < sz; i++)
        {
            ptri[i] = &mv_[i];
        }
        node_ = new branch_quad_tree_node(ptri.begin(), ptri.end());
    }
    triangle_quad_tree::~triangle_quad_tree()
    {
        delete node_;
    }
    bool triangle_quad_tree::test(const vector2& p) const
    {
        return node_->test(p);
    }
}