#include "nurbs_to_bspline.h"
#include "bspline_to_bezier.h"
#include <cmath>

namespace kaze
{

    static bool is_weight_one(real x, real eps)
    {
        if (fabs(x - 1.0) > eps) return false;
        return true;
    }

    static bool is_weight_one(const std::vector<real>& w, real eps)
    {
        for (size_t i = 0; i < w.size(); i++)
        {
            if (!is_weight_one(w[i], eps)) return false;
        }
        return true;
    }

    static bool is_weight_one(const bezier_curve<real>& bc, real eps)
    {
        int sz = bc.get_cp_size();
        for (int i = 0; i < sz; i++)
        {
            if (!is_weight_one(bc.get_cp_at(i), eps)) return false;
        }
        return true;
    }

    static bool is_weight_one(const multi_bezier_curve<real>& mbc, real eps)
    {
        int sz = mbc.get_curve_size();
        for (int i = 0; i < sz; i++)
        {
            if (!is_weight_one(mbc.get_curve_at(i), eps)) return false;
        }
        return true;
    }

    static real mid_weight(const std::vector<real>& w)
    {
        size_t n = 0;
        real ww = 0;
        size_t sz = w.size();
        for (size_t i = 0; i < sz; i++)
        {
            real k = w[i];
            if (fabs(k) > ww)
            {
                ww = fabs(k);
                n = i;
            }
        }
        return w[n];
    }

    static real mid_weight(const bezier_curve<real>& w)
    {
        return mid_weight(w.get_cp());
    }

    template <class T>
    static void split_for_weight(
        std::vector<real>& knots,
        std::vector<bezier_curve<T> >& cps,
        std::vector<bezier_curve<real> >& ws,
        real k0,
        real k1,
        const bezier_curve<T>& cp,
        const bezier_curve<real>& w,
        real eps,
        int level = 0)
    {
        if (!is_weight_one(w, eps))
        {
            bezier_curve<T> cpx(cp);
            bezier_curve<real> wx(w);

            real wmax = mid_weight(w);
            for (int i = 0; i < wx.get_cp_size(); i++)
            {
                wx.set_at(i, wx.get_at(i) / wmax);
            }
            //------------------------------------
            for (int i = 0; i < cpx.get_cp_size(); i++)
            {
                cpx.set_at(i, cpx.get_at(i) * wx.get_at(i));
            }
            bezier_curve<T> cp2[2];
            bezier_curve<real> w2[2];
            cpx.split(cp2, 0.5);
            wx.split(w2, 0.5);

            for (int i = 0; i < cpx.get_cp_size(); i++)
            {
                cp2[0].set_at(i, cp2[0].get_at(i) / w2[0].get_at(i));
                cp2[1].set_at(i, cp2[1].get_at(i) / w2[1].get_at(i));
            }

            real km = (k0 + k1) * 0.5;
            split_for_weight(knots, cps, ws, k0, km, cp2[0], w2[0], eps, level + 1);
            split_for_weight(knots, cps, ws, km, k1, cp2[1], w2[1], eps, level + 1);
        }
        else
        {
            knots.push_back(k0);
            cps.push_back(cp);
            ws.push_back(w);
        }
    }

    template <class T>
    static void split_for_weight(multi_bezier_curve<T>& cp, multi_bezier_curve<real>& w, real eps)
    {
        std::vector<real> knots;
        std::vector<bezier_curve<T> > cps;
        std::vector<bezier_curve<real> > ws;
        int sz = cp.get_curve_size();
        for (int i = 0; i < sz; i++)
        {
            real k0 = cp.get_knot_at(i);
            real k1 = cp.get_knot_at(i + 1);
            split_for_weight(knots, cps, ws, k0, k1, cp.get_curve_at(i), w.get_curve_at(i), eps);
        }
        knots.push_back(cp.get_knot_at(sz));

        multi_bezier_curve<T> tcp(cps, knots);
        multi_bezier_curve<real> tw(ws, knots);

        cp.swap(tcp);
        w.swap(tw);
    }

    template <class T>
    static void split_for_weight_(multi_bezier_patch<T>& cp, multi_bezier_patch<real>& w, real eps, int level = 0)
    {
        int nPu = w.get_patch_size_u();
        int nPv = w.get_patch_size_v();

        std::vector<bool> u_split(nPu);
        std::vector<bool> v_split(nPv);
        bool bSplit = false;
        for (int i = 0; i < nPu; i++)
            u_split[i] = false;
        for (int i = 0; i < nPv; i++)
            v_split[i] = false;

        if (level >= 4) return;

        for (int j = 0; j < nPv; j++)
        {
            for (int i = 0; i < nPu; i++)
            {
                const bezier_patch<real>& patch = w.get_patch_at(i, j);
                int nu = patch.get_nu();
                int nv = patch.get_nv();

                std::vector<real> ws = patch.get_cp();

                real wmax = mid_weight(ws);

                for (int k = 0; k < (int)(ws.size()); k++)
                {
                    ws[k] = ws[k] / wmax;
                }
                if (!is_weight_one(ws, eps))
                {
                    u_split[i] = true;
                    v_split[j] = true;
                    bSplit = true;
                }
            }
        }

        if (!bSplit) return;

        std::vector<real> knots_u;
        std::vector<real> knots_v;
        for (int i = 0; i < nPu; i++)
        {
            real k0 = w.get_knot_at_u(i);
            real k1 = w.get_knot_at_u(i + 1);
            if (u_split[i])
            {
                real km = (k0 + k1) * 0.5;
                knots_u.push_back(k0);
                knots_u.push_back(km);
            }
            else
            {
                knots_u.push_back(k0);
            }
        }
        knots_u.push_back(w.get_knot_at_u(nPu));

        for (int i = 0; i < nPv; i++)
        {
            real k0 = w.get_knot_at_v(i);
            real k1 = w.get_knot_at_v(i + 1);
            if (v_split[i])
            {
                real km = (k0 + k1) * 0.5;
                knots_v.push_back(k0);
                knots_v.push_back(km);
            }
            else
            {
                knots_v.push_back(k0);
            }
        }
        knots_v.push_back(w.get_knot_at_v(nPv));

        std::vector<bezier_patch<T> > cpx;
        std::vector<bezier_patch<real> > wx;

        int kPu = (int)knots_u.size() - 1;
        int kPv = (int)knots_v.size() - 1;

        for (int j = 0; j < nPv; j++)
        {
            for (int i = 0; i < nPu; i++)
            {
                const bezier_patch<T>& cpatch = cp.get_patch_at(i, j);
                const bezier_patch<real>& wpatch = w.get_patch_at(i, j);
                if (u_split[i])
                {
                    bezier_patch<T> ct[2];
                    bezier_patch<real> wt[2];
                    cpatch.split_u(ct, 0.5);
                    wpatch.split_u(wt, 0.5);
                    cpx.push_back(ct[0]);
                    cpx.push_back(ct[1]);
                    wx.push_back(wt[0]);
                    wx.push_back(wt[1]);
                }
                else
                {
                    cpx.push_back(cpatch);
                    wx.push_back(wpatch);
                }
            }
        }

        std::vector<bezier_patch<T> > cpx2;
        std::vector<bezier_patch<real> > wx2;

        for (int i = 0; i < kPu; i++)
        {
            for (int j = 0; j < nPv; j++)
            {
                const bezier_patch<T>& cpatch = cpx[j * kPu + i];
                const bezier_patch<real>& wpatch = wx[j * kPu + i];

                if (v_split[j])
                {
                    bezier_patch<T> ct[2];
                    bezier_patch<real> wt[2];
                    cpatch.split_v(ct, 0.5);
                    wpatch.split_v(wt, 0.5);
                    cpx2.push_back(ct[0]);
                    cpx2.push_back(ct[1]);
                    wx2.push_back(wt[0]);
                    wx2.push_back(wt[1]);
                }
                else
                {
                    cpx2.push_back(cpatch);
                    wx2.push_back(wpatch);
                }
            }
        }

        //-------------------------------------------------------
        cpx.clear();
        wx.clear();
        for (int j = 0; j < kPv; j++)
        {
            for (int i = 0; i < kPu; i++)
            {
                const bezier_patch<T>& cpatch = cpx2[i * kPv + j];
                const bezier_patch<real>& wpatch = wx2[i * kPv + j];
                cpx.push_back(cpatch);
                wx.push_back(wpatch);
            }
        }
        //------------------------------------------------------
        multi_bezier_patch<T> mcp(knots_u, knots_v, cpx);
        multi_bezier_patch<real> mw(knots_u, knots_v, wx);

        cp.swap(mcp);
        w.swap(mw);
        split_for_weight_(cp, w, eps, level + 1);
    }

    template <class T>
    static void split_for_weight(multi_bezier_patch<T>& cp, multi_bezier_patch<real>& w, real eps)
    {
        int nPu = w.get_patch_size_u();
        int nPv = w.get_patch_size_v();
        for (int j = 0; j < nPv; j++)
        {
            for (int i = 0; i < nPu; i++)
            {
                bezier_patch<T>& cpatch = cp.get_patch_at(i, j);
                bezier_patch<real>& wpatch = w.get_patch_at(i, j);
                int nu = cpatch.get_nu();
                int nv = cpatch.get_nv();
                for (int jj = 0; jj < nv; jj++)
                {
                    for (int ii = 0; ii < nu; ii++)
                    {
                        cpatch.set_at(ii, jj, cpatch.get_at(ii, jj) * wpatch.get_at(ii, jj));
                    }
                }
            }
        }

        split_for_weight_(cp, w, eps, 0);

        nPu = w.get_patch_size_u();
        nPv = w.get_patch_size_v();

        for (int j = 0; j < nPv; j++)
        {
            for (int i = 0; i < nPu; i++)
            {
                bezier_patch<T>& cpatch = cp.get_patch_at(i, j);
                bezier_patch<real>& wpatch = w.get_patch_at(i, j);
                int nu = cpatch.get_nu();
                int nv = cpatch.get_nv();
                for (int jj = 0; jj < nv; jj++)
                {
                    for (int ii = 0; ii < nu; ii++)
                    {
                        cpatch.set_at(ii, jj, cpatch.get_at(ii, jj) / wpatch.get_at(ii, jj));
                    }
                }
            }
        }
    }

    static void mbz_knots(std::vector<real>& knots, const std::vector<real>& mbknots, int nOrder)
    {
        int k = 0;
        {
            for (int j = 0; j < nOrder; j++)
            {
                knots.push_back(mbknots[k]);
            }
            k++;
        }
        for (k = 1; k < (int)(mbknots.size()) - 1; k++)
        {
            for (int j = 0; j < nOrder - 1; j++)
            {
                knots.push_back(mbknots[k]);
            }
        }
        {
            for (int j = 0; j < nOrder; j++)
            {
                knots.push_back(mbknots[k]);
            }
            k++;
        }
    }

    template <class T>
    static void mbz_to_nurbs(nurbs_curve<T>& out, const multi_bezier_curve<T>& cp, const multi_bezier_curve<real>& w)
    {
        std::vector<T> vcp;
        std::vector<real> vw;
        int sz = cp.get_curve_size();
        for (int i = 0; i < sz; i++)
        {
            const bezier_curve<T>& bzcp = cp.get_curve_at(i);
            const bezier_curve<real> bzw = w.get_curve_at(i);
            int bsz = bzcp.get_cp_size();
            for (int j = 0; j < bsz - 1; j++)
            {
                vcp.push_back(bzcp.get_cp_at(j));
                vw.push_back(bzw.get_cp_at(j));
            }
            if (i == sz - 1)
            {
                vcp.push_back(bzcp.get_cp_at(bsz - 1));
                vw.push_back(bzw.get_cp_at(bsz - 1));
            }
        }
        int nOrder = cp.order();
        std::vector<real> knots;
        mbz_knots(knots, cp.get_knots(), nOrder);

        nurbs_curve<T> tmp(vcp, vw, knots);
        out.swap(tmp);
    }

    template <class T>
    static void mbz_to_nurbs(nurbs_patch<T>& out, const multi_bezier_patch<T>& cp, const multi_bezier_patch<real>& w)
    {
        int nu = cp.get_patch_at(0, 0).get_nu();
        int nv = cp.get_patch_at(0, 0).get_nv();
        int nPu = cp.get_patch_size_u();
        int nPv = cp.get_patch_size_v();
        int ku = (nu - 1) * nPu + 1;
        int kv = (nv - 1) * nPv + 1;
        std::vector<T> cpx(ku * kv);
        std::vector<real> wx(ku * kv);
        for (int jj = 0; jj < nPv; jj++)
        {
            for (int ii = 0; ii < nPu; ii++)
            {
                const bezier_patch<T>& cpatch = cp.get_patch_at(ii, jj);
                const bezier_patch<real>& wpatch = w.get_patch_at(ii, jj);

                for (int j = 0; j < nv; j++)
                {
                    for (int i = 0; i < nu; i++)
                    {
                        int di = ii * (nu - 1) + i;
                        int dj = jj * (nv - 1) + j;
                        int dst = ku * dj + di;
                        cpx[dst] = cpatch.get_at(i, j);
                        wx[dst] = wpatch.get_at(i, j);
                    }
                }
            }
        }

        std::vector<real> knots_u;
        std::vector<real> knots_v;
        mbz_knots(knots_u, cp.get_knots_u(), nu);
        mbz_knots(knots_v, cp.get_knots_v(), nv);

        nurbs_patch<T> tmp(ku, kv, cpx, wx, knots_u, knots_v);
        out.swap(tmp);
    }

    template <class T>
    static void nurbs_to_nurbs_(nurbs_curve<T>& b, const nurbs_curve<T>& a, real eps)
    {
        nurbs_curve<T> tmp(a);
        tmp.to_bezier_type();

        bspline_curve<T> pbsp(tmp.get_cp(), tmp.get_knots());
        bspline_curve<real> wbsp(tmp.get_w(), tmp.get_knots());

        multi_bezier_curve<T> pmbz;
        multi_bezier_curve<real> wmbz;

        bspline_to_bezier(pmbz, pbsp);
        bspline_to_bezier(wmbz, wbsp);

        split_for_weight(pmbz, wmbz, eps);

        nurbs_curve<T> tmp2;
        mbz_to_nurbs(tmp2, pmbz, wmbz);

        b.swap(tmp2);
    }

    template <class T>
    static void nurbs_to_nurbs_(nurbs_patch<T>& b, const nurbs_patch<T>& a, real eps)
    {
        nurbs_patch<T> tmp(a);
        tmp.to_bezier_type();

        bspline_patch<T> pbsp(tmp.get_nu(), tmp.get_nv(), tmp.get_cp(), tmp.get_knots_u(), tmp.get_knots_v());
        bspline_patch<real> wbsp(tmp.get_nu(), tmp.get_nv(), tmp.get_w(), tmp.get_knots_u(), tmp.get_knots_v());

        multi_bezier_patch<T> pmbz;
        multi_bezier_patch<real> wmbz;

        bspline_to_bezier(pmbz, pbsp);
        bspline_to_bezier(wmbz, wbsp);

        split_for_weight(pmbz, wmbz, eps);

        nurbs_patch<T> tmp2;
        mbz_to_nurbs(tmp2, pmbz, wmbz);

        b.swap(tmp2);
    }

    template <class T>
    static void nurbs_to_bspline_(bspline_curve<T>& bsp, const nurbs_curve<T>& nbs, real eps = 0.1)
    {
        if (!is_weight_one(nbs.get_w(), eps))
        {
            nurbs_curve<T> tn;
            nurbs_to_nurbs_(tn, nbs, eps);
            bspline_curve<T> tmp(tn.get_cp(), tn.get_knots());
            bsp.swap(tmp);
        }
        else
        {
            bspline_curve<T> tmp(nbs.get_cp(), nbs.get_knots());
            bsp.swap(tmp);
        }
    }

    template <class T>
    static void nurbs_to_bspline_(bspline_patch<T>& bsp, const nurbs_patch<T>& nbs, real eps = 0.1)
    {
        if (!is_weight_one(nbs.get_w(), eps))
        {
            nurbs_patch<T> tn;
            nurbs_to_nurbs_(tn, nbs, eps);
            bspline_patch<T> tmp(
                tn.get_nu(), tn.get_nv(),
                tn.get_cp(),
                tn.get_knots_u(), tn.get_knots_v());
            bsp.swap(tmp);
        }
        else
        {
            bspline_patch<T> tmp(
                nbs.get_nu(), nbs.get_nv(),
                nbs.get_cp(),
                nbs.get_knots_u(), nbs.get_knots_v());
            bsp.swap(tmp);
        }
    }

#define DEC_NBS_TO_BSP(TYPE)                                                                \
    void nurbs_to_bspline(bspline_curve<TYPE>& bsp, const nurbs_curve<TYPE>& nbs, real eps) \
    {                                                                                       \
        nurbs_to_bspline_(bsp, nbs, eps);                                                   \
    }                                                                                       \
    void nurbs_to_bspline(bspline_patch<TYPE>& bsp, const nurbs_patch<TYPE>& nbs, real eps) \
    {                                                                                       \
        nurbs_to_bspline_(bsp, nbs, eps);                                                   \
    }

    DEC_NBS_TO_BSP(real)
    DEC_NBS_TO_BSP(vector2)
    DEC_NBS_TO_BSP(vector3)

#undef DEC_NBS_TO_BSP
}
