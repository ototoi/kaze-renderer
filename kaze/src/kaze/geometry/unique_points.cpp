#include "unique_points.h"
#include <vector>
#include <algorithm>
#include <limits>
#include <iterator>
#include "values.h"

namespace kaze
{
    namespace
    {
        struct kd_point
        {
            vector2 pos;
            char plane;
            kd_point* p_next;
            bool bChecked;

            bool is_contain(kd_point* p)
            {
                if (this == p) return true;
                kd_point* pp = p_next;
                while (pp && pp != this)
                {
                    if (pp == p) return true;
                    pp = pp->p_next;
                }
                return false;
            }

            void connect_chain(kd_point* p)
            {
                if (is_contain(p)) return;
                kd_point* pn = p_next;
                p_next = p;

                kd_point* pp = p;
                while (pp->p_next != p)
                {
                    pp = pp->p_next;
                }
                pp->p_next = pn;
            }
            void get_chain(std::vector<kd_point*>& points)
            {
                points.push_back(this);
                kd_point* pp = p_next;
                while (pp != this)
                {
                    points.push_back(pp);
                    pp = pp->p_next;
                }
            }
        };

        struct kd_less
        {
            kd_less(int plane) : plane_(plane) {}
            bool operator()(const kd_point& a, const kd_point& b)
            {
                return a.pos[plane_] < b.pos[plane_];
            }
            int plane_;
        };

        typedef std::vector<kd_point>::iterator iterator;
        typedef std::vector<kd_point>::const_iterator const_iterator;

        void construct_kdtree(iterator a, iterator b, const vector2& min, const vector2& max)
        {
            size_t sz = b - a;
            if (sz <= 1) return;

            vector2 wid = max - min;

            int plane = 0;
            if (wid[1] > wid[plane]) plane = 1;

            std::sort(a, b, kd_less(plane));

            size_t m = sz >> 1;
            iterator c = a + m;
            c->plane = plane;
            real part = c->pos[plane];
            vector2 cmin, cmax;

            cmin = min;
            cmax = max;
            cmax[plane] = part;
            construct_kdtree(a, c, cmin, cmax);

            cmin = min;
            cmin[plane] = part;
            cmax = max;
            construct_kdtree(c + 1, b, cmin, cmax);
        }
        void construct_kdtree(std::vector<kd_point>& points)
        {
            vector2 min = points[0].pos;
            vector2 max = points[0].pos;
            for (size_t i = 1; i < points.size(); i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (min[j] > points[i].pos[j]) min[j] = points[i].pos[j];
                    if (max[j] < points[i].pos[j]) max[j] = points[i].pos[j];
                }
            }
            construct_kdtree(points.begin(), points.end(), min, max);
        }

        struct kdt_info
        {
            kd_point* p;
            real d2;
        };

        bool find_near_kdtree(kdt_info& info, const_iterator a, const_iterator b, const vector2& p, real d2)
        {
#if 0
      bool bRet = false;
      for(const_iterator i = a;i != b;i++){
          real l2 = (i->pos-p).sqr_length();
          if(l2<d2){
              d2 = l2;
              info.d2 = d2;
              info.p = const_cast<kd_point*>(&(*i));
              bRet = true;
          }
      }
      return bRet;

#else
            size_t sz = b - a;
            if (sz <= 0) return false;
            if (sz <= 8)
            {
                bool bRet = false;
                for (const_iterator i = a; i != b; i++)
                {
                    real l2 = (i->pos - p).sqr_length();
                    if (l2 < d2)
                    {
                        d2 = l2;
                        info.d2 = d2;
                        info.p = const_cast<kd_point*>(&(*i));
                        bRet = true;
                    }
                }
                return bRet;
            }
            else
            {
                const_iterator c = a + (sz >> 1);
                int plane = c->plane;
                vector2 pos = c->pos;

                real s2 = pos[plane] - p[plane];
                s2 *= s2;

                bool bRet = false;
                if (p[plane] < pos[plane])
                {
                    if (find_near_kdtree(info, a, c, p, d2))
                    {
                        d2 = info.d2;
                        bRet = true;
                    }
                    if (s2 <= d2)
                    {
                        real l2 = (pos - p).sqr_length();
                        if (l2 < d2)
                        {
                            d2 = l2;
                            info.d2 = d2;
                            info.p = const_cast<kd_point*>(&(*c));
                            bRet = true;
                        }
                        if (find_near_kdtree(info, c + 1, b, p, d2))
                        {
                            d2 = info.d2;
                            bRet = true;
                        }
                    }
                }
                else
                {
                    if (find_near_kdtree(info, c + 1, b, p, d2))
                    {
                        d2 = info.d2;
                        bRet = true;
                    }
                    if (s2 <= d2)
                    {
                        real l2 = (pos - p).sqr_length();
                        if (l2 < d2)
                        {
                            d2 = l2;
                            info.d2 = d2;
                            info.p = const_cast<kd_point*>(&(*c));
                            bRet = true;
                        }
                        if (find_near_kdtree(info, a, c, p, d2))
                        {
                            d2 = info.d2;
                            bRet = true;
                        }
                    }
                }
                return bRet;
            }
#endif
        }

        bool find_near_kdtree(std::vector<kd_point*>& info, const std::vector<kd_point>& points, const vector2& p)
        {
            kdt_info kinfo;
            if (find_near_kdtree(kinfo, points.begin(), points.end(), p, values::far()))
            {
                info.push_back(kinfo.p);
                return true;
            }
            return false;
        }

        bool find_range_kdtree(std::vector<kd_point*>& info, const_iterator a, const_iterator b, const vector2& p, real rad2)
        {
#if 0
      bool bRet = false;
      for(const_iterator i = a; i!=b; i++){
        if((i->pos-p).sqr_length()<=rad2){
            kd_point* pp = const_cast<kd_point*>(&(*i));
            info.push_back(pp);
            bRet = true;            
        }
      }
      return bRet;
#else
            size_t sz = b - a;
            if (sz <= 0) return false;
            if (sz <= 8)
            {
                bool bRet = false;
                for (const_iterator i = a; i != b; i++)
                {
                    if ((i->pos - p).sqr_length() <= rad2)
                    {
                        kd_point* pp = const_cast<kd_point*>(&(*i));
                        info.push_back(pp);
                        bRet = true;
                    }
                }
                return bRet;
            }
            else
            {
                size_t m = sz >> 1;
                const_iterator c = a + m;
                int plane = c->plane;
                real part = c->pos[plane];
                if (p[plane] < part)
                {
                    bool bRet = false;
                    if (find_range_kdtree(info, a, c, p, rad2))
                    {
                        bRet = true;
                    }
                    real s2 = part - p[plane];
                    s2 *= s2;
                    if (s2 <= rad2)
                    {
                        if ((c->pos - p).sqr_length() <= rad2)
                        {
                            kd_point* pp = const_cast<kd_point*>(&(*c));
                            info.push_back(pp);
                            bRet = true;
                        }
                        if (find_range_kdtree(info, c + 1, b, p, rad2))
                        {
                            bRet = true;
                        }
                    }
                    return bRet;
                }
                else
                {
                    bool bRet = false;
                    if (find_range_kdtree(info, c + 1, b, p, rad2))
                    {
                        bRet = true;
                    }
                    real s2 = p[plane] - part;
                    s2 *= s2;
                    if (s2 <= rad2)
                    {
                        if ((c->pos - p).sqr_length() <= rad2)
                        {
                            kd_point* pp = const_cast<kd_point*>(&(*c));
                            info.push_back(pp);
                            bRet = true;
                        }
                        if (find_range_kdtree(info, a, c, p, rad2))
                        {
                            bRet = true;
                        }
                    }
                    return bRet;
                }
            }
#endif
        }

        bool find_range_kdtree(std::vector<kd_point*>& info, const std::vector<kd_point>& points, const vector2& p, real rad2)
        {
            return find_range_kdtree(info, points.begin(), points.end(), p, rad2);
        }
    }

    void unique_points(std::vector<vector2>& out, const std::vector<vector2>& points, real radius)
    {
        size_t sz = points.size();
        std::vector<kd_point> vertices(sz);
        memset(&vertices[0], 0, sizeof(kd_point) * sz);
        for (size_t i = 0; i < sz; i++)
        {
            vertices[i].pos = points[i];
        }
        construct_kdtree(vertices);

        for (size_t i = 0; i < sz; i++)
        {
            vertices[i].p_next = &vertices[i];
        }

        real rad2 = std::max<real>(std::numeric_limits<real>::epsilon(), radius * radius);
        std::vector<kd_point*> info;
        for (size_t i = 0; i < sz; i++)
        {
            info.clear();
            if (find_range_kdtree(info, vertices, vertices[i].pos, rad2))
            {
                size_t isz = info.size();
                for (size_t j = 0; j < isz; j++)
                {
                    if (&vertices[i] != info[j])
                    {
                        vertices[i].connect_chain(info[j]);
                    }
                }
            }
        }

        for (size_t i = 0; i < sz; i++)
        {
            if (vertices[i].bChecked) continue;
            if (vertices[i].p_next == &vertices[i])
            {
                out.push_back(vertices[i].pos);
                vertices[i].bChecked = true;
            }
            else
            {
                std::vector<kd_point*> info;
                vertices[i].get_chain(info);
                size_t isz = info.size();
                vector2 cp = info[0]->pos;
                for (size_t j = 1; j < isz; j++)
                {
                    cp += info[j]->pos;
                    info[j]->bChecked = true;
                }
                cp *= real(1.0) / isz;
                out.push_back(cp);
            }
        }
    }

    void unique_points(std::vector<vector2>& inout, real radius)
    {
        std::vector<vector2> out;
        unique_points(out, inout, radius);
        inout.swap(out);
    }

    void unique_lines(std::vector<vector2>& vertices, std::vector<size_t>& indices, const std::vector<vector2>& lines, real radius)
    {
        std::vector<vector2> points;
        unique_points(points, lines, radius);
        size_t sz = points.size();
        std::vector<kd_point> kd_verts(sz);
        memset(&kd_verts[0], 0, sizeof(kd_point) * sz);
        for (size_t i = 0; i < sz; i++)
        {
            kd_verts[i].pos = points[i];
        }
        construct_kdtree(kd_verts);

        std::vector<kd_point*> info;
        for (size_t i = 0; i < lines.size(); i++)
        {
            info.clear();
            if (find_near_kdtree(info, kd_verts, lines[i]))
            {
                indices.push_back(info[0] - &kd_verts[0]);
            }
        }
        for (size_t i = 0; i < kd_verts.size(); i++)
        {
            vertices.push_back(kd_verts[i].pos);
        }
    }

    void unique_lines(std::vector<vector2>& vertices, std::vector<std::vector<size_t> >& indices, const std::vector<std::vector<vector2> >& loops, real radius)
    {
        std::vector<vector2> lines;
        for (size_t j = 0; j < loops.size(); j++)
        {
            std::copy(loops[j].begin(), loops[j].end(), std::back_inserter(lines));
        }

        std::vector<vector2> points;
        unique_points(points, lines, radius);
        size_t sz = points.size();
        std::vector<kd_point> kd_verts(sz);
        memset(&kd_verts[0], 0, sizeof(kd_point) * sz);
        for (size_t i = 0; i < sz; i++)
        {
            kd_verts[i].pos = points[i];
        }
        construct_kdtree(kd_verts);

        indices.resize(loops.size());
        std::vector<kd_point*> info;
        for (size_t j = 0; j < loops.size(); j++)
        {
            for (size_t i = 0; i < loops[j].size(); i++)
            {
                info.clear();
                if (find_near_kdtree(info, kd_verts, loops[j][i]))
                {
                    indices[j].push_back(info[0] - &kd_verts[0]);
                }
            }
        }

        for (size_t i = 0; i < kd_verts.size(); i++)
        {
            vertices.push_back(kd_verts[i].pos);
        }
    }

    void unique_lines(std::vector<vector2>& inoutlines, real radius)
    {
        std::vector<vector2> out;
        unique_lines(out, inoutlines, radius);
        inoutlines.swap(out);
    }

    void unique_lines(std::vector<vector2>& outlines, const std::vector<vector2>& lines, real radius)
    {
        std::vector<vector2> vtmp;
        std::vector<size_t> indices;
        unique_lines(vtmp, indices, lines, radius);
        for (size_t i = 0; i < indices.size(); i++)
        {
            outlines.push_back(vtmp[indices[i]]);
        }
    }
}