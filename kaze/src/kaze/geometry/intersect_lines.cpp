#include "intersect_lines.h"

#include "values.h"

#include <queue>
#include <vector>
#include <list>
#include <set>
#include <map>

#define DEBUG_PRINT

#ifdef DEBUG_PRINT
#include "timer.h"
#include "logger.h"
#endif

namespace kaze
{
    namespace
    {
        enum EVENT_TYPE
        {
            EDGE_START,
            EDGE_END,
            EDGE_CROSS,
            EDGE_HORIZONTAL
        };

        static double get_intersection(const vector2& a, const vector2& b, double v)
        {
            double k = (v - a[1]) / (b[1] - a[1]);
            //assert(0<=k&&k<=1);
            double u = (1 - k) * a[0] + k * b[0];

            return u;
        }

        struct edge
        {
            edge()
            {
                p[0] = NULL;
                p[1] = NULL;
            }
            edge(const vector2* a, const vector2* b)
            {
                p[0] = a;
                p[1] = b;
            }

            const vector2* p[2];
            real sweep_x;
            real sweep_x2;

            real sweep(real y) const
            {
                return get_intersection(*p[0], *p[1], y);
            }
        };

        struct edge_stage
        {
            edge_stage(edge* e_, real y)
            {
                e = e_;
                sweep_x = e->sweep(y);
                sweep_x2 = e->sweep(y - 0.001);
            }
            edge* e;
            real sweep_x;
            real sweep_x2;
        };

        struct edge_event
        {
            edge_event(int id_, EVENT_TYPE t_, const vector2& p_, edge* e_, edge* e2_ = NULL) : type(t_), id(id_), p(p_), e(e_), e2(e2_) {}
            edge_event(const edge_event& ev) : type(ev.type), id(ev.id), p(ev.p), e(ev.e), e2(ev.e2) {}

            EVENT_TYPE type;
            int id;
            vector2 p;
            edge* e;
            edge* e2;
        };

        static bool less_function(const edge_event& a, const edge_event& b)
        {
            if (a.p[1] != b.p[1])
            {
                return a.p[1] > b.p[1]; //Y
            }
            else
            {
                if (a.type != b.type)
                {
                    return a.type < b.type;
                }
                else
                {
                    if (a.p[0] != b.p[0])
                    {
                        return a.p[0] < b.p[0];
                    }
                    else
                    {
                        assert(a.id != b.id);
                        return a.id < b.id;
                    }
                }
            }
        }

        struct edge_greater
        {
            bool operator()(const edge_event& a, const edge_event& b)
            {
                return !less_function(a, b);
            }
        };

        typedef std::priority_queue<edge_event, std::vector<edge_event>, edge_greater> event_queue;

        static double tri_area(const vector2& a, const vector2& b, const vector2& c)
        {
            return (a[0] - c[0]) * (b[1] - c[1]) - (a[1] - c[1]) * (b[0] - c[0]);
        }

        static bool is_zero(double x)
        {
            static const double EPSILON = values::epsilon() * 1024;
            return fabs(x) < EPSILON;
        }

        static real rate_line(const vector2& a, const vector2& b, const vector2& c)
        {
            vector2 ab = b - a;
            vector2 ac = c - a;

            double lab = length(ab);
            double lac = length(ac);

            assert(lab != 0);

            if (dot(ac, ab) > 0)
            {
                return (lac / lab);
            }
            else
            {
                return -(lac / lab);
            }
        }

        static bool test_line_box(const vector2& p0, const vector2& p1, const vector2& min, const vector2& max)
        {
            static const real EPSILON = values::epsilon() * 1024;

            vector2 e = max - min;
            vector2 m = (p0 + p1) - (min + max);
            vector2 d = p1 - p0;

            real adx = fabs(d[0]);
            if (fabs(m[0]) > e[0] + adx) return false;
            real ady = fabs(d[1]);
            if (fabs(m[1]) > e[1] + ady) return false;

            adx += EPSILON;
            ady += EPSILON;
            if (fabs(m[0] * d[1] - m[1] * d[0]) > e[0] * ady + e[1] * adx) return false;
            return true;
        }

        static bool test_line_line(
            vector2& vout,
            std::vector<real>& aout, std::vector<real>& bout,
            const vector2& a, const vector2& b, const vector2& c, const vector2& d)
        {
            double a1 = tri_area(a, b, d);
            double a2 = tri_area(a, b, c);

            double a3 = tri_area(c, d, a);
            double a4 = tri_area(c, d, b);

            if (a1 * a2 < 0)
            {
                if (a3 * a4 < 0)
                {
                    double t = a3 / (a3 - a4);
                    double s = a2 / (a2 - a1);
#ifdef _DEBUG
                    vector2 aa = (1 - t) * a + t * b;
                    vector2 cc = (1 - s) * c + s * d;
#endif
                    assert(0 < t && t < 1);
                    assert(0 < s && s < 1);
                    aout.push_back(t);
                    bout.push_back(s);

                    vout = (1 - t) * a + t * b;
                    return true;
                }
            }

            //½s
            if (is_zero(a1) && is_zero(a2) && is_zero(a3) && is_zero(a4))
            {

                double kd = rate_line(a, b, d);
                double kc = rate_line(a, b, c);
                double ka = rate_line(c, d, a);
                double kb = rate_line(c, d, b);
                if (0 < kd && kd < 1)
                {
                    aout.push_back(kd);
                }
                if (0 < kc && kc < 1)
                {
                    aout.push_back(kc);
                }
                if (0 < ka && ka < 1)
                {
                    bout.push_back(ka);
                }
                if (0 < kb && kb < 1)
                {
                    bout.push_back(kb);
                }

                if (!aout.empty())
                {
                    real t = aout[0];
                    vout = (1 - t) * a + t * b;
                    return true;
                }
                else if (!bout.empty())
                {
                    real t = bout[0];
                    vout = (1 - t) * c + t * d;
                    return true;
                }
                else
                {
                    return false;
                }
            }

            if (is_zero(a1) && !is_zero(a2))
            {
                double kd = rate_line(a, b, d);
                if (0 < kd && kd < 1)
                {
                    aout.push_back(kd);
                }
            }
            else if (!is_zero(a1) && is_zero(a2))
            {
                double kc = rate_line(a, b, c);
                if (0 < kc && kc < 1)
                {
                    aout.push_back(kc);
                }
            }
            else if (is_zero(a3) && !is_zero(a4))
            {
                double ka = rate_line(c, d, a);
                if (0 < ka && ka < 1)
                {
                    bout.push_back(ka);
                }
            }
            else if (!is_zero(a3) && is_zero(a4))
            {
                double kb = rate_line(c, d, b);
                if (0 < kb && kb < 1)
                {
                    bout.push_back(kb);
                }
            }

            if (!aout.empty())
            {
                real t = aout[0];
                vout = (1 - t) * a + t * b;
                return true;
            }
            else if (!bout.empty())
            {
                real t = bout[0];
                vout = (1 - t) * c + t * d;
                return true;
            }
            else
            {
                return false;
            }
        }

        class stage_pred : public std::binary_function<edge_stage, edge_stage, bool>
        {
        public:
            bool operator()(const edge_stage& a, const edge_stage& b) const
            {
                return less(a, b);
            }
            static bool less(const edge_stage& a, const edge_stage& b)
            {
                if (a.sweep_x == b.sweep_x)
                {
                    if (a.sweep_x2 == b.sweep_x2)
                    {
                        return a.e < b.e;
                    }
                    else
                    {
                        return a.sweep_x2 < b.sweep_x2;
                    }
                }
                else
                {
                    return a.sweep_x2 < b.sweep_x2;
                }
            }
        };

        static bool is_chained(edge* e0, edge* e1)
        {
            return (e0->p[1] == e1->p[0] || e0->p[0] == e1->p[1]);
        }

        static void add_params(std::vector<real>& out, const std::vector<real>& in)
        {
            size_t sz = in.size();
            for (size_t i = 0; i < sz; i++)
            {
                if (0 < in[i] && in[i] < 1)
                {
                    out.push_back(in[i]);
                }
            }
        }

        static int unique_params(std::vector<real>& params)
        {
            if (params.size() >= 2)
            {
                std::sort(params.begin(), params.end());
                params.erase(std::unique(params.begin(), params.end()), params.end());
            }
            return (int)params.size();
        }

        static void get_points(std::vector<vector2>& out, const edge* e, const std::vector<real>& params)
        {
            vector2 p0 = *(e->p[0]);
            vector2 p1 = *(e->p[1]);

            for (size_t i = 0; i < params.size(); i++)
            {
                assert(0 < params[i] && params[i] < 1);
                out.push_back((1 - params[i]) * p0 + (params[i]) * p1);
            }
        }

        template <class IT>
        static IT prev_iter(IT it)
        {
            it--;
            return it;
        }

        template <class IT>
        static IT next_iter(IT it)
        {
            it++;
            return it;
        }

        static void test_intersection(std::map<edge*, std::vector<real> >& P, std::vector<edge>& E)
        {
            static const real EPSILON = values::epsilon();
            event_queue Q;
            size_t sz = E.size();
            //
            int id = 0;
            std::vector<edge*> H;
            for (size_t i = 0; i < sz; i++)
            {
                vector2 p0 = *(E[i].p[0]);
                vector2 p1 = *(E[i].p[1]);
                if (p0[1] < p1[1]) std::swap(p0, p1);
                assert(p0[1] >= p1[1]);
                if (fabs(p0[1] - p1[1]) > EPSILON)
                {
                    assert(p0[1] > p1[1]);
                    Q.push(edge_event(id++, EDGE_START, p0, &E[i]));
                    Q.push(edge_event(id++, EDGE_END, p1, &E[i]));
                }
                else
                {
                    if (p0[0] > p1[0]) std::swap(p0, p1);
                    Q.push(edge_event(id++, EDGE_HORIZONTAL, p1, &E[i]));
                    H.push_back(&E[i]);
                }
            }
            //------------------------------
            typedef std::set<edge*> edge_set;
            typedef std::set<edge_stage, stage_pred> stage_type;
            edge_set Es;

            vector2 vout;
            std::vector<real> aout;
            std::vector<real> bout;
            int count = 0;
            while (!Q.empty())
            {
                edge_event ev = Q.top();
                Q.pop();

                switch (ev.type)
                {
                case EDGE_START:
                {
                    stage_type S;
                    for (edge_set::iterator it = Es.begin(); it != Es.end(); it++)
                    {
                        edge* e = *it;
                        S.insert(edge_stage(e, ev.p[1]));
                    }
                    Es.insert(ev.e);
                    std::pair<stage_type::iterator, bool> ret = S.insert(edge_stage(ev.e, ev.p[1]));

                    stage_type::iterator it = ret.first;
                    if (it != S.end())
                    {
                        if (it != S.begin())
                        {
                            stage_type::iterator prev = it;
                            prev--;
                            edge* a = prev->e;
                            edge* b = it->e;
                            if (a != b)
                            {
                                aout.clear();
                                bout.clear();
                                if (!is_chained(a, b) && test_line_line(vout, aout, bout, *a->p[0], *a->p[1], *b->p[0], *b->p[1]))
                                {
                                    add_params(P[a], aout);
                                    add_params(P[b], bout);
                                    Q.push(edge_event(id++, EDGE_CROSS, vout, a, b));
                                }
                            }
                        }
                        stage_type::iterator next = it;
                        next++;
                        if (next != S.end())
                        {
                            edge* a = it->e;
                            edge* b = next->e;
                            if (a != b)
                            {
                                aout.clear();
                                bout.clear();
                                if (!is_chained(a, b) && test_line_line(vout, aout, bout, *a->p[0], *a->p[1], *b->p[0], *b->p[1]))
                                {
                                    add_params(P[a], aout);
                                    add_params(P[b], bout);
                                    Q.push(edge_event(id++, EDGE_CROSS, vout, a, b));
                                }
                            }
                        }
                    }
                }
                break;
                case EDGE_CROSS:
                { //-----------------------------------------------------------
                    stage_type S;
                    for (edge_set::iterator it = Es.begin(); it != Es.end(); it++)
                    {
                        edge* e = *it;
                        S.insert(edge_stage(e, ev.p[1]));
                    }

                    stage_type::iterator it1 = S.find(edge_stage(ev.e, ev.p[1]));
                    stage_type::iterator it2 = S.find(edge_stage(ev.e2, ev.p[1]));

                    if (it1 != S.end() && it2 != S.end())
                    {
                        //-------------------------------
                        if (!stage_pred::less(*it1, *it2))
                        {
                            std::swap(it1, it2);
                        }
                        //-------------------------------
                        { //left
                            stage_type::iterator ita = it1;
                            if (ita != S.begin())
                            {
                                stage_type::iterator itb = prev_iter(ita);
                                edge* a = itb->e;
                                edge* b = ev.e2;
                                if (a != ev.e && a != b)
                                {
                                    aout.clear();
                                    bout.clear();
                                    if (!is_chained(a, b) && test_line_line(vout, aout, bout, *a->p[0], *a->p[1], *b->p[0], *b->p[1]))
                                    {
                                        if (vout[1] < ev.p[1])
                                        {
                                            add_params(P[a], aout);
                                            add_params(P[b], bout);
                                            Q.push(edge_event(id++, EDGE_CROSS, vout, a, b));
                                        }
                                    }
                                }
                            }
                        }
                        { //right
                            stage_type::iterator ita = it2;
                            stage_type::iterator itb = next_iter(ita);
                            if (itb != S.end())
                            {
                                edge* a = ev.e;
                                edge* b = itb->e;
                                if (b != ev.e2 && a != b)
                                {
                                    aout.clear();
                                    bout.clear();
                                    if (!is_chained(a, b) && test_line_line(vout, aout, bout, *a->p[0], *a->p[1], *b->p[0], *b->p[1]))
                                    {
                                        if (vout[1] < ev.p[1])
                                        {
                                            add_params(P[a], aout);
                                            add_params(P[b], bout);
                                            Q.push(edge_event(id++, EDGE_CROSS, vout, a, b));
                                        }
                                    }
                                }
                            }
                        } //
                    }
                }
                break;
                case EDGE_HORIZONTAL:
                {
                    for (edge_set::iterator it = Es.begin(); it != Es.end(); it++)
                    {
                        edge* a = *it;
                        edge* b = ev.e;
                        assert(a != b);
                        aout.clear();
                        bout.clear();
                        if (!is_chained(a, b) && test_line_line(vout, aout, bout, *a->p[0], *a->p[1], *b->p[0], *b->p[1]))
                        {
                            add_params(P[a], aout);
                            add_params(P[b], bout);
                        }
                    }
                }
                break;
                case EDGE_END:
                {
                    stage_type S;
                    for (edge_set::iterator it = Es.begin(); it != Es.end(); it++)
                    {
                        edge* e = *it;
                        S.insert(edge_stage(e, ev.p[1]));
                    }
                    Es.erase(ev.e);

                    stage_type::iterator it = S.find(edge_stage(ev.e, ev.p[1]));

                    if (it != S.end())
                    {
                        if (it != S.begin())
                        {
                            stage_type::iterator ita = prev_iter(it);
                            stage_type::iterator itb = next_iter(it);
                            if (itb != S.end())
                            {
                                edge* a = ita->e;
                                edge* b = itb->e;
                                assert(a != ev.e && b != ev.e);
                                if (a != b)
                                {
                                    aout.clear();
                                    bout.clear();
                                    if (!is_chained(a, b) && test_line_line(vout, aout, bout, *a->p[0], *a->p[1], *b->p[0], *b->p[1]))
                                    {
                                        if (vout[1] < ev.p[1])
                                        {
                                            add_params(P[a], aout);
                                            add_params(P[b], bout);
                                            Q.push(edge_event(id++, EDGE_CROSS, vout, a, b));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                break;
                }

                count++;
            }
            //------------------------
            {
                size_t hsz = H.size();
                for (size_t i = 0; i < hsz; i++)
                {
                    for (size_t j = i + 1; j < hsz; j++)
                    {
                        edge* a = H[i];
                        edge* b = H[j];
                        aout.clear();
                        bout.clear();
                        if (!is_chained(a, b) && test_line_line(vout, aout, bout, *a->p[0], *a->p[1], *b->p[0], *b->p[1]))
                        {
                            add_params(P[a], aout);
                            add_params(P[b], bout);
                        }
                    }
                }
            }
        }
    }

    void intersect_loop(std::vector<vector2>& out, const std::vector<vector2>& v)
    {
        size_t sz = v.size();
        if (sz < 2) return;
        std::vector<edge> E(sz);
        //------------------------------
        {
            for (size_t i = 0; i < sz - 1; i++)
            {
                E[i].p[0] = &v[i];
                E[i].p[1] = &v[i + 1];
            }
            E[sz - 1].p[0] = &v[sz - 1];
            E[sz - 1].p[1] = &v[0];
        }
        typedef std::map<edge*, std::vector<real> > param_type;
        std::map<edge*, std::vector<real> > P;
        //------------------------------
        test_intersection(P, E);
        //------------------------------

        int cross_count = 0;
        //-----------------------------
        for (param_type::iterator i = P.begin(); i != P.end(); i++)
        {
            cross_count += unique_params(i->second);
        }
        //-----------------------------
        std::vector<vector2> tout;
        tout.reserve(sz + cross_count);
        for (size_t i = 0; i < sz; i++)
        {
            tout.push_back(*(E[i].p[0]));
            param_type::const_iterator it = P.find(&E[i]);
            if (it != P.end())
            {
                get_points(tout, &E[i], it->second);
            }
        }
        out.swap(tout);
    }

    void intersect_loop(std::vector<vector2>& inout)
    {
#ifdef DEBUG_PRINT
        timer t;
        t.start();
#endif
        std::vector<vector2> tmp;
        intersect_loop(tmp, inout);
        inout.swap(tmp);
#ifdef DEBUG_PRINT
        t.end();
        print_debug("intersect_loop:%d ms\n", t.msec());
#endif
    }

    void intersect_loop(std::vector<std::vector<vector2> >& out, const std::vector<std::vector<vector2> >& loops)
    {
        size_t lsz = loops.size();
        std::vector<edge> E;
        for (size_t j = 0; j < lsz; j++)
        {
            const std::vector<vector2>& v = loops[j];
            size_t sz = v.size();
            edge e;
            for (size_t i = 0; i < sz - 1; i++)
            {
                e.p[0] = &v[i];
                e.p[1] = &v[i + 1];
                E.push_back(e);
            }
            e.p[0] = &v[sz - 1];
            e.p[1] = &v[0];
            E.push_back(e);
        }

        typedef std::map<edge*, std::vector<real> > param_type;
        std::map<edge*, std::vector<real> > P;
        //------------------------------
        test_intersection(P, E);
        //------------------------------

        int cross_count = 0;
        //-----------------------------
        for (param_type::iterator i = P.begin(); i != P.end(); i++)
        {
            cross_count += unique_params(i->second);
        }
        //-----------------------------
        out.resize(lsz);
        size_t total = 0;
        for (size_t j = 0; j < lsz; j++)
        {
            const std::vector<vector2>& v = loops[j];
            size_t sz = v.size();
            std::vector<vector2> tout;
            tout.reserve(sz);
            for (size_t i = total; i < total + sz; i++)
            {
                tout.push_back(*(E[i].p[0]));
                param_type::const_iterator it = P.find(&E[i]);
                if (it != P.end())
                {
                    get_points(tout, &E[i], it->second);
                }
            }
            out[j].swap(tout);
            total += sz;
        }
    }

    void intersect_loop(std::vector<std::vector<vector2> >& inout)
    {
        std::vector<std::vector<vector2> > tmp;
        intersect_loop(tmp, inout);
        inout.swap(tmp);
    }

    void intersect_lines(std::vector<std::pair<vector2, vector2> >& out, const std::vector<std::pair<vector2, vector2> >& v)
    {
        size_t sz = v.size();
        if (sz < 2) return;
        std::vector<edge> E(sz);
        //------------------------------
        {
            for (size_t i = 0; i < sz; i++)
            {
                E[i].p[0] = &(v[i].first);
                E[i].p[1] = &(v[i].second);
            }
        }
        typedef std::map<edge*, std::vector<real> > param_type;
        std::map<edge*, std::vector<real> > P;
        //------------------------------
        test_intersection(P, E);
        //------------------------------
        int cross_count = 0;
        //-----------------------------
        for (param_type::iterator i = P.begin(); i != P.end(); i++)
        {
            cross_count += unique_params(i->second);
        }
        //-----------------------------
        std::vector<std::pair<vector2, vector2> > tout;
        tout.reserve(sz + cross_count);
        std::vector<vector2> points;
        for (size_t i = 0; i < sz; i++)
        {
            points.clear();
            points.push_back(*(E[i].p[0]));

            param_type::const_iterator it = P.find(&E[i]);
            if (it != P.end())
            {
                get_points(points, &E[i], it->second);
            }

            points.push_back(*(E[i].p[1]));
            size_t psz = points.size();
            for (size_t j = 0; j < psz - 1; j++)
            {
                tout.push_back(std::make_pair(points[j], points[j + 1]));
            }
        }
        out.swap(tout);
    }

    void intersect_lines(std::vector<std::pair<vector2, vector2> >& inout)
    {
        std::vector<std::pair<vector2, vector2> > tmp;
        intersect_lines(tmp, inout);
        inout.swap(tmp);
    }
}