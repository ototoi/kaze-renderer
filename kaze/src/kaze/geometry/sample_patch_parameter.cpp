#include "sample_patch_parameter.h"
#include "values.h"
//#include "logger.h"

#include <algorithm>
#include <cmath>

namespace kaze
{
    namespace
    {
        template <class CURVE>
        class distance_func2
        {
        public:
            distance_func2(real d) : distance_(d) {}
            bool operator()(const CURVE& curve, real t0, real t1) const
            {
                typedef typename CURVE::value_type value_type;
                value_type p0 = curve.evaluate(t0);
                value_type p1 = curve.evaluate(t1);
                value_type ppm = (p0 + p1) * real(0.5);
                real tm = (t0 + t1) * real(0.5);
                value_type ptm = curve.evaluate(tm);
                real l = length(ptm - ppm);
                return (l <= distance_);
            }

        private:
            real distance_;
        };

        template <class CURVE, class PATCH>
        class distance_func3
        {
        public:
            distance_func3(real d, const PATCH& p) : distance_(d), patch_(p) {}
            bool operator()(const CURVE& curve, real t0, real t1) const
            {
                typedef typename CURVE::value_type value_type2;
                typedef typename PATCH::value_type value_type3;
                value_type2 p0 = curve.evaluate(t0);
                value_type2 p1 = curve.evaluate(t1);
                value_type2 ppm = (p0 + p1) * real(0.5);
                real tm = (t0 + t1) * real(0.5);
                value_type2 ptm = curve.evaluate(tm);
                value_type3 ppm3 = patch_.evaluate(ppm[0], ppm[1]);
                value_type3 ptm3 = patch_.evaluate(ptm[0], ptm[1]);
                real distance = distance_;
                if (distance < length(ptm3 - ppm3)) return false;
                value_type3 p03 = patch_.evaluate(p0[0], p0[1]);
                value_type3 p13 = patch_.evaluate(p1[0], p1[1]);
                value_type3 pm3 = (p03 + p13) * real(0.5);
                if (distance < length(ptm3 - pm3)) return false;

                return true;
            }

        private:
            real distance_;
            const PATCH& patch_;
        };

        template <class CURVE, class PATCH>
        class distance_func3a
        { //angle
        public:
            typedef typename CURVE::value_type value_type2;
            typedef typename PATCH::value_type value_type3;
            typedef typename value_type3::value_type element_type;

        public:
            distance_func3a(real d, const PATCH& p, real angle) : distance_(d), patch_(p)
            {
                cos_ = std::cos(angle);
            }
            bool operator()(const CURVE& curve, real t0, real t1) const
            {
                value_type2 p0 = curve.evaluate(t0);
                value_type2 p1 = curve.evaluate(t1);
                real tm = (t0 + t1) * real(0.5);
                value_type2 ptm = curve.evaluate(tm);
                value_type3 ptm3 = patch_.evaluate(ptm[0], ptm[1]);

                value_type3 p03 = patch_.evaluate(p0[0], p0[1]);
                value_type3 p13 = patch_.evaluate(p1[0], p1[1]);
                value_type3 pm3 = (p03 + p13) * real(0.5);

                real distance = distance_;
                if (distance < length(ptm3 - pm3)) return false;
                if (!check_angle(p03, ptm3, p13)) return false;

                return true;
            }

        protected:
            bool check_angle(const value_type3& p0, const value_type3& pm, const value_type3& p1) const
            {
                if (length(p1 - p0) <= distance_)
                {
                    return true;
                }
                else
                {
                    if (!check_angle(pm - p0, p1 - p0)) return false;
                    if (!check_angle(pm - p1, p0 - p1)) return false;
                    return true;
                }
            }
            bool check_angle(const value_type3& v0, const value_type3& v1) const
            {
                static const real EPSILON = values::epsilon() * 8;

                real l0 = v0.length();
                real l1 = v1.length();
                if (l0 <= EPSILON || l1 <= EPSILON)
                {
                    return true;
                }
                value_type3 n0 = v0 * (real(1) / l0);
                value_type3 n1 = v1 * (real(1) / l1);

                real cos = dot(n0, n1);
                if (cos >= cos_)
                {
                    return true;
                }
                return false;
            }

        private:
            real distance_;
            const PATCH& patch_;
            real cos_;
        };

        template <class PATCH>
        class distance_func3p
        { //patch
        public:
            typedef typename PATCH::value_type value_type3;
            typedef typename value_type3::value_type element_type;

        public:
            distance_func3p(real d, const PATCH& p, real angle, int nu, int nv)
                : distance_(d), patch_(p), nu_(nu), nv_(nv)
            {
                cos_ = std::cos(angle);
            }

            bool operator()(real u0, real u1, real v0, real v1) const
            {
                return (check(u0, u1, v0, v1) && check_normal(u0, u1, v0, v1));
            }

            static int get_sample(int nu, int level)
            {
                //return nu;

                if (level < 0 || level > 6)
                {
                    return 1;
                }
                else
                {
                    return (int)ceil(double(nu) / (1 << level));
                }
            }

            bool check_u(real u0, real u1, real v0, real v1, int levelU, int levelV) const
            {
                int nSampleU = get_sample(nu_, levelU);
                //int nSampleV = get_sample(nv_,levelV);

                if (nSampleU <= 2)
                {
                    if (!check_u(u0, u1, v0, v1, real(0.0), real(0.5))) return false;
                    if (!check_u(u0, u1, v0, v1, real(0.5), real(0.5))) return false;
                    if (!check_u(u0, u1, v0, v1, real(1.0), real(0.5))) return false;
                    return true;
                }
                else
                {
                    real du = real(1) / (nSampleU - 1);
                    for (int u = 0; u < nSampleU; u++)
                    {
                        real uu = u * du;
                        if (!check_u(u0, u1, v0, v1, uu, real(0.0))) return false;
                        if (!check_u(u0, u1, v0, v1, uu, real(0.5))) return false;
                        if (!check_u(u0, u1, v0, v1, uu, real(1.0))) return false;
                    }

                    return true;
                }
            }

            bool check_v(real u0, real u1, real v0, real v1, int levelU, int levelV) const
            {
                //int nSampleU = get_sample(nu_,levelU);
                int nSampleV = get_sample(nv_, levelV);

                if (nSampleV <= 2)
                {
                    if (!check_v(u0, u1, v0, v1, real(0.5), real(0.0))) return false;
                    if (!check_v(u0, u1, v0, v1, real(0.5), real(0.5))) return false;
                    if (!check_v(u0, u1, v0, v1, real(0.5), real(1.0))) return false;
                    return true;
                }
                else
                {
                    real dv = real(1) / (nSampleV - 1);
                    for (int v = 0; v < nSampleV; v++)
                    {
                        real vv = v * dv;
                        if (!check_v(u0, u1, v0, v1, real(0.0), vv)) return false;
                        if (!check_v(u0, u1, v0, v1, real(0.5), vv)) return false;
                        if (!check_v(u0, u1, v0, v1, real(1.0), vv)) return false;
                    }
                    return true;
                }
            }

            bool check_u(real u0, real u1, real v0, real v1, real t, real s) const
            {
                real um = (u0 * (1 - t) + u1 * t);
                real vm = (v0 * (1 - s) + v1 * s);
                value_type3 p0 = patch_.evaluate(u0, vm);
                value_type3 pm = patch_.evaluate(um, vm);
                value_type3 p1 = patch_.evaluate(u1, vm);

                real distance = distance_;
                if (distance < get_distance(pm, (p0 * (1 - t) + p1 * t))) return false; //center
                if (!check_angle(p0, pm, p1)) return false;                             //---
                return true;
            }

            bool check_v(real u0, real u1, real v0, real v1, real t, real s) const
            {
                real um = (u0 * (1 - t) + u1 * t);
                real vm = (v0 * (1 - s) + v1 * s);
                value_type3 p0 = patch_.evaluate(um, v0);
                value_type3 pm = patch_.evaluate(um, vm);
                value_type3 p1 = patch_.evaluate(um, v1);

                real distance = distance_;
                if (distance < get_distance(pm, (p0 * (1 - s) + p1 * s))) return false; //center
                if (!check_angle(p0, pm, p1)) return false;                             //---
                return true;
            }

            static real get_dist(
                const vector3& p1, const vector3& p2,
                const vector3& p3, const vector3& p4)
            {
                static const real EPS = values::epsilon() * 1024;
                vector3 v1 = (p2 - p1);
                vector3 v2 = (p4 - p3);
                real l1 = length(v1);
                real l2 = length(v2);
                if (l1 < EPS || l2 < EPS) return 0;
                l1 = (1 / l1);
                l2 = (1 / l2);
                v1 *= l1;
                v2 *= l2;

                real D1 = dot(p3 - p1, v1);
                real D2 = dot(p3 - p1, v2);
                real Dv = dot(v1, v2);

                real k = (1 - Dv * Dv);
                if (fabs(k) < EPS) return 0;

                real t1 = (D1 - D2 * Dv) / k * l1;
                real t2 = -(D2 - D1 * Dv) / k * l2;

                if (t1 < 0)
                {
                    t1 = 0;
                }
                else if (t1 > 1)
                {
                    t1 = 1;
                }
                if (t2 < 0)
                {
                    t2 = 0;
                }
                else if (t2 > 1)
                {
                    t2 = 1;
                }

                vector3 Q1 = p1 * (1 - t1) + p2 * t1;
                vector3 Q2 = p3 * (1 - t2) + p4 * t2;

                return length(Q1 - Q2);
            }

            bool check_twist(real u0, real u1, real v0, real v1) const
            {
                real um = (u0 + u1) * real(0.5);
                real vm = (v0 + v1) * real(0.5);

                value_type3 p00 = patch_.evaluate(u0, v0);
                value_type3 p01 = patch_.evaluate(u0, v1);
                value_type3 p10 = patch_.evaluate(u1, v0);
                value_type3 p11 = patch_.evaluate(u1, v1);

                real l = get_dist(p00, p11, p01, p10);
                real distance = distance_;
                if (distance < l) return false;

                value_type3 pmm = patch_.evaluate(um, vm);

                bool bDegen = false;
                {
                    if (distance > get_distance(p00, p10)) bDegen = true;
                    if (distance > get_distance(p01, p11)) bDegen = true;
                    if (distance > get_distance(p00, p01)) bDegen = true;
                    if (distance > get_distance(p10, p11)) bDegen = true;
                }

                if (!bDegen)
                {
                    value_type3 pxm = (p00 + p11 + p01 + p10) * real(0.25);
                    if (distance * 2 < get_distance(pmm, pxm)) return false;
                    //real la = get_distance(p00,p11);
                    //real lb = get_distance(p01,p10);

                    //real dd1 = std::max(distance    , std::max(la,lb)*0.75);
                    //real dd2 = std::max(distance*0.5, (la+lb)*0.5*0.5*0.5);

                    //if(fabs(la-lb)>dd1)return false;

                    //value_type3 pa = (p00+p11)*real(0.5);
                    //value_type3 pb = (p01+p10)*real(0.5);

                    //if(distance < get_distance(pmm, pxm))return false;
                    //if(distance < get_distance(pa, pb))return false;
                    //if(distance*2 < get_distance(pmm, pa))return false;
                    //if(distance*2 < get_distance(pmm, pb))return false;

                    if (!check_angle(p00, pmm, p11)) return false;
                    if (!check_angle(p01, pmm, p10)) return false;
                }
                return true;
            }

            bool check_normal(real u0, real u1, real v0, real v1) const
            {
                real um = (u0 + u1) * real(0.5);
                real vm = (v0 + v1) * real(0.5);

                value_type3 u00 = patch_.evaluate_deriv_u(u0, v0);
                value_type3 u10 = patch_.evaluate_deriv_u(u1, v0);
                value_type3 u01 = patch_.evaluate_deriv_u(u0, v1);
                value_type3 u11 = patch_.evaluate_deriv_u(u1, v1);

                value_type3 um0 = patch_.evaluate_deriv_u(um, v0); //top
                value_type3 u0m = patch_.evaluate_deriv_u(u0, vm); //left
                value_type3 umm = patch_.evaluate_deriv_u(um, vm); //center
                value_type3 u1m = patch_.evaluate_deriv_u(u1, vm); //right
                value_type3 um1 = patch_.evaluate_deriv_u(um, v1); //bootom

                if (!check_angle(um0, u00)) return false;
                if (!check_angle(um0, u10)) return false;

                if (!check_angle(umm, u0m)) return false;
                if (!check_angle(umm, u1m)) return false;

                if (!check_angle(um1, u01)) return false;
                if (!check_angle(um1, u11)) return false;

                if (!check_angle(umm, u00)) return false;
                if (!check_angle(umm, u11)) return false;
                if (!check_angle(umm, u01)) return false;
                if (!check_angle(umm, u11)) return false;

                if (!check_angle(u00, u10)) return false;
                if (!check_angle(u00, u01)) return false;
                if (!check_angle(u00, u11)) return false;
                if (!check_angle(u10, u01)) return false;
                if (!check_angle(u10, u11)) return false;
                if (!check_angle(u01, u11)) return false;

                value_type3 v00 = patch_.evaluate_deriv_v(u0, v0);
                value_type3 v10 = patch_.evaluate_deriv_v(u1, v0);
                value_type3 v01 = patch_.evaluate_deriv_v(u0, v1);
                value_type3 v11 = patch_.evaluate_deriv_v(u1, v1);

                value_type3 vm0 = patch_.evaluate_deriv_v(um, v0); //top
                value_type3 v0m = patch_.evaluate_deriv_v(u0, vm); //left
                value_type3 vmm = patch_.evaluate_deriv_v(um, vm); //center
                value_type3 v1m = patch_.evaluate_deriv_v(u1, vm); //right
                value_type3 vm1 = patch_.evaluate_deriv_v(um, v1); //bootom

                if (!check_angle(vm0, v00)) return false;
                if (!check_angle(vm0, v10)) return false;

                if (!check_angle(vmm, v0m)) return false;
                if (!check_angle(vmm, v1m)) return false;

                if (!check_angle(vm1, v01)) return false;
                if (!check_angle(vm1, v11)) return false;

                if (!check_angle(vmm, v00)) return false;
                if (!check_angle(vmm, v11)) return false;
                if (!check_angle(vmm, v01)) return false;
                if (!check_angle(vmm, v11)) return false;

                if (!check_angle(v00, v10)) return false;
                if (!check_angle(v00, v01)) return false;
                if (!check_angle(v00, v11)) return false;
                if (!check_angle(v10, v01)) return false;
                if (!check_angle(v10, v11)) return false;
                if (!check_angle(v01, v11)) return false;

                return true;
            }
            bool check(real u0, real u1, real v0, real v1) const
            {
                real um = (u0 + u1) * real(0.5);
                real vm = (v0 + v1) * real(0.5);
                value_type3 p00 = patch_.evaluate(u0, v0);
                value_type3 p10 = patch_.evaluate(u1, v0);
                value_type3 p01 = patch_.evaluate(u0, v1);
                value_type3 p11 = patch_.evaluate(u1, v1);

                value_type3 pm0 = patch_.evaluate(um, v0);
                value_type3 p0m = patch_.evaluate(u0, vm);
                value_type3 pmm = patch_.evaluate(um, vm);
                value_type3 p1m = patch_.evaluate(u1, vm);
                value_type3 pm1 = patch_.evaluate(um, v1);

                real distance = distance_;
                if (distance < get_distance(pm0, (p00 + p10) * real(0.5))) return false;              //top
                if (distance < get_distance(p0m, (p00 + p01) * real(0.5))) return false;              //left
                if (distance < get_distance(pmm, (p00 + p01 + p10 + p11) * real(0.25))) return false; //center
                if (distance < get_distance(p1m, (p10 + p11) * real(0.5))) return false;              //right
                if (distance < get_distance(pm1, (p01 + p11) * real(0.5))) return false;              //bottom

                if (!check_angle(p00, p0m, p01)) return false; //|
                if (!check_angle(pm0, pmm, pm1)) return false; // |
                if (!check_angle(p10, p1m, p11)) return false; //  |

                if (!check_angle(p00, pm0, p10)) return false; //---
                if (!check_angle(p0m, pmm, p1m)) return false; //---
                if (!check_angle(p01, pm1, p11)) return false; //---

                return true;
            }

        protected:
            static real get_distance(const value_type3& a, const value_type3& b)
            {
                return length(b - a);
            }
            bool check_angle(const value_type3& p0, const value_type3& pm, const value_type3& p1) const
            {
                if (length(p1 - p0) <= distance_)
                {
                    return true;
                }
                else
                {
                    if (!check_angle(pm - p0, p1 - p0)) return false;
                    if (!check_angle(pm - p1, p0 - p1)) return false;
                    return true;
                }
            }
            bool check_angle(const value_type3& v0, const value_type3& v1) const
            {
                static const real EPSILON = values::epsilon() * 8;

                real l0 = v0.length();
                real l1 = v1.length();
                if (l0 <= EPSILON || l1 <= EPSILON)
                {
                    return true;
                }
                value_type3 n0 = v0 * (real(1) / l0);
                value_type3 n1 = v1 * (real(1) / l1);

                real cos = dot(n0, n1);
                if (cos >= cos_)
                {
                    return true;
                }
                return false;
            }

        private:
            real distance_;
            const PATCH& patch_;
            real cos_;
            int nu_;
            int nv_;
        };
    }

    template <class CURVE, class FUNC>
    static void push_sample_parameter(std::vector<real>& out, const CURVE& curve, real t0, real t1, const FUNC& f)
    {
        if (!f(curve, t0, t1))
        {
            real tm = (t0 + t1) * real(0.5);
            push_sample_parameter(out, curve, t0, tm, f);
            out.push_back(tm);
            push_sample_parameter(out, curve, tm, t1, f);
        }
    }

    template <class CURVE>
    void create_sample_parameter_(std::vector<real>& out, const CURVE& curve, const std::vector<real>& params, real distance)
    {
        size_t sz = params.size();
        if (sz <= 1)
        {
            out = params;
            return;
        }
        distance_func2<CURVE> f(distance);
        for (size_t i = 0; i < sz - 1; i++)
        {
            out.push_back(params[i]);
            push_sample_parameter(out, curve, params[i], params[i + 1], f);
        }
        out.push_back(params.back());
    }

    template <class CURVE, class PATCH>
    void create_sample_parameter_(std::vector<real>& out, const CURVE& curve, const PATCH& patch, const std::vector<real>& params, real distance)
    {
        size_t sz = params.size();
        if (sz <= 1)
        {
            out = params;
            return;
        }
        distance_func3<CURVE, PATCH> f(distance, patch);
        for (size_t i = 0; i < sz - 1; i++)
        {
            out.push_back(params[i]);
            push_sample_parameter(out, curve, params[i], params[i + 1], f);
        }
        out.push_back(params.back());
    }

    template <class CURVE, class PATCH>
    void create_sample_parameter_(std::vector<real>& out, const CURVE& curve, const PATCH& patch, const std::vector<real>& params, real distance, real angle)
    {
        size_t sz = params.size();
        if (sz <= 1)
        {
            out = params;
            return;
        }
        distance_func3a<CURVE, PATCH> f(distance, patch, angle);

        for (size_t i = 0; i < sz - 1; i++)
        {
            out.push_back(params[i]);
            push_sample_parameter(out, curve, params[i], params[i + 1], f);
        }
        out.push_back(params.back());
    }

    template <class FUNC>
    static void push_sample_parameter(std::vector<vector2>& out, real u0, real u1, real v0, real v1, const FUNC& f, int nCloseFlag, int levelU, int levelV)
    {
        int nFlag = 0;
        //if(!f.check_u(u0,u1,v0,v1,levelU,levelV))nFlag |= 1;
        //if(!f.check_v(u0,u1,v0,v1,levelU,levelV))nFlag |= 2;

        if (!f.check_twist(u0, u1, v0, v1))
        {
            nFlag |= 3;
        }
        else
        {
            if (!f.check_u(u0, u1, v0, v1, levelU, levelV)) nFlag |= 1;
            if (!f.check_v(u0, u1, v0, v1, levelU, levelV)) nFlag |= 2;
        }
        /*
        if(nFlag==0)
        {
            if(!f.check_twist(u0,u1,v0,v1)){
                nFlag |= 3;
            }
        }
        */

        switch (nFlag)
        {
        case 0:
        {
            //                   out.push_back(vector2(u0,v0));//LU
            if (nCloseFlag & 0x1) out.push_back(vector2(u1, v0));  //RU
            if (nCloseFlag & 0x2) out.push_back(vector2(u0, v1));  //LB
            if (nCloseFlag == 0x3) out.push_back(vector2(u1, v1)); //RB
        }
        break;
        case 1:
        {
            levelU++;
            //levelV=100;

            real um = (u0 + u1) * real(0.5);
            push_sample_parameter(out, u0, um, v0, v1, f, nCloseFlag & 0x2, levelU, levelV);
            out.push_back(vector2(um, v0)); //MU
            push_sample_parameter(out, um, u1, v0, v1, f, nCloseFlag & 0x3, levelU, levelV);
        }
        break;
        case 2:
        {
            //levelU=100;
            levelV++;

            real vm = (v0 + v1) * real(0.5);
            push_sample_parameter(out, u0, u1, v0, vm, f, nCloseFlag & 0x1, levelU, levelV);
            out.push_back(vector2(u0, vm)); //MU
            push_sample_parameter(out, u0, u1, vm, v1, f, nCloseFlag & 0x3, levelU, levelV);
        }
        break;
        default:
        {
            levelU++;
            levelV++;

            real um = (u0 + u1) * real(0.5);
            real vm = (v0 + v1) * real(0.5);
            push_sample_parameter(out, u0, um, v0, vm, f, nCloseFlag & 0x0, levelU, levelV); //[L-M U-M]
            out.push_back(vector2(um, v0));                                                  //MU
            push_sample_parameter(out, um, u1, v0, vm, f, nCloseFlag & 0x1, levelU, levelV); //[M-R U-M]
            out.push_back(vector2(u0, vm));                                                  //LM
            push_sample_parameter(out, u0, um, vm, v1, f, nCloseFlag & 0x2, levelU, levelV); //[L-M M-B]
            out.push_back(vector2(um, vm));                                                  //MM
            push_sample_parameter(out, um, u1, vm, v1, f, nCloseFlag & 0x3, levelU, levelV); //[M-R M-B]
        }
        break;
        }
    }

    template <class PATCH>
    void create_sample_parameter_(std::vector<vector2>& out, const PATCH& patch, const std::vector<real>& u_params, const std::vector<real>& v_params, real distance, real angle, int nu, int nv)
    {
        size_t usz = u_params.size();
        size_t vsz = v_params.size();

        distance_func3p<PATCH> f(distance, patch, angle, nu, nv);
        for (size_t v = 0; v < vsz - 1; v++)
        {
            real v0 = v_params[v + 0];
            real v1 = v_params[v + 1];
            int nFlagV = 0;
            if (v == vsz - 2) nFlagV = 2;
            for (size_t u = 0; u < usz - 1; u++)
            {
                real u0 = u_params[u + 0];
                real u1 = u_params[u + 1];

                int nFlagU = 0;
                if (u == usz - 2) nFlagU = 1;
                out.push_back(vector2(u0, v0)); //LU
                push_sample_parameter(out, u0, u1, v0, v1, f, nFlagU | nFlagV, 0, 0);
            }
        }
    }

    template <class V>
    void create_sample_parameter_(std::vector<vector2>& out, const bezier_patch<V>& patch, real distance, real angle)
    {
        static const real knots_[] = {0, 1};
        static const std::vector<real> knots(knots_, knots_ + 2);

        int nu = patch.get_nu();
        int nv = patch.get_nv();
        create_sample_parameter_(out, patch, knots, knots, distance, angle, nu, nv);
    }

    static std::vector<real> get_space_knots(const std::vector<real>& knots_org, const std::vector<real>& knots_spn)
    {
        size_t sz = knots_spn.size();
        std::vector<real> out = knots_spn;
        for (int i = 1; i < (int)sz - 1; i++)
        {
            int k = -1;
            real d_max = 1;
            real wa = (knots_spn[i] - knots_spn[i - 1]) / 3;
            real wb = (knots_spn[i + 1] - knots_spn[i]) / 3;
            for (int j = 1; j < (int)knots_org.size() - 1; j++)
            {
                if (knots_org[j] < knots_spn[i])
                {
                    real diff = knots_spn[i] - knots_org[j];
                    if (diff < d_max)
                    {
                        if (diff < wa)
                        {
                            d_max = diff;
                            k = j;
                        }
                    }
                }
                else
                {
                    real diff = knots_org[j] - knots_spn[i];
                    if (diff < d_max)
                    {
                        if (diff < wb)
                        {
                            d_max = diff;
                            k = j;
                        }
                    }
                }
            }
            if (k > 0)
            {
                out[i] = knots_org[k];
            }
        }
        return out;
    }

    static std::vector<real> get_span_knots(const std::vector<real>& knots)
    {
        size_t n = knots.size();
        std::vector<real> tmp(n);
        real a = knots[0];
        real b = knots[n - 1];
        real delta = (b - a) / (n - 1);
        tmp[0] = a;
        for (int i = 1; i < n - 1; i++)
        {
            tmp[i] = i * delta;
        }
        tmp[n - 1] = b;
        return tmp;
    }

    template <class V>
    static std::vector<real> get_clean_knots_u(const multi_bezier_patch<V>& patch, real distance)
    {
        //static const real EPS = 1.0/100;
        const std::vector<real>& knots_u = patch.get_knots_u();
        const std::vector<real>& knots_v = patch.get_knots_v();
        int sz = (int)knots_u.size();
        if (sz <= 2)
        {
            return knots_u;
        }
        std::vector<bool> bOut(sz);
        bOut[0] = true;
        bOut[sz - 1] = true;
        for (size_t i = 1; i < sz - 1; i++)
        {
            real t0 = knots_u[i - 1];
            real tm = knots_u[i];
            real t1 = knots_u[i + 1];

            //real d0 = fabs(tm-t0);
            //real d1 = fabs(t1-t1);
            //if(d0 < EPS || d1 < EPS){
            bool b = false;
            for (size_t j = 0; j < knots_v.size(); j++)
            {
                real s = knots_v[j];
                V p0 = patch.evaluate(t0, s);
                V pm = patch.evaluate(tm, s);
                V p1 = patch.evaluate(t1, s);
                real l0 = length(p0 - pm);
                real l1 = length(p1 - pm);
                if (!(l0 < distance || l1 < distance))
                {
                    b = true;
                    break;
                }
            }
            bOut[i] = b;
            //}else{
            //	bOut[i] = true;
            //}
        }
        std::vector<real> tmp;
        tmp.reserve(sz);
#if 0
        for(size_t i = 0;i<sz;i++){
            if(bOut[i]){
                tmp.push_back(knots_u[i]);
            }
        }
#else
        int nFirstOut = -1;
        for (int i = 0; i < sz; i++)
        {
            if (bOut[i])
            {
                int nDelta = i - nFirstOut;
                if (nDelta >= 3)
                {
                    real knotAvg = 0;
                    for (int j = nFirstOut + 1; j < i; j++)
                    {
                        knotAvg += knots_u[j];
                    }
                    knotAvg /= (nDelta - 1);
                    tmp.push_back(knotAvg);
                    //print_log("%d:%f %d:%f -> %f\n",nFirstOut,knots[nFirstOut], i, knots[i], knotAvg);
                }
                tmp.push_back(knots_u[i]);
                nFirstOut = i;
            }
        }
#endif
        return tmp;
    }

    template <class V>
    static std::vector<real> get_clean_knots_v(const multi_bezier_patch<V>& patch, real distance)
    {
        //static const real EPS = 1.0/100;
        const std::vector<real>& knots_u = patch.get_knots_u();
        const std::vector<real>& knots_v = patch.get_knots_v();
        int sz = (int)knots_v.size();
        if (sz <= 2)
        {
            return knots_v;
        }
        std::vector<bool> bOut(sz);
        bOut[0] = true;
        bOut[sz - 1] = true;
        for (int i = 1; i < sz - 1; i++)
        {
            real t0 = knots_v[i - 1];
            real tm = knots_v[i];
            real t1 = knots_v[i + 1];

            //real d0 = fabs(tm-t0);
            //real d1 = fabs(t1-t1);
            //if(d0 < EPS || d1 < EPS){
            bool b = false;
            for (size_t j = 0; j < knots_v.size(); j++)
            {
                real s = knots_v[j];
                V p0 = patch.evaluate(s, t0);
                V pm = patch.evaluate(s, tm);
                V p1 = patch.evaluate(s, t1);
                real l0 = length(p0 - pm);
                real l1 = length(p1 - pm);
                if (!(l0 < distance || l1 < distance))
                {
                    b = true;
                    break;
                }
            }
            bOut[i] = b;
            //}else{
            //	bOut[i] = true;
            //}
        }
        std::vector<real> tmp;
        tmp.reserve(sz);
#if 0
        for(size_t i = 0;i<sz;i++){
            if(bOut[i]){
                tmp.push_back(knots_v[i]);
            }
        }
#else
        int nFirstOut = -1;
        for (int i = 0; i < sz; i++)
        {
            if (bOut[i])
            {
                int nDelta = i - nFirstOut;
                if (nDelta >= 3)
                {
                    real knotAvg = 0;
                    for (int j = nFirstOut + 1; j < i; j++)
                    {
                        knotAvg += knots_v[j];
                    }
                    knotAvg /= (nDelta - 1);
                    tmp.push_back(knotAvg);
                    //print_log("%d:%f %d:%f -> %f\n",nFirstOut,knots[nFirstOut], i, knots[i], knotAvg);
                }
                tmp.push_back(knots_v[i]);
                nFirstOut = i;
            }
        }
#endif
        return tmp;
    }

    template <class V>
    static std::vector<real> get_valid_knots_u_(const multi_bezier_patch<V>& patch, real distance)
    {
        //return get_span_knots(get_clean_knots_u(patch, distance));
        return get_space_knots(patch.get_knots_u(), get_span_knots(get_clean_knots_u(patch, distance)));
    }

    template <class V>
    static std::vector<real> get_valid_knots_v_(const multi_bezier_patch<V>& patch, real distance)
    {
        //return get_span_knots(get_clean_knots_v(patch, distance));
        return get_space_knots(patch.get_knots_v(), get_span_knots(get_clean_knots_v(patch, distance)));
    }

    template <class V>
    void create_sample_parameter_(std::vector<vector2>& out, const multi_bezier_patch<V>& patch, real distance, real angle)
    {
        int nu = patch.get_patch_at(0, 0).get_nu();
        int nv = patch.get_patch_at(0, 0).get_nv();
        std::vector<real> knots_u = get_valid_knots_u_(patch, distance);
        std::vector<real> knots_v = get_valid_knots_v_(patch, distance);
        create_sample_parameter_(out, patch, knots_u, knots_v, distance, angle, nu, nv);
    }

    static std::vector<real> get_clean_knots(const multi_bezier_curve<vector2>& curve, const multi_bezier_patch<vector3>& patch, real distance)
    {
        const std::vector<real>& knots = curve.get_knots();
        size_t sz = knots.size();
        if (sz <= 2)
        {
            return knots;
        }
        std::vector<bool> bOut(sz);
        bOut[0] = true;
        bOut[sz - 1] = true;
        for (size_t i = 1; i < sz - 1; i++)
        {
            real t0 = knots[i - 1];
            real tm = knots[i];
            real t1 = knots[i + 1];
            vector2 p20 = curve.evaluate(t0);
            vector2 p2m = curve.evaluate(tm);
            vector2 p21 = curve.evaluate(t1);
            vector3 p30 = patch.evaluate(p20[0], p20[1]);
            vector3 p3m = patch.evaluate(p2m[0], p2m[1]);
            vector3 p31 = patch.evaluate(p21[0], p21[1]);
            real l0 = length(p30 - p3m);
            real l1 = length(p31 - p3m);
            if (!(l0 < distance || l1 < distance))
            {
                bOut[i] = true;
            }
        }

        std::vector<real> tmp;
        tmp.reserve(sz);
#if 0
        for(size_t i = 0;i<sz;i++){
            if(bOut[i]){
                tmp.push_back(knots[i]);
            }
        }
#else
        int nFirstOut = -1;
        for (int i = 0; i < sz; i++)
        {
            if (bOut[i])
            {
                int nDelta = i - nFirstOut;
                if (nDelta >= 3)
                {
                    real knotAvg = 0;
                    for (int j = nFirstOut + 1; j < i; j++)
                    {
                        knotAvg += knots[j];
                    }
                    knotAvg /= (nDelta - 1);
                    tmp.push_back(knotAvg);
                    //print_log("%d:%f %d:%f -> %f\n",nFirstOut,knots[nFirstOut], i, knots[i], knotAvg);
                }
                tmp.push_back(knots[i]);
                nFirstOut = i;
            }
        }
#endif

        return tmp;
    }

    static std::vector<real> get_valid_knots_(const multi_bezier_curve<vector2>& curve, const multi_bezier_patch<vector3>& patch, real distance)
    {
        //return get_span_knots(get_clean_knots(curve, patch, distance));
        return get_space_knots(curve.get_knots(), get_span_knots(get_clean_knots(curve, patch, distance)));
    }

#define DEC_SAMPLE_PARAM(CURVE)                                                                                             \
    void sample_patch_parameter(std::vector<real>& out, const CURVE& curve, const std::vector<real>& params, real distance) \
    {                                                                                                                       \
        create_sample_parameter_(out, curve, params, distance);                                                             \
    }

    DEC_SAMPLE_PARAM(bezier_curve<vector2>)
    DEC_SAMPLE_PARAM(bezier_curve<vector3>)
    DEC_SAMPLE_PARAM(multi_bezier_curve<vector2>)
    DEC_SAMPLE_PARAM(multi_bezier_curve<vector3>)

#undef DEC_SAMPLE_PARAM

#define DEC_SAMPLE_PARAM(CURVE, PATCH)                                                                                                          \
    void sample_patch_parameter(std::vector<real>& out, const CURVE& curve, const PATCH& patch, const std::vector<real>& params, real distance) \
    {                                                                                                                                           \
        create_sample_parameter_(out, curve, patch, params, distance);                                                                          \
    }

    DEC_SAMPLE_PARAM(bezier_curve<vector2>, bezier_patch<vector2>)
    DEC_SAMPLE_PARAM(bezier_curve<vector2>, bezier_patch<vector3>)
    DEC_SAMPLE_PARAM(multi_bezier_curve<vector2>, multi_bezier_patch<vector2>)
    DEC_SAMPLE_PARAM(multi_bezier_curve<vector2>, multi_bezier_patch<vector3>)

#undef DEC_SAMPLE_PARAM

#define DEC_SAMPLE_PARAM(CURVE, PATCH)                                                                                                                      \
    void sample_patch_parameter(std::vector<real>& out, const CURVE& curve, const PATCH& patch, const std::vector<real>& params, real distance, real angle) \
    {                                                                                                                                                       \
        create_sample_parameter_(out, curve, patch, params, distance, angle);                                                                               \
    }

    DEC_SAMPLE_PARAM(bezier_curve<vector2>, bezier_patch<vector3>)
    DEC_SAMPLE_PARAM(multi_bezier_curve<vector2>, multi_bezier_patch<vector3>)

#undef DEC_SAMPLE_PARAM

#define DEC_SAMPLE_PARAM(PATCH)                                                                           \
    void sample_patch_parameter(std::vector<vector2>& out, const PATCH& patch, real distance, real angle) \
    {                                                                                                     \
        create_sample_parameter_(out, patch, distance, angle);                                            \
    }

    DEC_SAMPLE_PARAM(bezier_patch<vector3>)
    DEC_SAMPLE_PARAM(multi_bezier_patch<vector3>)

#undef DEC_SAMPLE_PARAM

    std::vector<real> get_valid_knots_u(const multi_bezier_patch<vector3>& patch, real distance)
    {
        return get_valid_knots_u_(patch, distance);
    }
    std::vector<real> get_valid_knots_v(const multi_bezier_patch<vector3>& patch, real distance)
    {
        return get_valid_knots_v_(patch, distance);
    }

    std::vector<real> get_valid_knots(const multi_bezier_curve<vector2>& curve, const multi_bezier_patch<vector3>& patch, real distance)
    {
        return get_valid_knots_(curve, patch, distance);
    }
}

namespace kaze
{
    namespace
    {

        static int find_span(const real knots[], int k, real t)
        {
            if (k <= 2) return 0;
            if (t < knots[0]) return 0;
            if (knots[k - 1] <= t)
            {
                for (int i = 1; i < k; i++)
                {
                    if (knots[k - 1] <= knots[i]) return i - 1;
                }
            }
            else
            {
                for (int i = 1; i < k; i++)
                {
                    if (t < knots[i]) return i - 1;
                }
            }
            return k - 2;
        }

        static int get_range(const std::vector<real>& ranges, real x)
        {
            return find_span(&ranges[0], (int)ranges.size(), x);
        }

        class reverse_knots_transfomer
        {
        public:
            reverse_knots_transfomer(const std::vector<real>& uknots, const std::vector<real>& vknots, int nu, int nv)
                : uknots_(uknots), vknots_(vknots), nu_(nu), nv_(nv) {}
            vector2 operator()(const vector2& p) const
            {
                int nu = nu_;
                int nv = nv_;

                real px = p[0];
                real py = p[1];
                int i = get_range(uknots_, px);
                int j = get_range(vknots_, py);
                real t = (px - uknots_[i]) / (uknots_[i + 1] - uknots_[i]);
                real s = (py - vknots_[j]) / (vknots_[j + 1] - vknots_[j]);
                real x = i + t;
                real y = j + s;

                return vector2(x * nu, y * nv);
            }

        private:
            const std::vector<real>& uknots_;
            const std::vector<real>& vknots_;
            int nu_;
            int nv_;
        };

        class knots_transfomer
        {
        public:
            knots_transfomer(const std::vector<real>& uknots, const std::vector<real>& vknots, int nu, int nv)
                : uknots_(uknots), vknots_(vknots), nu_(nu), nv_(nv) {}
            vector2 operator()(const vector2& p) const
            {
                int nu = nu_;
                int nv = nv_;

                real px = p[0] / nu;
                real py = p[1] / nv;
                int i = (int)floor(px);
                int j = (int)floor(py);
                int usz = (int)uknots_.size();
                int vsz = (int)vknots_.size();
                if (i < 0)
                {
                    i = 0;
                }
                else if (i > usz - 2)
                {
                    i = usz - 2;
                }
                if (j < 0)
                {
                    j = 0;
                }
                else if (j >= vsz - 2)
                {
                    j = vsz - 2;
                }

                real t = px - i;
                real s = py - j;

                real x = (1 - t) * uknots_[i] + t * uknots_[i + 1];
                real y = (1 - s) * vknots_[j] + s * vknots_[j + 1];

                return vector2(x, y);
            }

        private:
            const std::vector<real>& uknots_;
            const std::vector<real>& vknots_;
            int nu_;
            int nv_;
        };
    }

    /*knotted -> not knotted*/
    void reverse_knots_transform(std::vector<vector2>& points, const std::vector<real>& uknots, const std::vector<real>& vknots, int nu, int nv)
    {
        if (points.empty()) return;
        size_t sz = points.size();
        std::vector<vector2> tmp(sz);
        std::transform(points.begin(), points.end(), tmp.begin(), reverse_knots_transfomer(uknots, vknots, nu, nv));

        points.swap(tmp);
    }

    /*not knotted -> knotted*/
    void knots_transform(std::vector<vector2>& points, const std::vector<real>& uknots, const std::vector<real>& vknots, int nu, int nv)
    {
        if (points.empty()) return;
        size_t sz = points.size();
        std::vector<vector2> tmp(sz);
        std::transform(points.begin(), points.end(), tmp.begin(), knots_transfomer(uknots, vknots, nu, nv));

        points.swap(tmp);
    }
}
