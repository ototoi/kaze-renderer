#ifndef KAZE_HALFEDGE_MESH_BUILDER_H
#define KAZE_HALFEDGE_MESH_BUILDER_H

#include "types.h"
#include <vector>

namespace kaze
{

    struct HalfEdgeMeshVertex;
    struct HalfEdgeMeshEdge;
    struct HalfEdgeMeshFace;

    struct HalfEdgeMeshVertex
    {
        vector3 p;
        HalfEdgeMeshEdge* edge;
        size_t index;
    };

    struct HalfEdgeMeshEdge
    {
        HalfEdgeMeshVertex* src;
        HalfEdgeMeshEdge* prev;
        HalfEdgeMeshEdge* next;
        HalfEdgeMeshEdge* twin;
        HalfEdgeMeshFace* face;

        HalfEdgeMeshVertex* get_src() { return src; }
        HalfEdgeMeshVertex* get_dst() { return next->src; }
        size_t index;
    };

    struct HalfEdgeMeshFace
    {
        HalfEdgeMeshEdge* edge;
        size_t index;
    };

    class halfedge_mesh_builder
    {
    public:
        typedef std::vector<HalfEdgeMeshVertex*> vertex_array_type;
        typedef std::vector<HalfEdgeMeshEdge*> edge_array_type;
        typedef std::vector<HalfEdgeMeshFace*> face_array_type;

    public:
        halfedge_mesh_builder();
        ~halfedge_mesh_builder();
        HalfEdgeMeshVertex* set_vertex(const vector3& p);
        HalfEdgeMeshFace* set_face(const std::vector<size_t>& f);
        HalfEdgeMeshFace* set_face(size_t a, size_t b, size_t c);
        HalfEdgeMeshFace* set_face(size_t a, size_t b, size_t c, size_t d);
        void build();

    public:
        const std::vector<HalfEdgeMeshVertex*>& get_vertices() const { return vertices_; }
        const std::vector<HalfEdgeMeshEdge*>& get_edges() const { return edges_; }
        const std::vector<HalfEdgeMeshFace*>& get_faces() const { return faces_; }
    protected:
        void build_clear();
        void build_grid();
        void build_global();
        void attach_index();

    protected:
        std::vector<HalfEdgeMeshVertex*> vertices_;
        std::vector<HalfEdgeMeshEdge*> edges_;
        std::vector<HalfEdgeMeshFace*> faces_;
    };
}

#endif