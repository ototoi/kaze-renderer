#ifndef KAZE_MULTI_BEZIER_CURVE_HPP
#define KAZE_MULTI_BEZIER_CURVE_HPP

#include "bezier_curve.hpp"
#include <vector>
#include <algorithm>
#include <limits>

namespace kaze
{

    template <class T>
    class multi_bezier_curve
    {
    public:
        typedef T value_type;
        typedef bezier_curve<T> inner_type;
        typedef multi_bezier_curve<T> this_type;

    public:
        multi_bezier_curve() {}
        multi_bezier_curve(const this_type& rhs) : knots_(rhs.knots_), curves_(rhs.curves_) {}
        multi_bezier_curve(const std::vector<bezier_curve<T> >& curves) : curves_(curves)
        {
            size_t sz = curves.size();
            knots_.resize(sz + 1);
            knots_[0] = real(0);
            for (size_t i = 1; i < sz; i++)
            {
                knots_[i] = real(1.0 / sz) * i;
            }
            knots_[sz] = real(1.0);
        }

        multi_bezier_curve(const std::vector<bezier_curve<T> >& curves, const std::vector<real>& knots) : knots_(knots), curves_(curves)
        {
            make_regular(knots_, curves_); //check knots
        }

        this_type& operator=(const this_type& rhs)
        {
            knots_ = rhs.knots_;
            curves_ = rhs.curves_;
            return *this;
        }

        T evaluate(real t) const
        {
            int i = find_knot(t);
            real tt = multi_to_param(t, i); //
            return curves_[i].evaluate(tt);
        }

        T evaluate_deriv(real t) const
        {
            int i = find_knot(t);
            real tt = multi_to_param(t, i); //
            return curves_[i].evaluate_deriv(tt);
        }

        T evaluate_dPdT(real t) const
        {
            return evaluate_deriv(t);
        }

        void split(this_type curves[2], real t) const
        {
            assert(0); //no implement
        }
        int get_curve_size() const { return (int)curves_.size(); }
        const bezier_curve<T>& get_curve_at(int i) const { return curves_[i]; }
        void set_curve_at(int i, const bezier_curve<T>& p) { curves_[i] = p; }

        void swap(multi_bezier_curve<T>& rhs)
        {
            knots_.swap(rhs.knots_);
            curves_.swap(rhs.curves_);
        }

        int order() const
        {
            int nRet = 1;
            size_t sz = curves_.size();
            for (size_t i = 0; i < sz; i++)
            {
                nRet = std::max<int>(nRet, curves_[i].order());
            }
            return nRet;
        }
        int degree() const { return order() - 1; }

        int get_knot_size() const { return (int)knots_.size(); }
        real get_knot_at(int i) const { return knots_[i]; }

        const std::vector<real>& get_knots() const { return knots_; }
    public:
        bool equal(const this_type& rhs) const
        {
            return (knots_ == rhs.knots_) && (curves_ == rhs.curves_);
        }
        this_type& transform(const matrix3& m)
        {
            size_t sz = curves_.size();
            for (size_t i = 0; i < sz; i++)
            {
                curves_[i].transform(m);
            }
            return *this;
        }
        this_type& transform(const matrix4& m)
        {
            size_t sz = curves_.size();
            for (size_t i = 0; i < sz; i++)
            {
                curves_[i].transform(m);
            }
            return *this;
        }

    protected:
        int find_knot(real t) const
        {
            return get_range(knots_, t);
        }
        real multi_to_param(real t, int i) const
        {
            real a = t - knots_[i];
            real b = knots_[i + 1] - knots_[i];
            return div(a, b);
        }
        real param_to_multi(real t, int i) const
        { //
            real b = knots_[i + 1] - knots_[i];
            return t * b + knots_[i];
        }

    protected:
        static void make_regular(std::vector<real>& knots, std::vector<bezier_curve<T> >& curves)
        {
            std::sort(knots.begin(), knots.end());
            if (knots[0] != real(0))
            {
                size_t sz = knots.size();
                real min = knots[0];
                for (size_t i = 0; i < sz; i++)
                {
                    knots[i] -= min;
                }
            }
            if (knots.size() != curves.size() + 1)
            {
                size_t ksz = knots.size();
                size_t psz = curves.size() + 1;
                knots.resize(psz);
            }
            size_t ksz = knots.size();
            if (knots[ksz - 1] != real(1))
            {
                size_t sz = knots.size();
                for (size_t i = 0; i < sz; i++)
                {
                    knots[i] /= knots[ksz - 1];
                }
            }
        }

        static int get_range(const std::vector<real>& ranges, real x)
        {
            int sz = (int)ranges.size();
            if (sz <= 2) return 0;
#if 0
            if(x<ranges[   0])return 0;
            for(int i = 1;i<sz;i++){
                if(x<ranges[i])return i-1;
            }
            return sz-2;
#else
            if (x <= ranges[0]) return 0;
            if (ranges[sz - 1] <= x) return sz - 2;

            int a = 0;
            int b = sz - 1;
            while (1)
            {
                int m = a + ((b - a) >> 1);
                if (ranges[m] <= x)
                {
                    if (x < ranges[m + 1])
                    {
                        return m;
                    }
                    else
                    {
                        a = m + 1;
                    }
                }
                else
                {
                    b = m;
                }
            }
            return a;
#endif
        }

        static real div(real a, real b)
        {
            static real EPSILON = std::numeric_limits<real>::epsilon();
            if (fabs(b) <= EPSILON) return 0;
            return a / b;
        }

    private:
        std::vector<real> knots_;
        std::vector<bezier_curve<T> > curves_;
    };

    template <class T>
    inline bool operator==(const multi_bezier_curve<T>& lhs, const multi_bezier_curve<T>& rhs)
    {
        return lhs.equal(rhs);
    }

    template <class T>
    inline bool operator!=(const multi_bezier_curve<T>& lhs, const multi_bezier_curve<T>& rhs)
    {
        return !lhs.equal(rhs);
    }

    template <class T>
    inline multi_bezier_curve<T> operator*(const matrix3& m, const multi_bezier_curve<T>& rhs)
    {
        return multi_bezier_curve<T>(rhs).transform(m);
    }

    template <class T>
    inline multi_bezier_curve<T> operator*(const matrix4& m, const multi_bezier_curve<T>& rhs)
    {
        return multi_bezier_curve<T>(rhs).transform(m);
    }

    typedef multi_bezier_curve<real> multi_bezier_curve1;
    typedef multi_bezier_curve<vector2> multi_bezier_curve2;
    typedef multi_bezier_curve<vector3> multi_bezier_curve3;
    typedef multi_bezier_curve<vector4> multi_bezier_curve4;
}

#endif
