#include "tesselate_polygon.h"

#include "triangulate_polygon.h"
#include "sample_patch_parameter.h"

#include "values.h"
#include <iostream>

namespace kaze
{

    static void create_segment(std::vector<vector2>& loop, const vector2& p0, const vector2& p1, int div)
    {
        real delta = real(1) / div;
        for (int i = 0; i < div; i++)
        {
            real t = delta * i;
            vector2 pp = (1 - t) * p0 + t * (p1);
            loop.push_back(pp);
        }
    }

    static bool convert_curve(std::vector<vector2>& loop, const multi_bezier_curve<vector2>& curve, const multi_bezier_patch<vector3>& patch, real distance, real angle)
    {
        std::vector<real> tknots = get_valid_knots(curve, patch, distance);

        std::vector<real> params;
        sample_patch_parameter(params, curve, patch, tknots, distance, angle);
        for (int i = 0; i < (int)(params.size() - 1); i++)
        {
            loop.push_back(curve.evaluate(params[i]));
        }

        return true;
    }

    static bool convert_loop(std::vector<vector2>& loop, const std::vector<multi_bezier_curve<vector2> >& in, const multi_bezier_patch<vector3>& patch, real distance, real angle)
    {
        size_t sz = in.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (!convert_curve(loop, in[i], patch, distance, angle)) return false;
        }
        return true;
    }

    static bool tesselate_polygon(std::vector<vector2>& triangles, const trimmed_multi_bezier_patch<vector3, vector2>& patch, double dDist, double dRad)
    {
        int nu = patch.get_patch_at(0, 0).get_nu();
        int nv = patch.get_patch_at(0, 0).get_nv();

        std::vector<real> knots_u = get_valid_knots_u(patch, dDist);
        std::vector<real> knots_v = get_valid_knots_v(patch, dDist);

#if 0
		std::vector<real> dummy_knot;
        dummy_knot.push_back(0);
        dummy_knot.push_back(1);
		
		nu = nu*knots_u.size();
        nv = nv*knots_v.size();

		knots_u = dummy_knot;
        knots_v = dummy_knot;

#endif
        std::vector<std::vector<vector2> > outer_loops;
        std::vector<std::vector<vector2> > inner_loops;
        {
            int csz = patch.get_outer_loop_size();
            if (!csz)
            {
                std::vector<vector2> loop;
                vector2 p0 = vector2(0, 0);
                vector2 p1 = vector2(1, 0);
                vector2 p2 = vector2(1, 1);
                vector2 p3 = vector2(0, 1);

                create_segment(loop, p0, p1, 1);
                create_segment(loop, p1, p2, 1);
                create_segment(loop, p2, p3, 1);
                create_segment(loop, p3, p0, 1);
                reverse_knots_transform(loop, knots_u, knots_v, nu, nv);
                outer_loops.push_back(loop);
            }
            else
            {
                for (int i = 0; i < csz; i++)
                {
                    std::vector<vector2> loop;
                    convert_loop(loop, patch.get_outer_loop_at(i), patch, dDist, dRad);
                    reverse_knots_transform(loop, knots_u, knots_v, nu, nv);
                    outer_loops.push_back(loop);
                }
            }
        }
        //----------------------------------------------------------------------------------------
        {
            int csz = patch.get_inner_loop_size();
            for (int i = 0; i < csz; i++)
            {
                std::vector<vector2> loop;
                convert_loop(loop, patch.get_inner_loop_at(i), patch, dDist, dRad);
                reverse_knots_transform(loop, knots_u, knots_v, nu, nv);
                inner_loops.push_back(loop);
            }
        }
        //---------------------------------------------------------------------------------------
        std::vector<vector2> samples;
        sample_patch_parameter(samples, patch, dDist, dRad);
        reverse_knots_transform(samples, knots_u, knots_v, nu, nv);
        //---------------------------------------------------------------------------------------
        if (!triangulate_polygon(triangles, outer_loops, inner_loops, samples, 2)) return false;
        knots_transform(triangles, knots_u, knots_v, nu, nv);

        return true;
    }

    static bool check_area(const vector3& a, const vector3& b, const vector3& c, real EPS = values::epsilon() * 1024)
    {
        if ((a - b).sqr_length() < EPS) return false;
        if ((b - c).sqr_length() < EPS) return false;
        if ((c - a).sqr_length() < EPS) return false;
        return true;
    }

    bool tesselate_polygon(std::vector<vector3>& triangles, const trimmed_multi_bezier_patch<vector3, vector2>& patch, double dDist, double dRad)
    {
        std::vector<vector2> vec2;
        if (!tesselate_polygon(vec2, patch, dDist, dRad)) return false;
        size_t tsz = vec2.size();
        triangles.reserve(triangles.size() + tsz);
        tsz /= 3;
        for (size_t i = 0; i < tsz; i++)
        {
            vector2 c0 = vec2[3 * i + 0];
            vector2 c1 = vec2[3 * i + 1];
            vector2 c2 = vec2[3 * i + 2];
            vector3 p0 = patch.evaluate(c0[0], c0[1]);
            vector3 p1 = patch.evaluate(c1[0], c1[1]);
            vector3 p2 = patch.evaluate(c2[0], c2[1]);
            if (check_area(p0, p1, p2))
            {
                triangles.push_back(p0);
                triangles.push_back(p1);
                triangles.push_back(p2);
            }
        }
        return true;
    }

    static vector3 get_normal(const trimmed_multi_bezier_patch<vector3, vector2>& patch, real u, real v)
    {
        vector3 dpdu = patch.evaluate_dPdU(u, v);
        vector3 dpdv = patch.evaluate_dPdV(u, v);
        return normalize(cross(dpdu, dpdv));
    }

    bool tesselate_polygon(std::vector<vector3>& triangles, std::vector<vector3>& normals, const trimmed_multi_bezier_patch<vector3, vector2>& patch, double dDist, double dRad)
    {
        std::vector<vector2> vec2;
        if (!tesselate_polygon(vec2, patch, dDist, dRad)) return false;
        size_t tsz = vec2.size();
        triangles.reserve(triangles.size() + tsz);
        normals.reserve(normals.size() + tsz);
        tsz /= 3;
        for (size_t i = 0; i < tsz; i++)
        {
            vector2 c0 = vec2[3 * i + 0];
            vector2 c1 = vec2[3 * i + 1];
            vector2 c2 = vec2[3 * i + 2];
            vector3 p0 = patch.evaluate(c0[0], c0[1]);
            vector3 p1 = patch.evaluate(c1[0], c1[1]);
            vector3 p2 = patch.evaluate(c2[0], c2[1]);
            if (check_area(p0, p1, p2))
            {
                triangles.push_back(p0);
                triangles.push_back(p1);
                triangles.push_back(p2);
                vector3 n0 = get_normal(patch, c0[0], c0[1]);
                vector3 n1 = get_normal(patch, c1[0], c1[1]);
                vector3 n2 = get_normal(patch, c2[0], c2[1]);
                normals.push_back(n0);
                normals.push_back(n1);
                normals.push_back(n2);
            }
        }
        return true;
    }
}
