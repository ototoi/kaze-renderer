#include "halfedge_mesh_builder.h"
#include <vector>
#include <map>

#define DIV_NUM 1024

namespace kaze
{

    halfedge_mesh_builder::halfedge_mesh_builder()
    {
    }
    halfedge_mesh_builder::~halfedge_mesh_builder()
    {
        size_t vsz = vertices_.size();
        for (size_t i = 0; i < vsz; i++)
        {
            delete vertices_[i];
        }
        size_t esz = edges_.size();
        for (size_t i = 0; i < esz; i++)
        {
            delete edges_[i];
        }
        size_t fsz = faces_.size();
        for (size_t i = 0; i < fsz; i++)
        {
            delete faces_[i];
        }
    }
    HalfEdgeMeshVertex* halfedge_mesh_builder::set_vertex(const vector3& p)
    {
        HalfEdgeMeshVertex* v = new HalfEdgeMeshVertex();
        v->p = p;
        vertices_.push_back(v);
        return v;
    }
    HalfEdgeMeshFace* halfedge_mesh_builder::set_face(const std::vector<size_t>& f)
    {
        switch (f.size())
        {
        case 3:
            return this->set_face(f[0], f[1], f[2]);
        case 4:
            return this->set_face(f[0], f[1], f[2], f[3]);
        default:
        {
            assert(0);
            return NULL;
        }
        }
        return NULL;
    }

    HalfEdgeMeshFace* halfedge_mesh_builder::set_face(size_t a, size_t b, size_t c)
    {
        size_t vsz = vertices_.size();
        if (a >= vsz) return NULL;
        if (b >= vsz) return NULL;
        if (c >= vsz) return NULL;
        HalfEdgeMeshEdge* ea = new HalfEdgeMeshEdge();
        HalfEdgeMeshEdge* eb = new HalfEdgeMeshEdge();
        HalfEdgeMeshEdge* ec = new HalfEdgeMeshEdge();
        ea->src = vertices_[a];
        eb->src = vertices_[b];
        ec->src = vertices_[c];
        ea->prev = ec;
        eb->prev = ea;
        ec->prev = eb;
        ea->next = eb;
        eb->next = ec;
        ec->next = ea;
        ea->twin = NULL;
        eb->twin = NULL;
        ec->twin = NULL;

        ea->src->edge = ea;
        eb->src->edge = eb;
        ec->src->edge = ec;
        HalfEdgeMeshFace* f = new HalfEdgeMeshFace();
        f->edge = ea;

        ea->face = f;
        eb->face = f;
        ec->face = f;

        edges_.push_back(ea);
        edges_.push_back(eb);
        edges_.push_back(ec);
        faces_.push_back(f);

        return f;
    }
    HalfEdgeMeshFace* halfedge_mesh_builder::set_face(size_t a, size_t b, size_t c, size_t d)
    {
        size_t vsz = vertices_.size();
        if (a >= vsz) return NULL;
        if (b >= vsz) return NULL;
        if (c >= vsz) return NULL;
        if (d >= vsz) return NULL;
        HalfEdgeMeshEdge* ea = new HalfEdgeMeshEdge();
        HalfEdgeMeshEdge* eb = new HalfEdgeMeshEdge();
        HalfEdgeMeshEdge* ec = new HalfEdgeMeshEdge();
        HalfEdgeMeshEdge* ed = new HalfEdgeMeshEdge();
        ea->src = vertices_[a];
        eb->src = vertices_[b];
        ec->src = vertices_[c];
        ed->src = vertices_[d];
        ea->prev = ed;
        eb->prev = ea;
        ec->prev = eb;
        ed->prev = ec;
        ea->next = eb;
        eb->next = ec;
        ec->next = ed;
        ed->next = ea;
        ea->twin = NULL;
        eb->twin = NULL;
        ec->twin = NULL;
        ed->twin = NULL;

        ea->src->edge = ea;
        eb->src->edge = eb;
        ec->src->edge = ec;
        ed->src->edge = ed;

        HalfEdgeMeshFace* f = new HalfEdgeMeshFace();
        f->edge = ea;

        ea->face = f;
        eb->face = f;
        ec->face = f;
        ed->face = f;

        edges_.push_back(ea);
        edges_.push_back(eb);
        edges_.push_back(ec);
        edges_.push_back(ed);
        faces_.push_back(f);

        return f;
    }

    static inline uint32_t partby2(uint32_t n)
    {
        n = (n ^ (n << 16)) & 0xff0000ff;
        n = (n ^ (n << 8)) & 0x0300f00f;
        n = (n ^ (n << 4)) & 0x030c30c3;
        n = (n ^ (n << 2)) & 0x09249249;
        return n;
    }

    static inline uint32_t get_morton_code(uint32_t x, uint32_t y, uint32_t z)
    {
        return (partby2(x) << 2) | (partby2(y) << 1) | (partby2(z));
    }

    static inline uint32_t get_morton_code(const vector3& p, const vector3& min, const vector3& max)
    {
        uint32_t ix = (uint32_t)(DIV_NUM * ((p[0] - min[0]) / (max[0] - min[0])));
        uint32_t iy = (uint32_t)(DIV_NUM * ((p[1] - min[1]) / (max[1] - min[1])));
        uint32_t iz = (uint32_t)(DIV_NUM * ((p[2] - min[2]) / (max[2] - min[2])));
        return get_morton_code(ix, iy, iz);
    }

    static void get_minmax(vector3& min, vector3& max, const std::vector<HalfEdgeMeshVertex*>& vertices)
    {
        size_t sz = vertices.size();
        min = max = vertices[0]->p;
        for (size_t i = 1; i < sz; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (vertices[i]->p[j] < min[j]) min[j] = vertices[i]->p[j];
                if (vertices[i]->p[j] > max[j]) max[j] = vertices[i]->p[j];
            }
        }
    }
    static void pairing_edges(std::vector<HalfEdgeMeshEdge*>& edges)
    {
        std::map<HalfEdgeMeshVertex*, std::vector<HalfEdgeMeshEdge*> > srcMap;

        size_t esz = edges.size();
        for (size_t i = 0; i < esz; i++)
        {
            HalfEdgeMeshEdge* e = edges[i];
            if (!e->twin)
            {
                srcMap[e->get_src()].push_back(e);
            }
        }
        for (size_t i = 0; i < esz; i++)
        {
            HalfEdgeMeshEdge* e = edges[i];
            if (!e->twin)
            {
                std::vector<HalfEdgeMeshEdge*>& ed = srcMap[e->get_dst()];
                if (!ed.empty())
                {
                    for (size_t j = 0; j < ed.size(); j++)
                    {
                        if (e != ed[j] && !ed[j]->twin)
                        {
                            if (e->get_src() == ed[j]->get_dst())
                            {
                                e->twin = ed[j];
                                ed[j]->twin = e;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    void halfedge_mesh_builder::build_clear()
    {
        size_t esz = edges_.size();
        for (size_t i = 0; i < esz; i++)
        {
            HalfEdgeMeshEdge* e = edges_[i];
            e->twin = NULL;
        }
    }

    void halfedge_mesh_builder::attach_index()
    {
        size_t vsz = vertices_.size();
        for (size_t i = 0; i < vsz; i++)
        {
            vertices_[i]->index = i;
        }
        size_t esz = edges_.size();
        for (size_t i = 0; i < esz; i++)
        {
            edges_[i]->index = i;
        }
        size_t fsz = faces_.size();
        for (size_t i = 0; i < fsz; i++)
        {
            faces_[i]->index = i;
        }
    }

    void halfedge_mesh_builder::build_grid()
    {
        typedef std::map<uint32_t, std::vector<HalfEdgeMeshEdge*> > CodeMap;
        CodeMap cm;

        vector3 min;
        vector3 max;
        get_minmax(min, max, vertices_);

        size_t esz = edges_.size();
        for (size_t i = 0; i < esz; i++)
        {
            HalfEdgeMeshEdge* e = edges_[i];
            uint32_t code = get_morton_code((e->get_src()->p + e->get_dst()->p) * 0.5, min, max);
            cm[code].push_back(e);
        }
        for (CodeMap::iterator it = cm.begin(); it != cm.end(); it++)
        {
            std::vector<HalfEdgeMeshEdge*>& edges = it->second;
            if (!edges.empty())
            {
                pairing_edges(edges);
            }
        }
    }
    void halfedge_mesh_builder::build_global()
    {
        if (!edges_.empty())
        {
            pairing_edges(edges_);
        }
    }

    void halfedge_mesh_builder::build()
    {
        build_clear();
        attach_index();
        build_grid();
    }
}
