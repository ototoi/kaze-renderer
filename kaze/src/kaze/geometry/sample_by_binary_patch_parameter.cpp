#include "types.h"

#include <vector>

#define N 10
#define WWIDTH 1024

#define WIDTH (WWIDTH + 1)
#define HEIGHT (WWIDTH + 1)

/*
static const unsigned int ID_SW = 0;
static const unsigned int ID_SE = WWIDTH;
static const unsigned int ID_NW = ((WWIDTH + 1) * WWIDTH);    //4->20
static const unsigned int ID_NE = ((WWIDTH + 2) * WWIDTH);    //4->24
static const unsigned int ID_C = (WWIDTH * (WWIDTH + 2)) / 2; //4->12
*/

namespace kaze
{
    static void GetCoord(unsigned int ID, int& x, int& y)
    {
        y = ID / WIDTH;
        x = ID % WIDTH;
    }

    static vector2f GetCoord(unsigned int ID)
    {
        int x, y;
        GetCoord(ID, x, y);
        float xx = float(x) / WWIDTH;
        float yy = float(y) / WWIDTH;
        return vector2f(xx, yy);
    }

    static inline unsigned int GetID(int x, int y)
    {
        return y * HEIGHT + x;
    }

    class MeshRefiner
    {
    public:
        void MeshRefine()
        {
            //SubMeshRefine()
        }
        void SubMeshRefine(unsigned int il, unsigned int ia, unsigned int ir, int l)
        {
            if (l < N && IsActive(ia))
            {
            }
            else
            {
                DrawTriangle(il, ir, ia);
            }
        }
        void DrawTriangle(unsigned int a, unsigned int b, unsigned int c)
        {
            m_tri.push_back(a);
            m_tri.push_back(b);
            m_tri.push_back(c);
        }

    protected:
        bool IsActive(unsigned int ID)
        {
            vector2f c = GetCoord(ID);

            return false;
        }

    public:
        std::vector<unsigned int> m_tri;
    };
}
