#ifndef KAZE_BSPLINE_PATCH_HPP
#define KAZE_BSPLINE_PATCH_HPP

#include "bspline.h"
#include <vector>

namespace kaze
{

    template <class T>
    class bspline_patch
    {
    public:
        typedef T value_type;
        typedef bspline_patch<T> this_type;

    public:
        bspline_patch() : nu_(0), nv_(0) {}
        bspline_patch(
            int nu,
            int nv,
            const std::vector<T>& cp,
            const std::vector<real>& knots_u,
            const std::vector<real>& knots_v)
            : cp_(cp), nu_(nu), nv_(nv), knots_u_(knots_u), knots_v_(knots_v)
        {
        }

        bspline_patch(const bspline_patch<T>& rhs)
            : cp_(rhs.cp_), nu_(rhs.nu_), nv_(rhs.nv_), knots_u_(rhs.knots_u_), knots_v_(rhs.knots_v_)
        {
        }

        this_type& operator=(const bspline_patch<T>& rhs)
        {
            nu_ = rhs.nu_;
            nv_ = rhs.nv_;
            cp_ = rhs.cp_;
            knots_u_ = rhs.knots_u_;
            knots_v_ = rhs.knots_v_;
            return *this;
        }

        T evaluate(real u, real v) const
        {
            return bspline_evaluate(&cp_[0], nu_, nv_, &knots_u_[0], (int)knots_u_.size(), &knots_v_[0], (int)knots_v_.size(), u, v);
        }

        T evaluate_deriv_u(real u, real v) const
        {
            return bspline_evaluate_deriv_u(&cp_[0], nu_, nv_, &knots_u_[0], (int)knots_u_.size(), &knots_v_[0], (int)knots_v_.size(), u, v);
        }

        T evaluate_deriv_v(real u, real v) const
        {
            return bspline_evaluate_deriv_v(&cp_[0], nu_, nv_, &knots_u_[0], (int)knots_u_.size(), &knots_v_[0], (int)knots_v_.size(), u, v);
        }

        T evaluate_dPdU(real u, real v) const
        {
            return evaluate_deriv_u(u, v);
        }

        T evaluate_dPdV(real u, real v) const
        {
            return evaluate_deriv_v(u, v);
        }

        void swap(bspline_patch<T>& rhs)
        {
            std::swap(nu_, rhs.nu_);
            std::swap(nv_, rhs.nv_);
            cp_.swap(rhs.cp_);
            knots_u_.swap(rhs.knots_u_);
            knots_v_.swap(rhs.knots_v_);
        }

    public:
        bool equal(const this_type& rhs) const
        {
            if (nu_ != rhs.nu_) return false;
            if (nv_ != rhs.nv_) return false;
            if (knots_u_ != rhs.knots_u_) return false;
            if (knots_v_ != rhs.knots_v_) return false;
            if (cp_ != rhs.cp_) return false;
            return true;
        }

        this_type& transform(const matrix3& m)
        {
            size_t sz = cp_.size();
            for (size_t i = 0; i < sz; i++)
            {
                cp_[i] = m * cp_(i);
            }
            return *this;
        }

        this_type& transform(const matrix4& m)
        {
            size_t sz = cp_.size();
            for (size_t i = 0; i < sz; i++)
            {
                cp_[i] = m * cp_(i);
            }
            return *this;
        }

        T get_cp_at(int i, int j) const { return cp_[j * nu_ + i]; }
        void set_cp_at(int i, int j, const T& p) { cp_[j * nu_ + i] = p; }
        int get_cp_size() const { return (int)cp_.size(); }

        const std::vector<T>& get_cp() const { return cp_; }

        int get_nu() const { return nu_; }
        int get_nv() const { return nv_; }

        int order_u() const { return (int)knots_u_.size() - (int)nu_; }
        int degree_u() const { return order_u() - 1; }
        int order_v() const { return (int)knots_v_.size() - (int)nv_; }
        int degree_v() const { return order_v() - 1; }

        int get_knot_size_u() const { return (int)knots_u_.size(); }
        real get_knot_at_u(int i) const { return knots_u_[i]; }
        int get_knot_size_v() const { return (int)knots_v_.size(); }
        real get_knot_at_v(int i) const { return knots_v_[i]; }

        const std::vector<real>& get_knots_u() const { return knots_u_; }
        const std::vector<real>& get_knots_v() const { return knots_v_; }

    public:
        bool is_bezier_type_u() const
        {
            return bspline_is_bezier_type(&knots_u_[0], (int)knots_u_.size(), order_u());
        }
        bool is_open0_u() const
        {
            return bspline_is_open0(&knots_u_[0], (int)knots_u_.size(), order_u());
        }
        bool is_open1_u() const
        {
            return bspline_is_open1(&knots_u_[0], (int)knots_u_.size(), order_u());
        }
        bool is_open_u() const
        {
            return is_open0_u() && is_open1_u();
        }
        bool is_uniform_u() const
        {
            return bspline_is_uniform(&knots_u_[0], (int)knots_u_.size());
        }

        bool is_bezier_type_v() const
        {
            return bspline_is_bezier_type(&knots_v_[0], (int)knots_v_.size(), order_v());
        }
        bool is_open0_v() const
        {
            return bspline_is_open0(&knots_v_[0], (int)knots_v_.size(), order_v());
        }
        bool is_open1_v() const
        {
            return bspline_is_open1(&knots_v_[0], (int)knots_v_.size(), order_v());
        }
        bool is_open_v() const
        {
            return is_open0_v() && is_open1_v();
        }
        bool is_uniform_v() const
        {
            return bspline_is_uniform(&knots_v_[0], (int)knots_v_.size());
        }

        bool is_bezier_type() const
        {
            return is_bezier_type_u() && is_bezier_type_v();
        }

    public:
        this_type& to_regular()
        {
            to_regular(knots_u_);
            to_regular(knots_v_);
            return *this;
        }
        this_type& to_bezier_type()
        {
            to_regular();
            bspline_convert_bezier_type(cp_, nu_, nv_, knots_u_, knots_v_);
            to_regular();
            assert(is_bezier_type()); //
            return *this;
        }
        this_type& to_nonperiodic(bool periodicU, bool periodicV)
        {
            to_regular();
            bspline_kill_periodic(cp_, nu_, nv_, knots_u_, knots_v_, periodicU, periodicV);
            return *this;
        }

    public:
        static void to_regular(std::vector<real>& knots)
        {
            real min = knots.front();
            real max = knots.back();
            if (!(min == 0 && max == 1))
            {
                real wid = real(1.0) / (max - min);
                for (size_t i = 0; i < knots.size(); i++)
                {
                    knots[i] = (knots[i] - min) * wid;
                }
            }
        }

    private:
        int nu_;
        int nv_;
        std::vector<T> cp_;
        std::vector<real> knots_u_;
        std::vector<real> knots_v_;
    };

    template <class T>
    inline bool operator==(const bspline_patch<T>& lhs, const bspline_patch<T>& rhs)
    {
        return lhs.equal(rhs);
    }

    template <class T>
    inline bool operator!=(const bspline_patch<T>& lhs, const bspline_patch<T>& rhs)
    {
        return !lhs.equal(rhs);
    }

    template <class T>
    inline bspline_patch<T> operator*(const matrix3& m, const bspline_patch<T>& patch)
    {
        return bspline_patch<T>(patch).transform(m);
    }

    template <class T>
    inline bspline_patch<T> operator*(const matrix4& m, const bspline_patch<T>& patch)
    {
        return bspline_patch<T>(patch).transform(m);
    }
}

#endif
