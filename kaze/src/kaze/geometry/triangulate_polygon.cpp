#include "triangulate_polygon.h"

#include "triangulate_polygon_monotone.h"
#include "triangulate_polygon_earclip.h"
#include "triangulate_polygon_delaunay.h"

#include "enumerate_loops.h"

#include "logger.h"
#include "timer.h"

namespace kaze
{

    static const real EPSILON = values::epsilon() * 1024;

    static bool triangulate_polygon(std::vector<vector2>& triangles, const std::vector<polyloop_group>& loops)
    {
        int lsz = (int)loops.size();
        std::vector<std::vector<vector2> > vectriangles(lsz);

        bool bRet = true;
        if (lsz >= 4)
        {
#pragma omp parallel for
            for (int k = 0; k < lsz; k++)
            {
                if (!triangulate_polygon_delaunay(vectriangles[k], loops[k].outer_loop, loops[k].inner_loops, loops[k].points)) bRet = false; //TODO:
            }
            if (!bRet) return false;
        }
        else
        {
            for (int k = 0; k < lsz; k++)
            {
                if (!triangulate_polygon_delaunay(vectriangles[k], loops[k].outer_loop, loops[k].inner_loops, loops[k].points)) return false; //TODO:
            }
        }

        size_t tsz = 0;
        for (int k = 0; k < lsz; k++)
        {
            tsz += vectriangles[k].size();
        }
        triangles.resize(tsz);

        size_t l = 0;
        for (int k = 0; k < lsz; k++)
        {
            std::vector<vector2>& vecs = vectriangles[k];
            if (!vecs.empty())
            {
                size_t vsz = vecs.size();
                for (size_t i = 0; i < vsz; i++)
                {
                    triangles[l] = vecs[i];
                    l++;
                }
            }
        }

        return true;
    }

    //------------------------------------------------------------------------------------------

    bool triangulate_polygon(
        std::vector<vector2>& triangles,
        const std::vector<vector2>& outer_loop,
        int nFillType)
    {
        std::vector<std::vector<vector2> > outer_loops;
        outer_loops.push_back(outer_loop);
        std::vector<std::vector<vector2> > inner_loops;
        std::vector<vector2> points;

        std::vector<polyloop_group> loops;
        if (!enumerate_loops(loops, outer_loops, inner_loops, points, nFillType)) return false;

        return triangulate_polygon(triangles, loops);
    }
    bool triangulate_polygon(
        std::vector<vector2>& triangles,
        const std::vector<vector2>& outer_loop,
        const std::vector<vector2>& points,
        int nFillType)
    {
        std::vector<std::vector<vector2> > outer_loops;
        outer_loops.push_back(outer_loop);
        std::vector<std::vector<vector2> > inner_loops;
        //std::vector<vector2> points;

        std::vector<polyloop_group> loops;
        if (!enumerate_loops(loops, outer_loops, inner_loops, points, nFillType)) return false;

        return triangulate_polygon(triangles, loops);
    }

    bool triangulate_polygon(
        std::vector<vector2>& triangles,
        const std::vector<std::vector<vector2> >& outer_loops,
        const std::vector<std::vector<vector2> >& inner_loops,
        const std::vector<vector2>& points,
        int nFillType)
    {
        std::vector<polyloop_group> loops;
        if (!enumerate_loops(loops, outer_loops, inner_loops, points, nFillType)) return false;

        return triangulate_polygon(triangles, loops);
    }
}
