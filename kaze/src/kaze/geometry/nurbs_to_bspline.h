#ifndef KAZE_NURBS_TO_BSPLINE_H
#define KAZE_NURBS_TO_BSPLINE_H

#include "bspline_curve.hpp"
#include "bspline_patch.hpp"
#include "nurbs_curve.hpp"
#include "nurbs_patch.hpp"

namespace kaze
{

#define DEF_NBS_TO_BSP(TYPE)                                                                        \
    void nurbs_to_bspline(bspline_curve<TYPE>& bsp, const nurbs_curve<TYPE>& nbs, real eps = 0.01); \
    void nurbs_to_bspline(bspline_patch<TYPE>& bsp, const nurbs_patch<TYPE>& nbs, real eps = 0.01);

    DEF_NBS_TO_BSP(real)
    DEF_NBS_TO_BSP(vector2)
    DEF_NBS_TO_BSP(vector3)

#undef DEF_BSP_TO_BZR
}
#endif
