#ifndef KAZE_DART_HPP
#define KAZE_DART_HPP

#include <cassert>
#include <iterator>
#include <list>

namespace kaze
{

    template <class E>
    class basic_dart
    {
    public:
        typedef E edge_type;
        typedef typename E::vertex_type vertex_type;
        typedef basic_dart<E> dart_type;
        typedef basic_dart<E> this_type;

    public:
        basic_dart(edge_type* edge = NULL, bool bRegular = true) : edge_(edge), dir_(bRegular) {}
        basic_dart(const this_type& d) : edge_(d.edge_), dir_(d.dir_) {}

        this_type& operator=(const this_type& d)
        {
            edge_ = d.edge_;
            dir_ = d.dir_;
            return *this;
        }

    public:
        bool operator==(const basic_dart& d) const
        {
            return (edge_ == d.edge_) && (dir_ == d.dir_);
        }
        bool operator!=(const basic_dart& d) const
        {
            return !(*this == d);
        }

    public:
        edge_type* get_edge() { return edge_; }
        edge_type* get_edge() const { return edge_; }
        bool direction() const { return dir_; }
        edge_type* get_twin_edge() { return edge_->get_twin_edge(); }
        edge_type* get_twin_edge() const { return edge_->get_twin_edge(); }
        vertex_type* get_source_vertex()
        {
            if (dir_)
                edge_->get_source_vertex();
            else
                edge_->get_sink_vertex();
        }
        vertex_type* get_sink_vertex()
        {
            if (!dir_)
                edge_->get_source_vertex();
            else
                edge_->get_sink_vertex();
        }

    public:
        inline dart_type alpha0() const { return dart_type(edge_, !dir_); }
        inline dart_type alpha1() const
        {
            edge_type* e;
            if (dir_)
            {
                e = edge_;
                while (e->get_next_edge() != edge_)
                {
                    e = e->get_next_edge();
                }
                assert(e->get_next_edge() == edge_);
            }
            else
            {
                e = edge_->get_next_edge();
            }
            return dart_type(e, !dir_);
        }
        inline dart_type alpha2() const
        {
            if (edge_->get_twin_edge())
            {
                return dart_type(edge_->get_twin_edge(), !dir_);
            }
            else
            {
                return dart_type(*this);
            }
        }

    public:
        inline dart_type next_edge() const
        {
            return alpha0().alpha1();
        }
        inline dart_type next_face() const
        {
            return alpha1().alpha2();
        }
        inline dart_type prev_face() const
        {
            return alpha2().alpha1();
        }

    private:
        edge_type* edge_;
        bool dir_;
    };

    template <class D>
    inline bool is_boundary_edge(const D& d)
    {
        return (d.alpha2() == d); //no twin edge.
    }

    template <class D>
    inline bool is_boundary_face(const D& d)
    {
        D iter = d;
        do
        {
            if (is_boundary_edge(iter)) return true;
            iter = iter.next_edge();
        } while (iter != d);
        return false;
    }

    template <class D>
    inline bool is_boundary_vertex(const D& d)
    {
        D iter = d;
        do
        {
            if (is_boundary_edge(iter)) return true;
            iter = iter.next_face();
        } while (iter != d);
        return false;
    }

    //
    //face iterator
    //
    template <class D>
    class orbit0_iterator : public std::iterator<std::bidirectional_iterator_tag, D>
    {
    public:
        typedef orbit0_iterator<D> this_type;
        typedef D value_type;
        typedef typename D::edge_type edge_type;

    public:
        explicit orbit0_iterator(const D& d) : d_(d) {}
        explicit orbit0_iterator(edge_type* e) : d_(D(e)) {}
        orbit0_iterator(const this_type& rhs) : d_(rhs.d_) {}
        this_type& operator=(const this_type& rhs)
        {
            d_ = rhs.d_;
            return *this;
        }
        bool operator==(const this_type& rhs) const
        {
            return d_ == rhs.d_;
        }
        bool operator!=(const this_type& rhs) const
        {
            return d_ != rhs.d_;
        }

    public:
        this_type& operator++()
        {
            d_ = d_.alpha1().alpha2();
            return *this;
        }
        this_type operator++(int)
        {
            this_type tmp = *this;
            d_ = d_.alpha1().alpha2();
            return tmp;
        }
        this_type& operator--()
        {
            d_ = d_.alpha2().alpha1();
            return *this;
        }
        this_type operator--(int)
        {
            this_type tmp = *this;
            d_ = d_.alpha2().alpha1();
            return tmp;
        }
        value_type& operator*() { return d_; }
        const value_type& operator*() const { return d_; }
        value_type* operator->() { return &d_; }
        const value_type* operator->() const { return &d_; }
    private:
        D d_;
    };

    template <class D>
    orbit0_iterator<D> begin_orbit0(const orbit0_iterator<D>& a)
    {
        orbit0_iterator<D> i(a);
        do
        {
            if (!i->get_twin_edge()) return i;
            i--;
        } while (i != a);
        return i;
    }
    template <class D>
    orbit0_iterator<D> end_orbit0(const orbit0_iterator<D>& a)
    {
        orbit0_iterator<D> i(a);
        do
        {
            if (!i->get_twin_edge()) return i;
            i++;
        } while (i != a);
        return i;
    }

    //
    //edge inverse iterator //vertex iterate
    //
    template <class D>
    class orbit1_iterator : public std::iterator<std::bidirectional_iterator_tag, D>
    {
    public:
        typedef orbit1_iterator<D> this_type;
        typedef D value_type;
        typedef typename D::edge_type edge_type;

    public:
        explicit orbit1_iterator(const D& d) : d_(d) {}
        explicit orbit1_iterator(edge_type* e) : d_(D(e)) {}
        orbit1_iterator(const this_type& rhs) : d_(rhs.d_) {}
        this_type& operator=(const this_type& rhs)
        {
            d_ = rhs.d_;
            return *this;
        }
        bool operator==(const this_type& rhs) const
        {
            return d_ == rhs.d_;
        }
        bool operator!=(const this_type& rhs) const
        {
            return d_ != rhs.d_;
        }

    public:
        this_type& operator++()
        {
            d_ = d_.alpha2().alpha0();
            return *this;
        }
        this_type operator++(int)
        {
            this_type tmp = *this;
            d_ = d_.alpha2().alpha0();
            return tmp;
        }
        this_type& operator--()
        {
            d_ = d_.alpha0().alpha2();
            return *this;
        }
        this_type operator--(int)
        {
            this_type tmp = *this;
            d_ = d_.alpha0().alpha2();
            return tmp;
        }
        value_type& operator*() { return d_; }
        const value_type& operator*() const { return d_; }
        value_type* operator->() { return &d_; }
        const value_type* operator->() const { return &d_; }
    private:
        D d_;
    };

    //
    //edge rotate iterator //edge iterate
    //
    template <class D>
    class orbit2_iterator : public std::iterator<std::bidirectional_iterator_tag, D>
    {
    public:
        typedef orbit2_iterator<D> this_type;
        typedef D value_type;
        typedef typename D::edge_type edge_type;

    public:
        explicit orbit2_iterator(const D& d) : d_(d) {}
        explicit orbit2_iterator(edge_type* e) : d_(D(e)) {}
        orbit2_iterator(const this_type& rhs) : d_(rhs.d_) {}
        this_type& operator=(const this_type& rhs)
        {
            d_ = rhs.d_;
            return *this;
        }
        bool operator==(const this_type& rhs) const
        {
            return d_ == rhs.d_;
        }
        bool operator!=(const this_type& rhs) const
        {
            return d_ != rhs.d_;
        }

    public:
        this_type& operator++()
        {
            d_ = d_.alpha0().alpha1();
            return *this;
        }
        this_type operator++(int)
        {
            this_type tmp = *this;
            d_ = d_.alpha0().alpha1();
            return tmp;
        }
        this_type& operator--()
        {
            d_ = d_.alpha1().alpha0();
            return *this;
        }
        this_type operator--(int)
        {
            this_type tmp = *this;
            d_ = d_.alpha1().alpha0();
            return tmp;
        }
        value_type& operator*() { return d_; }
        const value_type& operator*() const { return d_; }
        value_type* operator->() { return &d_; }
        const value_type* operator->() const { return &d_; }
    private:
        D d_;
    };
}

#endif
