#ifndef KAZE_HALFEDGE_HPP
#define KAZE_HALFEDGE_HPP

namespace kaze
{

    class basic_halfedge_dummy_face
    {
    };

    template <class V, class F = basic_halfedge_dummy_face>
    class basic_halfedge
    {
    public:
        typedef V vertex_type;
        typedef basic_halfedge<V, F> edge_type;
        typedef basic_halfedge<V, F> this_type;
        typedef F face_type;

    public:
        basic_halfedge(
            vertex_type* src = 0,
            edge_type* next = 0,
            edge_type* twin = 0,
            face_type* face = 0)
            : src_(src), next_(next), twin_(twin), face_(face)
        {
        }

    public:
        void set_source_vertex(vertex_type* v) { src_ = v; }
        void set_next_edge(edge_type* e) { next_ = e; }
        void set_twin_edge(edge_type* e)
        {
            twin_ = e;
            if (e)
            {
                e->twin_ = this;
            }
        }
        void set_face(face_type* f) { face_ = f; }
    public:
        vertex_type* get_source_vertex() const { return src_; }
        vertex_type* get_sink_vertex() const { return next_->src_; }
        edge_type* get_next_edge() const { return next_; }
        edge_type* get_twin_edge() const { return twin_; }
        face_type* get_face() const { return face_; }
    private:
        vertex_type* src_; //source node vertex.
        edge_type* next_;  //next edge in face.
        edge_type* twin_;  //twin edge.
        face_type* face_;
    };
}

#endif
