#ifndef KAZE_NURBS_CURVE_HPP
#define KAZE_NURBS_CURVE_HPP

#include "bspline.h"
#include "nurbs.h"
#include <vector>

namespace kaze
{

    template <class T>
    class nurbs_curve
    {
    public:
        typedef T value_type;
        typedef nurbs_curve<T> this_type;

    public:
        nurbs_curve() {}

        nurbs_curve(const this_type& rhs)
            : cp_(rhs.cp_), w_(rhs.w_), knots_(rhs.knots_) {}

        nurbs_curve(
            const std::vector<T>& cp,
            const std::vector<real>& w,
            int nOrder, bool bOpen0 = true, bool bOpen1 = true, bool bRegular = true)
            : cp_(cp), w_(w)
        {
            int n = (int)cp.size();
            int m = nOrder;
            int k = n + m;

            assert(n >= m);

            std::vector<real> knots(k);
            bspline_create_uniform_knots(&knots[0], k, m, bOpen0, bOpen1);
            if (bRegular)
            {
                to_regular(knots);
            }
            knots_.swap(knots);
        }

        nurbs_curve(const std::vector<T>& cp, const std::vector<real>& w, const std::vector<real>& knots)
            : cp_(cp), w_(w), knots_(knots) {}

        this_type& operator=(const this_type& rhs)
        {
            cp_ = rhs.cp_;
            w_ = rhs.w_;
            knots_ = rhs.knots_;
            return *this;
        }

        T evaluate(real t) const
        {
            return nurbs_evaluate(&cp_[0], &w_[0], (int)cp_.size(), &knots_[0], (int)knots_.size(), t);
        }

        T evaluate_deriv(real t) const
        {
            return nurbs_evaluate_deriv(&cp_[0], &w_[0], (int)cp_.size(), &knots_[0], (int)knots_.size(), t);
        }

    public:
        T get_at(int i) const { return cp_[i]; }
        void set_at(int i, const T& p) { cp_[i] = p; }
        const std::vector<T>& get_cp() const { return cp_; }
        size_t size() const { return cp_.size(); }

        real get_w_at(int i) const { return w_[i]; }
        void set_w_at(int i, real w) { w_[i] = w; }

        const std::vector<real>& get_w() const { return w_; }

        int get_cp_size() const { return (int)cp_.size(); }
        T get_cp_at(int i) const { return cp_[i]; }
        int get_knot_size() const { return (int)knots_.size(); }
        real get_knot_at(int i) const { return knots_[i]; }

        int order() const { return (int)knots_.size() - (int)cp_.size(); }
        int degree() const { return order() - 1; }

        const std::vector<real>& get_knots() const { return knots_; }

        void swap(this_type& rhs)
        {
            cp_.swap(rhs.cp_);
            w_.swap(rhs.w_);
            knots_.swap(rhs.knots_);
        }

    public:
        bool is_bezier_type() const
        {
            return bspline_is_bezier_type(&knots_[0], get_knot_size(), order());
        }
        bool is_open0() const
        {
            return bspline_is_open0(&knots_[0], get_knot_size(), order());
        }
        bool is_open1() const
        {
            return bspline_is_open1(&knots_[0], get_knot_size(), order());
        }
        bool is_open() const
        {
            return is_open0() && is_open1();
        }
        bool is_uniform() const
        {
            return bspline_is_uniform(&knots_[0], get_knot_size());
        }

    public:
        this_type& to_regular()
        {
            to_regular(knots_);
            return *this;
        }
        this_type& to_bezier_type()
        {
            to_regular();
            nurbs_convert_bezier_type(cp_, w_, knots_);
            to_regular();
            assert(is_bezier_type()); //
            return *this;
        }

    public:
        bool equal(const this_type& rhs) { return (cp_ == rhs.cp_) && (knots_ == rhs.knots_); }
        this_type& transform(const matrix3& m)
        {
            size_t sz = cp_.size();
            for (size_t i = 0; i < sz; i++)
            {
                cp_[i] = m * cp_[i];
            }
            return *this;
        }
        this_type& transform(const matrix4& m)
        {
            size_t sz = cp_.size();
            for (size_t i = 0; i < sz; i++)
            {
                cp_[i] = m * cp_[i];
            }
            return *this;
        }

    protected:
        static void to_regular(std::vector<real>& knots)
        {
            real min = knots.front();
            real max = knots.back();
            if (!(min == 0 && max == 1))
            {
                real wid = real(1.0) / (max - min);
                for (size_t i = 0; i < knots.size(); i++)
                {
                    knots[i] = (knots[i] - min) * wid;
                }
            }
        }

    private:
        std::vector<T> cp_;
        std::vector<real> w_;
        std::vector<real> knots_;
    };

    template <class T>
    inline bool operator==(const nurbs_curve<T>& lhs, const nurbs_curve<T>& rhs)
    {
        return lhs.equal(rhs);
    }

    template <class T>
    inline bool operator!=(const nurbs_curve<T>& lhs, const nurbs_curve<T>& rhs)
    {
        return !lhs.equal(rhs);
    }

    template <class T>
    inline nurbs_curve<T> operator*(const matrix3& m, const nurbs_curve<T>& rhs)
    {
        return nurbs_curve<T>(rhs).transform(m);
    }

    template <class T>
    inline nurbs_curve<T> operator*(const matrix4& m, const nurbs_curve<T>& rhs)
    {
        return nurbs_curve<T>(rhs).transform(m);
    }

    typedef nurbs_curve<real> nurbs_curve1;
    typedef nurbs_curve<vector2> nurbs_curve2;
    typedef nurbs_curve<vector3> nurbs_curve3;
    typedef nurbs_curve<vector4> nurbs_curve4;
}

#endif