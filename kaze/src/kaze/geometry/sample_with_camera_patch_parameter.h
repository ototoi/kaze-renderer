#ifndef KAZE_SAMPLE_WITH_CAMERA_PATCH_PARAMETER_H
#define KAZE_SAMPLE_WITH_CAMERA_PATCH_PARAMETER_H

#include "sample_patch_parameter.h"

namespace kaze
{

    void sample_patch_parameter(std::vector<vector2>& out, const bezier_patch<vector3>& patch, const matrix4& mat, int width, int height);
    void sample_patch_parameter(std::vector<vector2>& out, const multi_bezier_patch<vector3>& patch, const matrix4& mat, int width, int height);
}

#endif