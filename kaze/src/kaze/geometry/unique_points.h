#ifndef KAZE_UNIQUE_POINTS_H
#define KAZE_UNIQUE_POINTS_H

#include <vector>
#include "../core/types.h"

namespace kaze
{

    void unique_points(std::vector<vector2>& inout, real radius);
    void unique_points(std::vector<vector2>& out, const std::vector<vector2>& points, real radius);
    void unique_lines(std::vector<vector2>& inoutlines, real radius);
    void unique_lines(std::vector<vector2>& outlines, const std::vector<vector2>& lines, real radius);
    void unique_lines(std::vector<vector2>& vertices, std::vector<size_t>& indices, const std::vector<vector2>& lines, real radius);
    void unique_lines(std::vector<vector2>& vertices, std::vector<std::vector<size_t> >& indices, const std::vector<std::vector<vector2> >& lines, real radius);
}

#endif