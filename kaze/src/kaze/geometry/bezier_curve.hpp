#ifndef KAZE_BEZIER_CURVE_HPP
#define KAZE_BEZIER_CURVE_HPP

#include "bezier.h"
#include <vector>

namespace kaze
{

    template <class T>
    class bezier_curve
    {
    public:
        typedef T value_type;
        typedef bezier_curve<T> this_type;

    public:
        bezier_curve() {}
        bezier_curve(const this_type& rhs) : cp_(rhs.cp_) {}
        bezier_curve(const T* cp, int n)
        {
            cp_.resize(n);
            for (int i = 0; i < n; i++)
            {
                cp_[i] = cp[i];
            }
        }
        explicit bezier_curve(const std::vector<T>& cp) : cp_(cp) {}

        this_type& operator=(const this_type& rhs)
        {
            cp_ = rhs.cp_;
            return *this;
        }

        T evaluate(real t) const
        {
            return bezier_evaluate(&cp_[0], (int)cp_.size(), t);
        }

        T evaluate_deriv(real t) const
        {
            return bezier_evaluate_deriv(&cp_[0], (int)cp_.size(), t);
        }

        T evaluate_dPdT(real t) const
        {
            return evaluate_deriv(t);
        }

        void split(this_type curves[2], real t) const
        {
            size_t sz = cp_.size();
            curves[0].cp_.resize(sz);
            curves[1].cp_.resize(sz);
            bezier_split(&(curves[0].cp_[0]), &(curves[1].cp_[0]), &cp_[0], (int)sz, t);
        }
        T get_at(int i) const { return cp_[i]; }
        void set_at(int i, const T& p) { cp_[i] = p; }
        const std::vector<T>& get_cp() const { return cp_; }
        size_t size() const { return cp_.size(); }
        int order() const { return (int)size(); }
        int degree() const { return order() - 1; }

        int get_cp_size() const { return (int)cp_.size(); }
        T get_cp_at(int i) const { return cp_[i]; }

    public:
        bool equal(const this_type& rhs) { return cp_ == rhs.cp_; }
        this_type& transform(const matrix3& m)
        {
            size_t sz = cp_.size();
            for (size_t i = 0; i < sz; i++)
            {
                cp_[i] = m * cp_[i];
            }
            return *this;
        }
        this_type& transform(const matrix4& m)
        {
            size_t sz = cp_.size();
            for (size_t i = 0; i < sz; i++)
            {
                cp_[i] = m * cp_[i];
            }
            return *this;
        }

    private:
        std::vector<T> cp_;
    };

    template <class T>
    inline bool operator==(const bezier_curve<T>& lhs, const bezier_curve<T>& rhs)
    {
        return lhs.equal(rhs);
    }

    template <class T>
    inline bool operator!=(const bezier_curve<T>& lhs, const bezier_curve<T>& rhs)
    {
        return !lhs.equal(rhs);
    }

    template <class T>
    inline bezier_curve<T> operator*(const matrix3& m, const bezier_curve<T>& rhs)
    {
        return bezier_curve<T>(rhs).transform(m);
    }

    template <class T>
    inline bezier_curve<T> operator*(const matrix4& m, const bezier_curve<T>& rhs)
    {
        return bezier_curve<T>(rhs).transform(m);
    }

    typedef bezier_curve<real> bezier_curve1;
    typedef bezier_curve<vector2> bezier_curve2;
    typedef bezier_curve<vector3> bezier_curve3;
    typedef bezier_curve<vector4> bezier_curve4;
}

#endif
