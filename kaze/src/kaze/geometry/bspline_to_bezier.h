#ifndef KAZE_BSPLINE_TO_BEZIER_H
#define KAZE_BSPLINE_TO_BEZIER_H

#include "bspline_curve.hpp"
#include "bezier_curve.hpp"
#include "multi_bezier_curve.hpp"

#include "bspline_patch.hpp"
#include "bezier_patch.hpp"
#include "multi_bezier_patch.hpp"

#include <vector>

namespace kaze
{

#define DEF_BSP_TO_BZR(TYPE)                                                               \
    void bspline_to_bezier(multi_bezier_curve<TYPE>& bzr, const bspline_curve<TYPE>& bsp); \
    void bspline_to_bezier(multi_bezier_patch<TYPE>& bzr, const bspline_patch<TYPE>& bsp);

    DEF_BSP_TO_BZR(real)
    DEF_BSP_TO_BZR(vector2)
    DEF_BSP_TO_BZR(vector3)

#undef DEF_BSP_TO_BZR
}

#endif