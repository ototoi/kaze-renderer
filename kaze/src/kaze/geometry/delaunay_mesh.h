#ifndef KAZE_DELAUNAY_MESH_H
#define KAZE_DELAUNAY_MESH_H

#include "types.h"
#include <vector>

namespace kaze
{

    struct delaunay_triangle
    {
        size_t points[3];
        size_t neighbor[3];
        unsigned int edge_type[3];
    };

    struct delaunay_mesh
    {
        std::vector<vector2> points;
        std::vector<delaunay_triangle> trinagles;
    };
}

#endif