#ifndef KAZE_TRIANGLE_QUAD_TREE_H
#define KAZE_TRIANGLE_QUAD_TREE_H

#include "../core/values.h"
#include <vector>

namespace kaze
{

    class triangle_quad_tree
    {
    public:
        struct tri_struct
        {
            vector2 c[3];
        };

        class quad_tree_node
        {
        public:
            virtual ~quad_tree_node() {}
            virtual bool test(const vector2& p) const = 0;
        };

    public:
        triangle_quad_tree(const std::vector<vector2>& tri);
        ~triangle_quad_tree();
        bool test(const vector2& p) const;

    public:
        quad_tree_node* node_;
        std::vector<tri_struct> mv_;
    };
}

#endif