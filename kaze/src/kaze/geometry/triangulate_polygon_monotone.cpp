#include "triangulate_polygon_monotone.h"

#include <algorithm>
#include <vector>
#include <list>
#include <map>
#include <set>
#include <iterator>

namespace
{
    using namespace kaze;

    enum
    {
        START_VERTEX,
        SPLIT_VERTEX,
        REGULAR_VERTEX_L, //upper->lower
        REGULAR_VERTEX_R, //lower->upper
        REGULAR_VERTEX_C, //plane
        END_VERTEX,
        MERGE_VERTEX
    };

    enum
    {
        ORIENTATION_CW = 0,
        ORIENTATION_CCW,
        ORIENTATION_UNKNOWN
    };

    static double det(double a, double b, double c, double d)
    {
        return a * d - b * c;
    }

    static int calc_determinant(const vector2& a, const vector2& b, const vector2& c)
    {
        vector2 ab = b - a;
        vector2 ac = c - a;
        double Determ = det(ab[0], ac[0], ab[1], ac[1]); //ad-bc

        if (Determ > 0)
        {
            return ORIENTATION_CCW;
        }
        else if (Determ < 0)
        {
            return ORIENTATION_CW;
        }
        else
        {
            return ORIENTATION_UNKNOWN;
        }
    }

    static int get_monotone_vertex_type(const vector2& prev, const vector2& crnt, const vector2& next)
    {
        int type = 0;
        if (prev[1] > crnt[1])
        {
            type |= 1;
        }
        else if (prev[1] < crnt[1])
        {
            type |= 2;
        }

        if (next[1] > crnt[1])
        {
            type |= 4;
        }
        else if (next[1] < crnt[1])
        {
            type |= 8;
        }

        //prev == crnt && crnt <  next    //0|4
        //prev >  crnt && crnt == next    //1|0
        //prev >  crnt && crnt <  next    //1|4

        //prev == crnt && crnt >  next    //0|8
        //prev <  crnt && crnt == next    //2|0
        //prev <  crnt && crnt >  next    //2|8

        switch (type)
        {
        case 1:
        case 4:
        case 5: //END or MERGE
        {
            int det = calc_determinant(prev, crnt, next);
            if (det == ORIENTATION_CCW)
            {
                return END_VERTEX;
            }
            else
            {
                return MERGE_VERTEX;
            }
        }
        break;
        case 2:
        case 8:
        case 10: //START or SPLIT
        {
            int det = calc_determinant(prev, crnt, next);
            if (det == ORIENTATION_CCW)
            {
                return START_VERTEX;
            }
            else
            {
                return SPLIT_VERTEX;
            }
        }
        break;
        default:
            if (crnt[1] == next[1] && crnt[1] == prev[1])
            {
                return REGULAR_VERTEX_C;
            }
            else if (crnt[1] > next[1])
            {
                return REGULAR_VERTEX_L;
            }
            else
            {
                return REGULAR_VERTEX_R;
            }
        }
    }

    static void get_monotone_vertex_type_internal(std::vector<int>& type, const std::vector<vector2>& v)
    {
        size_t sz = v.size();
        type.resize(sz);
        for (size_t i = 0; i < sz; i++)
        {
            vector2 prev = (i == 0) ? v[sz - 1] : v[i - 1];
            vector2 crnt = v[i];
            vector2 next = ((i + 1) == sz) ? v[0] : v[i + 1];
            type[i] = get_monotone_vertex_type(prev, crnt, next);
        }
    }

    static bool vertex_compare(const vector2& va, const vector2& vb)
    {
        if (va[1] == vb[1])
        {
            return va[0] < vb[0];
        }
        else
        {
            return va[1] > vb[1];
        }
    }

    struct vertex_sorter
    {
        vertex_sorter(const std::vector<vector2>& vv) : v(vv) {}

        bool operator()(int a, int b) const
        {
            vector2 va = v[a];
            vector2 vb = v[b];

            return vertex_compare(va, vb);
        }

        const std::vector<vector2>& v;
    };

    typedef std::map<int, int> iimap;
    typedef std::pair<int, int> iipair;

    static real get_intersection(const vector2& a, const vector2& b, real v)
    {
        real k = (v - a[1]) / (b[1] - a[1]);
        assert(0 <= k && k <= 1);
        real u = (1 - k) * a[0] + k * b[0];
        return u;
    }

    static bool get_intersection(real& x, const vector2& v0, const vector2& v1, real y)
    {
        static const real EPSILON = values::epsilon() * 1024;

        double a = v0[1];
        double b = v1[1];

        if (a > b)
        {
            std::swap(a, b);
        }

        if (fabs(b - a) < EPSILON)
        {
            return false;
        }

        if (a <= y && y < b)
        { //or a<v&&v<b
            x = get_intersection(v0, v1, y);
            return true;
        }

        return false;
    }

    static int get_left_edge(const iimap& T, int vi, const std::vector<vector2>& v)
    {
        if (T.empty()) return -1;

        vector2 p = v[vi];
        std::vector<std::pair<int, real> > tmp;
        for (iimap::const_iterator i = T.begin(); i != T.end(); i++)
        {
            int ia = i->first;
            int ib = (ia + 1 == v.size()) ? 0 : ia + 1;
            vector2 pa = v[ia];
            vector2 pb = v[ib];
            real x = 0;
            if (get_intersection(x, pa, pb, p[1]))
            {
                if (x < p[0])
                {
                    tmp.push_back(std::make_pair(ia, x));
                }
            }
        }
        if (tmp.empty()) return -1;
        int nRet = tmp[0].first;
        real dRet = tmp[0].second;
        for (size_t i = 1; i < tmp.size(); i++)
        {
            if (dRet < tmp[i].second)
            {
                nRet = tmp[i].first;
                dRet = tmp[i].second;
            }
        }
        return nRet;
    }

    void get_monotone_diagonal_internal(std::vector<iipair>& D, const std::vector<vector2>& v)
    {
        int sz = (int)v.size();
        std::vector<int> TYPE(sz);
        for (int i = 0; i < sz; i++)
        {
            vector2 prev = (i == 0) ? v[sz - 1] : v[i - 1];
            vector2 crnt = v[i];
            vector2 next = ((i + 1) == sz) ? v[0] : v[i + 1];
            TYPE[i] = get_monotone_vertex_type(prev, crnt, next);
        }

        std::vector<int> Q(sz);
        for (int i = 0; i < sz; i++)
        {
            Q[i] = i;
        }
        std::sort(Q.begin(), Q.end(), vertex_sorter(v));

        iimap T; //VERTEX,HELPER
        for (int i = 0; i < sz; i++)
        {
            int vi = Q[i];
            int type = TYPE[vi];
            switch (type)
            {
            case START_VERTEX:
            {
                T[vi] = vi;
            }
            break;
            case SPLIT_VERTEX:
            {
                int ej = get_left_edge(T, vi, v);
                if (ej >= 0)
                {
                    int hi = T[ej]; //
                    D.push_back(iipair(vi, hi));
                    T[ej] = vi;
                }
                //
                T[vi] = vi;
            }
            break;
            case REGULAR_VERTEX_L:
            {
                int ej = (vi == 0) ? sz - 1 : vi - 1;
                iimap::iterator it;
                if ((it = T.find(ej)) != T.end())
                {
                    int hi = it->second;
                    if (TYPE[hi] == MERGE_VERTEX)
                    {
                        D.push_back(iipair(vi, hi));
                    }
                    T.erase(ej);
                }
                T[vi] = vi;
            }
            break;
            case REGULAR_VERTEX_R:
            {
                int ej = get_left_edge(T, vi, v);
                if (ej >= 0)
                {
                    int hi = T[ej]; //
                    if (TYPE[hi] == MERGE_VERTEX)
                    {
                        D.push_back(iipair(vi, hi));
                    }
                    T[ej] = vi;
                }
            }
            break;
            case END_VERTEX:
            {
                int ej = (vi == 0) ? sz - 1 : vi - 1;
                iimap::iterator it;
                if ((it = T.find(ej)) != T.end())
                {
                    int hi = it->second;
                    if (TYPE[hi] == MERGE_VERTEX)
                    {
                        D.push_back(iipair(vi, hi));
                    }
                    T.erase(ej);
                }
            }
            break;
            case MERGE_VERTEX:
            {
                {
                    int ej = (vi == 0) ? sz - 1 : vi - 1;
                    iimap::iterator it;
                    if ((it = T.find(ej)) != T.end())
                    {
                        int hi = it->second;
                        if (TYPE[hi] == MERGE_VERTEX)
                        {
                            D.push_back(iipair(vi, hi));
                        }
                        T.erase(ej);
                    }
                }
                {
                    int ej = get_left_edge(T, vi, v);
                    if (ej >= 0)
                    {
                        int hi = T[ej]; //
                        if (TYPE[hi] == MERGE_VERTEX)
                        {
                            D.push_back(iipair(vi, hi));
                        }
                        T[ej] = vi;
                    }
                }
            }
            break;
            }
        }
#ifdef _DEBUG
        std::set<int> S;
        for (size_t i = 0; i < D.size(); i++)
        {
            S.insert(D[i].first);
            S.insert(D[i].second);
        }
        for (int i = 0; i < sz; i++)
        {
            int tp = TYPE[i];
            if (tp == SPLIT_VERTEX || tp == MERGE_VERTEX)
            {
                if (S.find(i) == S.end())
                {
                    assert(0);
                    return;
                }
            }
        }
#endif
    }

    static inline bool split_loop(std::vector<int>& a, std::vector<int>& b, const std::vector<int>& v, const std::pair<int, int>& d)
    {
        int p = d.first;
        int q = d.second;
        std::vector<int>::const_iterator ip = std::find(v.begin(), v.end(), p);
        if (ip != v.end())
        {
            std::vector<int>::const_iterator iq = std::find(v.begin(), v.end(), q);
            if (iq != v.end())
            {
                if (ip > iq) std::swap(ip, iq);
                std::copy(v.begin(), ip + 1, std::back_inserter(a));
                std::copy(iq, v.end(), std::back_inserter(a));

                std::copy(ip, iq + 1, std::back_inserter(b));
                return true;
            }
        }
        return false;
    }

    struct pair_sorter
    {
        bool operator()(const std::pair<int, int>& a, const std::pair<int, int>& b) const
        {
            if (a.first != b.first)
            {
                return a.first < b.first;
            }
            else
            {
                return a.second < b.second;
            }
        }
    };

    static void split_monotone_polygon_internal(std::vector<std::vector<vector2> >& loops, const std::vector<vector2>& v)
    {
        std::vector<std::pair<int, int> > D;
        get_monotone_diagonal_internal(D, v);
        typedef std::list<std::vector<int> > ivlist;
        std::list<std::vector<int> > lps;

        int sz = (int)v.size();
        std::vector<int> tmp(sz);
        for (int i = 0; i < sz; i++)
        {
            tmp[i] = i;
        }
        lps.push_back(tmp);

        if (!D.empty())
        {
            for (int i = 0; i < (int)D.size(); i++)
            {
                if (D[i].first > D[i].second) std::swap(D[i].first, D[i].second);
            }
            std::sort(D.begin(), D.end(), pair_sorter());

            std::vector<int> a;
            a.reserve(sz);
            std::vector<int> b;
            b.reserve(sz);
            for (int i = 0; i < (int)D.size(); i++)
            {
                for (ivlist::iterator j = lps.begin(); j != lps.end(); j++)
                {
                    a.clear();
                    b.clear();
                    if (split_loop(a, b, *j, D[i]))
                    {
                        lps.insert(j, a);
                        lps.insert(j, b);
                        lps.erase(j);
                        break;
                    }
                }
            }
        }

        loops.resize(lps.size());
        int jj = 0;
        for (ivlist::iterator j = lps.begin(); j != lps.end(); j++)
        {
            const std::vector<int>& intvec = *j;
            std::vector<vector2> vv(intvec.size());
            for (size_t i = 0; i < intvec.size(); i++)
            {
                vv[i] = v[intvec[i]];
            }
            loops[jj].swap(vv);
            jj++;
        }
    }

    enum
    {
        CHAIN_L,
        CHAIN_R,
        CHAIN_T
    };

    static inline int next_index(int i, int sz)
    {
        int n = i + 1;
        return (n == sz) ? 0 : n;
    }

    static inline int get_chain_type(const vector2& start, const vector2& end)
    {
        if (start[1] == end[1])
        {
            return CHAIN_T;
        }
        else
        {
            if (start[1] > end[1])
            {
                return CHAIN_L;
            }
            else
            {
                return CHAIN_R;
            }
        }
    }

    static int set_next_chain(int i, std::vector<int>& chain_type)
    {
        if (chain_type[i] == CHAIN_T)
        {
            chain_type[i] = set_next_chain(next_index(i, (int)chain_type.size()), chain_type);
        }
        return chain_type[i];
    }

    static bool triangulate_monotone_polygon_internal(std::vector<vector2>& tri, const std::vector<vector2>& v)
    {
        int sz = (int)v.size();
        if (sz <= 2) return false;
        std::vector<int> Q(sz);
        for (int i = 0; i < sz; i++)
        {
            Q[i] = i;
        }
        std::sort(Q.begin(), Q.end(), vertex_sorter(v));
        std::vector<int> chain_type(sz);
        for (int i = 0; i < sz; i++)
        {
            int crnt = i;
            int next = next_index(i, sz);

            chain_type[i] = (get_chain_type(v[crnt], v[next]));
        }
        for (int i = 0; i < sz; i++)
        {
            chain_type[i] = set_next_chain(i, chain_type);
        }

        std::vector<int> indices;
        std::vector<int> S; //stack
        int CHAIN = 0;
        S.push_back(Q[0]);
        S.push_back(Q[1]);
        CHAIN = chain_type[Q[1]];

        for (int i = 2; i < sz; i++)
        {
            //int CP = Q[i];
            int TCHAIN = chain_type[Q[i]];
            if (CHAIN != TCHAIN)
            {
                for (size_t j = 0; j < S.size() - 1; j++)
                {

                    if (CHAIN == CHAIN_L)
                    {
                        indices.push_back(S[j]);
                        indices.push_back(S[j + 1]);
                        indices.push_back(Q[i]);
                    }
                    else
                    {
                        indices.push_back(S[j + 1]);
                        indices.push_back(S[j]);
                        indices.push_back(Q[i]);
                    }
                }
                int last = S.back();
                S.clear();
                S.push_back(last);
                S.push_back(Q[i]);
            }
            else
            {
                while (1)
                {
                    int ssz = (int)S.size();
                    if (ssz < 2)
                    {
                        break;
                    }
                    int iprev;
                    int icrnt;
                    int inext;
                    if (CHAIN == CHAIN_L)
                    {
                        iprev = S[ssz - 2];
                        icrnt = S[ssz - 1];
                        inext = Q[i];
                    }
                    else
                    {
                        iprev = Q[i];
                        icrnt = S[ssz - 1];
                        inext = S[ssz - 2];
                    }

                    vector2 prev = v[iprev];
                    vector2 crnt = v[icrnt];
                    vector2 next = v[inext];
                    int nRet = calc_determinant(prev, crnt, next);
                    if (nRet == ORIENTATION_CCW)
                    {

                        indices.push_back(iprev);
                        indices.push_back(icrnt);
                        indices.push_back(inext);

                        S.pop_back();
                    }
                    else
                    {
                        break;
                    }
                }
                S.push_back(Q[i]);
            }
            CHAIN = TCHAIN;
        }
        if (!S.empty())
        {
            if (S.size() == 3)
            {
                if (CHAIN == CHAIN_L)
                {
                    indices.push_back(S[0]);
                    indices.push_back(S[1]);
                    indices.push_back(S[2]);
                }
                else
                {
                    indices.push_back(S[2]);
                    indices.push_back(S[1]);
                    indices.push_back(S[0]);
                }
            }
        }

        size_t isz = indices.size();
        assert((isz % 3) == 0);

        tri.reserve(tri.size() + isz);
        for (size_t i = 0; i < isz; i++)
        {
            tri.push_back(v[indices[i]]);
        }

        return true;
    }

    static bool check_monotone(const std::vector<vector2>& v)
    {
        int count = 0;
        size_t sz = v.size();
        for (size_t i = 0; i < sz; i++)
        {
            vector2 prev = v[(i == 0) ? (sz - 1) : i - 1];
            vector2 crnt = v[i];
            vector2 next = ((i + 1) == sz) ? v[0] : v[i + 1];
            int nType = get_monotone_vertex_type(prev, crnt, next);
            if (nType == SPLIT_VERTEX || nType == MERGE_VERTEX)
            {
                return false;
            }
        }
        return true;
    }
}

namespace kaze
{
    //----------------------------------------------------------------------------------------
    bool split_monotone_polygon(std::vector<std::vector<vector2> >& vs, const std::vector<vector2>& v)
    {
        if (v.size() < 3) return false;
        if (v.back() == v.front())
        {
            std::vector<vector2> tmp(v);
            tmp.pop_back();
            if (tmp.size() < 3) return false;
            split_monotone_polygon_internal(vs, tmp);
        }
        else
        {
            split_monotone_polygon_internal(vs, v);
        }

        return true;
    }

    void get_monotone_diagonal(std::vector<std::pair<int, int> >& D, const std::vector<vector2>& loop)
    {
        get_monotone_diagonal_internal(D, loop);
    }

    void get_monotone_vertex_type(std::vector<int>& type, const std::vector<vector2>& loop)
    {
        get_monotone_vertex_type_internal(type, loop);
    }

    bool triangulate_monotone_polygon(std::vector<vector2>& tri, const std::vector<vector2>& loop)
    {
        return triangulate_monotone_polygon_internal(tri, loop);
    }

    bool triangulate_polygon_monotone(std::vector<vector2>& tri, const std::vector<vector2>& loop)
    {
        std::vector<std::vector<vector2> > vs;
        if (split_monotone_polygon(vs, loop))
        {
            size_t sz = vs.size();
            for (size_t i = 0; i < sz; i++)
            {
                assert(check_monotone(vs[i]));
                triangulate_monotone_polygon(tri, vs[i]);
            }
            return true;
        }
        return false;
    }
}
