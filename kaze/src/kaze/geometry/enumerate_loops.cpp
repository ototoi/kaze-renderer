#include "enumerate_loops.h"

#include "simple_polygon.h"
#include "move_points_online.h"
#include "loop_test_points.h"

namespace kaze
{

    static bool test_bound(const vector2& min, const vector2& max, const vector2& p)
    {
        for (int i = 0; i < 2; i++)
        {
            if (p[i] < min[i]) return false;
            if (p[i] > max[i]) return false;
        }
        return true;
    }

    static bool test_inner(const vector2& amin, const vector2& amax, const vector2& bmin, const vector2& bmax)
    {
        vector2 b0(bmin[0], bmin[1]);
        vector2 b1(bmin[0], bmax[1]);
        vector2 b2(bmax[0], bmin[1]);
        vector2 b3(bmax[0], bmax[1]);
        if (!test_bound(amin, amax, b0)) return false;
        if (!test_bound(amin, amax, b1)) return false;
        if (!test_bound(amin, amax, b2)) return false;
        if (!test_bound(amin, amax, b3)) return false;
        return true;
    }

    static void get_minmax(vector2& min, vector2& max, const std::vector<vector2>& v)
    {
        min = max = v[0];
        size_t sz = v.size();
        for (size_t i = 0; i < sz; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                if (min[j] > v[i][j])
                {
                    min[j] = v[i][j];
                }
                else if (max[j] < v[i][j])
                {
                    max[j] = v[i][j];
                }
            }
        }
    }

    bool enumerate_loops(
        std::vector<polyloop_group>& loop_sets,
        const std::vector<std::vector<vector2> >& outer_loops,
        const std::vector<std::vector<vector2> >& inner_loops,
        const std::vector<vector2>& points,
        int nFillType)
    {
        //-------------------------------------
        std::vector<std::vector<vector2> > on;
        if (!outer_loops.empty())
        {
            if (nFillType != 4)
            {
                on.reserve(outer_loops.size());
                for (size_t i = 0; i < outer_loops.size(); i++)
                {
                    std::vector<std::vector<vector2> > lps;
                    if (!split_polygon(lps, outer_loops[i], nFillType)) return false;
                    for (int j = 0; j < lps.size(); j++)
                    {
                        on.push_back(lps[j]);
                    }
                }
            }
            else
            {
                on = outer_loops;
            }
        }
        else
        { //empty
            std::vector<vector2> loop;
            vector2 p0 = vector2(0, 0);
            vector2 p1 = vector2(1, 0);
            vector2 p2 = vector2(1, 1);
            vector2 p3 = vector2(0, 1);
            loop.push_back(p0);
            loop.push_back(p1);
            loop.push_back(p2);
            loop.push_back(p3);

            on.push_back(loop);
        }
        //-------------------------------------
        std::vector<std::vector<vector2> > in;
        {
            if (nFillType != 4)
            {
                in.reserve(inner_loops.size() * 2);
                for (size_t i = 0; i < inner_loops.size(); i++)
                {
                    std::vector<std::vector<vector2> > lps;
                    if (!split_polygon(lps, inner_loops[i], nFillType)) return false;
                    for (int j = 0; j < lps.size(); j++)
                    {
                        in.push_back(lps[j]);
                    }
                }
            }
            else
            {
                in = inner_loops;
            }
        }
        //-------------------------------------
        std::vector<vector2> samples = points;
        if (!samples.empty())
        {
            for (size_t i = 0; i < on.size(); i++)
            {
                std::vector<vector2> lp;
                std::vector<vector2> pt;
                if (!move_points_onloop(lp, pt, on[i], samples)) return false;
                on[i].swap(lp);
                samples.swap(pt);
            }
            //-------------------------------------
            for (size_t i = 0; i < in.size(); i++)
            {
                std::vector<vector2> lp;
                std::vector<vector2> pt;
                if (!move_points_onloop(lp, pt, in[i], samples)) return false;
                in[i].swap(lp);
                samples.swap(pt);
            }
            //-------------------------------------
            for (size_t i = 0; i < in.size(); i++)
            {
                std::vector<vector2> pt;
                if (!loop_outer_points(pt, in[i], samples)) return false;
                samples.swap(pt);
            }
        }

        std::vector<vector2> omin_set(on.size());
        std::vector<vector2> omax_set(on.size());
        for (size_t i = 0; i < on.size(); i++)
        {
            get_minmax(omin_set[i], omax_set[i], on[i]);
        }
        std::vector<vector2> imin_set(in.size());
        std::vector<vector2> imax_set(in.size());
        for (size_t i = 0; i < in.size(); i++)
        {
            get_minmax(imin_set[i], imax_set[i], in[i]);
        }
        //-------------------------------------
        for (size_t i = 0; i < in.size(); i++)
        {
            set_orientation_inner_loop(in[i]);
        }
        //-------------------------------------
        loop_sets.resize(on.size());
        for (size_t i = 0; i < on.size(); i++)
        {
            if (!samples.empty())
            {
                std::vector<vector2> pt;
                if (!loop_inner_points(pt, on[i], samples)) return false;
                loop_sets[i].points.swap(pt);
            }
            loop_sets[i].inner_loops.reserve(in.size());
            for (size_t j = 0; j < in.size(); j++)
            {
                if (test_inner(omin_set[i], omax_set[i], imin_set[j], imax_set[j]))
                {
                    if (test_loop_inner_points(on[i], in[j]))
                    {
                        loop_sets[i].inner_loops.push_back(in[j]);
                    }
                }
            }
            loop_sets[i].outer_loop.swap(on[i]);
        }
        return true;
    }
}
