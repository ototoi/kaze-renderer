#ifndef KAZE_MULTI_BEZIER_PATCH_HPP
#define KAZE_MULTI_BEZIER_PATCH_HPP

#include <vector>
#include <utility>
#include <limits>
#include "bezier_patch.hpp"

namespace kaze
{

    template <class T>
    class multi_bezier_patch
    {
    public:
        typedef T value_type;
        typedef multi_bezier_patch<T> this_type;
        typedef bezier_patch<T> patch_type;

    public:
        multi_bezier_patch()
        {
            static const real params[] = {0, 1};
            knots_u_.assign(params, params + 2);
            knots_v_.assign(params, params + 2);
            patches_.push_back(patch_type());
        }

        explicit multi_bezier_patch(const bezier_patch<T>& rhs)
        {
            static const real params[] = {0, 1};
            knots_u_.assign(params, params + 2);
            knots_v_.assign(params, params + 2);
            patches_.push_back(rhs);
        }
        multi_bezier_patch(const std::vector<real>& knot_u, const std::vector<real>& knot_v, const std::vector<bezier_patch<T> >& patches)
            : knots_u_(knot_u), knots_v_(knot_v), patches_(patches)
        {
        }

        multi_bezier_patch(const std::vector<bezier_patch<T> >& patches, const std::vector<real>& knot_u, const std::vector<real>& knot_v)
            : knots_u_(knot_u), knots_v_(knot_v), patches_(patches)
        {
        }

        multi_bezier_patch(const multi_bezier_patch<T>& rhs)
            : knots_u_(rhs.knots_u_), knots_v_(rhs.knots_v_), patches_(rhs.patches_)
        {
        }

        this_type& operator=(const multi_bezier_patch<T>& rhs)
        {
            knots_u_ = rhs.knots_u_;
            knots_v_ = rhs.knots_v_;
            patches_ = rhs.patches_;
            return *this;
        }

        T evaluate(real u, real v) const
        {
            int i = find_knot_u(u);
            int j = find_knot_v(v);
            real uu = div((u - knots_u_[i]), (knots_u_[i + 1] - knots_u_[i]));
            real vv = div((v - knots_v_[j]), (knots_v_[j + 1] - knots_v_[j]));
            return get_patch_at(i, j).evaluate(uu, vv);
        }

        T evaluate_deriv_u(real u, real v) const
        {
            int i = find_knot_u(u);
            int j = find_knot_v(v);
            real uu = div((u - knots_u_[i]), (knots_u_[i + 1] - knots_u_[i]));
            real vv = div((v - knots_v_[j]), (knots_v_[j + 1] - knots_v_[j]));
            return get_patch_at(i, j).evaluate_deriv_u(uu, vv);
        }

        T evaluate_dPdU(real u, real v) const
        {
            return evaluate_deriv_u(u, v);
        }

        T evaluate_deriv_v(real u, real v) const
        {
            int i = find_knot_u(u);
            int j = find_knot_v(v);
            real uu = div((u - knots_u_[i]), (knots_u_[i + 1] - knots_u_[i]));
            real vv = div((v - knots_v_[j]), (knots_v_[j + 1] - knots_v_[j]));
            return get_patch_at(i, j).evaluate_deriv_v(uu, vv);
        }

        T evaluate_dPdV(real u, real v) const
        {
            return evaluate_deriv_v(u, v);
        }

        T min() const
        {
            T min = patches_[0].min();
            for (size_t i = 1; i < patches_.size(); i++)
            {
                min = cp_min(min, patches_[i].min());
            }
            return min;
        }

        T max() const
        {
            T max = patches_[0].max();
            for (size_t i = 1; i < patches_.size(); i++)
            {
                max = cp_max(max, patches_[i].max());
            }
            return max;
        }

        void swap(multi_bezier_patch<T>& rhs)
        {
            knots_u_.swap(rhs.knots_u_);
            knots_v_.swap(rhs.knots_v_);
            patches_.swap(rhs.patches_);
        }

    public:
        patch_type& get_patch_at(int i, int j)
        {
            return patches_[get_patch_size_u() * j + i];
        }

        const patch_type& get_patch_at(int i, int j) const
        {
            return patches_[get_patch_size_u() * j + i];
        }

        int get_patch_size_u() const
        {
            return (int)(knots_u_.size() - 1);
        }

        int get_patch_size_v() const
        {
            return (int)(knots_v_.size() - 1);
        }

        bool equal(const this_type& rhs) const
        {
            if (knots_u_ != rhs.knots_u_) return false;
            if (knots_v_ != rhs.knots_v_) return false;
            if (patches_ != rhs.patches_) return false;
            return true;
        }

        this_type& transform(const matrix3& m)
        {
            size_t sz = patches_.size();
            for (size_t i = 0; i < sz; i++)
            {
                patches_[i].transform(m);
            }
            return *this;
        }

        this_type& transform(const matrix4& m)
        {
            size_t sz = patches_.size();
            for (size_t i = 0; i < sz; i++)
            {
                patches_[i].transform(m);
            }
            return *this;
        }

        int get_knot_size_u() const { return (int)knots_u_.size(); }
        real get_knot_at_u(int i) const { return knots_u_[i]; }
        int get_knot_size_v() const { return (int)knots_v_.size(); }
        real get_knot_at_v(int i) const { return knots_v_[i]; }

        const std::vector<real>& get_knots_u() const { return knots_u_; }
        const std::vector<real>& get_knots_v() const { return knots_v_; }
    public:
        void split(this_type patches[4], real u, real v) const
        {
            assert(0); //
        }

    protected:
        int find_knot_u(real u) const
        {
            assert(knots_u_.size() >= 2);
            assert(knots_u_.front() == 0);
            assert(knots_u_.back() == 1);

            return get_range(knots_u_, u);
        }

        int find_knot_v(real v) const
        {
            assert(knots_v_.size() >= 2);
            assert(knots_v_.front() == 0);
            assert(knots_v_.back() == 1);

            return get_range(knots_v_, v);
        }

    protected:
        template <class X>
        static X cp_min(const X& a, const X& b)
        {
            return std::min(a, b);
        }
        template <class X>
        static X cp_max(const X& a, const X& b)
        {
            return std::max(a, b);
        }

#define DEF_CPMINMAX(TYPE, SZ)                       \
    static TYPE cp_min(const TYPE& a, const TYPE& b) \
    {                                                \
        TYPE c = a;                                  \
        for (int i = 0; i < SZ; i++)                 \
        {                                            \
            c[i] = std::min(c[i], b[i]);             \
        }                                            \
        return c;                                    \
    }                                                \
    static TYPE cp_max(const TYPE& a, const TYPE& b) \
    {                                                \
        TYPE c = a;                                  \
        for (int i = 0; i < SZ; i++)                 \
        {                                            \
            c[i] = std::max(c[i], b[i]);             \
        }                                            \
        return c;                                    \
    }

        DEF_CPMINMAX(vector2, 2)
        DEF_CPMINMAX(vector3, 3)
        DEF_CPMINMAX(vector4, 4)

#undef DEF_CPMINMAX

        static int get_range(const std::vector<real>& ranges, real x)
        {
            int sz = (int)ranges.size();
            if (sz <= 2) return 0;
#if 0
            if(x<ranges[   0])return 0;
            for(int i = 1;i<sz;i++){
                if(x<ranges[i])return i-1;
            }
            return sz-2;
#else
            if (x <= ranges[0]) return 0;
            if (ranges[sz - 1] <= x) return sz - 2;

            int a = 0;
            int b = sz - 1;
            while (1)
            {
                int m = a + ((b - a) >> 1);
                if (ranges[m] <= x)
                {
                    if (x < ranges[m + 1])
                    {
                        return m;
                    }
                    else
                    {
                        a = m + 1;
                    }
                }
                else
                {
                    b = m;
                }
            }
            return a;
#endif
        }
        static real div(real a, real b)
        {
            static real EPSILON = std::numeric_limits<real>::epsilon();
            if (fabs(b) <= EPSILON) return 0;
            return a / b;
        }

    protected:
        std::vector<real> knots_u_;
        std::vector<real> knots_v_;
        std::vector<patch_type> patches_;
    };

    template <class T>
    inline bool operator==(const multi_bezier_patch<T>& lhs, const multi_bezier_patch<T>& rhs)
    {
        return lhs.equal(rhs);
    }

    template <class T>
    inline bool operator!=(const multi_bezier_patch<T>& lhs, const multi_bezier_patch<T>& rhs)
    {
        return !lhs.equal(rhs);
    }

    template <class T>
    inline multi_bezier_patch<T> operator*(const matrix3& m, const multi_bezier_patch<T>& patch)
    {
        return multi_bezier_patch<T>(patch).transform(m);
    }

    template <class T>
    inline multi_bezier_patch<T> operator*(const matrix4& m, const multi_bezier_patch<T>& patch)
    {
        return multi_bezier_patch<T>(patch).transform(m);
    }

    typedef multi_bezier_patch<real> multi_bezier_patch1;
    typedef multi_bezier_patch<vector2> multi_bezier_patch2;
    typedef multi_bezier_patch<vector3> multi_bezier_patch3;
    typedef multi_bezier_patch<vector4> multi_bezier_patch4;
}

#endif
