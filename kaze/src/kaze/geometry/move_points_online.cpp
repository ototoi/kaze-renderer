#include "move_points_online.h"
#include <utility>
#include <algorithm>
#include <vector>
#include <map>
#include <memory>

#include "values.h"

namespace kaze
{

    namespace
    {

        struct edge
        {
            const vector2* p0;
            const vector2* p1;

            vector2 min() const
            {
                return vector2(
                    std::min<real>((*p0)[0], (*p1)[0]),
                    std::min<real>((*p0)[1], (*p1)[1]));
            }
            vector2 max() const
            {
                return vector2(
                    std::max<real>((*p0)[0], (*p1)[0]),
                    std::max<real>((*p0)[1], (*p1)[1]));
            }

            vector2 evaluate(real t) const
            {
                return ((*p0) * (1 - t)) + ((*p1) * (t));
            }
        };

        typedef const edge* PCEDGE;

        typedef std::vector<PCEDGE>::iterator PCEDGE_ITERATOR;

        static void get_minmax(vector2& min, vector2& max, PCEDGE_ITERATOR a, PCEDGE_ITERATOR b)
        {
            min = (*a)->min();
            max = (*a)->max();
            vector2 cmin;
            vector2 cmax;
            for (PCEDGE_ITERATOR i = a; i != b; i++)
            {
                cmin = (*i)->min();
                cmax = (*i)->max();
                for (int j = 0; j < 2; j++)
                {
                    if (min[j] > cmin[j]) min[j] = cmin[j];
                    if (max[j] < cmax[j]) max[j] = cmax[j];
                }
            }
        }

        struct edge_sorter
        {
            edge_sorter(int p) : plane(p) {}
            bool operator()(PCEDGE a, PCEDGE b) const
            {
                real ac = a->min()[plane] + a->max()[plane];
                real bc = b->min()[plane] + b->max()[plane];
                return ac < bc;
            }
            int plane;
        };

        static inline real get_distance2(real x0, real y0, real x1, real y1, real px, real py)
        {
            real dx = x1 - x0;
            real dy = y1 - y0;
            real a = dx * dx + dy * dy;

            if (a <= values::epsilon())
            {
                return ((x0 - px) * (x0 - px) + (y0 - py) * (y0 - py));
            }

            real b = dx * (x0 - px) + dy * (y0 - py);
            real t = -(b / a);

            if (t < 0.0) t = 0.0;
            if (t > 1.0) t = 1.0;
            real x = t * dx + x0;
            real y = t * dy + y0;
            return ((x - px) * (x - px) + (y - py) * (y - py));
        }
        static inline real get_distance2(const vector2& a, const vector2& b, const vector2& p)
        {
            return get_distance2(a[0], a[1], b[0], b[1], p[0], p[1]);
        }

        static bool is_near_edge(const edge& e, const vector2& p, real radius)
        {
            real d2 = get_distance2(*e.p0, *e.p1, p);
            return d2 <= (radius * radius);
        }

        class bvh_node
        {
        public:
            virtual ~bvh_node() {}
            virtual const vector2& min() const = 0;
            virtual const vector2& max() const = 0;
            virtual vector2 cnt() const = 0;
            virtual bool get(std::vector<PCEDGE>& edges, const vector2& p, real radius) const = 0;

        protected:
            bool test(const vector2& p, real radius) const
            {
                vector2 cmin = vector2(p[0] - radius, p[1] - radius);
                vector2 cmax = vector2(p[0] + radius, p[1] + radius);
                const vector2& min = this->min();
                const vector2& max = this->max();
                if (min[0] <= cmax[0] && cmin[0] <= max[0])
                {
                    if (min[1] <= cmax[1] && cmin[1] <= max[1])
                    {
                        return true;
                    }
                }
                return false;
            }
        };

        class bvh_node_leaf : public bvh_node
        {
        public:
            bvh_node_leaf(PCEDGE_ITERATOR a, PCEDGE_ITERATOR b) : es_(a, b)
            {
                static const real EPSILON = values::epsilon() * 1024;

                get_minmax(min_, max_, a, b);
                min_ -= vector2(EPSILON, EPSILON);
                max_ += vector2(EPSILON, EPSILON);
            }
            const vector2& min() const
            {
                return min_;
            }
            const vector2& max() const
            {
                return max_;
            }
            vector2 cnt() const
            {
                return (min_ + max_) * 0.5;
            }
            bool get(std::vector<PCEDGE>& edges, const vector2& p, real radius) const
            {
                if (!test(p, radius)) return false;
                bool bRet = false;
                size_t sz = es_.size();
                for (size_t i = 0; i < sz; i++)
                {
                    if (is_near_edge(*es_[i], p, radius))
                    {
                        edges.push_back(es_[i]);
                        bRet = true;
                    }
                }
                return bRet;
            }

        private:
            std::vector<PCEDGE> es_;
            vector2 min_;
            vector2 max_;
        };

        class bvh_node_branch : public bvh_node
        {
        public:
            bvh_node_branch(PCEDGE_ITERATOR a, PCEDGE_ITERATOR b)
            {
                static const real EPSILON = values::epsilon() * 1024;

                vector2 min;
                vector2 max;
                get_minmax(min, max, a, b);

                min -= vector2(EPSILON, EPSILON);
                max += vector2(EPSILON, EPSILON);

                vector2 wid = max - min;
                int plane = (wid[0] > wid[1]) ? 0 : 1;
                std::sort(a, b, edge_sorter(plane));

                size_t sz = b - a;
                size_t msz = sz >> 1;

                size_t lsz = msz;
                size_t rsz = sz - msz;

                std::unique_ptr<bvh_node> lap;
                if (lsz)
                {
                    if (lsz <= 4)
                    {
                        lap.reset(new bvh_node_leaf(a, a + lsz));
                    }
                    else
                    {
                        lap.reset(new bvh_node_branch(a, a + lsz));
                    }
                }
                std::unique_ptr<bvh_node> rap;
                if (rsz)
                {
                    if (rsz <= 4)
                    {
                        rap.reset(new bvh_node_leaf(a + lsz, b));
                    }
                    else
                    {
                        rap.reset(new bvh_node_branch(a + lsz, b));
                    }
                }
                nodes_[0] = lap.release();
                nodes_[1] = rap.release();
                min_ = min;
                max_ = max;
            }
            ~bvh_node_branch()
            {
                if (nodes_[0]) delete nodes_[0];
                if (nodes_[1]) delete nodes_[1];
            }

            const vector2& min() const { return min_; }
            const vector2& max() const { return max_; }
            vector2 cnt() const { return (min_ + max_) * 0.5; }
            bool get(std::vector<PCEDGE>& edges, const vector2& p, real radius) const
            {
                if (!test(p, radius)) return false;
                bool bRet = false;

                if (nodes_[0])
                {
                    bRet |= nodes_[0]->get(edges, p, radius);
                }
                if (nodes_[1])
                {
                    bRet |= nodes_[1]->get(edges, p, radius);
                }

                return bRet;
            }

        private:
            vector2 min_;
            vector2 max_;
            bvh_node* nodes_[2];
        };

        static inline real edge_alpha(real x0, real y0, real x1, real y1, real px, real py)
        {
            real dx = x1 - x0;
            real dy = y1 - y0;
            real a = dx * dx + dy * dy;

            assert(a > values::epsilon());

            real b = dx * (x0 - px) + dy * (y0 - py);
            real t = -(b / a);
            return t;
        }

        static real edge_alpha(const edge& e, const vector2& p)
        {
            vector2 p0 = *(e.p0);
            vector2 p1 = *(e.p1);
            return edge_alpha(p0[0], p0[1], p1[0], p1[1], p[0], p[1]);
        }

        static bool move_points_onloop_internal(std::vector<vector2>& loop, std::vector<vector2>& points, const std::vector<vector2>& olp, const std::vector<vector2>& op, real radius)
        {
            static const real EPSILON = values::epsilon() * 1024;

            size_t esz = olp.size();
            std::vector<edge> edges(esz);

            for (size_t i = 0; i < esz - 1; i++)
            {
                edges[i].p0 = &olp[i];
                edges[i].p1 = &olp[i + 1];
            }
            edges[esz - 1].p0 = &olp[esz - 1];
            edges[esz - 1].p1 = &olp[0];
            std::vector<const edge*> pedges(esz);
            for (size_t i = 0; i < esz; i++)
            {
                pedges[i] = &edges[i];
            }
            bvh_node_branch tree(pedges.begin(), pedges.end());
            size_t psz = op.size();

            std::vector<bool> no_out(psz);
            std::map<PCEDGE, std::vector<real> > cross_point;
            std::vector<PCEDGE> outedges;
            for (size_t i = 0; i < psz; i++)
            {
                if (tree.get(outedges, op[i], radius))
                {
                    no_out[i] = true;
                    size_t csz = outedges.size();
                    for (size_t j = 0; j < csz; j++)
                    {
                        real t = edge_alpha(*outedges[j], op[i]);
                        if (EPSILON < t && t < 1 - EPSILON)
                        {
                            cross_point[outedges[j]].push_back(t);
                        }
                    }
                }
                outedges.clear();
            }

            //std::vector<vector2> ncpts;
            typedef std::map<PCEDGE, std::vector<real> >::iterator miterator;
            for (miterator it = cross_point.begin(); it != cross_point.end(); it++)
            {
                std::vector<real>& v = it->second;
                //const edge* e = it->first;
                if (!v.empty())
                {
                    std::sort(v.begin(), v.end());
                    v.erase(std::unique(v.begin(), v.end()), v.end());
                    //size_t csz = v.size();
                    //for(size_t j = 0;j<csz;j++){
                    //	ncpts.push_back(e->evaluate(v[j]));
                    //}
                }
            }
#if 0
		size_t nsz = ncpts.size();	
		real r2 = radius*radius;
		for(size_t i = 0;i<psz;i++){
			if(no_out[i])continue;
			for(size_t j = 0;j<nsz;j++){
				real d2 = (ncpts[j]-op[i]).sqr_length();
				if(d2<=r2){
					no_out[i] =  true;
					break;
				}				
			}
		}
#endif

            std::vector<vector2> pt_tmp;
            pt_tmp.reserve(psz);
            for (size_t i = 0; i < psz; i++)
            {
                if (!no_out[i]) pt_tmp.push_back(op[i]);
            }
            std::vector<vector2> lp_tmp;
            lp_tmp.reserve(esz);
            for (size_t i = 0; i < esz; i++)
            {
                lp_tmp.push_back(*(edges[i].p0));

                miterator it = cross_point.find(&(edges[i]));
                if (it != cross_point.end())
                { //if find map, evaluate points and push_back points
                    std::vector<real>& v = it->second;
                    size_t csz = v.size();
                    for (size_t j = 0; j < csz; j++)
                    {
                        lp_tmp.push_back(edges[i].evaluate(v[j]));
                    }
                }
            }

            loop.swap(lp_tmp);
            points.swap(pt_tmp);

            return true;
        }
    }

    bool move_points_onloop(std::vector<vector2>& loop, std::vector<vector2>& points, const std::vector<vector2>& olp, const std::vector<vector2>& op, real radius)
    {
        if (op.empty())
        {
            loop = olp;
            points.clear();
            return true;
        }
        if (olp.size() < 3) return false;
        if (olp.back() == olp.front())
        {
            std::vector<vector2> tlp(olp);
            tlp.pop_back();
            if (tlp.size() < 3) return false;
            return move_points_onloop_internal(loop, points, tlp, op, radius);
        }
        else
        {
            return move_points_onloop_internal(loop, points, olp, op, radius);
        }
    }
}
