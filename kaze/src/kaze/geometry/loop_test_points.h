#ifndef KAZE_LOOP_TEST_POINTS_H
#define KAZE_LOOP_TEST_POINTS_H

#include "../core/types.h"
#include <vector>

namespace kaze
{

    bool loop_inner_points(std::vector<vector2>& out, const std::vector<vector2>& loop, const std::vector<vector2>& points); //remove outer points
    bool loop_outer_points(std::vector<vector2>& out, const std::vector<vector2>& loop, const std::vector<vector2>& points); //remove inner points

    bool test_loop_inner_points(const std::vector<vector2>& loop, const std::vector<vector2>& points);
    bool test_loop_outer_points(const std::vector<vector2>& loop, const std::vector<vector2>& points);
}

#endif