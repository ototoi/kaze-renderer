#ifndef KAZE_TRIANGULATE_POLYGON_DELAUNAY_H
#define KAZE_TRIANGULATE_POLYGON_DELAUNAY_H

#include "../core/types.h"
#include <vector>

namespace kaze
{

    bool triangulate_polygon_delaunay(
        std::vector<vector2>& out,
        const std::vector<vector2>& outer_loop);

    bool triangulate_polygon_delaunay(
        std::vector<vector2>& out,
        const std::vector<vector2>& outer_loop,
        const std::vector<vector2>& pins);

    bool triangulate_polygon_delaunay(
        std::vector<vector2>& out,
        const std::vector<vector2>& outer_loop,
        const std::vector<std::vector<vector2> >& inner_loops,
        const std::vector<vector2>& pins);
}

#endif