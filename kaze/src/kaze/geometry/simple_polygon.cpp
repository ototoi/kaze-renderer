#include "simple_polygon.h"

#include <vector>
#include <cassert>
#include <algorithm>
#include <functional>
#include <iterator>

#include <cmath>

#include <iostream>
#include <queue>
#include <map>
#include <set>

#include "values.h"
#include "unique_points.h"

#include "intersect_lines.h"

namespace kaze
{
    namespace
    {
        enum
        {
            ORIENTATION_CW = 0,
            ORIENTATION_CCW,
            ORIENTATION_UNKNOWN
        };

        static double det(double a, double b, double c, double d)
        {
            return a * d - b * c;
        }

        static int calc_determinant(const vector2& a, const vector2& b, const vector2& c)
        {
            vector2 ab = b - a;
            vector2 ac = c - a;
            double Determ = det(ab[0], ac[0], ab[1], ac[1]); //ad-bc

            if (Determ > 0)
            {
                return ORIENTATION_CCW;
            }
            else if (Determ < 0)
            {
                return ORIENTATION_CW;
            }
            else
            {
                return ORIENTATION_UNKNOWN;
            }
        }

        //calc angle B
        static double calc_angle(const vector2& a, const vector2& b, const vector2& c)
        {
            int nDet = calc_determinant(a, b, c);
            if (nDet == ORIENTATION_UNKNOWN) return 0;

            vector2 ba = (a - b);
            ba = normalize(ba);
            vector2 bc = (c - b);
            bc = normalize(bc);

            double cc = -dot(ba, bc);
            double cos_m = acos(cc);
            if (nDet == ORIENTATION_CCW)
            {
                return cos_m;
            }
            else
            {
                return -cos_m;
            }
        }

        static double tri_area(const vector2& a, const vector2& b, const vector2& c)
        {
            return (a[0] - c[0]) * (b[1] - c[1]) - (a[1] - c[1]) * (b[0] - c[0]);
        }

        struct tedge
        {
            vector2 a;
            vector2 b;
        };
        struct tedge_sorter
        {
            bool operator()(const tedge& a, const tedge& b) const
            {
                vector2 aa = (a.a[0] < a.b[0]) ? a.a : a.b;
                vector2 bb = (b.a[0] < b.b[0]) ? b.a : b.b;
                if (aa[0] == bb[0])
                {
                    real da = fabs((a.a[0] - a.b[0]) / (a.a[1] - a.b[1]));
                    real db = fabs((b.a[0] - b.b[0]) / (b.a[1] - b.b[1]));
                    return da < db;
                }
                else
                {
                    return aa[0] < bb[0];
                }
            }
        };

        static bool is_ccw(const std::vector<vector2>& v)
        {
#if 1
            static const real EPS = values::epsilon();
            size_t sz = v.size();
            if (v.front() == v.back()) sz--;
            std::vector<tedge> edges;
            edges.reserve(sz);
            for (size_t i = 0; i < sz; i++)
            {
                vector2 a = v[i];
                vector2 b = (i != sz - 1) ? v[i + 1] : v[0];
                if (fabs(a[1] - b[1]) < EPS) continue;
                tedge t;
                t.a = a;
                t.b = b;
                edges.push_back(t);
            }
            std::sort(edges.begin(), edges.end(), tedge_sorter());
            for (size_t i = 0; i < edges.size(); i++)
            {
                //if(edges[i].a[1] == edges[i].b[1])continue;
                if (edges[i].a[1] > edges[i].b[1])
                    return true;
                else
                    return false;
            }
            return false;
#else
            double area = 0;
            size_t sz = v.size();
            if (v.front() == v.back()) sz--;
            for (size_t i = 0; i < sz; i++)
            {
                vector2 a = v[(i == 0) ? sz - 1 : i - 1];
                vector2 b = v[i];

                area += tri_area(a, b, vector2(0, 0));
            }
            if (area > 0)
                return true;
            else
                return false;
#endif
        }

        static void make_ccw(std::vector<vector2>& v)
        {
            if (!is_ccw(v))
            {
                std::reverse(v.begin(), v.end());
            }
        }

        static void make_cw(std::vector<vector2>& v)
        {
            if (is_ccw(v))
            {
                std::reverse(v.begin(), v.end());
            }
        }

        //for inners
        static bool check_cw(const std::vector<std::vector<vector2> >& loops)
        {
            for (size_t i = 0; i < loops.size(); i++)
            {
                if (is_ccw(loops[i])) return false;
            }
            return true;
        }

        //for outers
        static bool check_ccw(const std::vector<std::vector<vector2> >& loops)
        {
            for (size_t i = 0; i < loops.size(); i++)
            {
                if (!is_ccw(loops[i])) return false;
            }
            return true;
        }

        enum EDGE_TYPE
        {
            EDGE_CROSS0 = 1,
            EDGE_CROSS1 = 2,
            EDGE_KILLED = 4
        };

        struct edge
        {
            const vector2* p0;
            const vector2* p1;
            int type;
        };

        struct edge_event
        {
            edge_event(edge* e_) : e(e_) {}
            edge* e;
        };

        struct edge_event_sorter
        {
            static double calc_angle(const vector2& p0, const vector2& p1)
            {
                double x = fabs(p1[0] - p0[0]);
                double y = fabs(p1[1] - p0[1]);
                return std::atan2(x, y); //
            }
            static double calc_dx(const vector2& p0, const vector2& p1)
            {
                vector2 dd = normalize(p1 - p0);
                return dd[0];
            }
            static bool is_equal(const vector2* p0, const vector2* p1)
            {
                if (p0 == p1) return true;
                if (*p0 == *p1) return true;
                return false;
            }
            bool operator()(const edge_event& a, const edge_event& b) const
            {
                const vector2* ap0 = a.e->p0;
                const vector2* ap1 = a.e->p1;
                if ((*ap0)[0] > (*ap1)[0]) std::swap(ap0, ap1);
                const vector2* bp0 = b.e->p0;
                const vector2* bp1 = b.e->p1;
                if ((*bp0)[0] > (*bp1)[0]) std::swap(bp0, bp1);

                assert((*ap0)[0] <= (*ap1)[0]);
                assert((*bp0)[0] <= (*bp1)[0]);

                if (!is_equal(ap0, bp0))
                {
                    if ((*ap0)[0] != (*bp0)[0])
                    {
                        return ((*ap0)[0] < (*bp0)[0]);
                    }
                    else
                    {
                        return ((*ap0)[1] > (*bp0)[1]);
                    }
                }
                else
                {
                    //calc tangent
                    double aa = calc_angle(*ap0, *ap1);
                    double bb = calc_angle(*bp0, *bp1);
                    return aa < bb;
                }
            }
        };

        static edge* find_inner_next(const std::set<edge*>& S, const edge* e, const vector2* p)
        {
            edge* eRet = NULL;
            double angle = -values::far();
            const vector2* a = e->p0;
            const vector2* b = e->p1;
            if (a == p) std::swap(a, b);

            for (std::set<edge*>::const_iterator i = S.begin(); i != S.end(); i++)
            {
                edge* l = *i;
                const vector2* c = (l->p0 == p) ? l->p1 : l->p0;
                if (c != a)
                {
                    double ang = calc_angle(*a, *b, *c);
                    if (ang >= angle)
                    {
                        angle = ang;
                        eRet = l;
                    }
                }
            }
            return eRet;
        }

        static edge* find_outer_next(const std::set<edge*>& S, const edge* e, const vector2* p)
        {
            edge* eRet = NULL;
            double angle = +values::far();
            const vector2* a = e->p0;
            const vector2* b = e->p1;
            if (a == p) std::swap(a, b);

            for (std::set<edge*>::const_iterator i = S.begin(); i != S.end(); i++)
            {
                edge* l = *i;
                const vector2* c = (l->p0 == p) ? l->p1 : l->p0;
                if (c != a)
                {
                    double ang = calc_angle(*a, *b, *c);
                    if (ang <= angle)
                    {
                        angle = ang;
                        eRet = l;
                    }
                }
            }
            return eRet;
        }

        typedef std::map<const vector2*, std::set<edge*> > CrossMap;

        static bool create_inner_loop(std::vector<edge*>& el, edge* e, CrossMap& cross_map, const std::vector<edge>& edges)
        {
            size_t esz = edges.size();
            edge* eFirst = e;
            int dd = 1;
            if ((*e->p0)[1] < (*e->p1)[1])
            {
                dd = -1;
            }

            do
            {
                el.push_back(e);
                e->type |= EDGE_KILLED;

                const vector2* pp = NULL;
                if (dd > 0)
                {
                    pp = e->p1;
                }
                else
                {
                    pp = e->p0;
                }
                CrossMap::iterator it;
                if ((e->type & (EDGE_CROSS0 | EDGE_CROSS1)) && ((it = cross_map.find(pp)) != cross_map.end()))
                {
                    std::set<edge*>& S = it->second;
                    S.erase(e);
                    edge* en = find_inner_next(S, e, pp);
                    if (!en)
                    {
                        break;
                    }
                    S.erase(en);
                    if (en->p0 == pp)
                    {
                        dd = 1;
                    }
                    else
                    {
                        dd = -1;
                    }
                    e = en;
                }
                else
                {
                    if (dd > 0)
                    {
                        if (e == &edges[esz - 1])
                            e = (edge*)&edges[0];
                        else
                            e++;
                    }
                    else
                    {
                        if (e == &edges[0])
                            e = (edge*)&edges[esz - 1];
                        else
                            e--;
                    }
                }
            } while (e != eFirst);
            return true;
        }

        static bool create_outer_loop(std::vector<edge*>& el, edge* e, CrossMap& cross_map, const std::vector<edge>& edges)
        {
            size_t esz = edges.size();
            edge* eFirst = e;
            int dd = 1;
            if ((*e->p0)[1] < (*e->p1)[1])
            {
                dd = -1;
            }

            do
            {
                el.push_back(e);
                e->type |= EDGE_KILLED;

                const vector2* pp = NULL;
                if (dd > 0)
                {
                    pp = e->p1;
                }
                else
                {
                    pp = e->p0;
                }
                CrossMap::iterator it;
                if ((e->type & (EDGE_CROSS0 | EDGE_CROSS1)) && ((it = cross_map.find(pp)) != cross_map.end()))
                {
                    std::set<edge*>& S = it->second;
                    S.erase(e);
                    edge* en = find_outer_next(S, e, pp);
                    if (!en)
                    {
                        break;
                    }
                    S.erase(en);
                    if (en->p0 == pp)
                    {
                        dd = 1;
                    }
                    else
                    {
                        dd = -1;
                    }
                    e = en;
                }
                else
                {
                    if (dd > 0)
                    {
                        if (e == &edges[esz - 1])
                            e = (edge*)&edges[0];
                        else
                            e++;
                    }
                    else
                    {
                        if (e == &edges[0])
                            e = (edge*)&edges[esz - 1];
                        else
                            e--;
                    }
                }
            } while (e != eFirst);
            return true;
        }

        static bool separate_vertices(std::vector<vector2>& vertices, std::vector<size_t>& indices, const std::vector<vector2>& loop)
        {
            static const real EPSILON = values::epsilon() * 1024 * 8;

            std::vector<vector2> src(loop);
            if (src.size() < 3) return false;
            if (src.front() == src.back()) src.pop_back();
            if (src.size() < 3) return false;

            //-----------------------------------------------------------------------------
            //check intersection point
            intersect_loop(src); //

            //-----------------------------------------------------------------------------
            //unique
            unique_lines(vertices, indices, src, EPSILON);
            indices.erase(std::unique(indices.begin(), indices.end()), indices.end());

            return true;
        }

        static bool separate_vertices(std::vector<vector2>& vertices, std::vector<std::vector<size_t> >& indices, const std::vector<std::vector<vector2> >& loops)
        {
            static const real EPSILON = values::epsilon() * 1024 * 8;

            std::vector<std::vector<vector2> > tmp;

            for (size_t i = 0; i < loops.size(); i++)
            {
                std::vector<vector2> src(loops[i]);
                if (src.size() < 3) continue;
                if (src.front() == src.back()) src.pop_back();
                if (src.size() < 3) continue;
                tmp.push_back(src);
            }
            if (tmp.empty()) return false;

            //-----------------------------------------------------------------------------
            //check intersection point
            intersect_loop(tmp); //

            //-----------------------------------------------------------------------------
            //unique
            unique_lines(vertices, indices, tmp, EPSILON);
            for (size_t i = 0; i < indices[i].size(); i++)
            {
                indices[i].erase(std::unique(indices[i].begin(), indices[i].end()), indices[i].end());
            }

            return true;
        }

        static bool get_edge_events(std::vector<edge_event>& events, const std::vector<edge>& edges)
        {
            static const real EPSILON = values::epsilon() * 1024;
            size_t esz = edges.size();
            events.reserve(esz);
            for (size_t i = 0; i < esz; i++)
            {
                const vector2& p0 = *(edges[i].p0);
                const vector2& p1 = *(edges[i].p1);

                if (fabs(p0[1] - p1[1]) > EPSILON)
                {
                    events.push_back(edge_event((edge*)&edges[i]));
                }
            }
            std::sort(events.begin(), events.end(), edge_event_sorter());
            return true;
        }

        static bool get_edges(CrossMap& cross_map, std::vector<edge>& edges, const std::vector<vector2>& vertices, const std::vector<std::vector<size_t> >& indices)
        {
            size_t totals = 0;
            size_t isz = indices.size();
            std::vector<int> counts(vertices.size());
            for (size_t i = 0; i < isz; i++)
            {
                size_t jsz = indices[i].size();
                for (size_t j = 0; j < jsz; j++)
                {
                    counts[indices[i][j]]++;
                    totals++;
                }
            }

            edges.resize(totals);
            size_t k = 0;
            for (size_t i = 0; i < isz; i++)
            {
                size_t jsz = indices[i].size();
                for (size_t j = 0; j < jsz; j++)
                {
                    size_t i0 = indices[i][j];
                    size_t i1 = indices[i][(j == (jsz - 1)) ? 0 : j + 1];
                    edges[k].p0 = &vertices[i0];
                    edges[k].p1 = &vertices[i1];
                    edges[k].type = 0;
                    if (counts[i0] >= 2)
                    {
                        cross_map[&vertices[i0]].insert(&edges[k]);
                        edges[k].type |= EDGE_CROSS0;
                    }
                    if (counts[i1] >= 2)
                    {
                        cross_map[&vertices[i1]].insert(&edges[k]);
                        edges[k].type |= EDGE_CROSS1;
                    }
                    k++;
                }
            }

            return true;
        }

        static bool get_edges(CrossMap& cross_map, std::vector<edge>& edges, const std::vector<vector2>& vertices, const std::vector<size_t>& indices)
        {
            std::vector<std::vector<size_t> > tmp;
            tmp.push_back(indices);

            return get_edges(cross_map, edges, vertices, tmp);
        }

        static bool remap_edges(CrossMap& cross_map, std::vector<edge>& edges, const std::vector<edge*>& eloop)
        {
            size_t sz = eloop.size();
            if (sz < 2) return false;

            CrossMap tmp;
            edges.resize(sz);
            for (size_t i = 0; i < sz; i++)
            {
                edges[i] = *eloop[i];
                edges[i].type = 0;
            }

            if (!(edges[0].p1 == edges[1].p0 || edges[0].p1 == edges[1].p1))
            {
                std::swap(edges[0].p0, edges[0].p1);
            }
            tmp[edges[0].p0].insert(&edges[0]);
            tmp[edges[0].p1].insert(&edges[0]);

            const vector2* p0 = edges[0].p0;
            const vector2* p1 = edges[0].p1;

            for (size_t i = 1; i < sz; i++)
            {
                if (p1 != edges[i].p0)
                {
                    std::swap(edges[i].p0, edges[i].p1);
                }
                tmp[edges[i].p0].insert(&edges[i]);
                tmp[edges[i].p1].insert(&edges[i]);
                p1 = edges[i].p1;
            }

            for (CrossMap::iterator it = tmp.begin(); it != tmp.end(); it++)
            {
                std::set<edge*>& S = it->second;
                if (S.size() > 2)
                {
                    for (std::set<edge*>::iterator j = S.begin(); j != S.end(); j++)
                    {
                        (*j)->type |= (EDGE_CROSS0 | EDGE_CROSS1);
                    }
                    cross_map[it->first].swap(S);
                }
            }

            return true;
        }

        static bool convert_loop(std::vector<vector2>& loop, const std::vector<edge*>& eloop)
        {
            size_t sz = eloop.size();
            if (sz < 2) return false;
            std::vector<vector2> ep(sz);
            const vector2* p0 = eloop[0]->p0;
            const vector2* p1 = eloop[0]->p1;
            //
            if (!(p1 == eloop[1]->p0 || p1 == eloop[1]->p1))
            {
                std::swap(p0, p1);
            }

            ep[0] = *p0;
            for (size_t i = 1; i < sz; i++)
            {
                if (eloop[i]->p0 == p1)
                {
                    p0 = eloop[i]->p0;
                    p1 = eloop[i]->p1;
                }
                else
                {
                    p0 = eloop[i]->p1;
                    p1 = eloop[i]->p0;
                }
                ep[i] = *p0;
            }
            //make_ccw(ep);
            loop.swap(ep);
            return true;
        }

        static bool convert_loops(std::vector<std::vector<vector2> >& loops, const std::vector<std::vector<edge*> >& eloops)
        {
            size_t elsz = eloops.size();
            for (size_t j = 0; j < elsz; j++)
            {
                std::vector<vector2> tmp;
                if (convert_loop(tmp, eloops[j]))
                {
                    loops.push_back(tmp);
                }
            }
            return true;
        }

        static bool revert_loops(std::vector<std::vector<vector2> >& loops)
        {
            size_t sz = loops.size();
            for (size_t i = 0; i < sz; i++)
            {
                std::reverse(loops[i].begin(), loops[i].end());
            }
            return true;
        }

        static bool get_evenodd_loops(std::vector<std::vector<edge*> >& eloops, CrossMap& cross_map, const std::vector<edge>& edges, const std::vector<edge_event>& events)
        {
            size_t esz = edges.size();
            std::vector<edge*> el;
            //-----------------------------------------------------------------------------
            size_t evsz = events.size();
            size_t count = 0;
            for (size_t i = 0; i < evsz; i++)
            {
                edge* e = (edge*)(events[i].e);
                if (e->type & EDGE_KILLED) continue;
                el.clear();
                if (create_inner_loop(el, e, cross_map, edges))
                {
                    eloops.push_back(el);
                }
            }
            return true;
        }

        static bool get_outline_loops(std::vector<std::vector<edge*> >& eloops, CrossMap& cross_map, const std::vector<edge>& edges, const std::vector<edge_event>& events)
        {
            size_t esz = edges.size();

            std::vector<edge*> el;
            //-----------------------------------------------------------------------------
            size_t evsz = events.size();
            size_t count = 0;
            for (size_t i = 0; i < evsz; i++)
            {
                edge* e = (edge*)(events[i].e);
                if (e->type & EDGE_KILLED) continue;
                el.clear();
                if (create_outer_loop(el, e, cross_map, edges))
                {
                    eloops.push_back(el);
                }
                break;
            }
            return true;
        }

        static size_t get_killed_edge(const std::vector<edge>& edges)
        {
            size_t sz = edges.size();
            size_t count = 0;
            for (size_t i = 0; i < sz; i++)
            {
                if (edges[i].type & EDGE_KILLED) count++;
            }
            return count;
        }

        enum EDGE_EVENT
        {
            EDGE_EVENT_START = 1,
            EDGE_EVENT_END = 2
        };

        struct edge_count_event
        {
            edge_count_event(int type_, edge* e_) : type(type_), e(e_) {}
            const vector2* top() const { return ((*(e->p0))[1] >= (*(e->p1))[1]) ? e->p0 : e->p1; }
            const vector2* bottom() const { return ((*(e->p0))[1] < (*(e->p1))[1]) ? e->p0 : e->p1; }
            const vector2* pos() const
            {
                if (type == EDGE_EVENT_START)
                {
                    return top();
                }
                else
                {
                    return bottom();
                }
            }
            int type;
            edge* e;
        };

        static double get_intersection(const vector2& a, const vector2& b, double v)
        {
            double k = (v - a[1]) / (b[1] - a[1]);
            //assert(0<=k&&k<=1);
            double u = (1 - k) * a[0] + k * b[0];

            return u;
        }

        //for count in/out
        struct edge_count_sorter
        {
            bool operator()(const edge_count_event& a, const edge_count_event& b) const
            {
                const vector2* pap = a.pos();
                const vector2* pbp = b.pos();

                assert((*a.top())[1] >= (*a.bottom())[1]);
                assert((*b.top())[1] >= (*b.bottom())[1]);

                vector2 ap = *pap;
                vector2 bp = *pbp;
                if (ap[1] == bp[1])
                {
                    if (ap[0] == bp[0])
                    {
                        if (a.type == b.type)
                        {
                            vector2 pp = ap;
                            assert(pap == pbp);
                            if (a.type == EDGE_EVENT_START)
                            {
                                double ax = get_intersection(*(a.e->p0), *(a.e->p1), pp[1] - 0.01);
                                double bx = get_intersection(*(b.e->p0), *(b.e->p1), pp[1] - 0.01);
                                //assert(ax != bx);
                                if (ax == bx)
                                {
                                    return a.e < b.e;
                                }
                                else
                                {
                                    return ax < bx;
                                }
                            }
                            else
                            { //EDGE_EVENT_END
                                double ax = get_intersection(*(a.e->p0), *(a.e->p1), pp[1] + 0.01);
                                double bx = get_intersection(*(b.e->p0), *(b.e->p1), pp[1] + 0.01);
                                //assert(ax != bx);
                                if (ax == bx)
                                {
                                    return a.e < b.e;
                                }
                                else
                                {
                                    return ax < bx;
                                }
                            }
                        }
                        else
                        {
                            if (a.type > b.type)
                            { //end ->start
                                assert(a.type == EDGE_EVENT_END);
                                assert(b.type == EDGE_EVENT_START);
                                assert((*a.top())[1] >= (*b.top())[1]);
                                assert((*a.top())[1] >= (*b.bottom())[1]);
                            }
                            else
                            {
                                assert(a.type == EDGE_EVENT_START);
                                assert(b.type == EDGE_EVENT_END);
                                assert((*b.top())[1] >= (*a.top())[1]);
                                assert((*b.top())[1] >= (*a.bottom())[1]);
                            }
                            return a.type > b.type; //END<START
                        }
                    }
                    else
                    {
                        return ap[0] < bp[0];
                    }
                }
                else
                {
                    return ap[1] > bp[1]; //top
                }
            }
        };

        struct edge_pair
        {
            edge_pair(real x0_, real x1_, edge* e_) : x0(x0_), x1(x1_), e(e_) {}

            real x0;
            real x1;
            edge* e;
        };

        struct edge_pair_sorter
        {
            bool operator()(const edge_pair& a, const edge_pair& b) const
            {
                const vector2* ap0 = a.e->p0;
                const vector2* ap1 = a.e->p1;
                const vector2* bp0 = b.e->p0;
                const vector2* bp1 = b.e->p1;
                if ((*ap0)[1] < (*ap1)[1]) std::swap(ap0, ap1);
                if ((*bp0)[1] < (*bp1)[1]) std::swap(bp0, bp1);

                if (ap0 == bp0)
                {
                    double xa = get_intersection(*ap0, *ap1, (*ap0)[1] - 0.01);
                    double xb = get_intersection(*bp0, *bp1, (*bp0)[1] - 0.01);
                    return xa < xb;
                }
                else
                {
                    if (a.x0 != b.x0)
                    {
                        return a.x0 < b.x0;
                    }
                    else
                    {
                        return a.x1 < b.x1;
                    }
                }
            }
        };

        static bool get_nonezero_loops(std::vector<std::vector<edge*> >& eloops, CrossMap& cross_map, const std::vector<edge>& edges)
        {
            static const real EPSILON = std::numeric_limits<real>::epsilon();
            size_t sz = edges.size();
            std::vector<edge*> el;

            std::vector<edge_count_event> events;
            events.reserve(sz * 2);
            for (size_t i = 0; i < sz; i++)
            {
                edge* e = (edge*)&edges[i];
                if (fabs((*(e->p0))[1] - (*(e->p1))[1]) > EPSILON)
                {
                    events.push_back(edge_count_event(EDGE_EVENT_START, e));
                    events.push_back(edge_count_event(EDGE_EVENT_END, e));
                }
            }
            std::sort(events.begin(), events.end(), edge_count_sorter());
            size_t evsz = events.size();
            typedef std::set<edge*> edge_set;
            edge_set S;
            for (size_t i = 0; i < evsz; i++)
            {
                edge_count_event ev = events[i];
                switch (ev.type)
                {
                case EDGE_EVENT_START:
                {
                    edge* e = ev.e;
                    vector2 pp = *(ev.pos());
                    S.insert(e);
                    if (!(e->type & EDGE_KILLED))
                    {
                        std::vector<edge_pair> pairs;
                        for (edge_set::iterator it = S.begin(); it != S.end(); it++)
                        {
                            edge* ee = *it;
                            real x0 = get_intersection(*ee->p0, *ee->p1, pp[1]);
                            real x1 = get_intersection(*ee->p0, *ee->p1, pp[1] - 0.01);
                            pairs.push_back(edge_pair(x0, x1, ee));
                        }
                        std::sort(pairs.begin(), pairs.end(), edge_pair_sorter());
                        int count = 0;
                        int dd = 0;
                        {
                            edge* ee = pairs[0].e;
                            real y0 = (*ee->p0)[1];
                            real y1 = (*ee->p1)[1];
                            dd = (y0 > y1) ? 1 : -1;
                        }
                        for (size_t j = 0; j < pairs.size(); j++)
                        {
                            edge* ee = pairs[j].e;
                            real y0 = (*ee->p0)[1];
                            real y1 = (*ee->p1)[1];
                            int d = (y0 > y1) ? 1 : -1;
                            if (dd == d)
                            {
                                count++;
                            }
                            else
                            {
                                count--;
                            }

                            if (ee == e)
                            {
                                if (dd == d)
                                {
                                    if (count != 0 && count != 1)
                                    {
                                        el.clear();
                                        if (create_inner_loop(el, e, cross_map, edges))
                                        {
                                            eloops.push_back(el);
                                        }
                                    }
                                }
                                else
                                {
                                    if (count != 0 && count != -1)
                                    {
                                        el.clear();
                                        if (create_inner_loop(el, e, cross_map, edges))
                                        {
                                            eloops.push_back(el);
                                        }
                                    }
                                }

                                break;
                            }
                        }
                    }
                }
                break;
                case EDGE_EVENT_END:
                {
                    S.erase(ev.e);
                }
                break;
                default:
                    assert(0);
                }
            }

            return true;
        }

        static bool get_nonezero_loops(
            std::vector<std::vector<edge*> >& eouters, std::vector<std::vector<edge*> >& einners,
            CrossMap& cross_map,
            const std::vector<edge>& edges, const std::vector<edge_event>& events)
        {
            size_t esz = edges.size();

            size_t osz = 0;
            std::vector<std::vector<edge*> > enonezeros; //dummy
            if (!get_nonezero_loops(enonezeros, cross_map, edges)) return false;
            //einners.swap(enonezeros);
            if (!get_outline_loops(eouters, cross_map, edges, events)) return false; //out line

            std::vector<edge*> el;
            //-----------------------------------------------------------------------------
            size_t evsz = events.size();
            size_t count = 0;
            for (size_t i = 0; i < evsz; i++)
            {
                edge* e = (edge*)(events[i].e);
                if (e->type & EDGE_KILLED) continue;
                el.clear();
                if (create_inner_loop(el, e, cross_map, edges))
                {
                    einners.push_back(el);
                }
            }

            return true;
        }

        static bool split_evenodd(std::vector<std::vector<vector2> >& loops, const std::vector<vector2>& loop)
        {
            std::vector<vector2> vertices;
            std::vector<size_t> indices;
            if (!separate_vertices(vertices, indices, loop)) return false;
            //-----------------------------------------------------------------------------
            CrossMap cross_map;
            std::vector<edge> edges;
            if (!get_edges(cross_map, edges, vertices, indices)) return false;
            //-----------------------------------------------------------------------------
            std::vector<edge_event> events;
            if (!get_edge_events(events, edges)) return false;
            //-----------------------------------------------------------------------------
            std::vector<std::vector<edge*> > eloops;
            if (!get_evenodd_loops(eloops, cross_map, edges, events)) return false;
            //-----------------------------------------------------------------------------
            if (!convert_loops(loops, eloops)) return false;

            return true;
        }

        static bool split_nonezero(
            std::vector<std::vector<vector2> >& outers,
            std::vector<std::vector<vector2> >& inners,
            const std::vector<vector2>& loop)
        {
            std::vector<vector2> vertices;
            std::vector<size_t> indices;
            if (!separate_vertices(vertices, indices, loop)) return false; //check CCW
            //-----------------------------------------------------------------------------
            CrossMap cross_map;
            std::vector<edge> edges;
            if (!get_edges(cross_map, edges, vertices, indices)) return false;
            //-----------------------------------------------------------------------------
            std::vector<edge_event> events;
            if (!get_edge_events(events, edges)) return false;
            //-----------------------------------------------------------------------------

            std::vector<std::vector<edge*> > eouters;
            std::vector<std::vector<edge*> > einners;
            if (!get_nonezero_loops(eouters, einners, cross_map, edges, events)) return false;

            //-----------------------------------------------------------------------------
            {
                CrossMap cross_map2;
                std::vector<edge> edges2;
                if (!eouters.empty())
                {
                    if (!remap_edges(cross_map2, edges2, eouters[0])) return false;
                }
                else
                {
                    return false;
                }
                std::vector<edge_event> events2;
                if (!get_edge_events(events2, edges2)) return false;
                //-----------------------------------------------------------------------------
                std::vector<std::vector<edge*> > eloops2;
                if (!get_evenodd_loops(eloops2, cross_map2, edges2, events2)) return false;

                if (!convert_loops(outers, eloops2)) return false;
            }

            {
                if (!convert_loops(inners, einners)) return false;
                if (!revert_loops(inners)) return false;
            }

            //convert loops

            //assert(check_ccw(outers));
            //assert(check_cw (inners));

            return true;
        }

        static bool split_nonezero(std::vector<std::vector<vector2> >& loops, const std::vector<vector2>& loop)
        {
            std::vector<std::vector<vector2> > outers;
            std::vector<std::vector<vector2> > inners;
            if (split_nonezero(outers, inners, loop))
            {
                size_t osz = outers.size();
                size_t isz = inners.size();
                size_t sz = osz + isz;
                loops.resize(sz);
                for (size_t i = 0; i < osz; i++)
                {
                    loops[i].swap(outers[i]);
                }
                for (size_t i = 0; i < isz; i++)
                {
                    loops[osz + i].swap(inners[i]);
                }
                return true;
            }
            return false;
        }

        static bool split_outline(std::vector<std::vector<vector2> >& loops, const std::vector<vector2>& loop)
        {
            std::vector<vector2> vertices;
            std::vector<size_t> indices;
            if (!separate_vertices(vertices, indices, loop)) return false;
            //-----------------------------------------------------------------------------
            std::vector<edge*> eloop;
            CrossMap cross_map2;
            std::vector<edge> edges2;
            {
                CrossMap cross_map;
                std::vector<edge> edges;
                if (!get_edges(cross_map, edges, vertices, indices)) return false;
                //-----------------------------------------------------------------------------
                std::vector<edge_event> events;
                if (!get_edge_events(events, edges)) return false;
                //-----------------------------------------------------------------------------
                std::vector<std::vector<edge*> > eloops;
                if (!get_outline_loops(eloops, cross_map, edges, events)) return false;
                //-----------------------------------------------------------------------------
                if (!eloops.empty())
                {
                    if (!remap_edges(cross_map2, edges2, eloops[0])) return false;
                }
                else
                {
                    return false;
                }

                //if(!convert_loops(loops, eloops))return false;
                //return true;
            }
            {
                std::vector<edge_event> events;
                if (!get_edge_events(events, edges2)) return false;
                //-----------------------------------------------------------------------------
                std::vector<std::vector<edge*> > eloops;
                if (!get_evenodd_loops(eloops, cross_map2, edges2, events)) return false;

                if (!convert_loops(loops, eloops)) return false;
            }

            return true;
        }
        //-----------------------------------------------------------------------------

        bool merge_polygons_internal(std::vector<std::vector<vector2> >& out_loops, const std::vector<std::vector<vector2> >& in_loops)
        {
            std::vector<vector2> vertices;
            std::vector<std::vector<size_t> > indices;
            if (!separate_vertices(vertices, indices, in_loops)) return false;
            //-----------------------------------------------------------------------------
            std::vector<edge*> eloop;
            CrossMap cross_map2;
            std::vector<edge> edges2;
            {
                CrossMap cross_map;
                std::vector<edge> edges;
                if (!get_edges(cross_map, edges, vertices, indices)) return false;
                //-----------------------------------------------------------------------------
                std::vector<edge_event> events;
                if (!get_edge_events(events, edges)) return false;
                //-----------------------------------------------------------------------------
                std::vector<std::vector<edge*> > eloops;
                if (!get_outline_loops(eloops, cross_map, edges, events)) return false; //TODO
                //-----------------------------------------------------------------------------
                if (!eloops.empty())
                {
                    if (!remap_edges(cross_map2, edges2, eloops[0])) return false; //TODO
                }
                else
                {
                    return false;
                }

                //if(!convert_loops(loops, eloops))return false;
                //return true;
            }
            {
                std::vector<edge_event> events;
                if (!get_edge_events(events, edges2)) return false;
                //-----------------------------------------------------------------------------
                std::vector<std::vector<edge*> > eloops;
                if (!get_evenodd_loops(eloops, cross_map2, edges2, events)) return false;

                if (!convert_loops(out_loops, eloops)) return false;
            }

            return true;
        }

        bool set_orientation_inner_loop_internal(std::vector<vector2>& loop)
        {
            make_cw(loop);
            return true;
        }
    }
    //-------------------------------------------------------------------------------------------------------------
    bool split_polygon(std::vector<std::vector<vector2> >& loops, const std::vector<vector2>& loop, int filltype)
    {
        switch (filltype)
        {
        case 0:
            return split_polygon_evenodd(loops, loop);
        case 1:
            return split_polygon_nonezero(loops, loop);
        case 2:
            return split_polygon_outline(loops, loop);
        }
        return false;
    }
    bool split_polygon_evenodd(std::vector<std::vector<vector2> >& loops, const std::vector<vector2>& loop)
    {
        return split_evenodd(loops, loop);
    }

    bool split_polygon_nonezero(std::vector<std::vector<vector2> >& loops, const std::vector<vector2>& loop)
    {
        return split_nonezero(loops, loop);
    }

    bool split_polygon_outline(std::vector<std::vector<vector2> >& loops, const std::vector<vector2>& loop)
    {
        return split_outline(loops, loop);
    }

    bool split_polygon(std::vector<std::vector<vector2> >& outers, std::vector<std::vector<vector2> >& inners, const std::vector<vector2>& loop, int filltype)
    {
        switch (filltype)
        {
        case 0:
            return split_polygon_evenodd(outers, inners, loop);
        case 1:
            return split_polygon_nonezero(outers, inners, loop);
        case 2:
            return split_polygon_outline(outers, inners, loop);
        }
        return false;
    }

    bool split_polygon_evenodd(std::vector<std::vector<vector2> >& outers, std::vector<std::vector<vector2> >& inners, const std::vector<vector2>& loop)
    {
        return split_evenodd(outers, loop);
    }

    bool split_polygon_nonezero(std::vector<std::vector<vector2> >& outers, std::vector<std::vector<vector2> >& inners, const std::vector<vector2>& loop)
    {
        return split_nonezero(outers, inners, loop);
    }

    bool split_polygon_outline(std::vector<std::vector<vector2> >& outers, std::vector<std::vector<vector2> >& inners, const std::vector<vector2>& loop)
    {
        return split_outline(outers, loop);
    }

    //--------------------------------------------------------------------------------------------------------------------------------------------------------

    bool merge_polygons(std::vector<std::vector<vector2> >& out_loops, const std::vector<std::vector<vector2> >& in_loops)
    {
        return merge_polygons_internal(out_loops, in_loops);
    }

    bool set_orientation_inner_loop(std::vector<vector2>& loop)
    {
        return set_orientation_inner_loop_internal(loop);
    }
}
