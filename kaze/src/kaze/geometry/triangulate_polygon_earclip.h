#ifndef KAZE_TRIANGULATE_POLYGON_EARCLIP_H
#define KAZE_TRIANGULATE_POLYGON_EARCLIP_H

#include "../core/types.h"
#include <vector>

namespace kaze
{

    bool triangulate_polygon_earclip(std::vector<vector2>& tri, const std::vector<vector2>& loop);
}

#endif