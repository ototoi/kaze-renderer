#ifndef KAZE_SIMPLE_POLYGON_H
#define KAZE_SIMPLE_POLYGON_H

#include "types.h"
#include <vector>

namespace kaze
{

    bool set_orientation_inner_loop(std::vector<vector2>& loop);

    bool split_polygon(std::vector<std::vector<vector2> >& loops, const std::vector<vector2>& loop, int filltype = 0);
    bool split_polygon_evenodd(std::vector<std::vector<vector2> >& loops, const std::vector<vector2>& loop);
    bool split_polygon_nonezero(std::vector<std::vector<vector2> >& loops, const std::vector<vector2>& loop);
    bool split_polygon_outline(std::vector<std::vector<vector2> >& loops, const std::vector<vector2>& loop);

    bool split_polygon(std::vector<std::vector<vector2> >& outers, std::vector<std::vector<vector2> >& inners, const std::vector<vector2>& loop, int filltype = 0);
    bool split_polygon_evenodd(std::vector<std::vector<vector2> >& outers, std::vector<std::vector<vector2> >& inners, const std::vector<vector2>& loop);
    bool split_polygon_nonezero(std::vector<std::vector<vector2> >& outers, std::vector<std::vector<vector2> >& inners, const std::vector<vector2>& loop);
    bool split_polygon_outline(std::vector<std::vector<vector2> >& outers, std::vector<std::vector<vector2> >& inners, const std::vector<vector2>& loop);

    bool merge_polygons(std::vector<std::vector<vector2> >& out_loops, const std::vector<std::vector<vector2> >& in_loops);
}

#endif