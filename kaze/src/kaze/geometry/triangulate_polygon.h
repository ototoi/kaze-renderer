#ifndef KAZE_TRIANGULATE_POLYGON_H
#define KAZE_TRIANGULATE_POLYGON_H

#include "../core/types.h"
#include <vector>

namespace kaze
{
    bool triangulate_polygon(
        std::vector<vector2>& triangles,
        const std::vector<vector2>& outer_loop,
        int nFillType = 0);
    bool triangulate_polygon(
        std::vector<vector2>& triangles,
        const std::vector<vector2>& outer_loop,
        const std::vector<vector2>& points,
        int nFillType = 0);
    bool triangulate_polygon(
        std::vector<vector2>& triangles,
        const std::vector<std::vector<vector2> >& outer_loop,
        const std::vector<std::vector<vector2> >& inner_loops,
        const std::vector<vector2>& points,
        int nFillType = 0);
}

#endif