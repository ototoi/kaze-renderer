#ifndef KAZE_MOVE_POINTS_ONLINE_H
#define KAZE_MOVE_POINTS_ONLINE_H

#include "../core/types.h"
#include <vector>

namespace kaze
{

    bool move_points_onloop(std::vector<vector2>& loop, std::vector<vector2>& points, const std::vector<vector2>& olp, const std::vector<vector2>& op, real radius = 0.0000001);
}

#endif