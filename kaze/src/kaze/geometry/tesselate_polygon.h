#ifndef KAZE_TESSELATE_POLYGON_H
#define KAZE_TESSELATE_POLYGON_H

#include "trimmed_multi_bezier_patch.hpp"

namespace kaze
{

    bool tesselate_polygon(std::vector<vector3>& triangles, const trimmed_multi_bezier_patch<vector3, vector2>& patch, double dDist, double dRad);
    bool tesselate_polygon(std::vector<vector3>& triangles, std::vector<vector3>& normals, const trimmed_multi_bezier_patch<vector3, vector2>& patch, double dDist, double dRad);
}

#endif