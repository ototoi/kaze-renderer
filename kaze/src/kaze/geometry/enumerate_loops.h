#ifndef KAZE_ENUMERATE_LOOPS_H
#define KAZE_ENUMERATE_LOOPS_H

#include "types.h"
#include <vector>

namespace kaze
{

    struct polyloop_group
    {
        std::vector<vector2> outer_loop;                //outer
        std::vector<std::vector<vector2> > inner_loops; //inner
        std::vector<vector2> points;
    };

    bool enumerate_loops(
        std::vector<polyloop_group>& loop_sets,
        const std::vector<std::vector<vector2> >& outer_loops,
        const std::vector<std::vector<vector2> >& inner_loops,
        const std::vector<vector2>& points,
        int nFillType);
}

#endif