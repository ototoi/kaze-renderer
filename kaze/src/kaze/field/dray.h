#ifndef KAZE_DRAY_H
#define KAZE_DRAY_H

#include "ray.h"

namespace kaze
{

    class dray : public ray
    {
    public:
        dray(
            const vector3& org, const vector3& dir,
            const vector3& dx_org, const vector3& dx_dir,
            const vector3& dy_org, const vector3& dy_dir) : ray(org, dir),
                                                            dx_org_(dx_org), dx_dir_(dx_dir),
                                                            dy_org_(dy_org), dy_dir_(dy_dir) {}

    public:
        const vector3& dx_dir() const { return dx_dir_; }
        const vector3& dy_dir() const { return dy_dir_; }
        const vector3& dx_org() const { return dx_org_; }
        const vector3& dy_org() const { return dy_org_; }
    protected:
        vector3 dx_org_;
        vector3 dx_dir_;
        vector3 dy_org_;
        vector3 dy_dir_;
    };
}
#endif