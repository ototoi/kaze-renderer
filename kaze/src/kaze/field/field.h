#ifndef KAZE_FIELD_H
#define KAZE_FIELD_H

#include "dray.h"
#include "test_info.h"

namespace kaze
{

    class field
    {
    public:
        virtual ~field() {}
        virtual bool test(const dray& r, real dist) const = 0;
        virtual bool test(test_info* info, const dray& r, real dist) const = 0;
        virtual void finalize(test_info* info, const dray& r, real dist) const = 0;
        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;
    };
}

#endif