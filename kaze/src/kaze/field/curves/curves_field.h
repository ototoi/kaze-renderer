#ifndef KAZE_CURVES_FIELD_H
#define KAZE_CURVES_FIELD_H
#include "../field.h"
#include "curves/bezier_curves_loader.h"
#include "parameter_map.h"

namespace kaze
{

    class curves_field_imp;
    class curves_field : public field
    {
    public:
        curves_field(const bezier_curves_loader& loader, const parameter_map& param);
        ~curves_field();

    public:
        bool test(const dray& r, real dist) const;
        bool test(test_info* info, const dray& r, real dist) const;
        void finalize(test_info* info, const dray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        curves_field_imp* imp_;
    };
}

#endif
