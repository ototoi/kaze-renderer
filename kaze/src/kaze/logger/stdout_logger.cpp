
#include "stdout_logger.h"
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <mutex>

namespace kaze
{

#ifndef _WIN32

    static std::mutex mtx;

    void stdout_logger::print(const char* str)
    {
        std::lock_guard<std::mutex> lck(mtx);
        std::cout << str << std::flush;
        //fprintf(stdout, str);
    }
#else
    void stdout_logger::print(const char* str)
    {
        std::cout << str << std::flush;
        //fprintf(stdout, str);
    }
#endif

    namespace
    {
        const char* AA[] = {
            "\r��ߥ(ɄD`)�ߥ�        (߄D� )         \r", /*0*/
            "\r    (ɄD`)           (߄D� )          \r", /*1*/
            "\r��ߥ(ɄD`)�ߥ�      (߄D� )           \r", /*2*/
            "\r    (ɄD`)         (߄D� )            \r", /*3*/
            "\r��ߥ(ɄD`)�ߥ�    (߄D� )             \r", /*4*/
            "\r    (ɄD`)       (߄D� )              \r", /*5*/
            "\r��ߥ(ɄD`)�ߥ�  (߄D� )               \r", /*6*/
            "\r    (ɄD`)     (߄D� )                \r", /*7*/
            "\r��ߥ(ɄD`)�ߥ�(߄D� )                 \r", /*8*/
            "\r    (ɄD`)   (߄D� )                  \r", /*9*/
            "\r��ߥ(ɄD`)�R(߄D� )����               \r", /*A*/
            "\r    (ɄD`)�R(߄D� )                   \r", /*B*/
            "\r��ߥ(ɄD`)�R(߄D� )����               \r", /*C*/
            "\r    (ɄD`)�R(߄D� )                   \r", /*D*/
            "\r    (ɄD`)�R(߄D� )����               \r", /*E*/
            "\r    (ɄD`)�R(߄D� )                   \r", /*F*/
        };
    }

    stdout_2ch_logger::stdout_2ch_logger(int prt) : cnt_(0), idx_(0), prt_(prt)
    {
    }
    void stdout_2ch_logger::print(const char* str)
    {
        int i = cnt_++;
        if ((i % ((1 << prt_) - 1)) == 0)
        {
            int idx = idx_++;
            std::cout << AA[idx & 0xF];
        }
    }
}
