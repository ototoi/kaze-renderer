#ifndef KAZE_STDOUT_LOGGER_H
#define KAZE_STDOUT_LOGGER_H

#include "logger.h"

namespace kaze
{
    class stdout_logger : public logger
    {
    public:
        void print(const char* str);
    };

    class stdout_2ch_logger : public logger
    {
    public:
        stdout_2ch_logger(int prt = 8);
        void print(const char* str);

    private:
        int cnt_;
        int idx_;
        int prt_;
    };
}

#endif
