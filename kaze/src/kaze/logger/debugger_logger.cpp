#include "debugger_logger.h"

#ifdef _WIN32
#include <windows.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <cstring>

namespace kaze
{

    class debugger_logger_IMP
    {
    public:
        void print(const char* str);
    };

    void debugger_logger_IMP::print(const char* str)
    {
        using namespace std;
        if (strstr(str, "\r") != NULL) return;
#ifdef _WIN32
        ::OutputDebugStringA(str); ///< output debugger
#else
        fprintf(stderr, "%s", str); //...
#endif
    }

    //----------------------------------------------------------------------

    debugger_logger::debugger_logger()
    {
        imp_ = new debugger_logger_IMP();
    }

    debugger_logger::~debugger_logger()
    {
        delete imp_;
    }

    void debugger_logger::print(const char* str)
    {
        imp_->print(str);
    }
}
