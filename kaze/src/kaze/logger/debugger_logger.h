#ifndef KAZE_DEBUGGER_LOGGER_H
#define KAZE_DEBUGGER_LOGGER_H

#include "logger.h"

namespace kaze
{

    class debugger_logger_IMP;

    class debugger_logger : public logger
    {
    public:
        debugger_logger();
        ~debugger_logger();

    public:
        void print(const char* str);

    private:
        debugger_logger_IMP* imp_;
    };
}

#endif