#include "msgbox_logger.h"

#ifdef _WIN32
#include <windows.h>
#endif

namespace kaze
{

    msgbox_logger::msgbox_logger(const char* caption)
    {
        if (caption)
        {
            caption_ = caption;
        }
        else
        {
            caption_ = "msgbox_logger";
        }
    }

    void msgbox_logger::print(const char* cstr)
    {
#ifdef _WIN32
        ::MessageBoxA(0, cstr, caption_.c_str(), MB_OK);
#endif
    }
}
