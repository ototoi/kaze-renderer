#ifndef KAZE_MSGBOX_LOGGER_H
#define KAZE_MSGBOX_LOGGER_H

#include "logger.h"

namespace kaze
{
    class msgbox_logger : public logger
    {

    public:
        msgbox_logger(const char* caption = 0);

    public:
        void print(const char* cstr);

    private:
        std::string caption_;
    };
}

#endif
