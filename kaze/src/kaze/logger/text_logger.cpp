#include "text_logger.h"

#include <cstring>

namespace kaze
{

    text_logger::text_logger(const char* filename, const char* mode)
    {
        if (mode[0] == 'a')
        {
            ofs_.open(filename, std::ios_base::app);
        }
        else
        {
            ofs_.open(filename);
        }
        //using namespace system;
        //fileh = invalid_file_handle();
        //delete_file(filename);
        //open_file(&fileh,filename,FILE_OPEN_READ|FILE_OPEN_WRITE|FILE_OPEN_CREATE);
    }

    text_logger::~text_logger()
    {
        ofs_.close();
        //using namespace system;
        //try{
        //	if(fileh != invalid_file_handle())close_file(fileh);
        //}catch(...){
        //
        //}
    }

    void text_logger::print(const char* cstr)
    {
        using namespace std;
        //if(strstr(cstr,"\r") != NULL)return;

        ofs_ << cstr;
        //using namespace system;
        //std::string str(cstr);
        //try{
        //	write(fileh,str.c_str(),str.size());
        //}catch(...){
        //
        //}
    }
}
