#ifndef KAZE_TEXT_LOGGER_H
#define KAZE_TEXT_LOGGER_H

#include "logger.h"

#include <fstream>
//#include "./system/begin.h"
//#include "./system/file.h"
//#include "./system/end.h"

namespace kaze
{

    class text_logger : public logger
    {
    public:
        explicit text_logger(const char* filename, const char* mode = "w");
        ~text_logger();
        void print(const char* cstr);

    private:
        text_logger(const text_logger& rhs);
        text_logger& operator=(const text_logger& rhs);

    private:
        std::ofstream ofs_;
        //system::file_handle fileh;
    };
}

#endif
