#include "composite_render_subdivide_checker.h"

namespace kaze
{
    namespace
    {
        class check_image : public image<bool>
        {
        public:
            check_image(image<bool>* img) : img_(img) {}
        public:
            bool get(int x, int y) const
            {
                return img_->get(x, y);
            }
            void set(int x, int y, const bool& t)
            {
                if (t)
                {
                    if (!img_->get(x, y))
                    {
                        img_->set(x, y, t);
                    }
                }
            }
            int get_width() const { return img_->get_width(); }
            int get_height() const { return img_->get_height(); }
        private:
            image<bool>* img_;
        };
    }
    void composite_render_subdivide_checker::get(image<bool>* img, int x0, int x1, int y0, int y1) const
    {
        size_t sz = checker_.size();
        if (sz)
        {
            check_image cimg(img);
            for (size_t i = 0; i < sz; i++)
            {
                checker_[i]->get(&cimg, x0, x1, y0, y1);
            }
        }
    }

    void composite_render_subdivide_checker::add(const auto_count_ptr<render_subdivide_checker>& chk)
    {
        checker_.push_back(chk);
    }

    size_t composite_render_subdivide_checker::size() const
    {
        return checker_.size();
    }
}
