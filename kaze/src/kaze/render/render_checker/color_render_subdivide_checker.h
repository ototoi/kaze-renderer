#ifndef KAZE_COLOR_RENDER_SUBDIVIDE_CHECKER_H
#define KAZE_COLOR_RENDER_SUBDIVIDE_CHECKER_H

#include "render_subdivide_checker.h"
#include "count_ptr.hpp"

namespace kaze
{

    template <class CHEKER>
    class base_color_render_subdivide_checker : public render_subdivide_checker
    {
    public:
        void get(image<bool>* img, int x0, int x1, int y0, int y1) const
        {
            real V[3 * 3];
            for (int y = y0; y < y1; y++)
            {
                for (int x = x0; x < x1; x++)
                {
                    V[1] = get_value_(y - 1, x);
                    V[2] = get_value_(y - 1, x + 1);
                    V[3] = get_value_(y, x - 1);
                    V[4] = get_value_(y, x);
                    V[5] = get_value_(y, x + 1);
                    V[6] = get_value_(y + 1, x - 1);
                    V[7] = get_value_(y + 1, x);
                    V[8] = get_value_(y + 1, x + 1);
                    img->set(x, y, check_distance_(V));
                }
            }
        }

    protected:
        real get_value_(int x, int y) const
        {
            return static_cast<const CHEKER*>(this)->get_value(x, y);
        }
        bool check_distance_(real V[]) const
        {
            if (static_cast<const CHEKER*>(this)->check_distance(V[0], V[1])) return true;
            if (static_cast<const CHEKER*>(this)->check_distance(V[1], V[2])) return true;
            if (static_cast<const CHEKER*>(this)->check_distance(V[3], V[4])) return true;
            if (static_cast<const CHEKER*>(this)->check_distance(V[4], V[5])) return true;
            if (static_cast<const CHEKER*>(this)->check_distance(V[6], V[7])) return true;
            if (static_cast<const CHEKER*>(this)->check_distance(V[7], V[8])) return true;

            if (static_cast<const CHEKER*>(this)->check_distance(V[0], V[3])) return true;
            if (static_cast<const CHEKER*>(this)->check_distance(V[3], V[6])) return true;
            if (static_cast<const CHEKER*>(this)->check_distance(V[1], V[4])) return true;
            if (static_cast<const CHEKER*>(this)->check_distance(V[4], V[7])) return true;
            if (static_cast<const CHEKER*>(this)->check_distance(V[2], V[5])) return true;
            if (static_cast<const CHEKER*>(this)->check_distance(V[5], V[8])) return true;
            return false;
        }
    };

    class hue_render_subdivide_checker : public base_color_render_subdivide_checker<hue_render_subdivide_checker>
    {
    public:
        hue_render_subdivide_checker(const auto_count_ptr<image<color3> >& img, real limit = real(1.0 / 6.0));
        real get_value(int x, int y) const;
        bool check_distance(real a, real b) const;

    private:
        std::shared_ptr<image<color3> > img_;
        real limit_;
    };

    class intensity_render_subdivide_checker : public base_color_render_subdivide_checker<intensity_render_subdivide_checker>
    {
    public:
        intensity_render_subdivide_checker(const auto_count_ptr<image<color3> >& img, real limit = real(1.0 / 6.0));
        real get_value(int x, int y) const;
        bool check_distance(real a, real b) const;

    private:
        std::shared_ptr<image<color3> > img_;
        real limit_;
    };
}

#endif
