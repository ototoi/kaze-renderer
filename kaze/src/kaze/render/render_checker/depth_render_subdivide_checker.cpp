#include "depth_render_subdivide_checker.h"
#include "values.h"
#include <cmath>

namespace kaze{

    using namespace std;

    depth_render_subdivide_checker::depth_render_subdivide_checker(const auto_count_ptr< image<real> >& img, real distance)
        :img_(img), limit_(distance)
    {        
    }

    static
    bool check_distance(real a, real b, real limit)
    {
        if(fabs(b-a)>limit)return true;
        return false;
    }

    static
    bool  check_distance(real N[], real limit)
    {
       if(check_distance(N[0],N[1],limit))return true;
       if(check_distance(N[1],N[2],limit))return true;
       if(check_distance(N[3],N[4],limit))return true;
       if(check_distance(N[4],N[5],limit))return true;
       if(check_distance(N[6],N[7],limit))return true;
       if(check_distance(N[7],N[8],limit))return true;

       if(check_distance(N[0],N[3],limit))return true;
       if(check_distance(N[3],N[6],limit))return true;
       if(check_distance(N[1],N[4],limit))return true;
       if(check_distance(N[4],N[7],limit))return true;
       if(check_distance(N[2],N[5],limit))return true;
       if(check_distance(N[5],N[8],limit))return true;           
       return false;
    }
    
    void depth_render_subdivide_checker::get(image<bool>* img, int x0, int x1, int y0, int y1)const
    {
        real N[3*3];
        for(int y = y0;y<y1;y++){
            for(int x = x0;x<x1;x++){
                N[0] = get_distance(y-1,x-1);
                N[1] = get_distance(y-1,x  );
                N[2] = get_distance(y-1,x+1);
                N[3] = get_distance(y  ,x-1);
                N[4] = get_distance(y  ,x  );
                N[5] = get_distance(y  ,x+1);
                N[6] = get_distance(y+1,x-1);
                N[7] = get_distance(y+1,x  );
                N[8] = get_distance(y+1,x+1); 
                img->set(x, y, check_distance(N, limit_));
            }
        }        
    }

    real depth_render_subdivide_checker::get_distance(int x, int y)const
    {
        int width  = img_->get_width();
        int height = img_->get_height();
        if(x<0)x=0;
        else if(x>=width)x=width-1;
        if(y<0)y=0;
        else if(y>=height)y=height-1;

        return img_->get(x,y);
    }
	
	
}
