#include "color_render_subdivide_checker.h"
#include "color_convert.h"
#include "spectrum_component.h"

namespace kaze
{

    hue_render_subdivide_checker::hue_render_subdivide_checker(const auto_count_ptr<image<color3> >& img, real limit)
        : img_(img), limit_(limit)
    {
    }

    static real toHue(const color3& c)
    {
        color3 cc = RGBtoHSV(c);
        return cc[0];
    }

    real hue_render_subdivide_checker::get_value(int x, int y) const
    {
        int width = img_->get_width();
        int height = img_->get_height();
        if (x < 0)
            x = 0;
        else if (x >= width)
            x = width - 1;
        if (y < 0)
            y = 0;
        else if (y >= height)
            y = height - 1;

        return toHue(img_->get(x, y));
    }

    bool hue_render_subdivide_checker::check_distance(real a, real b) const
    {
        if (a > b) std::swap(a, b);
        real diff = std::min<real>(fabs(b - a), fabs(a - (b - 1)));
        return (diff > limit_);
    }

    //---------------------------------------------------------------------

    intensity_render_subdivide_checker::intensity_render_subdivide_checker(const auto_count_ptr<image<color3> >& img, real limit)
        : img_(img), limit_(limit)
    {
    }

    static real toValue(const color3& c)
    {
        return rgb_spectrum_component(c).intensity();
    }

    real intensity_render_subdivide_checker::get_value(int x, int y) const
    {
        int width = img_->get_width();
        int height = img_->get_height();
        if (x < 0)
            x = 0;
        else if (x >= width)
            x = width - 1;
        if (y < 0)
            y = 0;
        else if (y >= height)
            y = height - 1;

        return toValue(img_->get(x, y));
    }

    bool intensity_render_subdivide_checker::check_distance(real a, real b) const
    {
        real diff = fabs(b - a);
        return (diff > limit_);
    }
}
