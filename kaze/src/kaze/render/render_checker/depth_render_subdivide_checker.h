#ifndef KAZE_DEPTH_RENDER_SUBDIVIDE_CHECKER_H
#define KAZE_DEPTH_RENDER_SUBDIVIDE_CHECKER_H

#include "render_subdivide_checker.h"
#include "count_ptr.hpp"

namespace kaze
{

    class depth_render_subdivide_checker : public render_subdivide_checker
    {
    public:
        depth_render_subdivide_checker(const auto_count_ptr<image<real> >& img, real distance);
        void get(image<bool>* img, int x0, int x1, int y0, int y1) const;

    protected:
        real get_distance(int x, int y) const;

    private:
        std::shared_ptr<image<real> > img_;
        real limit_; //
    };
}

#endif