#include "normal_render_subdivide_checker.h"
#include "values.h"
#include <cmath>

namespace kaze
{

    using namespace std;

    typedef normal_render_subdivide_checker::normals normals;

    normal_render_subdivide_checker::normal_render_subdivide_checker(const auto_count_ptr<image<normals> >& img, real angle)
        : img_(img)
    {
        limit_ = cos(angle);
    }

    static bool check_normals(const normals& a, const normals& b, real limit)
    {
        if (a.exist && b.exist)
        {
            if (dot(a.n, b.n) < limit) return true;
        }
        return false;
    }

    static bool check_normals(normals N[], real limit)
    {
        if (check_normals(N[0], N[1], limit)) return true;
        if (check_normals(N[1], N[2], limit)) return true;
        if (check_normals(N[3], N[4], limit)) return true;
        if (check_normals(N[4], N[5], limit)) return true;
        if (check_normals(N[6], N[7], limit)) return true;
        if (check_normals(N[7], N[8], limit)) return true;

        if (check_normals(N[0], N[3], limit)) return true;
        if (check_normals(N[3], N[6], limit)) return true;
        if (check_normals(N[1], N[4], limit)) return true;
        if (check_normals(N[4], N[7], limit)) return true;
        if (check_normals(N[2], N[5], limit)) return true;
        if (check_normals(N[5], N[8], limit)) return true;
        return false;
    }

    normals normal_render_subdivide_checker::get_normals(int x, int y) const
    {
        int width = img_->get_width();
        int height = img_->get_height();
        if (x < 0 || x >= width || y < 0 || y >= height)
        {
            normals tmp;
            tmp.exist = false;
            return tmp;
        }
        else
        {
            return img_->get(x, y);
        }
    }

    void normal_render_subdivide_checker::get(image<bool>* img, int x0, int x1, int y0, int y1) const
    {
        //------------------------------
        //int width  = img_->get_width();
        //int height = img_->get_height();

        normals N[3 * 3];
        for (int y = y0; y < y1; y++)
        {
            for (int x = x0; x < x1; x++)
            {
                N[0] = get_normals(y - 1, x - 1);
                N[1] = get_normals(y - 1, x);
                N[2] = get_normals(y - 1, x + 1);
                N[3] = get_normals(y, x - 1);
                N[4] = get_normals(y, x);
                N[5] = get_normals(y, x + 1);
                N[6] = get_normals(y + 1, x - 1);
                N[7] = get_normals(y + 1, x);
                N[8] = get_normals(y + 1, x + 1);
                img->set(x, y, check_normals(N, limit_));
            }
        }
    }
}
