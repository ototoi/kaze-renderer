#ifndef KAZE_NORMAL_RENDER_SUBDIVIDE_CHECKER_H
#define KAZE_NORMAL_RENDER_SUBDIVIDE_CHECKER_H

#include "render_subdivide_checker.h"
#include "count_ptr.hpp"
#include "values.h"

namespace kaze
{

    class normal_render_subdivide_checker : public render_subdivide_checker
    {
    public:
        struct normals
        {
            vector3 n;
            bool exist;
        };

    public:
        normal_render_subdivide_checker(const auto_count_ptr<image<normals> >& img, real angle = values::radians(30));
        void get(image<bool>* img, int x0, int x1, int y0, int y1) const;

    protected:
        normals get_normals(int x, int y) const;

    private:
        std::shared_ptr<image<normals> > img_;
        real limit_; //
    };
}

#endif