#ifndef KAZE_COMPOSITE_RENDER_SUBDIVIDE_CHECKER_H
#define KAZE_COMPOSITE_RENDER_SUBDIVIDE_CHECKER_H

#include "render_subdivide_checker.h"
#include "count_ptr.hpp"
#include <vector>

namespace kaze
{

    class composite_render_subdivide_checker
    {
    public:
        void get(image<bool>* img, int x0, int x1, int y0, int y1) const;

    public:
        void add(const auto_count_ptr<render_subdivide_checker>& chk);
        size_t size() const;

    private:
        std::vector<std::shared_ptr<render_subdivide_checker> > checker_;
    };
}

#endif