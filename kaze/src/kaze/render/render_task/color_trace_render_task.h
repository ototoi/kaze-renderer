#ifndef KAZE_COLOR_TRACE_RENDER_TASK_H
#define KAZE_COLOR_TRACE_RENDER_TASK_H

#include "render_task.h"
#include "count_ptr.hpp"
#include "tracer.h"
#include "light.h"
#include "camera.h"
#include "render_frame.h"
#include "pixel_sampler.h"

namespace kaze
{

    class color_trace_render_task : public render_task
    {
    public:
        color_trace_render_task(int w, int h);
        ~color_trace_render_task();
        void add(const auto_count_ptr<tracer>& trc);
        void add(const auto_count_ptr<camera>& cmr);
        void add(const auto_count_ptr<render_frame>& rf);
        void add(const auto_count_ptr<pixel_sampler>& ps);

    public:
        int run();

    public:
        int render(int w, int h);

    private:
        int w_;
        int h_;
        std::shared_ptr<tracer> trc_;
        std::shared_ptr<camera> cmr_;
        std::shared_ptr<render_frame> rf_;
        std::shared_ptr<pixel_sampler> ps_;
    };
}

#endif