#ifndef KAZE_INCREMENTAL_PATHTRACE_RENDER_TASK_H
#define KAZE_INCREMENTAL_PATHTRACE_RENDER_TASK_H

#include "render_task.h"
#include "count_ptr.hpp"
#include "tracer.h"
#include "light.h"
#include "camera.h"
#include "render_frame.h"
#include "pixel_sampler.h"

namespace kaze
{

    class incremental_pathtrace_render_task : public render_task
    {
    public:
        incremental_pathtrace_render_task(int w, int h);
        ~incremental_pathtrace_render_task();
        void add(const auto_count_ptr<tracer>& trc);
        void add(const auto_count_ptr<camera>& cmr);
        void add(const auto_count_ptr<render_frame>& rf);
        void add(const auto_count_ptr<pixel_sampler>& ps);

    public:
        int run();

    public:
        int render(int w, int h);

    private:
        int w_;
        int h_;
        std::shared_ptr<tracer> trc_;
        std::shared_ptr<camera> cmr_;
        std::shared_ptr<render_frame> rf_;
        std::shared_ptr<pixel_sampler> ps_;
    };
}

#endif