#ifndef KAZE_Z_TRACE_RENDER_TASK_H
#define KAZE_Z_TRACE_RENDER_TASK_H

#include "render_task.h"
#include "count_ptr.hpp"
#include "camera.h"
#include "intersection.h"
#include "render_frame.h"

namespace kaze
{

    class z_trace_render_task : public render_task
    {
    public:
        z_trace_render_task(int w, int h, const matrix4& mat);
        ~z_trace_render_task();
        void add(const auto_count_ptr<intersection>& inter);
        void add(const auto_count_ptr<camera>& cmr);
        void add(const auto_count_ptr<render_frame>& rf);

    public:
        virtual int run();

    protected:
        int render(int w, int h, const matrix4& mat);

    private:
        int w_;
        int h_;
        matrix4 mat_; //clip
        std::shared_ptr<intersection> inter_;
        std::shared_ptr<camera> cmr_;
        std::shared_ptr<render_frame> rf_;
    };
}

#endif