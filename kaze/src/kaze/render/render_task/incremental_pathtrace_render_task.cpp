#include "incremental_pathtrace_render_task.h"

#include "render_region_distributor.h"
#include "auto_render_region_distributor.h"
#include "swirl_render_region_distributor.h"
#include "circular_render_region_distributor.h"
#include "render_frame.h"
#include "image.hpp"
#include "memory_image.hpp"

#include "camera.h"
#include "tracer.h"
#include "logger.h"
#include "timer.h"
#include "count_ptr.hpp"
#include "transformer.h"

#include "intersection.h"

#include "transform_matrix.h"
#include "parameter_map.h"
#include "transformed_intersection.h"

#include "shadered_intersection.h"
#include "bvh_composite_intersection.h"
#include "trace_shadower.h"
#include "shaded_trace_shadower.h"
#include "spot_light.h"
#include "exhibited_composite_light.h"
#include "ray_tracer.h"
#include "console_render_frame.h"
#include "rockenfield_render_frame.h"
#include "composite_render_frame.h"
#include "color_convert.h"

#include "pixel_sampler.h"
#include "grid_pixel_sampler.h"
#include "lds_pixel_sampler.h"
#include "time_pixel_sampler.h"
#include "lens_pixel_sampler.h"
#include "composite_pixel_sampler.h"

#include "basic_collection.hpp"

#include "print_progress_render_frame.h"

#include "renderer_util.h"

#include "QMC.h"

#include <string>
#include <iostream>
#include <thread>

#if defined(_MSC_VER)
#if (_MSC_VER < 1400) //msc_ver < vc2005
#define KZSNPRINTF _snprintf
#else
#define KZSNPRINTF _snprintf_s
#endif
#else
#define KZSNPRINTF snprintf
#endif

typedef kaze::memory_image<kaze::vector4> real_image;

namespace kaze
{
    namespace
    {

        struct thread_st
        {
            int count;
            render_region_distributor* rrg;
            render_frame* rf;
            real_image* old;
            real_image* img;
            const camera* c;
            const tracer* rt;
            const pixel_sampler* ps;
        };

        static color3 gamma(const color3& c, real g = 2.2)
        {
            return color3(pow(c[0], g), pow(c[1], g), pow(c[2], g));
        }

        static int render_task_(
            int count,
            real_image& old,
            real_image& img,
            const tracer& rt,
            const camera& c,
            const pixel_sampler& ps,
            const render_region& rr,
            int WIDTH, int HEIGHT)
        {
            mediation air;
            double iW = 2.0 / WIDTH;
            double iH = 2.0 / HEIGHT;

            int w = rr.x1 - rr.x0;
            int h = rr.y1 - rr.y0;

            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    int xx = rr.x0 + x;
                    int yy = rr.y0 + y;
                    img[yy * WIDTH + xx] = vector4(0, 0, 0, 0);
                }
            }

            std::vector<pixel_sample> samples;

            ps.get(samples, rr.x0, rr.y0, rr.x1, rr.y1);

            std::vector<int> totals(w * h);
            std::vector<int> hits(w * h);
            for (int i = 0; i < samples.size(); i++)
            {

                int x = samples[i].x;
                int y = samples[i].y;

                int dx = (int)x - rr.x0;
                int dy = (int)y - rr.y0;

                totals[dy * w + dx]++;

                ray r = c.shoot(samples[i], WIDTH, HEIGHT);
                spectrum spc;
                if (rt.trace(&spc, r, air))
                {
                    color3 col = gamma(convert(spc), 2.2);
                    real a = coverage(spc);
                    img[y * WIDTH + x] += vector4(col[0], col[1], col[2], a);
                    hits[dy * w + dx]++;
                }
            }
            //-----------------------------------------------------
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    int xx = rr.x0 + x;
                    int yy = rr.y0 + y;
                    if (hits[y * w + x])
                    {
                        vector4 c = img[yy * WIDTH + xx] * (1.0 / real(hits[y * w + x]));
                        color3 g = gamma(vector3(c[0], c[1], c[2]), 1.0 / 2.2);
                        img[yy * WIDTH + xx] = vector4(g[0], g[1], g[2], c[3] * real(hits[y * w + x]) / totals[y * w + x]);
                    }
                    else
                    {
                        img[yy * WIDTH + xx] = vector4(0, 0, 0, 0);
                    }
                }
            }

            if (count != 0)
            {
                int kkk = count + 1;
                real a = real(kkk - 1) / kkk;
                real b = 1 - a;
                for (int y = 0; y < h; y++)
                {
                    for (int x = 0; x < w; x++)
                    {
                        int xx = rr.x0 + x;
                        int yy = rr.y0 + y;

                        img[yy * WIDTH + xx] = a * old[yy * WIDTH + xx] + b * img[yy * WIDTH + xx];
                    }
                }
            }

            return w * h;
        }
#ifdef _WIN32
        unsigned int __stdcall render_thread(void* p)
        {
#else
        void* render_thread(void* p)
        {
#endif
            thread_st* pst = reinterpret_cast<thread_st*>(p);

#ifdef __unix__
/*
	    cpu_set_t mask;
	    CPU_ZERO(&mask);
	    CPU_SET(pst->cpu_id,&mask);
	    sched_setaffinity(0,sizeof(cpu_set_t),&mask);
	    */
#endif
            render_region rr;
            while (pst->rrg->get(&rr))
            {
                int n = render_task_(pst->count, *(pst->old), *(pst->img), *(pst->rt), *(pst->c), *(pst->ps), rr, pst->rrg->get_width(), pst->rrg->get_height());
                if (pst->rf) pst->rf->render(rr.x0, rr.y0, rr.x1, rr.y1, *(pst->img));
                std::this_thread::yield();
            }

            return 0;
        }

        const int N = 16;

        static int render_(int WIDTH, int HEIGHT,
                           const std::shared_ptr<tracer>& rt,
                           const std::shared_ptr<camera>& cmra,
                           const std::shared_ptr<render_frame>& rf,
                           const std::shared_ptr<pixel_sampler>& ps)
        {
            timer t1;

            std::shared_ptr<real_image> images[2];

            images[0] = std::shared_ptr<real_image>(new real_image(WIDTH, HEIGHT));
            images[1] = std::shared_ptr<real_image>(new real_image(WIDTH, HEIGHT));

            for (int k = 0; k < 2; k++)
            {
                real_image* img = images[k].get();
                for (int y = 0; y < HEIGHT; y++)
                {
                    for (int x = 0; x < WIDTH; x++)
                    {
                        img->set(x, y, vector4(0, 0, 0, 0));
                    }
                }
            }

            composite_render_frame crf;
            if (rf.get())
            {
                crf.add(rf);
            }
            crf.add(new print_progress_render_frame(WIDTH, HEIGHT));
            crf.begin();

            std::shared_ptr<pixel_sampler> cps;
            if (ps.get())
            {
                cps.reset(new composite_pixel_sampler(new lds_pixel_sampler(1, 1), new lds_time_pixel_sampler(), new lds_lens_pixel_sampler()));
            }
            else
            {
                cps.reset(new composite_pixel_sampler(new lds_pixel_sampler(1, 1), new lds_time_pixel_sampler(), new lds_lens_pixel_sampler()));
            }

            int nCPU = renderer_util::get_cpu_number();
            if (nCPU >= N) nCPU = N;

            //auto_render_region_distributor rrg(WIDTH,HEIGHT,nCPU);
            //swirl_render_region_distributor rrg(WIDTH,HEIGHT);
            circular_render_region_distributor rrg(WIDTH, HEIGHT);
            kaze::mediation air; //
            {

#ifdef _WIN32
                thread_st st;
                st.c = cmra.get();

                st.rrg = &rrg;
                st.rt = rt.get();
                st.rf = &crf;
                st.ps = cps.get();

                HANDLE hThreads[N];
                int k = 0;
                while (true)
                {
                    st.count = k;

                    real_image* old = images[(k + 1) & 1].get();
                    real_image* img = images[k & 1].get();

                    st.old = old;
                    st.img = img;

                    t1.start();
                    for (int i = 0; i < nCPU; i++)
                    {
                        hThreads[i] = (HANDLE)::_beginthreadex(NULL, 0, render_thread, &st, 0, NULL);
                    }

                    ::WaitForMultipleObjects(nCPU, hThreads, TRUE, INFINITE);
                    for (int i = 0; i < nCPU; i++)
                    {
                        ::CloseHandle(hThreads[i]);
                    }
                    t1.end();
                    kaze::print_log("rendering time %d ms\n", t1.msec());
                    crf.end();

                    std::this_thread::yield();
                    st.rrg->reset();
                    k++;
                }
#else
                thread_st st;
                st.c = cmra.get();

                st.rrg = &rrg;
                st.rt = rt.get();
                st.rf = &crf;
                st.ps = cps.get();

                pthread_t hThreads[N];
                int k = 0;
                while (true)
                {
                    st.count = k;
                    real_image* old = images[(k + 1) & 1].get();
                    real_image* img = images[k & 1].get();

                    st.old = old;
                    st.img = img;

                    t1.start();
                    for (int i = 0; i < N; i++)
                    {
                        pthread_create(hThreads + i, NULL, render_thread, &st);
                    }
                    for (int i = 0; i < N; i++)
                    {
                        pthread_join(hThreads[i], NULL);
                    }
                    t1.end();
                    kaze::print_log("rendering time %d ms\n", t1.msec());
                    crf.end();

                    std::this_thread::yield();
                    st.rrg->reset();
                    k++;
                }
#endif
            }

            //---------------------------------------------

            return 0;
        }
    }
    incremental_pathtrace_render_task::incremental_pathtrace_render_task(int w, int h)
        : w_(w), h_(h)
    {
        ; //
    }

    incremental_pathtrace_render_task::~incremental_pathtrace_render_task()
    {
        ;
    }

    void incremental_pathtrace_render_task::add(const auto_count_ptr<tracer>& trc)
    {
        trc_ = trc;
    }

    void incremental_pathtrace_render_task::add(const auto_count_ptr<camera>& cmr)
    {
        cmr_ = cmr;
    }

    void incremental_pathtrace_render_task::add(const auto_count_ptr<render_frame>& rf)
    {
        rf_ = rf;
    }
    void incremental_pathtrace_render_task::add(const auto_count_ptr<pixel_sampler>& ps)
    {
        ps_ = ps;
    }

    int incremental_pathtrace_render_task::render(int w, int h)
    {
        if (cmr_.get())
        {
            return render_(w, h, trc_, cmr_, rf_, ps_);
        }
        return 0;
    }

    int incremental_pathtrace_render_task::run()
    {
        return this->render(w_, h_);
    }
}
