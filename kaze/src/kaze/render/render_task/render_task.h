#ifndef KAZE_RENDER_TASK_H
#define KAZE_RENDER_TASK_H

namespace kaze
{

    class render_task
    {
    public:
        virtual ~render_task() {}
        virtual int run() = 0;
    };
}

#endif
