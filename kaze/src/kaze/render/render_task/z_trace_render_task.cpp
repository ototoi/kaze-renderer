#include "z_trace_render_task.h"

#include "render_region_distributor.h"
#include "auto_render_region_distributor.h"
#include "render_frame.h"
#include "composite_render_frame.h"
#include "image.hpp"
#include "memory_image.hpp"

#include "camera.h"
#include "tracer.h"
#include "logger.h"
#include "timer.h"
#include "count_ptr.hpp"
#include "intersection.h"
#include "bvh_composite_intersection.h"

#include "renderer_util.h"
#include "print_progress_render_frame.h"
#include "test_info.h"

#include <string>
#include <iostream>
#include <thread>

namespace kaze
{
    typedef memory_image<double> depth_image;

    namespace
    {

        struct thread_st
        {
            render_region_distributor* rrg;
            render_frame* rf;
            depth_image* img;
            const camera* c;
            const intersection* inter;
            int w;
            int h;
            matrix4 clip;
        };

        inline real get_z(const vector3& p, const matrix4& m)
        {
            vector4 p2 = m * vector4(p[0], p[1], p[2], 1);
            return p2[2] / p2[3];
        }

        static int render_task_(depth_image& img, const intersection& inter, const camera& c, const render_region& rr, int width, int height, const matrix4& clip)
        {
            mediation air;
            double iW = 2.0 / width;
            double iH = 2.0 / height;

            int w = rr.x1 - rr.x0;
            int h = rr.y1 - rr.y0;

            int x0 = rr.x0;
            int x1 = rr.x1;
            int y0 = rr.y0;
            int y1 = rr.y1;

            for (int y = y0; y < y1; y++)
            {
                for (int x = x0; x < x1; x++)
                {
                    double xx = x + 0.5;
                    double yy = y + 0.5;

                    xx = xx * iW - 1;
                    yy = -(yy * iH - 1);

                    real f = img.get(x, y);

                    ray r = c.shoot(xx, yy);
                    test_info info;
                    if (inter.test(&info, r, values::far()))
                    {
                        vector3 pos = r.origin() + info.get_distance() * r.direction();
                        real z = get_z(pos, clip);
                        if (0 < z && z < f)
                        {
                            img.set(x, y, z);
                        }
                    }
                }
            }

            return w * h;
        }

#ifdef _WIN32
        unsigned int __stdcall render_thread(void* p)
        {
#else
        void* render_thread(void* p)
        {
#endif
            thread_st* pst = reinterpret_cast<thread_st*>(p);
            render_region rr;
            while (pst->rrg->get(&rr))
            {
                int n = render_task_(*(pst->img), *(pst->inter), *(pst->c), rr, pst->rrg->get_width(), pst->rrg->get_height(), pst->clip);
                if (pst->rf) pst->rf->render(rr.x0, rr.y0, rr.x1, rr.y1, *(pst->img));
                std::this_thread::yield();
            }

            return 0;
        }

        static int render_(
            int width, int height,
            const std::shared_ptr<intersection>& inter,
            const std::shared_ptr<camera>& cmra,
            const std::shared_ptr<render_frame>& rf,
            const matrix4& clip)
        {
            static const int N = 16;

            timer t1;

            depth_image img(width, height);
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    img.set(x, y, values::far());
                }
            }
            composite_render_frame crf;
            if (rf.get())
            {
                crf.add(rf);
            }
            crf.add(new print_progress_render_frame(width, height));
            crf.begin();

            int nCPU = renderer_util::get_cpu_number();
            if (nCPU >= N) nCPU = N;

            auto_render_region_distributor rrg(width, height, nCPU);
            kaze::mediation air; //
            {
                t1.start();
                thread_st st;
                st.c = cmra.get();
                st.img = &img;
                st.rrg = &rrg;
                st.inter = inter.get();
                st.rf = &crf;
                st.clip = clip;
#ifdef _WIN32
                HANDLE hThreads[N];
                for (int i = 0; i < nCPU; i++)
                {
                    hThreads[i] = (HANDLE)::_beginthreadex(NULL, 0, render_thread, &st, 0, NULL);
                }

                ::WaitForMultipleObjects(nCPU, hThreads, TRUE, INFINITE);
                for (int i = 0; i < nCPU; i++)
                {
                    ::CloseHandle(hThreads[i]);
                }
#else
                pthread_t hThreads[N];
                thread_st stThreads[N];
                for (int i = 0; i < N; i++)
                {
                    stThreads[i] = st;
                    pthread_create(hThreads + i, NULL, render_thread, &st);
                }
                for (int i = 0; i < N; i++)
                {
                    pthread_join(hThreads[i], NULL);
                }
#endif
                t1.end();
                kaze::print_log("rendering time %d ms\n", t1.msec());
            }

            crf.end();
            return 0;
        }
    }
    z_trace_render_task::z_trace_render_task(int w, int h, const matrix4& mat)
        : w_(w), h_(h), mat_(mat)
    {
        inter_.reset(new bvh_composite_intersection());
        rf_.reset(new composite_render_frame());
    }

    z_trace_render_task::~z_trace_render_task() {}

    void z_trace_render_task::add(const auto_count_ptr<intersection>& inter)
    {
        dynamic_cast<bvh_composite_intersection*>(inter_.get())->add(inter);
    }

    void z_trace_render_task::add(const auto_count_ptr<camera>& cmr)
    {
        cmr_ = cmr;
    }

    void z_trace_render_task::add(const auto_count_ptr<render_frame>& rf)
    {
        composite_render_frame* crf = dynamic_cast<composite_render_frame*>(rf_.get());
        crf->add(rf);
    }

    int z_trace_render_task::run()
    {
        return this->render(w_, h_, mat_);
    }

    int z_trace_render_task::render(int w, int h, const matrix4& mat)
    {
        bvh_composite_intersection* bvh = dynamic_cast<bvh_composite_intersection*>(inter_.get());
        bvh->construct();

        if (cmr_.get())
        {
            return render_(w, h, inter_, cmr_, rf_, mat);
        }
        return 0;
    }
}
