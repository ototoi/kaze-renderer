#include "z_rasterize.h"

#include "values.h"
#include "rasterize_clipper.h"

#include "logger.h"

#include <vector>

namespace kaze
{
    namespace
    {

        //--------------------------------------------------------------------------------------
        typedef vector4 vertex;

        struct mesh_face
        {
            size_t i0;
            size_t i1;
            size_t i2;
        };

        struct scan_entry
        {
            vector2 min;
            vector2 max;
        };

        //--------------------------------------------------------------------------------------
        typedef std::vector<vertex> vertex_list;
        typedef std::vector<mesh_face> face_list;
        //--------------------------------------------------------------------------------------

        static inline int top_int(real x)
        {
            return (int)floor(x + real(0.5));
        }

        static inline vector2 delta_xz(const vector3& edge)
        {
            static const real EPSILON = values::epsilon() * 1000;

            assert(edge[1] > EPSILON);

            real ily = real(1) / edge[1];

            real dx = edge[0] * ily;
            real dz = edge[2] * ily;

            return vector2(dx, dz);
        }

        //--------------------------------------------------------------------------------------
        static void scan_horizontal(image<real>* img, int W, int H, int y, const scan_entry& se)
        {
            static const real EPSILON = values::epsilon() * 1000;

            int l, r;
            l = top_int(se.min[0]);
            r = top_int(se.max[0]);

            if (l < 0) l = 0;
            if (W < r) r = W;

            if (l < r)
            {
                real gap_x = real(l + 0.5) - se.min[0];
                assert(gap_x >= 0);
                //
                //start position
                //real ex = se.max[0] - se.min[0];
                //real ez = se.max[0] - se.min[0];
                vector2 e = se.max - se.min;

                assert(e[0] > EPSILON);
                //if(e[0]<EPSILON)return;

                real dz = e[1] / e[0];

                real sz = se.min[1] + dz * gap_x;

                int x = l;
                do
                {
                    //for(int x = l;x<r;x++){
                    real z = img->get(x, y);
                    if (z > sz)
                    {
                        img->set(x, y, sz);
                    }
                    sz += dz;

                    x++;
                } while (x < r);
            }
        }

        //--------------------------------------------------------------------------------------
        static inline vector3 branch(const vector3& a, const vector3& b, real x)
        {
            assert(b[1] != a[1]);
            real t = (x - a[1]) / (b[1] - a[1]);
            return vector3(a[0] * (1 - t) + b[0] * t, x, a[2] * (1 - t) + b[2] * t);
        }
        static void scan_vertical(image<real>* img, int W, int H, const vector3& pt, const vector3& pm, const vector3& pb)
        {
            static const real EPSILON = values::epsilon() * 1000;

            if (pb[1] - pt[1] < EPSILON) return;

            int t, b;
            t = top_int(pt[1]);
            b = top_int(pb[1]);

            if (!(t < b)) return;

            if (t < 0) t = 0;
            if (H < b) b = H;

            real mid = pm[1];

            vector3 pl, pr;
            vector3 tmp = branch(pt, pb, mid); //pt->
            if (tmp[0] < pm[0])
            {
                pl = tmp;
                pr = pm;
            }
            else
            {
                pl = pm;
                pr = tmp;
            }

            int m = top_int(mid);
            if (m < 0) m = 0;
            if (H < m) m = H;

            if (t < m)
            {                         //upper
                vector3 el = pl - pt; //pt->pl
                vector3 er = pr - pt; //pt->pr
                vector2 dl = delta_xz(el);
                vector2 dr = delta_xz(er);
                //
                real gap_y = real(t + 0.5) - pt[1];
                assert(gap_y >= 0);
                //
                //start position
                vector2 sl = vector2(pt[0], pt[2]) + dl * gap_y;
                vector2 sr = vector2(pt[0], pt[2]) + dr * gap_y;

                //for(int y=t;y<m;y++){
                int y = t;
                do
                {
                    scan_entry se;
                    se.min = sl;
                    se.max = sr;
                    scan_horizontal(img, W, H, y, se);
                    sl += dl; //
                    sr += dr; //

                    y++;
                } while (y < m);
                //}
            }
            if (m < b)
            {                         //lower
                vector3 el = pb - pl; //pl->pb
                vector3 er = pb - pr; //pr->pb
                vector2 dl = delta_xz(el);
                vector2 dr = delta_xz(er);
                //
                real gap_y = real(m + 0.5) - mid;
                assert(gap_y >= 0);
                //
                //start position
                vector2 sl = vector2(pl[0], pl[2]) + dl * gap_y;
                vector2 sr = vector2(pr[0], pr[2]) + dr * gap_y;

                //for(int y=m;y<b;y++){
                int y = m;
                do
                {
                    scan_entry se;
                    se.min = sl;
                    se.max = sr;
                    scan_horizontal(img, W, H, y, se);
                    sl += dl; //
                    sr += dr; //

                    y++;
                } while (y < b);
                //}
            }
        }
        //--------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------
        template <class T>
        static inline void swap_(T& a, T& b)
        {
            T t = a;
            a = b;
            b = t;
        }
        template <class T>
        static inline void sort_index_(T t[3], int i)
        {
            if (t[0][i] > t[1][i]) swap_(t[0], t[1]);
            if (t[1][i] > t[2][i]) swap_(t[1], t[2]);
            if (t[0][i] > t[1][i]) swap_(t[0], t[1]);
        }
        static void rasterize_triangle(image<real>* img, int W, int H, const vector3& p0, const vector3& p1, const vector3& p2)
        {
            vector3 points[3] = {p0, p1, p2};
            sort_index_(points, 1); //ys
            scan_vertical(img, W, H, points[0], points[1], points[2]);
        }

        //--------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------
        static inline vector3 clip_to_screen(const vertex& c, real W, real H)
        {
            real iw = real(1) / c[3];
            return vector3(W * c[0] * iw, H * (1 - c[1] * iw), c[2] * iw);
        }

        void z_rasterize_internal(image<real>* img, int W, int H, const matrix4& clip, const vector3& a, const vector3& b, const vector3& c)
        {
            vertex p0 = clip * vertex(a[0], a[1], a[2], 1.0);
            vertex p1 = clip * vertex(b[0], b[1], b[2], 1.0);
            vertex p2 = clip * vertex(c[0], c[1], c[2], 1.0);
            if (clip3(p0, p1, p2))
            { //CLIP
                rasterize_triangle(img, W, H, clip_to_screen(p0, W, H), clip_to_screen(p1, W, H), clip_to_screen(p2, W, H));
            }
        }
        //--------------------------------------------------------------------------------------
    }
}

//--------------------------------------------------------------------------------------
namespace kaze
{

    void z_rasterize(image<real>* img, const matrix4& clip, const vector3& a, const vector3& b, const vector3& c)
    {
        int w = img->get_width();
        int h = img->get_height();
        z_rasterize_internal(img, w, h, clip, a, b, c);
    }

    void z_rasterize(image<real>& img, const matrix4& clip, const vector3& a, const vector3& b, const vector3& c)
    {
        z_rasterize(&img, clip, a, b, c);
    }
}