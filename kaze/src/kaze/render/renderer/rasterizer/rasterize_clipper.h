#ifndef KAZE_RASTERIZE_CLIPPER_H
#define KAZE_RASTERIZE_CLIPPER_H

#include "types.h"

namespace kaze
{

    inline bool is_range_M_01(real v, real x)
    {
        return (0 > v) && (v > x);
    }
    inline bool is_range_P_01(real v, real x)
    {
        return (0 < v) && (v < x);
    }

    // -1..1
    inline bool is_range_P_MP(real v, real x)
    {
        return (-x < v) && (v < x);
    }
    inline bool is_range_M_MP(real v, real x)
    {
        return (-x > v) && (v > x);
    }

#ifndef KAZE_USE_CLIP_MP
    //0..1

    inline bool is_clipin(const vector4& v)
    {
        real x = v[3];
        return ((x > 0) ? is_range_P_01(v[0], x) && is_range_P_01(v[1], x) && is_range_P_01(v[2], x)
                        : is_range_M_01(v[0], x) && is_range_M_01(v[1], x) && is_range_M_01(v[2], x));
    }
#else
    inline bool is_clipin(const vector4& v)
    {
        real x = v[3];
        return ((x > 0) ? is_range_P_MP(v[0], x) && is_range_P_MP(v[1], x) && is_range_P_MP(v[2], x)
                        : is_range_M_MP(v[0], x) && is_range_M_MP(v[1], x) && is_range_M_MP(v[2], x));
    }
#endif

    int clip3(const vector4& p0, const vector4& p1, const vector4& p2);

    inline bool clip_face_01(const vector4& p0, const vector4& p1, const vector4& p2)
    {
        return (clip3(p0, p1, p2)) ? true : false;
    }

    bool clip_line_01(vector2& p0, vector2& p1);
    bool clip_line_01(vector3& p0, vector3& p1);
}

#endif