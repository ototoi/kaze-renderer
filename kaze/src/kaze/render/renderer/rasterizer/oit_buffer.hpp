#ifndef KAZE_BUFFER_HPP
#define KAZE_BUFFER_HPP

#include <vector>

namespace kaze
{

    /*
	 * class for Order Independent Trancparency rasreization.
	 */
    template <class T>
    class oit_buffer
    {
    public:
        struct tri
        {
            T value;
            int next;
        };

    public:
        oit_buffer(int w, int h) : w_(w), h_(h) { init(); }
        ~oit_buffer() {}
        void set(int x, int y, const T& t)
        {
            size_t idx = y * w_ + x;
            if (buffer_[idx] == -1)
            {
                int tsz = (int)tri_.size();
                tri tmp;
                tmp.value = t;
                tmp.next = -1;
                tri_.push_back(tmp);
                buffer_[idx] = tsz;
            }
            else
            {
                assert(buffer_[idx] < tri_.size());
                int k = buffer_[idx];
                while (1)
                {
                    if (tri_[k].next == -1) break;
                    k = tri_[k].next;
                }
                int tsz = (int)tri_.size();
                tri tmp;
                tmp.value = t;
                tmp.next = -1;
                tri_.push_back(tmp);
                buffer_[k] = tsz;
            }
        }
        void get(int x, int y, std::vector<T>& v)
        {
            size_t idx = y * w_ + x;
            if (buffer_[idx] != -1)
            {
                int k = buffer_[idx];
                do
                {
                    v.push_back(tri_[k].value);
                    k = tri_[k].next;
                } while (k != -1);
            }
        }
        int get_width() const { return w_; }
        int get_height() const { return h_; }

        void reserve(size_t sz)
        {
            tri_.reserve(sz);
        }

    protected:
        void init()
        {
            size_t sz = w_ * h_;
            buffer_.resize(sz);
            for (size_t i = 0; i < sz; i++)
            {
                buffer_[i] = -1;
            }
        }

    private:
        std::vector<int> buffer_;
        std::vector<tri> tri_;
        int h_;
        int w_;
    };
}

#endif