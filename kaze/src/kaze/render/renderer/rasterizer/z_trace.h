#ifndef KAZE_Z_TRACE_H
#define KAZE_Z_TRACE_H

#include "types.h"
#include "image.hpp"
#include "intersection.h"
#include "camera.h"

namespace kaze
{

    void z_trace(image<real>* img, const matrix4& clip, const camera* cmr, const intersection* inter);
    void z_trace(image<real>* img, const matrix4& clip, const camera& cmr, const intersection& inter);
    void z_trace(image<real>& img, const matrix4& clip, const camera& cmr, const intersection& inter);
}

#endif