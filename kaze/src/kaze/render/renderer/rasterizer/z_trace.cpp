#include "z_trace.h"

#include "test_info.h"
#include "values.h"
#include "logger.h"

namespace kaze
{
    namespace
    {

        inline real get_z(const vector3& p, const matrix4& m)
        {
            vector4 p2 = m * vector4(p[0], p[1], p[2], 1);
            return p2[2] / p2[3];
        }

        void z_trace_internal(image<real>* img, const matrix4& clip, const camera* cmr, const intersection* inter)
        {
            int W = img->get_width();
            int H = img->get_height();

            real iW = real(2.0) / W;
            real iH = real(2.0) / H;

            int total = W * H;
            double iTotal = 100.0 / total;
            int i = 0;
            for (int y = 0; y < H; y++)
            {
                for (int x = 0; x < W; x++)
                {

                    real xx = ((x + 0.5) - W * 0.5) * iW;
                    real yy = (H * 0.5 - (y + 0.5)) * iH;

                    ray r = cmr->shoot(xx, yy);

                    test_info info;
                    if (inter->test(&info, r, values::far()))
                    {
                        vector3 pos = r.origin() + info.get_distance() * r.direction();
                        real z = get_z(pos, clip);
                        if (0 <= z && z <= 1)
                        {
                            img->set(x, y, z);
                        }
                    }

                    i++;
                }
                print_progress("z_trace:%3.3f%%", i * iTotal);
            }
            print_progress("z_trace:%3.3f%%", 100.0);
        }
    }
}

namespace kaze
{

    void z_trace(image<real>* img, const matrix4& clip, const camera& cmr, const intersection& inter)
    {
        z_trace_internal(img, clip, &cmr, &inter);
    }

    void z_trace(image<real>* img, const matrix4& clip, const camera* cmr, const intersection* inter)
    {
        z_trace_internal(img, clip, cmr, inter);
    }

    void z_trace(image<real>& img, const matrix4& clip, const camera& cmr, const intersection& inter)
    {
        z_trace_internal(&img, clip, &cmr, &inter);
    }
}
