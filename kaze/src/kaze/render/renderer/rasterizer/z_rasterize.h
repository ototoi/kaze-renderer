#ifndef KAZE_Z_RASTERIZE_H
#define KAZE_Z_RASTERIZE_H

#include "types.h"
#include "image.hpp"
#include "mesh_saver.h"
#include "mesh_loader.h"

namespace kaze
{

    void z_rasterize(image<real>* img, const matrix4& clip, const vector3& a, const vector3& b, const vector3& c);
    void z_rasterize(image<real>& img, const matrix4& clip, const vector3& a, const vector3& b, const vector3& c);
}

#endif