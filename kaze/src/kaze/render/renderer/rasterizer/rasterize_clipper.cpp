#include "rasterize_clipper.h"

namespace kaze
{

    enum
    {
        KODE_LEFT = 1,
        KODE_RIGHT = 2,
        KODE_BOTTOM = 4,
        KODE_TOP = 8,
        KODE_NEAR = 16,
        KODE_FAR = 32
    };

    static inline int sgn(real x)
    {
        return (x < 0) ? 1 : 0;
    }

    static int outcode(const vector2& p)
    {
        int nRet = 0;
        nRet |= sgn(p[0]) << 0;
        nRet |= sgn(1 - p[0]) << 1;
        nRet |= sgn(p[1]) << 2;
        nRet |= sgn(1 - p[1]) << 3;

        return nRet;
    }

    static int outcode(const vector3& p)
    {
        int nRet = 0;
        nRet |= sgn(p[0]) << 0;
        nRet |= sgn(1 - p[0]) << 1;
        nRet |= sgn(p[1]) << 2;
        nRet |= sgn(1 - p[1]) << 3;
        nRet |= sgn(p[2]) << 4;
        nRet |= sgn(1 - p[2]) << 5;

        return nRet;
    }

    static int outcode(const vector4& p)
    {
        int nRet = 0;
        nRet |= sgn(p[0]) << 0;
        nRet |= sgn(p[3] - p[0]) << 1;
        nRet |= sgn(p[1]) << 2;
        nRet |= sgn(p[3] - p[1]) << 3;
        nRet |= sgn(p[2]) << 4;
        nRet |= sgn(p[3] - p[2]) << 5;

        return nRet;
    };

    /**
	 * KODE0 | KODE1 |
	 *     0 |     0 | all is visible.
	 *     0 |     1 | visible...  but p1 is outside.
	 *     1 |     0 | visible...  but p0 is outside.
	 *     1 |     1 | all isnot visible.
	 */

    static inline int todraw(int KODE0, int KODE1)
    {
        if ((KODE0 & KODE1))
            return 0;
        else
            return 1;
    }

    int clip3(const vector4& p0, const vector4& p1, const vector4& p2)
    {
        int KODE0 = outcode(p0);
        int KODE1 = outcode(p1);
        int KODE2 = outcode(p2);

        return (todraw(KODE0, KODE1) | todraw(KODE1, KODE2) | todraw(KODE2, KODE0));
    }

    static int clip_line_inside(vector2& p, const vector2& p0, const vector2& p1, int KODE)
    {
        static const real XMIN = 0;
        static const real XMAX = 1;
        static const real YMIN = 0;
        static const real YMAX = 1;

        real x = p[0];
        real y = p[1];

        if (KODE & KODE_LEFT)
        { //left
            y = p0[1] + (p1[1] - p0[1]) * (XMIN - p0[0]) / (p1[0] - p0[0]);
            x = 0;
        }
        else if (KODE & KODE_RIGHT)
        {
            y = p0[1] + (p1[1] - p0[1]) * (XMAX - p0[0]) / (p1[0] - p0[0]);
            x = 1;
        }
        else if (KODE & KODE_BOTTOM)
        {
            x = p0[0] + (p1[0] - p0[0]) * (YMIN - p0[1]) / (p1[1] - p0[1]);
            y = 0;
        }
        else
        {
            x = p0[0] + (p1[0] - p0[0]) * (YMAX - p0[1]) / (p1[1] - p0[1]);
            y = 1;
        }

        p[0] = x;
        p[1] = y;
        return outcode(p);
    }

    static int clip_line_inside(vector3& p, const vector3& p0, const vector3& p1, int KODE)
    {
        static const real XMIN = 0;
        static const real XMAX = 1;
        static const real YMIN = 0;
        static const real YMAX = 1;
        static const real ZMIN = 0;
        static const real ZMAX = 1;

        real x = p[0];
        real y = p[1];
        real z = p[2];

        if (KODE & KODE_LEFT)
        { //left
            y = p0[1] + (p1[1] - p0[1]) * (XMIN - p0[0]) / (p1[0] - p0[0]);
            z = p0[2] + (p1[2] - p0[2]) * (XMIN - p0[0]) / (p1[0] - p0[0]);
            x = XMIN;
        }
        else if (KODE & KODE_RIGHT)
        {
            y = p0[1] + (p1[1] - p0[1]) * (XMAX - p0[0]) / (p1[0] - p0[0]);
            z = p0[2] + (p1[2] - p0[2]) * (XMAX - p0[0]) / (p1[0] - p0[0]);
            x = XMAX;
        }
        else if (KODE & KODE_BOTTOM)
        {
            x = p0[0] + (p1[0] - p0[0]) * (YMIN - p0[1]) / (p1[1] - p0[1]);
            z = p0[2] + (p1[2] - p0[2]) * (YMIN - p0[1]) / (p1[1] - p0[1]);
            y = YMIN;
        }
        else if (KODE & KODE_TOP)
        {
            x = p0[0] + (p1[0] - p0[0]) * (YMAX - p0[1]) / (p1[1] - p0[1]);
            z = p0[2] + (p1[2] - p0[2]) * (YMAX - p0[1]) / (p1[1] - p0[1]);
            y = YMAX;
        }
        else if (KODE & KODE_NEAR)
        {
            x = p0[0] + (p1[0] - p0[0]) * (ZMIN - p0[2]) / (p1[2] - p0[2]);
            y = p0[1] + (p1[1] - p0[1]) * (ZMIN - p0[2]) / (p1[2] - p0[2]);
            z = ZMIN;
        }
        else
        {
            x = p0[0] + (p1[0] - p0[0]) * (ZMAX - p0[2]) / (p1[2] - p0[2]);
            y = p0[1] + (p1[1] - p0[1]) * (ZMAX - p0[2]) / (p1[2] - p0[2]);
            z = ZMAX;
        }

        p[0] = x;
        p[1] = y;
        p[2] = z;
        return outcode(p);
    }

    bool clip_line_01(vector2& p0, vector2& p1)
    {
        int KODE0;
        int KODE1;
        bool bRet = false;

        KODE0 = outcode(p0);
        KODE1 = outcode(p1);
        do
        {
            if (!(KODE0 | KODE1))
            { //all inside
                bRet = true;
                break; //
            }
            else if (KODE0 & KODE1)
            { //all noinside
                bRet = false;
                break;
            }
            else
            { // p0 or p1
                if (KODE0)
                {
                    KODE0 = clip_line_inside(p0, p0, p1, KODE0);
                }
                else
                {
                    KODE1 = clip_line_inside(p1, p0, p1, KODE1);
                }
            }
        } while (1);
        return bRet;
    }

    bool clip_line_01(vector3& p0, vector3& p1)
    {
        int KODE0;
        int KODE1;
        bool bRet = false;

        KODE0 = outcode(p0);
        KODE1 = outcode(p1);
        do
        {
            if (!(KODE0 | KODE1))
            { //all inside
                bRet = true;
                break; //
            }
            else if (KODE0 & KODE1)
            { //all noinside
                bRet = false;
                break;
            }
            else
            { // p0 or p1
                if (KODE0)
                {
                    KODE0 = clip_line_inside(p0, p0, p1, KODE0);
                }
                else
                {
                    KODE1 = clip_line_inside(p1, p0, p1, KODE1);
                }
            }
        } while (1);
        return bRet;
    }

    //------------------------------------------------------------

    static inline bool less4(real x0, real w0, real x1, real w1)
    {
        return x0 * w1 < x1 * w0;
    }
}