#ifndef KAZE_SIMPLE_TRACE_RENDERER_H
#define KAZE_SIMPLE_TRACE_RENDERER_H

#include "intersection.h"
#include "light.h"
#include "camera.h"
#include "count_ptr.hpp"
#include "render_frame.h"

namespace kaze
{

    class simple_trace_renderer
    {
    public:
        simple_trace_renderer();
        void add(const auto_count_ptr<intersection>& inter);
        void add(const auto_count_ptr<light>& l);
        void add(const auto_count_ptr<camera>& cmr);
        void add(const auto_count_ptr<render_frame>& rf);

    public:
        int render(int w, int h, const char* szBitmap);

    private:
        std::shared_ptr<intersection> inter_;
        std::shared_ptr<light> lt_;
        std::shared_ptr<camera> cmr_;
        std::shared_ptr<render_frame> rf_;
    };
}

#endif