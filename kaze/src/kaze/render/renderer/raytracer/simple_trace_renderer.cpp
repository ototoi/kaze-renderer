#include "simple_trace_renderer.h"

#include "render_region_distributor.h"
#include "auto_render_region_distributor.h"
#include "swirl_render_region_distributor.h"
#include "circular_render_region_distributor.h"
#include "render_frame.h"
#include "image.hpp"
#include "memory_image.hpp"

#include "camera.h"
#include "tracer.h"
#include "logger.h"
#include "timer.h"
#include "count_ptr.hpp"
#include "transformer.h"

#include "intersection.h"

#include "transform_matrix.h"
#include "parameter_map.h"
#include "transformed_intersection.h"

#include "shadered_intersection.h"
#include "bvh_composite_intersection.h"
#include "trace_shadower.h"
#include "shaded_trace_shadower.h"
#include "spot_light.h"
#include "exhibited_composite_light.h"
#include "light_integrator.h"
#include "ray_tracer.h"
#include "console_render_frame.h"
#include "rockenfield_render_frame.h"
#include "composite_render_frame.h"
#include "color_convert.h"

#include <fstream>

#include "pixel_sampler.h"
#include "grid_pixel_sampler.h"

#include "basic_collection.hpp"

#include "print_progress_render_frame.h"

#include "renderer_util.h"

#include "QMC.h"

#include <string>
#include <iostream>
#include <thread>

#if defined(_MSC_VER)
#if (_MSC_VER < 1400) //msc_ver < vc2005
#define KZSNPRINTF _snprintf
#else
#define KZSNPRINTF _snprintf_s
#endif
#else
#define KZSNPRINTF snprintf
#endif

typedef kaze::memory_image<kaze::vector4> real_image;

namespace
{

//using FNV1-a
#define FNV1_32_INIT ((Fnv32_t)0x811c9dc5)
#define FNV1_32A_INIT FNV1_32_INIT
/*
 * 32 bit magic FNV-1a prime
 */
#define FNV_32_PRIME ((Fnv32_t)0x01000193)

    /*
 * 32 bit FNV-0 hash type
 */
    typedef uint32_t Fnv32_t;

    /*
 * fnv_32a_buf - perform a 32 bit Fowler/Noll/Vo FNV-1a hash on a buffer
 *
 * input:
 *	buf	- start of buffer to hash
 *	len	- length of buffer in octets
 *	hval	- previous hash value or 0 if first call
 *
 * returns:
 *	32 bit hash as a static hash type
 *
 * NOTE: To use the recommended 32 bit FNV-1a hash, use FNV1_32A_INIT as the
 * 	 hval arg on the first call to either fnv_32a_buf() or fnv_32a_str().
 */
    Fnv32_t fnv_32a_buf(void* buf, size_t len, Fnv32_t hval)
    {
        unsigned char* bp = (unsigned char*)buf; /* start of buffer */
        unsigned char* be = bp + len;            /* beyond end of buffer */

        /*
     * FNV-1a hash each octet in the buffer
     */
        while (bp < be)
        {

            /* xor the bottom with the current octet */
            hval ^= (Fnv32_t)*bp++;

/* multiply by the 32 bit FNV magic prime mod 2^32 */
#if defined(NO_FNV_GCC_OPTIMIZATION)
            hval *= FNV_32_PRIME;
#else
            hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
#endif
        }

        /* return our new hash value */
        return hval;
    }
}

namespace kaze
{
    namespace
    {

        struct thread_st
        {
            render_region_distributor* rrg;
            render_frame* rf;
            real_image* img;
            const camera* c;
            const tracer* rt;
        };

        static int render_task(real_image& img, const tracer& rt, const camera& c, const render_region& rr, int WIDTH, int HEIGHT)
        {
            mediation air;
            double iW = 2.0 / WIDTH;
            double iH = 2.0 / HEIGHT;

            int w = rr.x1 - rr.x0;
            int h = rr.y1 - rr.y0;

            //delaunay_pixel_sampler g(1);
            grid_pixel_sampler g(2, 2);
            std::vector<pixel_sample> samples;

            g.get(samples, rr.x0, rr.y0, rr.x1, rr.y1);

            std::vector<int> totals(w * h);
            std::vector<int> hits(w * h);
            for (int i = 0; i < samples.size(); i++)
            {
                int x = samples[i].x;
                int y = samples[i].y;

                double xx = x + samples[i].dx;
                double yy = y + samples[i].dy;

                xx = xx * iW - 1;
                yy = -(yy * iH - 1);

                int dx = (int)x - rr.x0;
                int dy = (int)y - rr.y0;

                totals[dy * w + dx]++;
                ray r = c.shoot(xx, yy);

                real tm = samples[i].time;

                ray r2 = ray(r.origin(), r.direction(), tm);

                spectrum spc;
                if (rt.trace(&spc, r2, air))
                {
                    color3 col = convert(spc);
                    real a = coverage(spc);
                    img[y * WIDTH + x] += vector4(col[0], col[1], col[2], a);
                    hits[dy * w + dx]++;
                }
            }
            //-----------------------------------------------------
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {

                    int xx = rr.x0 + x;
                    int yy = rr.y0 + y;
                    vector4 c = img[yy * WIDTH + xx];
                    if (hits[y * w + x])
                    {
                        double cov = 1.0 / hits[y * w + x];
                        img[yy * WIDTH + xx] = vector4(c[0] * cov, c[1] * cov, c[2] * cov, c[3] / totals[y * w + x]);
                    }
                    else
                    {
                        img[yy * WIDTH + xx] = vector4(0, 0, 0, 0);
                    }
                }
            }

            return w * h;
        }
#ifdef _WIN32
        unsigned int __stdcall render_thread(void* p)
        {
#else
        void* render_thread(void* p)
        {
#endif
            thread_st* pst = reinterpret_cast<thread_st*>(p);

#ifdef __unix__
/*
	    cpu_set_t mask;
	    CPU_ZERO(&mask);
	    CPU_SET(pst->cpu_id,&mask);
	    sched_setaffinity(0,sizeof(cpu_set_t),&mask);
	    */
#endif
            render_region rr;
            while (pst->rrg->get(&rr))
            {
                int n = render_task(*(pst->img), *(pst->rt), *(pst->c), rr, pst->rrg->get_width(), pst->rrg->get_height());
                if (pst->rf) pst->rf->render(rr.x0, rr.y0, rr.x1, rr.y1, *(pst->img));
                std::this_thread::yield();
            }

            return 0;
        }

        const int N = 16;

        static int render_(int WIDTH, int HEIGHT, const std::string& strBitmap, const std::shared_ptr<intersection>& inter, const std::shared_ptr<camera>& cmra, const std::shared_ptr<light>& lt, const std::shared_ptr<render_frame>& rf)
        {
            timer t1;
            ray_tracer rt(inter, new light_integrator(lt));

            real_image img(WIDTH, HEIGHT);
            for (int y = 0; y < HEIGHT; y++)
            {
                for (int x = 0; x < WIDTH; x++)
                {
                    img.set(x, y, vector4(0, 0, 0, 0));
                }
            }
            composite_render_frame crf;
            if (rf.get())
            {
                crf.add(rf);
            }
            crf.add(new print_progress_render_frame(WIDTH, HEIGHT));
            crf.begin();

            int nCPU = renderer_util::get_cpu_number();
            if (nCPU >= N) nCPU = N;

            //auto_render_region_distributor rrg(WIDTH,HEIGHT,nCPU);
            //swirl_render_region_distributor rrg(WIDTH,HEIGHT);
            circular_render_region_distributor rrg(WIDTH, HEIGHT);
            kaze::mediation air; //
            {
                t1.start();

                thread_st st;
                st.c = cmra.get();
                st.img = &img;
                st.rrg = &rrg;
                st.rt = &rt;
                st.rf = &crf;
#ifdef _WIN32
                HANDLE hThreads[N];
                for (int i = 0; i < nCPU; i++)
                {
                    hThreads[i] = (HANDLE)::_beginthreadex(NULL, 0, render_thread, &st, 0, NULL);
                }

                ::WaitForMultipleObjects(nCPU, hThreads, TRUE, INFINITE);
                for (int i = 0; i < nCPU; i++)
                {
                    ::CloseHandle(hThreads[i]);
                }
#else
                pthread_t hThreads[N];
                for (int i = 0; i < N; i++)
                {
                    pthread_create(hThreads + i, NULL, render_thread, &st);
                }
                for (int i = 0; i < N; i++)
                {
                    pthread_join(hThreads[i], NULL);
                }
#endif
                t1.end();
                kaze::print_log("rendering time %d ms\n", t1.msec());
            }

            //---------------------------------------------

            crf.end();
            return 0;
        }
    }
    simple_trace_renderer::simple_trace_renderer()
    {
        inter_.reset(new bvh_composite_intersection());
        lt_.reset(new exhibited_composite_light());
    }

    void simple_trace_renderer::add(const auto_count_ptr<intersection>& inter)
    {
        dynamic_cast<bvh_composite_intersection*>(inter_.get())->add(inter);
    }

    void simple_trace_renderer::add(const auto_count_ptr<light>& l)
    {
        dynamic_cast<exhibited_composite_light*>(lt_.get())->add(l);
    }

    void simple_trace_renderer::add(const auto_count_ptr<camera>& cmr)
    {
        cmr_ = cmr;
    }

    void simple_trace_renderer::add(const auto_count_ptr<render_frame>& rf)
    {
        rf_ = rf;
    }

    int simple_trace_renderer::render(int w, int h, const char* szBitmap)
    {
        bvh_composite_intersection* bvh = dynamic_cast<bvh_composite_intersection*>(inter_.get());
        bvh->construct();

        if (cmr_.get())
        {
            return render_(w, h, szBitmap, inter_, cmr_, lt_, rf_);
        }
        return 0;
    }
}
