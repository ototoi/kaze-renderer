#include "renderer_util.h"

#ifdef _WIN32
#include <windows.h>
#endif

#if defined(__unix__) || defined(__APPLE__)
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#endif

#include <mutex>

namespace kaze
{
    static int GetCPUNumber()
    {
#if defined(_SC_NPROCESSORS_ONLN)
        return sysconf(_SC_NPROCESSORS_ONLN);
#elif defined(_SC_NPROCESSORS_CONF)
        return sysconf(_SC_NPROCESSORS_CONF);
#elif defined(__APPLE__) || defined(FREEBSD) || defined(NETBSD)
        size_t nproc = 1;
        int mib[2];
        size_t len;

        mib[0] = CTL_HW;
        mib[1] = HW_NCPU; //HW_AVAILCPU
        len = sizeof(nproc);
        sysctl(mib, 2, &nproc, &len, NULL, 0);
        return nproc;
#elif defined(_WIN32)
        SYSTEM_INFO info;
        ::GetSystemInfo(&info);
        return info.dwNumberOfProcessors;
#else
        return 4;
#endif
    }

    int renderer_util::get_cpu_number()
    {
        return GetCPUNumber();
    }
}