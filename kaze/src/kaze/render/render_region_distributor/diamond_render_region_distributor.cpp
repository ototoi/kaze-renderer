#include "diamond_render_region_distributor.h"

namespace kaze
{
    typedef diamond_render_region_distributor::pos pos;

    static void DiamondOrder(int n, int w, int h, std::vector<pos>& array)
    {

        int cx = w / 2;
        int cy = h / 2;

        int nx, ny;

        nx = cx - n;
        ny = cy;
        if (0 <= nx && nx < w && 0 <= ny && ny < h)
        {
            pos tile;
            tile.x = nx;
            tile.y = ny;
            array.push_back(tile);
        }

        while (nx < cx)
        {
            nx++;
            ny--;
            if (0 <= nx && nx < w && 0 <= ny && ny < h)
            {
                pos tile;
                tile.x = nx;
                tile.y = ny;
                array.push_back(tile);
            }
        }

        while (ny < cy)
        {
            nx++;
            ny++;
            if (0 <= nx && nx < w && 0 <= ny && ny < h)
            {
                pos tile;
                tile.x = nx;
                tile.y = ny;
                array.push_back(tile);
            }
        }

        while (nx > cx)
        {
            nx--;
            ny++;
            if (0 <= nx && nx < w && 0 <= ny && ny < h)
            {
                pos tile;
                tile.x = nx;
                tile.y = ny;
                array.push_back(tile);
            }
        }

        while (ny > cy + 1)
        {
            nx--;
            ny--;
            if (0 <= nx && nx < w && 0 <= ny && ny < h)
            {
                pos tile;
                tile.x = nx;
                tile.y = ny;
                array.push_back(tile);
            }
        }
    }

    //----------------------------------------------
    diamond_render_region_distributor::diamond_render_region_distributor(int w, int h)
        : w_(w), h_(h)
    {
        create_map();
        i_ = 0;
    }

    bool diamond_render_region_distributor::get_internal(render_region* reg)
    {
        int i = i_;
        if (i < (int)map_.size())
        {
            int x = map_[i].x;
            int y = map_[i].y;
            int x0 = x * BSZ;
            int y0 = y * BSZ;
            int x1 = x0 + BSZ;
            int y1 = y0 + BSZ;
            if (w_ < x1) x1 = w_;
            if (h_ < y1) y1 = h_;

            reg->x0 = x0;
            reg->y0 = y0;
            reg->x1 = x1;
            reg->y1 = y1;

            i_++;
            return true;
        }
        else
        {
            return false;
        }
    }

    void diamond_render_region_distributor::reset_internal()
    {
        i_ = 0;
    }

    void diamond_render_region_distributor::create_map()
    {
        int w = w_;
        int h = h_;
        int cw = w / BSZ;
        int ch = h / BSZ;
        if ((w % BSZ) != 0) cw++;
        if ((h % BSZ) != 0) ch++;

        std::vector<pos> array;
        pos dtile;
        dtile.x = 0;
        dtile.y = 0;
        array.push_back(dtile);
        for (int i = 0; !array.empty(); i++)
        {
            array.clear();
            DiamondOrder(i, cw, ch, array);
            for (int j = 0; j < array.size(); j++)
            {
                map_.push_back(array.at(j));
            }
        }
    }
}
