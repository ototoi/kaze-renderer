#ifndef KAZE_TILE_RENDER_REGION_DISTRBUTOR_H
#define KAZE_TILE_RENDER_REGION_DISTRBUTOR_H

#include "render_region_distributor.h"
#include "locked_render_region_distributor.hpp"

namespace kaze
{

    class tile_render_region_distributor : public locked_render_region_distributor<tile_render_region_distributor>
    {
    public:
        tile_render_region_distributor(int w, int h);
        void reset_internal();
        bool get_internal(render_region* reg);

        int get_width() const { return w_; }
        int get_height() const { return h_; }

    protected:
        int w_;
        int h_;
        int bw_;
        int bh_;
        int x_;
        int y_;
    };
}

#endif