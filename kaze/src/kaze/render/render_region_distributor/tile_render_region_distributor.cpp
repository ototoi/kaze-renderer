#include "tile_render_region_distributor.h"

namespace kaze
{

    tile_render_region_distributor::tile_render_region_distributor(int w, int h) : w_(w), h_(h)
    {
        int bw = w / BSZ;
        int bh = h / BSZ;
        if (w % BSZ) bw++;
        if (h % BSZ) bh++;
        bw_ = bw;
        bh_ = bh;
        x_ = 0;
        y_ = 0;
    }

    void tile_render_region_distributor::reset_internal()
    {
        x_ = 0;
        y_ = 0;
    }

    bool tile_render_region_distributor::get_internal(render_region* reg)
    {
        int idx = y_ * bw_ + x_;
        if (idx < bh_ * bw_)
        {
            int x0 = x_ * BSZ;
            int y0 = y_ * BSZ;
            int x1 = x0 + BSZ;
            int y1 = y0 + BSZ;
            if (w_ < x1) x1 = w_;
            if (h_ < y1) y1 = h_;

            reg->x0 = x0;
            reg->y0 = y0;
            reg->x1 = x1;
            reg->y1 = y1;

            int dx = x_ + 1;
            if (bw_ <= dx)
            {
                x_ = 0;
                y_++;
            }
            else
            {
                x_ = dx;
            }

            return true;
        }
        else
        {
            return false;
        }
    }
}
