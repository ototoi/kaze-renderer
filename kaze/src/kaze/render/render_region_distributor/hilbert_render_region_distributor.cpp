#include "hilbert_render_region_distributor.h"

#include <vector>

namespace kaze
{
    namespace
    {

        typedef hilbert_render_region_distributor::pos pos;

        class hilbert
        {
        public:
            hilbert(int order)
            {
                x = 0;
                y = 0;
                o = order;
            }

            void step(int dir, std::vector<pos>& p)
            {
                switch (dir & 3)
                {
                case 0:
                    x = x + 1;
                    break;
                case 1:
                    y = y + 1;
                    break;
                case 2:
                    x = x - 1;
                    break;
                case 3:
                    y = y - 1;
                    break;
                }
                pos tp;
                tp.x = x;
                tp.y = y;
                p.push_back(tp);
            }

            void get(int dir, int rot, int order, std::vector<pos>& p)
            {
                if (order == 0) return;
                dir = dir + rot;
                get(dir, -rot, order - 1, p);
                step(dir, p);
                dir = dir - rot;
                get(dir, rot, order - 1, p);
                step(dir, p);
                get(dir, rot, order - 1, p);
                dir = dir - rot;
                step(dir, p);
                get(dir, -rot, order - 1, p);
            }

        public:
            void get(std::vector<pos>& p)
            {
                x = -1;
                y = 0;
                step(0, p);
                get(0, 1, o, p);
            }

        private:
            int x;
            int y;
            int o;
        };
    }

    void hilbert_render_region_distributor::create_map()
    {
        int w = w_;
        int h = h_;
        int bw = w / BSZ;
        int bh = h / BSZ;
        if (w % BSZ) bw++;
        if (h % BSZ) bh++;

        int bs = bw < bh ? bh : bw; //max
        int order = 0;
        {
            int n = 1;
            while (n < bs)
            {
                n <<= 1;
                order++;
            }
            bs = n;
        }

        std::vector<pos> p;
        hilbert hil(order);
        p.reserve(bw * bh);
        hil.get(p);
        map_.reserve(bw * bh);
        for (size_t i = 0; i < p.size(); i++)
        {
            if (p[i].x < bw && p[i].y < bh)
            {
                map_.push_back(p[i]);
            }
        }
    }

    hilbert_render_region_distributor::hilbert_render_region_distributor(int w, int h)
        : w_(w), h_(h)
    {
        create_map();
        i_ = 0;
    }

    bool hilbert_render_region_distributor::get_internal(render_region* reg)
    {
        int i = i_;
        if (i < (int)map_.size())
        {
            int x = map_[i].x;
            int y = map_[i].y;
            int x0 = x * BSZ;
            int y0 = y * BSZ;
            int x1 = x0 + BSZ;
            int y1 = y0 + BSZ;
            if (w_ < x1) x1 = w_;
            if (h_ < y1) y1 = h_;

            reg->x0 = x0;
            reg->y0 = y0;
            reg->x1 = x1;
            reg->y1 = y1;

            i_++;
            return true;
        }
        else
        {
            return false;
        }
    }

    void hilbert_render_region_distributor::reset_internal()
    {
        i_ = 0;
    }
}
