#ifndef KAZE_AUTO_RENDER_REGION_DISTRIBUTOR_H
#define KAZE_AUTO_RENDER_REGION_DISTRIBUTOR_H

#include "render_region_distributor.h"

namespace kaze
{

    class auto_render_region_distributor_imp;

    class auto_render_region_distributor : public render_region_distributor
    {
    public:
        auto_render_region_distributor(int w, int h);
        auto_render_region_distributor(int w, int h, int nThread);
        ~auto_render_region_distributor();
        bool get(render_region* reg);
        void reset();
        int get_width() const;
        int get_height() const;

    private:
        auto_render_region_distributor_imp* imp_;
    };
}

#endif
