#include "auto_render_region_distributor.h"
#include "types.h"
#include "tile_render_region_distributor.h"
#include "hilbert_render_region_distributor.h"

#include <vector>
#include <algorithm>
#include <iostream>
#include <mutex>


#ifdef _WIN32
#include <windows.h>
#else
#include <pthread.h>
#include <unistd.h>
#endif

#include <time.h>

namespace kaze
{
    namespace
    {
        int GetCPUNumber()
        {
#if defined(_SC_NPROCESSORS_ONLN)
            return sysconf(_SC_NPROCESSORS_ONLN);
#elif defined(_SC_NPROCESSORS_CONF)
            return sysconf(_SC_NPROCESSORS_CONF);
#elif defined(__APPLE__) || defined(FREEBSD) || defined(NETBSD)
            size_t nproc = 1;
            int mib[2];
            size_t len;

            mib[0] = CTL_HW;
            mib[1] = HW_NCPU; //HW_AVAILCPU
            len = sizeof(nproc);
            sysctl(mib, 2, &nproc, &len, NULL, 0);
            return nproc;
#elif defined(_WIN32)
            SYSTEM_INFO info;
            ::GetSystemInfo(&info);
            return info.dwNumberOfProcessors;
#else
            return 4;
#endif
        }

#ifdef _WIN32
        typedef DWORD thread_id_t;
        inline thread_id_t get_current_thread_id()
        {
            return ::GetCurrentThreadId();
        }
#endif

#if defined(__unix__) || defined(__APPLE__)
        typedef unsigned long thread_id_t;
        inline thread_id_t get_current_thread_id()
        {
            return (thread_id_t)pthread_self();
        }
#endif

//using FNV1-a
#define FNV1_32_INIT ((Fnv32_t)0x811c9dc5)
#define FNV1_32A_INIT FNV1_32_INIT
/*
 * 32 bit magic FNV-1a prime
 */
#define FNV_32_PRIME ((Fnv32_t)0x01000193)

        /*
 * 32 bit FNV-0 hash type
 */
        typedef uint32_t Fnv32_t;

        /*
 * fnv_32a_buf - perform a 32 bit Fowler/Noll/Vo FNV-1a hash on a buffer
 *
 * input:
 *	buf	- start of buffer to hash
 *	len	- length of buffer in octets
 *	hval	- previous hash value or 0 if first call
 *
 * returns:
 *	32 bit hash as a static hash type
 *
 * NOTE: To use the recommended 32 bit FNV-1a hash, use FNV1_32A_INIT as the
 * 	 hval arg on the first call to either fnv_32a_buf() or fnv_32a_str().
 */
        Fnv32_t fnv_32a_buf(void* buf, size_t len, Fnv32_t hval)
        {
            unsigned char* bp = (unsigned char*)buf; /* start of buffer */
            unsigned char* be = bp + len;            /* beyond end of buffer */

            /*
     * FNV-1a hash each octet in the buffer
     */
            while (bp < be)
            {

                /* xor the bottom with the current octet */
                hval ^= (Fnv32_t)*bp++;

/* multiply by the 32 bit FNV magic prime mod 2^32 */
#if defined(NO_FNV_GCC_OPTIMIZATION)
                hval *= FNV_32_PRIME;
#else
                hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
#endif
            }

            /* return our new hash value */
            return hval;
        }

        static uint32_t UniqueThreadID()
        {
            uint32_t val = get_current_thread_id();
            return fnv_32a_buf(&val, 4, FNV1_32A_INIT);
        }

        class render_region_handler
        {
        public:
            render_region_handler(int x0, int y0, int x1, int y1);
            ~render_region_handler();
            bool get(render_region* reg);
            void reset();

        private:
            int dx_;
            int dy_;
            render_region_distributor* rr_;
        };

        render_region_handler::render_region_handler(int x0, int y0, int x1, int y1)
            : dx_(x0), dy_(y0)
        {
            //tile_
            rr_ = new hilbert_render_region_distributor(x1 - x0, y1 - y0);
        }
        render_region_handler::~render_region_handler()
        {
            delete rr_;
        }

        bool render_region_handler::get(render_region* reg)
        {
            if (rr_->get(reg))
            {
                int dx = dx_;
                int dy = dy_;
                reg->x0 += dx;
                reg->x1 += dx;
                reg->y0 += dy;
                reg->y1 += dy;
                return true;
            }
            else
            {
                return false;
            }
        }

        void render_region_handler::reset()
        {
            rr_->reset();
        }
    }

    class auto_render_region_distributor_imp
    {
    public:
        auto_render_region_distributor_imp(int w, int h);
        auto_render_region_distributor_imp(int w, int h, int nCPU);
        ~auto_render_region_distributor_imp();

        bool get(render_region* reg);
        void reset();
        int get_width() const { return w_; }
        int get_height() const { return h_; }
    private:
        void initialize(int nCPU);

        int w_;
        int h_;
        std::vector<render_region_handler*> vr_;
        std::vector<std::vector<int> > ord_;
        std::vector<int> cur_;
        int rnd_[256];
    };

    static int get_div(int w, int b)
    {
        if (w <= b) return 1;
        int n = w / b;
        if (w % b) n++;
        return n;
    }

    auto_render_region_distributor_imp::auto_render_region_distributor_imp(int w, int h)
        : w_(w), h_(h)
    {
        int nCPU = GetCPUNumber();
        initialize(nCPU);
    }

    auto_render_region_distributor_imp::auto_render_region_distributor_imp(int w, int h, int nCPU)
        : w_(w), h_(h)
    {
        initialize(nCPU);
    }

    void auto_render_region_distributor_imp::initialize(int nCPU)
    {
        int BSZ = render_region_distributor::BSZ;

        int w = w_;
        int h = h_;

        int gk = 8;

        //tile size
        int nw = get_div(w, BSZ * gk);
        int nh = get_div(h, BSZ * gk);

        while ((nw * nh) < nCPU && gk > 1)
        {
            gk >>= 1;
            nw = get_div(w, BSZ * gk);
            nh = get_div(h, BSZ * gk);
        }

        int ww = BSZ * gk;
        int wh = BSZ * gk;

        int sz = nw * nh;

        int delta = sz / nCPU;
        if (delta == 0) delta++;

        ord_.resize(sz);
        for (int i = 0; i < sz; i++)
        {
            ord_[i].resize(sz);
            int base = delta * i;
            for (int j = 0; j < sz; j++)
            {
                ord_[i][j] = (base + j) % sz;
            }
        }
        srand((unsigned int)time(NULL));
        for (int i = 0; i < 256; i++)
        {
            rnd_[i] = rand() % sz;
        }

        vr_.resize(sz);

        for (int y = 0; y < nh; y++)
        {
            for (int x = 0; x < nw; x++)
            {
                int x0 = x * ww;
                int y0 = y * wh;
                int x1 = x0 + ww;
                int y1 = y0 + wh;
                if (x1 > w) x1 = w;
                if (y1 > h) y1 = h;
                vr_[y * nw + x] = new render_region_handler(x0, y0, x1, y1);
            }
        }

        //--------------------------------------------------------------------------------
        cur_.resize(sz);
        for (int i = 0; i < sz; i++)
        {
            cur_[i] = 0;
        }
    }

    auto_render_region_distributor_imp::~auto_render_region_distributor_imp()
    {
        size_t sz = vr_.size();
        for (size_t i = 0; i < sz; i++)
        {
            delete vr_[i];
        }
    }

    bool auto_render_region_distributor_imp::get(render_region* reg)
    {
        int sz = (int)vr_.size();
        //int n1 = get_current_thread_id();
        int n = rnd_[UniqueThreadID() & 255];

        int start = cur_[n];

        for (int i = start; i < sz; i++)
        {
            int o = ord_[n][i];
            if (vr_[o]->get(reg))
            {
                return true;
            }
            else
            {
                cur_[n] = i + 1;
            }
        }

        return false;
    }

    void auto_render_region_distributor_imp::reset()
    {
        size_t sz = vr_.size();
        for (size_t i = 0; i < sz; i++)
        {
            vr_[i]->reset();

            cur_[i] = 0;
        }
    }

    //-------------------------------------------------------------------------

    auto_render_region_distributor::auto_render_region_distributor(int w, int h)
    {
        imp_ = new auto_render_region_distributor_imp(w, h);
    }

    auto_render_region_distributor::auto_render_region_distributor(int w, int h, int nThread)
    {
        imp_ = new auto_render_region_distributor_imp(w, h, nThread);
    }

    auto_render_region_distributor::~auto_render_region_distributor()
    {
        delete imp_;
    }

    bool auto_render_region_distributor::get(render_region* reg)
    {
        return imp_->get(reg);
    }

    void auto_render_region_distributor::reset()
    {
        imp_->reset();
    }

    int auto_render_region_distributor::get_width() const
    {
        return imp_->get_width();
    }

    int auto_render_region_distributor::get_height() const
    {
        return imp_->get_height();
    }
}
