#ifndef KAZE_RENDER_SUBDIVIDE_CHECKER_H
#define KAZE_RENDER_SUBDIVIDE_CHECKER_H

#include "types.h"
#include "image.hpp"

namespace kaze
{

    class render_subdivide_checker
    {
    public:
        virtual ~render_subdivide_checker() {}
        virtual void get(image<bool>* img, int x0, int x1, int y0, int y1) const = 0;
    };
}

#endif