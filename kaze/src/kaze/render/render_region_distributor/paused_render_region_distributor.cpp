#include "paused_render_region_distributor.h"

#include <cassert>
#include <cstdlib>
#include <cstring>

namespace kaze
{

    typedef paused_render_region_distributor this_type;
    typedef this_type::container_type container_type;
    typedef this_type::iter iter;
    typedef this_type::citer citer;

    static bool is_range(int a, int x, int b)
    {
        return (a <= x) && (x < b);
    }

    paused_render_region_distributor::paused_render_region_distributor(const auto_count_ptr<render_region_distributor>& rr)
        : rr_(rr)
    {
        ; //
    }

    paused_render_region_distributor::~paused_render_region_distributor()
    {
        ; //
    }

    bool paused_render_region_distributor::get(render_region* reg)
    {
        while (rr_->get(reg))
        {
            if (!test(reg)) continue;
            return true;
        }
        return false;
    }

    void paused_render_region_distributor::reset()
    {
        rr_->reset();
    }

    int paused_render_region_distributor::get_width() const
    {
        return rr_->get_width();
    }

    int paused_render_region_distributor::get_height() const
    {
        return rr_->get_height();
    }

    //------------------------------------------------------------------------

    static bool is_equal_x(const render_region& a, const render_region& b)
    {
        if ((a.x0 == b.x0) && (a.x1 == b.x1)) return true;
        return false;
    }
    static bool is_equal_y(const render_region& a, const render_region& b)
    {
        if ((a.y0 == b.y0) && (a.y1 == b.y1)) return true;
        return false;
    }

    static bool is_contain(const render_region& a, const render_region& b)
    {
        if (is_range(a.x0, b.x0, a.x1) && is_range(a.x0, b.x1 - 1, a.x1))
        {
            if (is_range(a.y0, b.y0, a.y1) && is_range(a.y0, b.y1 - 1, a.y1)) return true;
        }
        return false;
    }

    static bool be_merge(const render_region& a, const render_region& b)
    {
        //assert(0);//Now, implement not yet.
        if (is_equal_x(a, b))
        {
            if (is_range(a.x0, b.x0, a.x1 + 1)) return true;
            if (is_range(a.x0, b.x1, a.x1 + 1)) return true;
        }
        if (is_equal_y(a, b))
        {
            if (is_range(a.y0, b.y0, a.y1 + 1)) return true;
            if (is_range(a.y0, b.y1, a.y1 + 1)) return true;
        }

        if (is_contain(a, b)) return true;
        if (is_contain(b, a)) return true;

        return false;
    }

    static render_region merge_region(const render_region& a, const render_region& b)
    {
        render_region t;
        t.x0 = std::min<int>(a.x0, b.x0);
        t.y0 = std::min<int>(a.y0, b.y0);
        t.x1 = std::max<int>(a.x1, b.x1);
        t.y1 = std::max<int>(a.y1, b.y1);
        return t;
    }

    void paused_render_region_distributor::add_region(const render_region& r)
    {
        render_region v = r;

        iter i = l_.begin();
        while (i != l_.end())
        {
            render_region k = *i;
            if (be_merge(k, v))
            {
                v = merge_region(k, v);
                l_.erase(i++);
            }
            else
            {
                i++;
            }
        }
        l_.push_back(v);
    }

    //
    void paused_render_region_distributor::reset_region()
    {
        l_.clear();
    }

    static bool equal(const render_region& a, const render_region& b)
    {
        return (memcmp(&a, &b, sizeof(render_region)) == 0);
    }

    bool paused_render_region_distributor::test(render_region* reg) const
    {
        int x0 = reg->x0;
        int x1 = reg->x1;
        int y0 = reg->y0;
        int y1 = reg->y1;

        for (citer i = l_.begin(); i != l_.end(); i++)
        {
            render_region t = *i;
            if (equal(*reg, t)) return false;

            bool bCuts[4] = {false, false, false, false};

            if (is_range(t.x0, x0, t.x1)) bCuts[0] = true;
            if (is_range(t.x0, x1 - 1, t.x1)) bCuts[1] = true;
            if (is_range(t.y0, y0, t.y1)) bCuts[2] = true;
            if (is_range(t.y0, y1 - 1, t.y1)) bCuts[3] = true;

            if ((bCuts[0] || bCuts[1]) && (bCuts[2] || bCuts[3]))
            {
                if (bCuts[0]) x0 = t.x1;
                if (bCuts[1]) x1 = t.x0;
                if (bCuts[2]) y0 = t.y1;
                if (bCuts[3]) y1 = t.y0;
            }
        }

        int lx = x1 - x0;
        int ly = y1 - y0;
        if ((lx >= 2) && (ly >= 2))
        {
            reg->x0 = x0;
            reg->x1 = y0;
            reg->y0 = x1;
            reg->y1 = y1;
            return true;
        }
        else
        {
            return false;
        }
    }

    void paused_render_region_distributor::sort()
    {
        //
    }
}
