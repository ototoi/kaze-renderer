#include "partial_render_region_distributor.h"
#include "auto_render_region_distributor.h"

#include <cassert>

namespace kaze
{

    partial_render_region_distributor::partial_render_region_distributor(const auto_count_ptr<render_region_distributor>& rr, int dx, int dy, int w, int h)
        : rr_(rr), dx_(dx), dy_(dy), w_(w), h_(h)
    {
        assert(dx < w);
        assert(dy < h);
    }

    partial_render_region_distributor::partial_render_region_distributor(int x0, int y0, int x1, int y1, int w, int h)
        : dx_(x0), dy_(y0), w_(w), h_(h)
    {
        assert(x0 < w);
        assert(y0 < h);

        int cw = x1 - x0;
        int ch = y1 - y0;

        assert(0 < cw);
        assert(0 < ch);

        rr_.reset(new auto_render_region_distributor(cw, ch));
    }

    bool partial_render_region_distributor::get(render_region* reg)
    {
        int dx = dx_;
        int dy = dy_;
        int w = w_;
        int h = h_;

        while (rr_->get(reg))
        {
            int x0 = reg->x0 + dx;
            int y0 = reg->y0 + dy;
            int x1 = reg->x1 + dx;
            int y1 = reg->y1 + dy;

            if (w <= x0) continue;
            if (h <= y0) continue;

            if (w <= x1) x1 = w;
            if (h <= y1) y1 = h;

            reg->x0 = x0;
            reg->y0 = y0;
            reg->x1 = x1;
            reg->y1 = y1;

            return true;
        }
        return false;
    }

    void partial_render_region_distributor::reset()
    {
        rr_->reset();
    }

    int partial_render_region_distributor::get_width() const
    {
        return w_;
    }

    int partial_render_region_distributor::get_height() const
    {
        return h_;
    }
}
