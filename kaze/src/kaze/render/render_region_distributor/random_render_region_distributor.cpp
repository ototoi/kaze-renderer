#include "random_render_region_distributor.h"
#include <algorithm> //random_shuffle

namespace kaze
{

    random_render_region_distributor::random_render_region_distributor(int w, int h)
        : w_(w), h_(h)
    {
        i_ = 0;
        create_map();
    }

    bool random_render_region_distributor::get_internal(render_region* reg)
    {
        int i = i_;
        if (i < (int)map_.size())
        {
            int x = map_[i].x;
            int y = map_[i].y;
            int x0 = x * BSZ;
            int y0 = y * BSZ;
            int x1 = x0 + BSZ;
            int y1 = y0 + BSZ;
            if (w_ < x1) x1 = w_;
            if (h_ < y1) y1 = h_;

            reg->x0 = x0;
            reg->y0 = y0;
            reg->x1 = x1;
            reg->y1 = y1;

            i_++;
            return true;
        }
        else
        {
            return false;
        }
    }

    void random_render_region_distributor::reset_internal()
    {
        i_ = 0;
    }

    void random_render_region_distributor::create_map()
    {
        int w = w_;
        int h = h_;
        int bw = w / BSZ;
        int bh = h / BSZ;
        if (w % BSZ) bw++;
        if (h % BSZ) bh++;

        map_.resize(bw * bh);
        int i = 0;
        for (int y = 0; y < bh; y++)
        {
            for (int x = 0; x < bw; x++)
            {
                map_[i].x = x;
                map_[i].y = y;
                i++;
            }
        }

        std::random_shuffle(map_.begin(), map_.end());
    }
}