#ifndef KAZE_PAUSED_RENDER_REGION_DISTRIBUTOR_H
#define KAZE_PAUSED_RENDER_REGION_DISTRIBUTOR_H

#include "render_region_distributor.h"
#include "count_ptr.hpp"
#include <list>

namespace kaze
{

    class paused_render_region_distributor : public render_region_distributor
    {
    public:
        typedef std::list<render_region> container_type;
        typedef container_type::iterator iter;
        typedef container_type::const_iterator citer;

    public:
        paused_render_region_distributor(const auto_count_ptr<render_region_distributor>& rr);
        ~paused_render_region_distributor();

    public:
        bool get(render_region* reg);
        void reset();
        int get_width() const;
        int get_height() const;

    public:
        void add_region(const render_region& reg);
        void reset_region();

    protected:
        bool test(render_region* reg) const;
        void sort();

    protected:
        std::list<render_region> l_;
        std::shared_ptr<render_region_distributor> rr_;
    };
}

#endif
