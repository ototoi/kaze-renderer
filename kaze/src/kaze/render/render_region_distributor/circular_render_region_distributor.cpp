#include "circular_render_region_distributor.h"
#include <cmath>
#include <algorithm>

namespace kaze
{
    typedef circular_render_region_distributor::pos pos;

    struct Posk
    {
        float fx;
        float fy;
    };

    struct Sorter
    {
        Sorter(float x, float y) : cx(x), cy(y) {}
        bool operator()(const pos& a, const pos& b) const
        {
            float lax = a.x - cx + 0.5f;
            float lay = a.y - cy + 0.5f;

            float lbx = b.x - cx + 0.5f;
            float lby = b.y - cy + 0.5f;

            float la = lax * lax + lay * lay;
            float lb = lbx * lbx + lby * lby;

            if (la != lb)
            {
                return la < lb;
            }
            else
            {
                float aa = atan2f(lay, lax);
                float ab = atan2f(lby, lbx);
                return aa < ab;
            }
        }

        float cx;
        float cy;
    };

    static void CircularOrder(int w, int h, std::vector<pos>& array)
    {
        array.resize(w * h);
        for (int y = 0; y < h; y++)
        {
            for (int x = 0; x < w; x++)
            {
                array[y * w + x].x = x;
                array[y * w + x].y = y;
            }
        }

        float cx = w * 0.5f;
        float cy = h * 0.5f;

        std::sort(array.begin(), array.end(), Sorter(cx, cy));
    }

    //----------------------------------------------
    circular_render_region_distributor::circular_render_region_distributor(int w, int h)
        : w_(w), h_(h)
    {
        create_map();
        i_ = 0;
    }

    bool circular_render_region_distributor::get_internal(render_region* reg)
    {
        int BSZ = render_region_distributor::BSZ;
        int i = i_;
        if (i < (int)map_.size())
        {
            int x = map_[i].x;
            int y = map_[i].y;
            int x0 = x * BSZ;
            int y0 = y * BSZ;
            int x1 = x0 + BSZ;
            int y1 = y0 + BSZ;
            if (w_ < x1) x1 = w_;
            if (h_ < y1) y1 = h_;

            reg->x0 = x0;
            reg->y0 = y0;
            reg->x1 = x1;
            reg->y1 = y1;

            i_++;
            return true;
        }
        else
        {
            return false;
        }
    }

    void circular_render_region_distributor::reset_internal()
    {
        i_ = 0;
    }

    void circular_render_region_distributor::create_map()
    {
        int BSZ = render_region_distributor::BSZ;
        int w = w_;
        int h = h_;
        int cw = w / BSZ;
        int ch = h / BSZ;
        if ((w % BSZ) != 0) cw++;
        if ((h % BSZ) != 0) ch++;
        CircularOrder(cw, ch, map_);
    }
}
