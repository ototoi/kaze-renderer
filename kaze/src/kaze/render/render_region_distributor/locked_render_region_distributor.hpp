#ifndef KAZE_LOCKED_RENDER_REGION_DISTRIBUTOR_HPP
#define KAZE_LOCKED_RENDER_REGION_DISTRIBUTOR_HPP

#include "render_region_distributor.h"
#include <mutex>

namespace kaze
{

    template <class BASE>
    class locked_render_region_distributor : public render_region_distributor
    {
    public:
        locked_render_region_distributor() : bEqv_(true) {}
        bool get(render_region* reg)
        {
            if (bEqv_)
            {
                std::lock_guard<std::mutex> lck(mtx_);
                if (static_cast<BASE*>(this)->get_internal(reg))
                {
                    return true;
                }
                else
                {
                    bEqv_ = false;
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        void reset()
        {
            std::lock_guard<std::mutex> lck(mtx_);
            static_cast<BASE*>(this)->reset_internal();
            bEqv_ = true;
        }

    private:
        bool bEqv_;
        mutable std::mutex mtx_;
    };
}

#endif