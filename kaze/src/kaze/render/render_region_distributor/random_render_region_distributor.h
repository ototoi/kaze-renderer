#ifndef KAZE_RANDOM_RENDER_REGION_DISTRIBUTOR_H
#define KAZE_RANDOM_RENDER_REGION_DISTRIBUTOR_H

#include "render_region_distributor.h"
#include "locked_render_region_distributor.hpp"
#include <vector>

namespace kaze
{

    class random_render_region_distributor : public locked_render_region_distributor<random_render_region_distributor>
    {
    public:
        random_render_region_distributor(int w, int h);
        bool get_internal(render_region* reg);
        void reset_internal();

        int get_width() const { return w_; }
        int get_height() const { return h_; }

        struct pos
        {
            int x;
            int y;
        };

    protected:
        void create_map();

    protected:
        int w_;
        int h_;
        std::vector<pos> map_;
        int i_; //
    };
}

#endif
