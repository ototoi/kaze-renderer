#include "scanline_render_region_distributor.h"

namespace kaze
{

    scanline_render_region_distributor::scanline_render_region_distributor(int w, int h) : w_(w), h_(h)
    {
        y_ = 0;
    }

    void scanline_render_region_distributor::reset_internal()
    {
        y_ = 0;
    }

    bool scanline_render_region_distributor::get_internal(render_region* reg)
    {
        int y = y_;
        if (y < h_)
        {
            int x0 = 0;
            int y0 = y;
            int x1 = w_;
            int y1 = y + 1;

            reg->x0 = x0;
            reg->y0 = y0;
            reg->x1 = x1;
            reg->y1 = y1;

            y_++;
            return true;
        }
        else
        {
            return false;
        }
    }
}
