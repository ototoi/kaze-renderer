#ifndef KAZE_RENDER_REGION_DISTRIBUTOR_H
#define KAZE_RENDER_REGION_DISTRIBUTOR_H

namespace kaze
{

    struct render_region
    {
        int x0;
        int y0;
        int x1;
        int y1;
    };

    class render_region_distributor
    {
    public:
        static const int BSZ = 32; //Default Tile Block Size
    public:
        virtual bool get(render_region* reg) = 0;
        virtual void reset() = 0;
        virtual int get_width() const = 0;
        virtual int get_height() const = 0;
        virtual ~render_region_distributor() {}
    };
}

#endif