#include "swirl_render_region_distributor.h"

#include <algorithm>
#include <cassert>

namespace kaze
{

    typedef swirl_render_region_distributor::pos pos;

    pos make_pos(int x, int y)
    {
        pos p;
        p.x = x;
        p.y = y;
        return p;
    }

    bool operator==(const pos& lp, const pos& rp)
    {
        return (lp.x == rp.x) && (lp.y == rp.y);
    }

    bool operator<(const pos& lp, const pos& rp)
    {
        return (lp.x < rp.x) && (lp.y < rp.y);
    }

    void swirl_render_region_distributor::create_map()
    {
        int w = w_;
        int h = h_;
        int bw = w / BSZ;
        int bh = h / BSZ;
        if (w % BSZ) bw++;
        if (h % BSZ) bh++;

        int cx = bw >> 1;
        int cy = bh >> 1;

        int x = cx;
        int y = cy;

        int sz = bw * bh;

        if (sz <= 1)
        {
            map_.push_back(make_pos(0, 0));
        }
        else
        {
            map_.push_back(make_pos(x, y));

            int minx = x;
            int miny = y;
            int maxx = x;
            int maxy = y;

            x = x + 1;
            //y = y;
            maxx = x;

            while (1)
            {
                //down
                while (y <= maxy)
                {
                    if ((int)map_.size() < sz)
                    {
                        if (0 <= x && x < bw && 0 <= y && y < bh)
                        {
                            map_.push_back(make_pos(x, y));
                        }
                    }
                    else
                    {
                        return;
                    }
                    y++;
                }
                //left
                while (minx <= x)
                {
                    if ((int)map_.size() < sz)
                    {
                        if (0 <= x && x < bw && 0 <= y && y < bh)
                        {
                            map_.push_back(make_pos(x, y));
                        }
                    }
                    else
                    {
                        return;
                    }
                    x--;
                }
                //up
                while (miny <= y)
                {
                    if ((int)map_.size() < sz)
                    {
                        if (0 <= x && x < bw && 0 <= y && y < bh)
                        {
                            map_.push_back(make_pos(x, y));
                        }
                    }
                    else
                    {
                        return;
                    }
                    y--;
                }
                //right
                while (x <= maxx)
                {
                    if ((int)map_.size() < sz)
                    {
                        if (0 <= x && x < bw && 0 <= y && y < bh)
                        {
                            map_.push_back(make_pos(x, y));
                        }
                    }
                    else
                    {
                        return;
                    }
                    x++;
                }

                minx--;
                miny--;
                maxx++;
                maxy++;
            }
        }
    }

    //----------------------------------------------------------------------------

    swirl_render_region_distributor::swirl_render_region_distributor(int w, int h)
        : w_(w), h_(h)
    {
        create_map();
#ifdef _DEBUG
        std::vector<pos> tm = map_;
        std::sort(tm.begin(), tm.end());
        int tn = (int)tm.size();
        tm.erase(std::unique(tm.begin(), tm.end()), tm.end());
        assert(tn == (int)tm.size());
#endif
        i_ = 0;
    }

    bool swirl_render_region_distributor::get_internal(render_region* reg)
    {
        int i = i_;
        if (i < (int)map_.size())
        {
            int x = map_[i].x;
            int y = map_[i].y;
            int x0 = x * BSZ;
            int y0 = y * BSZ;
            int x1 = x0 + BSZ;
            int y1 = y0 + BSZ;
            if (w_ < x1) x1 = w_;
            if (h_ < y1) y1 = h_;

            reg->x0 = x0;
            reg->y0 = y0;
            reg->x1 = x1;
            reg->y1 = y1;

            i_++;
            return true;
        }
        else
        {
            return false;
        }
    }

    void swirl_render_region_distributor::reset_internal()
    {
        i_ = 0;
    }
}
