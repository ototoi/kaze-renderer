#ifndef KAZE_PARTIAL_RENDER_REGION_DISTRIBUTOR_H
#define KAZE_PARTIAL_RENDER_REGION_DISTRIBUTOR_H

#include "render_region_distributor.h"
#include "count_ptr.hpp"

namespace kaze
{

    class partial_render_region_distributor : public render_region_distributor
    {
    public:
        partial_render_region_distributor(const auto_count_ptr<render_region_distributor>& rr, int dx, int dy, int w, int h);
        partial_render_region_distributor(int x0, int y0, int x1, int y1, int w, int h);

    public:
        bool get(render_region* reg);
        void reset();
        int get_width() const;
        int get_height() const;

    private:
        std::shared_ptr<render_region_distributor> rr_;
        int dx_;
        int dy_;
        int w_;
        int h_;
    };
}

#endif