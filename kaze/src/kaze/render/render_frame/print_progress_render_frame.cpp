#include "print_progress_render_frame.h"
#include "logger.h"

namespace kaze
{

    void print_progress_render_frame::print_(int x0, int y0, int x1, int y1)
    {
        //static std::mutex mtx;
        int w = width_;
        int h = height_;
        int dx = x1 - x0;
        int dy = y1 - y0;
        int n = dx * dy;
        int t = (total_ += n); //TODO
        {
            //std::lock_guard<std::mutex> lck(mtx);
            print_progress("%3.3f", (100.0 * (t)) / (w * h));
        }
    }

    print_progress_render_frame::print_progress_render_frame(int width, int height)
        : width_(width), height_(height), total_(0) {}

    void print_progress_render_frame::begin()
    {
        total_ = 0;
        print_progress("%3.3f", 0.0);
    }
    void print_progress_render_frame::render(int x0, int y0, int x1, int y1, const image<real>&)
    {
        print_(x0, y0, x1, y1);
    }
    void print_progress_render_frame::render(int x0, int y0, int x1, int y1, const image<color3>&)
    {
        print_(x0, y0, x1, y1);
    }
    void print_progress_render_frame::render(int x0, int y0, int x1, int y1, const image<color4>&)
    {
        print_(x0, y0, x1, y1);
    }
    void print_progress_render_frame::end()
    {
        print_progress("%3.3f", 100.0);
    }
}