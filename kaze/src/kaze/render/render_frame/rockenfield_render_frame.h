#ifndef KAZE_ROCKENFIELD_RENDER_FRAME_H
#define KAZE_ROCKENFIELD_RENDER_FRAME_H

#include "render_frame.h"
#include "image.hpp"
#include "color.h"

namespace kaze
{

    class rockenfield_manager;
    class rockenfield_render_frame : public render_frame
    {
    public:
        rockenfield_render_frame(int width, int height);
        ~rockenfield_render_frame();
        void render(int x0, int y0, int x1, int y1, const image<color3>& img);
        void render(int x0, int y0, int x1, int y1, const image<color4>& img);

    private:
        rockenfield_manager* mgr_;
    };
}

#endif
