#ifndef KAZE_RENDER_FRAME_H
#define KAZE_RENDER_FRAME_H

#include "color.h"
#include "image.hpp"

namespace kaze
{

    class render_frame
    {
    public:
        virtual ~render_frame() {}
    public:
        virtual void begin() {}
        virtual void render(int x0, int y0, int x1, int y1, const image<real>&) {}
        virtual void render(int x0, int y0, int x1, int y1, const image<color3>&) {}
        virtual void render(int x0, int y0, int x1, int y1, const image<color4>&) {}
        virtual void end() {}
    };
}
#endif