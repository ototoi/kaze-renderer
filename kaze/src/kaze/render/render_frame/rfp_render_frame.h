#ifndef KAZE_RFP_RENDER_FRAME_H
#define KAZE_RFP_RENDER_FRAME_H

#include "render_frame.h"
#include "image.hpp"
#include "color.h"

namespace kaze
{

    class rfp_manager;
    class rfp_render_frame : public render_frame
    {
    public:
        rfp_render_frame(int width, int height);
        ~rfp_render_frame();
        void render(int x0, int y0, int x1, int y1, const image<color3>& img);
        void render(int x0, int y0, int x1, int y1, const image<color4>& img);

    private:
        rfp_manager* mgr_;
    };
}

#endif
