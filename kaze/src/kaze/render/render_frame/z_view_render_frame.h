#ifndef KAZE_Z_VIEW_RENDER_FRAME_H
#define KAZE_Z_VIEW_RENDER_FRAME_H

#include "render_frame.h"
#include "count_ptr.hpp"
#include <vector>

namespace kaze
{

    class z_view_render_frame : public render_frame
    {
    public:
        z_view_render_frame(const auto_count_ptr<render_frame>& rf);

    public:
        void begin();
        void render(int x0, int y0, int x1, int y1, const image<real>&);
        void render(int x0, int y0, int x1, int y1, const image<color3>& img);
        void render(int x0, int y0, int x1, int y1, const image<color4>& img);
        void end();

    protected:
        std::shared_ptr<render_frame> rf_;
        real zmin_;
        real zmax_;
        int i_;
    };
}

#endif