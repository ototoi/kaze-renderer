#ifndef KAZE_CONSOLE_RENDER_FRAME_H
#define KAZE_CONSOLE_RENDER_FRAME_H

#include "render_frame.h"
#include <vector>
#include <iostream>

#include "image.hpp"

namespace kaze
{

    class console_render_frame : public render_frame
    {
    public:
        console_render_frame(int w, int h, int backet_size = 32, std::ostream& os = std::cout);
        void render(int x0, int y0, int x1, int y1, const image<real>&);
        void render(int x0, int y0, int x1, int y1, const image<color3>&);
        void render(int x0, int y0, int x1, int y1, const image<color4>&);

    protected:
        void render(int x0, int y0, int x1, int y1);

    public:
        std::vector<int> v_;
        int w_;
        int h_;
        int nw_;
        int nh_;
        int bsz_;
        int total_;
        std::ostream& os_;
    };
}

#endif