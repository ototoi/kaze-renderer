#include "composite_render_frame.h"

namespace kaze
{

    void composite_render_frame::begin()
    {
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            mv_[i]->begin();
        }
    }

    void composite_render_frame::render(int x0, int y0, int x1, int y1, const image<real>& img)
    {
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            mv_[i]->render(x0, y0, x1, y1, img);
        }
    }

    void composite_render_frame::render(int x0, int y0, int x1, int y1, const image<color3>& img)
    {
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            mv_[i]->render(x0, y0, x1, y1, img);
        }
    }

    void composite_render_frame::render(int x0, int y0, int x1, int y1, const image<color4>& img)
    {
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            mv_[i]->render(x0, y0, x1, y1, img);
        }
    }

    void composite_render_frame::end()
    {
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            mv_[i]->end();
        }
    }

    void composite_render_frame::add(const auto_count_ptr<render_frame>& rf)
    {
        mv_.push_back(rf);
    }
}