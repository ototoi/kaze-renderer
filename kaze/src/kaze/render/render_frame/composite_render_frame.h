#ifndef KAZE_COMPOSITE_RENDER_FRAME_H
#define KAZE_COMPOSITE_RENDER_FRAME_H

#include "render_frame.h"
#include "count_ptr.hpp"
#include <vector>

namespace kaze
{

    class composite_render_frame : public render_frame
    {
    public:
        void begin();
        void render(int x0, int y0, int x1, int y1, const image<real>&);
        void render(int x0, int y0, int x1, int y1, const image<color3>&);
        void render(int x0, int y0, int x1, int y1, const image<color4>&);
        void end();

    public:
        void add(const auto_count_ptr<render_frame>& rf);

    protected:
        std::vector<std::shared_ptr<render_frame> > mv_;
    };
}

#endif
