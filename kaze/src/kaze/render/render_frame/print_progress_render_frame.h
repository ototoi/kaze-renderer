#ifndef KAZE_PRINT_PROGRESS_RENDER_FRAME_H
#define KAZE_PRINT_PROGRESS_RENDER_FRAME_H

#include "render_frame.h"

namespace kaze
{

    class print_progress_render_frame : public render_frame
    {
    public:
        print_progress_render_frame(int width, int height);

    public:
        void begin();
        void render(int x0, int y0, int x1, int y1, const image<real>&);
        void render(int x0, int y0, int x1, int y1, const image<color3>&);
        void render(int x0, int y0, int x1, int y1, const image<color4>&);
        void end();

    protected:
        void print_(int x0, int y0, int x1, int y1);

    protected:
        int width_;
        int height_;
        int total_;
    };
}

#endif
