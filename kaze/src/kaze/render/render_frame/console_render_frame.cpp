#include "console_render_frame.h"

#include <iostream>

namespace kaze
{
    console_render_frame::console_render_frame(int w, int h, int backet_size, std::ostream& os) : os_(os)
    {
        int nw = w / backet_size;
        int nh = h / backet_size;
        if (w % backet_size) nw++;
        if (h % backet_size) nh++;
        int sz = nw * nh;
        v_.resize(sz);
        for (int i = 0; i < sz; i++)
        {
            v_[i] = 0;
        }
        w_ = w;
        h_ = h;
        nw_ = nw;
        nh_ = nh;
        bsz_ = backet_size;
        total_ = 0;
    }

    void console_render_frame::render(int x0, int y0, int x1, int y1)
    {
#if 1
        int xw = x1 - x0;
        int yw = y1 - y0;
        int n = xw * yw;
        total_ += n;
        float ratio = total_ / double(w_*h_);
        int nr = std::ceil(ratio*100);
        int nrx = std::ceil(ratio*50);
        std::stringstream ss;
        ss << "[";
        for(int i=1;i<=100;i++)
        {
            if(i >= nr)
            {
                ss << " ";
            }
            else
            {
                ss << "|";
            }
        }
        ss << "]" << "\n";
        //printf("%3d : %s \r", nr, ss.str().c_str());
#else
        int bx0 = x0 / bsz_;
        int by0 = y0 / bsz_;
        int bx1 = x1 / bsz_;
        int by1 = y1 / bsz_;

        if (x1 % bsz_) bx1++;
        if (y1 % bsz_) by1++;

        int nw = nw_;
        int nh = nh_;

        for (int y = 0; y < nh; y++)
        {
            for (int x = 0; x < nw; x++)
            {
                if (v_[y * nw_ + x] == 2) v_[y * nw_ + x] = 1;
            }
        }

        for (int y = by0; y < by1; y++)
        {
            for (int x = bx0; x < bx1; x++)
            {
                v_[y * nw_ + x] = 2;
            }
        }

        //
        for (int x = 0; x < nw; x++)
        {
            os_ << "�|";
        }
        os_ << std::endl;
        for (int y = 0; y < nh; y++)
        {
            for (int x = 0; x < nw; x++)
            {
                const char* c = "";
                switch (v_[y * nw_ + x])
                {
                case 0:
                    c = "[_]";
                    break;
                case 1:
                    c = "[#]";
                    break;
                case 2:
                    c = "[*]";
                    break;
                }
                os_ << c;
            }
            os_ << std::endl;
        }
        for (int x = 0; x < nw; x++)
        {
            os_ << "�|";
        }
        os_ << std::endl;
#endif
    }

    void console_render_frame::render(int x0, int y0, int x1, int y1, const image<real>&)
    {
        render(x0, y0, x1, y1);
    }
    void console_render_frame::render(int x0, int y0, int x1, int y1, const image<color3>&)
    {
        render(x0, y0, x1, y1);
    }
    void console_render_frame::render(int x0, int y0, int x1, int y1, const image<color4>&)
    {
        render(x0, y0, x1, y1);
    }
}