#include "z_view_render_frame.h"

namespace kaze
{
    namespace
    {

        static inline real z_clamp(real z, real zmin, real zmax)
        {
            return (z - zmin) / (zmax - zmin);
        }

        class zimage : public image<real>
        {
        public:
            zimage(const image<real>& img, real zmin, real zmax) : img_(img), zmin_(zmin), zmax_(zmax) {}
            real get(int x, int y) const
            {
                return z_clamp(img_.get(x, y), zmin_, zmax_);
            }
            void set(int x, int y, const real& t) {}
            int get_width() const { return img_.get_width(); }
            int get_height() const { return img_.get_height(); }
        protected:
            const image<real>& img_;
            real zmin_;
            real zmax_;
        };
    }

    z_view_render_frame::z_view_render_frame(const auto_count_ptr<render_frame>& rf)
        : rf_(rf)
    {
        zmin_ = 1;
        zmax_ = 0;
        i_ = 0;
    }

    void z_view_render_frame::begin()
    {
        i_ = 0;
        rf_->begin();
    }

    void z_view_render_frame::render(int x0, int y0, int x1, int y1, const image<real>& img)
    {
        real zmin = zmin_;
        real zmax = zmax_;
        for (int y = y0; y < y1; y++)
        {
            for (int x = x0; x < x1; x++)
            {
                real z = img.get(x, y);
                if (z < zmin) zmin = z;
                if (z > zmax) zmax = z;
            }
        }

        zmin_ = zmin;
        zmax_ = zmax;

        if ((i_++ & 31) == 0)
        {
            rf_->render(0, 0, img.get_width(), img.get_height(), zimage(img, zmin, zmax));
        }
        else
        {
            rf_->render(x0, y0, x1, y1, zimage(img, zmin, zmax));
        }
    }

    void z_view_render_frame::render(int x0, int y0, int x1, int y1, const image<color3>& img)
    {
        rf_->render(x0, y0, x1, y1, img);
    }

    void z_view_render_frame::render(int x0, int y0, int x1, int y1, const image<color4>& img)
    {
        rf_->render(x0, y0, x1, y1, img);
    }

    void z_view_render_frame::end()
    {
        rf_->end();
    }
}