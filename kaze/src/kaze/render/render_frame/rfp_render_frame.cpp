#include "rfp_render_frame.h"
#include "logger.h"
#include "socket_io.h"

#include <thread>
#include <mutex>
#include <vector>

//----------------------------------
//from rockenfield @ lucille
//#define DEFAULT_PORT 21264 /* change as you like. */
#define MAXPACKETS 32 * 32
#define DEFAULT_APP "rfp"
#define LOCALADDR "127.0.0.1"

/* Commands sent to the server */
#define COMMAND_NEW 0
#define COMMAND_FINISH 1
#define COMMAND_PIXEL 2

/* Commands sent from the server */
#define COMMAND_CANCEL 10 // Cancel(e.g. The window is closed.)

/* Commands for ack */
#define COMMAND_FINISH_ACK 21
//----------------------------------

#define DEFAULT_FILE "/tmp/12348383.zzz"

namespace kaze
{
    namespace
    {
        struct pixel_packet
        {
            int x;
            int y;
            float col[4];
        };

        struct image_info
        {
            int width;
            int height;
        };
    }

    class rfp_manager
    {
    public:
        rfp_manager();
        ~rfp_manager();

    public:
        int open(int width, int height);
        int close();
        int progress();

    public:
        int write_packet(pixel_packet* packets, int n);

    private:
        socket_io* m_socket;
        mutable std::mutex mtx;
    };

    rfp_manager::rfp_manager()
        : m_socket(0)
    {
        m_socket = new socket_io();
    }

    rfp_manager::~rfp_manager()
    {
        if (m_socket) delete m_socket;
    }

    int rfp_manager::open(int width, int height)
    {
        std::lock_guard<std::mutex> lck(mtx);
        bool ret = m_socket->connect(DEFAULT_FILE);
        if (ret == false)
        {
            return -1;
        }

        int comm = COMMAND_NEW;
        int len = sizeof(image_info);
        image_info info;
        info.width = width;
        info.height = height;

        //print_log("(Disp  ) Established socket display driver: %s:%d¥n", m_addrString.c_str(), m_port);

        m_socket->send((char*)&comm, sizeof(int), 0);
        m_socket->send((char*)&len, sizeof(int), 0);
        m_socket->send((char*)&info, len, 0);
        return 0;
    }

    int rfp_manager::close()
    {
        std::lock_guard<std::mutex> lck(mtx);

        int comm;

        comm = COMMAND_FINISH;

        m_socket->send((char*)&comm, sizeof(int), 0);

        // Wait ack
        int recvSize = m_socket->recv((char*)&comm, sizeof(int), 0);
        if (recvSize <= 0)
        { // error
            return -1;
        }

        if (comm != COMMAND_FINISH_ACK)
        {
            print_log("Got invalid ack\n");
            return -1;
        }

        m_socket->close();
        return 0;
    }

    int rfp_manager::write_packet(pixel_packet* packets, int n)
    {
        if (n > 0)
        {
            std::lock_guard<std::mutex> lck(mtx);
            int comm = COMMAND_PIXEL;
            int len = sizeof(pixel_packet) * n;
            m_socket->send((char*)&comm, sizeof(int), 0);
            m_socket->send((char*)&len, sizeof(int), 0);
            m_socket->send((char*)packets, len, 0);
        }
        return 0;
    }

    int rfp_manager::progress()
    {
        return 0;
    }

    //--------------------------------------------------------------

    rfp_render_frame::rfp_render_frame(int width, int height)
    {
        mgr_ = new rfp_manager();
        int nRet = 0;
        int count = 0;
        while ( (nRet = mgr_->open(width, height)) != 0 && count < 5)
        {
            std::chrono::milliseconds dura( 10 );
            std::this_thread::sleep_for( dura );
            count++;
        }
        if (nRet != 0) 
        {
            delete mgr_;
            mgr_ = 0;
        }
    }

    rfp_render_frame::~rfp_render_frame()
    {
        if (mgr_)
        {
            mgr_->close();
            delete mgr_;
        }
    }

    void rfp_render_frame::render(int x0, int y0, int x1, int y1, const image<color3>& img)
    {
        if (mgr_)
        {
            int w = x1 - x0;
            int h = y1 - y0;
            int total = w * h;
            std::vector<pixel_packet> packets(total);
            int i = 0;
            for (int y = y0; y < y1; y++)
            {
                for (int x = x0; x < x1; x++)
                {
                    color3 col = img.get(x, y);
                    packets[i].x = x;
                    packets[i].y = y;
                    packets[i].col[0] = (float)col[0];
                    packets[i].col[1] = (float)col[1];
                    packets[i].col[2] = (float)col[2];
                    packets[i].col[3] = (float)1.0;
                    i++;
                }
            }
            mgr_->write_packet(&packets[0], total);
        }
    }

    void rfp_render_frame::render(int x0, int y0, int x1, int y1, const image<color4>& img)
    {
        if (mgr_)
        {
            int w = x1 - x0;
            int h = y1 - y0;
            int total = w * h;
            std::vector<pixel_packet> packets(total);
            int i = 0;
            for (int y = y0; y < y1; y++)
            {
                for (int x = x0; x < x1; x++)
                {
                    color4 col = img.get(x, y);
                    packets[i].x = x;
                    packets[i].y = y;
                    packets[i].col[0] = (float)col[0];
                    packets[i].col[1] = (float)col[1];
                    packets[i].col[2] = (float)col[2];
                    packets[i].col[3] = (float)col[3];
                    i++;
                }
            }
            mgr_->write_packet(&packets[0], total);
        }
    }
}
