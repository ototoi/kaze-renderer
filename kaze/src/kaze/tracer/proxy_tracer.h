#ifndef KAZE_PROXY_TRACER_H
#define KAZE_PROXY_TRACER_H

#include "tracer.h"

namespace kaze
{

    class proxy_tracer : public tracer
    {
    public:
        proxy_tracer(const tracer* trc) : trc_(trc) { ; }
        ~proxy_tracer() { /**/}

        virtual bool trace(spectrum* spec, const ray& r, const mediation& media) const { return trc_->trace(spec, r, media); }
        virtual bool trace(spectrum* spec, const ray& r, real tmin, real tmax, const mediation& media) const { return trc_->trace(spec, r, tmin, tmax, media); }
        virtual bool trace(const ray& r, real tmin, real tmax, const mediation& media) const { return trc_->trace(r, tmin, tmax, media); }
    protected:
        const tracer* trc_;
    };
}

#endif
