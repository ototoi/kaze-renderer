#ifndef KAZE_RAY_TRACER_H
#define KAZE_RAY_TRACER_H

#include "tracer.h"
#include "count_ptr.hpp"

namespace kaze
{

    class intersection;
    class integrator;

    class ray_tracer : public tracer
    {
    public:
        ray_tracer(const auto_count_ptr<intersection>& inter, const auto_count_ptr<integrator>& ilm);
        bool trace(spectrum* spec, const ray& r, real tmin, real tmax, const mediation& media) const;
        bool trace(spectrum* out_spec,
                   const ray& r,
                   const ray& r00,
                   const ray& r10,
                   const ray& r01,
                   const ray& r11,
                   const mediation& media) const;

    private:
        std::shared_ptr<intersection> inter_;
        std::shared_ptr<integrator> ilm_;
    };
}

#endif
