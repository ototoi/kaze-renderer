#ifndef KAZE_TRACER_H
#define KAZE_TRACER_H

#include "ray.h"
#include "mediation.h"
#include "spectrum.h"

namespace kaze
{

    class tracer
    {
    public:
        virtual bool trace(spectrum* spec, const ray& r, const mediation& media) const { return trace(spec, r, 1e-8, 1e+8, media); }
        virtual bool trace(const ray& r, const mediation& media) const { return trace(r, 1e-8, 1e+8, media); }
        virtual bool trace(spectrum* spec, const ray& r, real tmin, real tmax, const mediation& media) const { return false; }
        virtual bool trace(const ray& r, real tmin, real tmax, const mediation& media) const { return false; }
        virtual ~tracer() {}
    };

} //end of namespace.

#endif
