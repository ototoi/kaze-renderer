#include "ray_tracer.h"

#include "values.h"
#include "color.h"
#include "shader.h"
#include "integrator.h"

#include "test_info.h"

#include <algorithm> //max
#include <cmath>

#include <iostream>

#include "intersection.h"
#include "basic_collection.hpp"

namespace kaze
{

    static inline void finalize(test_info* info, const ray& r, const mediation& media)
    {
        static const real ZERO[3] = {0, 0, 0};

        if (info->p_intersection)
        {
            info->p_intersection->finalize(info, r, info->distance);
            if (memcmp(&(info->geometric), ZERO, sizeof(real) * 3) == 0)
            {
                info->geometric = info->normal;
            }
        }
    }

    static inline real opa(real x) { return (x < 1) ? x : 1; }
    static inline color3 opa(const color3& x)
    {
        return color3(opa(x[0]), opa(x[1]), opa(x[2]));
    }

    ray_tracer::ray_tracer(const auto_count_ptr<intersection>& inter, const auto_count_ptr<integrator>& ilm) : inter_(inter), ilm_(ilm) {}

    bool ray_tracer::trace(spectrum* out_spec, const ray& r, real tmin, real tmax, const mediation& media) const
    {
        static const real EPSILON = values::near() * 1024;
        int cnt = media.count();
        if (cnt >= 10)
        {
            return false;
        }

        test_info info;
        if (inter_->test(&info, r, tmax))
        {
            finalize(&info, r, media);

            const shader* pShader = info.get_shader();
            const surface_shader* ps = (pShader) ? pShader->get_surface_shader() : NULL;

            if (ps)
            {
                sufflight sl(
                    r.origin(), r.direction(),
                    info.get_position(),
                    info.get_geometric(),
                    info.get_normal(),
                    info.get_tangent(),
                    info.get_binormal(),
                    info.get_coord());

                sl.set_attr(info.i_attr);
                sl.set_index(info.index);

                sl.set_color1(info.col1);
                sl.set_color2(info.col2);
                sl.set_color3(info.col3);

                basic_collection<radiance> rs;
                ilm_->illuminate(&rs, sl, media);
                spectrum spc = ps->shade(rs, sl, *this, media);
                color3 c = spc.to_rgb();
                color3 o = opa(opacity(spc));
                real a = std::min<real>(o[0], std::min<real>(o[1], o[2]));
                if (a < 1)
                {
                    ray r2(info.get_position() + r.direction() * EPSILON, r.direction(), r.time());
                    spectrum spc2;
                    if (this->trace(&spc2, r2, 0, values::far(), media.next_t()))
                    {
                        color3 c2 = spc2.to_rgb();
                        color3 o2 = opacity(spc2);
                        color3 cx = c * o + (color3(1, 1, 1) - o) * c2;
                        color3 ox = o + o2;
                        *out_spec = convert(cx, ox);
                    }
                    else
                    {
                        *out_spec = spc;
                    }
                }
                else
                {
                    *out_spec = spc;
                }
            }
            else
            {
                //return false;
                *out_spec = spectrum_black();
            }

            return true;
        }

        return false;
    }
#if 1
    bool ray_tracer::trace(spectrum* out_spec,
                           const ray& r,
                           const ray& r00,
                           const ray& r10,
                           const ray& r01,
                           const ray& r11,
                           const mediation& media) const
    {
        test_info info;

        if (inter_->test(&info, r, values::far()))
        {
            finalize(&info, r, media);

            const shader* pShader = info.get_shader();
            const surface_shader* ps = (pShader) ? pShader->get_surface_shader() : NULL;

            if (ps)
            {
                {
                    sufflight sl(
                        r.origin(), r.direction(),
                        info.get_position(),
                        info.get_geometric(),
                        info.get_normal(),
                        info.get_tangent(),
                        info.get_binormal(),
                        info.get_coord());
                    sl.set_attr(info.i_attr);
                    sl.set_index(info.index);

                    sl.set_color1(info.col1);
                    sl.set_color2(info.col2);
                    sl.set_color3(info.col3);

                    basic_collection<radiance> rs;
                    ilm_->illuminate(&rs, sl, media);
                    *out_spec = ps->shade(rs, sl, *this, media);
                }
            }
            else
            {
                //return false;
                *out_spec = spectrum_black();
            }

            return true;
        }

        return false;
    }
#endif
}
