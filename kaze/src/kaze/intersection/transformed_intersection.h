#ifndef KAZE_TRANSFORMED_INTERSECTION_H
#define KAZE_TRANSFORMED_INTERSECTION_H

#include "bounded_intersection.h"

#include "transformer.h"

namespace kaze
{

    class transformed_intersection : public bounded_intersection
    {
    public:
        virtual ~transformed_intersection() {}

        virtual bool test(const ray& r, real dist) const = 0;
        virtual bool test(test_info* info, const ray& r, real dist) const = 0;
        virtual void finalize(test_info* info, const ray& r, real dist) const = 0;

        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;
    };

    class m4_transformed_intersection : public transformed_intersection
    {
    public:
        m4_transformed_intersection(intersection* inter, const matrix4& m);
        m4_transformed_intersection(const std::shared_ptr<intersection>& inter, const matrix4& m);
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        bool test(const ray& r, real tmin, real tmax) const;
        bool test(test_info* info, const ray& r, real tmin, real tmax) const;

        vector3 min() const;
        vector3 max() const;

    protected:
        bool test_internal(const ray& r, real dist) const;
        bool test_internal(test_info* info, const ray& r, real dist) const;

    protected:
        std::shared_ptr<intersection> inter_;
        matrix4 m_;
        matrix4 im_;
        vector3 min_;
        vector3 max_;
    };
}

#endif
