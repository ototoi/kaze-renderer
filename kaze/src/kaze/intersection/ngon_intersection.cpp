#include "ngon_intersection.h"

#include "test_info.h"

#include "triangulate_polygon.h"

#include "values.h"
#include <vector>
#include <algorithm>

#include "triangle_quad_tree.h"

namespace kaze
{
    namespace
    {
        void get_minmax(vector3& min, vector3& max, const std::vector<vector3>& v)
        {
            min = max = v[0];
            for (size_t i = 0; i < v.size(); i++)
            {
                for (int k = 0; k < 3; k++)
                {
                    if (v[i][k] < min[k]) min[k] = v[i][k];
                    if (v[i][k] > max[k]) max[k] = v[i][k];
                }
            }
        }
        //----------------------------------------------------------------

        struct uvt
        {
            real u, v, t;
        };

        static bool test_plane_(uvt* info, const vector3& pos_, const vector3& u_, const vector3& v_, const ray& r, real dist)
        {
            static const real EPSILON = values::epsilon() * 1000;
            real u, v, t;

            const vector3& org = r.origin();
            const vector3& dir = r.direction();
            const vector3& p0 = pos_;

            //-e1 = p0-p1
            vector3 e1 = -u_; //vA

            //-e2 = p0-p2
            vector3 e2 = -v_; //vB

            //dir = GHI

            vector3 bDir(cross(e2, dir));

            real iM = dot(e1, bDir);

            if (fabs(iM) > EPSILON)
            {
                iM = real(1.0) / iM;

                //p0-org
                vector3 vOrg(p0 - org); //JKL
                vector3 vE(cross(e1, vOrg));
                t = -dot(e2, vE) * iM;
                if (t <= 0 || dist <= t) return false;

                u = dot(vOrg, bDir) * iM;
                v = dot(dir, vE) * iM;

                info->u = u;
                info->v = v;
                info->t = t;
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    class ngon_intersection_imp
    {
    public:
        ngon_intersection_imp(const vector3& org, const vector3& u, const vector3& v);
        ngon_intersection_imp(const vector3& org, const vector3& u, const vector3& v, const std::vector<vector2>& loop);
        ~ngon_intersection_imp();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const { return min_; }
        vector3 max() const { return max_; }
    protected:
        void initialize(const vector3& org, const vector3& u, const vector3& v, const std::vector<vector2>& loop);

    private:
        triangle_quad_tree* tree_;
        vector3 min_;
        vector3 max_;

        vector3 o_;
        vector3 u_;
        vector3 v_;

        vector3 n_;
    };

    void ngon_intersection_imp::initialize(const vector3& org, const vector3& u, const vector3& v, const std::vector<vector2>& loop)
    {
        static const real EPSILON = values::epsilon() * 1024;

        if (loop.empty()) return;
        std::vector<vector2> src(loop);
        if (src.front() == src.back()) src.pop_back();

        std::vector<vector2> tri;
        triangulate_polygon(tri, src);

        assert(tri.size() % 3 == 0);

        std::vector<vector3> points;
        points.reserve(tri.size());

        for (size_t i = 0; i < tri.size() - 2; i += 3)
        {
            vector2 c0 = tri[i];
            vector2 c1 = tri[i + 1];
            vector2 c2 = tri[i + 2];

            vector3 p0 = org + u * c0[0] + v * c0[1];
            vector3 p1 = org + u * c1[0] + v * c1[1];
            vector3 p2 = org + u * c2[0] + v * c2[1];

            points.push_back(p0);
            points.push_back(p1);
            points.push_back(p2);
        }

        tree_ = new triangle_quad_tree(tri);

        get_minmax(min_, max_, points);
        min_ -= vector3(EPSILON, EPSILON, EPSILON);
        max_ += vector3(EPSILON, EPSILON, EPSILON);

        o_ = org;
        u_ = u;
        v_ = v;
        n_ = normalize(cross(u, v));
    }

    ngon_intersection_imp::ngon_intersection_imp(const vector3& org, const vector3& u, const vector3& v, const std::vector<vector2>& loop)
    {
        initialize(org, u, v, loop);
    }

    ngon_intersection_imp::~ngon_intersection_imp()
    {
        delete tree_;
    }

    bool ngon_intersection_imp::test(const ray& r, real dist) const
    {
        if (test_AABB(min_, max_, r, dist))
        {
            uvt uvinfo;
            if (test_plane_(&uvinfo, o_, u_, v_, r, dist))
            {
                vector2 c(uvinfo.u, uvinfo.v);
                return tree_->test(c);
            }
        }
        return false;
    }

    bool ngon_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        if (test_AABB(min_, max_, r, dist))
        {
            uvt uvinfo;
            if (test_plane_(&uvinfo, o_, u_, v_, r, dist))
            {
                vector2 c(uvinfo.u, uvinfo.v);
                if (tree_->test(c))
                {
                    info->distance = uvinfo.t;
                    info->coord = vector3(c[0], c[1], 0);
                    return true;
                }
            }
        }
        return false;
    }

    void ngon_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        info->position = r.origin() + dist * r.direction();
        vector3 n = n_;
        //if(dot(r.direction(),n_)>0)n.negate();
        info->normal = n_;
        info->geometric = n;
    }

    //--------------------------------------------------

    ngon_intersection::ngon_intersection(const vector3& org, const vector3& u, const vector3& v, const std::vector<vector2>& loop)
    {
        imp_ = new ngon_intersection_imp(org, u, v, loop);
    }

    ngon_intersection::~ngon_intersection()
    {
        delete imp_;
    }

    bool ngon_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool ngon_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void ngon_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        imp_->finalize(info, r, dist);
    }

    vector3 ngon_intersection::min() const
    {
        return imp_->min();
    }

    vector3 ngon_intersection::max() const
    {
        return imp_->max();
    }
}
