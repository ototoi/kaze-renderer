#ifndef KAZE_DOKODEMO_DOOR_INTERSECTION_H
#define KAZE_DOKODEMO_DOOR_INTERSECTION_H

#include "bounded_intersection.h"


/*
 * DOKODEMO DOOR 
 */

namespace kaze
{

    class dokodemo_door_intersection_imp;

    class dokodemo_door_intersection : public bounded_intersection
    {
    public:
        dokodemo_door_intersection(
            intersection* inter,
            const vector3& pos1, const vector3& u1, const vector3& v1,
            const vector3& pos2, const vector3& u2, const vector3& v2);
        dokodemo_door_intersection(
            const std::shared_ptr<intersection>& inter,
            const vector3& pos1, const vector3& u1, const vector3& v1,
            const vector3& pos2, const vector3& u2, const vector3& v2);
        ~dokodemo_door_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    private:
        dokodemo_door_intersection(const dokodemo_door_intersection& rhs);
        dokodemo_door_intersection& operator=(const dokodemo_door_intersection& rhs);

    private:
        dokodemo_door_intersection_imp* imp_;
    };
}

#endif
