#ifndef KAZE_PHANTOM_INTERSECTION_H
#define KAZE_PAHNTOM_INTERSECTION_H

#include "bounded_intersection.h"


namespace kaze
{
    class phantom_intersection : public bounded_intersection
    {
    public:
        enum
        {
            PHANTOM_NOSHADOW,
            PHANTOM_INVISIBLE,
            PHANTOM_NOEFFECT,
            PHANTOM_END
        };
        explicit phantom_intersection(const std::shared_ptr<intersection>& inter, int type = PHANTOM_NOSHADOW);
        ~phantom_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        void set_type(int t);

        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> inter_;
        int type_;
    };
}

#endif
