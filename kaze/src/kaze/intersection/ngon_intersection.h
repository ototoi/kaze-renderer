#ifndef KAZE_NGON_INTERSECTION_H
#define KAZE_NGON_INTERSECTION_H

#include "bounded_intersection.h"

#include <vector>

namespace kaze
{

    class ngon_intersection_imp;

    class ngon_intersection : public bounded_intersection
    {
    public:
        ngon_intersection(const vector3& org, const vector3& u, const vector3& v, const std::vector<vector2>& loop);
        ~ngon_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    private:
        ngon_intersection_imp* imp_;
    };
    typedef ngon_intersection Ngon_intersection;
    typedef ngon_intersection polygon_intersection;
}

#endif