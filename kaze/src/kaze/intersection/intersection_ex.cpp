#include "intersection_ex.h"

namespace kaze
{

    template <class T>
    struct epsilon_
    {
        static T value() { return T(4) * std::numeric_limits<T>::epsilon(); }
    };

    static inline real min3(const vector3& v)
    {
        return (v[0] < v[1]) ? std::min(v[0], v[2]) : std::min(v[1], v[2]);
    }

    static inline real max3(const vector3& v)
    {
        return (v[0] > v[1]) ? std::max(v[0], v[2]) : std::max(v[1], v[2]);
    }

    typedef bool (*AABB_RNG_FUNC)(range_AABB* rng, distbound_AABB* bnd, const vector3& min, const vector3& max, const ray& r, real dist);

    static bool test_AABB_RNG_MMM(range_AABB* rng, distbound_AABB* bnd, const vector3& min, const vector3& max, const ray& r, real dist)
    { //7
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] < min[0]) || (org[1] < min[1]) || (org[2] < min[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * a[1] - dir[1] * b[0] < 0) ||
            (dir[0] * b[1] - dir[1] * a[0] > 0) ||
            (dir[0] * b[2] - dir[2] * a[0] > 0) ||
            (dir[0] * a[2] - dir[2] * b[0] < 0) ||
            (dir[1] * a[2] - dir[2] * b[1] < 0) ||
            (dir[1] * b[2] - dir[2] * a[1] > 0))
        {
            return false;
        }

        bnd->tmin[0] = b[0] * idir[0];
        bnd->tmin[1] = b[1] * idir[1];
        bnd->tmin[2] = b[2] * idir[2];

        bnd->tmax[0] = a[0] * idir[0];
        bnd->tmax[1] = a[1] * idir[1];
        bnd->tmax[2] = a[2] * idir[2];

        rng->tmin = max3(bnd->tmin) * (real(1) - epsilon_<real>::value());
        rng->tmax = min3(bnd->tmax) * (real(1) + epsilon_<real>::value());

        // compute the intersection distance
        return (rng->tmin < dist);
    }

    static bool test_AABB_RNG_MMP(range_AABB* rng, distbound_AABB* bnd, const vector3& min, const vector3& max, const ray& r, real dist)
    { //6
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] < min[0]) || (org[1] < min[1]) || (org[2] > max[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * a[1] - dir[1] * b[0] < 0) ||
            (dir[0] * b[1] - dir[1] * a[0] > 0) ||
            (dir[0] * b[2] - dir[2] * b[0] > 0) ||
            (dir[0] * a[2] - dir[2] * a[0] < 0) ||
            (dir[1] * a[2] - dir[2] * a[1] < 0) ||
            (dir[1] * b[2] - dir[2] * b[1] > 0))
        {
            return false;
        }

        bnd->tmin[0] = b[0] * idir[0];
        bnd->tmin[1] = b[1] * idir[1];
        bnd->tmin[2] = a[2] * idir[2];

        bnd->tmax[0] = a[0] * idir[0];
        bnd->tmax[1] = a[1] * idir[1];
        bnd->tmax[2] = b[2] * idir[2];

        rng->tmin = max3(bnd->tmin) * (real(1) - epsilon_<real>::value());
        rng->tmax = min3(bnd->tmax) * (real(1) + epsilon_<real>::value());

        // compute the intersection distance
        return (rng->tmin < dist);
    }

    static bool test_AABB_RNG_MPM(range_AABB* rng, distbound_AABB* bnd, const vector3& min, const vector3& max, const ray& r, real dist)
    { //5
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] < min[0]) || (org[1] > max[1]) || (org[2] < min[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * a[1] - dir[1] * a[0] < 0) ||
            (dir[0] * b[1] - dir[1] * b[0] > 0) ||
            (dir[0] * b[2] - dir[2] * a[0] > 0) ||
            (dir[0] * a[2] - dir[2] * b[0] < 0) ||
            (dir[1] * b[2] - dir[2] * b[1] < 0) ||
            (dir[1] * a[2] - dir[2] * a[1] > 0))
        {
            return false;
        }

        bnd->tmin[0] = b[0] * idir[0];
        bnd->tmin[1] = a[1] * idir[1];
        bnd->tmin[2] = b[2] * idir[2];

        bnd->tmax[0] = a[0] * idir[0];
        bnd->tmax[1] = b[1] * idir[1];
        bnd->tmax[2] = a[2] * idir[2];

        rng->tmin = max3(bnd->tmin) * (real(1) - epsilon_<real>::value());
        rng->tmax = min3(bnd->tmax) * (real(1) + epsilon_<real>::value());

        // compute the intersection distance
        return (rng->tmin < dist);
    }

    static bool test_AABB_RNG_MPP(range_AABB* rng, distbound_AABB* bnd, const vector3& min, const vector3& max, const ray& r, real dist)
    { //4
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] < min[0]) || (org[1] > max[1]) || (org[2] > max[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * a[1] - dir[1] * a[0] < 0) ||
            (dir[0] * b[1] - dir[1] * b[0] > 0) ||
            (dir[0] * b[2] - dir[2] * b[0] > 0) ||
            (dir[0] * a[2] - dir[2] * a[0] < 0) ||
            (dir[1] * b[2] - dir[2] * a[1] < 0) ||
            (dir[1] * a[2] - dir[2] * b[1] > 0))
        {
            return false;
        }

        bnd->tmin[0] = b[0] * idir[0];
        bnd->tmin[1] = a[1] * idir[1];
        bnd->tmin[2] = a[2] * idir[2];

        bnd->tmax[0] = a[0] * idir[0];
        bnd->tmax[1] = b[1] * idir[1];
        bnd->tmax[2] = b[2] * idir[2];

        rng->tmin = max3(bnd->tmin) * (real(1) - epsilon_<real>::value());
        rng->tmax = min3(bnd->tmax) * (real(1) + epsilon_<real>::value());

        // compute the intersection distance
        return (rng->tmin < dist);
    }

    static bool test_AABB_RNG_PMM(range_AABB* rng, distbound_AABB* bnd, const vector3& min, const vector3& max, const ray& r, real dist)
    { //3
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] > max[0]) || (org[1] < min[1]) || (org[2] < min[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * b[1] - dir[1] * b[0] < 0) ||
            (dir[0] * a[1] - dir[1] * a[0] > 0) ||
            (dir[0] * a[2] - dir[2] * a[0] > 0) ||
            (dir[0] * b[2] - dir[2] * b[0] < 0) ||
            (dir[1] * a[2] - dir[2] * b[1] < 0) ||
            (dir[1] * b[2] - dir[2] * a[1] > 0))
        {
            return false;
        }

        bnd->tmin[0] = a[0] * idir[0];
        bnd->tmin[1] = b[1] * idir[1];
        bnd->tmin[2] = b[2] * idir[2];

        bnd->tmax[0] = b[0] * idir[0];
        bnd->tmax[1] = a[1] * idir[1];
        bnd->tmax[2] = a[2] * idir[2];

        rng->tmin = max3(bnd->tmin) * (real(1) - epsilon_<real>::value());
        rng->tmax = min3(bnd->tmax) * (real(1) + epsilon_<real>::value());

        // compute the intersection distance
        return (rng->tmin < dist);
    }

    static bool test_AABB_RNG_PMP(range_AABB* rng, distbound_AABB* bnd, const vector3& min, const vector3& max, const ray& r, real dist)
    { //2
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] > max[0]) || (org[1] < min[1]) || (org[2] > max[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * b[1] - dir[1] * b[0] < 0) ||
            (dir[0] * a[1] - dir[1] * a[0] > 0) ||
            (dir[0] * a[2] - dir[2] * b[0] > 0) ||
            (dir[0] * b[2] - dir[2] * a[0] < 0) ||
            (dir[1] * a[2] - dir[2] * a[1] < 0) ||
            (dir[1] * b[2] - dir[2] * b[1] > 0))
        {
            return false;
        }

        bnd->tmin[0] = a[0] * idir[0];
        bnd->tmin[1] = b[1] * idir[1];
        bnd->tmin[2] = a[2] * idir[2];

        bnd->tmax[0] = b[0] * idir[0];
        bnd->tmax[1] = a[1] * idir[1];
        bnd->tmax[2] = b[2] * idir[2];

        rng->tmin = max3(bnd->tmin) * (real(1) - epsilon_<real>::value());
        rng->tmax = min3(bnd->tmax) * (real(1) + epsilon_<real>::value());

        // compute the intersection distance
        return (rng->tmin < dist);
    }

    static bool test_AABB_RNG_PPM(range_AABB* rng, distbound_AABB* bnd, const vector3& min, const vector3& max, const ray& r, real dist)
    { //1
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] > max[0]) || (org[1] > max[1]) || (org[2] < min[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * b[1] - dir[1] * a[0] < 0) ||
            (dir[0] * a[1] - dir[1] * b[0] > 0) ||
            (dir[0] * a[2] - dir[2] * a[0] > 0) ||
            (dir[0] * b[2] - dir[2] * b[0] < 0) ||
            (dir[1] * b[2] - dir[2] * b[1] < 0) ||
            (dir[1] * a[2] - dir[2] * a[1] > 0))
        {
            return false;
        }

        bnd->tmin[0] = a[0] * idir[0];
        bnd->tmin[1] = a[1] * idir[1];
        bnd->tmin[2] = b[2] * idir[2];

        bnd->tmax[0] = b[0] * idir[0];
        bnd->tmax[1] = b[1] * idir[1];
        bnd->tmax[2] = a[2] * idir[2];

        rng->tmin = max3(bnd->tmin) * (real(1) - epsilon_<real>::value());
        rng->tmax = min3(bnd->tmax) * (real(1) + epsilon_<real>::value());

        // compute the intersection distance
        return (rng->tmin < dist);
    }

    static bool test_AABB_RNG_PPP(range_AABB* rng, distbound_AABB* bnd, const vector3& min, const vector3& max, const ray& r, real dist)
    { //0
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] > max[0]) || (org[1] > max[1]) || (org[2] > max[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * b[1] - dir[1] * a[0] < 0) ||
            (dir[0] * a[1] - dir[1] * b[0] > 0) ||
            (dir[0] * a[2] - dir[2] * b[0] > 0) ||
            (dir[0] * b[2] - dir[2] * a[0] < 0) ||
            (dir[1] * b[2] - dir[2] * a[1] < 0) ||
            (dir[1] * a[2] - dir[2] * b[1] > 0))
        {
            return false;
        }

        bnd->tmin[0] = a[0] * idir[0];
        bnd->tmin[1] = a[1] * idir[1];
        bnd->tmin[2] = a[2] * idir[2];

        bnd->tmax[0] = b[0] * idir[0];
        bnd->tmax[1] = b[1] * idir[1];
        bnd->tmax[2] = b[2] * idir[2];

        rng->tmin = max3(bnd->tmin) * (real(1) - epsilon_<real>::value());
        rng->tmax = min3(bnd->tmax) * (real(1) + epsilon_<real>::value());

        // compute the intersection distance
        return (rng->tmin < dist);
    }

    bool test_AABB(range_AABB* rng, const vector3& min, const vector3& max, const ray& r, real dist)
    {
        static const AABB_RNG_FUNC funcs[] = {
            test_AABB_RNG_PPP, test_AABB_RNG_MPP, test_AABB_RNG_PMP, test_AABB_RNG_MMP, //0,1,2,3
            test_AABB_RNG_PPM, test_AABB_RNG_MPM, test_AABB_RNG_PMM, test_AABB_RNG_MMM, //4,5,6,7
        };

        distbound_AABB bnd;
        return funcs[r.phase()](rng, &bnd, min, max, r, dist);
    }

    bool test_AABB(distbound_AABB* bnd, const vector3& min, const vector3& max, const ray& r, real dist)
    {
        static const AABB_RNG_FUNC funcs[] = {
            test_AABB_RNG_PPP, test_AABB_RNG_MPP, test_AABB_RNG_PMP, test_AABB_RNG_MMP, //0,1,2,3
            test_AABB_RNG_PPM, test_AABB_RNG_MPM, test_AABB_RNG_PMM, test_AABB_RNG_MMM, //4,5,6,7
        };

        range_AABB rng;
        return funcs[r.phase()](&rng, bnd, min, max, r, dist);
    }

    bool test_AABB(range_AABB* rng, const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    {
        if (test_AABB(rng, min, max, r, tmax))
        {
            if (tmin < rng->tmax)
            {
                return true;
            }
        }
        return false;
    }

    bool test_01cube(const vector3& org, const vector3& dir, real dist)
    {
        static const real min[3] = {-1, -1, 0};
        static const real max[3] = {1, 1, 1};

        real xa, ya, za, xb, yb, zb;
        real t0, t1, t2;

        int nRet = 0;
        //get classification
        for (int i = 0; i < 3; i++)
        {
            if (dir[i] < 0)
            {
                nRet |= (1 << (2 - i));
            }
        }

        switch (nRet)
        {
        case 7: //- - -
            if ((org[0] < min[0]) || (org[1] < min[1]) || (org[2] < min[2])) return false;

            xa = min[0] - org[0];
            ya = min[1] - org[1];
            za = min[2] - org[2];
            xb = max[0] - org[0];
            yb = max[1] - org[1];
            zb = max[2] - org[2];

            if ((dir[0] * ya - dir[1] * xb < 0) ||
                (dir[0] * yb - dir[1] * xa > 0) ||
                (dir[0] * zb - dir[2] * xa > 0) ||
                (dir[0] * za - dir[2] * xb < 0) ||
                (dir[1] * za - dir[2] * yb < 0) ||
                (dir[1] * zb - dir[2] * ya > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xb / dir[0];
            t1 = yb / dir[1];
            t2 = zb / dir[2];

            break;
        case 6: //- - +
            if ((org[0] < min[0]) || (org[1] < min[1]) || (org[2] > max[2])) return false;

            xa = min[0] - org[0];
            ya = min[1] - org[1];
            za = min[2] - org[2];
            xb = max[0] - org[0];
            yb = max[1] - org[1];
            zb = max[2] - org[2];

            if ((dir[0] * ya - dir[1] * xb < 0) ||
                (dir[0] * yb - dir[1] * xa > 0) ||
                (dir[0] * zb - dir[2] * xb > 0) ||
                (dir[0] * za - dir[2] * xa < 0) ||
                (dir[1] * za - dir[2] * ya < 0) ||
                (dir[1] * zb - dir[2] * yb > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xb / dir[0];
            t1 = yb / dir[1];
            t2 = za / dir[2];

            break;
        case 5: //- + -
            if ((org[0] < min[0]) || (org[1] > max[1]) || (org[2] < min[2])) return false;

            xa = min[0] - org[0];
            ya = min[1] - org[1];
            za = min[2] - org[2];
            xb = max[0] - org[0];
            yb = max[1] - org[1];
            zb = max[2] - org[2];

            if ((dir[0] * ya - dir[1] * xa < 0) ||
                (dir[0] * yb - dir[1] * xb > 0) ||
                (dir[0] * zb - dir[2] * xa > 0) ||
                (dir[0] * za - dir[2] * xb < 0) ||
                (dir[1] * zb - dir[2] * yb < 0) ||
                (dir[1] * za - dir[2] * ya > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xb / dir[0];
            t1 = ya / dir[1];
            t2 = zb / dir[2];

            break;
        case 4: //- + +
            if ((org[0] < min[0]) || (org[1] > max[1]) || (org[2] > max[2])) return false;

            xa = min[0] - org[0];
            ya = min[1] - org[1];
            za = min[2] - org[2];
            xb = max[0] - org[0];
            yb = max[1] - org[1];
            zb = max[2] - org[2];

            if ((dir[0] * ya - dir[1] * xa < 0) ||
                (dir[0] * yb - dir[1] * xb > 0) ||
                (dir[0] * zb - dir[2] * xb > 0) ||
                (dir[0] * za - dir[2] * xa < 0) ||
                (dir[1] * zb - dir[2] * ya < 0) ||
                (dir[1] * za - dir[2] * yb > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xb / dir[0];
            t1 = ya / dir[1];
            t2 = za / dir[2];

            break;
        case 3: //+ - -
            if ((org[0] > max[0]) || (org[1] < min[1]) || (org[2] < min[2])) return false;

            xa = min[0] - org[0];
            ya = min[1] - org[1];
            za = min[2] - org[2];
            xb = max[0] - org[0];
            yb = max[1] - org[1];
            zb = max[2] - org[2];

            if ((dir[0] * yb - dir[1] * xb < 0) ||
                (dir[0] * ya - dir[1] * xa > 0) ||
                (dir[0] * za - dir[2] * xa > 0) ||
                (dir[0] * zb - dir[2] * xb < 0) ||
                (dir[1] * za - dir[2] * yb < 0) ||
                (dir[1] * zb - dir[2] * ya > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xa / dir[0];
            t1 = yb / dir[1];
            t2 = zb / dir[2];

            break;
        case 2: //+ - +
            if ((org[0] > max[0]) || (org[1] < min[1]) || (org[2] > max[2])) return false;

            xa = min[0] - org[0];
            ya = min[1] - org[1];
            za = min[2] - org[2];
            xb = max[0] - org[0];
            yb = max[1] - org[1];
            zb = max[2] - org[2];

            if ((dir[0] * yb - dir[1] * xb < 0) ||
                (dir[0] * ya - dir[1] * xa > 0) ||
                (dir[0] * za - dir[2] * xb > 0) ||
                (dir[0] * zb - dir[2] * xa < 0) ||
                (dir[1] * za - dir[2] * ya < 0) ||
                (dir[1] * zb - dir[2] * yb > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xa / dir[0];
            t1 = yb / dir[1];
            t2 = za / dir[2];

            break;
        case 1: //+ + -
            if ((org[0] > max[0]) || (org[1] > max[1]) || (org[2] < min[2])) return false;

            xa = min[0] - org[0];
            ya = min[1] - org[1];
            za = min[2] - org[2];
            xb = max[0] - org[0];
            yb = max[1] - org[1];
            zb = max[2] - org[2];

            if ((dir[0] * yb - dir[1] * xa < 0) ||
                (dir[0] * ya - dir[1] * xb > 0) ||
                (dir[0] * za - dir[2] * xa > 0) ||
                (dir[0] * zb - dir[2] * xb < 0) ||
                (dir[1] * zb - dir[2] * yb < 0) ||
                (dir[1] * za - dir[2] * ya > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xa / dir[0];
            t1 = ya / dir[1];
            t2 = zb / dir[2];

            break;
        case 0: //+ + +
            if ((org[0] > max[0]) || (org[1] > max[1]) || (org[2] > max[2])) return false;

            xa = min[0] - org[0];
            ya = min[1] - org[1];
            za = min[2] - org[2];
            xb = max[0] - org[0];
            yb = max[1] - org[1];
            zb = max[2] - org[2];

            if ((dir[0] * yb - dir[1] * xa < 0) ||
                (dir[0] * ya - dir[1] * xb > 0) ||
                (dir[0] * za - dir[2] * xb > 0) ||
                (dir[0] * zb - dir[2] * xa < 0) ||
                (dir[1] * zb - dir[2] * ya < 0) ||
                (dir[1] * za - dir[2] * yb > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xa / dir[0];
            t1 = ya / dir[1];
            t2 = za / dir[2];

            break;
        default:
            assert(0);
            return false;
        }

        if (t1 > t0) t0 = t1;
        if (t2 > t0) t0 = t2;

        return (t0 < dist);
    }

#define EXPAND_VERTEX(A, B, C) vector3(A[0], B[1], C[2])

    static void expand_vertex(vector3 verts[8], const vector3& min, const vector3& max)
    {
        verts[0] = EXPAND_VERTEX(min, min, min);
        verts[1] = EXPAND_VERTEX(min, min, max);
        verts[2] = EXPAND_VERTEX(min, max, min);
        verts[3] = EXPAND_VERTEX(min, max, max);
        verts[4] = EXPAND_VERTEX(max, min, min);
        verts[5] = EXPAND_VERTEX(max, min, max);
        verts[6] = EXPAND_VERTEX(max, max, min);
        verts[7] = EXPAND_VERTEX(max, max, max);
    }
#undef EXPAND_VERTEX

    void get_plane_distance(range_AABB* rng, const vector3& min, const vector3& max, const vector3& org, const vector3& dir)
    {
        vector3 verts[8];
        real dist[8];

        //get corner vertices.
        expand_vertex(verts, min, max);

        //get each distance of vetex from org.
        for (int i = 0; i < 8; i++)
        {
            dist[i] = dot(dir, verts[i] - org);
        }

        //find min, max.
        real tmin, tmax;
        tmin = tmax = dist[0];
        for (int i = 1; i < 8; i++)
        {
            if (tmin > dist[i]) tmin = dist[i];
            if (tmax < dist[i]) tmax = dist[i];
        }

        rng->tmin = tmin * (real(1) - epsilon_<real>::value());
        rng->tmax = tmax * (real(1) + epsilon_<real>::value());
        ;
    }

    void get_distance(range_AABB* rng, const vector3& min, const vector3& max, const ray& r)
    {
        get_plane_distance(rng, min, max, r.origin(), r.direction());
    }
}
