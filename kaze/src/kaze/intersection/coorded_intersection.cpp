#include "coorded_intersection.h"


#include "test_info.h"

namespace kaze
{

    coorded_intersection::coorded_intersection(
        const std::shared_ptr<intersection>& ptr,
        const vector3& o, const vector3& x, const vector3& y, const vector3& z)
        : ptr_(ptr)
    {
        matrix4 shift = mat4_gen::translation(o[0], o[1], o[2]);
        matrix4 rotate = matrix4(
            x[0], y[0], z[0], 0,
            x[1], y[1], z[1], 0,
            x[2], y[2], z[2], 0,
            0, 0, 0, 1);

        matrix4 m = shift * rotate;
        matrix4 im = ~m;

        w2c_ = im; //
    }

    coorded_intersection::coorded_intersection(
        const std::shared_ptr<intersection>& ptr, const matrix4& mat)
        : ptr_(ptr), w2c_(mat)
    {
    }

    bool coorded_intersection::test(const ray& r, real dist) const
    {
        return ptr_->test(r, dist);
    }

    bool coorded_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (ptr_->test(info, r, dist))
        {
            if (info->p_intersection) info->p_intersection->finalize(info, r, info->distance);
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void coorded_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        info->coord = w2c_ * info->position;
    }

    vector3 coorded_intersection::min() const
    {
        return ptr_->min();
    }

    vector3 coorded_intersection::max() const
    {
        return ptr_->max();
    }
}
