#ifndef KAZE_TPOT_INTERSECTION_H
#define KAZE_TPOT_INTERSECTION_H

#include "bounded_intersection.h"
#include "transformer.h"

#include <vector>

namespace kaze
{

    class teapot_intersection_imp;

    class teapot_intersection : public bounded_intersection
    {
    public:
        teapot_intersection(const transformer& tr);
        ~teapot_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    private:
        teapot_intersection_imp* imp_;
    };

    typedef teapot_intersection tpot_intersection;
}

#endif
