#ifdef _MSC_VER
#pragma warning(disable : 4311)
#endif

#include <mutex>
#include "mailbox_intersection.h"

#include "test_info.h"

//#define KAZE_TLS
//#define KAZE_RWLOCK_USE
#define KAZE_MAXIMUM_OBJECT_NUM 5000

#include "mapset_selection.h"
#define MAP_TYPE KAZE_MAP_TYPE

#ifdef KAZE_TLS
#ifdef _MSC_VER
#if TLS_MINIMUM_AVAILABLE > KAZE_MAXIMUM_OBJECT_NUM
#define THREAD_VAR __declspec(thread)
#define KAZE_TLS_USE
#endif
#elif defined(__unix__) || defined(__APPLE__)
#if PTHREAD_KEYS_MAX > KAZE_MAXIMUM_OBJECT_NUM
#define THREAD_VAR __thread
#define KAZE_TLS_USE
#endif
#endif
#endif

namespace kaze
{

    namespace
    {

#ifdef _WIN32
        typedef DWORD thread_id_t;
        inline thread_id_t get_current_thread_id()
        {
            return ::GetCurrentThreadId();
        }
#endif

#if defined(__unix__) || defined(__APPLE__)
        typedef unsigned long thread_id_t;
        inline thread_id_t get_current_thread_id()
        {
            return (thread_id_t)pthread_self();
        }
#endif

//using FNV1-a
#define FNV1_32_INIT ((Fnv32_t)0x811c9dc5)
#define FNV1_32A_INIT FNV1_32_INIT
/*
 * 32 bit magic FNV-1a prime
 */
#define FNV_32_PRIME ((Fnv32_t)0x01000193)

        /*
 * 32 bit FNV-0 hash type
 */
        typedef uint32_t Fnv32_t;

        /*
 * fnv_32a_buf - perform a 32 bit Fowler/Noll/Vo FNV-1a hash on a buffer
 *
 * input:
 *	buf	- start of buffer to hash
 *	len	- length of buffer in octets
 *	hval	- previous hash value or 0 if first call
 *
 * returns:
 *	32 bit hash as a static hash type
 *
 * NOTE: To use the recommended 32 bit FNV-1a hash, use FNV1_32A_INIT as the
 * 	 hval arg on the first call to either fnv_32a_buf() or fnv_32a_str().
 */
        Fnv32_t fnv_32a_buf(void* buf, size_t len, Fnv32_t hval)
        {
            unsigned char* bp = (unsigned char*)buf; /* start of buffer */
            unsigned char* be = bp + len;            /* beyond end of buffer */

            /*
     * FNV-1a hash each octet in the buffer
     */
            while (bp < be)
            {

                /* xor the bottom with the current octet */
                hval ^= (Fnv32_t)*bp++;

/* multiply by the 32 bit FNV magic prime mod 2^32 */
#if defined(NO_FNV_GCC_OPTIMIZATION)
                hval *= FNV_32_PRIME;
#else
                hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
#endif
            }

            /* return our new hash value */
            return hval;
        }

        static uint32_t UniqueThreadID()
        {
            uint32_t val = get_current_thread_id();
            return fnv_32a_buf(&val, 4, FNV1_32A_INIT);
        }

        static uint32_t UniqueThreadID(uint32_t val)
        {
            return fnv_32a_buf(&val, 4, FNV1_32A_INIT);
        }

#ifndef KAZE_TLS_USE

        class mailbox
        {
        public:
            typedef MAP_TYPE<thread_id_t, ray_id_t> map_type;
            typedef map_type::value_type value_type;
            typedef map_type::iterator iterator;

        public:
            mailbox() {}

            bool find(const ray& r) const
            {
                return this->find(r.id());
            }

            bool find(ray_id_t rid) const
            {
                return this->find(get_current_thread_id(), rid);
            }
#ifdef KAZE_RWLOCK_USE
            bool find(thread_id_t tid, ray_id_t rid) const
            {
                {
                    system::rwlock_rdlocker lk(&rwlck);
                    iterator i = mv_.find(tid);

                    if (i != mv_.end())
                    { //find
                        bool bRet = (i->second == rid);
                        i->second = rid;
                        return bRet;
                    }
                }
                {
                    system::rwlock_wrlocker lk(&rwlck);
                    mv_.insert(map_type::value_type(tid, rid));
                    return false;
                }
            }

#else
            bool find(thread_id_t tid, ray_id_t rid) const
            {
                std::lock_guard<std::mutex> lck(mtx); //TODO: Replace "Reader/Writer Lock".
                iterator i = mv_.find(tid);

                bool bRet = false;
                if (i != mv_.end())
                { //find
                    bRet = (i->second == rid);
                    i->second = rid;
                }
                else
                {
                    mv_.insert(map_type::value_type(tid, rid));
                }
                return bRet;
            }
#endif

            bool find(thread_id_t tid, const ray& r) const
            {
                return this->find(tid, r.id());
            }

        private:
            mailbox(const mailbox& rhs);
            mailbox& operator=(const mailbox& rhs);
        private:
            mutable std::mutex mtx;
            mutable map_type mv_;
        };

#else
        class mailbox
        {
        public:
#ifdef _WIN32
            mailbox()
            {
                tls_key_ = ::TlsAlloc();
                if (tls_key_ == 0xFFFFFFFF) throw std::bad_alloc("mailbox");
            }
            ~mailbox() { ::TlsFree(tls_key_); }

            bool find(ray_id_t rid) const
            {
                ray_id_t trid = (ray_id_t)::TlsGetValue(tls_key_);
                bool bRet = (trid == rid);
                ::TlsSetValue(tls_key_, (LPVOID)rid);
                return bRet;
            }
#else
            mailbox()
            {
                if (pthread_key_create(&tls_key_, NULL) != 0) throw std::bad_alloc("mailbox");
            }
            ~mailbox() { pthread_key_delete(tls_key_); }

            bool find(ray_id_t rid) const
            {
                ray_id_t trid = (ray_id_t)pthread_getspecific(tls_key_);
                bool bRet = (trid == rid);
                pthread_setspecific(tls_key_, rid);
                return bRet;
            }

#endif
            bool find(const ray& r) const
            {
                return this->find(r.id());
            }

        private:
            mailbox(const mailbox& rhs);
            mailbox& operator=(const mailbox& rhs);
#ifdef _WIN32
            DWORD tls_key_; //
#else
            pthread_key_t tls_key_;
#endif
        };
#endif

        class multi_mailbox
        {
        public:
            static const unsigned int NUM = 8;
            static const unsigned int MASK = NUM - 1;

        public:
            bool find(const ray& r) const
            {
                return this->find(r.id());
            }

            bool find(ray_id_t rid) const
            {
                return this->find(get_current_thread_id(), rid);
            }

            bool find(thread_id_t tid, ray_id_t rid) const
            {
                unsigned int nP = MASK & UniqueThreadID(tid);
                return mbs_[nP].find(tid, rid);
            }

        private:
            mailbox mbs_[NUM];
        };

    } //end of unnamed namespace

    class mailbox_intersection_imp
    {
    public:
        mailbox_intersection_imp(intersection* inter) : inter_(inter) {}
        mailbox_intersection_imp(const std::shared_ptr<intersection>& inter) : inter_(inter) {}

    public:
        bool test(const ray& r, real dist) const
        {
            if (!this->is_visited(r))
            {
                return inter_->test(r, dist);
            }
            return false;
        }
        bool test(test_info* info, const ray& r, real dist) const
        {
            if (!this->is_visited(r))
            {
                return inter_->test(info, r, dist);
            }
            return false;
        }

        void finalize(test_info* info, const ray& r, real dist) const
        {
            inter_->finalize(info, r, dist);
        }

        bool test(const ray& r, real dist, ray_id_t rid) const
        {
            if (!this->is_visited(rid))
            {
                return inter_->test(r, dist);
            }
            return false;
        }

        bool test(test_info* info, const ray& r, real dist, ray_id_t rid) const
        {
            if (!this->is_visited(rid))
            {
                return inter_->test(info, r, dist);
            }
            return false;
        }

        vector3 min() const
        {
            return inter_->min();
        }
        vector3 max() const
        {
            return inter_->max();
        }

        const intersection* inter() const
        {
            return inter_.get();
        }

        bool is_visited(ray_id_t rid) const
        {
            //return false;
            return mb_.find(rid);
        }

        bool is_visited(const ray& r) const
        {
            return this->is_visited(r.id());
        }

    private:
        std::shared_ptr<intersection> inter_;
        multi_mailbox mb_;
    };

    //--------------------------------------------------------------------

    mailbox_intersection::mailbox_intersection(intersection* inter)
    {
        imp_ = new mailbox_intersection_imp(inter);
    }

    mailbox_intersection::mailbox_intersection(const std::shared_ptr<intersection>& inter)
    {
        imp_ = new mailbox_intersection_imp(inter);
    }

    mailbox_intersection::~mailbox_intersection()
    {
        delete imp_;
    }

    bool mailbox_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool mailbox_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }

    void mailbox_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        return imp_->finalize(info, r, dist);
    }

    bool mailbox_intersection::test(const ray& r, real dist, ray_id_t rid) const
    {
        return imp_->test(r, dist, rid);
    }

    bool mailbox_intersection::test(test_info* info, const ray& r, real dist, ray_id_t rid) const
    {
        return imp_->test(info, r, dist, rid);
    }

    bool mailbox_intersection::is_visited(ray_id_t rid) const
    {
        return imp_->is_visited(rid);
    }

    bool mailbox_intersection::is_visited(const ray& r) const
    {
        return imp_->is_visited(r);
    }

    vector3 mailbox_intersection::min() const
    {
        return imp_->min();
    }

    vector3 mailbox_intersection::max() const
    {
        return imp_->max();
    }

    const intersection* mailbox_intersection::inter() const
    {
        return imp_->inter();
    }
}
