#ifndef _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES
#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1
#endif

#include "transformed_intersection.h"
#include "intersection_ex.h"
#include "test_info.h"

#include <cstring>

#include <algorithm>
#include <iostream>

#include <stdarg.h>

namespace kaze
{

    static inline vector3 mul_n(const matrix3& m, const vector3& v)
    {
        return vector3(
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
    }

    static inline vector3 mul_n(const matrix4& m, const vector3& v)
    {
        return vector3(
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
    }

    static inline vector3 mul_v(const matrix4& m, const vector3& v)
    {
        return vector3(
            m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2],
            m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2],
            m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2]);
    }

    static inline vector3 mul_p(const matrix4& m, const vector3& v)
    {
        real iR = real(1) / (m[3][0] * v[0] + m[3][1] * v[1] + m[3][2] * v[2] + m[3][3]);
        return vector3(
            (m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2] + m[0][3]) * iR,
            (m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2] + m[1][3]) * iR,
            (m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2] + m[2][3]) * iR);
    }

    static vector3 safe_normalize(const vector3& v)
    {
        static const real EPSILON = values::epsilon();
        using namespace std;

        real l = v.sqr_length();
        if (l < EPSILON) return vector3(0, 0, 0);
        return v * (real(1) / sqrt(l));
    }
    //-------------------------------------------------------------------------
    m4_transformed_intersection::m4_transformed_intersection(intersection* inter, const matrix4& m)
        : inter_(inter)
    {
        m_ = m;
        im_ = ~m;

        vector3 min = inter_->min();
        vector3 max = inter_->max();

        vector3 vtx[8];
        vtx[0] = vector3(min[0], min[1], min[2]);
        vtx[1] = vector3(min[0], min[1], max[2]);
        vtx[2] = vector3(min[0], max[1], min[2]);
        vtx[3] = vector3(min[0], max[1], max[2]);
        vtx[4] = vector3(max[0], min[1], min[2]);
        vtx[5] = vector3(max[0], min[1], max[2]);
        vtx[6] = vector3(max[0], max[1], min[2]);
        vtx[7] = vector3(max[0], max[1], max[2]);

        vector3 nmin, nmax;
        nmin = nmax = m * (vtx[0]);
        for (int i = 1; i < 8; i++)
        {
            vector3 mv = m * (vtx[i]);
            for (int p = 0; p < 3; p++)
            {
                if (nmin[p] > mv[p]) nmin[p] = mv[p];
                if (nmax[p] < mv[p]) nmax[p] = mv[p];
            }
        }
        min_ = nmin;
        max_ = nmax;
    }

    m4_transformed_intersection::m4_transformed_intersection(const std::shared_ptr<intersection>& inter, const matrix4& m)
        : inter_(inter)
    {
        m_ = m;
        im_ = ~m;

        vector3 min = inter_->min();
        vector3 max = inter_->max();

        vector3 vtx[8];
        vtx[0] = vector3(min[0], min[1], min[2]);
        vtx[1] = vector3(min[0], min[1], max[2]);
        vtx[2] = vector3(min[0], max[1], min[2]);
        vtx[3] = vector3(min[0], max[1], max[2]);
        vtx[4] = vector3(max[0], min[1], min[2]);
        vtx[5] = vector3(max[0], min[1], max[2]);
        vtx[6] = vector3(max[0], max[1], min[2]);
        vtx[7] = vector3(max[0], max[1], max[2]);

        vector3 nmin, nmax;
        nmin = nmax = m * (vtx[0]);
        for (int i = 1; i < 8; i++)
        {
            vector3 mv = m * (vtx[i]);
            for (int p = 0; p < 3; p++)
            {
                if (nmin[p] > mv[p]) nmin[p] = mv[p];
                if (nmax[p] < mv[p]) nmax[p] = mv[p];
            }
        }
        min_ = nmin;
        max_ = nmax;
    }

    bool m4_transformed_intersection::test(const ray& r, real dist) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, dist))
        {
            dist = std::min<real>(rng.tmax, dist);
            return test_internal(r, dist);
        }
        return false;
    }

    bool m4_transformed_intersection::test(test_info* info, const ray& r, real dist) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, dist))
        {
            dist = std::min<real>(rng.tmax, dist);
            return test_internal(info, r, dist);
        }
        return false;
    }

    bool m4_transformed_intersection::test(const ray& r, real tmin, real tmax) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            //tmin = std::max<real>(rng.tmin, tmin);
            tmax = std::min<real>(rng.tmax, tmax);
            return test_internal(r, tmax);
        }
        return false;
    }
    bool m4_transformed_intersection::test(test_info* info, const ray& r, real tmin, real tmax) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            //tmin = std::max<real>(rng.tmin, tmin);
            tmax = std::min<real>(rng.tmax, tmax);
            return test_internal(info, r, tmax);
        }
        return false;
    }

    bool m4_transformed_intersection::test_internal(const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon();
        matrix4 im = im_;

        vector3 morg = mul_p(im, r.origin());
        vector3 mdir = mul_v(im, r.direction());

        real d = mdir.length();
        if (d < EPSILON) return false;
        real mdist = d * dist;
        if (values::far() < mdist) mdist = values::far();

        real id = real(1) / d;
        mdir *= id;

        return inter_->test(ray(morg, mdir, r.time()), mdist);
    }

    bool m4_transformed_intersection::test_internal(test_info* info, const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon();
        matrix4 im = im_;
        vector3 morg = mul_p(im, r.origin());
        vector3 mdir = mul_v(im, r.direction());

        real d = mdir.length();
        if (d < EPSILON) return false;
        real mdist = d * dist;
        if (values::far() < mdist) mdist = values::far();

        real id = real(1) / d;
        mdir *= id;

        ray mray(morg, mdir, r.time());
        if (inter_->test(info, mray, mdist))
        {
            if (info->p_intersection)
            {
                info->p_intersection->finalize(info, mray, info->distance);
            }
            info->distance = id * info->distance;
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void m4_transformed_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        matrix4 m = m_;
        matrix4 im = im_;

        info->position = (r.origin() + info->distance * r.direction());
        info->geometric = (safe_normalize(mul_n(im, info->geometric)));
        info->normal = (safe_normalize(mul_n(im, info->normal)));
        info->tangent = (safe_normalize(mul_n(im, info->tangent)));
        info->binormal = (safe_normalize(mul_n(im, info->binormal)));
        //--------------------------------------------------------------------
        info->p1 = mul_p(m, info->p1); //
        info->p2 = mul_p(m, info->p2); //
        info->p3 = mul_p(m, info->p3); //
                                       //--------------------------------------------------------------------
    }

    vector3 m4_transformed_intersection::min() const
    {
        return min_;
    }

    vector3 m4_transformed_intersection::max() const
    {
        return max_;
    }
}
