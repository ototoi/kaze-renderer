#include "composite_bezier_patch_intersection.h"
#include "bvh_composite_intersection.h"

#include "bezier_patch.hpp"
#include "multi_bezier_patch.hpp"
#include "trimmed_multi_bezier_patch.hpp"

#include "patch/bezier_patch_loader.h"
#include "patch/bezier_patch_saver.h"
#include "patch/convert_bezier_patch.h"

#include "multi_bezier_patch_intersection.h"
#include "trimmed_multi_bezier_patch_intersection.h"

#include <memory>
//#include <iostream>
//#include "logger.h"

namespace kaze
{
    namespace
    {

        class t_patch_saver : public bezier_patch_saver
        {
        public:
            bool set_patches(size_t sz)
            {
                mv_.reserve(sz);
                return true;
            }
            bool set_patch(size_t i, io_bezier_patch* f)
            {
                trimmed_multi_bezier_patch<vector3, vector2> patch;
                if (!convert_bezier_patch(patch, *f)) return false;
                mv_.push_back(patch);
                return true;
            }

        public:
            std::vector<trimmed_multi_bezier_patch<vector3, vector2> > mv_;
        };

        void Transform(const transformer& tr, bezier_patch<vector3>& patch)
        {
            int nu = patch.get_nu();
            int nv = patch.get_nv();

            const std::vector<vector3>& cp = patch.get_cp();
            std::vector<vector3> tmp(cp.size());
            for (size_t i = 0; i < cp.size(); i++)
            {
                tmp[i] = tr.transform_p(cp[i]);
            }
            patch.set_cp(nu, nv, tmp);
        }

        void Transform(const transformer& tr, trimmed_multi_bezier_patch<vector3, vector2>& patch)
        {
            int nPu = patch.get_patch_size_u();
            int nPv = patch.get_patch_size_v();
            for (int j = 0; j < nPv; j++)
            {
                for (int i = 0; i < nPu; i++)
                {
                    Transform(tr, patch.get_patch_at(i, j));
                }
            }
        }
    }

    class composite_bezier_patch_intersection_imp
    {
    public:
        composite_bezier_patch_intersection_imp(const bezier_patch_loader& pl, const transformer& tr);
        ~composite_bezier_patch_intersection_imp();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        composite_intersection* bvh_;
    };
    //------------------------------------------------------------------------

    composite_bezier_patch_intersection_imp::composite_bezier_patch_intersection_imp(const bezier_patch_loader& loader, const transformer& tr) : bvh_(NULL)
    {
        std::unique_ptr<bvh_composite_intersection> ap(new bvh_composite_intersection());

        std::vector<trimmed_multi_bezier_patch<vector3, vector2> > tmp;
        {
            t_patch_saver tps;
            if (!loader.load(&tps)) throw "composite_bezier_patch_intersection";
            tmp.swap(tps.mv_);
        }

        for (size_t i = 0; i < tmp.size(); i++)
        {
            Transform(tr, tmp[i]);
            ap->add(new trimmed_multi_bezier_patch_intersection(tmp[i], tmp[i].get_outer_loops(), tmp[i].get_inner_loops()));
        }
        ap->construct();
        bvh_ = ap.release();
    }

    composite_bezier_patch_intersection_imp::~composite_bezier_patch_intersection_imp()
    {
        if (bvh_) delete bvh_;
    }
    bool composite_bezier_patch_intersection_imp::test(const ray& r, real dist) const
    {
        return bvh_->test(r, dist);
    }
    bool composite_bezier_patch_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        return bvh_->test(info, r, dist);
    }
    vector3 composite_bezier_patch_intersection_imp::min() const
    {
        return bvh_->min();
    }
    vector3 composite_bezier_patch_intersection_imp::max() const
    {
        return bvh_->max();
    }

    //------------------------------------------------------------------------
    composite_bezier_patch_intersection::composite_bezier_patch_intersection(const bezier_patch_loader& pl)
    {
        no_transformer tr;
        imp_ = new composite_bezier_patch_intersection_imp(pl, tr);
    }

    composite_bezier_patch_intersection::composite_bezier_patch_intersection(const bezier_patch_loader& pl, const transformer& tr)
    {
        imp_ = new composite_bezier_patch_intersection_imp(pl, tr);
    }

    composite_bezier_patch_intersection::~composite_bezier_patch_intersection()
    {
        delete imp_;
    }

    bool composite_bezier_patch_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }
    bool composite_bezier_patch_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }
    void composite_bezier_patch_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }
    vector3 composite_bezier_patch_intersection::min() const
    {
        return imp_->min();
    }
    vector3 composite_bezier_patch_intersection::max() const
    {
        return imp_->max();
    }
}
