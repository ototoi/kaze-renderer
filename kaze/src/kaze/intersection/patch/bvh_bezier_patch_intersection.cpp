#include "bvh_bezier_patch_intersection.h"
#include "bezier_patch_intersection.h"
#include "intersection_ex.h"
#include <memory>

namespace kaze
{
    namespace
    {
        class bounded_bezier_intersection : public bounded_intersection
        {
        public:
            bounded_bezier_intersection(std::shared_ptr<bezier_patch_intersection>& inter, const vector3& min, const vector3& max)
                : inter_(inter), min_(min), max_(max)
            {
            }

            bool test(const ray& r, real dist) const
            {
                real tmin = 0;
                real tmax = dist;
                range_AABB rng;
                if (test_AABB(&rng, min_, max_, r, tmin, tmax))
                {
                    tmin = std::max<real>(tmin, rng.tmin);
                    tmax = std::min<real>(tmax, rng.tmax);
                    return inter_->test(r, tmin, tmax);
                }
                return false;
            }
            bool test(test_info* info, const ray& r, real dist) const
            {
                real tmin = 0;
                real tmax = dist;
                range_AABB rng;
                if (test_AABB(&rng, min_, max_, r, tmin, tmax))
                {
                    tmin = std::max<real>(tmin, rng.tmin);
                    tmax = std::min<real>(tmax, rng.tmax);
                    return inter_->test(info, r, tmin, tmax);
                }
                return false;
            }
            void finalize(test_info* info, const ray& r, real dist) const
            {
                ; //
            }
            vector3 min() const { return min_; }
            vector3 max() const { return max_; }
        protected:
            std::shared_ptr<bezier_patch_intersection> inter_;
            vector3 min_;
            vector3 max_;
        };

        struct minmax_
        {
            vector3 min;
            vector3 max;
        };

        static void GetSeparate(std::vector<minmax_>& out, const bezier_patch<vector3>& patch)
        {
            static const real EPS = values::epsilon() * 1024;

            int nu = patch.get_nu();
            int nv = patch.get_nv();
            real du = real(1.0) / (nu - 1);
            real dv = real(1.0) / (nv - 1);

            std::vector<bezier_patch<vector3> > patches_uv((nu - 1) * (nv - 1));

            std::vector<bezier_patch<vector3> > patches_u(nu - 1);
            {
                std::vector<real> rng_u(nu);
                rng_u[0] = 0;
                rng_u[nu - 1] = 1;
                for (int i = 1; i < nu - 1; i++)
                {
                    rng_u[i] = real(i) / real(i + 1);
                }
                //0 1/2 2/3
                bezier_patch<vector3> pat(patch);
                for (int i = nu - 2; i > 0; i--)
                {
                    bezier_patch<vector3> tmp[2];
                    pat.split_u(tmp, rng_u[i]);
                    patches_u[i].swap(tmp[1]);
                    pat.swap(tmp[0]);
                }
                patches_u[0].swap(pat);
            }

            {
                std::vector<real> rng_v(nv);
                rng_v[0] = 0;
                rng_v[nv - 1] = 1;
                for (int i = 1; i < nv - 1; i++)
                {
                    rng_v[i] = real(i) / real(i + 1);
                }
                //0 1/2 2/3
                for (int j = 0; j < nu - 1; j++)
                {
                    std::vector<bezier_patch<vector3> > patches_v(nv - 1);

                    bezier_patch<vector3> pat(patches_u[j]);
                    for (int i = nv - 2; i > 0; i--)
                    {
                        bezier_patch<vector3> tmp[2];
                        pat.split_v(tmp, rng_v[i]);
                        patches_v[i].swap(tmp[1]);
                        pat.swap(tmp[0]);
                    }
                    patches_v[0].swap(pat);

                    for (int k = 0; k < nv - 1; k++)
                    {
                        patches_uv[(nu - 1) * k + j].swap(patches_v[k]);
                    }
                }
            }

            out.reserve((nu - 1) * (nv - 1));
            for (int j = 0; j < nv - 1; j++)
            {
                for (int i = 0; i < nu - 1; i++)
                {
                    minmax_ tmp;
                    tmp.min = patches_uv[(nu - 1) * j + i].min();
                    tmp.max = patches_uv[(nu - 1) * j + i].max();

                    vector3 wid = tmp.max - tmp.min;
                    int plane = 0;
                    if (wid[1] > wid[plane]) plane = 1;
                    if (wid[2] > wid[plane]) plane = 2;

                    real EPSILON = EPS;

                    EPSILON = std::max(EPSILON, wid[plane] * 0.01);

                    tmp.min -= vector3(EPSILON, EPSILON, EPSILON);
                    tmp.max += vector3(EPSILON, EPSILON, EPSILON);

                    out.push_back(tmp);
                }
            }
        }
    }

    bvh_bezier_patch_intersection::bvh_bezier_patch_intersection(const bezier_patch<vector3>& patch)
        : bvh_(NULL)
    {
        std::unique_ptr<bvh_composite_intersection> bvh(new bvh_composite_intersection());
        std::shared_ptr<bezier_patch_intersection> inter(new bezier_patch_intersection(patch));
        std::vector<minmax_> m;
        GetSeparate(m, patch);
        for (size_t i = 0; i < m.size(); i++)
        {
            bvh->add(new bounded_bezier_intersection(inter, m[i].min, m[i].max));
        }
        bvh->construct();
        bvh_ = bvh.release();
    }

    bvh_bezier_patch_intersection::bvh_bezier_patch_intersection(const bezier_patch<vector3>& patch, real u0, real u1, real v0, real v1)
        : bvh_(NULL)
    {
        std::unique_ptr<bvh_composite_intersection> bvh(new bvh_composite_intersection());
        std::shared_ptr<bezier_patch_intersection> inter(new bezier_patch_intersection(patch, u0, u1, v0, v1));
        std::vector<minmax_> m;
        GetSeparate(m, patch);
        for (size_t i = 0; i < m.size(); i++)
        {
            bvh->add(new bounded_bezier_intersection(inter, m[i].min, m[i].max));
        }
        bvh->construct();
        bvh_ = bvh.release();
    }

    bvh_bezier_patch_intersection::~bvh_bezier_patch_intersection()
    {
        delete bvh_;
    }

    bool bvh_bezier_patch_intersection::test(const ray& r, real dist) const
    {
        return bvh_->test(r, dist);
    }

    bool bvh_bezier_patch_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return bvh_->test(info, r, dist);
    }
    void bvh_bezier_patch_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }
    vector3 bvh_bezier_patch_intersection::min() const
    {
        return bvh_->min();
    }
    vector3 bvh_bezier_patch_intersection::max() const
    {
        return bvh_->max();
    }
}
