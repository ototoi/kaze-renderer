#include "phong_patch_intersection.h"

/*
 * See "Direct Ray Tracing of Phong Tessellation"
 */

namespace kaze
{
    namespace
    {

        struct uvt_info
        {
            real u;
            real v;
            real t;
        };

        static bool test_phong_patch(uvt_info* info, const vector3 p[3], const vector3 n[3], const ray& r, real tmin, real tmax)
        {
            //

            //
            return false;
        }
    }

    phong_patch_intersection::phong_patch_intersection()
    {
    }

    phong_patch_intersection::~phong_patch_intersection()
    {
    }

    bool phong_patch_intersection::test(const ray& r, real dist) const
    {
        return false;
    }

    bool phong_patch_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return false;
    }

    void phong_patch_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
    }

    vector3 phong_patch_intersection::min() const
    {
        return min_;
    }

    vector3 phong_patch_intersection::max() const
    {
        return max_;
    }
}
