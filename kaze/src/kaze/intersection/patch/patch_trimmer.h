#ifndef KAZE_PATCH_TRIMMER_H
#define KAZE_PATCH_TRIMMER_H

namespace kaze
{

    class patch_trimmer
    {
    public:
        virtual ~patch_trimmer() {}
        virtual bool test(real u, real v) const = 0;
    };
}

#endif
