#ifndef KAZE_TRIMMED_NURBS_PATCH_INTERSECTION_H
#define KAZE_TRIMMED_NURBS_PATCH_INTERSECTION_H

#include "bounded_intersection.h"
#include "nurbs_curve.hpp"
#include "nurbs_patch.hpp"
#include <vector>

namespace kaze
{

    class trimmed_nurbs_patch_intersection : public bounded_intersection
    {
    public:
        trimmed_nurbs_patch_intersection(
            const nurbs_patch<vector3>& patch,
            const std::vector<std::vector<nurbs_curve<vector2> > >& outer_loops,
            const std::vector<std::vector<nurbs_curve<vector2> > >& inner_loops);
        ~trimmed_nurbs_patch_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        intersection* inter_;
    };
}

#endif