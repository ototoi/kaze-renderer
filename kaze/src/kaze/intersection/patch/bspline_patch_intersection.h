#ifndef KAZE_BSPLINE_PATCH_INTERSECTION_H
#define KAZE_BSPLINE_PATCH_INTERSECTION_H

#include "bounded_intersection.h"
#include "bspline_patch.hpp"

namespace kaze
{

    class bspline_patch_intersection : public bounded_intersection
    {
    public:
        bspline_patch_intersection(const bspline_patch<vector3>& patch);
        ~bspline_patch_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    public:
        intersection* inter_;
    };
}

#endif
