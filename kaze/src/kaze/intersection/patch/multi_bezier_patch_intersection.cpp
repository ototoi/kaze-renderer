#include "multi_bezier_patch_intersection.h"

#include "bvh_composite_intersection.h"
#include "bezier_patch_intersection.h"
#include "bvh_bezier_patch_intersection.h"

#include <memory>

namespace kaze
{

    struct ranged_patch
    {
        bezier_patch<vector3> patch;
        real u0;
        real u1;
        real v0;
        real v1;
    };

    static void GetSeparate(std::vector<ranged_patch>& out, const bezier_patch<vector3>& patch, real u0, real u1, real v0, real v1)
    {
        int nu = patch.get_nu();
        int nv = patch.get_nv();
        real du = real(1.0) / (nu - 1);
        real dv = real(1.0) / (nv - 1);

        std::vector<bezier_patch<vector3> > patches_uv((nu - 1) * (nv - 1));

        std::vector<bezier_patch<vector3> > patches_u(nu - 1);
        {
            std::vector<real> rng_u(nu);
            rng_u[0] = 0;
            rng_u[nu - 1] = 1;
            for (int i = 1; i < nu - 1; i++)
            {
                rng_u[i] = real(i) / real(i + 1);
            }
            //0 1/2 2/3
            bezier_patch<vector3> pat(patch);
            for (int i = nu - 2; i > 0; i--)
            {
                bezier_patch<vector3> tmp[2];
                pat.split_u(tmp, rng_u[i]);
                patches_u[i].swap(tmp[1]);
                pat.swap(tmp[0]);
            }
            patches_u[0].swap(pat);
        }

        {
            std::vector<real> rng_v(nv);
            rng_v[0] = 0;
            rng_v[nv - 1] = 1;
            for (int i = 1; i < nv - 1; i++)
            {
                rng_v[i] = real(i) / real(i + 1);
            }
            //0 1/2 2/3
            for (int j = 0; j < nu - 1; j++)
            {
                std::vector<bezier_patch<vector3> > patches_v(nv - 1);

                bezier_patch<vector3> pat(patches_u[j]);
                for (int i = nv - 2; i > 0; i--)
                {
                    bezier_patch<vector3> tmp[2];
                    pat.split_v(tmp, rng_v[i]);
                    patches_v[i].swap(tmp[1]);
                    pat.swap(tmp[0]);
                }
                patches_v[0].swap(pat);

                for (int k = 0; k < nv - 1; k++)
                {
                    patches_uv[(nu - 1) * k + j].swap(patches_v[k]);
                }
            }
        }

        out.reserve((nu - 1) * (nv - 1));
        for (int j = 0; j < nv - 1; j++)
        {
            for (int i = 0; i < nu - 1; i++)
            {
                ranged_patch tmp;
                tmp.patch = patches_uv[(nu - 1) * j + i];
                tmp.u0 = u0 + (u1 - u0) * du * i;
                tmp.u1 = u0 + (u1 - u0) * du * (i + 1);
                tmp.v0 = v0 + (v1 - v0) * dv * j;
                tmp.v1 = v0 + (v1 - v0) * dv * (j + 1);
                out.push_back(tmp);
            }
        }
    }
    //----------------------------------------------------------------------------------------------------------------------

    multi_bezier_patch_intersection::multi_bezier_patch_intersection(const multi_bezier_patch<vector3>& patch)
        : cmp_(NULL)
    {
        std::unique_ptr<bvh_composite_intersection> bvh(new bvh_composite_intersection());
        int nPu = patch.get_patch_size_u();
        int nPv = patch.get_patch_size_v();
        for (int v = 0; v < nPv; v++)
        {
            real v0 = patch.get_knot_at_v(v);
            real v1 = patch.get_knot_at_v(v + 1);

            for (int u = 0; u < nPu; u++)
            {
                real u0 = patch.get_knot_at_u(u);
                real u1 = patch.get_knot_at_u(u + 1);
#if 0
		            std::vector<ranged_patch> out;
		            GetSeparate(out, patch.get_patch_at(u,v), u0,u1,v0,v1);
		            for(int k=0;k<out.size();k++){
			            bvh->add( new bezier_patch_intersection(out[k].patch,out[k].u0,out[k].u1,out[k].v0,out[k].v1));
		            }
#else
                bvh->add(new bezier_patch_intersection(patch.get_patch_at(u, v), u0, u1, v0, v1));
//bvh->add(new bvh_bezier_patch_intersection(patch.get_patch_at(u,v),u0,u1,v0,v1));
#endif
            }
        }
        bvh->construct();
        cmp_ = bvh.release();
    }

    multi_bezier_patch_intersection::multi_bezier_patch_intersection(const std::vector<real>& knot_u, const std::vector<real>& knot_v, const std::vector<bezier_patch<vector3> >& patches)
        : cmp_(NULL)
    {
        std::unique_ptr<bvh_composite_intersection> bvh(new bvh_composite_intersection());
        int nPu = (int)knot_u.size() - 1;
        int nPv = (int)knot_v.size() - 1;
        for (int v = 0; v < nPv; v++)
        {
            real v0 = knot_v[v];
            real v1 = knot_v[v + 1];

            for (int u = 0; u < nPu; u++)
            {
                real u0 = knot_u[u];
                real u1 = knot_u[u + 1];
#if 0
	            std::vector<ranged_patch> out;
	            GetSeparate(out, patches[v*nPu+u], u0,u1,v0,v1);
	            for(int k=0;k<out.size();k++){
		            bvh->add( new bezier_patch_intersection(out[k].patch,out[k].u0,out[k].u1,out[k].v0,out[k].v1));
	            }
#else
                bvh->add(new bezier_patch_intersection(patches[v * nPu + u], u0, u1, v0, v1));
//bvh->add( new bvh_bezier_patch_intersection(patches[v*nPu+u],u0,u1,v0,v1));
#endif
            }
        }
        bvh->construct();
        cmp_ = bvh.release();
    }

    multi_bezier_patch_intersection::~multi_bezier_patch_intersection()
    {
        if (cmp_) delete cmp_;
    }

    bool multi_bezier_patch_intersection::test(const ray& r, real dist) const
    {
        return cmp_->test(r, dist);
    }

    bool multi_bezier_patch_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return cmp_->test(info, r, dist);
    }

    void multi_bezier_patch_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }

    vector3 multi_bezier_patch_intersection::min() const
    {
        return cmp_->min();
    }
    vector3 multi_bezier_patch_intersection::max() const
    {
        return cmp_->max();
    }
}
