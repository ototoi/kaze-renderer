#ifndef KAZE_BILINEAR_PATCH_INTERSECTION_H
#define KAZE_BILINEAR_PATCH_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class bilinear_patch_intersection : public bounded_intersection
    {
    public:
        bilinear_patch_intersection(const vector3& p00, const vector3& p10, const vector3& p01, const vector3& p11);
        bilinear_patch_intersection(const vector3 p[4]);
        ~bilinear_patch_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        vector3 p_[4];
    };

    bool test_bilinear_ribbon(real* t, real* u, real* v, const vector3 p[4], real tmin, real tmax); //000-001
    bool test_bilinear_patch(real* t, real* u, real* v, const vector3 p[4], real tmin, real tmax);  //000-001
    bool test_bilinear_patch(real* t, real* u, real* v, const vector3 p[4], const ray& r, real tmin, real tmax);
    bool test_bilinear_patch(test_info* info, const vector3 p[4], const ray& r, real tmin, real tmax);
}

#endif
