#ifndef KAZE_PHONG_PATCH_INTERSECTION_H
#define KAZE_PHONG_PATCH_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class phong_patch_intersection : public bounded_intersection
    {
    public:
        phong_patch_intersection();
        ~phong_patch_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        vector3 min_;
        vector3 max_;
    };
}

#endif
