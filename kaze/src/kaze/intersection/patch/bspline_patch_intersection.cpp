#include "bspline_patch_intersection.h"
#include "bezier_patch.hpp"
#include "multi_bezier_patch_intersection.h" //
#include "bspline_to_bezier.h"

namespace kaze
{

    bspline_patch_intersection::bspline_patch_intersection(const bspline_patch<vector3>& patch)
        : inter_(NULL)
    {
        multi_bezier_patch<vector3> mpatch;
        bspline_to_bezier(mpatch, patch);
        inter_ = new multi_bezier_patch_intersection(mpatch);
    }

    bspline_patch_intersection::~bspline_patch_intersection()
    {
        if (inter_) delete inter_;
    }

    bool bspline_patch_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool bspline_patch_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void bspline_patch_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }

    vector3 bspline_patch_intersection::min() const
    {
        return inter_->min();
    }

    vector3 bspline_patch_intersection::max() const
    {
        return inter_->max();
    }
}
