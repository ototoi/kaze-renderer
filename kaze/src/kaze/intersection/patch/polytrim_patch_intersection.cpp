#include "polytrim_patch_intersection.h"
#include "bvh_composite_intersection.h"

#include "test_info.h"
#include "logger.h"

#include <memory>

#define MAX_LOOP 32

#define MAX_DIV 32;

namespace kaze
{
    namespace
    {

        static const real EPSILON = values::epsilon();

        static void get_minmax(vector2& min, vector2& max, const std::vector<vector2>& v)
        {
            min = max = v[0];
            size_t sz = v.size();
            for (size_t i = 0; i < sz; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (min[j] > v[i][j])
                    {
                        min[j] = v[i][j];
                    }
                    else if (max[j] < v[i][j])
                    {
                        max[j] = v[i][j];
                    }
                }
            }
        }

        struct pl_line
        {
            pl_line() {}
            pl_line(const vector2& a_, const vector2& b_) : a(a_), b(b_), na(0) {}
            vector2 a;
            vector2 b;
            int na;
        };

        static void get_minmax(vector2& min, vector2& max, const pl_line& pl)
        {
            for (int i = 0; i < 2; i++)
            {
                min[i] = std::min<real>(pl.a[i], pl.b[i]);
                max[i] = std::max<real>(pl.a[i], pl.b[i]);
            }
        }

        class pl_node
        {
        public:
            virtual ~pl_node() {}
            virtual bool test(std::vector<real>& us, real v) const = 0;
            virtual real min() const = 0;
            virtual real max() const = 0;
        };
        class pl_node_leaf : public pl_node
        {
        public:
            pl_node_leaf(const pl_line& pl) : pl_(pl)
            {
                vmin_ = std::min<real>(pl.a[1], pl.b[1]);
                vmax_ = std::max<real>(pl.a[1], pl.b[1]);
                vmin_ -= EPSILON;
                vmax_ += EPSILON;
            }
            bool test(std::vector<real>& us, real v) const
            {
                if (v < vmin_ || vmax_ < v) return false;
                vector2 a = pl_.a;
                vector2 b = pl_.b;

                int na = pl_.na;

                if (a[1] == b[1])
                {
                    if (a[1] == v)
                    {
                        real u = (a[0] + b[0]) * 0.5;
                        us.push_back(u);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }

                //if(a[1]>b[1])std::swap(a,b);

                real k = (v - a[1]) / (b[1] - a[1]);

                if (!na)
                {
                    if (k < 0 || 1 <= k) return false; //fix me...
                }
                else
                {
                    if (k <= 0 || 1 <= k) return false; //fix me...
                }
                real u = a[0] + k * (b[0] - a[0]);
                us.push_back(u);
                return true;
            }
            real min() const { return vmin_; }
            real max() const { return vmax_; }
        private:
            pl_line pl_;
            real vmin_;
            real vmax_;
        };

        typedef pl_line* PLINE;
        typedef PLINE* LITER;
        class pl_node_branch : public pl_node
        {
        public:
            pl_node_branch(LITER a, LITER b)
            {
                size_t sz = b - a;
                size_t hsz = sz >> 1;
                LITER c = a + hsz;
                size_t lsz = c - a;
                size_t rsz = b - c;

                nodes_[0] = 0;
                nodes_[1] = 0;

                vmin_ = +std::numeric_limits<real>::max();
                vmax_ = -std::numeric_limits<real>::max();

                if (lsz)
                {
                    if (lsz == 1)
                    {
                        nodes_[0] = new pl_node_leaf(**a);
                    }
                    else
                    {
                        nodes_[0] = new pl_node_branch(a, c);
                    }
                    vmin_ = std::min<real>(vmin_, nodes_[0]->min());
                    vmax_ = std::max<real>(vmax_, nodes_[0]->max());
                }
                if (rsz)
                {
                    if (rsz == 1)
                    {
                        nodes_[1] = new pl_node_leaf(**c);
                    }
                    else
                    {
                        nodes_[1] = new pl_node_branch(c, b);
                    }
                    vmin_ = std::min<real>(vmin_, nodes_[1]->min());
                    vmax_ = std::max<real>(vmax_, nodes_[1]->max());
                }
                vmin_ -= EPSILON;
                vmax_ += EPSILON;
            }
            ~pl_node_branch()
            {
                if (nodes_[0]) delete nodes_[0];
                if (nodes_[1]) delete nodes_[1];
            }
            bool test(std::vector<real>& us, real v) const
            {
                if (v < vmin_ || vmax_ < v) return false;
                bool bRet = false;
                if (nodes_[0] && nodes_[0]->test(us, v)) bRet = true;
                if (nodes_[1] && nodes_[1]->test(us, v)) bRet = true;
                return bRet;
            }
            real min() const { return vmin_; }
            real max() const { return vmax_; }
        private:
            pl_node* nodes_[2];
            real vmin_;
            real vmax_;
        };

        //-----------------------------------------------------------
        struct pl_sorter
        {
            bool operator()(pl_line* a, pl_line* b) const
            {
                real av = a->a[1] + a->b[1];
                real bv = b->a[1] + b->b[1];
                return a < b;
            }
        };
        //----------------------------------------------------------------------------------------------

        static void convert_pl(std::vector<pl_line>& lines, const std::vector<vector2>& pols, bool bCheck = false)
        {
            size_t sz = pols.size();
            lines.reserve(lines.size() + sz);
            for (size_t i = 0; i < sz; i++)
            {
                pl_line l;
                if (i == sz - 1)
                {
                    l.a = pols[i];
                    l.b = pols[0];
                }
                else
                {
                    l.a = pols[i];
                    l.b = pols[i + 1];
                }
                l.na = 0;
                if (i == 0)
                {
                    real y0 = pols[0][1] - pols[sz - 1][1];
                    real y1 = l.b[1] - l.a[1];
                    if (y0 * y1 < 0)
                    {
                        l.na = 1;
                    }
                }
                else
                {
                    real y0 = pols[i][1] - pols[i - 1][1];
                    real y1 = l.b[1] - l.a[1];
                    if (y0 * y1 < 0)
                    {
                        l.na = 1;
                    }
                }
                if (bCheck)
                {
                    if (l.a[1] != l.b[1])
                    {
                        lines.push_back(l);
                    }
                }
                else
                {
                    lines.push_back(l);
                }
            }
        }

        class polyline_trimmer : public patch_trimmer
        {
        public:
            polyline_trimmer(const std::vector<vector2>& outer_loop, const std::vector<std::vector<vector2> >& inner_loops)
            {
                std::vector<pl_line> lines;
                convert_pl(lines, outer_loop);
                size_t isz = inner_loops.size();
                for (size_t i = 0; i < isz; i++)
                {
                    convert_pl(lines, inner_loops[i], true);
                }

                size_t sz = lines.size();
                std::vector<pl_line*> plines(sz);
                for (size_t i = 0; i < sz; i++)
                {
                    plines[i] = &lines[i];
                }
                //-------------------------------
                std::sort(&plines[0], &plines[0] + sz, pl_sorter());

                node_ = new pl_node_branch(&plines[0], &plines[0] + sz);
            }

            ~polyline_trimmer()
            {
                delete node_;
            }

            bool test(real u, real v) const
            {
                std::vector<real> us;
                if (!node_->test(us, v)) return false;
                if (!us.empty())
                {
                    std::sort(us.begin(), us.end());
                    //us.erase(std::unique(us.begin(),us.end()),us.end());
                    int usz = (int)us.size();
                    for (int i = 0; i < usz - 1; i += 2)
                    {
                        if (us[i] < u && u < us[i + 1])
                        {                //
                            return true; //inner is true
                        }
                    }
                }
                return false;
            }

        private:
            pl_node* node_;
        };

        static const int NOTEST0 = 0;
        static const int NOTEST1 = 1;
        static const int NEEDTEST = 2;

        static bool test_line_box(const vector2& p1, const vector2& p2, const vector2& min, const vector2& max)
        {
            static const real EPSILON = values::epsilon();

            if (fabs(p2[0] - p1[0]) <= EPSILON)
            {
                real x = (p2[0] + p1[0]) * 0.5;
                if (min[0] - EPSILON <= x || x <= max[0] + EPSILON) return true;
            }
            if (fabs(p2[1] - p1[1]) <= EPSILON)
            {
                real y = (p2[1] + p1[1]) * 0.5;
                if (min[1] - EPSILON <= y || y <= max[1] + EPSILON) return true;
            }

            if (min[0] - EPSILON <= p1[0] && p1[0] <= max[0] + EPSILON)
            {
                if (min[1] - EPSILON <= p1[1] && p1[1] <= max[1] + EPSILON) return true;
            }
            if (min[0] - EPSILON <= p2[0] && p2[0] <= max[0] + EPSILON)
            {
                if (min[1] - EPSILON <= p2[1] && p2[1] <= max[1] + EPSILON) return true;
            }

            vector2 d = (p2 - p1) * 0.5;
            vector2 e = (max - min) * 0.5;
            vector2 c = p1 + d - (min + max) * 0.5;
            vector2 ad = vector2(fabs(d[0]), fabs(d[1])); // Returns same vector with all components positive

            if (fabs(c[0]) > e[0] + ad[0])
                return false;
            if (fabs(c[1]) > e[1] + ad[1])
                return false;

            if (fabs(d[0] * c[1] - d[1] * c[0]) > e[0] * ad[1] + e[1] * ad[0] + EPSILON)
                return false;

            return true;
        }

        class grid_trimmer : public patch_trimmer
        {
        public:
            grid_trimmer(const std::vector<vector2>& outer_loop, const std::vector<std::vector<vector2> >& inner_loops)
                : ltr_(0)
            {
                static const real EPS = values::epsilon() * 1024;

                vector2 min;
                vector2 max;
                get_minmax(min, max, outer_loop);
                for (size_t i = 0; i < inner_loops.size(); i++)
                {
                    vector2 cmin;
                    vector2 cmax;
                    get_minmax(cmin, cmax, inner_loops[i]);
                    for (int j = 0; j < 2; j++)
                    {
                        if (min[j] > cmin[j]) min[j] = cmin[j];
                        if (max[j] < cmax[j]) max[j] = cmax[j];
                    }
                }

                int W = MAX_DIV;
                int H = MAX_DIV;

                min -= vector2(EPS, EPS);
                max += vector2(EPS, EPS);

                std::vector<pl_line> lines;
                convert_pl(lines, outer_loop);
                size_t isz = inner_loops.size();
                for (size_t i = 0; i < isz; i++)
                {
                    convert_pl(lines, inner_loops[i]);
                }

                vector2 wid = max - min;
                int plane = 0;
                if (wid[1] > wid[0]) plane = 1;
                if (plane == 0)
                {
                    H = (int)ceil(W * wid[1] / wid[0]);
                }
                else
                {
                    W = (int)ceil(H * wid[0] / wid[1]);
                }

                real D = wid[plane] / MAX_DIV;
                vector2 center = (min + max) * 0.5;
                min = center - 0.5 * vector2(W, H) * vector2(D, D);
                max = center + 0.5 * vector2(W, H) * vector2(D, D);

                real dx = (max[0] - min[0]) / W;
                real dy = (max[1] - min[1]) / H;

                grid_.resize(W * H);
                memset(&grid_[0], 0, W * H * sizeof(char));
                for (size_t i = 0; i < lines.size(); i++)
                {
                    vector2 cmin;
                    vector2 cmax;

                    get_minmax(cmin, cmax, lines[i]);

                    cmin -= vector2(EPS, EPS);
                    cmax += vector2(EPS, EPS);

                    int x0 = std::max<int>(0, (int)floor(W * (cmin[0] - min[0]) / (max[0] - min[0])));
                    int x1 = std::min<int>(W - 1, (int)ceil(W * (cmax[0] - min[0]) / (max[0] - min[0])));
                    int y0 = std::max<int>(0, (int)floor(H * (cmin[1] - min[1]) / (max[1] - min[1])));
                    int y1 = std::min<int>(H - 1, (int)ceil(H * (cmax[1] - min[1]) / (max[1] - min[1])));
                    for (int y = y0; y <= y1; y++)
                    {
                        for (int x = x0; x <= x1; x++)
                        {
                            real xx0 = min[0] + x * dx;
                            real xx1 = xx0 + dx;
                            real yy0 = min[1] + y * dy;
                            real yy1 = yy0 + dy;

                            vector2 bmin(xx0, yy0);
                            vector2 bmax(xx1, yy1);

                            bmin -= vector2(EPS, EPS);
                            bmax += vector2(EPS, EPS);

                            if (test_line_box(lines[i].a, lines[i].b, bmin, bmax))
                            {
                                grid_[y * W + x] = NEEDTEST;
                            }
                        }
                    }
                }

                std::unique_ptr<polyline_trimmer> ap(new polyline_trimmer(outer_loop, inner_loops));
                {
                    for (int y = 0; y < H; y++)
                    {
                        for (int x = 0; x < W; x++)
                        {
                            if (grid_[y * W + x] != NEEDTEST)
                            {
                                real xx = min[0] + (x + 0.5) * dx;
                                real yy = min[1] + (y + 0.5) * dy;
                                if (!ap->test(xx, yy))
                                {
                                    grid_[y * W + x] = NOTEST0;
                                }
                                else
                                {
                                    grid_[y * W + x] = NOTEST1;
                                }
                            }
                        }
                    }
                }
                min_ = min;
                max_ = max;
                delta_ = vector2(W, H) / (max - min);
                w_ = W;
                h_ = H;
                ltr_ = ap.release();
            }

            ~grid_trimmer()
            {
                delete ltr_;
            }

            bool debug(int N) const
            {
                for (int j = 0; j <= N; j++)
                {
                    real v = real(j) / N;
                    for (int i = 0; i <= N; i++)
                    {
                        real u = real(i) / N;
                        if (!debug(u, v))
                        {
                            print_log("fail\n");
                            return false;
                        }
                    }
                }
                return true;
            }

            bool debug(real u, real v) const
            {
                if (u < min_[0] || max_[0] <= u) return true;
                if (v < min_[1] || max_[1] <= v) return true;

                int x = (int)floor((u - min_[0]) * delta_[0]);
                int y = (int)floor((v - min_[1]) * delta_[1]);

                switch (grid_[y * w_ + x])
                {
                case NOTEST0:
                {
                    if (!ltr_->test(u, v)) return true;
                    return false;
                }
                case NOTEST1:
                {
                    if (ltr_->test(u, v)) return true;
                    return false;
                }
                default:
                {
                    return true;
                }
                }
            }

            bool test(real u, real v) const
            {
                if (u < min_[0] || max_[0] < u) return false;
                if (v < min_[1] || max_[1] < v) return false;

                int x = (int)floor((u - min_[0]) * delta_[0]);
                int y = (int)floor((v - min_[1]) * delta_[1]);

                if (w_ <= x) return false;
                if (h_ <= y) return false;

                switch (grid_[y * w_ + x])
                {
                case NOTEST0:
                {
                    assert(!ltr_->test(u, v));
                    return false;
                }
                case NOTEST1:
                {
                    assert(ltr_->test(u, v));
                    return true;
                }
                default:
                {
                    return ltr_->test(u, v);
                }
                }
            }

        private:
            vector2 min_;
            vector2 max_;
            vector2 delta_;
            int w_;
            int h_;
            std::vector<char> grid_;
            polyline_trimmer* ltr_;
        };
    }
    //----------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------
    polytrim_patch_intersection::polytrim_patch_intersection(
        const std::vector<vector2>& outer_loop,
        const std::vector<std::vector<vector2> >& inner_loops,
        const std::vector<std::shared_ptr<intersection> >& inters) : cmp_(0), trm_(0)
    {
        initialize(outer_loop, inner_loops, inters);
    }

    polytrim_patch_intersection::polytrim_patch_intersection(
        const std::vector<vector2>& outer_loop,
        const std::vector<std::shared_ptr<intersection> >& inters) : cmp_(0), trm_(0)
    {
        std::vector<std::vector<vector2> > inner_loops;
        initialize(outer_loop, inner_loops, inters);
    }

    polytrim_patch_intersection::~polytrim_patch_intersection()
    {
        if (cmp_) delete cmp_;
        if (trm_) delete trm_;
    }
    //----------------------------------------------------------------------------------------------

    void polytrim_patch_intersection::initialize(
        const std::vector<vector2>& outer_loop,
        const std::vector<std::vector<vector2> >& inner_loops,
        const std::vector<std::shared_ptr<intersection> >& inters)
    {
        std::unique_ptr<bvh_composite_intersection> bvh(new bvh_composite_intersection());

        size_t sz = inters.size();
        for (size_t i = 0; i < sz; i++)
        {
            bvh->add(inters[i]);
        }
        bvh->construct();

        //std::unique_ptr<polyline_trimmer> trm(new polyline_trimmer(outer_loop, inner_loops));
        std::unique_ptr<grid_trimmer> trm(new grid_trimmer(outer_loop, inner_loops));
        //trm->debug(20);

        trm_ = trm.release();
        cmp_ = bvh.release();
    }

    bool polytrim_patch_intersection::test(const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1024;

        real td = 0;
        real tt = dist;
        ray tr = r;
        test_info tmp;
        int n = 0;
        while (cmp_->test(&tmp, tr, tt) && n <= MAX_LOOP)
        {
            real u = tmp.coord[0];
            real v = tmp.coord[1];
            if (trm_->test(u, v))
            {
                return true;
            }
            else
            {
                real progress = tmp.distance + EPSILON;
                tt -= progress;
                if (tt <= 0) return false;
                td += progress;
                vector3 p = tr.origin() + progress * tr.direction();
                tr = ray(p, tr.direction());
            }
            n++;
        }
        return false;
    }

    bool polytrim_patch_intersection::test(test_info* info, const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1024;

        real td = 0;
        real tt = dist;
        ray tr = r;
        test_info tmp;
        int n = 0;
        while (cmp_->test(&tmp, tr, tt) && n <= MAX_LOOP)
        {
            real u = tmp.coord[0];
            real v = tmp.coord[1];
            if (trm_->test(u, v))
            {
                tmp.distance += td;
                *info = tmp;
                return true;
            }
            else
            {
                real progress = tmp.distance + EPSILON;
                tt -= progress;
                if (tt <= 0) return false;
                td += progress;
                vector3 p = tr.origin() + progress * tr.direction();
                tr = ray(p, tr.direction());
            }
            n++;
        }
        return false;
    }

    void polytrim_patch_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }

    vector3 polytrim_patch_intersection::min() const
    {
        return cmp_->min();
    }

    vector3 polytrim_patch_intersection::max() const
    {
        return cmp_->max();
    }
}
