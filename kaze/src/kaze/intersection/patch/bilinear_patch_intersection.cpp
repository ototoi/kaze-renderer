#include "bilinear_patch_intersection.h"
#include "test_info.h"

#include <string.h>
#include <assert.h>

namespace kaze
{

    static const real EPS = values::epsilon() * 1024;

    bilinear_patch_intersection::bilinear_patch_intersection(const vector3& p00, const vector3& p10, const vector3& p01, const vector3& p11)
    {
        p_[0] = p00;
        p_[1] = p10;
        p_[2] = p01;
        p_[3] = p11;
    }

    bilinear_patch_intersection::bilinear_patch_intersection(const vector3 p[4])
    {
        memcpy(p_, p, sizeof(vector3) * 4);
    }

    bilinear_patch_intersection::~bilinear_patch_intersection()
    {
        ; //
    }

    bool bilinear_patch_intersection::test(const ray& r, real dist) const
    {
        return test_bilinear_patch(NULL, p_, r, 0, dist);
    }

    bool bilinear_patch_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (test_bilinear_patch(info, p_, r, 0, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    static inline vector3 leap(const vector3& a, const vector3& b, real t)
    {
        return a * (1.0 - t) + b * t;
    }

    void bilinear_patch_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        real u = info->coord[0];
        real v = info->coord[1];

        vector3 U = normalize(leap(p_[1], p_[3], v) - leap(p_[0], p_[2], v));
        vector3 V = normalize(leap(p_[2], p_[3], u) - leap(p_[0], p_[1], u));

        vector3 N = cross(U, V);

        info->position = r.origin() + dist * r.direction();
        info->normal = N;
        info->geometric = N;
        info->tangent = U;
        info->binormal = V;
    }

    vector3 bilinear_patch_intersection::min() const
    {
        vector3 tmp = p_[0];
        for (int i = 1; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                tmp[j] = std::min(tmp[j], p_[i][j]);
            }
        }
        return tmp - vector3(EPS, EPS, EPS);
    }

    vector3 bilinear_patch_intersection::max() const
    {
        vector3 tmp = p_[0];
        for (int i = 1; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                tmp[j] = std::max(tmp[j], p_[i][j]);
            }
        }
        return tmp + vector3(EPS, EPS, EPS);
    }

    static bool solve_bilinear_patch(real* t, real* u, real* v, real tmin, real tmax, real tt, real uu, real vv)
    {
        if (tmin <= tt && tt <= tmax)
        {
            *t = tt;
            *u = uu;
            *v = vv;
            return true;
        }
        return false;
    }

    static int solve2(double root[2], const double coeff[3])
    {
        static const double EPS = std::numeric_limits<double>::min();

        double A = coeff[0];
        double B = coeff[1];
        double C = coeff[2];

        if (fabs(A) <= EPS)
        {
            if (fabs(B) <= EPS) return 0;
            double x = -C / B;
            root[0] = x;
            return 1;
        }
        else
        {
            double D = B * B - 4 * A * C;
            if (D < 0)
            {
                return 0;
            }
            else if (D == 0)
            {
                double x = -0.5 * B / A;
                root[0] = x;
                return 1;
            }
            else
            {
#if 0
                double sqrD = sqrt(D);
                double invA = 0.5/A;
                double xa = invA*(-B - sqrD);
                double xb = invA*(-B + sqrD);
                if(xa>xb)std::swap(xa,xb);
                root[0] = xa;
                root[1] = xb;
                return 2;
#else
                double x1 = (fabs(B) + sqrt(D)) / (2.0 * A);
                if (B >= 0.0)
                {
                    x1 = -x1;
                }
                double x2 = C / (A * x1);
                if (x1 > x2) std::swap(x1, x2);
                root[0] = x1;
                root[1] = x2;
                return 2;
#endif
            }
        }
    }

    static inline double compute_u(double A1, double A2, double B1, double B2, double C1, double C2, double D1, double D2, double v)
    {
        //return div((v*(C1-C2)+(D1-D2)),(v*(A2-A1)+(B2-B1)));
        double a = v * A2 + B2;
        double b = v * (A2 - A1) + B2 - B1;
        if (fabs(b) >= fabs(a))
        {
            return (v * (C1 - C2) + D1 - D2) / b;
        }
        else
        {
            return (-v * C2 - D2) / a;
        }
    }

    static inline double compute_t(double a, double b, double c, double d, double iq, double u, double v)
    {
        return ((u * v) * a + u * b + v * c + d) * iq;
    }

#define CALC_U(v) compute_u(A1, A2, B1, B2, C1, C2, D1, D2, v)
#define CALC_T(u, v) compute_t(a[nPlane], b[nPlane], c[nPlane], d[nPlane], iq[nPlane], u, v)

    bool test_bilinear_patch(real* t, real* u, real* v, const vector3 p[4], const ray& r, real tmin, real tmax)
    {
        const vector3& p00 = p[0];
        const vector3& p10 = p[1];
        const vector3& p01 = p[2];
        const vector3& p11 = p[3];

        const vector3& q = r.direction();
        const vector3& iq = r.inversed_direction();

        int nPlane = 0;
        {
            real DD[3];
            DD[0] = fabs(iq[0]);
            DD[1] = fabs(iq[1]);
            DD[2] = fabs(iq[2]);

            if (DD[nPlane] > DD[1]) nPlane = 1;
            if (DD[nPlane] > DD[2]) nPlane = 2;
        }

        vector3 a = p11 - p10 - p01 + p00;
        vector3 b = p10 - p00;
        vector3 c = p01 - p00;
        vector3 d = p00 - r.origin();

        //xz-zx
        real A1 = a[0] * q[2] - a[2] * q[0];
        real B1 = b[0] * q[2] - b[2] * q[0];
        real C1 = c[0] * q[2] - c[2] * q[0];
        real D1 = d[0] * q[2] - d[2] * q[0];

        //yz-zy
        real A2 = a[1] * q[2] - a[2] * q[1];
        real B2 = b[1] * q[2] - b[2] * q[1];
        real C2 = c[1] * q[2] - c[2] * q[1];
        real D2 = d[1] * q[2] - d[2] * q[1];

        real F1 = A2 * C1 - A1 * C2;
        real F2 = A2 * D1 - A1 * D2 + B2 * C1 - B1 * C2;
        real F3 = B2 * D1 - B1 * D2;

        double coeff[] = {F1, F2, F3};

        double root[2] = {};
        int nRet = solve2(root, coeff);

        if (nRet)
        {
            bool bRet = false;
            for (int i = 0; i < nRet; i++)
            {
                double vv = root[i];
                if (0 <= vv && vv <= 1)
                {
                    double uu = CALC_U(vv);
                    if (0 <= uu && uu <= 1)
                    {
                        double tt = CALC_T(uu, vv);
                        if (solve_bilinear_patch(t, u, v, tmin, tmax, tt, uu, vv))
                        {
                            tmax = *t;
                            bRet = true;
                        }
                    }
                }
            }
            return bRet;
        }
        return false;
    }

#undef CALC_T
#undef CALC_U

#define CALC_U(v) compute_u(A1, A2, B1, B2, C1, C2, D1, D2, v)
#define CALC_T(u, v) compute_t(a[nPlane], b[nPlane], c[nPlane], d[nPlane], 1, u, v)

    static void get_minmax(vector3& min, vector3& max, const vector3 p[4])
    {
        min = max = p[0];
        for (int i = 1; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                min[j] = std::min(min[j], p[i][j]);
                max[j] = std::max(max[j], p[i][j]);
            }
        }
    }

    bool test_bilinear_ribbon(real* t, real* u, real* v, const vector3 p[4], real tmin, real tmax)
    {
        const vector3& p00 = p[0];
        const vector3& p10 = p[1];
        const vector3& p01 = p[2];
        const vector3& p11 = p[3];

        /*
		vector3 min;
		vector3 max;
		get_minmax(min, max, p);
		if(0<min[0] || max[0]<0)return false;//x
        if(0<min[1] || max[1]<0)return false;//y
        if(max[2]<tmin || tmax<min[2])return false;//z
		*/

        static const int nPlane = 2;

        vector3 a = p11 - p10 - p01 + p00;
        vector3 b = p10 - p00;
        vector3 c = p01 - p00;
        vector3 d = p00;

        //xz-zx
        real A1 = a[0];
        real B1 = b[0];
        real C1 = c[0];
        real D1 = d[0];

        //yz-zy
        real A2 = a[1];
        real B2 = b[1];
        real C2 = c[1];
        real D2 = d[1];

        real F1 = A2 * C1 - A1 * C2;
        real F2 = A2 * D1 - A1 * D2 + B2 * C1 - B1 * C2;
        real F3 = B2 * D1 - B1 * D2;

        double coeff[] = {F1, F2, F3};

        double root[2] = {};
        int nRet = solve2(root, coeff);

        if (nRet)
        {
            bool bRet = false;
            for (int i = 0; i < nRet; i++)
            {
                double vv = root[i];
                if (0 <= vv && vv <= 1)
                {
                    double uu = CALC_U(vv);
                    if (0 <= uu && uu <= 1)
                    {
                        double tt = CALC_T(uu, vv);
                        if (solve_bilinear_patch(t, u, v, tmin, tmax, tt, uu, vv))
                        {
                            tmax = *t;
                            bRet = true;
                        }
                    }
                }
            }
            return bRet;
        }
        return false;
    }

    bool test_bilinear_patch(real* t, real* u, real* v, const vector3 p[4], real tmin, real tmax)
    {
        const vector3& p00 = p[0];
        const vector3& p10 = p[1];
        const vector3& p01 = p[2];
        const vector3& p11 = p[3];

        /*
		vector3 min;
		vector3 max;
		get_minmax(min, max, p);
		if(0<min[0] || max[0]<0)return false;//x
        if(0<min[1] || max[1]<0)return false;//y
        if(max[2]<tmin || tmax<min[2])return false;//z
		*/

        static const int nPlane = 2;

        vector3 a = p11 - p10 - p01 + p00;
        vector3 b = p10 - p00;
        vector3 c = p01 - p00;
        vector3 d = p00;

        //xz-zx
        real A1 = a[0];
        real B1 = b[0];
        real C1 = c[0];
        real D1 = d[0];

        //yz-zy
        real A2 = a[1];
        real B2 = b[1];
        real C2 = c[1];
        real D2 = d[1];

        real F1 = A2 * C1 - A1 * C2;
        real F2 = A2 * D1 - A1 * D2 + B2 * C1 - B1 * C2;
        real F3 = B2 * D1 - B1 * D2;

        double coeff[] = {F1, F2, F3};

        double root[2] = {};
        int nRet = solve2(root, coeff);

        if (nRet)
        {
            bool bRet = false;
            for (int i = 0; i < nRet; i++)
            {
                double vv = root[i];
                if (0 <= vv && vv <= 1)
                {
                    double uu = CALC_U(vv);
                    if (0 <= uu && uu <= 1)
                    {
                        double tt = CALC_T(uu, vv);
                        if (solve_bilinear_patch(t, u, v, tmin, tmax, tt, uu, vv))
                        {
                            tmax = *t;
                            bRet = true;
                        }
                    }
                }
            }
            return bRet;
        }
        return false;
    }

#undef CALC_T
#undef CALC_U

    bool test_bilinear_patch(test_info* info, const vector3 p[4], const ray& r, real tmin, real tmax)
    {
        real t = 0;
        real u = 0;
        real v = 0;
        if (test_bilinear_patch(&t, &u, &v, p, r, tmin, tmax))
        {
            if (info)
            {
                info->distance = t;
                info->coord = vector3(u, v, 0);
            }
            return true;
        }
        return false;
    }
}
