#include "trimmed_multi_bezier_patch_intersection.h"
#include "bvh_composite_intersection.h"
#include "polytrim_patch_intersection.h"

#include "sample_patch_parameter.h"
#include "enumerate_loops.h"

#include <vector>
#include <memory>

#define DIST_TOL 0.01
#define ANG_TOL 30
#define UV_TOL (1.0 / 10000.0)

#include "bezier_patch_intersection.h"
#include "bvh_bezier_patch_intersection.h"

namespace kaze
{

    namespace
    {
        static void get_minmax(vector2& min, vector2& max, const std::vector<vector2>& a)
        {
            size_t asz = a.size();
            min = a[0];
            max = a[0];
            for (size_t i = 0; i < asz; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (min[j] > a[i][j]) min[j] = a[i][j];
                    if (max[j] < a[i][j]) max[j] = a[i][j];
                }
            }
        }

        struct ranged_patch
        {
            bezier_patch<vector3> patch;
            real u0;
            real u1;
            real v0;
            real v1;
        };

        static void GetSeparate(std::vector<ranged_patch>& out, const bezier_patch<vector3>& patch, real u0, real u1, real v0, real v1)
        {
            int nu = patch.get_nu();
            int nv = patch.get_nv();
            real du = real(1.0) / (nu - 1);
            real dv = real(1.0) / (nv - 1);

            std::vector<bezier_patch<vector3> > patches_uv((nu - 1) * (nv - 1));

            std::vector<bezier_patch<vector3> > patches_u(nu - 1);
            {
                std::vector<real> rng_u(nu);
                rng_u[0] = 0;
                rng_u[nu - 1] = 1;
                for (int i = 1; i < nu - 1; i++)
                {
                    rng_u[i] = real(i) / real(i + 1);
                }
                //0 1/2 2/3
                bezier_patch<vector3> pat(patch);
                for (int i = nu - 2; i > 0; i--)
                {
                    bezier_patch<vector3> tmp[2];
                    pat.split_u(tmp, rng_u[i]);
                    patches_u[i].swap(tmp[1]);
                    pat.swap(tmp[0]);
                }
                patches_u[0].swap(pat);
            }

            {
                std::vector<real> rng_v(nv);
                rng_v[0] = 0;
                rng_v[nv - 1] = 1;
                for (int i = 1; i < nv - 1; i++)
                {
                    rng_v[i] = real(i) / real(i + 1);
                }
                //0 1/2 2/3
                for (int j = 0; j < nu - 1; j++)
                {
                    std::vector<bezier_patch<vector3> > patches_v(nv - 1);

                    bezier_patch<vector3> pat(patches_u[j]);
                    for (int i = nv - 2; i > 0; i--)
                    {
                        bezier_patch<vector3> tmp[2];
                        pat.split_v(tmp, rng_v[i]);
                        patches_v[i].swap(tmp[1]);
                        pat.swap(tmp[0]);
                    }
                    patches_v[0].swap(pat);

                    for (int k = 0; k < nv - 1; k++)
                    {
                        patches_uv[(nu - 1) * k + j].swap(patches_v[k]);
                    }
                }
            }

            out.reserve((nu - 1) * (nv - 1));
            for (int j = 0; j < nv - 1; j++)
            {
                for (int i = 0; i < nu - 1; i++)
                {
                    ranged_patch tmp;
                    tmp.patch = patches_uv[(nu - 1) * j + i];
                    tmp.u0 = u0 + (u1 - u0) * du * i;
                    tmp.u1 = u0 + (u1 - u0) * du * (i + 1);
                    tmp.v0 = v0 + (v1 - v0) * dv * j;
                    tmp.v1 = v0 + (v1 - v0) * dv * (j + 1);
                    out.push_back(tmp);
                }
            }
        }

        static bool convert_curve(std::vector<vector2>& loop, const multi_bezier_curve<vector2>& curve, const multi_bezier_patch<vector3>& patch)
        {
            std::vector<real> params;
            sample_patch_parameter(params, curve, curve.get_knots(), UV_TOL);
            for (int i = 0; i < (int)(params.size() - 1); i++)
            {
                loop.push_back(curve.evaluate(params[i]));
            }

            return true;
        }

        static bool convert_loop(std::vector<vector2>& loop, const std::vector<multi_bezier_curve<vector2> >& in, const multi_bezier_patch<vector3>& patch)
        {
            size_t sz = in.size();
            for (size_t i = 0; i < sz; i++)
            {
                if (!convert_curve(loop, in[i], patch)) return false;
            }
            return true;
        }
    }

    trimmed_multi_bezier_patch_intersection::trimmed_multi_bezier_patch_intersection(
        const multi_bezier_patch<vector3>& patch,
        const std::vector<std::vector<multi_bezier_curve<vector2> > >& outer_loops,
        const std::vector<std::vector<multi_bezier_curve<vector2> > >& inner_loops)
        : cmp_(0)
    {
        std::unique_ptr<bvh_composite_intersection> bvh(new bvh_composite_intersection());

        int nPu = patch.get_patch_size_u();
        int nPv = patch.get_patch_size_v();

        std::vector<ranged_patch> patchs;
        for (int j = 0; j < nPv; j++)
        {
            real v0 = patch.get_knot_at_v(j);
            real v1 = patch.get_knot_at_v(j + 1);

            for (int i = 0; i < nPu; i++)
            {
                real u0 = patch.get_knot_at_u(i);
                real u1 = patch.get_knot_at_u(i + 1);
#if 0
                std::vector<ranged_patch> out;
		        GetSeparate(out, patch.get_patch_at(i,j), u0,u1,v0,v1);
                for(int i = 0;i<out.size();i++){
                    patchs.push_back(out[i]);
                }
#else
                ranged_patch tmp;
                tmp.patch = patch.get_patch_at(i, j);
                tmp.u0 = u0;
                tmp.u1 = u1;
                tmp.v0 = v0;
                tmp.v1 = v1;
                patchs.push_back(tmp);
#endif
            }
        }

        std::vector<std::vector<vector2> > ol;
        std::vector<std::vector<vector2> > il;
        for (size_t i = 0; i < outer_loops.size(); i++)
        {
            std::vector<vector2> pols;
            convert_loop(pols, outer_loops[i], patch);
            ol.push_back(pols);
        }
        for (size_t i = 0; i < inner_loops.size(); i++)
        {
            std::vector<vector2> pols;
            convert_loop(pols, inner_loops[i], patch);
            il.push_back(pols);
        }

        size_t osz = ol.size();
        size_t isz = il.size();

        if (!osz && !isz)
        { //
            //no trim
            for (size_t j = 0; j < patchs.size(); j++)
            {
                real u0 = patchs[j].u0;
                real u1 = patchs[j].u1;
                real v0 = patchs[j].v0;
                real v1 = patchs[j].v1;

                //bvh->add(new bezier_patch_intersection(patchs[j].patch,u0,u1,v0,v1));
                bvh->add(new bvh_bezier_patch_intersection(patchs[j].patch, u0, u1, v0, v1));
            }
        }
        else
        {
            std::vector<vector2> points;
            std::vector<polyloop_group> loops;
            enumerate_loops(loops, ol, il, points, 4);
            for (size_t i = 0; i < loops.size(); i++)
            {
                vector2 min, max;
                get_minmax(min, max, loops[i].outer_loop);
                std::vector<std::shared_ptr<intersection> > inters;
                for (size_t j = 0; j < patchs.size(); j++)
                {
                    real u0 = patchs[j].u0;
                    real u1 = patchs[j].u1;
                    real v0 = patchs[j].v0;
                    real v1 = patchs[j].v1;

                    if (u1 < min[0] || max[0] < u0) continue;
                    if (v1 < min[1] || max[1] < v0) continue;

                    //inters.push_back(std::shared_ptr<intersection>(new bezier_patch_intersection(patchs[j].patch,u0,u1,v0,v1)));
                    inters.push_back(std::shared_ptr<intersection>(new bvh_bezier_patch_intersection(patchs[j].patch, u0, u1, v0, v1)));
                }
                bvh->add(new polytrim_patch_intersection(loops[i].outer_loop, loops[i].inner_loops, inters));
            }
        }

        bvh->construct();
        cmp_ = bvh.release();
    }

    trimmed_multi_bezier_patch_intersection::~trimmed_multi_bezier_patch_intersection()
    {
        if (cmp_) delete cmp_;
    }

    bool trimmed_multi_bezier_patch_intersection::test(const ray& r, real dist) const
    {
        return cmp_->test(r, dist);
    }

    bool trimmed_multi_bezier_patch_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return cmp_->test(info, r, dist);
    }

    void trimmed_multi_bezier_patch_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }

    vector3 trimmed_multi_bezier_patch_intersection::min() const
    {
        return cmp_->min();
    }
    vector3 trimmed_multi_bezier_patch_intersection::max() const
    {
        return cmp_->max();
    }
}
