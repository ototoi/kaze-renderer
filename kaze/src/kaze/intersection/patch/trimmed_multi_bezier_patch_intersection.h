#ifndef KAZE_TRIMMED_MULTI_BEZIER_PATCH_INTERSECTION_H
#define KAZE_TRIMMED_MULTI_BEZIER_PATCH_INTERSECTION_H

#include "bounded_intersection.h"
#include "composite_intersection.h"
#include "multi_bezier_curve.hpp"
#include "multi_bezier_patch.hpp"

namespace kaze
{

    class trimmed_multi_bezier_patch_intersection : public bounded_intersection
    {
    public:
        trimmed_multi_bezier_patch_intersection(
            const multi_bezier_patch<vector3>& patch,
            const std::vector<std::vector<multi_bezier_curve<vector2> > >& outer_loops,
            const std::vector<std::vector<multi_bezier_curve<vector2> > >& inner_loops);
        ~trimmed_multi_bezier_patch_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        composite_intersection* cmp_;
    };
}

#endif