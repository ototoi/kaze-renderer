#ifndef KAZE_COMOSITE_BEZIER_PATCH_INTERSECTION_H
#define KAZE_COMOSITE_BEZIER_PATCH_INTERSECTION_H

#include "bounded_intersection.h"
#include "patch/bezier_patch_loader.h"
#include "transformer.h"

namespace kaze
{

    class composite_bezier_patch_intersection_imp;
    class composite_bezier_patch_intersection : public bounded_intersection
    {
    public:
        composite_bezier_patch_intersection(const bezier_patch_loader& pl);
        composite_bezier_patch_intersection(const bezier_patch_loader& pl, const transformer& tr);
        ~composite_bezier_patch_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        composite_bezier_patch_intersection_imp* imp_;
    };
}

#endif
