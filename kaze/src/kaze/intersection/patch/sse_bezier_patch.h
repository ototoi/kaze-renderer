#ifndef KAZE_SSE_BEZIER_PATCH_H
#define KAZE_SSE_BEZIER_PATCH_H

#include "bezier.h"
#include <xmmintrin.h>

namespace kaze
{

    template <size_t Sz = 16>
    struct sse_bezier_evaluator
    {
#ifdef _MSC_VER
        typedef _declspec(align(16)) float float_array[4];
#else
        typedef __attribute__((aligned(16))) float float_array[4];
#endif

        static inline __m128 sse_bezier_evaluate_n(const __m128 cp[], int n, const float e[])
        {
            __m128 tmp = _mm_setzero_ps();
            for (int i = 0; i < n; i++)
            {
                tmp = _mm_add_ps(tmp, _mm_mul_ps(cp[i], _mm_set1_ps(e[i])));
            }
            return tmp;
        }

        static inline __m128 sse_bezier_evaluate_n(const __m128 cp[], int n, float t)
        {
            float e[Sz / 2];
            bernstein(t, e, n);
            return sse_bezier_evaluate_n(cp, n, e);
        }

        static inline __m128 sse_bezier_evaluate(const __m128 cp[], int n, float t)
        {
            return sse_bezier_evaluate_n(cp, n, t);
        }

        static inline __m128 sse_bezier_evaluate(const __m128 cp[], int nu, int nv, float u, float v)
        {
            __m128 tmp[Sz / 2];
            float eu[Sz / 2];
            bernstein(u, eu, nu);

            for (int j = 0; j < nv; j++)
            {
                tmp[j] = sse_bezier_evaluate_n(cp + nu * j, nu, eu);
            }
            return sse_bezier_evaluate(tmp, nv, v);
        }

        static inline __m128 sse_bezier_min(const __m128 cp[], int n)
        {
            __m128 tmp = cp[0];
            for (int i = 1; i < n; i++)
            {
                tmp = _mm_min_ps(tmp, cp[i]);
            }
            return tmp;
        }

        static inline __m128 sse_bezier_max(const __m128 cp[], int n)
        {
            __m128 tmp = cp[0];
            for (int i = 1; i < n; i++)
            {
                tmp = _mm_max_ps(tmp, cp[i]);
            }
            return tmp;
        }
#define BEZIER_RATE(A, B, t) _mm_add_ps(_mm_mul_ps(_mm_sub_ps(B, A), t), A)

        static inline void sse_bezier_split_2(__m128 a[], __m128 b[], const __m128 p[], __m128 t)
        {
            __m128 p10 = BEZIER_RATE(p[0], p[1], t);

            a[0] = p[0];
            a[1] = p10;

            b[0] = p10;
            b[1] = p[1];
        }

        static inline void sse_bezier_split_3(__m128 a[], __m128 b[], const __m128 p[], __m128 t)
        {
            __m128 p10 = BEZIER_RATE(p[0], p[1], t);
            __m128 p11 = BEZIER_RATE(p[1], p[2], t);

            __m128 p20 = BEZIER_RATE(p10, p11, t);

            a[0] = p[0];
            a[1] = p10;
            a[2] = p20;

            b[0] = p20;
            b[1] = p11;
            b[2] = p[2];
        }

        static inline void sse_bezier_split_4(__m128 a[], __m128 b[], const __m128 p[], __m128 t)
        {
            __m128 p10 = BEZIER_RATE(p[0], p[1], t);
            __m128 p11 = BEZIER_RATE(p[1], p[2], t);
            __m128 p12 = BEZIER_RATE(p[2], p[3], t);

            __m128 p20 = BEZIER_RATE(p10, p11, t);
            __m128 p21 = BEZIER_RATE(p11, p12, t);

            __m128 p30 = BEZIER_RATE(p20, p21, t);

            a[0] = p[0];
            a[1] = p10;
            a[2] = p20;
            a[3] = p30;

            b[0] = p30;
            b[1] = p21;
            b[2] = p12;
            b[3] = p[3];
        }

        static inline void sse_bezier_split_n(__m128 a[], __m128 b[], const __m128 p[], int i, int n, __m128 t)
        {
            int tn = n - 1 - i; //3

            a[i] = p[0];
            b[tn] = p[tn];
            switch (tn)
            {
            case 0:
                return;
            case 1:
            case 2:
            case 3:
            case 4:
            {
                __m128 tmp[4];
                //---------------------
                for (int j = 0; j < tn; j++)
                {
                    tmp[j] = BEZIER_RATE(p[j], p[j + 1], t);
                }
                //---------------------
                sse_bezier_split_n(a, b, &tmp[0], i + 1, n, t);
            }
            break;
            default:
            {
                __m128 tmp[Sz / 2];
                //---------------------
                for (int j = 0; j < tn; j++)
                {
                    tmp[j] = BEZIER_RATE(p[j], p[j + 1], t);
                }
                //---------------------
                sse_bezier_split_n(a, b, &tmp[0], i + 1, n, t);
            }
            break;
            }
        }
        static inline void sse_bezier_split_n(__m128 a[], __m128 b[], const __m128 p[], int n, __m128 t)
        {
            sse_bezier_split_n(a, b, p, 0, n, t);
        }

#undef BEZIER_RATE

        static inline void sse_bezier_split(__m128 a[], __m128 b[], const __m128 p[], int n, __m128 t)
        {
            switch (n)
            {
            case 2:
                sse_bezier_split_2(a, b, p, t);
                break;
            case 3:
                sse_bezier_split_3(a, b, p, t);
                break;
            case 4:
                sse_bezier_split_4(a, b, p, t);
                break;
            default:
                sse_bezier_split_n(a, b, p, n, t);
                break;
            }
        }

        static inline void sse_bezier_split(__m128 a[], __m128 b[], const __m128 p[], int n, float t)
        {
            sse_bezier_split(a, b, p, n, _mm_set1_ps(t));
        }

        static inline void sse_bezier_split_u(__m128 a[], __m128 b[], const __m128 p[], int nu, int nv, __m128 u)
        {
            for (int i = 0; i < nv; i++)
            {
                sse_bezier_split(&a[i * nu + 0], &b[i * nu + 0], &p[i * nu + 0], nu, u);
            }
        }

        static inline void sse_bezier_split_u(__m128 a[], __m128 b[], const __m128 p[], int nu, int nv, float u)
        {
            __m128 uu = _mm_set1_ps(u);
            sse_bezier_split_u(a, b, p, nu, nv, uu);
        }

        static inline void sse_bezier_split_v(__m128 a[], __m128 b[], const __m128 p[], int nu, int nv, __m128 v)
        {
            __m128 tmp[Sz / 2];
            __m128 atmp[Sz / 2];
            __m128 btmp[Sz / 2];
            for (int i = 0; i < nu; i++)
            {
                for (int j = 0; j < nv; j++)
                {
                    tmp[j] = p[j * nu + i];
                }
                sse_bezier_split(&atmp[0], &btmp[0], &tmp[0], nv, v);
                for (int j = 0; j < nv; j++)
                {
                    a[j * nu + i] = atmp[j];
                    b[j * nu + i] = btmp[j];
                }
            }
        }

        static inline void sse_bezier_split_v(__m128 a[], __m128 b[], const __m128 p[], int nu, int nv, float v)
        {
            __m128 vv = _mm_set1_ps(v);
            sse_bezier_split_v(a, b, p, nu, nv, vv);
        }

        static inline void sse_bezier_crop(__m128 a[], const __m128 p[], int n, float t0, float t1)
        {
            __m128 tmp0[Sz / 2];
            __m128 tmp1[Sz / 2];
            sse_bezier_split(&tmp0[0], &tmp1[0], p, n, t1);
            sse_bezier_split(&tmp1[0], a, &tmp0[0], n, t0 / t1);
        }

        static inline void sse_bezier_crop_tmp(__m128 a[], const __m128 p[], int n, __m128 t1, __m128 tx)
        {
            __m128 tmp0[Sz / 2];
            __m128 tmp1[Sz / 2];
            sse_bezier_split(&tmp0[0], &tmp1[0], p, n, t1);
            sse_bezier_split(&tmp1[0], a, &tmp0[0], n, tx);
        }

        static inline void sse_bezier_crop_u(__m128 a[], const __m128 p[], int nu, int nv, float u0, float u1)
        {
            __m128 uu1 = _mm_set1_ps(u1);
            __m128 uux = _mm_set1_ps(u0 / u1);
            for (int i = 0; i < nv; i++)
            {
                sse_bezier_crop_tmp(&a[i * nu + 0], &p[i * nu + 0], nu, uu1, uux);
            }
        }

        static inline void sse_bezier_crop_v(__m128 a[], const __m128 p[], int nu, int nv, float v0, float v1)
        {
            __m128 vv1 = _mm_set1_ps(v1);
            __m128 vvx = _mm_set1_ps(v0 / v1);
            __m128 tmp[Sz / 2];
            __m128 out[Sz / 2];
            for (int i = 0; i < nu; i++)
            {
                for (int j = 0; j < nv; j++)
                    tmp[j] = p[j * nu + i];
                sse_bezier_crop_tmp(&out[0], &tmp[0], nv, vv1, vvx);
                for (int j = 0; j < nv; j++)
                    a[j * nu + i] = out[j];
            }
        }

        static inline float sse_sum(const __m128 m)
        {
            float_array mm;
            _mm_store_ps(mm, m);
            return mm[0] + mm[1] + mm[2] + mm[3];
        }

        static inline void sse_transform(const __m128 mat[4], __m128 cp[], int n)
        {
            for (int i = 0; i < n; i++)
            {
                __m128 tmp[4];
                tmp[0] = _mm_mul_ps(mat[0], cp[i]);
                tmp[1] = _mm_mul_ps(mat[1], cp[i]);
                tmp[2] = _mm_mul_ps(mat[2], cp[i]);
                tmp[3] = _mm_mul_ps(mat[3], cp[i]);
                _MM_TRANSPOSE4_PS(tmp[3], tmp[2], tmp[1], tmp[0]);
                cp[i] = _mm_add_ps(_mm_add_ps(tmp[0], tmp[1]), _mm_add_ps(tmp[2], tmp[3]));
            }
        }
    };

    template <size_t Sz = 16>
    class sse_bezier_patch
    {
    public:
        typedef __m128 value_type;
        typedef sse_bezier_patch<Sz> this_type;
        typedef sse_bezier_evaluator<Sz> calc_type;
        typedef typename calc_type::float_array float_array;

        static const int DEFAUL__m128_ORDER = 4;

    public:
        static float safe_convert(float x)
        {
            return x;
        }

        static float safe_convert(double x)
        {
            static const double FMAX = +(double)std::numeric_limits<float>::max();
            static const double FMIN = -(double)std::numeric_limits<float>::max();
            if (FMAX < x) return (float)FMAX;
            if (x < FMIN) return (float)FMIN;
            return (float)x;
        }

        static inline __m128 convert(const vector3& v)
        {
            return _mm_set_ps(safe_convert(v[0]), safe_convert(v[1]), safe_convert(v[2]), 1.0f);
        }

        static inline vector3 convert(__m128 m)
        {
            float_array f;
            _mm_storer_ps(f, m);
            //assert(f[3] == 1.0f);
            return vector3(f[0], f[1], f[2]);
        }

    public:
        sse_bezier_patch()
            : nu_(DEFAUL__m128_ORDER), nv_(DEFAUL__m128_ORDER) {}
        sse_bezier_patch(const bezier_patch<vector3>& rhs)
            : nu_(rhs.get_nu()), nv_(rhs.get_nv())
        {
            int nu = nu_;
            int nv = nv_;
            assert(nu * nv <= Sz);
            for (int j = 0; j < nv; j++)
            {
                for (int i = 0; i < nu; i++)
                {
                    cp_[j * nu + i] = convert(rhs.get_cp_at(i, j));
                }
            }
        }

        vector3 evaluate(real u, real v) const
        {
            return convert(calc_type::sse_bezier_evaluate(&(cp_[0]), nu_, nv_, (float)u, (float)v));
        }

        int get_nu() const { return nu_; }
        int get_nv() const { return nv_; }
        __m128 get_at(int i, int j) const { return cp_[nu_ * j + i]; }
        vector3 get_cp_at(int i, int j) const { return convert(get_at(i, j)); }

        vector3 min() const
        {
            return convert(calc_type::sse_bezier_min(&(cp_[0]), nu_ * nv_));
        }

        vector3 max() const
        {
            return convert(calc_type::sse_bezier_max(&(cp_[0]), nu_ * nv_));
        }

        void minmax(vector3& min, vector3& max) const
        {
            min = convert(calc_type::sse_bezier_min(&(cp_[0]), nu_ * nv_));
            max = convert(calc_type::sse_bezier_max(&(cp_[0]), nu_ * nv_));
        }

#define SF_(X) safe_convert(X)
        this_type& transform(const matrix3& m)
        {
            __m128 mat[4];
            mat[0] = _mm_set_ps(SF_(m[0][0]), SF_(m[0][1]), SF_(m[0][2]), 0.0f);
            mat[1] = _mm_set_ps(SF_(m[1][0]), SF_(m[1][1]), SF_(m[1][2]), 0.0f);
            mat[2] = _mm_set_ps(SF_(m[2][0]), SF_(m[2][1]), SF_(m[2][2]), 0.0f);
            mat[3] = _mm_set_ps(0.0f, 0.0f, 0.0f, 1.0f);
            calc_type::sse_transform(mat, cp_, nu_ * nv_);
            return *this;
        }

        this_type& transform(const matrix4& m)
        {
            __m128 mat[4];
            mat[0] = _mm_set_ps(SF_(m[0][0]), SF_(m[0][1]), SF_(m[0][2]), SF_(m[0][3]));
            mat[1] = _mm_set_ps(SF_(m[1][0]), SF_(m[1][1]), SF_(m[1][2]), SF_(m[1][3]));
            mat[2] = _mm_set_ps(SF_(m[2][0]), SF_(m[2][1]), SF_(m[2][2]), SF_(m[2][3]));
            mat[3] = _mm_set_ps(SF_(m[3][0]), SF_(m[3][1]), SF_(m[3][2]), SF_(m[3][3]));
            calc_type::sse_transform(mat, cp_, nu_ * nv_);
            return *this;
        }
#undef SF_
    public:
        void split_u(this_type patches[2], real u) const
        {
            int nu = nu_;
            int nv = nv_;
            int sz = nu * nv;
            for (int i = 0; i < 2; i++)
            {
                patches[i].nu_ = nu;
                patches[i].nv_ = nv;
            }
            calc_type::sse_bezier_split_u(
                &(patches[0].cp_[0]),
                &(patches[1].cp_[0]),
                &(cp_[0]),
                nu, nv,
                (float)u);
        }
        void split_v(this_type patches[2], real v) const
        {
            int nu = nu_;
            int nv = nv_;
            int sz = nu * nv;
            for (int i = 0; i < 2; i++)
            {
                patches[i].nu_ = nu;
                patches[i].nv_ = nv;
            }
            calc_type::sse_bezier_split_v(
                &(patches[0].cp_[0]),
                &(patches[1].cp_[0]),
                &(cp_[0]),
                nu, nv,
                (float)v);
        }

        void crop_u(this_type& patch, real u0, real u1) const
        {
            int nu = nu_;
            int nv = nv_;
            int sz = nu * nv;
            {
                patch.nu_ = nu;
                patch.nv_ = nv;
            }
            calc_type::sse_bezier_crop_u(
                &(patch.cp_[0]),
                &(cp_[0]),
                nu, nv,
                (float)u0, (float)u1);
        }

        void crop_v(this_type& patch, real v0, real v1) const
        {
            int nu = nu_;
            int nv = nv_;
            int sz = nu * nv;
            {
                patch.nu_ = nu;
                patch.nv_ = nv;
            }
            calc_type::sse_bezier_crop_v(
                &(patch.cp_[0]),
                &(cp_[0]),
                nu, nv,
                (float)v0, (float)v1);
        }

    private:
        int nu_;
        int nv_;
        __m128 cp_[Sz];
    };
}

#endif
