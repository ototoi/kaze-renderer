#ifndef KAZE_BEZIER_PATCH_INTERSECTION_H
#define KAZE_BEZIER_PATCH_INTERSECTION_H

#include "bounded_intersection.h"
#include "bezier_patch.hpp"

namespace kaze
{

    class bezier_patch_intersection : public bounded_intersection
    {
    public:
        bezier_patch_intersection(const bezier_patch<vector3>& patch);
        bezier_patch_intersection(const bezier_patch<vector3>& patch, real u0, real u1, real v0, real v1);
        ~bezier_patch_intersection();

    public:
        bool test(const ray& r, real tmin, real tmax) const;
        bool test(test_info* info, const ray& r, real tmin, real tmax) const;

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    protected:
        bool test_internal(const ray& r, real tmin, real tmax, real eps) const;
        bool test_internal(test_info* info, const ray& r, real tmin, real tmax, real eps) const;

    protected:
        bezier_patch<vector3> patch_;
        real urange_[2];
        real vrange_[2];
        vector3 min_;
        vector3 max_;
    };
}

#endif
