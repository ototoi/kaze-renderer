#ifndef KAZE_NURBS_PATCH_INTERSECTION_H
#define KAZE_NURBS_PATCH_INTERSECTION_H

#include "bounded_intersection.h"
#include "nurbs_patch.hpp"

namespace kaze
{

    class nurbs_patch_intersection : public bounded_intersection
    {
    public:
        nurbs_patch_intersection(const nurbs_patch<vector3>& patch);
        ~nurbs_patch_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    public:
        intersection* inter_;
    };
}

#endif
