#ifndef KAZE_POLYTRIM_PATCH_INTERSECTION_H
#define KAZE_POLYTRIM_PATCH_INTERSECTION_H

#include "bounded_intersection.h"
#include "composite_intersection.h"
#include "patch_trimmer.h"
#include <vector>

namespace kaze
{

    class polytrim_patch_intersection : public bounded_intersection
    {
    public:
        polytrim_patch_intersection(
            const std::vector<vector2>& outer_loop,
            const std::vector<std::vector<vector2> >& inner_loops,
            const std::vector<std::shared_ptr<intersection> >& inters);
        polytrim_patch_intersection(
            const std::vector<vector2>& outer_loop,
            const std::vector<std::shared_ptr<intersection> >& inters);
        ~polytrim_patch_intersection();

    protected:
        void initialize(
            const std::vector<vector2>& outer_loop,
            const std::vector<std::vector<vector2> >& inner_loops,
            const std::vector<std::shared_ptr<intersection> >& inters);

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        composite_intersection* cmp_;
        patch_trimmer* trm_;
    };
}

#endif
