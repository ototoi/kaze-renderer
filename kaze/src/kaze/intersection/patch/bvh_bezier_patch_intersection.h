#ifndef KAZE_BVH_BEZIER_PATCH_INTERSECTION_H
#define KAZE_BVH_BEZIER_PATCH_INTERSECTION_H

#include "bounded_intersection.h"
#include "bezier_patch_intersection.h"
#include "bvh_composite_intersection.h"

namespace kaze
{

    class bvh_bezier_patch_intersection : public bounded_intersection
    {
    public:
        bvh_bezier_patch_intersection(const bezier_patch<vector3>& patch);
        bvh_bezier_patch_intersection(const bezier_patch<vector3>& patch, real u0, real u1, real v0, real v1);
        ~bvh_bezier_patch_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    protected:
        bvh_composite_intersection* bvh_;
    };
}

#endif