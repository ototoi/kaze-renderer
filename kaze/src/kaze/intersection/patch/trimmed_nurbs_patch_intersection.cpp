#include "trimmed_nurbs_patch_intersection.h"

#include "bezier_patch.hpp"
#include "trimmed_multi_bezier_patch_intersection.h" //
#include "nurbs_to_bspline.h"
#include "bspline_to_bezier.h"

#include <vector>
#include <memory>

namespace kaze
{

    static void ConvertPatch(multi_bezier_patch<vector3>& mpatch, const nurbs_patch<vector3>& patch)
    {
        bspline_patch<vector3> bpatch;
        nurbs_to_bspline(bpatch, patch);
        bspline_to_bezier(mpatch, bpatch);
    }

    static void ConvertCurve(multi_bezier_curve<vector2>& mcurve, const nurbs_curve<vector2>& curve)
    {
        bspline_curve<vector2> bcurve;
        nurbs_to_bspline(bcurve, curve);
        bspline_to_bezier(mcurve, bcurve);
    }

    static void ConvertLoop(
        std::vector<multi_bezier_curve<vector2> >& mloop,
        const std::vector<nurbs_curve<vector2> >& loop)
    {
        size_t sz = loop.size();
        mloop.reserve(sz);
        for (size_t i = 0; i < sz; i++)
        {
            multi_bezier_curve<vector2> tmp;
            ConvertCurve(tmp, loop[i]);
            mloop.push_back(tmp);
        }
    }

    static void ConvertLoops(
        std::vector<std::vector<multi_bezier_curve<vector2> > >& mloops,
        const std::vector<std::vector<nurbs_curve<vector2> > >& loops)
    {
        if (loops.empty()) return;
        size_t sz = loops.size();
        mloops.reserve(sz);
        for (size_t i = 0; i < sz; i++)
        {
            std::vector<multi_bezier_curve<vector2> > tmp;
            ConvertLoop(tmp, loops[i]);
            mloops.push_back(tmp);
        }
    }

    trimmed_nurbs_patch_intersection::trimmed_nurbs_patch_intersection(
        const nurbs_patch<vector3>& patch,
        const std::vector<std::vector<nurbs_curve<vector2> > >& outer_loops,
        const std::vector<std::vector<nurbs_curve<vector2> > >& inner_loops)
        : inter_(NULL)
    {
        multi_bezier_patch<vector3> mpatch;
        ConvertPatch(mpatch, patch);

        std::vector<std::vector<multi_bezier_curve<vector2> > > ol;
        std::vector<std::vector<multi_bezier_curve<vector2> > > il;
        ConvertLoops(ol, outer_loops);
        ConvertLoops(il, inner_loops);

        inter_ = new trimmed_multi_bezier_patch_intersection(mpatch, ol, il);
    }

    trimmed_nurbs_patch_intersection::~trimmed_nurbs_patch_intersection()
    {
        if (inter_) delete inter_;
    }

    bool trimmed_nurbs_patch_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool trimmed_nurbs_patch_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void trimmed_nurbs_patch_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }

    vector3 trimmed_nurbs_patch_intersection::min() const
    {
        return inter_->min();
    }

    vector3 trimmed_nurbs_patch_intersection::max() const
    {
        return inter_->max();
    }
}
