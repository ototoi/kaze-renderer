#ifndef KAZE_MULTI_BEZIER_PATCH_INTERSECTION_H
#define KAZE_MULTI_BEZIER_PATCH_INTERSECTION_H

#include "bounded_intersection.h"
#include "bezier_patch.hpp"
#include "multi_bezier_patch.hpp"

#include "composite_intersection.h"

namespace kaze
{

    class multi_bezier_patch_intersection : public bounded_intersection
    {
    public:
        multi_bezier_patch_intersection(const multi_bezier_patch<vector3>& patch);
        multi_bezier_patch_intersection(const std::vector<real>& knot_u, const std::vector<real>& knot_v, const std::vector<bezier_patch<vector3> >& patches);
        ~multi_bezier_patch_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        multi_bezier_patch_intersection(const multi_bezier_patch_intersection& rhs);
        multi_bezier_patch_intersection& operator=(const multi_bezier_patch_intersection& rhs);

    protected:
        composite_intersection* cmp_;
    };
}

#endif