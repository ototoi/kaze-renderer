#include "bezier_patch_intersection.h"
#include "bilinear_patch_intersection.h"

#include "test_info.h"
#include "intersection_ex.h"

#include <iostream>

//
#define DIRECT_BILINEAR 0
#define USE_BEZIERCLIP 1
#define USE_COARSESORT 1

#define USE_SSE 0

#if USE_SSE
#include "sse_bezier_patch.h"
#endif

#ifdef _MSC_VER
#define INLINE __forceinline
//define INLINE inline
#elif defined(__GNUC__)
#define INLINE __inline__
//define INLINE inline
#else
#define INLINE inline
#endif

namespace kaze
{
    static const real EPS = std::numeric_limits<real>::epsilon() * 10;
    static const real UVEPS = 1e-2;
    static const int MAX_LEVEL = 8 * 2;
    static const int STATIC_ALLOC = 16;

    template <class PATCH, class FLOAT>
    static void get_minmax(vector3& min, vector3& max, const PATCH& patch, FLOAT eps)
    {
        patch.minmax(min, max);
        min -= vector3(eps, eps, eps);
        max += vector3(eps, eps, eps);
    }

    static void get_minmax(vector3& min, vector3& max, const std::vector<vector3>& p)
    {
        min = max = p[0];
        for (size_t i = 1; i < p.size(); i++)
        {
            for (int j = 0; j < 3; j++)
            {
                min[j] = std::min(min[j], p[i][j]);
                max[j] = std::max(max[j], p[i][j]);
            }
        }
    }

    static void getZAlign(matrix3& mat, const vector3& dir)
    {
        vector3 z = dir;
        int plane = 0;
        if (fabs(z[1]) < fabs(z[plane])) plane = 1;
        if (fabs(z[2]) < fabs(z[plane])) plane = 2;
        vector3 x = vector3(0, 0, 0);
        x[plane] = 1;
        vector3 y = normalize(cross(z, x));
        x = cross(y, z);
        mat = matrix3(x[0], x[1], x[2], y[0], y[1], y[2], z[0], z[1], z[2]);
    }

    static void getZAlign(matrix4& mat, const ray& r)
    {
        vector3 org = r.origin();
        vector3 dir = r.direction();

        vector3 z = dir;
        int plane = 0;
        if (fabs(z[1]) < fabs(z[plane])) plane = 1;
        if (fabs(z[2]) < fabs(z[plane])) plane = 2;
        vector3 x = vector3(0, 0, 0);
        x[plane] = 1;
        vector3 y = normalize(cross(z, x));
        x = cross(y, z);
        matrix4 rot = matrix4(
            x[0], x[1], x[2], 0,
            y[0], y[1], y[2], 0,
            z[0], z[1], z[2], 0,
            0, 0, 0, 1);
        matrix4 trs = mat4_gen::translation(-org[0], -org[1], -org[2]);

        mat = rot * trs;
    }

    struct uvt_info
    {
        real u;
        real v;
        real t;
    };

    template <class VECTOR>
    static INLINE bool scan_minmax(const VECTOR& p)
    {
        size_t sz = p.size();
        int nUpper = 0;
        int nLower = 0;
        for (size_t i = 0; i < sz; i++)
        {
            nUpper += (p[i][1] > 0) ? 1 : 0;
            nLower += (p[i][1] < 0) ? 1 : 0;
            if (nUpper && nLower) return true;
        }
        return false;
    }

    template <class T, int Sz>
    class static_vector
    {
    public:
        static_vector() : sz_(0) {}
        static_vector(int sz) : sz_(sz) {}
        size_t size() const { return sz_; }
        void push_back(const T& v)
        {
            e_[sz_] = v;
            sz_++;
        }
        void pop_back() { sz_--; }
        T& operator[](size_t i) { return e_[i]; }
        const T& operator[](size_t i) const { return e_[i]; }
    private:
        T e_[Sz];
        int sz_;
    };

    template <class T, size_t Sz = 64>
    class static_bezier_patch
    {
    public:
        typedef T value_type;
        typedef static_bezier_patch<T, Sz> this_type;

        static const int DEFAULT_ORDER = 4;

    public:
        static_bezier_patch()
            : nu_(DEFAULT_ORDER), nv_(DEFAULT_ORDER) {}
        static_bezier_patch(const bezier_patch<T>& rhs)
            : nu_(rhs.get_nu()), nv_(rhs.get_nv())
        {
            int nu = nu_;
            int nv = nv_;
            assert(nu * nv <= Sz);
            for (int j = 0; j < nv; j++)
            {
                for (int i = 0; i < nu; i++)
                {
                    cp_[j * nu + i] = rhs.get_cp_at(i, j);
                }
            }
        }

        T evaluate(real u, real v) const
        {
            return bezier_evaluate(&(cp_[0]), nu_, nv_, u, v);
        }

        int get_nu() const { return nu_; }
        int get_nv() const { return nv_; }
        T get_at(int i, int j) const { return cp_[nu_ * j + i]; }
        T get_cp_at(int i, int j) const { return get_at(i, j); }

        T min() const
        {
            return bezier_min(&(cp_[0]), nu_ * nv_);
        }

        T max() const
        {
            return bezier_max(&(cp_[0]), nu_ * nv_);
        }

        void minmax(T& min, T& max) const
        {
            bezier_minmax(min, max, &(cp_[0]), nu_ * nv_);
        }

        this_type& transform(const matrix3& m)
        {
            size_t sz = nu_ * nv_;
            for (size_t i = 0; i < sz; i++)
            {
                cp_[i] = m * cp_[i];
            }
            return *this;
        }

        this_type& transform(const matrix4& m)
        {
            size_t sz = nu_ * nv_;
            for (size_t i = 0; i < sz; i++)
            {
                cp_[i] = m * cp_[i];
            }
            return *this;
        }

    public:
        void split_u(this_type patches[2], real u) const
        {
            int nu = nu_;
            int nv = nv_;
            int sz = nu * nv;
            for (int i = 0; i < 2; i++)
            {
                patches[i].nu_ = nu;
                patches[i].nv_ = nv;
                //patches[i].cp_.resize(sz);
            }
            bezier_split_u(
                &(patches[0].cp_[0]),
                &(patches[1].cp_[0]),
                &(cp_[0]),
                nu, nv,
                u);
        }

        void split_v(this_type patches[2], real v) const
        {
            int nu = nu_;
            int nv = nv_;
            int sz = nu * nv;
            for (int i = 0; i < 2; i++)
            {
                patches[i].nu_ = nu;
                patches[i].nv_ = nv;
                //patches[i].cp_.resize(sz);
            }
            bezier_split_v(
                &(patches[0].cp_[0]),
                &(patches[1].cp_[0]),
                &(cp_[0]),
                nu, nv,
                v);
        }

        void crop_u(this_type& patch, real u0, real u1) const
        {
            int nu = nu_;
            int nv = nv_;
            int sz = nu * nv;
            {
                patch.nu_ = nu;
                patch.nv_ = nv;
                //patch.cp_.resize(sz);
            }
            bezier_crop_u(
                &(patch.cp_[0]),
                &(cp_[0]),
                nu, nv,
                u0, u1);
        }

        void crop_v(this_type& patch, real v0, real v1) const
        {
            int nu = nu_;
            int nv = nv_;
            int sz = nu * nv;
            {
                patch.nu_ = nu;
                patch.nv_ = nv;
                //patch.cp_.resize(sz);
            }
            bezier_crop_v(
                &(patch.cp_[0]),
                &(cp_[0]),
                nu, nv,
                v0, v1);
        }

    private:
        int nu_;
        int nv_;
        T cp_[Sz];
    };

//define converx intersection
#if 0  
    static
    INLINE real cross(real u1, real u2, real v1, real v2)
    {
        return -v1*u2+u1*v2;
    }
    
    static
    INLINE real check_line(const vector2& a, const vector2& b, const vector2& p)
    {
        vector2 ab = b-a;
        vector2 ap = p-a;
        
        return cross(ab[0],ab[1],ap[0],ap[1]);
    }
    
    template<class VECTOR>
    static INLINE void push_upper_chain(VECTOR& chain, const vector2& p)
    {
        int sz = (int)chain.size();
        if(sz < 2){
            chain.push_back(p);
        }else{
            vector2 a = chain[sz-2];//
            vector2 b = chain[sz-1];//
            if(0<=check_line(a,b,p)){
                chain.pop_back();
                push_upper_chain(chain,p);
            }else{
                chain.push_back(p);
            }
        }
    }
    
    template<class VECTOR>
    static INLINE void push_lower_chain(VECTOR& chain, const vector2& p)
    {
        int sz = (int)chain.size();
        if(sz < 2){
            chain.push_back(p);
        }else{
            vector2 a = chain[sz-2];//
            vector2 b = chain[sz-1];//
            if(0>=check_line(a,b,p)){
                chain.pop_back();
                push_lower_chain(chain,p);
            }else{
                chain.push_back(p);
            }
        }
    }
        
    template<class VECTOR>
    static INLINE void convex_hull(VECTOR& upper_chain, VECTOR& lower_chain, const std::vector<vector2>& points) 
    {
        using namespace std;
        int sz = (int)points.size();
        if(sz<2)return;
        upper_chain.push_back(points[0]);
        upper_chain.push_back(points[1]);
        for(int i = 2;i<sz;i++){
            push_upper_chain(upper_chain,points[i]);
        }
        lower_chain.push_back(points[0]);
        lower_chain.push_back(points[1]);
        for(int i = 2;i<sz;i++){
            push_lower_chain(lower_chain,points[i]);
        }
    }
    
    template<class VECTOR>
    static INLINE int scan_chain(real t[], const VECTOR& chain){
        int n = 0;
        size_t sz = chain.size();
        if(sz<1)return 0;
        for(size_t i = 0;i<sz-1;i++)
        {
            if(chain[i][1]*chain[i+1][1]<=0)
            {
                real a = fabs(chain[i  ][1]);
                real b = fabs(chain[i+1][1]);
                real h = a+b;
                if(h){
                    real k = chain[i][0] + a*(chain[i+1][0]-chain[i][0])/h;
                    t[n] = k;
                    n++;
                }
            }
        }
        return n;
    }
    
    template<class VECTOR>
    static int scan_convex(real ts[], const VECTOR& p)
    {
        if(!scan_minmax(p))
        {          
            return 0;
        }

        size_t sz = p.size();
        if(sz<=4){
            static_vector<vector2,4> upper_chain;
            static_vector<vector2,4> lower_chain;
            convex_hull(upper_chain,lower_chain,p);
            
            int n = 0;
            n += scan_chain(&ts[n],upper_chain);
            n += scan_chain(&ts[n],lower_chain);
            return n;
        }else{
            std::vector<vector2> upper_chain;upper_chain.reserve(sz);
            std::vector<vector2> lower_chain;lower_chain.reserve(sz);
            convex_hull(upper_chain,lower_chain,p);
            
            int n = 0;
            n += scan_chain(&ts[n],upper_chain);
            n += scan_chain(&ts[n],lower_chain);
            return n;
        }
    }
#else
    static INLINE real slope(const vector2& a, const vector2& b)
    {
        vector2 dif = b - a;
        return fabs(dif[0] / dif[1]);
    }

    static INLINE real dist(const vector2& p0, const vector2& p1)
    {
        real tt = fabs(p0[1] / (p1[1] - p0[1]));
        real t = p0[0] + tt * (p1[0] - p0[0]);
        return t;
    }

    template <class VECTOR>
    static INLINE int scan_convex(real ts[], const VECTOR& p)
    {
        if (!scan_minmax(p))
        {
            return 0;
        }
        int n = 0;
        {
            int k = -1;
            int l = -1;
            real current = values::far();
            int sz = (int)p.size();
            for (int i = 1; i < sz; i++)
            {
                if (p[i][1] * p[0][1] < 0)
                {
                    real s = slope(p[i], p[0]);
                    if (s < current)
                    {
                        current = s;
                        k = i;
                    }
                }
            }
            if (k >= 0)
            {
                current = 0;
                for (int i = 0; i < k; i++)
                {
                    if (p[i][1] * p[k][1] < 0)
                    {
                        real s = slope(p[i], p[k]);
                        if (current < s)
                        {
                            current = s;
                            l = i;
                        }
                    }
                }
                if (l >= 0)
                {
                    ts[n++] = dist(p[l], p[k]);
                }
            }
        }
        {
            int k = -1;
            int l = -1;
            real current = values::far();
            int sz = (int)p.size();
            for (int i = 0; i < sz - 1; i++)
            {
                if (p[i][1] * p[sz - 1][1] < 0)
                {
                    real s = slope(p[i], p[sz - 1]);
                    if (s < current)
                    {
                        current = s;
                        k = i;
                    }
                }
            }
            if (k >= 0)
            {
                current = 0;
                for (int i = k + 1; i < sz; i++)
                {
                    if (p[i][1] * p[k][1] < 0)
                    {
                        real s = slope(p[i], p[k]);
                        if (current < s)
                        {
                            current = s;
                            l = i;
                        }
                    }
                }
                if (l >= 0)
                {
                    ts[n++] = dist(p[k], p[l]);
                }
            }
        }

        return n;
    }
#endif

    static inline real lerp(real a, real b, real u)
    {
        real t = real(1) - (real(1) - u);
        real s = real(1) - t;
        return s * a + t * b;
    }

    template <class PATCH>
    static inline void crop_u(PATCH& out, const PATCH& patch, real u0, real u1)
    {
        patch.crop_u(out, u0, u1);
    }

    template <class PATCH>
    static inline void crop_v(PATCH& out, const PATCH& patch, real v0, real v1)
    {
        patch.crop_v(out, v0, v1);
    }

    template <class VECTOR>
    static bool x_check(real rng[2], const VECTOR& curve)
    {
        real t[4] = {0};
        int nn = scan_convex(t, curve);
        if (nn)
        {
            real t0 = t[0];
            real t1 = t[0];

            for (int i = 1; i < nn; i++)
            {
                t0 = std::min(t0, t[i]);
                t1 = std::max(t1, t[i]);
            }

            rng[0] = t0;
            rng[1] = t1;
            return true;
        }
        return false;
    }

//#define PRINT_A() std::cout << "+"
//#define PRINT_B() std::cout << "-"
#define PRINT_A()
#define PRINT_B()

    static inline vector3 normalize2(const vector3& v)
    {
        real l = v[0] * v[0] + v[1] * v[1];
        l = 1.0 / sqrt(l);
        return vector3(v[0] * l, v[1] * l, 0);
    }
    static inline vector3 cross2(const vector3& x)
    {
        return vector3(
            -x[1], //xyzzy
            +x[0], //yzxxz
            0);
    }

    static void getRotate(matrix3& mat, const vector3& dx)
    {
        static const vector3 z = vector3(0, 0, 1);
        vector3 x = normalize2(dx); //x = normalize(x)
        vector3 y = cross2(x);      //cross(z,x)
        mat = matrix3(x[0], x[1], x[2], y[0], y[1], y[2], z[0], z[1], z[2]);
    }

    template <class PATCH>
    static inline vector3 getLv(const PATCH& patch)
    {
        int nu = patch.get_nu();
        int nv = patch.get_nv();

        return patch.get_cp_at(0, nv - 1) - patch.get_cp_at(0, 0) +
               patch.get_cp_at(nu - 1, nv - 1) - patch.get_cp_at(nu - 1, 0);
    }

    template <class PATCH>
    static inline vector3 getLu(const PATCH& patch)
    {
        int nu = patch.get_nu();
        int nv = patch.get_nv();

        return patch.get_cp_at(nu - 1, 0) - patch.get_cp_at(0, 0) +
               patch.get_cp_at(nu - 1, nv - 1) - patch.get_cp_at(0, nv - 1);
    }

    static double tri_area(const vector2& a, const vector2& b, const vector2& c)
    {
        return (a[0] - c[0]) * (b[1] - c[1]) - (a[1] - c[1]) * (b[0] - c[0]);
    }

    /*
    //site:
    //Robust and Numerically Stable Bezier Clipping Method for Ray Tracing NURBS Surfaces
    //
    template<class PATCH>
    static
    vector3 getLv2(const PATCH& patch)
    {
        static const real K1 = 0.5;
        static const real K2 = sqrt(3.0)*0.5;
        static const vector2 O(0,0);

        vector2 lu = normalize2(getLu(patch));
        vector2 lv = normalize2(getLv(patch));

        real c = fabs(dot(lu, lv));

        if(c>0.5){
            vector2 M = 0.5*(lu+lv);
            vector2 N1 = vector2(M[1],-M[0]);
            vector2 N2 = vector2(-M[1],M[0]);

            if(tri_area(lu,O,M)<0){
                std::swap(N1,N2);
            }

            vector2 R = K1*N1 + K2*M;
            return vector3(R[0],R[1],0);          
        }else{
            return vector3(lv[0],lv[1],0);                  
        }      
    }

    template<class PATCH>
    static
    vector3 getLu2(const PATCH& patch)
    {
        static const real K1 = 0.5;
        static const real K2 = sqrt(3.0)*0.5;
        static const vector2 O(0,0);

        vector2 lu = normalize2(getLu(patch));
        vector2 lv = normalize2(getLv(patch));

        real c = fabs(dot(lu, lv));

        if(c>0.5){
            vector2 M = 0.5*(lu+lv);
            vector2 N1 = vector2(M[1],-M[0]);
            vector2 N2 = vector2(-M[1],M[0]);

            if(tri_area(lu,O,M)<0){
                std::swap(N1,N2);
            }

            vector2 R = K1*N2 + K2*M;
            return vector3(R[0],R[1],0);          
        }else{
            return vector3(lu[0],lu[1],0);            
        }      
    }
    */
    template <class PATCH>
    static void rotate_u(PATCH& patch)
    {
        vector3 dx = getLv(patch);
        matrix3 rot;
        getRotate(rot, dx);
        patch.transform(rot);
    }

    template <class PATCH>
    static void rotate_v(PATCH& patch)
    {
        vector3 dx = getLu(patch);
        matrix3 rot;
        getRotate(rot, dx);
        patch.transform(rot);
    }

    template <class PATCH>
    static bool get_range_u(real out[2], const PATCH& patch)
    {
        int nu = patch.get_nu();
        int nv = patch.get_nv();

        real t0 = 1;
        real t1 = 0;

        if (nu <= 16)
        {
            static_vector<vector2, 16> curve(nu);
            real delta = 1.0 / (nu - 1);
            for (int i = 0; i < nu; i++)
            {
                curve[i][0] = i * delta;
            }
            real rng[2];
            for (int j = 0; j < nv; j++)
            {
                for (int i = 0; i < nu; i++)
                {
                    curve[i][1] = (patch.get_cp_at(i, j))[1];
                }
                if (x_check(rng, curve))
                {
                    t0 = std::min(t0, rng[0]);
                    t1 = std::max(t1, rng[1]);
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            std::vector<vector2> curve(nu);
            real delta = 1.0 / (nu - 1);
            for (int i = 0; i < nu; i++)
            {
                curve[i][0] = i * delta;
            }
            real rng[2];
            for (int j = 0; j < nv; j++)
            {
                for (int i = 0; i < nu; i++)
                {
                    curve[i][1] = (patch.get_cp_at(i, j))[1];
                }
                if (x_check(rng, curve))
                {
                    t0 = std::min(t0, rng[0]);
                    t1 = std::max(t1, rng[1]);
                }
                else
                {
                    return false;
                }
            }
        }

        out[0] = t0;
        out[1] = t1;
        return true;
    }

    template <class PATCH>
    static bool get_range_v(real out[2], const PATCH& patch)
    {
        int nu = patch.get_nu();
        int nv = patch.get_nv();

        real t0 = 1;
        real t1 = 0;

        if (nv <= 16)
        {
            static_vector<vector2, 16> curve(nv);
            real delta = 1.0 / (nv - 1);
            for (int i = 0; i < nv; i++)
            {
                curve[i][0] = i * delta;
            }

            real rng[2];
            for (int i = 0; i < nu; i++)
            {
                for (int j = 0; j < nv; j++)
                {
                    curve[j][1] = (patch.get_cp_at(i, j))[1];
                }
                if (x_check(rng, curve))
                {
                    t0 = std::min(t0, rng[0]);
                    t1 = std::max(t1, rng[1]);
                }
                else
                {
                    return false;
                }
            }
        }
        else
        {
            std::vector<vector2> curve(nv);
            real delta = 1.0 / (nv - 1);
            for (int i = 0; i < nv; i++)
            {
                curve[i][0] = i * delta;
            }

            real rng[2];
            for (int i = 0; i < nu; i++)
            {
                for (int j = 0; j < nv; j++)
                {
                    curve[j][1] = (patch.get_cp_at(i, j))[1];
                }
                if (x_check(rng, curve))
                {
                    t0 = std::min(t0, rng[0]);
                    t1 = std::max(t1, rng[1]);
                }
                else
                {
                    return false;
                }
            }
        }

        out[0] = t0;
        out[1] = t1;
        return true;
    }

    template <class PATCH>
    static bool test_bezier_clip_l(const PATCH& patch, real u0, real u1, real v0, real v1, real tmin, real tmax);

    template <class PATCH>
    static bool test_bezier_clip_u(const PATCH& patch, real u0, real u1, real v0, real v1, real tmin, real tmax, int level, int max_level, real eps);
    template <class PATCH>
    static bool test_bezier_clip_v(const PATCH& patch, real u0, real u1, real v0, real v1, real tmin, real tmax, int level, int max_level, real eps);

    template <class PATCH>
    static bool test_bezier_clip_l(uvt_info* info, const PATCH& patch, real u0, real u1, real v0, real v1, real tmin, real tmax);
    template <class PATCH>
    static bool test_bezier_clip_u(uvt_info* info, const PATCH& patch, real u0, real u1, real v0, real v1, real tmin, real tmax, int level, int max_level, real eps);
    template <class PATCH>
    static bool test_bezier_clip_v(uvt_info* info, const PATCH& patch, real u0, real u1, real v0, real v1, real tmin, real tmax, int level, int max_level, real eps);

    template <class PATCH>
    static bool test_bezier_clip_l(const PATCH& patch, real u0, real u1, real v0, real v1, real tmin, real tmax)
    {
        real t;
        real u;
        real v;
#if DIRECT_BILINEAR
        vector3 P[4];
        int nu = patch.get_nu();
        int nv = patch.get_nv();
        P[0] = patch.get_cp_at(0, 0);
        P[1] = patch.get_cp_at(nu - 1, 0);
        P[2] = patch.get_cp_at(0, nv - 1);
        P[3] = patch.get_cp_at(nu - 1, nv - 1);

        if (test_bilinear_patch(&t, &u, &v, P, tmin, tmax))
        {
            return true;
        }
        return false;
#else
        //bool bRet = false;
        int nPu = patch.get_nu() - 1;
        int nPv = patch.get_nv() - 1;

        real du = real(1) / nPu;
        real dv = real(1) / nPv;

        vector3 P[4];
        for (int j = 0; j < nPv; j++)
        {
            for (int i = 0; i < nPu; i++)
            {
                P[0] = patch.get_cp_at(i, j);
                P[1] = patch.get_cp_at(i + 1, j);
                P[2] = patch.get_cp_at(i, j + 1);
                P[3] = patch.get_cp_at(i + 1, j + 1);
                if (test_bilinear_patch(&t, &u, &v, P, tmin, tmax))
                {
                    return true;
                }
            }
        }
        return false;
#endif
    }

    template <class PATCH>
    static bool test_bezier_clip_l(uvt_info* info, const PATCH& patch, real u0, real u1, real v0, real v1, real tmin, real tmax)
    {
        real t;
        real u;
        real v;
#if DIRECT_BILINEAR
        vector3 P[4];
        int nu = patch.get_nu();
        int nv = patch.get_nv();
        P[0] = patch.get_cp_at(0, 0);
        P[1] = patch.get_cp_at(nu - 1, 0);
        P[2] = patch.get_cp_at(0, nv - 1);
        P[3] = patch.get_cp_at(nu - 1, nv - 1);

        if (test_bilinear_patch(&t, &u, &v, P, tmin, tmax))
        {
            u = u0 * (1 - u) + u1 * u;
            v = v0 * (1 - v) + v1 * v;
            info->u = u;
            info->v = v;
            info->t = t;
            return true;
        }
        return false;
#else
        bool bRet = false;
        int nPu = patch.get_nu() - 1;
        int nPv = patch.get_nv() - 1;

        real du = real(1) / nPu;
        real dv = real(1) / nPv;

        real uu = 0;
        real vv = 0;

        vector3 P[4];
        for (int j = 0; j < nPv; j++)
        {
            for (int i = 0; i < nPu; i++)
            {
                P[0] = patch.get_cp_at(i, j);
                P[1] = patch.get_cp_at(i + 1, j);
                P[2] = patch.get_cp_at(i, j + 1);
                P[3] = patch.get_cp_at(i + 1, j + 1);
                if (test_bilinear_patch(&t, &u, &v, P, tmin, tmax))
                {

                    u = lerp(i * du, (i + 1) * du, u);
                    v = lerp(j * dv, (j + 1) * dv, v);

                    uu = u;
                    vv = v;

                    tmax = t;

                    u = lerp(u0, u1, u);
                    v = lerp(v0, v1, v);

                    info->u = u;
                    info->v = v;
                    info->t = t;
                    bRet = true;
                }
            }
        }

        if (bRet)
        {
            vector3 p = patch.evaluate(uu, vv);
            info->t = p[2];
        }

        return bRet;
#endif
    }

    static inline bool is_level(int level, int max_level)
    {
        return (level >= max_level);
    }

    static inline bool is_eps(const vector3& min, const vector3& max, real eps)
    {
        return ((max[1] - min[1]) <= eps) && ((max[0] - min[0]) <= eps);
    }

    static inline real log2(real x)
    {
        static const real LOG2 = 1.0 / log(2.0);
        return log(x) * LOG2;
    }

    static inline bool is_clip(int level, int nc)
    {
        static const int div_level[] =
            {
                1,
                1, 1,
                2, 2,                   //4
                3, 3, 3, 3,             //8
                4, 4, 4, 4, 4, 4, 4, 4, //16
            };
#if USE_BEZIERCLIP
        int nlevel = 0;
        if (nc <= 16)
        {
            nlevel = div_level[nc];
        }
        else
        {
            nlevel = (int)ceil(log2(nc));
        }
        return nlevel * 2 <= level;
#else
        return false;
#endif
    }

    template <class PATCH>
    static void coarse_sort(int order[2], PATCH tmp[2])
    {
#if USE_COARSESORT
        real zs[2];
        for (int i = 0; i < 2; i++)
        {
            int u = tmp[i].get_nu() >> 1;
            int v = tmp[i].get_nv() >> 1;

            zs[i] = tmp[i].get_cp_at(u, v)[2];
        }
        if (zs[0] <= zs[1])
        {
            order[0] = 0;
            order[1] = 1;
        }
        else
        {
            order[0] = 1;
            order[1] = 0;
        }
#endif
    }

    template <class PATCH>
    static bool test_bezier_clip_u(const PATCH& patch, real u0, real u1, real v0, real v1, real tmin, real tmax, int level, int max_level, real eps)
    {
        PATCH tpatch(patch);
        rotate_u(tpatch);

        vector3 min;
        vector3 max;
        get_minmax(min, max, tpatch, eps * 1e-3);

        if (0 < min[0] || max[0] < 0) return false;       //x
        if (0 < min[1] || max[1] < 0) return false;       //y
        if (max[2] < tmin || tmax < min[2]) return false; //z

        if (is_eps(min, max, eps) || is_level(level, max_level))
        {
            return test_bezier_clip_l(patch, u0, u1, v0, v1, tmin, tmax);
        }

        real tw = 1;
        real tt0 = 0;
        real tt1 = 1;

        if (is_clip(level, tpatch.get_nu()))
        {
            real rng[2];
            if (get_range_u(rng, tpatch))
            {
                tt0 = rng[0];
                tt1 = rng[1];
                tw = tt1 - tt0;
            }
        }

        if (tw >= 0.4)
        {
            PATCH tmp[2];
            patch.split_u(tmp, 0.5);
            real um = (u0 + u1) * 0.5;
            real ut[] = {u0, um, um, u1};

            int order[2] = {0, 1};
            coarse_sort(order, tmp);
            //bool bRet = false;
            for (int i = 0; i < 2; i++)
            {
                if (test_bezier_clip_v(tmp[order[i]], ut[2 * order[i]], ut[2 * order[i] + 1], v0, v1, tmin, tmax, level + 1, max_level, eps))
                {
                    return true;
                }
            }
            return false;
        }
        else
        {
            tt0 = std::max<real>(0.0, tt0 - UVEPS);
            tt1 = std::min<real>(tt1 + UVEPS, 1.0);
            real ut[] = {lerp(u0, u1, tt0), lerp(u0, u1, tt1)};
            PATCH tmp;
            crop_u(tmp, patch, tt0, tt1);
            return test_bezier_clip_v(tmp, ut[0], ut[1], v0, v1, tmin, tmax, level + 1, max_level, eps);
        }
    }

    template <class PATCH>
    static bool test_bezier_clip_v(const PATCH& patch, real u0, real u1, real v0, real v1, real tmin, real tmax, int level, int max_level, real eps)
    {
        PATCH tpatch(patch);
        rotate_v(tpatch);

        vector3 min;
        vector3 max;
        get_minmax(min, max, tpatch, eps * 1e-3);

        if (0 < min[0] || max[0] < 0) return false;       //x
        if (0 < min[1] || max[1] < 0) return false;       //y
        if (max[2] < tmin || tmax < min[2]) return false; //z

        if (is_eps(min, max, eps) || is_level(level, max_level))
        {
            return test_bezier_clip_l(patch, u0, u1, v0, v1, tmin, tmax);
        }

        real tw = 1;
        real tt0 = 0;
        real tt1 = 1;
        if (is_clip(level, tpatch.get_nv()))
        {
            real rng[2];
            if (get_range_v(rng, tpatch))
            {
                tt0 = rng[0];
                tt1 = rng[1];
                tw = tt1 - tt0;
            }
        }

        if (tw >= 0.4)
        {
            PATCH tmp[2];
            patch.split_v(tmp, 0.5);
            real vm = (v0 + v1) * 0.5;
            real vt[] = {v0, vm, vm, v1};

            int order[2] = {0, 1};
            coarse_sort(order, tmp);
            //bool bRet = false;
            for (int i = 0; i < 2; i++)
            {
                if (test_bezier_clip_u(tmp[order[i]], u0, u1, vt[2 * order[i]], vt[2 * order[i] + 1], tmin, tmax, level + 1, max_level, eps))
                {
                    return true;
                }
            }
            return false;
        }
        else
        {
            tt0 = std::max<real>(0.0, tt0 - UVEPS);
            tt1 = std::min<real>(tt1 + UVEPS, 1.0);
            real vt[] = {lerp(v0, v1, tt0), lerp(v0, v1, tt1)};
            PATCH tmp;
            crop_v(tmp, patch, tt0, tt1);
            return test_bezier_clip_u(tmp, u0, u1, vt[0], vt[1], tmin, tmax, level + 1, max_level, eps);
        }
    }

    template <class PATCH>
    static bool test_bezier_clip_u(uvt_info* info, const PATCH& patch, real u0, real u1, real v0, real v1, real tmin, real tmax, int level, int max_level, real eps)
    {
        PATCH tpatch(patch);
        rotate_u(tpatch);

        vector3 min;
        vector3 max;
        get_minmax(min, max, tpatch, eps * 1e-3);

        if (0 < min[0] || max[0] < 0) return false;       //x
        if (0 < min[1] || max[1] < 0) return false;       //y
        if (max[2] < tmin || tmax < min[2]) return false; //z

        if (is_eps(min, max, eps) || is_level(level, max_level))
        {
            return test_bezier_clip_l(info, patch, u0, u1, v0, v1, tmin, tmax);
        }

        real tw = 1;
        real tt0 = 0;
        real tt1 = 1;

        if (is_clip(level, tpatch.get_nu()))
        {
            real rng[2];
            if (get_range_u(rng, tpatch))
            {
                tt0 = rng[0];
                tt1 = rng[1];
                tw = tt1 - tt0;
            }
        }

        if (tw >= 0.4)
        {
            PATCH tmp[2];
            patch.split_u(tmp, 0.5);
            real um = (u0 + u1) * 0.5;
            real ut[] = {u0, um, um, u1};

            int order[2] = {0, 1};
            coarse_sort(order, tmp);
            bool bRet = false;
            for (int i = 0; i < 2; i++)
            {
                if (test_bezier_clip_v(info, tmp[order[i]], ut[2 * order[i]], ut[2 * order[i] + 1], v0, v1, tmin, tmax, level + 1, max_level, eps))
                {
                    tmax = info->t;
                    bRet = true;
                }
            }
            return bRet;
        }
        else
        {
            tt0 = std::max<real>(0.0, tt0 - UVEPS);
            tt1 = std::min<real>(tt1 + UVEPS, 1.0);
            real ut[] = {lerp(u0, u1, tt0), lerp(u0, u1, tt1)};
            PATCH tmp;
            crop_u(tmp, patch, tt0, tt1);
            return test_bezier_clip_v(info, tmp, ut[0], ut[1], v0, v1, tmin, tmax, level + 1, max_level, eps);
        }
    }

    template <class PATCH>
    static bool test_bezier_clip_v(uvt_info* info, const PATCH& patch, real u0, real u1, real v0, real v1, real tmin, real tmax, int level, int max_level, real eps)
    {
        PATCH tpatch(patch);
        rotate_v(tpatch);

        vector3 min;
        vector3 max;
        get_minmax(min, max, tpatch, eps * 1e-3);

        if (0 < min[0] || max[0] < 0) return false;       //x
        if (0 < min[1] || max[1] < 0) return false;       //y
        if (max[2] < tmin || tmax < min[2]) return false; //z

        if (is_eps(min, max, eps) || is_level(level, max_level))
        {
            return test_bezier_clip_l(info, patch, u0, u1, v0, v1, tmin, tmax);
        }

        real tw = 1;
        real tt0 = 0;
        real tt1 = 1;
        if (is_clip(level, tpatch.get_nv()))
        {
            real rng[2];
            if (get_range_v(rng, tpatch))
            {
                tt0 = rng[0];
                tt1 = rng[1];
                tw = tt1 - tt0;
            }
        }

        if (tw >= 0.4)
        {
            PATCH tmp[2];
            patch.split_v(tmp, 0.5);
            real vm = (v0 + v1) * 0.5;
            real vt[] = {v0, vm, vm, v1};

            int order[2] = {0, 1};
            coarse_sort(order, tmp);
            bool bRet = false;
            for (int i = 0; i < 2; i++)
            {
                if (test_bezier_clip_u(info, tmp[order[i]], u0, u1, vt[2 * order[i]], vt[2 * order[i] + 1], tmin, tmax, level + 1, max_level, eps))
                {
                    tmax = info->t;
                    bRet = true;
                }
            }
            return bRet;
        }
        else
        {
            tt0 = std::max<real>(0.0, tt0 - UVEPS);
            tt1 = std::min<real>(tt1 + UVEPS, 1.0);
            real vt[] = {lerp(v0, v1, tt0), lerp(v0, v1, tt1)};
            PATCH tmp;
            crop_v(tmp, patch, tt0, tt1);
            return test_bezier_clip_u(info, tmp, u0, u1, vt[0], vt[1], tmin, tmax, level + 1, max_level, eps);
        }
    }

    template <class PATCH>
    static bool is_bilinear(const PATCH& patch)
    {
        int nu = patch.get_nu();
        int nv = patch.get_nv();
        return (nu == 2) && (nv == 2);
    }

    template <class PATCH>
    static bool test_bezier_patch(const PATCH& patch, real tmin, real tmax, real eps)
    {
        if (!is_bilinear(patch))
            return test_bezier_clip_u(patch, 0, 1, 0, 1, tmin, tmax, 0, MAX_LEVEL, eps);
        else
            return test_bezier_clip_l(patch, 0, 1, 0, 1, tmin, tmax);
    }

    template <class PATCH>
    static bool test_bezier_patch(uvt_info* info, const PATCH& patch, real tmin, real tmax, real eps)
    {
        if (!is_bilinear(patch))
            return test_bezier_clip_u(info, patch, 0, 1, 0, 1, tmin, tmax, 0, MAX_LEVEL, eps);
        else
            return test_bezier_clip_l(info, patch, 0, 1, 0, 1, tmin, tmax);
    }

    //-----------------------------------------------------------------------------

    static real get_eps(const ray& r)
    {
        return EPS;
    }

    bezier_patch_intersection::bezier_patch_intersection(const bezier_patch<vector3>& patch)
        : patch_(patch)
    {
        urange_[0] = 0;
        urange_[1] = 1;
        vrange_[0] = 0;
        vrange_[1] = 1;
        get_minmax(min_, max_, patch, 1e-5);
    }
    bezier_patch_intersection::bezier_patch_intersection(const bezier_patch<vector3>& patch, real u0, real u1, real v0, real v1)
        : patch_(patch)
    {
        urange_[0] = u0;
        urange_[1] = u1;
        vrange_[0] = v0;
        vrange_[1] = v1;
        get_minmax(min_, max_, patch, 1e-5);
    }

    bezier_patch_intersection::~bezier_patch_intersection()
    {
        ;
    }

    bool bezier_patch_intersection::test_internal(const ray& r, real tmin, real tmax, real eps) const
    {
        matrix4 mat;
        getZAlign(mat, r);

        int nu = patch_.get_nu();
        int nv = patch_.get_nv();

        if (nu * nv <= STATIC_ALLOC)
        {
#if USE_SSE
            sse_bezier_patch<STATIC_ALLOC> patch(patch_);
#else
            static_bezier_patch<vector3, STATIC_ALLOC> patch(patch_);
#endif
            patch.transform(mat);
            return test_bezier_patch(patch, tmin, tmax, eps);
        }
        else
        {
            bezier_patch<vector3> patch(patch_);
            patch.transform(mat);
            return test_bezier_patch(patch, tmin, tmax, eps);
        }
    }

    bool bezier_patch_intersection::test_internal(test_info* info, const ray& r, real tmin, real tmax, real eps) const
    {
        matrix4 mat;
        getZAlign(mat, r);

        int nu = patch_.get_nu();
        int nv = patch_.get_nv();

        bool bRet = false;
        uvt_info uvt;
        if (nu * nv <= STATIC_ALLOC)
        {
#if USE_SSE
            sse_bezier_patch<STATIC_ALLOC> patch(patch_);
#else
            static_bezier_patch<vector3, STATIC_ALLOC> patch(patch_);
#endif
            patch.transform(mat);
            bRet = test_bezier_patch(&uvt, patch, tmin, tmax, eps);
        }
        else
        {
            bezier_patch<vector3> patch(patch_);
            patch.transform(mat);
            bRet = test_bezier_patch(&uvt, patch, tmin, tmax, eps);
        }
        if (bRet)
        {
            real t = uvt.t;
            real u = uvt.u;
            real v = uvt.v;

            u = urange_[0] * (1 - u) + urange_[1] * u; //global
            v = vrange_[0] * (1 - v) + vrange_[1] * v; //global
            info->distance = t;
            info->coord = vector3(u, v, 0);
            uvt_info* fsp = reinterpret_cast<uvt_info*>(info->freearea);
            *fsp = uvt;
            //this->finalize(info, r, t);
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    bool bezier_patch_intersection::test(const ray& r, real tmin, real tmax) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            return this->test_internal(r, tmin, tmax, get_eps(r));
        }
        return false;
    }

    bool bezier_patch_intersection::test(test_info* info, const ray& r, real tmin, real tmax) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            return this->test_internal(info, r, tmin, tmax, get_eps(r));
        }
        return false;
    }

    bool bezier_patch_intersection::test(const ray& r, real dist) const
    {
        return this->test(r, 0, dist);
    }

    bool bezier_patch_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return this->test(info, r, 0, dist);
    }

    void bezier_patch_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        info->position = r.origin() + dist * r.direction();

        uvt_info uvt = *reinterpret_cast<uvt_info*>(info->freearea);

        real u = uvt.u;
        real v = uvt.v;

        vector3 U = normalize(patch_.evaluate_deriv_u(u, v));
        vector3 V = normalize(patch_.evaluate_deriv_v(u, v));
        vector3 N = cross(U, V);
        info->normal = N;
        info->geometric = N;
        info->tangent = U;
        info->binormal = V;
    }

    vector3 bezier_patch_intersection::min() const
    {
        return min_;
    }

    vector3 bezier_patch_intersection::max() const
    {
        return max_;
    }
}
