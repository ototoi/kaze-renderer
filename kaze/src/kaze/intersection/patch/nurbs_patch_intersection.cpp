#include "nurbs_patch_intersection.h"
#include "bezier_patch.hpp"
#include "multi_bezier_patch_intersection.h" //
#include "nurbs_to_bspline.h"
#include "bspline_to_bezier.h"

namespace kaze
{

    static void ConvertPatch(multi_bezier_patch<vector3>& mpatch, const nurbs_patch<vector3>& patch)
    {
        bspline_patch<vector3> bpatch;
        nurbs_to_bspline(bpatch, patch);
        bspline_to_bezier(mpatch, bpatch);
    }

    nurbs_patch_intersection::nurbs_patch_intersection(const nurbs_patch<vector3>& patch)
        : inter_(NULL)
    {
        multi_bezier_patch<vector3> mpatch;
        ConvertPatch(mpatch, patch);
        inter_ = new multi_bezier_patch_intersection(mpatch);
    }

    nurbs_patch_intersection::~nurbs_patch_intersection()
    {
        if (inter_) delete inter_;
    }

    bool nurbs_patch_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool nurbs_patch_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void nurbs_patch_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }

    vector3 nurbs_patch_intersection::min() const
    {
        return inter_->min();
    }

    vector3 nurbs_patch_intersection::max() const
    {
        return inter_->max();
    }
}
