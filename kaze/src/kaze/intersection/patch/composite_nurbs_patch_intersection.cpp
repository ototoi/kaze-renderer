#include "composite_nurbs_patch_intersection.h"
#include "bvh_composite_intersection.h"

#include "patch/nurbs_patch_loader.h"
#include "patch/nurbs_patch_saver.h"
#include "patch/convert_nurbs_patch.h"

#include "trimmed_nurbs_patch.hpp"

#include "trimmed_nurbs_patch_intersection.h"

#include <memory>

namespace kaze
{
    namespace
    {

        class t_patch_saver : public nurbs_patch_saver
        {
        public:
            bool set_patches(size_t sz)
            {
                mv_.reserve(sz);
                return true;
            }
            bool set_patch(size_t i, io_nurbs_patch* f)
            {
                trimmed_nurbs_patch<vector3, vector2> patch;
                if (!convert_nurbs_patch(patch, *f)) return false;
                mv_.push_back(patch);
                return true;
            }

        public:
            std::vector<trimmed_nurbs_patch<vector3, vector2> > mv_;
        };

        void Transform(const transformer& tr, trimmed_nurbs_patch<vector3, vector2>& patch)
        {
            int nu = patch.get_nu();
            int nv = patch.get_nv();
            for (int j = 0; j < nv; j++)
            {
                for (int i = 0; i < nu; i++)
                {
                    patch.set_cp_at(i, j, tr.transform_p(patch.get_cp_at(i, j)));
                }
            }
        }
    }

    class composite_nurbs_patch_intersection_imp
    {
    public:
        composite_nurbs_patch_intersection_imp(const nurbs_patch_loader& pl, const transformer& tr);
        ~composite_nurbs_patch_intersection_imp();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        composite_intersection* bvh_;
    };
    //------------------------------------------------------------------------

    composite_nurbs_patch_intersection_imp::composite_nurbs_patch_intersection_imp(const nurbs_patch_loader& loader, const transformer& tr) : bvh_(NULL)
    {
        std::unique_ptr<bvh_composite_intersection> ap(new bvh_composite_intersection());

        std::vector<trimmed_nurbs_patch<vector3, vector2> > tmp;
        {
            t_patch_saver tps;
            if (!loader.load(&tps)) throw "composite_nurbs_patch_intersection";
            tmp.swap(tps.mv_);
        }

        for (size_t i = 0; i < tmp.size(); i++)
        {
            Transform(tr, tmp[i]);
            ap->add(new trimmed_nurbs_patch_intersection(tmp[i], tmp[i].get_outer_loops(), tmp[i].get_inner_loops()));
        }
        ap->construct();
        bvh_ = ap.release();
    }

    composite_nurbs_patch_intersection_imp::~composite_nurbs_patch_intersection_imp()
    {
        if (bvh_) delete bvh_;
    }
    bool composite_nurbs_patch_intersection_imp::test(const ray& r, real dist) const
    {
        return bvh_->test(r, dist);
    }
    bool composite_nurbs_patch_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        return bvh_->test(info, r, dist);
    }
    vector3 composite_nurbs_patch_intersection_imp::min() const
    {
        return bvh_->min();
    }
    vector3 composite_nurbs_patch_intersection_imp::max() const
    {
        return bvh_->max();
    }

    //------------------------------------------------------------------------
    composite_nurbs_patch_intersection::composite_nurbs_patch_intersection(const nurbs_patch_loader& pl)
    {
        no_transformer tr;
        imp_ = new composite_nurbs_patch_intersection_imp(pl, tr);
    }

    composite_nurbs_patch_intersection::composite_nurbs_patch_intersection(const nurbs_patch_loader& pl, const transformer& tr)
    {
        imp_ = new composite_nurbs_patch_intersection_imp(pl, tr);
    }

    composite_nurbs_patch_intersection::~composite_nurbs_patch_intersection()
    {
        delete imp_;
    }

    bool composite_nurbs_patch_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }
    bool composite_nurbs_patch_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }
    void composite_nurbs_patch_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }
    vector3 composite_nurbs_patch_intersection::min() const
    {
        return imp_->min();
    }
    vector3 composite_nurbs_patch_intersection::max() const
    {
        return imp_->max();
    }
}
