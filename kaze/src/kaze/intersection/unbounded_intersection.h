#ifndef KAZE_UNBOUNDED_INTERSECTION_H
#define KAZE_UNBOUNDED_INTERSECTION_H

#include "intersection.h"

namespace kaze
{

    class unbounded_intersection : public intersection
    {
    public:
        virtual ~unbounded_intersection() {}

        //virtual bool test(const ray & r, real dist)const=0;
        //virtual bool test(test_info * info, const ray & r, real dist)const=0;
        //virtual void finalize(test_info * info, const ray & r, real dist)const=0;

        //
        vector3 min() const;
        vector3 max() const;
    };
}

#endif