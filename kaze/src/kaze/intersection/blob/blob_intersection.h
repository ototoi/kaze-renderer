#ifndef KAZE_BLOB_INTERSECTION_H
#define KAZE_BLOB_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class blob_intersection : public bounded_intersection
    {
    public:
        virtual ~blob_intersection() {}
        virtual bool test(const ray& r, real dist) const = 0;
        virtual bool test(test_info* info, const ray& r, real dist) const = 0;
        virtual void finalize(test_info* info, const ray& r, real dist) const = 0;

        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;

    public:
        virtual void add_point(const vector3& c, real radius) = 0;
        virtual void construct() = 0;
    };
}

#endif