#ifndef KAZE_EXHIBITED_BLOB_ACCELERATOR_H
#define KAZE_EXHIBITED_BLOB_ACCELERATOR_H

#include "blob_accelerator.h"
#include <vector>

namespace kaze
{

    class exhibited_blob_accelerator : public blob_accelerator
    {
    public:
        exhibited_blob_accelerator(const std::vector<const blob*>& blobs);
        ~exhibited_blob_accelerator();

        bool test(const blob_tester* tester, const ray& r, real dist) const;
        bool test(const blob_tester* tester, test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    private:
        std::vector<const blob*> blobs_;
        vector3 min_;
        vector3 max_;
    };
}

#endif
