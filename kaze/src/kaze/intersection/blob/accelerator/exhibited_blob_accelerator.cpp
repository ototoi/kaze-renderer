#include "exhibited_blob_accelerator.h"
#include "test_info.h"

namespace kaze
{

    exhibited_blob_accelerator::exhibited_blob_accelerator(const std::vector<const blob*>& blobs)
        : blobs_(blobs)
    {
        static const real EPSILON = values::epsilon();

        size_t bsz = blobs.size();
        std::vector<vector3> mins(bsz);
        std::vector<vector3> maxs(bsz);

        for (size_t i = 0; i < bsz; i++)
        {
            mins[i] = blobs[i]->min();
            maxs[i] = blobs[i]->max();
        }

        real FAR = values::far();
        vector3 min(+FAR, +FAR, +FAR);
        vector3 max(-FAR, -FAR, -FAR);
        for (size_t i = 0; i < bsz; i++)
        {
            vector3 cmin = mins[i];
            vector3 cmax = maxs[i];
            for (int j = 0; j < 3; j++)
            {
                if (min[j] > cmin[j]) min[j] = cmin[j];
                if (max[j] < cmax[j]) max[j] = cmax[j];
            }
        }
        min -= vector3(EPSILON, EPSILON, EPSILON);
        max += vector3(EPSILON, EPSILON, EPSILON);
        min_ = min;
        max_ = max;
    }

    exhibited_blob_accelerator::~exhibited_blob_accelerator()
    {
    }

    bool exhibited_blob_accelerator::test(const blob_tester* tester, const ray& r, real dist) const
    {
        if (tester->test(blobs_, r, dist))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool exhibited_blob_accelerator::test(const blob_tester* tester, test_info* info, const ray& r, real dist) const
    {
        if (tester->test(blobs_, info, r, dist))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    vector3 exhibited_blob_accelerator::min() const
    {
        return min_;
    }

    vector3 exhibited_blob_accelerator::max() const
    {
        return max_;
        ;
    }
}
