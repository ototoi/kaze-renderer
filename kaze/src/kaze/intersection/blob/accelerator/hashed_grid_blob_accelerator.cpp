#include "hashed_grid_blob_accelerator.h"

#include "intersection.h"
#include "intersection_ex.h"

#include "values.h"
#include <vector>

#include <iostream>

#include "mapset_selection.h"
#define MAP_TYPE KAZE_MAP_TYPE

namespace kaze
{
    namespace
    {
        class blob_node
        {
        public:
            void add(const blob* b) { v_.push_back(b); }
            const std::vector<const blob*>& get() const { return v_; }
        private:
            std::vector<const blob*> v_;
        };
    }

    //-------------------------------------------

    class hashed_grid_blob_accelerator_imp
    {
    public:
        typedef MAP_TYPE<size_t, blob_node*> map_type;
        typedef MAP_TYPE<size_t, blob_node*>::iterator iterator;
        typedef MAP_TYPE<size_t, blob_node*>::const_iterator citerator;
        typedef MAP_TYPE<size_t, blob_node*>::value_type value_type;

    public:
        hashed_grid_blob_accelerator_imp(const std::vector<const blob*>& blobs);
        ~hashed_grid_blob_accelerator_imp();
        const std::vector<const blob*>& get_blobs(int x, int y, int z) const;

        vector3 min() const;
        vector3 max() const;

        bool test(const blob_tester* tester, const ray& r, real dist) const;
        bool test(const blob_tester* tester, test_info* info, const ray& r, real dist) const;

    protected:
        bool test_xyz(int x, int y, int z, const blob_tester* tester, const ray& r, real dist) const;
        bool test_xyz(int x, int y, int z, const blob_tester* tester, test_info* info, const ray& r, real dist) const;

    private:
        map_type v_;
        int ndiv_[3];
        real delta_[3];
        real idelta_[3];
        vector3 min_;
        vector3 max_;
    };

    hashed_grid_blob_accelerator_imp::hashed_grid_blob_accelerator_imp(const std::vector<const blob*>& blobs)
    {
        static const real EPSILON = values::epsilon() * 1000;

        size_t bsz = blobs.size();
        double r = 0;
        for (size_t i = 0; i < bsz; i++)
        {
            r += blobs[i]->radius();
        }
        r /= bsz;

        std::vector<vector3> mins(bsz);
        std::vector<vector3> maxs(bsz);

        for (size_t i = 0; i < bsz; i++)
        {
            vector3 cmin;
            vector3 cmax;
            //blobs[i]->bound(cmin, cmax, &blobs[0],bsz);

            cmin = blobs[i]->min();
            cmax = blobs[i]->max();

            cmin -= vector3(EPSILON, EPSILON, EPSILON);
            cmax += vector3(EPSILON, EPSILON, EPSILON);

            mins[i] = cmin;
            maxs[i] = cmax;
        }

        real FAR = values::far();
        vector3 min(+FAR, +FAR, +FAR);
        vector3 max(-FAR, -FAR, -FAR);
        for (size_t i = 0; i < bsz; i++)
        {
            vector3 cmin = mins[i];
            vector3 cmax = maxs[i];
            for (int j = 0; j < 3; j++)
            {
                if (min[j] > cmin[j]) min[j] = cmin[j];
                if (max[j] < cmax[j]) max[j] = cmax[j];
            }
        }
        min -= vector3(EPSILON, EPSILON, EPSILON);
        max += vector3(EPSILON, EPSILON, EPSILON);

        double cell = 4 * r;
        vector3 wid = max - min;
        vector3 center = 0.5 * (max + min);
        int ndiv[3];
        real delta[3];
        real idelta[3];
        for (int j = 0; j < 3; j++)
        {
            ndiv[j] = (int)(ceil(wid[j] / cell));
        }

        for (int j = 0; j < 3; j++)
        {
            min[j] = center[j] - 0.5 * ndiv[j] * cell - EPSILON;
            max[j] = center[j] + 0.5 * ndiv[j] * cell + EPSILON;
        }
        max += vector3(EPSILON, EPSILON, EPSILON);

        wid = max - min;

        for (int j = 0; j < 3; j++)
        {
            delta[j] = wid[j] / ndiv[j];
            idelta[j] = ndiv[j] / wid[j];
        }

        for (size_t i = 0; i < bsz; i++)
        {
            int imin[3];
            int imax[3];
            vector3 cmin = mins[i];
            vector3 cmax = maxs[i];
            for (int j = 0; j < 3; j++)
            {
                imin[j] = (int)floor(ndiv[j] * (cmin[j] - min[j]) / wid[j]);
                imax[j] = (int)floor(ndiv[j] * (cmax[j] - min[j]) / wid[j]);

                assert(0 <= imin[j] && imin[j] < ndiv[j]);
                assert(0 <= imax[j] && imax[j] < ndiv[j]);
            }

            //-----------------------------------------
            for (int z = imin[2]; z <= imax[2]; z++)
            {
                for (int y = imin[1]; y <= imax[1]; y++)
                {
                    for (int x = imin[0]; x <= imax[0]; x++)
                    {
                        size_t index = (size_t)((z * ndiv[1] + y) * ndiv[0] + x);
                        iterator it = v_.find(index);
                        if (it == v_.end())
                        { //no find
                            std::pair<iterator, bool> r = v_.insert(value_type(index, new blob_node()));
                            it = r.first;
                        }
                        it->second->add(blobs[i]);
                    }
                }
            }

            //-----------------------------------------
        }
        memcpy(ndiv_, ndiv, sizeof(int) * 3);
        memcpy(delta_, delta, sizeof(real) * 3);
        memcpy(idelta_, idelta, sizeof(real) * 3);
        min_ = min;
        max_ = max;
    }

    hashed_grid_blob_accelerator_imp::~hashed_grid_blob_accelerator_imp()
    {
        for (iterator i = v_.begin(); i != v_.end(); i++)
        {
            delete i->second;
        }
    }

    const std::vector<const blob*>& hashed_grid_blob_accelerator_imp::get_blobs(int x, int y, int z) const
    {
        size_t index = (size_t)((z * ndiv_[1] + y) * ndiv_[0] + x);
        citerator it = v_.find(index);
        if (it != v_.end())
        {
            return it->second->get();
        }
        else
        {
            static std::vector<const blob*> nil_blobs;
            return nil_blobs;
        }
    }

    bool hashed_grid_blob_accelerator_imp::test_xyz(int x, int y, int z, const blob_tester* tester, const ray& r, real dist) const
    {
        const std::vector<const blob*>& blobs = this->get_blobs(x, y, z);
        if (!blobs.empty() && tester->test(blobs, r, dist))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool hashed_grid_blob_accelerator_imp::test_xyz(int x, int y, int z, const blob_tester* tester, test_info* info, const ray& r, real dist) const
    {
        const std::vector<const blob*>& blobs = this->get_blobs(x, y, z);
        if (!blobs.empty() && tester->test(blobs, info, r, dist))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool hashed_grid_blob_accelerator_imp::test(const blob_tester* tester, const ray& r, real dist) const
    {
        //get first intersection
        vector3 pos;
        if (!test_AABB(&pos, min_, max_, r, dist)) return false;

        const vector3& org = r.origin();
        const vector3& ird = r.inversed_direction();

        const int* const ndiv = this->ndiv_;
        const real* const delta = this->delta_;
        const real* const idelta = this->idelta_;

        int plane[3];
        int step[3];
        real td[3];
        real tn[3];
        int out[3];

        for (int idx = 0; idx < 3; idx++)
        {
            plane[idx] = static_cast<int>((pos[idx] - min_[idx]) * idelta[idx]);
            if (plane[idx] >= ndiv[idx]) plane[idx]--;
            assert(plane[idx] != ndiv[idx]);

            if (ird[idx] > 0)
            { //plus
                step[idx] = 1;
                td[idx] = delta[idx] * ird[idx];
                tn[idx] = ((min_[idx] + (plane[idx] + 1) * delta[idx]) - org[idx]) * ird[idx];
                out[idx] = ndiv[idx];
            }
            else if (ird[idx] < 0)
            { //minus
                step[idx] = -1;
                td[idx] = -delta[idx] * ird[idx];
                tn[idx] = ((min_[idx] + (plane[idx]) * delta[idx]) - org[idx]) * ird[idx];
                out[idx] = -1;
            }
            else
            {
                step[idx] = 0;
                td[idx] = 0;
                tn[idx] = values::far();
                out[idx] = -1;
            }
        }

        //debug
        assert(plane[0] >= 0);
        assert(plane[1] >= 0);
        assert(plane[2] >= 0);

        assert(plane[0] < ndiv_[0]);
        assert(plane[1] < ndiv_[1]);
        assert(plane[2] < ndiv_[2]);
        //debug

        real tmin = 0;
        while (true)
        {
            if (test_xyz(plane[0], plane[1], plane[2], tester, r, dist))
            {
                return true;
            }

            int min_plane = 0;
            if (tn[min_plane] > tn[1]) min_plane = 1;
            if (tn[min_plane] > tn[2]) min_plane = 2;

            if (dist < tn[min_plane]) break;

            /* 3D DDA */
            if (dist < tn[min_plane]) break;
            plane[min_plane] += step[min_plane];
            if (plane[min_plane] == out[min_plane]) break;

            tn[min_plane] += td[min_plane];
        }

        return false;
    }

    bool hashed_grid_blob_accelerator_imp::test(const blob_tester* tester, test_info* info, const ray& r, real dist) const
    {
        //get first intersection
        vector3 pos;
        if (!test_AABB(&pos, min_, max_, r, dist)) return false;

        const vector3& org = r.origin();
        const vector3& ird = r.inversed_direction();

        const int* const ndiv = this->ndiv_;
        const real* const delta = this->delta_;
        const real* const idelta = this->idelta_;

        int plane[3];
        int step[3];
        real td[3];
        real tn[3];
        int out[3];

        for (int idx = 0; idx < 3; idx++)
        {
            plane[idx] = static_cast<int>((pos[idx] - min_[idx]) * idelta[idx]);
            if (plane[idx] >= ndiv[idx]) plane[idx]--;
            assert(plane[idx] != ndiv[idx]);

            if (ird[idx] > 0)
            { //plus
                step[idx] = 1;
                td[idx] = delta[idx] * ird[idx];
                tn[idx] = ((min_[idx] + (plane[idx] + 1) * delta[idx]) - org[idx]) * ird[idx];
                out[idx] = ndiv[idx];
            }
            else if (ird[idx] < 0)
            { //minus
                step[idx] = -1;
                td[idx] = -delta[idx] * ird[idx];
                tn[idx] = ((min_[idx] + (plane[idx]) * delta[idx]) - org[idx]) * ird[idx];
                out[idx] = -1;
            }
            else
            {
                step[idx] = 0;
                td[idx] = 0;
                tn[idx] = values::far();
                out[idx] = -1;
            }
        }

        //debug
        assert(plane[0] >= 0);
        assert(plane[1] >= 0);
        assert(plane[2] >= 0);

        assert(plane[0] < ndiv_[0]);
        assert(plane[1] < ndiv_[1]);
        assert(plane[2] < ndiv_[2]);
        //debug
        bool bRet = false;
        real tmin = 0;
        while (true)
        {
            if (test_xyz(plane[0], plane[1], plane[2], tester, info, r, dist))
            {
                bRet = true;
                dist = info->get_distance();
            }

            int min_plane = 0;
            if (tn[min_plane] > tn[1]) min_plane = 1;
            if (tn[min_plane] > tn[2]) min_plane = 2;

            if (dist < tn[min_plane]) break;

            /* 3D DDA */
            plane[min_plane] += step[min_plane];
            if (plane[min_plane] == out[min_plane]) break;

            tn[min_plane] += td[min_plane];
        }

        return bRet;
    }

    vector3 hashed_grid_blob_accelerator_imp::min() const
    {
        return min_;
    }

    vector3 hashed_grid_blob_accelerator_imp::max() const
    {
        return max_;
    }

    //----------------------------------------------------------------------------

    hashed_grid_blob_accelerator::hashed_grid_blob_accelerator(const std::vector<const blob*>& blobs)
    {
        imp_ = new hashed_grid_blob_accelerator_imp(blobs);
    }

    hashed_grid_blob_accelerator::~hashed_grid_blob_accelerator()
    {
        delete imp_;
    }

    bool hashed_grid_blob_accelerator::test(const blob_tester* tester, const ray& r, real dist) const
    {
        return imp_->test(tester, r, dist);
    }

    bool hashed_grid_blob_accelerator::test(const blob_tester* tester, test_info* info, const ray& r, real dist) const
    {
        return imp_->test(tester, info, r, dist);
    }

    vector3 hashed_grid_blob_accelerator::min() const
    {
        return imp_->min();
    }

    vector3 hashed_grid_blob_accelerator::max() const
    {
        return imp_->max();
    }
}
