#ifndef KAZE_BLOB_ACCELERATOR_H
#define KAZE_BLOB_ACCELERATOR_H

#include "ray.h"

#include "blob.h"
#include "blob_tester.h"

namespace kaze
{

    class blob_accelerator
    {
    public:
        virtual ~blob_accelerator() {}
    public:
        virtual bool test(const blob_tester* tester, const ray& r, real dist) const = 0;
        virtual bool test(const blob_tester* tester, test_info* info, const ray& r, real dist) const = 0;

        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;
    };
}

#endif