#ifndef KAZE_HASHED_GRID_BLOB_ACCELERATOR_H
#define KAZE_HASHED_GRID_BLOB_ACCELERATOR_H

#include "blob_accelerator.h"

namespace kaze
{

    class hashed_grid_blob_accelerator_imp;
    class hashed_grid_blob_accelerator : public blob_accelerator
    {
    public:
        hashed_grid_blob_accelerator(const std::vector<const blob*>& blobs);
        ~hashed_grid_blob_accelerator();

        bool test(const blob_tester* tester, const ray& r, real dist) const;
        bool test(const blob_tester* tester, test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    private:
        hashed_grid_blob_accelerator_imp* imp_;
    };
}

#endif