#include "fast_blob_intersection.h"

#include "test_info.h"

#include "blob.h"
#include "blob_accelerator.h"
#include "blob_tester.h"

#include "hashed_grid_blob_accelerator.h"
#include "exhibited_blob_accelerator.h"

#include "mc_blob_tester.h"
#include "ap_blob_tester.h"
#include "nr_blob_tester.h"
#include "bc_blob_tester.h"
#include "st_blob_tester.h"
#include "f2_blob_tester.h"
#include "f2x_blob_tester.h"



#include "parameter_map.h"

#include <memory>
#include <vector>

namespace kaze
{
    namespace
    {

        class dummy_blob_accelerator : public blob_accelerator
        {
        public:
            bool test(const blob_tester* tester, const ray& r, real dist) const { return false; }
            bool test(const blob_tester* tester, test_info* info, const ray& r, real dist) const { return false; }

            vector3 min() const { return vector3(0, 0, 0); }
            vector3 max() const { return vector3(0, 0, 0); }
        };

        class dummy_blob_tester : public blob_tester
        {
        public:
            bool test(const std::vector<const blob*>& blobs, const ray& r, real dist) const { return false; }
            bool test(const std::vector<const blob*>& blobs, test_info* info, const ray& r, real dist) const { return false; }
            void finalize(test_info* info, const ray& r, real dist) const {}
        };
    }
}

namespace kaze
{

    class fast_blob_intersection_imp
    {
    public:
        fast_blob_intersection_imp();
        ~fast_blob_intersection_imp();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    public:
        void add_point(const vector3& c, real radius);
        void construct();
        void construct(const parameter_map& param);

        void construct(int nAcc, int nTester, real T);

    private:
        std::vector<blob*> mv_;
        std::shared_ptr<blob_accelerator> acc_;
        std::shared_ptr<blob_tester> tester_;
    };

    fast_blob_intersection_imp::fast_blob_intersection_imp()
        : acc_(0), tester_(0)
    {
        acc_.reset(new dummy_blob_accelerator());
        tester_.reset(new dummy_blob_tester());
    }

    fast_blob_intersection_imp::~fast_blob_intersection_imp()
    {
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            delete mv_[i];
        }
    }

    bool fast_blob_intersection_imp::test(const ray& r, real dist) const
    {
        return acc_->test(tester_.get(), r, dist);
    }

    bool fast_blob_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        return acc_->test(tester_.get(), info, r, dist);
    }

    void fast_blob_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        tester_->finalize(info, r, dist);
    }

    vector3 fast_blob_intersection_imp::min() const
    {
        return acc_->min();
    }

    vector3 fast_blob_intersection_imp::max() const
    {
        return acc_->max();
    }

    void fast_blob_intersection_imp::add_point(const vector3& c, real radius)
    {
        mv_.push_back(new point_blob(c, radius));
    }

    void fast_blob_intersection_imp::construct(int nAcc, int nTester, real T)
    {
        size_t bsz = mv_.size();
        if (bsz == 0) return;
        std::vector<const blob*> tblobs(bsz);
        for (size_t i = 0; i < bsz; i++)
        {
            tblobs[i] = mv_[i];
        }

        std::unique_ptr<blob_accelerator> ap_acc;
        std::unique_ptr<blob_tester> ap_tst;

        switch (nAcc)
        {
        case 0:
            ap_acc.reset(new exhibited_blob_accelerator(tblobs));
            break;
        default:
            ap_acc.reset(new hashed_grid_blob_accelerator(tblobs));
            break;
        }

        switch (nTester)
        {
        case 0:
        {
            vector3 cmin = ap_acc->min();
            vector3 cmax = ap_acc->max();

            real min_rad = values::far();
            for (size_t i = 0; i < bsz; i++)
            {
                real rad = tblobs[i]->radius();
                if (min_rad > rad) min_rad = rad;
            }
            real delta = min_rad / 16;

            ap_tst.reset(new mc_blob_tester(cmin, cmax, delta, T));
        }
        break;
        case 1:
            ap_tst.reset(new ap_blob_tester(T));
            break;
        case 2:
            ap_tst.reset(new nr_blob_tester(T));
            break;
        case 4:
            ap_tst.reset(new st_blob_tester(T));
            break;
        case 5:
            ap_tst.reset(new f2_blob_tester(T));
            break;
        case 6:
            ap_tst.reset(new f2x_blob_tester(T));
            break;
        default: //3
            ap_tst.reset(new bc_blob_tester(T));
            break;
        }

        acc_.reset(ap_acc.release());
        tester_.reset(ap_tst.release());
    }

    void fast_blob_intersection_imp::construct()
    {
        construct(0, 2, 0.5);
    }

    void fast_blob_intersection_imp::construct(const parameter_map& param)
    {
        int nTester = 3;
        std::string str_tester = param.get_string("BLOB_TESTER");
        if (str_tester == "3" || str_tester == "f6" || str_tester == "bezier clipping")
        {
            nTester = 3;
        }
        else if (str_tester == "2" || str_tester == "f4")
        {
            nTester = 2;
        }
        else if (str_tester == "1" || str_tester == "ap" || str_tester == "approximate")
        {
            nTester = 1;
        }
        else if (str_tester == "0" || str_tester == "mc" || str_tester == "marching cubes")
        {
            nTester = 0;
        }
        else if (str_tester == "4" || str_tester == "st" || str_tester == "sphere tracing")
        {
            nTester = 4;
        }
        else if (str_tester == "5" || str_tester == "f2")
        {
            nTester = 5;
        }
        else if (str_tester == "6" || str_tester == "f2x")
        {
            nTester = 6;
        }
        real T = param.get_double("THRESHOLD", 0.5);

        int nAcc = 0;
        std::string str_acc = param.get_string("BLOB_ACCELERATOR");
        if (str_acc == "hashed_grid")
        {
            nAcc = 1;
        }
        else if (str_acc == "exhibited")
        {
            nAcc = 0;
        }

        construct(nAcc, nTester, T);
    }

    //----------------------------------------------

    //----------------------------------------------

    fast_blob_intersection::fast_blob_intersection()
    {
        imp_ = new fast_blob_intersection_imp();
    }

    fast_blob_intersection::~fast_blob_intersection()
    {
        delete imp_;
    }

    bool fast_blob_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool fast_blob_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void fast_blob_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        imp_->finalize(info, r, dist);
    }

    vector3 fast_blob_intersection::min() const
    {
        return imp_->min();
    }

    vector3 fast_blob_intersection::max() const
    {
        return imp_->max();
    }

    void fast_blob_intersection::add_point(const vector3& c, real radius)
    {
        return imp_->add_point(c, radius);
    }

    void fast_blob_intersection::construct()
    {
        return imp_->construct();
    }

    void fast_blob_intersection::construct(const parameter_map& param)
    {
        return imp_->construct(param);
    }
}
