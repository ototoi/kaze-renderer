#ifndef KAZE_BC_BLOB_TESTER_H
#define KAZE_BC_BLOB_TESTER_H

#include "blob_tester.h"

namespace kaze
{

    /*
   *
   * Bezier Clipping Blob Tester
   *
   */

    class bc_blob_tester : public blob_tester
    {
    public:
        bc_blob_tester(real T = real(0.5));
        bool test(const std::vector<const blob*>& blobs, const ray& r, real dist) const;
        bool test(const std::vector<const blob*>& blobs, test_info* info, const ray& r, real dist) const;

    public:
        void finalize(test_info* info, const ray& r, real dist) const;

    private:
        real T_;
    };
}

#endif