#ifndef KAZE_MC_BLOB_TESTER_H
#define KAZE_MC_BLOB_TESTER_H

#include "blob_tester.h"

namespace kaze
{

    class mc_blob_tester : public blob_tester
    {
    public:
        mc_blob_tester(const vector3& min, const vector3& max, real delta, real T = 0.5);
        bool test(const std::vector<const blob*>& blobs, const ray& r, real dist) const;
        bool test(const std::vector<const blob*>& blobs, test_info* info, const ray& r, real dist) const;

    public:
        void finalize(test_info* info, const ray& r, real dist) const;

    private:
        vector3 min_;
        vector3 max_;
        real delta_;
        real T_;
    };

    int test_marching_cubes();
}

#endif