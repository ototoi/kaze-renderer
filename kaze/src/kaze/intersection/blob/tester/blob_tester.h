#ifndef KAZE_BLOB_TESTER_H
#define KAZE_BLOB_TESTER_H

#include "ray.h"
#include "test_info.h"
#include "blob.h"
#include <vector>

namespace kaze
{

    class blob_tester
    {
    public:
        virtual ~blob_tester() {}
    public:
        virtual bool test(const std::vector<const blob*>& blobs, const ray& r, real dist) const = 0;
        virtual bool test(const std::vector<const blob*>& blobs, test_info* info, const ray& r, real dist) const = 0;
        virtual void finalize(test_info* info, const ray& r, real dist) const = 0;
    };
}

#endif