#include "ap_blob_tester.h"

#include "values.h"
#include "test_info.h"

#include <vector>
#include <cmath>
#include <algorithm>

namespace kaze
{
    namespace
    {

        typedef const blob* CPBLOB;

        struct blob_info
        {
            real t;
        };

        struct blob_freearea_struct
        {
            int n;
            CPBLOB pointers[8];
        };

        struct poly2
        {
            real t;
            real w;
        };

        struct poly_sorter : public std::binary_function<poly2, poly2, bool>
        {
            bool operator()(const poly2& a, const poly2& b) const
            {
                return a.t < b.t;
            }
        };

        static double pow2(double x) { return x * x; }

        static double weight(double x)
        {
            return (2 * x - 3) * x * x + 1;
        }
        /*
	static double weight(double x)
	{
		double x2 = x*x;
		double x4 = x2*x2;
		double x6 = x2*x4;
		return -4.0/9.0*x6 + 17.0/9.0*x4 -22.0/9.0*x2 + 1;
	}
	*/

        real calc_weight(const vector3& P, const blob* blob)
        {
            real R2 = pow2(blob->radius());
            real L2 = blob->distance2(P);

            if (L2 > R2) return 0;

            real mx = weight(sqrt(L2 / R2));

            return mx;
        }

        void make_polynomial(
            std::vector<poly2>& interval,
            const std::vector<const blob*>& blobs,
            const ray& r,
            real t0, real t1)
        {
            static const real EPSILON = values::epsilon() * 1000;

            vector3 org = r.origin();
            vector3 dir = r.direction();

            std::vector<real> distances;

            size_t bsz = blobs.size();
            for (size_t i = 0; i < bsz; i++)
            {
                real R = blobs[i]->radius();
                vector3 vO = org - blobs[i]->center();
                vector3 vD = dir;

                real B = dot(vO, vD);
                real Cx = dot(vO, vO);

                real tm = -B;
                distances.push_back(tm);

                real iR = 1.0 / R;
                for (int d = 0; d < 5; d++)
                {
                    real dR = R - d * iR;
                    real C = Cx - dR * dR;

                    real D = B * B - C;
                    if (D <= 0) break;
                    real sqrD = std::sqrt(D);

                    static const int N = 3;
                    real dD = sqrD / N;
                    for (int k = 1; k <= N; k++)
                    {
                        real dd = k * dD;
                        distances.push_back(tm - dd);
                        distances.push_back(tm + dd);
                    }
                }
            }
            std::sort(distances.begin(), distances.end());
            distances.erase(std::unique(distances.begin(), distances.end()), distances.end());

            size_t dsz = distances.size();
            if (dsz >= 2)
            {
                for (size_t i = 0; i < dsz; i++)
                {
                    real t = distances[i];
                    vector3 P = r.origin() + t * r.direction();

                    real w = 0;
                    for (size_t j = 0; j < bsz; j++)
                    {
                        w += calc_weight(P, blobs[j]);
                    }
                    poly2 p = {t, w};
                    interval.push_back(p);

                    if (t1 < t) break;
                }
            }
        }

        bool test_polynomial(
            blob_info* info,
            const std::vector<poly2>& interval,
            const ray& r,
            real t0, real t1,
            real T = 0.5)
        {
            static const real EPSILON = values::epsilon() * 1000;
            // static const real T = 0.5;

            if (interval.size() < 2) return false;

            size_t sz = interval.size();
            for (size_t i = 0; i < sz - 1; i++)
            {
                real ts = interval[i].t;
                real te = interval[i + 1].t;

                if (te < t0) continue;
                if (t1 < ts) break;

                assert(ts <= te);

                real vs = -T + interval[i].w;
                real ve = -T + interval[i + 1].w;

                if ((vs < 0 && ve < 0) || (vs > 0 && ve > 0)) continue;
                if (vs == ve) continue;
                //real a = (te-ts)/(ve-vs)
                //real b = vs - a*ts;
                //real t = -b/a;
                //t = -(vs - (ve-vs)/(te-ts)*ts)*(te-ts)/(ve-vs);
                real t = ts - (vs) * (te - ts) / (ve - vs);

                if (t0 <= t && t <= t1 && ts <= t && t <= te)
                {
                    info->t = t;
                    return true;
                }
            }

            return false;
        }

        vector3 get_normal(const vector3& p, const CPBLOB* blobs, size_t sz)
        {
            static const real EPSILON = values::epsilon() * 1000;

            vector3 gradient = vector3(0.0f, 0.0f, 0.0f);

            bool bSet = false;
            for (size_t i = 0; i < sz; i++)
            {
                const blob* bp = blobs[i];

                real R = bp->radius();
                real R2 = R * R;

                vector3 normal = p - bp->get_near(p);
                real r2 = dot(normal, normal);
                if (R2 < r2) continue;
                real r = sqrt(r2);
                if (r <= EPSILON) continue;
                normal = normal * weight(r / R);

                bSet = true;
                gradient += normal;
            }

            if (bSet)
            {
                return normalize(gradient);
            }
            else
            {
                return vector3(1, 0, 0);
            }
        }

        bool test_blobs(
            blob_info* info,
            const std::vector<const blob*>& blobs,
            const ray& r,
            real t0, real t1,
            real T)
        {
            bool bRet = false;
            std::vector<poly2> interval;
            make_polynomial(interval, blobs, r, t0, t1);
            bRet = test_polynomial(info, interval, r, t0, t1, T);
            return bRet;
        }
    }

    ap_blob_tester::ap_blob_tester(real T) : T_(T) {}

    bool ap_blob_tester::test(const std::vector<const blob*>& blobs, const ray& r, real dist) const
    {
        real T = T_;
        blob_info binfo;
        if (test_blobs(&binfo, blobs, r, 0, dist, T))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool ap_blob_tester::test(const std::vector<const blob*>& blobs, test_info* info, const ray& r, real dist) const
    {
        real T = T_;
        blob_info binfo;
        if (test_blobs(&binfo, blobs, r, 0, dist, T))
        {
            info->distance = binfo.t;

            blob_freearea_struct* fp = reinterpret_cast<blob_freearea_struct*>(info->freearea);
            int bsz = (int)blobs.size();
            if (bsz <= 8)
            {
                fp->n = bsz;
                memcpy(fp->pointers, &(blobs[0]), blobs.size() * sizeof(const blob*));
            }
            else
            {
                fp->n = 0;
                vector3 p = binfo.t * r.direction() + r.origin();
                vector3 n = get_normal(p, &(blobs[0]), bsz);
                info->position = p;
                info->geometric = n;
                info->normal = n;
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    void ap_blob_tester::finalize(test_info* info, const ray& r, real dist) const
    {
        blob_freearea_struct* fp = reinterpret_cast<blob_freearea_struct*>(info->freearea);
        int bsz = fp->n;
        if (bsz)
        {
            vector3 p = info->distance * r.direction() + r.origin();
            vector3 n = get_normal(p, fp->pointers, bsz);
            info->position = p;
            info->geometric = n;
            info->normal = n;
        }
    }
}
