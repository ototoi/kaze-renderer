/*
 * Polygonising a scalar field(http://local.wasp.uwa.edu.au/~pbourke/geometry/polygonise/)
 */

#include "mc_blob_tester.h"
#include "values.h"
#include "intersection.h"
#include "logger.h"

namespace kaze
{
    namespace
    {

        /*
     *  edge index table
     */
        static const int edgeTable[256] =
            {
                0x0, 0x109, 0x203, 0x30a, 0x406, 0x50f, 0x605, 0x70c,
                0x80c, 0x905, 0xa0f, 0xb06, 0xc0a, 0xd03, 0xe09, 0xf00,
                0x190, 0x99, 0x393, 0x29a, 0x596, 0x49f, 0x795, 0x69c,
                0x99c, 0x895, 0xb9f, 0xa96, 0xd9a, 0xc93, 0xf99, 0xe90,
                0x230, 0x339, 0x33, 0x13a, 0x636, 0x73f, 0x435, 0x53c,
                0xa3c, 0xb35, 0x83f, 0x936, 0xe3a, 0xf33, 0xc39, 0xd30,
                0x3a0, 0x2a9, 0x1a3, 0xaa, 0x7a6, 0x6af, 0x5a5, 0x4ac,
                0xbac, 0xaa5, 0x9af, 0x8a6, 0xfaa, 0xea3, 0xda9, 0xca0,
                0x460, 0x569, 0x663, 0x76a, 0x66, 0x16f, 0x265, 0x36c,
                0xc6c, 0xd65, 0xe6f, 0xf66, 0x86a, 0x963, 0xa69, 0xb60,
                0x5f0, 0x4f9, 0x7f3, 0x6fa, 0x1f6, 0xff, 0x3f5, 0x2fc,
                0xdfc, 0xcf5, 0xfff, 0xef6, 0x9fa, 0x8f3, 0xbf9, 0xaf0,
                0x650, 0x759, 0x453, 0x55a, 0x256, 0x35f, 0x55, 0x15c,
                0xe5c, 0xf55, 0xc5f, 0xd56, 0xa5a, 0xb53, 0x859, 0x950,
                0x7c0, 0x6c9, 0x5c3, 0x4ca, 0x3c6, 0x2cf, 0x1c5, 0xcc,
                0xfcc, 0xec5, 0xdcf, 0xcc6, 0xbca, 0xac3, 0x9c9, 0x8c0,
                0x8c0, 0x9c9, 0xac3, 0xbca, 0xcc6, 0xdcf, 0xec5, 0xfcc,
                0xcc, 0x1c5, 0x2cf, 0x3c6, 0x4ca, 0x5c3, 0x6c9, 0x7c0,
                0x950, 0x859, 0xb53, 0xa5a, 0xd56, 0xc5f, 0xf55, 0xe5c,
                0x15c, 0x55, 0x35f, 0x256, 0x55a, 0x453, 0x759, 0x650,
                0xaf0, 0xbf9, 0x8f3, 0x9fa, 0xef6, 0xfff, 0xcf5, 0xdfc,
                0x2fc, 0x3f5, 0xff, 0x1f6, 0x6fa, 0x7f3, 0x4f9, 0x5f0,
                0xb60, 0xa69, 0x963, 0x86a, 0xf66, 0xe6f, 0xd65, 0xc6c,
                0x36c, 0x265, 0x16f, 0x66, 0x76a, 0x663, 0x569, 0x460,
                0xca0, 0xda9, 0xea3, 0xfaa, 0x8a6, 0x9af, 0xaa5, 0xbac,
                0x4ac, 0x5a5, 0x6af, 0x7a6, 0xaa, 0x1a3, 0x2a9, 0x3a0,
                0xd30, 0xc39, 0xf33, 0xe3a, 0x936, 0x83f, 0xb35, 0xa3c,
                0x53c, 0x435, 0x73f, 0x636, 0x13a, 0x33, 0x339, 0x230,
                0xe90, 0xf99, 0xc93, 0xd9a, 0xa96, 0xb9f, 0x895, 0x99c,
                0x69c, 0x795, 0x49f, 0x596, 0x29a, 0x393, 0x99, 0x190,
                0xf00, 0xe09, 0xd03, 0xc0a, 0xb06, 0xa0f, 0x905, 0x80c,
                0x70c, 0x605, 0x50f, 0x406, 0x30a, 0x203, 0x109, 0x0};

        /*
     *  triangle index table
     */
        static const int triTable[256][16] =
            {
                {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1},
                {3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1, -1},
                {3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1, -1},
                {3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1, -1},
                {9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1, -1},
                {1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1},
                {9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
                {2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1, -1},
                {8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1, -1},
                {9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
                {4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1, -1},
                {3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1, -1},
                {1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1, -1},
                {4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1, -1},
                {4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1, -1},
                {9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1, -1},
                {1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
                {5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1, -1},
                {2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1, -1},
                {9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
                {0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
                {2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1, -1},
                {10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1},
                {4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1, -1},
                {5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1, -1},
                {5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1},
                {9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1, -1},
                {0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1, -1},
                {1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1, -1},
                {10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1, -1},
                {8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1, -1},
                {2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1},
                {7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1},
                {9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1, -1},
                {2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1, -1},
                {11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1, -1},
                {9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1, -1},
                {5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0, -1},
                {11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0, -1},
                {11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
                {1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1, -1},
                {9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1, -1},
                {5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1, -1},
                {2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
                {0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
                {5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1, -1},
                {6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1, -1},
                {0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1, -1},
                {3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1, -1},
                {6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1, -1},
                {5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1, -1},
                {1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
                {10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1, -1},
                {6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1},
                {1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1, -1},
                {8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1, -1},
                {7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9, -1},
                {3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
                {5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1, -1},
                {0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1},
                {9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6, -1},
                {8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1, -1},
                {5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11, -1},
                {0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7, -1},
                {6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1, -1},
                {10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1},
                {10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1, -1},
                {8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1, -1},
                {1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1, -1},
                {3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1, -1},
                {0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1},
                {10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1, -1},
                {0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1, -1},
                {3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1, -1},
                {6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1, -1},
                {9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1, -1},
                {8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1, -1},
                {3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1, -1},
                {6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1, -1},
                {0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1, -1},
                {10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1, -1},
                {10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1, -1},
                {1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1, -1},
                {2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9, -1},
                {7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1, -1},
                {7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1, -1},
                {2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7, -1},
                {1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11, -1},
                {11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1, -1},
                {8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6, -1},
                {0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1, -1},
                {7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
                {10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
                {2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
                {6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1, -1},
                {7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1, -1},
                {2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1, -1},
                {1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1, -1},
                {10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1, -1},
                {10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1, -1},
                {0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1, -1},
                {7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1, -1},
                {6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1, -1},
                {8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1, -1},
                {9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1, -1},
                {6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1, -1},
                {1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1, -1},
                {4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1, -1},
                {10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3, -1},
                {8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1},
                {0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1, -1},
                {1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1, -1},
                {8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1, -1},
                {10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1, -1},
                {4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3, -1},
                {10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1},
                {5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
                {11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1, -1},
                {9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
                {6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1, -1},
                {7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1, -1},
                {3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6, -1},
                {7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1, -1},
                {9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1, -1},
                {3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1, -1},
                {6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8, -1},
                {9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1, -1},
                {1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4, -1},
                {4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10, -1},
                {7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1, -1},
                {6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1, -1},
                {3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1, -1},
                {0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1, -1},
                {6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1},
                {1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1, -1},
                {0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10, -1},
                {11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5, -1},
                {6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1, -1},
                {5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1, -1},
                {9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1, -1},
                {1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8, -1},
                {1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6, -1},
                {10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1, -1},
                {0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1, -1},
                {5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1, -1},
                {10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1, -1},
                {11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1, -1},
                {0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1, -1},
                {9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1, -1},
                {7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2, -1},
                {2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1},
                {8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1, -1},
                {9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1, -1},
                {9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2, -1},
                {1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1, -1},
                {9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1, -1},
                {9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1},
                {5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1, -1},
                {0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1, -1},
                {10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4, -1},
                {2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1, -1},
                {0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11, -1},
                {0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5, -1},
                {9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1, -1},
                {5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1, -1},
                {3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9, -1},
                {5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1, -1},
                {8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1, -1},
                {0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1, -1},
                {9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1},
                {0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1, -1},
                {1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1, -1},
                {3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4, -1},
                {4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1, -1},
                {9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3, -1},
                {11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1, -1},
                {11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1, -1},
                {2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1, -1},
                {9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7, -1},
                {3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10, -1},
                {1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1, -1},
                {4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1, -1},
                {4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1, -1},
                {0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1, -1},
                {3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1, -1},
                {3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1, -1},
                {0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1, -1},
                {9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1, -1},
                {1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
                {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}};

        typedef struct
        {
            vector3 p[3];
        } TRIANGLE;

        typedef struct
        {
            vector3 p[8];
            double val[8];
        } GRIDCELL;

        static vector3 VertexInterp(double T, const vector3& p1, const vector3& p2, double valp1, double valp2)
        {
            assert((valp2 - valp1) > 0.0001);

            vector3 p;
            double mu = (T - valp1) / (valp2 - valp1);

            //mu = (valp2) / (valp2 + valp1);
            p[0] = p1[0] + mu * (p2[0] - p1[0]);
            p[1] = p1[1] + mu * (p2[1] - p1[1]);
            p[2] = p1[2] + mu * (p2[2] - p1[2]);

            return (p);
        }

        static int polygonise(TRIANGLE* tri, const GRIDCELL& grid, double T)
        {
            int cubeindex = 0;
            if (grid.val[0] < T) cubeindex |= 1;
            if (grid.val[1] < T) cubeindex |= 2;
            if (grid.val[2] < T) cubeindex |= 4;
            if (grid.val[3] < T) cubeindex |= 8;
            if (grid.val[4] < T) cubeindex |= 16;
            if (grid.val[5] < T) cubeindex |= 32;
            if (grid.val[6] < T) cubeindex |= 64;
            if (grid.val[7] < T) cubeindex |= 128;

            vector3 vertlist[12];

            if (edgeTable[cubeindex] == 0 || edgeTable[cubeindex] == 255) return 0;

            if (edgeTable[cubeindex] & 1)
                vertlist[0] = VertexInterp(T, grid.p[0], grid.p[1], grid.val[0], grid.val[1]);
            if (edgeTable[cubeindex] & 2)
                vertlist[1] = VertexInterp(T, grid.p[1], grid.p[2], grid.val[1], grid.val[2]);
            if (edgeTable[cubeindex] & 4)
                vertlist[2] = VertexInterp(T, grid.p[2], grid.p[3], grid.val[2], grid.val[3]);
            if (edgeTable[cubeindex] & 8)
                vertlist[3] = VertexInterp(T, grid.p[3], grid.p[0], grid.val[3], grid.val[0]);
            if (edgeTable[cubeindex] & 16)
                vertlist[4] = VertexInterp(T, grid.p[4], grid.p[5], grid.val[4], grid.val[5]);
            if (edgeTable[cubeindex] & 32)
                vertlist[5] = VertexInterp(T, grid.p[5], grid.p[6], grid.val[5], grid.val[6]);
            if (edgeTable[cubeindex] & 64)
                vertlist[6] = VertexInterp(T, grid.p[6], grid.p[7], grid.val[6], grid.val[7]);
            if (edgeTable[cubeindex] & 128)
                vertlist[7] = VertexInterp(T, grid.p[7], grid.p[4], grid.val[7], grid.val[4]);
            if (edgeTable[cubeindex] & 256)
                vertlist[8] = VertexInterp(T, grid.p[0], grid.p[4], grid.val[0], grid.val[4]);
            if (edgeTable[cubeindex] & 512)
                vertlist[9] = VertexInterp(T, grid.p[1], grid.p[5], grid.val[1], grid.val[5]);
            if (edgeTable[cubeindex] & 1024)
                vertlist[10] = VertexInterp(T, grid.p[2], grid.p[6], grid.val[2], grid.val[6]);
            if (edgeTable[cubeindex] & 2048)
                vertlist[11] = VertexInterp(T, grid.p[3], grid.p[7], grid.val[3], grid.val[7]);

            /* Create the triangle */
            int n = 0;
            for (int i = 0; triTable[cubeindex][i] != -1; i += 3)
            {
                tri[n].p[0] = vertlist[triTable[cubeindex][i]];
                tri[n].p[1] = vertlist[triTable[cubeindex][i + 1]];
                tri[n].p[2] = vertlist[triTable[cubeindex][i + 2]];
                n++;
            }

            return n;
        }

        struct blob_info
        {
            real t;
            vector3 n;
        };

        typedef const blob* CPBLOB;
        struct blob_freearea_struct
        {
            int n;
            CPBLOB pointers[8];
        };

        static double pow2(double x) { return x * x; }
        static double weight(double x)
        {
            double x2 = x * x;
            double x4 = x2 * x2;
            double x6 = x2 * x4;
            return -4.0 / 9.0 * x6 + 17.0 / 9.0 * x4 - 22.0 / 9.0 * x2 + 1;
        }

        real get_weight(const std::vector<const blob*>& blobs, const vector3& p)
        {
            real w = 0;
            size_t bsz = blobs.size();
            for (size_t i = 0; i < bsz; i++)
            {
                real r = blobs[i]->radius();
                real r2 = r * r;
                real l2 = blobs[i]->distance2(p);
                if (r2 < l2) continue;

                w += weight(sqrt(l2 / r2));
            }
            return w;
        }

        class field_grid
        {
        public:
            field_grid(const vector3& cmin, const vector3& cmax, const vector3& min, const vector3& max, real delta, real T);
            void init(const std::vector<const blob*>& blobs);

        public:
            bool test(const ray& r, real dist) const;
            bool test(blob_info* info, const ray& r, real dist) const;
            bool test_xyz(int x, int y, int z, const ray& r, real dist) const;
            bool test_xyz(int x, int y, int z, blob_info* info, const ray& r, real dist) const;

        protected:
            void make_cell(GRIDCELL* cell, int x, int y, int z) const;

        private:
            std::vector<const blob*> blobs_;
            int ndiv_[3];
            real delta_[3];
            real idelta_[3];
            vector3 min_;
            vector3 max_;
            real T_;
        };

        field_grid::field_grid(const vector3& cmin, const vector3& cmax, const vector3& min, const vector3& max, real delta, real T)
        {
            //-----------------------------------------
            vector3 wid = max - min;
            //-----------------------------------------
            int imin[3];
            int imax[3];
            for (int k = 0; k < 3; k++)
            {
                real fmin = (cmin[k] - min[k]) / delta;
                real fmax = (cmax[k] - min[k]) / delta;
                imin[k] = (int)floor(fmin);
                imax[k] = (int)floor(fmax) + 1;
            }
            //-----------------------------------------
            vector3 fmin = min + vector3(delta * imin[0], delta * imin[1], delta * imin[2]);
            vector3 fmax = min + vector3(delta * imax[0], delta * imax[1], delta * imax[2]);

            int w = imax[0] - imin[0] + 1;
            int h = imax[1] - imin[1] + 1;
            int d = imax[2] - imin[2] + 1;

            ndiv_[0] = w;
            ndiv_[1] = h;
            ndiv_[2] = d;
            for (int i = 0; i < 3; i++)
            {
                delta_[i] = delta;
                idelta_[i] = 1.0 / delta;
            }
            //int sz = (w+1)*(h+1)*(d+1);
            //values_.resize(sz);
            min_ = fmin;
            max_ = fmax;
            T_ = T;
        }

        void field_grid::init(const std::vector<const blob*>& blobs)
        {
            blobs_ = blobs;
        }

        void field_grid::make_cell(GRIDCELL* cell, int x, int y, int z) const
        {
            //int w = ndiv_[0]+1;
            //int h = ndiv_[1]+1;
            //int d = ndiv_[2]+1;
            int px[8] = {x, x + 1, x + 1, x, x, x + 1, x + 1, x};
            int py[8] = {y, y, y, y, y + 1, y + 1, y + 1, y + 1};
            int pz[8] = {z, z, z + 1, z + 1, z, z, z + 1, z + 1};

            for (int i = 0; i < 8; i++)
            {
                int ix = px[i];
                int iy = py[i];
                int iz = pz[i];
                real xx = min_[0] + delta_[0] * ix;
                real yy = min_[1] + delta_[1] * iy;
                real zz = min_[2] + delta_[2] * iz;
                cell->p[i] = vector3(xx, yy, zz);
                cell->val[i] = get_weight(blobs_, cell->p[i]);
                //int idx = ((iz * h + iy) * w + ix);
                //cell->val[i] = values_[idx];
            }
        }

        bool test_TRIANGLE(const TRIANGLE& tri, const ray& r, real dist)
        {
            static const real EPSILON = values::epsilon() * 1000;
            real u, v, t;

            const vector3& org(r.origin());
            const vector3& dir(r.direction());
            const vector3& p0(tri.p[0]);
            const vector3& p1(tri.p[1]);
            const vector3& p2(tri.p[2]);

            //-e1 = p0-p1
            vector3 e1(p0 - p1); //vA

            //-e2 = p0-p2
            vector3 e2(p0 - p2); //vB

            //dir = GHI

            vector3 bDir(cross(e2, dir));

            real iM = dot(e1, bDir);

            if (EPSILON < iM)
            {
                //p0-org
                vector3 vOrg(p0 - org);

                u = dot(vOrg, bDir);
                if (u < 0 || iM < u) return false;

                vector3 vE(cross(e1, vOrg));

                v = dot(dir, vE);
                if (v < 0 || iM < u + v) return false;

                t = -dot(e2, vE);
                if (t <= 0) return false;
            }
            else if (iM < -EPSILON)
            {
                //p0-org
                vector3 vOrg(p0 - org); //JKL

                u = dot(vOrg, bDir);
                if (u > 0 || iM > u) return false;

                vector3 vE(cross(e1, vOrg));

                v = dot(dir, vE);
                if (v > 0 || iM > u + v) return false;

                t = -dot(e2, vE);
                if (t >= 0) return false;
            }
            else
            {
                return false;
            }

            iM = real(1.0) / iM;

            t *= iM;
            if (dist <= t) return false;

            return true;
        }

        bool test_TRIANGLE(blob_info* info, const TRIANGLE& tri, const ray& r, real dist)
        {
            static const real EPSILON = values::epsilon() * 1000;
            real u, v, t;

            const vector3& org(r.origin());
            const vector3& dir(r.direction());
            const vector3& p0(tri.p[0]);
            const vector3& p1(tri.p[1]);
            const vector3& p2(tri.p[2]);

            //-e1 = p0-p1
            vector3 e1(p0 - p1); //vA

            //-e2 = p0-p2
            vector3 e2(p0 - p2); //vB

            //dir = GHI

            vector3 bDir(cross(e2, dir));

            real iM = dot(e1, bDir);

            if (EPSILON < iM)
            {
                //p0-org
                vector3 vOrg(p0 - org);

                u = dot(vOrg, bDir);
                if (u < 0 || iM < u) return false;

                vector3 vE(cross(e1, vOrg));

                v = dot(dir, vE);
                if (v < 0 || iM < u + v) return false;

                t = -dot(e2, vE);
                if (t <= 0) return false;
            }
            else if (iM < -EPSILON)
            {
                //p0-org
                vector3 vOrg(p0 - org); //JKL

                u = dot(vOrg, bDir);
                if (u > 0 || iM > u) return false;

                vector3 vE(cross(e1, vOrg));

                v = dot(dir, vE);
                if (v > 0 || iM > u + v) return false;

                t = -dot(e2, vE);
                if (t >= 0) return false;
            }
            else
            {
                return false;
            }

            iM = real(1.0) / iM;

            t *= iM;
            if (dist <= t) return false;
            vector3 n = normalize(cross(e1, e2));
            ;
            //if(dot(n,dir)>0)n.negate();
            info->t = t;
            info->n = n;

            return true;
        }

        bool field_grid::test_xyz(int x, int y, int z, const ray& r, real dist) const
        {
            GRIDCELL cell;
            make_cell(&cell, x, y, z);
            TRIANGLE tri[8];
            int n = polygonise(tri, cell, T_);
            if (n)
            {
                for (int i = 0; i < n; i++)
                {
                    if (test_TRIANGLE(tri[i], r, dist)) return true;
                }
                return false;
            }
            else
            {
                return false;
            }
        }

        bool field_grid::test_xyz(int x, int y, int z, blob_info* info, const ray& r, real dist) const
        {
            GRIDCELL cell;
            make_cell(&cell, x, y, z);
            TRIANGLE tri[8];
            int n = polygonise(tri, cell, T_);
            if (n)
            {
                bool bRet = false;
                for (int i = 0; i < n; i++)
                {
                    if (test_TRIANGLE(info, tri[i], r, dist))
                    {
                        dist = info->t;
                        bRet = true;
                    }
                }
                return bRet;
            }
            else
            {
                return false;
            }
        }

        bool field_grid::test(const ray& r, real dist) const
        {
            vector3 pos;
            if (!test_AABB(&pos, min_, max_, r, dist)) return false;

            const vector3& org = r.origin();
            const vector3& ird = r.inversed_direction();

            const int* const ndiv = this->ndiv_;
            const real* const delta = this->delta_;
            const real* const idelta = this->idelta_;

            int plane[3];
            int step[3];
            real td[3];
            real tn[3];
            int out[3];

            for (int idx = 0; idx < 3; idx++)
            {
                plane[idx] = static_cast<int>((pos[idx] - min_[idx]) * idelta[idx]);
                //if(plane[idx] >= ndiv[idx])plane[idx]--;
                assert(plane[idx] != ndiv[idx]);

                if (ird[idx] > 0)
                { //plus
                    step[idx] = 1;
                    td[idx] = delta[idx] * ird[idx];
                    tn[idx] = ((min_[idx] + (plane[idx] + 1) * delta[idx]) - org[idx]) * ird[idx];
                    out[idx] = ndiv[idx];
                }
                else if (ird[idx] < 0)
                { //minus
                    step[idx] = -1;
                    td[idx] = -delta[idx] * ird[idx];
                    tn[idx] = ((min_[idx] + (plane[idx]) * delta[idx]) - org[idx]) * ird[idx];
                    out[idx] = -1;
                }
                else
                {
                    step[idx] = 0;
                    td[idx] = 0;
                    tn[idx] = values::far();
                    out[idx] = -1;
                }
            }

            //debug
            assert(plane[0] >= 0);
            assert(plane[1] >= 0);
            assert(plane[2] >= 0);

            assert(plane[0] < ndiv_[0]);
            assert(plane[1] < ndiv_[1]);
            assert(plane[2] < ndiv_[2]);
            //debug

            real tmin = 0;
            while (true)
            {
                if (test_xyz(plane[0], plane[1], plane[2], r, dist))
                {
                    return true;
                }

                int min_plane = 0;
                if (tn[min_plane] > tn[1]) min_plane = 1;
                if (tn[min_plane] > tn[2]) min_plane = 2;

                /* 3D DDA */
                if (dist < tn[min_plane]) break;
                plane[min_plane] += step[min_plane];
                if (plane[min_plane] == out[min_plane]) break;

                tn[min_plane] += td[min_plane];
            }

            return false;
        }

        bool field_grid::test(blob_info* info, const ray& r, real dist) const
        {
            vector3 pos;
            if (!test_AABB(&pos, min_, max_, r, dist)) return false;

            const vector3& org = r.origin();
            const vector3& ird = r.inversed_direction();

            const int* const ndiv = this->ndiv_;
            const real* const delta = this->delta_;
            const real* const idelta = this->idelta_;

            int plane[3];
            int step[3];
            real td[3];
            real tn[3];
            int out[3];

            for (int idx = 0; idx < 3; idx++)
            {
                plane[idx] = static_cast<int>((pos[idx] - min_[idx]) * idelta[idx]);
                //if(plane[idx] >= ndiv[idx])plane[idx]--;
                assert(plane[idx] != ndiv[idx]);

                if (ird[idx] > 0)
                { //plus
                    step[idx] = 1;
                    td[idx] = delta[idx] * ird[idx];
                    tn[idx] = ((min_[idx] + (plane[idx] + 1) * delta[idx]) - org[idx]) * ird[idx];
                    out[idx] = ndiv[idx];
                }
                else if (ird[idx] < 0)
                { //minus
                    step[idx] = -1;
                    td[idx] = -delta[idx] * ird[idx];
                    tn[idx] = ((min_[idx] + (plane[idx]) * delta[idx]) - org[idx]) * ird[idx];
                    out[idx] = -1;
                }
                else
                {
                    step[idx] = 0;
                    td[idx] = 0;
                    tn[idx] = values::far();
                    out[idx] = -1;
                }
            }

            //debug
            assert(plane[0] >= 0);
            assert(plane[1] >= 0);
            assert(plane[2] >= 0);

            assert(plane[0] < ndiv_[0]);
            assert(plane[1] < ndiv_[1]);
            assert(plane[2] < ndiv_[2]);
            //debug
            bool bRet = false;
            real tmin = 0;
            while (true)
            {
                if (test_xyz(plane[0], plane[1], plane[2], info, r, dist))
                {
                    bRet = true;
                    dist = info->t;
                }

                int min_plane = 0;
                if (tn[min_plane] > tn[1]) min_plane = 1;
                if (tn[min_plane] > tn[2]) min_plane = 2;

                /* 3D DDA */
                if (dist < tn[min_plane]) break;
                plane[min_plane] += step[min_plane];
                if (plane[min_plane] == out[min_plane]) break;

                tn[min_plane] += td[min_plane];
            }

            return bRet;
        }

        vector3 get_normal(const vector3& p, const CPBLOB* blobs, size_t sz)
        {
            static const real EPSILON = values::epsilon() * 1000;

            vector3 gradient = vector3(0.0f, 0.0f, 0.0f);

            bool bSet = false;
            for (size_t i = 0; i < sz; i++)
            {
                const blob* bp = blobs[i];

                real R = bp->radius();
                real R2 = R * R;

                vector3 normal = p - bp->get_near(p);
                real r2 = dot(normal, normal);
                if (R2 < r2) continue;
                real r = sqrt(r2);
                if (r <= EPSILON) continue;
                normal = normal * weight(r / R); //eight(r/R) / ;

                bSet = true;
                gradient += normal;
            }

            if (bSet)
            {
                return normalize(gradient);
            }
            else
            {
                return vector3(1, 0, 0);
            }
        }

        //---------------------------------------------------------------------------------
        bool test_blobs(blob_info* info, const vector3& min, const vector3& max, real delta, real T, const std::vector<const blob*>& blobs, const ray& r, real t0, real t1)
        {
            static const real EPSILON = values::epsilon() * 1000;
            //-----------------------------------------
            size_t bsz = blobs.size();
            if (bsz == 0) return false;
            real FAR = values::far();
            vector3 cmin(+FAR, +FAR, +FAR);
            vector3 cmax(-FAR, -FAR, -FAR);
            for (size_t i = 0; i < bsz; i++)
            {
                vector3 mmin = blobs[i]->min();
                vector3 mmax = blobs[i]->max();
                for (int k = 0; k < 3; k++)
                {
                    if (cmin[k] > mmin[k]) cmin[k] = mmin[k];
                    if (cmax[k] < mmax[k]) cmax[k] = mmax[k];
                }
            }

            field_grid fg(cmin, cmax, min, max, delta, T);
            fg.init(blobs);
            //-----------------------------------------
            if (info)
            {
                return fg.test(info, r, t1);
            }
            else
            {
                return fg.test(r, t1);
            }
        }
    }
}

namespace kaze
{

    mc_blob_tester::mc_blob_tester(const vector3& min, const vector3& max, real delta, real T)
    {
        min_ = min;
        max_ = max;
        delta_ = delta;
        T_ = T;
    }

    bool mc_blob_tester::test(const std::vector<const blob*>& blobs, const ray& r, real dist) const
    {
        return test_blobs(NULL, min_, max_, delta_, T_, blobs, r, 0, dist);
    }

    bool mc_blob_tester::test(const std::vector<const blob*>& blobs, test_info* info, const ray& r, real dist) const
    {
        blob_info binfo;
        if (test_blobs(&binfo, min_, max_, delta_, T_, blobs, r, 0, dist))
        {
            info->distance = binfo.t;
            info->geometric = binfo.n;

            blob_freearea_struct* fp = reinterpret_cast<blob_freearea_struct*>(info->freearea);
            int bsz = (int)blobs.size();
            if (bsz <= 8)
            {
                fp->n = bsz;
                memcpy(fp->pointers, &(blobs[0]), blobs.size() * sizeof(const blob*));
            }
            else
            {
                fp->n = 0;
                vector3 p = binfo.t * r.direction() + r.origin();
                vector3 n = get_normal(p, &(blobs[0]), bsz);
                info->position = p;
                //info->geometric = n;
                info->normal = n;
            }
            return true;
        }
        return false;
    }

    void mc_blob_tester::finalize(test_info* info, const ray& r, real dist) const
    {
        blob_freearea_struct* fp = reinterpret_cast<blob_freearea_struct*>(info->freearea);
        int bsz = fp->n;
        if (bsz)
        {
            vector3 p = info->distance * r.direction() + r.origin();
            vector3 n = get_normal(p, fp->pointers, bsz);
            info->position = p;
            //info->geometric = n;
            info->normal = n;
        }
    }

    int test_marching_cubes()
    {
        int x = 0;
        int y = 0;
        int z = 0;

        int px[8] = {x, x + 1, x + 1, x, x, x + 1, x + 1, x};
        int py[8] = {y, y, y, y, y + 1, y + 1, y + 1, y + 1};
        int pz[8] = {z, z, z + 1, z + 1, z, z, z + 1, z + 1};

        vector3 points[8];
        for (int i = 0; i < 8; i++)
        {
            points[i] = vector3(px[i], py[i], pz[i]);
        }

        GRIDCELL cell;
        for (int j = 0; j < 256; j++)
        {
            for (int i = 0; i < 8; i++)
            {
                cell.p[i] = points[i];
                cell.val[i] = (j & (1 << i)) ? 1 : 0;
            }
            TRIANGLE tri[8];
            int n = polygonise(tri, cell, 0.5);

            print_log("%d:", j);
            for (int k = 0; k < n; k++)
            {
                print_log("[(%f,%f,%f),(%f,%f,%f),(%f,%f,%f)],",
                          tri[k].p[0][0], tri[k].p[0][1], tri[k].p[0][2],
                          tri[k].p[1][0], tri[k].p[1][1], tri[k].p[1][2],
                          tri[k].p[2][0], tri[k].p[2][1], tri[k].p[2][2]);
            }
            print_log("\n");
        }

        return 0;
    }
}
