#include "st_blob_tester.h"

/*
 * cite: Sphere tracing: a geometric method for the antialiased ray tracing of implicit surfaces
 */

#include "intersection_ex.h"

namespace kaze
{
    namespace
    {

        static double pow2(double x) { return x * x; }

//
//
//
/*
    f(r)  = 2 * r^3 - 3 * r^2 +1
    f'(r) = 6 * r^2 - 6 * r
    f''(r)= 12* r - 6
    f''(r)=0:->r= 1/2
    |f'(1/2)|=|6/4 - 6/2|= |3/2-3| = 3/2
    */

/*
	f(r) = 1 -3*r + 3*r*r - r*r*r
	f'(r) = -3+6*r-3*r*r
	r''(r) = 6-6r
	f''(r)=0:->r= 1
	|f'(1)|=|-3+6-3|=0
	*/

#if 1
        static const double LIMIT_COEF = 2.0 / 3.0; //2.0/3.0;
        static double weight(double x)
        {
            return x * x * (2 * x - 3) + 1;
        }
#elif 1
        static const double LIMIT_COEF = 2.0 / 3.0;
        static double weight(double x)
        {
            return 1 - 3 * x + 3 * x * x - x * x * x;
            //1 - 3*A1 + 3*A1*A1 - A1*A1*A1
            //return x*x*(2*x-3)+1;
        }
#else
        static const double LIMIT_COEF = 3.0 / 5.0;
        static double weight(double x)
        {
            double x2 = x * x;
            double x4 = x2 * x2;
            double x6 = x2 * x4;
            return -4.0 / 9.0 * x6 + 17.0 / 9.0 * x4 - 22.0 / 9.0 * x2 + 1.0;
        }
#endif

        real get_distance(const std::vector<const blob*>& blobs, const vector3& p, real T)
        {
            size_t sz = blobs.size();

            real w = 0;
            real rad = 0;
            for (size_t i = 0; i < sz; i++)
            {
                real R = blobs[i]->radius();
                real R2 = R * R;
                real r2 = blobs[i]->distance2(p);
                if (r2 < R2)
                {
                    w += weight(sqrt(r2 / R2));
                }
                rad += R;
            }
            real ret = LIMIT_COEF * rad / sz * (T - w);

            return ret;
        }

        typedef const blob* CPBLOB;

        struct blob_info
        {
            real t;
        };

        struct blob_freearea_struct
        {
            int n;
            CPBLOB pointers[8];
        };

        vector3 get_normal(const vector3& p, const CPBLOB* blobs, size_t sz)
        {
            static const real EPSILON = values::epsilon() * 1000;

            vector3 gradient = vector3(0.0f, 0.0f, 0.0f);

            bool bSet = false;
            for (size_t i = 0; i < sz; i++)
            {
                const blob* bp = blobs[i];

                real R = bp->radius();
                real R2 = R * R;

                vector3 normal = p - bp->get_near(p);
                real r2 = dot(normal, normal);
                if (R2 < r2) continue;
                real r = sqrt(r2);
                if (r <= EPSILON) continue;
                normal = normal * weight(r / R);

                bSet = true;
                gradient += normal;
            }

            if (bSet)
            {
                return normalize(gradient);
            }
            else
            {
                return vector3(1, 0, 0);
            }
        }

        bool test_blobs_bound(const std::vector<const blob*>& blobs, const ray& r, real& t0, real& t1)
        {
            if (blobs.empty()) return false;
            size_t sz = blobs.size();
            vector3 cmin;
            vector3 cmax;

            cmin = blobs[0]->min();
            cmax = blobs[0]->max();
            for (size_t i = 1; i < sz; i++)
            {
                vector3 imin = blobs[i]->min();
                vector3 imax = blobs[i]->max();
                for (int j = 0; j < 3; j++)
                {
                    if (cmin[j] > imin[j]) cmin[j] = imin[j];
                    if (cmax[j] < imax[j]) cmax[j] = imax[j];
                }
            }

            range_AABB rng;
            if (test_AABB(&rng, cmin, cmax, r, t0, t1))
            {
                if (t0 < rng.tmin) t0 = rng.tmin;
                if (t1 > rng.tmax) t1 = rng.tmax;
                return true;
            }
            else
            {
                return false;
            }
        }

        bool test_blobs(
            blob_info* info,
            const std::vector<const blob*>& blobs,
            const ray& r,
            real t0, real t1,
            real T)
        {
            static const real EPSILON = values::epsilon() * 1000;

            if (test_blobs_bound(blobs, r, t0, t1))
            {
                real t = t0;
                while (t <= t1)
                {
                    vector3 p = r.origin() + r.direction() * t;
                    real d = get_distance(blobs, p, T);
                    if (d < EPSILON)
                    { // 'intersection' found
                        info->t = t + d;
                        return true;
                    }
                    else
                    {
                        t += d;
                    }
                }
                return false;
            }
            else
            {
                return false;
            }
        }
    }

    st_blob_tester::st_blob_tester(real T) : T_(T) {}

    bool st_blob_tester::test(const std::vector<const blob*>& blobs, const ray& r, real dist) const
    {
        blob_info binfo; //dummy
        return test_blobs(&binfo, blobs, r, 0, dist, T_);
    }
    bool st_blob_tester::test(const std::vector<const blob*>& blobs, test_info* info, const ray& r, real dist) const
    {

        blob_info binfo;
        if (test_blobs(&binfo, blobs, r, 0, dist, T_))
        {
            info->distance = binfo.t;

            blob_freearea_struct* fp = reinterpret_cast<blob_freearea_struct*>(info->freearea);
            int bsz = (int)blobs.size();
            if (bsz <= 8)
            {
                fp->n = bsz;
                memcpy(fp->pointers, &(blobs[0]), blobs.size() * sizeof(const blob*));
            }
            else
            {
                fp->n = 0;
                vector3 p = binfo.t * r.direction() + r.origin();
                vector3 n = get_normal(p, &(blobs[0]), bsz);
                info->position = p;
                info->geometric = n;
                info->normal = n;
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    void st_blob_tester::finalize(test_info* info, const ray& r, real dist) const
    {
        blob_freearea_struct* fp = reinterpret_cast<blob_freearea_struct*>(info->freearea);
        int bsz = fp->n;
        if (bsz)
        {
            vector3 p = info->distance * r.direction() + r.origin();
            vector3 n = get_normal(p, fp->pointers, bsz);
            info->position = p;
            info->geometric = n;
            info->normal = n;
        }
    }
}