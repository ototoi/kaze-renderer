#include "f2_blob_tester.h"

#include "values.h"
#include "test_info.h"
#include "newton_solver.h"

#include <vector>
#include <cmath>
#include <algorithm>

/*
 *	blob -
 *		Working Meta-ball ray tracing program from japanese PIXEL 
 *	magazine Number 2, 1989.  The only comments available were in Kanji. 
 */

#define EPSILON (1e-6)

namespace kaze
{
    namespace
    {

        struct blob_info
        {
            real t;
            vector3 p;
            vector3 n;
        };

        struct TList
        {
            const blob* bp;
            int i;
            double t[4], w, e;
            struct TList* next;
        };

        static TList* insert(TList* head, TList* tlp)
        {
            if (!head)
            {
                tlp->next = NULL;
                return tlp;
            }
            else if (tlp->t[tlp->i] <= head->t[head->i])
            {
                tlp->next = head;
                return tlp;
            }
            else
            {
                head->next = insert(head->next, tlp);
                return head;
            }
        }

        static TList* makeTList(TList tl[], int num)
        {
            TList* head = NULL;
            for (int i = 0; i < num; i++)
            {
                head = insert(head, &tl[i]);
            }
            return head;
        }

        static double w(double density, double di)
        {
            if (di > 1.0)
                return 0.0;
            else if (di > 1.0 / 3.0)
                return density * 1.5 * (1.0 - di) * (1.0 - di);
            else
                return density * (1.0 - 3.0 * di * di);
        }

        static double e_coeff[] = {
            0.333333333333333,
            0.333296108132269,
            0.333184627603903,
            0.332999486619232,
            0.332741690650759,
            0.332412677547021,
            0.332014357522061,
            0.331549178931945,
            0.331020129320951,
            0.330430913816224,
            0.329785916662490,
            0.329090433751777,
            0.328350770396655,
            0.327574345117823,
            0.326769966724207,
            0.325948038533753,
            0.325120870846941,
            0.324302936409454,
            0.323511297428632,
            0.322766080927225,
            0.322091038482183,
            0.321514205398798,
            0.321068355050146,
            0.320792326971123,
            0.320731397418826,
            0.320938554423058,
            0.321475645818065,
            0.322413889491328,
            0.323835220891029,
            0.325832756973406,
            0.328510866794271,
            0.331984578354516,
            0.336378729013820,
            0.341824362785149,
            0.348146334442774,
            0.354415094470032,
            0.360559982348853,
            0.366584982389539,
            0.372493871662183,
            0.378290220289637,
            0.383977443662945,
            0.389558944209076,
            0.395037659932655,
            0.400416769979840,
            0.405699012969088,
            0.410887172495860,
            0.415983904035928,
            0.420991752020325,
            0.425913143317177,
            0.430750406298490,
            0.435505771077590,
            0.440181392207373,
            0.444779383780036,
            0.449301732218590,
            0.453750364314801,
            0.458127137170539,
            0.462433874961966,
            0.466672277362941,
            0.470844017125615,
            0.474950720042299,
            0.478993949366151,
            0.482975212114009,
            0.486895961336373,
            0.490757608939165,
            0.494561514144152,
            0.498308998945394,
            0.502001349532131,
            0.505639800469869,
            0.509225549601718,
            0.512759767056639,
            0.516243573845068,
            0.519678070403481,
            0.523064320400961,
            0.526403338884434,
            0.529696144456683,
            0.532943706263207,
            0.536146950373003,
            0.539306802562028,
            0.542424157356508,
            0.545499879777966,
            0.548534810592947,
            0.551529765753788,
            0.554485538628647,
            0.557402910013114,
            0.560282632700581,
            0.563125445595638,
            0.565932049264223,
            0.568703117250259,
            0.571439367868627,
            0.574141469655286,
            0.576810017482981,
            0.579445652596002,
            0.582049040955012,
            0.584620670135048,
            0.587161227315352,
            0.589671202282686,
            0.592151208561621,
            0.594601716796599,
            0.597023351364347,
            0.599416477559829,
            0.601781460678009, /* added by heuristics */
        };

        static double e(double r)
        {
            int i;
            double w;

            if (r >= 1.0)
                return e_coeff[100];
            else if (r <= 0.0)
                return e_coeff[0];
            else
            {
                i = (int)floor(r * 100);
                w = (r - i * 0.01) * 100;
                return e_coeff[i] * (1.0 - w) + e_coeff[i + 1] * w;
            }
        }

        static bool intersectBlobSphere(const ray& r, const blob* bp, TList* tlp)
        {
            double t0 = 0;
            double tm = 0;
            double t3 = 0;
            double di, wi, ei, td, ds;

            const vector3 org = r.origin();
            const vector3 dir = r.direction();
            const vector3 center = bp->center();

            vector3 v = org - center;
            double rad = bp->radius();

            double a = dot(v, v);
            double b = dot(dir, v);
            double c = a - rad * rad;
            double d = b * b - c;
            if (d <= 0)
            {
                return false;
            }
            else
            {
                d = sqrt(d);
                if (b >= 0)
                {
                    t0 = -b - d;
                    t3 = c / (-b - d);
                }
                else
                {
                    t0 = c / (-b + d);
                    t3 = -b + d;
                }
                tm = (t0 + t3) / 2.0;

                /* di, wi, ei, td, ds */
                vector3 vi = dir * tm + org - center;
                di = dot(vi, vi); //distace
                di = sqrt(di);
                di = di / rad;
                wi = w(bp->density(), di);
                ei = e(di);
                td = (t3 - t0) / 2.0;
                ds = td * (1.0 - ei);
                /* bp */
                tlp->bp = bp;
                /* i */
                tlp->i = 0;
                /* t[] */
                tlp->t[0] = t0;
                tlp->t[1] = t0 + ds;
                tlp->t[2] = t3 - ds;
                tlp->t[3] = t3;
                /* w, e, a, b, c */
                tlp->w = wi;
                tlp->e = ei;
                return true;
            }
        }

        static vector3 getNormal(TList* head, const vector3& p)
        {
            int k = 0;
            vector3 n(0, 0, 0);
            for (TList* tlp = head; tlp; tlp = tlp->next)
            {
                if (tlp != head && tlp->i == 0)
                {
                    continue;
                }
                else
                {
                    const blob* bp = tlp->bp;
                    vector3 v = p - bp->center();
                    double r = length(v) / bp->radius();
                    double ratio = w(bp->density(), r);
                    n += v * ratio;
                    k++;
                }
            }

            if (k)
            {
                return normalize(n);
            }
            else
            {
                return vector3(0, 0, 1);
            }
        }

        static bool test_blobs(
            blob_info* info,
            const std::vector<const blob*>& blobs,
            const ray& r,
            double tmin, double tmax,
            real T)
        {
            const vector3 org = r.origin();
            const vector3 dir = r.direction();

            size_t bsz = blobs.size();
            std::vector<TList> tl;
            tl.reserve(bsz);
            for (size_t i = 0; i < bsz; i++)
            {
                TList tmp;
                if (intersectBlobSphere(r, blobs[i], &tmp))
                {
                    tl.push_back(tmp);
                }
            }
            int num = (int)tl.size();
            if (num)
            {
                double a = 0;
                double b = 0;
                double c = -1 + (0.5 - T);
                TList* head = makeTList(&tl[0], num);
                while (head && (head->next || head->i < 3))
                {
                    double td = (head->t[3] - head->t[0]) / 2.0;
                    double ds = td * (1 - head->e);
                    int i = head->i;
                    double tmp = 0;
                    switch (i)
                    {
                    case 0:
                        tmp = head->w / (ds * td);
                        break;
                    case 1:
                        tmp = head->w / (head->e * -ds * td);
                        break;
                    case 2:
                        tmp = head->w / (head->e * ds * td);
                        break;
                    case 3:
                        tmp = head->w / (-ds * td);
                        break;
                    default:
                        break;
                    }

                    double t0 = 0;
                    double t1 = 0;
                    a += tmp;
                    b += -2.0 * tmp * head->t[i];
                    c += tmp * head->t[i] * head->t[i];
                    if (a != 0.0)
                    {
                        double d = b * b - 4.0 * a * c;
                        if (d > 0.0)
                        {
                            t0 = (-b - sqrt(d)) / 2.0;
                            t1 = c / t0;
                            t0 /= a;
                            if (t0 > t1)
                            {
                                tmp = t0;
                                t0 = t1;
                                t1 = tmp;
                            }
                        }
                        else
                        {
                            t0 = t1 = -1;
                        }
                    }
                    else if (b != 0.0)
                    {
                        t0 = t1 = -c / b;
                    }
                    else
                    {
                        t0 = t1 = -1;
                    }

                    if (i < 3 && (!head->next || head->t[i + 1] < head->next->t[head->next->i]))
                    {
                        tmax = head->t[i + 1];
                    }
                    else
                    {
                        tmax = head->next->t[head->next->i];
                    }

                    if (t0 > tmin && head->t[i] - EPSILON <= t0 && t0 <= tmax + EPSILON)
                    {
                        vector3 p = org + t0 * dir;
                        vector3 n = getNormal(head, p);

                        info->t = t0;
                        info->p = p;
                        info->n = n;

                        return true;
                    }
                    else if (t1 > tmin && head->t[i] - EPSILON <= t1 && t1 <= tmax + EPSILON)
                    {
                        vector3 p = org + t1 * dir;
                        vector3 n = getNormal(head, p);

                        info->t = t1;
                        info->p = p;
                        info->n = n;

                        return true;
                    }

                    if (++head->i <= 3)
                    {
                        head = insert(head->next, head);
                    }
                    else
                    {
                        head = head->next;
                    }
                }
            }

            return false;
        }
    }

    f2_blob_tester::f2_blob_tester(real T) : T_(T) {}

    bool f2_blob_tester::test(const std::vector<const blob*>& blobs, const ray& r, real dist) const
    {
        real T = T_;
        blob_info binfo;
        if (test_blobs(&binfo, blobs, r, 0, dist, T))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool f2_blob_tester::test(const std::vector<const blob*>& blobs, test_info* info, const ray& r, real dist) const
    {
        real T = T_;
        blob_info binfo;
        if (test_blobs(&binfo, blobs, r, 0, dist, T))
        {
            info->distance = binfo.t;
            vector3 p = binfo.p;
            vector3 n = binfo.n;
            info->position = p;
            info->geometric = n;
            info->normal = n;
            return true;
        }
        else
        {
            return false;
        }
    }

    void f2_blob_tester::finalize(test_info* info, const ray& r, real dist) const
    {
        ;
    }
}