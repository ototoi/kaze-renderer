#include "f2x_blob_tester.h"

#include "values.h"
#include "test_info.h"
#include "newton_solver.h"

#include <vector>
#include <cmath>
#include <algorithm>

namespace kaze
{
    namespace
    {

        static int solve(double root[], const double coeff[], double x0, double x1)
        {
            static newton_solver ns;
            return ns.solve2(root, coeff, x0, x1);
        }
        //---------------------------------------------------------------------
        typedef const blob* CPBLOB;

        struct blob_info
        {
            real t;
        };

        struct blob_freearea_struct
        {
            int n;
            CPBLOB pointers[8];
        };

        struct poly
        {
            double t;
            double coeff[3];
        };

        struct poly_sorter : public std::binary_function<poly, poly, bool>
        {
            bool operator()(const poly& a, const poly& b) const
            {
                return a.t < b.t;
            }
        };

        const real density = 1.0;
        const real crad = 1.0;

        static double w(double di)
        {
            if (di > 1.0)
                return 0.0;
            else if (di > 1.0 / 3.0)
                return density * 1.5 * (1.0 - di) * (1.0 - di);
            else
                return density * (1.0 - 3.0 * di * di);
        }

        static double pow2(double x) { return x * x; }

        real calc_weight(const vector3& P, const blob* blob)
        {
            real R2 = pow2(blob->radius());
            real L2 = blob->distance2(P);

            if (L2 > R2) return 0;

            real mx = w(sqrt(L2 / R2));

            return mx;
        }

        void make_polynomial(
            std::vector<poly>& interval,
            const std::vector<const blob*>& blobs,
            const ray& r,
            real t0, real t1)
        {
            static const real EPSILON = values::epsilon() * 1000;

            vector3 org = r.origin();
            vector3 dir = r.direction();

            std::vector<real> distances;

            size_t bsz = blobs.size();
            for (size_t i = 0; i < bsz; i++)
            {
                real R = blobs[i]->radius() * crad;
                vector3 vO = org - blobs[i]->center();
                vector3 vD = dir;

                real B = dot(vO, vD);
                real Cx = dot(vO, vO);

                real C = Cx - R * R;

                real D = B * B - C;
                if (D < 0) continue;
                real x1 = fabs(B) + sqrt(D);
                if (B >= 0.0)
                {
                    x1 = -x1;
                }
                real x2 = C / x1;
                if (x1 > x2) std::swap(x1, x2);

                real C3 = Cx - R * R / 9.0;
                real D3 = B * B - C3;

                real iR = 1.0 / R;
                real iR2 = iR * iR;

                const double K = 1.0;

                double coeffs[3];
                coeffs[0] = density * 3.0 / 2.0 * iR2;
                coeffs[1] = density * 3.0 * (B - R) * iR2;
                coeffs[2] = density * 3.0 / 2.0 * (Cx - 2.0 * R * K + R * R) * iR2;

                if (D3 <= 0)
                { //out only
                    poly p0 = {x1, +coeffs[0], +coeffs[1], +coeffs[2]};
                    poly p1 = {x2, -coeffs[0], -coeffs[1], -coeffs[2]};

                    interval.push_back(p0);
                    interval.push_back(p1);
                }
                else
                {
                    real y1 = (fabs(B) + sqrt(D3));
                    if (B >= 0.0)
                    {
                        y1 = -y1;
                    }
                    real y2 = C / (y1);
                    if (y1 > y2) std::swap(y1, y2);

                    double coeffs3[3];
                    coeffs3[0] = density * -3.0 * iR2;
                    coeffs3[1] = density * -6.0 * iR2 * B;
                    coeffs3[2] = density * (1.0 - 3.0 * iR2 * Cx);

                    poly p0 = {x1, +coeffs[0], +coeffs[1], +coeffs[2]};
                    poly p1 = {y1, -coeffs[0] + coeffs3[0], -coeffs[1] + coeffs3[1], -coeffs[2] + coeffs3[2]};
                    poly p2 = {y2, +coeffs[0] - coeffs3[0], +coeffs[1] - coeffs3[1], +coeffs[2] - coeffs3[2]};
                    poly p3 = {x2, -coeffs[0], -coeffs[1], -coeffs[2]};

                    interval.push_back(p0);
                    interval.push_back(p1);
                    interval.push_back(p2);
                    interval.push_back(p3);
                }
            }
            std::sort(interval.begin(), interval.end(), poly_sorter());
        }

        bool test_polynomial(
            blob_info* info,
            const std::vector<poly>& interval,
            const ray& r,
            real t0, real t1,
            real T = 0.5)
        {
            static const real EPSILON = values::epsilon() * 1000;
            // static const real T = 0.5;

            if (interval.size() < 2) return false;

            double coeff[3] = {0, 0, -T};
            double root[2];

            size_t sz = interval.size();
            for (size_t i = 0; i < sz - 1; i++)
            {
                real ts = interval[i].t;
                real te = interval[i + 1].t;

                assert(ts <= te);

                for (int j = 0; j < 3; j++)
                {
                    coeff[j] += interval[i].coeff[j];
                }

                if (te - ts <= 0) continue;
                if (te < t0) continue;
                if (t1 < ts) break;

                int n = solve(root, coeff, ts, te);

                if (n)
                {
                    for (int k = 0; k < n; k++)
                    {
                        double t = root[k];
                        if (t1 <= t) break;
                        if (t0 <= t)
                        {
                            info->t = t;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        vector3 get_normal(const vector3& p, const CPBLOB* blobs, size_t sz)
        {
            static const real EPSILON = values::epsilon() * 1000;

            vector3 gradient = vector3(0.0f, 0.0f, 0.0f);

            bool bSet = false;
            for (size_t i = 0; i < sz; i++)
            {
                const blob* bp = blobs[i];

                real R = bp->radius() * crad;
                real R2 = R * R;

                vector3 normal = p - bp->get_near(p);
                real r2 = dot(normal, normal);
                if (R2 < r2) continue;
                real r = sqrt(r2);
                if (r <= EPSILON) continue;
                normal = normal * w(r / R); //eight(r/R) / ;

                bSet = true;
                gradient += normal;
            }

            if (bSet)
            {
                return normalize(gradient);
            }
            else
            {
                return vector3(1, 0, 0);
            }
        }

        bool test_blobs(
            blob_info* info,
            const std::vector<const blob*>& blobs,
            const ray& r,
            real t0, real t1,
            real T)
        {
            bool bRet = false;
            std::vector<poly> interval;
            make_polynomial(interval, blobs, r, t0, t1);
            bRet = test_polynomial(info, interval, r, t0, t1, T);
            return bRet;
        }
    }

    f2x_blob_tester::f2x_blob_tester(real T) : T_(T) {}

    bool f2x_blob_tester::test(const std::vector<const blob*>& blobs, const ray& r, real dist) const
    {
        real T = T_;
        blob_info binfo;
        if (test_blobs(&binfo, blobs, r, 0, dist, T))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool f2x_blob_tester::test(const std::vector<const blob*>& blobs, test_info* info, const ray& r, real dist) const
    {
        real T = T_;
        blob_info binfo;
        if (test_blobs(&binfo, blobs, r, 0, dist, T))
        {
            info->distance = binfo.t;

            blob_freearea_struct* fp = reinterpret_cast<blob_freearea_struct*>(info->freearea);
            int bsz = (int)blobs.size();
            if (bsz <= 8)
            {
                fp->n = bsz;
                memcpy(fp->pointers, &(blobs[0]), blobs.size() * sizeof(const blob*));
            }
            else
            {
                fp->n = 0;
                vector3 p = binfo.t * r.direction() + r.origin();
                vector3 n = get_normal(p, &(blobs[0]), bsz);
                info->position = p;
                info->geometric = n;
                info->normal = n;
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    void f2x_blob_tester::finalize(test_info* info, const ray& r, real dist) const
    {
        blob_freearea_struct* fp = reinterpret_cast<blob_freearea_struct*>(info->freearea);
        int bsz = fp->n;
        if (bsz)
        {
            vector3 p = info->distance * r.direction() + r.origin();
            vector3 n = get_normal(p, fp->pointers, bsz);
            info->position = p;
            info->geometric = n;
            info->normal = n;
        }
    }
}