#ifndef KAZE_AP_BLOB_TESTER_H
#define KAZE_AP_BLOB_TESTER_H

#include "blob_tester.h"

namespace kaze
{

    class ap_blob_tester : public blob_tester
    {
    public:
        ap_blob_tester(real T = real(0.5));

    public:
        bool test(const std::vector<const blob*>& blobs, const ray& r, real dist) const;
        bool test(const std::vector<const blob*>& blobs, test_info* info, const ray& r, real dist) const;

    public:
        void finalize(test_info* info, const ray& r, real dist) const;

    private:
        real T_;
    };
}

#endif