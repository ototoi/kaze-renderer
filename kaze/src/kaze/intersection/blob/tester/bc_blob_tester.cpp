#include "bc_blob_tester.h"

#include <vector>
#include <cmath>
#include <algorithm>

namespace kaze
{
    namespace
    {
        typedef const blob* CPBLOB;

        struct blob_info
        {
            real t;
        };

        struct blob_freearea_struct
        {
            int n;
            CPBLOB pointers[8];
        };

        static double pow2(double x) { return x * x; }
        static double weight(double x)
        {
            double x2 = x * x;
            double x4 = x2 * x2;
            double x6 = x2 * x4;
            return -4.0 / 9.0 * x6 + 17.0 / 9.0 * x4 - 22.0 / 9.0 * x2 + 1.0;
        }

        real calc_weight(const vector3& P, const blob* blob)
        {
            real R2 = pow2(blob->radius());
            real L2 = blob->distance2(P);

            if (L2 > R2) return 0;

            real mx = weight(sqrt(L2 / R2));

            return mx;
        }

        inline void meta_func(double e[3], double a)
        {
            double a2 = a * a;
            double ds = 16.0 / 27.0 * a2;
            double dm = 8.0 / 45.0 * (8.0 * a + 5.0) * a2;

            e[0] = ds; //f[2]
            e[1] = dm; //f[3]
            e[2] = ds; //f[4]
        }

        inline double cross(real u1, real u2, real v1, real v2)
        {
            return -v1 * u2 + u1 * v2;
        }

        inline double check_line(const vector2& a, const vector2& b, const vector2& p)
        {
            vector2 ab = b - a;
            vector2 ap = p - a;

            return cross(ab[0], ab[1], ap[0], ap[1]);
        }

        /*
	struct x_sorter{
		bool operator()(const vector2& a, const vector2& b)const
		{
			return a[0]<b[0];
		}
	};
    */

        void push_upper_chain(std::vector<vector2>& chain, const vector2& p)
        {
            int sz = (int)chain.size();
            if (sz < 2)
            {
                chain.push_back(p);
            }
            else
            {
                vector2 a = chain[sz - 2]; //
                vector2 b = chain[sz - 1]; //
                if (0 <= check_line(a, b, p))
                {
                    chain.pop_back();
                    push_upper_chain(chain, p);
                }
                else
                {
                    chain.push_back(p);
                }
            }
        }
        void push_lower_chain(std::vector<vector2>& chain, const vector2& p)
        {
            int sz = (int)chain.size();
            if (sz < 2)
            {
                chain.push_back(p);
            }
            else
            {
                vector2 a = chain[sz - 2]; //
                vector2 b = chain[sz - 1]; //
                if (0 >= check_line(a, b, p))
                {
                    chain.pop_back();
                    push_lower_chain(chain, p);
                }
                else
                {
                    chain.push_back(p);
                }
            }
        }

        void convex_hull(std::vector<vector2>& upper_chain, std::vector<vector2>& lower_chain, const std::vector<vector2>& points)
        {
            using namespace std;
            int sz = (int)points.size();
            if (sz < 3) return;
            upper_chain.reserve(sz >> 1);
            lower_chain.reserve(sz >> 1);

            //std::vector<vector2> tp(points);
            //std::sort(tp.begin(), tp.end(), x_sorter());

            //
            upper_chain.push_back(points[0]);
            upper_chain.push_back(points[1]);
            for (int i = 2; i < sz; i++)
            {
                push_upper_chain(upper_chain, points[i]);
            }

            //
            lower_chain.push_back(points[0]);
            lower_chain.push_back(points[1]);
            for (int i = 2; i < sz; i++)
            {
                push_lower_chain(lower_chain, points[i]);
            }
        }

        void split_bezier(double a[], double b[], const double p[], int i, int n, double t)
        {
            int tn = n - i; //3

            a[i] = p[0];
            b[tn] = p[tn];
            if (tn < 1) return;
            double tmp[6];
            //---------------------
            for (int j = 0; j < tn; j++)
            {
                tmp[j] = (1 - t) * p[j] + t * p[j + 1]; //p[j] + t*(p[j+1] - p[j])
            }
            //---------------------
            split_bezier(a, b, tmp, i + 1, n, t);
        }

        void split_bezier6(double a[7], double b[7], const double p[7], double t)
        {
            split_bezier(a, b, p, 0, 6, t);
        }

        void split_bezier6(double te0[7], double e[7], double t0, double t1)
        {
            double a = t0 / t1;
            double b = t1;
            double te1[7];
            double te2[7];
            split_bezier6(te1, te2, e, b); //te1
            split_bezier6(te2, te0, te1, a);
        }

        int scan_chain(double t[], std::vector<vector2>& chain)
        {
            int n = 0;
            size_t sz = chain.size();
            if (sz < 2) return n;
            for (size_t i = 0; i < sz - 1; i++)
            {
                if (chain[i][1] * chain[i + 1][1] <= 0)
                {
                    double a = fabs(chain[i][1]);
                    double b = fabs(chain[i + 1][1]);
                    double k = chain[i][0] + a / (a + b) * (chain[i + 1][0] - chain[i][0]);
                    t[n] = k;
                    n++;
                }
            }
            return n;
        }

        bool is_intersected(double e[7])
        {
            bool bUpper = false;
            bool bLower = false;
            for (int i = 0; i < 7; i++)
            {
                if (e[i] < 0)
                {
                    bLower = true;
                }
                else
                {
                    bUpper = true;
                }
            }
            return (bUpper && bLower);
        }

        int clip_bezier6(double r[], double e[7], double eps, double wide = 1.0)
        {
            if (!is_intersected(e)) return 0;

            std::vector<vector2> points(7);
            for (int i = 0; i < 7; i++)
            {
                points[i] = vector2(i * 1.0 / 6.0, e[i]);
            }
            std::vector<vector2> upper;
            std::vector<vector2> lower;
            convex_hull(upper, lower, points);

            int n = 0;
            double tt[2];
            n += scan_chain(tt + n, upper);
            n += scan_chain(tt + n, lower);

            if (n == 2)
            {
                if (tt[0] > tt[1]) std::swap(tt[0], tt[1]);

                double t0 = tt[0];
                double t1 = tt[1];

                double diff = t1 - t0;

                double world_diff = wide * diff;

                if (world_diff < eps)
                {
                    r[0] = 0.5 * (t0 + t1); //
                    return 1;
                }
                else
                {
                    if (diff < 0.5)
                    {
                        double a[7];
                        split_bezier6(a, e, t0, t1);

                        int k = 0;
                        int m;
                        m = clip_bezier6(r, a, eps, world_diff);
                        for (int j = 0; j < m; j++)
                        {
                            r[j] = t0 + diff * r[j];
                        }
                        k += m;

                        return k;
                    }
                    else
                    { //split
                        double tm = 0.5 * (t0 + t1);
                        double wm = 0.5 * diff;

                        world_diff *= 0.5;

                        double a[7];
                        double b[7];
                        split_bezier6(a, e, t0, tm);
                        split_bezier6(b, e, tm, t1);

                        int k = 0;
                        int m;
                        m = clip_bezier6(r, a, eps, world_diff);
                        for (int j = 0; j < m; j++)
                        {
                            r[j] = t0 + wm * r[j];
                        }
                        k += m;
                        r += m;
                        m = clip_bezier6(r, b, eps, world_diff);
                        for (int j = 0; j < m; j++)
                        {
                            r[j] = tm + wm * r[j];
                        }
                        k += m;
                        return k;
                    }
                }
            }
            else
            {
                return 0;
            }
        }

        struct poly
        {
            double t0;
            double t1;
            double e[7];
        };

        struct poly_sorter
        {
            bool operator()(const poly& a, const poly& b)
            {
                return a.t0 < b.t0;
            }
        };

        void composite_polynomial(
            std::vector<poly>& polys)
        {
            size_t psz = polys.size();
            //if(psz == 0)return;

            std::vector<double> dist(psz * 2);
            for (size_t i = 0; i < psz; i++)
            {
                dist[2 * i + 0] = polys[i].t0;
                dist[2 * i + 1] = polys[i].t1;
            }
            std::sort(dist.begin(), dist.end());
            dist.erase(std::unique(dist.begin(), dist.end()), dist.end());

            size_t dsz = dist.size();
            size_t csz = dsz - 1;
            std::vector<poly> cpoly(csz);
            memset(&cpoly[0], 0, sizeof(poly) * csz);
            for (size_t i = 0; i < csz; i++)
            {
                cpoly[i].t0 = dist[i];
                cpoly[i].t1 = dist[i + 1];
            }

            for (size_t j = 0; j < psz; j++)
            {
                double p0 = polys[j].t0;
                double p1 = polys[j].t1;
                double e[7];
                memcpy(e, polys[j].e, sizeof(double) * 7);

                for (size_t i = 0; i < csz; i++)
                {
                    double t0 = cpoly[i].t0;
                    double t1 = cpoly[i].t1;

                    if (t1 <= p0) continue;
                    if (p1 <= t0) break;

                    if (t0 <= p0)
                    {
                        if (p1 <= t1)
                        {
                            for (int k = 0; k < 7; k++)
                            {
                                cpoly[i].e[k] += e[k];
                            }
                            break;
                        }
                        else
                        {
                            double s = (t1 - p0) / (p1 - p0);
                            p0 = t1;

                            assert(0 < s && s < 1);

                            double a[7];
                            double b[7];
                            split_bezier6(a, b, e, s);

                            for (int k = 0; k < 7; k++)
                            {
                                cpoly[i].e[k] += a[k];
                            }
                            memcpy(e, b, sizeof(double) * 7);
                        }
                    }
                    else
                    {
                        assert(0);
                    }
                }
            }

            polys.swap(cpoly);
        }

        void make_polynomial(
            std::vector<poly>& polys,
            const std::vector<const blob*>& blobs,
            const ray& r,
            real t0, real t1)
        {
            static const real EPSILON = values::epsilon() * 1000;

            vector3 org = r.origin();
            vector3 dir = r.direction();

            std::vector<real> distances;

            size_t bsz = blobs.size();

            for (size_t i = 0; i < bsz; i++)
            {
                real R = blobs[i]->radius();
                vector3 vO = org - blobs[i]->center();
                vector3 vD = dir;

                real B = dot(vO, vD);
                real Cx = dot(vO, vO);

                real C = Cx - R * R;

                real D = B * B - C;
                if (D < EPSILON) continue;
                double sqrD = sqrt(D);
                double x1 = fabs(B) + sqrD;
                if (B >= 0.0)
                {
                    x1 = -x1;
                }
                double x2 = C / x1;
                if (x1 > x2) std::swap(x1, x2);

                double s0 = x1;
                double s1 = x2;

                if (s1 < t0 || t1 < s0) continue;

                double a = pow2(sqrD / R);

                double e[3];
                meta_func(e, a);

                poly p = {s0, s1, 0, 0, e[0], e[1], e[2], 0, 0};

                polys.push_back(p);
            }
            if (!polys.empty())
                composite_polynomial(polys);
        }

        bool test_polynomial(
            blob_info* info,
            const std::vector<poly>& polys,
            const ray& r,
            real t0, real t1,
            real T = 0.5)
        {
            static const real EPSILON = 1.0 / (1024 * 1024);
            // static const real T = 0.5;

            //if(polys.size()<1)return false;

            double root[6];

            double e[7];
            size_t sz = polys.size();
            for (size_t i = 0; i < sz; i++)
            {
                real ts = polys[i].t0;
                real te = polys[i].t1;

                if (te < t0) break;
                if (t1 < ts) continue;

                for (int j = 0; j < 7; j++)
                {
                    e[j] = polys[i].e[j] - T;
                }

                double dif = te - ts;
                int n = clip_bezier6(root, e, EPSILON, dif);
                if (n)
                {
                    for (int k = 0; k < n; k++)
                    {
                        double t = ts + root[k] * dif;
                        if (t1 <= t) break;
                        if (t0 <= t)
                        {
                            info->t = t;
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        vector3 get_normal(const vector3& p, const CPBLOB* blobs, size_t sz)
        {
            static const real EPSILON = values::epsilon() * 1000;

            vector3 gradient = vector3(0.0f, 0.0f, 0.0f);

            bool bSet = false;
            for (size_t i = 0; i < sz; i++)
            {
                const blob* bp = blobs[i];

                real R = bp->radius();
                real R2 = R * R;

                vector3 normal = p - bp->get_near(p);
                real r2 = dot(normal, normal);
                if (R2 < r2) continue;
                real r = sqrt(r2);
                if (r <= EPSILON) continue;
                normal = normal * weight(r / R); //eight(r/R) / ;

                bSet = true;
                gradient += normal;
            }

            if (bSet)
            {
                return normalize(gradient);
            }
            else
            {
                return vector3(1, 0, 0);
            }
        }

        bool test_blobs(
            blob_info* info,
            const std::vector<const blob*>& blobs,
            const ray& r,
            real t0, real t1,
            real T)
        {
            bool bRet = false;
            std::vector<poly> polys;
            make_polynomial(polys, blobs, r, t0, t1);
            bRet = test_polynomial(info, polys, r, t0, t1, T);
            return bRet;
        }
    }

    bc_blob_tester::bc_blob_tester(real T) : T_(T) {}

    bool bc_blob_tester::test(const std::vector<const blob*>& blobs, const ray& r, real dist) const
    {
        real T = T_;
        blob_info binfo;
        if (test_blobs(&binfo, blobs, r, 0, dist, T))
        {
            return true;
        }
        else
        {

            return false;
        }
    }

    bool bc_blob_tester::test(const std::vector<const blob*>& blobs, test_info* info, const ray& r, real dist) const
    {
        real T = T_;
        blob_info binfo;
        if (test_blobs(&binfo, blobs, r, 0, dist, T))
        {
            info->distance = binfo.t;

            blob_freearea_struct* fp = reinterpret_cast<blob_freearea_struct*>(info->freearea);
            int bsz = (int)blobs.size();
            if (bsz <= 8)
            {
                fp->n = bsz;
                memcpy(fp->pointers, &(blobs[0]), blobs.size() * sizeof(const blob*));
            }
            else
            {
                fp->n = 0;
                vector3 p = binfo.t * r.direction() + r.origin();
                vector3 n = get_normal(p, &(blobs[0]), bsz);
                info->position = p;
                info->geometric = n;
                info->normal = n;
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    void bc_blob_tester::finalize(test_info* info, const ray& r, real dist) const
    {
        blob_freearea_struct* fp = reinterpret_cast<blob_freearea_struct*>(info->freearea);
        int bsz = fp->n;
        if (bsz)
        {
            vector3 p = info->distance * r.direction() + r.origin();
            vector3 n = get_normal(p, fp->pointers, bsz);
            info->position = p;
            info->geometric = n;
            info->normal = n;
        }
    }
}