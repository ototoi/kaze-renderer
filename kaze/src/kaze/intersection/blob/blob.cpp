#include "blob.h"

#include "values.h"

namespace kaze
{
    static real pow2(real x) { return x * x; }

    real blob::distance(const vector3& p) const
    {
        return sqrt(distance2(p));
    }
    //-----------------------------------------

    point_blob::point_blob(const vector3& c, real r, real d)
        : cnt_(c), rad_(r), d_(d) {}

    real point_blob::radius() const
    {
        return rad_;
    }

    real point_blob::density() const
    {
        return d_;
    }

    vector3 point_blob::center() const
    {
        return cnt_;
    }

    vector3 point_blob::get_near(const vector3& p) const
    {
        return cnt_;
    }

    real point_blob::distance2(const vector3& p) const
    {
        return (p - cnt_).sqr_length();
    }

    vector3 point_blob::min() const
    {
        real R = rad_;
        return vector3(cnt_[0] - R, cnt_[1] - R, cnt_[2] - R);
    }

    vector3 point_blob::max() const
    {
        real R = rad_;
        return vector3(cnt_[0] + R, cnt_[1] + R, cnt_[2] + R);
    }

    void point_blob::bound(vector3& min, vector3& max, const PCBLOB* blobs, int n) const
    {
        real R = rad_;
        for (int i = 0; i < n; i++)
        {
            if (blobs[i] != this)
            {
                real L2 = blobs[i]->distance2(cnt_);
                real D2 = pow2(blobs[i]->radius() + rad_);
                if (L2 <= D2)
                {
                    R += blobs[i]->radius();
                }
            }
        }
        min = vector3(cnt_[0] - R, cnt_[1] - R, cnt_[2] - R);
        max = vector3(cnt_[0] + R, cnt_[1] + R, cnt_[2] + R);
    }

    bool point_blob::test(const ray& r, real dist) const
    {
        vector3 org = r.origin();
        vector3 dir = r.direction();

        real R = this->radius();
        vector3 vO = org - this->center();
        vector3 vD = dir;

        real B = dot(vO, vD);
        real Cx = dot(vO, vO);

        real C = Cx - R * R;

        real D = B * B - C;

        if (D < 0)
            return false;
        else
            return true;
    }
}
