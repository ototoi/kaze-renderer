#ifndef KAZE_FAST_BLOB_INTERECTION_H
#define KAZE_FAST_BLOB_INTERECTION_H

#include "blob_intersection.h"

#include "parameter_map.h"

namespace kaze
{

    class fast_blob_intersection_imp;
    class fast_blob_intersection : public blob_intersection
    {
    public:
        fast_blob_intersection();
        ~fast_blob_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    public:
        void add_point(const vector3& c, real radius);
        void construct();
        void construct(const parameter_map& param);

    private:
        fast_blob_intersection_imp* imp_;
    };
}

#endif