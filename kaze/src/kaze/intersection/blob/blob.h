#ifndef KAZE_BLOB_H
#define KAZE_BLOB_H

#include "types.h"
#include "ray.h"
#include <vector>

namespace kaze
{

    class blob;
    typedef const blob* PCBLOB;

    class blob
    {
    public:
        virtual ~blob() {}
        virtual real radius() const = 0;
        virtual real density() const = 0;
        virtual vector3 center() const = 0;
        virtual vector3 get_near(const vector3& p) const = 0;
        virtual real distance2(const vector3& p) const = 0;
        virtual real distance(const vector3& p) const;
        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;

        virtual void bound(vector3& min, vector3& max, const PCBLOB* blobs, int n) const = 0;

        virtual bool test(const ray& r, real dist) const = 0;
    };

    class point_blob : public blob
    {
    public:
        point_blob(const vector3& c, real r, real d = 4.0);

    public:
        real radius() const;
        real density() const;
        vector3 center() const;
        vector3 get_near(const vector3& p) const;
        real distance2(const vector3& p) const;
        vector3 min() const;
        vector3 max() const;
        void bound(vector3& min, vector3& max, const PCBLOB* blobs, int n) const;
        bool test(const ray& r, real dist) const;

    protected:
        vector3 cnt_;
        real rad_;
        real d_;
    };
}

#endif