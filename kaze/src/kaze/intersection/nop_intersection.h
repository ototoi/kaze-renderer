#ifndef KAZE_NOP_INTERSECTION_H
#define KAZE_NOP_INTERSECTION_H

#include "bounded_intersection.h"


namespace kaze
{

    class nop_intersection : public bounded_intersection
    {
    public:
        nop_intersection(const std::shared_ptr<intersection>& inter) : inter_(inter) {}
        ~nop_intersection() { ; }
        bool test(const ray& r, real dist) const { return false; }
        bool test(test_info* info, const ray& r, real dist) const { return false; }
        void finalize(test_info* info, const ray& r, real dist) const { /**/}
        vector3 min() const { return inter_->min(); }
        vector3 max() const { return inter_->max(); }
    private:
        std::shared_ptr<intersection> inter_;
    };
}

#endif