#ifndef KAZE_MAILBOX_INTERSECTION_H
#define KAZE_MAILBOX_INTERSECTION_H

#include "bounded_intersection.h"


namespace kaze
{

    class mailbox_intersection_imp;

    class mailbox_intersection : public bounded_intersection
    {
    public:
        explicit mailbox_intersection(intersection* inter);
        explicit mailbox_intersection(const std::shared_ptr<intersection>& inter);
        ~mailbox_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        bool test(const ray& r, real dist, ray_id_t rid) const;
        bool test(test_info* info, const ray& r, real dist, ray_id_t rid) const;

        bool is_visited(ray_id_t rid) const;
        bool is_visited(const ray& r) const;

        vector3 min() const;
        vector3 max() const;

    public:
        const intersection* inter() const;

    private:
        mailbox_intersection(const mailbox_intersection& rhs);
        mailbox_intersection& operator=(const mailbox_intersection& rhs);

    private:
        mailbox_intersection_imp* imp_;
    };
}

#endif
