#ifndef KAZE_SHADERED_INTERSECTION_H
#define KAZE_SHADERED_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class shader;

    class shadered_intersection : public bounded_intersection
    {
    public:
        shadered_intersection(intersection* inter, shader* shdr);
        shadered_intersection(const std::shared_ptr<intersection>& inter, const std::shared_ptr<shader>& shdr);

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> inter_;
        std::shared_ptr<shader> shader_;
    };
}

#endif