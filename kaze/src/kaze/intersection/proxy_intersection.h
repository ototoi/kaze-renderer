#ifndef KAZE_PROXY_INTERSECTION_H
#define KAZE_PROXY_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class proxy_intersection : public bounded_intersection
    {
    public:
        proxy_intersection(const intersection* inter) : inter_(inter) {}
        ~proxy_intersection() { /**/}
        bool test(const ray& r, real dist) const { return inter_->test(r, dist); }
        bool test(test_info* info, const ray& r, real dist) const { return inter_->test(info, r, dist); }
        void finalize(test_info* info, const ray& r, real dist) const { inter_->finalize(info, r, dist); }
        vector3 min() const { return inter_->min(); }
        vector3 max() const { return inter_->max(); }
    private:
        const intersection* inter_;
    };
}

#endif