#include "intersection.h"
#include "values.h"

namespace kaze
{

    template <class T>
    struct epsilon_
    {
        static T value() { return T(4) * std::numeric_limits<T>::epsilon(); }
    };

    static inline real min3(real a, real b, real c)
    {
        return (a < b) ? std::min(a, c) : std::min(b, c);
    }

    static inline real max3(real a, real b, real c)
    {
        return (a > b) ? std::max(a, c) : std::max(b, c);
    }

    static inline bool isrange(real tmin, real t, real tmax)
    {
        return (tmin <= t) && (t <= tmax);
    }

    typedef bool (*AABB_FUNC)(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax);

    static bool test_AABB_MMM(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    { //7
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] < min[0]) || (org[1] < min[1]) || (org[2] < min[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * a[1] - dir[1] * b[0] < 0) ||
            (dir[0] * b[1] - dir[1] * a[0] > 0) ||
            (dir[0] * b[2] - dir[2] * a[0] > 0) ||
            (dir[0] * a[2] - dir[2] * b[0] < 0) ||
            (dir[1] * a[2] - dir[2] * b[1] < 0) ||
            (dir[1] * b[2] - dir[2] * a[1] > 0))
        {
            return false;
        }

        // compute the intersection distance
        return isrange(tmin, max3(b[0] * idir[0], b[1] * idir[1], b[2] * idir[2]), tmax);
    }

    static bool test_AABB_MMP(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    { //6
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] < min[0]) || (org[1] < min[1]) || (org[2] > max[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * a[1] - dir[1] * b[0] < 0) ||
            (dir[0] * b[1] - dir[1] * a[0] > 0) ||
            (dir[0] * b[2] - dir[2] * b[0] > 0) ||
            (dir[0] * a[2] - dir[2] * a[0] < 0) ||
            (dir[1] * a[2] - dir[2] * a[1] < 0) ||
            (dir[1] * b[2] - dir[2] * b[1] > 0))
        {
            return false;
        }

        // compute the intersection distance
        return isrange(tmin, max3(b[0] * idir[0], b[1] * idir[1], a[2] * idir[2]), tmax);
    }

    static bool test_AABB_MPM(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    { //5
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] < min[0]) || (org[1] > max[1]) || (org[2] < min[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * a[1] - dir[1] * a[0] < 0) ||
            (dir[0] * b[1] - dir[1] * b[0] > 0) ||
            (dir[0] * b[2] - dir[2] * a[0] > 0) ||
            (dir[0] * a[2] - dir[2] * b[0] < 0) ||
            (dir[1] * b[2] - dir[2] * b[1] < 0) ||
            (dir[1] * a[2] - dir[2] * a[1] > 0))
        {
            return false;
        }

        // compute the intersection distance
        return isrange(tmin, max3(b[0] * idir[0], a[1] * idir[1], b[2] * idir[2]), tmax);
    }

    static bool test_AABB_MPP(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    { //4
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] < min[0]) || (org[1] > max[1]) || (org[2] > max[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * a[1] - dir[1] * a[0] < 0) ||
            (dir[0] * b[1] - dir[1] * b[0] > 0) ||
            (dir[0] * b[2] - dir[2] * b[0] > 0) ||
            (dir[0] * a[2] - dir[2] * a[0] < 0) ||
            (dir[1] * b[2] - dir[2] * a[1] < 0) ||
            (dir[1] * a[2] - dir[2] * b[1] > 0))
        {
            return false;
        }

        // compute the intersection distance
        return isrange(tmin, max3(b[0] * idir[0], a[1] * idir[1], a[2] * idir[2]), tmax);
    }

    static bool test_AABB_PMM(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    { //3
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] > max[0]) || (org[1] < min[1]) || (org[2] < min[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * b[1] - dir[1] * b[0] < 0) ||
            (dir[0] * a[1] - dir[1] * a[0] > 0) ||
            (dir[0] * a[2] - dir[2] * a[0] > 0) ||
            (dir[0] * b[2] - dir[2] * b[0] < 0) ||
            (dir[1] * a[2] - dir[2] * b[1] < 0) ||
            (dir[1] * b[2] - dir[2] * a[1] > 0))
        {
            return false;
        }

        // compute the intersection distance
        return isrange(tmin, max3(a[0] * idir[0], b[1] * idir[1], b[2] * idir[2]), tmax);
    }

    static bool test_AABB_PMP(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    { //2
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] > max[0]) || (org[1] < min[1]) || (org[2] > max[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * b[1] - dir[1] * b[0] < 0) ||
            (dir[0] * a[1] - dir[1] * a[0] > 0) ||
            (dir[0] * a[2] - dir[2] * b[0] > 0) ||
            (dir[0] * b[2] - dir[2] * a[0] < 0) ||
            (dir[1] * a[2] - dir[2] * a[1] < 0) ||
            (dir[1] * b[2] - dir[2] * b[1] > 0))
        {
            return false;
        }

        // compute the intersection distance
        return isrange(tmin, max3(a[0] * idir[0], b[1] * idir[1], a[2] * idir[2]), tmax);
    }

    static bool test_AABB_PPM(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    { //1
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] > max[0]) || (org[1] > max[1]) || (org[2] < min[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * b[1] - dir[1] * a[0] < 0) ||
            (dir[0] * a[1] - dir[1] * b[0] > 0) ||
            (dir[0] * a[2] - dir[2] * a[0] > 0) ||
            (dir[0] * b[2] - dir[2] * b[0] < 0) ||
            (dir[1] * b[2] - dir[2] * b[1] < 0) ||
            (dir[1] * a[2] - dir[2] * a[1] > 0))
        {
            return false;
        }

        // compute the intersection distance
        return isrange(tmin, max3(a[0] * idir[0], a[1] * idir[1], b[2] * idir[2]), tmax);
    }

    static bool test_AABB_PPP(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    { //0
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        if ((org[0] > max[0]) || (org[1] > max[1]) || (org[2] > max[2])) return false;

        vector3 a(min - org);
        vector3 b(max - org);

        if ((dir[0] * b[1] - dir[1] * a[0] < 0) ||
            (dir[0] * a[1] - dir[1] * b[0] > 0) ||
            (dir[0] * a[2] - dir[2] * b[0] > 0) ||
            (dir[0] * b[2] - dir[2] * a[0] < 0) ||
            (dir[1] * b[2] - dir[2] * a[1] < 0) ||
            (dir[1] * a[2] - dir[2] * b[1] > 0))
        {
            return false;
        }

        // compute the intersection distance
        return isrange(tmin, max3(a[0] * idir[0], a[1] * idir[1], a[2] * idir[2]), tmax);
    }

    bool test_AABB_simple(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    {
        int phase = r.phase();
        int sign[3] = {(phase >> 0) & 1, (phase >> 1) & 1, (phase >> 2) & 1};
        vector3 box[2] = {min, max};
        const vector3& org = r.origin();
        const vector3& idir = r.inversed_direction();

        for (int i = 0; i < 3; i++)
        {
            tmin = std::max<real>(tmin, (box[sign[i]][i] - org[i]) * idir[i]);
            tmax = std::min<real>(tmax, (box[1 - sign[i]][i] - org[i]) * idir[i]);
        }
        tmin *= real(1) - epsilon_<real>::value();
        tmax *= real(1) + epsilon_<real>::value();
        return tmin <= tmax;
    }

    bool test_AABB_fast(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    {
        static const real EPSILON = epsilon_<real>::value();

        bool inside = true;
        vector3 maxt(-1, -1, -1);
        vector3 point;

        // find candidate planes.
        for (int i = 0; i < 3; i++)
        {
            if (r.origin()[i] < min[i])
            {
                point[i] = min[i];
                inside = false;
                // calculate t distances to candidate planes
                if (EPSILON < std::abs(r.direction()[i])) maxt[i] = (min[i] - r.origin()[i]) * r.inversed_direction()[i];
            }
            else if (r.origin()[i] > max[i])
            {
                point[i] = max[i];
                inside = false;
                // calculate t distances to candidate planes
                if (EPSILON < std::abs(r.direction()[i])) maxt[i] = (max[i] - r.origin()[i]) * r.inversed_direction()[i];
            }
        }

        if (inside)
        {
            return true;
        }

        // get largest of the maxt's for final choice of intersection
        int whichplane = (maxt[0] < maxt[1]) ? ((maxt[1] < maxt[2]) ? 2 : 1) : ((maxt[0] < maxt[2]) ? 2 : 0);

        real t = maxt[whichplane];
        // check final candidate actually inside box
        if (tmax < t)
        {
            return false;
        }
        if (t < tmin)
        {
            return false;
        }

        switch (whichplane)
        {
        case 0:
            point[1] = r.origin()[1] + t * r.direction()[1];
            if (point[1] < min[1] - EPSILON || point[1] > max[1] + EPSILON) return false;
            point[2] = r.origin()[2] + t * r.direction()[2];
            if (point[2] < min[2] - EPSILON || point[2] > max[2] + EPSILON) return false;

            break;
        case 1:
            point[0] = r.origin()[0] + t * r.direction()[0];
            if (point[0] < min[0] - EPSILON || point[0] > max[0] + EPSILON) return false;
            point[2] = r.origin()[2] + t * r.direction()[2];
            if (point[2] < min[2] - EPSILON || point[2] > max[2] + EPSILON) return false;

            break;
        case 2:
            point[1] = r.origin()[1] + t * r.direction()[1];
            if (point[1] < min[1] - EPSILON || point[1] > max[1] + EPSILON) return false;
            point[0] = r.origin()[0] + t * r.direction()[0];
            if (point[0] < min[0] - EPSILON || point[0] > max[0] + EPSILON) return false;

            break;
        }

        return true; // ray hits box
    }

    bool test_AABB_pluecker(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    {
        static const AABB_FUNC funcs[] = {
            test_AABB_PPP, test_AABB_MPP, test_AABB_PMP, test_AABB_MMP, //0,1,2,3
            test_AABB_PPM, test_AABB_MPM, test_AABB_PMM, test_AABB_MMM, //4,5,6,7
        };
        return funcs[r.phase()](min, max, r, tmin, tmax);
    }

    bool test_AABB(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    {
        //return test_AABB_fast(min,max,r,tmax);
        return test_AABB_pluecker(min, max, r, tmin, tmax);
    }

    bool test_AABB(vector3* out_point, const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    {
        static const real EPSILON = values::epsilon() * 1000;

        vector3 point;
        bool inside = true;
        vector3 maxt(-1, -1, -1);

        // find candidate planes.
        for (int i = 0; i < 3; i++)
        {
            if (r.origin()[i] < min[i])
            {
                point[i] = min[i];
                inside = false;
                // calculate t distances to candidate planes
                if (EPSILON < std::abs(r.direction()[i])) maxt[i] = (min[i] - r.origin()[i]) * r.inversed_direction()[i];
            }
            else if (r.origin()[i] > max[i])
            {
                point[i] = max[i];
                inside = false;
                // calculate t distances to candidate planes
                if (EPSILON < std::abs(r.direction()[i])) maxt[i] = (max[i] - r.origin()[i]) * r.inversed_direction()[i];
            }
        }

        if (inside)
        {
            //*out_dist = 0;
            *out_point = r.origin();
            return true;
        }

        // get largest of the maxt's for final choice of intersection
        int whichplane = (maxt[0] < maxt[1]) ? ((maxt[1] < maxt[2]) ? 2 : 1) : ((maxt[0] < maxt[2]) ? 2 : 0);

        real t = maxt[whichplane];
        // check final candidate actually inside box
        if (tmax < t)
        {
            return false;
        }
        if (t < tmin)
        {
            return false;
        }

        switch (whichplane)
        {
        case 0:
            point[1] = r.origin()[1] + t * r.direction()[1];
            if (point[1] < min[1] - EPSILON || point[1] > max[1] + EPSILON) return false;
            point[2] = r.origin()[2] + t * r.direction()[2];
            if (point[2] < min[2] - EPSILON || point[2] > max[2] + EPSILON) return false;

            break;
        case 1:
            point[0] = r.origin()[0] + t * r.direction()[0];
            if (point[0] < min[0] - EPSILON || point[0] > max[0] + EPSILON) return false;
            point[2] = r.origin()[2] + t * r.direction()[2];
            if (point[2] < min[2] - EPSILON || point[2] > max[2] + EPSILON) return false;

            break;
        case 2:
            point[1] = r.origin()[1] + t * r.direction()[1];
            if (point[1] < min[1] - EPSILON || point[1] > max[1] + EPSILON) return false;
            point[0] = r.origin()[0] + t * r.direction()[0];
            if (point[0] < min[0] - EPSILON || point[0] > max[0] + EPSILON) return false;

            break;
        }

        //*out_dist = t;
        *out_point = point;

        return true; // ray hits box
    }

    bool test_AABB(real* out_dist, const vector3& min, const vector3& max, const ray& r, real tmin, real tmax)
    {
        static const real EPSILON = values::epsilon() * 1000;

        vector3 point;
        bool inside = true;
        vector3 maxt(-1, -1, -1);

        // find candidate planes.
        for (int i = 0; i < 3; i++)
        {
            if (r.origin()[i] < min[i])
            {
                point[i] = min[i];
                inside = false;
                // calculate t distances to candidate planes
                if (EPSILON < std::abs(r.direction()[i])) maxt[i] = (min[i] - r.origin()[i]) * r.inversed_direction()[i];
            }
            else if (r.origin()[i] > max[i])
            {
                point[i] = max[i];
                inside = false;
                // calculate t distances to candidate planes
                if (EPSILON < std::abs(r.direction()[i])) maxt[i] = (max[i] - r.origin()[i]) * r.inversed_direction()[i];
            }
        }

        if (inside)
        {
            *out_dist = 0;
            //*out_point = r.origin();
            return true;
        }

        // get largest of the maxt's for final choice of intersection
        int whichplane = (maxt[0] < maxt[1]) ? ((maxt[1] < maxt[2]) ? 2 : 1) : ((maxt[0] < maxt[2]) ? 2 : 0);

        real t = maxt[whichplane];
        // check final candidate actually inside box
        if (tmax < t)
        {
            return false;
        }
        if (t < tmin)
        {
            return false;
        }

        switch (whichplane)
        {
        case 0:
            point[1] = r.origin()[1] + t * r.direction()[1];
            if (point[1] < min[1] - EPSILON || point[1] > max[1] + EPSILON) return false;
            point[2] = r.origin()[2] + t * r.direction()[2];
            if (point[2] < min[2] - EPSILON || point[2] > max[2] + EPSILON) return false;

            break;
        case 1:
            point[0] = r.origin()[0] + t * r.direction()[0];
            if (point[0] < min[0] - EPSILON || point[0] > max[0] + EPSILON) return false;
            point[2] = r.origin()[2] + t * r.direction()[2];
            if (point[2] < min[2] - EPSILON || point[2] > max[2] + EPSILON) return false;

            break;
        case 2:
            point[1] = r.origin()[1] + t * r.direction()[1];
            if (point[1] < min[1] - EPSILON || point[1] > max[1] + EPSILON) return false;
            point[0] = r.origin()[0] + t * r.direction()[0];
            if (point[0] < min[0] - EPSILON || point[0] > max[0] + EPSILON) return false;

            break;
        }

        *out_dist = t;
        //*out_point = point;

        return true; // ray hits box
    }

    namespace
    {
        inline bool is_ZERO(const real v[3])
        {
            static real ZERO[3] = {0, 0, 0};
            return (memcmp(v, ZERO, sizeof(real) * 3) == 0);
        }
    }

    bool is_bounded(const intersection& inter)
    {
        vector3 min = inter.min();
        vector3 max = inter.max();
        if (is_ZERO((const real*)(&min)) || is_ZERO((const real*)(&max)))
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
