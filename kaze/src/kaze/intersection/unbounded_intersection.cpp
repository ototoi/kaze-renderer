#include "unbounded_intersection.h"

namespace kaze
{

    vector3 unbounded_intersection::min() const
    {
        return vector3(0, 0, 0);
    }

    vector3 unbounded_intersection::max() const
    {
        return vector3(0, 0, 0);
    }
};