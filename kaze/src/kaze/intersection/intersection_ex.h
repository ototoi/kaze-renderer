#ifndef KAZE_INTERSECTION_EX_H
#define KAZE_INTERSECTION_EX_H

#include "types.h"
#include "ray.h"

#include "intersection.h"

namespace kaze
{

    struct range_AABB
    {
        real tmin;
        real tmax;
    };

    bool test_AABB(range_AABB* rng, const vector3& min, const vector3& max, const ray& r, real dist);
    bool test_AABB(range_AABB* rng, const vector3& min, const vector3& max, const ray& r, real tmin, real tmax);

    struct distbound_AABB
    {
        vector3 tmin;
        vector3 tmax;
    };

    bool test_AABB(distbound_AABB* bnd, const vector3& min, const vector3& max, const ray& r, real dist);

    bool test_01cube(const vector3& org, const vector3& dir, real dist);

    void get_plane_distance(range_AABB* rng, const vector3& min, const vector3& max, const vector3& org, const vector3& dir);
    void get_distance(range_AABB* rng, const vector3& min, const vector3& max, const ray& r);
}

#endif
