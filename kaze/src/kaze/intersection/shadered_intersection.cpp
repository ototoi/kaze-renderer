#include "shadered_intersection.h"

#include "shader.h"
#include "test_info.h"

namespace kaze
{

    shadered_intersection::shadered_intersection(intersection* inter, shader* shdr)
        : inter_(inter), shader_(shdr)
    {
        //--------------------------
    }

    shadered_intersection::shadered_intersection(const std::shared_ptr<intersection>& inter, const std::shared_ptr<shader>& shdr)
        : inter_(inter), shader_(shdr)
    {
        //
    }

    bool shadered_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool shadered_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (inter_->test(info, r, dist))
        {
            info->p_shader = shader_.get();
            return true;
        }

        return false;
    }

    void shadered_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        //no implement
        assert(0);
        //inter_->finalize(info,r,dist);
    }

    vector3 shadered_intersection::min() const { return inter_->min(); }
    vector3 shadered_intersection::max() const { return inter_->max(); }
}
