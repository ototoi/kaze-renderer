#include "teapot_intersection.h"
#include "transformer.h"

#include "test_info.h"

#include "bezier.h"
#include "patch/teapot_patch_loader.h"
#include "patch/bezier_patch_saver.h"
#include "patch/convert_bezier_patch.h"

#include "patch/bezier_patch_intersection.h"

#include "composite/bvh_composite_intersection.h"

#include <memory>
#include <vector>
#include <cassert>

#define PREDIVIDE 0
namespace kaze
{
    namespace
    {
        class t_patch_saver : public bezier_patch_saver
        {
        public:
            bool set_patches(size_t sz)
            {
                mv_.reserve(sz);
                return true;
            }
            bool set_patch(size_t i, io_bezier_patch* f)
            {
                multi_bezier_patch<vector3> patch;
                if (!convert_bezier_patch(patch, *f)) return false;
                int nPu = patch.get_patch_size_u();
                int nPv = patch.get_patch_size_v();
                for (int j = 0; j < nPv; j++)
                {
                    for (int i = 0; i < nPu; i++)
                    {
                        mv_.push_back(patch.get_patch_at(i, j));
                    }
                }
                return true;
            }

        public:
            std::vector<bezier_patch<vector3> > mv_;
        };

        void Transform(const transformer& tr, bezier_patch<vector3>& patch)
        {
            int nu = patch.get_nu();
            int nv = patch.get_nv();

            const std::vector<vector3>& cp = patch.get_cp();
            std::vector<vector3> tmp(cp.size());
            for (size_t i = 0; i < cp.size(); i++)
            {
                tmp[i] = tr.transform_p(cp[i]);
            }
            patch.set_cp(nu, nv, tmp);
        }

        static void PushPatch(std::vector<bezier_patch<vector3> >& out, const bezier_patch<vector3>& patch, int level)
        {
            if (level == 0)
            {
                out.push_back(patch);
            }
            else
            {
                bezier_patch<vector3> bz[4];
                patch.split(bz, 0.5, 0.5);
                for (int i = 0; i < 4; i++)
                {
                    PushPatch(out, bz[i], level - 1);
                }
            }
        }
    }

    class teapot_intersection_imp
    {
    public:
        teapot_intersection_imp(const transformer& tr);
        ~teapot_intersection_imp();
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    private:
        bvh_composite_intersection cmp_;
    };

    teapot_intersection_imp::teapot_intersection_imp(const transformer& tr)
    {
        std::vector<bezier_patch<vector3> > tmp;
        {
            t_patch_saver tps;
            teapot_patch_loader loader;
            if (!loader.load(&tps)) throw "teapot_intersection";
            tmp.swap(tps.mv_);
        }

        size_t sz = tmp.size();
        for (size_t i = 0; i < sz; i++)
        {
            (void)Transform(tr, tmp[i]);
        }

        std::vector<bezier_patch<vector3> > tmp2;
        for (size_t i = 0; i < sz; i++)
        {
            PushPatch(tmp2, tmp[i], PREDIVIDE);
        }

        for (size_t i = 0; i < tmp2.size(); i++)
        {
            cmp_.add(std::shared_ptr<intersection>(new bezier_patch_intersection(tmp2[i])));
        }
        cmp_.construct();
    }

    teapot_intersection_imp::~teapot_intersection_imp()
    {
        ;
    }

    bool teapot_intersection_imp::test(const ray& r, real dist) const
    {
        return cmp_.test(r, dist);
    }

    bool teapot_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        return cmp_.test(info, r, dist);
    }

    void teapot_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        cmp_.finalize(info, r, dist);
    }

    vector3 teapot_intersection_imp::min() const
    {
        return cmp_.min();
    }

    vector3 teapot_intersection_imp::max() const
    {
        return cmp_.max();
    }

    //--------------------------------------------------------------

    teapot_intersection::teapot_intersection(const transformer& tr)
    {
        imp_ = new teapot_intersection_imp(tr);
    }

    teapot_intersection::~teapot_intersection()
    {
        delete imp_;
    }

    bool teapot_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool teapot_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            //info->p_intersection = this;
            return true;
        }
        return false;
    }

    void teapot_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        //imp_->finalize(info, r, dist);
    }

    vector3 teapot_intersection::min() const
    {
        return imp_->min();
    }

    vector3 teapot_intersection::max() const
    {
        return imp_->max();
    }
}
