#ifndef KAZE_PLQBVH_POINTS_ACCELERATOR_H
#define KAZE_PLQBVH_POINTS_ACCELERATOR_H

#include "points_accelerator.h"
#include "points_segment.h"

#include <vector>

namespace kaze
{

    class plqbvh_points_accelerator_imp;
    class plqbvh_points_accelerator : public points_accelerator
    {
    public:
        plqbvh_points_accelerator(const std::vector<points_segment>& segs);
        ~plqbvh_points_accelerator();

    public:
        bool test(const ray& r, real tmin, real tmax) const;
        bool test(point_test_info* info, const ray& r, real tmin, real tmax) const;
        vector3 min() const;
        vector3 max() const;

    private:
        plqbvh_points_accelerator_imp* imp_;
    };
}

#endif
