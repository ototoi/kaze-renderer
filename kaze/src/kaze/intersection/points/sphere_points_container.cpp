#include "sphere_points_container.h"
#include "point_test_info.h"
#include <vector>

namespace kaze
{

    static vector3 Convert(const vector3f& p)
    {
        return vector3(p[0], p[1], p[2]);
    }

    static int solve2e(real root[], real B, real C)
    {
        real D = B * B - C;
        if (D < 0)
        {
            return 0;
        }
        else if (D == 0)
        {
            double x = -B;
            root[0] = x;
            return 1;
        }
        else
        {
            real x1 = (fabs(B) + sqrt(D));
            if (B >= 0.0)
            {
                x1 = -x1;
            }
            real x2 = C / x1;
            if (x1 > x2) std::swap(x1, x2);
            root[0] = x1;
            root[1] = x2;
            return 2;
        }
    }

    static bool test_sphere(const vector3& P, const real rad, point_test_info* info, const ray& r, real tmin, real tmax)
    {
        vector3 center = P;
        vector3 rs = r.origin() - center;
        real radius = rad;

        real B = dot(rs, r.direction());
        real C = dot(rs, rs) - radius * radius;

        real root[2];
        int nRet = solve2e(root, B, C);
        if (nRet)
        {
            //for(int i=0;i<nRet;i++)
            {
                real t = root[0];
                if ((t > tmin) && (t < tmax))
                {
                    if (info)
                    {
                        info->t = t;
                    }
                    return true;
                }
            }
        }
        return false;
    }

    sphere_points_container::sphere_points_container(
        const std::vector<vector3f>& P,
        const std::vector<float>& R,
        const std::vector<vector3f>& C1,
        const std::vector<vector3f>& C2) : P_(P), R_(R), C1_(C1), C2_(C2)
    {
        ;
    }

    sphere_points_container::~sphere_points_container()
    {
        ;
    }

    size_t sphere_points_container::segments() const
    {
        return P_.size();
    }

    bool sphere_points_container::test(size_t i, const ray& r, real tmin, real tmax) const
    {
        vector3 P = Convert(P_[i]);
        real R = (real)R_[i];
        return test_sphere(P, R, NULL, r, tmin, tmax);
    }

    bool sphere_points_container::test(size_t i, point_test_info* info, const ray& r, real tmin, real tmax) const
    {
        vector3 P = Convert(P_[i]);
        real R = (real)R_[i];
        if (test_sphere(P, R, info, r, tmin, tmax))
        {
            info->u = 0;
            info->v = 0;
            info->p_container = this;
            info->nseg = i;
            return true;
        }
        return false;
    }

    void sphere_points_container::finalize(size_t i, point_test_info* info, const ray& r, real tmin, real tmax) const
    {
        vector3 P = Convert(P_[i]);
        real R = (real)R_[i];
        {
            vector3 C1 = Convert(C1_[i]);
            vector3 C2 = Convert(C2_[i]);

            real t = info->t;

            info->position = r.origin() + t * r.direction();
            vector3 N = normalize(info->position - P);
            info->normal = N;
            info->geometric = N;
            info->col1 = C1;
            info->col2 = C2;
            info->index = i;
        }
    }

    vector3 sphere_points_container::min(size_t i) const
    {
        vector3 P = Convert(P_[i]);
        real R = (real)R_[i] * 1.00001;
        return P - vector3(R, R, R);
    }

    vector3 sphere_points_container::max(size_t i) const
    {
        vector3 P = Convert(P_[i]);
        real R = (real)R_[i] * 1.00001;
        return P + vector3(R, R, R);
    }
}
