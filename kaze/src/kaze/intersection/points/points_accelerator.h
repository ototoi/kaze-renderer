#ifndef KAZE_POINTS_ACCELERATOR_H
#define KAZE_POINTS_ACCELERATOR_H

#include "point_test_info.h"
#include "ray.h"
#include <vector>

namespace kaze
{

    class points_accelerator
    {
    public:
        virtual ~points_accelerator() {}
    public:
        virtual bool test(const ray& r, real tmin, real tmax) const = 0;
        virtual bool test(point_test_info* info, const ray& r, real tmin, real tmax) const = 0;
        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;
    };
}

#endif
