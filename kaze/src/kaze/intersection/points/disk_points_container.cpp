#include "disk_points_container.h"
#include "point_test_info.h"
#include <vector>

namespace kaze
{

    static vector3 Convert(const vector3f& p)
    {
        return vector3(p[0], p[1], p[2]);
    }

    static int solve2e(real root[], real B, real C)
    {
        real D = B * B - C;
        if (D < 0)
        {
            return 0;
        }
        else if (D == 0)
        {
            double x = -B;
            root[0] = x;
            return 1;
        }
        else
        {
            real x1 = (fabs(B) + sqrt(D));
            if (B >= 0.0)
            {
                x1 = -x1;
            }
            real x2 = C / x1;
            if (x1 > x2) std::swap(x1, x2);
            root[0] = x1;
            root[1] = x2;
            return 2;
        }
    }

    static bool test_disk(const vector3& P, real R, const vector3& N, point_test_info* info, const ray& r, real tmin, real tmax)
    {
        vector3 P0 = r.origin();
        vector3 V = r.direction();
        real VN = dot(V, N);
        if (fabs(VN) <= std::numeric_limits<real>::min()) return false;
        real d = -dot(P, N);
        real t = -(dot(P0, N) + d) / VN;
        if (t <= tmin || tmax <= t) return false;
        vector3 p = r.origin() + t * r.direction();

        vector3 dif = (P - p);
        real d2 = dot(dif, dif);
        if (d2 >= R * R) return false;
        if (info)
        {
            info->t = t;
            info->position = p;
            info->u = 0;
            info->v = sqrt(d2 / (R * R));
            info->normal = N;
        }
        return true;
    }

    disk_points_container::disk_points_container(
        const std::vector<vector3f>& P,
        const std::vector<float>& R,
        const std::vector<vector3f>& C1,
        const std::vector<vector3f>& C2) : P_(P), R_(R), C1_(C1), C2_(C2)
    {
        ;
    }

    disk_points_container::~disk_points_container()
    {
        ;
    }

    size_t disk_points_container::segments() const
    {
        return P_.size();
    }

    bool disk_points_container::test(size_t i, const ray& r, real tmin, real tmax) const
    {
        vector3 P = Convert(P_[i]);
        real R = (real)R_[i];
        vector3 N = normalize(r.origin() - P);
        return test_disk(P, R, N, NULL, r, tmin, tmax);
    }

    bool disk_points_container::test(size_t i, point_test_info* info, const ray& r, real tmin, real tmax) const
    {
        vector3 P = Convert(P_[i]);
        real R = (real)R_[i];
        vector3 N = normalize(r.origin() - P);
        if (test_disk(P, R, N, info, r, tmin, tmax))
        {
            //info->u = 0;
            //info->v = 0;
            info->p_container = this;
            info->nseg = i;
            return true;
        }
        return false;
    }

    void disk_points_container::finalize(size_t i, point_test_info* info, const ray& r, real tmin, real tmax) const
    {
        //vector3 P = Convert(P_[i]);
        //real    R = (real)R_[i];
        //vector3 N = normalize(r.origin()-P);
        {
            vector3 C1 = Convert(C1_[i]);
            vector3 C2 = Convert(C2_[i]);

            //real t = info->t;
            //info->position = r.origin() + t * r.direction();
            vector3 N = normalize(info->normal);
            info->normal = N;
            info->geometric = N;
            info->col1 = C1;
            info->col2 = C2;
            info->index = i;
        }
    }

    vector3 disk_points_container::min(size_t i) const
    {
        vector3 P = Convert(P_[i]);
        real R = (real)R_[i] * 1.00001;
        return P - vector3(R, R, R);
    }

    vector3 disk_points_container::max(size_t i) const
    {
        vector3 P = Convert(P_[i]);
        real R = (real)R_[i] * 1.00001;
        return P + vector3(R, R, R);
    }

    //-------------------------------------------------------------------------

    normal_disk_points_container::normal_disk_points_container(
        const std::vector<vector3f>& P,
        const std::vector<float>& R,
        const std::vector<vector3f>& N,
        const std::vector<vector3f>& C1,
        const std::vector<vector3f>& C2) : disk_points_container(P, R, C1, C2), N_(N)
    {
        ;
    }

    bool normal_disk_points_container::test(size_t i, const ray& r, real tmin, real tmax) const
    {
        vector3 P = Convert(P_[i]);
        real R = (real)R_[i];
        vector3 N = Convert((N_.size() == 1) ? N_[0] : N_[i]);
        return test_disk(P, R, N, NULL, r, tmin, tmax);
    }

    bool normal_disk_points_container::test(size_t i, point_test_info* info, const ray& r, real tmin, real tmax) const
    {
        vector3 P = Convert(P_[i]);
        real R = (real)R_[i];
        vector3 N = Convert((N_.size() == 1) ? N_[0] : N_[i]);
        if (test_disk(P, R, N, info, r, tmin, tmax))
        {
            //info->u = 0;
            //info->v = 0;
            info->p_container = this;
            info->nseg = i;
            return true;
        }
        return false;
    }

    void normal_disk_points_container::finalize(size_t i, point_test_info* info, const ray& r, real tmin, real tmax) const
    {
        disk_points_container::finalize(i, info, r, tmin, tmax);
    }
    vector3 normal_disk_points_container::min(size_t i) const
    {
        return disk_points_container::min(i);
    }
    vector3 normal_disk_points_container::max(size_t i) const
    {
        return disk_points_container::max(i);
    }
}
