#ifndef KAZE_POINTS_SEGMENT_H
#define KAZE_POINTS_SEGMENT_H

#include "points_container.h"

namespace kaze
{

    class points_container;

    struct points_segment
    {
        const points_container* p_container;
        size_t index;
    };
}

#endif
