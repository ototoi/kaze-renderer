#ifndef KAZE_POINT_TEST_INFO_H
#define KAZE_POINT_TEST_INFO_H

#include "types.h"

namespace kaze
{

    class points_container;

    struct point_test_info
    {
        real t;
        real u;
        real v;
        vector3 position;
        vector3 normal;
        vector3 geometric;
        vector3 tangent;
        vector3 binormal;
        vector3 col1;
        vector3 col2;
        const points_container* p_container;
        size_t nseg;
        size_t index;
    };
}

#endif