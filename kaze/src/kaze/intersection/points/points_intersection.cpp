#include "points_intersection.h"
#include "test_info.h"

#include "plqbvh_points_accelerator.h"

#include <vector>
#include <memory>

namespace kaze
{

    class points_intersection_imp
    {
    public:
        points_intersection_imp(const std::shared_ptr<points_container>& pc)
            : pc_(pc)
        {
            size_t sz = pc->segments();
            std::vector<points_segment> segs(sz);
            for (size_t i = 0; i < sz; i++)
            {
                segs[i].p_container = pc.get();
                segs[i].index = i;
            }
            std::unique_ptr<points_accelerator> acc(new plqbvh_points_accelerator(segs));
            root_ = acc.release();
        }

        ~points_intersection_imp()
        {
            delete root_;
        }

    public:
        bool test(const ray& r, real dist) const
        {
            return root_->test(r, 0, dist);
        }
        bool test(test_info* info, const ray& r, real dist) const
        {
            point_test_info cinfo;
            if (root_->test(&cinfo, r, 0, dist))
            {
                if (cinfo.p_container)
                {
                    cinfo.p_container->finalize(cinfo.nseg, &cinfo, r, 0, cinfo.t);

                    vector3 N = vector3(cinfo.normal);
                    vector3 U = vector3(cinfo.tangent);
                    vector3 V = vector3(cinfo.binormal);
                    vector3 G = vector3(cinfo.geometric);
                    info->distance = cinfo.t;
                    info->position = vector3(cinfo.position);
                    info->normal = N;
                    info->geometric = G;
                    info->tangent = U;
                    info->binormal = V;
                    info->coord = vector3(cinfo.u, cinfo.v, 0);
                    info->col1 = vector3(cinfo.col1);
                    info->col2 = vector3(cinfo.col2);
                    info->index = cinfo.index;
                    return true;
                }
            }
            return false;
        }

        vector3 min() const { return root_->min(); }
        vector3 max() const { return root_->max(); }
    private:
        points_accelerator* root_;
        std::shared_ptr<points_container> pc_;
    };

    points_intersection::points_intersection(points_container* pc)
    {
        std::shared_ptr<points_container> ppc(pc);
        imp_ = new points_intersection_imp(ppc);
    }

    points_intersection::points_intersection(const std::shared_ptr<points_container>& pc)
    {
        imp_ = new points_intersection_imp(pc);
    }

    points_intersection::~points_intersection()
    {
        delete imp_;
    }

    bool points_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool points_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void points_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }

    vector3 points_intersection::min() const
    {
        return imp_->min();
    }
    vector3 points_intersection::max() const
    {
        return imp_->max();
    }
}
