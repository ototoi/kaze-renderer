#ifndef KAZE_POINTS_CONTAINER_H
#define KAZE_POINTS_CONTAINER_H

#include "types.h"
#include "ray.h"

namespace kaze
{

    struct point_test_info;

    class points_container
    {
    public:
        virtual ~points_container() {}
        virtual size_t segments() const = 0;
        virtual bool test(size_t nseg, const ray& r, real tmin, real tmax) const = 0;
        virtual bool test(size_t nseg, point_test_info* info, const ray& r, real tmin, real tmax) const = 0;
        virtual void finalize(size_t nseg, point_test_info* info, const ray& r, real tmin, real tmax) const = 0;
        virtual vector3 min(size_t nseg) const = 0;
        virtual vector3 max(size_t nseg) const = 0;
    };
}

#endif
