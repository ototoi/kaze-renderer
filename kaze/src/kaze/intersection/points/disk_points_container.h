#ifndef KAZE_DISK_POINTS_CONTAINER_H
#define KAZE_DISK_POINTS_CONTAINER_H

#include "points_container.h"
#include <vector>

namespace kaze
{

    class disk_points_container : public points_container
    {
    public:
        disk_points_container(
            const std::vector<vector3f>& P,
            const std::vector<float>& R,
            const std::vector<vector3f>& C1,
            const std::vector<vector3f>& C2);
        ~disk_points_container();
        size_t segments() const;
        bool test(size_t i, const ray& r, real tmin, real tmax) const;
        bool test(size_t i, point_test_info* info, const ray& r, real tmin, real tmax) const;
        void finalize(size_t i, point_test_info* info, const ray& r, real tmin, real tmax) const;
        vector3 min(size_t i) const;
        vector3 max(size_t i) const;

    protected:
        std::vector<vector3f> P_;
        std::vector<float> R_;
        std::vector<vector3f> C1_;
        std::vector<vector3f> C2_;
    };

    class normal_disk_points_container : public disk_points_container
    {
    public:
        normal_disk_points_container(
            const std::vector<vector3f>& P,
            const std::vector<float>& R,
            const std::vector<vector3f>& N,
            const std::vector<vector3f>& C1,
            const std::vector<vector3f>& C2);

    public:
        bool test(size_t i, const ray& r, real tmin, real tmax) const;
        bool test(size_t i, point_test_info* info, const ray& r, real tmin, real tmax) const;
        void finalize(size_t i, point_test_info* info, const ray& r, real tmin, real tmax) const;
        vector3 min(size_t i) const;
        vector3 max(size_t i) const;

    protected:
        std::vector<vector3f> N_;
    };
}

#endif
