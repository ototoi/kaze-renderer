#ifndef KAZE_POINTS_INTERSECTION_H
#define KAZE_POINTS_INTERSECTION_H

#include "bounded_intersection.h"
#include "points_container.h"

namespace kaze
{

    class points_intersection_imp;

    class points_intersection : public bounded_intersection
    {
    public:
        points_intersection(points_container* pc);
        points_intersection(const std::shared_ptr<points_container>& pc);
        ~points_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    private:
        points_intersection_imp* imp_;
    };
}

#endif
