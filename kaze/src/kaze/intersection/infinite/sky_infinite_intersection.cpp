#include "sky_infinite_intersection.h"
#include "values.h"

#include "test_info.h"

namespace kaze
{

    class shader;

    static shader* g_pSky = NULL; //DUMMY

    sky_infinite_intersection::sky_infinite_intersection(const std::shared_ptr<intersection>& inter)
        : infinite_intersection(inter) {}

    void sky_infinite_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        vector3 upper = vector3(0, 1, 0);

        vector3 p = (this->min() + this->max());
        vector3 d = r.direction();
        vector3 n = -d;
        vector3 t = cross(d, upper);
        vector3 b = cross(n, t);

        info->position = p;
        info->normal = n;
        info->geometric = n;
        info->tangent = t;
        info->binormal = b;
        info->p_shader = g_pSky;
    }

    bool sky_infinite_intersection::test_internal(test_info* info, const ray& r, real dist) const
    {
        info->distance = values::infinity();
        info->p_intersection = this;
        return true;
    }
}
