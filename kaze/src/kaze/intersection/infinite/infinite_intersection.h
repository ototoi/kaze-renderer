#ifndef KAZE_INFINITE_INTERSECTION_H
#define KAZE_INFINITE_INTERSECTION_H

#include "bounded_intersection.h"


namespace kaze
{

    class infinite_intersection : public bounded_intersection
    {
    public:
        infinite_intersection(const std::shared_ptr<intersection>& inter)
            : inter_(inter) {}

        bool test(const ray& r, real dist) const { return false; }
        bool test(test_info* info, const ray& r, real dist) const
        {
            if (inter_->test(info, r, dist)) return true;
            return test_internal(info, r, dist);
        }

        vector3 min() const { return inter_->min(); }
        vector3 max() const { return inter_->max(); }
    public:
        virtual void finalize(test_info* info, const ray& r, real dist) const = 0;

    protected:
        virtual bool test_internal(test_info* info, const ray& r, real dist) const = 0;

    protected:
        std::shared_ptr<intersection> inter_;
    };
}

#endif