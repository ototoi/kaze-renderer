#ifndef KAZE_SKY_INFINITE_INTERSECTION_H
#define KAZE_SKY_INFINITE_INTERSECTION_H

#include "infinite_intersection.h"

namespace kaze
{

    class sky_infinite_intersection : public infinite_intersection
    {
    public:
        sky_infinite_intersection(const std::shared_ptr<intersection>& inter);

    public:
        void finalize(test_info* info, const ray& r, real dist) const;

    protected:
        bool test_internal(test_info* info, const ray& r, real dist) const;
    };
}

#endif