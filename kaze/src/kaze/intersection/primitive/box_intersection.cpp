#include "box_intersection.h"
#include "test_info.h"

#include <cmath>

#define KAZE_BOX_PLUCKER

namespace kaze
{

    //-----------------------------------
    //BOX
    //-----------------------------------
    struct box_freearea_struct
    {
        int plane;
    };

    static bool check_minmax(const vector3& min, const vector3& max)
    {
        for (int i = 0; i < 3; i++)
        {
            if (min[i] > max[i]) return false;
        }
        return true;
    }

    box_intersection::box_intersection(const vector3& min, const vector3& max)
    {
        assert(check_minmax(min, max));
        min_ = min;
        max_ = max;
    }

#ifndef KAZE_BOX_PLUCKER
    bool box_intersection::test(const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1000 * 100;

        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        bool inside = true;
        vector3 max_t(-1, -1, -1);
        vector3 point;
        real t;

        // find candidate planes.
        for (int i = 0; i < 3; i++)
        {
            if (org[i] < min_[i])
            {
                point[i] = min_[i];
                inside = false;
                // calculate t distances to candidate planes
                if (EPSILON < std::abs(dir[i])) max_t[i] = (point[i] - org[i]) * idir[i];
            }
            else if (org[i] > max_[i])
            {
                point[i] = max_[i];
                inside = false;
                // calculate t distances to candidate planes
                if (EPSILON < std::abs(dir[i])) max_t[i] = (point[i] - org[i]) * idir[i];
            }
        }

        if (inside)
        {
            for (int i = 0; i < 3; i++)
            {
                if (dir[i] < -EPSILON)
                {
                    point[i] = min_[i];
                    max_t[i] = (point[i] - org[i]) * idir[i];
                }
                else if (EPSILON < dir[i])
                {
                    point[i] = max_[i];
                    max_t[i] = (point[i] - org[i]) * idir[i];
                }
            }
        }

        // get largest of the max_t's for final choice of intersection
        int whichplane = (max_t[0] < max_t[1]) ? ((max_t[1] < max_t[2]) ? 2 : 1) : ((max_t[0] < max_t[2]) ? 2 : 0);

        t = max_t[whichplane];
        // check final candidate actually inside box_intersection
        if (dist <= t)
        {
            return false;
        }
        if (t < EPSILON)
        {
            return false;
        }

        switch (whichplane)
        {
        case 0:
            point[1] = r.origin()[1] + t * r.direction()[1];
            if (point[1] < min_[1] || point[1] > max_[1]) return false;
            point[2] = r.origin()[2] + t * r.direction()[2];
            if (point[2] < min_[2] || point[2] > max_[2]) return false;

            break;
        case 1:
            point[0] = r.origin()[0] + t * r.direction()[0];
            if (point[0] < min_[0] || point[0] > max_[0]) return false;
            point[2] = r.origin()[2] + t * r.direction()[2];
            if (point[2] < min_[2] || point[2] > max_[2]) return false;

            break;
        case 2:
            point[1] = r.origin()[1] + t * r.direction()[1];
            if (point[1] < min_[1] || point[1] > max_[1]) return false;
            point[0] = r.origin()[0] + t * r.direction()[0];
            if (point[0] < min_[0] || point[0] > max_[0]) return false;

            break;
        }

        //info->distance = (t);
        //(reinterpret_cast<box_freearea_struct*>(info->freearea))->plane = whichplane;
        //info->p_intersection= this;

        return true; // ray hits box_intersection
    }

    bool box_intersection::test(test_info* info, const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1000 * 100;

        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        bool inside = true;
        vector3 max_t(-1, -1, -1);
        vector3 point;
        real t;

        // find candidate planes.
        for (int i = 0; i < 3; i++)
        {
            if (org[i] < min_[i])
            {
                point[i] = min_[i];
                inside = false;
                // calculate t distances to candidate planes
                if (EPSILON < std::abs(dir[i])) max_t[i] = (point[i] - org[i]) * idir[i];
            }
            else if (org[i] > max_[i])
            {
                point[i] = max_[i];
                inside = false;
                // calculate t distances to candidate planes
                if (EPSILON < std::abs(dir[i])) max_t[i] = (point[i] - org[i]) * idir[i];
            }
        }

        if (inside)
        {
            for (int i = 0; i < 3; i++)
            {
                if (dir[i] < -EPSILON)
                {
                    point[i] = min_[i];
                    max_t[i] = (point[i] - org[i]) * idir[i];
                }
                else if (EPSILON < dir[i])
                {
                    point[i] = max_[i];
                    max_t[i] = (point[i] - org[i]) * idir[i];
                }
            }
        }

        // get largest of the max_t's for final choice of intersection
        int whichplane = (max_t[0] < max_t[1]) ? ((max_t[1] < max_t[2]) ? 2 : 1) : ((max_t[0] < max_t[2]) ? 2 : 0);

        t = max_t[whichplane];
        // check final candidate actually inside box_intersection
        if (dist <= t)
        {
            return false;
        }
        if (t < EPSILON)
        {
            return false;
        }

        switch (whichplane)
        {
        case 0:
            point[1] = org[1] + t * dir[1];
            if (point[1] < min_[1] || point[1] > max_[1]) return false;
            point[2] = org[2] + t * dir[2];
            if (point[2] < min_[2] || point[2] > max_[2]) return false;

            break;
        case 1:
            point[0] = org[0] + t * dir[0];
            if (point[0] < min_[0] || point[0] > max_[0]) return false;
            point[2] = org[2] + t * r.direction()[2];
            if (point[2] < min_[2] || point[2] > max_[2]) return false;

            break;
        case 2:
            point[1] = org[1] + t * dir[1];
            if (point[1] < min_[1] || point[1] > max_[1]) return false;
            point[0] = org[0] + t * dir[0];
            if (point[0] < min_[0] || point[0] > max_[0]) return false;

            break;
        }

        info->distance = (t);
        (reinterpret_cast<box_freearea_struct*>(info->freearea))->plane = whichplane;
        info->p_intersection = this;

        return true; // ray hits box_intersection
    }
#else
    bool box_intersection::test(const ray& r, real dist) const
    {
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        real xa, ya, za, xb, yb, zb;
        real t0, t1, t2;
        int nRet = 0;

        //get classification
        for (int i = 0; i < 3; i++)
        {
            if (dir[i] < 0)
            {
                nRet |= (1 << (2 - i));
            }
        }

        switch (nRet)
        {
        case 7: //- - -
            if ((org[0] < min_[0]) || (org[1] < min_[1]) || (org[2] < min_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * ya - dir[1] * xb < 0) ||
                (dir[0] * yb - dir[1] * xa > 0) ||
                (dir[0] * zb - dir[2] * xa > 0) ||
                (dir[0] * za - dir[2] * xb < 0) ||
                (dir[1] * za - dir[2] * yb < 0) ||
                (dir[1] * zb - dir[2] * ya > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xb * idir[0];
            t1 = yb * idir[1];
            t2 = zb * idir[2];

            break;
        case 6: //- - +
            if ((org[0] < min_[0]) || (org[1] < min_[1]) || (org[2] > max_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * ya - dir[1] * xb < 0) ||
                (dir[0] * yb - dir[1] * xa > 0) ||
                (dir[0] * zb - dir[2] * xb > 0) ||
                (dir[0] * za - dir[2] * xa < 0) ||
                (dir[1] * za - dir[2] * ya < 0) ||
                (dir[1] * zb - dir[2] * yb > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xb * idir[0];
            t1 = yb * idir[1];
            t2 = za * idir[2];

            break;
        case 5: //- + -
            if ((org[0] < min_[0]) || (org[1] > max_[1]) || (org[2] < min_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * ya - dir[1] * xa < 0) ||
                (dir[0] * yb - dir[1] * xb > 0) ||
                (dir[0] * zb - dir[2] * xa > 0) ||
                (dir[0] * za - dir[2] * xb < 0) ||
                (dir[1] * zb - dir[2] * yb < 0) ||
                (dir[1] * za - dir[2] * ya > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xb * idir[0];
            t1 = ya * idir[1];
            t2 = zb * idir[2];

            break;
        case 4: //- + +
            if ((org[0] < min_[0]) || (org[1] > max_[1]) || (org[2] > max_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * ya - dir[1] * xa < 0) ||
                (dir[0] * yb - dir[1] * xb > 0) ||
                (dir[0] * zb - dir[2] * xb > 0) ||
                (dir[0] * za - dir[2] * xa < 0) ||
                (dir[1] * zb - dir[2] * ya < 0) ||
                (dir[1] * za - dir[2] * yb > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xb * idir[0];
            t1 = ya * idir[1];
            t2 = za * idir[2];

            break;
        case 3: //+ - -
            if ((org[0] > max_[0]) || (org[1] < min_[1]) || (org[2] < min_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * yb - dir[1] * xb < 0) ||
                (dir[0] * ya - dir[1] * xa > 0) ||
                (dir[0] * za - dir[2] * xa > 0) ||
                (dir[0] * zb - dir[2] * xb < 0) ||
                (dir[1] * za - dir[2] * yb < 0) ||
                (dir[1] * zb - dir[2] * ya > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xa * idir[0];
            t1 = yb * idir[1];
            t2 = zb * idir[2];

            break;
        case 2: //+ - +
            if ((org[0] > max_[0]) || (org[1] < min_[1]) || (org[2] > max_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * yb - dir[1] * xb < 0) ||
                (dir[0] * ya - dir[1] * xa > 0) ||
                (dir[0] * za - dir[2] * xb > 0) ||
                (dir[0] * zb - dir[2] * xa < 0) ||
                (dir[1] * za - dir[2] * ya < 0) ||
                (dir[1] * zb - dir[2] * yb > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xa * idir[0];
            t1 = yb * idir[1];
            t2 = za * idir[2];

            break;
        case 1: //+ + -
            if ((org[0] > max_[0]) || (org[1] > max_[1]) || (org[2] < min_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * yb - dir[1] * xa < 0) ||
                (dir[0] * ya - dir[1] * xb > 0) ||
                (dir[0] * za - dir[2] * xb > 0) ||
                (dir[0] * zb - dir[2] * xb < 0) ||
                (dir[1] * zb - dir[2] * yb < 0) ||
                (dir[1] * za - dir[2] * ya > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xa * idir[0];
            t1 = ya * idir[1];
            t2 = zb * idir[2];

            break;
        case 0: //+ + +
            if ((org[0] > max_[0]) || (org[1] > max_[1]) || (org[2] > max_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * yb - dir[1] * xa < 0) ||
                (dir[0] * ya - dir[1] * xb > 0) ||
                (dir[0] * za - dir[2] * xb > 0) ||
                (dir[0] * zb - dir[2] * xa < 0) ||
                (dir[1] * zb - dir[2] * ya < 0) ||
                (dir[1] * za - dir[2] * yb > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xa * idir[0];
            t1 = ya * idir[1];
            t2 = za * idir[2];

            break;
        default:
            assert(0); //ERROR
            return false;
        }

        if (t1 > t0) t0 = t1;
        if (t2 > t0) t0 = t2;

        return (t0 < dist);
    }

    bool box_intersection::test(test_info* info, const ray& r, real dist) const
    {
        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& idir = r.inversed_direction();

        real xa, ya, za, xb, yb, zb;
        real t0, t1, t2;
        int nRet = 0;

        //get classification
        for (int i = 0; i < 3; i++)
        {
            if (dir[i] < 0)
            {
                nRet |= (1 << (2 - i));
            }
        }

        switch (nRet)
        {
        case 7: //- - -
            if ((org[0] < min_[0]) || (org[1] < min_[1]) || (org[2] < min_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * ya - dir[1] * xb < 0) ||
                (dir[0] * yb - dir[1] * xa > 0) ||
                (dir[0] * zb - dir[2] * xa > 0) ||
                (dir[0] * za - dir[2] * xb < 0) ||
                (dir[1] * za - dir[2] * yb < 0) ||
                (dir[1] * zb - dir[2] * ya > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xb * idir[0];
            t1 = yb * idir[1];
            t2 = zb * idir[2];

            break;
        case 6: //- - +
            if ((org[0] < min_[0]) || (org[1] < min_[1]) || (org[2] > max_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * ya - dir[1] * xb < 0) ||
                (dir[0] * yb - dir[1] * xa > 0) ||
                (dir[0] * zb - dir[2] * xb > 0) ||
                (dir[0] * za - dir[2] * xa < 0) ||
                (dir[1] * za - dir[2] * ya < 0) ||
                (dir[1] * zb - dir[2] * yb > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xb * idir[0];
            t1 = yb * idir[1];
            t2 = za * idir[2];

            break;
        case 5: //- + -
            if ((org[0] < min_[0]) || (org[1] > max_[1]) || (org[2] < min_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * ya - dir[1] * xa < 0) ||
                (dir[0] * yb - dir[1] * xb > 0) ||
                (dir[0] * zb - dir[2] * xa > 0) ||
                (dir[0] * za - dir[2] * xb < 0) ||
                (dir[1] * zb - dir[2] * yb < 0) ||
                (dir[1] * za - dir[2] * ya > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xb * idir[0];
            t1 = ya * idir[1];
            t2 = zb * idir[2];

            break;
        case 4: //- + +
            if ((org[0] < min_[0]) || (org[1] > max_[1]) || (org[2] > max_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * ya - dir[1] * xa < 0) ||
                (dir[0] * yb - dir[1] * xb > 0) ||
                (dir[0] * zb - dir[2] * xb > 0) ||
                (dir[0] * za - dir[2] * xa < 0) ||
                (dir[1] * zb - dir[2] * ya < 0) ||
                (dir[1] * za - dir[2] * yb > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xb * idir[0];
            t1 = ya * idir[1];
            t2 = za * idir[2];

            break;
        case 3: //+ - -
            if ((org[0] > max_[0]) || (org[1] < min_[1]) || (org[2] < min_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * yb - dir[1] * xb < 0) ||
                (dir[0] * ya - dir[1] * xa > 0) ||
                (dir[0] * za - dir[2] * xa > 0) ||
                (dir[0] * zb - dir[2] * xb < 0) ||
                (dir[1] * za - dir[2] * yb < 0) ||
                (dir[1] * zb - dir[2] * ya > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xa * idir[0];
            t1 = yb * idir[1];
            t2 = zb * idir[2];

            break;
        case 2: //+ - +
            if ((org[0] > max_[0]) || (org[1] < min_[1]) || (org[2] > max_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * yb - dir[1] * xb < 0) ||
                (dir[0] * ya - dir[1] * xa > 0) ||
                (dir[0] * za - dir[2] * xb > 0) ||
                (dir[0] * zb - dir[2] * xa < 0) ||
                (dir[1] * za - dir[2] * ya < 0) ||
                (dir[1] * zb - dir[2] * yb > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xa * idir[0];
            t1 = yb * idir[1];
            t2 = za * idir[2];

            break;
        case 1: //+ + -
            if ((org[0] > max_[0]) || (org[1] > max_[1]) || (org[2] < min_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * yb - dir[1] * xa < 0) ||
                (dir[0] * ya - dir[1] * xb > 0) ||
                (dir[0] * za - dir[2] * xa > 0) ||
                (dir[0] * zb - dir[2] * xb < 0) ||
                (dir[1] * zb - dir[2] * yb < 0) ||
                (dir[1] * za - dir[2] * ya > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xa * idir[0];
            t1 = ya * idir[1];
            t2 = zb * idir[2];

            break;
        case 0: //+ + +
            if ((org[0] > max_[0]) || (org[1] > max_[1]) || (org[2] > max_[2])) return false;

            xa = min_[0] - org[0];
            ya = min_[1] - org[1];
            za = min_[2] - org[2];
            xb = max_[0] - org[0];
            yb = max_[1] - org[1];
            zb = max_[2] - org[2];

            if ((dir[0] * yb - dir[1] * xa < 0) ||
                (dir[0] * ya - dir[1] * xb > 0) ||
                (dir[0] * za - dir[2] * xb > 0) ||
                (dir[0] * zb - dir[2] * xa < 0) ||
                (dir[1] * zb - dir[2] * ya < 0) ||
                (dir[1] * za - dir[2] * yb > 0))
            {
                return false;
            }

            // compute the intersection distance
            t0 = xa * idir[0];
            t1 = ya * idir[1];
            t2 = za * idir[2];

            break;
        default:
            assert(0); //ERROR
            return false;
        }

        int plane = 0;

        if (t1 > t0)
        {
            t0 = t1;
            plane = 1;
        }
        if (t2 > t0)
        {
            t0 = t2;
            plane = 2;
        }

        if (t0 < dist)
        {
            info->distance = (t0);
            (reinterpret_cast<box_freearea_struct*>(info->freearea))->plane = plane;
            info->p_intersection = this;

            return true;
        }
        else
        {
            return false;
        }
    }

#endif

    void box_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        int plane = (reinterpret_cast<box_freearea_struct*>(info->freearea))->plane;

        vector3 n(0, 0, 0);

        n[plane] = (r.direction()[plane] < 0) ? real(1) : real(-1);

        info->normal = (n);
        info->geometric = n;

        vector3 pos(r.origin() + info->distance * r.direction());
        info->position = pos;

        vector3 coord;
        coord[0] = (min_[0] - pos[0]) / (max_[0] - min_[0]);
        coord[1] = (min_[1] - pos[1]) / (max_[1] - min_[1]);
        coord[2] = (min_[2] - pos[2]) / (max_[2] - min_[2]);
        coord = coord;

        return;
    }

    vector3 box_intersection::min() const
    {
        static const real EPSILON = values::epsilon() * 1000;
        return min_ - vector3(EPSILON, EPSILON, EPSILON);
    }
    vector3 box_intersection::max() const
    {
        static const real EPSILON = values::epsilon() * 1000;
        return max_ + vector3(EPSILON, EPSILON, EPSILON);
    }
}
