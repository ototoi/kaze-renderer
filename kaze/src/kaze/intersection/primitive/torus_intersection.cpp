#include "torus_intersection.h"
#include "test_info.h"
#include "values.h"
#include "types.h"

#include "intersection_ex.h"
#include "primitive_intersection_ex.h"

namespace kaze
{
    inline static vector3 mul_normal(const matrix3& m, const vector3& v)
    {
        return vector3(
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
    }

    inline static vector3 safe_normalize(const vector3& n)
    {
        static const real EPSILON = values::epsilon();
        real l2 = n.sqr_length();
        if (l2 < EPSILON) return vector3(0, 0, 0);
        return n * real(1) / std::sqrt(l2);
    }

    class torus_intersection_imp
    {
    public:
        torus_intersection_imp(const vector3& o, real ro, real ri, real umin, real umax, real vmin, real vmax);
        torus_intersection_imp(const vector3& o, const vector3& x, const vector3& y, real ri);
        ~torus_intersection_imp();
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const { return min_; }
        vector3 max() const { return max_; }
    protected:
        bool set_matrix(const vector3& x, const vector3& y, const vector3& z);
        void set_minmax();

    protected:
        bool test_internal(const ray& r, real tmin, real tmax) const;
        bool test_internal(test_info* info, const ray& r, real tmin, real tmax) const;

    private:
        real ro_;
        real ri_;
        vector3 center_;
        matrix3 w2l_; //
        matrix3 l2w_; //
        vector3 min_;
        vector3 max_;
        real umin_;
        real umax_;
        real vmin_;
        real vmax_;
    };
    //---------------------------------------------------------------
    torus_intersection_imp::torus_intersection_imp(const vector3& o, real ro, real ri, real umin, real umax, real vmin, real vmax)
    {
        center_ = o;
        ro_ = ro;
        ri_ = ri;
        vector3 x = vector3(1, 0, 0);
        vector3 y = vector3(0, 1, 0);
        vector3 z = vector3(0, 0, 1);
        set_matrix(x, y, z);
        set_minmax();
        umin_ = umin;
        umax_ = umax;
        vmin_ = vmin;
        vmax_ = vmax;
    }

    torus_intersection_imp::torus_intersection_imp(const vector3& o, const vector3& x, const vector3& y, real ri)
    {
        center_ = o;
        ro_ = 1;
        ri_ = ri;
        vector3 z = normalize(cross(x, y));
        set_matrix(x, y, z);
        set_minmax();
        umin_ = 0;
        umax_ = 1;
        vmin_ = 0;
        vmax_ = 1;
    }

    bool torus_intersection_imp::set_matrix(const vector3& x, const vector3& y, const vector3& z)
    {
        matrix3 m = matrix3(x[0], y[0], z[0], x[1], y[1], z[1], x[2], y[2], z[2]);
        if (!m.is_invertible()) return false;
        matrix3 im = ~m;
        l2w_ = m;  //local2world;
        w2l_ = im; //world2local;
        return true;
    }

    inline static real sgn(int s, real v)
    {
        return s ? -v : v;
    }

    void torus_intersection_imp::set_minmax()
    {
        static const real FAR = values::far();
        static const real EPSILON = values::epsilon() * 1024;

        vector3 min = vector3(+FAR, +FAR, +FAR);
        vector3 max = vector3(-FAR, -FAR, -FAR);

        real rr = ri_ + ro_;

        vector3 p[8];
        int idx = 0;
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                for (int k = 0; k < 2; k++)
                {
                    p[idx] = vector3(sgn(i, rr), sgn(j, rr), sgn(k, ri_));
                    idx++;
                }
            }
        }

        for (int i = 0; i < 8; i++)
        {
            vector3 pp = l2w_ * (p[i] + center_);
            for (int j = 0; j < 3; j++)
            {
                if (min[j] > pp[j]) min[j] = pp[j];
                if (max[j] < pp[j]) max[j] = pp[j];
            }
        }

        min_ = min - vector3(EPSILON, EPSILON, EPSILON);
        max_ = max + vector3(EPSILON, EPSILON, EPSILON);
    }

    torus_intersection_imp::~torus_intersection_imp()
    {
    }

    bool torus_intersection_imp::test_internal(const ray& r, real tmin, real tmax) const
    {
        real ro = ro_;
        real ri = ri_;
        vector3 morg = w2l_ * (r.origin() - this->center_);
        vector3 mdir = w2l_ * r.direction();
        primitive_test_info pinf;
        if (test_torus(&pinf, ro, ri, umin_, umax_, vmin_, vmax_, morg, mdir, tmin, tmax))
        {
            return true;
        }
        return false;
    }

    bool torus_intersection_imp::test_internal(test_info* info, const ray& r, real tmin, real tmax) const
    {
        real ro = ro_;
        real ri = ri_;
        vector3 morg = w2l_ * (r.origin() - this->center_);
        vector3 mdir = w2l_ * r.direction();
        primitive_test_info pinf;
        if (test_torus(&pinf, ro, ri, umin_, umax_, vmin_, vmax_, morg, mdir, tmin, tmax))
        {
            info->distance = pinf.t;
            primitive_test_info* fsp = reinterpret_cast<primitive_test_info*>(info->freearea);
            *fsp = pinf;
            return true;
        }
        return false;
    }

    bool torus_intersection_imp::test(const ray& r, real dist) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, dist))
        {
            real tmin = std::max<real>(0, rng.tmin);
            real tmax = std::min<real>(dist, rng.tmax);
            return test_internal(r, tmin, tmax);
        }
        return false;
    }

    bool torus_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, dist))
        {
            real tmin = std::max<real>(0, rng.tmin);
            real tmax = std::min<real>(dist, rng.tmax);
            return test_internal(info, r, tmin, tmax);
        }
        return false;
    }

    void torus_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        primitive_test_info* fsp = reinterpret_cast<primitive_test_info*>(info->freearea);
        real u = fsp->u;
        real v = fsp->v;
        real w = fsp->w;
        info->position = r.origin() + dist * r.direction();
        vector3 n = safe_normalize(mul_normal(w2l_, fsp->n));
        vector3 dpdu = safe_normalize(mul_normal(w2l_, fsp->dpdu));
        vector3 dpdv = safe_normalize(mul_normal(w2l_, fsp->dpdv));
        info->geometric = n;
        info->normal = n;
        info->coord = vector3(u, v, w);
        info->tangent = dpdu;
        info->binormal = dpdv;
    }

    //---------------------------------------------------------------

    torus_intersection::torus_intersection(const vector3& o, real ro, real ri, real umin, real umax, real vmin, real vmax)
    {
        imp_ = new torus_intersection_imp(o, ro, ri, umin, umax, vmin, vmax);
    }

    torus_intersection::torus_intersection(const vector3& o, const vector3& x, const vector3& y, real ri)
    {
        imp_ = new torus_intersection_imp(o, x, y, ri);
    }

    torus_intersection::~torus_intersection()
    {
        delete imp_;
    }

    bool torus_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool torus_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void torus_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        imp_->finalize(info, r, dist);
    }

    vector3 torus_intersection::min() const
    {
        return imp_->min();
    }

    vector3 torus_intersection::max() const
    {
        return imp_->max();
    }
}
