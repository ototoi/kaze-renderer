#include "frame_box_intersection.h"
#include "test_info.h"

#include "cylinder_intersection.h"
#include "capsule_intersection.h"

#include <memory>

namespace kaze
{

    //-----------------------------------
    //FRAME_BOX
    //-----------------------------------
    //struct frame_box_freearea_struct:public capsule_freearea_struct{
    //	std::size_t i;
    //};

    frame_box_intersection::frame_box_intersection(const vector3& min, const vector3& max, real radius)
    {
        static const real EPSILON = values::epsilon() * 1000;

        real r;

        r = radius + EPSILON;

        min_ = min - vector3(r, r, r);
        max_ = max + vector3(r, r, r);

        r = radius;

        try
        {
            std::unique_ptr<intersection> ap;

            //typedef cylinder_intersection frame;
            typedef capsule_intersection frame;

            ap.reset(new frame(vector3(min[0], min[1], min[2]), vector3(max[0], min[1], min[2]), r));
            mv_.push_back(ap.release());
            ap.reset(new frame(vector3(max[0], min[1], min[2]), vector3(max[0], max[1], min[2]), r));
            mv_.push_back(ap.release());
            ap.reset(new frame(vector3(max[0], max[1], min[2]), vector3(min[0], max[1], min[2]), r));
            mv_.push_back(ap.release());
            ap.reset(new frame(vector3(min[0], max[1], min[2]), vector3(min[0], min[1], min[2]), r));
            mv_.push_back(ap.release());

            ap.reset(new frame(vector3(min[0], min[1], max[2]), vector3(max[0], min[1], max[2]), r));
            mv_.push_back(ap.release());
            ap.reset(new frame(vector3(max[0], min[1], max[2]), vector3(max[0], max[1], max[2]), r));
            mv_.push_back(ap.release());
            ap.reset(new frame(vector3(max[0], max[1], max[2]), vector3(min[0], max[1], max[2]), r));
            mv_.push_back(ap.release());
            ap.reset(new frame(vector3(min[0], max[1], max[2]), vector3(min[0], min[1], max[2]), r));
            mv_.push_back(ap.release());

            ap.reset(new frame(vector3(min[0], min[1], min[2]), vector3(min[0], min[1], max[2]), r));
            mv_.push_back(ap.release());
            ap.reset(new frame(vector3(max[0], min[1], min[2]), vector3(max[0], min[1], max[2]), r));
            mv_.push_back(ap.release());
            ap.reset(new frame(vector3(max[0], max[1], min[2]), vector3(max[0], max[1], max[2]), r));
            mv_.push_back(ap.release());
            ap.reset(new frame(vector3(min[0], max[1], min[2]), vector3(min[0], max[1], max[2]), r));
            mv_.push_back(ap.release());

            /*
			ap.reset(new capsule_intersection(vector3(min[0],min[1],min[2]),r,vector3(max[0],min[1],min[2]),r));mv_.push_back(ap.release());
			ap.reset(new capsule_intersection(vector3(max[0],min[1],min[2]),r,vector3(max[0],max[1],min[2]),r));mv_.push_back(ap.release());
			ap.reset(new capsule_intersection(vector3(max[0],max[1],min[2]),r,vector3(min[0],max[1],min[2]),r));mv_.push_back(ap.release());
			ap.reset(new capsule_intersection(vector3(min[0],max[1],min[2]),r,vector3(min[0],min[1],min[2]),r));mv_.push_back(ap.release());

			ap.reset(new capsule_intersection(vector3(min[0],min[1],max[2]),r,vector3(max[0],min[1],max[2]),r));mv_.push_back(ap.release());
			ap.reset(new capsule_intersection(vector3(max[0],min[1],max[2]),r,vector3(max[0],max[1],max[2]),r));mv_.push_back(ap.release());
			ap.reset(new capsule_intersection(vector3(max[0],max[1],max[2]),r,vector3(min[0],max[1],max[2]),r));mv_.push_back(ap.release());
			ap.reset(new capsule_intersection(vector3(min[0],max[1],max[2]),r,vector3(min[0],min[1],max[2]),r));mv_.push_back(ap.release());

			ap.reset(new capsule_intersection(vector3(min[0],min[1],min[2]),r,vector3(min[0],min[1],max[2]),r));mv_.push_back(ap.release());
			ap.reset(new capsule_intersection(vector3(max[0],min[1],min[2]),r,vector3(max[0],min[1],max[2]),r));mv_.push_back(ap.release());
			ap.reset(new capsule_intersection(vector3(max[0],max[1],min[2]),r,vector3(max[0],max[1],max[2]),r));mv_.push_back(ap.release());
			ap.reset(new capsule_intersection(vector3(min[0],max[1],min[2]),r,vector3(min[0],max[1],max[2]),r));mv_.push_back(ap.release());
			*/
        }
        catch (std::bad_alloc& e)
        {
            std::size_t sz = mv_.size();
            for (std::size_t i = 0; i < sz; i++)
            {
                delete mv_[i];
            }

            throw e;
        }
    }

    frame_box_intersection::~frame_box_intersection()
    {
        std::size_t sz = mv_.size();
        for (std::size_t i = 0; i < sz; i++)
        {
            delete mv_[i];
        }
    }

    bool frame_box_intersection::test(const ray& r, real dist) const
    {
        if (!test_AABB(min_, max_, r, dist)) return false;

        std::size_t sz = mv_.size();
        for (std::size_t i = 0; i < sz; i++)
        {
            if (mv_[i]->test(r, dist)) return true;
        }

        return false;
    }

    bool frame_box_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (!test_AABB(min_, max_, r, dist)) return false;

        bool bRet = false;

        std::size_t sz = mv_.size();
        for (std::size_t i = 0; i < sz; i++)
        {
            if (mv_[i]->test(info, r, dist))
            {
                dist = info->distance;
                //frame_box_freearea_struct * fsp = reinterpret_cast<frame_box_freearea_struct*>(info->freearea);
                //fsp->i = i;
                bRet = true;
            }
        }

        //if(bRet){
        //info->p_intersection = this;
        //}

        return bRet;
    }

    void frame_box_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        //NO IMPLEMENT
        //frame_box_freearea_struct * fsp = reinterpret_cast<frame_box_freearea_struct*>(info->freearea);
        //mv_[fsp->i]->finalize(info,r,dist);
        assert(0);
    }
}
