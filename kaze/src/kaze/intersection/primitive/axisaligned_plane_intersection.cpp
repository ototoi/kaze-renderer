#include "axisaligned_plane_intersection.h"
#include "test_info.h"

#include "logger.h"

namespace kaze
{

    //-----------------------------------
    //axisaligned_plane_intersection
    //-----------------------------------

    axisaligned_plane_intersection::axisaligned_plane_intersection(int axis, const vector3& center, real h_width, real v_width)
    {
        axis %= 6;

        if (axis < 3)
        {
            d_plane_ = (axis + 0) % 3; //0,1,2
            h_plane_ = (axis + 1) % 3; //1,2,0
            v_plane_ = (axis + 2) % 3; //2,0,1
        }
        else
        {
            d_plane_ = (axis - 0) % 3; //0,1,2
            h_plane_ = (axis - 1) % 3; //2,0,1
            v_plane_ = (axis - 2) % 3; //1,2,0
        }

        real h_half = h_width * real(0.5);
        real v_half = v_width * real(0.5);

        min_[0] = center[h_plane_] - h_half;
        min_[1] = center[v_plane_] - v_half;

        max_[0] = center[h_plane_] + h_half;
        max_[1] = center[v_plane_] + v_half;

        depth_ = center[d_plane_];

        //print_log("d_plane: %d\n",d_plane_);
    }

    bool axisaligned_plane_intersection::test(const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1000 * 100;

        vector3 org = r.origin();
        vector3 dir = r.direction();
        vector3 idir = r.inversed_direction();

        real d = depth_;
        real t = (d - org[d_plane_]) * idir[d_plane_] - EPSILON;
        if (t < 0 || dist <= t) return false;

        //return true;

        real h = org[h_plane_] + t * dir[h_plane_];
        if (h < min_[0] || max_[0] < h) return false;

        real v = org[v_plane_] + t * dir[v_plane_];
        if (v < min_[1] || max_[1] < v) return false;

        return true;
    }

    bool axisaligned_plane_intersection::test(test_info* info, const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1000 * 100;

        vector3 org = r.origin();
        vector3 dir = r.direction();
        vector3 idir = r.inversed_direction();

        real d = depth_;
        real t = (d - org[d_plane_]) * idir[d_plane_] - EPSILON;
        if (t < 0 || dist <= t) return false;

        //info->p_intersection = this;
        //info->distance = (t);
        //return true;

        real h = org[h_plane_] + t * dir[h_plane_];
        if (h < min_[0] || max_[0] < h) return false;

        real v = org[v_plane_] + t * dir[v_plane_];
        if (v < min_[1] || max_[1] < v) return false;

        info->distance = (t);

        real uu = (h - min_[0]) / (max_[0] - min_[0]);
        real vv = (v - min_[1]) / (max_[1] - min_[1]);
        info->coord = vector3(uu, vv, 0);
        /*
		info->position[h_plane_] = h;
		info->position[v_plane_] = v;
		info->position[d_plane_] = d;
		*/
        info->p_intersection = this;
        return true;
    }

    void axisaligned_plane_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        real dd = (r.direction()[d_plane_] <= 0) ? 1 : -1;
        vector3 n, t, b;

        t[h_plane_] = 1;
        t[v_plane_] = 0;
        t[d_plane_] = 0;

        b[h_plane_] = 0;
        b[v_plane_] = 1;
        b[d_plane_] = 0;

        n[h_plane_] = 0;
        n[v_plane_] = 0;
        n[d_plane_] = 1;

        info->position = (r.origin() + info->distance * r.direction());
        info->normal = n;

        info->tangent = t;
        info->binormal = b;

        //n[h_plane_] = 0;
        //n[v_plane_] = 0;
        n[d_plane_] = dd;
        info->geometric = n;
    }

    vector3 axisaligned_plane_intersection::min() const
    {
        static const real EPSILON = values::epsilon() * 1000;

        vector3 ret;

        ret[h_plane_] = min_[0];
        ret[v_plane_] = min_[1];
        ret[d_plane_] = depth_ - EPSILON;
        return ret;
    }

    vector3 axisaligned_plane_intersection::max() const
    {
        static const real EPSILON = values::epsilon() * 1000;

        vector3 ret;

        ret[h_plane_] = max_[0];
        ret[v_plane_] = max_[1];
        ret[d_plane_] = depth_ + EPSILON;
        return ret;
    }

    bool axisaligned_plane_intersection::get_vertices(size_t& sz) const
    {
        sz = 4;
        return true;
    }
    bool axisaligned_plane_intersection::get_faces(size_t& sz) const
    {
        sz = 2;
        return true;
    }
    bool axisaligned_plane_intersection::get_vertex(size_t i, io_mesh_vertex* v) const
    {
        if (i >= 4) return false;
        double pos[3];
        switch (i)
        {
        case 0:
            pos[h_plane_] = min_[0];
            pos[v_plane_] = min_[1];
            pos[d_plane_] = depth_;
            break;
        case 1:
            pos[h_plane_] = max_[0];
            pos[v_plane_] = min_[1];
            pos[d_plane_] = depth_;
            break;
        case 2:
            pos[h_plane_] = min_[0];
            pos[v_plane_] = max_[1];
            pos[d_plane_] = depth_;
            break;
        default:
            pos[h_plane_] = max_[0];
            pos[v_plane_] = max_[1];
            pos[d_plane_] = depth_;
        }
        memcpy(v->pos, pos, sizeof(double) * 3);

        //
        /*
		print_log("plane:min:(%f,%f)\n",min_[0],min_[1]);
		print_log("plane:max:(%f,%f)\n",max_[0],max_[1]);
		print_log("plane:%d:(%f,%f,%f)\n",i,pos[0],pos[1],pos[2]);
		*/
        //

        return true;
    }

    bool axisaligned_plane_intersection::get_face(size_t i, io_mesh_face* f) const
    {
        static const double puv0[] = {0, 0};
        static const double puv1[] = {1, 0};
        static const double puv2[] = {0, 1};
        static const double puv3[] = {1, 1};

        if (i >= 2) return false;

        size_t i0, i1, i2;
        double n[3];
        double uv0[2];
        double uv1[2];
        double uv2[2];

        n[h_plane_] = 0;
        n[v_plane_] = 0;
        n[d_plane_] = 1;

        /*
			0  1
			2  3

		 */

        if (i == 0)
        {
            i0 = 0;
            i1 = 3;
            i2 = 1;

            memcpy(uv0, puv0, sizeof(double) * 2);
            memcpy(uv1, puv3, sizeof(double) * 2);
            memcpy(uv2, puv1, sizeof(double) * 2);
        }
        else
        {
            i0 = 0;
            i1 = 2;
            i2 = 3;

            memcpy(uv0, puv0, sizeof(double) * 2);
            memcpy(uv1, puv2, sizeof(double) * 2);
            memcpy(uv2, puv3, sizeof(double) * 2);
        }

        f->i0 = i0;
        f->i1 = i1;
        f->i2 = i2;

        memcpy(f->n0, n, sizeof(double) * 3);
        memcpy(f->n1, n, sizeof(double) * 3);
        memcpy(f->n2, n, sizeof(double) * 3);

        memcpy(f->uv0, uv0, sizeof(double) * 2);
        memcpy(f->uv1, uv1, sizeof(double) * 2);
        memcpy(f->uv2, uv2, sizeof(double) * 2);

        return true;
    }
    /*
	bool axisaligned_plane_intersection::coord(vector3& c, const ray& r)const{
		static const real EPSILON = values::epsilon() * 1000 * 100; 

		const vector3&  org = r.origin();
		const vector3&  dir = r.direction();
		const vector3& idir = r.inversed_direction();
		real d = depth_;
		real t = (d - org[d_plane_])*idir[d_plane_] - EPSILON;
		if(t<0)return false;

		real h = org[h_plane_] + t * dir[h_plane_];
		//if(h<min_[0] || max_[0]<h)return false;
		
		real v = org[v_plane_] + t * dir[v_plane_];
		//if(v<min_[1] || max_[1]<v)return false;
		
		real uu = (h-min_[0])/(max_[0]-min_[0]);
		real vv = (v-min_[1])/(max_[1]-min_[1]);
		c = vector3(uu,vv,0);

		return true;
	}
*/
}
