#include "triangle_intersection.h"
#include "test_info.h"

#include "mesh/mesh_face.h"

namespace kaze
{

    triangle_intersection::triangle_intersection(const vector3& p0, const vector3& p1, const vector3& p2) : p0_(p0), p1_(p1), p2_(p2)
    {
        n_ = normalize(cross(p1_ - p0_, p2_ - p0_));
    }

    bool triangle_intersection::test(const ray& r, real dist) const
    {
        mesh_face mf;
        memset(&mf, 0, sizeof(mesh_face));
        mf.p_p0 = &(this->p0_);
        mf.p_p1 = &(this->p1_);
        mf.p_p2 = &(this->p2_);
        return mf.test_DS(r, dist);
    }

    bool triangle_intersection::test(test_info* info, const ray& r, real dist) const
    {
        mesh_face mf;
        memset(&mf, 0, sizeof(mesh_face));
        mf.p_p0 = &(this->p0_);
        mf.p_p1 = &(this->p1_);
        mf.p_p2 = &(this->p2_);
        mf.n0 = n_;
        mf.n1 = n_;
        mf.n2 = n_;
        mf.uv0 = vector2(0, 0);
        mf.uv1 = vector2(1, 0);
        mf.uv2 = vector2(0, 1);
        if (mf.test_DS(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void triangle_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        mesh_face mf;
        memset(&mf, 0, sizeof(mesh_face));
        mf.p_p0 = &(this->p0_);
        mf.p_p1 = &(this->p1_);
        mf.p_p2 = &(this->p2_);
        mf.n0 = n_;
        mf.n1 = n_;
        mf.n2 = n_;
        mf.uv0 = vector2(0, 0);
        mf.uv1 = vector2(1, 0);
        mf.uv2 = vector2(0, 1);
        mf.finalize(info, r, dist);
    }

    vector3 triangle_intersection::min() const
    {
        vector3 ret(values::far(), values::far(), values::far());

        for (int i = 0; i < 3; i++)
        {
            if (p0_[i] < ret[i]) ret[i] = p0_[i];
            if (p1_[i] < ret[i]) ret[i] = p1_[i];
            if (p2_[i] < ret[i]) ret[i] = p2_[i];
        }

        return ret;
    }

    vector3 triangle_intersection::max() const
    {
        vector3 ret(-values::far(), -values::far(), -values::far());

        for (int i = 0; i < 3; i++)
        {
            if (ret[i] < p0_[i]) ret[i] = p0_[i];
            if (ret[i] < p1_[i]) ret[i] = p1_[i];
            if (ret[i] < p2_[i]) ret[i] = p2_[i];
        }

        return ret;
    }
}
