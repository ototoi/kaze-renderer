#include "ellipsoid_intersection.h"
#include "test_info.h"
#include "values.h"
#include "types.h"

#include "intersection_ex.h"
#include "primitive_intersection_ex.h"

namespace kaze
{

    inline static vector3 mul_normal(const matrix3& m, const vector3& v)
    {
        return vector3(
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
    }

    inline static vector3 safe_normalize(const vector3& n)
    {
        static const real EPSILON = values::epsilon();
        real l2 = n.sqr_length();
        if (l2 < EPSILON) return vector3(0, 0, 0);
        return n * real(1) / std::sqrt(l2);
    }

    class ellipsoid_intersection_imp
    {
    public:
        ellipsoid_intersection_imp(const vector3& o, real r, real umin, real umax, real vmin, real vmax);
        ellipsoid_intersection_imp(const vector3& o, const vector3& x, const vector3& y, const vector3& z);
        //~ellipsoid_intersection_imp();
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    protected:
        bool set_matrix(const vector3& x, const vector3& y, const vector3& z);
        void set_minmax(const vector3& x, const vector3& y, const vector3& z);

    private:
        vector3 center_;
        matrix3 w2l_; //
        matrix3 l2w_; //
        vector3 min_;
        vector3 max_;
        real umin_;
        real umax_;
        real vmin_;
        real vmax_;
    };

    bool ellipsoid_intersection_imp::set_matrix(const vector3& x, const vector3& y, const vector3& z)
    {
        matrix3 m = matrix3(x[0], y[0], z[0], x[1], y[1], z[1], x[2], y[2], z[2]);
        if (!m.is_invertible()) return false;
        matrix3 im = ~m;
        l2w_ = m;  //local2world;
        w2l_ = im; //world2local;
        return true;
    }

    inline static vector3 sgn(int s, const vector3& v)
    {
        return s ? -v : v;
    }

    void ellipsoid_intersection_imp::set_minmax(const vector3& x, const vector3& y, const vector3& z)
    {
        static const real FAR = values::far();
        vector3 p[8];
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                for (int k = 0; k < 2; k++)
                {
                    int idx = i | j * 2 | k * 4;
                    p[idx] = sgn(i, x) + sgn(j, y) + sgn(k, z) + center_;
                }
            }
        }
        vector3 min(FAR, FAR, FAR);
        vector3 max(-FAR, -FAR, -FAR);
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (min[j] > p[i][j]) min[j] = p[i][j];
                if (max[j] < p[i][j]) max[j] = p[i][j];
            }
        }
        min_ = min;
        max_ = max;
    }

    ellipsoid_intersection_imp::ellipsoid_intersection_imp(const vector3& o, real radius, real umin, real umax, real vmin, real vmax) : center_(o)
    {
        vector3 x = vector3(1, 0, 0) * radius;
        vector3 y = vector3(0, 1, 0) * radius;
        vector3 z = vector3(0, 0, 1) * radius;

        if (!set_matrix(x, y, z))
        {
            throw "ellipsoid_intersection:set_matrix";
        }
        set_minmax(x, y, z);
        umin_ = umin;
        umax_ = umax;
        vmin_ = vmin;
        vmax_ = vmax;
    }

    ellipsoid_intersection_imp::ellipsoid_intersection_imp(const vector3& o, const vector3& x, const vector3& y, const vector3& z) : center_(o)
    {
        if (!set_matrix(x, y, z))
        {
            throw "ellipsoid_intersection:set_matrix";
        }
        set_minmax(x, y, z);
        umin_ = 0;
        umax_ = 1;
        vmin_ = 0;
        vmax_ = 1;
    }

    bool ellipsoid_intersection_imp::test(const ray& r, real dist) const
    {
        real tmin = 0;
        real tmax = dist;
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            vector3 morg = w2l_ * (r.origin() - this->center_);
            vector3 mdir = w2l_ * r.direction();
            primitive_test_info pinf;
            if (test_sphere(&pinf, umin_, umax_, vmin_, vmax_, morg, mdir, tmin, tmax))
            {
                return true;
            }
        }
        return false;
    }

    bool ellipsoid_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        real tmin = 0;
        real tmax = dist;
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            vector3 morg = w2l_ * (r.origin() - this->center_);
            vector3 mdir = w2l_ * r.direction();
            primitive_test_info pinf;
            if (test_sphere(&pinf, umin_, umax_, vmin_, vmax_, morg, mdir, tmin, tmax))
            {
                info->distance = pinf.t;
                primitive_test_info* fsp = reinterpret_cast<primitive_test_info*>(info->freearea);
                *fsp = pinf;
                return true;
            }
        }
        return false;
    }

    void ellipsoid_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        primitive_test_info* fsp = reinterpret_cast<primitive_test_info*>(info->freearea);
        real u = fsp->u;
        real v = fsp->v;
        real w = fsp->w;
        info->position = r.origin() + dist * r.direction();
        vector3 n = safe_normalize(mul_normal(w2l_, fsp->n));
        vector3 dpdu = safe_normalize(mul_normal(w2l_, fsp->dpdu));
        vector3 dpdv = safe_normalize(mul_normal(w2l_, fsp->dpdv));
        info->geometric = n;
        info->normal = n;
        info->coord = vector3(u, v, w);
        info->tangent = dpdu;
        info->binormal = dpdv;
    }

    vector3 ellipsoid_intersection_imp::min() const { return min_; }
    vector3 ellipsoid_intersection_imp::max() const { return max_; }

    //-----------------------------------------------------------------------------------------

    ellipsoid_intersection::ellipsoid_intersection(const vector3& o, real r, real umin, real umax, real vmin, real vmax)
    {
        imp_ = new ellipsoid_intersection_imp(o, r, umin, umax, vmin, vmax);
    }
    ellipsoid_intersection::ellipsoid_intersection(const vector3& o, const vector3& x, const vector3& y, const vector3& z)
    {
        imp_ = new ellipsoid_intersection_imp(o, x, y, z);
    }
    ellipsoid_intersection::~ellipsoid_intersection()
    {
        delete imp_;
    }
    bool ellipsoid_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }
    bool ellipsoid_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }
    void ellipsoid_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        imp_->finalize(info, r, dist);
    }
    vector3 ellipsoid_intersection::min() const
    {
        return imp_->min();
    }
    vector3 ellipsoid_intersection::max() const
    {
        return imp_->max();
    }
}
