#ifndef KAZE_TRIANGLE_INTERSECTION_H
#define KAZE_TRIANGLE_INTERSECTION_H

#include "primitive_intersection.h"

namespace kaze
{

    class triangle_intersection : public primitive_intersection
    {
    public:
        triangle_intersection(const vector3& p0, const vector3& p1, const vector3& p2);
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        vector3 p0_;
        vector3 p1_;
        vector3 p2_;

        vector3 n_;
    };
}

#endif
