#ifndef KAZE_HYPERBOLOID_INTERSECTION_H
#define KAZE_HYPERBOLOID_INTERSECTION_H

#include "primitive_intersection.h"

namespace kaze
{

    class hyperboloid_intersection_imp;
    class hyperboloid_intersection : public primitive_intersection
    {
    public:
        hyperboloid_intersection(const vector3& point1, const vector3& point2, real umax = 1.0);
        ~hyperboloid_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        hyperboloid_intersection_imp* imp_;
    };
}

#endif
