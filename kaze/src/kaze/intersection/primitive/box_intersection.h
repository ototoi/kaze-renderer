#ifndef KAZE_INTERSECTION_BOX_H
#define KAZE_INTERSECTION_BOX_H

#include "primitive_intersection.h"

namespace kaze
{

    //BOX
    class box_intersection : public primitive_intersection
    {
    public:
        box_intersection(const vector3& min, const vector3& max);
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        vector3 min_;
        vector3 max_;
    };
}

#endif
