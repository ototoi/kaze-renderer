#include "cylinder_intersection.h"
#include "test_info.h"

#include "intersection_ex.h"
#include "primitive_intersection_ex.h"

#include "logger.h"
#include <sstream>
#include <iostream>

#include <cmath>

namespace kaze
{

    inline static vector3 mul_normal(const matrix3& m, const vector3& v)
    {
        return vector3(
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
    }

    inline static vector3 safe_normalize(const vector3& n)
    {
        static const real EPSILON = values::epsilon();
        real l2 = n.sqr_length();
        if (l2 < EPSILON) return vector3(0, 0, 0);
        return n * real(1) / std::sqrt(l2);
    }

    class cylinder_intersection_imp
    {
    public:
        cylinder_intersection_imp(const vector3& p0, const vector3& p1, real radius, real umin, real umax);
        cylinder_intersection_imp(const vector3& p0, const vector3& x, const vector3& y, const vector3& z, real umin, real umax);
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    public:
        bool test_wo_AABB(const ray& r, real dist) const;
        bool test_wo_AABB(test_info* info, const ray& r, real dist) const;

    protected:
        void initialize(const vector3& p0, const vector3& x, const vector3& y, const vector3& z);

    private:
        vector3 center_;
        matrix3 w2l_; //
        matrix3 l2w_; //
        vector3 min_;
        vector3 max_;
        real umin_;
        real umax_;
    };

    static const real pts[8][3] = {
        {1, 1, 0}, {-1, 1, 0}, {1, -1, 0}, {-1, -1, 0}, {1, 1, 1}, {-1, 1, 1}, {1, -1, 1}, {-1, -1, 1}};
    void cylinder_intersection_imp::initialize(const vector3& p0, const vector3& x, const vector3& y, const vector3& z)
    {
        static const real EPSILON = values::epsilon() * 1024;

        matrix3 m = matrix3(x[0], y[0], z[0], x[1], y[1], z[1], x[2], y[2], z[2]);
        matrix3 im = ~m;

        vector3 pointv[8];
        for (int i = 0; i < 8; i++)
        {
            pointv[i] = (m * vector3(pts[i])) + p0;
        }

        vector3 min = pointv[0];
        vector3 max = pointv[0];

        for (int i = 1; i < 8; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (min[j] > pointv[i][j]) min[j] = pointv[i][j];
                if (max[j] < pointv[i][j]) max[j] = pointv[i][j];
            }
        }
        //
        center_ = p0;
        w2l_ = im; //
        l2w_ = m;  //
        min_ = min - vector3(EPSILON, EPSILON, EPSILON);
        max_ = max + vector3(EPSILON, EPSILON, EPSILON);
    }

    cylinder_intersection_imp::cylinder_intersection_imp(const vector3& p0, const vector3& p1, real radius, real umin, real umax)
    {
        vector3 z(p1 - p0);
        vector3 nz = normalize(z);
        int nPlane = 0;
        if (fabs(nz[nPlane]) > fabs(nz[1])) nPlane = 1;
        if (fabs(nz[nPlane]) > fabs(nz[2])) nPlane = 2;
        vector3 x(0, 0, 0);
        x[nPlane] = 1;
        vector3 y = cross(nz, x);
        x = cross(y, nz);
        initialize(p0, x * radius, y * radius, z);
        umin_ = umin;
        umax_ = umax;
    }

    cylinder_intersection_imp::cylinder_intersection_imp(const vector3& p0, const vector3& x, const vector3& y, const vector3& z, real umin, real umax)
    {
        initialize(p0, x, y, z);
        umin_ = umin;
        umax_ = umax;
    }

    bool cylinder_intersection_imp::test(const ray& r, real dist) const
    {
        real tmin = 0;
        real tmax = dist;
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            vector3 morg = w2l_ * (r.origin() - this->center_);
            vector3 mdir = w2l_ * r.direction();
            primitive_test_info pinf;
            if (test_cylinder(&pinf, umin_, umax_, 0, 1, morg, mdir, tmin, tmax))
            {
                return true;
            }
        }
        return false;
    }

    bool cylinder_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        real tmin = 0;
        real tmax = dist;
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            vector3 morg = w2l_ * (r.origin() - this->center_);
            vector3 mdir = w2l_ * r.direction();
            primitive_test_info pinf;
            if (test_cylinder(&pinf, umin_, umax_, 0, 1, morg, mdir, tmin, tmax))
            {
                info->distance = pinf.t;
                primitive_test_info* fsp = reinterpret_cast<primitive_test_info*>(info->freearea);
                *fsp = pinf;
                return true;
            }
        }
        return false;
    }

    void cylinder_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        primitive_test_info* fsp = reinterpret_cast<primitive_test_info*>(info->freearea);
        real u = fsp->u;
        real v = fsp->v;
        real w = fsp->w;
        info->position = r.origin() + dist * r.direction();
        vector3 n = safe_normalize(mul_normal(w2l_, fsp->n));
        vector3 dpdu = safe_normalize(mul_normal(w2l_, fsp->dpdu));
        vector3 dpdv = safe_normalize(mul_normal(w2l_, fsp->dpdv));
        info->geometric = n;
        info->normal = n;
        info->coord = vector3(u, v, w);
        info->tangent = dpdu;
        info->binormal = dpdv;
    }

    vector3 cylinder_intersection_imp::min() const { return min_; }
    vector3 cylinder_intersection_imp::max() const { return max_; }

    bool cylinder_intersection_imp::test_wo_AABB(const ray& r, real dist) const
    {
        vector3 morg = w2l_ * (r.origin() - this->center_);
        vector3 mdir = w2l_ * r.direction();
        primitive_test_info pinf;
        if (test_cylinder(&pinf, morg, mdir, 0, dist))
        {
            return true;
        }
        return false;
    }
    bool cylinder_intersection_imp::test_wo_AABB(test_info* info, const ray& r, real dist) const
    {
        vector3 morg = w2l_ * (r.origin() - this->center_);
        vector3 mdir = w2l_ * r.direction();
        primitive_test_info pinf;
        if (test_cylinder(&pinf, morg, mdir, 0, dist))
        {
            info->distance = pinf.t;
            primitive_test_info* fsp = reinterpret_cast<primitive_test_info*>(info->freearea);
            *fsp = pinf;
            return true;
        }
        return false;
    }
    //-------------------------------------------------------------
    //
    //-------------------------------------------------------------
    cylinder_intersection::cylinder_intersection(const vector3& p0, const vector3& p1, real radius, real umin, real umax)
    {
        imp_ = new cylinder_intersection_imp(p0, p1, radius, umin, umax);
    }

    cylinder_intersection::cylinder_intersection(const vector3& p0, const vector3& vx, const vector3& vy, const vector3& vz, real umin, real umax)
    {
        imp_ = new cylinder_intersection_imp(p0, vx, vy, vz, umin, umax);
    }

    cylinder_intersection::~cylinder_intersection()
    {
        delete imp_;
    }

    bool cylinder_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool cylinder_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void cylinder_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        imp_->finalize(info, r, dist);
    }

    vector3 cylinder_intersection::min() const
    {
        return imp_->min();
    }

    vector3 cylinder_intersection::max() const
    {
        return imp_->max();
    }

    bool cylinder_intersection::test_wo_AABB(const ray& r, real dist) const
    {
        return imp_->test_wo_AABB(r, dist);
    }

    bool cylinder_intersection::test_wo_AABB(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test_wo_AABB(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }
}
