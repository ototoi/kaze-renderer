#ifndef KAZE_PARABOLOID_INTERSECTION_H
#define KAZE_PARABOLOID_INTERSECTION_H

#include "primitive_intersection.h"

namespace kaze
{

    class paraboloid_intersection_imp;
    class paraboloid_intersection : public primitive_intersection
    {
    public:
        paraboloid_intersection(real radius, real zmin, real zmax, real umax = 1.0);
        ~paraboloid_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        paraboloid_intersection_imp* imp_;
    };
}

#endif
