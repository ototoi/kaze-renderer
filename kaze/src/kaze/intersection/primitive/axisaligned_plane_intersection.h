#ifndef KAZE_AXISALIGNED_PLANE_INTERSECTION_H
#define KAZE_AXISALIGNED_PLANE_INTERSECTION_H

#include "primitive_intersection.h"
#include "mesh_loader.h"

namespace kaze
{

    class axisaligned_plane_intersection : public primitive_intersection,
                                           public mesh_loader
    {
    public:
        enum
        {
            YZ_PLANE = 0,
            ZX_PLANE = 1,
            XY_PLANE = 2,
            ZY_PLANE = 3,
            XZ_PLANE = 4,
            YX_PLANE = 5,
        };

    public:
        axisaligned_plane_intersection(int axis, const vector3& center, real h_width, real v_width);
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    public:
        bool get_vertices(size_t& sz) const;
        bool get_faces(size_t& sz) const;
        bool get_vertex(size_t i, io_mesh_vertex* v) const;
        bool get_face(size_t i, io_mesh_face* f) const;

    protected:
        bool coord(vector3& c, const ray& r) const;

    private:
        vector2 min_;
        vector2 max_;
        real depth_;

        int h_plane_; //horizaon
        int v_plane_; //vertical
        int d_plane_; //depth
    };
}

#endif
