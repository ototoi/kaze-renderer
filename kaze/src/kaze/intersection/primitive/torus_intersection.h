#ifndef KAZE_TORUS_INTERSECTION_H
#define KAZE_TORUS_INTERSECTION_H

#include "primitive_intersection.h"

namespace kaze
{

    class torus_intersection_imp;
    class torus_intersection : public primitive_intersection
    {
    public:
        torus_intersection(const vector3& o, real ro, real ri, real umin = 0, real umax = 1, real vmin = 0, real vmax = 1);
        torus_intersection(const vector3& o, const vector3& x, const vector3& y, real ri);
        ~torus_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    private:
        torus_intersection_imp* imp_;
    };
}

#endif