#ifndef KAZE_CAPSULE_INTERSECTION_H
#define KAZE_CAPSULE_INTERSECTION_H

#include "primitive_intersection.h"

namespace kaze
{

    class capsule_intersection : public primitive_intersection
    {
    public:
        capsule_intersection(const vector3& p0, const vector3& p1, real radius);

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    private:
        vector3 p0_;
        vector3 p1_;
        real r_;

        vector3 min_;
        vector3 max_;
    };

    /*
class capsule_intersection:public bounded_intersection{
	public:
		capsule_intersection(const vector3& p0, real r0, const vector3& p1,real r1);
		bool test(const ray & r, real dist)const;
		bool test(test_info * info, const ray & r, real dist)const;
		void finalize(test_info * info, const ray & r, real dist)const;
		vector3 min()const;
		vector3 max()const;

	private:
		vector3 p0_;
		vector3 p1_;
		real r0_;
		real r1_;

		vector3 min_;
		vector3 max_;
	};

	*/
}

#endif
