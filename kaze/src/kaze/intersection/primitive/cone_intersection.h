#ifndef KAZE_CONE_INTERSECTION_H
#define KAZE_CONE_INTERSECTION_H

#include "primitive_intersection.h"

namespace kaze
{

    class cone_intersection_imp;
    class cone_intersection : public primitive_intersection
    {
    public:
        cone_intersection(const vector3& p0, const vector3& p1, real radius, real umin = 0, real umax = 1);
        cone_intersection(const vector3& p0, const vector3& x, const vector3& y, const vector3& z);
        ~cone_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        cone_intersection_imp* imp_;
    };
}

#endif
