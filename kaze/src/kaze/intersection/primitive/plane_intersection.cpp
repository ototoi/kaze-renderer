#include "plane_intersection.h"
#include "test_info.h"

//#include "logger.h"

#include <vector>

namespace kaze
{
    namespace
    {
        struct uvt
        {
            real u, v, t;
        };
    }

    static bool test_plane_(uvt* info, const vector3& pos_, const vector3& u_, const vector3& v_, const ray& r, real dist)
    {
        static const real EPSILON = values::epsilon() * 1000;
        real u, v, t;

        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& p0 = pos_;

        //-e1 = p0-p1
        vector3 e1 = -u_; //vA

        //-e2 = p0-p2
        vector3 e2 = -v_; //vB

        //dir = GHI

        vector3 bDir(cross(e2, dir));

        real iM = dot(e1, bDir);

        if (fabs(iM) > EPSILON)
        {
            iM = real(1.0) / iM;

            //p0-org
            vector3 vOrg(p0 - org); //JKL
            vector3 vE(cross(e1, vOrg));
            t = -dot(e2, vE) * iM;
            if (t <= 0 || dist <= t) return false;

            u = dot(vOrg, bDir) * iM;
            v = dot(dir, vE) * iM;
            if (u < 0 || 1 < u) return false;
            if (v < 0 || 1 < v) return false;

            info->u = u;
            info->v = v;
            info->t = t;
            return true;
        }
        else
        {
            return false;
        }
    }

    static void get_minmax(vector3& min, vector3& max, const std::vector<vector3>& v)
    {
        min = max = v[0];
        for (size_t i = 0; i < v.size(); i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (min[j] < v[i][j]) min[j] = v[i][j];
                if (max[j] > v[i][j]) max[j] = v[i][j];
            }
        }
    }

    plane_intersection::plane_intersection(const vector3& org, const vector3& u, const vector3& v)
        : o_(org), u_(u), v_(v)
    {
        std::vector<vector3> tmp;
        tmp.push_back(o_);
        tmp.push_back(o_ + u_);
        tmp.push_back(o_ + v_);
        tmp.push_back(o_ + u_ + v_);
        get_minmax(min_, max_, tmp);
        n_ = normalize(cross(u_, v_));
    }

    bool plane_intersection::test(const ray& r, real dist) const
    {
        uvt uvt_info;
        return test_plane_(&uvt_info, o_, u_, v_, r, dist);
    }

    bool plane_intersection::test(test_info* info, const ray& r, real dist) const
    {
        uvt uvt_info;
        if (test_plane_(&uvt_info, o_, u_, v_, r, dist))
        {
            info->distance = uvt_info.t;
            info->coord = vector3(uvt_info.u, uvt_info.v, 0);
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void plane_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        info->position = r.origin() + r.direction() * dist;
        info->normal = n_;
        info->geometric = n_;
    }

    vector3 plane_intersection::min() const
    {
        return min_;
    }

    vector3 plane_intersection::max() const
    {
        return max_;
    }
}
