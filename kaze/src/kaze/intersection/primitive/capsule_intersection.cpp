#include "capsule_intersection.h"
#include "test_info.h"

namespace kaze
{

    struct capsule_freearea_struct
    {
        int side;
    };

    capsule_intersection::capsule_intersection(const vector3& p0, const vector3& p1, real radius) : p0_(p0), p1_(p1), r_(radius)
    {
        static const real EPSILON = values::epsilon() * 1000;

        for (int i = 0; i < 3; i++)
        {
            min_[i] = std::min(p0[i], p1[i]) - radius - EPSILON;
            max_[i] = std::max(p0[i], p1[i]) + radius + EPSILON;
        }
    }

    bool capsule_intersection::test(const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1000;
        if (!test_AABB(min_, max_, r, dist)) return false;

        const vector3& O = r.origin();
        const vector3& D = r.direction();

        real t;
        real b;

        vector3 E(p1_ - p0_);

        real EE = dot(E, E);
        real DE = dot(D, E);

        real RR = r_ * r_;

        real A = EE - DE * DE;

        vector3 F(O - p0_);
        vector3 G(O - p1_);

        real FE = dot(F, E);
        //real GE =  dot(G,E);

        real FF = dot(F, F);
        real GG = dot(G, G);

        if (std::abs(A) < EPSILON)
        {
            if (FE < 0)
            {
                if (FF > RR)
                {
                    if (DE > 0)
                    { //P NEAR
                        b = dot(D, F);
                        t = b * b - (FF - RR);
                        if (t < 0) return false;
                        t = -b - std::sqrt(t);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (DE > 0)
                    { //Q FAR
                        b = dot(D, G);
                        t = b * b - (GG - RR);
                        if (t < 0) return false;
                        t = -b + std::sqrt(t);
                    }
                    else
                    { //P NEAR
                        b = dot(D, F);
                        t = b * b - (FF - RR);
                        if (t < 0) return false;
                        t = -b - std::sqrt(t);
                    }
                }
            }
            else if (EE < FE)
            {
                if (GG > RR)
                {
                    if (DE < 0)
                    { //Q NEAR
                        b = dot(D, G);
                        t = b * b - (GG - RR);
                        if (t < 0) return false;
                        t = -b - std::sqrt(t);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (DE > 0)
                    { //Q NEAR
                        b = dot(D, G);
                        t = b * b - (GG - RR);
                        if (t < 0) return false;
                        t = -b - std::sqrt(t);
                    }
                    else
                    { //P FAR
                        b = dot(D, F);
                        t = b * b - (FF - RR);
                        if (t < 0) return false;
                        t = -b + std::sqrt(t);
                    }
                }
            }
            else
            {
                if (DE < 0)
                { //PFAR
                    b = dot(D, F);
                    t = b * b - (FF - RR);
                    if (t < 0) return false;
                    t = -b + std::sqrt(t);
                }
                else
                { //Q FAR
                    b = dot(D, G);
                    t = b * b - (GG - RR);
                    if (t < 0) return false;
                    t = -b + std::sqrt(t);
                }
            }
            if (t < 0 || dist <= t) return false;
            //info->distance = (t);
            //info->p_intersection = this;
            return true;
        }

        //if(-FE > r_    && DE < 0)return false;
        //if( FE > EE+r_ && DE > 0)return false;

        real C = EE * (FF - RR) - (FE * FE);
        real B = EE * dot(D, F) - DE * FE;

        real D4 = B * B - A * C;
        if (D4 < 0) return false;
        t = (-B - std::sqrt(D4)) / A;

        if (t < 0 || dist <= t) return false;

        //int side = 0;
        real FEtDE = FE + t * DE;
        if (FEtDE < 0)
        {
            if (FF > RR)
            {
                b = dot(D, F);
                t = b * b - (FF - RR);
                if (t < 0) return false;
                t = -b - std::sqrt(t);
                //side = 1;
            }
            else
            {
                return false;
            }
        }
        else if (FEtDE > EE)
        {
            if (GG > RR)
            {
                b = dot(D, G);
                t = b * b - (GG - RR);
                if (t < 0) return false;
                t = -b - std::sqrt(t);
                //side = 2;
            }
            else
            {
                return false;
            }
        }

        if (t < 0 || dist <= t) return false;

        return true;
    }

    bool capsule_intersection::test(test_info* info, const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1000;
        if (!test_AABB(min_, max_, r, dist)) return false;

        const vector3& O = r.origin();
        const vector3& D = r.direction();

        real t;
        real b;

        vector3 E(p1_ - p0_);

        real EE = dot(E, E);
        real DE = dot(D, E);

        real RR = r_ * r_;

        real A = EE - DE * DE;

        vector3 F(O - p0_);
        vector3 G(O - p1_);

        real FE = dot(F, E);
        //real GE =  dot(G,E);

        real FF = dot(F, F);
        real GG = dot(G, G);

        if (std::abs(A) < EPSILON)
        {
            if (FE < 0)
            {
                if (FF > RR)
                {
                    if (DE > 0)
                    { //P NEAR
                        b = dot(D, F);
                        t = b * b - (FF - RR);
                        if (t < 0) return false;
                        t = -b - std::sqrt(t);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (DE > 0)
                    { //Q FAR
                        b = dot(D, G);
                        t = b * b - (GG - RR);
                        if (t < 0) return false;
                        t = -b + std::sqrt(t);
                    }
                    else
                    { //P NEAR
                        b = dot(D, F);
                        t = b * b - (FF - RR);
                        if (t < 0) return false;
                        t = -b - std::sqrt(t);
                    }
                }
            }
            else if (EE < FE)
            {
                if (GG > RR)
                {
                    if (DE < 0)
                    { //Q NEAR
                        b = dot(D, G);
                        t = b * b - (GG - RR);
                        if (t < 0) return false;
                        t = -b - std::sqrt(t);
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    if (DE > 0)
                    { //Q NEAR
                        b = dot(D, G);
                        t = b * b - (GG - RR);
                        if (t < 0) return false;
                        t = -b - std::sqrt(t);
                    }
                    else
                    { //P FAR
                        b = dot(D, F);
                        t = b * b - (FF - RR);
                        if (t < 0) return false;
                        t = -b + std::sqrt(t);
                    }
                }
            }
            else
            {
                if (DE < 0)
                { //PFAR
                    b = dot(D, F);
                    t = b * b - (FF - RR);
                    if (t < 0) return false;
                    t = -b + std::sqrt(t);
                }
                else
                { //Q FAR
                    b = dot(D, G);
                    t = b * b - (GG - RR);
                    if (t < 0) return false;
                    t = -b + std::sqrt(t);
                }
            }
            if (t < 0 || dist <= t) return false;
            info->distance = (t);
            info->p_intersection = this;
            return true;
        }

        //if(-FE > r_    && DE < 0)return false;
        //if( FE > EE+r_ && DE > 0)return false;

        real C = EE * (FF - RR) - (FE * FE);
        real B = EE * dot(D, F) - DE * FE;

        real D4 = B * B - A * C;
        if (D4 < 0) return false;
        t = (-B - std::sqrt(D4)) / A;

        if (t < 0 || dist <= t) return false;

        int side = 0;
        real FEtDE = FE + t * DE;
        if (FEtDE < 0)
        {
            if (FF > RR)
            {
                b = dot(D, F);
                t = b * b - (FF - RR);
                if (t < 0) return false;
                t = -b - std::sqrt(t);
                side = 1;
            }
            else
            {
                return false;
            }
        }
        else if (FEtDE > EE)
        {
            if (GG > RR)
            {
                b = dot(D, G);
                t = b * b - (GG - RR);
                if (t < 0) return false;
                t = -b - std::sqrt(t);
                side = 2;
            }
            else
            {
                return false;
            }
        }

        if (t < 0 || dist <= t) return false;

        info->distance = (t);
        info->p_intersection = this;
        reinterpret_cast<capsule_freearea_struct*>(info->freearea)->side = side;

        return true;
    }

    void capsule_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        vector3 pos = r.origin() + info->distance * r.direction();
        info->position = (pos);

        const capsule_freearea_struct* fsp = reinterpret_cast<const capsule_freearea_struct*>(info->freearea);
        int side = fsp->side;
        if (side)
        {
            vector3 V;
            if (side == 1)
            {
                V = pos - p0_;
            }
            else
            {
                V = pos - p1_;
            }
            vector3 N = normalize(V);

            //if(dot(r.direction(),N)<0){
            info->normal = N;
            info->geometric = N;
            //}else{
            //	info->normal = (-N);
            //}
        }
        else
        {

            vector3 V = pos - p0_;
            vector3 D = p1_ - p0_;
            vector3 W = (dot(V, D) / dot(D, D)) * D;
            vector3 N = normalize(V - W);

            info->normal = N;
            info->geometric = N;
        }
    }

    vector3 capsule_intersection::min() const
    {
        return vector3();
    }

    vector3 capsule_intersection::max() const
    {
        return vector3();
    }
}

#if 0
nemespace kaze{
	//-----------------------------------
	//CAPSULE
	//-----------------------------------
	struct capsule_freearea_struct{
		real k;
	};

	capsule_intersection::capsule_intersection(const vector3& p0, real r0, const vector3& p1, real r1):p0_(p0),p1_(p1),r0_(r0),r1_(r1){
		min_ = min();
		max_ = max();
	}

	bool capsule_intersection::test(const ray & r, real dist)const{
		//static const real EPSILON = values::epsilon() * 1000;

		if(!test_AABB(min_,max_,r,dist))return false;

		const vector3&  O = r.origin();
		const vector3&  D = r.direction();

		real t,k;

		real dr = r1_ - r0_;

		vector3 E(p1_ - p0_);
		vector3 F(p0_ - O  );
		
		real ED = dot(E,D);
		real FD = dot(F,D);
		
		real A = ((ED*ED) - dot(E,E)) + (dr  *  dr );
		real B = ((ED*FD) - dot(E,F)) + (r0_ *  dr );
		real C = ((FD*FD) - dot(F,F)) + (r0_ *  r0_);

		real D4;
		//real axis =  -B/A;

		if(A > 0){
			if(-2*B<A){
				k = 1;
				D4 = A+2*B+C;
			}else{
				k = 0;
				D4 = C;
			}
		}else{
			if(B < 0){//if(axis < 0){
				k = 0;
				D4 = C;				
			}else if(B < -A){//if(axis <= 1){//
				k = -B/A;
				D4 = k*B+C;				
			}else{
				k = 1;
				D4 = A+2*B+C;
			}
		}

		if(D4 < 0)return false;

		real KX = FD+k*ED;
		real SQ = std::sqrt(D4); 

		if(KX > SQ){
			t = KX - SQ;
		}else{
			t = KX + SQ;
		}

		if(t < 0 || dist <= t)return false;

		//info->distance = (t);
		//info->p_intersection = this;

		//capsule_freearea_struct * fsp = reinterpret_cast<capsule_freearea_struct*>(info->freearea);
		//fsp->k = k;

		return true;
	}

	bool capsule_intersection::test(test_info * info, const ray & r, real dist)const{
		static const real EPSILON = values::epsilon() * 1000;

		const vector3&  O = r.origin();
		const vector3&  D = r.direction();

		real t,k;

		real rd = r1_ - r0_;

		vector3 E(p1_ - p0_);
		vector3 F(p0_ - O  );

		real ED = dot(E,D);
		real EF = dot(E,F);


		
		real FD = dot(F,D);

		
		real D4;
		
		//real axis =  -B/A;

		
		real A = ((ED*ED) - dot(E,E)) + (rd  * rd );
		real B = ((ED*FD) - dot(E,F)) + (r0_ * rd );
		real C = ((FD*FD) - dot(F,F)) + (r0_ * r0_);

		if(A >= 0){			
			if(-2*B<=A){
				k = 1;
				D4 = A+2*B+C;
			}else{
				k = 0;
				D4 = C;
			}

		}else{		

			if(B < 0){//if(axis < 0){//OK
				//return false;
				k = 0;
				D4 = C;
				if(D4 < 0)return false;
				real KX = FD+k*ED;
				real SQ = std::sqrt(D4); 

				
				t = KX - SQ;
			}else if(B <= -A){//if(axis <= 1){//UGAH!
				//return false;
				if(-2*B>A){
					k = 1;
					D4 = A+2*B+C;
				}else{
					k = 0;
					D4 = C;
				}
				//k = -B/A;
				//D4 = k*B+C;
				//D4 = A+2*B+C;
				if(D4 < 0)return false;
				real KX = FD+k*ED;
				real SQ = std::sqrt(D4); 

				
				t = KX - SQ;
			}else{//OK
				//return false;
				k = 1;
				D4 = A+2*B+C;
				if(D4 < 0)return false;
				real KX = FD+k*ED;
				real SQ = std::sqrt(D4); 

				
				t = KX - SQ;

			}

		}		


		//real KX = FD+k*ED;
		//real SQ = std::sqrt(D4); 

		//if(KX > SQ){
		//	t = KX - SQ;
		//}else{
		//	t = KX + SQ;
		//}

		if(t < 0 || dist <= t)return false;

		info->distance = (t);
		capsule_freearea_struct * fsp = reinterpret_cast<capsule_freearea_struct*>(info->freearea);
		fsp->k = k;
		
		info->p_intersection = this;

		

		return true;		
	}

	void capsule_intersection::finalize(test_info * info, const ray & r, real dist)const{
		capsule_freearea_struct * fsp = reinterpret_cast<capsule_freearea_struct*>(info->freearea);
		real k = fsp->k;
		real rk = k * (r1_-r0_) + r0_;
		vector3 posk = k *(p1_-p0_) + p0_;
		vector3 pos = r.origin() + info->distance * r.direction();
		vector3 n = (pos-posk)*real(1)/rk;

		info->position = (pos);
		info->normal = (n);
		//info->normal = (-r.direction());
	}

	vector3 capsule_intersection::min()const{
		vector3 ret(values::far(),values::far(),values::far());

		for(int i = 0; i < 3; i++){
			real m0 = p0_[i]-r0_;
			if(m0<ret[i])ret[i]=m0;
			real m1 = p1_[i]-r1_;
			if(m1<ret[i])ret[i]=m1;
		}

		return ret;
	}

	vector3 capsule_intersection::max()const{
		vector3 ret(-values::far(),-values::far(),-values::far());

		for(int i = 0; i < 3; i++){
			real m0 = p0_[i]+r0_;
			if(m0>ret[i])ret[i]=m0;
			real m1 = p1_[i]+r1_;
			if(m1>ret[i])ret[i]=m1;
		}

		return ret;
	}

}

#endif
