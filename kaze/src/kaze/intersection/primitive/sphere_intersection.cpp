#include "sphere_intersection.h"
#include "test_info.h"
#include "values.h"
#include "types.h"
#include <cmath>

namespace kaze
{
#if 1
    namespace
    {

        inline static vector3 safe_normalize(const vector3& n)
        {
            static const real EPSILON = values::epsilon();
            real l2 = n.sqr_length();
            if (l2 < EPSILON) return vector3(0, 0, 0);
            return n * real(1) / std::sqrt(l2);
        }

        struct sphere_freearea_struct
        {
            bool inside;
        };

        struct sphere_coord_struct
        {
            real u;
            real v;
            vector3 dpdu;
            vector3 dpdv;
        };

        inline real adjust_rad(real x)
        {
            using namespace std;
            static const real PI_ = values::pi();
            if (x < 0) x += 2 * PI_;
            return x;
        }

        //[--------------------------------------------------------------

        //[--------------------------------------------------------------

        void calc_coord(
            sphere_coord_struct* nf,
            const vector3& np)
        {
            using namespace std;

            static const vector3 x = vector3(1, 0, 0);
            static const vector3 y = vector3(0, 1, 0);
            static const vector3 z = vector3(0, 0, 1);

            real xx = np[0];
            real yy = np[1];
            real zz = np[2];
            real xy = sqrt(xx * xx + yy * yy);

            real theta = adjust_rad(atan2(yy, xx));
            real phi = atan2(zz, xy);

            assert(0 <= theta && theta <= 2 * values::pi());
            assert(-values::pi() / 2 <= phi && phi <= values::pi() / 2);

            real u = theta / (2 * values::pi());
            real v = 0.5 - phi / (values::pi()); //0.5 * [0...2]

            assert(0 <= u && u <= 1);
            assert(0 <= v && v <= 1);

            vector3 dpdu, dpdv;

            if (dot(z, np) >= 1)
            {
                dpdu = vector3(0, 0, 0);
                dpdv = vector3(0, 0, 0);
            }
            else
            {
                dpdu = cross(z, np);
                dpdv = cross(np, dpdu);
            }

            nf->u = u;
            nf->v = v;
            nf->dpdu = dpdu;
            nf->dpdv = dpdv;
        }

        bool test_sphere_with_radius(
            const ray& r, real dist,
            const vector3& center, //center
            real radius            //radius
            )
        {
            vector3 rs = r.origin() - center;

            double B = dot(rs, r.direction());
            double C = dot(rs, rs) - radius * radius;
            double D = B * B - C;

            if (D > 0.0)
            {
                double t = -B - sqrt(D);

                if ((t > 0.0) && (t < dist))
                {
                    //info->distance = t;
                    //reinterpret_cast<sphere_freearea_struct*>(info->freearea)->inside = false;
                    return true;
                }
            }

            return false;
        }

        bool test_sphere_with_radius(
            test_info* info,
            const ray& r, real dist,
            const vector3& center, //center
            real radius            //radius
            )
        {
            vector3 rs = r.origin() - center;

            double B = dot(rs, r.direction());
            double C = dot(rs, rs) - radius * radius;
            double D = B * B - C;

            if (D > 0.0)
            {
                double t = -B - sqrt(D);

                if ((t > 0.0) && (t < dist))
                {
                    info->distance = t;
                    reinterpret_cast<sphere_freearea_struct*>(info->freearea)->inside = false;
                    return true;
                }
            }

            return false;
        }

        void finalize_sphere_with_radius(
            test_info* info,
            const ray& r, real dist,
            const vector3& center, //center
            real radius            //radius
            )
        {
            vector3 pos = r.origin() + dist * r.direction();

            vector3 normal = normalize(pos - center); //positive;

            bool inside = reinterpret_cast<sphere_freearea_struct*>(info->freearea)->inside;
            if (!inside)
            {
                info->geometric = normal;
            }
            else
            {
                info->geometric = -normal; //negative
            }

            info->position = pos;
            info->normal = normal;
        }
    }

    class classic_sphere_intersection_imp
    {
    public:
        classic_sphere_intersection_imp(const vector3& o, real r);
        //~classic_sphere_intersection_imp();
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        vector3 center_;
        real r_;
    };

    classic_sphere_intersection_imp::classic_sphere_intersection_imp(const vector3& o, real r) : center_(o), r_(r) {}

    bool classic_sphere_intersection_imp::test(const ray& r, real dist) const
    {
        return test_sphere_with_radius(r, dist, center_, r_);
    }

    bool classic_sphere_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        return test_sphere_with_radius(info, r, dist, center_, r_);
    }

    void classic_sphere_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        finalize_sphere_with_radius(info, r, dist, center_, r_);
    }

    vector3 classic_sphere_intersection_imp::min() const { return center_ - vector3(r_, r_, r_); }
    vector3 classic_sphere_intersection_imp::max() const { return center_ + vector3(r_, r_, r_); }

    //-----------------------------------------------------------------------------------------

    classic_sphere_intersection::classic_sphere_intersection(const vector3& o, real r)
    {
        imp_ = new classic_sphere_intersection_imp(o, r);
    }
    classic_sphere_intersection::~classic_sphere_intersection()
    {
        delete imp_;
    }
    bool classic_sphere_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }
    bool classic_sphere_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }
    void classic_sphere_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        imp_->finalize(info, r, dist);
    }
    vector3 classic_sphere_intersection::min() const
    {
        return imp_->min();
    }
    vector3 classic_sphere_intersection::max() const
    {
        return imp_->max();
    }
#endif
}
