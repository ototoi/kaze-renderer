#include "primitive_intersection_ex.h"

#include "newton_solver.h"
#include "values.h"

#include <cmath>

namespace kaze
{

    static const real EPSILON = values::epsilon() * 1000;

    static real clamp(real x, real min, real max)
    {
        return std::max<real>(min, std::min<real>(x, max));
    }

    static inline real dot2(const vector3& a, const vector3& b)
    {
        return a[0] * b[0] + a[1] * b[1];
    }
    static inline real dot_sub_z(const vector3& a, const vector3& b)
    {
        return a[0] * b[0] + a[1] * b[1] - a[2] * b[2];
    }
    static inline bool is_range(real tmin, real t, real tmax)
    {
        return (tmin <= t && t <= tmax);
    }

    static inline real adjust_rad(real x)
    {
        static const real PI_ = values::pi();
        if (x < 0) x += 2 * PI_;
        return x;
    }

    static int solve(double root[], const double coeff[], int n, double x0, double x1)
    {
        static newton_solver ns;
        return ns.solve(root, coeff, n, x0, x1);
    }

    static int solve2(double root[], double A, double B, double C, double x0, double x1)
    {
        double coef[] = {A, B, C};
        return solve(root, coef, 2, x0, x1);
    }

    static int solve4(double root[], double A, double B, double C, double D, double E, double x0, double x1)
    {
        double coef[] = {A, B, C, D, E};
        return solve(root, coef, 4, x0, x1);
    }

    //----------------------------------------

    bool test_quad(primitive_test_info* info, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        return test_quad(info, 0, 1, 0, 1, org, dir, tmin, tmax);
    }

    bool test_quad(primitive_test_info* info, real u0, real u1, real v0, real v1, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        real t = -org[2] / dir[2];
        if (t < tmin || tmax < t) return false;
        real x = org[0] + t * dir[0];
        real y = org[1] + t * dir[1];
        if (x < -1 || 1 < x) return false;
        if (y < -1 || 1 < y) return false;

        real u = (x + 1) * 0.5;
        real v = (y + 1) * 0.5;
        if (u < u0 || u1 < u) return false;
        if (v < v0 || v1 < v) return false;

        vector3 dpdv = -vector3(x, y, 0);
        vector3 dpdu = cross(dpdv, vector3(0, 0, 1));

        info->u = u;
        info->v = v;
        info->w = 0;
        info->t = t;

        info->dpdu = dpdu;
        info->dpdv = dpdv;
        info->n = vector3(0, 0, 1);

        return true;
    }

    bool test_disk(primitive_test_info* info, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        return test_disk(info, 0, 1, 0, 1, org, dir, tmin, tmax);
    }

    bool test_disk(primitive_test_info* info, real u0, real u1, real v0, real v1, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        real t = -org[2] / dir[2];
        if (t < tmin || tmax < t) return false;
        real x = org[0] + t * dir[0];
        real y = org[1] + t * dir[1];
        real l = x * x + y * y;
        if (l > 1) return false;
        real arc = atan2(y, x);
        if (arc < 0) arc += 2 * values::pi();
        real u = arc / (2 * values::pi());
        real v = 1 - sqrt(l);
        if (u < u0 || u1 < u) return false;
        if (v < v0 || v1 < v) return false;

        vector3 dpdv = -vector3(x, y, 0);
        vector3 dpdu = cross(dpdv, vector3(0, 0, 1));

        info->u = u;
        info->v = v;
        info->w = 0;
        info->t = t;

        info->dpdu = dpdu;
        info->dpdv = dpdv;
        info->n = vector3(0, 0, 1);

        return true;
    }

    bool test_cylinder(primitive_test_info* info, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        return test_cylinder(info, 0, 1, 0, 1, org, dir, tmin, tmax);
    }

    bool test_cylinder(primitive_test_info* info, real u0, real u1, real v0, real v1, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        real A = dot2(dir, dir);
        real B = 2 * dot2(org, dir);
        real C = dot2(org, org) - 1;

        double tt[2];
        int nRet = solve2(tt, A, B, C, tmin, tmax);
        if (nRet)
        {
            for (int k = 0; k < nRet; k++)
            {
                real t = tt[k];
                real z = org[2] + t * dir[2];

                if (z < 0 || 1 < z) continue;

                real x = org[0] + t * dir[0];
                real y = org[1] + t * dir[1];
                real arc = atan2(y, x);
                if (arc < 0) arc += 2 * values::pi();
                real u = arc / (2 * values::pi());
                real v = z;

                if (u < u0 || u1 < u) continue;
                if (v < v0 || v1 < v) continue;

                vector3 dpdv = vector3(0, 0, 1);
                vector3 n = vector3(x, y, 0);
                vector3 dpdu = cross(dpdv, n);

                info->u = u;
                info->v = v;
                info->w = 0;
                info->t = t;

                info->dpdu = dpdu;
                info->dpdv = dpdv;
                info->n = n;

                return true;
            }
        }
        return false;
    }

    bool test_cone(primitive_test_info* info, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        return test_cone(info, 0, 1, 0, 1, org, dir, tmin, tmax);
    }

    bool test_cone(primitive_test_info* info, real u0, real u1, real v0, real v1, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        vector3 xOrg = org - vector3(0, 0, 1);
        real A = dot_sub_z(dir, dir);
        real B = 2 * dot_sub_z(xOrg, dir);
        real C = dot_sub_z(xOrg, xOrg);

        double tt[2];
        int nRet = solve2(tt, A, B, C, tmin, tmax);
        if (nRet)
        {
            for (int k = 0; k < nRet; k++)
            {
                real t = tt[k];
                real z = org[2] + t * dir[2];

                if (z < 0 || 1 < z) continue;

                real x = org[0] + t * dir[0];
                real y = org[1] + t * dir[1];
                real arc = atan2(y, x);
                if (arc < 0) arc += 2 * values::pi();
                real u = arc / (2 * values::pi());
                real v = z;

                if (u < u0 || u1 < u) continue;
                if (v < v0 || v1 < v) continue;

                vector3 n = vector3(x, y, sqrt(x * x + y * y));
                vector3 dpdu = cross(vector3(0, 0, 1), vector3(x, y, z));
                vector3 dpdv = cross(n, dpdu);

                info->u = u;
                info->v = v;
                info->w = 0;
                info->t = t;

                info->dpdu = dpdu;
                info->dpdv = dpdv;
                info->n = n;

                return true;
            }
        }
        return false;
    }

    bool test_sphere(primitive_test_info* info, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        return test_sphere(info, 0, 1, 0, 1, org, dir, tmin, tmax);
    }

    bool test_sphere(primitive_test_info* info, real u0, real u1, real v0, real v1, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        real A = dot(dir, dir);
        real B = 2 * dot(dir, org);
        real C = dot(org, org) - 1;

        double tt[2];
        int nRet = solve2(tt, A, B, C, tmin, tmax);
        if (nRet)
        {
            for (int k = 0; k < nRet; k++)
            {
                real t = tt[k];

                real x = org[0] + t * dir[0];
                real y = org[1] + t * dir[1];
                real z = org[2] + t * dir[2];

                real xy = sqrt(x * x + y * y);

                real theta = adjust_rad(atan2(y, x));
                real phi = atan2(z, xy); //-

                real u = theta / (2 * values::pi());
                real v = phi / (values::pi()) + 0.5; //0.5 * [0...2]

                if (u < u0 || u1 < u) continue;
                if (v < v0 || v1 < v) continue;

                vector3 n = vector3(x, y, z);
                vector3 dpdu = cross(vector3(0, 0, 1), n);
                vector3 dpdv = cross(n, dpdu);

                info->u = u;
                info->v = v;
                info->w = 0;
                info->t = t;

                info->dpdu = dpdu;
                info->dpdv = dpdv;
                info->n = n;

                return true;
            }
        }
        return false;
    }

    //--------------------------------------------------------------------------------------------------------------------------

    bool test_torus(primitive_test_info* info, real ro, real ri, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        return test_torus(info, ro, ri, 0, 1, 0, 1, org, dir, tmin, tmax);
    }

    bool test_torus(primitive_test_info* info, real ro, real ri, real u0, real u1, real v0, real v1, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        real ro2 = ro * ro;
        real ri2 = ri * ri;

        double rd2x = dir[0] * dir[0];
        double rd2y = dir[1] * dir[1];
        double rd2z = dir[2] * dir[2];
        double ro2x = org[0] * org[0];
        double ro2y = org[1] * org[1];
        double ro2z = org[2] * org[2];

        // compute some common factors
        double alpha = rd2x + rd2y + rd2z; //dot(dir,dir);
        double beta = 2 * dot(org, dir);
        double gamma = (ro2x + ro2y + ro2z) - ri2 - ro2;
        // setup quartic coefficients
        double A = alpha * alpha;
        double B = 2 * alpha * beta;
        double C = beta * beta + 2 * alpha * gamma + 4 * ro2 * rd2z;
        double D = 2 * beta * gamma + 8 * ro2 * org[2] * dir[2];
        double E = gamma * gamma + 4 * ro2 * ro2z - 4 * ro2 * ri2;

        double ret[4];
        int nRet = solve4(ret, A, B, C, D, E, tmin, tmax);
        if (nRet)
        {
            std::sort(ret, ret + nRet);

            for (int k = 0; k < nRet; k++)
            {
                real t = ret[k];
                vector3 p = org + t * dir;
                real deriv = dot(p, p) - ri2 - ro2;

                real x = p[0];
                real y = p[1];
                real z = p[2];

                real l = (sqrt(x * x + y * y) - ro) / ri;

                //double phi = asin(clamp(z / ri, -1, 1));
                double phi = adjust_rad(atan2(clamp(z / ri, -1, 1), l));
                double theta = adjust_rad(atan2(y, x));

                real u = theta / (2 * values::pi());
                real v = phi / (2 * values::pi());
                //real v = (phi + values::pi() / 2) / values::pi();

                if (u < u0 || u1 < u) continue;
                if (v < v0 || v1 < v) continue;

                vector3 n = normalize(p * deriv + vector3(0, 0, 2 * ro2 * z));
                vector3 dpdu = cross(vector3(0, 0, 1), normalize(vector3(x, y, 0)));
                vector3 dpdv = cross(n, dpdu);

                info->u = u;
                info->v = v;
                info->w = 0;
                info->t = t;

                info->dpdu = dpdu;
                info->dpdv = dpdv;
                info->n = n;

                return true;
            }
        }

        return false;
    }
}
