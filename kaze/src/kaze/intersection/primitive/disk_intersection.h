#ifndef KAZE_DISK_INTERSECTION_H
#define KAZE_DISK_INTERSECTION_H

#include "primitive_intersection.h"

namespace kaze
{

    class disk_intersection_imp;
    class disk_intersection : public primitive_intersection
    {
    public:
        disk_intersection(const vector3& p0, real radius, real umin = 0, real umax = 1);
        disk_intersection(const vector3& p0, const vector3& x, const vector3& y, real umin = 0, real umax = 1);
        ~disk_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        disk_intersection_imp* imp_;
    };
}

#endif