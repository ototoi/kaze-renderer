#ifndef KAZE_ELLIPSOID_INTERSECTION_H
#define KAZE_ELLIPSOID_INTERSECTION_H

#include "primitive_intersection.h"

namespace kaze
{

    class ellipsoid_intersection_imp;
    class ellipsoid_intersection : public primitive_intersection
    {
    public:
        ellipsoid_intersection(const vector3& o, real r, real umin = 0, real umax = 1, real vmin = 0, real vmax = 1);
        ellipsoid_intersection(const vector3& o, const vector3& x, const vector3& y, const vector3& z);
        ~ellipsoid_intersection();
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        ellipsoid_intersection_imp* imp_;
    };
}

#endif