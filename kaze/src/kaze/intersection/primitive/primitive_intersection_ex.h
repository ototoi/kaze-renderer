#ifndef KAZE_PRIMITIVE_INTERSECTION_EX_H
#define KAZE_PRIMITIVE_INTERSECTION_EX_H

#include "types.h"

namespace kaze
{

    struct primitive_test_info
    {
        real u;
        real v;
        real w;
        real t;
        vector3 dpdu;
        vector3 dpdv;
        vector3 n; //n = cross(dpdu,dpdv)
    };

    bool test_quad(primitive_test_info* info, const vector3& org, const vector3& dir, real tmin, real tmax);
    bool test_quad(primitive_test_info* info, real u0, real u1, real v0, real v1, const vector3& org, const vector3& dir, real tmin, real tmax);

    bool test_disk(primitive_test_info* info, const vector3& org, const vector3& dir, real tmin, real tmax);
    bool test_disk(primitive_test_info* info, real u0, real u1, real v0, real v1, const vector3& org, const vector3& dir, real tmin, real tmax);

    bool test_cylinder(primitive_test_info* info, const vector3& org, const vector3& dir, real tmin, real tmax);
    bool test_cylinder(primitive_test_info* info, real u0, real u1, real v0, real v1, const vector3& org, const vector3& dir, real tmin, real tmax);

    bool test_cone(primitive_test_info* info, const vector3& org, const vector3& dir, real tmin, real tmax);
    bool test_cone(primitive_test_info* info, real u0, real u1, real v0, real v1, const vector3& org, const vector3& dir, real tmin, real tmax);

    bool test_sphere(primitive_test_info* info, const vector3& org, const vector3& dir, real tmin, real tmax);
    bool test_sphere(primitive_test_info* info, real u0, real u1, real v0, real v1, const vector3& org, const vector3& dir, real tmin, real tmax);

    bool test_torus(primitive_test_info* info, real ro, real ri, const vector3& org, const vector3& dir, real tmin, real tmax);
    bool test_torus(primitive_test_info* info, real ro, real ri, real u0, real u1, real v0, real v1, const vector3& org, const vector3& dir, real tmin, real tmax);
}

#endif