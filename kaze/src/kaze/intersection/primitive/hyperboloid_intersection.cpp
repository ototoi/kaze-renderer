#include "hyperboloid_intersection.h"
#include "test_info.h"
#include "newton_solver.h"

#include <math.h>
#include <utility>

namespace kaze
{

    static real dot2(const vector3& a, const vector3& b)
    {
        return a[0] * b[0] + a[1] * b[1];
    }

    static inline real dot_sub_z(const vector3& a, const vector3& b)
    {
        return a[0] * b[0] + a[1] * b[1] - a[2] * b[2];
    }

    static inline real adjust_rad(real x)
    {
        static const real PI_ = values::pi();
        if (x < 0) x += 2 * PI_;
        return x;
    }

    static int solve2(double root[2], const double coeff[3], double x0, double x1)
    {
        static newton_solver ns;
        return ns.solve2(root, coeff, x0, x1);
    }

#ifdef _MSC_VER
    static bool isnan(double x)
    {
        return _isnan(x) != 0;
    }
#endif

    static bool isinf(double x)
    {
#ifdef _MSC_VER
        return _finite(x) == 0;
#else
        return finite(x) == 0;
#endif
    }

    class hyperboloid_intersection_imp
    {
    public:
        hyperboloid_intersection_imp(const vector3& point1, const vector3& point2, real umax);

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    protected:
        real a_;
        //real b_;
        real c_;
        real rmax_;
        real zmin_;
        real zmax_;
        real umax_;
        vector3 p1_;
        vector3 p2_;
    };

    static real div(real a, real b)
    {
        if (a == b)
            return 0;
        else
            return a / b;
    }

    hyperboloid_intersection_imp::hyperboloid_intersection_imp(const vector3& point1, const vector3& point2, real umax)
    {
        vector3 p1 = point1;
        vector3 p2 = point2;

        real rad_1 = sqrt(dot2(p1, p1));
        real rad_2 = sqrt(dot2(p2, p2));
        real rmax = std::max(rad_1, rad_2);
        real zmin = std::min(p1[2], p2[2]);
        real zmax = std::max(p1[2], p2[2]);

        //
        if (p2[0] == 0.0) std::swap(p1, p2);

        p1_ = p1;
        p2_ = p2;

        vector3 pp = p1;
        real a = 0.0;
        real b = 0.0;
        real c = 0.0;
        do
        {
            pp += 2.0 * (p2 - p1);
            real xy1 = dot2(pp, pp);
            real xy2 = dot2(p2, p2);
            real pp2 = pp[2] * pp[2];
            real p22 = p2[2] * p2[2];
            real a0 = (div(1, xy1) - div(pp2, (xy1 * p22)));
            real a1 = (1 - div((xy2 * pp2), (xy1 * p22)));
            a = div(a0, a1);
            c = (a * xy2 - 1) / p22;
        } while (isinf(a));
        a_ = a;
        c_ = c;
        rmax_ = rmax;
        zmin_ = zmin;
        zmax_ = zmax;
        umax_ = umax;
    }

    bool hyperboloid_intersection_imp::test(const ray& r, real dist) const
    {
        static const real PI2 = 2.0 * values::pi();

        vector3 org = r.origin();
        vector3 dir = r.direction();
        vector3 p1 = p1_;
        vector3 p2 = p2_;

        real zmin = zmin_;
        real zmax = zmax_;
        real umin = 0;
        real umax = umax_;
        real vmin = 0;
        real vmax = 1;

        real a = a_;
        real c = c_;

        real A = (a * dir[0] * dir[0] + a * dir[1] * dir[1] - c * dir[2] * dir[2]);
        real B = 2 * (a * dir[0] * org[0] + a * dir[1] * org[1] - c * dir[2] * org[2]);
        real C = (a * org[0] * org[0] + a * org[1] * org[1] - c * org[2] * org[2]) - 1;

        double coeff[] = {A, B, C};

        double root[2] = {};
        int nRet = solve2(root, coeff, 0, dist);
        for (int i = 0; i < nRet; i++)
        {
            real t = root[i];
            vector3 p = r.origin() + t * r.direction();
            real v = (p[2] - p1[2]) / (p2[2] - p1[2]);
            vector3 pr = (1 - v) * p1 + v * p2;
            real phi = adjust_rad(atan2(pr[0] * p[1] - p[0] * pr[1], p[0] * pr[0] + p[1] * pr[1]));
            real u = phi / PI2;

            if (u < umin || umax < u) continue;
            if (v < vmin || vmax < v) continue;
            if (p[2] < zmin || zmax < p[2]) continue;

            return true;
        }
        return false;
    }

    bool hyperboloid_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        static const real PI2 = 2.0 * values::pi();

        vector3 org = r.origin();
        vector3 dir = r.direction();
        vector3 p1 = p1_;
        vector3 p2 = p2_;

        real zmin = zmin_;
        real zmax = zmax_;
        real umin = 0;
        real umax = umax_;
        real vmin = 0;
        real vmax = 1;

        real a = a_;
        real c = c_;

        real A = (a * dir[0] * dir[0] + a * dir[1] * dir[1] - c * dir[2] * dir[2]);
        real B = 2 * (a * dir[0] * org[0] + a * dir[1] * org[1] - c * dir[2] * org[2]);
        real C = (a * org[0] * org[0] + a * org[1] * org[1] - c * org[2] * org[2]) - 1;

        double coeff[] = {A, B, C};

        double root[2] = {};
        int nRet = solve2(root, coeff, 0, dist);
        for (int i = 0; i < nRet; i++)
        {
            real t = root[i];
            vector3 p = r.origin() + t * r.direction();
            real v = (p[2] - p1[2]) / (p2[2] - p1[2]);
            vector3 pr = (1 - v) * p1 + v * p2;
            real phi = adjust_rad(atan2(pr[0] * p[1] - p[0] * pr[1], p[0] * pr[0] + p[1] * pr[1]));
            real u = phi / PI2;

            if (u < umin || umax < u) continue;
            if (v < vmin || vmax < v) continue;
            if (p[2] < zmin || zmax < p[2]) continue;

            info->distance = t;
            info->position = p;
            info->coord = vector3(u, v, 0);

            return true;
        }
        return false;
    }

    void hyperboloid_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        static const real PI2 = 2.0 * values::pi();

        vector3 p1 = p1_;
        vector3 p2 = p2_;
        vector3 p = info->position;
        real u = info->coord[0];
        real phi = PI2 * u;

        real cosphi = cos(phi);
        real sinphi = sin(phi);

        vector3 dpdu = normalize(vector3(-p[1], +p[0], 0));
        vector3 dd = p2 - p1;
        real xx = dd[0] * cosphi - dd[1] * sinphi;
        real yy = dd[0] * sinphi + dd[1] * cosphi;
        real zz = dd[2];
        vector3 dpdv = normalize(vector3(xx, yy, zz));
        vector3 n = cross(dpdu, dpdv);

        info->normal = n;
        info->geometric = n;
        info->tangent = dpdu;
        info->binormal = dpdv;
    }

    vector3 hyperboloid_intersection_imp::min() const
    {
        return vector3(-rmax_, -rmax_, zmin_);
    }

    vector3 hyperboloid_intersection_imp::max() const
    {
        return vector3(+rmax_, +rmax_, zmax_);
    }

    hyperboloid_intersection::hyperboloid_intersection(const vector3& point1, const vector3& point2, real umax)
    {
        imp_ = new hyperboloid_intersection_imp(point1, point2, umax);
    }

    hyperboloid_intersection::~hyperboloid_intersection()
    {
        delete imp_;
    }

    bool hyperboloid_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool hyperboloid_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void hyperboloid_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        imp_->finalize(info, r, dist);
    }

    vector3 hyperboloid_intersection::min() const
    {
        return imp_->min();
    }

    vector3 hyperboloid_intersection::max() const
    {
        return imp_->max();
    }
}
