#ifndef KAZE_BILLBOARD_INTERSECTION_H
#define KAZE_BILLBOARD_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class disk_billbord_intersection : public bounded_intersection
    {
    public:
        disk_billbord_intersection(const vector3& center, real radius);

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const { return center_ - vector3(radius_, radius_, radius_); }
        vector3 max() const { return center_ + vector3(radius_, radius_, radius_); }
    private:
        vector3 center_;
        real radius_;
    };
}

#endif
