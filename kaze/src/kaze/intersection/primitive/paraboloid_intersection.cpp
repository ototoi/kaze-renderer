#include "paraboloid_intersection.h"
#include "test_info.h"
#include "newton_solver.h"

#include <math.h>

namespace kaze
{

    static real dot2(const vector3& a, const vector3& b)
    {
        return a[0] * b[0] + a[1] * b[1];
    }

    static inline real adjust_rad(real x)
    {
        static const real PI_ = values::pi();
        if (x < 0) x += 2 * PI_;
        return x;
    }

    static int solve2(double root[2], const double coeff[3], double x0, double x1)
    {
        static newton_solver ns;
        return ns.solve2(root, coeff, x0, x1);
    }

    class paraboloid_intersection_imp
    {
    public:
        paraboloid_intersection_imp();
        paraboloid_intersection_imp(real radius, real zmin, real zmax, real umax);

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    protected:
        real radius_;
        real zmin_;
        real zmax_;
        real umax_;
    };

    paraboloid_intersection_imp::paraboloid_intersection_imp()
        : radius_(1.0), zmin_(0.0), zmax_(1.0), umax_(1.0)
    {
        ;
    }

    paraboloid_intersection_imp::paraboloid_intersection_imp(real radius, real zmin, real zmax, real umax)
        : radius_(radius), zmin_(zmin), zmax_(zmax), umax_(umax)
    {
        ;
    }

    bool paraboloid_intersection_imp::test(const ray& r, real dist) const
    {
        static const real PI2 = 2.0 * values::pi();

        real radius = radius_;
        real zmin = zmin_;
        real zmax = zmax_;
        real umin = 0.0;
        real umax = umax_;
        real vmin = zmin / zmax;
        real vmax = 1.0;

        const vector3& org = r.origin();
        const vector3& dir = r.direction();

        double k = zmax / (radius * radius);
        double A = k * dot2(dir, dir);
        double B = 2 * k * dot2(dir, org) - dir[2];
        double C = k * dot2(org, org) - org[2];

        double coeff[] = {A, B, C};

        double root[2] = {};
        int nRet = solve2(root, coeff, 0, dist);
        for (int i = 0; i < nRet; i++)
        {
            real t = root[i];
            vector3 p = r.origin() + t * r.direction();

            real phi = adjust_rad(atan2(p[1], p[0]));
            real u = phi / PI2;
            real v = p[2] / zmax;
            if (u < umin || umax < u) continue;
            if (v < vmin || vmax < v) continue;

            return true;
        }

        return false;
    }

    bool paraboloid_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        static const real PI2 = 2.0 * values::pi();

        real radius = radius_;
        real zmin = zmin_;
        real zmax = zmax_;
        real umin = 0.0;
        real umax = umax_;
        real vmin = zmin / zmax;
        real vmax = 1.0;

        const vector3& org = r.origin();
        const vector3& dir = r.direction();

        double k = zmax / (radius * radius);
        double A = k * dot2(dir, dir);
        double B = 2 * k * dot2(dir, org) - dir[2];
        double C = k * dot2(org, org) - org[2];

        double coeff[] = {A, B, C};

        double root[2] = {};
        int nRet = solve2(root, coeff, 0, dist);

        for (int i = 0; i < nRet; i++)
        {
            real t = root[i];
            vector3 p = r.origin() + t * r.direction();

            real phi = adjust_rad(atan2(p[1], p[0]));
            real u = phi / PI2;
            real v = p[2] / zmax;
            if (u < umin || umax < u) continue;
            if (v < vmin || vmax < v) continue;

            info->distance = t;
            info->position = p;
            info->coord = vector3(u, v, 0);

            return true;
        }

        return false;
    }

    void paraboloid_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        real radius = radius_;
        real zmin = zmin_;
        real zmax = zmax_;
        real umax = umax_;

        vector3 p = info->position;
        vector3 dpdu = normalize(vector3(-p[1], p[0], 0));
        vector3 dpdv = normalize((zmax - zmin) * vector3(p[0], p[1], 2 * p[2]));
        vector3 n = cross(dpdu, dpdv);

        info->normal = n;
        info->geometric = n;
        info->tangent = dpdu;
        info->binormal = dpdv;
    }

    vector3 paraboloid_intersection_imp::min() const
    {
        return vector3(-radius_, -radius_, zmin_);
    }

    vector3 paraboloid_intersection_imp::max() const
    {
        return vector3(+radius_, +radius_, zmax_);
    }

    paraboloid_intersection::paraboloid_intersection(real radius, real zmin, real zmax, real umax)
    {
        imp_ = new paraboloid_intersection_imp(radius, zmin, zmax, umax);
    }

    paraboloid_intersection::~paraboloid_intersection()
    {
        delete imp_;
    }

    bool paraboloid_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool paraboloid_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void paraboloid_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        imp_->finalize(info, r, dist);
    }

    vector3 paraboloid_intersection::min() const
    {
        return imp_->min();
    }

    vector3 paraboloid_intersection::max() const
    {
        return imp_->max();
    }
}
