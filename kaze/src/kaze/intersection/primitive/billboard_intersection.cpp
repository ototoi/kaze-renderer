#include "billboard_intersection.h"
#include "test_info.h"

namespace kaze
{

    disk_billbord_intersection::disk_billbord_intersection(const vector3& center, real radius) : center_(center), radius_(radius) {}

    bool disk_billbord_intersection::test(const ray& r, real dist) const
    {
        vector3 OC = center_ - r.origin();

        real t = dot(r.direction(), OC);
        if (t < 0 || dist <= t) return false;
        real rad = radius_;
        if (rad * rad < (-OC + t * r.direction()).sqr_length()) return false;

        return true;
    }

    bool disk_billbord_intersection::test(test_info* info, const ray& r, real dist) const
    {
        vector3 OC = center_ - r.origin();

        real t = dot(r.direction(), OC);
        if (t < 0 || dist <= t) return false;
        real rad = radius_;
        if (rad * rad < (-OC + t * r.direction()).sqr_length()) return false;

        info->distance = (t);
        info->p_intersection = this;

        return true;
    }

    void disk_billbord_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        vector3 n = -r.direction();
        info->position = (r.origin() + info->distance * r.direction());
        info->normal = n;
        info->geometric = n;
    }
}
