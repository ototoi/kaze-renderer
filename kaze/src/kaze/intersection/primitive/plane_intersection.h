#ifndef KAZE_PLANE_INTERSECTION_H
#define KAZE_PLANE_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class plane_intersection : public bounded_intersection
    {
    public:
        plane_intersection(const vector3& org, const vector3& u, const vector3& v);
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        vector3 min_;
        vector3 max_;

        vector3 o_;
        vector3 u_;
        vector3 v_;
        vector3 n_;
    };
}

#endif
