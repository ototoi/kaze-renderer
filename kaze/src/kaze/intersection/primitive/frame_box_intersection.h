#ifndef KAZE_FRAME_BOX_INTERSECTION_H
#define KAZE_FRAME_BOX_INTERSECTION_H

#include "bounded_intersection.h"
#include <vector>

namespace kaze
{

    //FRAME_BOX
    class frame_box_intersection : public bounded_intersection
    {
    public:
        frame_box_intersection(const vector3& min, const vector3& max, real radius);
        ~frame_box_intersection();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const { return min_; }
        vector3 max() const { return max_; }
    private:
        vector3 min_;
        vector3 max_;

        std::vector<intersection*> mv_;
    };
}

#endif
