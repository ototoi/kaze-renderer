#ifndef KAZE_SPHERE_INTERSECTION_H
#define KAZE_SPHERE_INTERSECTION_H

#include "primitive_intersection.h"

#include "ellipsoid_intersection.h"

namespace kaze
{

    //SPHERE

    class classic_sphere_intersection_imp;
    class classic_sphere_intersection : public primitive_intersection
    {
    public:
        classic_sphere_intersection(const vector3& o, real r);
        ~classic_sphere_intersection();
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        classic_sphere_intersection(const classic_sphere_intersection& rhs);
        classic_sphere_intersection& operator=(const classic_sphere_intersection& rhs);

    private:
        classic_sphere_intersection_imp* imp_;
    };

    typedef ellipsoid_intersection sphere_intersection;
}

#endif
