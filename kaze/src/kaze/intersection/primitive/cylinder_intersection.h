#ifndef KAZE_CYLINDER_INTERSECTION_H
#define KAZE_CYLINDER_INTERSECTION_H

#include "primitive_intersection.h"

namespace kaze
{

    class cylinder_intersection_imp;
    class cylinder_intersection : public primitive_intersection
    {
    public:
        cylinder_intersection(const vector3& p0, const vector3& p1, real radius, real umin = 0, real umax = 1);
        cylinder_intersection(const vector3& p0, const vector3& vx, const vector3& vy, const vector3& vz, real umin = 0, real umax = 1);
        ~cylinder_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    public:
        bool test_wo_AABB(const ray& r, real dist) const;
        bool test_wo_AABB(test_info* info, const ray& r, real dist) const;

    private:
        cylinder_intersection_imp* imp_;
    };
}

#endif
