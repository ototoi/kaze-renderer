#include "wire_intersection.h"

#include "test_info.h"

#include <vector>

namespace kaze
{

    struct wire_line
    {
        wire_line(const vector3& p0, const vector3& p1)
        {
            p[0] = p0;
            p[1] = p1;
        }

        vector3 p[2];
    };

    static void get_minmax(vector3& min, vector3& max, const wire_line& w)
    {
        static const real FAR = values::far();
        min = vector3(FAR, FAR, FAR);
        max = -vector3(FAR, FAR, FAR);
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (min[j] > w.p[i][j]) min[j] = w.p[i][j];
                if (max[j] < w.p[i][j]) max[j] = w.p[i][j];
            }
        }
    }

    //------------------------------------
    //------------------------------------
    struct wire_info
    {
        real t;
    };

    static inline bool test_wire(wire_info* info, const wire_line& wire, real radius, const ray& r, real tmin, real tmax)
    {
        static const real EPSILON = values::epsilon() * 1024 * 1024;
        vector3 p = wire.p[0];
        vector3 q = wire.p[1];
        vector3 d = q - p;
        vector3 n = r.direction();
        vector3 m = r.origin() - p;

        real md = dot(m, d);
        real nd = dot(n, d);
        real dd = dot(d, d);

        real nn = 1; //dot(n, n);
        real mn = dot(m, n);
        real a = dd * nn - nd * nd;

        real b = dd * mn - nd * md;
        real k = dot(m, m) - radius * radius;
        real c = dd * k - md * md;

        real t = 0;
        if (fabs(a) <= EPSILON)
        {
            return false;
            //t = -c/(2*b);//2bt+c=0
        }
        else
        {

            real Q = b * b - a * c;

            if (Q <= EPSILON) return false;
            //if(Q < EPSILON){
            //  t = -b/a;
            //}else{
            real SQ = sqrt(Q);
            t = (-b + SQ) / a;
            //t *= c;
            //t = -b/a -SQ/a;

            //}
        }

        if (t <= tmin || tmax <= t) return false;

        real s = md + t * nd;
        if (s < 0 || dd < s) return false;

        info->t = t;

        return true;
    }

    //------------------------------------
    //------------------------------------

    class wire_intersection_imp
    {
    public:
        wire_intersection_imp(real radius);
        wire_intersection_imp(const vector3& min, const vector3& max, real r);
        ~wire_intersection_imp();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    public:
        void add(const vector3& p0, const vector3& p1)
        {
            add(wire_line(p0, p1));
        }

        void add(const wire_line& w)
        {
            lines_.push_back(w);
            vector3 min;
            vector3 max;
            get_minmax(min, max, w);
            expand(min, max);
        }

    protected:
        void expand(vector3& min, vector3& max)
        {
            for (int i = 0; i < 3; i++)
            {
                if (min_[i] > min[i]) min_[i] = min[i];
                if (max_[i] < max[i]) max_[i] = max[i];
            }
        }

    protected:
        bool test_inner(const ray& r, real dist) const;
        bool test_inner(test_info* info, const ray& r, real dist) const;

    private:
        vector3 min_;
        vector3 max_;
        std::vector<wire_line> lines_;
        real radius_;
    };

    //------------------------------------

    static const real FAR = values::far();

    wire_intersection_imp::wire_intersection_imp(real radius)
        : min_(vector3(FAR, FAR, FAR)), max_(-vector3(FAR, FAR, FAR)), radius_(radius)
    {
        ; //
    }

    wire_intersection_imp::wire_intersection_imp(const vector3& min, const vector3& max, real radius)
        : min_(min), max_(max), radius_(radius)
    {
        vector3 p[8];
        p[0] = vector3(min[0], min[1], min[2]);
        p[1] = vector3(min[0], min[1], max[2]);
        p[2] = vector3(min[0], max[1], min[2]);
        p[3] = vector3(min[0], max[1], max[2]);
        p[4] = vector3(max[0], min[1], min[2]);
        p[5] = vector3(max[0], min[1], max[2]);
        p[6] = vector3(max[0], max[1], min[2]);
        p[7] = vector3(max[0], max[1], max[2]);

        add(p[0], p[1]);
        add(p[0], p[2]);
        add(p[0], p[4]);

        //add(p[1],p[0]);
        add(p[1], p[3]);
        add(p[1], p[5]);

        add(p[2], p[3]);
        //add(p[2],p[0]);
        add(p[2], p[6]);

        //add(p[3],p[1]);
        //add(p[3],p[2]);
        add(p[3], p[7]);

        //add(p[4],p[0]);
        add(p[4], p[5]);
        add(p[4], p[6]);

        //add(p[5],p[1]);
        //add(p[5],p[4]);
        add(p[5], p[7]);

        //add(p[6],p[2]);
        //add(p[6],p[6]);
        add(p[6], p[7]);

        //add(p[7],p[3]);
        //add(p[7],p[5]);
        //add(p[7],p[6]);
    }

    wire_intersection_imp::~wire_intersection_imp()
    {
        ; //
    }

    bool wire_intersection_imp::test(const ray& r, real dist) const
    {
        if (!lines_.empty() && test_AABB(min(), max(), r, dist))
        {
            return test_inner(r, dist);
        }
        return false;
    }

    bool wire_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        if (!lines_.empty() && test_AABB(min(), max(), r, dist))
        {
            return test_inner(info, r, dist);
        }
        return false;
    }

    void wire_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        info->position = r.origin() + dist * r.direction();
        vector3 n = -r.direction();
        info->geometric = n;
        info->normal = n;
    }

    vector3 wire_intersection_imp::min() const
    {
        real R = radius_ + 0.001;
        return min_ - vector3(R, R, R);
    }

    vector3 wire_intersection_imp::max() const
    {
        real R = radius_ + 0.001;
        return max_ + vector3(R, R, R);
        ;
    }

    bool wire_intersection_imp::test_inner(const ray& r, real dist) const
    {
        real radius = radius_;
        wire_info winfo;
        size_t sz = lines_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (test_wire(&winfo, lines_[i], radius, r, 0, dist)) return false;
        }
        return false;
    }

    bool wire_intersection_imp::test_inner(test_info* info, const ray& r, real dist) const
    {
        bool bRet = false;
        real radius = radius_;
        wire_info winfo;
        size_t sz = lines_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (test_wire(&winfo, lines_[i], radius, r, 0, dist))
            {
                dist = winfo.t;
                info->distance = dist;
                bRet = true;
            }
        }
        return bRet;
    }
    //------------------------------------

    wire_intersection::wire_intersection(real r)
    {
        imp_ = new wire_intersection_imp(r);
    }

    wire_intersection::wire_intersection(const vector3& min, const vector3& max, real r)
    {
        imp_ = new wire_intersection_imp(min, max, r);
    }

    wire_intersection::~wire_intersection()
    {
        delete imp_;
    }

    bool wire_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool wire_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void wire_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        imp_->finalize(info, r, dist);
    }

    vector3 wire_intersection::min() const
    {
        return imp_->min();
    }

    vector3 wire_intersection::max() const
    {
        return imp_->max();
    }

    void wire_intersection::add(const vector3& p0, const vector3& p1)
    {
        imp_->add(p0, p1);
    }
}
