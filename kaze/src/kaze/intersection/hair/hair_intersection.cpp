#include "hair_intersection.h"
#include "curves/curves_intersection.h"

namespace kaze
{

    hair_intersection::hair_intersection(const bezier_curves_loader& loader)
    {
        parameter_map pm;
        pm.set_string("CURVE_TYPE", "hair");
        inter_.reset(new curves_intersection(loader, pm));
    }

    hair_intersection::~hair_intersection()
    {
        ;
    }

    bool hair_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool hair_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void hair_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ;
    }

    vector3 hair_intersection::min() const
    {
        return inter_->min();
    }

    vector3 hair_intersection::max() const
    {
        return inter_->max();
    }
}
