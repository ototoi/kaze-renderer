#ifndef KAZE_HAIR_INTERSECTION_H
#define KAZE_HAIR_INTERSECTION_H

#include "bounded_intersection.h"
#include "curves/bezier_curves_loader.h"


namespace kaze
{

    class hair_intersection : public bounded_intersection
    {
    public:
        hair_intersection(const bezier_curves_loader& loader);
        ~hair_intersection();
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    protected:
        std::shared_ptr<intersection> inter_;
    };
}

#endif
