#include "julia_fractal_intersection.h"

#include "test_info.h"
#include "values.h"
#include <cmath>
#include <vector>
#include <algorithm>

#include "intersection_ex.h"

namespace kaze
{
    namespace
    {
        static const real BOUNDING_RADIUS = (real)sqrt(3.0);
        static const real BOUNDING_RADIUS2 = 3.0;
        static const real ESCAPE_THRESHOLD = 1e1;
        static const real DELTA = 1e-4;
        static const real EPSILON = 0.00001;
        static const int MAX_ITERATIONS = 15;

        struct uv_struct
        {
            real t;
            //vector3 p;
            vector3 n;
        };

        static real length(real w, real x, real y, real z)
        {
            return (real)sqrt(w * w + x * x + y * y + z * z);
        }

        static bool test_julia_fractal(uv_struct* info, const vector4& cc, const vector3& org, const vector3& dir, real tmin, real tmax)
        {
            real cw = cc[0];
            real cx = cc[1];
            real cy = cc[2];
            real cz = cc[3];

            // intersect with bounding sphere
            real A = dot(dir, dir);
            real C = dot(org, org) - BOUNDING_RADIUS2;
            real qt = tmin;
            if (C > 0)
            {
                // we are starting outside the sphere, find intersection on the
                // sphere
                real A = dot(dir, dir);
                real B = dot(org, dir); //2B

                real Q = B * B - A * C;
                if (Q < EPSILON) return false;
                real SQ = sqrt(Q);

                real iA = real(1) / A;
                real t0 = (-B - SQ) * iA;
                real t1 = (-B + SQ) * iA;

                if (t0 >= tmax || t1 <= tmin) return false;

                real t;
                if (t0 < tmin)
                {
                    t = t1;
                }
                else
                {
                    t = t0;
                }
                qt = t;
            }
            real dist = tmax;
            vector3 p = org + qt * dir;
            real invRayLength = (real)(1.0 / A);

            while (true)
            {
                real zw = p[0];
                real zx = p[1];
                real zy = p[2];
                real zz = 0;

                real zpw = 1;
                real zpx = 0;
                real zpy = 0;
                real zpz = 0;

                // run several iterations
                real dotz = 0;
                for (int i = 0; i < MAX_ITERATIONS; i++)
                {
                    {
                        // zp = 2 * (z * zp)
                        real nw = zw * zpw - zx * zpx - zy * zpy - zz * zpz;
                        real nx = zw * zpx + zx * zpw + zy * zpz - zz * zpy;
                        real ny = zw * zpy + zy * zpw + zz * zpx - zx * zpz;
                        zpz = 2 * (zw * zpz + zz * zpw + zx * zpy - zy * zpx);
                        zpw = 2 * nw;
                        zpx = 2 * nx;
                        zpy = 2 * ny;
                    }
                    {
                        // z = z*z + c
                        real nw = zw * zw - zx * zx - zy * zy - zz * zz + cw;
                        zx = 2 * zw * zx + cx;
                        zy = 2 * zw * zy + cy;
                        zz = 2 * zw * zz + cz;
                        zw = nw;
                    }
                    dotz = zw * zw + zx * zx + zy * zy + zz * zz;
                    if (dotz > ESCAPE_THRESHOLD)
                        break;
                }
                real normZ = (real)sqrt(dotz);
                dist = 0.5 * normZ * (real)log(normZ) / length(zpw, zpx, zpy, zpz);
                p += dist * dir;
                qt += dist;
                if (dist * invRayLength < EPSILON)
                    break;
                if (dot(p, p) > BOUNDING_RADIUS2)
                    return false;
            }

            if (qt <= tmin || tmax <= qt) return false;

            if (dist * invRayLength < EPSILON)
            {
                info->t = qt;
                return true;
            }
            return false;
        }

        void finalize_julia_fractal(uv_struct* info, const vector4& cc, const vector3& org, const vector3& dir, real dist)
        {
            real cw = cc[0];
            real cx = cc[1];
            real cy = cc[2];
            real cz = cc[3];

            vector3 p = org + dist * dir;
            real gx1w = p[0] - DELTA;
            real gx1x = p[1];
            real gx1y = p[2];
            real gx1z = 0;
            real gx2w = p[0] + DELTA;
            real gx2x = p[1];
            real gx2y = p[2];
            real gx2z = 0;

            real gy1w = p[0];
            real gy1x = p[1] - DELTA;
            real gy1y = p[2];
            real gy1z = 0;
            real gy2w = p[0];
            real gy2x = p[1] + DELTA;
            real gy2y = p[2];
            real gy2z = 0;

            real gz1w = p[0];
            real gz1x = p[1];
            real gz1y = p[2] - DELTA;
            real gz1z = 0;
            real gz2w = p[0];
            real gz2x = p[1];
            real gz2y = p[2] + DELTA;
            real gz2z = 0;

            for (int i = 0; i < MAX_ITERATIONS; i++)
            {
                {
                    // z = z*z + c
                    real nw = gx1w * gx1w - gx1x * gx1x - gx1y * gx1y - gx1z * gx1z + cw;
                    gx1x = 2 * gx1w * gx1x + cx;
                    gx1y = 2 * gx1w * gx1y + cy;
                    gx1z = 2 * gx1w * gx1z + cz;
                    gx1w = nw;
                }
                {
                    // z = z*z + c
                    real nw = gx2w * gx2w - gx2x * gx2x - gx2y * gx2y - gx2z * gx2z + cw;
                    gx2x = 2 * gx2w * gx2x + cx;
                    gx2y = 2 * gx2w * gx2y + cy;
                    gx2z = 2 * gx2w * gx2z + cz;
                    gx2w = nw;
                }
                {
                    // z = z*z + c
                    real nw = gy1w * gy1w - gy1x * gy1x - gy1y * gy1y - gy1z * gy1z + cw;
                    gy1x = 2 * gy1w * gy1x + cx;
                    gy1y = 2 * gy1w * gy1y + cy;
                    gy1z = 2 * gy1w * gy1z + cz;
                    gy1w = nw;
                }
                {
                    // z = z*z + c
                    real nw = gy2w * gy2w - gy2x * gy2x - gy2y * gy2y - gy2z * gy2z + cw;
                    gy2x = 2 * gy2w * gy2x + cx;
                    gy2y = 2 * gy2w * gy2y + cy;
                    gy2z = 2 * gy2w * gy2z + cz;
                    gy2w = nw;
                }
                {
                    // z = z*z + c
                    real nw = gz1w * gz1w - gz1x * gz1x - gz1y * gz1y - gz1z * gz1z + cw;
                    gz1x = 2 * gz1w * gz1x + cx;
                    gz1y = 2 * gz1w * gz1y + cy;
                    gz1z = 2 * gz1w * gz1z + cz;
                    gz1w = nw;
                }
                {
                    // z = z*z + c
                    real nw = gz2w * gz2w - gz2x * gz2x - gz2y * gz2y - gz2z * gz2z + cw;
                    gz2x = 2 * gz2w * gz2x + cx;
                    gz2y = 2 * gz2w * gz2y + cy;
                    gz2z = 2 * gz2w * gz2z + cz;
                    gz2w = nw;
                }
            }
            real gradX = length(gx2w, gx2x, gx2y, gx2z) - length(gx1w, gx1x, gx1y, gx1z);
            real gradY = length(gy2w, gy2x, gy2y, gy2z) - length(gy1w, gy1x, gy1y, gy1z);
            real gradZ = length(gz2w, gz2x, gz2y, gz2z) - length(gz1w, gz1x, gz1y, gz1z);

            vector3 n = normalize(vector3(gradX, gradY, gradZ));

            info->n = n;
            //info->p = p + n*EPSILON;
        }

        inline static vector3 safe_normalize(const vector3& n)
        {
            static const real EPSILON = values::epsilon();
            real l2 = n.sqr_length();
            if (l2 < EPSILON) return vector3(0, 0, 0);
            return n * real(1) / std::sqrt(l2);
        }
    }

    class julia_fractal_intersection_imp
    {
    public:
        julia_fractal_intersection_imp();
        julia_fractal_intersection_imp(
            real c0, real c1, real c2, real c3,
            const vector3& o,
            const vector3& x, const vector3& y, const vector3& z);
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    protected:
        bool set_matrix(const vector3& x, const vector3& y, const vector3& z);
        void set_minmax();

    private:
        vector4 cc_;

        vector3 center_;
        matrix3 w2l_;
        matrix3 l2w_;
        vector3 min_;
        vector3 max_;
    };

    bool julia_fractal_intersection_imp::set_matrix(const vector3& x, const vector3& y, const vector3& z)
    {
        matrix3 m = matrix3(x[0], y[0], z[0], x[1], y[1], z[1], x[2], y[2], z[2]);
        if (!m.is_invertible()) return false;
        matrix3 im = ~m;
        l2w_ = m;  //local2world;
        w2l_ = im; //world2local;
        return true;
    }

    static const real pts[8][3] = {
        {1, 1, -1}, {-1, 1, -1}, {1, -1, -1}, {-1, -1, -1}, {1, 1, 1}, {-1, 1, 1}, {1, -1, 1}, {-1, -1, 1}};
    void julia_fractal_intersection_imp::set_minmax()
    {
        static const real FAR = values::far();

        vector3 min(FAR, FAR, FAR);
        vector3 max(-FAR, -FAR, -FAR);

        vector3 p[8];
        for (int i = 0; i < 8; i++)
        {
            p[i] = (l2w_ * BOUNDING_RADIUS * vector3(pts[i])) + center_;
            for (int j = 0; j < 3; j++)
            {
                if (min[j] > p[i][j]) min[j] = p[i][j];
                if (max[j] < p[i][j]) max[j] = p[i][j];
            }
        }
        min_ = min;
        max_ = max;
    }

    julia_fractal_intersection_imp::julia_fractal_intersection_imp()
    {
        cc_[0] = -.4;
        cc_[1] = .2;
        cc_[2] = .3;
        cc_[3] = -.2;

        set_matrix(vector3(1, 0, 0), vector3(0, 1, 0), vector3(0, 0, 1));
        set_minmax();
    }

    julia_fractal_intersection_imp::julia_fractal_intersection_imp(
        real c0, real c1, real c2, real c3,
        const vector3& o,
        const vector3& x, const vector3& y, const vector3& z)
    {
        cc_[0] = c0;
        cc_[1] = c1;
        cc_[2] = c2;
        cc_[3] = c3;
        center_ = o;
        set_matrix(x, y, z);
        set_minmax();
    }

    bool julia_fractal_intersection_imp::test(const ray& r, real dist) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, dist))
        {
            vector3 morg = w2l_ * (r.origin() - this->center_);
            vector3 mdir = w2l_ * r.direction();

            real tmin = std::max<real>(0, rng.tmin);
            real tmax = std::min<real>(dist, rng.tmax);
            uv_struct uv_info;
            if (test_julia_fractal(&uv_info, cc_, morg, mdir, tmin, tmax))
            {
                return true;
            }
        }
        return false;
    }

    bool julia_fractal_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, dist))
        {
            vector3 morg = w2l_ * (r.origin() - this->center_);
            vector3 mdir = w2l_ * r.direction();

            real tmin = std::max<real>(0, rng.tmin);
            real tmax = std::min<real>(dist, rng.tmax);
            uv_struct uv_info;
            if (test_julia_fractal(&uv_info, cc_, morg, mdir, tmin, tmax))
            {
                info->distance = uv_info.t;
                return true;
            }
        }
        return false;
    }

    void julia_fractal_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        vector3 morg = w2l_ * (r.origin() - this->center_);
        vector3 mdir = w2l_ * r.direction();

        uv_struct uv_info;
        finalize_julia_fractal(&uv_info, cc_, morg, mdir, dist);

        vector3 n = safe_normalize(l2w_ * uv_info.n);
        info->position = r.origin() + dist * r.direction();
        info->normal = n;
        info->geometric = n;
        info->coord = vector3(0, 0, 0);
        info->tangent = vector3(0, 0, 0);
        info->binormal = vector3(0, 0, 0);
    }

    vector3 julia_fractal_intersection_imp::min() const
    {
        return min_;
    }

    vector3 julia_fractal_intersection_imp::max() const
    {
        return max_;
    }

    //-----------------------------------------------------------------------------------

    julia_fractal_intersection::julia_fractal_intersection(
        real c0, real c1, real c2, real c3,
        const vector3& o,
        const vector3& x, const vector3& y, const vector3& z)
    {
        imp_ = new julia_fractal_intersection_imp(c0, c1, c2, c3, o, x, y, z);
    }

    julia_fractal_intersection::~julia_fractal_intersection()
    {
        delete imp_;
    }

    bool julia_fractal_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool julia_fractal_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void julia_fractal_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        imp_->finalize(info, r, dist);
    }

    vector3 julia_fractal_intersection::min() const
    {
        return imp_->min();
    }

    vector3 julia_fractal_intersection::max() const
    {
        return imp_->max();
    }
}
