#ifndef KAZE_JULIA_FRACTAL_INTERSECTION_H
#define KAZE_JULIA_FRACTAL_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class julia_fractal_intersection_imp;
    class julia_fractal_intersection : public bounded_intersection
    {
    public:
        julia_fractal_intersection(
            real c0 = -.4, real c1 = .2, real c2 = .3, real c3 = -.2,
            const vector3& o = vector3(0, 0, 0),
            const vector3& x = vector3(1, 0, 0), const vector3& y = vector3(0, 1, 0), const vector3& z = vector3(0, 0, 1));
        ~julia_fractal_intersection();
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    protected:
        julia_fractal_intersection_imp* imp_;
    };
}

#endif