#ifndef KAZE_MOTION_MESH_FACE_H
#define KAZE_MOTION_MESH_FACE_H

#include "mesh_face.h"

namespace kaze
{

    struct motion_mesh_face
    {
        bool test_DS(const ray& r, real dist) const;
        bool test_DS(test_info* info, const ray& r, real dist) const;
        bool test_SS(const ray& r, real dist) const;
        bool test_SS(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

        mesh_face faces[2];
        real times[2];
        size_t index;
    };
}

#endif