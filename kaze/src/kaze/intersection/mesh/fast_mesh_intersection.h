#ifndef FAST_MESH_INTERSECTION_H
#define FAST_MESH_INTERSECTION_H

#include "mesh_intersection.h"
#include "io_mesh.h"
#include "transformer.h"
#include "parameter_map.h"

#include "mesh_loader.h"

namespace kaze
{

    class fast_mesh_intersection_imp;

    class fast_mesh_intersection : public mesh_intersection,
                                   public mesh_loader
    {
    public:
        fast_mesh_intersection(const mesh_loader& ml, const parameter_map& param);
        ~fast_mesh_intersection();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    public:
        //bool load(mesh_saver* ms)const{return false;}
        bool get_vertices(size_t& sz) const;
        bool get_faces(size_t& sz) const;
        bool get_vertex(size_t i, io_mesh_vertex* v) const;
        bool get_face(size_t i, io_mesh_face* f) const;

    private:
        fast_mesh_intersection_imp* imp_;
    };
}

#endif