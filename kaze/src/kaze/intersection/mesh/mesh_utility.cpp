#include "mesh_utility.h"

//#include "mmap_array.hpp"

#include "logger.h"
#include "timer.h"

#define VEC_ARRAY std::vector

namespace kaze
{

#if 0 //!defined( _WIN32 )					//large
	typedef mmap_array<mesh_face>    face_list;
	typedef mmap_array<vector3>  vertex_list;
#else
    //WIN
    typedef std::vector<mesh_face> face_list;
    typedef std::vector<vector3> vertex_list;
#endif

    typedef struct sorted_indices_tag
    {
        size_t i0;
        size_t i1;
        size_t i2;
    } sorted_indices;

    static inline sorted_indices get_si(const mesh_face& mf)
    {
        size_t iset[] = {mf.i0, mf.i1, mf.i2};
        if (iset[0] > iset[1]) std::swap(iset[0], iset[1]);
        if (iset[1] > iset[2]) std::swap(iset[1], iset[2]);

        sorted_indices tmp = {iset[0], iset[1], iset[2]};

        return tmp;
    }

    static inline bool operator==(const sorted_indices& a, const sorted_indices& b)
    {
        return a.i0 == b.i0 && a.i1 == b.i1 && a.i2 == b.i2;
    }

    static int get_hash(const mesh_face& mf)
    {
        size_t iset[] = {mf.i0, mf.i1, mf.i2};
        if (iset[0] > iset[1]) std::swap(iset[0], iset[1]);
        if (iset[1] > iset[2]) std::swap(iset[1], iset[2]);

        return int(iset[0] ^ iset[1] ^ iset[2]);
    }

    struct face_flag
    {
        int hash;
        size_t index;
        int layer;
    };

    struct face_flag_sorter : public std::binary_function<face_flag, face_flag, bool>
    {
        bool operator()(const face_flag& a, const face_flag& b)
        {
            return a.hash < b.hash;
        }
    };

    static inline bool select_normal(vector3& out, const vector3& nv, const vector3& nt, real limit_cos)
    {
        real lv = nv.sqr_length();
        real lt = nt.sqr_length();
        int n = 0;
        if (lv != 0) n |= 1;
        if (lt != 0) n |= 2;
        if (n == 0) return false;

        switch (n)
        {
        case 1:
            out = nv;
            break;
        case 2:
            out = nt;
            break;
        default:
        {
            if (dot(nt, nv) <= limit_cos)
            {
                out = nt;
            }
            else
            {
                out = nv;
            }
        }
        }
        return true;
    }

    //----------------------------------------------------------------------------------------------

    bool calc_normals_safety(mesh_face* fl, size_t fsz, const vector3* vl, size_t vsz, real limit_angle)
    {
        if (fl == NULL) return false;
        if (vl == NULL) return false;
        if (fsz == 0 || vsz == 0) return false;

        std::vector<face_flag> refs(fsz);
        for (size_t i = 0; i < fsz; i++)
        {
            refs[i].index = i;
            refs[i].hash = get_hash(fl[i]);
            refs[i].layer = 0;
        }
        std::sort(refs.begin(), refs.end(), face_flag_sorter());

        size_t cur = 0;
        int cur_hash = refs[cur].hash;
        sorted_indices cur_si = get_si(fl[refs[cur].index]);
        int cur_layer = 0;
        cur++;
        while (cur < fsz)
        {
            if (cur_hash == refs[cur].hash)
            {
                sorted_indices tsi = get_si(fl[refs[cur].index]);
                if (cur_si == tsi)
                {
                    cur_layer++;
                    refs[cur].layer = cur_layer;
                }
                else
                {
                    cur_layer = 0;
                    cur_si = tsi;
                }
            }
            else
            {
                cur_hash = refs[cur].hash;
                cur_layer = 0;
                cur_si = get_si(fl[refs[cur].index]);
            }
            cur++;
        }

        std::vector<bool> flags(fsz);
        for (size_t i = 0; i < fsz; i++)
        {
            flags[i] = true;
        }

        for (size_t i = 0; i < fsz; i++)
        {
            if (refs[i].layer != 0)
            { //multi layer
                int hash = refs[i].hash;
                sorted_indices si = get_si(fl[refs[i].index]);
                for (size_t j = 0; j < fsz; j++)
                {
                    if (refs[j].hash == hash)
                    {
                        if (si == get_si(fl[refs[j].index]))
                        {
                            flags[refs[j].index] = false;
                        }
                    }
                }
            }
        }
        //-------------------------------------------------------

        vertex_list ntl;
        ntl.resize(fsz);
        vertex_list nvl;
        nvl.resize(vsz);

        std::memset(&(ntl[0]), 0, sizeof(vector3) * fsz);
        std::memset(&(nvl[0]), 0, sizeof(vector3) * vsz);

        for (size_t i = 0; i < fsz; i++)
        {
            size_t idx0 = fl[i].i0;
            size_t idx1 = fl[i].i1;
            size_t idx2 = fl[i].i2;

            if (vsz <= idx0 || vsz <= idx1 || vsz <= idx2) return false;

            const vector3& p0 = vl[idx0];
            const vector3& p1 = vl[idx1];
            const vector3& p2 = vl[idx2];

            vector3 crs(cross(p1 - p0, p2 - p0));

            if (flags[i])
            {
                nvl[idx0] += crs;
                nvl[idx1] += crs;
                nvl[idx2] += crs;
            }
            real cl = crs.sqr_length();
            if (cl != 0.0)
            {
                cl = std::sqrt(cl);
                crs *= real(1) / cl; //tmp normal
            }
            ntl[i] = crs;
        }

        for (size_t i = 0; i < vsz; i++)
        {
            real cl = nvl[i].sqr_length();
            if (cl != 0.0)
            {
                cl = std::sqrt(cl);
                nvl[i] *= real(1) / cl;
            }
        }

        real limit_cos = std::cos(limit_angle); //

        for (size_t i = 0; i < fsz; i++)
        {
            vector3 nt = ntl[i];
            if (!flags[i]) nt = vector3(0, 0, 0);

            size_t idx0 = fl[i].i0;
            size_t idx1 = fl[i].i1;
            size_t idx2 = fl[i].i2;

            vector3 out;
            if (!select_normal(out, nvl[idx0], nt, limit_cos)) return false;
            (fl)[i].n0 = out;
            if (!select_normal(out, nvl[idx1], nt, limit_cos)) return false;
            (fl)[i].n1 = out;
            if (!select_normal(out, nvl[idx2], nt, limit_cos)) return false;
            (fl)[i].n2 = out;
        }

        return true;
    }

    bool calc_normals_fast(mesh_face* fl, size_t fsz, const vector3* vl, size_t vsz, real limit_angle)
    {
        if (fl == NULL) return false;
        if (vl == NULL) return false;
        if (fsz == 0 || vsz == 0) return false;

        vertex_list ntl;
        ntl.resize(fsz);
        vertex_list nvl;
        nvl.resize(vsz);

        std::memset(&(ntl[0]), 0, sizeof(vector3) * fsz);
        std::memset(&(nvl[0]), 0, sizeof(vector3) * vsz);

        for (size_t i = 0; i < fsz; i++)
        {
            size_t idx0 = fl[i].i0;
            size_t idx1 = fl[i].i1;
            size_t idx2 = fl[i].i2;

            if (vsz <= idx0 || vsz <= idx1 || vsz <= idx2) return false;

            const vector3& p0 = vl[idx0];
            const vector3& p1 = vl[idx1];
            const vector3& p2 = vl[idx2];

            vector3 crs(cross(p1 - p0, p2 - p0));

            //
            nvl[idx0] += crs;
            nvl[idx1] += crs;
            nvl[idx2] += crs;
            //

            real cl = crs.sqr_length();
            if (cl == 0.0)
            {
                //print_log("error:normal1\n");
                continue;
                //return false;
            }
            cl = std::sqrt(cl);

            ntl[i] = crs * real(1) / cl; //tmp normal
        }

        for (size_t i = 0; i < vsz; i++)
        {
            real cl = nvl[i].sqr_length();
            if (cl == 0.0)
            {
                //print_log("error:normal2\n");
                continue;
                //return false;
            }
            cl = std::sqrt(cl);
            nvl[i] *= real(1) / cl;
        }

        real limit_cos = std::cos(limit_angle); //

        for (size_t i = 0; i < fsz; i++)
        {
            vector3 nt = ntl[i];

            size_t idx0 = fl[i].i0;
            size_t idx1 = fl[i].i1;
            size_t idx2 = fl[i].i2;

            vector3 out;
            if (!select_normal(out, nvl[idx0], nt, limit_cos)) return false;
            (fl)[i].n0 = out;
            if (!select_normal(out, nvl[idx1], nt, limit_cos)) return false;
            (fl)[i].n1 = out;
            if (!select_normal(out, nvl[idx2], nt, limit_cos)) return false;
            (fl)[i].n2 = out;
        }

        return true;
    }

    bool calc_normals_flat(mesh_face* fl, size_t fsz, const vector3* vl, size_t vsz, real limit_angle)
    {
        if (fl == NULL) return false;
        if (vl == NULL) return false;
        if (fsz == 0 || vsz == 0) return false;

        double isz = 100.0 / fsz;
        int cnt = 0;

        for (size_t i = 0; i < fsz; i++)
        {
            size_t idx0 = fl[i].i0;
            size_t idx1 = fl[i].i1;
            size_t idx2 = fl[i].i2;

            if (vsz <= idx0 || vsz <= idx1 || vsz <= idx2) return false;

            const vector3& p0 = vl[idx0];
            const vector3& p1 = vl[idx1];
            const vector3& p2 = vl[idx2];

            vector3 crs(cross(p1 - p0, p2 - p0));

            real cl = crs.sqr_length();
            if (cl == 0.0)
            {
                fl[i].n0 = vector3(0, 0, 0);
                fl[i].n1 = vector3(0, 0, 0);
                fl[i].n2 = vector3(0, 0, 0);
            }
            else
            {
                cl = std::sqrt(cl);

                crs *= real(1) / cl; //tmp normal
                fl[i].n0 = crs;
                fl[i].n1 = crs;
                fl[i].n2 = crs;
            }

            //if(cnt++ == 255){
            //print_progress("%f%%\r",i*isz);
            //	cnt = 0;
            //}
        }
        return true;
    }

    namespace
    {
        struct tri
        {
            size_t vert[3];
            size_t next_face[3];
            vector3 normal;
        };

        struct vtx
        {
            size_t face;
        };

        static const size_t EMPTY_MASK = (size_t)-1;
    }

    bool calc_normals_facevarying(mesh_face* fl, size_t fsz, const vector3* vl, size_t vsz, real limit_angle)
    {
        real limit_cos = std::cos(limit_angle);

        std::vector<tri> tries(fsz);
        for (size_t i = 0; i < fsz; i++)
        {
            tries[i].vert[0] = fl[i].i0;
            tries[i].vert[1] = fl[i].i1;
            tries[i].vert[2] = fl[i].i2;
            tries[i].next_face[0] = EMPTY_MASK;
            tries[i].next_face[1] = EMPTY_MASK;
            tries[i].next_face[2] = EMPTY_MASK;

            vector3 p0 = vl[tries[i].vert[0]];
            vector3 p1 = vl[tries[i].vert[1]];
            vector3 p2 = vl[tries[i].vert[2]];

            vector3 e1 = p1 - p0;
            vector3 e2 = p2 - p0;
            tries[i].normal = cross(e1, e2);
        }
        std::vector<vtx> verts(vsz);
        for (size_t i = 0; i < vsz; i++)
        {
            verts[i].face = EMPTY_MASK;
        }

        std::vector<vector3> ntl(fsz * 3);
        for (size_t i = 0; i < fsz; i++)
        {
            ntl[3 * i + 0] = tries[i].normal;
            ntl[3 * i + 1] = tries[i].normal;
            ntl[3 * i + 2] = tries[i].normal;
        }
#ifdef _DEBUG
        std::vector<int> vrefs(vsz);
        for (size_t i = 0; i < vsz; i++)
        {
            vrefs[i] = 0;
        }
        for (size_t i = 0; i < fsz; i++)
        {
            vrefs[tries[i].vert[0]]++;
            vrefs[tries[i].vert[1]]++;
            vrefs[tries[i].vert[2]]++;
        }
#endif
        for (size_t i = 0; i < fsz; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                size_t vidx = tries[i].vert[j];

                assert(verts[vidx].face != i);

                if (verts[vidx].face == EMPTY_MASK)
                {
                    verts[vidx].face = i;
                }
                else
                {
                    size_t fidx = verts[vidx].face;

                    int k = 0;
                    while (1)
                    {
                        bool bFind = false;
                        for (k = 0; k < 3; k++)
                        {
                            if (tries[fidx].vert[k] == vidx)
                            {
                                bFind = true;
                                break;
                            }
                        }
                        assert(bFind);
                        if (tries[fidx].next_face[k] == EMPTY_MASK) break;
                        fidx = tries[fidx].next_face[k];
                    }

                    tries[fidx].next_face[k] = i;
                }
            }
        }

        std::vector<size_t> vface_indices;
        for (size_t i = 0; i < fsz; i++)
        {
            vector3 iN = tries[i].normal;
            bool bLeng = false;
            if (iN.sqr_length() >= 0)
            {
                iN = normalize(iN);
                bLeng = true;
            }
            for (int j = 0; j < 3; j++)
            {
                size_t vidx = tries[i].vert[j];

                vface_indices.clear();
                if (verts[vidx].face != EMPTY_MASK)
                {
                    size_t fidx = verts[vidx].face;

                    int k = 0;
                    while (1)
                    {
                        vface_indices.push_back(fidx);

                        bool bFind = false;
                        for (k = 0; k < 3; k++)
                        {
                            if (tries[fidx].vert[k] == vidx)
                            {
                                bFind = true;
                                break;
                            }
                        }
                        if (!bFind) break;
                        if (tries[fidx].next_face[k] == EMPTY_MASK) break;
                        fidx = tries[fidx].next_face[k];
                    };
                }
#ifdef _DEBUG
                std::sort(vface_indices.begin(), vface_indices.end());
                assert(vrefs[tries[i].vert[j]] == vface_indices.size());
#endif
                for (size_t l = 0; l < vface_indices.size(); l++)
                {
                    if (vface_indices[l] != i)
                    {
                        vector3 lN = tries[vface_indices[l]].normal;
                        if (bLeng)
                        {
                            real d = dot(iN, normalize(lN));
                            if (d < 0)
                            {
                                iN *= -1;
                                d *= -1;
                            }
                            if (d > limit_cos)
                            {
                                ntl[3 * i + j] += lN;
                            }
                        }
                        else
                        {
                            ntl[3 * i + j] += lN;
                        }
                    }
                }
            }
        }

        for (size_t i = 0; i < fsz * 3; i++)
        {
            if (ntl[i].sqr_length() <= 0) continue;
            ntl[i] = normalize(ntl[i]);
        }

        for (size_t i = 0; i < fsz; i++)
        {
            fl[i].n0 = ntl[3 * i + 0];
            fl[i].n1 = ntl[3 * i + 1];
            fl[i].n2 = ntl[3 * i + 2];
        }

        return true;
    }

    bool calc_normals(mesh_face* fl, size_t fsz, const vector3* vl, size_t vsz, real limit_angle)
    {
        return calc_normals_facevarying(fl, fsz, vl, vsz, limit_angle);
    }

//Tomas Moller's polygon VS box check functions
//See also
//static bool planeBoxOverlap(const vector3 & normal,const vector3 & vert,const vector3& maxbox);
//static bool triBoxOverlap(const vector3 & min, const vector3 & max, const vector3 & vtx0,const vector3 & vtx1, const vector3& vtx2);

#define FINDMINMAX(x0, x1, x2, min, max) \
    min = max = x0;                      \
    if (x1 < min) min = x1;              \
    if (x1 > max) max = x1;              \
    if (x2 < min) min = x2;              \
    if (x2 > max) max = x2;

/*======================== X-tests ========================*/

#define AXISTEST_X01(a, b, fa, fb)                   \
    p0 = a * v0[1] - b * v0[2];                      \
    p2 = a * v2[1] - b * v2[2];                      \
    if (p0 < p2)                                     \
    {                                                \
        min = p0;                                    \
        max = p2;                                    \
    }                                                \
    else                                             \
    {                                                \
        min = p2;                                    \
        max = p0;                                    \
    }                                                \
    rad = fa * boxhalfsize[1] + fb * boxhalfsize[2]; \
    if (min > rad || max < -rad) return false;

#define AXISTEST_X2(a, b, fa, fb)                    \
    p0 = a * v0[1] - b * v0[2];                      \
    p1 = a * v1[1] - b * v1[2];                      \
    if (p0 < p1)                                     \
    {                                                \
        min = p0;                                    \
        max = p1;                                    \
    }                                                \
    else                                             \
    {                                                \
        min = p1;                                    \
        max = p0;                                    \
    }                                                \
    rad = fa * boxhalfsize[1] + fb * boxhalfsize[2]; \
    if (min > rad || max < -rad) return false;

/*======================== Y-tests ========================*/

#define AXISTEST_Y02(a, b, fa, fb)                   \
    p0 = -a * v0[0] + b * v0[2];                     \
    p2 = -a * v2[0] + b * v2[2];                     \
    if (p0 < p2)                                     \
    {                                                \
        min = p0;                                    \
        max = p2;                                    \
    }                                                \
    else                                             \
    {                                                \
        min = p2;                                    \
        max = p0;                                    \
    }                                                \
    rad = fa * boxhalfsize[0] + fb * boxhalfsize[2]; \
    if (min > rad || max < -rad) return false;

#define AXISTEST_Y1(a, b, fa, fb)                    \
    p0 = -a * v0[0] + b * v0[2];                     \
    p1 = -a * v1[0] + b * v1[2];                     \
    if (p0 < p1)                                     \
    {                                                \
        min = p0;                                    \
        max = p1;                                    \
    }                                                \
    else                                             \
    {                                                \
        min = p1;                                    \
        max = p0;                                    \
    }                                                \
    rad = fa * boxhalfsize[0] + fb * boxhalfsize[2]; \
    if (min > rad || max < -rad) return false;

/*======================== Z-tests ========================*/

#define AXISTEST_Z12(a, b, fa, fb)                   \
    p1 = a * v1[0] - b * v1[1];                      \
    p2 = a * v2[0] - b * v2[1];                      \
    if (p2 < p1)                                     \
    {                                                \
        min = p2;                                    \
        max = p1;                                    \
    }                                                \
    else                                             \
    {                                                \
        min = p1;                                    \
        max = p2;                                    \
    }                                                \
    rad = fa * boxhalfsize[0] + fb * boxhalfsize[1]; \
    if (min > rad || max < -rad) return false;

#define AXISTEST_Z0(a, b, fa, fb)                    \
    p0 = a * v0[0] - b * v0[1];                      \
    p1 = a * v1[0] - b * v1[1];                      \
    if (p0 < p1)                                     \
    {                                                \
        min = p0;                                    \
        max = p1;                                    \
    }                                                \
    else                                             \
    {                                                \
        min = p1;                                    \
        max = p0;                                    \
    }                                                \
    rad = fa * boxhalfsize[0] + fb * boxhalfsize[1]; \
    if (min > rad || max < -rad) return false;

    bool planeBoxOverlap(const vector3& normal, const vector3& vert, const vector3& maxbox)
    {

        vector3 vmin, vmax;
        real v;
        for (int i = 0; i < 3; i++)
        {
            v = vert[i];
            if (normal[i] > 0.0)
            {
                vmin[i] = -maxbox[i] - v;
                vmax[i] = maxbox[i] - v;
            }
            else
            {
                vmin[i] = maxbox[i] - v;
                vmax[i] = -maxbox[i] - v;
            }
        }
        if (dot(normal, vmin) > 0.0) return false;
        if (dot(normal, vmax) >= 0.0) return true;

        return false;
    }

    bool triBoxOverlap(const vector3& boxcenter, const vector3& boxhalfsize, const vector3& vtx0, const vector3& vtx1, const vector3& vtx2)
    {

        /*	  use separating axis theorem to test overlap between triangle and box */
        /*	  need to test for overlap in these directions: */
        /*	  1) the {x,y,z}-directions (actually, since we use the AABB of the triangle */
        /*		 we do not even need to test these) */
        /*	  2) normal of the triangle */
        /*	  3) crossproduct(edge from tri, {x,y,z}-directin) */
        /*		 this gives 3x3=9 more tests */

        //real d;
        /* This is the fastest branch on Sun */
        /* move everything so that the boxcenter is in (0,0,0) */
        vector3 v0(vtx0 - boxcenter);
        vector3 v1(vtx1 - boxcenter);
        vector3 v2(vtx2 - boxcenter);

        /* compute triangle edges */
        vector3 e0(v1 - v0);
        vector3 e1(v2 - v1);
        vector3 e2(v0 - v2);

#if 1
        real min, max, p0, p1, p2, rad, fex, fey, fez;
        /* Bullet 3:  */
        /*	test the 9 tests first (this was faster) */
        fex = std::abs(e0[0]);
        fey = std::abs(e0[1]);
        fez = std::abs(e0[2]);
        AXISTEST_X01(e0[2], e0[1], fez, fey);
        AXISTEST_Y02(e0[2], e0[0], fez, fex);
        AXISTEST_Z12(e0[1], e0[0], fey, fex);

        fex = std::abs(e1[0]);
        fey = std::abs(e1[1]);
        fez = std::abs(e1[2]);
        AXISTEST_X01(e1[2], e1[1], fez, fey);
        AXISTEST_Y02(e1[2], e1[0], fez, fex);
        AXISTEST_Z0(e1[1], e1[0], fey, fex);

        fex = std::abs(e2[0]);
        fey = std::abs(e2[1]);
        fez = std::abs(e2[2]);
        AXISTEST_X2(e2[2], e2[1], fez, fey);
        AXISTEST_Y1(e2[2], e2[0], fez, fex);
        AXISTEST_Z12(e2[1], e2[0], fey, fex);
#endif

#if 1
        /* Bullet 1: */
        /*	first test overlap in the {x,y,z}-directions */
        /*	find min, max of the triangle each direction, and test for overlap in */
        /*	that direction -- this is equivalent to testing a minimum AABB around */
        /*	the triangle against the AABB */

        /* test in X-direction */
        FINDMINMAX(v0[0], v1[0], v2[0], min, max);
        if (min > boxhalfsize[0] || max < -boxhalfsize[0]) return false;

        /* test in Y-direction */
        FINDMINMAX(v0[1], v1[1], v2[1], min, max);
        if (min > boxhalfsize[1] || max < -boxhalfsize[1]) return false;

        /* test in Z-direction */
        FINDMINMAX(v0[2], v1[2], v2[2], min, max);
        if (min > boxhalfsize[2] || max < -boxhalfsize[2]) return false;
#endif

        /* Bullet 2: */
        /*	test if the box intersects the plane of the triangle */
        /*	compute plane equation of triangle: normal*x+d=0 */
        vector3 normal(cross(e0, e1));
        //d = -dot(normal,v0);	/* plane eq: normal.x+d=0 */
        if (!planeBoxOverlap(normal, v0, boxhalfsize)) return false;

        return true; /* box and triangle overlaps */
    }

#undef VEC_ARRAY
}
