#include "mesh_face.h"

#include "test_info.h"
#include "values.h"
#include "logger.h"

#include <cstring>
#include <cmath>

namespace kaze
{

    template <class V>
    inline V barycentric_convert(real w, real u, real v, const V& p0, const V& p1, const V& p2)
    {
        return ((w)*p0 + (u)*p1 + (v)*p2);
    }

    static inline void coordinate_system(vector3& x, vector3& y, const vector3& z)
    {
        using namespace std;
        if (fabs(z[0]) > fabs(z[1]))
        {
            real invLen = real(1) / sqrt(z[0] * z[0] + z[2] * z[2]);
            x = vector3(-z[2] * invLen, 0, z[0] * invLen);
        }
        else
        {
            real invLen = real(1) / sqrt(z[1] * z[1] + z[2] * z[2]);
            x = vector3(0, z[2] * invLen, -z[1] * invLen);
        }

        y = cross(z, x);
    }

    static void set_normals(vector3& x, vector3& y, const vector3& z)
    {
        static const real EPSILON = values::epsilon() * 1000;
        real xl = x.sqr_length();
        real yl = y.sqr_length();
        if (xl < EPSILON || yl < EPSILON)
        {
            coordinate_system(x, y, z);
        }
        else
        {
            x = x * (real(1) / sqrt(xl));
            y = y * (real(1) / sqrt(yl));
        }
    }

    static vector3 safe_normalize(const vector3& v)
    {
        static const real EPSILON = values::epsilon() * 1000;
        using namespace std;

        real l = v.sqr_length();
        if (l < EPSILON) return vector3(0, 0, 0);
        return v * (real(1) / sqrt(l));
    }

    bool mesh_face::test_DS(const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1000;
        real u, v, t;

        const vector3& org(r.origin());
        const vector3& dir(r.direction());
        const vector3& p0(*(this->p_p0));
        const vector3& p1(*(this->p_p1));
        const vector3& p2(*(this->p_p2));

        //-e1 = p0-p1
        vector3 e1(p0 - p1); //vA

        //-e2 = p0-p2
        vector3 e2(p0 - p2); //vB

        //dir = GHI

        vector3 bDir(cross(e2, dir));

        real iM = dot(e1, bDir);

        if (EPSILON < iM)
        {
            //p0-org
            vector3 vOrg(p0 - org);

            u = dot(vOrg, bDir);
            if (u < 0 || iM < u) return false;

            vector3 vE(cross(e1, vOrg));

            v = dot(dir, vE);
            if (v < 0 || iM < u + v) return false;

            t = -dot(e2, vE);
            if (t <= 0 || dist * iM < t) return false;
        }
        else if (iM < -EPSILON)
        {
            //p0-org
            vector3 vOrg(p0 - org); //JKL

            u = dot(vOrg, bDir);
            if (u > 0 || iM > u) return false;

            vector3 vE(cross(e1, vOrg));

            v = dot(dir, vE);
            if (v > 0 || iM > u + v) return false;

            t = -dot(e2, vE);
            if (t >= 0 || dist * iM > t) return false;
        }
        else
        {
            return false;
        }

        return true;
    }

    bool mesh_face::test_DS(test_info* info, const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1000;
        real u, v, t;

        assert(info);
        assert(this);
        assert(this->p_p0);
        assert(this->p_p1);
        assert(this->p_p2);

        const vector3& org(r.origin());
        const vector3& dir(r.direction());
        const vector3& p0(*(this->p_p0));
        const vector3& p1(*(this->p_p1));
        const vector3& p2(*(this->p_p2));

        //-e1 = p0-p1
        vector3 e1(p0 - p1); //vA

        //-e2 = p0-p2
        vector3 e2(p0 - p2); //vB

        //dir = GHI

        vector3 bDir(cross(e2, dir));

        real iM = dot(e1, bDir);

        if (EPSILON < iM)
        {
            //p0-org
            vector3 vOrg(p0 - org);

            u = dot(vOrg, bDir);
            if (u < 0 || iM < u) return false;

            vector3 vE(cross(e1, vOrg));

            v = dot(dir, vE);
            if (v < 0 || iM < u + v) return false;

            t = -dot(e2, vE);
            if (t <= 0) return false;
        }
        else if (iM < -EPSILON)
        {
            //p0-org
            vector3 vOrg(p0 - org); //JKL

            u = dot(vOrg, bDir);
            if (u > 0 || iM > u) return false;

            vector3 vE(cross(e1, vOrg));

            v = dot(dir, vE);
            if (v > 0 || iM > u + v) return false;

            t = -dot(e2, vE);
            if (t >= 0) return false;
        }
        else
        {
            return false;
        }

        iM = real(1.0) / iM;

        t *= iM;
        if (dist < t) return false;
        u *= iM;
        v *= iM;

        info->distance = (t);
        info->p_shader = p_shader;
        info->i_attr = i_attr;

        mesh_face_freearea_struct* fsp = reinterpret_cast<mesh_face_freearea_struct*>(info->freearea);
        fsp->face = this; //4
        fsp->u = u;       //8
        fsp->v = v;       //8
                          //total 20byte
        return true;
    }

    bool mesh_face::test_SS(const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1000;
        real u, v, t;

        const vector3& org(r.origin());
        const vector3& dir(r.direction());
        const vector3& p0(*(this->p_p0));
        const vector3& p1(*(this->p_p1));
        const vector3& p2(*(this->p_p2));

        //-e1 = p0-p1
        vector3 e1(p0 - p1); //vA

        //-e2 = p0-p2
        vector3 e2(p0 - p2); //vB

        //dir = GHI

        vector3 bDir(cross(e2, dir));

        real iM = dot(e1, bDir);

        if (iM < -EPSILON)
        {
            //p0-org
            vector3 vOrg(p0 - org); //JKL

            u = dot(vOrg, bDir);
            if (u > 0 || iM > u) return false;

            vector3 vE(cross(e1, vOrg));

            v = dot(dir, vE);
            if (v > 0 || iM > u + v) return false;

            t = -dot(e2, vE);
            if (t >= 0 || dist * iM > t) return false;
        }
        else
        {
            return false;
        }

        return true;
    }

    bool mesh_face::test_SS(test_info* info, const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1000;
        real u, v, t;

        assert(info);
        assert(this);
        assert(this->p_p0);
        assert(this->p_p1);
        assert(this->p_p2);

        const vector3& org(r.origin());
        const vector3& dir(r.direction());
        const vector3& p0(*(this->p_p0));
        const vector3& p1(*(this->p_p1));
        const vector3& p2(*(this->p_p2));

        //-e1 = p0-p1
        vector3 e1(p0 - p1); //vA

        //-e2 = p0-p2
        vector3 e2(p0 - p2); //vB

        //dir = GHI

        vector3 bDir(cross(e2, dir));

        real iM = dot(e1, bDir);

        if (iM < -EPSILON)
        {
            //p0-org
            vector3 vOrg(p0 - org); //JKL

            u = dot(vOrg, bDir);
            if (u > 0 || iM > u) return false;

            vector3 vE(cross(e1, vOrg));

            v = dot(dir, vE);
            if (v > 0 || iM > u + v) return false;

            t = -dot(e2, vE);
            if (t >= 0) return false;
        }
        else
        {
            return false;
        }

        iM = real(1.0) / iM;

        t *= iM;
        if (dist < t) return false;
        u *= iM;
        v *= iM;

        info->distance = (t);
        info->p_shader = p_shader;
        info->i_attr = i_attr;

        mesh_face_freearea_struct* fsp = reinterpret_cast<mesh_face_freearea_struct*>(info->freearea);
        fsp->face = this; //4
        fsp->u = u;       //8
        fsp->v = v;       //8
                          //total 20 byte
        return true;
    }

    void mesh_face::finalize(test_info* info, const ray& r, real dist) const
    {
        //static const real EPSILON = 0;//0;//  values::epsilon()*1000;

        mesh_face_freearea_struct* fsp = reinterpret_cast<mesh_face_freearea_struct*>(info->freearea);

        vector3 e1(*p_p1 - *p_p0);
        vector3 e2(*p_p2 - *p_p0);

        vector3 pos(r.origin() + info->distance * r.direction());
        info->position = pos;

        vector3 geo(safe_normalize(cross(e1, e2)));
        info->geometric = geo;

        real u = fsp->u;
        real v = fsp->v;

        this->finalize(info, e1, e2, u, v);
    }

    void mesh_face::finalize(test_info* info, const vector3& e1, const vector3& e2, real u, real v) const
    {
        static const real EPSILON = values::epsilon() * 1000;

        real w = real(1.0) - (u + v);

        vector3 n0_ = (vector3)n0;
        vector3 n1_ = (vector3)n1;
        vector3 n2_ = (vector3)n2;

        vector2 c0_ = (vector2)uv0;
        vector2 c1_ = (vector2)uv1;
        vector2 c2_ = (vector2)uv2;

        //------------------------------

        vector3 normal = safe_normalize(barycentric_convert<vector3>(w, u, v, n0_, n1_, n2_));
        info->normal = normal;

        //------------------------------

        vector2 c = barycentric_convert<vector2>(w, u, v, c0_, c1_, c2_);
        info->coord = vector3(c[0], c[1], 0);

        //------------------------------

        real u0 = c0_[0];
        real u1 = c1_[0];
        real u2 = c2_[0];
        real v0 = c0_[1];
        real v1 = c1_[1];
        real v2 = c2_[1];

        real du1 = u1 - u0;
        real du2 = u2 - u0;
        real dv1 = v1 - v0;
        real dv2 = v2 - v0;

        vector3 dpdu = dv2 * e1 - dv1 * e2;
        vector3 dpdv = -du2 * e1 + du1 * e2;

        set_normals(dpdu, dpdv, normal);

        info->tangent = dpdu;
        info->binormal = dpdv;

        //------------------------------
    }
    /*
	bool mesh_face::coord(vector2* c, const ray& r)const{
		static const real EPSILON = values::epsilon()*1000;
		real w, u, v;

		const vector3& org(r.origin());
		const vector3& dir(r.direction());
		const vector3& p0(*(this->p_p0)); 
		const vector3& p1(*(this->p_p1)); 
		const vector3& p2(*(this->p_p2));
		
		//-e1 = p0-p1
		vector3 e1(p0-p1);//vA
		
		//-e2 = p0-p2
		vector3 e2(p0-p2);//vB 

		//dir = GHI
				
		vector3 bDir(cross(e2,dir));
		
		real iM = dot(e1,bDir);

		if(EPSILON < abs(iM)){
			//p0-org
			vector3 vOrg(p0 - org);
			vector3 vE(cross(e1,vOrg));
	    u = dot(vOrg,bDir);
			v = dot(dir,vE);
			iM = real(1)/iM;
			u *= iM;
			v *= iM;
			w = 1-u-v;

			vector2 uv;
			uv = barycentric_convert(w,u,v,uv0,uv1,uv2);
			*c = uv;
			return true;
		}else{
			return false;
		}		
	}
*/
}
