#include "kdtree_mesh_accelerator.h"

#include <memory>
#include <cstring>
#include <cmath>
#include <functional>
#include <algorithm>
#include <vector>

#include "test_info.h"

#include "mesh_utility.h"

#include "mesh_face.h"
#include "intersection.h"
#include "intersection_ex.h"

//#include "mmap_array.hpp"

#include "logger.h"

#define KAZE_KDT_USE_NULLOBJECT 0
#define KAZE_KDT_NODELEAF_MAXFACE 16
#define KAZE_GRID_MAXDIV 16

namespace kaze
{

    namespace
    {
        typedef const mesh_face* PCFACE;
        typedef mesh_face* PFACE;
        typedef PCFACE* face_iterator;
        typedef std::vector<PCFACE> face_list;

#if 0
		class axis_separator{
		public:
			void set(real value, int axis){
				fi.f = static_cast<float>(value);
				fi.i &= ~0x3;
				fi.i |= 0x3&axis; 
			}

			real get_value()const{
				return (real)(fi.f); 
			}

			int get_axis()const{
				return 0x3&fi.i;
			}
		
		private:
			
			union float_int{
				float f;
				int i;
			} fi;
		};
#else
        class axis_separator
        {
        public:
            void set(real value, int axis)
            {
                f = static_cast<float>(value);
                i = axis;
            }

            real get_value() const
            {
                return (real)(f);
            }

            int get_axis() const
            {
                return i;
            }

        private:
            float f;
            int i;
        };

#endif

        inline double log2(double x)
        {
            using namespace std;
            static const double LOG2 = log(2.0);
            return log(x) / LOG2;
        }
        int get_level(size_t face_num, int density = 8)
        {
            using namespace std;
            double f = log2(double(face_num * density * density * density));
            int nRet = int(ceil(f)) + 4;
            return nRet;
        }

        struct progress_struct
        {
            size_t now; //
            size_t max; //
            double isz;
            int cnt;
        } prg;
    }

    namespace
    {
        class bound
        {
        public:
            bound(const vector3& min, const vector3& max) : min_(min), max_(max) {}

            const vector3& min() const { return min_; }
            const vector3& max() const { return max_; }
        private:
            vector3 min_;
            vector3 max_;
        };

        class kdtm_tree_node
        {
        public:
            virtual bool test(const ray& r, real tmin, real tmax) const = 0;
            virtual bool test(test_info* info, const ray& r, real tmin, real tmax) const = 0;
            virtual size_t size() const { return 0; }
            virtual size_t memory_size() const { return 0; }
        public:
            virtual ~kdtm_tree_node() {}
        };

        class kdtm_node_null : public kdtm_tree_node
        {
        public:
            bool test(const ray& r, real tmin, real tmax) const { return false; }
            bool test(test_info* info, const ray& r, real tmin, real tmax) const { return false; }
        };
    }

    //------------------------------------------------------------------
    //kdtm_tree_node_leaf
    //
    namespace
    {
        class kdtm_node_leaf_SS : public kdtm_tree_node
        {
        public:
            kdtm_node_leaf_SS(face_iterator a, face_iterator b);
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;

            size_t size() const { return mv_.size(); }
            size_t memory_size() const { return sizeof(kdtm_node_leaf_SS) + sizeof(PCFACE) * mv_.capacity(); }
        private:
            face_list mv_;
        };

        class kdtm_node_leaf_DS : public kdtm_tree_node
        {
        public:
            kdtm_node_leaf_DS(face_iterator a, face_iterator b);
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;

            size_t size() const { return mv_.size(); }
            size_t memory_size() const { return sizeof(kdtm_node_leaf_DS) + sizeof(PCFACE) * mv_.capacity(); }
        private:
            face_list mv_;
        };

        kdtm_node_leaf_SS::kdtm_node_leaf_SS(face_iterator a, face_iterator b) : mv_(a, b) {}
        kdtm_node_leaf_DS::kdtm_node_leaf_DS(face_iterator a, face_iterator b) : mv_(a, b) {}

        bool kdtm_node_leaf_SS::test(const ray& r, real tmin, real tmax) const
        {
            size_t sz = mv_.size();
            const PCFACE* p_face = &(mv_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (p_face[i]->test_SS(r, tmax))
                {
                    return true;
                }
            }

            return false;
        }
        bool kdtm_node_leaf_DS::test(const ray& r, real tmin, real tmax) const
        {
            size_t sz = mv_.size();
            const PCFACE* p_face = &(mv_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (p_face[i]->test_DS(r, tmax))
                {
                    return true;
                }
            }

            return false;
        }

        bool kdtm_node_leaf_SS::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
            bool bHit = false;
            size_t sz = mv_.size();
            const PCFACE* p_face = &(mv_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (p_face[i]->test_SS(info, r, tmax))
                {
                    tmax = info->distance;
                    bHit = true;
                }
            }

            return bHit;
        }
        bool kdtm_node_leaf_DS::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
            bool bHit = false;
            size_t sz = mv_.size();
            const PCFACE* p_face = &(mv_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (p_face[i]->test_DS(info, r, tmax))
                {
                    tmax = info->distance;
                    bHit = true;
                }
            }
            return bHit;
        }
    }

    //------------------------------------------------------------------
    //kdtm_tree_node_branch
    //

    namespace
    {
        class pred_L : public std::unary_function<PCFACE, bool>
        {
        public:
            pred_L(int axis, real pos, const bound& bnd) : axis_(axis), pos_(pos)
            {
                vector3 min = bnd.min();
                vector3 max = bnd.max();
                max[axis] = pos;

                C_ = (max + min) * 0.5;
                W_ = (max - min) * 0.5;
            }

            bool operator()(PCFACE p_face) const
            {
                int axis = axis_;
                real pos = pos_;

                const vector3& rtx0 = (*(p_face->p_p0));
                const vector3& rtx1 = (*(p_face->p_p1));
                const vector3& rtx2 = (*(p_face->p_p2));

                int flag = 0;
                if (rtx0[axis] <= pos) flag++;
                if (rtx1[axis] <= pos) flag++;
                if (rtx2[axis] <= pos) flag++;

                switch (flag)
                {
                case 3:
                    return true;
                case 0:
                    return false;
                default:
                    return triBoxOverlap(C_, W_, rtx0, rtx1, rtx2);
                }
            }

        private:
            int axis_;
            real pos_;
            vector3 C_;
            vector3 W_;
        };

        class pred_R : public std::unary_function<PCFACE, bool>
        {
        public:
            pred_R(int axis, real pos, const bound& bnd) : axis_(axis), pos_(pos)
            {
                vector3 min = bnd.min();
                min[axis] = pos;
                vector3 max = bnd.max();

                C_ = (max + min) * 0.5;
                W_ = (max - min) * 0.5;
            }

            bool operator()(PCFACE p_face) const
            {
                int axis = axis_;
                real pos = pos_;

                const vector3& rtx0 = (*(p_face->p_p0));
                const vector3& rtx1 = (*(p_face->p_p1));
                const vector3& rtx2 = (*(p_face->p_p2));

                int flag = 0;
                if (rtx0[axis] >= pos) flag++;
                if (rtx1[axis] >= pos) flag++;
                if (rtx2[axis] >= pos) flag++;

                switch (flag)
                {
                case 3:
                    return true;
                case 0:
                    return false;
                default: //1,2
                    return triBoxOverlap(C_, W_, rtx0, rtx1, rtx2);
                }
            }

        private:
            int axis_;
            real pos_;
            vector3 C_;
            vector3 W_;
        };

        class pred_L_ONLY : public pred_R
        {
        public:
            pred_L_ONLY(int plane, real pos, const bound& bnd) : pred_R(plane, pos, bnd) {}
            bool operator()(PCFACE p_face) const
            {
                return !(pred_R::operator()(p_face));
            }
        };

        class pred_R_ONLY : public pred_L
        {
        public:
            pred_R_ONLY(int plane, real pos, const bound& bnd) : pred_L(plane, pos, bnd) {}
            bool operator()(PCFACE p_face) const
            {
                return !(pred_L::operator()(p_face));
            }
        };

        static bound get_bound(face_iterator a, face_iterator b)
        {
            static const real FAR = values::far();

            vector3 min(+FAR, +FAR, +FAR);
            vector3 max(-FAR, -FAR, -FAR);
            for (face_iterator i = a; i != b; i++)
            {
                PCFACE face = *i;
                vector3 vl[3] = {*(face->p_p0), *(face->p_p1), *(face->p_p2)};

                for (int j = 0; j < 3; j++)
                {
                    if (vl[j][0] < min[0])
                    {
                        min[0] = vl[j][0];
                    }
                    if (vl[j][1] < min[1])
                    {
                        min[1] = vl[j][1];
                    }
                    if (vl[j][2] < min[2])
                    {
                        min[2] = vl[j][2];
                    }
                    if (max[0] < vl[j][0])
                    {
                        max[0] = vl[j][0];
                    }
                    if (max[1] < vl[j][1])
                    {
                        max[1] = vl[j][1];
                    }
                    if (max[2] < vl[j][2])
                    {
                        max[2] = vl[j][2];
                    }
                }
            }
            return bound(min, max);
        }

        static vector3 get_mean(face_iterator a, face_iterator b)
        {
            vector3 v(0, 0, 0);
            for (face_iterator i = a; i != b; i++)
            {
                PCFACE face = *i;
                v += *(face->p_p0);
                v += *(face->p_p1);
                v += *(face->p_p2);
            }
            v *= (real(1) / ((b - a) * 3));
            return v;
        }

        static bound union_bound(const bound& a, const bound& b)
        {
            vector3 min = a.min();
            vector3 max = a.max();

            for (int i = 0; i < 3; i++)
            {
                if (min[i] < b.min()[i]) min[i] = b.min()[i];
                if (max[i] > b.max()[i]) max[i] = b.max()[i];
            }
            return bound(min, max);
        }

        class kdtm_node_branch : public kdtm_tree_node
        {
        public:
            kdtm_node_branch(face_iterator a, face_iterator b, const bound& bnd, int level, int max_level, const mesh_accelerator_property& prop);
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;

            size_t size() const
            {
                size_t sz = 0;
                if (nodes_[0]) sz += nodes_[0]->size();
                if (nodes_[1]) sz += nodes_[1]->size();
                return sz;
            }
            size_t memory_size() const
            {
                size_t sz = sizeof(kdtm_node_branch);
                if (nodes_[0]) sz += nodes_[0]->memory_size();
                if (nodes_[1]) sz += nodes_[1]->memory_size();
                return sz;
            }

        private:
            kdtm_tree_node* nodes_[2];
            axis_separator ax_;
        };

        kdtm_node_branch::kdtm_node_branch(face_iterator a, face_iterator b, const bound& bnd, int level, int max_level, const mesh_accelerator_property& prop)
        {
            using namespace std;

            bound cbnd = bnd;
            //bound cbnd = union_bound(bnd, get_bound(a,b));

            vector3 wide = cbnd.max() - cbnd.min();

            int plane = 0;
            if (wide[plane] < wide[1]) plane = 1;
            if (wide[plane] < wide[2]) plane = 2;

            real pos = cbnd.min()[plane] + wide[plane] * real(0.5);
            //real pos = get_mean(a,b)[plane];

            face_iterator begin, end;
            size_t sz;

#if KAZE_KDT_USE_NULLOBJECT
            auto_ptr<kdtm_tree_node> ap_l(new kdtm_node_null());
            auto_ptr<kdtm_tree_node> ap_r(new kdtm_node_null());
#else
            auto_ptr<kdtm_tree_node> ap_l;
            auto_ptr<kdtm_tree_node> ap_r;
#endif

            begin = a;
            end = partition(a, b, pred_L(plane, pos, cbnd));
            sz = distance(begin, end);

            if (sz)
            {
                if (KAZE_KDT_NODELEAF_MAXFACE < sz && (level + 1 < max_level))
                { //branch
                    vector3 min = cbnd.min();
                    vector3 max = cbnd.max();
                    max[plane] = pos;

                    ap_l.reset(new kdtm_node_branch(begin, end, bound(min, max), level + 1, max_level, prop));
                }
                else
                {
                    if (prop.singleside)
                    {
                        ap_l.reset(new kdtm_node_leaf_SS(begin, end));
                    }
                    else
                    {
                        ap_l.reset(new kdtm_node_leaf_DS(begin, end));
                    }
                    prg.now += sz;
                }
            }

            //begin = a;
            //end   = partition(a,b,pred_R(plane,pos,cbnd));
            begin = partition(begin, end, pred_L_ONLY(plane, pos, cbnd));
            end = b;
            sz = distance(begin, end);

            if (sz)
            {
                if (KAZE_KDT_NODELEAF_MAXFACE < sz && (level + 1 < max_level))
                { //branch
                    vector3 min = cbnd.min();
                    min[plane] = pos;
                    vector3 max = cbnd.max();

                    ap_r.reset(new kdtm_node_branch(begin, end, bound(min, max), level + 1, max_level, prop));
                }
                else
                {
                    if (prop.singleside)
                    {
                        ap_r.reset(new kdtm_node_leaf_SS(begin, end));
                    }
                    else
                    {
                        ap_r.reset(new kdtm_node_leaf_DS(begin, end));
                    }
                    prg.now += sz;
                }
            }

            nodes_[0] = ap_l.release();
            nodes_[1] = ap_r.release();

            ax_.set(pos, plane);
        }

#if 0
		inline int eval_phase(int phase,int axis){//rid<0
			return (phase>>axis) & 0x1;
		}
#else
        inline int eval_phase(int phase, int axis)
        { //rid<0
            static const int table[][3] = {
                {0, 0, 0}, //0
                {1, 0, 0}, //1
                {0, 1, 0}, //2
                {1, 1, 0}, //3
                {0, 0, 1}, //4
                {1, 0, 1}, //5
                {0, 1, 1}, //6
                {1, 1, 1}  //7
            };
            return table[phase][axis];
        }
#endif

#if KAZE_KDT_USE_NULLOBJECT

#define KAZE_TEST(N, r, a, b) (N->test(r, a, b))
#define KAZE_TEST_INFO(N, i, r, a, b) (N->test(i, r, a, b))

#else

#define KAZE_TEST(N, r, a, b) (N && N->test(r, a, b))
#define KAZE_TEST_INFO(N, i, r, a, b) (N && N->test(i, r, a, b))

#endif

        bool kdtm_node_branch::test(const ray& r, real tmin, real tmax) const
        {
            //static const real EPSILON = values::epsilon()*1000;

            const vector3& org = r.origin();
            const vector3& idir = r.inversed_direction();
            int phase = r.phase();

            int axis = ax_.get_axis();
            real pos = ax_.get_value();

            real ro = org[axis];
            real rid = idir[axis];

            int n_s = eval_phase(phase, axis);
            int f_s = 1 - n_s;

            real tpos = (pos - ro) * rid;

            if (tmax < tpos)
            { //min < max < pos
                if (KAZE_TEST(nodes_[n_s], r, tmin, tmax)) return true;
            }
            else if (tmin < tpos)
            { //min < pos < max
                if (KAZE_TEST(nodes_[n_s], r, tmin, tpos)) return true;
                if (KAZE_TEST(nodes_[f_s], r, tpos, tmax)) return true;
            }
            else
            { //pos < min < max
                if (KAZE_TEST(nodes_[f_s], r, tmin, tmax)) return true;
            }

            return false;
        }
        bool kdtm_node_branch::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
            //static const real EPSILON = values::epsilon()*1000;

            const vector3& org = r.origin();
            const vector3& idir = r.inversed_direction();
            int phase = r.phase();

            int axis = ax_.get_axis();
            real pos = ax_.get_value();

            real ro = org[axis];
            real rid = idir[axis];

            int n_s = eval_phase(phase, axis);
            int f_s = 1 - n_s;

            real tpos = (pos - ro) * rid;

            if (tmax < tpos)
            { //min < max < pos
                if (KAZE_TEST_INFO(nodes_[n_s], info, r, tmin, tmax)) return true;
            }
            else if (tmin < tpos)
            { //min < pos < max
                if (KAZE_TEST_INFO(nodes_[n_s], info, r, tmin, tpos)) return true;
                if (KAZE_TEST_INFO(nodes_[f_s], info, r, tpos, tmax)) return true;
            }
            else
            { //pos < min < max
                if (KAZE_TEST_INFO(nodes_[f_s], info, r, tmin, tmax)) return true;
            }

            return false;
        }
#undef KAZE_TEST_INFO
#undef KAZE_TEST
    }

    //------------------------------------------------------------------
    //kdtree_mesh_accelerator_imp
    //

    class kdtree_mesh_accelerator_imp
    {
    public:
        virtual bool test(const ray& r, real dist) const = 0;
        virtual bool test(test_info* info, const ray& r, real dist) const = 0;

        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;

        virtual size_t size() const { return 0; }
        virtual size_t memory_size() const { return 0; }
    };

    namespace
    { //
        class kdtree_root : public kdtree_mesh_accelerator_imp
        {
        public:
            kdtree_root(const vector3& min, const vector3& max);
            ~kdtree_root();
            bool test(const ray& r, real dist) const
            {
                range_AABB rng;
                if (test_AABB(&rng, this->min_, this->max_, r, dist))
                {
                    return imp_->test(r, std::max<real>(0, rng.tmin), std::min<real>(rng.tmax, dist));
                }
                return false;
            }
            bool test(test_info* info, const ray& r, real dist) const
            {
                range_AABB rng;
                if (test_AABB(&rng, this->min_, this->max_, r, dist))
                {
                    return imp_->test(info, r, std::max<real>(0, rng.tmin), std::min<real>(rng.tmax, dist));
                }
                return false;
            }
            vector3 min() const { return min_; }
            vector3 max() const { return max_; }

            size_t size() const { return imp_->size(); }
            size_t memory_size() const { return sizeof(kdtree_root) + imp_->memory_size(); }

        public:
            bool test_internal(const ray& r, real dist) const
            {
                assert(imp_);
                return imp_->test(r, 0, dist);
            }

            bool test_internal(test_info* info, const ray& r, real dist) const
            {
                assert(imp_);
                return imp_->test(info, r, 0, dist);
            }

            bool test_internal2(const ray& r, real tmin, real tmax) const
            {
                assert(imp_);
                return imp_->test(r, tmin, tmax);
            }

            bool test_internal2(test_info* info, const ray& r, real tmin, real tmax) const
            {
                assert(imp_);
                return imp_->test(info, r, tmin, tmax);
            }

            void add(face_iterator begin, face_iterator end, const mesh_accelerator_property& prop);

        private:
            vector3 min_;
            vector3 max_;

            kdtm_tree_node* imp_;
        };

        kdtree_root::kdtree_root(const vector3& min, const vector3& max) : imp_(0), min_(min), max_(max) {}
        kdtree_root::~kdtree_root()
        {
            if (imp_) delete imp_;
        }
        void kdtree_root::add(face_iterator begin, face_iterator end, const mesh_accelerator_property& prop)
        {
            using namespace std;
            if (imp_) delete imp_;

            size_t csz = end - begin;
            int max_level = get_level(csz);

            imp_ = new kdtm_node_branch(begin, end, bound(min_, max_), 0, max_level, prop);
        }
    }

    //grid_root
    namespace
    {
        typedef int int_range[2];

        class grid_root : public kdtree_mesh_accelerator_imp
        {
        private:
            typedef const kdtree_root* P_C_TR;

        public:
            grid_root(const vector3& min, const vector3& max);
            ~grid_root();
            void add(face_iterator begin, face_iterator end, const mesh_accelerator_property& prop);

        public:
            bool test(const ray& r, real dist) const;
            bool test(test_info* info, const ray& r, real dist) const;

            vector3 min() const { return min_; }
            vector3 max() const { return max_; }

            size_t size() const
            {
                size_t sz = 0;
                for (int k = 0; k < ndiv_[2]; k++)
                {
                    for (int j = 0; j < ndiv_[1]; j++)
                    {
                        for (int i = 0; i < ndiv_[0]; i++)
                        {
                            size_t idx = (size_t)((k * ndiv_[1] + j) * ndiv_[0] + i);
                            if (nodes_[idx])
                            {
                                sz += nodes_[idx]->size();
                            }
                        }
                    }
                }
                return sz;
            }
            size_t memory_size() const
            {
                size_t sz = sizeof(grid_root);

                sz += sizeof(kdtree_root*) * ndiv_[0] * ndiv_[1] * ndiv_[2];
                for (int k = 0; k < ndiv_[2]; k++)
                {
                    for (int j = 0; j < ndiv_[1]; j++)
                    {
                        for (int i = 0; i < ndiv_[0]; i++)
                        {
                            size_t idx = (size_t)((k * ndiv_[1] + j) * ndiv_[0] + i);
                            if (nodes_[idx])
                            {
                                sz += nodes_[idx]->memory_size();
                            }
                        }
                    }
                }
                return sz;
            }

        protected:
            void set_face(int_range range[3], face_iterator begin, face_iterator end, const bound& bnd, const mesh_accelerator_property& prop);

        private:
            vector3 min_;
            vector3 max_;
            real delta_[3];
            real idelta_[3];
            int ndiv_[3];

            //mmap_array<kdtree_root*> nodes_;
            std::vector<kdtree_root*> nodes_;
        };

        grid_root::grid_root(const vector3& min, const vector3& max)
        {
            static const real EPSILON = values::epsilon() * 1000;
            static const int DIVNUM = KAZE_GRID_MAXDIV;

            vector3 wide((max - min) * (1 + EPSILON));
            //vector3 wide((max - min));

            int max_edge = 0;
            if (wide[max_edge] < wide[1]) max_edge = 1;
            if (wide[max_edge] < wide[2]) max_edge = 2;

//---------------
//0,1,2

#if 0
		ndiv_[0] = std::max<int>(1, int( ((wide[0]/wide[max_edge]) * DIVNUM) ));
		ndiv_[1] = std::max<int>(1, int( ((wide[1]/wide[max_edge]) * DIVNUM) ));
		ndiv_[2] = std::max<int>(1, int( ((wide[2]/wide[max_edge]) * DIVNUM) ));

		delta_[0] = wide[0]/ndiv_[0];
		delta_[1] = wide[1]/ndiv_[1];
		delta_[2] = wide[2]/ndiv_[2];
		
		idelta_[0] = ndiv_[0]/wide[0];
		idelta_[1] = ndiv_[1]/wide[1];
		idelta_[2] = ndiv_[2]/wide[2];
#else
            real delta = wide[max_edge] / DIVNUM;
            real idelta = DIVNUM / wide[max_edge];

            delta_[0] = delta;
            delta_[1] = delta;
            delta_[2] = delta;

            idelta_[0] = idelta;
            idelta_[1] = idelta;
            idelta_[2] = idelta;

            ndiv_[0] = std::max<int>(1, (int)ceil(wide[0] * idelta_[0]));
            ndiv_[1] = std::max<int>(1, (int)ceil(wide[1] * idelta_[1]));
            ndiv_[2] = std::max<int>(1, (int)ceil(wide[2] * idelta_[2]));

            ndiv_[0] = std::min<int>(DIVNUM, ndiv_[0]);
            ndiv_[1] = std::min<int>(DIVNUM, ndiv_[1]);
            ndiv_[2] = std::min<int>(DIVNUM, ndiv_[2]);

            ndiv_[max_edge] = DIVNUM;
#endif

            size_t sz = ndiv_[0] * ndiv_[1] * ndiv_[2];
            nodes_.resize(sz);

            std::memset(&nodes_[0], 0, sz * sizeof(kdtree_root*)); //set NULL

            this->min_ = min;
            this->max_ = max;
        }

        grid_root::~grid_root()
        {
            size_t sz = nodes_.size();
            for (size_t i = 0; i < sz; i++)
            {
                if (nodes_[i]) delete nodes_[i];
            }
        }

#if 0		
		inline bool is_nounit(int delta[3] ){
			size_t csz = (size_t)(delta[0]*delta[1]*delta[2]);
			//print_log("csz = %d\n",csz);
			assert(csz);//need not zero
			return (csz > 1);
		}
#else
        inline int is_nounit(int delta[3])
        {
            //return 0x1 == (delta[0]^delta[1]^delta[2]);
            return (0x1 != delta[0]) || (0x1 != delta[1]) || (0x1 != delta[2]);
        }
#endif

        void grid_root::set_face(int_range range[3], face_iterator begin, face_iterator end, const bound& bnd, const mesh_accelerator_property& prop)
        {
            using namespace std;

            int delta[3];

            delta[0] = range[0][1] - range[0][0];
            delta[1] = range[1][1] - range[1][0];
            delta[2] = range[2][1] - range[2][0];

            if (is_nounit(delta))
            {
                int plane = 0;
                if (delta[plane] < delta[1]) plane = 1;
                if (delta[plane] < delta[2]) plane = 2;

                //1,2,3,,,4  3 1
                int div = range[plane][0] + (delta[plane] >> 1);
                int_range crange[3];
                memcpy(crange, range, sizeof(crange));

                vector3 min, max;
                face_iterator cbegin, cend;
                size_t ccsz;

                //-----------------------------------------
                real pos = min_[plane] + div * delta_[plane];

                //min[plane] = min_[plane] + crange[plane][0] * delta_[plane];

                /*						
				min[0] = min_[0] + crange[0][0] * delta_[0];
				min[1] = min_[1] + crange[1][0] * delta_[1];
				min[2] = min_[2] + crange[2][0] * delta_[2];

				max[0] = min_[0] + crange[0][1] * delta_[0];
				max[1] = min_[1] + crange[1][1] * delta_[1];
				max[2] = min_[2] + crange[2][1] * delta_[2];
				*/

                cbegin = begin;
                cend = partition(begin, end, pred_L(plane, pos, bnd));

                ccsz = distance(cbegin, cend);
                if (ccsz)
                {
                    min = bnd.min();
                    max = bnd.max();
                    max[plane] = pos;

                    crange[plane][0] = range[plane][0]; //min = org
                    crange[plane][1] = div;             //max = div
                    set_face(crange, cbegin, cend, bound(min, max), prop);
                }

                //-----------------------------------------

                /*
				min[0] = min_[0] + crange[0][0] * delta_[0];
				min[1] = min_[1] + crange[1][0] * delta_[1];
				min[2] = min_[2] + crange[2][0] * delta_[2];

				max[0] = min_[0] + crange[0][1] * delta_[0];
				max[1] = min_[1] + crange[1][1] * delta_[1];
				max[2] = min_[2] + crange[2][1] * delta_[2];
				*/

                //cbegin = begin;
                //cend = partition(begin,end,pred_R(plane,pos,bnd));
                cbegin = partition(cbegin, cend, pred_L_ONLY(plane, pos, bnd));
                cend = end;

                ccsz = distance(cbegin, cend);
                if (ccsz)
                {
                    min = bnd.min();
                    min[plane] = pos;
                    max = bnd.max();

                    crange[plane][0] = div;
                    crange[plane][1] = range[plane][1]; //max = org
                    set_face(crange, cbegin, cend, bound(min, max), prop);
                }

                //-----------------------------------------
            }
            else
            { //1

                size_t idx = (size_t)((range[2][0] * ndiv_[1] + range[1][0]) * ndiv_[0] + range[0][0]);
                //size_t idx = range[2][0] * ndiv_[1]*ndiv_[0] + range[1][0] * ndiv_[0] + range[0][0];

                assert(idx < (size_t)(ndiv_[0] * ndiv_[1] * ndiv_[2]));
                /*
				vector3 min,max;
				
				min[0] = min_[0] + range[0][0] * delta_[0];
				min[1] = min_[1] + range[1][0] * delta_[1];
				min[2] = min_[2] + range[2][0] * delta_[2];

				max[0] = min_[0] + range[0][1] * delta_[0];
				max[1] = min_[1] + range[1][1] * delta_[1];
				max[2] = min_[2] + range[2][1] * delta_[2];
	*/

                std::unique_ptr<kdtree_root> ap(new kdtree_root(bnd.min(), bnd.max()));
                ap->add(begin, end, prop);

                if (nodes_[idx]) delete nodes_[idx];

                nodes_[idx] = ap.release();
            }
        }

        void grid_root::add(face_iterator begin, face_iterator end, const mesh_accelerator_property& prop)
        {
            int_range range[3];

            range[0][0] = 0;
            range[0][1] = ndiv_[0];
            range[1][0] = 0;
            range[1][1] = ndiv_[1];
            range[2][0] = 0;
            range[2][1] = ndiv_[2];

            vector3 wide;

            wide[0] = ndiv_[0] * delta_[0];
            wide[1] = ndiv_[1] * delta_[1];
            wide[2] = ndiv_[2] * delta_[2];

            this->set_face(range, begin, end, bound(min_, min_ + wide), prop);
        }

        bool grid_root::test(const ray& r, real dist) const
        {
            vector3 pos;
            if (!test_AABB(&pos, min_, max_, r, dist)) return false;

            //vector3 ird( real(1) / r.direction()[0], real(1) / r.direction()[1],  real(1) / r.direction()[2]);
            const vector3& org = r.origin();
            const vector3& ird = r.inversed_direction();
            const int* ndiv = this->ndiv_;

            int axis[3];
            int step[3];
            real td[3];
            real tn[3];
            int out[3];

            //d = delta/D
            //d*D = delta
            //t for delta
            //t for next axis

            int phase = 0;
            for (size_t idx = 0; idx < 3; idx++)
            {
                axis[idx] = static_cast<int>((pos[idx] - min_[idx]) * idelta_[idx]);
                //if(axis[idx] == ndiv[idx])axis[idx]--;
                assert(axis[idx] != ndiv[idx]);

                if (ird[idx] > 0)
                { //plus
                    step[idx] = 1;
                    td[idx] = delta_[idx] * ird[idx];                                              //td * rd  = delta
                    tn[idx] = ((min_[idx] + (axis[idx] + 1) * delta_[idx]) - org[idx]) * ird[idx]; // next t distance
                    out[idx] = ndiv[idx];                                                          // out idx
                }
                else if (ird[idx] < 0)
                { //minus
                    step[idx] = -1;
                    td[idx] = -delta_[idx] * ird[idx];
                    tn[idx] = ((min_[idx] + (axis[idx]) * delta_[idx]) - org[idx]) * ird[idx];
                    out[idx] = -1;
                }
                else
                {
                    step[idx] = 0;
                    td[idx] = 0;
                    tn[idx] = values::far();
                    out[idx] = -1;
                }
            }

            //debug
            assert(axis[0] >= 0);
            assert(axis[1] >= 0);
            assert(axis[2] >= 0);

            assert(axis[0] < ndiv_[0]);
            assert(axis[1] < ndiv_[1]);
            assert(axis[2] < ndiv_[2]);
            //debug

            const P_C_TR* nodes = &(nodes_[0]);

            while (true)
            {
                //test matrix
                size_t idx = (size_t)((axis[2] * ndiv[1] + axis[1]) * ndiv[0] + axis[0]);
                //size_t idx = k * ndiv_[1]*ndiv_[0] + j * ndiv_[0] + i;

                //if(nodes[idx] && nodes[idx]->test_internal(r, dist))return true;
                if (nodes[idx] && nodes[idx]->test_internal2(r, 0, dist)) return true;

                int min_axis = 0;
                if (tn[min_axis] > tn[1]) min_axis = 1;
                if (tn[min_axis] > tn[2]) min_axis = 2;

                /* 3D DDA */
                if (dist < tn[min_axis]) break;
                axis[min_axis] += step[min_axis];
                if (axis[min_axis] == out[min_axis]) break;
                tn[min_axis] += td[min_axis];
            }

            return false;
        }

        namespace
        {
            real max3(real a, real b, real c)
            {
                using namespace std;
                return max(max(a, b), c);
            }
        }
        bool grid_root::test(test_info* info, const ray& r, real dist) const
        {
            //get first intersection
            vector3 pos;
            if (!test_AABB(&pos, min_, max_, r, dist)) return false;

            //vector3 ird( real(1) / r.direction()[0], real(1) / r.direction()[1],  real(1) / r.direction()[2]);
            const vector3& org = r.origin();
            const vector3& ird = r.inversed_direction();
            const int* ndiv = this->ndiv_;

            int axis[3];
            int step[3];
            real td[3];
            real tn[3];
            int out[3];

            int phase = 0;
            for (size_t idx = 0; idx < 3; idx++)
            {
                axis[idx] = static_cast<int>((pos[idx] - min_[idx]) * idelta_[idx]);
                //if(axis[idx] == ndiv[idx])axis[idx]--;
                assert(axis[idx] != ndiv[idx]);

                if (ird[idx] > 0)
                { //plus
                    step[idx] = 1;
                    td[idx] = delta_[idx] * ird[idx];
                    tn[idx] = ((min_[idx] + (axis[idx] + 1) * delta_[idx]) - org[idx]) * ird[idx];
                    out[idx] = ndiv[idx];
                }
                else if (ird[idx] < 0)
                { //minus
                    step[idx] = -1;
                    td[idx] = -delta_[idx] * ird[idx];
                    tn[idx] = ((min_[idx] + (axis[idx]) * delta_[idx]) - org[idx]) * ird[idx];
                    out[idx] = -1;
                }
                else
                {
                    step[idx] = 0;
                    td[idx] = 0;
                    tn[idx] = values::far();
                    out[idx] = -1;
                }
            }

            //debug
            assert(axis[0] >= 0);
            assert(axis[1] >= 0);
            assert(axis[2] >= 0);

            assert(axis[0] < ndiv[0]);
            assert(axis[1] < ndiv[1]);
            assert(axis[2] < ndiv[2]);
            //debug

            const P_C_TR* nodes = &(nodes_[0]);

            bool bRet = false;

            while (true)
            {
                //test matrix
                size_t idx = (size_t)((axis[2] * ndiv[1] + axis[1]) * ndiv[0] + axis[0]);
                //size_t idx = k * ndiv[1]*ndiv[0] + j * ndiv[0] + i;

                //if(nodes[idx] && nodes[idx]->test_internal(info, r, dist))return true;
                //if(nodes[idx] && nodes[idx]->test_internal2(info, r,0,dist))return true;

                if (nodes[idx] && nodes[idx]->test_internal2(info, r, 0, dist))
                {
                    bRet = true;
                    dist = info->distance;
                }

                int min_axis = 0;
                if (tn[min_axis] > tn[1]) min_axis = 1;
                if (tn[min_axis] > tn[2]) min_axis = 2;

                /* 3D DDA */
                if (dist < tn[min_axis]) break;
                axis[min_axis] += step[min_axis];
                if (axis[min_axis] == out[min_axis]) break;
                tn[min_axis] += td[min_axis];
            }
            return bRet;
        }
    }

    kdtree_mesh_accelerator::kdtree_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        size_t csz = end - begin;
        prg.now = 0;
        prg.max = csz;
        prg.isz = 100.0 / csz;
        prg.cnt = 0;

        std::unique_ptr<kdtree_root> ap(new kdtree_root(min, max));
        //std::unique_ptr<grid_root> ap(new grid_root(min,max));
        ap->add(begin, end, prop);

        //print_log("%d,%d\n",csz,ap->size());
        //print_log("kdtree_mesh_accelerator:memory size:%d bytes\n", ap->memory_size());

        imp_ = ap.release();
    }

    kdtree_mesh_accelerator::~kdtree_mesh_accelerator()
    {
        delete imp_;
    }

    bool kdtree_mesh_accelerator::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool kdtree_mesh_accelerator::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }

    vector3 kdtree_mesh_accelerator::min() const
    {
        return imp_->min();
    }

    vector3 kdtree_mesh_accelerator::max() const
    {
        return imp_->max();
    }

    size_t kdtree_mesh_accelerator::memory_size() const
    {
        return imp_->memory_size();
    }
}
