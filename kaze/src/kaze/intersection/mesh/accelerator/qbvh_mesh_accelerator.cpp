#include "qbvh_mesh_accelerator.h"

#include "intersection_ex.h"
#include "test_info.h"
#include "logger.h"

#include "mesh_face.h"

#include "values.h"

#include "aligned_vector.hpp"

#include <memory>
#include <cstring>
#include <cmath>
#include <functional>
#include <algorithm>
#include <vector>
#include <utility>

#include <xmmintrin.h>
#include <emmintrin.h>

#include <stdlib.h>

namespace kaze
{

    typedef const mesh_face* PCFACE;

    static const int MIN_FACE = 4;

    static const int STACK_SIZE = ((int)(sizeof(size_t) * 16));

    static const float FEPS = 0;

    namespace
    {

        //For float
        static inline int test_AABB(
            const __m128 bboxes[2][3], //4boxes : min-max[2] of xyz[3] of boxes[4]
            const __m128 org[3],       //ray origin
            const __m128 idir[3],      //ray inveresed direction
            const int sign[3],         //ray xyz direction -> +:0,-:1
            __m128 tmin, __m128 tmax   //ray range tmin-tmax
            )
        {
            // x coordinate
            tmin = _mm_max_ps(
                tmin,
                _mm_mul_ps(_mm_sub_ps(bboxes[sign[0]][0], org[0]), idir[0]));
            tmax = _mm_min_ps(
                tmax,
                _mm_mul_ps(_mm_sub_ps(bboxes[1 - sign[0]][0], org[0]), idir[0]));

            // y coordinate
            tmin = _mm_max_ps(
                tmin,
                _mm_mul_ps(_mm_sub_ps(bboxes[sign[1]][1], org[1]), idir[1]));
            tmax = _mm_min_ps(
                tmax,
                _mm_mul_ps(_mm_sub_ps(bboxes[1 - sign[1]][1], org[1]), idir[1]));

            // z coordinate
            tmin = _mm_max_ps(
                tmin,
                _mm_mul_ps(_mm_sub_ps(bboxes[sign[2]][2], org[2]), idir[2]));
            tmax = _mm_min_ps(
                tmax,
                _mm_mul_ps(_mm_sub_ps(bboxes[1 - sign[2]][2], org[2]), idir[2]));

            return _mm_movemask_ps(_mm_cmpge_ps(tmax, tmin));
        }

        static inline vector3 conv(const vector3f& v)
        {
            return vector3(v[0], v[1], v[2]);
        }

        static inline int test_AABB(const vector3f bboxes[4][2], const ray& r, real tmin, real tmax)
        {
            int nMask = 0;
            for (int i = 0; i < 4; i++)
            {
                if (test_AABB_simple(conv(bboxes[i][0]), conv(bboxes[i][1]), r, tmin, tmax))
                {
                    nMask |= (1 << i);
                }
            }
            return nMask;
        }

        /*
    template<bool b64 = true>
    struct size_t_helper{
        static const size_t sign_mask  = 0x8000000000000000ULL;
        static const size_t empty_mask = 0xFFFFFFFFFFFFFFFFULL;
    };
    template<>
    struct size_t_helper<false>{
        static const size_t sign_mask  = 0x80000000U;
        static const size_t empty_mask = 0xFFFFFFFFU;
    };
    static const size_t SIGN_MASK   = size_t_helper<sizeof(size_t)==8>::sign_mask;
    static const size_t EMPTY_MASK  = size_t_helper<sizeof(size_t)==8>::empty_mask;
    */
        static const size_t EMPTY_MASK = size_t(-1);        //0xFFFF...
        static const size_t SIGN_MASK = ~(EMPTY_MASK >> 1); //0x8000...

#pragma pack(push, 4)
        struct SIMDBVHNode
        {
            __m128 bboxes[2][3]; //768
            size_t children[4];  //128
            int axis_top;        //128
            int axis_right;
            int axis_left;
            int reserved;
        };
#pragma pack(pop)

        struct CPUBVHNode
        {
            vector3f bboxes[4][2];
            size_t children[4];
            int axis_top;
            int axis_right;
            int axis_left;
            int reserved;
        };

        inline bool IsLeaf(size_t i)
        {
            return (SIGN_MASK & i) ? true : false;
        }
        inline bool IsBranch(size_t i)
        {
            return !IsLeaf(i);
        }
        inline bool IsEmpty(size_t i)
        {
            return i == EMPTY_MASK;
        }

        inline size_t MakeLeafIndex(size_t i)
        {
            return SIGN_MASK | i;
        }

        inline size_t GetFaceFirst(size_t i)
        {
            return (~SIGN_MASK) & i;
        }

        /*
void print_table(){ 
  using namespace std;

  cout << "static const int OrderTable[] = {" << endl; 
  for(int visit = 0;visit<16;visit++){
    cout << "    ";
    for(int sign = 0;sign<8;sign++){
      int top   = sign & 4;
      int left  = sign & 2;
      int right = sign & 1;

      int order[] = {0,1,2,3};
      if(left ) swap(order[0],order[1]);
      if(right) swap(order[2],order[3]);
      if(top  ){swap(order[0],order[2]);swap(order[1],order[3]);}
      
      char cOut[4] = {'4','4','4','4'};
      int c = 0;
      for(int i = 0;i<4;i++){
        if(visit & (1<<order[i])){
          cOut[c++] = '0'+ order[i]; 
        }
      }
      char cOut2[4] = {'4','4','4','4'};
      for(int i = 0;i<c;i++){//reverse for stack
        cOut2[i] = cOut[c-1-i];
      }
      cout << "0x" << '4' << cOut2[3] << cOut2[2] << cOut2[1] << cOut2[0]<< ",";
    }
    cout << endl;
  }
  cout << "};" << endl;
}
*/

        //visit order table
        //8*16 = 128
        static const int OrderTable[] = {
            0x44444, 0x44444, 0x44444, 0x44444, 0x44444, 0x44444, 0x44444, 0x44444,
            0x44440, 0x44440, 0x44440, 0x44440, 0x44440, 0x44440, 0x44440, 0x44440,
            0x44441, 0x44441, 0x44441, 0x44441, 0x44441, 0x44441, 0x44441, 0x44441,
            0x44401, 0x44401, 0x44410, 0x44410, 0x44401, 0x44401, 0x44410, 0x44410,
            0x44442, 0x44442, 0x44442, 0x44442, 0x44442, 0x44442, 0x44442, 0x44442,
            0x44402, 0x44402, 0x44402, 0x44402, 0x44420, 0x44420, 0x44420, 0x44420,
            0x44412, 0x44412, 0x44412, 0x44412, 0x44421, 0x44421, 0x44421, 0x44421,
            0x44012, 0x44012, 0x44102, 0x44102, 0x44201, 0x44201, 0x44210, 0x44210,
            0x44443, 0x44443, 0x44443, 0x44443, 0x44443, 0x44443, 0x44443, 0x44443,
            0x44403, 0x44403, 0x44403, 0x44403, 0x44430, 0x44430, 0x44430, 0x44430,
            0x44413, 0x44413, 0x44413, 0x44413, 0x44431, 0x44431, 0x44431, 0x44431,
            0x44013, 0x44013, 0x44103, 0x44103, 0x44301, 0x44301, 0x44310, 0x44310,
            0x44423, 0x44432, 0x44423, 0x44432, 0x44423, 0x44432, 0x44423, 0x44432,
            0x44023, 0x44032, 0x44023, 0x44032, 0x44230, 0x44320, 0x44230, 0x44320,
            0x44123, 0x44132, 0x44123, 0x44132, 0x44231, 0x44321, 0x44231, 0x44321,
            0x40123, 0x40132, 0x41023, 0x41032, 0x42301, 0x43201, 0x42310, 0x43210,
        };
    }

    namespace
    {
        class face_sorter : public std::binary_function<PCFACE, PCFACE, bool>
        {
        public:
            face_sorter(int plane) : plane_(plane) {}

            bool operator()(PCFACE a, PCFACE b) const
            {
                int plane = plane_;
                real ax = (*a->p_p0)[plane] + (*a->p_p1)[plane] + (*a->p_p2)[plane];
                real bx = (*b->p_p0)[plane] + (*b->p_p1)[plane] + (*b->p_p2)[plane];
                return ax < bx;
            }

        private:
            int plane_;
        };

        class pred_L : public std::unary_function<PCFACE, bool>
        {
        public:
            pred_L(int axis, real pos) : axis_(axis), pos_(pos) {}

            bool operator()(PCFACE p_face) const
            {
                int axis = axis_;
                real pos = pos_;

                const vector3& rtx0 = (*(p_face->p_p0));
                const vector3& rtx1 = (*(p_face->p_p1));
                const vector3& rtx2 = (*(p_face->p_p2));

                real center = (rtx0[axis] + rtx1[axis] + rtx2[axis]);

                return (center < pos * 3);
            }

        private:
            int axis_;
            real pos_;
        };
    }

    class qbvh_mesh_accelerator_imp
    {
    public:
        virtual ~qbvh_mesh_accelerator_imp() {}
    public:
        virtual size_t memory_size() const = 0;

    protected:
        virtual bool test_inner(const ray& r, real tmin, real tmax) const = 0;
        virtual bool test_inner(test_info* info, const ray& r, real tmin, real tmax) const = 0;

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        const vector3& min() const;
        const vector3& max() const;

    protected:
        inline bool test_faces(size_t nFirst, const ray& r, real dist) const;
        inline bool test_faces(size_t nFirst, test_info* info, const ray& r, real dist) const;

        vector3 min_;
        vector3 max_;
        int nTypeFace_;
        std::vector<PCFACE> faces_;
    };

    bool qbvh_mesh_accelerator_imp::test_faces(size_t nFirst, const ray& r, real dist) const
    {
        const PCFACE* faces = &faces_[0];
        if (nTypeFace_ == 0)
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (faces[i]->test_DS(r, dist)) return true;
                i++;
            }
        }
        else
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (faces[i]->test_SS(r, dist)) return true;
                i++;
            }
        }
        return false;
    }

    bool qbvh_mesh_accelerator_imp::test_faces(size_t nFirst, test_info* info, const ray& r, real dist) const
    {
        const PCFACE* faces = &faces_[0];
        bool bRet = false;
        if (nTypeFace_ == 0)
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (faces[i]->test_DS(info, r, dist))
                {
                    dist = info->distance;
                    bRet = true;
                }
                i++;
            }
        }
        else
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (faces[i]->test_SS(info, r, dist))
                {
                    dist = info->distance;
                    bRet = true;
                }
                i++;
            }
        }
        return bRet;
    }

    const vector3& qbvh_mesh_accelerator_imp::min() const
    {
        return min_;
    }

    const vector3& qbvh_mesh_accelerator_imp::max() const
    {
        return max_;
    }

    bool qbvh_mesh_accelerator_imp::test(const ray& r, real dist) const
    {
        real tmin = 0;
        real tmax = dist;
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            return test_inner(r, tmin, tmax);
        }
        else
        {
            return false;
        }
    }

    bool qbvh_mesh_accelerator_imp::test(test_info* info, const ray& r, real dist) const
    {
        real tmin = 0;
        real tmax = dist;
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            return test_inner(info, r, tmin, tmax);
        }
        else
        {
            return false;
        }
    }
    //-------------------------------------------------------------------

    static float safe_convert(double x)
    {
        static const double FMAX = +(double)std::numeric_limits<float>::max();
        static const double FMIN = -(double)std::numeric_limits<float>::max();
        if (FMAX < x) return (float)FMAX;
        if (x < FMIN) return (float)FMIN;
        return (float)x;
    }

    static inline float safe_convert(float x)
    {
        return x;
    }

    static inline vector3f safe_convert(const vector3& v)
    {
        return vector3f(safe_convert(v[0]), safe_convert(v[1]), safe_convert(v[2]));
    }

    static void get_minmax(PCFACE face, vector3& pmin, vector3& pmax)
    {
        const real FAR = values::far();
        vector3 min = vector3(+FAR, +FAR, +FAR);
        vector3 max = vector3(-FAR, -FAR, -FAR);

        vector3 points[3];
        points[0] = *(face->p_p0);
        points[1] = *(face->p_p1);
        points[2] = *(face->p_p2);

        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (points[i][j] < min[j]) min[j] = points[i][j];
                if (points[i][j] > max[j]) max[j] = points[i][j];
            }
        }

        pmin = min;
        pmax = max;
    }

    static void get_minmax(PCFACE* a, PCFACE* b, vector3& pmin, vector3& pmax)
    {
        const real FAR = values::far();
        vector3 min = vector3(+FAR, +FAR, +FAR);
        vector3 max = vector3(-FAR, -FAR, -FAR);

        for (PCFACE* i = a; i != b; i++)
        {
            vector3 cmin, cmax;
            get_minmax(*i, cmin, cmax);
            for (int j = 0; j < 3; j++)
            {
                if (cmin[j] < min[j]) min[j] = cmin[j];
                if (cmax[j] > max[j]) max[j] = cmax[j];
            }
        }

        pmin = min;
        pmax = max;
    }

    static int get_plane(const vector3& min, const vector3& max)
    {
        vector3 wid = max - min;
        int plane = 0;
        if (wid[1] > wid[plane]) plane = 1;
        if (wid[2] > wid[plane]) plane = 2;
        return plane;
    }

    static void find_sort(PCFACE* mid, PCFACE* a, PCFACE* b, real min, real max, int plane)
    {
        if (a <= mid && mid < b)
        {
            size_t sz = b - a;
            if (sz <= 256)
            {
                std::sort(a, b, face_sorter(plane));
            }
            else
            {
                real pos = (min + max) * 0.5;
                PCFACE* c = std::partition(a, b, pred_L(plane, pos));
                if (a == c || b == c)
                {
                    std::sort(a, b, face_sorter(plane));
                }
                else
                {
                    find_sort(mid, a, c, min, pos, plane);
                    find_sort(mid, c, b, pos, max, plane);
                }
            }
        }
    }

    static size_t get_node_size(size_t face_num)
    {
        if (face_num <= MIN_FACE) return 1;
        size_t p = face_num / 4;
        if (face_num & 3) p++;
        return std::max<size_t>(1, 1 + 4 * get_node_size(p));
    }
    //-------------------------------------------------------------------

    class simd_qbvh_mesh_accelerator_imp : public qbvh_mesh_accelerator_imp
    {
    public:
        simd_qbvh_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);

        size_t memory_size() const;

    protected:
        bool test_inner(const ray& r, real tmin, real tmax) const;
        bool test_inner(test_info* info, const ray& r, real tmin, real tmax) const;

    protected:
        size_t construct(PCFACE* begin, PCFACE* end, const vector3& min, const vector3& max, int level);

    protected:
        aligned_vector<SIMDBVHNode> nodes_;
    };

    size_t simd_qbvh_mesh_accelerator_imp::memory_size() const
    {
        return sizeof(simd_qbvh_mesh_accelerator_imp) + sizeof(SIMDBVHNode) * nodes_.capacity() + sizeof(PCFACE) * faces_.capacity();
    }

    size_t simd_qbvh_mesh_accelerator_imp::construct(PCFACE* begin, PCFACE* end, const vector3& min, const vector3& max, int level)
    {
        static const real EPSILON = (real)std::numeric_limits<float>::epsilon();

        size_t sz = end - begin;
        if (sz == 0) return EMPTY_MASK;
        if (level >= 30 || sz <= MIN_FACE && !nodes_.empty())
        { //
            size_t first = faces_.size();
            size_t last = first + sz + 1; //zero terminate
            faces_.resize(last);
            for (size_t i = first; i < last; i++)
            {
                faces_[i] = 0;
            }

            for (size_t i = 0; i < sz; i++)
            {
                faces_[first + i] = *(begin + i);
            }
            size_t nRet = MakeLeafIndex(first);
            assert(IsLeaf(nRet));
            return nRet;
        }
        else
        { //
            size_t offset = (size_t)nodes_.size();

            int axis_top;
            int axis_left;
            int axis_right;

            PCFACE* mid_T;
            PCFACE* mid_L;
            PCFACE* mid_R;

            vector3 cmin, cmax;
            int axis;
            PCFACE* a;
            PCFACE* b;
            PCFACE* c;
            {
                //get_minmax(begin, end, tmin, tmax);
                a = begin;
                b = end;
                c = a + ((b - a) >> 1);

                cmin = min;
                cmax = max;
                axis = get_plane(cmin, cmax);
                find_sort(c, a, b, cmin[axis], cmax[axis], axis);

                axis_top = axis;
                mid_T = c;
            }
            {
                a = begin;
                b = mid_T;
                c = a + ((b - a) >> 1);

                get_minmax(a, b, cmin, cmax);
                axis = get_plane(cmin, cmax);
                find_sort(c, a, b, cmin[axis], cmax[axis], axis);

                axis_left = axis;
                mid_L = c;
            }
            {
                a = mid_T;
                b = end;
                c = a + ((b - a) >> 1);

                get_minmax(a, b, cmin, cmax);
                axis = get_plane(cmin, cmax);
                find_sort(c, a, b, cmin[axis], cmax[axis], axis);

                axis_right = axis;
                mid_R = c;
            }

            PCFACE* ranges[4][2];

            ranges[0][0] = begin;
            ranges[0][1] = mid_L;
            ranges[1][0] = mid_L;
            ranges[1][1] = mid_T;
            ranges[2][0] = mid_T;
            ranges[2][1] = mid_R;
            ranges[3][0] = mid_R;
            ranges[3][1] = end;

            vector3 minmax[4][2];
            for (int i = 0; i < 4; i++)
            {
                get_minmax(ranges[i][0], ranges[i][1], minmax[i][0], minmax[i][1]);
                minmax[i][0] -= vector3(EPSILON, EPSILON, EPSILON);
                minmax[i][1] += vector3(EPSILON, EPSILON, EPSILON);
            }
            //convert & swizzle
            float bboxes[2][3][4];
            //for(int m = 0;m<2;m++){//minmax
            for (int j = 0; j < 3; j++)
            { //xyz
                for (int k = 0; k < 4; k++)
                {                                                           //box
                    bboxes[0][j][k] = safe_convert(minmax[k][0][j] - FEPS); //
                    bboxes[1][j][k] = safe_convert(minmax[k][1][j] + FEPS); //
                }
            }
            //}

            SIMDBVHNode node;
            node.axis_top = axis_top;
            node.axis_left = axis_left;
            node.axis_right = axis_right;
            //for(int i = 0;i<4;i++){
            for (int m = 0; m < 2; m++)
            { //minmax
                for (int j = 0; j < 3; j++)
                { //xyz
                    node.bboxes[m][j] = _mm_setzero_ps();
                    node.bboxes[m][j] = _mm_loadu_ps(bboxes[m][j]);
                }
            }
            //}
            nodes_.push_back(node);

            for (int i = 0; i < 4; i++)
            {
                volatile size_t index = construct(ranges[i][0], ranges[i][1], minmax[i][0], minmax[i][1], level + 1);
                nodes_[offset].children[i] = index;
            }
            return offset;
        }
    }

    simd_qbvh_mesh_accelerator_imp::simd_qbvh_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        min_ = min;
        max_ = max;

        size_t sz = end - begin;
        if (sz == 0) return;

        nodes_.reserve(get_node_size(sz));
        faces_.reserve(std::max<size_t>(MIN_FACE, sz + (int)ceil(double(sz) / MIN_FACE)));
        construct(begin, end, min, max, 0);

        std::vector<PCFACE>(faces_).swap(faces_);

        //------------------
        if (prop.singleside)
        {
            nTypeFace_ = 1;
        }
        else
        {
            nTypeFace_ = 0;
        }
        //------------------
        //print_log("qbvh_mesh_accelerator:memory size:%d bytes\n", memory_size());
    }

    bool simd_qbvh_mesh_accelerator_imp::test_inner(const ray& r, real tmin, real tmax) const
    {
        if (nodes_.empty()) return false;

        __m128 sseOrg[3];
        __m128 sseiDir[3];
        int sign[3];
        __m128 sseTMin;
        __m128 sseTMax;

        sseOrg[0] = _mm_set1_ps(safe_convert(r.origin()[0]));
        sseOrg[1] = _mm_set1_ps(safe_convert(r.origin()[1]));
        sseOrg[2] = _mm_set1_ps(safe_convert(r.origin()[2]));

        sseiDir[0] = _mm_set1_ps(safe_convert(r.inversed_direction()[0]));
        sseiDir[1] = _mm_set1_ps(safe_convert(r.inversed_direction()[1]));
        sseiDir[2] = _mm_set1_ps(safe_convert(r.inversed_direction()[2]));

        int phase = r.phase();
        sign[0] = (phase >> 0) & 1;
        sign[1] = (phase >> 1) & 1;
        sign[2] = (phase >> 2) & 1;

        sseTMin = _mm_set1_ps(safe_convert(tmin));
        sseTMax = _mm_set1_ps(safe_convert(tmax));

        const SIMDBVHNode* nodes = &nodes_[0];

        int todoNode = 0;
        size_t nodeStack[STACK_SIZE];
        nodeStack[0] = 0;

        //bool bRet = false;
        while (todoNode >= 0)
        {
            size_t idx = nodeStack[todoNode];

            if (IsBranch(idx))
            {
                todoNode--; //pop stack
                const SIMDBVHNode& node = nodes[idx];

                int HitMask = test_AABB(node.bboxes, sseOrg, sseiDir, sign, sseTMin, sseTMax);

                if (HitMask)
                {
                    int NodeIdx = (sign[node.axis_top] << 2) | (sign[node.axis_left] << 1) | (sign[node.axis_right]);
                    int Order = OrderTable[HitMask * 8 + NodeIdx];

                    while (!(Order & 0x4))
                    {
                        todoNode++;
                        nodeStack[todoNode] = node.children[Order & 0x3];
                        Order >>= 4;
                    }
                }
            }
            else
            { // if(IsLeaf(idx))
                todoNode--;
                if (IsEmpty(idx)) continue;

                size_t fi = GetFaceFirst(idx); //convert face index.

                if (this->test_faces(fi, r, tmax))
                {
                    return true; //true
                }
            }

            assert(todoNode < STACK_SIZE);
        }

        return false;
    }

    bool simd_qbvh_mesh_accelerator_imp::test_inner(test_info* info, const ray& r, real tmin, real tmax) const
    {
        if (nodes_.empty()) return false;

        __m128 sseOrg[3];
        __m128 sseiDir[3];
        int sign[3];
        __m128 sseTMin;
        __m128 sseTMax;

        sseOrg[0] = _mm_set1_ps(safe_convert(r.origin()[0]));
        sseOrg[1] = _mm_set1_ps(safe_convert(r.origin()[1]));
        sseOrg[2] = _mm_set1_ps(safe_convert(r.origin()[2]));

        sseiDir[0] = _mm_set1_ps(safe_convert(r.inversed_direction()[0]));
        sseiDir[1] = _mm_set1_ps(safe_convert(r.inversed_direction()[1]));
        sseiDir[2] = _mm_set1_ps(safe_convert(r.inversed_direction()[2]));

        int phase = r.phase();
        sign[0] = (phase >> 0) & 1;
        sign[1] = (phase >> 1) & 1;
        sign[2] = (phase >> 2) & 1;

        sseTMin = _mm_set1_ps(safe_convert(tmin));
        sseTMax = _mm_set1_ps(safe_convert(tmax));

        const SIMDBVHNode* nodes = &nodes_[0];

        int todoNode = 0;
        size_t nodeStack[STACK_SIZE];
        nodeStack[0] = 0;

        bool bRet = false;
        while (todoNode >= 0)
        {
            size_t idx = nodeStack[todoNode];

            if (IsBranch(idx))
            {
                todoNode--; //pop stack
                const SIMDBVHNode& node = nodes[idx];

                int HitMask = test_AABB(node.bboxes, sseOrg, sseiDir, sign, sseTMin, sseTMax);

                if (HitMask)
                {
                    int NodeIdx = (sign[node.axis_top] << 2) | (sign[node.axis_left] << 1) | (sign[node.axis_right]);
                    int Order = OrderTable[HitMask * 8 + NodeIdx];

                    while (!(Order & 0x4))
                    {
                        todoNode++;
                        nodeStack[todoNode] = node.children[Order & 0x3];
                        Order >>= 4;
                    }
                }
            }
            else
            { // if(IsLeaf(idx))
                todoNode--;
                if (IsEmpty(idx)) continue;

                size_t fi = GetFaceFirst(idx); //convert face index.

                if (this->test_faces(fi, info, r, tmax))
                {
                    bRet = true;
                    tmax = info->distance;
                    sseTMax = _mm_set1_ps(safe_convert(tmax));
                }
            }

            assert(todoNode < STACK_SIZE);
        }

        return bRet;
    }

    //-------------------------------------------------------------

    class sisd_qbvh_mesh_accelerator_imp : public qbvh_mesh_accelerator_imp
    {
    public:
        sisd_qbvh_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);

        size_t memory_size() const;

    protected:
        bool test_inner(const ray& r, real tmin, real tmax) const;
        bool test_inner(test_info* info, const ray& r, real tmin, real tmax) const;

    protected:
        size_t construct(PCFACE* begin, PCFACE* end, const vector3& min, const vector3& max);

    protected:
        std::vector<CPUBVHNode> nodes_;
    };

    size_t sisd_qbvh_mesh_accelerator_imp::memory_size() const
    {
        return sizeof(sisd_qbvh_mesh_accelerator_imp) + sizeof(CPUBVHNode) * nodes_.capacity() + sizeof(PCFACE) * faces_.capacity();
    }

    size_t sisd_qbvh_mesh_accelerator_imp::construct(PCFACE* begin, PCFACE* end, const vector3& min, const vector3& max)
    {
        static const real EPSILON = (real)std::numeric_limits<float>::epsilon();

        size_t sz = end - begin;
        if (sz == 0) return EMPTY_MASK;
        if (sz <= MIN_FACE && !nodes_.empty())
        { //
            size_t first = faces_.size();
            size_t last = first + sz + 1; //zero terminate
            faces_.resize(last);
            for (size_t i = first; i < last; i++)
            {
                faces_[i] = 0;
            }

            for (size_t i = 0; i < sz; i++)
            {
                faces_[first + i] = *(begin + i);
            }
            size_t nRet = MakeLeafIndex(first);
            assert(IsLeaf(nRet));
            return nRet;
        }
        else
        { //
            size_t offset = (size_t)nodes_.size();

            int axis_top;
            int axis_left;
            int axis_right;

            PCFACE* mid_T;
            PCFACE* mid_L;
            PCFACE* mid_R;

            vector3 cmin, cmax;
            int axis;
            PCFACE* a;
            PCFACE* b;
            PCFACE* c;
            {
                //get_minmax(begin, end, tmin, tmax);
                a = begin;
                b = end;
                c = a + ((b - a) >> 1);

                cmin = min;
                cmax = max;
                axis = get_plane(cmin, cmax);
                find_sort(c, a, b, cmin[axis], cmax[axis], axis);

                axis_top = axis;
                mid_T = c;
            }
            {
                a = begin;
                b = mid_T;
                c = a + ((b - a) >> 1);

                get_minmax(a, b, cmin, cmax);
                axis = get_plane(cmin, cmax);
                find_sort(c, a, b, cmin[axis], cmax[axis], axis);

                axis_left = axis;
                mid_L = c;
            }
            {
                a = mid_T;
                b = end;
                c = a + ((b - a) >> 1);

                get_minmax(a, b, cmin, cmax);
                axis = get_plane(cmin, cmax);
                find_sort(c, a, b, cmin[axis], cmax[axis], axis);

                axis_right = axis;
                mid_R = c;
            }

            PCFACE* ranges[4][2];

            ranges[0][0] = begin;
            ranges[0][1] = mid_L;
            ranges[1][0] = mid_L;
            ranges[1][1] = mid_T;
            ranges[2][0] = mid_T;
            ranges[2][1] = mid_R;
            ranges[3][0] = mid_R;
            ranges[3][1] = end;

            vector3 minmax[4][2];
            for (int i = 0; i < 4; i++)
            {
                get_minmax(ranges[i][0], ranges[i][1], minmax[i][0], minmax[i][1]);
                minmax[i][0] -= vector3(EPSILON, EPSILON, EPSILON);
                minmax[i][1] += vector3(EPSILON, EPSILON, EPSILON);
            }

            nodes_.push_back(CPUBVHNode());

            nodes_[offset].axis_top = axis_top;
            nodes_[offset].axis_left = axis_left;
            nodes_[offset].axis_right = axis_right;

            for (int i = 0; i < 4; i++)
            {
                nodes_[offset].bboxes[i][0] = safe_convert(minmax[i][0] - vector3(FEPS, FEPS, FEPS));
                nodes_[offset].bboxes[i][1] = safe_convert(minmax[i][1] + vector3(FEPS, FEPS, FEPS));
            }

            for (int i = 0; i < 4; i++)
            {
                volatile size_t index = construct(ranges[i][0], ranges[i][1], minmax[i][0], minmax[i][1]);
                nodes_[offset].children[i] = index;
            }
            return offset;
        }
    }

    sisd_qbvh_mesh_accelerator_imp::sisd_qbvh_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        min_ = min;
        max_ = max;

        size_t sz = end - begin;
        if (sz == 0) return;

        nodes_.reserve(get_node_size(sz));
        faces_.reserve(std::max<size_t>(MIN_FACE, sz + (int)ceil(double(sz) / MIN_FACE)));
        construct(begin, end, min, max);

        std::vector<PCFACE>(faces_).swap(faces_);
        std::vector<CPUBVHNode>(nodes_).swap(nodes_);
        //------------------
        if (prop.singleside)
        {
            nTypeFace_ = 1;
        }
        else
        {
            nTypeFace_ = 0;
        }
        //------------------
        //print_log("qbvh_mesh_accelerator:memory size:%d bytes\n", memory_size());
    }

    bool sisd_qbvh_mesh_accelerator_imp::test_inner(const ray& r, real tmin, real tmax) const
    {
        if (nodes_.empty()) return false;

        int sign[3];
        int phase = r.phase();
        sign[0] = (phase >> 0) & 1;
        sign[1] = (phase >> 1) & 1;
        sign[2] = (phase >> 2) & 1;

        const CPUBVHNode* nodes = &nodes_[0];

        int todoNode = 0;
        size_t nodeStack[STACK_SIZE];
        nodeStack[0] = 0;

        //bool bRet = false;
        while (todoNode >= 0)
        {
            size_t idx = nodeStack[todoNode];

            if (IsBranch(idx))
            {
                todoNode--; //pop stack
                const CPUBVHNode& node = nodes[idx];

                int HitMask = test_AABB(node.bboxes, r, tmin, tmax);

                if (HitMask)
                {
                    int NodeIdx = (sign[node.axis_top] << 2) | (sign[node.axis_left] << 1) | (sign[node.axis_right]);
                    int Order = OrderTable[HitMask * 8 + NodeIdx];

                    while (!(Order & 0x4))
                    {
                        todoNode++;
                        nodeStack[todoNode] = node.children[Order & 0x3];
                        Order >>= 4;
                    }
                }
            }
            else
            { // if(IsLeaf(idx))
                todoNode--;
                if (IsEmpty(idx)) continue;

                size_t fi = GetFaceFirst(idx); //convert face index.

                if (this->test_faces(fi, r, tmax))
                {
                    return true; //true
                }
            }

            assert(todoNode < STACK_SIZE);
        }

        return false;
    }

    bool sisd_qbvh_mesh_accelerator_imp::test_inner(test_info* info, const ray& r, real tmin, real tmax) const
    {
        if (nodes_.empty()) return false;

        int sign[3];
        int phase = r.phase();
        sign[0] = (phase >> 0) & 1;
        sign[1] = (phase >> 1) & 1;
        sign[2] = (phase >> 2) & 1;

        const CPUBVHNode* nodes = &nodes_[0];

        int todoNode = 0;
        size_t nodeStack[STACK_SIZE];
        nodeStack[0] = 0;

        bool bRet = false;
        while (todoNode >= 0)
        {
            size_t idx = nodeStack[todoNode];

            if (IsBranch(idx))
            {
                todoNode--; //pop stack
                const CPUBVHNode& node = nodes[idx];

                int HitMask = test_AABB(node.bboxes, r, tmin, tmax);

                if (HitMask)
                {
                    int NodeIdx = (sign[node.axis_top] << 2) | (sign[node.axis_left] << 1) | (sign[node.axis_right]);
                    int Order = OrderTable[HitMask * 8 + NodeIdx];

                    while (!(Order & 0x4))
                    {
                        todoNode++;
                        nodeStack[todoNode] = node.children[Order & 0x3];
                        Order >>= 4;
                    }
                }
            }
            else
            { // if(IsLeaf(idx))
                todoNode--;
                if (IsEmpty(idx)) continue;

                size_t fi = GetFaceFirst(idx); //convert face index.

                if (this->test_faces(fi, info, r, tmax))
                {
                    bRet = true;
                    tmax = info->distance;
                }
            }

            assert(todoNode < STACK_SIZE);
        }

        return bRet;
    }

    qbvh_mesh_accelerator::qbvh_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        if (prop.nosimd)
        {
            imp_ = new sisd_qbvh_mesh_accelerator_imp(min, max, begin, end, prop); //single interaction
        }
        else
        {
            imp_ = new simd_qbvh_mesh_accelerator_imp(min, max, begin, end, prop);
        }
    }

    qbvh_mesh_accelerator::~qbvh_mesh_accelerator()
    {
        delete imp_;
    }

    bool qbvh_mesh_accelerator::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool qbvh_mesh_accelerator::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }

    vector3 qbvh_mesh_accelerator::min() const
    {
        return imp_->min();
    }

    vector3 qbvh_mesh_accelerator::max() const
    {
        return imp_->max();
    }

    size_t qbvh_mesh_accelerator::memory_size() const
    {
        return imp_->memory_size();
    }

    static inline mesh_accelerator_property mod_simd(const mesh_accelerator_property& prop)
    {
        mesh_accelerator_property tmp = prop;
        tmp.nosimd = false;
        return tmp;
    }

    static inline mesh_accelerator_property mod_cpu(const mesh_accelerator_property& prop)
    {
        mesh_accelerator_property tmp = prop;
        tmp.nosimd = true;
        return tmp;
    }

    simd_qbvh_mesh_accelerator::simd_qbvh_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
        : qbvh_mesh_accelerator(min, max, begin, end, mod_simd(prop)) {}

    sisd_qbvh_mesh_accelerator::sisd_qbvh_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
        : qbvh_mesh_accelerator(min, max, begin, end, mod_cpu(prop)) {}
}
