#include "lbvh_utility.h"
#include "logger.h"
#include "timer.h"

#include "values.h"

#include <limits>
#include <vector>
#include <queue>

#include <xmmintrin.h>
#include <emmintrin.h>

#if defined(_OPENMP) && !defined(_WIN32)
#include <omp.h>
#endif

#define DIV_BIT 10
#define DIV_NUM 1024
#define VOX_M 6

#define MIN_FACE 16
#define MAX_FACE 32

namespace kaze
{
    typedef const mesh_face* PCFACE;

    namespace
    {
        static inline vector3 get_center(PCFACE face)
        {
            return (*(face->p_p0) + *(face->p_p1) + *(face->p_p2)) * (real(1) / 3);
        }

        static inline uint32_t partby2(uint32_t n)
        {
            n = (n ^ (n << 16)) & 0xff0000ff;
            n = (n ^ (n << 8)) & 0x0300f00f;
            n = (n ^ (n << 4)) & 0x030c30c3;
            n = (n ^ (n << 2)) & 0x09249249;
            return n;
        }

        static inline __m128i partby2(__m128i n)
        {
            static const __m128i C0 = _mm_set1_epi32(0xff0000ff);
            static const __m128i C1 = _mm_set1_epi32(0x0300f00f);
            static const __m128i C2 = _mm_set1_epi32(0x030c30c3);
            static const __m128i C3 = _mm_set1_epi32(0x09249249);

            n = _mm_and_si128(_mm_xor_si128(n, _mm_slli_epi32(n, 16)), C0);
            n = _mm_and_si128(_mm_xor_si128(n, _mm_slli_epi32(n, 8)), C1);
            n = _mm_and_si128(_mm_xor_si128(n, _mm_slli_epi32(n, 4)), C2);
            n = _mm_and_si128(_mm_xor_si128(n, _mm_slli_epi32(n, 2)), C3);

            return n;
        }

        static inline uint32_t get_morton_code(uint32_t x, uint32_t y, uint32_t z)
        {
            return (partby2(x) << 2) | (partby2(y) << 1) | (partby2(z));
        }

        static inline uint32_t get_morton_code_SIMD(uint32_t x, uint32_t y, uint32_t z)
        {
#ifdef _MSC_VER
            _declspec(align(16)) uint32_t data[4];
#else
            __attribute__((aligned(16))) uint32_t data[4];
#endif
            _mm_store_si128((__m128i*)data, partby2(_mm_set_epi32(0, z, y, x)));

            return (data[0] << 2) | (data[1] << 1) | (data[2]);
        }

        static inline uint32_t get_morton_code_SIMD(__m128i x)
        {
#ifdef _MSC_VER
            _declspec(align(16)) uint32_t data[4];
#else
            __attribute__((aligned(16))) uint32_t data[4];
#endif
            _mm_store_si128((__m128i*)data, partby2(x));
            return (data[0] << 2) | (data[1] << 1) | (data[2]);
        }

        static inline uint32_t get_morton_code_SIMD(__m128 fp, __m128 fmin, __m128 frev)
        {
            //__m128i nRet = _mm_cvtps_epi32(_mm_mul_ps(_mm_set1_ps((float)DIV_NUM),_mm_div_ps(_mm_sub_ps(fp,fmin),_mm_sub_ps(fmax,fmin))));
            __m128i nRet = _mm_cvtps_epi32(_mm_mul_ps(_mm_set1_ps((float)DIV_NUM), _mm_mul_ps(_mm_sub_ps(fp, fmin), frev)));
            return get_morton_code_SIMD(nRet);
        }

        static inline uint32_t get_morton_code(const float p[3], const float min[3], const float max[3])
        {
            uint32_t ix = (uint32_t)(DIV_NUM * ((p[0] - min[0]) / (max[0] - min[0])));
            uint32_t iy = (uint32_t)(DIV_NUM * ((p[1] - min[1]) / (max[1] - min[1])));
            uint32_t iz = (uint32_t)(DIV_NUM * ((p[2] - min[2]) / (max[2] - min[2])));
            return get_morton_code(ix, iy, iz);
        }

        inline void create_morton_codes_(unsigned int* codes, const float* coords, size_t fsz, const float min[3], const float max[3])
        {
            __m128 fmin = _mm_setr_ps(min[0], min[1], min[2], 0);
            __m128 fmax = _mm_setr_ps(max[0], max[1], max[2], 0);
            __m128 frev = _mm_rcp_ps(_mm_sub_ps(fmax, fmin));

#if defined(_OPENMP)
            static const int DIV = 65536;

            size_t sz = fsz;
            std::vector<int> n;
            if (sz <= DIV)
            {
                n.push_back((int)sz);
            }
            else
            {
                int ksz = sz / DIV;
                for (int k = 0; k < ksz; k++)
                {
                    n.push_back(DIV);
                }
                int rem = sz & (DIV - 1);
                if (rem) n.push_back(rem);
            }

            int ksz = (int)n.size();
#pragma omp parallel
            for (int k = 0; k < ksz; k++)
            {
                int nsz = (int)n[k];
#pragma omp for
                for (int j = 0; j < nsz; j++)
                {
                    size_t i = k * DIV + j;
                    __m128 fp = _mm_setr_ps(coords[3 * i + 0], coords[3 * i + 1], coords[3 * i + 2], 0);
                    codes[i] = get_morton_code_SIMD(fp, fmin, frev);
                }
            }

#else
            for (size_t i = 0; i < fsz; i++)
            {
                __m128 fp = _mm_setr_ps(coords[3 * i + 0], coords[3 * i + 1], coords[3 * i + 2], 0);
                codes[i] = get_morton_code_SIMD(fp, fmin, frev);
            }
#endif
        }

        static inline void create_morton_code_(unsigned int* codes, const PCFACE* faces, size_t fsz, const float min[3], const float max[3])
        {
            __m128 fmin = _mm_setr_ps(min[0], min[1], min[2], 0);
            __m128 fmax = _mm_setr_ps(max[0], max[1], max[2], 0);
            __m128 frev = _mm_rcp_ps(_mm_sub_ps(fmax, fmin));

#if defined(_OPENMP)
            static const int DIV = 65536;

            size_t sz = fsz;
            std::vector<int> n;
            if (sz <= DIV)
            {
                n.push_back((int)sz);
            }
            else
            {
                int ksz = (int)(sz / DIV);
                for (int k = 0; k < ksz; k++)
                {
                    n.push_back(DIV);
                }
                int rem = sz & (DIV - 1);
                if (rem) n.push_back(rem);
            }

            int ksz = (int)n.size();
#pragma omp parallel
            for (int k = 0; k < ksz; k++)
            {
                int nsz = (int)n[k];
#pragma omp for
                for (int j = 0; j < nsz; j++)
                {
                    size_t i = k * DIV + j;
                    vector3 c = get_center(faces[i]);
                    __m128 fp = _mm_setr_ps((float)c[0], (float)c[1], (float)c[2], 0);
                    codes[i] = get_morton_code_SIMD(fp, fmin, frev);
                }
            }

#else
            for (size_t i = 0; i < fsz; i++)
            {
                vector3 c = get_center(faces[i]);
                __m128 fp = _mm_setr_ps((float)c[0], (float)c[1], (float)c[2], 0);
                codes[i] = get_morton_code_SIMD(fp, fmin, frev);
            }
#endif
        }
    }

    void create_morton_code(std::vector<unsigned int>& codes, const std::vector<vector3f>& points, const vector3f& min, const vector3f& max)
    {
        create_morton_code(&codes[0], (const float*)(&(points[0])), points.size(), (const float*)(&min), (const float*)(&max));
    }
    void create_morton_code(std::vector<unsigned int>& codes, const std::vector<vector3d>& points, const vector3d& min, const vector3d& max)
    {
        create_morton_code(&codes[0], (const double*)(&(points[0])), points.size(), (const double*)(&min), (const double*)(&max));
    }
    void create_morton_code(unsigned int* codes, const float* coords, size_t fsz, const float min[3], const float max[3])
    {
        create_morton_codes_(codes, coords, fsz, min, max);
    }
    void create_morton_code(unsigned int* codes, const double* coords, size_t fsz, const double min[3], const double max[3])
    {
        std::vector<float> fp(3 * fsz);
        for (size_t i = 0; i < 3 * fsz; i++)
        {
            fp[i] = (float)coords[i];
        }
        float fmin[3];
        float fmax[3];
        for (int i = 0; i < 3; i++)
        {
            fmin[i] = (float)min[i];
            fmax[i] = (float)max[i];
        }
        create_morton_codes_(codes, &fp[0], fsz, fmin, fmax);
    }

    void create_morton_code(std::vector<uint32_t>& codes, const std::vector<const mesh_face*>& faces, const vector3& min, const vector3& max)
    {
        float tmin[] = {(float)min[0], (float)min[1], (float)min[2]};
        float tmax[] = {(float)max[0], (float)max[1], (float)max[2]};
        create_morton_code_(&codes[0], &faces[0], faces.size(), tmin, tmax);
    }

    void create_morton_code(std::vector<uint32_t>& codes, const mesh_face** const faces, size_t sz, const vector3& min, const vector3& max)
    {
        float tmin[] = {(float)min[0], (float)min[1], (float)min[2]};
        float tmax[] = {(float)max[0], (float)max[1], (float)max[2]};
        create_morton_code_(&codes[0], faces, sz, tmin, tmax);
    }

    //---------------------------------------------------------------------------------------------------------------------------
    namespace
    {

        typedef std::vector<size_t>::iterator indices_iter;
        typedef const uint32_t* code_iter;

        static void sort_index_with_code(indices_iter fa, indices_iter fb, const std::vector<uint32_t>& codes, int bits)
        {
#if 0
		std::sort(fa, fb, code_sorter(codes));
#else
            //RADIX SORT
            std::vector<size_t> buckets[256]; //1 2 4 8 16

            std::vector<size_t> tmp(fa, fb);
            size_t sz = tmp.size();

            size_t rsv = std::max<size_t>(8, sz / 256);

#pragma omp parallel for
            for (int i = 0; i < 256; i++)
            {
                buckets[i].reserve(rsv);
            }
            for (int b = 0; b < bits; b += 8)
            {
                for (size_t i = 0; i < sz; i++)
                {
                    int n = (codes[tmp[i]] >> b) & 0xFF;
                    buckets[n].push_back(tmp[i]);
                }
                size_t t = 0;
                for (int i = 0; i < 256; i++)
                {
                    size_t bksz = buckets[i].size();
                    if (bksz)
                    {
                        memcpy(&tmp[t], &(buckets[i][0]), sizeof(size_t) * bksz);
                        t += bksz;
                        buckets[i].clear();
                    }
                }
            }
            std::copy(tmp.begin(), tmp.end(), fa);
#endif
        }

        struct code_sorter
        {
            code_sorter(const std::vector<uint32_t>& codes) : codes_(codes) {}
            bool operator()(size_t a, size_t b) const
            {
                return codes_[a] < codes_[b];
            }
            const std::vector<uint32_t>& codes_;
        };

        static void sort_index_with_code2(indices_iter fa, indices_iter fb, const std::vector<uint32_t>& codes, int bits)
        {
            std::sort(fa, fb, code_sorter(codes));
        }

        static void get_run_heads(std::vector<size_t>& run_heads, const std::vector<uint32_t>& codes, int d)
        {
            size_t sz = codes.size();
            uint32_t cr = codes[0] >> d;

            run_heads.push_back(0);
            for (size_t i = 1; i < sz; i++)
            {
                uint32_t cl = codes[i] >> d;
                if (cr != cl)
                {
                    run_heads.push_back(i);
                    cr = cl;
                }
            }
        }
    }
    void get_morton_bound(vector3& tmin, vector3& tmax, const vector3& min, const vector3& max)
    {
        vector3 center = (min + max) * 0.5;
        vector3 extend = (max - min) * 0.5;

        real wid = extend[0];
        if (wid < extend[1]) wid = extend[1];
        if (wid < extend[2]) wid = extend[2];
        wid += +values::epsilon() * 1024;
        tmin = center - vector3(wid, wid, wid);
        tmax = center + vector3(wid, wid, wid);
    }

    void sort_morton_code(std::vector<size_t>& indices, std::vector<uint32_t>& codes)
    {
        int n = DIV_BIT; //10
        int m = VOX_M;   //6

        timer t;

        size_t sz = codes.size();

        int d = 3 * (n - m);
        std::vector<size_t> run_heads;

        t.start();
        get_run_heads(run_heads, codes, d);
        t.end();

        //print_log("get_run_heads:%d ms\n",t.msec());
        //--------------------------------------------------------------
        t.start();
        size_t M = run_heads.size();
        std::vector<uint32_t> run_codes(M);
        std::vector<size_t> run_indices(M);
        for (size_t i = 0; i < M; i++)
        {
            run_codes[i] = codes[run_heads[i]] >> d;
            run_indices[i] = i;
        }
        t.end();
        //print_log("get_run_indices:%d ms\n",t.msec());
        //--------------------------------------------------------------
        t.start();
        sort_index_with_code(run_indices.begin(), run_indices.end(), run_codes, 3 * m);
        t.end();
        //print_log("top level sort:%d ms\n",t.msec());
        //--------------------------------------------------------------
        t.start();
        std::vector<size_t> run_lengthes(M);
        for (size_t i = 0; i < M; i++)
        {
            if (run_indices[i] == M - 1)
            {
                run_lengthes[i] = sz - run_heads[run_indices[i]];
            }
            else
            {
                run_lengthes[i] = run_heads[run_indices[i] + 1] - run_heads[run_indices[i]];
            }
        }
        //--------------------------------------------------------------
        std::vector<size_t> out_indices;
        out_indices.reserve(sz);
        std::vector<size_t> tmp_indices;
        //#pragma omp parallel for
        for (size_t i = 0; i < M; i++)
        {
            if (run_lengthes[i] >= 2)
            {
                tmp_indices.clear();
                tmp_indices.reserve(run_lengthes[i]);
                size_t index = run_indices[i];
                size_t offset = run_heads[index];
                size_t length = run_lengthes[i];
                for (size_t j = 0; j < length; j++)
                {
                    tmp_indices.push_back(offset + j);
                }

                sort_index_with_code2(tmp_indices.begin(), tmp_indices.end(), codes, d);

                for (size_t j = 0; j < length; j++)
                {
                    out_indices.push_back(tmp_indices[j]);
                }
            }
            else
            {
                out_indices.push_back(run_heads[run_indices[i]]);
            }
        }
        t.end();
        //print_log("voxel level sort:%d ms\n",t.msec());
        //--------------------------------------------------------------

        t.start();
        std::vector<uint32_t> out_codes(sz);
        for (size_t i = 0; i < sz; i++)
        {
            out_codes[i] = codes[out_indices[i]];
        }

        indices.swap(out_indices);
        codes.swap(out_codes);
        t.end();
        //print_log("copy faces & codes:%d ms\n",t.msec());
        //--------------------------------------------------------------
    }
}
