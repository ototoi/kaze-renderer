#ifndef KAZE_TRQBVH_MESH_ACCELERATOR_H
#define KAZE_TRQBVH_MESH_ACCELERATOR_H

#include "mesh_accelerator.h"

namespace kaze
{

    class trqbvh_mesh_accelerator_imp;
    class trqbvh_mesh_accelerator : public mesh_accelerator
    {
    public:
        typedef const mesh_face* PCFACE;

    public:
        trqbvh_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
        ~trqbvh_mesh_accelerator();
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    public:
        size_t memory_size() const;

    public:
        static float set_Ci(float v);
        static float set_Ct(float v);
        static int set_Iteration(int i);

    private:
        trqbvh_mesh_accelerator_imp* imp_;
    };
}

#endif
