/*
 * Partitioned Linear Bounding Volume Hierarchy construction
 */

#include "plqbvh_mesh_accelerator.h"

#include "intersection_ex.h"
#include "test_info.h"
#include "logger.h"
#include "timer.h"

#include "mesh_face.h"

#include "values.h"

#include "aligned_vector.hpp"

#include "lbvh_utility.h"

#include <memory>
#include <cstring>
#include <cmath>
#include <functional>
#include <algorithm>
#include <vector>
#include <utility>

#include <xmmintrin.h>
#include <emmintrin.h>

#include <stdlib.h>

#if 0
#define PRINTLOG print_log
#else
static void print_dummy(const char* str, ...) {}
#define PRINTLOG print_dummy
#endif

#if 0
#include <omp.h>
#endif
#define DIV_BIT 10
#define DIV_NUM 1024
#define VOX_M 6

#define MIN_FACE 4

static const int STACK_SIZE = ((int)(sizeof(size_t) * 16));

namespace kaze
{
    namespace
    {
        typedef plqbvh_mesh_accelerator::PCFACE PCFACE;
        typedef const PCFACE* face_iter;
        typedef const uint32_t* code_iter;

        //For float
        static inline int test_AABB(
            const __m128 bboxes[2][3], //4boxes : min-max[2] of xyz[3] of boxes[4]
            const __m128 org[3],       //ray origin
            const __m128 idir[3],      //ray inveresed direction
            const int sign[3],         //ray xyz direction -> +:0,-:1
            __m128 tmin, __m128 tmax   //ray range tmin-tmax
            )
        {
            // x coordinate
            tmin = _mm_max_ps(
                tmin,
                _mm_mul_ps(_mm_sub_ps(bboxes[sign[0]][0], org[0]), idir[0]));
            tmax = _mm_min_ps(
                tmax,
                _mm_mul_ps(_mm_sub_ps(bboxes[1 - sign[0]][0], org[0]), idir[0]));

            // y coordinate
            tmin = _mm_max_ps(
                tmin,
                _mm_mul_ps(_mm_sub_ps(bboxes[sign[1]][1], org[1]), idir[1]));
            tmax = _mm_min_ps(
                tmax,
                _mm_mul_ps(_mm_sub_ps(bboxes[1 - sign[1]][1], org[1]), idir[1]));

            // z coordinate
            tmin = _mm_max_ps(
                tmin,
                _mm_mul_ps(_mm_sub_ps(bboxes[sign[2]][2], org[2]), idir[2]));
            tmax = _mm_min_ps(
                tmax,
                _mm_mul_ps(_mm_sub_ps(bboxes[1 - sign[2]][2], org[2]), idir[2]));

            return _mm_movemask_ps(_mm_cmpge_ps(tmax, tmin));
        }

        static const size_t EMPTY_MASK = size_t(-1);        //0xFFFF...
        static const size_t SIGN_MASK = ~(EMPTY_MASK >> 1); //0x8000...

        inline bool IsLeaf(size_t i)
        {
            return (SIGN_MASK & i) ? true : false;
        }
        inline bool IsBranch(size_t i)
        {
            return !IsLeaf(i);
        }
        inline bool IsEmpty(size_t i)
        {
            return i == EMPTY_MASK;
        }

        inline size_t MakeLeafIndex(size_t i)
        {
            return SIGN_MASK | i;
        }

        inline size_t GetFaceFirst(size_t i)
        {
            return (~SIGN_MASK) & i;
        }

#pragma pack(push, 4)
        struct SIMDBVHNode
        {
            __m128 bboxes[2][3]; //768
            size_t children[4];  //128
            int axis_top;        //128
            int axis_right;
            int axis_left;
            int reserved;
        };
#pragma pack(pop)

        void get_minmax(vector3& pmin, vector3& pmax, PCFACE face)
        {
            static const real FAR = values::far();

            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);

            vector3 points[3];
            points[0] = *(face->p_p0);
            points[1] = *(face->p_p1);
            points[2] = *(face->p_p2);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    min[j] = std::min(min[j], points[i][j]);
                    max[j] = std::max(max[j], points[i][j]);
                }
            }

            pmin = min;
            pmax = max;
        }

        void get_minmax(vector3f& pmin, vector3f& pmax, PCFACE face)
        {
            const float FAR = std::numeric_limits<float>::max();
            vector3f min = vector3f(+FAR, +FAR, +FAR);
            vector3f max = vector3f(-FAR, -FAR, -FAR);

            vector3f points[3];
            points[0] = vector3f(*(face->p_p0));
            points[1] = vector3f(*(face->p_p1));
            points[2] = vector3f(*(face->p_p2));

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    min[j] = std::min(min[j], points[i][j]);
                    max[j] = std::max(max[j], points[i][j]);
                }
            }

            pmin = min;
            pmax = max;
        }

        template <class ITER>
        void get_minmax(vector3& pmin, vector3& pmax, ITER a, ITER b)
        {
            static const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            for (ITER i = a; i != b; i++)
            {
                vector3 cmin;
                vector3 cmax;
                get_minmax(cmin, cmax, *i);
                for (int j = 0; j < 3; j++)
                {
                    min[j] = std::min(min[j], cmin[j]);
                    max[j] = std::max(max[j], cmax[j]);
                }
            }
            pmin = min;
            pmax = max;
        }

        static void get_minmax(vector3f& pmin, vector3f& pmax, const PCFACE* faces, const size_t* indices, size_t a, size_t b)
        {
            static const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            for (size_t i = a; i != b; i++)
            {
                size_t idx = indices[i];
                PCFACE face = faces[idx];
                vector3 cmin;
                vector3 cmax;
                get_minmax(cmin, cmax, face);
                for (int j = 0; j < 3; j++)
                {
                    min[j] = std::min(min[j], cmin[j]);
                    max[j] = std::max(max[j], cmax[j]);
                }
            }
            pmin = min;
            pmax = max;
        }

        static inline uint32_t partby2(uint32_t n)
        {
            n = (n ^ (n << 16)) & 0xff0000ff;
            n = (n ^ (n << 8)) & 0x0300f00f;
            n = (n ^ (n << 4)) & 0x030c30c3;
            n = (n ^ (n << 2)) & 0x09249249;
            return n;
        }

        static inline uint32_t get_morton_code(uint32_t x, uint32_t y, uint32_t z)
        {
            return (partby2(x) << 2) | (partby2(y) << 1) | (partby2(z));
        }

        static inline uint32_t get_morton_code(const vector3& p, const vector3& min, const vector3& max)
        {
            uint32_t ix = (uint32_t)(DIV_NUM * ((p[0] - min[0]) / (max[0] - min[0])));
            uint32_t iy = (uint32_t)(DIV_NUM * ((p[1] - min[1]) / (max[1] - min[1])));
            uint32_t iz = (uint32_t)(DIV_NUM * ((p[2] - min[2]) / (max[2] - min[2])));
            return get_morton_code(ix, iy, iz);
        }

        static inline vector3 get_center(PCFACE face)
        {
            return (*(face->p_p0) + *(face->p_p1) + *(face->p_p2)) * (real(1) / 3);
        }

        struct separator
        {
            separator(int level, const uint32_t* codes) : codes_(codes)
            {
                int p = 3 * DIV_BIT - 1 - level;
                nMask_ = 1 << p;
            }
            inline bool operator()(size_t i) const
            {
                return (nMask_ & codes_[i]) == 0;
            }

            uint32_t nMask_;
            const uint32_t* codes_;
        };
    }

    class plqbvh_mesh_accelerator_imp
    {
    public:
        plqbvh_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
        ~plqbvh_mesh_accelerator_imp();
        bool test(const ray& r, real tmin, real tmax) const;
        bool test(test_info* info, const ray& r, real tmin, real tmax) const;
        vector3 min() const;
        vector3 max() const;
        size_t memory_size() const;

        void add(PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);

    protected:
        bool test_faces(size_t nFirst, const ray& r, real dist) const;
        bool test_faces(size_t nFirst, test_info* info, const ray& r, real dist) const;
        bool test_inner(const ray& r, real tmin, real tmax) const;
        bool test_inner(test_info* info, const ray& r, real tmin, real tmax) const;
        void construct(
            const PCFACE* faces,
            const uint32_t* codes,
            size_t* indices,
            size_t a,
            size_t b);

    private:
        vector3 min_;
        vector3 max_;

        int nTypeFace_;
        std::vector<PCFACE> faces_;
        aligned_vector<SIMDBVHNode> nodes_;
    };

    plqbvh_mesh_accelerator_imp::plqbvh_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
        : min_(min), max_(max)
    {
        this->add(begin, end, prop);
    }
    plqbvh_mesh_accelerator_imp::~plqbvh_mesh_accelerator_imp()
    {
        ; //
    }

    bool plqbvh_mesh_accelerator_imp::test(const ray& r, real tmin, real tmax) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            return test_inner(r, tmin, tmax);
        }
        else
        {
            return false;
        }
    }

    bool plqbvh_mesh_accelerator_imp::test(test_info* info, const ray& r, real tmin, real tmax) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            return test_inner(info, r, tmin, tmax);
        }
        else
        {
            return false;
        }
    }

    vector3 plqbvh_mesh_accelerator_imp::min() const
    {
        return min_;
    }

    vector3 plqbvh_mesh_accelerator_imp::max() const
    {
        return max_;
    }

    size_t plqbvh_mesh_accelerator_imp::memory_size() const
    {
        return sizeof(plqbvh_mesh_accelerator_imp) + sizeof(SIMDBVHNode) * nodes_.capacity() + sizeof(PCFACE) * faces_.capacity();
    }

    bool plqbvh_mesh_accelerator_imp::test_faces(size_t nFirst, const ray& r, real dist) const
    {
        const PCFACE* faces = &faces_[0];
        if (nTypeFace_ == 0)
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (faces[i]->test_DS(r, dist)) return true;
                i++;
            }
        }
        else
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (faces[i]->test_SS(r, dist)) return true;
                i++;
            }
        }
        return false;
    }

    bool plqbvh_mesh_accelerator_imp::test_faces(size_t nFirst, test_info* info, const ray& r, real dist) const
    {
        const PCFACE* faces = &faces_[0];
        bool bRet = false;
        if (nTypeFace_ == 0)
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (faces[i]->test_DS(info, r, dist))
                {
                    dist = info->distance;
                    bRet = true;
                }
                i++;
            }
        }
        else
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (faces[i]->test_SS(info, r, dist))
                {
                    dist = info->distance;
                    bRet = true;
                }
                i++;
            }
        }
        return bRet;
    }

    //visit order table
    //8*16 = 128
    static const int OrderTable[] = {
        0x44444, 0x44444, 0x44444, 0x44444, 0x44444, 0x44444, 0x44444, 0x44444,
        0x44440, 0x44440, 0x44440, 0x44440, 0x44440, 0x44440, 0x44440, 0x44440,
        0x44441, 0x44441, 0x44441, 0x44441, 0x44441, 0x44441, 0x44441, 0x44441,
        0x44401, 0x44401, 0x44410, 0x44410, 0x44401, 0x44401, 0x44410, 0x44410,
        0x44442, 0x44442, 0x44442, 0x44442, 0x44442, 0x44442, 0x44442, 0x44442,
        0x44402, 0x44402, 0x44402, 0x44402, 0x44420, 0x44420, 0x44420, 0x44420,
        0x44412, 0x44412, 0x44412, 0x44412, 0x44421, 0x44421, 0x44421, 0x44421,
        0x44012, 0x44012, 0x44102, 0x44102, 0x44201, 0x44201, 0x44210, 0x44210,
        0x44443, 0x44443, 0x44443, 0x44443, 0x44443, 0x44443, 0x44443, 0x44443,
        0x44403, 0x44403, 0x44403, 0x44403, 0x44430, 0x44430, 0x44430, 0x44430,
        0x44413, 0x44413, 0x44413, 0x44413, 0x44431, 0x44431, 0x44431, 0x44431,
        0x44013, 0x44013, 0x44103, 0x44103, 0x44301, 0x44301, 0x44310, 0x44310,
        0x44423, 0x44432, 0x44423, 0x44432, 0x44423, 0x44432, 0x44423, 0x44432,
        0x44023, 0x44032, 0x44023, 0x44032, 0x44230, 0x44320, 0x44230, 0x44320,
        0x44123, 0x44132, 0x44123, 0x44132, 0x44231, 0x44321, 0x44231, 0x44321,
        0x40123, 0x40132, 0x41023, 0x41032, 0x42301, 0x43201, 0x42310, 0x43210,
    };

    static float safe_convert(double x)
    {
        static const double FMAX = +(double)std::numeric_limits<float>::max();
        static const double FMIN = -(double)std::numeric_limits<float>::max();
        if (FMAX < x) return (float)FMAX;
        if (x < FMIN) return (float)FMIN;
        return (float)x;
    }

    static inline float safe_convert(float x)
    {
        return x;
    }

    static inline vector3f safe_convert(const vector3& v)
    {
        return vector3f(safe_convert(v[0]), safe_convert(v[1]), safe_convert(v[2]));
    }

    bool plqbvh_mesh_accelerator_imp::test_inner(const ray& r, real tmin, real tmax) const
    {
        if (nodes_.empty()) return false;

        __m128 sseOrg[3];
        __m128 sseiDir[3];
        int sign[3];
        __m128 sseTMin;
        __m128 sseTMax;

        sseOrg[0] = _mm_set1_ps(safe_convert(r.origin()[0]));
        sseOrg[1] = _mm_set1_ps(safe_convert(r.origin()[1]));
        sseOrg[2] = _mm_set1_ps(safe_convert(r.origin()[2]));

        sseiDir[0] = _mm_set1_ps(safe_convert(r.inversed_direction()[0]));
        sseiDir[1] = _mm_set1_ps(safe_convert(r.inversed_direction()[1]));
        sseiDir[2] = _mm_set1_ps(safe_convert(r.inversed_direction()[2]));

        int phase = r.phase();
        sign[0] = (phase >> 0) & 1;
        sign[1] = (phase >> 1) & 1;
        sign[2] = (phase >> 2) & 1;

        sseTMin = _mm_set1_ps(safe_convert(tmin));
        sseTMax = _mm_set1_ps(safe_convert(tmax));

        const SIMDBVHNode* nodes = &nodes_[0];

        int todoNode = 0;
        size_t nodeStack[STACK_SIZE];
        nodeStack[0] = 0;

        //bool bRet = false;
        while (todoNode >= 0)
        {
            size_t idx = nodeStack[todoNode];

            if (IsBranch(idx))
            {
                todoNode--; //pop stack
                const SIMDBVHNode& node = nodes[idx];

                int HitMask = test_AABB(node.bboxes, sseOrg, sseiDir, sign, sseTMin, sseTMax);

                if (HitMask)
                {
                    int NodeIdx = (sign[node.axis_top] << 2) | (sign[node.axis_left] << 1) | (sign[node.axis_right]);
                    int Order = OrderTable[HitMask * 8 + NodeIdx];

                    while (!(Order & 0x4))
                    {
                        todoNode++;
                        nodeStack[todoNode] = node.children[Order & 0x3];
                        Order >>= 4;
                    }
                }
            }
            else
            { // if(IsLeaf(idx))
                todoNode--;
                if (IsEmpty(idx)) continue;

                size_t fi = GetFaceFirst(idx); //convert face index.

                if (this->test_faces(fi, r, tmax))
                {
                    return true; //true
                }
            }

            assert(todoNode < STACK_SIZE);
        }

        return false;
    }

    bool plqbvh_mesh_accelerator_imp::test_inner(test_info* info, const ray& r, real tmin, real tmax) const
    {
        if (nodes_.empty()) return false;

        __m128 sseOrg[3];
        __m128 sseiDir[3];
        int sign[3];
        __m128 sseTMin;
        __m128 sseTMax;

        sseOrg[0] = _mm_set1_ps(safe_convert(r.origin()[0]));
        sseOrg[1] = _mm_set1_ps(safe_convert(r.origin()[1]));
        sseOrg[2] = _mm_set1_ps(safe_convert(r.origin()[2]));

        sseiDir[0] = _mm_set1_ps(safe_convert(r.inversed_direction()[0]));
        sseiDir[1] = _mm_set1_ps(safe_convert(r.inversed_direction()[1]));
        sseiDir[2] = _mm_set1_ps(safe_convert(r.inversed_direction()[2]));

        int phase = r.phase();
        sign[0] = (phase >> 0) & 1;
        sign[1] = (phase >> 1) & 1;
        sign[2] = (phase >> 2) & 1;

        sseTMin = _mm_set1_ps(safe_convert(tmin));
        sseTMax = _mm_set1_ps(safe_convert(tmax));

        const SIMDBVHNode* nodes = &nodes_[0];

        int todoNode = 0;
        size_t nodeStack[STACK_SIZE];
        nodeStack[0] = 0;

        bool bRet = false;
        while (todoNode >= 0)
        {
            size_t idx = nodeStack[todoNode];

            if (IsBranch(idx))
            {
                todoNode--; //pop stack
                const SIMDBVHNode& node = nodes[idx];

                int HitMask = test_AABB(node.bboxes, sseOrg, sseiDir, sign, sseTMin, sseTMax);

                if (HitMask)
                {
                    int NodeIdx = (sign[node.axis_top] << 2) | (sign[node.axis_left] << 1) | (sign[node.axis_right]);
                    int Order = OrderTable[HitMask * 8 + NodeIdx];

                    while (!(Order & 0x4))
                    {
                        todoNode++;
                        nodeStack[todoNode] = node.children[Order & 0x3];
                        Order >>= 4;
                    }
                }
            }
            else
            { // if(IsLeaf(idx))
                todoNode--;
                if (IsEmpty(idx)) continue;

                size_t fi = GetFaceFirst(idx); //convert face index.

                if (this->test_faces(fi, info, r, tmax))
                {
                    bRet = true;
                    tmax = info->distance;
                    sseTMax = _mm_set1_ps(safe_convert(tmax));
                }
            }

            assert(todoNode < STACK_SIZE);
        }

        return bRet;
    }

    static inline size_t get_branch_node_size(size_t face_num)
    {
        if (face_num <= MIN_FACE) return 1;
        size_t p = face_num / 4;
        if (face_num & 3) p++;
        return std::max<size_t>(1, 1 + 4 * get_branch_node_size(p));
    }
    static inline size_t get_leaf_node_size(size_t face_num)
    {
        return std::max<size_t>(MIN_FACE, face_num + (int)ceil(double(face_num) / (MIN_FACE)));
    }

    static inline void checkbound(vector3f& min, vector3f& max)
    {
        static const float EPS = std::numeric_limits<float>::epsilon() * 256;

        vector3f wid = (max - min) * 0.5f;
        vector3f cnt = (max + min) * 0.5f;
        for (int i = 0; i < 3; i++)
        {
            wid[i] = std::max<float>(wid[i], EPS);
        }

        min = cnt - wid;
        max = cnt + wid;
    }

    static size_t construct_deep(
        std::vector<PCFACE>& out_faces,
        aligned_vector<SIMDBVHNode>& out_nodes,
        vector3f& min, vector3f& max,
        const PCFACE* faces,
        const uint32_t* codes,
        size_t* indices,
        size_t a,
        size_t b,
        int level)
    {
        static const float EPS = std::numeric_limits<float>::epsilon();
        static const float FAR = std::numeric_limits<float>::max();

        size_t sz = b - a;
        if (sz == 0) return EMPTY_MASK;
        if (sz <= MIN_FACE || level >= DIV_BIT * 3)
        {
            size_t first = out_faces.size();
            size_t last = first + sz + 1; //zero terminate
            out_faces.resize(last);
            for (size_t i = 0; i < sz; i++)
            {
                out_faces[first + i] = faces[indices[a + i]];
            }
            out_faces[last - 1] = 0;
            get_minmax(min, max, faces, indices, a, b);

            min -= vector3f(EPS, EPS, EPS);
            max += vector3f(EPS, EPS, EPS);

            checkbound(min, max);

            size_t nRet = MakeLeafIndex(first);
            assert(IsLeaf(nRet));
            return nRet;
        }
        else
        {
            size_t* ia = indices + a;
            size_t* ib = indices + b;
            size_t* ic;
            size_t* il;
            size_t* ir;
            {
                ic = std::partition(ia, ib, separator(level, codes));
                size_t* iters[2];
                size_t* idx[2][2] = {{ia, ic}, {ic, ib}};
                for (int i = 0; i < 2; i++)
                {
                    iters[i] = std::partition(idx[i][0], idx[i][1], separator(level + 1, codes));
                }
                il = iters[0];
                ir = iters[1];
            }

            size_t csz = ic - ia;
            size_t lsz = il - ia;
            size_t rsz = ir - ic;

            size_t offset = out_nodes.size();

            size_t c = a + csz;
            size_t l = a + lsz;
            size_t r = c + rsz;

            SIMDBVHNode node;
            node.axis_top = level % 3;
            node.axis_left = node.axis_right = (level + 1) % 3;
            out_nodes.push_back(node);

            vector3f minmax[4][2];
            for (int i = 0; i < 4; i++)
            {
                minmax[i][0] = vector3f(+FAR, +FAR, +FAR);
                minmax[i][1] = vector3f(-FAR, -FAR, -FAR);
            }

            size_t ranges[4][2];
            ranges[0][0] = a;
            ranges[0][1] = l;
            ranges[1][0] = l;
            ranges[1][1] = c;
            ranges[2][0] = c;
            ranges[2][1] = r;
            ranges[3][0] = r;
            ranges[3][1] = b;

            for (int i = 0; i < 4; i++)
            {
                volatile size_t index = construct_deep(out_faces, out_nodes, minmax[i][0], minmax[i][1], faces, codes, indices, ranges[i][0], ranges[i][1], level + 2);
                out_nodes[offset].children[i] = index;
            }

            //convert & swizzle
            float bboxes[2][3][4];
            //for(int m = 0;m<2;m++){//minmax
            for (int j = 0; j < 3; j++)
            { //xyz
                for (int k = 0; k < 4; k++)
                {                                      //box
                    bboxes[0][j][k] = minmax[k][0][j]; //
                    bboxes[1][j][k] = minmax[k][1][j]; //
                }
            }
            //}

            //for(int i = 0;i<4;i++){
            for (int m = 0; m < 2; m++)
            { //minmax
                for (int j = 0; j < 3; j++)
                { //xyz
                    out_nodes[offset].bboxes[m][j] = _mm_setzero_ps();
                    out_nodes[offset].bboxes[m][j] = _mm_loadu_ps(bboxes[m][j]);
                }
            }
            //}

            min = minmax[0][0];
            max = minmax[0][1];
            for (int i = 1; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                { //xyz
                    min[j] = std::min(min[j], minmax[i][0][j]);
                    max[j] = std::max(max[j], minmax[i][1][j]);
                }
            }

            min -= vector3f(EPS, EPS, EPS);
            max += vector3f(EPS, EPS, EPS);

            return offset;
        }
    }

    void plqbvh_mesh_accelerator_imp::construct(
        const PCFACE* faces,
        const uint32_t* codes,
        size_t* indices,
        size_t a,
        size_t b)
    {
        static const float FAR = std::numeric_limits<float>::max();
        static const int level = 0;

        size_t sz = b - a;
        if (sz == 0) return;
        size_t* ia = indices + a;
        size_t* ib = indices + b;
        size_t* ic = NULL;
        ;
        size_t* il = NULL;
        size_t* ir = NULL;
        {
            ic = std::partition(ia, ib, separator(level, codes));
            size_t* iters[2] = {0, 0};
            size_t* idx[2][2] = {};
            idx[0][0] = ia;
            idx[0][1] = ic;
            idx[1][0] = ic;
            idx[1][1] = ib;
            //{{ia,ic},{ic,ib}};
            {
                //#pragma omp parallel for
                for (int i = 0; i < 2; i++)
                {
                    iters[i] = std::partition(idx[i][0], idx[i][1], separator(level + 1, codes));
                }
            }
            il = iters[0];
            ir = iters[1];
        }

        size_t csz = ic - ia;
        size_t lsz = il - ia;
        size_t rsz = ir - ic;

        size_t offset = nodes_.size();

        size_t c = a + csz;
        size_t l = a + lsz;
        size_t r = c + rsz;

        SIMDBVHNode node;
        node.axis_top = level & 3;
        node.axis_left = node.axis_right = (level + 1) % 3;
        nodes_.push_back(node);

        vector3f minmax[4][2];
        for (int i = 0; i < 4; i++)
        {
            minmax[i][0] = vector3f(+FAR, +FAR, +FAR);
            minmax[i][1] = vector3f(-FAR, -FAR, -FAR);
        }

        size_t ranges[4][2];
        ranges[0][0] = a;
        ranges[0][1] = l;
        ranges[1][0] = l;
        ranges[1][1] = c;
        ranges[2][0] = c;
        ranges[2][1] = r;
        ranges[3][0] = r;
        ranges[3][1] = b;

        {
            size_t sub_indices[4];
            std::vector<PCFACE> sub_faces[4];
            aligned_vector<SIMDBVHNode> sub_nodes[4];
            for (int i = 0; i < 4; i++)
            {
                size_t fsz = ranges[i][1] - ranges[i][0];
                sub_faces[i].reserve(get_leaf_node_size(fsz));
                sub_nodes[i].reserve(get_branch_node_size(fsz));
            }

            {
#pragma omp parallel for
                for (int i = 0; i < 4; i++)
                {
                    sub_indices[i] = construct_deep(sub_faces[i], sub_nodes[i], minmax[i][0], minmax[i][1], faces, codes, indices, ranges[i][0], ranges[i][1], level + 2);
                }
            }

            PRINTLOG("plqbvh_mesh_accelerator:construct deep.\n");

            size_t face_offsets[4 + 1];
            size_t node_offsets[4 + 1];
            face_offsets[0] = 0;
            node_offsets[0] = 1;
            for (int i = 0; i < 4; i++)
            {
                face_offsets[i + 1] = sub_faces[i].size();
                node_offsets[i + 1] = sub_nodes[i].size();
            }
            for (int i = 0; i < 4; i++)
            {
                face_offsets[i + 1] += face_offsets[i];
                node_offsets[i + 1] += node_offsets[i];
            }

            for (int k = 0; k < 4; k++)
            {
                size_t idx = sub_indices[k];
                if (IsBranch(idx))
                { //
                    idx += node_offsets[k];
                    sub_indices[k] = idx;
                }
                else if (!IsEmpty(idx))
                { //
                    idx = GetFaceFirst(idx);
                    idx += face_offsets[k];
                    sub_indices[k] = MakeLeafIndex(idx);
                }
                nodes_[offset].children[k] = sub_indices[k];
            }

            faces_.resize(face_offsets[4]);
            nodes_.resize(node_offsets[4]);

            {
#pragma omp parallel for
                for (int i = 0; i < 4; i++)
                {
                    aligned_vector<SIMDBVHNode>& nodes = sub_nodes[i];
                    size_t jsz = nodes.size();
                    for (size_t j = 0; j < jsz; j++)
                    {
                        SIMDBVHNode& nd = nodes[j];
                        for (int k = 0; k < 4; k++)
                        {
                            size_t idx = nd.children[k];
                            if (IsBranch(idx))
                            { //
                                idx += node_offsets[i];
                                nd.children[k] = idx;
                            }
                            else if (!IsEmpty(idx))
                            { //
                                idx = GetFaceFirst(idx);
                                idx += face_offsets[i];
                                nd.children[k] = MakeLeafIndex(idx);
                            }
                        }
                    }

                    if (!sub_faces[i].empty())
                    {
                        memcpy(&faces_[face_offsets[i]], &(sub_faces[i][0]), sizeof(PCFACE) * sub_faces[i].size());
                    }
                    if (!sub_nodes[i].empty())
                    {
                        memcpy(&nodes_[node_offsets[i]], &(sub_nodes[i][0]), sizeof(SIMDBVHNode) * sub_nodes[i].size());
                    }
                }
            }
        }

        //convert & swizzle
        float bboxes[2][3][4];
        //for(int m = 0;m<2;m++){//minmax
        for (int j = 0; j < 3; j++)
        { //xyz
            for (int k = 0; k < 4; k++)
            {                                      //box
                bboxes[0][j][k] = minmax[k][0][j]; //
                bboxes[1][j][k] = minmax[k][1][j]; //
            }
        }
        //}

        //for(int i = 0;i<4;i++){
        for (int m = 0; m < 2; m++)
        { //minmax
            for (int j = 0; j < 3; j++)
            { //xyz
                nodes_[offset].bboxes[m][j] = _mm_setzero_ps();
                nodes_[offset].bboxes[m][j] = _mm_loadu_ps(bboxes[m][j]);
            }
        }
        //}
    }

    void plqbvh_mesh_accelerator_imp::add(PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        if (prop.singleside)
        {
            this->nTypeFace_ = 1;
        }
        else
        {
            this->nTypeFace_ = 0;
        }
        timer t;

        size_t sz = end - begin;
        std::vector<uint32_t> codes(sz);
        {
            vector3 tmin, tmax;
            get_morton_bound(tmin, tmax, min_, max_);
            t.start();
            create_morton_code(codes, begin, sz, tmin, tmax);
            t.end();
            PRINTLOG("plqbvh_mesh_accelerator:create morton code:%d ms\n", t.msec());
        }

        std::vector<size_t> indices(sz);
        for (size_t i = 0; i < sz; i++)
        {
            indices[i] = i;
        }
        //std::random_shuffle(indices.begin(), indices.end());
        t.start();
        construct(begin, &codes[0], &indices[0], 0, sz);
        t.end();
        PRINTLOG("plqbvh_mesh_accelerator:construct BVH tree:%d ms\n", t.msec());
    }

    //---------------------------------------------------------------
    //---------------------------------------------------------------

    plqbvh_mesh_accelerator::plqbvh_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        imp_ = new plqbvh_mesh_accelerator_imp(min, max, begin, end, prop);
    }

    plqbvh_mesh_accelerator::~plqbvh_mesh_accelerator()
    {
        delete imp_;
    }

    bool plqbvh_mesh_accelerator::test(const ray& r, real dist) const
    {
        return imp_->test(r, 0, dist);
    }

    bool plqbvh_mesh_accelerator::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, 0, dist);
    }

    vector3 plqbvh_mesh_accelerator::min() const
    {
        return imp_->min();
    }

    vector3 plqbvh_mesh_accelerator::max() const
    {
        return imp_->max();
    }
    size_t plqbvh_mesh_accelerator::memory_size() const
    {
        return imp_->memory_size();
    }
}
