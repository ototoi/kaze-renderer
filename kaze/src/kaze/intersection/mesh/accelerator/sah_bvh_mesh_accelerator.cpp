#include "sah_bvh_mesh_accelerator.h"

#include "intersection_ex.h"
#include "test_info.h"
#include "logger.h"

#include "mesh_face.h"

#include "values.h"

#include <memory>
#include <cstring>
#include <cmath>
#include <functional>
#include <algorithm>
#include <vector>
#include <utility>

#define MIN_FACE 16
#define LEAF_TEST_MINMAX 1
#define BVH_BIN_SIZE 64

namespace kaze
{

    typedef const mesh_face* PCFACE;

    using namespace std;

    namespace
    {

        class face_sorter : public std::binary_function<PCFACE, PCFACE, bool>
        {
        public:
            face_sorter(int plane) : plane_(plane) {}

            bool operator()(PCFACE a, PCFACE b) const
            {
                int plane = plane_;
                real ax = (*a->p_p0)[plane] + (*a->p_p1)[plane] + (*a->p_p2)[plane];
                real bx = (*b->p_p0)[plane] + (*b->p_p1)[plane] + (*b->p_p2)[plane];
                return ax < bx;
            }

        private:
            int plane_;
        };

        class pred_L : public std::unary_function<PCFACE, bool>
        {
        public:
            pred_L(int axis, real pos) : axis_(axis), pos_(pos) {}

            bool operator()(PCFACE p_face) const
            {
                int axis = axis_;
                real pos = pos_;

                const vector3& rtx0 = (*(p_face->p_p0));
                const vector3& rtx1 = (*(p_face->p_p1));
                const vector3& rtx2 = (*(p_face->p_p2));

                real center = (rtx0[axis] + rtx1[axis] + rtx2[axis]);

                return (center < pos * 3);
            }

        private:
            int axis_;
            real pos_;
        };

        class bvh_node
        {
        public:
            virtual ~bvh_node() {}
        public:
            virtual bool test(const ray& r, real tmin, real tmax) const = 0;
            virtual bool test(test_info* info, const ray& r, real tmin, real tmax) const = 0;
#if LEAF_TEST_MINMAX
            virtual const vector3& min() const = 0;
            virtual const vector3& max() const = 0;
#endif
            virtual size_t memory_size() const = 0;
        };

        class bvh_node_branch : public bvh_node
        {
        public:
            bvh_node_branch(PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
            ~bvh_node_branch();
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;
            //#if LEAF_TEST_MINMAX
            const vector3& min() const;
            const vector3& max() const;
            //#endif
            size_t memory_size() const;

        protected:
            bool test_internal(const ray& r, real tmin, real tmax) const;
            bool test_internal(test_info* info, const ray& r, real tmin, real tmax) const;

        private:
            bvh_node* nodes_[2];

            int plane_;

            vector3 min_;
            vector3 max_;
        };

        class bvh_node_leaf_SS : public bvh_node
        {
        public:
            bvh_node_leaf_SS(PCFACE* begin, PCFACE* end);

        public:
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;
#if LEAF_TEST_MINMAX
            const vector3& min() const;
            const vector3& max() const;
#endif
            size_t memory_size() const;

        private:
            std::vector<PCFACE> mv_;
#if LEAF_TEST_MINMAX
            vector3 min_;
            vector3 max_;
#endif
        };

        class bvh_node_leaf_DS : public bvh_node
        {
        public:
            bvh_node_leaf_DS(PCFACE* begin, PCFACE* end);

        public:
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;
#if LEAF_TEST_MINMAX
            const vector3& min() const;
            const vector3& max() const;
#endif
            size_t memory_size() const;

        private:
            std::vector<PCFACE> mv_;
#if LEAF_TEST_MINMAX
            vector3 min_;
            vector3 max_;
#endif
        };

        //---------------------------------------------------------------------------------------------

        //#region

        inline int eval_phase(int phase, int axis)
        { //rid<0
            return (phase >> axis) & 0x1;
        }

        void get_minmax(vector3& pmin, vector3& pmax, PCFACE face)
        {
            const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);

            vector3 points[3];
            points[0] = *(face->p_p0);
            points[1] = *(face->p_p1);
            points[2] = *(face->p_p2);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (points[i][j] < min[j]) min[j] = points[i][j];
                    if (points[i][j] > max[j]) max[j] = points[i][j];
                }
            }

            pmin = min;
            pmax = max;
        }

        void get_minmax(vector3& pmin, vector3& pmax, PCFACE* a, PCFACE* b)
        {
            const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            for (PCFACE* i = a; i != b; i++)
            {
                vector3 cmin;
                vector3 cmax;
                get_minmax(cmin, cmax, *i);
                for (int j = 0; j < 3; j++)
                {
                    if (cmin[j] < min[j]) min[j] = cmin[j];
                    if (cmax[j] > max[j]) max[j] = cmax[j];
                }
            }
            pmin = min;
            pmax = max;
        }

        static void find_sort(PCFACE* mid, PCFACE* a, PCFACE* b, real min, real max, int plane)
        {
            if (a <= mid && mid < b)
            {
                size_t sz = b - a;
                if (sz <= 256)
                {
                    std::sort(a, b, face_sorter(plane));
                }
                else
                {
                    real pos = (min + max) * 0.5;
                    PCFACE* c = std::partition(a, b, pred_L(plane, pos));
                    if (a == c || b == c)
                    {
                        std::sort(a, b, face_sorter(plane));
                    }
                    else
                    {
                        find_sort(mid, a, c, min, pos, plane);
                        find_sort(mid, c, b, pos, max, plane);
                    }
                }
            }
        }

        struct BinBuffer
        {
            /*
         * (min, max) * xyz * binsize
         */
            size_t bin[2][3][BVH_BIN_SIZE];
        };

        static void getBinBuffer(BinBuffer* bins, const vector3& scene_min, const vector3& scene_max, PCFACE* a, PCFACE* b)
        {
            static const real EPS = values::epsilon() * 1024;

            real binsize = (real)BVH_BIN_SIZE;

            /*
		 * Calculate extent
		 */
            vector3 scene_size, scene_invsize;
            scene_size = scene_max - scene_min;
            for (int i = 0; i < 3; ++i)
            {
                assert(scene_size[i] >= 0.0f);

                if (scene_size[i] > EPS)
                {
                    scene_invsize[i] = binsize / scene_size[i];
                }
                else
                {
                    scene_invsize[i] = 0.0f;
                }
            }

            /*
		 * Clear bin data
		 */
            memset(bins, 0, sizeof(BinBuffer));

            size_t idx_bmin[3];
            size_t idx_bmax[3];

            for (PCFACE* i = a; i != b; ++i)
            {
                /*
			 * Quantize the position into [0, BIN_SIZE)
			 *
			 * q[i] = (int)(p[i] - scene_bmin) / scene_size
			 */
                vector3 bmin;
                vector3 bmax;
                get_minmax(bmin, bmax, (*i));
                vector3 quantized_bmin = (bmin - scene_min) * scene_invsize;
                vector3 quantized_bmax = (bmax - scene_min) * scene_invsize;
                //size_t fsz = (*i)->face_size;

                /*
			 * idx is now in [0, BIN_SIZE)
			 */
                for (int j = 0; j < 3; ++j)
                {
                    idx_bmin[j] = (unsigned int)floor(quantized_bmin[j]);
                    idx_bmax[j] = (unsigned int)floor(quantized_bmax[j]);

                    if (idx_bmin[j] >= BVH_BIN_SIZE) idx_bmin[j] = BVH_BIN_SIZE - 1;
                    if (idx_bmax[j] >= BVH_BIN_SIZE) idx_bmax[j] = BVH_BIN_SIZE - 1;

                    assert(idx_bmin[j] < BVH_BIN_SIZE);
                    assert(idx_bmax[j] < BVH_BIN_SIZE);

                    /*
				 * Increment bin counter
				 */

                    bins->bin[0][j][idx_bmin[j]] += 1;
                    bins->bin[1][j][idx_bmax[j]] += 1;
                }
            }
        }

        static real calcSurfaceArea(const vector3& min, const vector3& max)
        {
            vector3 box = max - min;
            return 2 * (box[0] * box[1] + box[1] * box[2] + box[2] * box[0]);
        }

        static real SAH(
            size_t ns1,
            real leftArea,
            size_t ns2,
            real rightArea,
            real s)
        {
            const real Taabb = 0.2f;
            const real Ttri = 0.8f;
            real T;

            T = 2.0f * Taabb + (leftArea / s) * (real)(ns1)*Ttri + (rightArea / s) * (real)(ns2)*Ttri;

            return T;
        }

        static void findCutFromBin(
            const BinBuffer* bins,
            real& cutPos,
            int& cutAxis,
            const vector3& bmin,
            const vector3& bmax,
            size_t ntriangles)
        {
            int minCostAxis = 0;
            real minCostPos = 0.0f;
            real minCost = std::numeric_limits<real>::max();
            size_t left, right;
            vector3 bsize, bstep;
            vector3 bminLeft, bmaxLeft;
            vector3 bminRight, bmaxRight;
            real saLeft, saRight, saTotal;
            real pos;

            bsize = bmax - bmin;
            bstep = bsize * (1.0 / BVH_BIN_SIZE);
            saTotal = calcSurfaceArea(bmin, bmax);

            for (int j = 0; j < 3; ++j)
            {
                /*
			 * Compute SAH cost for right side of each cell of the bbox.
			 * Exclude both extreme side of the bbox.
			 *
			 *  i:      0    1    2    3
			 *     +----+----+----+----+----+
			 *     |    |    |    |    |    |
			 *     +----+----+----+----+----+
			 */
                left = 0;
                right = ntriangles;
                bminLeft = bminRight = bmin;
                bmaxLeft = bmaxRight = bmax;

                for (int i = 0; i < BVH_BIN_SIZE - 1; ++i)
                {
                    left += bins->bin[0][j][i];
                    right -= bins->bin[1][j][i];

                    assert(left <= ntriangles);
                    assert(right <= ntriangles);

                    /*
				 * Split pos bmin + (i + 1) * (bsize / BIN_SIZE)
				 * +1 for i since we want a position on right side of the cell.
				 */

                    pos = bmin[j] + (i + 0.5) * bstep[j];
                    bmaxLeft[j] = pos;
                    bminRight[j] = pos;

                    saLeft = calcSurfaceArea(bminLeft, bmaxLeft);
                    saRight = calcSurfaceArea(bminRight, bmaxRight);

                    real cost = SAH(left, saLeft, right, saRight, saTotal);
                    if (cost < minCost)
                    {
                        /*
					 * Update the mincost
					 */
                        minCost = cost;
                        minCostAxis = j;
                        minCostPos = pos;
                    }
                }
            }

            cutAxis = minCostAxis;
            cutPos = minCostPos;
        }

        bvh_node_branch::bvh_node_branch(PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
        {
            static const real EPS = values::epsilon() * 1000;
            nodes_[0] = 0;
            nodes_[1] = 0;
            plane_ = 0;
            vector3 min;
            vector3 max;
            get_minmax(min, max, begin, end);
            vector3 wid = max - min;
            int plane = 0;
            if (wid[1] > wid[plane]) plane = 1;
            if (wid[2] > wid[plane]) plane = 2;

            size_t fsz = end - begin;
            real cutPos;
            int cutAxis;
            BinBuffer bins; //
            getBinBuffer(&bins, min, max, begin, end);
            findCutFromBin(&bins, cutPos, cutAxis, min, max, fsz);

            PCFACE* mid = std::partition(begin, end, pred_L(cutAxis, cutPos));

            size_t lsz = mid - begin;
            size_t rsz = end - mid;
            if (lsz == 0 || rsz == 0)
            {
                mid = begin + (fsz >> 1);
                find_sort(mid, begin, end, min[cutAxis], max[cutAxis], cutAxis);
                mid = begin + (fsz >> 1);
            }

            //---------------------------------------
            PCFACE* a;
            PCFACE* b;
            size_t sz;
            //---------------------------------------

            auto_ptr<bvh_node> ap_l;
            auto_ptr<bvh_node> ap_r;

            //----------------------------------
            a = begin;
            b = mid;
            sz = b - a;
            if (sz)
            {
                if (MIN_FACE < sz)
                {
                    ap_l.reset(new bvh_node_branch(a, b, prop));
                }
                else
                {
                    if (prop.singleside)
                    {
                        ap_l.reset(new bvh_node_leaf_SS(a, b));
                    }
                    else
                    {
                        ap_l.reset(new bvh_node_leaf_DS(a, b));
                    }
                }
            }
            //----------------------------------

            a = mid;
            b = end;
            sz = b - a;
            if (sz)
            {
                if (MIN_FACE < sz)
                {
                    ap_r.reset(new bvh_node_branch(a, b, prop));
                }
                else
                {
                    if (prop.singleside)
                    {
                        ap_r.reset(new bvh_node_leaf_SS(a, b));
                    }
                    else
                    {
                        ap_r.reset(new bvh_node_leaf_DS(a, b));
                    }
                }
            }

            plane_ = plane;
            min_ = min + vector3(-EPS, -EPS, -EPS);
            max_ = max + vector3(+EPS, +EPS, +EPS);
            nodes_[0] = ap_l.release();
            nodes_[1] = ap_r.release();
        }

        bvh_node_branch::~bvh_node_branch()
        {
            if (nodes_[0]) delete nodes_[0];
            if (nodes_[1]) delete nodes_[1];
        }

        bool bvh_node_branch::test(const ray& r, real tmin, real tmax) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, tmin, tmax))
            {
                tmin = std::max<real>(tmin, rng.tmin);
                tmax = std::min<real>(tmax, rng.tmax);
                return this->test_internal(r, tmin, tmax);
            }
            return false;
        }

        bool bvh_node_branch::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, tmin, tmax))
            {
                tmin = std::max<real>(tmin, rng.tmin);
                tmax = std::min<real>(tmax, rng.tmax);
                return this->test_internal(info, r, tmin, tmax);
            }
            return false;
        }

        bool bvh_node_branch::test_internal(const ray& r, real tmin, real tmax) const
        {
            int phase = r.phase();
            int plane = plane_;
            int nNear = eval_phase(phase, plane);
            int nFar = 1 - nNear;

            if (nodes_[nNear] && nodes_[nNear]->test(r, tmin, tmax)) return true;
            if (nodes_[nFar] && nodes_[nFar]->test(r, tmin, tmax)) return true;

            return false;
        }

        bool bvh_node_branch::test_internal(test_info* info, const ray& r, real tmin, real tmax) const
        {
            int phase = r.phase();
            int plane = plane_;
            int nNear = eval_phase(phase, plane);
            int nFar = 1 - nNear;

            bool bRet = false;
            if (nodes_[nNear] && nodes_[nNear]->test(info, r, tmin, tmax))
            {
                tmax = info->distance;
                bRet = true;
            }

            if (nodes_[nFar] && nodes_[nFar]->test(info, r, tmin, tmax))
            {
                tmax = info->distance;
                bRet = true;
            }

            return bRet;
        }
        //#if LEAF_TEST_MINMAX
        const vector3& bvh_node_branch::min() const { return min_; }
        const vector3& bvh_node_branch::max() const { return max_; }
        //#endif

        size_t bvh_node_branch::memory_size() const
        {
            size_t tmp = 0;
            if (nodes_[0]) tmp += nodes_[0]->memory_size();
            if (nodes_[1]) tmp += nodes_[1]->memory_size();
            tmp += sizeof(bvh_node_branch);
            return tmp;
        }

        //---------------------------------------------------------------------------------------------
        bvh_node_leaf_SS::bvh_node_leaf_SS(PCFACE* begin, PCFACE* end)
        {
#if LEAF_TEST_MINMAX
            //---------------------------------------
            const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            //---------------------------------------
            for (PCFACE* i = begin; i != end; i++)
            {
                vector3 cmin, cmax;
                get_minmax(cmin, cmax, *i);
                for (int j = 0; j < 3; j++)
                {
                    if (cmin[j] < min[j]) min[j] = cmin[j];
                    if (cmax[j] > max[j]) max[j] = cmax[j];
                }
            }
            //---------------------------------------
            min_ = min;
            max_ = max;
#endif
            mv_.assign(begin, end);
        }

        bool bvh_node_leaf_SS::test(const ray& r, real tmin, real tmax) const
        {
#if LEAF_TEST_MINMAX
            if (!test_AABB(min_, max_, r, tmax)) return false;
#endif
            size_t sz = mv_.size();
            const PCFACE* faces = &(mv_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (faces[i]->test_SS(r, tmax)) return true;
            }
            return false;
        }
        bool bvh_node_leaf_SS::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
#if LEAF_TEST_MINMAX
            if (!test_AABB(min_, max_, r, tmax)) return false;
#endif
            bool bRet = false;
            size_t sz = mv_.size();
            const PCFACE* faces = &(mv_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (faces[i]->test_SS(info, r, tmax))
                {
                    tmax = info->distance;
                    bRet = true;
                }
            }
            return bRet;
        }
#if LEAF_TEST_MINMAX
        const vector3& bvh_node_leaf_SS::min() const
        {
            return min_;
        }
        const vector3& bvh_node_leaf_SS::max() const { return max_; }
#endif

        size_t bvh_node_leaf_SS::memory_size() const
        {
            size_t tmp = 0;
            tmp += sizeof(bvh_node_leaf_SS);
            tmp += sizeof(PCFACE) * mv_.capacity();
            return tmp;
        }

        //---------------------------------------------------------------------------------------------
        bvh_node_leaf_DS::bvh_node_leaf_DS(PCFACE* begin, PCFACE* end)
        {
#if LEAF_TEST_MINMAX
            //---------------------------------------
            const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            //---------------------------------------
            for (PCFACE* i = begin; i != end; i++)
            {
                vector3 cmin, cmax;
                get_minmax(cmin, cmax, *i);
                for (int j = 0; j < 3; j++)
                {
                    if (cmin[j] < min[j]) min[j] = cmin[j];
                    if (cmax[j] > max[j]) max[j] = cmax[j];
                }
            }
            //---------------------------------------
            min_ = min;
            max_ = max;
#endif
            mv_.assign(begin, end);
        }

        bool bvh_node_leaf_DS::test(const ray& r, real tmin, real tmax) const
        {
#if LEAF_TEST_MINMAX
            if (!test_AABB(min_, max_, r, tmax)) return false;
#endif
            size_t sz = mv_.size();
            const PCFACE* faces = &(mv_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (faces[i]->test_DS(r, tmax)) return true;
            }
            return false;
        }
        bool bvh_node_leaf_DS::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
#if LEAF_TEST_MINMAX
            if (!test_AABB(min_, max_, r, tmax)) return false;
#endif
            bool bRet = false;
            size_t sz = mv_.size();
            const PCFACE* faces = &(mv_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (faces[i]->test_DS(info, r, tmax))
                {
                    tmax = info->distance;
                    bRet = true;
                }
            }
            return bRet;
        }
#if LEAF_TEST_MINMAX
        const vector3& bvh_node_leaf_DS::min() const
        {
            return min_;
        }
        const vector3& bvh_node_leaf_DS::max() const { return max_; }
#endif

        size_t bvh_node_leaf_DS::memory_size() const
        {
            size_t tmp = 0;
            tmp += sizeof(bvh_node_leaf_DS);
            tmp += sizeof(PCFACE) * mv_.capacity();
            return tmp;
        }

        //#endregion

        //---------------------------------------------------------------------------------------------
    }
    //---------------------------------------------------------------------------------------------

    class sah_bvh_mesh_accelerator_imp
    {
    public:
        sah_bvh_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
        ~sah_bvh_mesh_accelerator_imp();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;
        size_t memory_size() const { return sizeof(sah_bvh_mesh_accelerator_imp) + root_->memory_size(); }
    private:
        bvh_node_branch* root_;
    };

    sah_bvh_mesh_accelerator_imp::sah_bvh_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        root_ = new bvh_node_branch(begin, end, prop);

        //print_log("sah_bvh_mesh_accelerator:memory size:%d bytes\n", root_->memory_size());
    }

    sah_bvh_mesh_accelerator_imp::~sah_bvh_mesh_accelerator_imp()
    {
        delete root_;
    }

    bool sah_bvh_mesh_accelerator_imp::test(const ray& r, real dist) const
    {
        return root_->test(r, 0, dist);
    }

    bool sah_bvh_mesh_accelerator_imp::test(test_info* info, const ray& r, real dist) const
    {
        return root_->test(info, r, 0, dist);
    }

    vector3 sah_bvh_mesh_accelerator_imp::min() const
    {
        return root_->min();
    }

    vector3 sah_bvh_mesh_accelerator_imp::max() const
    {
        return root_->max();
    }

    //-----------------------------------------------------------------------------------------------

    sah_bvh_mesh_accelerator::sah_bvh_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        imp_ = new sah_bvh_mesh_accelerator_imp(min, max, begin, end, prop);
    }

    sah_bvh_mesh_accelerator::~sah_bvh_mesh_accelerator()
    {
        delete imp_;
    }

    bool sah_bvh_mesh_accelerator::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool sah_bvh_mesh_accelerator::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }

    vector3 sah_bvh_mesh_accelerator::min() const
    {
        return imp_->min();
    }

    vector3 sah_bvh_mesh_accelerator::max() const
    {
        return imp_->max();
    }

    size_t sah_bvh_mesh_accelerator::memory_size() const
    {
        return imp_->memory_size();
    }
}
