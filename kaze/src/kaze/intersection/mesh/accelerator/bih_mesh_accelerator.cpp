#include "bih_mesh_accelerator.h"

#include <memory>
#include <cstring>
#include <cmath>
#include <functional>
#include <algorithm>
#include <vector>

#include "test_info.h"
#include "mesh_face.h"

#include "intersection_ex.h"

//#include "mmap_array.hpp"

#include "logger.h"

#define MIN_FACE 16

namespace
{
    using namespace kaze;

    typedef const mesh_face* PCFACE;
    typedef PCFACE* face_iterator; //face_iterator
    typedef std::vector<PCFACE> face_list;

    //typedef std::size_t size_t;

    inline double log2(double x)
    {
        using namespace std;
        static const double LOG2 = log(2.0);
        return log(x) / LOG2;
    }
    int get_level(size_t face_num, int density = 8)
    {
        using namespace std;
        double f = log2(double(face_num * density * density * density));
        int nRet = int(ceil(f)) + 2;
        return nRet;
    }

    struct progress_struct
    {
        size_t now; //
        size_t max; //
        double isz;
        int cnt;
    };

    class bound
    {
    public:
        bound(const vector3& min, const vector3& max) : min_(min), max_(max) {}

        const vector3& min() const { return min_; }
        const vector3& max() const { return max_; }
    private:
        vector3 min_;
        vector3 max_;
    };

    class bih_node
    {
    public:
        virtual ~bih_node() {}

        virtual bool test(/**/ const ray& r, real tmin, real tmax) const = 0;
        virtual bool test(test_info* info, const ray& r, real tmin, real tmax) const = 0;

        virtual size_t size() const = 0;
        virtual size_t memory_size() const = 0;
    };

    class bih_node_leaf_SS : public bih_node
    {
    public:
        bih_node_leaf_SS(face_iterator begin, face_iterator end) : mv_(begin, end) {}
        bool test(/**/ const ray& r, real tmin, real tmax) const;
        bool test(test_info* info, const ray& r, real tmin, real tmax) const;

        size_t size() const { return mv_.size(); }
        size_t memory_size() const { return sizeof(bih_node_leaf_SS) + sizeof(PCFACE*) * mv_.capacity(); }
    private:
        std::vector<PCFACE> mv_;
    };

    class bih_node_leaf_DS : public bih_node
    {
    public:
        bih_node_leaf_DS(face_iterator begin, face_iterator end) : mv_(begin, end) {}
        bool test(/**/ const ray& r, real tmin, real tmax) const;
        bool test(test_info* info, const ray& r, real tmin, real tmax) const;

        size_t size() const { return mv_.size(); }
        size_t memory_size() const { return sizeof(bih_node_leaf_DS) + sizeof(PCFACE*) * mv_.capacity(); }
    private:
        std::vector<PCFACE> mv_;
    };

    class bih_node_branch : public bih_node
    {
    public:
        bih_node_branch(face_iterator a, face_iterator b, const bound& bnd, int level, int max_level, const mesh_accelerator_property& prop, progress_struct* prg);
        ~bih_node_branch();
        bool test(/**/ const ray& r, real tmin, real tmax) const;
        bool test(test_info* info, const ray& r, real tmin, real tmax) const;

        size_t size() const
        {
            size_t sz = 0;
            if (p_l_) sz += p_l_->size();
            if (p_r_) sz += p_r_->size();
            return sz;
        }
        size_t memory_size() const
        {
            size_t sz = sizeof(bih_node_branch);
            if (p_l_) sz += p_l_->memory_size();
            if (p_r_) sz += p_r_->memory_size();
            return sz;
        }

    private:
        char axis_;
        real clip_[2];
        bih_node* p_l_;
        bih_node* p_r_;
    };

    bool bih_node_leaf_SS::test(/**/ const ray& r, real tmin, real tmax) const
    {
        const PCFACE* p_face = &(mv_[0]);
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (p_face[i]->test_SS(r, tmax)) return true;
        }
        return false;
    }

    bool bih_node_leaf_SS::test(test_info* info, const ray& r, real tmin, real tmax) const
    {
        bool bRet = false;
        const PCFACE* p_face = &(mv_[0]);
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (p_face[i]->test_SS(info, r, tmax))
            {
                tmax = info->distance;
                bRet = true;
            }
        }
        return bRet;
    }

    bool bih_node_leaf_DS::test(/**/ const ray& r, real tmin, real tmax) const
    {
        const PCFACE* p_face = &(mv_[0]);
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (p_face[i]->test_DS(r, tmax)) return true;
        }
        return false;
    }

    bool bih_node_leaf_DS::test(test_info* info, const ray& r, real tmin, real tmax) const
    {
        bool bRet = false;
        const PCFACE* p_face = &(mv_[0]);
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (p_face[i]->test_DS(info, r, tmax))
            {
                tmax = info->distance;
                bRet = true;
            }
        }
        return bRet;
    }

    class pred_L : public std::unary_function<PCFACE, bool>
    {
    public:
        pred_L(int axis, real pos) : axis_(axis), pos_(pos) {}

        bool operator()(PCFACE p_face) const
        {
            int axis = axis_;
            real pos = pos_;

            const vector3& rtx0 = (*(p_face->p_p0));
            const vector3& rtx1 = (*(p_face->p_p1));
            const vector3& rtx2 = (*(p_face->p_p2));

            real center = (rtx0[axis] + rtx1[axis] + rtx2[axis]);

            return (center < pos * 3);
        }

    private:
        int axis_;
        real pos_;
    };

    inline real get_min(face_iterator a, face_iterator b, int axis)
    {
        real val = +values::far();
        for (face_iterator i = a; i != b; i++)
        {
            PCFACE p_face = *i;

            const vector3& rtx0 = (*(p_face->p_p0));
            const vector3& rtx1 = (*(p_face->p_p1));
            const vector3& rtx2 = (*(p_face->p_p2));

            if (val > rtx0[axis]) val = rtx0[axis];
            if (val > rtx1[axis]) val = rtx1[axis];
            if (val > rtx2[axis]) val = rtx2[axis];
        }

        return val;
    }

    inline real get_max(face_iterator a, face_iterator b, int axis)
    {
        real val = -values::far();
        for (face_iterator i = a; i != b; i++)
        {
            PCFACE p_face = *i;

            const vector3& rtx0 = (*(p_face->p_p0));
            const vector3& rtx1 = (*(p_face->p_p1));
            const vector3& rtx2 = (*(p_face->p_p2));

            if (val < rtx0[axis]) val = rtx0[axis];
            if (val < rtx1[axis]) val = rtx1[axis];
            if (val < rtx2[axis]) val = rtx2[axis];
        }

        return val;
    }

#define DO_PRGRESS()                                   \
    prg->now += sz;                                    \
    prg->cnt++;                                        \
    if (prg->cnt <= 10)                                \
    {                                                  \
        prg->cnt = 0;                                  \
        print_progress("%f%%\r", prg->now * prg->isz); \
    }

    bih_node_branch::bih_node_branch(face_iterator a, face_iterator b, const bound& bnd, int level, int max_level, const mesh_accelerator_property& prop, progress_struct* prg)
    {
        static const real FAR = values::far();

        p_l_ = 0;
        p_r_ = 0;

        vector3 wide = bnd.max() - bnd.min();

        int axis = 0;
        if (wide[axis] < wide[1]) axis = 1;
        if (wide[axis] < wide[2]) axis = 2;

        real pos = bnd.min()[axis] + wide[axis] * real(0.5);

        face_iterator begin, end;
        size_t sz;

        std::unique_ptr<bih_node> ap_l;
        std::unique_ptr<bih_node> ap_r;

        real clip[2];
        clip[0] = bnd.min()[axis];
        clip[1] = bnd.max()[axis];

        face_iterator split = std::partition(a, b, pred_L(axis, pos));

        begin = a;
        end = split;
        sz = std::distance(begin, end);

        if (sz)
        {
            if ((MIN_FACE < sz) && (level + 1 < max_level))
            { //branch
                vector3 min = bnd.min();
                vector3 max = bnd.max();
                max[axis] = pos;

                ap_l.reset(new bih_node_branch(begin, end, bound(min, max), level + 1, max_level, prop, prg));
            }
            else
            {
                if (prop.singleside)
                {
                    ap_l.reset(new bih_node_leaf_SS(begin, end));
                }
                else
                {
                    ap_l.reset(new bih_node_leaf_DS(begin, end));
                }

                //DO_PRGRESS();
            }
            clip[0] = get_max(begin, end, axis);
        }

        begin = split;
        end = b;
        sz = std::distance(begin, end);

        if (sz)
        {
            if (MIN_FACE < sz && (level + 1 < max_level))
            { //branch
                vector3 min = bnd.min();
                min[axis] = pos;
                vector3 max = bnd.max();

                ap_r.reset(new bih_node_branch(begin, end, bound(min, max), level + 1, max_level, prop, prg));
            }
            else
            {

                if (prop.singleside)
                {
                    ap_r.reset(new bih_node_leaf_SS(begin, end));
                }
                else
                {
                    ap_r.reset(new bih_node_leaf_DS(begin, end));
                }

                //DO_PRGRESS();
            }
            clip[1] = get_min(begin, end, axis);
        }

        p_l_ = ap_l.release();
        p_r_ = ap_r.release();

        axis_ = axis;
        memcpy(clip_, clip, sizeof(real) * 2);
    }

    bih_node_branch::~bih_node_branch()
    {
        if (p_l_) delete p_l_;
        if (p_r_) delete p_r_;
    }

//-------------------------------------------------------
#if 1
    inline int eval_phase(int phase, int axis)
    { //rid<0
        return (phase >> axis) & 0x1;
    }
#else
    inline int eval_phase(int phase, int axis)
    { //rid<0
        static const int table[][3] = {
            {0, 0, 0}, //0
            {1, 0, 0}, //1
            {0, 1, 0}, //2
            {1, 1, 0}, //3
            {0, 0, 1}, //4
            {1, 0, 1}, //5
            {0, 1, 1}, //6
            {1, 1, 1}  //7
        };
        return table[phase][axis];
    }
#endif

    inline int sgn(real x)
    {
        return x <= 0;
    }
//-------------------------------------------------------

#define TEST_(p, r, min, max) p->test(r, min, max)
#define TESTINFO_(p, i, r, min, max) p->test(i, r, min, max)

    bool bih_node_branch::test(/**/ const ray& r, real tmin, real tmax) const
    {
        const vector3& org = r.origin();
        const vector3& idir = r.inversed_direction();
        int phase = r.phase();

        int axis = this->axis_;
        //real clip[2];
        //memcpy(clip,clip_,sizeof(real)*2);

        const bih_node* nodes[2];
        nodes[0] = p_l_;
        nodes[1] = p_r_;

        real ro = org[axis];
        real rid = idir[axis];

        int n_s = eval_phase(phase, axis);
        int f_s = 1 - n_s;

        real tpos[2];
        tpos[0] = (clip_[0] - ro) * rid;
        tpos[1] = (clip_[1] - ro) * rid;

        if (nodes[n_s] && tmin <= tpos[n_s])
        {
            if (TEST_(nodes[n_s], r, tmin, std::min<real>(tmax, tpos[n_s]))) return true;
        }
        if (nodes[f_s] && tpos[f_s] <= tmax)
        {
            if (TEST_(nodes[f_s], r, std::max<real>(tmin, tpos[f_s]), tmax)) return true;
        }

        return false;
    }

    bool bih_node_branch::test(test_info* info, const ray& r, real tmin, real tmax) const
    {
        const vector3& org = r.origin();
        const vector3& idir = r.inversed_direction();
        int phase = r.phase();

        int axis = this->axis_;
        //real clip[2];
        //memcpy(clip,clip_,sizeof(real)*2);

        const bih_node* nodes[2];
        nodes[0] = p_l_;
        nodes[1] = p_r_;

        real ro = org[axis];
        real rid = idir[axis];

        int n_s = eval_phase(phase, axis);
        int f_s = 1 - n_s;

        real tpos[2];
        tpos[0] = (clip_[0] - ro) * rid;
        tpos[1] = (clip_[1] - ro) * rid;

        bool bRet = false;

        if (nodes[n_s] && tmin <= tpos[n_s])
        {
            if (TESTINFO_(nodes[n_s], info, r, tmin, std::min<real>(tmax, tpos[n_s])))
            {
                bRet = true;
                tmax = info->distance;
            }
        }

        if (nodes[f_s] && tpos[f_s] <= tmax)
        {
            if (TESTINFO_(nodes[f_s], info, r, std::max<real>(tmin, tpos[f_s]), tmax))
            {
                bRet = true;
                tmax = info->distance; //no need
            }
        }

        return bRet;
    }
}

namespace kaze
{

    class bih_mesh_accelerator_imp
    {
    public:
        bih_mesh_accelerator_imp(const vector3& min, const vector3& max) : min_(min), max_(max), root_(0) {}

        void add(face_iterator begin, face_iterator end, const mesh_accelerator_property& prop);

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;

        const vector3& min() const { return min_; }
        const vector3& max() const { return max_; }

        size_t size() const { return root_->size(); }
        size_t memory_size() const { return sizeof(bih_mesh_accelerator_imp) + root_->memory_size(); }

    private:
        vector3 min_;
        vector3 max_;

        bih_node_branch* root_;
    };

    void bih_mesh_accelerator_imp::add(face_iterator begin, face_iterator end, const mesh_accelerator_property& prop)
    {
        using namespace std;
        if (root_) delete root_;

        size_t csz = end - begin;
        int max_level = get_level(csz);

        progress_struct prg;
        prg.now = 0;
        prg.max = csz;
        prg.isz = 100.0 / csz;
        prg.cnt = 0;

        root_ = new bih_node_branch(begin, end, bound(min_, max_), 0, max_level, prop, &prg);

        //print_log("%d,%d\n",csz,root_->size());
        //print_log("bih_mesh_accelerator:memory size:%d bytes\n",root_->memory_size());
    }

    bool bih_mesh_accelerator_imp::test(const ray& r, real dist) const
    {
        assert(root_);
        range_AABB rng;
        if (test_AABB(&rng, this->min_, this->max_, r, dist))
        {
            return root_->test(r, std::max<real>(0, rng.tmin), std::min<real>(rng.tmax, dist));
        }
        return false;
    }

    bool bih_mesh_accelerator_imp::test(test_info* info, const ray& r, real dist) const
    {
        assert(root_);
        range_AABB rng;
        if (test_AABB(&rng, this->min_, this->max_, r, dist))
        {
            return root_->test(info, r, std::max<real>(0, rng.tmin), std::min<real>(rng.tmax, dist));
        }
        return false;
    }

    //---------------------------------------------------------------
    //---------------------------------------------------------------

    bih_mesh_accelerator::bih_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        std::unique_ptr<bih_mesh_accelerator_imp> ap(new bih_mesh_accelerator_imp(min, max));
        ap->add(begin, end, prop);
        imp_ = ap.release();
    }

    bih_mesh_accelerator::~bih_mesh_accelerator()
    {
        delete imp_;
    }

    bool bih_mesh_accelerator::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool bih_mesh_accelerator::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }

    vector3 bih_mesh_accelerator::min() const
    {
        return imp_->min();
    }

    vector3 bih_mesh_accelerator::max() const
    {
        return imp_->max();
    }
    size_t bih_mesh_accelerator::memory_size() const
    {
        return imp_->memory_size();
    }
}
