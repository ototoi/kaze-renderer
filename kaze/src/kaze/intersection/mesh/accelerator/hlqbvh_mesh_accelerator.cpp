#include "hlqbvh_mesh_accelerator.h"

#include "intersection_ex.h"
#include "test_info.h"
#include "logger.h"
#include "timer.h"
#include "mesh_face.h"

#include "values.h"

#include "aligned_vector.hpp"

#include "lbvh_utility.h"

#include <memory>
#include <cstring>
#include <cmath>
#include <functional>
#include <algorithm>
#include <vector>
#include <utility>

#include <xmmintrin.h>
#include <emmintrin.h>

#include <stdlib.h>

#define DIV_BIT 10
#define DIV_NUM 1024
#define VOX_M 6

#define MIN_FACE 16
#define MAX_FACE 32

static const int STACK_SIZE = ((int)(sizeof(size_t) * 16));

static const float FEPS = std::numeric_limits<float>::epsilon() * 2;

namespace kaze
{
    namespace
    {
        //For float
        static inline int test_AABB(
            const __m128 bboxes[2][3], //4boxes : min-max[2] of xyz[3] of boxes[4]
            const __m128 org[3],       //ray origin
            const __m128 idir[3],      //ray inveresed direction
            const int sign[3],         //ray xyz direction -> +:0,-:1
            __m128 tmin, __m128 tmax   //ray range tmin-tmax
            )
        {
            // x coordinate
            tmin = _mm_max_ps(
                tmin,
                _mm_mul_ps(_mm_sub_ps(bboxes[sign[0]][0], org[0]), idir[0]));
            tmax = _mm_min_ps(
                tmax,
                _mm_mul_ps(_mm_sub_ps(bboxes[1 - sign[0]][0], org[0]), idir[0]));

            // y coordinate
            tmin = _mm_max_ps(
                tmin,
                _mm_mul_ps(_mm_sub_ps(bboxes[sign[1]][1], org[1]), idir[1]));
            tmax = _mm_min_ps(
                tmax,
                _mm_mul_ps(_mm_sub_ps(bboxes[1 - sign[1]][1], org[1]), idir[1]));

            // z coordinate
            tmin = _mm_max_ps(
                tmin,
                _mm_mul_ps(_mm_sub_ps(bboxes[sign[2]][2], org[2]), idir[2]));
            tmax = _mm_min_ps(
                tmax,
                _mm_mul_ps(_mm_sub_ps(bboxes[1 - sign[2]][2], org[2]), idir[2]));

            return _mm_movemask_ps(_mm_cmpge_ps(tmax, tmin));
        }

        static const size_t EMPTY_MASK = size_t(-1);        //0xFFFF...
        static const size_t SIGN_MASK = ~(EMPTY_MASK >> 1); //0x8000...

#pragma pack(push, 4)
        struct SIMDBVHNode
        {
            __m128 bboxes[2][3]; //768
            size_t children[4];  //128
            int axis_top;        //128
            int axis_right;
            int axis_left;
            int reserved;
        };
#pragma pack(pop)

        inline bool IsLeaf(size_t i)
        {
            return (SIGN_MASK & i) ? true : false;
        }
        inline bool IsBranch(size_t i)
        {
            return !IsLeaf(i);
        }
        inline bool IsEmpty(size_t i)
        {
            return i == EMPTY_MASK;
        }

        inline size_t MakeLeafIndex(size_t i)
        {
            return SIGN_MASK | i;
        }

        inline size_t GetFaceFirst(size_t i)
        {
            return (~SIGN_MASK) & i;
        }
    }
    namespace
    {

        typedef hlqbvh_mesh_accelerator::PCFACE PCFACE;
        typedef const PCFACE* face_iter;
        typedef const uint32_t* code_iter;
        typedef mesh_accelerator_property PROP;

        void get_minmax(vector3& pmin, vector3& pmax, PCFACE face)
        {
            const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);

            vector3 points[3];
            points[0] = *(face->p_p0);
            points[1] = *(face->p_p1);
            points[2] = *(face->p_p2);

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (points[i][j] < min[j]) min[j] = points[i][j];
                    if (points[i][j] > max[j]) max[j] = points[i][j];
                }
            }

            pmin = min;
            pmax = max;
        }
        void get_minmax(vector3f& pmin, vector3f& pmax, PCFACE face)
        {
            const float FAR = std::numeric_limits<float>::max();
            vector3f min = vector3f(+FAR, +FAR, +FAR);
            vector3f max = vector3f(-FAR, -FAR, -FAR);

            vector3f points[3];
            points[0] = vector3f(*(face->p_p0));
            points[1] = vector3f(*(face->p_p1));
            points[2] = vector3f(*(face->p_p2));

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (points[i][j] < min[j]) min[j] = points[i][j];
                    if (points[i][j] > max[j]) max[j] = points[i][j];
                }
            }

            pmin = min;
            pmax = max;
        }

        template <class ITER>
        void get_minmax(vector3& pmin, vector3& pmax, ITER a, ITER b)
        {
            static const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            for (ITER i = a; i != b; i++)
            {
                vector3 cmin;
                vector3 cmax;
                get_minmax(cmin, cmax, *i);
                for (int j = 0; j < 3; j++)
                {
                    if (cmin[j] < min[j]) min[j] = cmin[j];
                    if (cmax[j] > max[j]) max[j] = cmax[j];
                }
            }
            pmin = min;
            pmax = max;
        }

        static void get_minmax(vector3f& pmin, vector3f& pmax, const PCFACE* faces, size_t a, size_t b)
        {
            static const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            for (size_t i = a; i != b; i++)
            {
                PCFACE face = faces[i];
                vector3 cmin;
                vector3 cmax;
                get_minmax(cmin, cmax, face);
                for (int j = 0; j < 3; j++)
                {
                    if (cmin[j] < min[j]) min[j] = cmin[j];
                    if (cmax[j] > max[j]) max[j] = cmax[j];
                }
            }
            pmin = min;
            pmax = max;
        }

        static inline uint32_t partby2(uint32_t n)
        {
            n = (n ^ (n << 16)) & 0xff0000ff;
            n = (n ^ (n << 8)) & 0x0300f00f;
            n = (n ^ (n << 4)) & 0x030c30c3;
            n = (n ^ (n << 2)) & 0x09249249;
            return n;
        }

        static inline uint32_t get_morton_code(uint32_t x, uint32_t y, uint32_t z)
        {
            return (partby2(x) << 2) | (partby2(y) << 1) | (partby2(z));
        }

        static inline uint32_t get_morton_code(const vector3& p, const vector3& min, const vector3& max)
        {
            uint32_t ix = (uint32_t)(DIV_NUM * ((p[0] - min[0]) / (max[0] - min[0])));
            uint32_t iy = (uint32_t)(DIV_NUM * ((p[1] - min[1]) / (max[1] - min[1])));
            uint32_t iz = (uint32_t)(DIV_NUM * ((p[2] - min[2]) / (max[2] - min[2])));
            return get_morton_code(ix, iy, iz);
        }

        static inline vector3 get_center(PCFACE face)
        {
            return (*(face->p_p0) + *(face->p_p1) + *(face->p_p2)) * (real(1) / 3);
        }

        static inline size_t get_dif_position_mask(code_iter ca, code_iter cb, int nMask)
        {
            size_t sz = cb - ca;
            if (sz < 16)
            {
                int nRet = 0;
                for (code_iter i = ca; i != cb; i++)
                {
                    if (nMask & *i) return nRet;
                    nRet++;
                }
                return nRet;
            }
            else
            {
                size_t msz = sz >> 1;
                code_iter cc = ca + msz;
                if (nMask & *cc)
                { //1
                    return get_dif_position_mask(ca, cc + 1, nMask);
                }
                else
                {
                    return msz + 1 + get_dif_position_mask(cc + 1, cb, nMask);
                }
            }
        }

        static size_t get_dif_position(code_iter ca, code_iter cb, int level)
        {
            int p = 3 * DIV_BIT - 1 - level;

            uint32_t nMask = 1 << p;
            return get_dif_position_mask(ca, cb, nMask);
        }

        struct code_sorter
        {
            code_sorter(const std::vector<uint32_t>& codes) : codes_(codes) {}
            bool operator()(size_t a, size_t b) const
            {
                return codes_[a] < codes_[b];
            }
            const std::vector<uint32_t>& codes_;
        };
    }

    typedef std::vector<size_t>::iterator indices_iter;

    class hlqbvh_mesh_accelerator_imp
    {
    public:
        hlqbvh_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
        ~hlqbvh_mesh_accelerator_imp();
        bool test(const ray& r, real tmin, real tmax) const;
        bool test(test_info* info, const ray& r, real tmin, real tmax) const;
        vector3 min() const;
        vector3 max() const;
        size_t memory_size() const;

        void add(PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);

    protected:
        bool test_faces(size_t nFirst, const ray& r, real dist) const;
        bool test_faces(size_t nFirst, test_info* info, const ray& r, real dist) const;
        bool test_inner(const ray& r, real tmin, real tmax) const;
        bool test_inner(test_info* info, const ray& r, real tmin, real tmax) const;
        void construct(
            const PCFACE* faces,
            const uint32_t* codes,
            size_t a,
            size_t b);

    private:
        vector3 min_;
        vector3 max_;

        int nTypeFace_;
        std::vector<PCFACE> faces_;
        aligned_vector<SIMDBVHNode> nodes_;
    };

    hlqbvh_mesh_accelerator_imp::hlqbvh_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
        : min_(min), max_(max)
    {
        this->add(begin, end, prop);
    }
    hlqbvh_mesh_accelerator_imp::~hlqbvh_mesh_accelerator_imp()
    {
        ; //
    }

    bool hlqbvh_mesh_accelerator_imp::test(const ray& r, real tmin, real tmax) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            return test_inner(r, tmin, tmax);
        }
        else
        {
            return false;
        }
    }

    bool hlqbvh_mesh_accelerator_imp::test(test_info* info, const ray& r, real tmin, real tmax) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            return test_inner(info, r, tmin, tmax);
        }
        else
        {
            return false;
        }
    }

    vector3 hlqbvh_mesh_accelerator_imp::min() const
    {
        return min_;
    }

    vector3 hlqbvh_mesh_accelerator_imp::max() const
    {
        return max_;
    }

    size_t hlqbvh_mesh_accelerator_imp::memory_size() const
    {
        return sizeof(hlqbvh_mesh_accelerator_imp) + sizeof(SIMDBVHNode) * nodes_.capacity() + sizeof(PCFACE) * faces_.capacity();
    }

    bool hlqbvh_mesh_accelerator_imp::test_faces(size_t nFirst, const ray& r, real dist) const
    {
        const PCFACE* faces = &faces_[0];
        if (nTypeFace_ == 0)
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (faces[i]->test_DS(r, dist)) return true;
                i++;
            }
        }
        else
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (faces[i]->test_SS(r, dist)) return true;
                i++;
            }
        }
        return false;
    }

    bool hlqbvh_mesh_accelerator_imp::test_faces(size_t nFirst, test_info* info, const ray& r, real dist) const
    {
        const PCFACE* faces = &faces_[0];
        bool bRet = false;
        if (nTypeFace_ == 0)
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (faces[i]->test_DS(info, r, dist))
                {
                    dist = info->distance;
                    bRet = true;
                }
                i++;
            }
        }
        else
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (faces[i]->test_SS(info, r, dist))
                {
                    dist = info->distance;
                    bRet = true;
                }
                i++;
            }
        }
        return bRet;
    }

    //visit order table
    //8*16 = 128
    static const int OrderTable[] = {
        0x44444, 0x44444, 0x44444, 0x44444, 0x44444, 0x44444, 0x44444, 0x44444,
        0x44440, 0x44440, 0x44440, 0x44440, 0x44440, 0x44440, 0x44440, 0x44440,
        0x44441, 0x44441, 0x44441, 0x44441, 0x44441, 0x44441, 0x44441, 0x44441,
        0x44401, 0x44401, 0x44410, 0x44410, 0x44401, 0x44401, 0x44410, 0x44410,
        0x44442, 0x44442, 0x44442, 0x44442, 0x44442, 0x44442, 0x44442, 0x44442,
        0x44402, 0x44402, 0x44402, 0x44402, 0x44420, 0x44420, 0x44420, 0x44420,
        0x44412, 0x44412, 0x44412, 0x44412, 0x44421, 0x44421, 0x44421, 0x44421,
        0x44012, 0x44012, 0x44102, 0x44102, 0x44201, 0x44201, 0x44210, 0x44210,
        0x44443, 0x44443, 0x44443, 0x44443, 0x44443, 0x44443, 0x44443, 0x44443,
        0x44403, 0x44403, 0x44403, 0x44403, 0x44430, 0x44430, 0x44430, 0x44430,
        0x44413, 0x44413, 0x44413, 0x44413, 0x44431, 0x44431, 0x44431, 0x44431,
        0x44013, 0x44013, 0x44103, 0x44103, 0x44301, 0x44301, 0x44310, 0x44310,
        0x44423, 0x44432, 0x44423, 0x44432, 0x44423, 0x44432, 0x44423, 0x44432,
        0x44023, 0x44032, 0x44023, 0x44032, 0x44230, 0x44320, 0x44230, 0x44320,
        0x44123, 0x44132, 0x44123, 0x44132, 0x44231, 0x44321, 0x44231, 0x44321,
        0x40123, 0x40132, 0x41023, 0x41032, 0x42301, 0x43201, 0x42310, 0x43210,
    };

    static float safe_convert(double x)
    {
        static const double FMAX = +(double)std::numeric_limits<float>::max();
        static const double FMIN = -(double)std::numeric_limits<float>::max();
        if (FMAX < x) return (float)FMAX;
        if (x < FMIN) return (float)FMIN;
        return (float)x;
    }

    static inline float safe_convert(float x)
    {
        return x;
    }

    static inline vector3f safe_convert(const vector3& v)
    {
        return vector3f(safe_convert(v[0]), safe_convert(v[1]), safe_convert(v[2]));
    }

    bool hlqbvh_mesh_accelerator_imp::test_inner(const ray& r, real tmin, real tmax) const
    {
        if (nodes_.empty()) return false;

        __m128 sseOrg[3];
        __m128 sseiDir[3];
        int sign[3];
        __m128 sseTMin;
        __m128 sseTMax;

        sseOrg[0] = _mm_set1_ps(safe_convert(r.origin()[0]));
        sseOrg[1] = _mm_set1_ps(safe_convert(r.origin()[1]));
        sseOrg[2] = _mm_set1_ps(safe_convert(r.origin()[2]));

        sseiDir[0] = _mm_set1_ps(safe_convert(r.inversed_direction()[0]));
        sseiDir[1] = _mm_set1_ps(safe_convert(r.inversed_direction()[1]));
        sseiDir[2] = _mm_set1_ps(safe_convert(r.inversed_direction()[2]));

        int phase = r.phase();
        sign[0] = (phase >> 0) & 1;
        sign[1] = (phase >> 1) & 1;
        sign[2] = (phase >> 2) & 1;

        sseTMin = _mm_set1_ps(safe_convert(tmin));
        sseTMax = _mm_set1_ps(safe_convert(tmax));

        const SIMDBVHNode* nodes = &nodes_[0];

        int todoNode = 0;
        size_t nodeStack[STACK_SIZE];
        nodeStack[0] = 0;

        //bool bRet = false;
        while (todoNode >= 0)
        {
            size_t idx = nodeStack[todoNode];

            if (IsBranch(idx))
            {
                todoNode--; //pop stack
                const SIMDBVHNode& node = nodes[idx];

                int HitMask = test_AABB(node.bboxes, sseOrg, sseiDir, sign, sseTMin, sseTMax);

                if (HitMask)
                {
                    int NodeIdx = (sign[node.axis_top] << 2) | (sign[node.axis_left] << 1) | (sign[node.axis_right]);
                    int Order = OrderTable[HitMask * 8 + NodeIdx];

                    while (!(Order & 0x4))
                    {
                        todoNode++;
                        nodeStack[todoNode] = node.children[Order & 0x3];
                        Order >>= 4;
                    }
                }
            }
            else
            { // if(IsLeaf(idx))
                todoNode--;
                if (IsEmpty(idx)) continue;

                size_t fi = GetFaceFirst(idx); //convert face index.

                if (this->test_faces(fi, r, tmax))
                {
                    return true; //true
                }
            }

            assert(todoNode < STACK_SIZE);
        }

        return false;
    }

    bool hlqbvh_mesh_accelerator_imp::test_inner(test_info* info, const ray& r, real tmin, real tmax) const
    {
        if (nodes_.empty()) return false;

        __m128 sseOrg[3];
        __m128 sseiDir[3];
        int sign[3];
        __m128 sseTMin;
        __m128 sseTMax;

        sseOrg[0] = _mm_set1_ps(safe_convert(r.origin()[0]));
        sseOrg[1] = _mm_set1_ps(safe_convert(r.origin()[1]));
        sseOrg[2] = _mm_set1_ps(safe_convert(r.origin()[2]));

        sseiDir[0] = _mm_set1_ps(safe_convert(r.inversed_direction()[0]));
        sseiDir[1] = _mm_set1_ps(safe_convert(r.inversed_direction()[1]));
        sseiDir[2] = _mm_set1_ps(safe_convert(r.inversed_direction()[2]));

        int phase = r.phase();
        sign[0] = (phase >> 0) & 1;
        sign[1] = (phase >> 1) & 1;
        sign[2] = (phase >> 2) & 1;

        sseTMin = _mm_set1_ps(safe_convert(tmin));
        sseTMax = _mm_set1_ps(safe_convert(tmax));

        const SIMDBVHNode* nodes = &nodes_[0];

        int todoNode = 0;
        size_t nodeStack[STACK_SIZE];
        nodeStack[0] = 0;

        bool bRet = false;
        while (todoNode >= 0)
        {
            size_t idx = nodeStack[todoNode];

            if (IsBranch(idx))
            {
                todoNode--; //pop stack
                const SIMDBVHNode& node = nodes[idx];

                int HitMask = test_AABB(node.bboxes, sseOrg, sseiDir, sign, sseTMin, sseTMax);

                if (HitMask)
                {
                    int NodeIdx = (sign[node.axis_top] << 2) | (sign[node.axis_left] << 1) | (sign[node.axis_right]);
                    int Order = OrderTable[HitMask * 8 + NodeIdx];

                    while (!(Order & 0x4))
                    {
                        todoNode++;
                        nodeStack[todoNode] = node.children[Order & 0x3];
                        Order >>= 4;
                    }
                }
            }
            else
            { // if(IsLeaf(idx))
                todoNode--;
                if (IsEmpty(idx)) continue;

                size_t fi = GetFaceFirst(idx); //convert face index.

                if (this->test_faces(fi, info, r, tmax))
                {
                    bRet = true;
                    tmax = info->distance;
                    sseTMax = _mm_set1_ps(safe_convert(tmax));
                }
            }

            assert(todoNode < STACK_SIZE);
        }

        return bRet;
    }

    static void get_run_heads(std::vector<size_t>& run_heads, const std::vector<uint32_t>& codes, int d)
    {
        size_t sz = codes.size();
        uint32_t cr = codes[0] >> d;

        run_heads.push_back(0);
        for (size_t i = 1; i < sz; i++)
        {
            uint32_t cl = codes[i] >> d;
            if (cr != cl)
            {
                run_heads.push_back(i);
                cr = cl;
            }
        }
    }

    static void sort_index_with_code(indices_iter fa, indices_iter fb, const std::vector<uint32_t>& codes, int bits)
    {
#if 0
		std::sort(fa, fb, code_sorter(codes));
#else
        //RADIX SORT
        std::vector<size_t> buckets[256]; //1 2 4 8 16
        std::vector<size_t> tmp(fa, fb);
        size_t sz = tmp.size();
        for (int b = 0; b < bits; b += 8)
        {
            for (size_t i = 0; i < sz; i++)
            {
                int n = (codes[tmp[i]] >> b) & 0xFF;
                buckets[n].push_back(tmp[i]);
            }
            size_t t = 0;
            for (int i = 0; i < 256; i++)
            {
                size_t bksz = buckets[i].size();
                if (bksz)
                {
                    memcpy(&tmp[t], &(buckets[i][0]), sizeof(size_t) * bksz);
                    t += bksz;
                    buckets[i].clear();
                }
            }
        }
        std::copy(tmp.begin(), tmp.end(), fa);
#endif
    }

    static void sort_index_with_code2(indices_iter fa, indices_iter fb, const std::vector<uint32_t>& codes, int bits)
    {
        std::sort(fa, fb, code_sorter(codes));
    }

    static void face_sort(std::vector<PCFACE>& faces, std::vector<uint32_t>& codes)
    {
        timer t;
        t.start();
        int n = DIV_BIT; //10
        int m = VOX_M;   //6
        size_t sz = faces.size();

        int d = 3 * (n - m);
        std::vector<size_t> run_heads;

        t.start();
        get_run_heads(run_heads, codes, d);
        t.end();
        print_log("get_run_heads:%d ms\n", t.msec());
        //--------------------------------------------------------------
        t.start();
        size_t M = run_heads.size();
        std::vector<uint32_t> run_codes(M);
        std::vector<size_t> run_indices(M);
        for (size_t i = 0; i < M; i++)
        {
            run_codes[i] = codes[run_heads[i]] >> d;
            run_indices[i] = i;
        }
        t.end();
        print_log("get_run_indices:%d ms\n", t.msec());
        //--------------------------------------------------------------
        t.start();
        sort_index_with_code(run_indices.begin(), run_indices.end(), run_codes, 3 * m);
        t.end();
        print_log("top level sort:%d ms\n", t.msec());
        //--------------------------------------------------------------
        std::vector<size_t> run_lengthes(M);
        for (size_t i = 0; i < M; i++)
        {
            if (run_indices[i] == M - 1)
            {
                run_lengthes[i] = sz - run_heads[run_indices[i]];
            }
            else
            {
                run_lengthes[i] = run_heads[run_indices[i] + 1] - run_heads[run_indices[i]];
            }
        }
        //--------------------------------------------------------------
        t.start();
        std::vector<size_t> out_indices;
        out_indices.reserve(sz);
        std::vector<size_t> tmp_indices;
        //#pragma omp parallel for
        for (size_t i = 0; i < M; i++)
        {
            if (run_lengthes[i] >= 2)
            {
                tmp_indices.clear();
                tmp_indices.reserve(run_lengthes[i]);
                size_t index = run_indices[i];
                size_t offset = run_heads[index];
                size_t length = run_lengthes[i];
                for (size_t j = 0; j < length; j++)
                {
                    tmp_indices.push_back(offset + j);
                }

                sort_index_with_code2(tmp_indices.begin(), tmp_indices.end(), codes, d);

                for (size_t j = 0; j < length; j++)
                {
                    out_indices.push_back(tmp_indices[j]);
                }
            }
            else
            {
                out_indices.push_back(run_heads[run_indices[i]]);
            }
        }
        t.end();
        print_log("voxel level sort:%d ms\n", t.msec());
        //--------------------------------------------------------------

        t.start();
        std::vector<PCFACE> out_faces(sz);
        std::vector<uint32_t> out_codes(sz);
        for (size_t i = 0; i < sz; i++)
        {
            out_faces[i] = faces[out_indices[i]];
            out_codes[i] = codes[out_indices[i]];
        }

        faces.swap(out_faces);
        codes.swap(out_codes);

        t.end();
        print_log("copy faces & codes:%d ms\n", t.msec());
        //--------------------------------------------------------------
    }

    static inline size_t get_branch_node_size(size_t face_num)
    {
        if (face_num <= MIN_FACE) return 1;
        size_t p = face_num / 4;
        if (face_num & 3) p++;
        return std::max<size_t>(1, 1 + 4 * get_branch_node_size(p));
    }
    static inline size_t get_leaf_node_size(size_t face_num)
    {
        return std::max<size_t>(MIN_FACE, face_num + (int)ceil(double(face_num) / (MIN_FACE)));
    }

    static size_t construct_deep(
        std::vector<PCFACE>& out_faces,
        aligned_vector<SIMDBVHNode>& out_nodes,
        vector3f& min, vector3f& max,
        const PCFACE* faces,
        const uint32_t* codes,
        size_t a,
        size_t b,
        int level)
    {
        static const float EPS = std::numeric_limits<float>::epsilon() * 1024;
        static const float FAR = std::numeric_limits<float>::max();

        size_t sz = b - a;
        if (sz == 0) return EMPTY_MASK;
        if (sz <= MIN_FACE || level >= DIV_BIT * 3)
        {
            const PCFACE* fa = faces + a;
            size_t first = out_faces.size();
            size_t last = first + sz + 1; //zero terminate
            out_faces.resize(last);
            memcpy(&out_faces[first], fa, sz * sizeof(PCFACE));
            out_faces[last - 1] = 0;
            get_minmax(min, max, faces, a, b);

            min -= vector3f(EPS, EPS, EPS);
            max += vector3f(EPS, EPS, EPS);

            size_t nRet = MakeLeafIndex(first);
            assert(IsLeaf(nRet));
            return nRet;
        }
        else
        {
            const uint32_t* ca = codes + a;
            const uint32_t* cb = codes + b;

            size_t csz = get_dif_position(ca, cb, level);
            size_t lsz = get_dif_position(ca, ca + csz, level + 1);
            size_t rsz = get_dif_position(ca + csz, cb, level + 1);

            size_t offset = out_nodes.size();

            size_t c = a + csz;
            size_t l = a + lsz;
            size_t r = c + rsz;

            SIMDBVHNode node;
            node.axis_top = level % 3;
            node.axis_left = node.axis_right = (level + 1) % 3;
            out_nodes.push_back(node);

            vector3f minmax[4][2];
            for (int i = 0; i < 4; i++)
            {
                minmax[i][0] = vector3f(+FAR, +FAR, +FAR);
                minmax[i][1] = vector3f(-FAR, -FAR, -FAR);
            }

            size_t ranges[4][2];
            ranges[0][0] = a;
            ranges[0][1] = l;
            ranges[1][0] = l;
            ranges[1][1] = c;
            ranges[2][0] = c;
            ranges[2][1] = r;
            ranges[3][0] = r;
            ranges[3][1] = b;

            for (int i = 0; i < 4; i++)
            {
                volatile size_t index = construct_deep(out_faces, out_nodes, minmax[i][0], minmax[i][1], faces, codes, ranges[i][0], ranges[i][1], level + 2);
                out_nodes[offset].children[i] = index;
            }

            //convert & swizzle
            float bboxes[2][3][4];
            //for(int m = 0;m<2;m++){//minmax
            for (int j = 0; j < 3; j++)
            { //xyz
                for (int k = 0; k < 4; k++)
                {                                             //box
                    bboxes[0][j][k] = minmax[k][0][j] - FEPS; //
                    bboxes[1][j][k] = minmax[k][1][j] + FEPS; //
                }
            }
            //}

            //for(int i = 0;i<4;i++){
            for (int m = 0; m < 2; m++)
            { //minmax
                for (int j = 0; j < 3; j++)
                { //xyz
                    out_nodes[offset].bboxes[m][j] = _mm_setzero_ps();
                    out_nodes[offset].bboxes[m][j] = _mm_loadu_ps(bboxes[m][j]);
                }
            }
            //}

            min = minmax[0][0];
            max = minmax[0][1];
            for (int i = 1; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                { //xyz
                    if (min[j] > minmax[i][0][j]) min[j] = minmax[i][0][j];
                    if (max[j] < minmax[i][1][j]) max[j] = minmax[i][1][j];
                }
            }

            min -= vector3f(EPS, EPS, EPS);
            max += vector3f(EPS, EPS, EPS);

            return offset;
        }
    }

    void hlqbvh_mesh_accelerator_imp::construct(
        const PCFACE* faces,
        const uint32_t* codes,
        size_t a,
        size_t b)
    {
        static const float FAR = std::numeric_limits<float>::max();
        static const int level = 0;

        size_t sz = b - a;
        if (sz == 0) return;

        const uint32_t* ca = codes + a;
        const uint32_t* cb = codes + b;

        size_t csz = get_dif_position(ca, cb, level);
        size_t lsz = get_dif_position(ca, ca + csz, level + 1);
        size_t rsz = get_dif_position(ca + csz, cb, level + 1);

        size_t offset = nodes_.size();

        size_t c = a + csz;
        size_t l = a + lsz;
        size_t r = c + rsz;

        SIMDBVHNode node;
        node.axis_top = level % 3;
        node.axis_left = node.axis_right = (level + 1) % 3;
        nodes_.push_back(node);

        vector3f minmax[4][2];
        for (int i = 0; i < 4; i++)
        {
            minmax[i][0] = vector3f(+FAR, +FAR, +FAR);
            minmax[i][1] = vector3f(-FAR, -FAR, -FAR);
        }

        size_t ranges[4][2];
        ranges[0][0] = a;
        ranges[0][1] = l;
        ranges[1][0] = l;
        ranges[1][1] = c;
        ranges[2][0] = c;
        ranges[2][1] = r;
        ranges[3][0] = r;
        ranges[3][1] = b;

        {
            size_t sub_indices[4];
            std::vector<PCFACE> sub_faces[4];
            aligned_vector<SIMDBVHNode> sub_nodes[4];
            for (int i = 0; i < 4; i++)
            {
                size_t fsz = ranges[i][1] - ranges[i][0];
                sub_faces[i].reserve(get_leaf_node_size(fsz));
                sub_nodes[i].reserve(get_branch_node_size(fsz));
            }

#pragma omp parallel for
            for (int i = 0; i < 4; i++)
            {
                sub_indices[i] = construct_deep(sub_faces[i], sub_nodes[i], minmax[i][0], minmax[i][1], faces, codes, ranges[i][0], ranges[i][1], level + 2);
            }

            size_t face_offsets[4 + 1];
            size_t node_offsets[4 + 1];
            face_offsets[0] = 0;
            node_offsets[0] = 1;
            for (int i = 0; i < 4; i++)
            {
                face_offsets[i + 1] = sub_faces[i].size();
                node_offsets[i + 1] = sub_nodes[i].size();
            }
            for (int i = 0; i < 4; i++)
            {
                face_offsets[i + 1] += face_offsets[i];
                node_offsets[i + 1] += node_offsets[i];
            }

            for (int k = 0; k < 4; k++)
            {
                size_t idx = sub_indices[k];
                if (IsBranch(idx))
                { //
                    idx += node_offsets[k];
                    sub_indices[k] = idx;
                }
                else if (!IsEmpty(idx))
                { //
                    idx = GetFaceFirst(idx);
                    idx += face_offsets[k];
                    sub_indices[k] = MakeLeafIndex(idx);
                }
                nodes_[offset].children[k] = sub_indices[k];
            }

            faces_.resize(face_offsets[4]);
            nodes_.resize(node_offsets[4]);

#pragma omp parallel for
            for (int i = 0; i < 4; i++)
            {
                aligned_vector<SIMDBVHNode>& nodes = sub_nodes[i];
                size_t jsz = nodes.size();
                for (size_t j = 0; j < jsz; j++)
                {
                    SIMDBVHNode& nd = nodes[j];
                    for (int k = 0; k < 4; k++)
                    {
                        size_t idx = nd.children[k];
                        if (IsBranch(idx))
                        { //
                            idx += node_offsets[i];
                            nd.children[k] = idx;
                        }
                        else if (!IsEmpty(idx))
                        { //
                            idx = GetFaceFirst(idx);
                            idx += face_offsets[i];
                            nd.children[k] = MakeLeafIndex(idx);
                        }
                    }
                }

                memcpy(&faces_[face_offsets[i]], &(sub_faces[i][0]), sizeof(PCFACE) * sub_faces[i].size());
                memcpy(&nodes_[node_offsets[i]], &(sub_nodes[i][0]), sizeof(SIMDBVHNode) * sub_nodes[i].size());
            }
        }

        //convert & swizzle
        float bboxes[2][3][4];
        //for(int m = 0;m<2;m++){//minmax
        for (int j = 0; j < 3; j++)
        { //xyz
            for (int k = 0; k < 4; k++)
            {                                             //box
                bboxes[0][j][k] = minmax[k][0][j] - FEPS; //
                bboxes[1][j][k] = minmax[k][1][j] + FEPS; //
            }
        }
        //}

        //for(int i = 0;i<4;i++){
        for (int m = 0; m < 2; m++)
        { //minmax
            for (int j = 0; j < 3; j++)
            { //xyz
                nodes_[offset].bboxes[m][j] = _mm_setzero_ps();
                nodes_[offset].bboxes[m][j] = _mm_loadu_ps(bboxes[m][j]);
            }
        }
        //}
    }

    void hlqbvh_mesh_accelerator_imp::add(PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        if (prop.singleside)
        {
            this->nTypeFace_ = 1;
        }
        else
        {
            this->nTypeFace_ = 0;
        }

        timer t;
        std::vector<PCFACE> faces(begin, end);
        size_t sz = faces.size();
        std::vector<uint32_t> codes(sz);
        {
            vector3 tmin, tmax;
            get_morton_bound(tmin, tmax, min_, max_);
            t.start();
            create_morton_code(codes, faces, tmin, tmax);
            t.end();
            print_log("create morton code:%d ms\n", t.msec());
        }

        face_sort(faces, codes);
        t.start();
        construct(&faces[0], &codes[0], 0, sz);
        t.end();
        print_log("construct BVH tree:%d ms\n", t.msec());
    }

    //---------------------------------------------------------------
    //---------------------------------------------------------------

    hlqbvh_mesh_accelerator::hlqbvh_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        imp_ = new hlqbvh_mesh_accelerator_imp(min, max, begin, end, prop);
    }

    hlqbvh_mesh_accelerator::~hlqbvh_mesh_accelerator()
    {
        delete imp_;
    }

    bool hlqbvh_mesh_accelerator::test(const ray& r, real dist) const
    {
        return imp_->test(r, 0, dist);
    }

    bool hlqbvh_mesh_accelerator::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, 0, dist);
    }

    vector3 hlqbvh_mesh_accelerator::min() const
    {
        return imp_->min();
    }

    vector3 hlqbvh_mesh_accelerator::max() const
    {
        return imp_->max();
    }
    size_t hlqbvh_mesh_accelerator::memory_size() const
    {
        return imp_->memory_size();
    }
}
