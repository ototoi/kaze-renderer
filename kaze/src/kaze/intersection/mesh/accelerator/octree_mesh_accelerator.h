#ifndef KAZE_OCTREE_MESH_ACCELERATOR_H
#define KAZE_OCTREE_MESH_ACCELERATOR_H

#include "mesh_accelerator.h"

namespace kaze
{

    class octree_mesh_accelerator_imp;

    class octree_mesh_accelerator : public mesh_accelerator
    {
    public:
        typedef const mesh_face* PCFACE;

    public:
        octree_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
        ~octree_mesh_accelerator();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;
        size_t memory_size() const;

    private:
        octree_mesh_accelerator& operator=(const octree_mesh_accelerator& rhs);

    private:
        octree_mesh_accelerator_imp* imp_;
    };
}

#endif
