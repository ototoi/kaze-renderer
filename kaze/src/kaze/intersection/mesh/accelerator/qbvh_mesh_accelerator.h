#ifndef KAZE_QBVH_MESH_ACCELERATOR_H
#define KAZE_QBVH_MESH_ACCELERATOR_H

#include "mesh_accelerator.h"

namespace kaze
{

    class qbvh_mesh_accelerator_imp;

    class qbvh_mesh_accelerator : public mesh_accelerator
    {
    public:
        typedef const mesh_face* PCFACE;

    public:
        qbvh_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
        ~qbvh_mesh_accelerator();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;
        size_t memory_size() const;

    private:
        qbvh_mesh_accelerator& operator=(const qbvh_mesh_accelerator& rhs);

    private:
        qbvh_mesh_accelerator_imp* imp_;
    };

    class simd_qbvh_mesh_accelerator : public qbvh_mesh_accelerator
    {
    public:
        simd_qbvh_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
    };
    class sisd_qbvh_mesh_accelerator : public qbvh_mesh_accelerator
    {
    public:
        sisd_qbvh_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
    };
}
#endif
