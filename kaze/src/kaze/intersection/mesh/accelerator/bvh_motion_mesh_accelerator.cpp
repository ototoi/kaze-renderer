#include "bvh_motion_mesh_accelerator.h"

#include "intersection_ex.h"
#include "test_info.h"
#include "logger.h"

#include "values.h"

#include <memory>
#include <cstring>
#include <cmath>
#include <functional>
#include <algorithm>
#include <vector>
#include <utility>

#define MIN_FACE 16
#define LEAF_TEST_MINMAX 1

namespace kaze
{

    typedef bvh_motion_mesh_accelerator::PCFACE PCFACE;

    using namespace std;

    namespace
    {

        class face_sorter : public std::binary_function<PCFACE, PCFACE, bool>
        {
        public:
            face_sorter(int plane) : plane_(plane) {}

            bool operator()(PCFACE a, PCFACE b) const
            {
                vector3 ax = a->min() + a->max();
                vector3 bx = b->min() + b->max();
                return ax[plane_] < bx[plane_];
            }

        private:
            int plane_;
        };

        class pred_L : public std::unary_function<PCFACE, bool>
        {
        public:
            pred_L(int axis, real pos) : axis_(axis), pos_(pos) {}

            bool operator()(PCFACE p_face) const
            {
                int axis = axis_;
                real pos = pos_;

                vector3 center = (p_face->min() + p_face->max()) * 0.5;

                return (center[axis] < pos);
            }

        private:
            int axis_;
            real pos_;
        };

        class bvh_node
        {
        public:
            virtual ~bvh_node() {}
        public:
            virtual bool test(const ray& r, real tmin, real tmax) const = 0;
            virtual bool test(test_info* info, const ray& r, real tmin, real tmax) const = 0;
#if LEAF_TEST_MINMAX
            virtual const vector3& min() const = 0;
            virtual const vector3& max() const = 0;
#endif
            virtual size_t memory_size() const = 0;
        };

        class bvh_node_branch : public bvh_node
        {
        public:
            bvh_node_branch(PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
            ~bvh_node_branch();
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;
            //#if LEAF_TEST_MINMAX
            const vector3& min() const;
            const vector3& max() const;
            //#endif
            size_t memory_size() const;

        protected:
            bool test_internal(const ray& r, real tmin, real tmax) const;
            bool test_internal(test_info* info, const ray& r, real tmin, real tmax) const;

        private:
            bvh_node* nodes_[2];

            int plane_;

            vector3 min_;
            vector3 max_;
        };

        class bvh_node_leaf_SS : public bvh_node
        {
        public:
            bvh_node_leaf_SS(PCFACE* begin, PCFACE* end);

        public:
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;
#if LEAF_TEST_MINMAX
            const vector3& min() const;
            const vector3& max() const;
#endif
            size_t memory_size() const;

        private:
            std::vector<PCFACE> mv_;
#if LEAF_TEST_MINMAX
            vector3 min_;
            vector3 max_;
#endif
        };

        class bvh_node_leaf_DS : public bvh_node
        {
        public:
            bvh_node_leaf_DS(PCFACE* begin, PCFACE* end);

        public:
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;
#if LEAF_TEST_MINMAX
            const vector3& min() const;
            const vector3& max() const;
#endif
            size_t memory_size() const;

        private:
            std::vector<PCFACE> mv_;
#if LEAF_TEST_MINMAX
            vector3 min_;
            vector3 max_;
#endif
        };

        //---------------------------------------------------------------------------------------------

        //#region

        inline int eval_phase(int phase, int axis)
        { //rid<0
            return (phase >> axis) & 0x1;
        }

        void get_minmax(vector3& pmin, vector3& pmax, PCFACE face)
        {
            pmin = face->min();
            pmax = face->max();
        }

        void get_minmax(vector3& pmin, vector3& pmax, PCFACE* a, PCFACE* b)
        {
            const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            for (PCFACE* i = a; i != b; i++)
            {
                vector3 cmin;
                vector3 cmax;
                get_minmax(cmin, cmax, *i);
                for (int j = 0; j < 3; j++)
                {
                    if (cmin[j] < min[j]) min[j] = cmin[j];
                    if (cmax[j] > max[j]) max[j] = cmax[j];
                }
            }
            pmin = min;
            pmax = max;
        }

        static void find_sort(PCFACE* mid, PCFACE* a, PCFACE* b, real min, real max, int plane)
        {
            if (a <= mid && mid < b)
            {
                size_t sz = b - a;
                if (sz <= 256)
                {
                    std::sort(a, b, face_sorter(plane));
                }
                else
                {
                    real pos = (min + max) * 0.5;
                    PCFACE* c = std::partition(a, b, pred_L(plane, pos));
                    if (a == c || b == c)
                    {
                        std::sort(a, b, face_sorter(plane));
                    }
                    else
                    {
                        find_sort(mid, a, c, min, pos, plane);
                        find_sort(mid, c, b, pos, max, plane);
                    }
                }
            }
        }

        bvh_node_branch::bvh_node_branch(PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
        {
            static const real EPS = values::epsilon() * 1000;
            nodes_[0] = 0;
            nodes_[1] = 0;
            plane_ = 0;
            vector3 min;
            vector3 max;
            get_minmax(min, max, begin, end);
            vector3 wid = max - min;
            int plane = 0;
            if (wid[1] > wid[plane]) plane = 1;
            if (wid[2] > wid[plane]) plane = 2;

            PCFACE* mid = begin + ((end - begin) >> 1);

            find_sort(mid, begin, end, min[plane], max[plane], plane);

            //---------------------------------------
            PCFACE* a;
            PCFACE* b;
            size_t sz;
            //---------------------------------------

            auto_ptr<bvh_node> ap_l;
            auto_ptr<bvh_node> ap_r;

            //----------------------------------
            a = begin;
            b = mid;
            sz = b - a;
            if (sz)
            {
                if (MIN_FACE < sz)
                {
                    ap_l.reset(new bvh_node_branch(a, b, prop));
                }
                else
                {
                    if (prop.singleside)
                    {
                        ap_l.reset(new bvh_node_leaf_SS(a, b));
                    }
                    else
                    {
                        ap_l.reset(new bvh_node_leaf_DS(a, b));
                    }
                }
            }
            //----------------------------------

            a = mid;
            b = end;
            sz = b - a;
            if (sz)
            {
                if (MIN_FACE < sz)
                {
                    ap_r.reset(new bvh_node_branch(a, b, prop));
                }
                else
                {
                    if (prop.singleside)
                    {
                        ap_r.reset(new bvh_node_leaf_SS(a, b));
                    }
                    else
                    {
                        ap_r.reset(new bvh_node_leaf_DS(a, b));
                    }
                }
            }

            plane_ = plane;
            min_ = min + vector3(-EPS, -EPS, -EPS);
            max_ = max + vector3(+EPS, +EPS, +EPS);
            nodes_[0] = ap_l.release();
            nodes_[1] = ap_r.release();
        }

        bvh_node_branch::~bvh_node_branch()
        {
            if (nodes_[0]) delete nodes_[0];
            if (nodes_[1]) delete nodes_[1];
        }

        bool bvh_node_branch::test(const ray& r, real tmin, real tmax) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, tmin, tmax))
            {
                tmin = std::max<real>(tmin, rng.tmin);
                tmax = std::min<real>(tmax, rng.tmax);
                return this->test_internal(r, tmin, tmax);
            }
            return false;
        }

        bool bvh_node_branch::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, tmin, tmax))
            {
                tmin = std::max<real>(tmin, rng.tmin);
                tmax = std::min<real>(tmax, rng.tmax);
                return this->test_internal(info, r, tmin, tmax);
            }
            return false;
        }

        bool bvh_node_branch::test_internal(const ray& r, real tmin, real tmax) const
        {
            int phase = r.phase();
            int plane = plane_;
            int nNear = eval_phase(phase, plane);
            int nFar = 1 - nNear;

            if (nodes_[nNear] && nodes_[nNear]->test(r, tmin, tmax)) return true;
            if (nodes_[nFar] && nodes_[nFar]->test(r, tmin, tmax)) return true;

            return false;
        }

        bool bvh_node_branch::test_internal(test_info* info, const ray& r, real tmin, real tmax) const
        {
            int phase = r.phase();
            int plane = plane_;
            int nNear = eval_phase(phase, plane);
            int nFar = 1 - nNear;

            bool bRet = false;
            if (nodes_[nNear] && nodes_[nNear]->test(info, r, tmin, tmax))
            {
                tmax = info->distance;
                bRet = true;
            }

            if (nodes_[nFar] && nodes_[nFar]->test(info, r, tmin, tmax))
            {
                tmax = info->distance;
                bRet = true;
            }

            return bRet;
        }
        //#if LEAF_TEST_MINMAX
        const vector3& bvh_node_branch::min() const { return min_; }
        const vector3& bvh_node_branch::max() const { return max_; }
        //#endif

        size_t bvh_node_branch::memory_size() const
        {
            size_t tmp = 0;
            if (nodes_[0]) tmp += nodes_[0]->memory_size();
            if (nodes_[1]) tmp += nodes_[1]->memory_size();
            tmp += sizeof(bvh_node_branch);
            return tmp;
        }

        //---------------------------------------------------------------------------------------------
        bvh_node_leaf_SS::bvh_node_leaf_SS(PCFACE* begin, PCFACE* end)
        {
#if LEAF_TEST_MINMAX
            //---------------------------------------
            const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            //---------------------------------------
            for (PCFACE* i = begin; i != end; i++)
            {
                vector3 cmin, cmax;
                get_minmax(cmin, cmax, *i);
                for (int j = 0; j < 3; j++)
                {
                    if (cmin[j] < min[j]) min[j] = cmin[j];
                    if (cmax[j] > max[j]) max[j] = cmax[j];
                }
            }
            //---------------------------------------
            min_ = min;
            max_ = max;
#endif
            mv_.assign(begin, end);
        }

        bool bvh_node_leaf_SS::test(const ray& r, real tmin, real tmax) const
        {
#if LEAF_TEST_MINMAX
            if (!test_AABB(min_, max_, r, tmax)) return false;
#endif
            size_t sz = mv_.size();
            const PCFACE* faces = &(mv_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (faces[i]->test_SS(r, tmax)) return true;
            }
            return false;
        }
        bool bvh_node_leaf_SS::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
#if LEAF_TEST_MINMAX
            if (!test_AABB(min_, max_, r, tmax)) return false;
#endif
            bool bRet = false;
            size_t sz = mv_.size();
            const PCFACE* faces = &(mv_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (faces[i]->test_SS(info, r, tmax))
                {
                    tmax = info->distance;
                    bRet = true;
                }
            }
            return bRet;
        }
#if LEAF_TEST_MINMAX
        const vector3& bvh_node_leaf_SS::min() const
        {
            return min_;
        }
        const vector3& bvh_node_leaf_SS::max() const { return max_; }
#endif

        size_t bvh_node_leaf_SS::memory_size() const
        {
            size_t tmp = 0;
            tmp += sizeof(bvh_node_leaf_SS);
            tmp += sizeof(PCFACE) * mv_.capacity();
            return tmp;
        }

        //---------------------------------------------------------------------------------------------
        bvh_node_leaf_DS::bvh_node_leaf_DS(PCFACE* begin, PCFACE* end)
        {
#if LEAF_TEST_MINMAX
            //---------------------------------------
            const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            //---------------------------------------
            for (PCFACE* i = begin; i != end; i++)
            {
                vector3 cmin, cmax;
                get_minmax(cmin, cmax, *i);
                for (int j = 0; j < 3; j++)
                {
                    if (cmin[j] < min[j]) min[j] = cmin[j];
                    if (cmax[j] > max[j]) max[j] = cmax[j];
                }
            }
            //---------------------------------------
            min_ = min;
            max_ = max;
#endif
            mv_.assign(begin, end);
        }

        bool bvh_node_leaf_DS::test(const ray& r, real tmin, real tmax) const
        {
#if LEAF_TEST_MINMAX
            if (!test_AABB(min_, max_, r, tmax)) return false;
#endif
            size_t sz = mv_.size();
            const PCFACE* faces = &(mv_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (faces[i]->test_DS(r, tmax)) return true;
            }
            return false;
        }
        bool bvh_node_leaf_DS::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
#if LEAF_TEST_MINMAX
            if (!test_AABB(min_, max_, r, tmax)) return false;
#endif
            bool bRet = false;
            size_t sz = mv_.size();
            const PCFACE* faces = &(mv_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (faces[i]->test_DS(info, r, tmax))
                {
                    tmax = info->distance;
                    bRet = true;
                }
            }
            return bRet;
        }
#if LEAF_TEST_MINMAX
        const vector3& bvh_node_leaf_DS::min() const
        {
            return min_;
        }
        const vector3& bvh_node_leaf_DS::max() const { return max_; }
#endif

        size_t bvh_node_leaf_DS::memory_size() const
        {
            size_t tmp = 0;
            tmp += sizeof(bvh_node_leaf_DS);
            tmp += sizeof(PCFACE) * mv_.capacity();
            return tmp;
        }

        //#endregion

        //---------------------------------------------------------------------------------------------
    }
    //---------------------------------------------------------------------------------------------

    class bvh_motion_mesh_accelerator_imp
    {
    public:
        bvh_motion_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
        ~bvh_motion_mesh_accelerator_imp();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;
        size_t memory_size() const { return sizeof(bvh_motion_mesh_accelerator_imp) + root_->memory_size(); }
    private:
        bvh_node_branch* root_;
    };

    bvh_motion_mesh_accelerator_imp::bvh_motion_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        root_ = new bvh_node_branch(begin, end, prop);

        //print_log("bvh_motion_mesh_accelerator:memory size:%d bytes\n", root_->memory_size());
    }

    bvh_motion_mesh_accelerator_imp::~bvh_motion_mesh_accelerator_imp()
    {
        delete root_;
    }

    bool bvh_motion_mesh_accelerator_imp::test(const ray& r, real dist) const
    {
        return root_->test(r, 0, dist);
    }

    bool bvh_motion_mesh_accelerator_imp::test(test_info* info, const ray& r, real dist) const
    {
        return root_->test(info, r, 0, dist);
    }

    vector3 bvh_motion_mesh_accelerator_imp::min() const
    {
        return root_->min();
    }

    vector3 bvh_motion_mesh_accelerator_imp::max() const
    {
        return root_->max();
    }

    //-----------------------------------------------------------------------------------------------

    bvh_motion_mesh_accelerator::bvh_motion_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        imp_ = new bvh_motion_mesh_accelerator_imp(min, max, begin, end, prop);
    }

    bvh_motion_mesh_accelerator::~bvh_motion_mesh_accelerator()
    {
        delete imp_;
    }

    bool bvh_motion_mesh_accelerator::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool bvh_motion_mesh_accelerator::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }

    vector3 bvh_motion_mesh_accelerator::min() const
    {
        return imp_->min();
    }

    vector3 bvh_motion_mesh_accelerator::max() const
    {
        return imp_->max();
    }

    size_t bvh_motion_mesh_accelerator::memory_size() const
    {
        return imp_->memory_size();
    }
}
