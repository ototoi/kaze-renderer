#include "sbvh_mesh_accelerator.h"

#include "intersection_ex.h"
#include "test_info.h"
#include "logger.h"

#include "mesh_face.h"

#include "values.h"

#include <vector>
#include <memory>
#include <cstring>
#include <cmath>
#include <functional>
#include <algorithm>
#include <utility>

#define MIN_FACE 8

#define MAX_LEVEL 24
#define MAX_S_LEVEL 18

#define MAX_BINS 8

#define SPLIT_ALPHA 1e-5
#define SAH_NODE_COST 0.4
#define SAH_TRIANGLE_COST 0.6
#define SAH_NODE_BATCH 1
#define SAH_TRIANGLE_BATCH 1

using namespace std;

namespace kaze
{
    namespace
    {
        typedef const mesh_face* PCFACE;

        struct Bound
        {
            vector3 min;
            vector3 max;
        };

        struct Reference
        {
            PCFACE pFace;
            Bound bnd;
        };

        struct Property
        {
            bool singleside;
            int min_face;
            int max_level;
            int max_s_level;
            real min_overlap;
        };

        struct ObjectSplit
        {
            real sah;
            int plane;
            size_t lsz;
            size_t rsz;
            Bound lbnd;
            Bound rbnd;
        };

        struct SpatialSplit
        {
            real sah;
            int plane;
            real pos;
        };

        struct SpatialBin
        {
            Bound bnd;
            int enter;
            int exit;
        };

        class sbvh_node
        {
        public:
            virtual ~sbvh_node() {}
            virtual bool test(const ray& r, real tmin, real tmax) const = 0;
            virtual bool test(test_info* info, const ray& r, real tmin, real tmax) const = 0;
            virtual size_t memory_size() const = 0;
        };

        class sbvh_node_branch : public sbvh_node
        {
        public:
            sbvh_node_branch(const vector3& min, const vector3& max, int plane, sbvh_node* nodes[2]);
            ~sbvh_node_branch();
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;
            size_t memory_size() const;

        private:
            sbvh_node* nodes_[2];
            int plane_;
            vector3 min_;
            vector3 max_;
        };

        class sbvh_node_leaf : public sbvh_node
        {
        public:
            sbvh_node_leaf(const vector3& min, const vector3& max, const std::vector<PCFACE>& faces);
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;
            size_t memory_size() const;

        protected:
            std::vector<PCFACE> faces_;
            vector3 min_;
            vector3 max_;
        };

        typedef sbvh_node_leaf sbvh_node_leaf_DS;

        class sbvh_node_leaf_SS : public sbvh_node_leaf
        {
        public:
            sbvh_node_leaf_SS(const vector3& min, const vector3& max, const std::vector<PCFACE>& faces);
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;
        };

        static inline int eval_phase(int phase, int axis)
        { //rid<0
            return (phase >> axis) & 0x1;
        }

        sbvh_node_branch::sbvh_node_branch(const vector3& min, const vector3& max, int plane, sbvh_node* nodes[2])
            : plane_(plane), min_(min), max_(max)
        {
            nodes_[0] = nodes[0];
            nodes_[1] = nodes[1];
        }

        sbvh_node_branch::~sbvh_node_branch()
        {
            if (nodes_[0]) delete nodes_[0];
            if (nodes_[1]) delete nodes_[1];
        }

        bool sbvh_node_branch::test(const ray& r, real tmin, real tmax) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, tmin, tmax))
            {
                tmin = std::max<real>(tmin, rng.tmin);
                tmax = std::min<real>(tmax, rng.tmax);

                int phase = r.phase();
                int plane = plane_;
                int nNear = eval_phase(phase, plane);
                int nFar = 1 - nNear;

                if (nodes_[nNear] && nodes_[nNear]->test(r, tmin, tmax)) return true;
                if (nodes_[nFar] && nodes_[nFar]->test(r, tmin, tmax)) return true;
            }
            return false;
        }

        bool sbvh_node_branch::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, tmin, tmax))
            {
                tmin = std::max<real>(tmin, rng.tmin);
                tmax = std::min<real>(tmax, rng.tmax);

                int phase = r.phase();
                int plane = plane_;
                int nNear = eval_phase(phase, plane);
                int nFar = 1 - nNear;

                bool bRet = false;
                if (nodes_[nNear] && nodes_[nNear]->test(info, r, tmin, tmax))
                {
                    tmax = info->distance;
                    bRet = true;
                }
                if (nodes_[nFar] && nodes_[nFar]->test(info, r, tmin, tmax))
                {
                    tmax = info->distance;
                    bRet = true;
                }
                return bRet;
            }
            return false;
        }

        size_t sbvh_node_branch::memory_size() const
        {
            size_t sz = sizeof(sbvh_node_branch);
            if (nodes_[0]) sz += nodes_[0]->memory_size();
            if (nodes_[1]) sz += nodes_[1]->memory_size();

            return sz;
        }

        //-----------------------------------------------------------------------------

        sbvh_node_leaf::sbvh_node_leaf(const vector3& min, const vector3& max, const std::vector<PCFACE>& faces)
            : min_(min), max_(max), faces_(faces)
        {
            ;
        }

        bool sbvh_node_leaf::test(const ray& r, real tmin, real tmax) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, tmin, tmax))
            {
                tmin = std::max<real>(tmin, rng.tmin);
                tmax = std::min<real>(tmax, rng.tmax);

                size_t sz = faces_.size();
                const PCFACE* faces = &(faces_[0]);
                for (size_t i = 0; i < sz; i++)
                {
                    if (faces[i]->test_DS(r, tmax)) return true;
                }
            }
            return false;
        }

        bool sbvh_node_leaf::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, tmin, tmax))
            {
                tmin = std::max<real>(tmin, rng.tmin);
                tmax = std::min<real>(tmax, rng.tmax);

                bool bRet = false;
                size_t sz = faces_.size();
                const PCFACE* faces = &(faces_[0]);
                for (size_t i = 0; i < sz; i++)
                {
                    if (faces[i]->test_DS(info, r, tmax))
                    {
                        tmax = info->distance;
                        bRet = true;
                    }
                }
                return bRet;
            }
            return false;
        }

        size_t sbvh_node_leaf::memory_size() const
        {
            size_t sz = sizeof(sbvh_node_leaf);
            sz += sizeof(PCFACE) * faces_.capacity();
            return sz;
        }

        sbvh_node_leaf_SS::sbvh_node_leaf_SS(const vector3& min, const vector3& max, const std::vector<PCFACE>& faces)
            : sbvh_node_leaf(min, max, faces)
        {
        }

        bool sbvh_node_leaf_SS::test(const ray& r, real tmin, real tmax) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, tmin, tmax))
            {
                tmin = std::max<real>(tmin, rng.tmin);
                tmax = std::min<real>(tmax, rng.tmax);

                size_t sz = faces_.size();
                const PCFACE* faces = &(faces_[0]);
                for (size_t i = 0; i < sz; i++)
                {
                    if (faces[i]->test_SS(r, tmax)) return true;
                }
            }
            return false;
        }
        bool sbvh_node_leaf_SS::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, tmin, tmax))
            {
                tmin = std::max<real>(tmin, rng.tmin);
                tmax = std::min<real>(tmax, rng.tmax);

                bool bRet = false;
                size_t sz = faces_.size();
                const PCFACE* faces = &(faces_[0]);
                for (size_t i = 0; i < sz; i++)
                {
                    if (faces[i]->test_SS(info, r, tmax))
                    {
                        tmax = info->distance;
                        bRet = true;
                    }
                }
                return bRet;
            }
            return false;
        }

        static void CreateFaces(std::vector<PCFACE>& faces, const std::vector<Reference>& refs)
        {
            size_t sz = refs.size();
            faces.resize(sz);
            for (size_t i = 0; i < sz; i++)
            {
                faces[i] = refs[i].pFace;
            }
            std::sort(faces.begin(), faces.end());
            faces.erase(std::unique(faces.begin(), faces.end()), faces.end());
        }

        static void GetMinMax(vector3& min, vector3& max, const vector3 points[], size_t sz = 3)
        {
            min = max = points[0];
            for (size_t i = 1; i < sz; i++)
            {
                vector3 p = points[i];
                for (int j = 0; j < 3; j++)
                {
                    if (min[j] > p[j]) min[j] = p[j];
                    if (max[j] < p[j]) max[j] = p[j];
                }
            }
        }

        static void GetMinMax(vector3& min, vector3& max, const std::vector<Reference>& refs)
        {
            min = refs[0].bnd.min;
            max = refs[0].bnd.max;
            size_t sz = refs.size();
            for (size_t i = 1; i < sz; i++)
            {
                vector3 cmin = refs[i].bnd.min;
                vector3 cmax = refs[i].bnd.max;
                for (int j = 0; j < 3; j++)
                {
                    if (min[j] > cmin[j]) min[j] = cmin[j];
                    if (max[j] < cmax[j]) max[j] = cmax[j];
                }
            }
        }

        static real GetMin(const vector3& v)
        {
            return std::min(v[0], std::min(v[1], v[2]));
        }
        static real GetMax(const vector3& v)
        {
            return std::max(v[0], std::max(v[1], v[2]));
        }
        static real GetSum(const vector3& v)
        {
            return v[0] + v[1] + v[2];
        }

        static real GetArea(const vector3& min, const vector3& max)
        {
            static const int TRAP[] = {0, 1, 2, 0, 1, 2};

            vector3 wid = max - min;
            real s = 0;
            for (int i = 0; i < 3; i++)
            {
                s += wid[TRAP[i + 1]] * wid[TRAP[i + 2]];
            }
            return 2 * s;
        }

        static real GetArea(const Bound& bnd)
        {
            return GetArea(bnd.min, bnd.max);
        }

        static size_t RoundToTriangleBatchSize(size_t n)
        {
            return ((n + SAH_TRIANGLE_BATCH - 1) / SAH_TRIANGLE_BATCH) * SAH_TRIANGLE_BATCH;
        }

        static size_t RoundToNodeBatchSize(size_t n)
        {
            return ((n + SAH_NODE_BATCH - 1) / SAH_NODE_BATCH) * SAH_NODE_BATCH;
        }

        static real GetTriangleCost(size_t n)
        {
            return RoundToTriangleBatchSize(n) * SAH_TRIANGLE_COST;
        }

        static real GetNodeCost(size_t n)
        {
            return RoundToNodeBatchSize(n) * SAH_NODE_COST;
        }

        struct ObjectSorter
        {
            ObjectSorter(int plane) : plane_(plane) {}

            bool operator()(const Reference& a, const Reference& b)
            {
                int p = plane_;
                real la = (a.bnd.min[p] + a.bnd.max[p]);
                real lb = (b.bnd.min[p] + b.bnd.max[p]);
                return (la < lb) || (la == lb && a.pFace < b.pFace);
            }

            int plane_;
        };

        static void GrowBound(Bound& bnd, const vector3& p)
        {
            for (int i = 0; i < 3; i++)
            {
                bnd.min[i] = std::min(bnd.min[i], p[i]);
                bnd.max[i] = std::max(bnd.max[i], p[i]);
            }
        };

        static void GrowBound(Bound& bnd, const Bound& p)
        {
            for (int i = 0; i < 3; i++)
            {
                bnd.min[i] = std::min(bnd.min[i], p.min[i]);
                bnd.max[i] = std::max(bnd.max[i], p.max[i]);
            }
        };

        static void IntersectBound(Bound& bnd, const Bound& p)
        {
            for (int i = 0; i < 3; i++)
            {
                bnd.min[i] = std::max(bnd.min[i], p.min[i]);
                bnd.max[i] = std::min(bnd.max[i], p.max[i]);
            }

            for (int i = 0; i < 3; i++)
            {
                if (bnd.min[i] > bnd.max[i]) bnd.max[i] = bnd.min[i];
            }
        }

        static ObjectSplit FindObjectSplit(std::vector<Reference>& refs, const Property& prop, real nodeSAH)
        {
            size_t sz = refs.size();

            ObjectSplit split;
            split.sah = std::numeric_limits<real>::max();

            // Sort along each dimension.
            for (int p = 0; p < 3; p++)
            {
                std::sort(refs.begin(), refs.end(), ObjectSorter(p));

                std::vector<Bound> rbnds(sz);

                {
                    Bound rbnd = refs[sz - 1].bnd;
                    rbnds[sz - 1] = rbnd;
                    for (size_t i = sz - 1; i > 0; i--)
                    {
                        GrowBound(rbnd, refs[i].bnd);
                        rbnds[i - 1] = rbnd;
                    }
                }

                {
                    real bestTieBreak = std::numeric_limits<real>::max();

                    Bound lbnd = refs[0].bnd;
                    for (size_t i = 1; i < sz; i++)
                    {
                        GrowBound(lbnd, refs[i - 1].bnd);

                        size_t lsz = i;
                        size_t rsz = sz - i;

                        real sah =
                            nodeSAH +
                            GetArea(lbnd) * GetTriangleCost(lsz) +
                            GetArea(rbnds[i - 1]) * GetTriangleCost(rsz);

                        real tieBreak = sqrt((real)lsz) + sqrt((real)(rsz));
                        if (sah < split.sah || (sah == split.sah && tieBreak < bestTieBreak))
                        {
                            split.sah = sah;
                            split.plane = p;
                            split.lsz = lsz;
                            split.rsz = rsz;
                            split.lbnd = lbnd;
                            split.rbnd = rbnds[i - 1];

                            bestTieBreak = tieBreak;
                        }
                    }
                }
            }
            return split;
        }

        template <class T>
        inline T clamp(T x, T a, T b)
        {
            return std::max<T>(a, std::min<T>(x, b));
        }

        inline vector3 lerp(const vector3& a, const vector3& b, real t)
        {
            return a * (1 - t) + b * (t);
        }

        static void SplitReference(Reference& left, Reference& right, const Reference& ref, int plane, real pos)
        {
            static const real FAR = std::numeric_limits<real>::max();

            PCFACE pFace = ref.pFace;

            left.pFace = right.pFace = ref.pFace;

            Bound lbnd;
            Bound rbnd;

            lbnd.min = rbnd.min = +vector3(FAR, FAR, FAR);
            lbnd.max = rbnd.max = -vector3(FAR, FAR, FAR);

            vector3 verts[3] = {*(pFace->p_p0), *(pFace->p_p1), *(pFace->p_p2)};

            const vector3* v1 = &verts[2];
            for (int i = 0; i < 3; i++)
            {
                const vector3* v0 = v1;
                v1 = &verts[i];
                real v0p = (*v0)[plane];
                real v1p = (*v1)[plane];

                // Insert vertex to the boxes it belongs to.

                if (v0p <= pos)
                {
                    GrowBound(lbnd, *v0);
                }
                if (v0p >= pos)
                {
                    GrowBound(rbnd, *v0);
                }

                // Edge intersects the plane => insert intersection to both boxes.

                if ((v0p < pos && v1p > pos) || (v0p > pos && v1p < pos))
                {
                    vector3 t = lerp(*v0, *v1, clamp((pos - v0p) / (v1p - v0p), 0.0, 1.0));
                    GrowBound(lbnd, t);
                    GrowBound(rbnd, t);
                }
            }

            // Intersect with original bounds.

            lbnd.max[plane] = pos;
            rbnd.min[plane] = pos;

            IntersectBound(lbnd, ref.bnd);
            IntersectBound(rbnd, ref.bnd);

            left.bnd = lbnd;
            right.bnd = rbnd;
        }

        static SpatialSplit FindSpatialSplit(std::vector<Reference>& refs, const vector3& min, const vector3& max, const Property& prop, real nodeSAH)
        {
            static const real FAR = std::numeric_limits<real>::max();

            size_t sz = refs.size();

            SpatialSplit split;
            split.sah = std::numeric_limits<real>::max();

            vector3 origin = min;
            vector3 binSize = (max - min) * (1.0 / MAX_BINS);
            vector3 invBinSize = vector3(1, 1, 1) / binSize;

            SpatialBin bins[3][MAX_BINS];
            for (int p = 0; p < 3; p++)
            {
                for (int i = 0; i < MAX_BINS; i++)
                {
                    SpatialBin& bin = bins[p][i];
                    bin.bnd.min = +vector3(FAR, FAR, FAR);
                    bin.bnd.max = -vector3(FAR, FAR, FAR);
                    bin.enter = 0;
                    bin.exit = 0;
                }
            }

            for (size_t idx = 0; idx < sz; idx++)
            {
                const Reference& ref = refs[idx];
                vector3 b0 = (ref.bnd.min - origin) * invBinSize;
                vector3 b1 = (ref.bnd.max - origin) * invBinSize;

                int firstBin[3] = {(int)b0[0], (int)b0[1], (int)b0[2]};
                int lastBin[3] = {(int)b1[0], (int)b1[1], (int)b1[2]};

                for (int p = 0; p < 3; p++)
                {
                    firstBin[p] = clamp(firstBin[p], 0, MAX_BINS - 1);
                    lastBin[p] = clamp(lastBin[p], 0, MAX_BINS - 1);
                }

                for (int p = 0; p < 3; p++)
                {
                    Reference currRef = ref;
                    for (int i = firstBin[p]; i < lastBin[p]; i++)
                    {
                        Reference leftRef, rightRef;
                        SplitReference(leftRef, rightRef, currRef, p, origin[p] + binSize[p] * (real)(i + 1));
                        GrowBound(bins[p][i].bnd, leftRef.bnd);
                        currRef = rightRef;
                    }
                    GrowBound(bins[p][lastBin[p]].bnd, currRef.bnd);
                    bins[p][firstBin[p]].enter++;
                    bins[p][lastBin[p]].exit++;
                }
            }

            for (int p = 0; p < 3; p++)
            {
                Bound rbnds[MAX_BINS];

                Bound rbnd = bins[p][MAX_BINS - 1].bnd;
                for (int i = MAX_BINS - 1; i > 0; i--)
                {
                    GrowBound(rbnd, bins[p][i].bnd);
                    rbnds[i - 1] = rbnd;
                }

                Bound lbnd = bins[p][0].bnd;
                size_t lsz = 0;
                size_t rsz = sz;

                for (int i = 1; i < MAX_BINS; i++)
                {
                    GrowBound(lbnd, bins[p][i - 1].bnd);
                    lsz += bins[p][i - 1].enter;
                    rsz -= bins[p][i - 1].exit;

                    real sah =
                        nodeSAH +
                        GetArea(lbnd) * GetTriangleCost(lsz) +
                        GetArea(rbnds[i - 1]) * GetTriangleCost(rsz);

                    if (sah < split.sah)
                    {
                        split.sah = sah;
                        split.plane = p;
                        split.pos = origin[p] + binSize[p] * (real)i;
                    }
                }
            }
            return split;
        }

        static void PerformObjectSplit(std::vector<Reference>& left, std::vector<Reference>& right, std::vector<Reference>& refs, const Property& prop, const ObjectSplit& split)
        {
            std::sort(refs.begin(), refs.end(), ObjectSorter(split.plane));

            size_t lsz = split.lsz;
            left.resize(lsz);
            memcpy(&left[0], &refs[0], sizeof(Reference) * lsz);

            size_t rsz = split.rsz;
            right.resize(rsz);
            memcpy(&right[0], &refs[lsz], sizeof(Reference) * rsz);
        }

        static void PushReference(std::vector<Reference>& refs, const Reference& ref)
        {
            vector3 wid = ref.bnd.max - ref.bnd.min;
            if (GetMin(wid) < 0.0 || GetSum(wid) == GetMax(wid))
            {
                ;
            }
            else
            {
                refs.push_back(ref);
            }
        }

        static void PerformSpatialSplit(std::vector<Reference>& left, std::vector<Reference>& right, const std::vector<Reference>& refs, const Property& prop, const SpatialSplit& split)
        {
            size_t sz = refs.size();
            left.reserve(sz >> 1);
            right.reserve(sz >> 1);
            int plane = split.plane;
            real pos = split.pos;
            for (size_t idx = 0; idx < sz; idx++)
            {
                const Reference& ref = refs[idx];
                if (ref.bnd.max[plane] <= pos)
                {
                    left.push_back(ref);
                }
                else if (pos <= ref.bnd.min[plane])
                {
                    right.push_back(ref);
                }
                else
                {
                    Reference leftRef, rightRef;
                    SplitReference(leftRef, rightRef, ref, plane, pos);
                    PushReference(left, leftRef);
                    PushReference(right, rightRef);
                }
            }
        }

        static sbvh_node* CreateBVHNode(const vector3& min, const vector3& max, const std::vector<PCFACE>& faces, const Property& prop)
        {
            size_t sz = faces.size();
            if (sz == 0)
            {
                return 0;
            }
            if (prop.singleside)
            {
                return new sbvh_node_leaf_SS(min, max, faces);
            }
            else
            {
                return new sbvh_node_leaf_DS(min, max, faces);
            }
        }

        static sbvh_node* CreateBVHNode(std::vector<Reference>& refs, int level, const Property& prop)
        {
            static const real EPS = values::epsilon();

            size_t sz = refs.size();
            if (sz == 0) return 0;

            vector3 min, max;
            GetMinMax(min, max, refs);

            min -= vector3(EPS, EPS, EPS);
            max += vector3(EPS, EPS, EPS);

            std::vector<PCFACE> faces;
            size_t fsz = refs.size();
            if (fsz <= 1024)
            { //Create leaf
                CreateFaces(faces, refs);
                fsz = faces.size();
                if (faces.size() <= prop.min_face || level >= prop.max_level)
                {
                    return CreateBVHNode(min, max, faces, prop);
                }
            }

            //
            real area = GetArea(min, max);
            real leafSAH = area * GetTriangleCost(fsz);
            real nodeSAH = area * GetNodeCost(2);

            ObjectSplit object = FindObjectSplit(refs, prop, nodeSAH);

            SpatialSplit spatial;
            spatial.sah = std::numeric_limits<real>::max();
            if (level < prop.max_s_level)
            {
                Bound overlap = object.lbnd;
                IntersectBound(overlap, object.rbnd);
                if (GetArea(overlap) >= prop.min_overlap)
                    spatial = FindSpatialSplit(refs, min, max, prop, nodeSAH);
            }

            real minSAH = std::min(leafSAH, std::min(object.sah, spatial.sah));

            if (minSAH == leafSAH) //leaf
            {
                if (faces.empty())
                {
                    CreateFaces(faces, refs);
                }
                return CreateBVHNode(min, max, faces, prop);
            }
            {
                std::vector<PCFACE> tmp;
                faces.swap(tmp);
            }

            int plane = 0;
            std::vector<Reference> left;
            std::vector<Reference> right;
            if (minSAH == spatial.sah)
            {
                PerformSpatialSplit(left, right, refs, prop, spatial);
                plane = spatial.plane;
            }
            if (minSAH == object.sah || left.empty() || right.empty())
            {
                left.clear();
                right.clear();
                PerformObjectSplit(left, right, refs, prop, object);
                plane = object.plane;
            }
            {
                std::vector<Reference> tmp;
                tmp.swap(refs);
            }

            sbvh_node* nodes[2] = {0, 0};
            nodes[0] = CreateBVHNode(left, level + 1, prop);
            nodes[1] = CreateBVHNode(right, level + 1, prop);

            return new sbvh_node_branch(min, max, plane, nodes);
        }

        void CreateReference(std::vector<Reference>& refs, PCFACE* begin, PCFACE* end)
        {
            size_t sz = end - begin;
            refs.reserve(sz);
            for (size_t i = 0; i < sz; i++)
            {
                PCFACE face = *(begin + i);

                vector3 p0 = *face->p_p0;
                vector3 p1 = *face->p_p1;
                vector3 p2 = *face->p_p2;

                vector3 points[] = {p0, p1, p2};

                vector3 min, max;
                GetMinMax(min, max, points, 3);
                vector3 wid = max - min;
                if (GetMin(wid) < 0.0 || GetSum(wid) == GetMax(wid))
                {
                    ;
                }
                else
                {
                    Reference ref;
                    ref.pFace = face;
                    ref.bnd.min = min;
                    ref.bnd.max = max;
                    refs.push_back(ref);
                }
            }
        }
    }

    class sbvh_mesh_accelerator_imp
    {
    public:
        sbvh_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
        ~sbvh_mesh_accelerator_imp();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;
        size_t memory_size() const;

    protected:
        sbvh_node* root_;
        vector3 min_;
        vector3 max_;
    };

    //----------------------------------------------------------------------------------------------------
    static inline double log2(double x)
    {
        using namespace std;
        static const double LOG2 = log(2.0);
        return log(x) / LOG2;
    }

    static int get_level(size_t face_num)
    {
        using namespace std;
        double f = log2(double(face_num) / MIN_FACE);
        int nRet = int(ceil(f)) + 1;
        return nRet;
    }

    sbvh_mesh_accelerator_imp::sbvh_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
        : min_(min), max_(max)
    {
        size_t sz = end - begin;
        Property mp;
        mp.singleside = prop.singleside;
        mp.min_face = MIN_FACE;
        mp.max_level = std::min(get_level(sz), MAX_LEVEL);
        mp.max_s_level = std::min((int)ceil(mp.max_level * 3.0 / 4.0), MAX_S_LEVEL);
        mp.min_overlap = GetArea(min, max) * SPLIT_ALPHA; //

        std::vector<Reference> refs;
        CreateReference(refs, begin, end);
        root_ = CreateBVHNode(refs, 0, mp);
    }
    sbvh_mesh_accelerator_imp::~sbvh_mesh_accelerator_imp()
    {
        delete root_;
    }

    bool sbvh_mesh_accelerator_imp::test(const ray& r, real dist) const
    {
        return root_->test(r, 0, dist);
    }
    bool sbvh_mesh_accelerator_imp::test(test_info* info, const ray& r, real dist) const
    {
        return root_->test(info, r, 0, dist);
    }
    vector3 sbvh_mesh_accelerator_imp::min() const
    {
        return min_;
    }
    vector3 sbvh_mesh_accelerator_imp::max() const
    {
        return max_;
    }
    size_t sbvh_mesh_accelerator_imp::memory_size() const
    {
        return root_->memory_size();
    }

    sbvh_mesh_accelerator::sbvh_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        imp_ = new sbvh_mesh_accelerator_imp(min, max, begin, end, prop);
    }
    sbvh_mesh_accelerator::~sbvh_mesh_accelerator()
    {
        delete imp_;
    }
    bool sbvh_mesh_accelerator::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }
    bool sbvh_mesh_accelerator::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }
    vector3 sbvh_mesh_accelerator::min() const
    {
        return imp_->min();
    }
    vector3 sbvh_mesh_accelerator::max() const
    {
        return imp_->max();
    }

    size_t sbvh_mesh_accelerator::memory_size() const
    {
        return imp_->memory_size();
    }
}
