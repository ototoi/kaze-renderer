#ifndef KAZE_KDTREE_MESH_ACCELERATOR_H
#define KAZE_KDTREE_MESH_ACCELERATOR_H

#include "mesh_accelerator.h"

namespace kaze
{

    class kdtree_mesh_accelerator_imp;

    class kdtree_mesh_accelerator : public mesh_accelerator
    {
    public:
        typedef const mesh_face* PCFACE;

    public:
        kdtree_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
        ~kdtree_mesh_accelerator();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;
        size_t memory_size() const;

    private:
        kdtree_mesh_accelerator& operator=(const kdtree_mesh_accelerator& rhs);

    private:
        kdtree_mesh_accelerator_imp* imp_;
    };
}

#endif
