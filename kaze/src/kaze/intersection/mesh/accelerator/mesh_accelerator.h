#ifndef KAZE_MESH_ACCELERATOR_H
#define KAZE_MESH_ACCELERATOR_H

#include "ray.h"
#include "types.h"

namespace kaze
{

    class test_info;
    struct mesh_face;

    typedef struct mesh_accelerator_property_
    {
        bool singleside;
        bool compress; //
        bool nosimd;
        char freearea[255];
    } mesh_accelerator_property;

    class mesh_accelerator
    {
    public:
        typedef const mesh_face* PCFACE;

    public:
        virtual ~mesh_accelerator() {}

    public:
        virtual bool test(const ray& r, real dist) const = 0;
        virtual bool test(test_info* info, const ray& r, real dist) const = 0;

        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;

    public:
        virtual size_t memory_size() const = 0;
    };
}

#endif