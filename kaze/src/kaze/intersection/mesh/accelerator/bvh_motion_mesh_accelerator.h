#ifndef KAZE_BVH_MOTION_MESH_ACCELERATOR_H
#define KAZE_BVH_MOTION_MESH_ACCELERATOR_H

#include "mesh_accelerator.h"
#include "motion_mesh_face.h"

namespace kaze
{

    class bvh_motion_mesh_accelerator_imp;

    class bvh_motion_mesh_accelerator : public mesh_accelerator
    {
    public:
        typedef const motion_mesh_face* PCFACE;

    public:
        bvh_motion_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
        ~bvh_motion_mesh_accelerator();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;
        size_t memory_size() const;

    private:
        bvh_motion_mesh_accelerator_imp* imp_;
    };
}

#endif