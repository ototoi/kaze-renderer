#ifndef KAZE_SBVH_MESH_ACCELERATOR_H
#define KAZE_SBVH_MESH_ACCELERATOR_H

#include "mesh_accelerator.h"

namespace kaze
{

    class sbvh_mesh_accelerator_imp;

    class sbvh_mesh_accelerator : public mesh_accelerator
    {
    public:
        typedef const mesh_face* PCFACE;

    public:
        sbvh_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
        ~sbvh_mesh_accelerator();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;
        size_t memory_size() const;

    private:
        sbvh_mesh_accelerator& operator=(const sbvh_mesh_accelerator& rhs);

    private:
        sbvh_mesh_accelerator_imp* imp_;
    };
}

#endif