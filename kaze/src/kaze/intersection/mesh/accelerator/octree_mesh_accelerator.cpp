//#define _SECURE_SCL 0
#include "logger.h"

#include "octree_mesh_accelerator.h"

#include <memory>
#include <cstring>
#include <cmath>
#include <functional>
#include <algorithm>
#include <vector>

#include "test_info.h"

#include "mesh_utility.h"

#include "mesh_face.h"

#include "intersection.h"
#include "intersection_ex.h"

//#include "mmap_array.hpp"

//#include <iostream>
//#include <sstream>

//#define KAZE_octree_mesh_DEBUG		//Output debug message.
//#define KAZE_CHECK_TEST_LIMIT

#define KAZE_OCT_COMPRESS_FORCE 0

#define KAZE_OCT_NODELEAF_MINFACE 16
#define KAZE_GRID_MAXDIV 16

namespace kaze
{
    typedef const mesh_face* PCFACE;
    typedef mesh_face* PFACE;
    typedef PCFACE* face_iterator;
    typedef int oct_range[2];

    namespace
    {

        template <class T>
        inline void mswap(T& a, T& b)
        {
            std::swap(a, b);
        }

        /*
		inline int count_bits(long bits) {
		  bits = (bits & 0x55555555) + (bits >> 1 & 0x55555555);//2
		  bits = (bits & 0x33333333) + (bits >> 2 & 0x33333333);//4
		  bits = (bits & 0x0f0f0f0f) + (bits >> 4 & 0x0f0f0f0f);//8
		  bits = (bits & 0x00ff00ff) + (bits >> 8 & 0x00ff00ff);//16
		  return (bits & 0x0000ffff) + (bits >>16 & 0x0000ffff);//32
		}
		*/
        inline int count_bits(long bits)
        {
            assert(bits <= 0xFF);
            bits = (bits & 0x55555555) + (bits >> 1 & 0x55555555); //2
            bits = (bits & 0x33333333) + (bits >> 2 & 0x33333333); //4
            return (bits & 0x0f0f0f0f) + (bits >> 4 & 0x0f0f0f0f); //8
        }

        inline real oct_min2(real a, real b) { return (a < b) ? a : b; }
        inline real oct_min3(real a, real b, real c) { return (a < b) ? ((a < c) ? a : c) : ((b < c) ? b : c); }
        inline real oct_min4(real a, real b, real c, real d) { return oct_min2(oct_min2(a, b), oct_min2(c, d)); }

        inline real oct_max2(real a, real b) { return (a > b) ? a : b; }
        inline real oct_max3(real a, real b, real c) { return (a > b) ? ((a > c) ? a : c) : ((b > c) ? b : c); }
        inline real oct_max4(real a, real b, real c, real d) { return oct_max2(oct_max2(a, b), oct_max2(c, d)); }

        double log8(double x)
        {
            using namespace std;
            static const double LOG8 = log(8.0);
            return log(x) / LOG8;
        }

        int get_level(size_t face_num, int density = 8)
        {
            using namespace std;
            double f = log8(double(face_num * density * density * density));
            int nRet = (int)(ceil(f));
            return nRet;
        }

        class bound
        {
        public:
            bound(const vector3& min, const vector3& max) : min_(min), max_(max) {}

            const vector3& min() const { return min_; }
            const vector3& max() const { return max_; }
        private:
            vector3 min_;
            vector3 max_;
        };

        class pred_L : public std::unary_function<PCFACE, bool>
        {
        public:
            pred_L(int plane, real pos, const bound& bnd) : plane_(plane), pos_(pos)
            {
                vector3 min = bnd.min();
                vector3 max = bnd.max();
                max[plane] = pos;

                center_ = (max + min) * 0.5;
                half_ = (max - min) * 0.5;
            }

            bool operator()(PCFACE p_face) const
            {
                int plane = plane_;
                real pos = pos_;

                const vector3& rtx0 = (*(p_face->p_p0));
                const vector3& rtx1 = (*(p_face->p_p1));
                const vector3& rtx2 = (*(p_face->p_p2));

                int flag = 0;
                if (rtx0[plane] <= pos) flag++;
                if (rtx1[plane] <= pos) flag++;
                if (rtx2[plane] <= pos) flag++;

                switch (flag)
                {
                case 3:
                    return true;
                case 0:
                    return false;
                default:
                    return triBoxOverlap(center_, half_, rtx0, rtx1, rtx2);
                }
            }

        private:
            int plane_;
            real pos_;
            vector3 center_;
            vector3 half_;
        };

        class pred_R : public std::unary_function<PCFACE, bool>
        {
        public:
            pred_R(int plane, real pos, const bound& bnd) : plane_(plane), pos_(pos)
            {
                vector3 min = bnd.min();
                min[plane] = pos;
                vector3 max = bnd.max();

                center_ = (max + min) * 0.5;
                half_ = (max - min) * 0.5;
            }

            bool operator()(PCFACE p_face) const
            {
                int plane = plane_;
                real pos = pos_;

                const vector3& rtx0 = (*(p_face->p_p0));
                const vector3& rtx1 = (*(p_face->p_p1));
                const vector3& rtx2 = (*(p_face->p_p2));

                int flag = 0;
                if (rtx0[plane] >= pos) flag++;
                if (rtx1[plane] >= pos) flag++;
                if (rtx2[plane] >= pos) flag++;

                switch (flag)
                {
                case 3:
                    return true;
                case 0:
                    return false;
                default: //1,2
                    return triBoxOverlap(center_, half_, rtx0, rtx1, rtx2);
                }
            }

        private:
            int plane_;
            real pos_;
            vector3 center_;
            vector3 half_;
        };

        class pred_L_ONLY : public pred_R
        {
        public:
            pred_L_ONLY(int plane, real pos, const bound& bnd) : pred_R(plane, pos, bnd) {}
            bool operator()(PCFACE p_face) const
            {
                return !(pred_R::operator()(p_face));
            }
        };

        class pred_R_ONLY : public pred_L
        {
        public:
            pred_R_ONLY(int plane, real pos, const bound& bnd) : pred_L(plane, pos, bnd) {}
            bool operator()(PCFACE p_face) const
            {
                return !(pred_L::operator()(p_face));
            }
        };

        struct progress_struct
        {
            size_t now; //
            size_t max; //
            double isz;
            int cnt;
        } prg;

        struct tester_DS
        {
            static bool test(PCFACE face, const ray& r, real dist) { return face->test_DS(r, dist); }
            static bool test(PCFACE face, test_info* info, const ray& r, real dist) { return face->test_DS(info, r, dist); }
        };

        struct tester_SS
        {
            static bool test(PCFACE face, const ray& r, real dist) { return face->test_SS(r, dist); }
            static bool test(PCFACE face, test_info* info, const ray& r, real dist) { return face->test_SS(info, r, dist); }
        };

        //-------------------------------------------------------------------------------------------------
        //octree class defines;
        //-------------------------------------------------------------------------------------------------
        class octree_node
        {
        public:
            virtual bool test(
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                test_info* info, const ray& r, real tmin, real tmax) const = 0;

            virtual bool test(
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                /**/ const ray& r, real tmin, real tmax) const = 0;

        public:
            virtual bool is_null() const { return false; }
            virtual size_t size() const { return 0; }
            virtual size_t memory_size() const = 0;

            virtual size_t face_size() const { return 0; }
        public:
            virtual ~octree_node() {}
        };

        typedef octree_node* PNODE;
        typedef const octree_node* PCNODE;

        typedef std::unique_ptr<octree_node> ap_nodes;

        //-------------------------------------------------------------------------------------------------
        //octree_node_leaf
        //-------------------------------------------------------------------------------------------------

        class octree_node_leaf : public octree_node
        {
        public:
            octree_node_leaf(face_iterator begin, face_iterator end)
            {
                faces_.assign(begin, end);
            }

        public:
            virtual bool test(
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                test_info* info, const ray& r, real tmin, real tmax) const = 0;

            virtual bool test(
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                /**/ const ray& r, real tmin, real tmax) const = 0;

            size_t size() const { return faces_.size(); }
            size_t memory_size() const { return sizeof(octree_node_leaf) + sizeof(PCFACE) * faces_.capacity(); }
        public:
            size_t face_size() const { return faces_.size(); }
            PCFACE get_face(size_t i) const { return faces_[i]; }
        protected: //
            std::vector<PCFACE> faces_;
        };

        class octree_node_leaf_DS : public octree_node_leaf
        {
        public:
            octree_node_leaf_DS(face_iterator begin, face_iterator end) : octree_node_leaf(begin, end) {}
        public:
            bool test(
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                test_info* info, const ray& r, real tmin, real tmax) const;

            bool test(
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                /**/ const ray& r, real tmin, real tmax) const;
            size_t memory_size() const
            {
                return octree_node_leaf::memory_size() + (sizeof(octree_node_leaf_DS) - sizeof(octree_node_leaf));
            }
        };

        class octree_node_leaf_SS : public octree_node_leaf
        {
        public:
            octree_node_leaf_SS(face_iterator begin, face_iterator end) : octree_node_leaf(begin, end) {}
        public:
            bool test(
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                test_info* info, const ray& r, real tmin, real tmax) const;

            bool test(
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                /**/ const ray& r, real tmin, real tmax) const;
            size_t memory_size() const
            {
                return octree_node_leaf::memory_size() + (sizeof(octree_node_leaf_SS) - sizeof(octree_node_leaf));
            }
        };
        //------------------------------------------------------------------------

        bool octree_node_leaf_DS::test(
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            test_info* info, const ray& r, real tmin, real tmax) const
        {
            typedef PCFACE P_C_MF;

            bool bHit = false;
            size_t sz = faces_.size();
            const P_C_MF* p_face = &(faces_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (p_face[i]->test_DS(info, r, tmax))
                {
                    tmax = info->distance;
                    bHit = true;
                }
            }

            return bHit;
        }

        bool octree_node_leaf_DS::test(
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            /**/ const ray& r, real tmin, real tmax) const
        {
            typedef PCFACE P_C_MF;

            size_t sz = faces_.size();
            const P_C_MF* p_face = &(faces_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (p_face[i]->test_DS(r, tmax))
                {
                    return true;
                }
            }

            return false;
        }

        bool octree_node_leaf_SS::test(
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            test_info* info, const ray& r, real tmin, real tmax) const
        {
            typedef PCFACE P_C_MF;

            bool bHit = false;
            size_t sz = faces_.size();
            const P_C_MF* p_face = &(faces_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (p_face[i]->test_SS(info, r, tmax))
                {
                    tmax = info->distance;
                    bHit = true;
                }
            }

            return bHit;
        }

        bool octree_node_leaf_SS::test(
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            /**/ const ray& r, real tmin, real tmax) const
        {
            typedef PCFACE P_C_MF;

            size_t sz = faces_.size();
            const P_C_MF* p_face = &(faces_[0]);
            for (size_t i = 0; i < sz; i++)
            {
                if (p_face[i]->test_SS(r, tmax))
                {
                    return true;
                }
            }

            return false;
        }

        //-------------------------------------------------------------------------------------------------
        //octree_node_branch
        //-------------------------------------------------------------------------------------------------

        class octree_node_branch_base : public octree_node
        {
        public:
            bool test(
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                test_info* info, const ray& r, real tmin, real tmax) const;

            bool test(
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                /**/ const ray& r, real tmin, real tmax) const;

        protected:
            virtual bool test_node(
                int i,
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                test_info* info, const ray& r, real tmin, real tmax) const = 0;

            virtual bool test_node(
                int i,
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                /**/ const ray& r, real tmin, real tmax) const = 0;
        };

        int first_node(real tmin, real txm, real tym, real tzm)
        {
            int nRet = 0;
            if (txm < tmin) nRet |= 1; //
            if (tym < tmin) nRet |= 2; //
            if (tzm < tmin) nRet |= 4; //
            return nRet;
        }

        inline int next_node(real t1, int i1, real t2, int i2, real t3, int i3, real dist)
        { //min
            if (t1 < t2 && t1 < t3)
            {
                if (dist < t1) return 8;
                return i1;
            }
            else if (t2 < t3)
            {
                if (dist < t2) return 8;
                return i2;
            }
            else
            {
                if (dist < t3) return 8;
                return i3;
            }
            //return( (t1<t2&&t1<t3) ? i1 : (t2<t3 ? i2 : i3) );
        }

        inline int next_node0(real t1, real t2, real t3, real dist)
        {
            if (t1 < t2 && t1 < t3)
            {
                if (dist < t1) return 8;
                return 1;
            }
            else if (t2 < t3)
            {
                if (dist < t2) return 8;
                return 2;
            }
            else
            {
                if (dist < t3) return 8;
                return 4;
            }
        }

        inline int next_node1(real t1, real t2, real t3, real dist)
        {
            if (t1 < t2 && t1 < t3)
            {
                //if(dist<t1)return 8;
                return 8;
            }
            else if (t2 < t3)
            {
                if (dist < t2) return 8;
                return 3;
            }
            else
            {
                if (dist < t3) return 8;
                return 5;
            }
        }

        inline int next_node2(real t1, real t2, real t3, real dist)
        {
            if (t1 < t2 && t1 < t3)
            {
                if (dist < t1) return 8;
                return 3;
            }
            else if (t2 < t3)
            {
                //if(dist<t2)return 8;
                return 8;
            }
            else
            {
                if (dist < t3) return 8;
                return 6;
            }
        }

        inline int next_node3(real t1, real t2, real t3, real dist)
        {
            if (t1 < t2 && t1 < t3)
            {
                //if(dist<t1)return 8;
                return 8;
            }
            else if (t2 < t3)
            {
                //if(dist<t2)return 8;
                return 8;
            }
            else
            {
                if (dist < t3) return 8;
                return 7;
            }
        }

        inline int next_node4(real t1, real t2, real t3, real dist)
        {
            if (t1 < t2 && t1 < t3)
            {
                if (dist < t1) return 8;
                return 5;
            }
            else if (t2 < t3)
            {
                if (dist < t2) return 8;
                return 6;
            }
            else
            {
                //if(dist<t3)return 8;
                return 8;
            }
        }

        inline int next_node5(real t1, real t2, real t3, real dist)
        {
            if (t1 < t2 && t1 < t3)
            {
                //if(dist<t1)return 8;
                return 8;
            }
            else if (t2 < t3)
            {
                if (dist < t2) return 8;
                return 7;
            }
            else
            {
                //if(dist<t3)return 8;
                return 8;
            }
        }

        inline int next_node6(real t1, real t2, real t3, real dist)
        {
            if (t1 < t2 && t1 < t3)
            {
                if (dist < t1) return 8;
                return 7;
            }
            else if (t2 < t3)
            {
                //if(dist<t2)return 8;
                return 8;
            }
            else
            {
                //if(dist<t3)return 8;
                return 8;
            }
        }

        bool octree_node_branch_base::test(
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            test_info* info, const ray& r, real tmin, real tmax) const
        {
            real txm, tym, tzm;

            txm = (tx0 + tx1) * real(0.5);
            tym = (ty0 + ty1) * real(0.5);
            tzm = (tz0 + tz1) * real(0.5);

            tmin = oct_max4(tmin, tx0, ty0, tz0);
            int node_number = first_node(tmin, txm, tym, tzm);

            bool bRet = false;
            int phase = r.phase();
            do
            {
                switch (node_number)
                {
                case 0:
                    if (test_node(phase, tx0, ty0, tz0, txm, tym, tzm, info, r, tmin, tmax))
                    {
                        tmax = info->distance;
                        bRet = true;
                    }
                    //node_number = next_node(txm, 1, tym, 2, tzm, 4, tmax);
                    node_number = next_node0(txm, tym, tzm, tmax);
                    break;

                case 1:
                    if (test_node(1 ^ phase, txm, ty0, tz0, tx1, tym, tzm, info, r, tmin, tmax))
                    {
                        tmax = info->distance;
                        bRet = true;
                    }
                    //node_number = next_node(tx1, 8, tym, 3, tzm, 5, tmax);
                    node_number = next_node1(tx1, tym, tzm, tmax);
                    break;

                case 2:
                    if (test_node(2 ^ phase, tx0, tym, tz0, txm, ty1, tzm, info, r, tmin, tmax)) return true;
                    //node_number = next_node(txm, 3, ty1, 8, tzm, 6, tmax);
                    node_number = next_node2(txm, ty1, tzm, tmax);
                    break;

                case 3:
                    if (test_node(3 ^ phase, txm, tym, tz0, tx1, ty1, tzm, info, r, tmin, tmax))
                    {
                        tmax = info->distance;
                        bRet = true;
                    }
                    //node_number = next_node(tx1, 8, ty1, 8, tzm, 7, tmax);
                    node_number = next_node3(tx1, ty1, tzm, tmax);
                    break;

                case 4:
                    if (test_node(4 ^ phase, tx0, ty0, tzm, txm, tym, tz1, info, r, tmin, tmax))
                    {
                        tmax = info->distance;
                        bRet = true;
                    }
                    //node_number = next_node(txm, 5, tym, 6, tz1, 8, tmax);
                    node_number = next_node4(txm, tym, tz1, tmax);
                    break;

                case 5:
                    if (test_node(5 ^ phase, txm, ty0, tzm, tx1, tym, tz1, info, r, tmin, tmax))
                    {
                        tmax = info->distance;
                        bRet = true;
                    }
                    //node_number = next_node(tx1, 8, tym, 7, tz1, 8, tmax);
                    node_number = next_node5(tx1, tym, tz1, tmax);
                    break;

                case 6:
                    if (test_node(6 ^ phase, tx0, tym, tzm, txm, ty1, tz1, info, r, tmin, tmax))
                    {
                        tmax = info->distance;
                        bRet = true;
                    }
                    //node_number = next_node(txm, 7, ty1, 8, tz1, 8, tmax);
                    node_number = next_node6(txm, ty1, tz1, tmax);
                    break;

                case 7:
                    if (test_node(7 ^ phase, txm, tym, tzm, tx1, ty1, tz1, info, r, tmin, tmax))
                    {
                        tmax = info->distance;
                        bRet = true;
                    }
                    node_number = 8;
                    break;
                default:
                    return bRet;
                }
            } while (true);
        }

        bool octree_node_branch_base::test(
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            /**/ const ray& r, real tmin, real tmax) const
        {
            real txm, tym, tzm;

            txm = (tx0 + tx1) * real(0.5);
            tym = (ty0 + ty1) * real(0.5);
            tzm = (tz0 + tz1) * real(0.5);

            tmin = oct_max4(tmin, tx0, ty0, tz0);
            int node_number = first_node(tmin, txm, tym, tzm);

            int phase = r.phase();
            do
            {
                switch (node_number)
                {
                case 0:
                    if (test_node(phase, tx0, ty0, tz0, txm, tym, tzm, r, tmin, tmax)) return true;
                    //node_number = next_node(txm, 1, tym, 2, tzm, 4, tmax);
                    node_number = next_node0(txm, tym, tzm, tmax);
                    break;

                case 1:
                    if (test_node(1 ^ phase, txm, ty0, tz0, tx1, tym, tzm, r, tmin, tmax)) return true;
                    //node_number = next_node(tx1, 8, tym, 3, tzm, 5, tmax);
                    node_number = next_node1(tx1, tym, tzm, tmax);
                    break;

                case 2:
                    if (test_node(2 ^ phase, tx0, tym, tz0, txm, ty1, tzm, r, tmin, tmax)) return true;
                    //node_number = next_node(txm, 3, ty1, 8, tzm, 6, tmax);
                    node_number = next_node2(txm, ty1, tzm, tmax);
                    break;

                case 3:
                    if (test_node(3 ^ phase, txm, tym, tz0, tx1, ty1, tzm, r, tmin, tmax)) return true;
                    //node_number = next_node(tx1, 8, ty1, 8, tzm, 7, tmax);
                    node_number = next_node3(tx1, ty1, tzm, tmax);
                    break;

                case 4:
                    if (test_node(4 ^ phase, tx0, ty0, tzm, txm, tym, tz1, r, tmin, tmax)) return true;
                    //node_number = next_node(txm, 5, tym, 6, tz1, 8, tmax);
                    node_number = next_node4(txm, tym, tz1, tmax);
                    break;

                case 5:
                    if (test_node(5 ^ phase, txm, ty0, tzm, tx1, tym, tz1, r, tmin, tmax)) return true;
                    //node_number = next_node(tx1, 8, tym, 7, tz1, 8, tmax);
                    node_number = next_node5(tx1, tym, tz1, tmax);
                    break;

                case 6:
                    if (test_node(6 ^ phase, tx0, tym, tzm, txm, ty1, tz1, r, tmin, tmax)) return true;
                    //node_number = next_node(txm, 7, ty1, 8, tz1, 8, tmax);
                    node_number = next_node6(txm, ty1, tz1, tmax);
                    break;

                case 7:
                    if (test_node(7 ^ phase, txm, tym, tzm, tx1, ty1, tz1, r, tmin, tmax)) return true;
                    node_number = 8;
                    break;
                default:
                    return false;
                }
            } while (true);
        }

        //-------------------------------------------------------------------------------------------------
        //octree_node_branch
        //-------------------------------------------------------------------------------------------------
        class octree_node_branch : public octree_node_branch_base
        {
        public:
            octree_node_branch(ap_nodes nodes[8]);
            ~octree_node_branch();

        public:
            size_t size() const
            {
                size_t sz = 1;
                for (int i = 0; i < 8; i++)
                {
                    if (nodes_[i])
                    {
                        sz += nodes_[i]->size();
                    }
                }
                return sz;
            }

            size_t memory_size() const
            {
                size_t sz = sizeof(octree_node_branch);
                for (int i = 0; i < 8; i++)
                {
                    if (nodes_[i])
                    {
                        sz += nodes_[i]->memory_size();
                    }
                }
                return sz;
            }

            bool test_node(
                int i,
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                test_info* info, const ray& r, real tmin, real tmax) const;

            bool test_node(
                int i,
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                /**/ const ray& r, real tmin, real tmax) const;

        private:
            PNODE nodes_[8];
        };

        octree_node_branch::octree_node_branch(ap_nodes nodes[8])
        {
            memset(nodes_, 0, sizeof(octree_node*) * 8);
            for (int i = 0; i < 8; i++)
            {
                if (nodes[i].get())
                {
                    nodes_[i] = nodes[i].release();
                }
            }
        }

        octree_node_branch::~octree_node_branch()
        {
            for (int i = 0; i < 8; i++)
            {
                if (nodes_[i]) delete nodes_[i];
            }
        }

        bool octree_node_branch::test_node(
            int i,
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            test_info* info, const ray& r, real tmin, real tmax) const
        {
            if (nodes_[i])
            {
                return nodes_[i]->test(tx0, ty0, tz0, tx1, ty1, tz1, info, r, tmin, tmax);
            }
            return false;
        }

        bool octree_node_branch::test_node(
            int i,
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            /**/ const ray& r, real tmin, real tmax) const
        {
            if (nodes_[i])
            {
                return nodes_[i]->test(tx0, ty0, tz0, tx1, ty1, tz1, r, tmin, tmax);
            }
            return false;
        }

        //-------------------------------------------------------------------------------------------------
        //octree_node_branch_compress
        //-------------------------------------------------------------------------------------------------

        class octree_node_branch_compress : public octree_node_branch_base
        {
        public:
            octree_node_branch_compress(ap_nodes nodes[8]);
            ~octree_node_branch_compress();

        public:
            size_t size() const
            {
                size_t sz = 1;
                int c = count_bits(mask_);
                for (int i = 0; i < c; i++)
                {
                    if (nodes_[i])
                    {
                        sz += nodes_[i]->size();
                    }
                }
                return sz;
            }

            size_t memory_size() const
            {
                size_t sz = sizeof(octree_node_branch_compress);
                int c = count_bits(mask_);
                sz += sizeof(PNODE) * c;
                for (int i = 0; i < c; i++)
                {
                    if (nodes_[i])
                    {
                        sz += nodes_[i]->memory_size();
                    }
                }
                return sz;
            }

            bool test_node(
                int i,
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                test_info* info, const ray& r, real tmin, real tmax) const;

            bool test_node(
                int i,
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                /**/ const ray& r, real tmin, real tmax) const;

        private:
            unsigned char mask_;
            PNODE* nodes_;
        };

        octree_node_branch_compress::octree_node_branch_compress(ap_nodes nodes[8])
        {
            int mask = 0;
            for (int i = 0; i < 8; i++)
            {
                if (nodes[i].get())
                {
                    mask |= (1 << i);
                }
            }
            int c = count_bits(mask);
            nodes_ = new PNODE[c];

            int p = 0;
            for (int i = 0; i < 8; i++)
            {
                if (nodes[i].get())
                {
                    nodes_[p] = nodes[i].release();
                    p++;
                }
            }
            mask_ = mask;
        }

        octree_node_branch_compress::~octree_node_branch_compress()
        {
            int c = count_bits(mask_);
            for (int i = 0; i < c; i++)
            {
                delete nodes_[i];
            }
            delete[] nodes_;
        }

        bool octree_node_branch_compress::test_node(
            int i,
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            test_info* info, const ray& r, real tmin, real tmax) const
        {
            int mask = mask_;
            int s = 1 << i;
            if (mask & s)
            {
                int p = count_bits((s - 1) & mask);
                return nodes_[p]->test(tx0, ty0, tz0, tx1, ty1, tz1, info, r, tmin, tmax);
            }
            return false;
        }

        bool octree_node_branch_compress::test_node(
            int i,
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            /**/ const ray& r, real tmin, real tmax) const
        {
            int mask = mask_;
            int s = 1 << i;
            if (mask & s)
            {
                int p = count_bits((s - 1) & mask);
                return nodes_[p]->test(tx0, ty0, tz0, tx1, ty1, tz1, r, tmin, tmax);
            }
            return false;
        }

        //-------------------------------------------------------------------------------------------------
        //octree_node_leaf_compress
        //-------------------------------------------------------------------------------------------------

        template <class ITYPE, class TEST>
        class octree_node_leaf_compress : public octree_node_branch_base
        {
        public:
            octree_node_leaf_compress(ap_nodes nodes[8])
            {
                size_t total;

                int mask = 0;
                total = 0;
                std::vector<octree_node_leaf*> leafs;
                for (int i = 0; i < 8; i++)
                {
                    if (nodes[i].get())
                    {
                        octree_node_leaf* p_leaf = dynamic_cast<octree_node_leaf*>(nodes[i].get());
                        total += p_leaf->face_size();
                        leafs.push_back(p_leaf);
                        mask |= (1 << i);
                    }
                }
                mask_ = mask;

                L.resize(total);
                size_t lsz = leafs.size();
                H.resize(lsz + 1);
                H[0] = 0;
                total = 0;

                //assert(Sz==lsz);
                for (size_t i = 0; i < lsz; i++)
                {
                    size_t fsz = leafs[i]->face_size();
                    for (size_t j = 0; j < fsz; j++)
                    {
                        L[total + j] = leafs[i]->get_face(j);
                    }
                    total += fsz;
                    H[i + 1] = total;
                }
            }
            ~octree_node_leaf_compress() {}

            size_t size() const
            {
                return L.size();
            }

            size_t memory_size() const
            {
                size_t sz = sizeof(octree_node_leaf_compress<ITYPE, TEST>);
                sz += sizeof(ITYPE) * H.size();
                sz += sizeof(PCFACE) * L.size();
                return sz;
            }

            bool test_node(
                int i,
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                test_info* info, const ray& r, real tmin, real tmax) const;

            bool test_node(
                int i,
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                /**/ const ray& r, real tmin, real tmax) const;

        protected:
            char mask_;
            std::vector<ITYPE> H;
            std::vector<PCFACE> L;
        };

        template <class ITYPE, class TEST>
        bool octree_node_leaf_compress<ITYPE, TEST>::test_node(
            int i,
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            test_info* info, const ray& r, real tmin, real tmax) const
        {
            int mask = mask_;
            int s = 1 << i;
            if (mask & s)
            {
                bool bRet = false;
                int p = count_bits((s - 1) & mask);
                for (size_t i = H[p]; i < H[p + 1]; i++)
                {
                    if (TEST::test(L[i], info, r, tmax))
                    {
                        tmax = info->distance;
                        bRet = true;
                    }
                }
                return bRet;
            }
            return false;
        }

        template <class ITYPE, class TEST>
        bool octree_node_leaf_compress<ITYPE, TEST>::test_node(
            int i,
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            /**/ const ray& r, real tmin, real tmax) const
        {
            int mask = mask_;
            int s = 1 << i;
            if (mask & s)
            {
                int p = count_bits((s - 1) & mask);
                for (size_t i = H[p]; i < H[p + 1]; i++)
                {
                    if (TEST::test(L[i], r, tmax)) return true;
                }
            }
            return false;
        }

        //-------------------------------------------------------------------------------------------------
        //octree_node_branch_fixed
        //-------------------------------------------------------------------------------------------------

        template <int N, int S>
        struct c__
        {
            static const int v = (N & (1 << S)) ? 1 : 0;
        };

        template <int N>
        struct count8
        {
            static const int value = c__<N, 0>::v + c__<N, 1>::v + c__<N, 2>::v + c__<N, 3>::v + c__<N, 4>::v + c__<N, 5>::v + c__<N, 6>::v + c__<N, 7>::v;
        };

        template <int MASK>
        class octree_node_branch_fixed : public octree_node_branch_base
        {
        public:
            static const int Sz = count8<MASK>::value;
            octree_node_branch_fixed(ap_nodes nodes[8]);
            ~octree_node_branch_fixed();

            size_t size() const
            {
                size_t sz = 1;
                for (int i = 0; i < Sz; i++)
                {
                    sz += nodes_[i]->size();
                }
                return sz;
            }

            size_t memory_size() const
            {
                size_t sz = sizeof(octree_node_branch_fixed<MASK>);
                for (int i = 0; i < Sz; i++)
                {
                    sz += nodes_[i]->memory_size();
                }
                return sz;
            }
            bool test_node(
                int i,
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                test_info* info, const ray& r, real tmin, real tmax) const;

            bool test_node(
                int i,
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                /**/ const ray& r, real tmin, real tmax) const;

        private:
            PNODE nodes_[Sz];
        };

        template <int MASK>
        octree_node_branch_fixed<MASK>::octree_node_branch_fixed(ap_nodes nodes[8])
        {
            int p = 0;
            for (int i = 0; i < 8; i++)
            {
                if (nodes[i].get())
                {
                    nodes_[p] = nodes[i].release();
                    p++;
                }
            }
        }

        template <int MASK>
        octree_node_branch_fixed<MASK>::~octree_node_branch_fixed()
        {
            for (int i = 0; i < Sz; i++)
            {
                delete nodes_[i];
            }
        }

        template <int MASK>
        bool octree_node_branch_fixed<MASK>::test_node(
            int i,
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            test_info* info, const ray& r, real tmin, real tmax) const
        {
            int s = 1 << i;
            if (MASK & s)
            {
                int p = count_bits((s - 1) & MASK);
                return nodes_[p]->test(tx0, ty0, tz0, tx1, ty1, tz1, info, r, tmin, tmax);
            }
            return false;
        }

        template <int MASK>
        bool octree_node_branch_fixed<MASK>::test_node(
            int i,
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            /**/ const ray& r, real tmin, real tmax) const
        {
            int s = 1 << i;
            if (MASK & s)
            {
                int p = count_bits((s - 1) & MASK);
                return nodes_[p]->test(tx0, ty0, tz0, tx1, ty1, tz1, r, tmin, tmax);
            }
            return false;
        }

        //-------------------------------------------------------------------------------------------------
        //octree_node_leaf_fixed
        //-------------------------------------------------------------------------------------------------

        template <int MASK, class ITYPE, class TEST>
        class octree_node_leaf_fixed : public octree_node_branch_base
        {
        public:
            static const int Sz = count8<MASK>::value;
            octree_node_leaf_fixed(ap_nodes nodes[8])
            {
                size_t total;

                total = 0;
                std::vector<octree_node_leaf*> leafs;
                for (int i = 0; i < 8; i++)
                {
                    if (nodes[i].get())
                    {
                        octree_node_leaf* p_leaf = dynamic_cast<octree_node_leaf*>(nodes[i].get());
                        total += p_leaf->face_size();
                        leafs.push_back(p_leaf);
                    }
                }
                L.resize(total);

                H[0] = 0;
                total = 0;
                size_t lsz = leafs.size();
                assert(Sz == lsz);
                for (size_t i = 0; i < lsz; i++)
                {
                    size_t fsz = leafs[i]->face_size();
                    for (size_t j = 0; j < fsz; j++)
                    {
                        L[total + j] = leafs[i]->get_face(j);
                    }
                    total += fsz;
                    H[i + 1] = total;
                }
            }
            ~octree_node_leaf_fixed() {}

            size_t size() const
            {
                return L.size();
            }

            size_t memory_size() const
            {
                size_t sz = sizeof(octree_node_leaf_fixed<MASK, ITYPE, TEST>);
                sz += sizeof(PCFACE) * L.size();
                return sz;
            }

            bool test_node(
                int i,
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                test_info* info, const ray& r, real tmin, real tmax) const;

            bool test_node(
                int i,
                real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
                /**/ const ray& r, real tmin, real tmax) const;

        protected:
            ITYPE H[Sz + 1];
            std::vector<PCFACE> L;
        };

        template <int MASK, class ITYPE, class TEST>
        bool octree_node_leaf_fixed<MASK, ITYPE, TEST>::test_node(
            int i,
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            test_info* info, const ray& r, real tmin, real tmax) const
        {
            int s = 1 << i;
            if (MASK & s)
            {
                bool bRet = false;
                int p = count_bits((s - 1) & MASK);
                for (size_t i = H[p]; i < H[p + 1]; i++)
                {
                    if (TEST::test(L[i], info, r, tmax))
                    {
                        tmax = info->distance;
                        bRet = true;
                    }
                }
                return bRet;
            }
            return false;
        }

        template <int MASK, class ITYPE, class TEST>
        bool octree_node_leaf_fixed<MASK, ITYPE, TEST>::test_node(
            int i,
            real tx0, real ty0, real tz0, real tx1, real ty1, real tz1,
            /**/ const ray& r, real tmin, real tmax) const
        {
            int s = 1 << i;
            if (MASK & s)
            {
                int p = count_bits((s - 1) & MASK);
                for (size_t i = H[p]; i < H[p + 1]; i++)
                {
                    if (TEST::test(L[i], r, tmax)) return true;
                }
            }
            return false;
        }

#if 0

#define RET(N) \
    case N:    \
        return new octree_node_branch_fixed<N>(nodes);		
		octree_node* create_node_branch_fixed(ap_nodes nodes[8]){
			int MASK = 0;
			for(int i = 0;i<8;i++){
				if(nodes[i].get()){
					MASK |= (1<<i);
				}
			}
			assert(1<=MASK && MASK<=255);
			switch(MASK){			
				RET( 1 );
				RET( 2 );
				RET( 3 );
				RET( 4 );
				RET( 5 );
				RET( 6 );
				RET( 7 );
				RET( 8 );
				RET( 9 );
				RET( 10 );
				RET( 11 );
				RET( 12 );
				RET( 13 );
				RET( 14 );
				RET( 15 );
				RET( 16 );
				RET( 17 );
				RET( 18 );
				RET( 19 );
				RET( 20 );
				RET( 21 );
				RET( 22 );
				RET( 23 );
				RET( 24 );
				RET( 25 );
				RET( 26 );
				RET( 27 );
				RET( 28 );
				RET( 29 );
				RET( 30 );
				RET( 31 );
				RET( 32 );
				RET( 33 );
				RET( 34 );
				RET( 35 );
				RET( 36 );
				RET( 37 );
				RET( 38 );
				RET( 39 );
				RET( 40 );
				RET( 41 );
				RET( 42 );
				RET( 43 );
				RET( 44 );
				RET( 45 );
				RET( 46 );
				RET( 47 );
				RET( 48 );
				RET( 49 );
				RET( 50 );
				RET( 51 );
				RET( 52 );
				RET( 53 );
				RET( 54 );
				RET( 55 );
				RET( 56 );
				RET( 57 );
				RET( 58 );
				RET( 59 );
				RET( 60 );
				RET( 61 );
				RET( 62 );
				RET( 63 );
				RET( 64 );
				RET( 65 );
				RET( 66 );
				RET( 67 );
				RET( 68 );
				RET( 69 );
				RET( 70 );
				RET( 71 );
				RET( 72 );
				RET( 73 );
				RET( 74 );
				RET( 75 );
				RET( 76 );
				RET( 77 );
				RET( 78 );
				RET( 79 );
				RET( 80 );
				RET( 81 );
				RET( 82 );
				RET( 83 );
				RET( 84 );
				RET( 85 );
				RET( 86 );
				RET( 87 );
				RET( 88 );
				RET( 89 );
				RET( 90 );
				RET( 91 );
				RET( 92 );
				RET( 93 );
				RET( 94 );
				RET( 95 );
				RET( 96 );
				RET( 97 );
				RET( 98 );
				RET( 99 );
				RET( 100 );
				RET( 101 );
				RET( 102 );
				RET( 103 );
				RET( 104 );
				RET( 105 );
				RET( 106 );
				RET( 107 );
				RET( 108 );
				RET( 109 );
				RET( 110 );
				RET( 111 );
				RET( 112 );
				RET( 113 );
				RET( 114 );
				RET( 115 );
				RET( 116 );
				RET( 117 );
				RET( 118 );
				RET( 119 );
				RET( 120 );
				RET( 121 );
				RET( 122 );
				RET( 123 );
				RET( 124 );
				RET( 125 );
				RET( 126 );
				RET( 127 );
				RET( 128 );
				RET( 129 );
				RET( 130 );
				RET( 131 );
				RET( 132 );
				RET( 133 );
				RET( 134 );
				RET( 135 );
				RET( 136 );
				RET( 137 );
				RET( 138 );
				RET( 139 );
				RET( 140 );
				RET( 141 );
				RET( 142 );
				RET( 143 );
				RET( 144 );
				RET( 145 );
				RET( 146 );
				RET( 147 );
				RET( 148 );
				RET( 149 );
				RET( 150 );
				RET( 151 );
				RET( 152 );
				RET( 153 );
				RET( 154 );
				RET( 155 );
				RET( 156 );
				RET( 157 );
				RET( 158 );
				RET( 159 );
				RET( 160 );
				RET( 161 );
				RET( 162 );
				RET( 163 );
				RET( 164 );
				RET( 165 );
				RET( 166 );
				RET( 167 );
				RET( 168 );
				RET( 169 );
				RET( 170 );
				RET( 171 );
				RET( 172 );
				RET( 173 );
				RET( 174 );
				RET( 175 );
				RET( 176 );
				RET( 177 );
				RET( 178 );
				RET( 179 );
				RET( 180 );
				RET( 181 );
				RET( 182 );
				RET( 183 );
				RET( 184 );
				RET( 185 );
				RET( 186 );
				RET( 187 );
				RET( 188 );
				RET( 189 );
				RET( 190 );
				RET( 191 );
				RET( 192 );
				RET( 193 );
				RET( 194 );
				RET( 195 );
				RET( 196 );
				RET( 197 );
				RET( 198 );
				RET( 199 );
				RET( 200 );
				RET( 201 );
				RET( 202 );
				RET( 203 );
				RET( 204 );
				RET( 205 );
				RET( 206 );
				RET( 207 );
				RET( 208 );
				RET( 209 );
				RET( 210 );
				RET( 211 );
				RET( 212 );
				RET( 213 );
				RET( 214 );
				RET( 215 );
				RET( 216 );
				RET( 217 );
				RET( 218 );
				RET( 219 );
				RET( 220 );
				RET( 221 );
				RET( 222 );
				RET( 223 );
				RET( 224 );
				RET( 225 );
				RET( 226 );
				RET( 227 );
				RET( 228 );
				RET( 229 );
				RET( 230 );
				RET( 231 );
				RET( 232 );
				RET( 233 );
				RET( 234 );
				RET( 235 );
				RET( 236 );
				RET( 237 );
				RET( 238 );
				RET( 239 );
				RET( 240 );
				RET( 241 );
				RET( 242 );
				RET( 243 );
				RET( 244 );
				RET( 245 );
				RET( 246 );
				RET( 247 );
				RET( 248 );
				RET( 249 );
				RET( 250 );
				RET( 251 );
				RET( 252 );
				RET( 253 );
				RET( 254 );
				RET( 255 );
			}	
			return NULL;
		}

#undef RET

#define RET(N) \
    case N:    \
        return new octree_node_leaf_fixed<N, ITYPE, TEST>(nodes);			
		template<class ITYPE, class TEST>
		octree_node* create_octree_node_leaf_fixed(ap_nodes nodes[8]){
			int MASK = 0;
			for(int i = 0;i<8;i++){
				if(nodes[i].get()){
					MASK |= (1<<i);
				}
			}
			assert(1<=MASK && MASK<=255);
			switch(MASK){			
				RET( 1 );
				RET( 2 );
				RET( 3 );
				RET( 4 );
				RET( 5 );
				RET( 6 );
				RET( 7 );
				RET( 8 );
				RET( 9 );
				RET( 10 );
				RET( 11 );
				RET( 12 );
				RET( 13 );
				RET( 14 );
				RET( 15 );
				RET( 16 );
				RET( 17 );
				RET( 18 );
				RET( 19 );
				RET( 20 );
				RET( 21 );
				RET( 22 );
				RET( 23 );
				RET( 24 );
				RET( 25 );
				RET( 26 );
				RET( 27 );
				RET( 28 );
				RET( 29 );
				RET( 30 );
				RET( 31 );
				RET( 32 );
				RET( 33 );
				RET( 34 );
				RET( 35 );
				RET( 36 );
				RET( 37 );
				RET( 38 );
				RET( 39 );
				RET( 40 );
				RET( 41 );
				RET( 42 );
				RET( 43 );
				RET( 44 );
				RET( 45 );
				RET( 46 );
				RET( 47 );
				RET( 48 );
				RET( 49 );
				RET( 50 );
				RET( 51 );
				RET( 52 );
				RET( 53 );
				RET( 54 );
				RET( 55 );
				RET( 56 );
				RET( 57 );
				RET( 58 );
				RET( 59 );
				RET( 60 );
				RET( 61 );
				RET( 62 );
				RET( 63 );
				RET( 64 );
				RET( 65 );
				RET( 66 );
				RET( 67 );
				RET( 68 );
				RET( 69 );
				RET( 70 );
				RET( 71 );
				RET( 72 );
				RET( 73 );
				RET( 74 );
				RET( 75 );
				RET( 76 );
				RET( 77 );
				RET( 78 );
				RET( 79 );
				RET( 80 );
				RET( 81 );
				RET( 82 );
				RET( 83 );
				RET( 84 );
				RET( 85 );
				RET( 86 );
				RET( 87 );
				RET( 88 );
				RET( 89 );
				RET( 90 );
				RET( 91 );
				RET( 92 );
				RET( 93 );
				RET( 94 );
				RET( 95 );
				RET( 96 );
				RET( 97 );
				RET( 98 );
				RET( 99 );
				RET( 100 );
				RET( 101 );
				RET( 102 );
				RET( 103 );
				RET( 104 );
				RET( 105 );
				RET( 106 );
				RET( 107 );
				RET( 108 );
				RET( 109 );
				RET( 110 );
				RET( 111 );
				RET( 112 );
				RET( 113 );
				RET( 114 );
				RET( 115 );
				RET( 116 );
				RET( 117 );
				RET( 118 );
				RET( 119 );
				RET( 120 );
				RET( 121 );
				RET( 122 );
				RET( 123 );
				RET( 124 );
				RET( 125 );
				RET( 126 );
				RET( 127 );
				RET( 128 );
				RET( 129 );
				RET( 130 );
				RET( 131 );
				RET( 132 );
				RET( 133 );
				RET( 134 );
				RET( 135 );
				RET( 136 );
				RET( 137 );
				RET( 138 );
				RET( 139 );
				RET( 140 );
				RET( 141 );
				RET( 142 );
				RET( 143 );
				RET( 144 );
				RET( 145 );
				RET( 146 );
				RET( 147 );
				RET( 148 );
				RET( 149 );
				RET( 150 );
				RET( 151 );
				RET( 152 );
				RET( 153 );
				RET( 154 );
				RET( 155 );
				RET( 156 );
				RET( 157 );
				RET( 158 );
				RET( 159 );
				RET( 160 );
				RET( 161 );
				RET( 162 );
				RET( 163 );
				RET( 164 );
				RET( 165 );
				RET( 166 );
				RET( 167 );
				RET( 168 );
				RET( 169 );
				RET( 170 );
				RET( 171 );
				RET( 172 );
				RET( 173 );
				RET( 174 );
				RET( 175 );
				RET( 176 );
				RET( 177 );
				RET( 178 );
				RET( 179 );
				RET( 180 );
				RET( 181 );
				RET( 182 );
				RET( 183 );
				RET( 184 );
				RET( 185 );
				RET( 186 );
				RET( 187 );
				RET( 188 );
				RET( 189 );
				RET( 190 );
				RET( 191 );
				RET( 192 );
				RET( 193 );
				RET( 194 );
				RET( 195 );
				RET( 196 );
				RET( 197 );
				RET( 198 );
				RET( 199 );
				RET( 200 );
				RET( 201 );
				RET( 202 );
				RET( 203 );
				RET( 204 );
				RET( 205 );
				RET( 206 );
				RET( 207 );
				RET( 208 );
				RET( 209 );
				RET( 210 );
				RET( 211 );
				RET( 212 );
				RET( 213 );
				RET( 214 );
				RET( 215 );
				RET( 216 );
				RET( 217 );
				RET( 218 );
				RET( 219 );
				RET( 220 );
				RET( 221 );
				RET( 222 );
				RET( 223 );
				RET( 224 );
				RET( 225 );
				RET( 226 );
				RET( 227 );
				RET( 228 );
				RET( 229 );
				RET( 230 );
				RET( 231 );
				RET( 232 );
				RET( 233 );
				RET( 234 );
				RET( 235 );
				RET( 236 );
				RET( 237 );
				RET( 238 );
				RET( 239 );
				RET( 240 );
				RET( 241 );
				RET( 242 );
				RET( 243 );
				RET( 244 );
				RET( 245 );
				RET( 246 );
				RET( 247 );
				RET( 248 );
				RET( 249 );
				RET( 250 );
				RET( 251 );
				RET( 252 );
				RET( 253 );
				RET( 254 );
				RET( 255 );
			}	
			return NULL;
		}

#undef RET

#endif

        //-------------------------------------------------------------------------------------------------
        //create_octree_node
        //-------------------------------------------------------------------------------------------------

        static octree_node* create_octree_node(face_iterator begin, face_iterator end, const bound& bnd, int level, int max_level, const mesh_accelerator_property& prop);

        bound get_L(int plane, real pos, const bound& bnd)
        {
            vector3 min = bnd.min();
            vector3 max = bnd.max();
            max[plane] = pos;
            return bound(min, max);
        }

        bound get_R(int plane, real pos, const bound& bnd)
        {
            vector3 min = bnd.min();
            min[plane] = pos;
            vector3 max = bnd.max();
            return bound(min, max);
        }

        void get_z_node(ap_nodes children[8], face_iterator begin, face_iterator end, const bound& bnd, int sgn, const mesh_accelerator_property& prop, int level, int max_level)
        {
            static const int SGNCODE = 4;
            static const int PLANE = 2;

            using namespace std;

            size_t tsz = distance(begin, end);
            if (tsz == 0) return;

            vector3 cmin, cmax;
            face_iterator cbegin, cend;

            real pos = (bnd.min()[PLANE] + bnd.max()[PLANE]) * real(0.5);
            bool bIsLeafLevel = (level >= max_level);

            size_t csz;
            size_t lsz, rsz;

            cbegin = begin;
            cend = partition(begin, end, pred_L(PLANE, pos, bnd));
            csz = distance(cbegin, cend);
            lsz = csz;

            if (csz)
            {
                children[sgn].reset(create_octree_node(cbegin, cend, get_L(PLANE, pos, bnd), level, max_level, prop));
            }

            //cbegin = begin;
            //cend = partition(begin,end,pred_R(PLANE,pos,bnd));
            cbegin = partition(cbegin, cend, pred_L_ONLY(PLANE, pos, bnd));
            cend = end;

            csz = distance(cbegin, cend);
            rsz = csz;

            if (csz)
            { //
                children[sgn | SGNCODE].reset(create_octree_node(cbegin, cend, get_R(PLANE, pos, bnd), level, max_level, prop));
            }
        }

        void get_y_node(ap_nodes children[8], face_iterator begin, face_iterator end, const bound& bnd, int sgn, const mesh_accelerator_property& prop, int level, int max_level)
        {
            static const int SGNCODE = 2;
            static const int PLANE = 1;

            using namespace std;

            size_t tsz = distance(begin, end);
            if (tsz == 0) return;

            vector3 cmin, cmax;
            face_iterator cbegin, cend;

            real pos = (bnd.min()[PLANE] + bnd.max()[PLANE]) * real(0.5);

            cbegin = begin;
            cend = partition(begin, end, pred_L(PLANE, pos, bnd));
            get_z_node(children, cbegin, cend, get_L(PLANE, pos, bnd), sgn, prop, level, max_level);

            //cbegin = begin;
            //cend = std::partition(begin,end,pred_R(PLANE,pos,bnd));
            cbegin = partition(cbegin, cend, pred_L_ONLY(PLANE, pos, bnd));
            cend = end;
            get_z_node(children, cbegin, cend, get_R(PLANE, pos, bnd), sgn | SGNCODE, prop, level, max_level);
        }

        void get_x_node(ap_nodes children[8], face_iterator begin, face_iterator end, const bound& bnd, int sgn, const mesh_accelerator_property& prop, int level, int max_level)
        {
            static const int SGNCODE = 1;
            static const int PLANE = 0;

            using namespace std;

            size_t tsz = distance(begin, end);
            if (tsz == 0) return;

            vector3 cmin, cmax;
            face_iterator cbegin, cend;

            real pos = (bnd.min()[PLANE] + bnd.max()[PLANE]) * real(0.5);

            cbegin = begin;
            cend = partition(begin, end, pred_L(PLANE, pos, bnd));
            get_y_node(children, cbegin, cend, get_L(PLANE, pos, bnd), sgn, prop, level, max_level);

            //cbegin = begin;
            //cend   = partition(begin,end,pred_R(PLANE,pos,bnd));
            cbegin = partition(cbegin, cend, pred_L_ONLY(PLANE, pos, bnd));
            cend = end;
            get_y_node(children, cbegin, cend, get_R(PLANE, pos, bnd), sgn | SGNCODE, prop, level, max_level);
        }

        octree_node* create_octree_node(face_iterator begin, face_iterator end, const bound& bnd, int level, int max_level, const mesh_accelerator_property& prop)
        {
            size_t sz = end - begin;

            if (sz <= KAZE_OCT_NODELEAF_MINFACE || level >= max_level)
            {
                if (prop.singleside)
                {
                    return new octree_node_leaf_SS(begin, end);
                }
                else
                {
                    return new octree_node_leaf_DS(begin, end);
                }
            }
            else
            {
                ap_nodes children[8];
                get_x_node(children, begin, end, bnd, 0, prop, level + 1, max_level);
#if 1
                return new octree_node_branch(children);
#else
                if (!prop.compress)
                {
                    return new octree_node_branch(children);
                }
                else
                {

                    bool bLeaf = true;

                    size_t total = 0;
                    for (int i = 0; i < 8; i++)
                    {
                        if (children[i].get())
                        {
                            size_t fsz = children[i]->face_size();
                            if (fsz == 0)
                            {
                                bLeaf = false;
                            }
                            total += fsz;
                        }
                    }

#if 1
                    if (bLeaf)
                    {
                        if (prop.singleside)
                        {
                            if (total <= std::numeric_limits<uint8_t>::max())
                            {
                                return create_octree_node_leaf_fixed<uint8_t, tester_SS>(children);
                            }
                            else if (total <= std::numeric_limits<uint16_t>::max())
                            {
                                return create_octree_node_leaf_fixed<uint16_t, tester_SS>(children);
                            }
                            else if (total <= std::numeric_limits<uint32_t>::max())
                            {
                                return create_octree_node_leaf_fixed<uint32_t, tester_SS>(children);
                            }
                            else
                            {
                                return create_octree_node_leaf_fixed<size_t, tester_SS>(children);
                            }
                        }
                        else
                        {
                            if (total <= std::numeric_limits<uint8_t>::max())
                            {
                                return create_octree_node_leaf_fixed<uint8_t, tester_DS>(children);
                            }
                            else if (total <= std::numeric_limits<uint16_t>::max())
                            {
                                return create_octree_node_leaf_fixed<uint16_t, tester_DS>(children);
                            }
                            else if (total <= std::numeric_limits<uint32_t>::max())
                            {
                                return create_octree_node_leaf_fixed<uint32_t, tester_DS>(children);
                            }
                            else
                            {
                                return create_octree_node_leaf_fixed<size_t, tester_DS>(children);
                            }
                        }
                    }
                    else
                    {
                        return create_node_branch_fixed(children);
                    }
#else
                    if (bLeaf)
                    {
                        if (prop.singleside)
                        {
                            if (total <= std::numeric_limits<uint8_t>::max())
                            {
                                return new octree_node_leaf_compress<uint8_t, tester_SS>(children);
                            }
                            else if (total <= std::numeric_limits<uint16_t>::max())
                            {
                                return new octree_node_leaf_compress<uint16_t, tester_SS>(children);
                            }
                            else if (total <= std::numeric_limits<uint32_t>::max())
                            {
                                return new octree_node_leaf_compress<uint32_t, tester_SS>(children);
                            }
                            else
                            {
                                return new octree_node_leaf_compress<size_t, tester_SS>(children);
                            }
                        }
                        else
                        {
                            if (total <= std::numeric_limits<uint8_t>::max())
                            {
                                return new octree_node_leaf_compress<uint8_t, tester_DS>(children);
                            }
                            else if (total <= std::numeric_limits<uint16_t>::max())
                            {
                                return new octree_node_leaf_compress<uint16_t, tester_DS>(children);
                            }
                            else if (total <= std::numeric_limits<uint32_t>::max())
                            {
                                return new octree_node_leaf_compress<uint32_t, tester_DS>(children);
                            }
                            else
                            {
                                return new octree_node_leaf_compress<size_t, tester_DS>(children);
                            }
                        }
                    }
                    else
                    {
                        return new octree_node_branch_compress(children);
                    }
#endif
                }
            }
            return NULL;
#endif
            }
        }
    } //end of nmespace

    //-------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------

    class octree_mesh_accelerator_imp
    {
    public:
        virtual bool test(const ray& r, real dist) const = 0;
        virtual bool test(test_info* info, const ray& r, real dist) const = 0;

        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;

        //virtual void add(face_iterator begin, face_iterator end, const mesh_accelerator_property& prop){}

        virtual size_t size() const { return 0; }
        virtual size_t memory_size() const = 0;
        virtual bool is_null() const { return false; }
        virtual bool test_internal(const ray& r, real tmin, real tmax) const { return false; }
        virtual bool test_internal(test_info* info, const ray& r, real tmin, real tmax) const { return false; }
        virtual ~octree_mesh_accelerator_imp() {}
    };

    class null_root : public octree_mesh_accelerator_imp
    {
        bool test(const ray& r, real dist) const { return false; }
        bool test(test_info* info, const ray& r, real dist) const { return false; }

        vector3 min() const { return vector3(0, 0, 0); }
        vector3 max() const { return vector3(0, 0, 0); }
    public:
        size_t memory_size() const { return sizeof(null_root); }

    public:
        bool is_null() const { return true; }

    public:
        static null_root* p_instance()
        {
            static null_root ins;
            return &ins;
        }
    };
    class octree_root : public octree_mesh_accelerator_imp
    {
    public:
        octree_root(const vector3& min, const vector3& max);
        ~octree_root();
        void add(face_iterator begin, face_iterator end, const mesh_accelerator_property& prop);

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const { return min_; }
        vector3 max() const { return max_; }

    public:
        bool test_internal(const ray& r, real tmin, real tmax) const;
        bool test_internal(test_info* info, const ray& r, real tmin, real tmax) const;

        size_t size() const { return root_->size(); }
        size_t memory_size() const { return sizeof(octree_root) + root_->memory_size(); }
    private:
        vector3 min_;
        vector3 max_;
        octree_node* root_;
    };
    class grid_root : public octree_mesh_accelerator_imp
    {
    private:
        typedef const octree_root* P_C_TR;

    public:
        grid_root(const vector3& min, const vector3& max);
        ~grid_root();
        void add(face_iterator begin, face_iterator end, const mesh_accelerator_property& prop);

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;

        vector3 min() const { return min_; }
        vector3 max() const { return max_; }

    protected:
        void set_face(oct_range range[3], face_iterator begin, face_iterator end, const bound& bnd, const mesh_accelerator_property& prop);

    public:
        size_t size() const
        {
            size_t sz = 0;
            for (size_t i = 0; i < nodes_.size(); i++)
            {
                if (nodes_[i])
                {
                    sz += nodes_[i]->size();
                }
            }
            return sz;
        }
        size_t memory_size() const
        {
            size_t sz = sizeof(grid_root);
            sz += sizeof(octree_root*) * nodes_.size();
            for (size_t i = 0; i < nodes_.size(); i++)
            {
                if (nodes_[i])
                {
                    sz += nodes_[i]->memory_size();
                }
            }
            return sz;
        }

    private:
        vector3 min_;
        vector3 max_;
        real delta_[3];
        real idelta_[3];
        int ndiv_[3];

        //mmap_array<octree_root*> nodes_;
        std::vector<octree_root*> nodes_;
    };

    namespace
    {
#if 0		
		inline bool is_nounit(int delta[3] ){
			size_t csz = (size_t)(delta[0]*delta[1]*delta[2]);
			//print_log("csz = %d\n",csz);
			assert(csz);//need not zero
			return (csz > 1);
		}
#else
    inline int is_nounit(int delta[3])
    {
        //return 0x1 == (delta[0]^delta[1]^delta[2]);
        return (0x1 != delta[0]) || (0x1 != delta[1]) || (0x1 != delta[2]);
    }
#endif
    }

    //-------------------------------------------------------------------------------------------------
    //grid_root
    //-------------------------------------------------------------------------------------------------
    grid_root::grid_root(const vector3& min, const vector3& max)
    {
        static const real EPSILON = values::epsilon() * 1000;
        static const int DIVNUM = KAZE_GRID_MAXDIV;

        vector3 wide((max - min) * (1 + EPSILON));
        //vector3 wide((max - min));

        int max_edge = 0;
        if (wide[max_edge] < wide[1]) max_edge = 1;
        if (wide[max_edge] < wide[2]) max_edge = 2;

//---------------
//0,1,2

#if 0
		ndiv_[0] = std::max<int>(1, int( ((wide[0]/wide[max_edge]) * DIVNUM) ));
		ndiv_[1] = std::max<int>(1, int( ((wide[1]/wide[max_edge]) * DIVNUM) ));
		ndiv_[2] = std::max<int>(1, int( ((wide[2]/wide[max_edge]) * DIVNUM) ));

		delta_[0] = wide[0]/ndiv_[0];
		delta_[1] = wide[1]/ndiv_[1];
		delta_[2] = wide[2]/ndiv_[2];
		
		idelta_[0] = ndiv_[0]/wide[0];
		idelta_[1] = ndiv_[1]/wide[1];
		idelta_[2] = ndiv_[2]/wide[2];
#else
    real delta = wide[max_edge] / DIVNUM;
    real idelta = DIVNUM / wide[max_edge];

    delta_[0] = delta;
    delta_[1] = delta;
    delta_[2] = delta;

    idelta_[0] = idelta;
    idelta_[1] = idelta;
    idelta_[2] = idelta;

    ndiv_[0] = std::max<int>(1, (int)ceil(wide[0] * idelta_[0]));
    ndiv_[1] = std::max<int>(1, (int)ceil(wide[1] * idelta_[1]));
    ndiv_[2] = std::max<int>(1, (int)ceil(wide[2] * idelta_[2]));

    ndiv_[0] = std::min<int>(DIVNUM, ndiv_[0]);
    ndiv_[1] = std::min<int>(DIVNUM, ndiv_[1]);
    ndiv_[2] = std::min<int>(DIVNUM, ndiv_[2]);

    ndiv_[max_edge] = DIVNUM;

#endif

        size_t sz = ndiv_[0] * ndiv_[1] * ndiv_[2];
        nodes_.resize(sz);
        std::memset(&nodes_[0], 0, sz * sizeof(octree_root*)); //set NULL
        this->min_ = min;
        this->max_ = max;
    }
    grid_root::~grid_root()
    {
        size_t sz = nodes_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (nodes_[i]) delete nodes_[i];
        }
    }

    void grid_root::set_face(oct_range range[3], face_iterator begin, face_iterator end, const bound& bnd, const mesh_accelerator_property& prop)
    {
        using namespace std;

        int delta[3];

        delta[0] = range[0][1] - range[0][0];
        delta[1] = range[1][1] - range[1][0];
        delta[2] = range[2][1] - range[2][0];

        if (is_nounit(delta))
        {
            int plane = 0;
            if (delta[plane] < delta[1]) plane = 1;
            if (delta[plane] < delta[2]) plane = 2;

            //1,2,3,,,4  3 1
            int div = range[plane][0] + (delta[plane] >> 1);
            oct_range crange[3];
            memcpy(crange, range, sizeof(crange));

            vector3 min, max;
            size_t ccsz;

            //-----------------------------------------
            real pos = min_[plane] + div * delta_[plane];

            face_iterator cbegin, cend;

            cbegin = begin;
            cend = partition(begin, end, pred_L(plane, pos, bnd));

            ccsz = distance(cbegin, cend);
            if (ccsz)
            {
                crange[plane][0] = range[plane][0]; //min = org
                crange[plane][1] = div;             //max = div
                set_face(crange, cbegin, cend, get_L(plane, pos, bnd), prop);
            }

            //-----------------------------------------

            //cbegin = begin;
            //cend   = partition(begin,end,pred_R(plane,pos,bound(min,max)));
            cbegin = partition(cbegin, cend, pred_L_ONLY(plane, pos, bnd));
            cend = end;

            ccsz = distance(cbegin, cend);
            if (ccsz)
            {
                crange[plane][0] = div;
                crange[plane][1] = range[plane][1]; //max = org
                set_face(crange, cbegin, cend, get_R(plane, pos, bnd), prop);
            }

            //-----------------------------------------
        }
        else
        { //1

            size_t idx = (size_t)((range[2][0] * ndiv_[1] + range[1][0]) * ndiv_[0] + range[0][0]);
            //size_t idx = range[2][0] * ndiv_[1]*ndiv_[0] + range[1][0] * ndiv_[0] + range[0][0];

            assert(idx < (size_t)(ndiv_[0] * ndiv_[1] * ndiv_[2]));

            std::unique_ptr<octree_root> ap(new octree_root(bnd.min(), bnd.max()));
            ap->add(begin, end, prop);

            if (nodes_[idx])
            {
                delete nodes_[idx];
            }

            nodes_[idx] = ap.release();
        }
    }

    void grid_root::add(face_iterator begin, face_iterator end, const mesh_accelerator_property& prop)
    {
        oct_range range[3];

        range[0][0] = 0;
        range[0][1] = ndiv_[0];
        range[1][0] = 0;
        range[1][1] = ndiv_[1];
        range[2][0] = 0;
        range[2][1] = ndiv_[2];

        vector3 wide;

        wide[0] = ndiv_[0] * delta_[0];
        wide[1] = ndiv_[1] * delta_[1];
        wide[2] = ndiv_[2] * delta_[2];

        this->set_face(range, begin, end, bound(min_, min_ + wide), prop);
    }

//#define CALC_TMIN

#define KZ_TEST(r, a, b) nodes[idx] && nodes[idx]->test_internal(r, a, b)
#define KZ_TEST_INFO(I, r, a, b) nodes[idx] && nodes[idx]->test_internal(I, r, a, b)

    bool grid_root::test(const ray& r, real dist) const
    {
        //get first intersection
        vector3 pos;
        if (!test_AABB(&pos, min_, max_, r, dist)) return false;

        const vector3& org = r.origin();
        const vector3& ird = r.inversed_direction();
        const int* const ndiv = this->ndiv_;
        const real* const delta = this->delta_;
        const real* const idelta = this->idelta_;

        int ipos[3]; //
        int step[3]; //
        real td[3];  //
        real tn[3];  //
        int out[3];

        for (int i = 0; i < 3; i++)
        {
            ipos[i] = static_cast<int>((pos[i] - min_[i]) * idelta[i]);
            //if(ipos[i] == ndiv[i])ipos[i]--;
            assert(ipos[i] != ndiv[i]);

            if (ird[i] > 0)
            { //plus
                step[i] = 1;
                td[i] = delta[i] * ird[i];
                tn[i] = ((min_[i] + (ipos[i] + 1) * delta[i]) - org[i]) * ird[i];
                out[i] = ndiv[i];
            }
            else if (ird[i] < 0)
            { //minus
                step[i] = -1;
                td[i] = -delta[i] * ird[i];
                tn[i] = ((min_[i] + (ipos[i]) * delta[i]) - org[i]) * ird[i];
                out[i] = -1;
            }
            else
            {
                step[i] = 0;
                td[i] = 0;
                tn[i] = values::far();
                out[i] = -1;
            }
        }

        //debug
        assert(ipos[0] >= 0);
        assert(ipos[1] >= 0);
        assert(ipos[2] >= 0);

        assert(ipos[0] < ndiv_[0]);
        assert(ipos[1] < ndiv_[1]);
        assert(ipos[2] < ndiv_[2]);
        //debug

        const P_C_TR* nodes = &(nodes_[0]);

        real tmin = 0;
        while (true)
        {
            //test matrix
            size_t idx = (size_t)((ipos[2] * ndiv[1] + ipos[1]) * ndiv[0] + ipos[0]);
//size_t idx = k * ndiv_[1]*ndiv_[0] + j * ndiv_[0] + i;
#ifdef CALC_TMIN
            tmin = oct_max4(tmin, tn[0] - td[0], tn[1] - td[1], tn[2] - td[2]);
#endif
            if (KZ_TEST(r, tmin, dist)) return true;

            int min_plane = 0;
            if (tn[min_plane] > tn[1]) min_plane = 1;
            if (tn[min_plane] > tn[2]) min_plane = 2;

            /* 3D DDA */
            if (dist < tn[min_plane]) break;
            ipos[min_plane] += step[min_plane];
            if (ipos[min_plane] == out[min_plane]) break;
            tn[min_plane] += td[min_plane];
        }

        return false;
    }

    bool grid_root::test(test_info* info, const ray& r, real dist) const
    {
        //get first intersection
        vector3 pos;
        if (!test_AABB(&pos, min_, max_, r, dist)) return false;

        const vector3& org = r.origin();
        const vector3& ird = r.inversed_direction();
        const int* const ndiv = this->ndiv_;
        const real* const delta = this->delta_;
        const real* const idelta = this->idelta_;

        int ipos[3];
        int step[3];
        real td[3];
        real tn[3];
        int out[3];

        for (int i = 0; i < 3; i++)
        {
            ipos[i] = static_cast<int>((pos[i] - min_[i]) * idelta[i]);
            //if(ipos[i] >= ndiv[i])ipos[i]--;
            assert(ipos[i] != ndiv[i]);

            if (ird[i] > 0)
            { //plus
                step[i] = 1;
                td[i] = delta[i] * ird[i];
                tn[i] = ((min_[i] + (ipos[i] + 1) * delta[i]) - org[i]) * ird[i];
                out[i] = ndiv[i];
            }
            else if (ird[i] < 0)
            { //minus
                step[i] = -1;
                td[i] = -delta[i] * ird[i];
                tn[i] = ((min_[i] + (ipos[i]) * delta[i]) - org[i]) * ird[i];
                out[i] = -1;
            }
            else
            {
                step[i] = 0;
                td[i] = 0;
                tn[i] = values::far();
                out[i] = -1;
            }
        }

        //debug
        assert(ipos[0] >= 0);
        assert(ipos[1] >= 0);
        assert(ipos[2] >= 0);

        assert(ipos[0] < ndiv_[0]);
        assert(ipos[1] < ndiv_[1]);
        assert(ipos[2] < ndiv_[2]);
        //debug

        const P_C_TR* nodes = &(nodes_[0]);

        bool bRet = false;

        real tmin = 0;
        while (true)
        {
            //test matrix
            size_t idx = (size_t)((ipos[2] * ndiv[1] + ipos[1]) * ndiv[0] + ipos[0]);
//size_t idx = k * ndiv_[1]*ndiv_[0] + j * ndiv_[0] + i;
#ifdef CALC_TMIN
            tmin = oct_max4(tmin, tn[0] - td[0], tn[1] - td[1], tn[2] - td[2]);
#endif
            if (KZ_TEST_INFO(info, r, tmin, dist))
            {
                bRet = true;
                dist = info->distance;
            }

            int min_plane = 0;
            if (tn[min_plane] > tn[1]) min_plane = 1;
            if (tn[min_plane] > tn[2]) min_plane = 2;

            /* 3D DDA */
            if (dist < tn[min_plane]) break;
            ipos[min_plane] += step[min_plane];
            if (ipos[min_plane] == out[min_plane]) break;
            tn[min_plane] += td[min_plane];
        }

        return bRet;
    }

#undef KZ_TEST
#undef KZ_TEST_INFO
    //---------------------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------
    //octree_root
    //-------------------------------------------------------------------------------------------------
    octree_root::octree_root(const vector3& min, const vector3& max) : min_(min), max_(max), root_(0) {}
    octree_root::~octree_root()
    {
        if (root_) delete root_;
    }
    void octree_root::add(face_iterator begin, face_iterator end, const mesh_accelerator_property& prop)
    {
        using namespace std;
        if (root_) delete root_;

        size_t csz = end - begin;
        int max_level = get_level(csz);

        root_ = create_octree_node(begin, end, bound(min_, max_), 0, max_level, prop);
    }

    //--------------
    bool octree_root::test(const ray& r, real dist) const
    {
        //if(!test_AABB(min_,max_,r,dist))return false;
        //return this->test_internal(r,dist);//
        distbound_AABB bnd;
        if (!test_AABB(&bnd, min_, max_, r, dist)) return false;
        return root_->test(
            bnd.tmin[0], bnd.tmin[1], bnd.tmin[2],
            bnd.tmax[0], bnd.tmax[1], bnd.tmax[2],
            r, 0, dist);
    }
    bool octree_root::test(test_info* info, const ray& r, real dist) const
    {
        //if(!test_AABB(min_,max_,r,dist))return false;
        //return this->test_internal(info,r,dist);
        distbound_AABB bnd;
        if (!test_AABB(&bnd, min_, max_, r, dist)) return false;
        return root_->test(
            bnd.tmin[0], bnd.tmin[1], bnd.tmin[2],
            bnd.tmax[0], bnd.tmax[1], bnd.tmax[2],
            info, r, 0, dist);
    }
    bool octree_root::test_internal(const ray& r, real tmin, real tmax) const
    {
        assert(root_);

        //real tx0,tx1,ty0,ty1,tz0,tz1;

        const vector3& org = r.origin();
        const vector3& idir = r.inversed_direction();
        int phase = r.phase();

        vector3 t0 = (min_ - org) * idir;
        vector3 t1 = (max_ - org) * idir;
        if (phase & 0x1) mswap<real>(t0[0], t1[0]);
        if (phase & 0x2) mswap<real>(t0[1], t1[1]);
        if (phase & 0x4) mswap<real>(t0[2], t1[2]);

        return root_->test(t0[0], t0[1], t0[2], t1[0], t1[1], t1[2], /**/ r, tmin, tmax);

        //if(oct_max3(tx0,ty0,tz0) < oct_min3(tx1,ty1,tz1) ){
        //	return root_->test(tx0,ty0,tz0,tx1,ty1,tz1,/**/r,dist);
        //}else{return false;}
    }
    bool octree_root::test_internal(test_info* info, const ray& r, real tmin, real tmax) const
    {
        assert(root_);

        //real tx0,tx1,ty0,ty1,tz0,tz1;

        const vector3& org = r.origin();
        const vector3& idir = r.inversed_direction();
        int phase = r.phase();

        vector3 t0 = (min_ - org) * idir;
        vector3 t1 = (max_ - org) * idir;
        if (phase & 1) mswap<real>(t0[0], t1[0]);
        if (phase & 2) mswap<real>(t0[1], t1[1]);
        if (phase & 4) mswap<real>(t0[2], t1[2]);

        return root_->test(t0[0], t0[1], t0[2], t1[0], t1[1], t1[2], info, r, tmin, tmax);

        //if(oct_max3(tx0,ty0,tz0) < oct_min3(tx1,ty1,tz1) ){
        //	return root_->test(tx0,ty0,tz0,tx1,ty1,tz1,info,r,dist);
        //}else{return false;}
    }

    //-------------------------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------------------------

    octree_mesh_accelerator::octree_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        std::unique_ptr<grid_root> ap(new grid_root(min, max));
        //std::unique_ptr<octree_root> ap(new octree_root(min,max));
        mesh_accelerator_property prop2 = prop;
#if KAZE_OCT_COMPRESS_FORCE
        prop2.compress = true;
#endif
        size_t csz = end - begin;
        prg.now = 0;
        prg.max = csz;
        prg.isz = 100.0 / csz;
        prg.cnt = 0;

        ap->add(begin, end, prop2);

        //print_log("face = %d,%d,node = %d\n",csz,ap->size(),prg.now);

        //print_log("octree_mesh_accelerator:memory size:%d bytes\n", ap->memory_size());
        imp_ = ap.release();
    }

    octree_mesh_accelerator::~octree_mesh_accelerator()
    {
        delete imp_;
    }

    bool octree_mesh_accelerator::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool octree_mesh_accelerator::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }

    vector3 octree_mesh_accelerator::min() const
    {
        return imp_->min();
    }

    vector3 octree_mesh_accelerator::max() const
    {
        return imp_->max();
    }

    size_t octree_mesh_accelerator::memory_size() const
    {
        return imp_->memory_size();
    }
}
