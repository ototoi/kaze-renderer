#ifndef KAZE_LBVH_UTILITY_H
#define KAZE_LBVH_UTILITY_H

#include "types.h"
#include "mesh_face.h"
#include <vector>

namespace kaze
{

    void create_morton_code(std::vector<uint32_t>& codes, const std::vector<vector3f>& points, const vector3f& min, const vector3f& max);
    void create_morton_code(uint32_t* codes, const float* coords, size_t fsz, const float min[3], const float max[3]);
    void create_morton_code(std::vector<uint32_t>& codes, const std::vector<vector3d>& points, const vector3d& min, const vector3d& max);
    void create_morton_code(uint32_t* codes, const double* coords, size_t fsz, const double min[3], const double max[3]);
    void create_morton_code(std::vector<uint32_t>& codes, const std::vector<const mesh_face*>& faces, const vector3& min, const vector3& max);
    void create_morton_code(std::vector<uint32_t>& codes, const mesh_face** const faces, size_t sz, const vector3& min, const vector3& max);

    void get_morton_bound(vector3& tmin, vector3& tmax, const vector3& min, const vector3& max);

    void sort_morton_code(std::vector<size_t>& indices, std::vector<uint32_t>& codes);
}

#endif