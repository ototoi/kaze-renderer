
#include "exhibited_mesh_accelerator.h"
#include "values.h"
#include "test_info.h"

#include "mesh_face.h"

#include <vector>

namespace kaze
{

    typedef const mesh_face* PCFACE;

    class exhibited_mesh_accelerator_imp
    {
    public:
        exhibited_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop);
        ~exhibited_mesh_accelerator_imp();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;
        size_t memory_size() const;

    protected:
        bool test(PCFACE f, const ray& r, real dist) const;
        bool test(PCFACE f, test_info* info, const ray& r, real dist) const;

    private:
        std::vector<PCFACE> v_;
        vector3 min_;
        vector3 max_;
        bool bDS_;
    };

    size_t exhibited_mesh_accelerator_imp::memory_size() const
    {
        return sizeof(exhibited_mesh_accelerator_imp) + v_.capacity() * sizeof(PCFACE);
    }

    exhibited_mesh_accelerator_imp::exhibited_mesh_accelerator_imp(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        v_.assign(begin, end);
        if (prop.singleside)
        {
            bDS_ = false;
        }
        else
        {
            bDS_ = true;
        }
        min_ = min;
        max_ = max;
    }
    exhibited_mesh_accelerator_imp::~exhibited_mesh_accelerator_imp() {}

    bool exhibited_mesh_accelerator_imp::test(const ray& r, real dist) const
    {
        size_t sz = v_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (test(v_[i], r, dist))
            {
                return true;
            }
        }
        return false;
    }
    bool exhibited_mesh_accelerator_imp::test(test_info* info, const ray& r, real dist) const
    {
        bool bDS = bDS_;
        bool bRet = false;
        size_t sz = v_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (test(v_[i], info, r, dist))
            {
                bRet = true;
                dist = info->get_distance();
            }
        }
        return bRet;
    }

    vector3 exhibited_mesh_accelerator_imp::min() const
    {
        return min_;
    }

    vector3 exhibited_mesh_accelerator_imp::max() const
    {
        return max_;
    }
    //---------------------------------------------

    bool exhibited_mesh_accelerator_imp::test(PCFACE f, const ray& r, real dist) const
    {
        if (bDS_)
        {
            return f->test_DS(r, dist);
        }
        else
        {
            return f->test_SS(r, dist);
        }
    }

    bool exhibited_mesh_accelerator_imp::test(PCFACE f, test_info* info, const ray& r, real dist) const
    {
        if (bDS_)
        {
            return f->test_DS(info, r, dist);
        }
        else
        {
            return f->test_SS(info, r, dist);
        }
    }

    //---------------------------------------------
    //---------------------------------------------
    //---------------------------------------------

    exhibited_mesh_accelerator::exhibited_mesh_accelerator(const vector3& min, const vector3& max, PCFACE* begin, PCFACE* end, const mesh_accelerator_property& prop)
    {
        imp_ = new exhibited_mesh_accelerator_imp(min, max, begin, end, prop);
    }

    exhibited_mesh_accelerator::~exhibited_mesh_accelerator()
    {
        delete imp_;
    }

    bool exhibited_mesh_accelerator::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool exhibited_mesh_accelerator::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }

    vector3 exhibited_mesh_accelerator::min() const
    {
        return imp_->min();
    }

    vector3 exhibited_mesh_accelerator::max() const
    {
        return imp_->max();
    }

    size_t exhibited_mesh_accelerator::memory_size() const
    {
        return imp_->memory_size();
    }
}
