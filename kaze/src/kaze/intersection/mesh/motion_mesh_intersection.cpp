#include "motion_mesh_intersection.h"
#include "motion_mesh_face.h"

#include <vector>

namespace kaze
{

    class motion_mesh_intersection_imp
    {
    public:
        motion_mesh_intersection_imp(const mesh_loader& ml, const parameter_map& param);
        ~motion_mesh_intersection_imp();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    private:
        std::vector<motion_mesh_face> mv_;
        vector3 min_;
        vector3 max_;
    };

    motion_mesh_intersection_imp::motion_mesh_intersection_imp(const mesh_loader& ml, const parameter_map& param)
    {
    }

    motion_mesh_intersection_imp::~motion_mesh_intersection_imp()
    {
    }

    bool motion_mesh_intersection_imp::test(const ray& r, real dist) const
    {

        return false;
    }

    bool motion_mesh_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {

        return false;
    }

    void motion_mesh_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
    }

    vector3 motion_mesh_intersection_imp::min() const
    {
        return min_;
    }

    vector3 motion_mesh_intersection_imp::max() const
    {
        return max_;
    }

    //--------------------------------------------------------------------------------------------------

    motion_mesh_intersection::motion_mesh_intersection(const mesh_loader& ml, const parameter_map& param)
    {
        imp_ = new motion_mesh_intersection_imp(ml, param);
    }

    motion_mesh_intersection::~motion_mesh_intersection()
    {
        delete imp_;
    }

    bool motion_mesh_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool motion_mesh_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }

    void motion_mesh_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        return imp_->finalize(info, r, dist);
    }

    vector3 motion_mesh_intersection::min() const
    {
        return imp_->min();
    }
    vector3 motion_mesh_intersection::max() const
    {
        return imp_->max();
    }
}
