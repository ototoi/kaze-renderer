#ifndef KAZE_MESH_FACE_H
#define KAZE_MESH_FACE_H

#include "types.h"
#include "ray.h"

#include "fp16_vector2.h"
#include "fp16_vector3.h"

namespace kaze
{

    class shader;
    class test_info;

    struct mesh_face_base
    {
        union
        {
            const vector3* p_p0;
            size_t i0;
        };
        union
        {
            const vector3* p_p1;
            size_t i1;
        };
        union
        {
            const vector3* p_p2;
            size_t i2;
        };
    };

    template <class BASE_FACE, class V>
    struct mesh_face_normal : public BASE_FACE
    {
        V n0;
        V n1;
        V n2;
    };

    template <class BASE_FACE, class V>
    struct mesh_face_uv : public BASE_FACE
    {
        V uv0;
        V uv1;
        V uv2;
    };

    struct mesh_face : public mesh_face_uv<mesh_face_normal<mesh_face_base, fp16_vector3>, fp16_vector2>
    {
        union
        {
            size_t i_attr;
            const shader* p_shader;
        };

        bool test_DS(const ray& r, real dist) const;
        bool test_DS(test_info* info, const ray& r, real dist) const;
        bool test_SS(const ray& r, real dist) const;
        bool test_SS(test_info* info, const ray& r, real dist) const;

        void finalize(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const vector3& e1, const vector3& e2, real u, real v) const;
    };

    struct mesh_face_freearea_struct
    {
        const mesh_face* face;
        real u;
        real v;
    };
}

#endif
