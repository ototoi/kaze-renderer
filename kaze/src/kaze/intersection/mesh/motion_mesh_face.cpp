#include "motion_mesh_face.h"
#include "values.h"
#include "test_info.h"

namespace kaze
{

    static inline vector2 lerp(const vector2& p0, const vector2& p1, real t)
    {
        return p0 * (t) + p1 * (1.0 - t);
    }
    static inline vector3 lerp(const vector3& p0, const vector3& p1, real t)
    {
        return p0 * (t) + p1 * (1.0 - t);
    }
    static vector3 safe_normalize(const vector3& v)
    {
        static const real EPSILON = values::epsilon();
        using namespace std;

        real l = v.sqr_length();
        if (l < EPSILON) return vector3(0, 0, 0);
        return v * (real(1) / sqrt(l));
    }

    bool motion_mesh_face::test_DS(const ray& r, real dist) const
    {
        real time = r.time();
        if (times[0] <= time && time <= times[1])
        {
            real t = (time - times[0]) / (times[1] - times[0]);

            vector3 P[3] =
                {
                    lerp(*(faces[0].p_p0), *(faces[1].p_p0), t),
                    lerp(*(faces[0].p_p1), *(faces[1].p_p1), t),
                    lerp(*(faces[0].p_p2), *(faces[1].p_p2), t)};

            mesh_face face = faces[0];
            face.p_p0 = P + 0;
            face.p_p1 = P + 1;
            face.p_p2 = P + 2;
            face.i_attr = index;

            return face.test_DS(r, dist);
        }
        return false;
    }

    bool motion_mesh_face::test_DS(test_info* info, const ray& r, real dist) const
    {
        real time = r.time();
        if (times[0] <= time && time <= times[1])
        {
            real t = (time - times[0]) / (times[1] - times[0]);

            vector3 P[3] =
                {
                    lerp(*(faces[0].p_p0), *(faces[1].p_p0), t),
                    lerp(*(faces[0].p_p1), *(faces[1].p_p1), t),
                    lerp(*(faces[0].p_p2), *(faces[1].p_p2), t)};

            vector3 N[3] =
                {
                    safe_normalize(lerp((faces[0].n0), (faces[1].n0), t)),
                    safe_normalize(lerp((faces[0].n1), (faces[1].n1), t)),
                    safe_normalize(lerp((faces[0].n2), (faces[1].n2), t))};

            vector2 C[3] =
                {
                    lerp(faces[0].uv0, faces[1].uv0, t),
                    lerp(faces[0].uv1, faces[1].uv1, t),
                    lerp(faces[0].uv2, faces[1].uv2, t)};

            mesh_face face = faces[0];
            face.p_p0 = P + 0;
            face.p_p1 = P + 1;
            face.p_p2 = P + 2;
            face.n0 = N[0];
            face.n1 = N[1];
            face.n2 = N[2];
            face.uv0 = C[0];
            face.uv1 = C[1];
            face.uv2 = C[2];

            if (face.test_DS(info, r, dist))
            {
                face.finalize(info, r, info->distance);
                info->index = index;
                return true;
            }
        }
        return false;
    }

    bool motion_mesh_face::test_SS(const ray& r, real dist) const
    {
        real time = r.time();
        if (times[0] <= time && time <= times[1])
        {
            real t = (time - times[0]) / (times[1] - times[0]);

            vector3 P[3] =
                {
                    lerp(*(faces[0].p_p0), *(faces[1].p_p0), t),
                    lerp(*(faces[0].p_p1), *(faces[1].p_p1), t),
                    lerp(*(faces[0].p_p2), *(faces[1].p_p2), t)};

            mesh_face face = faces[0];
            face.p_p0 = P + 0;
            face.p_p1 = P + 1;
            face.p_p2 = P + 2;
            face.i_attr = index;

            return face.test_SS(r, dist);
        }
        return false;
    }

    bool motion_mesh_face::test_SS(test_info* info, const ray& r, real dist) const
    {
        real time = r.time();
        if (times[0] <= time && time <= times[1])
        {
            real t = (time - times[0]) / (times[1] - times[0]);

            vector3 P[3] =
                {
                    lerp(*(faces[0].p_p0), *(faces[1].p_p0), t),
                    lerp(*(faces[0].p_p1), *(faces[1].p_p1), t),
                    lerp(*(faces[0].p_p2), *(faces[1].p_p2), t)};

            vector3 N[3] =
                {
                    safe_normalize(lerp((faces[0].n0), (faces[1].n0), t)),
                    safe_normalize(lerp((faces[0].n1), (faces[1].n1), t)),
                    safe_normalize(lerp((faces[0].n2), (faces[1].n2), t))};

            vector2 C[3] =
                {
                    lerp(faces[0].uv0, faces[1].uv0, t),
                    lerp(faces[0].uv1, faces[1].uv1, t),
                    lerp(faces[0].uv2, faces[1].uv2, t)};

            mesh_face face = faces[0];
            face.p_p0 = P + 0;
            face.p_p1 = P + 1;
            face.p_p2 = P + 2;
            face.n0 = N[0];
            face.n1 = N[1];
            face.n2 = N[2];
            face.uv0 = C[0];
            face.uv1 = C[1];
            face.uv2 = C[2];

            if (face.test_SS(info, r, dist))
            {
                face.finalize(info, r, info->distance);
                info->index = index;
                return true;
            }
        }
        return false;
    }

    vector3 motion_mesh_face::min() const
    {
        vector3 points[] = {
            *(faces[0].p_p0),
            *(faces[0].p_p1),
            *(faces[0].p_p2),
            *(faces[1].p_p0),
            *(faces[1].p_p1),
            *(faces[1].p_p2)};
        vector3 p = points[0];
        for (int i = 1; i < 6; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                p[j] = std::min(p[j], points[i][j]);
            }
        }
        return p;
    }

    vector3 motion_mesh_face::max() const
    {
        vector3 points[] = {
            *(faces[0].p_p0),
            *(faces[0].p_p1),
            *(faces[0].p_p2),
            *(faces[1].p_p0),
            *(faces[1].p_p1),
            *(faces[1].p_p2)};
        vector3 p = points[0];
        for (int i = 1; i < 6; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                p[j] = std::max(p[j], points[i][j]);
            }
        }
        return p;
    }
}
