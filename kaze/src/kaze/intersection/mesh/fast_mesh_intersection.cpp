#include "fast_mesh_intersection.h"

#include "test_info.h"
#include "mesh_face.h"
#include "logger.h"
#include "timer.h"

#include "mesh_accelerator.h"

#include "exhibited_mesh_accelerator.h"
#include "octree_mesh_accelerator.h"
#include "kdtree_mesh_accelerator.h"
#include "bih_mesh_accelerator.h"
#include "bvh_mesh_accelerator.h"

#include "qbvh_mesh_accelerator.h"

#include "hlqbvh_mesh_accelerator.h"
#include "plqbvh_mesh_accelerator.h"
#include "trqbvh_mesh_accelerator.h"

#include "sbvh_mesh_accelerator.h"

#include "mesh_utility.h"
#include "mesh_saver.h"

//#include "mmap_array.hpp"
#include <string>
#include <memory>
#include <stdexcept>

#if 0
#define PRINTLOG print_log
#else
static void print_dummy(const char* str, ...) {}
#define PRINTLOG print_dummy
#endif

namespace kaze
{

    namespace
    {
        template <class VLIST, class FLIST>
        class template_mesh_saver : public mesh_saver
        {
        public:
            template_mesh_saver(VLIST& vl, FLIST& fl) : vl_(vl), fl_(fl) {}
        public:
            bool set_vertices(size_t sz)
            {
                vl_.resize(sz);
                return true;
            }
            bool set_faces(size_t sz)
            {
                fl_.resize(sz);
                return true;
            }

            bool set_vertex(size_t i, io_mesh_vertex* v)
            {
                vector3& vout = *(&(vl_[i]));
                vout = vector3(v->pos);
                return true;
            }
            bool set_face(size_t i, io_mesh_face* f)
            {
                mesh_face& fout = *(&(fl_[i]));

                fout.i0 = size_t(f->i0);
                fout.i1 = size_t(f->i1);
                fout.i2 = size_t(f->i2);

                fout.n0 = vector3(f->n0);
                fout.n1 = vector3(f->n1);
                fout.n2 = vector3(f->n2);

                fout.uv0 = vector2(f->uv0);
                fout.uv1 = vector2(f->uv1);
                fout.uv2 = vector2(f->uv2);

                const shader* p_shader = (const shader*)(f->p_shader);
                if (p_shader)
                {
                    fout.i_attr = 0;
                    fout.p_shader = p_shader;
                }
                else
                {
                    fout.p_shader = NULL;
                    fout.i_attr = f->i_shader;
                }
                return true;
            }

        private:
            VLIST& vl_;
            FLIST& fl_;
        };
    }

#if 0 //!defined( _WIN32 )					//large
	typedef mmap_array<mesh_face>    face_list;
	typedef mmap_array<vector3>  vertex_list;
#else
    typedef std::vector<mesh_face> face_list;
    typedef std::vector<vector3> vertex_list;
#endif

    typedef const mesh_face* PCFACE;
    typedef mesh_face* PFACE;

    typedef template_mesh_saver<vertex_list, face_list> tm_mesh_saver;

    class fast_mesh_exception : public std::exception
    {
    public:
        fast_mesh_exception(const char* str) : str_(str) {}
        const char* what() const throw() { return str_.c_str(); } //"virtual" overloaded
        ~fast_mesh_exception() throw() {}
    private:
        std::string str_;
    };

    class fast_mesh_intersection_imp
    {
    public:
        fast_mesh_intersection_imp(const mesh_loader& ml, const parameter_map& param);
        ~fast_mesh_intersection_imp();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    public:
        bool get_vertices(size_t& sz) const;
        bool get_faces(size_t& sz) const;
        bool get_vertex(size_t i, io_mesh_vertex* v) const;
        bool get_face(size_t i, io_mesh_face* f) const;

    private:
        face_list fl_;
        vertex_list vl_;
        mesh_accelerator* root_;
    };

    fast_mesh_intersection_imp::fast_mesh_intersection_imp(const mesh_loader& ml, const parameter_map& param)
    {
        static const real EPSILON = values::epsilon() * 1024;

        kaze::timer t1;
        //---------------------------------------

        PRINTLOG("fast_mesh_intersection:loading start ...\n");
        t1.start();

        {
            tm_mesh_saver ms(vl_, fl_);

            if (!ml.load(&ms))
            {
                throw fast_mesh_exception("fast_mesh_intersection:[file open fail]");
            }
        }

        t1.end();
        PRINTLOG("fast_mesh_intersection:loading end %d ms\n", t1.msec());

        size_t vsz = vl_.size();
        size_t fsz = fl_.size();
        if (fsz == 0 || vsz == 0)
        {
            throw fast_mesh_exception("fast_mesh_intersection:[file open fail]");
        }

        //---------------------------------------

        PFACE fl = &(fl_[0]);
        vector3* vl = &(vl_[0]);

        PRINTLOG("fast_mesh_intersection:%u faces\n", fsz);
        PRINTLOG("fast_mesh_intersection:%u vertices\n", vsz);

        //---------------------------------------
        size_t memory_size = fsz * sizeof(mesh_face) + vsz * sizeof(vector3);
        PRINTLOG("fast_mesh_intersection:total %u bytes\n", memory_size);
        int nSzMode = 0; //0:normal, 1:gigantec
        if (memory_size > 100 * 1024 * 1024)
        { //todo:parameter
            nSzMode = 1;
        }
        //---------------------------------------
        PRINTLOG("fast_mesh_intersection:getting min-max start ...\n");
        t1.start();

        vector3 min, max;
        min = max = vl[0];
        for (size_t i = 1; i < vsz; i++)
        {
            if (vl[i][0] < min[0])
            {
                min[0] = vl[i][0];
            }
            if (vl[i][1] < min[1])
            {
                min[1] = vl[i][1];
            }
            if (vl[i][2] < min[2])
            {
                min[2] = vl[i][2];
            }

            if (max[0] < vl[i][0])
            {
                max[0] = vl[i][0];
            }
            if (max[1] < vl[i][1])
            {
                max[1] = vl[i][1];
            }
            if (max[2] < vl[i][2])
            {
                max[2] = vl[i][2];
            }
        }

        min -= vector3(EPSILON, EPSILON, EPSILON);
        max += vector3(EPSILON, EPSILON, EPSILON);

        t1.end();
        PRINTLOG("fast_mesh_intersection:getting min-max end %d ms\n", t1.msec());

        //---------------------------------------

        PRINTLOG("fast_mesh_intersection:calculating normals start ...\n");
        t1.start();

        real ang = param.get_double("SMOOTH_ANGLE", 30.0);

        int nCalcNormal = param.get_int("CALC_NORMALS", 1);

        if (ang <= 0)
        {
            bool bNormal = calc_normals_flat(&fl[0], fsz, &vl[0], vsz, values::radians(ang));
            if (!bNormal)
            {
                throw fast_mesh_exception("fast_mesh_intersection:[file open fail]");
            }
        }
        else
        {
            bool bNormal = true;
            if (nCalcNormal)
            {
                bNormal = calc_normals_facevarying(&fl[0], fsz, &vl[0], vsz, values::radians(ang));
                if (!bNormal)
                {
                    bNormal = calc_normals_flat(&fl[0], fsz, &vl[0], vsz, values::radians(ang));
                }
            }

            if (!bNormal)
            {
                throw fast_mesh_exception("fast_mesh_intersection:[file open fail]");
            }
        }

        t1.end();
        PRINTLOG("fast_mesh_intersection:calculating normals end %d ms\n", t1.msec());

        //---------------------------------------

        PRINTLOG("fast_mesh_intersection:converting index to pointer start ...\n");
        t1.start();
        for (size_t i = 0; i < fsz; i++)
        {
            fl[i].p_p0 = &(vl[fl[i].i0]);
            fl[i].p_p1 = &(vl[fl[i].i1]);
            fl[i].p_p2 = &(vl[fl[i].i2]);
        }
        t1.end();
        PRINTLOG("fast_mesh_intersection:converting index to pointer end %d ms\n", t1.msec());

        //PRINTLOG("%f,%f,%f, %f,%f,%f\n",min[0],min[1],min[2], max[0],max[1],max[2]);
        //---------------------------------------

        PRINTLOG("fast_mesh_intersection:making temporary start ...\n");
        t1.start();
        std::vector<PCFACE> ptmp(fsz);
        for (size_t i = 0; i < fsz; i++)
        {
            ptmp[i] = &(fl[i]);
        }
        t1.end();
        PRINTLOG("fast_mesh_intersection:making temporary end %d ms\n", t1.msec());

        PRINTLOG("fast_mesh_intersection:construction start ...\n");
        t1.start();
        mesh_accelerator_property prop;
        memset(&prop, 0, sizeof(prop));
        int nSingle = param.get_int("SINGLE_SIDE", 0);
        prop.singleside = (nSingle) ? true : false;

        std::string strAccelerator = param.get_string("MESH_ACCELERATOR");

        std::unique_ptr<mesh_accelerator> ar;
#define DEFACC(ACC) \
    if (strAccelerator == #ACC) ar.reset(new ACC##_mesh_accelerator(min, max, &(ptmp[0]), &(ptmp[0]) + fsz, prop))

        DEFACC(exhibited);
        DEFACC(octree);
        DEFACC(bvh);
        DEFACC(bih);
        
        DEFACC(kdtree);

        DEFACC(qbvh);

        DEFACC(hlqbvh);
        DEFACC(plqbvh);

        DEFACC(sbvh);

        DEFACC(trqbvh);

#undef DEFACC

        //ar.reset(new octree_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        //ar.reset(new bvh_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        //ar.reset(new bih_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        //ar.reset(new tritree_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        //ar.reset(new stack_kdtree_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        //ar.reset(new kdtree_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        //ar.reset(new compact_grid_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        //ar.reset(new vector_grid_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        //ar.reset(new engaged_grid_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        //ar.reset(new minimum_grid_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        //ar.reset(new mgh_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        //ar.reset(new qbvh_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        //ar.reset(new bbvh_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        //ar.reset(new hlbvh_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));

        if (!ar.get())
        {
            ar.reset(new hlqbvh_mesh_accelerator(min, max, &(ptmp[0]), &(ptmp[0]) + fsz, prop));
            //ar.reset(new sbvh_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
        }
        /*
		if(nSzMode==0){
			prop.compress = false;
			//ar.reset(new octree_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));	
		}else{//bigger
			prop.compress = true;
			//ar.reset(new mgh_mesh_accelerator(min,max,&(ptmp[0]),&(ptmp[0])+fsz, prop));
		}
		*/
        root_ = ar.release();

        t1.end();
        PRINTLOG("fast_mesh_intersection:construction end %d ms\n", t1.msec());
    }

    fast_mesh_intersection_imp::~fast_mesh_intersection_imp()
    {
        delete root_;
    }

    bool fast_mesh_intersection_imp::test(const ray& r, real dist) const
    {
        return root_->test(r, dist);
    }

    bool fast_mesh_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        return root_->test(info, r, dist);
    }

    void fast_mesh_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        assert(reinterpret_cast<mesh_face_freearea_struct*>(info->freearea)->face);
        mesh_face_freearea_struct* fsp = reinterpret_cast<mesh_face_freearea_struct*>(info->freearea);
        const mesh_face* p_face = fsp->face;
        p_face->finalize(info, r, dist);
        size_t index = p_face - &fl_[0];
        info->index = index;
    }

    vector3 fast_mesh_intersection_imp::min() const
    {
        return root_->min();
    }

    vector3 fast_mesh_intersection_imp::max() const
    {
        return root_->max();
    }

    bool fast_mesh_intersection_imp::get_vertices(size_t& sz) const
    {
        sz = vl_.size();
        return true;
    }

    bool fast_mesh_intersection_imp::get_faces(size_t& sz) const
    {
        sz = fl_.size();
        return true;
    }

    static void conv_v(double d[3], const vector3& v)
    {
        d[0] = (double)v[0];
        d[1] = (double)v[1];
        d[2] = (double)v[2];
    }

    static void conv_v(double d[2], const vector2& v)
    {
        d[0] = (double)v[0];
        d[1] = (double)v[1];
    }

    bool fast_mesh_intersection_imp::get_vertex(size_t i, io_mesh_vertex* v) const
    {
        if (i < vl_.size())
        {
            v->pos[0] = vl_[i][0];
            v->pos[1] = vl_[i][1];
            v->pos[2] = vl_[i][2];
            return true;
        }
        else
        {
            return false;
        }
    }

    bool fast_mesh_intersection_imp::get_face(size_t i, io_mesh_face* f) const
    {
        if (i < fl_.size())
        {
            size_t i0, i1, i2;
            i0 = fl_[i].p_p0 - &vl_[0];
            i1 = fl_[i].p_p1 - &vl_[0];
            i2 = fl_[i].p_p2 - &vl_[0];
            f->i0 = i0;
            f->i1 = i1;
            f->i2 = i2;
            conv_v(f->n0, (vector3)fl_[i].n0);
            conv_v(f->n1, (vector3)fl_[i].n1);
            conv_v(f->n2, (vector3)fl_[i].n2);

            conv_v(f->uv0, (vector2)fl_[i].uv0);
            conv_v(f->uv1, (vector2)fl_[i].uv1);
            conv_v(f->uv2, (vector2)fl_[i].uv2);

            return true;
        }
        else
        {
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    //----------------------------------------------------------------------------------------------

    fast_mesh_intersection::fast_mesh_intersection(const mesh_loader& ml, const parameter_map& param)
    {
        imp_ = new fast_mesh_intersection_imp(ml, param);
    }

    fast_mesh_intersection::~fast_mesh_intersection()
    {
        delete imp_;
    }

    bool fast_mesh_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool fast_mesh_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void fast_mesh_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        imp_->finalize(info, r, dist);
    }

    vector3 fast_mesh_intersection::min() const
    {
        return imp_->min();
    }

    vector3 fast_mesh_intersection::max() const
    {
        return imp_->max();
    }

    //-----------------------------------------
    bool fast_mesh_intersection::get_vertices(size_t& sz) const
    {
        return imp_->get_vertices(sz);
    }

    bool fast_mesh_intersection::get_faces(size_t& sz) const
    {
        return imp_->get_faces(sz);
    }

    bool fast_mesh_intersection::get_vertex(size_t i, io_mesh_vertex* v) const
    {
        return imp_->get_vertex(i, v);
    }

    bool fast_mesh_intersection::get_face(size_t i, io_mesh_face* f) const
    {
        return imp_->get_face(i, f);
    }

    //-----------------------------------------
}
