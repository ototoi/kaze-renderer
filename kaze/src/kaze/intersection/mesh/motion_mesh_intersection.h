#ifndef KAZE_MOTION_MESH_INTERSECTION_H
#define KAZE_MOTION_MESH_INTERSECTION_H

#include "mesh_intersection.h"
#include "io_mesh.h"
#include "transformer.h"
#include "parameter_map.h"

#include "mesh_loader.h"

namespace kaze
{

    class motion_mesh_intersection_imp;

    class motion_mesh_intersection : public mesh_intersection
    {
    public:
        motion_mesh_intersection(const mesh_loader& ml, const parameter_map& param);
        ~motion_mesh_intersection();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    private:
        motion_mesh_intersection_imp* imp_;
    };
}

#endif