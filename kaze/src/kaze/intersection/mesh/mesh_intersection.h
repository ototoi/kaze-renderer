#ifndef KAZE_MESH_INTERSECTION_H
#define KAZE_MESH_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class mesh_intersection : public bounded_intersection
    {
    public:
        virtual ~mesh_intersection() {}
        virtual bool test(const ray& r, real dist) const = 0;
        virtual bool test(test_info* info, const ray& r, real dist) const = 0;
        virtual void finalize(test_info* info, const ray& r, real dist) const = 0;

        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;
    };
}

#endif