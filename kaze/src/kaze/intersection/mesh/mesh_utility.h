#ifndef KAZE_MESH_UTILITY_H
#define KAZE_MESH_UTILITY_H

#include "mesh_face.h"

namespace kaze
{

    bool calc_normals(mesh_face* fl, size_t fsz, const vector3* vl, size_t vsz, real limit_angle);
    bool calc_normals_flat(mesh_face* fl, size_t fsz, const vector3* vl, size_t vsz, real limit_angle);
    bool calc_normals_fast(mesh_face* fl, size_t fsz, const vector3* vl, size_t vsz, real limit_angle);
    bool calc_normals_safety(mesh_face* fl, size_t fsz, const vector3* vl, size_t vsz, real limit_angle);
    bool calc_normals_facevarying(mesh_face* fl, size_t fsz, const vector3* vl, size_t vsz, real limit_angle);

    //Tomas Moller's polygon VS box check functions
    bool planeBoxOverlap(const vector3& normal, const vector3& vert, const vector3& maxbox);
    bool triBoxOverlap(const vector3& boxcenter, const vector3& boxhalfsize, const vector3& vtx0, const vector3& vtx1, const vector3& vtx2);
}

#endif
