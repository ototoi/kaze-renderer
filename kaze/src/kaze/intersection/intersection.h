#ifndef KAZE_INTERSECTION_H
#define KAZE_INTERSECTION_H

#include "ray.h"

namespace kaze
{

    class test_info;

    class intersection
    {
    public:
        virtual ~intersection() {}

        virtual bool test(const ray& r, real dist) const = 0;
        virtual bool test(test_info* info, const ray& r, real dist) const = 0;
        virtual void finalize(test_info* info, const ray& r, real dist) const = 0;

        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;
    };

    bool test_AABB_simple(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax);
    bool test_AABB_fast(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax);
    bool test_AABB_pluecker(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax);
    bool test_AABB(const vector3& min, const vector3& max, const ray& r, real tmin, real tmax);

    bool test_AABB(real* out_dist, const vector3& min, const vector3& max, const ray& r, real tmin, real tmax);
    bool test_AABB(vector3* out_point, const vector3& min, const vector3& max, const ray& r, real tmin, real tmax);

    inline bool test_AABB_simple(const vector3& min, const vector3& max, const ray& r, real dist) { return test_AABB_simple(min, max, r, 0, dist); }
    inline bool test_AABB_fast(const vector3& min, const vector3& max, const ray& r, real dist) { return test_AABB_fast(min, max, r, 0, dist); }
    inline bool test_AABB_pluecker(const vector3& min, const vector3& max, const ray& r, real dist) { return test_AABB_pluecker(min, max, r, 0, dist); }
    inline bool test_AABB(const vector3& min, const vector3& max, const ray& r, real dist) { return test_AABB(min, max, r, 0, dist); }

    inline bool test_AABB(real* out_dist, const vector3& min, const vector3& max, const ray& r, real dist) { return test_AABB(out_dist, min, max, r, 0, dist); }
    inline bool test_AABB(vector3* out_point, const vector3& min, const vector3& max, const ray& r, real dist) { return test_AABB(out_point, min, max, r, 0, dist); }

    bool is_bounded(const intersection& inter);
}

#endif
