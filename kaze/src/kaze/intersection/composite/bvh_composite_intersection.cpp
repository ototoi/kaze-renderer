#include "bvh_composite_intersection.h"

#include "test_info.h"

#include <vector>

#include "intersection_ex.h"

#include "logger.h"


#include <memory>

namespace kaze
{

    typedef const intersection* PCINTER;
    typedef PCINTER* INTER_ITERATOR;

    //-------------------------------------------------------------------------------
    namespace
    {

        class bvh_node
        {
        public:
            virtual ~bvh_node() {}
        public:
            virtual bool test(const ray& r, real tmin, real tmax) const = 0;
            virtual bool test(test_info* info, const ray& r, real tmin, real tmax) const = 0;
            virtual vector3 min() const = 0;
            virtual vector3 max() const = 0;
        };

        class bvh_node_null : public bvh_node
        {
        public:
            bool test(const ray& r, real tmin, real tmax) const { return false; }
            bool test(test_info* info, const ray& r, real tmin, real tmax) const { return false; }
            vector3 min() const { return vector3(0, 0, 0); }
            vector3 max() const { return vector3(0, 0, 0); }
        };

        class bvh_node_branch : public bvh_node
        {
        public:
            bvh_node_branch(INTER_ITERATOR s, INTER_ITERATOR e);
            ~bvh_node_branch();

        protected:
            void initialize(INTER_ITERATOR s, INTER_ITERATOR e);

        public:
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;
            vector3 min() const;
            vector3 max() const;

        protected:
            bool test_internal(const ray& r, real tmin, real tmax) const;
            bool test_internal(test_info* info, const ray& r, real tmin, real tmax) const;

        private:
            int plane_;
            bvh_node* nodes_[2];
            vector3 min_;
            vector3 max_;
        };

        class bvh_node_leaf : public bvh_node
        {
        public:
            bvh_node_leaf(INTER_ITERATOR s, INTER_ITERATOR e);
            ~bvh_node_leaf();

        public:
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;
            vector3 min() const;
            vector3 max() const;

        private:
            PCINTER p_inter_;
        };

        //------------------------------------------------------------
        //------------------------------------------------------------
        inline int eval_phase(int phase, int axis)
        { //rid<0
            return (phase >> axis) & 0x1;
        }

        bvh_node_branch::bvh_node_branch(INTER_ITERATOR s, INTER_ITERATOR e)
        {
            nodes_[0] = 0;
            nodes_[1] = 0;
            initialize(s, e);
        }

        bvh_node_branch::~bvh_node_branch()
        {
            if (nodes_[0]) delete nodes_[0];
            if (nodes_[1]) delete nodes_[1];
        }

        struct bvh_sorter : public std::binary_function<PCINTER, PCINTER, bool>
        {
            bvh_sorter(int plane) : plane_(plane) {}
            bool operator()(PCINTER a, PCINTER b) const
            {
                int plane = plane_;
                real ac = (a->min()[plane] + a->max()[plane]);
                real bc = (b->min()[plane] + b->max()[plane]);
                return ac < bc;
            }
            int plane_;
        };

        void bvh_node_branch::initialize(INTER_ITERATOR s, INTER_ITERATOR e)
        {
            static const real EPSILON = values::epsilon() * 1024;

            vector3 min, max;
            min = (*s)->min();
            max = (*s)->max();
            {
                INTER_ITERATOR it = s + 1;
                while (it != e)
                {
                    vector3 cmin = (*it)->min();
                    vector3 cmax = (*it)->max();
                    for (int i = 0; i < 3; i++)
                    {
                        min[i] = std::min(min[i], cmin[i]);
                        max[i] = std::max(max[i], cmax[i]);
                    }
                    it++;
                }
            }

            min -= vector3(EPSILON, EPSILON, EPSILON);
            max += vector3(EPSILON, EPSILON, EPSILON);

            vector3 wid = max - min;

            int plane = 0;
            if (wid[plane] < wid[1]) plane = 1;
            if (wid[plane] < wid[2]) plane = 2;

            std::sort(s, e, bvh_sorter(plane));

            size_t sz = e - s;
            size_t msz = sz >> 1;

            assert(sz > 0);

            INTER_ITERATOR m = s + msz;

            std::unique_ptr<bvh_node> node_l;
            std::unique_ptr<bvh_node> node_r;

            size_t lsz = m - s;
            size_t rsz = e - m;

            if (lsz)
            {
                if (lsz == 1)
                {
                    node_l.reset(new bvh_node_leaf(s, m));
                }
                else
                {
                    node_l.reset(new bvh_node_branch(s, m));
                }
            }

            if (rsz)
            {
                if (rsz == 1)
                {
                    node_r.reset(new bvh_node_leaf(m, e));
                }
                else
                {
                    node_r.reset(new bvh_node_branch(m, e));
                }
            }

            nodes_[0] = node_l.release();
            nodes_[1] = node_r.release();

            plane_ = plane;
            min_ = min;
            max_ = max;
        }

        bool bvh_node_branch::test(const ray& r, real tmin, real tmax) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, tmin, tmax))
            {
                tmin = std::max<real>(tmin, rng.tmin);
                tmax = std::min<real>(tmax, rng.tmax);
                return this->test_internal(r, tmin, tmax);
            }
            return false;
        }

        bool bvh_node_branch::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, tmin, tmax))
            {
                tmin = std::max<real>(tmin, rng.tmin);
                tmax = std::min<real>(tmax, rng.tmax);
                return this->test_internal(info, r, tmin, tmax);
            }
            return false;
        }

        bool bvh_node_branch::test_internal(const ray& r, real tmin, real tmax) const
        {
            int phase = r.phase();
            int plane = plane_;
            int nNear = eval_phase(phase, plane);
            int nFar = 1 - nNear;

            if (nodes_[nNear] && nodes_[nNear]->test(r, tmin, tmax)) return true;
            if (nodes_[nFar] && nodes_[nFar]->test(r, tmin, tmax)) return true;

            return false;
        }

        bool bvh_node_branch::test_internal(test_info* info, const ray& r, real tmin, real tmax) const
        {
            int phase = r.phase();
            int plane = plane_;
            int nNear = eval_phase(phase, plane);
            int nFar = 1 - nNear;

            bool bRet = false;
            if (nodes_[nNear] && nodes_[nNear]->test(info, r, tmin, tmax))
            {
                tmax = info->distance;
                bRet = true;
            }

            if (nodes_[nFar] && nodes_[nFar]->test(info, r, tmin, tmax))
            {
                tmax = info->distance;
                bRet = true;
            }

            return bRet;
        }

        vector3 bvh_node_branch::min() const
        {
            return min_;
        }

        vector3 bvh_node_branch::max() const
        {
            return max_;
        }

        //------------------------------------------------------------
        //------------------------------------------------------------

        bvh_node_leaf::bvh_node_leaf(INTER_ITERATOR s, INTER_ITERATOR e)
        {
            assert((e - s) == 1);
            p_inter_ = *s;
        }

        bvh_node_leaf::~bvh_node_leaf()
        {
            //
        }

        bool bvh_node_leaf::test(const ray& r, real tmin, real tmax) const
        {
            return p_inter_->test(r, tmax);
        }

        bool bvh_node_leaf::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
            return p_inter_->test(info, r, tmax);
        }

        vector3 bvh_node_leaf::min() const
        {
            return p_inter_->min();
        }

        vector3 bvh_node_leaf::max() const
        {
            return p_inter_->max();
        }

        //------------------------------------------------------------
        //------------------------------------------------------------
    }
    //-------------------------------------------------------------------------------
    class bvh_composite_intersection_imp
    {
    public:
        bvh_composite_intersection_imp();
        ~bvh_composite_intersection_imp();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        //void finalize(test_info * info, const ray& r, real dist)const;// -- //NO IMPLEMENT!

        vector3 min() const;
        vector3 max() const;

    public:
        void construct();
        void add(const std::shared_ptr<intersection>& inter);

    private:
        bvh_node* root_;
        std::vector<std::shared_ptr<intersection> > mv_;
    };

    //-------------------------------------------------------------------------------

    bvh_composite_intersection_imp::bvh_composite_intersection_imp()
    {
        root_ = new bvh_node_null();
    }

    bvh_composite_intersection_imp::~bvh_composite_intersection_imp()
    {
        delete root_;
    }

    bool bvh_composite_intersection_imp::test(const ray& r, real dist) const
    {
        return root_->test(r, 0, dist);
    }

    bool bvh_composite_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        return root_->test(info, r, 0, dist);
    }

    vector3 bvh_composite_intersection_imp::min() const
    {
        return root_->min();
    }

    vector3 bvh_composite_intersection_imp::max() const
    {
        return root_->max();
    }

    void bvh_composite_intersection_imp::add(const std::shared_ptr<intersection>& inter)
    {
        mv_.push_back(inter);
    }

    void bvh_composite_intersection_imp::construct()
    {
        size_t sz = mv_.size();
        if (sz == 0) return;
        std::vector<const intersection*> tmp;
        for (size_t i = 0; i < sz; i++)
        {
            tmp.push_back(mv_[i].get());
        }
        std::unique_ptr<bvh_node_branch> ap(new bvh_node_branch(&tmp[0], &tmp[0] + sz));

        if (root_) delete root_;
        root_ = ap.release();
    }

    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------
    //-------------------------------------------------------------------------------

    bvh_composite_intersection::bvh_composite_intersection()
    {
        imp_ = new bvh_composite_intersection_imp();
    }

    bvh_composite_intersection::~bvh_composite_intersection()
    {
        delete imp_;
    }

    bool bvh_composite_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool bvh_composite_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }

    void bvh_composite_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        // -- //NO IMPLEMENT!
    }

    vector3 bvh_composite_intersection::min() const
    {
        return imp_->min();
    }

    vector3 bvh_composite_intersection::max() const
    {
        return imp_->max();
    }

    void bvh_composite_intersection::construct()
    {
        imp_->construct();
    }

    void bvh_composite_intersection::add(const std::shared_ptr<intersection>& inter)
    {
        imp_->add(inter);
    }
}
