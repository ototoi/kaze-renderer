/**
 * @file bih_composite_intersection.cpp
 * @author Toru Matsuoka/ototoi
 * @brief Defines Object Maneger with Kd-tree struction. 
 */

#include "bih_composite_intersection.h"
#include "test_info.h"

#include "intersection_ex.h"

#include "logger.h"

#include <memory>
#include <vector>
#include <algorithm>
#include <cassert>
#include <cmath>

#define KAZE_BIH_INTER_USE_MINMAX 0
#define KAZE_BIH_CHILD_MINMAX 0

namespace kaze
{

    namespace
    {

        typedef const intersection* PCINT;

        class bound
        {
        public:
            bound(const vector3& min, const vector3& max) : min_(min), max_(max) {}

            const vector3& min() const { return min_; }
            const vector3& max() const { return max_; }
        private:
            vector3 min_;
            vector3 max_;
        };

        struct bih_bound_set
        { //Inter_Index with MinMax
            PCINT inter;
            vector3 min;
            vector3 max;
        };

        inline double log2(double x)
        {
            using namespace std;
            static const double LOG2 = log(2.0);
            return log(x) / LOG2;
        }
        int get_level(size_t face_num)
        {
            using namespace std;
            double f = log2(double(face_num));
            int nRet = (int)(ceil(f));

            return nRet * 2;
        }

        class bih_node
        {
        public:
            virtual bool test(const ray& r, real tmin, real tmax) const = 0;
            virtual bool test(test_info* info, const ray& r, real tmin, real tmax) const = 0;

        public:
            virtual ~bih_node() {}
        };

        class bih_node_null : public bih_node
        {
        public:
            bool test(const ray& r, real tmin, real tmax) const { return false; }
            bool test(test_info* info, const ray& r, real tmin, real tmax) const { return false; }
        };

        class bih_node_leaf : public bih_node
        {
        public:
            bih_node_leaf(const std::vector<const bih_bound_set*>& array);

        public:
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;

        private:
            std::vector<PCINT> inter_;
        };

        class bih_node_branch : public bih_node
        {
        public:
            bih_node_branch(const std::vector<const bih_bound_set*>& array, const bound& bnd, int level, int max_level);
            ~bih_node_branch()
            {
                if (left_) delete left_;
                if (right_) delete right_;
            }

        public:
            bool test(const ray& r, real tmin, real tmax) const;
            bool test(test_info* info, const ray& r, real tmin, real tmax) const;

        private:
            int axis_;
            real clip_[2];

            bih_node* left_;
            bih_node* right_;
        };

        //--------------------------------------------------------------------------
        //--------------------------------------------------------------------------

        bih_node_leaf::bih_node_leaf(const std::vector<const bih_bound_set*>& array)
        {
            size_t sz = array.size();
            inter_.resize(sz);
            for (size_t i = 0; i < sz; i++)
            {
                inter_[i] = array[i]->inter;
            }
        }

        bool bih_node_leaf::test(const ray& r, real tmin, real tmax) const
        {
            const PCINT* array = &(inter_[0]);
            size_t sz = inter_.size();
            for (size_t i = 0; i < sz; i++)
            {
                if (array[i]->test(r, tmax)) return true;
            }
            return false;
        }

        bool bih_node_leaf::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
            bool bRet = false;

            const PCINT* array = &(inter_[0]);
            size_t sz = inter_.size();
            for (size_t i = 0; i < sz; i++)
            {
                if (array[i]->test(info, r, tmax))
                {
                    tmax = info->distance;
                    bRet = true;
                }
            }
            return bRet;
        }

        //--------------------------------------------------------------------------
        //--------------------------------------------------------------------------
        namespace
        {
            class bih_min_sorter : public std::binary_function<const bih_bound_set*, const bih_bound_set*, bool>
            {
            public:
                bih_min_sorter(int axis) : axis_(axis) {}

                bool operator()(const bih_bound_set* a, const bih_bound_set* b) const
                {
                    int axis = this->axis_;
                    return (a->min[axis] < b->min[axis]);
                }

            private:
                int axis_;
            };

            class bih_max_sorter : public std::binary_function<const bih_bound_set*, const bih_bound_set*, bool>
            {
            public:
                bih_max_sorter(int axis) : axis_(axis) {}

                bool operator()(const bih_bound_set* a, const bih_bound_set* b) const
                {
                    int axis = this->axis_;
                    return (a->max[axis] > b->max[axis]);
                }

            private:
                int axis_;
            };

            struct delta_area
            {
                real delta;
                real area;
                int nMax;
            };
            inline int prg_1(int x)
            {
                static const int table[] = {1, 2, 0};
                return table[x];
            }
            inline int prg_2(int x)
            {
                static const int table[] = {2, 0, 1};
                return table[x];
            }

            real get_min(const std::vector<const bih_bound_set*>& array, int axis)
            {
                real val = +values::far();

                size_t sz = array.size();
                for (size_t i = 0; i < sz; i++)
                {
                    if (val > array[i]->min[axis]) val = array[i]->min[axis];
                }

                return val;
            }

            real get_max(const std::vector<const bih_bound_set*>& array, int axis)
            {
                real val = -values::far();

                size_t sz = array.size();
                for (size_t i = 0; i < sz; i++)
                {
                    if (val < array[i]->max[axis]) val = array[i]->max[axis];
                }

                return val;
            }
        }

        bih_node_branch::bih_node_branch(const std::vector<const bih_bound_set*>& array, const bound& bnd, int level, int max_level)
        {
            static const real FAR = values::far();
            static const real EPSILON = values::epsilon() * 1024;

            std::unique_ptr<bih_node> ap_l;
            std::unique_ptr<bih_node> ap_r;

            size_t sz = array.size(); //
            vector3 min = bnd.min();
            vector3 max = bnd.max();
            vector3 wide = max - min;

            real pos;
            int axis;
            real clip[2];

            axis = 0;
            if (wide[1] > wide[axis]) axis = 1;
            if (wide[2] > wide[axis]) axis = 2;

            pos = wide[axis] * 0.5;

            clip[0] = bnd.min()[axis];
            clip[1] = bnd.max()[axis];

            std::vector<const bih_bound_set*> bound_set[2];

            for (size_t i = 0; i < sz; i++)
            {
                real center = (array[i]->min[axis] + array[i]->max[axis]) * 0.5;
                if (center < pos)
                {
                    bound_set[0].push_back(array[i]);
                }
                else
                {
                    bound_set[1].push_back(array[i]);
                }
            }

            if (!bound_set[0].empty())
            {
                size_t csz = bound_set[0].size();
                if ((level + 1 < max_level) && (csz > 1))
                {
                    vector3 cmin = min;
                    vector3 cmax = max;
                    cmax[axis] = pos;
                    ap_l.reset(new bih_node_branch(bound_set[0], bound(cmin, cmax), level + 1, max_level));
                }
                else
                {
                    ap_l.reset(new bih_node_leaf(bound_set[0]));
                }
                clip[0] = get_max(bound_set[0], axis);
            }
            if (!bound_set[1].empty())
            {
                size_t csz = bound_set[1].size();
                if ((level + 1 < max_level) && (csz > 1))
                {
                    vector3 cmin = min;
                    cmin[axis] = pos;
                    vector3 cmax = max;
                    ap_r.reset(new bih_node_branch(bound_set[1], bound(cmin, cmax), level + 1, max_level));
                }
                else
                {
                    ap_r.reset(new bih_node_leaf(bound_set[1]));
                }
                clip[1] = get_min(bound_set[1], axis);
            }

            axis_ = axis;
            memcpy(clip_, clip, sizeof(real) * 2);

            left_ = ap_l.release();
            right_ = ap_r.release();
        }

        namespace
        {
#if 0
		inline int eval_phase(int phase,int axis){//rid<0
			return (phase>>axis) & 0x1;
		}
#else
            inline int eval_phase(int phase, int axis)
            { //rid<0
                static const int table[][3] = {
                    {0, 0, 0}, //0
                    {1, 0, 0}, //1
                    {0, 1, 0}, //2
                    {1, 1, 0}, //3
                    {0, 0, 1}, //4
                    {1, 0, 1}, //5
                    {0, 1, 1}, //6
                    {1, 1, 1}  //7
                };
                return table[phase][axis];
            }
#endif
        }

#define TEST_(p, r, min, max) p->test(r, min, max)
#define TESTINFO_(p, i, r, min, max) p->test(i, r, min, max)

        bool bih_node_branch::test(const ray& r, real tmin, real tmax) const
        {
            const vector3& org = r.origin();
            const vector3& idir = r.inversed_direction();
            int phase = r.phase();

            int axis = this->axis_;

            real ro = org[axis];
            real rid = idir[axis];

            //real clip[2];
            //memcpy(clip,clip_,sizeof(real)*2);

            const bih_node* nodes[2];
            nodes[0] = left_;
            nodes[1] = right_;

            int n_s = eval_phase(phase, axis);
            int f_s = 1 - n_s;

            real tpos[2];
            tpos[0] = (clip_[0] - ro) * rid;
            tpos[1] = (clip_[1] - ro) * rid;

            if (nodes[n_s] && tmin <= tpos[n_s])
            {
                if (TEST_(nodes[n_s], r, tmin, std::min<real>(tmax, tpos[n_s]))) return true;
            }
            if (nodes[f_s] && tpos[f_s] <= tmax)
            {
                if (TEST_(nodes[f_s], r, std::max<real>(tmin, tpos[f_s]), tmax)) return true;
            }

            return false;
        }

        bool bih_node_branch::test(test_info* info, const ray& r, real tmin, real tmax) const
        {
            const vector3& org = r.origin();
            const vector3& idir = r.inversed_direction();
            int phase = r.phase();

            int axis = this->axis_;

            real ro = org[axis];
            real rid = idir[axis];

            //real clip[2];
            //memcpy(clip,clip_,sizeof(real)*2);

            const bih_node* nodes[2];
            nodes[0] = left_;
            nodes[1] = right_;

            int n_s = eval_phase(phase, axis);
            int f_s = 1 - n_s;

            real tpos[2];
            tpos[0] = (clip_[0] - ro) * rid;
            tpos[1] = (clip_[1] - ro) * rid;

            bool bRet = false;

            if (nodes[n_s] && tmin <= tpos[n_s])
            {
                if (TESTINFO_(nodes[n_s], info, r, tmin, std::min<real>(tmax, tpos[n_s])))
                {
                    bRet = true;
                    tmax = info->distance;
                }
            }

            if (nodes[f_s] && tpos[f_s] <= tmax)
            {
                if (TESTINFO_(nodes[f_s], info, r, std::max<real>(tmin, tpos[f_s]), tmax))
                {
                    bRet = true;
                    tmax = info->distance; //no need
                }
            }

            return bRet;
        }
    }

    //-----------------------------------------------
    typedef bih_composite_intersection::inter_type inter_type;

    class bih_composite_intersection_imp
    {
        typedef const intersection* inter_type;

    public:
        bih_composite_intersection_imp();
        ~bih_composite_intersection_imp();

        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;

        vector3 min() const { return min_; }
        vector3 max() const { return max_; }
    public:
        void construct();
        void add(const std::shared_ptr<intersection>& inter);

    private:
        bih_node* root_;
        std::vector<std::shared_ptr<intersection> > mv_;
        vector3 min_;
        vector3 max_;
    };

    //--------------------------------------------------------------------------
    //--------------------------------------------------------------------------

    bih_composite_intersection_imp::bih_composite_intersection_imp()
    {
        root_ = new bih_node_null();
        //real far = values::far();
        min_ = vector3(0, 0, 0);
        max_ = vector3(0, 0, 0);
    }

    bih_composite_intersection_imp::~bih_composite_intersection_imp()
    {
        delete root_;
    }

    bool bih_composite_intersection_imp::test(const ray& r, real dist) const
    {
#if 0
		real tmin;
		if(test_AABB(&tmin,min_,max_,r,dist)){
			return root_->test(r,tmin,dist);
		}
#else
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, dist))
        {
            return root_->test(r, std::max(real(0), rng.tmin), std::min(rng.tmax, dist));
        }
#endif
        return false;
    }

    bool bih_composite_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
#if 0
		real tmin;
		if(test_AABB(&tmin,min_,max_,r,dist)){
			return root_->test(info,r,tmin,dist);
		}
#else
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, dist))
        {
            return root_->test(info, r, std::max(real(0), rng.tmin), std::min(rng.tmax, dist));
        }
#endif
        return false;
    }

    void bih_composite_intersection_imp::construct()
    {
        size_t sz = mv_.size();
        std::vector<bih_bound_set> bounds(sz);
        for (size_t i = 0; i < sz; i++)
        {
            bounds[i].inter = mv_[i].get();
            bounds[i].min = mv_[i]->min();
            bounds[i].max = mv_[i]->max();
        }

        //Get MINMAX
        vector3 vmin = this->min();
        vector3 vmax = this->max();
        //Get MINMAX

        std::vector<const bih_bound_set*> p_bounds(sz);
        for (size_t i = 0; i < sz; i++)
        {
            p_bounds[i] = &(bounds[i]);
        }

        std::unique_ptr<bih_node_branch> ap(
            new bih_node_branch(p_bounds, bound(vmin, vmax), 0, get_level(sz)));

        delete root_;         //
        root_ = ap.release(); //
    }

    void bih_composite_intersection_imp::add(const std::shared_ptr<intersection>& inter)
    {

        vector3 min = inter->min();
        vector3 max = inter->max();

        if (mv_.empty())
        {
            min_ = min;
            max_ = max;
        }
        else
        {
            if (min[0] < min_[0]) min_[0] = min[0];
            if (min[1] < min_[1]) min_[1] = min[1];
            if (min[2] < min_[2]) min_[2] = min[2];

            if (max_[0] < max[0]) max_[0] = max[0];
            if (max_[1] < max[1]) max_[1] = max[1];
            if (max_[2] < max[2]) max_[2] = max[2];
        }

        mv_.push_back(inter);
    }

    //-----------------------------------------------------------------

    bih_composite_intersection::bih_composite_intersection()
    {
        imp_ = new bih_composite_intersection_imp();
    }
    bih_composite_intersection::~bih_composite_intersection()
    {
        delete imp_;
    }

    bool bih_composite_intersection::test(const ray& r, real dist) const { return imp_->test(r, dist); }
    bool bih_composite_intersection::test(test_info* info, const ray& r, real dist) const { return imp_->test(info, r, dist); }
    void bih_composite_intersection::finalize(test_info* info, const ray& r, real dist) const
    { //NO IMP
        assert(0);
        return;
    }

    vector3 bih_composite_intersection::min() const { return imp_->min(); }
    vector3 bih_composite_intersection::max() const { return imp_->max(); }

    void bih_composite_intersection::construct()
    {
        imp_->construct();
    }

    void bih_composite_intersection::add(const std::shared_ptr<intersection>& inter)
    {
        imp_->add(inter);
    }
}
