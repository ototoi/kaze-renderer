#ifndef KAZE_COMPOSITE_INTERSECTION_H
#define KAZE_COMPOSITE_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class composite_intersection : public bounded_intersection
    {
    public:
        virtual bool test(const ray& r, real dist) const = 0;
        virtual bool test(test_info* info, const ray& r, real dist) const = 0;
        virtual void finalize(test_info* info, const ray& r, real dist) const = 0; // -- //NO IMPLEMENT!

        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;

    public:
        virtual void construct() = 0;
        virtual void add(intersection* inter)
        {
            std::shared_ptr<intersection> ptr(inter);
            this->add(ptr);
        }
        virtual void add(const std::shared_ptr<intersection>& inter) = 0;

    public:
        virtual ~composite_intersection() {}
    };
}

#endif
