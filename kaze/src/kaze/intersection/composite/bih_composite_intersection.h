#ifndef KAZE_BIH_COMPOSITE_INTERSECTION_H
#define KAZE_BIH_COMPOSITE_INTERSECTION_H

#include "composite_intersection.h"


namespace kaze
{

    class bih_composite_intersection_imp;

    class bih_composite_intersection : public composite_intersection
    {
    public:
        typedef const intersection* inter_type;

    public:
        bih_composite_intersection();
        ~bih_composite_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    public:
        void construct();
        void add(intersection* inter){ composite_intersection::add(inter); }
        void add(const std::shared_ptr<intersection>& inter);

    private:
        bih_composite_intersection(const bih_composite_intersection& rhs);
        bih_composite_intersection& operator=(const bih_composite_intersection& rhs);

    private:
        bih_composite_intersection_imp* imp_;
    };
}

#endif
