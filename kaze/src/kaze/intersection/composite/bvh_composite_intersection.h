#ifndef KAZE_BVH_COMPOSITE_INTERSECTION_H
#define KAZE_BVH_COMPOSITE_INTERSECTION_H

#include "composite_intersection.h"

namespace kaze
{

    class bvh_composite_intersection_imp;

    class bvh_composite_intersection : public composite_intersection
    {
    public:
        bvh_composite_intersection();
        ~bvh_composite_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const; // -- //NO IMPLEMENT!

        vector3 min() const;
        vector3 max() const;

    public:
        void construct();
        void add(intersection* inter){ composite_intersection::add(inter); }
        void add(const std::shared_ptr<intersection>& inter);

    private:
        bvh_composite_intersection_imp* imp_;
    };
}

#endif