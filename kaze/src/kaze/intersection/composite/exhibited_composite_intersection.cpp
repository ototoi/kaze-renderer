#include "exhibited_composite_intersection.h"
#include "test_info.h"

#include <memory>
//#include <iostream>

namespace kaze
{
    typedef const intersection* PCINT;

    class exhibited_composite_intersection_imp
    {
    public:
        exhibited_composite_intersection_imp();
        ~exhibited_composite_intersection_imp();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    public:
        void construct();
        void add(const std::shared_ptr<intersection>& inter);

    private:
        std::vector<std::shared_ptr<intersection> > mv_;
        vector3 min_;
        vector3 max_;
    };

    //------------------------------------------------------------------------
    exhibited_composite_intersection_imp::exhibited_composite_intersection_imp()
    {
        min_ = vector3(0, 0, 0);
        max_ = vector3(0, 0, 0);
    }

    exhibited_composite_intersection_imp::~exhibited_composite_intersection_imp()
    {
    }

    bool exhibited_composite_intersection_imp::test(const ray& r, real dist) const
    {
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (mv_[i]->test(r, dist)) return true;
        }
        return false;
    }

    bool exhibited_composite_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        bool bRet = false;

        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (mv_[i]->test(info, r, dist))
            {
                dist = info->distance;
                bRet = true;
            }
        }

        return bRet;
    }

    vector3 exhibited_composite_intersection_imp::min() const
    {
        return min_;
    }

    vector3 exhibited_composite_intersection_imp::max() const
    {
        return max_;
    }

    void exhibited_composite_intersection_imp::construct()
    {
        //std::cout << "exhibited_composite_intersection_imp::construct¥n" << std::endl;
    }

    void exhibited_composite_intersection_imp::add(const std::shared_ptr<intersection>& inter)
    {
        vector3 min = inter->min();
        vector3 max = inter->max();
        if (mv_.empty())
        {
            min_ = min;
            max_ = max;
        }
        else
        {
            if (min_[0] > min[0]) min_[0] = min[0];
            if (min_[1] > min[1]) min_[1] = min[1];
            if (min_[2] > min[2]) min_[2] = min[2];

            if (max_[0] < max[0]) max_[0] = max[0];
            if (max_[1] < max[1]) max_[1] = max[1];
            if (max_[2] < max[2]) max_[2] = max[2];
        }

        mv_.push_back(inter);
    }

    //-----------------------

    exhibited_composite_intersection::exhibited_composite_intersection()
    {
        imp_ = new exhibited_composite_intersection_imp();
    }

    exhibited_composite_intersection::~exhibited_composite_intersection()
    {
        delete imp_;
    }

    bool exhibited_composite_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool exhibited_composite_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return imp_->test(info, r, dist);
    }

    void exhibited_composite_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        // -- //NO IMPLEMENT!
    }

    vector3 exhibited_composite_intersection::min() const
    {
        return imp_->min();
    }

    vector3 exhibited_composite_intersection::max() const
    {
        return imp_->max();
    }

    void exhibited_composite_intersection::construct()
    {
        imp_->construct();
    }

    void exhibited_composite_intersection::add(const std::shared_ptr<intersection>& inter)
    {
        imp_->add(inter);
    }
}
