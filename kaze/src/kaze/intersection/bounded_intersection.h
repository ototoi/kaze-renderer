#ifndef KAZE_BOUNDED_INTERSECTION_H
#define KAZE_BOUNDED_INTERSECTION_H

#include "intersection.h"
#include <memory>

namespace kaze
{
    class bounded_intersection : public intersection
    {
    public:
        virtual ~bounded_intersection() {}
        virtual bool test(const ray& r, real dist) const = 0;
        virtual bool test(test_info* info, const ray& r, real dist) const = 0;
        virtual void finalize(test_info* info, const ray& r, real dist) const = 0;

        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;
    };
}

#endif
