#ifndef KAZE_COORDED_INTERSECTION_H
#define KAZE_COORDED_INTERSECTION_H

#include "bounded_intersection.h"


namespace kaze
{

    class coorded_intersection : public bounded_intersection
    {
    public:
        coorded_intersection(
            const std::shared_ptr<intersection>& ptr,
            const vector3& o = vector3(0, 0, 0), const vector3& x = vector3(1, 0, 0), const vector3& y = vector3(0, 1, 0), const vector3& z = vector3(0, 0, 1));
        coorded_intersection(
            const std::shared_ptr<intersection>& ptr, const matrix4& mat);

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> ptr_;
        matrix4 w2c_;
    };
}

#endif
