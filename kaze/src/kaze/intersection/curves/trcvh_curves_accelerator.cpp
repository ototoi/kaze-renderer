#include "trcvh_curves_accelerator.h"
#include "intersection_ex.h"

#include "curve_test_info.h"
#include "aligned_vector.hpp"

#include "intersection_ex.h"
#include "lbvh_utility.h"

#include "logger.h"
#include "timer.h"
#include "bezier.h"

#include <memory>
#include <xmmintrin.h>
#include <emmintrin.h>

#if 1
#define PRINTLOG print_log
#else
static void print_dummy(const char* str, ...) {}
#define PRINTLOG print_dummy
#endif

#if 0
#include <omp.h>
#endif
#define DIV_BIT 10
#define DIV_NUM 1024
#define VOX_M 6

#define MIN_FACE 16
#define MAX_FACE 32

static const int STACK_SIZE = ((int)(sizeof(size_t) * 16));

static const double kExtendRadiusRate = 1.4;

namespace kaze
{
    namespace
    {

        struct curve_segment
        {
            const curve_strand* p_strand;
            int nseg;
            real s0;
            real s1;
            vector3 p0;
            vector3 p1;
            real rad;
        };

        struct inter_segment
        {
            real s0;
            real s1;
            vector3 p0;
            vector3 p1;
            real rad;
        };

        typedef curve_segment FACE;
        typedef const curve_segment* PCFACE;
        typedef const PCFACE* face_iter;
        typedef const uint32_t* code_iter;

        template <class T>
        static void cropBezier(T out[], const T v[], int nOrder, real t0, real t1)
        {
            if (t0 <= 0)
            {
                if (1 <= t1)
                {
                    for (int i = 0; i < nOrder; i++)
                        out[i] = v[i];
                }
                else
                {
                    std::vector<T> b(nOrder);
                    bezier_split(out, &b[0], v, nOrder, t1);
                }
            }
            else if (1 <= t1)
            {
                std::vector<T> a(nOrder);
                bezier_split(&a[0], out, v, nOrder, t0);
            }
            else
            {

                std::vector<T> a(nOrder);
                std::vector<T> b(nOrder);
                bezier_split(&a[0], &b[0], v, nOrder, t1);
                bezier_split(&b[0], out, &a[0], nOrder, t0 / t1);
            }
        }

        static void getH(std::vector<real>& out, const std::vector<vector3>& P, const vector3& P0, const vector3& P1)
        {
            int nOrder = (int)P.size();
            vector3 L = P1 - P0;
            out.clear();
            out.push_back(0);
            for (int i = 1; i < nOrder - 1; i++)
            {
                vector3 V1 = P[i] - P0;
                real h = fabs(length(cross(L, V1)) / length(L));
                out.push_back(h);
            }
            out.push_back(0);
        }

        static real max_(const std::vector<real>& R)
        {
            int nOrder = (int)R.size();
            real ret = R[0];
            for (int i = 1; i < nOrder; i++)
                ret = std::max(ret, R[i]);
            return ret;
        }

        static matrix3 getZAlign(const vector3& dir)
        {
            vector3 z = dir;
            int plane = 0;
            if (fabs(z[1]) < fabs(z[plane])) plane = 1;
            if (fabs(z[2]) < fabs(z[plane])) plane = 2;
            vector3 x = vector3(0, 0, 0);
            x[plane] = 1;
            vector3 y = normalize(cross(z, x));
            x = cross(y, z);
            return matrix3(x[0], x[1], x[2], y[0], y[1], y[2], z[0], z[1], z[2]);
        }

        static bool checkSplit(const std::vector<vector3>& P)
        {
            int nOrder = (int)P.size();
            vector3 N = P[nOrder - 1] - P[0];
            for (int i = 0; i < nOrder - 1; i++)
            {
                if (dot(N, P[i + 1] - P[i]) <= 0) return true;
            }
            for (int i = 0; i < nOrder - 2; i++)
            {
                if (dot(P[i + 2] - P[i + 1], P[i + 1] - P[i]) <= 0) return true;
            }
            return false;
        }

        static void makeInterSegment(std::vector<inter_segment>& out, const std::vector<vector3>& P, const std::vector<real>& R, real s0, real s1)
        {
            int nOrder = (int)P.size();
            {
                bool bEq = true;
                for (int i = 0; i < nOrder - 1; i++)
                {
                    if (P[i] != P[i + 1])
                    {
                        bEq = false;
                        break;
                    }
                }
                if (bEq) return;
            }
            if (nOrder == 2)
            {
                inter_segment seg;
                seg.s0 = s0;
                seg.s1 = s1;
                seg.p0 = P[0];
                seg.p1 = P[nOrder - 1];
                seg.rad = std::max(R[0], R[1]);
                out.push_back(seg);
                return;
            }
            else if (P[0] == P[nOrder - 1])
            {
                std::vector<vector3> P0(nOrder);
                std::vector<vector3> P1(nOrder);
                std::vector<real> R0(nOrder);
                std::vector<real> R1(nOrder);
                bezier_split(&P0[0], &P1[0], &P[0], nOrder, 0.5);
                bezier_split(&R0[0], &R1[0], &R[0], nOrder, 0.5);
                real sm = (s0 + s1) * 0.5;
                makeInterSegment(out, P0, R0, s0, sm);
                makeInterSegment(out, P1, R1, sm, s1);
                return;
            }
            else
            {
                bool bSplit = checkSplit(P);
                if (!bSplit)
                {
                    real maxR = max_(R);
                    std::vector<real> hs;
                    getH(hs, P, P[0], P[nOrder - 1]);
                    real maxH = max_(hs);
                    real totR = maxH + maxR;
                    if (totR <= maxR * kExtendRadiusRate) //1.1*R
                    {
                        inter_segment seg;
                        seg.s0 = s0;
                        seg.s1 = s1;
                        seg.p0 = P[0];
                        seg.p1 = P[nOrder - 1];
                        seg.rad = totR;
                        out.push_back(seg);
                        return;
                    }
                }

                {
                    std::vector<vector3> P0(nOrder);
                    std::vector<vector3> P1(nOrder);
                    std::vector<real> R0(nOrder);
                    std::vector<real> R1(nOrder);
                    bezier_split(&P0[0], &P1[0], &P[0], nOrder, 0.5);
                    bezier_split(&R0[0], &R1[0], &R[0], nOrder, 0.5);
                    real sm = (s0 + s1) * 0.5;
                    makeInterSegment(out, P0, R0, s0, sm);
                    makeInterSegment(out, P1, R1, sm, s1);
                    return;
                }
            }
        }

        static void get_minmax(vector3& min, vector3& max, const std::vector<vector3>& P)
        {
            size_t sz = P.size();
            min = max = P[0];
            for (size_t i = 1; i < sz; i++)
            {
                vector3 p = P[i];
                min = vector3(std::min(min[0], p[0]), std::min(min[1], p[1]), std::min(min[2], p[2]));
                max = vector3(std::max(max[0], p[0]), std::max(max[1], p[1]), std::max(max[2], p[2]));
            }
        }

        static void get_minmax(vector3& min, vector3& max, PCFACE face)
        {
            real r = face->rad;
            vector3 p0 = face->p0;
            vector3 p1 = face->p1;
            min = vector3(std::min(p0[0], p1[0]), std::min(p0[1], p1[1]), std::min(p0[2], p1[2])) - vector3(r, r, r);
            max = vector3(std::max(p0[0], p1[0]), std::max(p0[1], p1[1]), std::max(p0[2], p1[2])) + vector3(r, r, r);
        }

        static void get_minmax(vector3& min, vector3& max, const PCFACE* a, const PCFACE* b)
        {
            static const real FAR = values::far();

            min = vector3(+FAR, +FAR, +FAR);
            max = vector3(-FAR, -FAR, -FAR);

            for (const PCFACE* i = a; i != b; i++)
            {
                vector3 cmin, cmax;
                get_minmax(cmin, cmax, *i);
                for (int j = 0; j < 3; j++)
                {
                    if (cmin[j] < min[j]) min[j] = cmin[j];
                    if (cmax[j] > max[j]) max[j] = cmax[j];
                }
            }
        }

        static void get_minmax(vector3f& min, vector3f& max, const PCFACE* a, const PCFACE* b)
        {
            vector3 tmin, tmax;
            get_minmax(tmin, tmax, a, b);
            for (int i = 0; i < 3; i++)
            {
                min[i] = (float)tmin[i];
                max[i] = (float)tmax[i];
            }
        }

        static void get_minmax(vector3f& pmin, vector3f& pmax, const PCFACE* faces, const size_t* indices, size_t a, size_t b)
        {
            static const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            for (size_t i = a; i != b; i++)
            {
                size_t idx = indices[i];
                PCFACE face = faces[idx];
                vector3 cmin;
                vector3 cmax;
                get_minmax(cmin, cmax, face);
                for (int j = 0; j < 3; j++)
                {
                    if (cmin[j] < min[j]) min[j] = cmin[j];
                    if (cmax[j] > max[j]) max[j] = cmax[j];
                }
            }
            pmin = min;
            pmax = max;
        }

        static inline uint32_t partby2(uint32_t n)
        {
            n = (n ^ (n << 16)) & 0xff0000ff;
            n = (n ^ (n << 8)) & 0x0300f00f;
            n = (n ^ (n << 4)) & 0x030c30c3;
            n = (n ^ (n << 2)) & 0x09249249;
            return n;
        }

        static inline uint32_t get_morton_code(uint32_t x, uint32_t y, uint32_t z)
        {
            return (partby2(x) << 2) | (partby2(y) << 1) | (partby2(z));
        }

        static inline uint32_t get_morton_code(const vector3& p, const vector3& min, const vector3& max)
        {
            uint32_t ix = (uint32_t)(DIV_NUM * ((p[0] - min[0]) / (max[0] - min[0])));
            uint32_t iy = (uint32_t)(DIV_NUM * ((p[1] - min[1]) / (max[1] - min[1])));
            uint32_t iz = (uint32_t)(DIV_NUM * ((p[2] - min[2]) / (max[2] - min[2])));
            return get_morton_code(ix, iy, iz);
        }

        static vector3 get_center(PCFACE face)
        {
            vector3 min = face->p0;
            vector3 max = face->p1;
            return (min + max) * real(0.5);
        }

        struct separator
        {
            separator(int level, const uint32_t* codes) : codes_(codes)
            {
                int p = 3 * DIV_BIT - 1 - level;
                nMask_ = 1 << p;
            }
            inline bool operator()(size_t i) const
            {
                return (nMask_ & codes_[i]) == 0;
            }

            uint32_t nMask_;
            const uint32_t* codes_;
        };

        class InterNode
        {
        public:
            virtual bool is_leaf() const = 0;
        };
    }

    class trcvh_curves_accelerator_imp
    {
    public:
        trcvh_curves_accelerator_imp(const std::vector<const curve_strand*>& strands);

    public:
        bool test(const ray& r, real tmin, real tmax) const;
        bool test(curve_test_info* info, const ray& r, real tmin, real tmax) const;
        vector3 min() const { return min_; }
        vector3 max() const { return max_; }
    protected:
        bool test_faces(size_t nFirst, const ray& r, real tmin, real tmax) const;
        bool test_faces(size_t nFirst, curve_test_info* info, const ray& r, real tmin, real tmax) const;
        bool test_inner(const ray& r, real tmin, real tmax) const;
        bool test_inner(curve_test_info* info, const ray& r, real tmin, real tmax) const;

    protected:
        void construct(
            const PCFACE* faces,
            const uint32_t* codes,
            size_t* indices,
            size_t a,
            size_t b);

    protected:
        vector3 min_;
        vector3 max_;
        std::vector<curve_segment> orgs_;
        std::vector<PCFACE> faces_;
    };

    //------------------------------------------------------------------
    trcvh_curves_accelerator_imp::trcvh_curves_accelerator_imp(const std::vector<const curve_strand*>& strands)
    {
        timer t;

        PRINTLOG("trcvh_curves_accelerator_imp:get segments start ...\n");
        t.start();
        size_t ssz = strands.size();
        size_t total_seg = 0;
        for (size_t i = 0; i < ssz; i++) //
        {
            total_seg += strands[i]->segments();
        }
        std::vector<curve_segment> segments;
        segments.reserve(total_seg * 4);

        for (size_t i = 0; i < ssz; i++) //
        {
            int psz = strands[i]->segments();
            for (int j = 0; j < psz; j++)
            {
                std::vector<vector3> P;
                std::vector<real> R;
                strands[i]->get_position(j, P);
                strands[i]->get_radius(j, R);

                std::vector<inter_segment> isegs;
                makeInterSegment(isegs, P, R, 0, 1);
                size_t ksz = isegs.size();
                for (size_t k = 0; k < ksz; k++)
                {
                    curve_segment hs;
                    hs.p_strand = strands[i];
                    hs.nseg = j;
                    hs.s0 = isegs[k].s0;
                    hs.s1 = isegs[k].s1;
                    hs.p0 = isegs[k].p0;
                    hs.p1 = isegs[k].p1;
                    hs.rad = isegs[k].rad;
                    segments.push_back(hs);
                }
            }
        }
        t.end();

        PRINTLOG("trcvh_curves_accelerator_imp:get segments end %d ms\n", t.msec());

        PRINTLOG("trcvh_curves_accelerator_imp:%u strands\n", ssz);

        PRINTLOG("trcvh_curves_accelerator_imp:%u segments\n", total_seg);
        PRINTLOG("trcvh_curves_accelerator_imp:%u segments\n", segments.size());

        orgs_.swap(segments);

        size_t sz = orgs_.size();
        std::vector<PCFACE> pFaces(sz);
        for (size_t i = 0; i < sz; i++)
        {
            pFaces[i] = &orgs_[i];
        }

        vector3 min, max;
        get_minmax(min, max, &pFaces[0], &pFaces[0] + sz);

        std::vector<uint32_t> codes(sz);
        {
            vector3 tmin, tmax;
            get_morton_bound(tmin, tmax, min, max);
            t.start();

            for (size_t i = 0; i < sz; i++)
            {
                codes[i] = get_morton_code(get_center(pFaces[i]), tmin, tmax);
            }
            t.end();
            PRINTLOG("trcvh_curves_accelerator:create morton code:%d ms\n", t.msec());
        }

        min_ = min;
        max_ = max;
    }

    bool trcvh_curves_accelerator_imp::test_faces(size_t nFirst, const ray& r, real tmin, real tmax) const
    {
        return false;
    }

    bool trcvh_curves_accelerator_imp::test_faces(size_t nFirst, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        return false;
    }

    //-------------------------------------------------------------

    bool trcvh_curves_accelerator_imp::test_inner(const ray& r, real tmin, real tmax) const
    {
        return false;
    }

    bool trcvh_curves_accelerator_imp::test_inner(curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        return false;
    }

    bool trcvh_curves_accelerator_imp::test(const ray& r, real tmin, real tmax) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            return test_inner(r, tmin, tmax);
        }
        else
        {
            return false;
        }
    }

    bool trcvh_curves_accelerator_imp::test(curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            return test_inner(info, r, tmin, tmax);
        }
        else
        {
            return false;
        }
    }

    //----------------------------------------------------------------------------------------------
    trcvh_curves_accelerator::trcvh_curves_accelerator(const std::vector<const curve_strand*>& strands)
    {
        imp_ = new trcvh_curves_accelerator_imp(strands);
    }

    trcvh_curves_accelerator::~trcvh_curves_accelerator()
    {
        delete imp_;
    }

    bool trcvh_curves_accelerator::test(const ray& r, real tmin, real tmax) const
    {
        return imp_->test(r, tmin, tmax);
    }

    bool trcvh_curves_accelerator::test(curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        return imp_->test(info, r, tmin, tmax);
    }

    vector3 trcvh_curves_accelerator::min() const
    {
        return imp_->min();
    }

    vector3 trcvh_curves_accelerator::max() const
    {
        return imp_->max();
    }
}
