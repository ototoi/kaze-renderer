#include "curves_intersection.h"
#include "test_info.h"
#include "curve_strand.h"
#include "bezier_curve_strand.h"
#include "curves_accelerator.h"
#include "sbvh_curves_accelerator.h"
#include "plqbvh_curves_accelerator.h"
#include "splqbvh_curves_accelerator.h"
#include "curve_test_info.h"
#include "intersection_ex.h"

#include "logger.h"
#include "timer.h"

#include <memory>
#include <vector>

#if 0
#define PRINTLOG print_log
#else
static void print_dummy(const char* str, ...) {}
#define PRINTLOG print_dummy
#endif

namespace kaze
{
    namespace
    {
        struct T2I
        {
            const char* name;
            int num;
        };

        static T2I CURVE_TYPE[] = {
            {"default", 0},
            {"ribbon", 0},
            {"tube", 1},
            {"cylinder", 1},
            {"hair", 1}, //
            {NULL, 0},
        };
    }

    static int GetCurveType(const char* szCurveType)
    {
        int i = 0;
        while (CURVE_TYPE[i].name)
        {
            if (strcmp(CURVE_TYPE[i].name, szCurveType) == 0)
            {
                return CURVE_TYPE[i].num;
            }
            i++;
        }
        return 0;
    }

    static curve_strand* convert_strand_cylinder(const io_bezier_curve_strand& str, size_t index)
    {
        int order = str.order;
        size_t psz = str.points.size();
        std::vector<vector3f> P(psz);
        std::vector<float> R(psz);
        std::vector<vector3f> C1(psz);
        std::vector<vector3f> C2(psz);
        for (size_t i = 0; i < psz; i++)
        {
            P[i] = vector3f(str.points[i].position);
            R[i] = str.points[i].radius;
            C1[i] = color3f(str.points[i].color1);
            C2[i] = color3f(str.points[i].color2);
        }
        return new cylinder_bezier_curve_strand(P, R, C1, C2, order);
    }

    static curve_strand* convert_strand_ribbon(const io_bezier_curve_strand& str, size_t index)
    {
        int order = str.order;
        size_t psz = str.points.size();

        std::vector<vector3f> P(psz);
        std::vector<float> R(psz);
        std::vector<vector3f> N(psz);
        std::vector<vector3f> C1(psz);
        std::vector<vector3f> C2(psz);

        if (str.has_normal)
        {
            for (size_t i = 0; i < psz; i++)
            {
                P[i] = vector3f(str.points[i].position);
                R[i] = str.points[i].radius;
                N[i] = vector3f(str.points[i].normal);
                C1[i] = color3f(str.points[i].color1);
                C2[i] = color3f(str.points[i].color2);
            }
            return new normal_ribbon_bezier_curve_strand(P, R, N, C1, C2, order, index);
        }
        else
        {
            for (size_t i = 0; i < psz; i++)
            {
                P[i] = vector3f(str.points[i].position);
                R[i] = str.points[i].radius;
                C1[i] = color3f(str.points[i].color1);
                C2[i] = color3f(str.points[i].color2);
            }
            return new ribbon_bezier_curve_strand(P, R, C1, C2, order);
        }
    }

    class curves_intersection_imp
    {
    public:
        curves_intersection_imp(const bezier_curves_loader& loader, const parameter_map& param);
        ~curves_intersection_imp();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    protected:
        void load(const bezier_curves_loader& loader, const parameter_map& param);
        void initialize();

    protected:
        curves_accelerator* root_;
        std::vector<const curve_strand*> strands_;
    };

    curves_intersection_imp::curves_intersection_imp(const bezier_curves_loader& loader, const parameter_map& param)
        : root_(0)
    {
        load(loader, param);
        initialize();
    }

    curves_intersection_imp::~curves_intersection_imp()
    {
        delete root_;
        size_t sz = strands_.size();
        for (size_t i = 0; i < sz; i++)
        {
            delete strands_[i];
        }
    }

    void curves_intersection_imp::load(const bezier_curves_loader& loader, const parameter_map& param)
    {
        timer t;
        PRINTLOG("curves_intersection:loading curves start ...\n");
        t.start();
        size_t ssz;
        if (!loader.get_strands(ssz)) return;

        std::string strCurveType = param.get_string("CURVE_TYPE", "default");
        int nCurveType = GetCurveType(strCurveType.c_str());

        //
        //ssz = 10;
        //vector3 min(+FAR,+FAR,+FAR);
        //vector3 max(-FAR,-FAR,-FAR);
        strands_.reserve(ssz);
        {
            io_bezier_curve_strand strand;
            for (size_t i = 0; i < ssz; i++)
            {
                if (!loader.get_strand(i, &strand)) return;

                if (nCurveType == 0)
                {
                    strands_.push_back(convert_strand_ribbon(strand, i)); //
                }
                else if (nCurveType == 1)
                {
                    strands_.push_back(convert_strand_cylinder(strand, i)); //
                }
            }
        }
        t.end();
        PRINTLOG("curves_intersection:loading curves end %d ms\n", t.msec());
    }

    void curves_intersection_imp::initialize()
    {
        timer t;
        PRINTLOG("curves_intersection:construct tree start ...\n");
        t.start();
        root_ = new splqbvh_curves_accelerator(strands_);
        //root_ = new plqbvh_curves_accelerator(strands_);
        t.end();
        PRINTLOG("curves_intersection:construct tree end %d ms\n", t.msec());
    }

    bool curves_intersection_imp::test(const ray& r, real dist) const
    {
        return root_->test(r, 0, dist);
    }

    bool curves_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        curve_test_info cinfo;
        cinfo.p_strand = NULL;
        if (root_->test(&cinfo, r, 0, dist))
        {
            if (cinfo.p_strand)
            {
                cinfo.p_strand->finalize(cinfo.nseg, &cinfo, r, 0, cinfo.t);

                vector3 N = vector3(cinfo.normal);
                vector3 U = vector3(cinfo.tangent);
                vector3 V = vector3(cinfo.binormal);
                vector3 G = vector3(cinfo.geometric);
                info->distance = cinfo.t;
                info->position = vector3(cinfo.position);
                info->normal = N;
                info->geometric = G;
                info->tangent = U;
                info->binormal = V;
                info->coord = vector3(cinfo.u, cinfo.v, 0);
                info->col1 = vector3(cinfo.col1);
                info->col2 = vector3(cinfo.col2);
                info->index = cinfo.index;

                return true;
            }
        }
        return false;
    }

    void curves_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }

    vector3 curves_intersection_imp::min() const
    {
        return root_->min();
    }

    vector3 curves_intersection_imp::max() const
    {
        return root_->max();
    }
    //------------------------------------------------------------------

    curves_intersection::curves_intersection(const bezier_curves_loader& loader, const parameter_map& param)
    {
        imp_ = new curves_intersection_imp(loader, param);
    }

    curves_intersection::~curves_intersection()
    {
        delete imp_;
    }

    bool curves_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }

    bool curves_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }

    void curves_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        imp_->finalize(info, r, dist);
    }

    vector3 curves_intersection::min() const
    {
        return imp_->min();
    }

    vector3 curves_intersection::max() const
    {
        return imp_->max();
    }
}
