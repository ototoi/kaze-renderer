#include "motion_points_curve_strand.h"
#include "curve_test_info.h"

namespace kaze
{

    static vector3 Convert(const vector3f& p)
    {
        return vector3(p[0], p[1], p[2]);
    }

    template <class T>
    T lerp(const T& a, const T& b, real t)
    {
        return a * (1 - t) + b * (t);
    }

    static int solve2e(real root[], real B, real C)
    {
        real D = B * B - C;
        if (D < 0)
        {
            return 0;
        }
        else if (D == 0)
        {
            double x = -B;
            root[0] = x;
            return 1;
        }
        else
        {
            real x1 = (fabs(B) + sqrt(D));
            if (B >= 0.0)
            {
                x1 = -x1;
            }
            real x2 = C / x1;
            if (x1 > x2) std::swap(x1, x2);
            root[0] = x1;
            root[1] = x2;
            return 2;
        }
    }

    static bool test_disk(const vector3& P, real R, const vector3& N, curve_test_info* info, const ray& r, real tmin, real tmax)
    {
        vector3 P0 = r.origin();
        vector3 V = r.direction();
        real VN = dot(V, N);
        if (fabs(VN) <= std::numeric_limits<real>::min()) return false;
        real d = -dot(P, N);
        real t = -(dot(P0, N) + d) / VN;
        if (t <= tmin || tmax <= t) return false;
        vector3 p = r.origin() + t * r.direction();

        vector3 dif = (P - p);
        real d2 = dot(dif, dif);
        if (d2 >= R * R) return false;
        if (info)
        {
            info->t = t;
            info->position = p;
            info->normal = N;
        }
        return true;
    }

    static bool test_sphere(const vector3& P, const real rad, curve_test_info* info, const ray& r, real tmin, real tmax)
    {
        vector3 center = P;
        vector3 rs = r.origin() - center;
        real radius = rad;

        real B = dot(rs, r.direction());
        real C = dot(rs, rs) - radius * radius;

        real root[2];
        int nRet = solve2e(root, B, C);
        if (nRet)
        {
            //for(int i=0;i<nRet;i++)
            {
                real t = root[0];
                if ((t > tmin) && (t < tmax))
                {
                    if (info)
                    {
                        info->t = t;
                    }
                    return true;
                }
            }
        }
        return false;
    }
    //-----------------------------------------------------------------------------

    disk_motion_points_curve_strand::disk_motion_points_curve_strand(
        const std::vector<float>& times,
        const std::vector<vector3f>& P,
        const std::vector<float>& R,
        const std::vector<vector3f>& C1,
        const std::vector<vector3f>& C2,
        size_t index) : times_(times), P_(P), R_(R), C1_(C1), C2_(C2), index_(index)
    {
        ;
    }

    int disk_motion_points_curve_strand::segments() const
    {
        return (int)times_.size() - 1;
    }

    bool disk_motion_points_curve_strand::test(int nseg, const ray& r, real tmin, real tmax) const
    {
        real t0 = (real)times_[nseg];
        real t1 = (real)times_[nseg + 1];
        real time = r.time();
        if (t0 <= time && time <= t1)
        {
            real t = (t1 - time) / (t1 - t0);
            vector3 P = lerp(Convert(P_[nseg]), Convert(P_[nseg + 1]), t);
            real R = lerp((real)R_[nseg], (real)R_[nseg + 1], t);
            vector3 N = normalize(r.origin() - P);

            return test_disk(P, R, N, NULL, r, tmin, tmax);
        }
        return false;
    }

    bool disk_motion_points_curve_strand::test(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        real t0 = (real)times_[nseg];
        real t1 = (real)times_[nseg + 1];
        real time = r.time();
        if (t0 <= time && time <= t1)
        {
            real t = (t1 - time) / (t1 - t0);
            vector3 P = lerp(Convert(P_[nseg]), Convert(P_[nseg + 1]), t);
            real R = lerp((real)R_[nseg], (real)R_[nseg + 1], t);
            vector3 N = normalize(r.origin() - P);

            if (test_disk(P, R, N, info, r, tmin, tmax))
            {
                info->u = 0;
                info->v = 0;
                info->d = t; //
                info->p_strand = this;
                info->nseg = nseg;

                return true;
            }
        }
        return false;
    }

    void disk_motion_points_curve_strand::finalize(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        real t = info->d;
        vector3 N = info->normal;
        vector3 P0 = Convert(P_[nseg]);
        vector3 P1 = Convert(P_[nseg + 1]);
        vector3 V = P1 - P0;
        vector3 U = cross(V, N);
        vector3 C1 = lerp(Convert(C1_[nseg]), Convert(C1_[nseg + 1]), t);
        vector3 C2 = lerp(Convert(C2_[nseg]), Convert(C2_[nseg + 1]), t);

        info->geometric = N;
        info->tangent = U;
        info->binormal = V;
        info->col1 = C1;
        info->col2 = C2;
        info->index = index_;
    }

    vector3 disk_motion_points_curve_strand::min(int nseg) const
    {
        vector3 P0 = Convert(P_[nseg]);
        vector3 P1 = Convert(P_[nseg + 1]);
        real R0 = (real)R_[nseg];
        real R1 = (real)R_[nseg + 1];

        vector3 points[] =
            {
                P0 - vector3(R0, R0, R0),
                P1 - vector3(R1, R1, R1)};
        vector3 p = points[0];
        for (int i = 1; i < 2; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (p[j] > points[i][j]) p[j] = points[i][j];
            }
        }
        return p;
    }

    vector3 disk_motion_points_curve_strand::max(int nseg) const
    {
        vector3 P0 = Convert(P_[nseg]);
        vector3 P1 = Convert(P_[nseg + 1]);
        real R0 = (real)R_[nseg];
        real R1 = (real)R_[nseg + 1];

        vector3 points[] =
            {
                P0 + vector3(R0, R0, R0),
                P1 + vector3(R1, R1, R1)};
        vector3 p = points[0];
        for (int i = 1; i < 2; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (p[j] < points[i][j]) p[j] = points[i][j];
            }
        }
        return p;
    }

    //-----------------------------------------------------------------------------

    normal_disk_motion_points_curve_strand::normal_disk_motion_points_curve_strand(
        const std::vector<float>& times,
        const std::vector<vector3f>& P,
        const std::vector<float>& R,
        const std::vector<vector3f>& N,
        const std::vector<vector3f>& C1,
        const std::vector<vector3f>& C2,
        size_t index) : disk_motion_points_curve_strand(times, P, R, C1, C2, index), normals_(N)
    {
        ;
    }

    int normal_disk_motion_points_curve_strand::segments() const
    {
        return (int)times_.size() - 1;
    }

    bool normal_disk_motion_points_curve_strand::test(int nseg, const ray& r, real tmin, real tmax) const
    {
        real t0 = (real)times_[nseg];
        real t1 = (real)times_[nseg + 1];
        real time = r.time();
        if (t0 <= time && time <= t1)
        {
            real t = (t1 - time) / (t1 - t0);
            vector3 P = lerp(Convert(P_[nseg]), Convert(P_[nseg + 1]), t);
            real R = lerp((real)R_[nseg], (real)R_[nseg + 1], t);
            vector3 N = normalize(lerp(Convert(normals_[nseg]), Convert(normals_[nseg + 1]), t));

            return test_disk(P, R, N, NULL, r, tmin, tmax);
        }
        return false;
    }

    bool normal_disk_motion_points_curve_strand::test(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        real t0 = (real)times_[nseg];
        real t1 = (real)times_[nseg + 1];
        real time = r.time();
        if (t0 <= time && time <= t1)
        {
            real t = (t1 - time) / (t1 - t0);
            vector3 P = lerp(Convert(P_[nseg]), Convert(P_[nseg + 1]), t);
            real R = lerp((real)R_[nseg], (real)R_[nseg + 1], t);
            vector3 N = normalize(lerp(Convert(normals_[nseg]), Convert(normals_[nseg + 1]), t));

            if (test_disk(P, R, N, info, r, tmin, tmax))
            {
                info->u = 0;
                info->v = 0;
                info->d = t; //
                info->p_strand = this;
                info->nseg = nseg;

                return true;
            }
        }
        return false;
    }

    void normal_disk_motion_points_curve_strand::finalize(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        real t = info->d;
        vector3 N = info->normal;
        vector3 P0 = Convert(P_[nseg]);
        vector3 P1 = Convert(P_[nseg + 1]);
        vector3 V = P1 - P0;
        vector3 U = cross(V, N);
        vector3 C1 = lerp(Convert(C1_[nseg]), Convert(C1_[nseg + 1]), t);
        vector3 C2 = lerp(Convert(C2_[nseg]), Convert(C2_[nseg + 1]), t);

        info->geometric = N;
        info->tangent = U;
        info->binormal = V;
        info->col1 = C1;
        info->col2 = C2;
        info->index = index_;
    }

    vector3 normal_disk_motion_points_curve_strand::min(int nseg) const
    {
        vector3 P0 = Convert(P_[nseg]);
        vector3 P1 = Convert(P_[nseg + 1]);
        real R0 = (real)R_[nseg];
        real R1 = (real)R_[nseg + 1];

        vector3 points[] =
            {
                P0 - vector3(R0, R0, R0),
                P1 - vector3(R1, R1, R1)};
        vector3 p = points[0];
        for (int i = 1; i < 2; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (p[j] > points[i][j]) p[j] = points[i][j];
            }
        }
        return p;
    }

    vector3 normal_disk_motion_points_curve_strand::max(int nseg) const
    {
        vector3 P0 = Convert(P_[nseg]);
        vector3 P1 = Convert(P_[nseg + 1]);
        real R0 = (real)R_[nseg];
        real R1 = (real)R_[nseg + 1];

        vector3 points[] =
            {
                P0 + vector3(R0, R0, R0),
                P1 + vector3(R1, R1, R1)};
        vector3 p = points[0];
        for (int i = 1; i < 2; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (p[j] < points[i][j]) p[j] = points[i][j];
            }
        }
        return p;
    }

    //-----------------------------------------------------------------------------

    sphere_motion_points_curve_strand::sphere_motion_points_curve_strand(
        const std::vector<float>& times,
        const std::vector<vector3f>& P,
        const std::vector<float>& R,
        const std::vector<vector3f>& C1,
        const std::vector<vector3f>& C2,
        size_t index) : disk_motion_points_curve_strand(times, P, R, C1, C2, index)
    {
        ;
    }

    bool sphere_motion_points_curve_strand::test(int nseg, const ray& r, real tmin, real tmax) const
    {
        real t0 = (real)times_[nseg];
        real t1 = (real)times_[nseg + 1];
        real time = r.time();
        if (t0 <= time && time <= t1)
        {
            real t = (t1 - time) / (t1 - t0);
            vector3 P = lerp(Convert(P_[nseg]), Convert(P_[nseg + 1]), t);
            real R = lerp((real)R_[nseg], (real)R_[nseg + 1], t);

            return test_sphere(P, R, NULL, r, tmin, tmax);
        }
        return false;
    }

    bool sphere_motion_points_curve_strand::test(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        real t0 = (real)times_[nseg];
        real t1 = (real)times_[nseg + 1];
        real time = r.time();
        if (t0 <= time && time <= t1)
        {
            real t = (t1 - time) / (t1 - t0);
            vector3 P = lerp(Convert(P_[nseg]), Convert(P_[nseg + 1]), t);
            real R = lerp((real)R_[nseg], (real)R_[nseg + 1], t);

            if (test_sphere(P, R, info, r, tmin, tmax))
            {
                info->u = 0;
                info->v = 0;
                info->d = t; //
                info->p_strand = this;
                info->nseg = nseg;

                return true;
            }
        }
        return false;
    }

    void sphere_motion_points_curve_strand::finalize(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        int i = nseg;
        vector3 P = Convert(P_[i]);
        real R = (real)R_[i];
        {
            vector3 C1 = Convert(C1_[i]);
            vector3 C2 = Convert(C2_[i]);

            real t = info->t;

            info->position = r.origin() + t * r.direction();
            vector3 N = normalize(info->position - P);
            info->normal = N;
            info->geometric = N;
            info->col1 = C1;
            info->col2 = C2;
            info->index = i;
        }
    }
}
