#ifndef KAZE_SBVH_CURVES_ACCELERATOR_H
#define KAZE_SBVH_CURVES_ACCELERATOR_H

#include "curves_accelerator.h"

namespace kaze
{

    class sbvh_curves_accelerator_imp;
    class sbvh_curves_accelerator : public curves_accelerator
    {
    public:
        sbvh_curves_accelerator(const std::vector<const curve_strand*>& strands);
        ~sbvh_curves_accelerator();

    public:
        bool test(const ray& r, real tmin, real tmax) const;
        bool test(curve_test_info* info, const ray& r, real tmin, real tmax) const;
        vector3 min() const;
        vector3 max() const;

    private:
        sbvh_curves_accelerator_imp* imp_;
    };
}

#endif
