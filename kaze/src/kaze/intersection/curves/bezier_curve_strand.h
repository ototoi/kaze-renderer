#ifndef KAZE_BEZIER_CURVE_STRAND_H
#define KAZE_BEZIER_CURVE_STRAND_H

#include "curve_strand.h"
#include "cylinder_bezier_curve_tester.h"
#include "ribbon_bezier_curve_tester.h"
#include "normal_ribbon_bezier_curve_tester.h"

#include <vector>
#include <iostream>

namespace kaze
{

    class base_bezier_curve_strand : public curve_strand
    {
    public:
        base_bezier_curve_strand(
            const std::vector<vector3f>& P,
            const std::vector<float>& R,
            const std::vector<vector3f>& C1,
            const std::vector<vector3f>& C2,
            int nOrder = 4, size_t index = 0);
        ~base_bezier_curve_strand();
        int segments() const;
        bool test(int nseg, real v0, real v1, const ray& r, real tmin, real tmax) const;
        bool test(int nseg, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax) const;
        bool test(int nseg, const ray& r, real tmin, real tmax) const;
        bool test(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const;
        void finalize(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const;
        vector3 min(int nseg) const;
        vector3 max(int nseg) const;
        void get_position(int nseg, std::vector<vector3>& v) const;
        void get_radius(int nseg, std::vector<real>& v) const;

    protected:
        virtual bool test(const vector3 P[], const real R[], int nOrder, real v0, real v1, const ray& r, real tmin, real tmax) const = 0;
        virtual bool test(const vector3 P[], const real R[], int nOrder, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax) const = 0;
        virtual void finalize(const vector3 P[], const real R[], int nOrder, curve_test_info* info, const ray& r, real tmin, real tmax) const = 0;
        virtual vector3 min(const vector3 P[], const real R[], int nOrder) const = 0;
        virtual vector3 max(const vector3 P[], const real R[], int nOrder) const = 0;

    protected:
        std::vector<vector3f> points_;
        std::vector<float> rads_;
        std::vector<vector3f> c1_;
        std::vector<vector3f> c2_;
        int nOrder_;
        size_t index_;
    };

    class base_normal_bezier_curve_strand : public curve_strand
    {
    public:
        base_normal_bezier_curve_strand(
            const std::vector<vector3f>& P,
            const std::vector<float>& R,
            const std::vector<vector3f>& N,
            const std::vector<vector3f>& C1,
            const std::vector<vector3f>& C2,
            int nOrder = 4, size_t index = 0);
        ~base_normal_bezier_curve_strand();
        int segments() const;

    public:
        bool test(int nseg, real v0, real v1, const ray& r, real tmin, real tmax) const;
        bool test(int nseg, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax) const;
        bool test(int nseg, const ray& r, real tmin, real tmax) const;
        bool test(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const;
        void finalize(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const;
        vector3 min(int nseg) const;
        vector3 max(int nseg) const;
        void get_position(int nseg, std::vector<vector3>& v) const;
        void get_radius(int nseg, std::vector<real>& v) const;

    public:
        virtual bool test(const vector3 P[], const real R[], const vector3 N[], int nOrder, real v0, real v1, const ray& r, real tmin, real tmax) const = 0;
        virtual bool test(const vector3 P[], const real R[], const vector3 N[], int nOrder, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax) const = 0;
        virtual void finalize(const vector3 P[], const real R[], const vector3 N[], int nOrder, curve_test_info* info, const ray& r, real tmin, real tmax) const = 0;
        virtual vector3 min(const vector3 P[], const real R[], const vector3 N[], int nOrder) const = 0;
        virtual vector3 max(const vector3 P[], const real R[], const vector3 N[], int nOrder) const = 0;

    protected:
        std::vector<vector3f> points_;
        std::vector<float> rads_;
        std::vector<vector3f> normals_;
        std::vector<vector3f> c1_;
        std::vector<vector3f> c2_;
        int nOrder_;
        size_t index_;
    };

    template <class ALGO>
    class algo_bezier_curve_strand : public base_bezier_curve_strand
    {
    public:
        algo_bezier_curve_strand(
            const std::vector<vector3f>& P,
            const std::vector<float>& R,
            const std::vector<vector3f>& C1,
            const std::vector<vector3f>& C2,
            int nOrder = 4, size_t index = 0)
            : base_bezier_curve_strand(P, R, C1, C2, nOrder, index) {}
    public:
        bool test(const vector3 P[], const real R[], int nOrder, real v0, real v1, const ray& r, real tmin, real tmax) const
        {
            return ALGO::test(P, R, nOrder, v0, v1, r, tmin, tmax);
        }
        bool test(const vector3 P[], const real R[], int nOrder, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax) const
        {
            return ALGO::test(P, R, nOrder, v0, v1, info, r, tmin, tmax);
        }
        void finalize(const vector3 P[], const real R[], int nOrder, curve_test_info* info, const ray& r, real tmin, real tmax) const
        {
            ALGO::finalize(P, R, nOrder, info, r, tmin, tmax);
        }
        vector3 min(const vector3 P[], const real R[], int nOrder) const
        {
            return ALGO::min(P, R, nOrder);
        }
        vector3 max(const vector3 P[], const real R[], int nOrder) const
        {
            return ALGO::max(P, R, nOrder);
        }
    };

    typedef algo_bezier_curve_strand<ribbon_bezier_curve_tester> ribbon_bezier_curve_strand;
    typedef algo_bezier_curve_strand<cylinder_bezier_curve_tester> cylinder_bezier_curve_strand;

    template <class ALGO>
    class algo_normal_bezier_curve_strand : public base_normal_bezier_curve_strand
    {
    public:
        algo_normal_bezier_curve_strand(
            const std::vector<vector3f>& P,
            const std::vector<float>& R,
            const std::vector<vector3f>& N,
            const std::vector<vector3f>& C1,
            const std::vector<vector3f>& C2,
            int nOrder = 4, size_t index = 0) : base_normal_bezier_curve_strand(P, R, N, C1, C2, nOrder, index) {}
    public:
        bool test(const vector3 P[], const real R[], const vector3 N[], int nOrder, real v0, real v1, const ray& r, real tmin, real tmax) const
        {
            return ALGO::test(P, R, N, nOrder, v0, v1, r, tmin, tmax);
        }
        bool test(const vector3 P[], const real R[], const vector3 N[], int nOrder, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax) const
        {
            return ALGO::test(P, R, N, nOrder, v0, v1, info, r, tmin, tmax);
        }
        void finalize(const vector3 P[], const real R[], const vector3 N[], int nOrder, curve_test_info* info, const ray& r, real tmin, real tmax) const
        {
            ALGO::finalize(P, R, N, nOrder, info, r, tmin, tmax);
        }
        vector3 min(const vector3 P[], const real R[], const vector3 N[], int nOrder) const
        {
            return ALGO::min(P, R, N, nOrder);
        }
        vector3 max(const vector3 P[], const real R[], const vector3 N[], int nOrder) const
        {
            return ALGO::max(P, R, N, nOrder);
        }
    };

    typedef algo_normal_bezier_curve_strand<normal_ribbon_bezier_curve_tester> normal_ribbon_bezier_curve_strand;
}

#endif
