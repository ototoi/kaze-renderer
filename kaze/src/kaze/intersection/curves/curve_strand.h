#ifndef KAZE_CURVE_STRAND_H
#define KAZE_CURVE_STRAND_H

#include "types.h"
#include "color.h"
#include "ray.h"

#include <vector>

namespace kaze
{

    struct curve_test_info;

    class curve_strand
    {
    public:
        virtual ~curve_strand() {}
        virtual int segments() const = 0;
        virtual bool test(int nseg, real v0, real v1, const ray& r, real tmin, real tmax) const { return test(nseg, r, tmin, tmax); }
        virtual bool test(int nseg, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax) const { return test(nseg, info, r, tmin, tmax); }
        virtual bool test(int nseg, const ray& r, real tmin, real tmax) const = 0;
        virtual bool test(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const = 0;
        virtual void finalize(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const = 0;
        virtual vector3 min(int nseg) const = 0;
        virtual vector3 max(int nseg) const = 0;
        virtual void get_position(int nseg, std::vector<vector3>& v) const { ; }
        virtual void get_radius(int nseg, std::vector<real>& v) const { ; }
    };
}
#endif
