#include "cylinder_bezier_curve_tester.h"
#include "curve_test_info.h"
#include "bezier.h"
#include "patch/bilinear_patch_intersection.h"
#include "intersection_ex.h"
#include <vector>

#define DIRECT_BILINEAR 1
#define USE_BEZIERCLIP 1
#define USE_COARSESORT 1

#ifdef _MSC_VER
#define INLINE __forceinline
//define INLINE inline
#elif defined(__GNUC__)
#define INLINE __inline__
//define INLINE inline
#else
#define INLINE inline
#endif

namespace kaze
{
    static const real EPS = 1e-4;
    static const real EPS3 = 1e-16;
    static const real UVEPS = 1e-2;

    static int max_level_ = 8;
    static bool use_bezierclip_ = (bool)USE_BEZIERCLIP;

    static int GetMaxLevel()
    {
        return max_level_;
    }

    static void SetMaxLevel(int lv)
    {
        if (lv >= 16)
        {
            lv = 16;
        }
        max_level_ = lv;
    }

    static bool GetUseBezierClip()
    {
        return use_bezierclip_;
    }

    static void SetUseBezierClip(bool b)
    {
        use_bezierclip_ = b;
    }

    namespace
    {
        struct isect_info
        {
            real t;
            real v;
            vector3 c;
        };
    }

    static void get_minmax(vector3& min, vector3& max, const vector3 P[], int nOrder)
    {
        min = P[0];
        max = P[0];
        for (int i = 1; i < nOrder; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                min[j] = std::min(min[j], P[i][j]);
                max[j] = std::max(max[j], P[i][j]);
            }
        }
    }

    static inline vector3 mat_mul(const matrix3& m, const vector3& v)
    {
        return m * v;
    }

    template <class T>
    static void splitBezier(T left[], T right[], const T v[], int nOrder)
    {
        bezier_split(left, right, v, nOrder, 0.5);
    }

    template <class T, int Sz>
    static void cropBezier(T out[], const T v[], int nOrder, real t0, real t1)
    {
        bezier_crop(out, v, nOrder, t0, t1);
    }

    template <class T>
    static T evaluateBezier(const T v[], int nOrder, real t)
    {
        return bezier_evaluate(v, nOrder, t);
    }

    template <class T>
    static T evaluateBezierTangent(const T v[], int nOrder, real t)
    {
        return bezier_evaluate_deriv(v, nOrder, t);
    }

    template <class T>
    static T evaluateLinear(const T v[], int nOrder, real t)
    {
        return (1 - t) * v[0] + t * v[nOrder - 1];
    }

    static inline real lerp(real a, real b, real t)
    {
        return a + (b - a) * t;
    }

    static void getZAlign(matrix3& mat, const vector3& dir)
    {
        vector3 z = dir;
        int plane = 0;
        if (fabs(z[1]) < fabs(z[plane])) plane = 1;
        if (fabs(z[2]) < fabs(z[plane])) plane = 2;
        vector3 x = vector3(0, 0, 0);
        x[plane] = 1;
        vector3 y = normalize(cross(z, x));
        x = cross(y, z);
        mat = matrix3(x[0], x[1], x[2], y[0], y[1], y[2], z[0], z[1], z[2]);
    }

    static INLINE bool scan_minmax(const vector2 p[], int sz)
    {
        int nUpper = 0;
        int nLower = 0;
        for (int i = 0; i < sz; i++)
        {
            if (p[i][1] > 0)
                nUpper++;
            else
                nLower++;
            if (nUpper && nLower) return true;
        }
        return false;
    }

    static INLINE real slope(const vector2& a, const vector2& b)
    {
        vector2 dif = b - a;
        if (fabs(dif[1]) < values::epsilon())
        {
            return values::far();
        }
        else
        {
            return fabs(dif[0] / dif[1]);
        }
    }

    static INLINE real dist(const vector2& p0, const vector2& p1)
    {
        real tt = fabs(p0[1] / (p1[1] - p0[1]));
        real t = p0[0] + tt * (p1[0] - p0[0]);

        return t;
    }

    static INLINE int scan_convex(real ts[], const vector2 p[], int sz)
    {
        if (!scan_minmax(p, sz))
        {
            return 0;
        }
        int n = 0;
        {
            int k = -1;
            int l = -1;
            real current = values::far();
            //int sz = (int)p.size();
            for (int i = 1; i < sz; i++)
            {
                if (p[i][1] * p[0][1] < 0)
                {
                    real s = slope(p[i], p[0]);
                    if (s < current)
                    {
                        current = s;
                        k = i;
                    }
                }
            }
            if (k >= 0)
            {
                current = 0;
                for (int i = 0; i < k; i++)
                {
                    if (p[i][1] * p[k][1] < 0)
                    {
                        real s = slope(p[i], p[k]);
                        if (current < s)
                        {
                            current = s;
                            l = i;
                        }
                    }
                }
                if (l >= 0)
                {
                    ts[n++] = dist(p[l], p[k]);
                }
            }
        }
        {
            int k = -1;
            int l = -1;
            real current = values::far();
            //int sz = (int)p.size();
            for (int i = 0; i < sz - 1; i++)
            {
                if (p[i][1] * p[sz - 1][1] < 0)
                {
                    real s = slope(p[i], p[sz - 1]);
                    if (s < current)
                    {
                        current = s;
                        k = i;
                    }
                }
            }
            if (k >= 0)
            {
                current = 0;
                for (int i = k + 1; i < sz; i++)
                {
                    if (p[i][1] * p[k][1] < 0)
                    {
                        real s = slope(p[i], p[k]);
                        if (current < s)
                        {
                            current = s;
                            l = i;
                        }
                    }
                }
                if (l >= 0)
                {
                    ts[n++] = dist(p[k], p[l]);
                }
            }
        }

        return n;
    }

    static bool x_check(real rng[2], const vector2 curve[], int sz)
    {
        real t[2] = {0};
        int nn = scan_convex(t, curve, sz);
        if (nn)
        {
            if (nn != 2) return false;

            real t0 = t[0];
            real t1 = t[1];

            if (t0 > t1) std::swap(t0, t1);

            rng[0] = t0;
            rng[1] = t1;
            return true;
        }
        return false;
    }

    static inline vector3 normalize2(const vector3& v)
    {
        real l = v[0] * v[0] + v[1] * v[1];
        if (l >= EPS3)
        {
            l = 1.0 / sqrt(l);
            return vector3(v[0] * l, v[1] * l, 0);
        }
        else
        {
            return vector3(0, 1, 0);
        }
    }
    static inline vector3 cross2(const vector3& y)
    {
        return cross(y, vector3(0, 0, 1));
    }

    static matrix3 getRotate(const vector3& dy)
    {
        static const vector3 z = vector3(0, 0, 1);

        vector3 y = normalize2(dy); //y = normalize(y)
        vector3 x = cross2(y);      //cross(y,z)
        return matrix3(x[0], x[1], x[2], y[0], y[1], y[2], z[0], z[1], z[2]);
    }

    static vector3 sum_dy(const vector3 in[], int sz)
    {
        vector3 dd = vector3(0, 0, 0);
        for (int i = 0; i < sz - 1; i++)
        {
            dd += in[i + 1] - in[i];
        }
        return dd;
    }

    static void rotate_u(vector3 out[], const vector3 in[], int sz)
    {
        vector3 dy = sum_dy(in, sz);
        matrix3 rot = getRotate(dy);
        for (int i = 0; i < sz; i++)
        {
            out[i] = rot * in[i];
        }
    }

    static vector3 make_dPdv(const vector3 P[], int nOrder, int i)
    {
        if (i == 0)
            return (P[i + 1] - P[i]);
        else if (i == nOrder - 1)
            return (P[i] - P[i - 1]);
        else
            return 0.5 * (P[i + 1] - P[i - 1]);
    }

    static bool make_band(vector3 A[], vector3 B[], const vector3 P[], const real R[], int nOrder)
    {
        for (int i = 0; i < nOrder; i++)
        {
            vector3 p = P[i];
            if (i == 0)
                p = evaluateBezier(P, nOrder, -0.0001);
            else if (i == nOrder - 1)
                p = evaluateBezier(P, nOrder, +1.0001);
            if (p.sqr_length() < EPS3) return false;
            vector3 n = -normalize(p);
            vector3 dPdv = make_dPdv(P, nOrder, i);
            vector3 dPdu = cross(dPdv, n);
            if (dPdu.sqr_length() < EPS3) return false;
            dPdu = normalize(dPdu);

            real r = R[i];
            A[i] = p - r * dPdu;
            B[i] = p + r * dPdu;
        }
        return true;
    }

    static int solve2e(real root[], real B, real C)
    {
        real D = B * B - C;
        if (D < 0)
        {
            return 0;
        }
        else if (D == 0)
        {
            double x = -B;
            root[0] = x;
            return 1;
        }
        else
        {
            real x1 = (fabs(B) + sqrt(D));
            if (B >= 0.0)
            {
                x1 = -x1;
            }
            real x2 = C / x1;
            if (x1 > x2) std::swap(x1, x2);
            root[0] = x1;
            root[1] = x2;
            return 2;
        }
    }

    static int solve2e(real root[], real A, real B, real C)
    {
        if (fabs(A) <= std::numeric_limits<real>::epsilon())
        {
            real x = -C / B;
            root[0] = x;
            return 1;
        }
        else
        {
            real D = B * B - A * C;
            if (D < 0)
            {
                return 0;
            }
            else if (D == 0)
            {
                real x = -B / A;
                root[0] = x;
                return 1;
            }
            else
            {
                real x1 = (fabs(B) + sqrt(D)) / A;
                if (B >= 0.0)
                {
                    x1 = -x1;
                }
                real x2 = C / (A * x1);
                if (x1 > x2) std::swap(x1, x2);
                root[0] = x1;
                root[1] = x2;
                return 2;
            }
        }
    }

    static bool test_sphere(isect_info* info, const vector3& p, real r, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        real r2 = r * r;
        vector3 ORG = org;
        vector3 DIR = dir;
        vector3 O = ORG - p;
        real B = dot(O, DIR);
        real C = dot(O, O) - r2;

        real root[2] = {};
        int nRet = solve2e(root, B, C);
        if (!nRet) return false;
        real t0 = root[0];

        if (tmin <= t0 && t0 <= tmax)
        {
            info->c = p;
            info->t = t0;
            return true;
        }
        return false;
    }

    static bool test_cylinder(isect_info* info, const vector3& p, const vector3& q, real r0, real r1, const vector3& org, const vector3& dir, real tmin, real tmax)
    {
        real rr = std::max<real>(r0, r1);
        vector3 ORG = org;
        vector3 n = dir;
        vector3 d = q - p;
        vector3 m = ORG - p;

        real md = dot(m, d);
        real nd = dot(n, d);
        real dd = dot(d, d);

        if (md <= 0.0 && nd <= 0.0) return false;
        if (md >= dd && nd >= 0.0) return false;

        real nn = dot(n, n);
        real mn = dot(m, n);
        real A = dd * nn - nd * nd;
        real k = dot(m, m) - rr * rr;
        real C = dd * k - md * md;
        real B = dd * mn - nd * md; //

        real root[2] = {};
        int nRet = solve2e(root, A, B, C);
        if (nRet)
        {
            real t = root[0];
            if (tmin <= t && t <= tmax)
            {
                real s = md + t * nd;
                s /= dd;
                if (0 <= s && s <= 1)
                {
                    info->t = t;
                    info->v = s;
                    vector3 c = p + s * (q - p);
                    info->c = c;
                    return true;
                }
            }
        }
        return false;
    }

    static bool test_cupsule(isect_info* info, const vector3& P0, const vector3& P1, real R0, real R1, real v0, real v1, real tmin, real tmax)
    {
        vector3 PP[] = {P0, P1};
        real RR[] = {R0, R1};

        bool bRet = false;
        if (test_cylinder(info, PP[0], PP[1], RR[0], RR[1], vector3(0, 0, 0), vector3(0, 0, 1), tmin, tmax))
        {
            tmax = info->t;
            bRet = true;
        }
        if (!bRet)
        {
            if (v0 > 0)
            {
                if (test_sphere(info, PP[0], RR[0], vector3(0, 0, 0), vector3(0, 0, 1), tmin, tmax))
                {
                    info->v = 0;
                    tmax = info->t;
                    bRet = true;
                }
            }

            if (v1 < 1)
            {
                if (test_sphere(info, PP[1], RR[1], vector3(0, 0, 0), vector3(0, 0, 1), tmin, tmax))
                {
                    info->v = 1;
                    tmax = info->t;
                    bRet = true;
                }
            }
        }

        if (bRet)
        {
            real v = info->v;
            v = v0 * (1 - v) + v1 * v;
            info->v = v;
            return true;
        }
        return false;
    }

    template <int Sz>
    static bool test_bezier_clip_l(isect_info* info, const vector3 P[], const real R[], int nOrder, real v0, real v1, real tmin, real tmax)
    {
#if 1 //DIRECT_BILINEAR
        return test_cupsule(info, P[0], P[nOrder - 1], R[0], R[nOrder - 1], v0, v1, tmin, tmax);
#else

        bool bRet = false;
        real delta = real(1.0) / (nOrder - 1);
        for (int i = 0; i < nOrder - 1; i++)
        {
            real tv0 = lerp(v0, v1, i * delta);
            real tv1 = lerp(v0, v1, (i + 1) * delta);
            if (test_cupsule(info, P[i], P[i + 1], R[i], R[i + 1], tv0, tv1, tmin, tmax))
            {
                tmax = info->t;
                bRet = true;
            }
        }
        return bRet;
#endif
    }

    static inline bool is_level(int level, int max_level)
    {
        return level >= max_level;
    }

    static inline bool is_eps(const vector3& min, const vector3& max, real eps)
    {
        real yw = max[1] - min[1];
        if (yw <= eps)
            return true;
        else
            return false;
    }

    static inline real log2(real x)
    {
        static const real LOG2 = 1.0 / log(2.0);
        return log(x) * LOG2;
    }

    static inline bool is_clip(int level, int nc)
    {
        static const int div_level[] =
            {
                1,
                1, 1,                   //2->
                2, 2,                   //4
                3, 3, 3, 3,             //8
                4, 4, 4, 4, 4, 4, 4, 4, //16
            };

        if (!GetUseBezierClip()) return false;

        int nlevel = 0;
        if (nc <= 16)
        {
            nlevel = div_level[nc];
        }
        else
        {
            nlevel = (int)ceil(log2(nc));
        }
        return nlevel <= level;
    }

    template <int Sz>
    static bool get_range(real out[2], const vector3 B[], int nOrder)
    {
        real t0 = 1;
        real t1 = 0;

        vector2 curve[Sz];
        real delta = 1.0 / (nOrder - 1);
        for (int i = 0; i < nOrder; i++)
        {
            curve[i][0] = i * delta;
        }

        real rng[2];
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < nOrder; j++)
            {
                curve[j][1] = B[i * nOrder + j][1]; //Y
            }
            if (x_check(rng, curve, nOrder))
            {
                if (fabs(rng[1] - rng[0]) < delta)
                {
                    t0 = std::min(t0, rng[0]);
                    t1 = std::max(t1, rng[1]);
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        out[0] = t0;
        out[1] = t1;
        return true;
    }

    template <int Sz>
    static bool test_bezier_clip_v(isect_info* info, const vector3 P[], const real R[], int nOrder, real v0, real v1, real tmin, real tmax, int level, int max_level, real eps)
    {
        vector3 min;
        vector3 max;
        real r1 = std::max<real>(R[0], R[nOrder - 1]);
        vector3 Q[Sz];
        rotate_u(Q, P, nOrder);
        get_minmax(min, max, Q, nOrder); //

        if (+r1 < min[0] || max[0] < -r1) return false; //x
        if (+r1 < min[1] || max[1] < -r1) return false; //y

        if (is_eps(min, max, eps) || is_level(level, max_level))
        {
            return test_bezier_clip_l<Sz>(info, P, R, nOrder, v0, v1, tmin, tmax);
        }

        real tt0 = 1;
        real tt1 = 0;
        real tw = 1;

        vector3 B[Sz * 2];
        if (make_band(B, B + nOrder, Q, R, nOrder))
        {
            get_minmax(min, max, B, nOrder * 2); //

            min -= vector3(0, r1, r1);
            max += vector3(0, r1, r1);

            if (0 < min[0] || max[0] < 0) return false;       //x
            if (0 < min[1] || max[1] < 0) return false;       //y
            if (max[2] < tmin || tmax < min[2]) return false; //z

            if (is_clip(level, nOrder))
            {
                real rng[2];
                if (get_range<Sz>(rng, B, nOrder))
                {
                    tt0 = rng[0];
                    tt1 = rng[1];
                    tw = fabs(tt1 - tt0); //
                }
            }
        }

        if (tw >= 0.4)
        {
            real RL[Sz];
            real RR[Sz];
            vector3 PL[Sz];
            vector3 PR[Sz];
            splitBezier(RL, RR, R, nOrder);
            splitBezier(PL, PR, P, nOrder);

            real vm = (v0 + v1) * 0.5;

            bool bRet = false;
            if (P[0][2] <= P[nOrder - 1][2])
            { //L->R
                if (test_bezier_clip_v<Sz>(info, PL, RL, nOrder, v0, vm, tmin, tmax, level + 1, max_level, eps))
                {
                    tmax = info->t;
                    bRet = true;
                }
                if (test_bezier_clip_v<Sz>(info, PR, RR, nOrder, vm, v1, tmin, tmax, level + 1, max_level, eps))
                {
                    tmax = info->t;
                    bRet = true;
                }
            }
            else
            {
                if (test_bezier_clip_v<Sz>(info, PR, RR, nOrder, vm, v1, tmin, tmax, level + 1, max_level, eps))
                {
                    tmax = info->t;
                    bRet = true;
                }
                if (test_bezier_clip_v<Sz>(info, PL, RL, nOrder, v0, vm, tmin, tmax, level + 1, max_level, eps))
                {
                    tmax = info->t;
                    bRet = true;
                }
            }
            return bRet;
        }
        else
        {
            tt0 = std::max<real>(0.0, tt0 - UVEPS);
            tt1 = std::min<real>(tt1 + UVEPS, 1.0);
            real tv0 = lerp(v0, v1, tt0);
            real tv1 = lerp(v0, v1, tt1);

            vector3 tP[Sz];
            real tR[Sz];
            cropBezier<vector3, Sz>(tP, P, nOrder, tt0, tt1);
            cropBezier<real, Sz>(tR, R, nOrder, tt0, tt1);
            return test_bezier_clip_v<Sz>(info, tP, tR, nOrder, tv0, tv1, tmin, tmax, level + 1, max_level, eps);
        }
    }

    template <int Sz>
    static inline bool intersectSegment(isect_info* info, const vector3 P[], const real R[], int nOrder, real tmin, real tmax)
    {
        if (nOrder != 2)
            return test_bezier_clip_v<Sz>(info, P, R, nOrder, 0, 1, tmin, tmax, 0, GetMaxLevel(), EPS);
        else
            return test_bezier_clip_l<Sz>(info, P, R, nOrder, 0, 1, tmin, tmax);
    }

    template <int Sz>
    static inline bool test_(const vector3 P[], const real R[], int nOrder, real v0, real v1, const ray& r, real tmin, real tmax)
    {
        vector3 Q[Sz];
        cropBezier<vector3, Sz>(Q, P, nOrder, v0, v1);

        vector3 min, max;
        get_minmax(min, max, Q, nOrder);
        real r1 = std::max<real>(R[0], R[nOrder - 1]) + EPS;
        min -= vector3(r1, r1, r1);
        max += vector3(r1, r1, r1);

        range_AABB rng;
        if (test_AABB(&rng, min, max, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);

            vector3 org = r.origin();
            vector3 dir = r.direction();
            matrix3 mat;
            getZAlign(mat, dir);
            for (int i = 0; i < nOrder; i++)
            {
                Q[i] = mat_mul(mat, P[i] - org);
            }

            isect_info inf;
            return intersectSegment<Sz>(&inf, &Q[0], &R[0], nOrder, tmin, tmax);
        }
        return false;
    }

    template <int Sz>
    static bool test_(const vector3 P[], const real R[], int nOrder, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax)
    {
        vector3 Q[Sz];
        cropBezier<vector3, Sz>(Q, P, nOrder, v0, v1);

        real S[Sz];
        cropBezier<real, Sz>(S, R, nOrder, v0, v1);

        vector3 min, max;
        get_minmax(min, max, Q, nOrder);
        real r1 = std::max<real>(S[0], S[nOrder - 1]) + EPS;
        min -= vector3(r1, r1, r1);
        max += vector3(r1, r1, r1);
        ;

        range_AABB rng;
        if (test_AABB(&rng, min, max, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);

            vector3 org = r.origin();
            vector3 dir = r.direction();
            matrix3 mat;
            getZAlign(mat, dir);
            for (int i = 0; i < nOrder; i++)
            {
                Q[i] = mat_mul(mat, Q[i] - org);
            }

            isect_info inf;
            if (intersectSegment<Sz>(&inf, &Q[0], &S[0], nOrder, tmin, tmax))
            {
                real t = inf.t;
                real v = inf.v;
                vector3 c = inf.c;

                vector3 p = r.origin() + t * r.direction();
                vector3 n = normalize(p - (mat_mul(transpose(mat), c) + org));

                info->t = t;
                info->position = p;
                info->geometric = n;
                //info->u = u;
                info->rate = v;
                info->center = c;

                return true;
            }
        }

        return false;
    }

    template <int Sz>
    static void finalize_(const vector3 P[], const real R[], int nOrder, curve_test_info* info, const ray& r, real tmin, real tmax)
    {
        int sz = info->p_strand->segments();
        int nseg = info->nseg;

        real v = info->rate;

        vector3 pp = evaluateBezier(P, nOrder, v);
        vector3 tt = normalize(evaluateBezierTangent(P, nOrder, v));
        vector3 gg = info->geometric;
        //vector3 nn = normalize(info->position-pp);

        real radius = evaluateBezier(R, nOrder, v);

        vector3 N = gg; //z
        vector3 V = tt; //y
        vector3 U = normalize(cross(V, N));
        //V = normalize(cross(N,U));
        //vector3 c = info->center;
        //real dx = c[0];
        //real dy = c[1];
        //real u = sqrt(dx*dx+dy*dy)/radius;
        real u = fabs(dot(N, r.direction()));
        info->u = u;
        info->v = (nseg + v) / sz;

        info->normal = N;

        info->tangent = U;
        info->binormal = V;
        info->radius = radius;
    }

    bool cylinder_bezier_curve_tester::test(const vector3 P[], const real R[], int nOrder, real v0, real v1, const ray& r, real tmin, real tmax)
    {
        if (nOrder <= 4) return test_<4>(P, R, nOrder, v0, v1, r, tmin, tmax);
        if (nOrder <= 8) return test_<8>(P, R, nOrder, v0, v1, r, tmin, tmax);
        if (nOrder <= 16) return test_<16>(P, R, nOrder, v0, v1, r, tmin, tmax);
        if (nOrder <= 32) return test_<32>(P, R, nOrder, v0, v1, r, tmin, tmax);

        return false;
    }
    bool cylinder_bezier_curve_tester::test(const vector3 P[], const real R[], int nOrder, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax)
    {
        if (nOrder <= 4) return test_<4>(P, R, nOrder, v0, v1, info, r, tmin, tmax);
        if (nOrder <= 8) return test_<8>(P, R, nOrder, v0, v1, info, r, tmin, tmax);
        if (nOrder <= 16) return test_<16>(P, R, nOrder, v0, v1, info, r, tmin, tmax);
        if (nOrder <= 32) return test_<32>(P, R, nOrder, v0, v1, info, r, tmin, tmax);

        return false;
    }

    void cylinder_bezier_curve_tester::finalize(const vector3 P[], const real R[], int nOrder, curve_test_info* info, const ray& r, real tmin, real tmax)
    {
        if (nOrder <= 4)
        {
            finalize_<4>(P, R, nOrder, info, r, tmin, tmax);
            return;
        }
        if (nOrder <= 8)
        {
            finalize_<8>(P, R, nOrder, info, r, tmin, tmax);
            return;
        }
        if (nOrder <= 16)
        {
            finalize_<16>(P, R, nOrder, info, r, tmin, tmax);
            return;
        }
        if (nOrder <= 32)
        {
            finalize_<32>(P, R, nOrder, info, r, tmin, tmax);
            return;
        }
    }

    vector3 cylinder_bezier_curve_tester::min(const vector3 P[], const real R[], int nOrder)
    {
        real r = R[0];
        vector3 p = P[0];
        for (int i = 1; i < nOrder; i++)
        {
            r = std::max<real>(r, R[i]);
            vector3 c = P[i];
            for (int j = 0; j < 3; j++)
            {
                if (p[j] > c[j]) p[j] = c[j];
            }
        }
        p -= vector3(r, r, r);
        return p;
    }

    vector3 cylinder_bezier_curve_tester::max(const vector3 P[], const real R[], int nOrder)
    {
        real r = R[0];
        vector3 p = P[0];
        for (int i = 1; i < nOrder; i++)
        {
            r = std::max<real>(r, R[i]);
            vector3 c = P[i];
            for (int j = 0; j < 3; j++)
            {
                if (p[j] < c[j]) p[j] = c[j];
            }
        }
        p += vector3(r, r, r);
        return p;
    }

    void cylinder_bezier_curve_tester::set_max_level(int lv)
    {
        SetMaxLevel(lv);
    }

    void cylinder_bezier_curve_tester::set_use_bezierclip(bool b)
    {
        SetUseBezierClip(b);
    }
}
