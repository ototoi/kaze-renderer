#include "bezier_curve_strand.h"
#include "bezier.h"
#include "curve_test_info.h"

namespace kaze
{

    static vector3 Convert(const vector3f& p)
    {
        return vector3(p[0], p[1], p[2]);
    }

    template <class T, class FLOAT>
    static T evaluateBezier(const T v[], int nOrder, FLOAT t)
    {
        return bezier_evaluate(v, nOrder, t);
    }

    base_bezier_curve_strand::base_bezier_curve_strand(
        const std::vector<vector3f>& P,
        const std::vector<float>& R,
        const std::vector<vector3f>& C1,
        const std::vector<vector3f>& C2,
        int nOrder, size_t index)
        : points_(P), rads_(R), c1_(C1), c2_(C2), nOrder_(nOrder), index_(index)
    {
        size_t psz = P.size();
        if (psz < nOrder)
        {
            points_.clear();
        }
    }

    base_bezier_curve_strand::~base_bezier_curve_strand()
    {
        ; //
    }

    int base_bezier_curve_strand::segments() const
    {
        if (points_.empty()) return 0;
        size_t psz = points_.size();
        size_t seg = (psz - 1) / (nOrder_ - 1);
        return (int)seg;
    }

    bool base_bezier_curve_strand::test(int nseg, real v0, real v1, const ray& r, real tmin, real tmax) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<vector3> P(nOrder);
        std::vector<real> R(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            P[i] = Convert(points_[offset + i]);
            R[i] = (real)(rads_[offset + i]);
        }

        return test(&P[0], &R[0], nOrder, v0, v1, r, tmin, tmax);
    }

    bool base_bezier_curve_strand::test(int nseg, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<vector3> P(nOrder);
        std::vector<real> R(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            P[i] = Convert(points_[offset + i]);
            R[i] = (real)(rads_[offset + i]);
        }

        if (test(&P[0], &R[0], nOrder, v0, v1, info, r, tmin, tmax))
        {
            info->nseg = nseg;
            info->p_strand = this;
            info->index = index_;
            return true;
        }
        return false;
    }

    bool base_bezier_curve_strand::test(int nseg, const ray& r, real tmin, real tmax) const
    {
        return this->test(nseg, 0, 1, r, tmin, tmax);
    }

    bool base_bezier_curve_strand::test(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        return this->test(nseg, 0, 1, info, r, tmin, tmax);
    }

    void base_bezier_curve_strand::finalize(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<vector3> P(nOrder);
        std::vector<real> R(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            P[i] = Convert(points_[offset + i]);
            R[i] = (real)(rads_[offset + i]);
        }
        finalize(&P[0], &R[0], nOrder, info, r, tmin, tmax);
        float v = (float)info->rate;

        std::vector<vector3f> C1(nOrder);
        std::vector<vector3f> C2(nOrder);

        for (int i = 0; i < nOrder; i++)
        {
            C1[i] = c1_[offset + i];
            C2[i] = c2_[offset + i];
        }

        vector3f c1 = evaluateBezier(&C1[0], nOrder, v);
        vector3f c2 = evaluateBezier(&C2[0], nOrder, v);
        info->col1 = Convert(c1);
        info->col2 = Convert(c2);
    }

    vector3 base_bezier_curve_strand::min(int nseg) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<vector3> P(nOrder);
        std::vector<real> R(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            P[i] = Convert(points_[offset + i]);
            R[i] = (real)(rads_[offset + i]);
        }
        return min(&P[0], &R[0], nOrder);
    }

    vector3 base_bezier_curve_strand::max(int nseg) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<vector3> P(nOrder);
        std::vector<real> R(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            P[i] = Convert(points_[offset + i]);
            R[i] = (real)(rads_[offset + i]);
        }
        return max(&P[0], &R[0], nOrder);
    }

    void base_bezier_curve_strand::get_position(int nseg, std::vector<vector3>& v) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<vector3> P(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            P[i] = Convert(points_[offset + i]);
        }
        v.swap(P);
    }
    void base_bezier_curve_strand::get_radius(int nseg, std::vector<real>& v) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<real> R(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            R[i] = (real)(rads_[offset + i]);
        }
        v.swap(R);
    }

    base_normal_bezier_curve_strand::base_normal_bezier_curve_strand(
        const std::vector<vector3f>& P,
        const std::vector<float>& R,
        const std::vector<vector3f>& N,
        const std::vector<vector3f>& C1,
        const std::vector<vector3f>& C2,
        int nOrder, size_t index) : points_(P), rads_(R), normals_(N), c1_(C1), c2_(C2), nOrder_(nOrder), index_(index)
    {
        size_t psz = P.size();
        if (psz < nOrder)
        {
            points_.clear();
        }
    }

    base_normal_bezier_curve_strand::~base_normal_bezier_curve_strand()
    {
        ;
    }

    int base_normal_bezier_curve_strand::segments() const
    {
        if (points_.empty()) return 0;
        size_t psz = points_.size();
        size_t seg = (psz - 1) / (nOrder_ - 1);
        return (int)seg;
    }

    bool base_normal_bezier_curve_strand::test(int nseg, real v0, real v1, const ray& r, real tmin, real tmax) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<vector3> P(nOrder);
        std::vector<real> R(nOrder);
        std::vector<vector3> N(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            P[i] = Convert(points_[offset + i]);
            R[i] = (real)(rads_[offset + i]);
            N[i] = Convert(normals_[offset + i]);
        }

        return test(&P[0], &R[0], &N[0], nOrder, v0, v1, r, tmin, tmax);
    }
    bool base_normal_bezier_curve_strand::test(int nseg, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<vector3> P(nOrder);
        std::vector<real> R(nOrder);
        std::vector<vector3> N(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            P[i] = Convert(points_[offset + i]);
            R[i] = (real)(rads_[offset + i]);
            N[i] = Convert(normals_[offset + i]);
        }

        if (test(&P[0], &R[0], &N[0], nOrder, v0, v1, info, r, tmin, tmax))
        {
            info->nseg = nseg;
            info->p_strand = this;
            info->index = index_;
            return true;
        }
        return false;
    }

    bool base_normal_bezier_curve_strand::test(int nseg, const ray& r, real tmin, real tmax) const
    {
        return test(nseg, 0, 1, r, tmin, tmax);
    }

    bool base_normal_bezier_curve_strand::test(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        return test(nseg, 0, 1, info, r, tmin, tmax);
    }

    void base_normal_bezier_curve_strand::finalize(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<vector3> P(nOrder);
        std::vector<real> R(nOrder);
        std::vector<vector3> N(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            P[i] = Convert(points_[offset + i]);
            R[i] = (real)(rads_[offset + i]);
            N[i] = Convert(normals_[offset + i]);
        }
        finalize(&P[0], &R[0], &N[0], nOrder, info, r, tmin, tmax);
        float v = (float)info->rate;

        std::vector<vector3f> C1(nOrder);
        std::vector<vector3f> C2(nOrder);

        for (int i = 0; i < nOrder; i++)
        {
            C1[i] = c1_[offset + i];
            C2[i] = c2_[offset + i];
        }

        vector3f c1 = evaluateBezier(&C1[0], nOrder, v);
        vector3f c2 = evaluateBezier(&C2[0], nOrder, v);
        info->col1 = Convert(c1);
        info->col2 = Convert(c2);
    }

    vector3 base_normal_bezier_curve_strand::min(int nseg) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<vector3> P(nOrder);
        std::vector<real> R(nOrder);
        std::vector<vector3> N(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            P[i] = Convert(points_[offset + i]);
            R[i] = (real)(rads_[offset + i]);
            N[i] = Convert(normals_[offset + i]);
        }
        return min(&P[0], &R[0], &N[0], nOrder);
    }

    vector3 base_normal_bezier_curve_strand::max(int nseg) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<vector3> P(nOrder);
        std::vector<real> R(nOrder);
        std::vector<vector3> N(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            P[i] = Convert(points_[offset + i]);
            R[i] = (real)(rads_[offset + i]);
            N[i] = Convert(normals_[offset + i]);
        }
        return max(&P[0], &R[0], &N[0], nOrder);
    }

    void base_normal_bezier_curve_strand::get_position(int nseg, std::vector<vector3>& v) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<vector3> P(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            P[i] = Convert(points_[offset + i]);
        }
        v.swap(P);
    }
    void base_normal_bezier_curve_strand::get_radius(int nseg, std::vector<real>& v) const
    {
        int nOrder = nOrder_;
        int offset = nseg * (nOrder - 1);

        std::vector<real> R(nOrder);
        for (int i = 0; i < nOrder; i++)
        {
            R[i] = (real)(rads_[offset + i]);
        }
        v.swap(R);
    }
}
