#ifndef KAZE_NORMAL_RIBBON_BEZIER_CURVE_TESTER_H
#define KAZE_NORMAL_RIBBON_BEZIER_CURVE_TESTER_H

#include "curve_strand.h"

namespace kaze
{

    class normal_ribbon_bezier_curve_tester
    {
    public:
        static bool test(const vector3 P[], const real R[], const vector3 N[], int nOrder, real v0, real v1, const ray& r, real tmin, real tmax);
        static bool test(const vector3 P[], const real R[], const vector3 N[], int nOrder, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax);
        static void finalize(const vector3 P[], const real R[], const vector3 N[], int nOrder, curve_test_info* info, const ray& r, real tmin, real tmax);
        static vector3 min(const vector3 P[], const real R[], const vector3 N[], int nOrder);
        static vector3 max(const vector3 P[], const real R[], const vector3 N[], int nOrder);
    };
}

#endif