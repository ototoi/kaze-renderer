#ifndef KAZE_MOTION_POINTS_CURVE_STRAND_H
#define KAZE_MOTION_POINTS_CURVE_STRAND_H

#include "curve_strand.h"
#include <vector>

namespace kaze
{

    class disk_motion_points_curve_strand : public curve_strand
    {
    public:
        disk_motion_points_curve_strand(
            const std::vector<float>& times,
            const std::vector<vector3f>& P,
            const std::vector<float>& R,
            const std::vector<vector3f>& C1,
            const std::vector<vector3f>& C2,
            size_t index = 0);

    public:
        int segments() const;
        bool test(int nseg, const ray& r, real tmin, real tmax) const;
        bool test(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const;
        void finalize(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const;
        vector3 min(int nseg) const;
        vector3 max(int nseg) const;

    protected:
        std::vector<float> times_;
        std::vector<vector3f> P_;
        //std::vector<vector3f> normals_;
        std::vector<float> R_;
        std::vector<vector3f> C1_;
        std::vector<vector3f> C2_;
        size_t index_;
    };

    class normal_disk_motion_points_curve_strand : public disk_motion_points_curve_strand
    {
    public:
        normal_disk_motion_points_curve_strand(
            const std::vector<float>& times,
            const std::vector<vector3f>& P,
            const std::vector<float>& R,
            const std::vector<vector3f>& N,
            const std::vector<vector3f>& C1,
            const std::vector<vector3f>& C2,
            size_t index = 0);

    public:
        int segments() const;
        bool test(int nseg, const ray& r, real tmin, real tmax) const;
        bool test(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const;
        void finalize(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const;
        vector3 min(int nseg) const;
        vector3 max(int nseg) const;

    protected:
        std::vector<vector3f> normals_;
    };

    class sphere_motion_points_curve_strand : public disk_motion_points_curve_strand
    {
    public:
        sphere_motion_points_curve_strand(
            const std::vector<float>& times,
            const std::vector<vector3f>& P,
            const std::vector<float>& R,
            const std::vector<vector3f>& C1,
            const std::vector<vector3f>& C2,
            size_t index = 0);

    public:
        bool test(int nseg, const ray& r, real tmin, real tmax) const;
        bool test(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const;
        void finalize(int nseg, curve_test_info* info, const ray& r, real tmin, real tmax) const;
    };
}

#endif