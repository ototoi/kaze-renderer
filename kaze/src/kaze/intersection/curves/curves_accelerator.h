#ifndef KAZE_CURVES_ACCELERATOR_H
#define KAZE_CURVES_ACCELERATOR_H

#include "types.h"
#include "curve_strand.h"
#include "ray.h"
#include <vector>

namespace kaze
{

    class curves_accelerator
    {
    public:
        virtual ~curves_accelerator() {}
    public:
        virtual bool test(const ray& r, real tmin, real tmax) const = 0;
        virtual bool test(curve_test_info* info, const ray& r, real tmin, real tmax) const = 0;
        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;
    };
}

#endif
