#include "splqbvh_curves_accelerator.h"
#include "intersection_ex.h"

#include "curve_test_info.h"
#include "aligned_vector.hpp"

#include "intersection_ex.h"
#include "lbvh_utility.h"

#include "timer.h"
#include "bezier.h"

#include <memory>
#include <xmmintrin.h>
#include <emmintrin.h>

#if 0
#define PRINTLOG print_log
#else
static void print_dummy(const char* str, ...) {}
#define PRINTLOG print_dummy
#endif

#if 0
#include <omp.h>
#endif
#define DIV_BIT 10
#define DIV_NUM 1024
#define VOX_M 6

#define MIN_FACE 16
#define MAX_FACE 32

static const int STACK_SIZE = ((int)(sizeof(size_t) * 16));

static const double kExtendRadiusRate = 1.4;

namespace kaze
{
    namespace
    {

        struct curve_segment
        {
            const curve_strand* p_strand;
            int nseg;
            real s0;
            real s1;
            vector3 p0;
            vector3 p1;
            real rad;
        };

        struct inter_segment
        {
            real s0;
            real s1;
            vector3 p0;
            vector3 p1;
            real rad;
        };

        typedef curve_segment FACE;
        typedef const curve_segment* PCFACE;
        typedef const PCFACE* face_iter;
        typedef const uint32_t* code_iter;

        template <class T>
        static void cropBezier(T out[], const T v[], int nOrder, real t0, real t1)
        {
            if (t0 <= 0)
            {
                if (1 <= t1)
                {
                    for (int i = 0; i < nOrder; i++)
                        out[i] = v[i];
                }
                else
                {
                    std::vector<T> b(nOrder);
                    bezier_split(out, &b[0], v, nOrder, t1);
                }
            }
            else if (1 <= t1)
            {
                std::vector<T> a(nOrder);
                bezier_split(&a[0], out, v, nOrder, t0);
            }
            else
            {

                std::vector<T> a(nOrder);
                std::vector<T> b(nOrder);
                bezier_split(&a[0], &b[0], v, nOrder, t1);
                bezier_split(&b[0], out, &a[0], nOrder, t0 / t1);
            }
        }

        static void getH(std::vector<real>& out, const std::vector<vector3>& P, const vector3& P0, const vector3& P1)
        {
            int nOrder = (int)P.size();
            vector3 L = P1 - P0;
            out.clear();
            out.push_back(0);
            for (int i = 1; i < nOrder - 1; i++)
            {
                vector3 V1 = P[i] - P0;
                real h = fabs(length(cross(L, V1)) / length(L));
                out.push_back(h);
            }
            out.push_back(0);
        }

        static real max_(const std::vector<real>& R)
        {
            int nOrder = (int)R.size();
            real ret = R[0];
            for (int i = 1; i < nOrder; i++)
                ret = std::max(ret, R[i]);
            return ret;
        }

        static bool checkSplit(const std::vector<vector3>& P)
        {
            int nOrder = (int)P.size();
            vector3 N = P[nOrder - 1] - P[0];
            for (int i = 0; i < nOrder - 1; i++)
            {
                if (dot(N, P[i + 1] - P[i]) <= 0) return true;
            }
            for (int i = 0; i < nOrder - 2; i++)
            {
                if (dot(P[i + 2] - P[i + 1], P[i + 1] - P[i]) <= 0) return true;
            }
            return false;
        }

        static void makeInterSegment(std::vector<inter_segment>& out, const std::vector<vector3>& P, const std::vector<real>& R, real s0, real s1, int level = 0)
        {
            int nOrder = (int)P.size();
            int nMaxLevel = std::min(16, 1 << nOrder);
            {
                bool bEq = true;
                for (int i = 0; i < nOrder - 1; i++)
                {
                    if (P[i] != P[i + 1])
                    {
                        bEq = false;
                        break;
                    }
                }
                if (bEq) return;
            }
            if (nOrder == 2 || level >= nMaxLevel)
            {
                inter_segment seg;
                seg.s0 = s0;
                seg.s1 = s1;
                seg.p0 = P[0];
                seg.p1 = P[nOrder - 1];
                seg.rad = std::max(R[0], R[1]);
                out.push_back(seg);
                return;
            }
            else if (P[0] == P[nOrder - 1])
            {
                std::vector<vector3> P0(nOrder);
                std::vector<vector3> P1(nOrder);
                std::vector<real> R0(nOrder);
                std::vector<real> R1(nOrder);
                bezier_split(&P0[0], &P1[0], &P[0], nOrder, 0.5);
                bezier_split(&R0[0], &R1[0], &R[0], nOrder, 0.5);
                real sm = (s0 + s1) * 0.5;
                makeInterSegment(out, P0, R0, s0, sm, level + 1);
                makeInterSegment(out, P1, R1, sm, s1, level + 1);
                return;
            }
            else
            {
                bool bSplit = checkSplit(P);
                if (!bSplit)
                {
                    real maxR = max_(R);
                    std::vector<real> hs;
                    getH(hs, P, P[0], P[nOrder - 1]);
                    real maxH = max_(hs);
                    real totR = maxH + maxR;
                    if (totR <= maxR * kExtendRadiusRate) //1.1*R
                    {
                        inter_segment seg;
                        seg.s0 = s0;
                        seg.s1 = s1;
                        seg.p0 = P[0];
                        seg.p1 = P[nOrder - 1];
                        seg.rad = totR;
                        out.push_back(seg);
                        return;
                    }
                }

                {
                    std::vector<vector3> P0(nOrder);
                    std::vector<vector3> P1(nOrder);
                    std::vector<real> R0(nOrder);
                    std::vector<real> R1(nOrder);
                    bezier_split(&P0[0], &P1[0], &P[0], nOrder, 0.5);
                    bezier_split(&R0[0], &R1[0], &R[0], nOrder, 0.5);
                    real sm = (s0 + s1) * 0.5;
                    makeInterSegment(out, P0, R0, s0, sm, level + 1);
                    makeInterSegment(out, P1, R1, sm, s1, level + 1);
                    return;
                }
            }
        }

        static void get_minmax(vector3& min, vector3& max, const std::vector<vector3>& P)
        {
            size_t sz = P.size();
            min = max = P[0];
            for (size_t i = 1; i < sz; i++)
            {
                vector3 p = P[i];
                min = vector3(std::min(min[0], p[0]), std::min(min[1], p[1]), std::min(min[2], p[2]));
                max = vector3(std::max(max[0], p[0]), std::max(max[1], p[1]), std::max(max[2], p[2]));
            }
        }

        //For float
        static inline int test_AABB(
            const __m128 bboxes[2][3], //4boxes : min-max[2] of xyz[3] of boxes[4]
            const __m128 org[3],       //ray origin
            const __m128 idir[3],      //ray inveresed direction
            const int sign[3],         //ray xyz direction -> +:0,-:1
            __m128 tmin, __m128 tmax   //ray range tmin-tmax
            )
        {
            // x coordinate
            tmin = _mm_max_ps(
                tmin,
                _mm_mul_ps(_mm_sub_ps(bboxes[sign[0]][0], org[0]), idir[0]));
            tmax = _mm_min_ps(
                tmax,
                _mm_mul_ps(_mm_sub_ps(bboxes[1 - sign[0]][0], org[0]), idir[0]));

            // y coordinate
            tmin = _mm_max_ps(
                tmin,
                _mm_mul_ps(_mm_sub_ps(bboxes[sign[1]][1], org[1]), idir[1]));
            tmax = _mm_min_ps(
                tmax,
                _mm_mul_ps(_mm_sub_ps(bboxes[1 - sign[1]][1], org[1]), idir[1]));

            // z coordinate
            tmin = _mm_max_ps(
                tmin,
                _mm_mul_ps(_mm_sub_ps(bboxes[sign[2]][2], org[2]), idir[2]));
            tmax = _mm_min_ps(
                tmax,
                _mm_mul_ps(_mm_sub_ps(bboxes[1 - sign[2]][2], org[2]), idir[2]));

            return _mm_movemask_ps(_mm_cmpge_ps(tmax, tmin));
        }

        static const size_t EMPTY_MASK = size_t(-1);        //0xFFFF...
        static const size_t SIGN_MASK = ~(EMPTY_MASK >> 1); //0x8000...

        inline bool IsLeaf(size_t i)
        {
            return (SIGN_MASK & i) ? true : false;
        }
        inline bool IsBranch(size_t i)
        {
            return !IsLeaf(i);
        }
        inline bool IsEmpty(size_t i)
        {
            return i == EMPTY_MASK;
        }

        inline size_t MakeLeafIndex(size_t i)
        {
            return SIGN_MASK | i;
        }

        inline size_t GetFaceFirst(size_t i)
        {
            return (~SIGN_MASK) & i;
        }

#pragma pack(push, 4)
        struct SIMDBVHNode
        {
            __m128 bboxes[2][3]; //768
            size_t children[4];  //128
            int axis_top;        //128
            int axis_right;
            int axis_left;
            int reserved;
        };
#pragma pack(pop)

        static vector3 Convert(const vector3f& p)
        {
            return vector3(p[0], p[1], p[2]);
        }

        static int get_node_size(size_t face_num)
        {
            if (face_num <= MIN_FACE) return 1;
            size_t p = face_num / 4;
            if (face_num & 1) p++;
            return std::max<int>(1, 1 + 4 * get_node_size(p));
        }

        static void get_minmax(vector3& min, vector3& max, PCFACE face)
        {
            real r = face->rad;
            vector3 p0 = face->p0;
            vector3 p1 = face->p1;
            min = vector3(std::min(p0[0], p1[0]), std::min(p0[1], p1[1]), std::min(p0[2], p1[2])) - vector3(r, r, r);
            max = vector3(std::max(p0[0], p1[0]), std::max(p0[1], p1[1]), std::max(p0[2], p1[2])) + vector3(r, r, r);
        }

        static void get_minmax(vector3& min, vector3& max, const PCFACE* a, const PCFACE* b)
        {
            static const real FAR = values::far();

            min = vector3(+FAR, +FAR, +FAR);
            max = vector3(-FAR, -FAR, -FAR);

            for (const PCFACE* i = a; i != b; i++)
            {
                vector3 cmin, cmax;
                get_minmax(cmin, cmax, *i);
                for (int j = 0; j < 3; j++)
                {
                    if (cmin[j] < min[j]) min[j] = cmin[j];
                    if (cmax[j] > max[j]) max[j] = cmax[j];
                }
            }
        }

        static void get_minmax(vector3f& min, vector3f& max, const PCFACE* a, const PCFACE* b)
        {
            vector3 tmin, tmax;
            get_minmax(tmin, tmax, a, b);
            for (int i = 0; i < 3; i++)
            {
                min[i] = (float)tmin[i];
                max[i] = (float)tmax[i];
            }
        }

        static void get_minmax(vector3f& pmin, vector3f& pmax, const PCFACE* faces, const size_t* indices, size_t a, size_t b)
        {
            static const real FAR = values::far();
            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            for (size_t i = a; i != b; i++)
            {
                size_t idx = indices[i];
                PCFACE face = faces[idx];
                vector3 cmin;
                vector3 cmax;
                get_minmax(cmin, cmax, face);
                for (int j = 0; j < 3; j++)
                {
                    if (cmin[j] < min[j]) min[j] = cmin[j];
                    if (cmax[j] > max[j]) max[j] = cmax[j];
                }
            }
            pmin = min;
            pmax = max;
        }

        static inline uint32_t partby2(uint32_t n)
        {
            n = (n ^ (n << 16)) & 0xff0000ff;
            n = (n ^ (n << 8)) & 0x0300f00f;
            n = (n ^ (n << 4)) & 0x030c30c3;
            n = (n ^ (n << 2)) & 0x09249249;
            return n;
        }

        static inline uint32_t get_morton_code(uint32_t x, uint32_t y, uint32_t z)
        {
            return (partby2(x) << 2) | (partby2(y) << 1) | (partby2(z));
        }

        static inline uint32_t get_morton_code(const vector3& p, const vector3& min, const vector3& max)
        {
            uint32_t ix = (uint32_t)(DIV_NUM * ((p[0] - min[0]) / (max[0] - min[0])));
            uint32_t iy = (uint32_t)(DIV_NUM * ((p[1] - min[1]) / (max[1] - min[1])));
            uint32_t iz = (uint32_t)(DIV_NUM * ((p[2] - min[2]) / (max[2] - min[2])));
            return get_morton_code(ix, iy, iz);
        }

        static vector3 get_center(PCFACE face)
        {
            vector3 min = face->p0;
            vector3 max = face->p1;
            return (min + max) * real(0.5);
        }

        struct separator
        {
            separator(int level, const uint32_t* codes) : codes_(codes)
            {
                int p = 3 * DIV_BIT - 1 - level;
                nMask_ = 1 << p;
            }
            inline bool operator()(size_t i) const
            {
                return (nMask_ & codes_[i]) == 0;
            }

            uint32_t nMask_;
            const uint32_t* codes_;
        };

        static bool test_face(PCFACE face, const ray& r, real tmin, real tmax)
        {
            return face->p_strand->test(face->nseg, face->s0, face->s1, r, tmin, tmax);
        }

        static bool test_face(PCFACE face, curve_test_info* info, const ray& r, real tmin, real tmax)
        {
            return face->p_strand->test(face->nseg, face->s0, face->s1, info, r, tmin, tmax);
        }

        //visit order table
        //8*16 = 128
        static const int OrderTable[] = {
            0x44444, 0x44444, 0x44444, 0x44444, 0x44444, 0x44444, 0x44444, 0x44444,
            0x44440, 0x44440, 0x44440, 0x44440, 0x44440, 0x44440, 0x44440, 0x44440,
            0x44441, 0x44441, 0x44441, 0x44441, 0x44441, 0x44441, 0x44441, 0x44441,
            0x44401, 0x44401, 0x44410, 0x44410, 0x44401, 0x44401, 0x44410, 0x44410,
            0x44442, 0x44442, 0x44442, 0x44442, 0x44442, 0x44442, 0x44442, 0x44442,
            0x44402, 0x44402, 0x44402, 0x44402, 0x44420, 0x44420, 0x44420, 0x44420,
            0x44412, 0x44412, 0x44412, 0x44412, 0x44421, 0x44421, 0x44421, 0x44421,
            0x44012, 0x44012, 0x44102, 0x44102, 0x44201, 0x44201, 0x44210, 0x44210,
            0x44443, 0x44443, 0x44443, 0x44443, 0x44443, 0x44443, 0x44443, 0x44443,
            0x44403, 0x44403, 0x44403, 0x44403, 0x44430, 0x44430, 0x44430, 0x44430,
            0x44413, 0x44413, 0x44413, 0x44413, 0x44431, 0x44431, 0x44431, 0x44431,
            0x44013, 0x44013, 0x44103, 0x44103, 0x44301, 0x44301, 0x44310, 0x44310,
            0x44423, 0x44432, 0x44423, 0x44432, 0x44423, 0x44432, 0x44423, 0x44432,
            0x44023, 0x44032, 0x44023, 0x44032, 0x44230, 0x44320, 0x44230, 0x44320,
            0x44123, 0x44132, 0x44123, 0x44132, 0x44231, 0x44321, 0x44231, 0x44321,
            0x40123, 0x40132, 0x41023, 0x41032, 0x42301, 0x43201, 0x42310, 0x43210,
        };
    }

    class splqbvh_curves_accelerator_imp
    {
    public:
        splqbvh_curves_accelerator_imp(const std::vector<const curve_strand*>& strands);

    public:
        bool test(const ray& r, real tmin, real tmax) const;
        bool test(curve_test_info* info, const ray& r, real tmin, real tmax) const;
        vector3 min() const { return min_; }
        vector3 max() const { return max_; }
    protected:
        bool test_faces(size_t nFirst, const ray& r, real tmin, real tmax) const;
        bool test_faces(size_t nFirst, curve_test_info* info, const ray& r, real tmin, real tmax) const;
        bool test_inner(const ray& r, real tmin, real tmax) const;
        bool test_inner(curve_test_info* info, const ray& r, real tmin, real tmax) const;

    protected:
        void construct(
            const PCFACE* faces,
            const uint32_t* codes,
            size_t* indices,
            size_t a,
            size_t b);

    protected:
        vector3 min_;
        vector3 max_;
        std::vector<curve_segment> orgs_;
        std::vector<PCFACE> faces_;
        aligned_vector<SIMDBVHNode> nodes_;
    };

    //------------------------------------------------------------------
    splqbvh_curves_accelerator_imp::splqbvh_curves_accelerator_imp(const std::vector<const curve_strand*>& strands)
    {
        timer t;

        PRINTLOG("splqbvh_curves_accelerator_imp:get segments start ...\n");
        t.start();
        size_t ssz = strands.size();
        size_t total_seg = 0;
        for (size_t i = 0; i < ssz; i++) //
        {
            total_seg += strands[i]->segments();
        }
        std::vector<curve_segment> segments;
        segments.reserve(total_seg * 4);

        std::vector<vector3> P;
        std::vector<real> R;
        std::vector<inter_segment> isegs;
        for (size_t i = 0; i < ssz; i++) //
        {
            int psz = strands[i]->segments();
            for (int j = 0; j < psz; j++)
            {
                P.clear();
                R.clear();
                strands[i]->get_position(j, P);
                strands[i]->get_radius(j, R);

                isegs.clear();
                makeInterSegment(isegs, P, R, 0, 1);
                size_t ksz = isegs.size();
                for (size_t k = 0; k < ksz; k++)
                {
                    curve_segment hs;
                    hs.p_strand = strands[i];
                    hs.nseg = j;
                    hs.s0 = isegs[k].s0;
                    hs.s1 = isegs[k].s1;
                    hs.p0 = isegs[k].p0;
                    hs.p1 = isegs[k].p1;
                    hs.rad = isegs[k].rad;
                    segments.push_back(hs);
                }
            }
        }
        t.end();

        PRINTLOG("splqbvh_curves_accelerator_imp:get segments end %d ms\n", t.msec());

        PRINTLOG("splqbvh_curves_accelerator_imp:%u strands\n", ssz);

        PRINTLOG("splqbvh_curves_accelerator_imp:%u segments\n", total_seg);
        PRINTLOG("splqbvh_curves_accelerator_imp:%u segments\n", segments.size());

        orgs_.swap(segments);

        size_t sz = orgs_.size();
        std::vector<PCFACE> pFaces(sz);
        for (size_t i = 0; i < sz; i++)
        {
            pFaces[i] = &orgs_[i];
        }

        vector3 min, max;
        get_minmax(min, max, &pFaces[0], &pFaces[0] + sz);

        std::vector<uint32_t> codes(sz);
        {
            vector3 tmin, tmax;
            get_morton_bound(tmin, tmax, min, max);
            t.start();
            //create_morton_code(codes, begin, sz, tmin, tmax);
            for (size_t i = 0; i < sz; i++)
            {
                codes[i] = get_morton_code(get_center(pFaces[i]), tmin, tmax);
            }
            t.end();
            PRINTLOG("splqbvh_curves_accelerator:create morton code:%d ms\n", t.msec());
        }

        std::vector<size_t> indices(sz);
        for (size_t i = 0; i < sz; i++)
        {
            indices[i] = i;
        }
        t.start();
        construct(&pFaces[0], &codes[0], &indices[0], 0, sz);
        t.end();
        PRINTLOG("splqbvh_curves_accelerator:construct BVH tree:%d ms\n", t.msec());

        min_ = min;
        max_ = max;
    }

    static inline size_t get_branch_node_size(size_t face_num)
    {
        if (face_num <= MIN_FACE) return 1;
        size_t p = face_num / 4;
        if (face_num & 3) p++;
        return std::max<size_t>(1, 1 + 4 * get_branch_node_size(p));
    }
    static inline size_t get_leaf_node_size(size_t face_num)
    {
        return std::max<size_t>(MIN_FACE, face_num + (int)ceil(double(face_num) / (MIN_FACE)));
    }

    static size_t construct_deep(
        std::vector<PCFACE>& out_faces,
        aligned_vector<SIMDBVHNode>& out_nodes,
        vector3f& min, vector3f& max,
        const PCFACE* faces,
        const uint32_t* codes,
        size_t* indices,
        size_t a,
        size_t b,
        int level)
    {
        static const float EPS = std::numeric_limits<float>::epsilon() * 1024;
        static const float FAR = std::numeric_limits<float>::max();

        size_t sz = b - a;
        if (sz == 0) return EMPTY_MASK;
        if (sz <= MIN_FACE || level >= DIV_BIT * 3)
        {
            size_t first = out_faces.size();
            size_t last = first + sz + 1; //zero terminate
            out_faces.resize(last);
            for (size_t i = 0; i < sz; i++)
            {
                out_faces[first + i] = faces[indices[a + i]];
            }
            out_faces[last - 1] = 0;
            get_minmax(min, max, faces, indices, a, b);

            min -= vector3f(EPS, EPS, EPS);
            max += vector3f(EPS, EPS, EPS);

            size_t nRet = MakeLeafIndex(first);
            assert(IsLeaf(nRet));
            return nRet;
        }
        else
        {
            size_t* ia = indices + a;
            size_t* ib = indices + b;
            size_t* ic;
            size_t* il;
            size_t* ir;
            {
                ic = std::partition(ia, ib, separator(level, codes));
                size_t* iters[2];
                size_t* idx[2][2] = {{ia, ic}, {ic, ib}};
                for (int i = 0; i < 2; i++)
                {
                    iters[i] = std::partition(idx[i][0], idx[i][1], separator(level + 1, codes));
                }
                il = iters[0];
                ir = iters[1];
            }

            size_t csz = ic - ia;
            size_t lsz = il - ia;
            size_t rsz = ir - ic;

            size_t offset = out_nodes.size();

            size_t c = a + csz;
            size_t l = a + lsz;
            size_t r = c + rsz;

            SIMDBVHNode node;
            node.axis_top = level % 3;
            node.axis_left = node.axis_right = (level + 1) % 3;
            out_nodes.push_back(node);

            vector3f minmax[4][2];
            for (int i = 0; i < 4; i++)
            {
                minmax[i][0] = vector3f(+FAR, +FAR, +FAR);
                minmax[i][1] = vector3f(-FAR, -FAR, -FAR);
            }

            size_t ranges[4][2];
            ranges[0][0] = a;
            ranges[0][1] = l;
            ranges[1][0] = l;
            ranges[1][1] = c;
            ranges[2][0] = c;
            ranges[2][1] = r;
            ranges[3][0] = r;
            ranges[3][1] = b;

            for (int i = 0; i < 4; i++)
            {
                volatile size_t index = construct_deep(out_faces, out_nodes, minmax[i][0], minmax[i][1], faces, codes, indices, ranges[i][0], ranges[i][1], level + 2);
                out_nodes[offset].children[i] = index;
            }

            //convert & swizzle
            float bboxes[2][3][4];
            //for(int m = 0;m<2;m++){//minmax
            for (int j = 0; j < 3; j++)
            { //xyz
                for (int k = 0; k < 4; k++)
                {                                      //box
                    bboxes[0][j][k] = minmax[k][0][j]; //
                    bboxes[1][j][k] = minmax[k][1][j]; //
                }
            }
            //}

            //for(int i = 0;i<4;i++){
            for (int m = 0; m < 2; m++)
            { //minmax
                for (int j = 0; j < 3; j++)
                { //xyz
                    out_nodes[offset].bboxes[m][j] = _mm_setzero_ps();
                    out_nodes[offset].bboxes[m][j] = _mm_loadu_ps(bboxes[m][j]);
                }
            }
            //}

            min = minmax[0][0];
            max = minmax[0][1];
            for (int i = 1; i < 4; i++)
            {
                for (int j = 0; j < 3; j++)
                { //xyz
                    if (min[j] > minmax[i][0][j]) min[j] = minmax[i][0][j];
                    if (max[j] < minmax[i][1][j]) max[j] = minmax[i][1][j];
                }
            }

            min -= vector3f(EPS, EPS, EPS);
            max += vector3f(EPS, EPS, EPS);

            return offset;
        }
    }

    void splqbvh_curves_accelerator_imp::construct(
        const PCFACE* faces,
        const uint32_t* codes,
        size_t* indices,
        size_t a,
        size_t b)
    {
        static const float FAR = std::numeric_limits<float>::max();
        static const int level = 0;

        size_t sz = b - a;
        if (sz == 0) return;
        size_t* ia = indices + a;
        size_t* ib = indices + b;
        size_t* ic = NULL;
        ;
        size_t* il = NULL;
        size_t* ir = NULL;
        {
            ic = std::partition(ia, ib, separator(level, codes));
            size_t* iters[2] = {0, 0};
            size_t* idx[2][2] = {};
            idx[0][0] = ia;
            idx[0][1] = ic;
            idx[1][0] = ic;
            idx[1][1] = ib;
            //{{ia,ic},{ic,ib}};
            {
                //#pragma omp parallel for
                for (int i = 0; i < 2; i++)
                {
                    iters[i] = std::partition(idx[i][0], idx[i][1], separator(level + 1, codes));
                }
            }
            il = iters[0];
            ir = iters[1];
        }

        size_t csz = ic - ia;
        size_t lsz = il - ia;
        size_t rsz = ir - ic;

        size_t offset = nodes_.size();

        size_t c = a + csz;
        size_t l = a + lsz;
        size_t r = c + rsz;

        SIMDBVHNode node;
        node.axis_top = level & 3;
        node.axis_left = node.axis_right = (level + 1) % 3;
        nodes_.push_back(node);

        vector3f minmax[4][2];
        for (int i = 0; i < 4; i++)
        {
            minmax[i][0] = vector3f(+FAR, +FAR, +FAR);
            minmax[i][1] = vector3f(-FAR, -FAR, -FAR);
        }

        size_t ranges[4][2];
        ranges[0][0] = a;
        ranges[0][1] = l;
        ranges[1][0] = l;
        ranges[1][1] = c;
        ranges[2][0] = c;
        ranges[2][1] = r;
        ranges[3][0] = r;
        ranges[3][1] = b;

        {
            size_t sub_indices[4];
            std::vector<PCFACE> sub_faces[4];
            aligned_vector<SIMDBVHNode> sub_nodes[4];
            for (int i = 0; i < 4; i++)
            {
                size_t fsz = ranges[i][1] - ranges[i][0];
                sub_faces[i].reserve(get_leaf_node_size(fsz));
                sub_nodes[i].reserve(get_branch_node_size(fsz));
            }

            {
#pragma omp parallel for
                for (int i = 0; i < 4; i++)
                {
                    sub_indices[i] = construct_deep(sub_faces[i], sub_nodes[i], minmax[i][0], minmax[i][1], faces, codes, indices, ranges[i][0], ranges[i][1], level + 2);
                }
            }

            PRINTLOG("splqbvh_curves_accelerator:construct deep.\n");

            size_t face_offsets[4 + 1];
            size_t node_offsets[4 + 1];
            face_offsets[0] = 0;
            node_offsets[0] = 1;
            for (int i = 0; i < 4; i++)
            {
                face_offsets[i + 1] = sub_faces[i].size();
                node_offsets[i + 1] = sub_nodes[i].size();
            }
            for (int i = 0; i < 4; i++)
            {
                face_offsets[i + 1] += face_offsets[i];
                node_offsets[i + 1] += node_offsets[i];
            }

            for (int k = 0; k < 4; k++)
            {
                size_t idx = sub_indices[k];
                if (IsBranch(idx))
                { //
                    idx += node_offsets[k];
                    sub_indices[k] = idx;
                }
                else if (!IsEmpty(idx))
                { //
                    idx = GetFaceFirst(idx);
                    idx += face_offsets[k];
                    sub_indices[k] = MakeLeafIndex(idx);
                }
                nodes_[offset].children[k] = sub_indices[k];
            }

            faces_.resize(face_offsets[4]);
            nodes_.resize(node_offsets[4]);

            {
#pragma omp parallel for
                for (int i = 0; i < 4; i++)
                {
                    aligned_vector<SIMDBVHNode>& nodes = sub_nodes[i];
                    size_t jsz = nodes.size();
                    for (size_t j = 0; j < jsz; j++)
                    {
                        SIMDBVHNode& nd = nodes[j];
                        for (int k = 0; k < 4; k++)
                        {
                            size_t idx = nd.children[k];
                            if (IsBranch(idx))
                            { //
                                idx += node_offsets[i];
                                nd.children[k] = idx;
                            }
                            else if (!IsEmpty(idx))
                            { //
                                idx = GetFaceFirst(idx);
                                idx += face_offsets[i];
                                nd.children[k] = MakeLeafIndex(idx);
                            }
                        }
                    }

                    if (!sub_faces[i].empty())
                    {
                        memcpy(&faces_[face_offsets[i]], &(sub_faces[i][0]), sizeof(PCFACE) * sub_faces[i].size());
                    }
                    if (!sub_nodes[i].empty())
                    {
                        memcpy(&nodes_[node_offsets[i]], &(sub_nodes[i][0]), sizeof(SIMDBVHNode) * sub_nodes[i].size());
                    }
                }
            }
        }

        //convert & swizzle
        float bboxes[2][3][4];
        //for(int m = 0;m<2;m++){//minmax
        for (int j = 0; j < 3; j++)
        { //xyz
            for (int k = 0; k < 4; k++)
            {                                      //box
                bboxes[0][j][k] = minmax[k][0][j]; //
                bboxes[1][j][k] = minmax[k][1][j]; //
            }
        }
        //}

        //for(int i = 0;i<4;i++){
        for (int m = 0; m < 2; m++)
        { //minmax
            for (int j = 0; j < 3; j++)
            { //xyz
                nodes_[offset].bboxes[m][j] = _mm_setzero_ps();
                nodes_[offset].bboxes[m][j] = _mm_loadu_ps(bboxes[m][j]);
            }
        }
        //}
    }

    bool splqbvh_curves_accelerator_imp::test_faces(size_t nFirst, const ray& r, real tmin, real tmax) const
    {
        const PCFACE* faces = &faces_[0];
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (test_face(faces[i], r, tmin, tmax)) return true;
                i++;
            }
        }
        return false;
    }

    bool splqbvh_curves_accelerator_imp::test_faces(size_t nFirst, curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        bool bRet = false;
        const PCFACE* faces = &faces_[0];
        {
            size_t i = nFirst;
            while (faces[i])
            {
                if (test_face(faces[i], info, r, tmin, tmax))
                {
                    tmax = info->t;
                    bRet = true;
                }
                i++;
            }
        }
        return bRet;
    }

    //-------------------------------------------------------------

    bool splqbvh_curves_accelerator_imp::test_inner(const ray& r, real tmin, real tmax) const
    {
        if (nodes_.empty()) return false;

        __m128 sseOrg[3];
        __m128 sseiDir[3];
        int sign[3];
        __m128 sseTMin;
        __m128 sseTMax;

        sseOrg[0] = _mm_set1_ps(float(r.origin()[0]));
        sseOrg[1] = _mm_set1_ps(float(r.origin()[1]));
        sseOrg[2] = _mm_set1_ps(float(r.origin()[2]));

        sseiDir[0] = _mm_set1_ps(float(r.inversed_direction()[0]));
        sseiDir[1] = _mm_set1_ps(float(r.inversed_direction()[1]));
        sseiDir[2] = _mm_set1_ps(float(r.inversed_direction()[2]));

        int phase = r.phase();
        sign[0] = (phase >> 0) & 1;
        sign[1] = (phase >> 1) & 1;
        sign[2] = (phase >> 2) & 1;

        sseTMin = _mm_set1_ps(float(tmin));
        sseTMax = _mm_set1_ps(float(tmax));

        const SIMDBVHNode* nodes = &nodes_[0];

        int todoNode = 0;
        size_t nodeStack[STACK_SIZE];
        nodeStack[0] = 0;

        //bool bRet = false;
        while (todoNode >= 0)
        {
            size_t idx = nodeStack[todoNode];

            if (IsBranch(idx))
            {
                todoNode--; //pop stack
                const SIMDBVHNode& node = nodes[idx];

                int HitMask = test_AABB(node.bboxes, sseOrg, sseiDir, sign, sseTMin, sseTMax);

                if (HitMask)
                {
                    int NodeIdx = (sign[node.axis_top] << 2) | (sign[node.axis_left] << 1) | (sign[node.axis_right]);
                    int Order = OrderTable[HitMask * 8 + NodeIdx];

                    while (!(Order & 0x4))
                    {
                        todoNode++;
                        nodeStack[todoNode] = node.children[Order & 0x3];
                        Order >>= 4;
                    }
                }
            }
            else
            { // if(IsLeaf(idx))
                todoNode--;
                if (IsEmpty(idx)) continue;

                size_t fi = GetFaceFirst(idx); //convert face index.

                if (this->test_faces(fi, r, tmin, tmax))
                {
                    return true; //true
                }
            }

            assert(todoNode < STACK_SIZE);
        }

        return false;
    }

    bool splqbvh_curves_accelerator_imp::test_inner(curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        if (nodes_.empty()) return false;

        __m128 sseOrg[3];
        __m128 sseiDir[3];
        int sign[3];
        __m128 sseTMin;
        __m128 sseTMax;

        sseOrg[0] = _mm_set1_ps(float(r.origin()[0]));
        sseOrg[1] = _mm_set1_ps(float(r.origin()[1]));
        sseOrg[2] = _mm_set1_ps(float(r.origin()[2]));

        sseiDir[0] = _mm_set1_ps(float(r.inversed_direction()[0]));
        sseiDir[1] = _mm_set1_ps(float(r.inversed_direction()[1]));
        sseiDir[2] = _mm_set1_ps(float(r.inversed_direction()[2]));

        int phase = r.phase();
        sign[0] = (phase >> 0) & 1;
        sign[1] = (phase >> 1) & 1;
        sign[2] = (phase >> 2) & 1;

        sseTMin = _mm_set1_ps(float(tmin));
        sseTMax = _mm_set1_ps(float(tmax));

        const SIMDBVHNode* nodes = &nodes_[0];

        int todoNode = 0;
        size_t nodeStack[STACK_SIZE];
        nodeStack[0] = 0;

        bool bRet = false;
        while (todoNode >= 0)
        {
            size_t idx = nodeStack[todoNode];

            if (IsBranch(idx))
            {
                todoNode--; //pop stack
                const SIMDBVHNode& node = nodes[idx];

                int HitMask = test_AABB(node.bboxes, sseOrg, sseiDir, sign, sseTMin, sseTMax);

                if (HitMask)
                {
                    int NodeIdx = (sign[node.axis_top] << 2) | (sign[node.axis_left] << 1) | (sign[node.axis_right]);
                    int Order = OrderTable[HitMask * 8 + NodeIdx];

                    while (!(Order & 0x4))
                    {
                        todoNode++;
                        nodeStack[todoNode] = node.children[Order & 0x3];
                        Order >>= 4;
                    }
                }
            }
            else
            { // if(IsLeaf(idx))
                todoNode--;
                if (IsEmpty(idx)) continue;

                size_t fi = GetFaceFirst(idx); //convert face index.

                if (this->test_faces(fi, info, r, tmin, tmax))
                {
                    bRet = true;
                    tmax = (real)info->t;
                    sseTMax = _mm_set1_ps(float(tmax));
                }
            }

            assert(todoNode < STACK_SIZE);
        }

        return bRet;
    }

    bool splqbvh_curves_accelerator_imp::test(const ray& r, real tmin, real tmax) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            return test_inner(r, tmin, tmax);
        }
        else
        {
            return false;
        }
    }

    bool splqbvh_curves_accelerator_imp::test(curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        range_AABB rng;
        if (test_AABB(&rng, min_, max_, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);
            return test_inner(info, r, tmin, tmax);
        }
        else
        {
            return false;
        }
    }

    //----------------------------------------------------------------------------------------------
    splqbvh_curves_accelerator::splqbvh_curves_accelerator(const std::vector<const curve_strand*>& strands)
    {
        imp_ = new splqbvh_curves_accelerator_imp(strands);
    }

    splqbvh_curves_accelerator::~splqbvh_curves_accelerator()
    {
        delete imp_;
    }

    bool splqbvh_curves_accelerator::test(const ray& r, real tmin, real tmax) const
    {
        return imp_->test(r, tmin, tmax);
    }

    bool splqbvh_curves_accelerator::test(curve_test_info* info, const ray& r, real tmin, real tmax) const
    {
        return imp_->test(info, r, tmin, tmax);
    }

    vector3 splqbvh_curves_accelerator::min() const
    {
        return imp_->min();
    }

    vector3 splqbvh_curves_accelerator::max() const
    {
        return imp_->max();
    }
}
