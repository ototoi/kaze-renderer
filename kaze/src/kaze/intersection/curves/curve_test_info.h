#ifndef KAZE_CURVE_TEST_INFO_H
#define KAZE_CURVE_TEST_INFO_H

#include "types.h"
#include "color.h"

namespace kaze
{

    class curve_strand;
    struct curve_test_info
    {
        real t;
        real rate;
        real d;
        real u;
        real v;
        real radius;
        vector3 center;
        vector3 position;
        vector3 normal;
        vector3 geometric;
        vector3 tangent;
        vector3 binormal;
        color3 col1;
        color3 col2;
        const curve_strand* p_strand;
        int nseg;
        size_t index;
    };
}

#endif