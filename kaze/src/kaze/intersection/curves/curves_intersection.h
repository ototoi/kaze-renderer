#ifndef KAZE_CURVES_INTERSECTION_H
#define KAZE_CURVES_INTERSECTION_H

#include "bounded_intersection.h"
#include "curves/bezier_curves_loader.h"
#include "parameter_map.h"

namespace kaze
{

    class curves_intersection_imp;
    class curves_intersection : public bounded_intersection
    {
    public:
        curves_intersection(const bezier_curves_loader& loader, const parameter_map& param);
        ~curves_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        curves_intersection_imp* imp_;
    };
}

#endif
