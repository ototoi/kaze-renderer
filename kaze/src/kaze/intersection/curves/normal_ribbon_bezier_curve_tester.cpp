#include "normal_ribbon_bezier_curve_tester.h"
#include "curve_test_info.h"
#include "bezier.h"
#include "patch/bilinear_patch_intersection.h"
#include "intersection_ex.h"
#include <vector>

#define DIRECT_BILINEAR 0
#define USE_BEZIERCLIP 1
#define USE_COARSESORT 1

#ifdef _MSC_VER
#define INLINE __forceinline
//define INLINE inline
#elif defined(__GNUC__)
#define INLINE __inline__
//define INLINE inline
#else
#define INLINE inline
#endif

namespace kaze
{
    static const real EPS = 1e-4;
    static const real UVEPS = 1e-2;
    static const real EPS3 = std::numeric_limits<real>::epsilon();

    static int max_level_ = 8;
    static bool use_bezierclip_ = (bool)USE_BEZIERCLIP;

    static int GetMaxLevel()
    {
        return max_level_;
    }

    static void SetMaxLevel(int lv)
    {
        if (lv >= 16)
        {
            lv = 16;
        }
        max_level_ = lv;
    }

    static bool GetUseBezierClip()
    {
        return use_bezierclip_;
    }

    static void SetUseBezierClip(bool b)
    {
        use_bezierclip_ = b;
    }

    namespace
    {
        struct isect_info
        {
            real t;
            real u;
            real v;
        };
    }

    static void get_minmax(vector3& min, vector3& max, const vector3 P[], int nOrder)
    {
        min = P[0];
        max = P[0];
        for (int i = 1; i < nOrder; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                min[j] = std::min(min[j], P[i][j]);
                max[j] = std::max(max[j], P[i][j]);
            }
        }
    }

    static inline vector3 mat_mul(const matrix3& m, const vector3& v)
    {
        return m * v;
    }

    inline vector3 mul_normal(const matrix3& m, const vector3& v)
    {
        return vector3(
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
    }

    template <class T>
    static void splitBezier(T left[], T right[], const T v[], int nOrder)
    {
        bezier_split(left, right, v, nOrder, 0.5);
    }

    template <class T, int Sz>
    static void cropBezier(T out[], const T v[], int nOrder, real t0, real t1)
    {
        bezier_crop(out, v, nOrder, t0, t1);
    }

    template <class T>
    static T evaluateBezier(const T v[], int nOrder, real t)
    {
        return bezier_evaluate(v, nOrder, t);
    }

    template <class T>
    static T evaluateBezierTangent(const T v[], int nOrder, real t)
    {
        return bezier_evaluate_deriv(v, nOrder, t);
    }

    template <class T>
    static T evaluateLinear(const T v[], int nOrder, real u)
    {
        real t = real(1) - (real(1) - u);
        real s = real(1) - t;
        return s * v[0] + t * v[nOrder - 1];
    }

    static inline real lerp(real a, real b, real u)
    {
        real t = real(1) - (real(1) - u);
        real s = real(1) - t;
        return s * a + t * b;
    }

    static void getZAlign(matrix3& mat, const vector3& dir)
    {
        vector3 z = dir;
        int plane = 0;
        if (fabs(z[1]) < fabs(z[plane])) plane = 1;
        if (fabs(z[2]) < fabs(z[plane])) plane = 2;
        vector3 x = vector3(0, 0, 0);
        x[plane] = 1;
        vector3 y = normalize(cross(z, x));
        x = cross(y, z);
        mat = matrix3(x[0], x[1], x[2], y[0], y[1], y[2], z[0], z[1], z[2]);
    }

    static INLINE bool scan_minmax(const vector2 p[], int sz)
    {
        int nUpper = 0;
        int nLower = 0;
        for (int i = 0; i < sz; i++)
        {
            if (p[i][1] > 0)
                nUpper++;
            else
                nLower++;
            if (nUpper && nLower) return true;
        }
        return false;
    }

    static INLINE real slope(const vector2& a, const vector2& b)
    {
        vector2 dif = b - a;
        return fabs(dif[0] / dif[1]);
    }

    static INLINE real dist(const vector2& p0, const vector2& p1)
    {
        real tt = fabs(p0[1] / (p1[1] - p0[1]));
        real t = p0[0] + tt * (p1[0] - p0[0]);
        return t;
    }

    static INLINE int scan_convex(real ts[], const vector2 p[], int sz)
    {
        if (!scan_minmax(p, sz))
        {
            return 0;
        }
        int n = 0;
        {
            int k = -1;
            int l = -1;
            real current = values::far();
            //int sz = (int)p.size();
            for (int i = 1; i < sz; i++)
            {
                if (p[i][1] * p[0][1] < 0)
                {
                    real s = slope(p[i], p[0]);
                    if (s < current)
                    {
                        current = s;
                        k = i;
                    }
                }
            }
            if (k >= 0)
            {
                current = 0;
                for (int i = 0; i < k; i++)
                {
                    if (p[i][1] * p[k][1] < 0)
                    {
                        real s = slope(p[i], p[k]);
                        if (current < s)
                        {
                            current = s;
                            l = i;
                        }
                    }
                }
                if (l >= 0)
                {
                    ts[n++] = dist(p[l], p[k]);
                }
            }
        }
        {
            int k = -1;
            int l = -1;
            real current = values::far();
            //int sz = (int)p.size();
            for (int i = 0; i < sz - 1; i++)
            {
                if (p[i][1] * p[sz - 1][1] < 0)
                {
                    real s = slope(p[i], p[sz - 1]);
                    if (s < current)
                    {
                        current = s;
                        k = i;
                    }
                }
            }
            if (k >= 0)
            {
                current = 0;
                for (int i = k + 1; i < sz; i++)
                {
                    if (p[i][1] * p[k][1] < 0)
                    {
                        real s = slope(p[i], p[k]);
                        if (current < s)
                        {
                            current = s;
                            l = i;
                        }
                    }
                }
                if (l >= 0)
                {
                    ts[n++] = dist(p[k], p[l]);
                }
            }
        }

        return n;
    }

    static bool x_check(real rng[2], const vector2 curve[], int sz)
    {
        real t[4] = {0};
        int nn = scan_convex(t, curve, sz);
        if (nn)
        {
            real t0 = t[0];
            real t1 = t[0];

            for (int i = 1; i < nn; i++)
            {
                t0 = std::min(t0, t[i]);
                t1 = std::max(t1, t[i]);
            }

            rng[0] = t0;
            rng[1] = t1;
            return true;
        }
        return false;
    }

    static inline vector3 normalize2(const vector3& v)
    {
        real l = v[0] * v[0] + v[1] * v[1];
        l = 1.0 / sqrt(l);
        return vector3(v[0] * l, v[1] * l, 0);
    }
    static inline vector3 cross2(const vector3& x)
    {
        return vector3(
            -x[1], //xyzzy
            +x[0], //yzxxz
            0);
    }

    static matrix3 getRotate(const vector3& dy)
    {
        static const vector3 z = vector3(0, 0, 1);
        vector3 y = normalize2(dy); //y = normalize(y)
        vector3 x = cross2(y);      //cross(y,z)
        return matrix3(x[0], x[1], x[2], y[0], y[1], y[2], z[0], z[1], z[2]);
    }

    static vector3 sum_dy(const vector3 in[], int sz)
    {
        vector3 dd = vector3(0, 0, 0);
        for (int i = 0; i < sz - 1; i++)
        {
            dd += in[i + 1] - in[i];
        }
        return dd;
    }

    static void rotate_u(vector3 Q[], vector3 G[], const vector3 P[], const vector3 N[], int sz)
    {
        vector3 dy = sum_dy(P, sz);
        matrix3 rot = getRotate(dy);
        //matrix3 irot = transpose(rot);
        for (int i = 0; i < sz; i++)
        {
            Q[i] = rot * P[i];
            G[i] = rot * N[i]; //mul_normal(irot, N[i]);
        }
    }

    static vector3 make_dPdv_(const vector3 P[], int nOrder, int i)
    {
        if (i == 0)
            return (P[i + 1] - P[i]);
        else if (i == nOrder - 1)
            return (P[i] - P[i - 1]);
        else
            return 0.5 * (P[i + 1] - P[i - 1]);
    }

    static vector3 make_dPdv(const vector3 P[], int nOrder, int i)
    {
        vector3 dPdv = P[nOrder - 1] - P[0];
        for (int k = i; i < nOrder; i++)
        {
            vector3 dPdv = make_dPdv_(P, nOrder, k);
            if (dPdv.sqr_length() >= EPS3) return dPdv;
        }
        return dPdv;
    }

    static bool make_band(vector3 A[], vector3 B[], const vector3 P[], const real R[], const vector3 N[], int nOrder)
    {
        for (int i = 0; i < nOrder; i++)
        {
            vector3 dPdv = normalize(make_dPdv(P, nOrder, i));
            vector3 n = normalize(N[i]);
            vector3 dPdu = cross(dPdv, n);
            if (dPdu.sqr_length() < EPS3) return false;
            dPdu = normalize(dPdu);
            vector3 p = P[i];
            if (i == 0)
                p = evaluateBezier(P, nOrder, -0.0001);
            else if (i == nOrder - 1)
                p = evaluateBezier(P, nOrder, +1.0001);
            real r = R[i];
            A[i] = p - r * dPdu;
            B[i] = p + r * dPdu;
        }
        return true;
    }

    template <int Sz>
    static bool test_bezier_clip_l(isect_info* info, const vector3 P[], const real R[], const vector3 N[], int nOrder, real v0, real v1, real tmin, real tmax)
    {
        real t;
        real u;
        real v;
#if DIRECT_BILINEAR
        vector3 B[Sz * 2];
        if (!make_band(B, B + nOrder, P, R, N, nOrder, false)) return false;

        vector3 tP[4];
        tP[0] = B[0];
        tP[1] = B[nOrder - 1];
        tP[2] = B[nOrder];
        tP[3] = B[2 * nOrder - 1];

        if (test_bilinear_ribbon(&t, &u, &v, tP, tmin, tmax))
        {
            v = v0 * (1 - v) + v1 * v;
            if (0 <= v && v <= 1)
            {
                info->u = u;
                info->v = v;
                info->t = t;
                return true;
            }
        }
        return false;
#else
        vector3 B[Sz * 2];
        if (!make_band(B, B + nOrder, P, R, N, nOrder)) return false;

        bool bRet = false;
        //int nPu = 1;
        int nPv = nOrder - 1;
        real dv = real(1) / nPv;

        vector3 tP[4];
        for (int j = 0; j < nPv; j++)
        {
            {
                tP[0] = B[j];
                tP[1] = B[j + nOrder];
                tP[2] = B[j + 1];
                tP[3] = B[j + 1 + nOrder];
                if (test_bilinear_ribbon(&t, &u, &v, tP, tmin, tmax))
                {
                    v = lerp(j * dv, (j + 1) * dv, v);
                    v = v0 * (1 - v) + v1 * v;
                    if (0 <= v && v <= 1)
                    {
                        tmax = t;
                        info->u = u;
                        info->v = v;
                        info->t = t;
                        bRet = true;
                    }
                }
            }
        }
        return bRet;
#endif
    }

    static inline bool is_level(int level, int max_level)
    {
        return level >= max_level;
    }

    static inline bool is_eps(const vector3& min, const vector3& max, real eps)
    {
        real yw = max[1] - min[1];
        if (yw <= eps)
            return true;
        else
            return false;
    }

    static inline real log2(real x)
    {
        static const real LOG2 = 1.0 / log(2.0);
        return log(x) * LOG2;
    }

    static inline bool is_clip(int level, int nc)
    {
        static const int div_level[] =
            {
                1,
                1, 1,                   //2->
                2, 2,                   //4
                3, 3, 3, 3,             //8
                4, 4, 4, 4, 4, 4, 4, 4, //16
            };

        if (!GetUseBezierClip()) return false;

        int nlevel = 0;
        if (nc <= 16)
        {
            nlevel = div_level[nc];
        }
        else
        {
            nlevel = (int)ceil(log2(nc));
        }
        return nlevel <= level;
    }

    template <int Sz>
    static bool get_range(real out[2], const vector3 B[], int nOrder)
    {
        real t0 = 1;
        real t1 = 0;

        vector2 curve[Sz];
        real delta = 1.0 / (nOrder - 1);
        for (int i = 0; i < nOrder; i++)
        {
            curve[i][0] = i * delta;
        }

        real rng[2];
        for (int i = 0; i < 2; i++)
        {
            for (int j = 0; j < nOrder; j++)
            {
                curve[j][1] = B[i * nOrder + j][1]; //Y
            }
            if (x_check(rng, curve, nOrder))
            {
                t0 = std::min(t0, rng[0]);
                t1 = std::max(t1, rng[1]);
            }
            else
            {
                return false;
            }
        }

        out[0] = t0;
        out[1] = t1;
        return true;
    }

    // || is_flat(P,nOrder, eps)
    template <int Sz>
    static bool test_bezier_clip_v(isect_info* info, const vector3 P[], const real R[], const vector3 N[], int nOrder, real v0, real v1, real tmin, real tmax, int level, int max_level, real eps)
    {
        vector3 min;
        vector3 max;
        real r1 = std::max<real>(R[0], R[nOrder - 1]);
        vector3 Q[Sz];
        vector3 G[Sz];
        rotate_u(Q, G, P, N, nOrder);
        get_minmax(min, max, Q, nOrder); //

        if (+r1 < min[0] || max[0] < -r1) return false; //x
        if (+r1 < min[1] || max[1] < -r1) return false; //y

        if (is_eps(min, max, eps) || is_level(level, max_level))
        {
            return test_bezier_clip_l<Sz>(info, P, R, N, nOrder, v0, v1, tmin, tmax);
        }

        real tt0 = 1;
        real tt1 = 0;
        real tw = 1;

        for (int i = 0; i < nOrder; i++)
        {
            G[i] = normalize(G[i]);
        }

        vector3 B[Sz * 2];
        if (make_band(B, B + nOrder, Q, R, G, nOrder))
        {
            get_minmax(min, max, B, nOrder * 2); //

            if (0 < min[0] || max[0] < 0) return false;       //x
            if (0 < min[1] || max[1] < 0) return false;       //y
            if (max[2] < tmin || tmax < min[2]) return false; //z

            if (is_clip(level, nOrder))
            {
                real rng[2];
                if (get_range<Sz>(rng, B, nOrder))
                {
                    tt0 = rng[0];
                    tt1 = rng[1];
                    tw = fabs(tt1 - tt0); //
                }
            }
        }

        if (tw >= 0.4)
        {
            real RL[Sz];
            real RR[Sz];
            vector3 PL[Sz];
            vector3 PR[Sz];
            vector3 NL[Sz];
            vector3 NR[Sz];
            splitBezier(RL, RR, R, nOrder);
            splitBezier(PL, PR, P, nOrder);
            splitBezier(NL, NR, N, nOrder);
            for (int i = 0; i < nOrder; i++)
            {
                NL[i] = normalize(NL[i]);
                NR[i] = normalize(NR[i]);
            }

            real vm = (v0 + v1) * 0.5;

            bool bRet = false;
            if (P[0][2] <= P[nOrder - 1][2])
            { //L->R
                if (test_bezier_clip_v<Sz>(info, PL, RL, NL, nOrder, v0, vm, tmin, tmax, level + 1, max_level, eps))
                {
                    tmax = info->t;
                    bRet = true;
                }
                if (test_bezier_clip_v<Sz>(info, PR, RR, NR, nOrder, vm, v1, tmin, tmax, level + 1, max_level, eps))
                {
                    tmax = info->t;
                    bRet = true;
                }
            }
            else
            {
                if (test_bezier_clip_v<Sz>(info, PR, RR, NR, nOrder, vm, v1, tmin, tmax, level + 1, max_level, eps))
                {
                    tmax = info->t;
                    bRet = true;
                }
                if (test_bezier_clip_v<Sz>(info, PL, RL, NL, nOrder, v0, vm, tmin, tmax, level + 1, max_level, eps))
                {
                    tmax = info->t;
                    bRet = true;
                }
            }
            return bRet;
        }
        else
        {
            tt0 = std::max<real>(0.0, tt0 - UVEPS);
            tt1 = std::min<real>(tt1 + UVEPS, 1.0);
            real tv0 = lerp(v0, v1, tt0);
            real tv1 = lerp(v0, v1, tt1);
            vector3 tP[Sz];
            real tR[Sz];
            vector3 tN[Sz];
            cropBezier<vector3, Sz>(tP, P, nOrder, tt0, tt1);
            cropBezier<real, Sz>(tR, R, nOrder, tt0, tt1);
            cropBezier<vector3, Sz>(tN, N, nOrder, tt0, tt1);
            for (int i = 0; i < nOrder; i++)
            {
                tN[i] = normalize(tN[i]);
            }
            return test_bezier_clip_v<Sz>(info, tP, tR, tN, nOrder, tv0, tv1, tmin, tmax, level + 1, max_level, eps);
        }
    }

    template <int Sz>
    static inline bool intersectSegment(isect_info* info, const vector3 P[], const real R[], const vector3 N[], int nOrder, real v0, real v1, real tmin, real tmax)
    {
        return test_bezier_clip_v<Sz>(info, P, R, N, nOrder, v0, v1, tmin, tmax, 0, GetMaxLevel(), EPS);
    }

    template <int Sz>
    static inline bool test_(const vector3 P[], const real R[], const vector3 N[], int nOrder, real v0, real v1, const ray& r, real tmin, real tmax)
    {
        vector3 Q[Sz];
        cropBezier<vector3, Sz>(Q, P, nOrder, v0, v1);

        real S[Sz];
        cropBezier<real, Sz>(S, R, nOrder, v0, v1);

        vector3 G[Sz];
        cropBezier<vector3, Sz>(G, N, nOrder, v0, v1);
        for (int i = 0; i < nOrder; i++)
        {
            G[i] = normalize(G[i]);
        }

        vector3 min, max;
        get_minmax(min, max, Q, nOrder);
        real r1 = std::max<real>(S[0], S[nOrder - 1]) + EPS;
        min -= vector3(r1, r1, r1);
        max += vector3(r1, r1, r1);

        range_AABB rng;
        if (test_AABB(&rng, min, max, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);

            vector3 org = r.origin();
            vector3 dir = r.direction();
            matrix3 mat;
            getZAlign(mat, dir);
            vector3 G[Sz];
            for (int i = 0; i < nOrder; i++)
            {
                Q[i] = mat_mul(mat, Q[i] - org);
                G[i] = mat_mul(mat, G[i]); //mul_normal(imat, N[i]);
            }

            isect_info inf;
            return intersectSegment<Sz>(&inf, &Q[0], &S[0], &G[0], nOrder, v0, v1, tmin, tmax);
        }
        return false;
    }

    template <int Sz>
    static bool test_(const vector3 P[], const real R[], const vector3 N[], int nOrder, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax)
    {
        vector3 Q[Sz];
        cropBezier<vector3, Sz>(Q, P, nOrder, v0, v1);

        real S[Sz];
        cropBezier<real, Sz>(S, R, nOrder, v0, v1);

        vector3 G[Sz];
        cropBezier<vector3, Sz>(G, N, nOrder, v0, v1);
        for (int i = 0; i < nOrder; i++)
        {
            G[i] = normalize(G[i]);
        }

        vector3 min, max;
        get_minmax(min, max, Q, nOrder);
        real r1 = std::max<real>(S[0], S[nOrder - 1]) + EPS;
        min -= vector3(r1, r1, r1);
        max += vector3(r1, r1, r1);

        range_AABB rng;
        if (test_AABB(&rng, min, max, r, tmin, tmax))
        {
            tmin = std::max<real>(tmin, rng.tmin);
            tmax = std::min<real>(tmax, rng.tmax);

            vector3 org = r.origin();
            vector3 dir = r.direction();
            matrix3 mat;
            getZAlign(mat, dir);
            for (int i = 0; i < nOrder; i++)
            {
                Q[i] = mat_mul(mat, Q[i] - org);
                G[i] = mat_mul(mat, G[i]); //mul_normal(imat, N[i]);
            }

            isect_info inf;
            if (intersectSegment<Sz>(&inf, &Q[0], &S[0], &G[0], nOrder, v0, v1, tmin, tmax))
            {
                real t = inf.t;
                real u = inf.u;
                real v = inf.v;
                info->t = t;
                info->u = u;
                info->rate = v;

                return true;
            }
        }

        return false;
    }

    template <int Sz>
    static void finalize_(const vector3 P[], const real R[], const vector3 N[], int nOrder, curve_test_info* info, const ray& r, real tmin, real tmax)
    {
        int sz = info->p_strand->segments();
        int nseg = info->nseg;

        real v = info->rate;

        vector3 p = r.origin() + tmax * r.direction();
        //vector3 pp = evaluateBezier(P, nOrder, v);
        vector3 tt = normalize(evaluateBezierTangent(P, nOrder, v));
        vector3 nn = normalize(evaluateBezier(N, nOrder, v));

        real radius = evaluateBezier(R, nOrder, v);

        vector3 V = tt;
        vector3 U = normalize(cross(tt, nn));
        vector3 N2 = normalize(cross(U, V));

        //real u = 1.0 - fabs(dot(U, r.direction()));
        real u = info->u;

        info->position = p;
        info->u = u;
        info->v = (nseg + v) / sz;

        info->normal = N2;
        info->geometric = N2;

        info->tangent = U;
        info->binormal = V;
        info->radius = radius;
    }

    //----------------------------------------------------------------------------------------------------------------

    bool normal_ribbon_bezier_curve_tester::test(const vector3 P[], const real R[], const vector3 N[], int nOrder, real v0, real v1, const ray& r, real tmin, real tmax)
    {
        if (nOrder <= 4) return test_<4>(P, R, N, nOrder, v0, v1, r, tmin, tmax);
        if (nOrder <= 8) return test_<8>(P, R, N, nOrder, v0, v1, r, tmin, tmax);
        if (nOrder <= 16) return test_<16>(P, R, N, nOrder, v0, v1, r, tmin, tmax);
        if (nOrder <= 32) return test_<32>(P, R, N, nOrder, v0, v1, r, tmin, tmax);

        return false;
    }

    bool normal_ribbon_bezier_curve_tester::test(const vector3 P[], const real R[], const vector3 N[], int nOrder, real v0, real v1, curve_test_info* info, const ray& r, real tmin, real tmax)
    {
        if (nOrder <= 4) return test_<4>(P, R, N, nOrder, v0, v1, info, r, tmin, tmax);
        if (nOrder <= 8) return test_<8>(P, R, N, nOrder, v0, v1, info, r, tmin, tmax);
        if (nOrder <= 16) return test_<16>(P, R, N, nOrder, v0, v1, info, r, tmin, tmax);
        if (nOrder <= 32) return test_<32>(P, R, N, nOrder, v0, v1, info, r, tmin, tmax);

        return false;
    }

    void normal_ribbon_bezier_curve_tester::finalize(const vector3 P[], const real R[], const vector3 N[], int nOrder, curve_test_info* info, const ray& r, real tmin, real tmax)
    {
        if (nOrder <= 4)
        {
            finalize_<4>(P, R, N, nOrder, info, r, tmin, tmax);
            return;
        }
        if (nOrder <= 8)
        {
            finalize_<8>(P, R, N, nOrder, info, r, tmin, tmax);
            return;
        }
        if (nOrder <= 16)
        {
            finalize_<16>(P, R, N, nOrder, info, r, tmin, tmax);
            return;
        }
        if (nOrder <= 32)
        {
            finalize_<32>(P, R, N, nOrder, info, r, tmin, tmax);
            return;
        }
    }

    template <int Sz>
    static vector3 GetMin_(const vector3 P[], const real R[], const vector3 N[], int nOrder)
    {
        vector3 B[Sz * 2];
        if (make_band(B, B + nOrder, P, R, N, nOrder))
        {
            vector3 p = B[0];
            for (int i = 1; i < nOrder * 2; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    p[j] = std::min(p[j], B[i][j]);
                }
            }
            return p - vector3(EPS, EPS, EPS);
        }
        else
        {
            real r = R[0];
            vector3 p = P[0];
            for (int i = 1; i < nOrder; i++)
            {
                r = std::max<real>(r, R[i]);
                for (int j = 0; j < 3; j++)
                {
                    p[j] = std::min(p[j], P[i][j]);
                }
            }
            p -= vector3(r, r, r);
            return p - vector3(EPS, EPS, EPS);
        }
    }

    template <int Sz>
    static vector3 GetMax_(const vector3 P[], const real R[], const vector3 N[], int nOrder)
    {
        vector3 B[Sz * 2];
        if (make_band(B, B + nOrder, P, R, N, nOrder))
        {
            vector3 p = B[0];
            for (int i = 1; i < nOrder * 2; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    p[j] = std::max(p[j], B[i][j]);
                }
            }
            return p + vector3(EPS, EPS, EPS);
        }
        else
        {
            real r = R[0];
            vector3 p = P[0];
            for (int i = 1; i < nOrder; i++)
            {
                r = std::max<real>(r, R[i]);
                for (int j = 0; j < 3; j++)
                {
                    p[j] = std::max(p[j], P[i][j]);
                }
            }
            p += vector3(r, r, r);
            return p + vector3(EPS, EPS, EPS);
        }
    }

    vector3 normal_ribbon_bezier_curve_tester::min(const vector3 P[], const real R[], const vector3 N[], int nOrder)
    {
        if (nOrder <= 4) return GetMin_<4>(P, R, N, nOrder);
        if (nOrder <= 8) return GetMin_<8>(P, R, N, nOrder);
        if (nOrder <= 16) return GetMin_<16>(P, R, N, nOrder);
        return GetMin_<32>(P, R, N, nOrder);
    }

    vector3 normal_ribbon_bezier_curve_tester::max(const vector3 P[], const real R[], const vector3 N[], int nOrder)
    {
        if (nOrder <= 4) return GetMax_<4>(P, R, N, nOrder);
        if (nOrder <= 8) return GetMax_<8>(P, R, N, nOrder);
        if (nOrder <= 16) return GetMax_<16>(P, R, N, nOrder);
        return GetMax_<32>(P, R, N, nOrder);
    }
}
