
#include "phantom_intersection.h"

namespace kaze
{

    phantom_intersection::phantom_intersection(const std::shared_ptr<intersection>& inter, int type)
        : inter_(inter), type_(type)
    {
    }

    phantom_intersection::~phantom_intersection() {}

    bool phantom_intersection::test(const ray& r, real dist) const
    {
        bool bRet = false;
        switch (type_)
        {
        case PHANTOM_NOEFFECT:
            bRet = inter_->test(r, dist);
            break;
        case PHANTOM_INVISIBLE:
            bRet = inter_->test(r, dist);
            break;
        case PHANTOM_NOSHADOW:
            bRet = false;
            break;
        }
        return bRet;
    }

    bool phantom_intersection::test(test_info* info, const ray& r, real dist) const
    {
        bool bRet = false;
        switch (type_)
        {
        case PHANTOM_NOEFFECT:
            bRet = inter_->test(info, r, dist);
            break;
        case PHANTOM_INVISIBLE:
            bRet = false;
            break;
        case PHANTOM_NOSHADOW:
            bRet = inter_->test(info, r, dist);
            break;
        }
        return bRet;
    }

    void phantom_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        //NO IMPLEMENT!
        assert(0);
    }

    void phantom_intersection::set_type(int type)
    {
        type_ = type % PHANTOM_END;
    }

    vector3 phantom_intersection::min() const { return inter_->min(); }

    vector3 phantom_intersection::max() const { return inter_->max(); }
}
