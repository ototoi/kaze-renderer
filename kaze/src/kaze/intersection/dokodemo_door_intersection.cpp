#include "dokodemo_door_intersection.h"

#include "test_info.h"

namespace kaze
{

    class dokodemo_door_intersection_imp : public bounded_intersection
    {
    public:
        dokodemo_door_intersection_imp(intersection* inter, const vector3& pos1, const vector3& u1, const vector3& v1, const vector3& pos2, const vector3& u2, const vector3& v2);
        dokodemo_door_intersection_imp(const std::shared_ptr<intersection>& inter, const vector3& pos1, const vector3& u1, const vector3& v1, const vector3& pos2, const vector3& u2, const vector3& v2);

        void initialize(const vector3& pos1, const vector3& u1, const vector3& v1, const vector3& pos2, const vector3& u2, const vector3& v2);

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    protected:
        bool test_kocchi(const ray& r, real dist) const;
        bool test_acchi(test_info* info, const ray& r, real dist) const;
        bool test_acchi(const ray& r, real dist) const;

        intersection* get() { return inter_.get(); }
        const intersection* get() const { return inter_.get(); }

    private:
        matrix4 mat_;
        std::shared_ptr<intersection> inter_;
        vector3 min_;
        vector3 max_;

        vector3 pos_;
        vector3 u_;
        vector3 v_;
    };

    dokodemo_door_intersection_imp::dokodemo_door_intersection_imp(intersection* inter, const vector3& pos1, const vector3& u1, const vector3& v1, const vector3& pos2, const vector3& u2, const vector3& v2)
        : inter_(inter)
    {
        initialize(pos1, u1, v1, pos2, u2, v2);
    }

    dokodemo_door_intersection_imp::dokodemo_door_intersection_imp(const std::shared_ptr<intersection>& inter, const vector3& pos1, const vector3& u1, const vector3& v1, const vector3& pos2, const vector3& u2, const vector3& v2)
        : inter_(inter)
    {
        initialize(pos1, u1, v1, pos2, u2, v2);
    }

    void dokodemo_door_intersection_imp::initialize(const vector3& pos1, const vector3& u1, const vector3& v1, const vector3& pos2, const vector3& u2, const vector3& v2)
    {
        static const real FAR = values::far();

        vector3 w1 = cross(u1, v1);
        vector3 w2 = cross(u2, v2);

        matrix4 T1 = mat4_gen::translation(-pos1[0], -pos1[1], -pos1[2]);
        matrix4 R1 = matrix4(
            u1[0], v1[0], w1[0], 0,
            u1[1], v1[1], w1[1], 0,
            u1[2], v1[2], w1[2], 0,
            0, 0, 0, 1);
        matrix4 M1 = R1 * T1;

        matrix4 R2 = matrix4(
            u1[0], u1[1], u1[2], 0,
            v1[0], v1[1], v1[2], 0,
            w1[0], w1[1], w1[2], 0,
            0, 0, 0, 1);
        matrix4 T2 = mat4_gen::translation(+pos2[0], +pos2[1], +pos2[2]);
        matrix4 M2 = T2 * R2;

        matrix4 OUT_M = M2 * M1;

        vector3 min(FAR, FAR, FAR);
        vector3 max(-FAR, -FAR, -FAR);
        vector3 points[] = {pos1, pos1 + u1, pos1 + v1, pos1 + u1 + v1};
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (min[j] > points[i][j]) min[j] = points[i][j];
                if (max[j] < points[i][j]) max[j] = points[i][j];
            }
        }

        this->mat_ = OUT_M;

        this->min_ = min;
        this->max_ = max;

        this->pos_ = pos1;
        this->u_ = u1;
        this->v_ = v1;
    }

    bool dokodemo_door_intersection_imp::test_kocchi(const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1000;
        real u, v, t;

        const vector3& org = r.origin();
        const vector3& dir = r.direction();
        const vector3& p0 = pos_;

        //-e1 = p0-p1
        const vector3& e1 = u_; //vA

        //-e2 = p0-p2
        const vector3& e2 = v_; //vB

        //dir = GHI

        vector3 bDir(cross(e2, dir));

        real iM = dot(e1, bDir);

        if (iM < -EPSILON)
        {
            //p0-org
            vector3 vOrg(p0 - org); //JKL

            u = dot(vOrg, bDir);
            if (u > 0 || iM > u) return false;

            vector3 vE(cross(e1, vOrg));

            v = dot(dir, vE);
            if (v > 0 || 2 * iM > u + v) return false;

            t = -dot(e2, vE);
            if (t >= 0 || dist * iM >= t) return false;
        }
        else
        {
            return false;
        }

        return true;
    }

    //---------------------------------------------------------------------------------------
    inline static vector4 v3_v4(const vector3& v, real w) { return vector4(v[0], v[1], v[2], w); }
    inline static vector3 v4_v3p(const vector4& v)
    {
        real iw = real(1) / v[3];
        return vector3(v[0] * iw, v[1] * iw, v[2] * iw);
    }
    inline static vector3 v4_v3(const vector4& v) { return vector3(v[0], v[1], v[2]); }

    inline static vector3 mul_m4_v3(const matrix4& m, const vector3& v)
    {
        return vector3(
            m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2],
            m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2],
            m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2]);
    }

    inline static vector3 mul_m4_v3_normal(const matrix4& m, const vector3& v)
    {
        return vector3(
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
    }

    static const char MODIFIER_ID_FORMAT_STRING[] = "KZDDIIID";
    static const int KAZE_MODIFIER_ID_SIZE = sizeof(MODIFIER_ID_FORMAT_STRING);

    namespace
    {
        struct dokodemo_door_freearea_struct
        {
            void* address;
            char id_string[KAZE_MODIFIER_ID_SIZE];
        };
    }

    inline static void SET_MODIFIER_ID(test_info* info, const void* p_mod)
    {
        using namespace std;
        dokodemo_door_freearea_struct* fsp = reinterpret_cast<dokodemo_door_freearea_struct*>(info->freearea);
        fsp->address = (void*)p_mod;
        memcpy(fsp->id_string, MODIFIER_ID_FORMAT_STRING, sizeof(MODIFIER_ID_FORMAT_STRING));
    }

    inline static bool CHECK_MODIFIER_ID(test_info* info, const void* p_mod)
    {
        using namespace std;
        const dokodemo_door_freearea_struct* fsp = reinterpret_cast<const dokodemo_door_freearea_struct*>(info->freearea);
        return ((void*)p_mod == fsp->address) && (memcmp(fsp->id_string, MODIFIER_ID_FORMAT_STRING, sizeof(MODIFIER_ID_FORMAT_STRING)) == 0);
    }

    //---------------------------------------------------------------------------------------

    bool dokodemo_door_intersection_imp::test_acchi(test_info* info, const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1000;

        vector3 morg = v4_v3p(mat_ * v3_v4(r.origin(), 1));
        vector3 mdir = mul_m4_v3(mat_, r.direction());

        real d = mdir.length();
        if (d < EPSILON) return false;

        real id = real(1) / d;
        mdir *= id;

        ray mray(morg, mdir);

        if (get()->test(info, mray, dist))
        {
            if (info->p_intersection == get())
            {
                info->distance = (id * info->distance);
                info->p_intersection = this;
            }
            else
            {
                real zdist = id * info->distance;
                info->distance = (zdist);
                info->p_intersection->finalize(info, mray, zdist);

                SET_MODIFIER_ID(info, this); //ID
                info->p_intersection = this;
            }
            return true;
        }
        return false;
    }

    bool dokodemo_door_intersection_imp::test_acchi(const ray& r, real dist) const
    {
        static const real EPSILON = values::epsilon() * 1000;

        vector3 morg = v4_v3p(mat_ * v3_v4(r.origin(), 1));
        vector3 mdir = mul_m4_v3(mat_, r.direction());

        real d = mdir.length();
        if (d < EPSILON) return false;

        real id = real(1) / d;
        mdir *= id;

        return get()->test(ray(morg, mdir), dist);
    }

    bool dokodemo_door_intersection_imp::test(const ray& r, real dist) const
    {
        static const real FAR = values::far();
        if (this->test_kocchi(r, dist))
        {
            if (this->test_acchi(r, FAR)) return true;
        }
        return false;
    }

    bool dokodemo_door_intersection_imp::test(test_info* info, const ray& r, real dist) const
    {
        static const real FAR = values::far();
        if (this->test_kocchi(r, dist))
        {
            if (this->test_acchi(info, r, FAR)) return true;
        }
        return false;
    }

    void dokodemo_door_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
    {
        if (!CHECK_MODIFIER_ID(info, this))
        {
            vector3 morg = v4_v3p(mat_ * v3_v4(r.origin(), 1));
            vector3 mdir = mul_m4_v3(mat_, r.direction());
            real d = mdir.length();
            //if(d < EPSILON)return false;
            real mdist = d * info->distance;
            //if(mdist == values::infinity())mdist = values::far();

            real id = real(1) / d;
            mdir *= id;

            get()->finalize(info, ray(morg, mdir), mdist);
        }

        info->position = (r.origin() + info->distance * r.direction());
        info->normal = (normalize(mul_m4_v3_normal(mat_, info->normal)));
    }

    vector3 dokodemo_door_intersection_imp::min() const
    {
        return min_;
    }

    vector3 dokodemo_door_intersection_imp::max() const
    {
        return max_;
    }

    dokodemo_door_intersection::dokodemo_door_intersection(
        intersection* inter,
        const vector3& pos1, const vector3& u1, const vector3& v1,
        const vector3& pos2, const vector3& u2, const vector3& v2)
    {
        imp_ = new dokodemo_door_intersection_imp(inter, pos1, u1, v1, pos2, u2, v2);
    }

    dokodemo_door_intersection::dokodemo_door_intersection(
        const std::shared_ptr<intersection>& inter,
        const vector3& pos1, const vector3& u1, const vector3& v1,
        const vector3& pos2, const vector3& u2, const vector3& v2)
    {
        imp_ = new dokodemo_door_intersection_imp(inter, pos1, u1, v1, pos2, u2, v2);
    }

    dokodemo_door_intersection::~dokodemo_door_intersection()
    {
        delete imp_;
    }

    bool dokodemo_door_intersection::test(const ray& r, real dist) const
    {
        return imp_->test(r, dist);
    }
    bool dokodemo_door_intersection::test(test_info* info, const ray& r, real dist) const
    {
        if (imp_->test(info, r, dist))
        {
            info->p_intersection = this;
            return true;
        }
        return false;
    }
    void dokodemo_door_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        info->p_intersection = this;
        imp_->finalize(info, r, dist);
    }

    vector3 dokodemo_door_intersection::min() const
    {
        return imp_->min();
    }
    vector3 dokodemo_door_intersection::max() const
    {
        return imp_->max();
    }
}