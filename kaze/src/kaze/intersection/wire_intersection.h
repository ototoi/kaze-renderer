#ifndef KAZE_WIRE_INTERSECTION_H
#define KAZE_WIRE_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class wire_intersection_imp;
    class wire_intersection : public bounded_intersection
    {
    public:
        wire_intersection(real r = 1);
        wire_intersection(const vector3& min, const vector3& max, real r = 1);
        ~wire_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;

        vector3 min() const;
        vector3 max() const;

    public:
        void add(const vector3& p0, const vector3& p1);

    private:
        wire_intersection_imp* imp_;
    };
}

#endif