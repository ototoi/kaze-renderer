#ifndef KAZE_DISK_LIGHT_SAMPLER_H
#define KAZE_DISK_LIGHT_SAMPLER_H

#include "light_sampler.h"

namespace kaze
{
    class disk_light_sampler_imp;
    class disk_light_sampler : public light_sampler
    {
    public:
        disk_light_sampler(const vector3& p0, const vector3& x, const vector3& y, real umin = 0, real umax = 1, int num_samples = 1);
        ~disk_light_sampler();
        virtual void sample(light_sample* s, const sufflight& sl) const;
        virtual int sample(std::vector<vector3>& samples, const sufflight& sl) const;

    private:
        disk_light_sampler_imp* imp_;
    };
}

#endif
