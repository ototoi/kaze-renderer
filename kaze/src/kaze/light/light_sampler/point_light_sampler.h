#ifndef KAZE_POINT_LIGHT_SAMPLER_H
#define KAZE_POINT_LIGHT_SAMPLER_H

#include "light_sampler.h"

namespace kaze
{

    class point_light_sampler : public light_sampler
    {
    public:
        point_light_sampler(const vector3& pos);
        point_light_sampler(const matrix4& mat);
        ~point_light_sampler();
        int sample(std::vector<vector3>& samples, const sufflight& sl) const;

    private:
        vector3 pos_;
    };
}

#endif