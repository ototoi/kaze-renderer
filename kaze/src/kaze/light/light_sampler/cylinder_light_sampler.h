#ifndef KAZE_CYLINDER_LIGHT_SAMPLER_H
#define KAZE_CYLINDER_LIGHT_SAMPLER_H

#include "light_sampler.h"

namespace kaze
{
    class cylinder_light_sampler_imp;
    class cylinder_light_sampler : public light_sampler
    {
    public:
        cylinder_light_sampler(const vector3& p0, const vector3& x, const vector3& y, const vector3& z, real umin, real umax, int nsamples);
        ~cylinder_light_sampler();
        int sample(std::vector<vector3>& samples, const sufflight& sl) const;

    private:
        cylinder_light_sampler_imp* imp_;
    };
}

#endif