#define _USE_MATH_DEFINES
#include <math.h>

#include <mutex>

#include "disk_light_sampler.h"
#include "QMC.h"
#include "ray.h"

namespace kaze
{
namespace 
{
    class d_sampler
    {
    public:
        d_sampler()
        {
            k_ = 0;
        }
        vector2 sample() const
        {
#ifdef _WIN32
            LONG s = ::InterlockedExchangeAdd(&k_, (LONG)1);
            float u = ri_vdC(s, 12345);
            float v = ri_S(s, 12345);
#else
            float u = ri_vdC(k_, 12345);
            float v = ri_S(k_, 12345);
            {
                std::lock_guard<std::mutex> lck(mtx_);
                k_++;
            }
#endif
            return vector2(u, v);
        }

    protected:
#ifdef _WIN32
        mutable LONG k_;
#else
        mutable int k_;
        mutable std::mutex mtx_;
#endif
    };
}

    class disk_light_sampler_imp
    {
    public:
        disk_light_sampler_imp(const vector3& p0, const vector3& x, const vector3& y, real umin, real umax, int num_samples);
        ~disk_light_sampler_imp();
        void sample(light_sample* s, const sufflight& sl) const;
        int sample(std::vector<vector3>& samples, const sufflight& sl) const;

    protected:
        vector3 sample(const sufflight& sl) const;
        vector2 sample_uv(const sufflight& sl) const;
        vector2 round_uv(const vector2& uv) const;

    private:
        vector3 p0_;
        vector3 x_;
        vector3 y_;
        real umin_;
        real umax_;
        int num_;
        d_sampler s;
    };

    disk_light_sampler_imp::disk_light_sampler_imp(const vector3& p0, const vector3& x, const vector3& y, real umin, real umax, int num_samples)
        : p0_(p0), x_(x), y_(y), umin_(umin), umax_(umax), num_(num_samples)
    {
        ; //
    }

    disk_light_sampler_imp::~disk_light_sampler_imp()
    {
        ; //
    }

    void disk_light_sampler_imp::sample(light_sample* s, const sufflight& sl) const
    {
        vector2 uv = sample_uv(sl);
        vector3 p = p0_ + uv[0] * x_ + uv[1] * y_;
        s->position = p;
    }

    vector2 disk_light_sampler_imp::round_uv(const vector2& uv) const
    {
        real u = uv[0];
        real v = uv[1];
        real angle = atan2(v, u);
        real a = (angle / M_PI + 1) / 2;
        real l = sqrt(u * u + v * v);

        real a2 = (umax_ - umin_) * a + umin_;
        real angle2 = a2 * 2 * M_PI;

        real u2 = cos(angle2);
        real v2 = sin(angle2);

        return vector2(u2 * l, v2 * l);
    }

    vector2 disk_light_sampler_imp::sample_uv(const sufflight& sl) const
    {
        vector2 p = s.sample();
        real u = 2 * p[0] - 1;
        real v = 2 * p[1] - 1;
        real l = u * u + v * v;
        while (1 < l)
        {
            p = s.sample();
            u = 2 * p[0] - 1;
            v = 2 * p[1] - 1;
            l = u * u + v * v;
        }
        return vector2(u, v);
    }

    vector3 disk_light_sampler_imp::sample(const sufflight& sl) const
    {
        vector2 uv = sample_uv(sl);
        return p0_ + uv[0] * x_ + uv[1] * y_;
    }

    int disk_light_sampler_imp::sample(std::vector<vector3>& samples, const sufflight& sl) const
    {
        if (umin_ != umax_)
        {
            int n = num_;
            if (umin_ == 0 && umax_ == 1)
            {
                samples.reserve(samples.size() + n);
                for (int i = 0; i < n; i++)
                {
                    samples.push_back(this->sample(sl));
                }
                return n;
            }
            else
            {
                for (int i = 0; i < n; i++)
                {
                    vector2 uv = round_uv(sample_uv(sl));
                    samples.push_back(p0_ + uv[0] * x_ + uv[1] * y_);
                }
                return n;
            }
        }
        else
        {
            return 0;
        }
    }

    //--------------------------------------------------------------------------------------------------------------------

    disk_light_sampler::disk_light_sampler(const vector3& p0, const vector3& x, const vector3& y, real umin, real umax, int num_samples)
    {
        imp_ = new disk_light_sampler_imp(p0, x, y, umin, umax, num_samples);
    }

    disk_light_sampler::~disk_light_sampler()
    {
        delete imp_;
    }

    void disk_light_sampler::sample(light_sample* s, const sufflight& sl) const
    {
        imp_->sample(s, sl);
    }

    int disk_light_sampler::sample(std::vector<vector3>& samples, const sufflight& sl) const
    {
        return imp_->sample(samples, sl);
    }
}