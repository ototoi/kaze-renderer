#define _USE_MATH_DEFINES
#include <math.h>

#include "cylinder_light_sampler.h"

#include <vector>

namespace kaze
{
    namespace
    {
        class sampler_x
        {
        public:
            vector2 sample(const sufflight& sl) const
            {
                return vector2(0, 0);
            };
        };

        class grid_sampler
        {
        public:
        private:
            int w_;
            int h_;
            std::vector<sampler_x*> samplers_;
        };

        class sampler_patch
        {
        public:
            virtual bool sample(vector3& pos, const vector2& uv) const = 0;
        };
    }

    class cylinder_light_sampler_imp
    {
    public:
        cylinder_light_sampler_imp(const vector3& p0, const vector3& x, const vector3& y, const vector3& z, real umin, real umax, int nsamples);
        ~cylinder_light_sampler_imp();
        int sample(std::vector<vector3>& samples, const sufflight& sl) const;
    };

    //------------------------------------------------------------------------------------------------------------------------------------

    cylinder_light_sampler_imp::cylinder_light_sampler_imp(const vector3& p0, const vector3& x, const vector3& y, const vector3& z, real umin, real umax, int nsamples)
    {
        real lx = length(x);
        real ly = length(y);
        real lz = length(z);

        real radius = sqrt(lx * lx + ly * ly);
        real circle = 2 * M_PI * radius;
    }

    cylinder_light_sampler_imp::~cylinder_light_sampler_imp()
    {
    }

    int cylinder_light_sampler_imp::sample(std::vector<vector3>& samples, const sufflight& sl) const
    {
        return 0;
    }

    //------------------------------------------------------------------------------------------------------------------------------------

    cylinder_light_sampler::cylinder_light_sampler(const vector3& p0, const vector3& x, const vector3& y, const vector3& z, real umin, real umax, int nsamples)
    {
        imp_ = new cylinder_light_sampler_imp(p0, x, y, z, umin, umax, nsamples);
    }

    cylinder_light_sampler::~cylinder_light_sampler()
    {
        delete imp_;
    }

    int cylinder_light_sampler::sample(std::vector<vector3>& samples, const sufflight& sl) const
    {
        return imp_->sample(samples, sl);
    }
}