#ifndef KAZE_SPHERE_LIGHT_SAMPLER_H
#define KAZE_SPHERE_LIGHT_SAMPLER_H

#include "light_sampler.h"

namespace kaze
{
    class sphere_light_sampler_imp;
    class sphere_light_sampler : public light_sampler
    {
    public:
        sphere_light_sampler(const vector3& o, real r, real umin, real umax, real vmin, real vmax, const matrix4& mat, int num_samples = 1);
        ~sphere_light_sampler();
        int sample(std::vector<vector3>& samples, const sufflight& sl) const;

    private:
        sphere_light_sampler_imp* imp_;
    };
}

#endif
