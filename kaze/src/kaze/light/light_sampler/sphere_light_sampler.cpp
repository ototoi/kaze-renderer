#define _USE_MATH_DEFINES
#include <math.h>
#include <mutex>

#include "sphere_light_sampler.h"
#include "QMC.h"
#include "ray.h"

namespace kaze
{
    class d_sampler
    {
    public:
        d_sampler()
        {
            k_ = 0;
        }
        vector2 sample() const
        {
#ifdef _WIN32
            LONG s = ::InterlockedExchangeAdd(&k_, (LONG)1);
            float u = ri_vdC(s, 12345);
            float v = ri_S(s, 12345);
#else
            float u = ri_vdC(k_, 12345);
            float v = ri_S(k_, 12345);
            {
                std::lock_guard<std::mutex> lck(mtx_);
                k_++;
            }
#endif
            return vector2(u, v);
        }

    protected:
#ifdef _WIN32
        mutable LONG k_;
#else
        mutable int k_;
        mutable std::mutex mtx_;
#endif
    };

    class sphere_light_sampler_imp
    {
    public:
        sphere_light_sampler_imp(const vector3& o, real r, real umin, real umax, real vmin, real vmax, const matrix4& mat, int num_samples);
        ~sphere_light_sampler_imp();
        int sample(std::vector<vector3>& samples, const sufflight& sl) const;

    protected:
        vector3 sample(const sufflight& sl) const;

    private:
        vector3 o_;
        real r_;
        real umin_;
        real umax_;
        real vmin_;
        real vmax_;
        matrix4 mat_;
        int num_;
        d_sampler s;
    };

    sphere_light_sampler_imp::sphere_light_sampler_imp(const vector3& o, real r, real umin, real umax, real vmin, real vmax, const matrix4& mat, int num_samples)
        : o_(o), r_(r), umin_(umin), umax_(umax), vmin_(vmin), vmax_(vmax), mat_(mat), num_(num_samples)
    {
        ; //
    }

    sphere_light_sampler_imp::~sphere_light_sampler_imp()
    {
        ; //
    }

    vector3 sphere_light_sampler_imp::sample(const sufflight& sl) const
    {
        return mat_ * vector3(0, 0, 0);
    }

    int sphere_light_sampler_imp::sample(std::vector<vector3>& samples, const sufflight& sl) const
    {
        if (umin_ != umax_)
        {
            int n = num_;
            if (umin_ == 0 && umax_ == 1)
            {
                samples.reserve(samples.size() + n);
                for (int i = 0; i < n; i++)
                {
                    samples.push_back(this->sample(sl));
                }
                return n;
            }
            else
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }

    //--------------------------------------------------------------------------------------------------------------------

    sphere_light_sampler::sphere_light_sampler(const vector3& o, real r, real umin, real umax, real vmin, real vmax, const matrix4& mat, int num_samples)
    {
        imp_ = new sphere_light_sampler_imp(o, r, umin, umax, vmin, vmax, mat, num_samples);
    }

    sphere_light_sampler::~sphere_light_sampler()
    {
        delete imp_;
    }

    int sphere_light_sampler::sample(std::vector<vector3>& samples, const sufflight& sl) const
    {
        return imp_->sample(samples, sl);
    }
}