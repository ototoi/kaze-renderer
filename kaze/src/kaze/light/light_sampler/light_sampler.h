#ifndef KAZE_LIGHT_SAMPLER_H
#define KAZE_LIGHT_SAMPLER_H

#include "types.h"
#include "light.h"
#include "sufflight.h"

namespace kaze
{
    struct light_sample
    {
        real intensity;
        real pdf;
        vector3 coord;
        vector3 position;
        vector3 normal;
    };

    class light_sampler
    {
    public:
        virtual ~light_sampler() {}
        virtual void sample(light_sample* s, const sufflight& sl) const {}
        virtual void sample(std::vector<light_sample>& samples, const sufflight& sl, int numsamples) const
        {
            for (int i = 0; i < numsamples; i++)
            {
                light_sample smp;
                this->sample(&smp, sl);
                samples.push_back(smp);
            }
        }
        virtual int sample(std::vector<vector3>& samples, const sufflight& sl) const = 0;
    };
}

#endif