#include "point_light_sampler.h"

namespace kaze
{

    point_light_sampler::point_light_sampler(const vector3& pos)
        : pos_(pos)
    {
    }

    point_light_sampler::point_light_sampler(const matrix4& mat)
    {
        pos_ = mat * vector3(0, 0, 0);
    }

    point_light_sampler::~point_light_sampler()
    {
    }

    int point_light_sampler::sample(std::vector<vector3>& samples, const sufflight& sl) const
    {
        samples.push_back(pos_);
        return 1;
    }
}