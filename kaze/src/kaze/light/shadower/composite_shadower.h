#ifndef KAZE_COMPOSITE_SHADOWER_H
#define KAZE_COMPOSITE_SHADOWER_H

#include "shadower.h"
#include "count_ptr.hpp"
#include <vector>

namespace kaze
{

    class composite_shadower : public shadower
    {
    public:
        void cast(aggregator<lightpath>* lps, const lightpath& lp) const;
        void cast(aggregator<lightpath>* lps, const enumerator<lightpath>& enlps) const;

    public:
        void add(const auto_count_ptr<shadower>& ap);

    protected:
        std::vector<std::shared_ptr<shadower> > mv_;
    };
}

#endif