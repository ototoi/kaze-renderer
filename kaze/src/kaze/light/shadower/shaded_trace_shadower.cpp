#include "shaded_trace_shadower.h"
#include "shader.h"
#include "test_info.h"

#include "logger.h"

namespace kaze
{

    namespace
    {
        inline void finalize(test_info* info, const ray& r)
        {
            static const real ZERO[3] = {0, 0, 0};

            if (info->p_intersection)
            {
                info->p_intersection->finalize(info, r, info->distance);
                if (memcmp(&(info->geometric), ZERO, sizeof(real) * 3) == 0)
                {
                    info->geometric = info->normal;
                }
                vector3 d = r.direction();
                vector3 g = info->geometric;
                if (dot(d, g) > 0) g = -g;
                info->geometric = g;
            }
        }
    }

    void shaded_trace_shadower::cast(aggregator<lightpath>* lps, const lightpath& lp) const
    {
        this->cast(lps, lp, 0);
    }

    void shaded_trace_shadower::cast(aggregator<lightpath>* lps, const lightpath& lp, int n) const
    {
        static const real IEPS = 0.01;
        vector3 vl = lp.to() - lp.from();
        real leng = vl.length();
        real leng_e = leng - epsilon_;

        if (leng_e > 0)
        {
            vl *= real(1) / leng;

            ray r(lp.from(), vl);

            test_info info;
            if (!this->test(&info, r, leng_e))
            { //no shadow
                lps->add(lp);
                return;
            }
            else
            {
                finalize(&info, r);

                const shader* shd = info.get_shader();
                if (shd)
                {
                    sufflight sl(
                        r.origin(), r.direction(),
                        info.get_position(),
                        info.get_geometric(),
                        info.get_normal(),
                        info.get_tangent(),
                        info.get_binormal(),
                        info.get_coord());

                    sl.set_color1(info.col1);
                    sl.set_color2(info.col2);
                    sl.set_color3(info.col3);

                    const shadow_shader* pShadow = shd->get_shadow_shader();
                    if (pShadow)
                    {
                        kaze::mediation air;
                        spectrum c = pShadow->shade(radiance(vl, lp.power()), sl, air);

                        if (c.intensity() >= IEPS)
                        {
                            lightpath lp2(info.get_position() + vl * epsilon_, lp.to(), c);
                            this->cast(lps, lp2, n + 1);
                        }
                    }
                }
            }
        }
    }
}
