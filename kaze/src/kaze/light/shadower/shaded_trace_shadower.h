#ifndef KAZE_SHADED_TRACE_SHADOWER_H
#define KAZE_SHADED_TRACE_SHADOWER_H

#include "trace_shadower.h"

namespace kaze
{

    class shaded_trace_shadower : public trace_shadower
    {
    public:
        shaded_trace_shadower(const auto_count_ptr<intersection>& inter, real epsilon = real(0.000001)) : trace_shadower(inter, epsilon) {}

        void cast(aggregator<lightpath>* lps, const lightpath& lp) const;

    protected:
        void cast(aggregator<lightpath>* lps, const lightpath& lp, int n) const;
    };
}

#endif