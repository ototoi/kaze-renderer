#include "sample_shadowmap_sampler.h"
#include "basic_collection.hpp"

namespace kaze
{

    sample_shadowmap_sampler::sample_shadowmap_sampler(const auto_count_ptr<sampler<vector2> >& smp, int n) : smp_(smp)
    {
        nSamples_ = n;
    }

    void sample_shadowmap_sampler::get(aggregator<vector2>* agr) const
    {
        for (size_t i = 0; i < nSamples_; i++)
        {
            vector2 v = smp_->get();
            double x = (2.0 * v[0] - 1.0);
            double y = (2.0 * v[1] - 1.0);
            double l = x * x + y + y;
            if (l <= 1.0)
            {
                agr->add(vector2(x, y));
            }
        }
    }
}