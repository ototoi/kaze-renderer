#ifndef KAZE_CHAINED_COMPOSITE_SHADOWER_H
#define KAZE_CHAINED_COMPOSITE_SHADOWER_H

#include "shadower.h"

namespace kaze
{

    class chained_shadower;
    class chained_composite_shadower : public shadower
    {
    public:
        chained_composite_shadower();
        ~chained_composite_shadower();
        void add(shadower* shd);
        void add(const std::shared_ptr<shadower>& shd);

    public:
        void cast(aggregator<lightpath>* lps, const lightpath& lp) const;

    private:
        chained_shadower* shd_;
    };
}

#endif