#include "shadow_bound_shadower.h"
#include "basic_collection.hpp"

namespace kaze
{

    shadow_bound_shadower::shadow_bound_shadower(const auto_count_ptr<shadow_bound>& bnd)
        : bnd_(bnd)
    {
    }

    void shadow_bound_shadower::cast(aggregator<lightpath>* lps, const lightpath& lp) const
    {
        static const real EPSILON = values::epsilon() * 1024;

        basic_collection<shadow_segment> segs;
        vector3 from = lp.from();
        vector3 to = lp.to();
        vector3 tf = to - from;
        to = from + tf * (1.0 - EPSILON);
        if (bnd_->cast(&segs, from, to))
        {
            color3 c = lp.power().to_rgb();
            size_t sz = segs.size();
            for (size_t i = 0; i < sz; i++)
            {
                c *= segs[i].opacity;
            }
            lps->add(lightpath(lp.from(), lp.to(), spectrum(c)));
        }
        else
        {
            lps->add(lp);
        }
    }
}
