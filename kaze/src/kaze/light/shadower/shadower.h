#ifndef KAZE_SHADOWER_H
#define KAZE_SHADOWER_H

#include "lightpath.h"
#include "aggregator.hpp"
#include "enumerator.hpp"

namespace kaze
{

    class shadower
    {
    public:
        virtual ~shadower() {}
        virtual void cast(aggregator<lightpath>* lps, const lightpath& lp) const = 0;
        virtual void cast(aggregator<lightpath>* lps, const enumerator<lightpath>& enlps) const;
    };
}

#endif