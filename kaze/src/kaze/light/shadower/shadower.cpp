#include "shadower.h"

namespace kaze
{
    void shadower::cast(aggregator<lightpath>* lps, const enumerator<lightpath>& enlps) const
    {
        size_t sz = enlps.size();
        for (size_t i = 0; i < sz; i++)
        {
            this->cast(lps, enlps[i]);
        }
    }
}