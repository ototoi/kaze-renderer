#include "shadowmap_shadower.h"
#include "sampler.hpp"
#include "basic_collection.hpp"

namespace kaze
{

    shadowmap_shadower::shadowmap_shadower(
        const auto_count_ptr<image<real> >& img,
        const auto_count_ptr<transformer>& tr,
        const auto_count_ptr<shadowmap_sampler>& smp,
        real radius) : img_(img), tfm_(tr), smp_(smp)
    {
        radius_ = radius;
        spn_ = 1.0 / std::min<int>(img_->get_width(), img_->get_height());
    }

    shadowmap_shadower::~shadowmap_shadower()
    {
        ;
    }

    void shadowmap_shadower::cast(aggregator<lightpath>* lps, const lightpath& lp) const
    {
        vector3 p = tfm_->transform_p(lp.to());
        int nPass = 0;
        int nTest = 0;
        test(nPass, nTest, p);
        if (nPass == 0)
        {
            //
        }
        else if (nPass == nTest)
        {
            lps->add(lp);
        }
        else
        {
            lps->add(lightpath(lp.from(), lp.to(), real(nPass) / nTest * lp.power()));
        }
    }

    //-----------------------------------------------------------------------------------------
    void shadowmap_shadower::test(int& nPass, int& nTest, const vector3& p) const
    {
        real rad = radius_;

        basic_collection<vector2> points;
        smp_->get(&points);

        int total = (int)points.size();
        int pass = 0;
        for (int j = 0; j < total; j++)
        {
            real xx = p[0] + rad * points[j][0];
            real yy = p[1] + rad * points[j][1];
            real zz = p[2];

            if (!this->test(xx, yy, zz))
            { //no hit
                pass++;
            }
        }
        nPass = pass;
        nTest = total;
    }

    bool shadowmap_shadower::test(real x, real y, real z) const
    {
        if (x < 0 || 1 <= x) return true;
        if (y < 0 || 1 <= y) return true;

        int ww = img_->get_width();
        int hh = img_->get_height();

        int ix = (int)(x * ww);
        int iy = (int)(y * hh);

        assert(ix < ww);
        assert(iy < hh);

        real zz = img_->get(ix, iy);
        real spn = spn_;

        if (zz + spn > z)
        { //pass
            return false;
        }
        else
        { //no pass
            return true;
        }
    }
}
