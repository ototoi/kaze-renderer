#ifndef KAZE_NULL_SHADOWER_H
#define KAZE_NULL_SHADOWER_H

#include "shadower.h"

namespace kaze
{

    class null_shadower : public shadower
    {
    public:
        virtual void cast(aggregator<lightpath>* lps, const lightpath& lp) const;
    };
}

#endif