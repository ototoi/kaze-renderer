#ifndef KAZE_SPHERE_SHADOW_BOUND_H
#define KAZE_SPHERE_SHADOW_BOUND_H

#include "shadow_bound.h"

namespace kaze
{

    class sphere_shadow_bound : public shadow_bound
    {
    public:
        sphere_shadow_bound(const vector3& p, real radius) : p_(p), radius_(radius) {}
        vector3 min() const;
        vector3 max() const;
        bool cast(aggregator<shadow_segment>* segs, const ray& r, real dist) const;

    private:
        vector3 p_;
        real radius_;
    };
}

#endif
