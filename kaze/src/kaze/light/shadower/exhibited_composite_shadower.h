#ifndef KAZE_EXHIBITED_COMPOSITE_SHADOWER_H
#define KAZE_EXHIBITED_COMPOSITE_SHADOWER_H

#include "chained_composite_shadower.h"

namespace kaze
{

    typedef chained_composite_shadower exhibited_composite_shadower;
}

#endif
