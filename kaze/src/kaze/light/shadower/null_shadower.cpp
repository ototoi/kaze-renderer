#include "null_shadower.h"

namespace kaze
{

    void null_shadower::cast(aggregator<lightpath>* lps, const lightpath& lp) const
    {
        lps->add(lp);
    }
}