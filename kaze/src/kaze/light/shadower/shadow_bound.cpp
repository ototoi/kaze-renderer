#include "shadow_bound.h"

namespace kaze
{

    bool shadow_bound::cast(aggregator<shadow_segment>* segs, const ray& r, real dist) const
    {
        return this->cast(segs, r.origin(), r.origin() + dist * r.direction());
    }

    bool shadow_bound::cast(aggregator<shadow_segment>* segs, const vector3& from, const vector3& to) const
    {
        vector3 dir = to - from;
        real dist = dir.length();
        if (dist <= 0) return false;
        dir *= real(1) / dist;
        return this->cast(segs, ray(from, dir), dist);
    }
}
