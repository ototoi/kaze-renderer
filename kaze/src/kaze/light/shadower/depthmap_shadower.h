#ifndef KAZE_DEPTHMAP_SHADOWER_H
#define KAZE_DEPTHMAP_SHADOWER_H

#include "shadower.h"
#include <vector>
#include "values.h"

namespace kaze
{

    class depthmap_shadower_imp;

    class depthmap_shadower : public shadower
    {
    public:
        depthmap_shadower();
        depthmap_shadower(const vector3& c, const std::vector<vector3>& vec, real arg = values::radians(30));
        ~depthmap_shadower();

    public:
        void cast(aggregator<lightpath>* lps, const lightpath& lp) const;

    private:
        depthmap_shadower_imp* imp_;
    };
}

#endif