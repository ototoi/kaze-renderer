#ifndef KAZE_light_attenuator_H
#define KAZE_light_attenuator_H

#include "shadower.h"
#include "filter.h"

namespace kaze
{

    class light_attenuator : public shadower
    {
    public:
        light_attenuator(real dist);
        light_attenuator(const auto_count_ptr<filter>& f);
        light_attenuator(const auto_count_ptr<filter>& f, real dist);
        void cast(aggregator<lightpath>* lps, const lightpath& lp) const;

    private:
        std::shared_ptr<filter> f_;
    };
}

#endif