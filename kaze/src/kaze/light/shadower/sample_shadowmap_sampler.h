#ifndef KAZE_SAMPLE_SHADOWMAP_SAMPLER_H
#define KAZE_SAMPLE_SHADOWMAP_SAMPLER_H

#include "shadowmap_sampler.h"
#include "aggregator.hpp"
#include "sampler.hpp"
#include "count_ptr.hpp"

namespace kaze
{

    class sample_shadowmap_sampler : public shadowmap_sampler
    {
    public:
        sample_shadowmap_sampler(const auto_count_ptr<sampler<vector2> >& smp, int n);
        void get(aggregator<vector2>* agr) const;

    private:
        std::shared_ptr<sampler<vector2> > smp_;
        int nSamples_;
    };
}
#endif
