#ifndef KAZE_SHADOWMAP_SAMPLER_H
#define KAZE_SHADOWMAP_SAMPLER_H

#include "aggregator.hpp"
#include "types.h"

namespace kaze
{

    class shadowmap_sampler
    {
    public:
        virtual ~shadowmap_sampler() {}
        virtual void get(aggregator<vector2>* agr) const = 0;
    };
}

#endif