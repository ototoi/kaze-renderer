#ifndef SHADOW_BOUND_SHADOWER_H
#define SHADOW_BOUND_SHADOWER_H

#include "shadower.h"
#include "shadow_bound.h"
#include "count_ptr.hpp"

namespace kaze
{

    class shadow_bound_shadower : public shadower
    {
    public:
        shadow_bound_shadower(const auto_count_ptr<shadow_bound>& bnd);
        void cast(aggregator<lightpath>* lps, const lightpath& lp) const;

    protected:
        std::shared_ptr<shadow_bound> bnd_;
    };
}

#endif
