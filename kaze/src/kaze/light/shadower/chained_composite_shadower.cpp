#include "chained_composite_shadower.h"
#include "basic_collection.hpp"

namespace kaze
{
#if 0
    class chain{
    public:
        chain(){}
        explicit chain(const std::shared_ptr<shadower>& shd):shd_(shd){}
        void cast(aggregator<lightpath>* lps, const lightpath& lp)const
        {
            if(!ch_.get()){
                lps->add(lp);
            }else{
                basic_collection<lightpath> c;
                shd_->cast(&c, lp);
                if(!c.empty()){
                    size_t sz = c.size();
                    for(size_t i = 0;i<sz;i++){
                        ch_->cast(lps,c[i]);
                    }
                }
            }
        } 
        void add(const std::shared_ptr<shadower>& shd){
            if(!ch_.get()){
                ch_.reset(new chain(shd));                
            }else{
                ch_->add(shd);                
            }
        }
    private:
        std::shared_ptr<shadower> shd_;
        std::shared_ptr<chain>    ch_;
    };
#endif

    class chained_shadower : public shadower
    {
    public:
        chained_shadower() {}
        void cast(aggregator<lightpath>* lps, const lightpath& lp) const
        {
            if (!ch_.empty())
            {
                size_t sz = ch_.size();
                basic_collection<lightpath> in;
                basic_collection<lightpath> out;
                in.add(lp);
                for (size_t i = 0; i < sz; i++)
                {
                    ch_[i]->cast(&out, in);
                    in.swap(out);
                    out.clear();
                }
                size_t lsz = in.size();
                for (size_t i = 0; i < lsz; i++)
                {
                    lps->add(in[i]);
                }
            }
            else
            {
                lps->add(lp);
            }
        }
        void add(const std::shared_ptr<shadower>& shd)
        {
            ch_.push_back(shd);
        }

    private:
        std::vector<std::shared_ptr<shadower> > ch_;
    };

    chained_composite_shadower::chained_composite_shadower()
    {
        shd_ = new chained_shadower();
    }

    chained_composite_shadower::~chained_composite_shadower()
    {
        delete shd_;
    }

    void chained_composite_shadower::add(shadower* shd)
    {
        std::shared_ptr<shadower> cp(shd);
        this->add(cp);
    }

    void chained_composite_shadower::add(const std::shared_ptr<shadower>& shd)
    {
        shd_->add(shd);
    }

    void chained_composite_shadower::cast(aggregator<lightpath>* lps, const lightpath& lp) const
    {
        shd_->cast(lps, lp);
    }
}
