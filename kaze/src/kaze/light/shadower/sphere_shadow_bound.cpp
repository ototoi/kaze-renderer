#include "sphere_shadow_bound.h"

namespace kaze
{
    namespace
    {
        struct range_
        {
            real t0;
            real t1;
        };
    }

    vector3 sphere_shadow_bound::min() const
    {
        real r = radius_;
        return vector3(p_[0] - r, p_[1] - r, p_[2] - r);
    }

    vector3 sphere_shadow_bound::max() const
    {
        real r = radius_;
        return vector3(p_[0] + r, p_[1] + r, p_[2] + r);
    }

    static bool test_sphere(range_* rng, const vector3& p, real rad, const vector3& org, const vector3& dir)
    {
        static const real EPSILON = std::numeric_limits<real>::epsilon() * 1024;

        real r2 = rad * rad;
        vector3 O = org - p;
        real B = dot(O, dir);
        real C = dot(O, O) - r2;
        real D = B * B - C;
        if (D < EPSILON) return false;
        D = sqrt(D);

        real t0 = -B - D;
        real t1 = -B + D;

        rng->t0 = t0;
        rng->t1 = t1;

        return true;
    }

    bool sphere_shadow_bound::cast(aggregator<shadow_segment>* segs, const ray& r, real dist) const
    {
        real rr = 2 * radius_;

        vector3 org = r.origin();
        vector3 dir = r.direction();
        range_ rng;
        if (test_sphere(&rng, p_, radius_, org, dir))
        {
            real t0 = rng.t0;
            real t1 = rng.t1;
            if (dist < t0) return false;
            if (t0 < 0)
            {
                t0 = 0;
            }
            if (t1 < 0) return false;
            if (t1 > dist)
            {
                t1 = dist;
            }

            real diff = t1 - t0;
            if (diff < 0) return false;
            real k = diff / rr;

            real red = pow(0.3, k);
            real green = pow(0.3, k);
            real blue = pow(0.9, k);
            shadow_segment tmp;
            tmp.t = t1;
            tmp.opacity = color3(red, green, blue);
            segs->add(tmp);
            return true;
        }
        return false;
    }
}
