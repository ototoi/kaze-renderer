#include "depthmap_shadower.h"

#include "values.h"

#include <vector>
#include <algorithm>
#include <functional>
#include <iterator>
#include <cmath>

#include "spectrum.h"

namespace kaze
{
    namespace
    {
        struct dm_node
        {
            vector3 pos; //normalized position
            real depth;
            int plane;
        };

        class depth_map
        {
        public:
            depth_map(const vector3& c, const std::vector<vector3>& vec);
            bool find(std::vector<real>& d, const vector3& dir, real r);

        protected:
            bool find_internal(std::vector<real>& d, size_t a, size_t b, const vector3& pos, real r2);
            bool find_i(std::vector<real>& d, size_t i, const vector3& pos, real r2);

        private:
            std::vector<dm_node> nodes_;
        };

        typedef std::vector<dm_node*>::iterator iter;

        struct axis_sorter : public std::binary_function<dm_node*, dm_node*, bool>
        {
            axis_sorter(int p) : p_(p) {}
            bool operator()(dm_node* a, dm_node* b) const
            {
                return a->pos[p_] < b->pos[p_];
            }
            int p_;
        };

        void construct(iter a, iter b, const vector3& min, const vector3& max)
        {
            size_t sz = b - a;
            if (sz <= 1) return;

            vector3 wid = max - min;
            int plane = 0;
            if (wid[1] > wid[plane]) plane = 1;
            if (wid[2] > wid[plane]) plane = 2;

            std::sort(a, b, axis_sorter(plane));

            vector3 cmin, cmax;
            size_t m = (sz >> 1);
            iter c = a + m;

            real pos = (*c)->pos[plane];
            (*c)->plane = plane;

            cmin = min;
            cmin = max;
            cmax[plane] = pos;
            construct(a, c, cmin, cmax);

            cmin = min;
            cmin[plane] = pos;
            cmin = max;
            construct(c + 1, b, cmin, cmax);
        }

        depth_map::depth_map(const vector3& c, const std::vector<vector3>& vec)
        {
            size_t sz = vec.size();
            if (sz == 0) return;

            nodes_.resize(sz);
            memset(&(nodes_[0]), 0, sizeof(dm_node) * sz);
            for (size_t i = 0; i < sz; i++)
            {
                vector3 d = vec[i] - c;
                real l = d.length();
                d *= real(1) / l;
                nodes_[i].depth = l + (values::epsilon() * 1000);
                nodes_[i].pos = d;
            }
            std::vector<dm_node*> tmp(sz);
            for (size_t i = 0; i < sz; i++)
            {
                tmp[i] = &(nodes_[i]);
            }

            vector3 min, max;
            /*
    min = max = nodes_[0].pos;
    for(size_t i = 1;i<sz;i++){
      for(int j = 0;j<3;j++){
        if(min[j]>nodes_[i].pos[j])min[j]=nodes_[i].pos[j];
        if(max[j]<nodes_[i].pos[j])max[j]=nodes_[i].pos[j];
      }
    }
    */
            min = vector3(-1, -1, -1);
            max = vector3(+1, +1, +1);

            //
            construct(tmp.begin(), tmp.end(), min, max);
            //

            std::vector<dm_node> s(sz);
            for (size_t i = 0; i < sz; i++)
            {
                s[i] = *(tmp[i]);
            }
            nodes_.swap(s);
        }

        bool depth_map::find_i(std::vector<real>& d, size_t i, const vector3& pos, real r2)
        {
            vector3 diff = (nodes_[i].pos - pos);
            real l2 = diff.sqr_length();
            if (l2 <= r2)
            {
                d.push_back(nodes_[i].depth);
                return true;
            }
            return false;
        }

        bool depth_map::find_internal(std::vector<real>& d, size_t a, size_t b, const vector3& pos, real r2)
        {
            bool bRet = false;
            size_t sz = b - a;
            if (sz <= 8)
            {
                for (size_t i = a; i < b; i++)
                {
                    bRet |= find_i(d, i, pos, r2);
                }
            }
            else
            {
                size_t m = a + (sz >> 1);
                int plane = nodes_[m].plane;
                real l = nodes_[m].pos[plane] - pos[plane];
                real l2 = l * l;
                //
                //+ | -
                //
                if (l > 0)
                {
                    bRet |= find_internal(d, a, m, pos, r2); //LEFT
                    if (l2 <= r2)
                    {
                        bRet |= find_i(d, m, pos, r2);               //MIDDLE
                        bRet |= find_internal(d, m + 1, b, pos, r2); //RIGHT
                    }
                }
                else if (l < 0)
                {
                    bRet |= find_internal(d, m + 1, b, pos, r2); //RIGHT
                    if (l2 <= r2)
                    {
                        bRet |= find_i(d, m, pos, r2);           //MIDDLE
                        bRet |= find_internal(d, a, m, pos, r2); //LEFT
                    }
                }
                else
                {
                    bRet |= find_internal(d, a, m, pos, r2);     //LEFT
                    bRet |= find_i(d, m, pos, r2);               //MIDDLE
                    bRet |= find_internal(d, m + 1, b, pos, r2); //RIGHT
                }
            }
            return bRet;
        }

        bool depth_map::find(std::vector<real>& d, const vector3& dir, real r)
        {
            size_t sz = nodes_.size();
            real r2 = r * r;
            return find_internal(d, 0, sz, dir, r2);
        }
    }

    class depthmap_shadower_imp
    {
    public:
        depthmap_shadower_imp();
        depthmap_shadower_imp(const vector3& c, const std::vector<vector3>& vec, real arg = values::radians(30));
        ~depthmap_shadower_imp();

    public:
        void cast(aggregator<lightpath>* lps, const lightpath& lp) const;

    private:
        vector3 c_;
        depth_map* p_dm;
        real rad_;
    };
    //--------------------------------------------------------------------------------
    //
    //
    //
    //
    inline static real pow2(real x) { return x * x; }
    static real get_radius(real radians)
    {
        radians = std::min<real>(radians, values::pi()); //arc180
        if (radians <= 0) return 0;
        real theta = radians * 0.5;
        //real s = sin(theta);
        //real c = cos(theta);
        //real l = sqrt(pow2(s)+pow2(1-c));
        real l = 2 * sin(0.5 * theta);
        //real l = sqrt(2*(1-cos(theta)));
        return l;
    }
    //--------------------------------------------------------------------------------

    depthmap_shadower_imp::depthmap_shadower_imp()
        : c_(vector3(0, 0, 0)), p_dm(0)
    {
        rad_ = get_radius(values::radians(30.0));
    }

    depthmap_shadower_imp::depthmap_shadower_imp(const vector3& c, const std::vector<vector3>& vec, real arg)
        : c_(c), p_dm(0)
    {
        rad_ = get_radius(arg);
        p_dm = new depth_map(c, vec);
    }

    depthmap_shadower_imp::~depthmap_shadower_imp()
    {
        if (p_dm) delete p_dm;
    }

    void depthmap_shadower_imp::cast(aggregator<lightpath>* lps, const lightpath& lp) const
    {
        static const real EPSILON = 0.000001;
        real rad = rad_; //TODO
        if (p_dm)
        {
            vector3 d = lp.to() - c_;
            real l = d.length();
            if (l > EPSILON)
            {
                d *= real(1) / l;

                std::vector<real> ds;
                if (p_dm->find(ds, d, rad))
                {

                    assert(!(ds.empty()));

                    int total = ds.size();
                    int count = 0;
                    for (int i = 0; i < total; i++)
                    {
                        if (ds[i] + EPSILON > l) count++;
                    }
                    if (count)
                    {
                        lps->add(lightpath(lp.from(), lp.to(), ((real(count)) / total) * lp.power()));
                    }
                }
                else
                {
                    lps->add(lp);
                }
            }
            else
            {
                lps->add(lp);
            }
        }
    }

    //--------------------------------------------------------------------------------

    depthmap_shadower::depthmap_shadower()
    {
        imp_ = new depthmap_shadower_imp();
    }

    depthmap_shadower::depthmap_shadower(const vector3& c, const std::vector<vector3>& vec, real arg)
    {
        imp_ = new depthmap_shadower_imp(c, vec, arg);
    }

    depthmap_shadower::~depthmap_shadower()
    {
        delete imp_;
    }

    void depthmap_shadower::cast(aggregator<lightpath>* lps, const lightpath& lp) const
    {
        imp_->cast(lps, lp);
    }
}
