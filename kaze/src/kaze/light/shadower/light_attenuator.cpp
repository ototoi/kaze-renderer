#include "light_attenuator.h"
#include "filter.h"
#include "values.h"

#include <algorithm>
#include <cmath>

namespace kaze
{
    namespace
    {
        class distance_limit_filter : public filter
        {
        public:
            distance_limit_filter(real dist) : dist_(dist)
            {
            }
            real get(real x) const
            {
                return (x < dist_) ? 1 : 0;
            }

        private:
            real dist_;
        };

        class distance_adaptor_filter : public filter
        {
        public:
            distance_adaptor_filter(real dist, const std::shared_ptr<filter>& f)
                : f_(f)
            {
                assert(values::epsilon() < dist);
                dist = std::max(values::epsilon(), dist);
                idist_ = real(1.0) / dist;
            }
            real get(real x) const
            {
                return f_->get(x * idist_);
            }

        private:
            real idist_;
            std::shared_ptr<filter> f_;
        };
    }

    light_attenuator::light_attenuator(real dist)
    {
        f_.reset(new distance_limit_filter(dist));
    }

    light_attenuator::light_attenuator(const auto_count_ptr<filter>& f)
    {
        f_ = f;
    }

    light_attenuator::light_attenuator(const auto_count_ptr<filter>& f, real dist)
    {
        f_.reset(new distance_adaptor_filter(dist, f));
    }

    void light_attenuator::cast(aggregator<lightpath>* lps, const lightpath& lp) const
    {
        real l = lp.length();
        real a = f_->get(l);
        if (a > 0)
        {
            if (a < 1)
            {
                lps->add(lightpath(lp.from(), lp.to(), a * lp.power()));
            }
            else
            {
                lps->add(lp);
            }
        }
    }
}
