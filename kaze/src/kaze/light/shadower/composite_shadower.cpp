#include "composite_shadower.h"
#include "basic_collection.hpp"

namespace kaze
{

    void composite_shadower::cast(aggregator<lightpath>* lps, const lightpath& lp) const
    {
        size_t msz = mv_.size();
        if (msz >= 2)
        {
            basic_collection<lightpath> c;
            basic_collection<lightpath> c2;
            c.add(lp);
            for (size_t i = 0; i < msz; i++)
            {
                c2.clear();
                mv_[i]->cast(&c2, c);
                c.swap(c2);
                if (c.empty()) return;
            }
            size_t csz = c.size();
            for (size_t i = 0; i < csz; i++)
            {
                lps->add(c.get(i));
            }
        }
        else if (msz == 1)
        {
            mv_[0]->cast(lps, lp);
        }
        else
        {
            lps->add(lp);
        }
    }

    void composite_shadower::cast(aggregator<lightpath>* lps, const enumerator<lightpath>& enlps) const
    {
        size_t msz = mv_.size();
        if (msz >= 2)
        {
            basic_collection<lightpath> c;
            basic_collection<lightpath> c2;
            size_t sz = enlps.size();
            for (size_t i = 0; i < sz; i++)
            {
                c.add(enlps.get(i));
            }
            for (size_t i = 0; i < msz; i++)
            {
                c2.clear();
                mv_[i]->cast(&c2, c);
                c.swap(c2);
                if (c.empty()) return;
            }
            size_t csz = c.size();
            for (size_t i = 0; i < csz; i++)
            {
                lps->add(c.get(i));
            }
        }
        else if (msz == 1)
        {
            mv_[0]->cast(lps, enlps);
        }
        else
        {
            size_t sz = enlps.size();
            for (size_t i = 0; i < sz; i++)
            {
                lps->add(enlps.get(i));
            }
        }
    }

    void composite_shadower::add(const auto_count_ptr<shadower>& ap)
    {
        mv_.push_back(ap);
    }
}
