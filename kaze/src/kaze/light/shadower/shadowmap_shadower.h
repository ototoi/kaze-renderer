#ifndef KAZE_SHADOWMAP_SHADOWER_H
#define KAZE_SHADOWMAP_SHADOWER_H

#include "shadower.h"

#include "image.hpp"
#include "transformer.h"
#include "shadowmap_sampler.h"
#include "count_ptr.hpp"

namespace kaze
{

    class shadowmap_shadower : public shadower
    {
    public:
        shadowmap_shadower(
            const auto_count_ptr<image<real> >& img,
            const auto_count_ptr<transformer>& tr,
            const auto_count_ptr<shadowmap_sampler>& smp,
            real radius);
        ~shadowmap_shadower();
        void cast(aggregator<lightpath>* lps, const lightpath& lp) const;

    protected:
        void test(int& nPass, int& nTest, const vector3& p) const;
        bool test(real x, real y, real z) const;

    private:
        std::shared_ptr<image<real> > img_;
        std::shared_ptr<transformer> tfm_;
        std::shared_ptr<shadowmap_sampler> smp_;
        real radius_;
        real spn_;
    };
}

#endif