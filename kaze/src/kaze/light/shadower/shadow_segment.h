#ifndef KAZE_CAST_INFO_H
#define KAZE_CAST_INFO_H

#include "types.h"
#include "color.h"
#include <vector>

namespace kaze
{

    struct shadow_segment
    {
        real t;
        color3 opacity;
    };
}

#endif
