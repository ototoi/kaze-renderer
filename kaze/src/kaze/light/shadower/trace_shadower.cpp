#include "trace_shadower.h"
#include "ray.h"
#include "test_info.h"
#include "shader.h"

#include "lightpath.h"

namespace kaze
{

    trace_shadower::trace_shadower(const auto_count_ptr<intersection>& inter, real epsilon)
        : inter_(inter), epsilon_(epsilon)
    {
        ;
    }

    void trace_shadower::cast(aggregator<lightpath>* lps, const lightpath& lp) const
    {
        vector3 vl = lp.to() - lp.from();
        real leng = vl.length();
        real leng_e = leng - epsilon_;

        if (leng_e > 0)
        {
            vl *= real(1) / leng;
            if (!this->test(ray(lp.from(), vl), leng_e))
            { //no shadow
                lps->add(lp);
            }
        }
    }

    bool trace_shadower::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool trace_shadower::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }
}