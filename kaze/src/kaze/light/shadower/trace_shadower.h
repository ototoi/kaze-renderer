#ifndef KAZE_TRACE_SHADOWER_H
#define KAZE_TRACE_SHADOWER_H

#include "shadower.h"
#include "intersection.h"
#include "count_ptr.hpp"

#include <vector>

namespace kaze
{

    class trace_shadower : public shadower
    {
    public:
        trace_shadower(const auto_count_ptr<intersection>& inter, real epsilon = real(0.000001));
        void cast(aggregator<lightpath>* lps, const lightpath& lp) const;

    protected:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;

    protected:
        std::shared_ptr<intersection> inter_;
        real epsilon_;
    };
}

#endif