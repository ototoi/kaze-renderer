#ifndef KAZE_SHADOW_BOUND_H
#define KAZE_SHADOW_BOUND_H

#include "types.h"
#include "ray.h"
#include "shadow_segment.h"
#include "aggregator.hpp"

namespace kaze
{

    class shadow_bound
    {
    public:
        virtual ~shadow_bound() {}
        virtual vector3 min() const = 0;
        virtual vector3 max() const = 0;
        virtual bool cast(aggregator<shadow_segment>* segs, const ray& r, real dist) const;
        virtual bool cast(aggregator<shadow_segment>* segs, const vector3& from, const vector3& to) const;
    };
}

#endif
