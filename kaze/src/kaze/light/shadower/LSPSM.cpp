#include "LSPSM.h"
#include <vector>
#include <cmath>

#include "transform_matrix.h"

//from monsho's sample No.60

namespace kaze
{
    namespace
    {
        //! LiSPSMステータス
        typedef struct LSPSM_info
        {
            matrix4 matProj;   //!< 投影行列
            matrix4 matView;   //!< ビュー行列
            vector3 vLightDir; //!< ライトベクトル
            vector3 vViewDir;  //!< ビューベクトル
            vector3 vEyePos;   //!< 視点位置
            real fNearDist;    //!< ビューのニアクリップ面の距離
        } LSPSM_info;

        typedef struct view_info
        {
            vector3 from;
            vector3 to;
            real angle;
            real aspect;
            real near;
            real far;
        } view_info;

        typedef struct light_info
        {
            vector3 direction;
        } light_info;

        //----------------------------------------------
        /**
	 * @param v view vector
	 * @param l light vector
	 */
        inline vector3 calc_upvector(const vector3& v, const vector3& l)
        {
            vector3 side = cross(v, l);
            vector3 up = cross(l, side);
            return normalize(up); //normalize
        }
        //----------------------------------------------
        inline void get_minmax(vector3& min, vector3& max, const std::vector<vector3>& v)
        {
            min = max = v[0];
            size_t sz = v.size();
            for (size_t i = 1; i < sz; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (min[j] > v[i][j]) min[j] = v[i][j];
                    if (max[j] < v[i][j]) max[j] = v[i][j];
                }
            }
        }
        //----------------------------------------------
        inline void transform_v3(const matrix4& m, std::vector<vector3>& v)
        {
            size_t sz = v.size();
            for (size_t i = 0; i < sz; i++)
            {
                v[i] = m * v[i];
            }
        }
        //----------------------------------------------
        //
        //from monsho's sample No.60
        void calc_points_viewvolume(
            std::vector<vector3>& vec,
            matrix4& matView, real fAngle, real fAspect, real fNear, real fFar)
        {
            using namespace std;
            std::vector<vector3> v;

            real t = tan(fAngle / 2.0);
            real fNLen = t * fNear;
            real fFLen = t * fFar;
            vector3 vPos;

            // ニアプレーンの点を求める
            vPos[0] = fNLen * fAspect;
            vPos[1] = fNLen;
            vPos[2] = fNear;
            v.push_back(vPos);

            vPos[0] *= -1;
            v.push_back(vPos);

            vPos[1] *= -1;
            v.push_back(vPos);

            vPos[0] *= -1;
            v.push_back(vPos);

            // ファープレーンの点を求める
            vPos[0] = fNLen * fAspect;
            vPos[1] = fNLen;
            vPos[2] = fNear;
            v.push_back(vPos);

            vPos[0] *= -1;
            v.push_back(vPos);

            vPos[1] *= -1;
            v.push_back(vPos);

            vPos[0] *= -1;
            v.push_back(vPos);

            // ビュー行列の逆行列で変換する
            matrix4 matInvView = ~matView;
            for (int i = 0; i < 8; i++)
            {
                v[i] = matInvView * v[i];
            }

            vec.swap(v);
        }

//-----------------------------------------------------------
//-----------------------------------------------------------
#if 0
	void calc_LSPSM_matrix(
		LSPSM_info* info, 
		const view_info*  vinfo, 
		const light_info* linfo,
		const std::vector<vector3>& vec)
	{
		vector3		min, max;

		vector3 vdir = normalize(vinfo->to-vinfo->from);
		vector3 ldir = linfo->direction;
		
		real			cosGamma = dot(vdir, ldir);//
		real			sinGamma = sqrt(1.0 - cosGamma * cosGamma);		

		vector3 up = calc_upvector(vdir,ldir);// アップベクトルを求める
		matrix4 matLightCamera = world2camera(vinfo->from,vinfo->from+linfo->direction,up);// カメラ視点、ライトベクトル、アップベクトルからビュー行列を求める
		
		std::vector<vector3> tvec = vec;
		// ポイントリストを変換する
		transform_v3(matLightCamera, tvec);
		// 変換したポイントリストを内包するAABBを取得する
		get_minmax(min,max,tvec);

		// ライトスペースに垂直なビューのニアクリップ面とファークリップ面を求める
		// AABB はライトのビュー行列で変換されているので y が上記ビューの奥行きになる
		real    virtual_min = min[1];
		real    virtual_max = max[1];
		real    d = virtual_max-virtual_min;
		real    n = (virtual_min>0)?(virtual_min + d * 0.1f) : (d * 0.25f);
		real	f = n + d;
		real	p = virtual_min - n;

		// ニアクリップ面とファークリップ面からY方向への透視変換行列を求める
		D3DXMatrixIdentity(&matLisp);
		matLisp._22 = f / (f - n);
		matLisp._42 = -f * n / (f - n);
		matLisp._24 = 1.0f;
		matLisp._44 = 0.0f;

		// 現在のライト視点からの変換行列を求める
		D3DXMATRIX	matLVP;
		D3DXMatrixTranslation(&matLVP, 0.0f, -p, 0.0f);
		D3DXMatrixMultiply(&(pStatus->matView), &(pStatus->matView), &matLVP);
		D3DXMatrixMultiply(&matLVP, &matLVP, &matLisp);

		// この行列でポイントリストを変換する
		ListCopy.Transform(&matLVP);

		// AABBを取得する
		ListCopy.GetAABB(&max, &min);

		// 上記のAABBが (-1, -1, 0) 〜 (1, 1, 1) の範囲になるような行列を作成する
		// 位置とサイズを正確なものにするため
		ScaleMatrixToFit(&(pStatus->matProj), &max, &min);

		// 最終的な透視変換行列を求める
		D3DXMatrixMultiply(&(pStatus->matProj), &matLisp, &(pStatus->matProj));
	}

#endif
    }
}
