#ifndef KAZE_LINE_LIGHT_H
#define KAZE_LINE_LIGHT_H

#include "light.h"
#include "shadower.h"
#include "texture.hpp"
#include "count_ptr.hpp"

#include <vector>

namespace kaze
{

    class line_light : public light
    {
    public:
        line_light(const vector3& a, const vector3& b, const spectrum& pow, const auto_count_ptr<shadower>& shdr);
        line_light(const vector3& a, const vector3& b, const auto_count_ptr<texture<spectrum> >& bnd, const auto_count_ptr<shadower>& shdr);

    public:
        void cast(aggregator<lightpath>* lps, const sufflight& sl) const;

    public:
        void sample(aggregator<lightpath>* lps, const sufflight& sl, real w) const;

    private:
        vector3 pos_[2];
        std::shared_ptr<texture<spectrum> > bnd_;
        std::shared_ptr<shadower> shdr_;
    };
}

#endif
