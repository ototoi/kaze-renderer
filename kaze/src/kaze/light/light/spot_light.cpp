#include "spot_light.h"

#include <cmath>
#include <cassert>
#include <algorithm>

#include "shadower.h"
#include "basic_collection.hpp"
#include "logger.h"
#include "constant_texture.hpp"
#include "count_ptr.hpp"

#include "values.h"

namespace kaze
{

    using namespace std;

    static inline vector3 forward(const vector3& EYE, const vector3& N)
    {
        if (dot(EYE, N) > 0)
        {
            return -N;
        }
        else
        {
            return N;
        }
    }

    spot_light::spot_light(
        const vector3& org,
        const vector3& target,
        real angle_inter,
        real angle_outer,
        const spectrum& power,
        const auto_count_ptr<shadower>& shdr) : shdr_(shdr)
    {
        org_ = org;
        vec_ = normalize(target - org);

        real ai = std::min<real>(angle_inter, angle_outer);
        real ao = std::max<real>(angle_inter, angle_outer);

        ai = std::max<real>(std::min<real>(ai, values::pi()), 0);
        ao = std::max<real>(std::min<real>(ao, values::pi()), 0);

        iang_ = sin(ai / 2);
        oang_ = sin(ao / 2);

        tex_.reset(new constant_texture<spectrum>(power));
    }

    spot_light::spot_light(
        const vector3& org,
        const vector3& target,
        real angle_inter,
        real angle_outer,
        const auto_count_ptr<texture<spectrum> >& tex,
        const auto_count_ptr<shadower>& shdr) : tex_(tex), shdr_(shdr)
    {
        org_ = org;
        vec_ = normalize(target - org);

        real ai = std::min<real>(angle_inter, angle_outer);
        real ao = std::max<real>(angle_inter, angle_outer);

        ai = std::max<real>(std::min<real>(ai, values::pi()), 0);
        ao = std::max<real>(std::min<real>(ao, values::pi()), 0);

        iang_ = std::sin(ai / 2);
        oang_ = std::sin(ao / 2);
    }

    spot_light::~spot_light()
    {
        //
    }

    void spot_light::cast(aggregator<lightpath>* lps, const sufflight& sl) const
    {
        vector3 p = sl.position();
        vector3 o = this->org_;
        vector3 op = p - o;

        real iang = iang_;
        real oang = oang_;

        //vector3 sn = forward(sl.direction(),sl.geometric());
        // p += sn*0.00001;
        //real d = dot(sn,op);
        //if(d<0){
        real tang = sqrt(1 - dot(this->vec_, normalize(op)));
        if (tang <= oang)
        {
            real f = tang / oang;
            real alpha = 1.0;
            if (tang > iang)
            {
                alpha = (oang - tang) / (oang - iang);
            }
            shdr_->cast(lps, lightpath(o, p, alpha * tex_->get(f)));
        }
        //}
    }
}