#ifndef KAZE_LIGHT_TRANSFORM_MATRIX_H
#define KAZE_LIGHT_TRANSFORM_MATRIX_H

#include "types.h"

namespace kaze
{

    matrix4 spot_light_world2clip(const vector3& from, const vector3& to, real near, real far, real angle);
    matrix4 spot_light_world2camera(const vector3& from, const vector3& to);
    matrix4 spot_light_clip2screen(real w, real h, real d = real(1));
    matrix4 spot_light_shadow_matrix(
        const vector3& from, const vector3& to, real near, real far, real angle,
        real w, real h, real d = real(1));
}

#endif