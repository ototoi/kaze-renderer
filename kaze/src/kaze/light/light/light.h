#ifndef KAZE_LIGHT_H
#define KAZE_LIGHT_H

#include "sufflight.h"
#include "lightpath.h"
#include "aggregator.hpp"

namespace kaze
{

    class light
    {
    public:
        virtual ~light() {}
    public:
        virtual void cast(aggregator<lightpath>* lps, const sufflight& sl) const = 0;
    };
}

#endif