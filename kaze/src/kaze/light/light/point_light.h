#ifndef KAZE_POINT_LIGHT_H
#define KAZE_POINT_LIGHT_H

#include "light.h"
#include "spectrum.h"
#include "intersection.h"

#include "shadower.h"
#include "count_ptr.hpp"

namespace kaze
{

    class point_light : public light
    {
    public:
        point_light(
            const vector3& org,
            const spectrum& power,
            const auto_count_ptr<shadower>& shdr);
        ~point_light();

    public:
        void cast(aggregator<lightpath>* lps, const sufflight& sl) const;

    private:
        vector3 org_;
        spectrum pow_;
        std::shared_ptr<shadower> shdr_;
    };
}

#endif
