#include "environment_light.h"
#include "values.h"
#include "memory_image.hpp"

namespace kaze
{

    environment_light_sample_map::environment_light_sample_map(const std::shared_ptr<image<real> >& img)
    {
        int width = img->get_width();
        int height = img->get_height();
        {
            std::shared_ptr<image<float> > cdf_img(new memory_image<float>(width, height));
            for (int y = 0; y < height; y++)
            {
                float sum = 0.0f;
                for (int x = 0; x < width; x++)
                {
                    float intensity = (float)img->get(x, y);
                    sum += intensity;
                    cdf_img->set(x, y, sum);
                }

                float isum = 1.0f / sum;
                for (int x = 0; x < width; x++)
                {
                    float intensity = (float)cdf_img->get(x, y);
                    intensity *= isum;
                    cdf_img->set(x, y, intensity);
                }
            }
            cdf_ = cdf_img;
        }

        {
            ycdf_.resize(height);

            float sum = 0.0f;
            for (int y = 0; y < height; y++)
            {
                float yy = fabs(2.0f * ((y + 0.5f) / height) - 1.0f);
                float rr = sqrt(1.0f - yy * yy);
                sum += rr;
                ycdf_[y] = sum;
            }
            float isum = 1.0f / sum;
            for (int y = 0; y < height; y++)
            {
                ycdf_[y] *= isum;
            }
        }
    }

    static float FindYIndex(const std::vector<float>& v, int i0, int i1, float y)
    {
        int diff = i1 - i0;
        if (1 < diff)
        {
            int im = (i0 + i1) >> 1;
            if (y <= v[im])
            {
                return FindYIndex(v, i0, im, y);
            }
            else
            {
                return FindYIndex(v, im, i1, y);
            }
        }
        else
        {
            float a = (y - v[i0]) / (v[i1] - v[i0]);
            return i0 + a;
        }
    }

    static float FindYIndex(const std::vector<float>& v, float y)
    {
        int i0 = 0;
        int i1 = (int)(v.size() - 1);
        return FindYIndex(v, i0, i1, y);
    }

    static float FindXIndex(const std::shared_ptr<image<float> >& img, float x, int y)
    {
        return 0;
    }

    vector2 environment_light_sample_map::sample(const vector2& uv) const
    {
        real u = uv[0];
        real v = uv[1];

        float yy = FindYIndex(ycdf_, v);
        int y0 = (int)floor(yy);
        int y1 = (int)y0 + 1;
        float ya = yy - y0;

        return uv;
    }

    environment_light::environment_light(const std::shared_ptr<image<color3> >& img, const std::shared_ptr<shadower>& shd)
        : shdr_(shd)
    {
        int width = img->get_width();
        int height = img->get_height();
        std::shared_ptr<image<real> > int_img(new memory_image<real>(width, height));
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                real intensity = spectrum(img->get(x, y)).intensity();
                int_img->set(x, y, intensity);
            }
        }
        //cdf_ = int_img;
    }

    static vector3 GetPosistion(const vector2& uv)
    {
        return vector3(1, 1, 1);
    }

    void environment_light::cast(aggregator<lightpath>* lps, const sufflight& sl) const
    {
        vector2 uv1 = smp_->get();
        vector2 uv2 = cdf_->sample(uv1);
        vector3 o = GetPosistion(uv2) * values::far();
        spectrum c = tex_->get(uv2[0], uv2[1]);
        shdr_->cast(lps, lightpath(o, sl.position(), c));
    }
}
