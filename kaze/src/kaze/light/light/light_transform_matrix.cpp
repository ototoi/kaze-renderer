#include "light_transform_matrix.h"
#include "transform_matrix.h"

namespace kaze
{

    matrix4 spot_light_world2clip(const vector3& from, const vector3& to, real near, real far, real angle_outer)
    {
        vector3 dir = normalize(to - from);
        //
        int plane = 0;
        if (fabs(dir[plane]) > fabs(dir[1])) plane = 1;
        if (fabs(dir[plane]) > fabs(dir[2])) plane = 2;
        vector3 upper(0, 0, 0);
        upper[plane] = real(1);
        //
        matrix4 cam = world2camera(from, to, upper);
        matrix4 clp = camera2clip(angle_outer, real(1), near, far);

        return clp * cam;
    }

    matrix4 spot_light_world2camera(const vector3& from, const vector3& to)
    {
        vector3 dir = normalize(to - from);
        //
        int plane = 0;
        if (fabs(dir[plane]) > fabs(dir[1])) plane = 1;
        if (fabs(dir[plane]) > fabs(dir[2])) plane = 2;
        vector3 upper(0, 0, 0);
        upper[plane] = real(1);
        //
        matrix4 cam = world2camera(from, to, upper);
        return cam;
    }

    matrix4 spot_light_clip2screen(real w, real h, real d)
    {
        return clip2screen(w, h, d);
    }

    matrix4 spot_light_shadow_matrix(
        const vector3& from, const vector3& to, real near, real far, real angle,
        real w, real h, real d)
    {
        return spot_light_clip2screen(w, h, d) * spot_light_world2clip(from, to, near, far, angle);
    }
}
