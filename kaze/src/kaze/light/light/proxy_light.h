#ifndef KAZE_PROXY_LIGHT_H
#define KAZE_PROXY_LIGHT_H

#include "light.h"

namespace kaze
{

    class proxy_light : public light
    {
    public:
        proxy_light(const light* l) : light_(l) {}
        void cast(aggregator<lightpath>* lps, const sufflight& sl) const { light_->cast(lps, sl); }
    protected:
        const light* light_;
    };
}

#endif