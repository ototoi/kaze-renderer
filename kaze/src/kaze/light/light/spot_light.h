#ifndef KAZE_SPOT_LIGHT_H
#define KAZE_SPOT_LIGHT_H

#include "light.h"
#include "spectrum.h"
#include "texture.hpp"
#include "shadower.h"
#include "filter.h"
#include "count_ptr.hpp"

namespace kaze
{

    class spot_light : public light
    {
    public:
        spot_light(
            const vector3& org,
            const vector3& target,
            real angle_inter,
            real angle_outer,
            const spectrum& power,
            const auto_count_ptr<shadower>& shdr);

        spot_light(
            const vector3& org,
            const vector3& target,
            real angle_inter,
            real angle_outer,
            const auto_count_ptr<texture<spectrum> >& tex,
            const auto_count_ptr<shadower>& shdr);

        ~spot_light();

    public:
        void cast(aggregator<lightpath>* lps, const sufflight& sl) const;

    private:
        vector3 org_;
        vector3 vec_;
        real iang_;
        real oang_;
        std::shared_ptr<texture<spectrum> > tex_;
        std::shared_ptr<shadower> shdr_;
    };
}

#endif