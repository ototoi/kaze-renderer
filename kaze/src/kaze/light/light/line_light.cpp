#include "line_light.h"
#include "random.h"
#include "band_texture.hpp"
#include "constant_texture.hpp"

#define NSAMPLE 20
#define DSAMPLE (1.0 / NSAMPLE)

namespace kaze
{

    static real rand01()
    {
        static const real c = 1.0 / (real(std::numeric_limits<rand_t>::max()) + 1);
        return real(xor128()) * c;
    }

    static inline vector3 forward(const vector3& EYE, const vector3& N)
    {
        if (dot(EYE, N) > 0)
        {
            return -N;
        }
        else
        {
            return N;
        }
    }

    line_light::line_light(const vector3& a, const vector3& b, const spectrum& pow, const auto_count_ptr<shadower>& shdr)
        : shdr_(shdr)
    {
        pos_[0] = a;
        pos_[1] = b;
        bnd_.reset(new constant_texture<spectrum>(pow));
    }

    line_light::line_light(const vector3& a, const vector3& b, const auto_count_ptr<texture<spectrum> >& bnd, const auto_count_ptr<shadower>& shdr)
        : bnd_(bnd), shdr_(shdr)
    {
        pos_[0] = a;
        pos_[1] = b;
    }

    void line_light::sample(aggregator<lightpath>* lps, const sufflight& sl, real w) const
    {
        vector3 p = sl.position();
        vector3 sn = forward(sl.direction(), sl.geometric());
        {
            real v = rand01();
            vector3 o = (1 - v) * pos_[0] + v * pos_[1];
            vector3 op = p - o;
            real d = dot(sn, op);
            if (d < 0)
            {
                shdr_->cast(lps, lightpath(o, sl.position(), bnd_->get(v) * w));
            }
        }
    }

    void line_light::cast(aggregator<lightpath>* lps, const sufflight& sl) const
    {
        vector3 p = sl.position();
        vector3 sn = forward(sl.direction(), sl.geometric());

        for (size_t i = 0; i < NSAMPLE; i++)
        {
            sample(lps, sl, DSAMPLE);
        }
    }
}
