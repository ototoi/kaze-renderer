#ifndef KAZE_SPHERE_LIGHT_H
#define KAZE_SPHERE_LIGHT_H

#include "light.h"
#include "shadower.h"
#include "count_ptr.hpp"

namespace kaze
{

    class sphere_light : public light
    {
    public:
        sphere_light(
            const vector3& org, real radius,
            const spectrum& pow,
            const std::shared_ptr<shadower>& shdr);

        void cast(aggregator<lightpath>* lps, const sufflight& sl) const;

    private:
        vector3 org_;
        real rad_;
        spectrum pow_;
        std::shared_ptr<shadower> shdr_;
    };
}

#endif
