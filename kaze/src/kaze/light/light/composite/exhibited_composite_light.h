#ifndef KAZE_EXHIBITED_COMPOSITE_LIGHT_H
#define KAZE_EXHIBITED_COMPOSITE_LIGHT_H

#include "composite_light.h"
#include "count_ptr.hpp"
#include <vector>

namespace kaze
{

    class exhibited_composite_light : public composite_light
    {
    public:
        exhibited_composite_light();
        ~exhibited_composite_light();

    public:
        void add(light* l);
        void add(const auto_count_ptr<light>& l);
        void cast(aggregator<lightpath>* lps, const sufflight& sl) const;

    private:
        std::vector<std::shared_ptr<light> > mv_;
    };
}

#endif