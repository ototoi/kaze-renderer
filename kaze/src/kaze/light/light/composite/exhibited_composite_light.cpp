#include "exhibited_composite_light.h"

namespace kaze
{

    exhibited_composite_light::exhibited_composite_light() {}

    exhibited_composite_light::~exhibited_composite_light()
    {
        ; //
    }

    void exhibited_composite_light::add(light* l)
    {
        std::shared_ptr<light> cp(l);
        mv_.push_back(cp);
    }

    void exhibited_composite_light::add(const auto_count_ptr<light>& l)
    {
        mv_.push_back(l);
    }

    void exhibited_composite_light::cast(aggregator<lightpath>* lps, const sufflight& sl) const
    {
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            mv_[i]->cast(lps, sl);
        }
    }
}
