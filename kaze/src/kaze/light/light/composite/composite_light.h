#ifndef KAZE_COMPOSITE_LIGHT_H
#define KAZE_COMPOSITE_LIGHT_H

#include "light.h"

namespace kaze
{

    class composite_light : public light
    {
    public:
        virtual ~composite_light() {}
    public:
        virtual void add(light* l) = 0;
        virtual void cast(aggregator<lightpath>* lps, const sufflight& sl) const = 0;
    };
}

#endif
