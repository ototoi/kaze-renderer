#ifndef KAZE_ENVIRONMENT_LIGHT_H
#define KAZE_ENVIRONMENT_LIGHT_H

#include "light.h"
#include "shadower.h"
#include "sampler.hpp"
#include "image.hpp"
#include "texture.hpp"
#include "count_ptr.hpp"

namespace kaze
{

    class environment_light_sample_map
    {
    public:
        environment_light_sample_map(const std::shared_ptr<image<real> >& img);
        vector2 sample(const vector2& uv) const;

    private:
        std::vector<float> ycdf_;
        std::shared_ptr<image<float> > cdf_;
    };

    class environment_light : public light
    {
    public:
        environment_light(const std::shared_ptr<image<color3> >& img, const std::shared_ptr<shadower>& shd);

    public:
        void cast(aggregator<lightpath>* lps, const sufflight& sl) const;

    public:
        std::shared_ptr<texture<spectrum> > tex_;
        std::shared_ptr<shadower> shdr_;
        std::shared_ptr<sampler<vector2> > smp_;
        std::shared_ptr<environment_light_sample_map> cdf_;
    };
}

#endif