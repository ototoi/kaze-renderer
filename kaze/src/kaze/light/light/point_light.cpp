#include "point_light.h"
#include <cassert>

namespace kaze
{

    static inline vector3 forward(const vector3& EYE, const vector3& N)
    {
        if (dot(EYE, N) > 0)
        {
            return -N;
        }
        else
        {
            return N;
        }
    }

    point_light::point_light(
        const vector3& org,
        const spectrum& power,
        const auto_count_ptr<shadower>& shdr) : org_(org), pow_(power), shdr_(shdr) { ; }

    point_light::~point_light()
    {
        ;
    }

    void point_light::cast(aggregator<lightpath>* lps, const sufflight& sl) const
    {
        vector3 p = sl.position();
        vector3 o = this->org_;
        vector3 op = p - o;
        vector3 sn = forward(sl.direction(), sl.geometric());
        real d = dot(sn, op);
        if (d < 0)
        {
            shdr_->cast(lps, lightpath(o, sl.position(), pow_));
        }
    }
}
