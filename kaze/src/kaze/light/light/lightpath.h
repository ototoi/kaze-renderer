#ifndef KAZE_LIGHTPATH_H
#define KAZE_LIGHTPATH_H

#include "types.h"
#include "spectrum.h"

namespace kaze
{

    class lightpath
    {
    public:
        lightpath(const vector3& a, const vector3& b, const spectrum& pow) : from_(a), to_(b), pow_(pow) {}
        lightpath(const lightpath& rhs) : from_(rhs.from_), to_(rhs.to_), pow_(rhs.pow_) {}

        lightpath& operator=(const lightpath& rhs)
        {
            from_ = rhs.from_;
            to_ = rhs.to_;
            pow_ = rhs.pow_;
            return *this;
        }

    public:
        const vector3& from() const { return from_; }
        const vector3& to() const { return to_; }
        const spectrum& power() const { return pow_; }
        real length() const { return ((to() - from())).length(); }
    public:
        vector3 direction() const { return normalize(to() - from()); }
    private:
        vector3 from_;
        vector3 to_;
        spectrum pow_;
    };
}

#endif