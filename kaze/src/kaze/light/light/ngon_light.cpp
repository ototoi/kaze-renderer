#include "ngon_light.h"

#include "triangulate_polygon.h"
#include "random.h"

#include "shadower.h"
#include "count_ptr.hpp"

#include "triangle_quad_tree.h"

#include "constant_texture.hpp"

#include <vector>

namespace kaze
{
    static inline vector3 forward(const vector3& EYE, const vector3& N)
    {
        if (dot(EYE, N) > 0)
        {
            return -N;
        }
        else
        {
            return N;
        }
    }

    //-----------------------------------------------------------------s
    class ngon_light_imp
    {
    public:
        ngon_light_imp(
            const vector3& org, const vector3& u, const vector3& v,
            const std::vector<vector2>& loop,
            const std::shared_ptr<texture<spectrum> >& tex);
        void cast(aggregator<lightpath>* lps, const sufflight& sl) const;

    private:
        vector3 org_;
        vector3 u_;
        vector3 v_;
        std::shared_ptr<texture<spectrum> > tex_;
        std::shared_ptr<shadower> shdr_;
    };

    static void get_minmax(vector2& min, vector2& max, const std::vector<vector2>& v)
    {
        min = max = v[0];
        for (size_t i = 1; i < v.size(); i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (min[j] > v[i][j]) min[j] = v[i][j];
                if (max[j] < v[i][j]) max[j] = v[i][j];
            }
        }
    }

    ngon_light_imp::ngon_light_imp(
        const vector3& org, const vector3& u, const vector3& v,
        const std::vector<vector2>& loop,
        const std::shared_ptr<texture<spectrum> >& tex) : tex_(tex)
    {
        std::vector<vector2> tris;
        triangulate_polygon(tris, loop);
    }

    void ngon_light_imp::cast(aggregator<lightpath>* lps, const sufflight& sl) const
    {
    }

    //-----------------------------------------------------------------

    ngon_light::ngon_light(
        const vector3& org, const vector3& u, const vector3& v,
        const std::vector<vector2>& loop,
        const spectrum& pow)
    {
        std::shared_ptr<texture<spectrum> > tex(new constant_texture<spectrum>(pow));
        imp_ = new ngon_light_imp(org, u, v, loop, tex);
    }

    ngon_light::ngon_light(
        const vector3& org, const vector3& u, const vector3& v,
        const std::vector<vector2>& loop,
        const auto_count_ptr<texture<spectrum> >& tex)
    {
        imp_ = new ngon_light_imp(org, u, v, loop, tex);
    }

    ngon_light::~ngon_light()
    {
        delete imp_;
    }

    void ngon_light::cast(aggregator<lightpath>* lps, const sufflight& sl) const
    {
        imp_->cast(lps, sl);
    }
}
