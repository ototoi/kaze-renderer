#ifndef KAZE_NGON_LIGHT_H
#define KAZE_NGON_LIGHT_H

#include "light.h"

#include <vector>

#include "shadower.h"
#include "count_ptr.hpp"
#include "texture.hpp"

namespace kaze
{

    class ngon_light_imp;
    class ngon_light : public light
    {
    public:
        ngon_light(
            const vector3& org, const vector3& u, const vector3& v,
            const std::vector<vector2>& loop,
            const spectrum& pow);

        ngon_light(
            const vector3& org, const vector3& u, const vector3& v,
            const std::vector<vector2>& loop,
            const auto_count_ptr<texture<spectrum> >& tex);
        ~ngon_light();
        void cast(aggregator<lightpath>* lps, const sufflight& sl) const;

    private:
        ngon_light_imp* imp_;
    };

    typedef ngon_light polygon_light;
}

#endif
