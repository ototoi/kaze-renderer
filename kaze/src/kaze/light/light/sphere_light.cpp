#include "sphere_light.h"

namespace kaze
{

    sphere_light::sphere_light(
        const vector3& org, real radius,
        const spectrum& pow,
        const std::shared_ptr<shadower>& shdr) : org_(org), rad_(radius), pow_(pow), shdr_(shdr)
    {
        ; //
    }

    void sphere_light::cast(aggregator<lightpath>* lps, const sufflight& sl) const
    {
        ; //
    }
}