#ifndef KAZE_COMPRESSED_UV_H
#define KAZE_COMPRESSED_UV_H

#include "types.h"
namespace kaze
{

    class compressed_uv
    {
    public:
        typedef unsigned short int uint16;

    public:
        compressed_uv() {}
        compressed_uv(const compressed_uv& rhs)
        {
            mv_[0] = rhs.mv_[0];
            mv_[1] = rhs.mv_[1];
        }
        compressed_uv& operator=(const compressed_uv& rhs)
        {
            mv_[0] = rhs.mv_[0];
            mv_[1] = rhs.mv_[1];
            return *this;
        }
        explicit compressed_uv(const vector2& rhs);
        compressed_uv& operator=(const vector2& rhs);

        real operator[](std::size_t idx) const
        {
            static const real cof = real(1) / (std::numeric_limits<compressed_uv::uint16>::max() - 1);
            return cof * mv_[idx];
        }

        operator vector2() const
        {
            static const real cof = real(1) / (std::numeric_limits<compressed_uv::uint16>::max() - 1);
            return vector2(cof * mv_[0], cof * mv_[1]);
        }

    public:
        friend vector2 barycentric_convert(real w, real u, real v, const compressed_uv& uv0, const compressed_uv& uv1, const compressed_uv& uv2);

    private:
        uint16 mv_[2];
    };

    vector2 barycentric_convert(real w, real u, real v, const compressed_uv& uv0, const compressed_uv& uv1, const compressed_uv& uv2);
}

#endif
