#ifndef KAZE_MEMORY_MANAGER_HPP
#define KAZE_MEMORY_MANAGER_HPP

#include <vector>
#include <memory>

#include "types.h"
#include "aggregator.hpp"

namespace kaze
{

    template <class T>
    class memory_manager : public aggregator<T*>
    {
    public:
        typedef T* PTR_T;
        void add(const PTR_T& pT)
        {
            std::unique_ptr<T> ap(pT);
            mv_.push_back(ap.release());
            //mv_[mv_.size()-1] = ap.release();
        }

    public:
        memory_manager() {}
        ~memory_manager()
        {
            this->clear();
        }
        void clear()
        {
            std::size_t sz = mv_.size();
            for (std::size_t i = 0; i < sz; i++)
            {
                if (mv_[i]) delete mv_[i];
            }
            mv_.clear();
        }

        void release()
        {
            std::size_t sz = mv_.size();
            for (std::size_t i = 0; i < sz; i++)
            {
                mv_[i] = 0;
            }
            mv_.clear();
        }

        void swap(memory_manager& rhs)
        {
            mv_.swap(rhs.mv_);
        }

        std::size_t size() const { return mv_.size(); }
        PTR_T& operator[](std::size_t i) { return mv_[i]; }
        const PTR_T& operator[](std::size_t i) const { return mv_[i]; }
    private:
        memory_manager(const memory_manager& rhs);
        memory_manager& operator=(const memory_manager& rhs);

    private:
        std::vector<T*> mv_;
    };
}

#endif
