#ifndef KAZE_LEXICAL_CAST_HPP
#define KAZE_LEXICAL_CAST_HPP

#include <string>
#include <sstream>

namespace kaze
{

    template <class T>
    T lexical_cast(const char* str)
    {
        std::stringstream ss(str);
        T tmp;
        ss >> tmp;

        return tmp;
    }

    template <class T>
    T lexical_cast(int i)
    {
        std::stringstream ss;
        ss << i;
        T tmp;
        ss >> tmp;

        return tmp;
    }

    template <class T>
    T lexical_cast(double i)
    {
        std::stringstream ss;
        ss << i;
        T tmp;
        ss >> tmp;

        return tmp;
    }

    template <class T>
    T lexical_cast(const std::string& str)
    {
        return lexical_cast<T>(str.c_str());
    }
}

#endif