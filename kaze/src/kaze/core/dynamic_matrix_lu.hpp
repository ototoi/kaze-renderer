#ifndef KAZE_DYNAMIC_MATRIX_LU_HPP
#define KAZE_DYNAMIC_MATRIX_LU_HPP

#include "dynamic_matrix.hpp"
#include <vector>
#include <cmath>

namespace kaze
{

    template <class T>
    bool lu_separate(dynamic_matrix<T>& out, int* index)
    {
        using namespace std;

        int i, j, k, ii, ik;
        T t, u, det;

        int Sz = (int)out.rows();

        dynamic_matrix<T>& a = out;
        std::vector<T> buffer(Sz);

        det = T(0); //det
        for (k = 0; k < Sz; k++)
        {                 //row
            index[k] = k; //
            u = 0;        //max
            for (j = 0; j < Sz; j++)
            {
                t = fabs(a[k][j]);
                if (t > u) u = t;
            }
            if (u == 0) return false;
            buffer[k] = 1 / u; //
        }

        det = T(1); //
        for (k = 0; k < Sz; k++)
        { //
            u = -1;
            for (i = k; i < Sz; i++)
            {                  //less under
                ii = index[i]; //
                t = fabs(a[ii][k]) * buffer[ii];
                if (t > u)
                {
                    u = t;
                    j = i;
                }
            }

            ik = index[j];

            if (j != k)
            {
                index[j] = index[k];
                index[k] = ik; //swap
                det = -det;    //chage sign
            }

            u = a[ik][k];
            det *= u;

            if (u == 0) return false;
            for (i = k + 1; i < Sz; i++)
            {
                ii = index[i];
                t = (a[ii][k] /= u);
                for (j = k + 1; j < Sz; j++)
                    a[ii][j] -= t * a[ik][j];
            }
        }

        return (det != 0);
    }

    template <class T> //gauss joldan
    bool invert_lu(dynamic_matrix<T>& out, const dynamic_matrix<T>& rhs)
    {
        if (rhs.rows() != rhs.cols()) return false;
        int i, j, k, ii;
        T t;

        int Sz = (int)rhs.rows();

        std::vector<int> index(Sz);

        out = rhs;
        dynamic_matrix<T> a(rhs);
        dynamic_matrix<T>& a_inv(out);

        if (!lu_separate(a, &index[0])) return false;

        for (k = 0; k < Sz; k++)
        {
            for (i = 0; i < Sz; i++)
            {
                ii = index[i];
                t = (ii == k) ? T(1) : T(0);
                for (j = 0; j < i; j++)
                    t -= a[ii][j] * a_inv[j][k];
                a_inv[i][k] = t;
            }
            for (i = Sz - 1; i >= 0; i--)
            {
                t = a_inv[i][k];
                ii = index[i];
                for (j = i + 1; j < Sz; j++)
                    t -= a[ii][j] * a_inv[j][k];
                a_inv[i][k] = t / a[ii][i];
            }
        }

        return true;
    }
}

#endif
