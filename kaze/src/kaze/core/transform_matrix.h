#ifndef KAZE_TRANSFORM_MATRIX_H
#define KAZE_TRANSFORM_MATRIX_H

#include "types.h"

namespace kaze
{
    matrix4 world2camera(const vector3& from, const vector3& to, const vector3& upper);
    matrix4 world2camera_LH(const vector3& from, const vector3& to, const vector3& upper);
    matrix4 world2camera_RH(const vector3& from, const vector3& to, const vector3& upper);

    matrix4 camera2clip(real angle, real ratio, real Zn, real Zf);
    matrix4 camera2clip(real l, real r, real b, real t, real n, real f);

    matrix4 camera2clip_MP(real angle, real ratio, real Zn, real Zf);
    matrix4 camera2clip_MP(real l, real r, real b, real t, real n, real f);
    matrix4 camera2clip_01(real angle, real ratio, real Zn, real Zf);
    matrix4 camera2clip_01(real l, real r, real b, real t, real n, real f);

    matrix4 clip2screen_MP(real w, real h, real d);
    matrix4 clip2screen_01(real w, real h, real d);
    matrix4 clip2screen(real w, real h, real d = real(1));

    //
    matrix4 camera2world(const vector3& from, const vector3& to, const vector3& upper);
    matrix4 camera2world_LH(const vector3& from, const vector3& to, const vector3& upper);
    matrix4 camera2world_RH(const vector3& from, const vector3& to, const vector3& upper);
}

#endif