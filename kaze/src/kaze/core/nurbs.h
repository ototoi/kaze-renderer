#ifndef KAZE_NURBS_H
#define KAZE_NURBS_H

#include "bspline.h"

namespace kaze
{

#define DEF_BSPLINE(TYPE, FLOAT)                                                                            \
    TYPE nurbs_evaluate(const TYPE p[], const FLOAT w[], int n, const FLOAT knots[], int k, FLOAT t);       \
    TYPE nurbs_evaluate_deriv(const TYPE p[], const FLOAT w[], int n, const FLOAT knots[], int k, FLOAT t); \
    TYPE nurbs_evaluate(const TYPE p[], const FLOAT w[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v);

    DEF_BSPLINE(float, float)
    DEF_BSPLINE(vector2f, float)
    DEF_BSPLINE(vector3f, float)
    DEF_BSPLINE(double, double)
    DEF_BSPLINE(vector2d, double)
    DEF_BSPLINE(vector3d, double)

#undef DEF_BSPLINE

#define DEF_BSPLINE(TYPE, FLOAT)                                                                            \
    void nurbs_convert_bezier_type(std::vector<TYPE>& p, std::vector<FLOAT>& w, std::vector<FLOAT>& knots); \
    void nurbs_convert_bezier_type(std::vector<TYPE>& p, std::vector<FLOAT>& w, int& nu, int& nv, std::vector<FLOAT>& uknots, std::vector<FLOAT>& vknots);

    DEF_BSPLINE(float, float)
    DEF_BSPLINE(vector2f, float)
    DEF_BSPLINE(vector3f, float)
    DEF_BSPLINE(double, double)
    DEF_BSPLINE(vector2d, double)
    DEF_BSPLINE(vector3d, double)

#undef DEF_BSPLINE
}

#endif
