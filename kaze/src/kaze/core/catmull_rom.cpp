#include "catmull_rom.h"

#include <algorithm>

namespace kaze
{
    namespace
    {
        static matrix4 matCatmullRom = 1.0 / 2.0 * matrix4(
                                                       -1, 3, -3, 1,
                                                       2, -5, 4, -1,
                                                       -1, 0, 1, 0,
                                                       0, 2, 0, 0);
        static matrix4 matCatmullRom0 = 1.0 / 2.0 * matrix4(
                                                        0, 0, 0, 0,
                                                        0, 1, -2, 1,
                                                        0, -3, 4, -1,
                                                        0, 2, 0, 0);
        static matrix4 matCatmullRom1 = 1.0 / 2.0 * matrix4(
                                                        0, 0, 0, 0,
                                                        1, -2, 1, 0,
                                                        -1, 0, 1, 0,
                                                        0, 2, 0, 0);

        template <class T>
        struct zero_value
        {
            static inline T value() { return T(0); }
        };

        template <>
        struct zero_value<vector2>
        {
            static inline vector2 value() { return vector2(0, 0); }
        };

        template <>
        struct zero_value<vector3>
        {
            static inline vector3 value() { return vector3(0, 0, 0); }
        };

        template <>
        struct zero_value<vector4>
        {
            static inline vector4 value() { return vector4(0, 0, 0, 0); }
        };

        template <class T>
        static void mul_matrix(T out[4], const matrix4& mat, const T pt[4])
        {
            for (int i = 0; i < 4; i++)
            {
                out[i] = mat[i][0] * pt[0] + mat[i][1] * pt[1] + mat[i][2] * pt[2] + mat[i][3] * pt[3];
            }
        }
    }

    template <class T>
    static T catmull_rom_evaluate_t(const T cp[], int sz, real x)
    {
        if (sz > 2)
        {
            x *= sz - 1;
            int nSpan = std::max<int>(0, std::min<int>(sz - 2, (int)x));
            real t = x - nSpan;
            T P[4];
            T Q[4];
            if (nSpan == 0)
            {
                P[0] = zero_value<T>::value();
                P[1] = cp[nSpan + 0];
                P[2] = cp[nSpan + 1];
                P[3] = cp[nSpan + 2];
                mul_matrix(Q, matCatmullRom0, P);
            }
            else if (nSpan == sz - 2)
            {
                P[0] = cp[nSpan - 1];
                P[1] = cp[nSpan + 0];
                P[2] = cp[nSpan + 1];
                P[3] = zero_value<T>::value();
                mul_matrix(Q, matCatmullRom1, P);
            }
            else
            {
                P[0] = cp[nSpan - 1];
                P[1] = cp[nSpan + 0];
                P[2] = cp[nSpan + 1];
                P[3] = cp[nSpan + 2];
                mul_matrix(Q, matCatmullRom, P);
            }
            return (((Q[0] * t + Q[1]) * t + Q[2]) * t + Q[3]);
        }
        else if (sz == 2)
        {
            return cp[0] * (1 - x) + cp[1] * x;
        }
        else
        {
            return cp[0];
        }
    }

    template <class T>
    static T catmull_rom_evaluate_deriv_t(const T cp[], int sz, real x)
    {
        if (sz > 2)
        {
            x *= sz - 1;
            int nSpan = std::max<int>(0, std::min<int>(sz - 2, (int)x));
            real t = x - nSpan;
            T P[4];
            T Q[4];
            if (nSpan == 0)
            {
                P[0] = zero_value<T>::value();
                P[1] = cp[nSpan + 0];
                P[2] = cp[nSpan + 1];
                P[3] = cp[nSpan + 2];
                mul_matrix(Q, matCatmullRom0, P);
            }
            else if (nSpan == sz - 2)
            {
                P[0] = cp[nSpan - 1];
                P[1] = cp[nSpan + 0];
                P[2] = cp[nSpan + 1];
                P[3] = zero_value<T>::value();
                mul_matrix(Q, matCatmullRom1, P);
            }
            else
            {
                P[0] = cp[nSpan - 1];
                P[1] = cp[nSpan + 0];
                P[2] = cp[nSpan + 1];
                P[3] = cp[nSpan + 2];
                mul_matrix(Q, matCatmullRom, P);
            }
            return (((3.0 * Q[0] * t + 2.0 * Q[1]) * t + Q[2]));
        }
        else if (sz == 2)
        {
            return (cp[1] - cp[0]);
        }
        else
        {
            return zero_value<T>::value();
        }
    }

    template <class T>
    static T catmull_rom_evaluate_close_t(const T cp[], int sz, real x)
    {
        if (sz > 2)
        {
            x *= sz; //
            int nSpan = std::max<int>(0, std::min<int>(sz - 1, (int)x));
            real t = x - nSpan;
            T P[4];
            T Q[4];
            if (nSpan == 0)
            {
                P[0] = cp[sz - 1];
                P[1] = cp[nSpan + 0];
                P[2] = cp[nSpan + 1];
                P[3] = cp[nSpan + 2];
                mul_matrix(Q, matCatmullRom, P);
            }
            else if (nSpan == sz - 2)
            {
                P[0] = cp[nSpan - 1];
                P[1] = cp[nSpan + 0];
                P[2] = cp[nSpan + 1];
                P[3] = cp[0];
                mul_matrix(Q, matCatmullRom, P);
            }
            else if (nSpan == sz - 1)
            {
                P[0] = cp[nSpan - 1];
                P[1] = cp[nSpan + 0];
                P[2] = cp[0];
                P[3] = cp[1];
                mul_matrix(Q, matCatmullRom, P);
            }
            else
            {
                P[0] = cp[nSpan - 1];
                P[1] = cp[nSpan + 0];
                P[2] = cp[nSpan + 1];
                P[3] = cp[nSpan + 2];
                mul_matrix(Q, matCatmullRom, P);
            }
            return (((Q[0] * t + Q[1]) * t + Q[2]) * t + Q[3]);
        }
        else if (sz == 2)
        {
            x *= sz; //
            int nSpan = std::max<int>(0, std::min<int>(sz - 1, (int)x));
            real t = x - nSpan;
            T P[4];
            T Q[4];
            if (nSpan == 0)
            {
                P[0] = cp[1];
                P[1] = cp[0];
                P[2] = cp[1];
                P[3] = cp[0];
            }
            else
            {
                P[0] = cp[0];
                P[1] = cp[1];
                P[2] = cp[0];
                P[3] = cp[1];
            }
            mul_matrix(Q, matCatmullRom, P);
            return (((Q[0] * t + Q[1]) * t + Q[2]) * t + Q[3]);
        }
        else
        {
            return cp[0];
        }
    }

#define DEF_SPLINE(TYPE)                                                                                                 \
    TYPE catmull_rom_evaluate(const TYPE cp[], int sz, real x) { return catmull_rom_evaluate_t(cp, sz, x); }             \
    TYPE catmull_rom_evaluate_deriv(const TYPE cp[], int sz, real x) { return catmull_rom_evaluate_deriv_t(cp, sz, x); } \
    TYPE catmull_rom_evaluate_close(const TYPE cp[], int sz, real x) { return catmull_rom_evaluate_close_t(cp, sz, x); }

    DEF_SPLINE(real)
    DEF_SPLINE(vector2)
    DEF_SPLINE(vector3)
    DEF_SPLINE(vector4)

#undef DEF_SPLINE
}
