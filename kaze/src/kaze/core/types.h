#ifndef KAZE_TYPES_H
#define KAZE_TYPES_H

#include <cstddef>

#include <tempest/vector.hpp>
#include <tempest/vector2.hpp>
#include <tempest/vector3.hpp>
#include <tempest/vector4.hpp>
#include <tempest/matrix.hpp>
#include <tempest/matrix2.hpp>
#include <tempest/matrix3.hpp>
#include <tempest/matrix4.hpp>
#include <tempest/transform.hpp>
#include <tempest/matrix_generator.hpp>
#include <tempest/quaternion.hpp>

//#include <tempest/et_vector.hpp>

//#include <tempest/vector_functions_ex.hpp>
//#include <tempest/matrix_functions_ex.hpp>

#ifdef __GNUC__
#include <stdint.h>
#elif defined(_MSC_VER)
#if _MSC_VER >= 1600
#include <stdint.h>
#else
typedef char int8_t;
typedef short int16_t;
typedef __int32 int32_t;
typedef __int64 int64_t;

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned __int32 uint32_t;
typedef unsigned __int64 uint64_t;
#endif
#endif

namespace kaze
{

    //typedef unsigned short fp16_t;
    typedef float fp32_t;
    typedef double fp64_t;

    //#ifndef size_t
    //typedef std::size_t size_t;
    //#endif

    //typedef float real;
    typedef double real;

#define TM_DEF_VEC(V, T, Sz, P) \
    typedef tempest::V<T, Sz> V##Sz##P;

#define TM_DEF_TYPE(C, V, D) \
    typedef V D;

    TM_DEF_VEC(vector, real, 2, )
    TM_DEF_VEC(vector, real, 3, )
    TM_DEF_VEC(vector, real, 4, )
    TM_DEF_VEC(vector, float, 2, f)
    TM_DEF_VEC(vector, float, 3, f)
    TM_DEF_VEC(vector, float, 4, f)
    TM_DEF_VEC(vector, double, 2, d)
    TM_DEF_VEC(vector, double, 3, d)
    TM_DEF_VEC(vector, double, 4, d)

    TM_DEF_VEC(matrix, real, 2, )
    TM_DEF_VEC(matrix, real, 3, )
    TM_DEF_VEC(matrix, real, 4, )
    TM_DEF_VEC(matrix, float, 2, f)
    TM_DEF_VEC(matrix, float, 3, f)
    TM_DEF_VEC(matrix, float, 4, f)
    TM_DEF_VEC(matrix, double, 2, d)
    TM_DEF_VEC(matrix, double, 3, d)
    TM_DEF_VEC(matrix, double, 4, d)

    TM_DEF_TYPE(struct, tempest::matrix_generator<matrix2>, matrix_generator2)
    TM_DEF_TYPE(struct, tempest::matrix_generator<matrix3>, matrix_generator3)
    TM_DEF_TYPE(struct, tempest::matrix_generator<matrix4>, matrix_generator4)

    typedef matrix_generator2 mat2_gen;
    typedef matrix_generator3 mat3_gen;
    typedef matrix_generator4 mat4_gen;

    TM_DEF_TYPE(class, tempest::quaternion<real>, quaternion)
    TM_DEF_TYPE(class, tempest::quaternion<float>, quaternionf)
    TM_DEF_TYPE(class, tempest::quaternion<double>, quaterniond)

#undef TM_DEF_VEC
#undef TM_DEF_TYPE
}

#endif
