#ifndef KAZE_SVD_H
#define KAZE_SVD_H

#include <vector>

namespace kaze
{
    namespace svd
    {
        class vector
        {
        public:
            explicit vector(int n) : mv_(n) {}
            vector(const vector& rhs) : mv_(rhs.mv_) {}
            vector& operator=(const vector& rhs)
            {
                mv_ = rhs.mv_;
                return *this;
            }
            int size() const { return (int)mv_.size(); }
            double& operator()(int i) { return mv_[i]; }
            double operator()(int i) const { return mv_[i]; }
            double& operator[](int i) { return mv_[i]; }
            double operator[](int i) const { return mv_[i]; }
        private:
            std::vector<double> mv_;
        };

        class matrix
        {
        public:
            matrix(int m, int n) : rows_(m), cols_(n), mv_(m * n) {}
            matrix(const matrix& rhs) : rows_(rhs.rows_), cols_(rhs.cols_), mv_(rhs.mv_) {}
            matrix& operator=(const matrix& rhs)
            {
                rows_ = rhs.rows_;
                cols_ = rhs.cols_;
                mv_ = rhs.mv_;
                return *this;
            }
            int rows() const { return rows_; }
            int cols() const { return cols_; }
            int size() const { return (int)mv_.size(); }
            double& operator()(int i, int j) { return mv_[i * cols_ + j]; }
            double operator()(int i, int j) const { return mv_[i * cols_ + j]; }
            double* operator[](int i) { return &mv_[i * cols_]; }
            const double* operator[](int i) const { return &mv_[i * cols_]; }

            void transpose();

        private:
            int rows_;
            int cols_;
            std::vector<double> mv_;
        };

        matrix operator*(const matrix& a, const matrix& b);

        bool decomp(matrix& A, vector& S, matrix& V);
        bool decomp(const matrix& A, matrix& U, vector& S, matrix& V);
        void backsub(const matrix& U, const vector& S, const matrix& V, const vector& B, vector& X);
    }
}

#endif
