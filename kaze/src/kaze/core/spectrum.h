#ifndef KAZE_SPECTRUM_H
#define KAZE_SPECTRUM_H

#include "types.h"
#include "color.h"

namespace kaze
{

    class spectrum_component
    {
    public:
        virtual ~spectrum_component() {}
        virtual color3 to_rgb() const = 0;
        virtual spectrum_component* clone() const = 0;
        virtual int type() const { return -1; }
        virtual const char* id_string() const = 0;
        virtual bool equal(const spectrum_component* sc) const = 0;
        virtual real intensity() const = 0;
        virtual void add(const spectrum_component* c) = 0;
        virtual void mul(const spectrum_component* c) = 0;
        virtual void mul(real rhs) = 0;
    };

    class spectrum
    {
    public:
        spectrum();
        ~spectrum();

        explicit spectrum(spectrum_component* sc);
        spectrum(const spectrum& rhs);
        spectrum& operator=(const spectrum& rhs);

        spectrum& operator*=(real rhs);

        spectrum& operator+=(const spectrum& rhs);
        spectrum& operator*=(const spectrum& rhs);

        bool equal(const spectrum& rhs) const;
        real intensity() const;

    public:
        const spectrum_component* sc() const { return sc_; }
    public:
        explicit spectrum(const color3& rhs);
        explicit spectrum(real f);
        color3 to_rgb() const;

    private:
        spectrum_component* sc_;
    };

    spectrum operator+(const spectrum& lhs, const spectrum& rhs);
    spectrum operator*(const spectrum& lhs, const spectrum& rhs);

    inline spectrum operator*(const spectrum& lhs, real rhs) { return spectrum(lhs) *= rhs; }
    inline spectrum operator*(real lhs, const spectrum& rhs) { return spectrum(rhs) *= lhs; }

    inline bool operator==(const spectrum& lhs, const spectrum& rhs) { return lhs.equal(rhs); }
    inline bool operator!=(const spectrum& lhs, const spectrum& rhs) { return !lhs.equal(rhs); }

    inline color3 convert(const spectrum& s) { return s.to_rgb(); }
    inline spectrum convert(const color3& c) { return spectrum(c); }
    spectrum convert(const color3& c, const color3& o);

    real coverage(const spectrum& s);
    color3 opacity(const spectrum& s);

    color3 get_Ci(const spectrum& s);
    color3 get_Oi(const spectrum& s);

#define DEF_SPECTRUM(COLOR)                                  \
    \
inline const spectrum& spectrum_##COLOR()                    \
    {                                                        \
        static const spectrum spc(convert(color_##COLOR())); \
        return spc;                                          \
    \
}

    DEF_SPECTRUM(black)
    DEF_SPECTRUM(white)
    DEF_SPECTRUM(red)
    DEF_SPECTRUM(green)
    DEF_SPECTRUM(blue)
    DEF_SPECTRUM(cyan)
    DEF_SPECTRUM(mazenta)
    DEF_SPECTRUM(yellow)

#undef DEF_SPECTRUM
}

#endif
