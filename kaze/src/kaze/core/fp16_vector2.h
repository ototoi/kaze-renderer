#ifndef KAZE_FP16_VECTOR2_H
#define KAZE_FP16_VECTOR2_H

#include "fp16.h"
#include "types.h"

namespace kaze
{

    class fp16_vector2
    {
    public:
        fp16_vector2() {}
        fp16_vector2(const fp16_vector2& rhs)
        {
            mv_[0] = rhs.mv_[0];
            mv_[1] = rhs.mv_[1];
        }

        explicit fp16_vector2(const vector2& rhs)
        {
            mv_[0] = f32to16((float)rhs[0]);
            mv_[1] = f32to16((float)rhs[1]);
        }

#define DEF_CONS(TYPE)                       \
    fp16_vector2(TYPE a, TYPE b)             \
    {                                        \
        mv_[0] = f32to16((float)a);          \
        mv_[1] = f32to16((float)b);          \
    }                                        \
    explicit fp16_vector2(const TYPE rhs[2]) \
    {                                        \
        mv_[0] = f32to16((float)rhs[0]);     \
        mv_[1] = f32to16((float)rhs[1]);     \
    }

        DEF_CONS(float)
        DEF_CONS(double)
        DEF_CONS(long double)

#undef DEF_CONS

        fp16_vector2& operator=(const fp16_vector2& rhs)
        {
            mv_[0] = rhs.mv_[0];
            mv_[1] = rhs.mv_[1];
            return *this;
        }

        fp16_vector2& operator=(const vector2& rhs)
        {
            mv_[0] = f32to16((float)rhs[0]);
            mv_[1] = f32to16((float)rhs[1]);
            return *this;
        }

        fp16_vector2& negate()
        {
            mv_[0] = f16negate(mv_[0]);
            mv_[1] = f16negate(mv_[1]);

            return *this;
        }

        operator vector2() const
        {
            return vector2((real)f16to32(mv_[0]), (real)f16to32(mv_[1]));
        }

        size_t size() const { return 2; }
        float operator[](std::size_t idx) const { return f16to32(mv_[idx]); }

    private:
        fp16_t mv_[2];
    };

    inline fp16_vector2 operator-(const fp16_vector2& rhs)
    {
        return fp16_vector2(rhs).negate();
    }
}

#endif