#ifndef KAZE_BASIC_COLLECTION_HPP
#define KAZE_BASIC_COLLECTION_HPP

#include "aggregator.hpp"
#include "enumerator.hpp"

#include <vector>

namespace kaze
{

    template <class T, class C = std::vector<T> >
    class basic_collection : public aggregator<T>, public enumerator<T>
    {
    public:
        typedef T value_type;
        typedef basic_collection<T> this_type;
        typedef typename C::iterator iterator;
        typedef typename C::const_iterator const_iterator;

    public:
        //void add(const base_value_type& rhs);
        virtual inline void add(const T& rhs) { mv_.push_back(rhs); }
    public:
        basic_collection() {}
        basic_collection(std::size_t sz) { mv_.reserve(sz); }
        explicit basic_collection(const basic_collection& rhs) : mv_(rhs.mv_) {}
        basic_collection& operator=(const basic_collection& rhs)
        {
            mv_ = rhs.mv_;
            return *this;
        }

        iterator begin() { return mv_.begin(); }
        iterator end() { return mv_.end(); }
        const_iterator begin() const { return mv_.begin(); }
        const_iterator end() const { return mv_.end(); }

        void clear() { mv_.clear(); }

        bool empty() const { return mv_.empty(); }
        std::size_t size() const { return mv_.size(); }

        T& operator[](std::size_t i) { return mv_[i]; }
        const T& operator[](std::size_t i) const { return mv_[i]; }

        T& at(std::size_t i)
        {
            if (!(i < mv_.size()))
            {
                throw std::out_of_range("basic_collection");
            }
            return mv_[i];
        }
        const T& at(std::size_t i) const
        {
            if (!(i < mv_.size()))
            {
                throw std::out_of_range("basic_collection");
            }
            return mv_[i];
        }

        void swap(basic_collection& rhs) { mv_.swap(rhs.mv_); }

    private:
        C mv_;
    };
}

#endif