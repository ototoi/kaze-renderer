#include "bspline.h"

#include <vector>
#include <algorithm>

#include <fstream>

namespace kaze
{

    template <class FLOAT>
    static FLOAT clamp(FLOAT x, FLOAT a, FLOAT b)
    {
        return std::max<FLOAT>(a, std::min<FLOAT>(x, b));
    }

    static const double BASIS[4][4] = {
        {-1.0 / 6.0, 3.0 / 6.0, -3.0 / 6.0, 1.0 / 6.0},
        {3.0 / 6.0, -6.0 / 6.0, 3.0 / 6.0, 0.0 / 6.0},
        {-3.0 / 6.0, 0.0 / 6.0, 3.0 / 6.0, 0.0 / 6.0},
        {1.0 / 6.0, 4.0 / 6.0, 1.0 / 6.0, 0.0 / 6.0}};

    template <class T, class FLOAT>
    static T bspline4_evaluate_t(const T cp[], int n, FLOAT x)
    {
        //knot_sz  = cpsz(n)+order(4)
        //nspans = ksz-7
        //
        int nspans = n - 3; //
        assert(nspans >= 1);

        x = clamp(x, (FLOAT)0, (FLOAT)1) * nspans;
        int span = (int)x;
        if (span >= nspans)
            span = nspans;

        x -= span;
        cp += span;

        T tmp[4];
        for (int i = 0; i < 4; i++)
        {
            tmp[i] = (FLOAT)BASIS[i][0] * cp[0] + (FLOAT)BASIS[i][1] * cp[1] + (FLOAT)BASIS[i][2] * cp[2] + (FLOAT)BASIS[i][3] * cp[3];
        }

        return (((tmp[0] * x + tmp[1]) * x + tmp[2]) * x + tmp[3]);
    }

#define DEC_BSPLINE(TYPE, FLOAT)                           \
    TYPE bspline4_evaluate(const TYPE p[], int n, FLOAT t) \
    {                                                      \
        return bspline4_evaluate_t(p, n, t);               \
    }

    DEC_BSPLINE(float, float)
    DEC_BSPLINE(vector2f, float)
    DEC_BSPLINE(vector3f, float)
    DEC_BSPLINE(vector4f, float)

    DEC_BSPLINE(double, double)
    DEC_BSPLINE(vector2d, double)
    DEC_BSPLINE(vector3d, double)
    DEC_BSPLINE(vector4d, double)

#undef DEC_BSPLINE

    template <class FLOAT>
    static void bspline_create_uniform_knots_(FLOAT knots[], int k, int nOrder, bool bOpen0, bool bOpen1)
    {
        int nType = 0;
        if (bOpen0)
            nType |= 1;
        if (bOpen1)
            nType |= 2;

        switch (nType)
        {
        case 0:
        {
            for (int i = 0; i < k; i++)
            {
                knots[i] = (FLOAT)i;
            }
        }
        break;
        case 1:
        {
            for (int i = 0; i < nOrder; i++)
            {
                knots[i] = (FLOAT)0;
            }
            int p = 1;
            for (int i = nOrder; i < k; i++)
            {
                knots[i] = (FLOAT)p;
                p++;
            }
        }
        break;
        case 2:
        {
            int p = 0;
            for (int i = 0; i < k - nOrder; i++)
            {
                knots[i] = (FLOAT)p;
                p++;
            }
            for (int i = k - nOrder; i < k; i++)
            {
                knots[i] = (FLOAT)p;
            }
        }
        break;
        case 3:
        {
            for (int i = 0; i < nOrder; i++)
            {
                knots[i] = (FLOAT)0;
            }
            int p = 1;
            for (int i = nOrder; i < k - nOrder; i++)
            {
                knots[i] = (FLOAT)p;
                p++;
            }
            for (int i = k - nOrder; i < k; i++)
            {
                knots[i] = (FLOAT)p;
            }
        }
        break;
        }
    }

    template <class FLOAT>
    static bool bspline_is_open0_(const FLOAT knots[], int k, int m)
    {
        int nOrder = m;
        int nKnots = k;
        if (nKnots >= nOrder)
        {
            for (int i = 1; i < nOrder; i++)
            {
                if (knots[i] != knots[0])
                    return false;
            }
            return true;
        }
        return false;
    }

    template <class FLOAT>
    static bool bspline_is_open1_(const FLOAT knots[], int k, int m)
    {
        int nOrder = m;
        int nKnots = k;
        if (nKnots >= nOrder)
        {
            for (int i = 1; i < nOrder; i++)
            {
                if (knots[nKnots - 1 - i] != knots[nKnots - 1])
                    return false;
            }
            return true;
        }
        return false;
    }

    template <class FLOAT>
    static bool is_equal(FLOAT a, FLOAT b)
    {
        return fabs(a - b) <= std::numeric_limits<FLOAT>::epsilon() * 2;
    }

    template <class FLOAT>
    static bool bspline_is_uniform_(const FLOAT knots[], int k)
    {
        int nKnots = k;
        if (nKnots < 2)
            return false;
        FLOAT val = knots[1] - knots[0];
        for (int i = 1; i < nKnots - 1; i++)
        {
            if (!is_equal(val, knots[i + 1] - knots[i + 0]))
                return false;
        }
        return true;
    }

    template <class FLOAT>
    static bool is_open(const FLOAT knots[], int k, int m)
    {
        return bspline_is_open0(knots, k, m) && bspline_is_open1(knots, k, m);
    }

    template <class FLOAT>
    static bool bspline_is_bezier_type_(const FLOAT knots[], int k, int m)
    {
        if (is_open(knots, k, m))
        {
            int nOrder = m;
            int nDegree = m - 1;
            int nKnots = k;
            if (nKnots > 2 * nOrder)
            {
                int nRem = nKnots - 2 * nOrder;
                if ((nRem % nDegree) != 0)
                    return false;
                int nSpan = nRem / nDegree;
                for (int i = 0; i < nSpan; i++)
                {
                    int idx = nOrder + i * nDegree;
                    for (int j = 1; j < nDegree; j++)
                    {
                        if (knots[idx] != knots[idx + j])
                            return false;
                    }
                }
            }
            return true;
        }
        return false;
    }

#define DEC_BSPLINE_IS(FLOAT)                                                                     \
    void bspline_create_uniform_knots(FLOAT knots[], int k, int nOrder, bool bOpen0, bool bOpen1) \
    {                                                                                             \
        bspline_create_uniform_knots_(knots, k, nOrder, bOpen0, bOpen1);                          \
    }                                                                                             \
    bool bspline_is_open0(const FLOAT knots[], int k, int m)                                      \
    {                                                                                             \
        return bspline_is_open0_(knots, k, m);                                                    \
    }                                                                                             \
    bool bspline_is_open1(const FLOAT knots[], int k, int m)                                      \
    {                                                                                             \
        return bspline_is_open1_(knots, k, m);                                                    \
    }                                                                                             \
    bool bspline_is_open(const FLOAT knots[], int k, int m)                                       \
    {                                                                                             \
        return is_open(knots, k, m);                                                              \
    }                                                                                             \
    bool bspline_is_bezier_type(const FLOAT knots[], int k, int m)                                \
    {                                                                                             \
        return bspline_is_bezier_type_(knots, k, m);                                              \
    }

    DEC_BSPLINE_IS(float)
    DEC_BSPLINE_IS(double)

#undef DEC_BSPLINE_IS

    static int get_tree_size(int n)
    {                                      //n is depth
        return 1 + ((n * n + n - 2) >> 1); //1,3,6,10
    }

    template <class FLOAT>
    static int find_span(const FLOAT knots[], int k, FLOAT t)
    {
        if (k <= 2)
            return 0;
        if (t < knots[0])
            return 0;
        if (knots[k - 1] <= t)
        {
            for (int i = 1; i < k; i++)
            {
                if (knots[k - 1] <= knots[i])
                    return i - 1;
            }
        }
        else
        {
            for (int i = 1; i < k; i++)
            {
                if (t < knots[i])
                    return i - 1;
            }
        }
        return k - 2;
    }

    template <class FLOAT>
    static bool is_zero(FLOAT x)
    {
        static const FLOAT EPS = std::numeric_limits<FLOAT>::min();
        return (fabs(x) <= EPS);
    }

    template <class FLOAT>
    static FLOAT div(FLOAT x, FLOAT y)
    {
        if (is_zero(fabs(x - y)))
            return 1;
        //if(is_zero(x))return 0;
        if (is_zero(y))
            return 0;
        return x / y;
    }

    template <class T, class FLOAT>
    static inline T mul_coef(const FLOAT e[], const T v[], int n)
    {
        T tmp = e[0] * v[0];
        for (int i = 1; i < n; i++)
            tmp += e[i] * v[i];
        return tmp;
    }

    template <class FLOAT>
    static void make_bezier_type_knots(std::vector<FLOAT>& out, const std::vector<FLOAT>& in, int m)
    {
        size_t isz = in.size();
        std::vector<FLOAT> tknots;
        for (int i = m - 1; i < (int)isz - m + 1; i++)
        {
            tknots.push_back(in[i]);
        }
        if (!tknots.empty())
        {
            tknots.erase(std::unique(tknots.begin(), tknots.end()), tknots.end());
        }
        out.clear();
        int tsz = (int)tknots.size();
        int osz = (m - 1) * 2 + (tsz - 2) * (m - 1);
        out.reserve(osz);
        for (int i = 0; i < m; i++)
        {
            out.push_back(tknots[0]);
        }
        for (int i = 1; i < tsz - 1; i++)
        {
            for (int j = 0; j < m - 1; j++)
            {
                out.push_back(tknots[i]);
            }
        }
        for (int i = 0; i < m; i++)
        {
            out.push_back(tknots[tsz - 1]);
        }
    }

    template <class FLOAT>
    static FLOAT insert_coef(int i, int j, int k, const FLOAT x[], const FLOAT y[])
    {
        if (k <= 1)
        {
            if (x[i] <= y[j] && y[j] < x[i + 1])
                return 1;
            else
                return 0;
        }
        else
        {
            FLOAT A = div(y[j + k - 1] - x[i], x[i + k - 1] - x[i]);
            if (!is_zero(A))
            {
                A *= insert_coef(i, j, k - 1, x, y);
            }
            FLOAT B = div(x[i + k] - y[j + k - 1], x[i + k] - x[i + 1]);
            if (!is_zero(B))
            {
                B *= insert_coef(i + 1, j, k - 1, x, y);
            }
            return A + B;
        }
    }

    template <class FLOAT>
    static void make_new_params(FLOAT params[], int p, const FLOAT x[], const FLOAT y[], int j, int k)
    {
        for (int i = 0; i < p; i++)
        {
            params[i] = insert_coef(i, j, k, x, y);
        }
    }

    template <class FLOAT>
    static int find_span_raw(const FLOAT knots[], int k, FLOAT t)
    {
        if (k <= 2)
            return 0;

        for (int i = 1; i < k; i++)
        {
            if (t < knots[i])
                return i - 1;
        }

        return k - 2;
    }

    template <class FLOAT>
    static int make_new_params_offset(FLOAT params[], int nOrder, const FLOAT x[], int xsz, const FLOAT y[], int ysz, int j)
    {
        (void)ysz;

        FLOAT t = y[j];
        int msz = nOrder;
        if (x[xsz - 1] <= t)
        {
            memset(params, 0, sizeof(FLOAT) * msz);
            params[msz - 1] = 1;
            return xsz - 2 * msz;
        }
        else
        {
            int nDegree = nOrder - 1;
            int nSpan = find_span(x, xsz, t);

            int tsz = get_tree_size(nOrder);
            std::vector<FLOAT> tmp(tsz);

            tmp[0] = 1;
            int idx = 1;
            for (int m = 1; m < msz; m++)
            {
                for (int p = 0; p <= m; p++)
                {
                    int i = nSpan - m + p;
                    if (p == 0)
                    {
                        int bidx = idx - m;
                        tmp[idx] = div(x[i + m + 1] - y[j + m], x[i + m + 1] - x[i + 1]) * tmp[bidx];
                    }
                    else if (p == m)
                    {
                        int aidx = idx - m - 1;
                        tmp[idx] = div(y[j + m] - x[i], x[i + m] - x[i]) * tmp[aidx];
                    }
                    else
                    {
                        int aidx = idx - m - 1;
                        int bidx = idx - m;
                        tmp[idx] = div(y[j + m] - x[i], x[i + m] - x[i]) * tmp[aidx] + div(x[i + m + 1] - y[j + m], x[i + m + 1] - x[i + 1]) * tmp[bidx];
                    }
                    idx++;
                }
            }
            memcpy(params, &tmp[tsz - msz], sizeof(FLOAT) * msz);
            return nSpan - nDegree;
        }
    }

    template <class T, class FLOAT>
    void make_bezier_type(
        std::vector<T>& Q, std::vector<FLOAT>& Mknots,
        const std::vector<T>& P, const std::vector<FLOAT>& Nknots)
    {
        int n = (int)Nknots.size();
        int p = (int)P.size();
        int k = n - p; //order
        make_bezier_type_knots(Mknots, Nknots, k);
        int m = (int)Mknots.size();
        int q = m - k;
        Q.resize(q);
#if 0
        std::vector<FLOAT> params(p);
		for(int j = 0;j<q;j++){
			make_new_params(&params[0], p, &Nknots[0], &Mknots[0], j, k);
            Q[j] = mul_coef(&params[0], &P[0], p);
#else
        std::vector<FLOAT> params(k);
        for (int j = 0; j < q; j++)
        {
            //std::vector<FLOAT> params1(p);
            //make_new_params(&params1[0], p, &Nknots[0], &Mknots[0], j, k);

            int offset = make_new_params_offset(
                &params[0], k,
                &Nknots[0], (int)Nknots.size(),
                &Mknots[0], (int)Mknots.size(),
                j);
            if (offset == 0)
            {
                Q[j] = mul_coef(&params[0], &P[0], k);
            }
            else if (offset < 0)
            {
                Q[j] = mul_coef(&params[-offset], &P[0], k + offset);
            }
            else if (offset < p)
            {
                Q[j] = mul_coef(&params[0], &P[offset], std::min<int>(k, p - offset)); //k-2-m+1->n-1
            }
#endif
        }
    }

    template <class T, class FLOAT>
    void insert_knot(
        std::vector<T>& Q, std::vector<FLOAT>& Qw, const std::vector<FLOAT>& Mknots,
        const std::vector<T>& P, std::vector<FLOAT>& Pw, const std::vector<FLOAT>& Nknots)
    {
        int n = (int)Nknots.size();
        int p = (int)P.size();
        int k = n - p; //order
        //make_bezier_type_knots(Mknots,Nknots,k);
        int m = (int)Mknots.size();
        int q = m - k;
        Q.resize(q);
        Qw.resize(q);
    #if 0
            std::vector<FLOAT> params(p);
            for(int j = 0;j<q;j++){
                make_new_params(&params[0], p, &Nknots[0], &Mknots[0], j, k);
                Q[j] = mul_coef(&params[0], &P[0], p);
                Qw[j] = mul_coef(&params[0], &Pw[0], p);
    #else
        std::vector<FLOAT> params(k);
        for (int j = 0; j < q; j++)
        {
            //std::vector<FLOAT> params1(p);
            //make_new_params(&params1[0], p, &Nknots[0], &Mknots[0], j, k);

            int offset = make_new_params_offset(
                &params[0], k,
                &Nknots[0], (int)Nknots.size(),
                &Mknots[0], (int)Mknots.size(),
                j);
            if (offset == 0)
            {
                Q[j] = mul_coef(&params[0], &P[0], k);
                Qw[j] = mul_coef(&params[0], &Pw[0], k);
            }
            else if (offset < 0)
            {
                Q[j] = mul_coef(&params[-offset], &P[0], k + offset);
                Qw[j] = mul_coef(&params[-offset], &Pw[0], k + offset);
            }
            else if (offset < p)
            {
                Q[j] = mul_coef(&params[0], &P[offset], std::min<int>(k, p - offset)); //k-2-m+1->n-1
                Qw[j] = mul_coef(&params[0], &Pw[offset], std::min<int>(k, p - offset));
            }
    #endif
        }
    }

    template <class T, class FLOAT>
    void make_bezier_type(
        std::vector<T>& Q, std::vector<FLOAT>& Qw, std::vector<FLOAT>& Mknots,
        const std::vector<T>& P, std::vector<FLOAT>& Pw, const std::vector<FLOAT>& Nknots)
    {
        int n = (int)Nknots.size();
        int p = (int)P.size();
        int k = n - p; //order
        make_bezier_type_knots(Mknots, Nknots, k);
        insert_knot(Q, Qw, Mknots, P, Pw, Nknots);
    }

    template <class T, class FLOAT>
    void bspline_convert_bezier_type_(std::vector<T>& points, std::vector<FLOAT>& knots)
    {
        int nKnots = (int)knots.size();
        int nPoints = (int)points.size();
        int nOrder = nKnots - nPoints;
        if (!bspline_is_bezier_type(&knots[0], nKnots, nOrder))
        {
            std::vector<T> tpoints;
            std::vector<FLOAT> tknots;
            make_bezier_type(tpoints, tknots, points, knots);
            points.swap(tpoints);
            knots.swap(tknots);
        }
    }

    template <class T, class FLOAT>
    void bspline_convert_bezier_type_(std::vector<T>& points, std::vector<FLOAT>& weights, std::vector<FLOAT>& knots)
    {
        int nKnots = (int)knots.size();
        int nPoints = (int)points.size();
        int nOrder = nKnots - nPoints;
        if (!bspline_is_bezier_type(&knots[0], nKnots, nOrder))
        {
            std::vector<T> tpoints;
            std::vector<FLOAT> tweights;
            std::vector<FLOAT> tknots;
            make_bezier_type(tpoints, tweights, tknots, points, weights, knots);
            points.swap(tpoints);
            weights.swap(tweights);
            knots.swap(tknots);
        }
    }

    template <class T>
    std::vector<T> get_row(const std::vector<T>& points, int j, int nu, int nv)
    {
        assert(size_t(nu*nv)<=points.size());

        std::vector<T> tmp(nu);
        for (int i = 0; i < nu; i++)
        {
            tmp[i] = points[j * nu + i];
        }
        return tmp;
    }

    template <class T>
    std::vector<T> get_col(const std::vector<T>& points, int i, int nu, int nv)
    {
        std::vector<T> tmp(nv);
        for (int j = 0; j < nv; j++)
        {
            tmp[j] = points[j * nu + i];
        }
        return tmp;
    }

    template <class T, class FLOAT>
    int bspline_convert_bezier_type_u_(std::vector<T>& points, int nu, int nv, std::vector<FLOAT>& uknots, const std::vector<FLOAT>& vknots)
    {
        int nKnots_u = (int)uknots.size();
        int nOrder_u = nKnots_u - nu;

        if (!bspline_is_bezier_type(&uknots[0], nKnots_u, nOrder_u))
        {
            std::vector<std::vector<T> > rows(nv);

            std::vector<FLOAT> tk = uknots;
            for (int j = 0; j < nv; j++)
            {
                std::vector<FLOAT> tknots;
                make_bezier_type(rows[j], tknots, get_row(points, j, nu, nv), tk);
                uknots.swap(tknots);
            }
            int nnu = (int)rows[0].size();
            std::vector<T> tmp(nnu * nv);
            for (int j = 0; j < nv; j++)
            {
                assert(nnu == (int)rows[j].size());

                for (int i = 0; i < nnu; i++)
                {
                    tmp[j * nnu + i] = rows[j][i];
                }
            }
            points.swap(tmp);
            return nnu;
        }
        return nu;
    }

    template <class T, class FLOAT>
    int bspline_convert_bezier_type_v_(std::vector<T>& points, int nu, int nv, const std::vector<FLOAT>& uknots, std::vector<FLOAT>& vknots)
    {
        int nKnots_v = (int)vknots.size();
        int nOrder_v = nKnots_v - nv;

        if (!bspline_is_bezier_type(&vknots[0], nKnots_v, nOrder_v))
        {
            std::vector<std::vector<T> > cols(nu);

            std::vector<FLOAT> tk = vknots;
            for (int i = 0; i < nu; i++)
            {
                std::vector<FLOAT> tknots;
                std::vector<T> col = get_col(points, i, nu, nv);
                make_bezier_type(cols[i], tknots, col, tk);
                vknots.swap(tknots);
            }
            int nnv = (int)cols[0].size();
            std::vector<T> tmp(nu * nnv);
            for (int i = 0; i < nu; i++)
            {
                assert(nnv == (int)cols[i].size());

                for (int j = 0; j < nnv; j++)
                {
                    tmp[j * nu + i] = cols[i][j];
                }
            }
            points.swap(tmp);
            return nnv;
        }
        return nv;
    }

    template <class T, class FLOAT>
    void bspline_convert_bezier_type_(std::vector<T>& points, int& nu, int& nv, std::vector<FLOAT>& uknots, std::vector<FLOAT>& vknots)
    {
        /*
        int nKnots_u = (int)uknots.size();
        int nOrder_u = nKnots_u - nu;

        int nKnots_v = (int)vknots.size();
        int nOrder_v = nKnots_v - nv;
        */
        nu = bspline_convert_bezier_type_u_(points, nu, nv, uknots, vknots);
        nv = bspline_convert_bezier_type_v_(points, nu, nv, uknots, vknots);
    }

    template <class FLOAT>
    FLOAT de_boor_cox_(const FLOAT x[], int i, int k, FLOAT t)
    {
        if (k <= 1)
        {
            if (x[i] <= t && t < x[i + 1])
                return 1;
            else
                return 0;
        }
        else
        {
            FLOAT A = div(t - x[i], x[i + k - 1] - x[i]);
            FLOAT B = div(x[i + k] - t, x[i + k] - x[i + 1]);
            if (!is_zero(A))
            {
                A *= de_boor_cox_(x, i, k - 1, t);
            }
            else
            {
                A = 0;
            }
            if (!is_zero(B))
            {
                B *= de_boor_cox_(x, i + 1, k - 1, t);
            }
            else
            {
                B = 0;
            }
            return A + B;
        }
    }

    template <class FLOAT>
    FLOAT de_boor_cox_deriv_(const FLOAT x[], int i, int k, FLOAT t)
    {
        if (k <= 1)
        {
            return 0;
        }
        else
        {
            FLOAT A = div((FLOAT)1, x[i + k - 1] - x[i]);
            FLOAT B = div((FLOAT)1, x[i + k] - x[i + 1]);
            if (!is_zero(A))
            {
                A *= (de_boor_cox_(x, i, k - 1, t) + (t - x[i]) * de_boor_cox_deriv_(x, i, k - 1, t));
            }
            else
            {
                A = 0;
            }
            if (!is_zero(B))
            {
                B *= (-de_boor_cox_(x, i + 1, k - 1, t) + (x[i + k] - t) * de_boor_cox_deriv_(x, i + 1, k - 1, t));
            }
            else
            {
                B = 0;
            }
            return A + B;
        }
    }

    template <class FLOAT>
    int de_boor_cox_offset_(FLOAT params[], int msz, const FLOAT knots[], int ksz, FLOAT t)
    {
        int nOrder = msz;
        int nDegree = msz - 1;

        int nSpan = find_span(knots, ksz, t);
        int tsz = get_tree_size(msz);
        std::vector<FLOAT> tmp(tsz);

        tmp[0] = 1;
        int idx = 1;
        for (int m = 1; m < msz; m++)
        {
            for (int p = 0; p <= m; p++)
            {
                int i = nSpan - m + p;
                if (p == 0)
                {
                    int bidx = idx - m;
                    tmp[idx] = div((knots[i + m + 1] - t), (knots[i + m + 1] - knots[i + 1])) * tmp[bidx];
                }
                else if (p == m)
                {
                    int aidx = idx - m - 1;
                    tmp[idx] = div((t - knots[i]), (knots[i + m] - knots[i])) * tmp[aidx];
                }
                else
                {
                    int aidx = idx - m - 1;
                    int bidx = idx - m;
                    tmp[idx] = div((t - knots[i]), (knots[i + m] - knots[i])) * tmp[aidx] + div((knots[i + m + 1] - t), (knots[i + m + 1] - knots[i + 1])) * tmp[bidx];
                }
                idx++;
            }
        }
        memcpy(params, &tmp[tsz - msz], sizeof(FLOAT) * msz);
        return nSpan - nDegree;
    }

    template <class FLOAT>
    int de_boor_cox_deriv_offset_(FLOAT params[], int msz, const FLOAT knots[], int ksz, FLOAT t)
    {
        int nOrder = msz;
        int nDegree = msz - 1;
        {
            int nSpan = find_span(knots, ksz, t);
            int tsz = get_tree_size(msz - 1);
            std::vector<FLOAT> tmp(tsz);
            { //---------------------
                tmp[0] = 1;
                int idx = 1;
                for (int m = 1; m < msz - 1; m++)
                {
                    for (int p = 0; p <= m; p++)
                    {
                        int i = nSpan - m + p;
                        if (p == 0)
                        {
                            int bidx = idx - m;
                            tmp[idx] = div((knots[i + m + 1] - t), (knots[i + m + 1] - knots[i + 1])) * tmp[bidx];
                        }
                        else if (p == m)
                        {
                            int aidx = idx - m - 1;
                            tmp[idx] = div((t - knots[i]), (knots[i + m] - knots[i])) * tmp[aidx];
                        }
                        else
                        {
                            int aidx = idx - m - 1;
                            int bidx = idx - m;
                            tmp[idx] = div((t - knots[i]), (knots[i + m] - knots[i])) * tmp[aidx] + div((knots[i + m + 1] - t), (knots[i + m + 1] - knots[i + 1])) * tmp[bidx];
                        }
                        idx++;
                    }
                }
            } //---------------------
            int jsz = get_tree_size(msz);
            std::vector<FLOAT> jmp(jsz);
            {
                jmp[0] = 0;
                int idx = 1;
                for (int m = 1; m < msz; m++)
                {
                    for (int p = 0; p <= m; p++)
                    {
                        int i = nSpan - m + p;
                        if (p == 0)
                        {
                            int bidx = idx - m;
                            jmp[idx] = div(
                                -tmp[bidx] + (knots[i + m + 1] - t) * jmp[bidx],
                                (knots[i + m + 1] - knots[i + 1]));
                        }
                        else if (p == m)
                        {
                            int aidx = idx - m - 1;
                            jmp[idx] = div(
                                tmp[aidx] + (t - knots[i]) * jmp[aidx],
                                (knots[i + m] - knots[i]));
                        }
                        else
                        {
                            int aidx = idx - m - 1;
                            int bidx = idx - m;
                            jmp[idx] = div(
                                        tmp[aidx] + (t - knots[i]) * jmp[aidx],
                                        (knots[i + m] - knots[i])) +
                                    div(
                                        -tmp[bidx] + (knots[i + m + 1] - t) * jmp[bidx],
                                        (knots[i + m + 1] - knots[i + 1]));
                        }
                        idx++;
                    }
                }
            }

            memcpy(params, &jmp[jsz - msz], sizeof(FLOAT) * msz);
            return nSpan - nDegree;
        }
    }

    template <class FLOAT>
    void de_boor_cox_(FLOAT params[], int p, const FLOAT knots[], int k, FLOAT t)
    {
    #if 0
            int m = k-p;
            for(int i = 0;i<p;i++){
                if(i == p-1){
                    if(knots[k-1]<=t){
                        params[i] = 1;
                    }else{
                        params[i] = de_boor_cox(knots,i,m,t);
                    }
                }else{
                    params[i] = de_boor_cox(knots,i,m,t);
                }
            }
    #else
        int m = k - p;
        std::vector<FLOAT> tmp(p);
        int offset = de_boor_cox_offset_(&tmp[0], m, knots, k, t);
        memset(params, 0, sizeof(FLOAT) * p);

        if (offset == 0)
        {
            memcpy(params, &tmp[0], sizeof(FLOAT) * m);
        }
        else if (offset < 0)
        {
            memcpy(params, &tmp[-offset], sizeof(FLOAT) * (m + offset));
        }
        else
        {
            memcpy(params + offset, &tmp[0], sizeof(FLOAT) * (std::min<int>(m, p - offset)));
        }
    #endif
    }

#define DEC_DE_BOOR_COX(FLOAT)                                                               \
    FLOAT de_boor_cox(const FLOAT knots[], int i, int m, FLOAT t)                            \
    {                                                                                        \
        return de_boor_cox_(knots, i, m, t);                                                 \
    }                                                                                        \
    FLOAT de_boor_cox_deriv(const FLOAT knots[], int i, int m, FLOAT t)                      \
    {                                                                                        \
        return de_boor_cox_deriv_(knots, i, m, t);                                           \
    }                                                                                        \
    int de_boor_cox_offset(FLOAT params[], int m, const FLOAT knots[], int k, FLOAT t)       \
    {                                                                                        \
        return de_boor_cox_offset_(params, m, knots, k, t);                                  \
    }                                                                                        \
    int de_boor_cox_deriv_offset(FLOAT params[], int m, const FLOAT knots[], int k, FLOAT t) \
    {                                                                                        \
        return de_boor_cox_deriv_offset_(params, m, knots, k, t);                            \
    }                                                                                        \
    void de_boor_cox(FLOAT params[], int p, const FLOAT knots[], int k, FLOAT t)             \
    {                                                                                        \
        de_boor_cox_(params, p, knots, k, t);                                                \
    }

DEC_DE_BOOR_COX(float)
DEC_DE_BOOR_COX(double)

#undef DEC_DE_BOOR_COX

    template <class T, class FLOAT>
    T bspline_evaluate_n(const T p[], int n, const FLOAT knots[], int k, FLOAT t)
    {
        int m = k - n;
        int d = m - 1;
        assert(m >= 1);
        t = (1 - t) * knots[d] + t * knots[k - 1 - d];
    #if 1
        std::vector<FLOAT> params(m);
        int offset = de_boor_cox_offset(&params[0], m, knots, k, t);
        if (offset == 0)
        {
            return mul_coef(&params[0], p, m);
        }
        else if (offset < 0)
        {
            return mul_coef(&params[-offset], p, m + offset);
        }
        else
        {
            return mul_coef(&params[0], p + offset, std::min<int>(m, n - offset)); //k-2-m+1->n-1
        }
    #else
        std::vector<FLOAT> params(n);
        for (int i = 0; i < p; i++)
        {
            if (i == p - 1)
            {
                if (knots[k - 1] <= t)
                {
                    params[i] = 1;
                }
                else
                {
                    params[i] = de_boor_cox(knots, i, m, t);
                }
            }
            else
            {
                params[i] = de_boor_cox(knots, i, m, t);
            }
        }
        return mul_coef(&params[0], p, n);
    #endif
    }

    template <class T, class FLOAT>
    T bspline_evaluate_n(const T p[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v)
    {
        std::vector<T> pv(nv);
        for (int j = 0; j < nv; j++)
        {
            pv[j] = bspline_evaluate_n(p + j * nu, nu, uknots, ku, u);
        }
        return bspline_evaluate_n(&pv[0], nv, vknots, kv, v);
    }

    template <class T, class FLOAT>
    T bspline_evaluate_deriv_n(const T p[], int n, const FLOAT knots[], int k, FLOAT t)
    {
        int m = k - n;
        int d = m - 1;
        assert(m >= 1);
        t = (1 - t) * knots[d] + t * knots[k - 1 - d];
    #if 1
        std::vector<FLOAT> params(m);
        int offset = de_boor_cox_deriv_offset(&params[0], m, knots, k, t);
        if (offset == 0)
        {
            return mul_coef(&params[0], p, m);
        }
        else if (offset < 0)
        {
            return mul_coef(&params[-offset], p, m + offset);
        }
        else
        {
            return mul_coef(&params[0], p + offset, std::min<int>(m, n - offset)); //k-2-m+1->n-1
        }
    #else
        std::vector<FLOAT> params(n);
        for (int i = 0; i < p; i++)
        {
            if (i == p - 1)
            {
                if (knots[k - 1] <= t)
                {
                    params[i] = 1;
                }
                else
                {
                    params[i] = de_boor_cox_deriv(knots, i, m, t);
                }
            }
            else
            {
                params[i] = de_boor_cox_deriv(knots, i, m, t);
            }
        }
        return mul_coef(&params[0], p, n);
    #endif
    }

    template <class T, class FLOAT>
    T bspline_evaluate_deriv_u_n(const T p[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v)
    {
        std::vector<T> pu(nu);
        std::vector<T> pv(nv);
        for (int i = 0; i < nu; i++)
        {
            for (int j = 0; j < nv; j++)
            {
                pv[j] = p[j * nu + i];
            }
            pu[i] = bspline_evaluate_n(&pv[0], nv, vknots, kv, v);
        }
        return bspline_evaluate_deriv_n(&pu[0], nu, uknots, ku, u);
    }

    template <class T, class FLOAT>
    T bspline_evaluate_deriv_v_n(const T p[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v)
    {
        std::vector<T> pu(nu);
        std::vector<T> pv(nv);
        for (int j = 0; j < nv; j++)
        {
            for (int i = 0; i < nu; i++)
            {
                pu[i] = p[j * nu + i];
            }
            pv[j] = bspline_evaluate_n(&p[0], nu, uknots, ku, u);
        }
        return bspline_evaluate_deriv_n(&pv[0], nv, vknots, kv, v);
    }

    //---------------------------------------------------------------------------------------------------
    template <class FLOAT>
    static void to_regular(std::vector<FLOAT>& knots)
    {
        FLOAT min = knots.front();
        FLOAT max = knots.back();
        if (!(min == 0 && max == 1))
        {
            FLOAT wid = FLOAT(1.0) / (max - min);
            for (size_t i = 0; i < knots.size(); i++)
            {
                knots[i] = (knots[i] - min) * wid;
            }
        }
    }

    template <class FLOAT>
    static void make_open_knots(std::vector<FLOAT>& out, const std::vector<FLOAT>& in, int m) //m==order
    {
        int nKnots = (int)in.size(); //
        out.resize(nKnots);
        int k = 0;
        for (int i = 0; i < m; i++)
        {
            out[i] = (FLOAT)k;
        }
        k++;
        for (int i = m; i < nKnots - m; i++)
        {
            out[i] = (FLOAT)(k++);
        }
        for (int i = nKnots - m; i < nKnots; i++)
        {
            out[i] = (FLOAT)(k);
        }

        FLOAT max = FLOAT(1.0) / k;
        for (int i = 0; i < nKnots; i++)
        {
            out[i] *= max;
        }
    }

    template <class T, class FLOAT>
    void bspline_kill_periodic_(std::vector<T>& points, std::vector<FLOAT>& knots)
    {
        int nCps = (int)points.size();
        int nKnots = (int)knots.size();
        int nOrder = nKnots - nCps;
        int nDegree = nOrder - 1;
        std::vector<T> tpoints(points);
        std::vector<FLOAT> tknots(knots);

        int nCase = 0;
        if (bspline_is_open0(&knots[0], nKnots, nOrder))
            nCase |= 1;
        if (bspline_is_open1(&knots[0], nKnots, nOrder))
            nCase |= 2;

        switch (nCase)
        {
        case 0:
        { //smooth
            for (int i = 0; i < nDegree; i++)
            {
                tpoints.push_back(tpoints[i]);
            }
            for (int i = 0; i < nDegree; i++)
            {
                tknots.push_back(tknots.back() + tknots[i + 1] - tknots[i]);
            }
            to_regular(tknots);
        }
        break;
        case 1: //open close
        {
            tpoints.push_back(tpoints.front());
            tknots.push_back(2 * tknots[nKnots - 1] - tknots[nKnots - 2]); //
            to_regular(tknots);
        }
        break;
        case 2: //close open
        {
            tpoints.push_back(tpoints.front());
            tknots.push_back(tknots[nKnots - 1] + tknots[1] - tknots[0]); //
            to_regular(tknots);
        }
        break;
        case 3: //open open
        {
            tpoints.push_back(tpoints.front());
            tknots.push_back(tknots.back()); //
            make_open_knots(tknots, tknots, nOrder);
        }
        break;
        }
        points.swap(tpoints);
        knots.swap(tknots);
    }

    template <class T, class FLOAT>
    int bspline_kill_periodic_u_(std::vector<T>& points, int nu, int nv, std::vector<FLOAT>& uknots, const std::vector<FLOAT>& vknots)
    {
        int nKnots_u = (int)uknots.size();
        int nOrder_u = nKnots_u - nu;

        {
            std::vector<std::vector<T> > rows(nv);

            std::vector<FLOAT> tk = uknots;
            for (int j = 0; j < nv; j++)
            {
                std::vector<FLOAT> tknots = tk;
                rows[j] = get_row(points, j, nu, nv);
                bspline_kill_periodic_(rows[j], tknots);
                uknots.swap(tknots);
            }
            int nnu = (int)rows[0].size();
            std::vector<T> tmp(nnu * nv);
            for (int j = 0; j < nv; j++)
            {
                assert(nnu == (int)rows[j].size());

                for (int i = 0; i < nnu; i++)
                {
                    tmp[j * nnu + i] = rows[j][i];
                }
            }
            points.swap(tmp);
            return nnu;
        }
        return nu;
    }

    template <class T, class FLOAT>
    int bspline_kill_periodic_v_(std::vector<T>& points, int nu, int nv, const std::vector<FLOAT>& uknots, std::vector<FLOAT>& vknots)
    {
        int nKnots_v = (int)vknots.size();
        int nOrder_v = nKnots_v - nv;

        {
            std::vector<std::vector<T> > cols(nu);

            std::vector<FLOAT> tk = vknots;
            for (int i = 0; i < nu; i++)
            {
                std::vector<FLOAT> tknots = tk;
                cols[i] = get_col(points, i, nu, nv);
                bspline_kill_periodic_(cols[i], tknots);
                vknots.swap(tknots);
            }
            int nnv = (int)cols[0].size();
            std::vector<T> tmp(nu * nnv);
            for (int i = 0; i < nu; i++)
            {
                assert(nnv == (int)cols[i].size());

                for (int j = 0; j < nnv; j++)
                {
                    tmp[j * nu + i] = cols[i][j];
                }
            }
            points.swap(tmp);
            return nnv;
        }
        return nv;
    }

    template <class T, class FLOAT>
    void bspline_kill_periodic_(std::vector<T>& points, int& nu, int& nv, std::vector<FLOAT>& uknots, std::vector<FLOAT>& vknots, bool periodicU, bool periodicV)
    {
        int nKnots_u = (int)uknots.size();
        int nOrder_u = nKnots_u - nu;

        int nKnots_v = (int)vknots.size();
        int nOrder_v = nKnots_v - nv;

        if (!periodicU && !periodicV)
            return;
        if (!periodicV)
        { //U
            nu = bspline_kill_periodic_u_(points, nu, nv, uknots, vknots);
            //nv = bspline_kill_periodic_v_(points, nu, nv, uknots, vknots);
        }
        else if (!periodicU)
        { //V
            //nu = bspline_kill_periodic_u_(points, nu, nv, uknots, vknots);
            nv = bspline_kill_periodic_v_(points, nu, nv, uknots, vknots);
        }
        else
        { //U&V
            nu = bspline_kill_periodic_u_(points, nu, nv, uknots, vknots);
            nv = bspline_kill_periodic_v_(points, nu, nv, uknots, vknots);
        }
    }

//---------------------------------------------------------------------------------------------------

#define DEC_BSPLINE(TYPE, FLOAT)                                                                                                                \
    TYPE bspline_evaluate(const TYPE p[], int n, const FLOAT knots[], int k, FLOAT t)                                                           \
    {                                                                                                                                           \
        return bspline_evaluate_n(p, n, knots, k, t);                                                                                           \
    }                                                                                                                                           \
    TYPE bspline_evaluate_deriv(const TYPE p[], int n, const FLOAT knots[], int k, FLOAT t)                                                     \
    {                                                                                                                                           \
        return bspline_evaluate_deriv_n(p, n, knots, k, t);                                                                                     \
    }                                                                                                                                           \
    TYPE bspline_evaluate(const TYPE p[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v)         \
    {                                                                                                                                           \
        return bspline_evaluate_n(p, nu, nv, uknots, ku, vknots, kv, u, v);                                                                     \
    }                                                                                                                                           \
    TYPE bspline_evaluate_deriv_u(const TYPE p[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v) \
    {                                                                                                                                           \
        return bspline_evaluate_deriv_u_n(p, nu, nv, uknots, ku, vknots, kv, u, v);                                                             \
    }                                                                                                                                           \
    TYPE bspline_evaluate_deriv_v(const TYPE p[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v) \
    {                                                                                                                                           \
        return bspline_evaluate_deriv_v_n(p, nu, nv, uknots, ku, vknots, kv, u, v);                                                             \
    }

DEC_BSPLINE(float, float)
DEC_BSPLINE(vector2f, float)
DEC_BSPLINE(vector3f, float)
DEC_BSPLINE(vector4f, float)
DEC_BSPLINE(double, double)
DEC_BSPLINE(vector2, double)
DEC_BSPLINE(vector3, double)
DEC_BSPLINE(vector4, double)

#undef DEC_BSPLINE

#define DEC_BSPLINE(TYPE, FLOAT)                                                                                                     \
    void bspline_convert_bezier_type(std::vector<TYPE>& p, std::vector<FLOAT>& knots)                                                \
    {                                                                                                                                \
        bspline_convert_bezier_type_(p, knots);                                                                                      \
    }                                                                                                                                \
    void bspline_convert_bezier_type(std::vector<TYPE>& p, int& nu, int& nv, std::vector<FLOAT>& uknots, std::vector<FLOAT>& vknots) \
    {                                                                                                                                \
        bspline_convert_bezier_type_(p, nu, nv, uknots, vknots);                                                                     \
    }                                                                                                                                \
    void bspline_convert_bezier_type(std::vector<TYPE>& p, std::vector<FLOAT>& w, std::vector<FLOAT>& knots)                         \
    {                                                                                                                                \
        bspline_convert_bezier_type_(p, w, knots);                                                                                   \
    }

DEC_BSPLINE(float, float)
DEC_BSPLINE(vector2f, float)
DEC_BSPLINE(vector3f, float)
DEC_BSPLINE(vector4f, float)
DEC_BSPLINE(double, double)
DEC_BSPLINE(vector2, double)
DEC_BSPLINE(vector3, double)
DEC_BSPLINE(vector4, double)

#undef DEC_BSPLINE

#define DEC_BSPLINE(TYPE, FLOAT)                                                                                                                                    \
    void bspline_kill_periodic(std::vector<TYPE>& points, std::vector<FLOAT>& knots)                                                                                \
    {                                                                                                                                                               \
        bspline_kill_periodic_(points, knots);                                                                                                                      \
    }                                                                                                                                                               \
    void bspline_kill_periodic(std::vector<TYPE>& points, int& nu, int& nv, std::vector<FLOAT>& uknots, std::vector<FLOAT>& vknots, bool periodicU, bool periodicV) \
    {                                                                                                                                                               \
        bspline_kill_periodic_(points, nu, nv, uknots, vknots, periodicU, periodicV);                                                                               \
    }

DEC_BSPLINE(float, float)
DEC_BSPLINE(vector2f, float)
DEC_BSPLINE(vector3f, float)
DEC_BSPLINE(vector4f, float)
DEC_BSPLINE(double, double)
DEC_BSPLINE(vector2, double)
DEC_BSPLINE(vector3, double)
DEC_BSPLINE(vector4, double)

#undef DEC_BSPLINE
}
