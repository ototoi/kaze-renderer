#ifndef KAZE_MEDIATION_H
#define KAZE_MEDIATION_H

#include "types.h"

namespace kaze
{

    class mediation
    {
    public:
        mediation(const mediation& rhs) : ior_(rhs.ior_), cnt_(rhs.cnt_), cnt_r_(rhs.cnt_r_), cnt_t_(rhs.cnt_t_) {}
        mediation(real ior = real(1), int cnt = 1, int r_cnt = 1, int t_cnt = 1) : ior_(ior), cnt_(cnt), cnt_r_(r_cnt), cnt_t_(t_cnt) {}

        mediation& operator=(const mediation& rhs)
        {
            ior_ = rhs.ior_;
            cnt_ = rhs.cnt_;
            cnt_r_ = rhs.cnt_r_;
            cnt_t_ = rhs.cnt_t_;
            return *this;
        }

        mediation next_r() const { return next_r(ior_); }
        mediation next_t() const { return next_t(ior_); }

        mediation next_r(real ior) const { return mediation(ior, cnt_ + 1, cnt_r_ + 1, cnt_t_ + 0); }
        mediation next_t(real ior) const { return mediation(ior, cnt_ + 1, cnt_r_ + 0, cnt_t_ + 1); }

        real ior() const { return ior_; }
        int count() const { return cnt_; }
        int count_r() const { return cnt_r_; }
        int count_t() const { return cnt_t_; }

    private:
        real ior_;

        int cnt_;
        int cnt_r_;
        int cnt_t_;
    };
}

#endif
