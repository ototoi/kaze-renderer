#ifndef KAZE_COLOR_H
#define KAZE_COLOR_H

#include "types.h"

namespace kaze
{
    typedef real color1;
    typedef vector2 color2;
    typedef vector3 color3;
    typedef vector4 color4;
    typedef float color1f;
    typedef vector2f color2f;
    typedef vector3f color3f;
    typedef vector4f color4f;
    typedef double color1d;
    typedef vector2d color2d;
    typedef vector3d color3d;
    typedef vector4d color4d;

    inline color3 color_white() { return color3(1, 1, 1); }
    inline color3 color_black() { return color3(0, 0, 0); }

    inline color3 color_red() { return color3(1, 0, 0); }
    inline color3 color_green() { return color3(0, 1, 0); }
    inline color3 color_blue() { return color3(0, 0, 1); }

    inline color3 color_cyan() { return color_green() + color_blue(); }
    inline color3 color_mazenta() { return color_blue() + color_red(); }
    inline color3 color_yellow() { return color_red() + color_green(); }

} //end of namespace

#endif
