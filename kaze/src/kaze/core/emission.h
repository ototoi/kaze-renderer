#ifndef KAZE_EMISSION_H
#define KAZE_EMISSION_H

#include "types.h"
#include "spectrum.h"

namespace kaze
{

    class emission
    {
    public:
        emission(const emission& rhs) : dir_(rhs.dir_), pow_(rhs.pow_) {}
        emission(const vector3& direction, const spectrum& power) : dir_(direction), pow_(power) {}

        emission& operator=(const emission& rhs)
        {
            this->dir_ = rhs.dir_;
            this->pow_ = rhs.pow_;
            return *this;
        }

        emission& operator*=(real rhs)
        {
            this->pow_ *= rhs;
            return *this;
        }

    public:
        const spectrum& power() const { return pow_; }
        const vector3& direction() const { return dir_; }
    private:
        vector3 dir_;
        spectrum pow_;
    };
}

#endif
