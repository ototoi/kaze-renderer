#ifndef KAZE_QMC_H
#define KAZE_QMC_H

#include <cstdlib>
#include <cmath>

namespace kaze
{

    //from Efficient Multidimensional Sampling

    //van der Courput
    // fast base-2 van der Corput, Sobel, and Larcher & Pillichshammer sequences,
    // all from "Efficient Multidimensional Sampling" by Alexander Keller
    inline double ri_vdC(unsigned int bits, unsigned int r = 0)
    {
        //
        bits = (bits << 16) | (bits >> 16);
        bits = ((bits & 0x00ff00ff) << 8) | ((bits & 0xff00ff00) >> 8);
        bits = ((bits & 0x0f0f0f0f) << 4) | ((bits & 0xf0f0f0f0) >> 4);
        bits = ((bits & 0x33333333) << 2) | ((bits & 0xcccccccc) >> 2);
        bits = ((bits & 0x55555555) << 1) | ((bits & 0xaaaaaaaa) >> 1);
        //
        bits ^= r;
        return (double)bits / (double)0x100000000LL;
    }

    inline double ri_S(unsigned int i, unsigned int r = 0)
    {
        for (unsigned int v = 1 << 31; i; i >>= 1, v ^= v >> 1)
            if (i & 1) r ^= v;
        return double(r) / (double)0x100000000LL;
    }

    inline double ri_LP(unsigned int i, unsigned int r = 0)
    {
        for (unsigned int v = 1 << 31; i; i >>= 1, v |= v >> 1)
            if (i & 1) r ^= v;
        return double(r) / (double)0x100000000LL;
    }

    inline double sobol2(unsigned int i, unsigned int r = 0) { return ri_S(i, r); }

    double halton2(int i);
    /*
	 * Generate Hammersley points in [0,1)^2
	 *
	 */
    void hammersley2(double* out, int n);
    void hammersley2_r(double* out, int n, unsigned int base);
    void hammersley2_s(double* out, int n, int nLook = 1);
    //

    double halton(unsigned int i, unsigned int base);

    double generalized_vdC(int i, int base, int** p);
    double generalized_scrambled_halton(int i, int offset, int dim, int** p);
    double generalized_scrambled_hammersley(int i, int offset, int n, int dim, int** p);

    int nextPrime(int lastPrime);
}

#endif