#ifndef KAZE_TYPE_TRAITS_HPP
#define KAZE_TYPE_TRAITS_HPP

namespace kaze
{

    template <class T>
    struct paramater_traits
    {
        typedef const T& input_type;
        typedef const T& output_type;
    };

#define DEF_PT(TYPE)              \
    template <>                   \
	struct paramater_traits<TYPE> \
    {                             \
        typedef TYPE input_type;  \
        typedef TYPE out_type;    \
    };

    DEF_PT(char);
    DEF_PT(unsigned char);
    DEF_PT(short);
    DEF_PT(unsigned short);
    DEF_PT(int);
    DEF_PT(unsigned int);
    DEF_PT(float);
    DEF_PT(double);
    DEF_PT(long double);

#undef DEF_PT
}

#endif