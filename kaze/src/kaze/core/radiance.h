#ifndef KAZE_RADIANCE_H
#define KAZE_RADIANCE_H

#include "emission.h"

namespace kaze
{
    typedef emission radiance;
}

#endif