#ifndef KAZE_MEMORY_UTILITY_H
#define KAZE_MEMORY_UTILITY_H

#include <cstdlib>
#include <stdlib.h>

namespace kaze
{

#ifdef _WIN32
    inline void* aligned_malloc(std::size_t sz, std::size_t al)
    {
        return ::_aligned_malloc(sz, al);
    }

    inline void aligned_free(void* p)
    {
        ::_aligned_free(p);
    }
#elif defined(__unix__) || defined(__APPLE__)
    inline void* aligned_malloc(std::size_t sz, std::size_t al)
    {
        //int posix_memalign(void **memptr, size_t alignment, size_t size);
        void* ptr;
        if (posix_memalign(&ptr, al, sz) == 0)
        {
            return ptr;
        }
        return NULL;
    }

    inline void aligned_free(void* p)
    {
        free(p);
    }
#endif
}

#endif