#ifndef KAZE_PHOTON_H
#define KAZE_PHOTON_H

#include "types.h"
#include "spectrum.h"

namespace kaze
{

    class photon
    {
    public:
        photon() {}
        photon(const photon& rhs) : pos_(rhs.pos_), dir_(rhs.dir_), pow_(rhs.pow_) {}
        photon(const vector3& pos, const vector3& dir, const color3& pow) : pos_(pos), dir_(dir), pow_(convert(pow)) {}
        photon(const vector3& pos, const vector3& dir, const spectrum& pow) : pos_(pos), dir_(dir), pow_(pow) {}

        photon& operator=(const photon& rhs)
        {
            this->pos_ = rhs.pos_;
            this->dir_ = rhs.dir_;
            this->pow_ = rhs.pow_;
            return *this;
        }

        const vector3& position() const
        {
            return pos_;
        }

        const vector3& direction() const
        {
            return dir_;
        }

        const spectrum& power() const
        {
            return pow_;
        }

    private:
        vector3 pos_;  //position
        vector3 dir_;  //direction
        spectrum pow_; //power
    };
};

#endif
