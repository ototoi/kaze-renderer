#ifndef KAZE_SPECTRUM_COMPONENT_H
#define KAZE_SPECTRUM_COMPONENT_H

#include "spectrum.h"
#include "color.h"
#include <vector>

namespace kaze
{

    struct spectrum_pair
    {
        int wavelength;
        float value;
    };

    class rgb_spectrum_component : public spectrum_component
    {
    public:
        rgb_spectrum_component(real r, real g, real b);
        explicit rgb_spectrum_component(const color3& c);
        color3 to_rgb() const;
        spectrum_component* clone() const;
        const char* id_string() const;
        bool equal(const spectrum_component* sc) const;
        real intensity() const;
        void add(const spectrum_component* sc);
        void mul(const spectrum_component* sc);
        void mul(real rhs);

    protected:
        inline void add(const color3& c) { c_ += c; }
        inline void mul(const color3& c) { c_ *= c; }
    public:
        static const char* IDSTRING;

    private:
        color3 c_;
    };

    class rgba_spectrum_component : public spectrum_component
    {
    public:
        rgba_spectrum_component(real r, real g, real b, real a);
        explicit rgba_spectrum_component(const color4& c);
        color3 to_rgb() const;
        spectrum_component* clone() const;
        const char* id_string() const;
        bool equal(const spectrum_component* sc) const;
        real intensity() const;
        void add(const spectrum_component* sc);
        void mul(const spectrum_component* sc);
        void mul(real rhs);

    public:
        real alpha() const { return c_[3]; }
    protected:
        void add(const color3& c);
        void mul(const color3& c);

    public:
        static const char* IDSTRING;

    private:
        color4 c_;
    };

    class color_opacity_spectrum_component : public spectrum_component
    {
    public:
        color_opacity_spectrum_component(const color3& c, const color3& o);
        color3 to_rgb() const;
        spectrum_component* clone() const;
        const char* id_string() const;
        bool equal(const spectrum_component* sc) const;
        real intensity() const;
        void add(const spectrum_component* sc);
        void mul(const spectrum_component* sc);
        void mul(real rhs);

    public:
        color3 opacity() const;

    public:
        static const char* IDSTRING;

    protected:
        color3 c_;
        color3 o_;
    };

    class CiOi_spectrum_component : public spectrum_component
    {
    public:
        CiOi_spectrum_component(const color3& c, const color3& o);
        color3 to_rgb() const;
        spectrum_component* clone() const;
        const char* id_string() const;
        bool equal(const spectrum_component* sc) const;
        real intensity() const;
        void add(const spectrum_component* sc);
        void mul(const spectrum_component* sc);
        void mul(real rhs);

    public:
        color3 Ci() const;
        color3 Oi() const;

    public:
        static const char* IDSTRING;

    public:
        static color3 get_Ci(const spectrum_component* sc);
        static color3 get_Oi(const spectrum_component* sc);

    protected:
        color3 Ci_;
        color3 Oi_;
    };

    class xyz_spectrum_component : public spectrum_component
    {
    public:
        xyz_spectrum_component(double x, double y, double z);
        explicit xyz_spectrum_component(const double* xyz);
        explicit xyz_spectrum_component(const color3& c);
        color3 to_rgb() const;
        spectrum_component* clone() const;
        const char* id_string() const;
        bool equal(const spectrum_component* sc) const;
        real intensity() const;
        void add(const spectrum_component* sc);
        void mul(const spectrum_component* sc);
        void mul(real rhs);

    public:
        static const char* IDSTRING;

    private:
        double c_[3];
    };

    class black_spectrum_component : public spectrum_component
    {
    public:
        black_spectrum_component();
        color3 to_rgb() const;
        spectrum_component* clone() const;
        const char* id_string() const;
        bool equal(const spectrum_component* sc) const;
        real intensity() const;
        void add(const spectrum_component* sc);
        void mul(const spectrum_component* sc);
        void mul(real rhs);

    public:
        static const char* IDSTRING;
    };

    class singlesample_spectrum_component : public spectrum_component
    {
    public:
        singlesample_spectrum_component(int w, float v);
        explicit singlesample_spectrum_component(const spectrum_pair& pr);
        color3 to_rgb() const;
        spectrum_component* clone() const;
        const char* id_string() const;
        bool equal(const spectrum_component* sc) const;
        real intensity() const;

        void add(const spectrum_component* c);
        void mul(const spectrum_component* c);
        void mul(real rhs);

    public:
        const spectrum_pair& get_spectrum_pair() const;
        bool is_addable(const singlesample_spectrum_component* ssc) const;
        static const char* IDSTRING;

    private:
        spectrum_pair pr_;
    };

    class multisample_spectrum_component : public spectrum_component
    {
    public:
        multisample_spectrum_component();
        explicit multisample_spectrum_component(const multisample_spectrum_component& rhs);
        explicit multisample_spectrum_component(const color3& c);

        spectrum_component* clone() const;
        const char* id_string() const;
        bool equal(const spectrum_component* sc) const;
        color3 to_rgb() const;
        real intensity() const;
        void add(const spectrum_component* sc);
        void mul(const spectrum_component* sc);
        void mul(real rhs);

    public:
        size_t samples() const;
        void add(const color3& c);
        void mul(const color3& c);
        void add(const spectrum_pair& sp);
        void mul(const spectrum_pair& sp);

    public:
        size_t size() const { return mv_.size(); };
        const spectrum_pair& operator[](size_t i) const { return mv_[i]; }
    public:
        static const char* IDSTRING;

    protected:
        static bool equal_wavelengthes(const multisample_spectrum_component* a, const multisample_spectrum_component* b);

    private:
        std::vector<spectrum_pair> mv_;
    };

    bool type_equal(const spectrum_component* a, const spectrum_component* b);
    spectrum_component* spectrum_plus(const spectrum_component* a, const spectrum_component* b);
    spectrum_component* spectrum_multiply(const spectrum_component* a, const spectrum_component* b);

    //----------------------------------------------------------------------------------------------

    class blackbody_spectrum_component : public spectrum_component
    {
    public:
        blackbody_spectrum_component();
        explicit blackbody_spectrum_component(real t, real v = real(1));

        color3 to_rgb() const;

        spectrum_component* clone() const;
        const char* id_string() const;
        bool equal(const spectrum_component* sc) const;
        real intensity() const;
        void add(const spectrum_component* sc);
        void mul(const spectrum_component* sc);
        void mul(real rhs);

    public:
        static const char* IDSTRING;
        double sample(real l) const;

    private:
        real t_;
        real v_;
    };
}

#endif