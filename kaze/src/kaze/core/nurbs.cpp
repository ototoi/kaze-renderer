#include "nurbs.h"
#include "bspline.h"

namespace kaze
{

    template <class T, class FLOAT>
    static inline T mul_coef(const FLOAT e[], const T v[], const FLOAT w[], int n)
    {
        T tmp = e[0] * w[0] * v[0];
        FLOAT wtmp = e[0] * w[0];
        for (int i = 1; i < n; i++)
        {
            tmp += e[i] * w[i] * v[i];
            wtmp += e[i] * w[i];
        }
        return tmp / wtmp;
    }

    template <class T, class FLOAT>
    T nurbs_evaluate_n(const T p[], const FLOAT w[], int n, const FLOAT knots[], int k, FLOAT t)
    {
        int m = k - n;
        int d = m - 1;
        assert(m >= 1);
        t = (1 - t) * knots[d] + t * knots[k - 1 - d];
        std::vector<FLOAT> params(m);
        int offset = de_boor_cox_offset(&params[0], m, knots, k, t);
        if (offset == 0)
        {
            return mul_coef(&params[0], p, w, m);
        }
        else if (offset < 0)
        {
            return mul_coef(&params[-offset], p, w, m + offset);
        }
        else
        {
            return mul_coef(&params[0], p + offset, w + offset, std::min<int>(m, n - offset)); //k-2-m+1->n-1
        }
    }

    template <class T, class FLOAT>
    T nurbs_evaluate_deriv_n(const T p[], const FLOAT w[], int n, const FLOAT knots[], int k, FLOAT t)
    {
        int m = k - n;
        int d = m - 1;
        assert(m >= 1);
        t = (1 - t) * knots[d] + t * knots[k - 1 - d];
        std::vector<FLOAT> params(m);
        int offset = de_boor_cox_deriv_offset(&params[0], m, knots, k, t);
        if (offset == 0)
        {
            return mul_coef(&params[0], p, w, m);
        }
        else if (offset < 0)
        {
            return mul_coef(&params[-offset], p, w, m + offset);
        }
        else
        {
            return mul_coef(&params[0], p + offset, w + offset, std::min<int>(m, n - offset)); //k-2-m+1->n-1
        }
    }

    template <class TYPE, class FLOAT>
    TYPE nurbs_evaluate_n(const TYPE p[], const FLOAT w[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v)
    {
        size_t sz = (size_t)(nu * nv);
        std::vector<TYPE> pw(p, p + sz);
        for (size_t i = 0; i < sz; i++)
            pw[i] *= w[i];
        return bspline_evaluate(&pw[0], nu, nv, uknots, ku, vknots, kv, u, v) /
               bspline_evaluate(w, nu, nv, uknots, ku, vknots, kv, u, v);
    }

    template <class TYPE, class FLOAT>
    TYPE nurbs_evaluate_deriv_u_n(const TYPE p[], const FLOAT w[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v)
    {
        std::vector<TYPE> pu(nu);
        std::vector<TYPE> pv(nv);
        std::vector<FLOAT> wu(nu);
        std::vector<FLOAT> wv(nv);
        for (int i = 0; i < nu; i++)
        {
            for (int j = 0; j < nv; j++)
            {
                pv[j] = p[j * nu + i];
                wv[j] = w[j * nu + i];
            }
            pu[i] = bspline_evaluate(&pv[0], nv, vknots, kv, v);
            wu[i] = bspline_evaluate(&wv[0], nv, vknots, kv, v);
        }
        return nurbs_evaluate_deriv_n(&pu[0], &wu[0], nu, uknots, ku, u);
    }

    template <class TYPE, class FLOAT>
    TYPE nurbs_evaluate_deriv_v_n(const TYPE p[], const FLOAT w[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v)
    {
        std::vector<TYPE> pu(nu);
        std::vector<TYPE> pv(nv);
        std::vector<FLOAT> wu(nu);
        std::vector<FLOAT> wv(nv);
        for (int j = 0; j < nv; j++)
        {
            for (int i = 0; i < nu; i++)
            {
                pu[i] = p[j * nu + i];
                wu[i] = w[j * nu + i];
            }
            pv[j] = bspline_evaluate(&p[0], nu, uknots, ku, u);
            wv[j] = bspline_evaluate(&w[0], nu, uknots, ku, u);
        }
        return nurbs_evaluate_deriv_n(&pv[0], &wu[0], nv, vknots, kv, v);
    }

#define DEC_BSPLINE(TYPE, FLOAT)                                                                                                                               \
    TYPE nurbs_evaluate(const TYPE p[], const FLOAT w[], int n, const FLOAT knots[], int k, FLOAT t)                                                           \
    {                                                                                                                                                          \
        return nurbs_evaluate_n(p, w, n, knots, k, t);                                                                                                         \
    }                                                                                                                                                          \
    TYPE nurbs_evaluate_deriv(const TYPE p[], const FLOAT w[], int n, const FLOAT knots[], int k, FLOAT t)                                                     \
    {                                                                                                                                                          \
        return nurbs_evaluate_deriv_n(p, w, n, knots, k, t);                                                                                                   \
    }                                                                                                                                                          \
    TYPE nurbs_evaluate(const TYPE p[], const FLOAT w[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v)         \
    {                                                                                                                                                          \
        return nurbs_evaluate_n(p, w, nu, nv, uknots, ku, vknots, kv, u, v);                                                                                   \
    }                                                                                                                                                          \
    TYPE nurbs_evaluate_deriv_u(const TYPE p[], const FLOAT w[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v) \
    {                                                                                                                                                          \
        return nurbs_evaluate_deriv_u_n(p, w, nu, nv, uknots, ku, vknots, kv, u, v);                                                                           \
    }                                                                                                                                                          \
    TYPE nurbs_evaluate_deriv_v(const TYPE p[], const FLOAT w[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v) \
    {                                                                                                                                                          \
        return nurbs_evaluate_deriv_v_n(p, w, nu, nv, uknots, ku, vknots, kv, u, v);                                                                           \
    }

    DEC_BSPLINE(float, float)
    DEC_BSPLINE(vector2f, float)
    DEC_BSPLINE(vector3f, float)
    DEC_BSPLINE(double, double)
    DEC_BSPLINE(vector2d, double)
    DEC_BSPLINE(vector3d, double)

#undef DEC_BSPLINE

    template <class TYPE, class FLOAT>
    void nurbs_convert_bezier_type_(std::vector<TYPE>& p, std::vector<FLOAT>& w, std::vector<FLOAT>& knots)
    {
        bspline_convert_bezier_type(p, w, knots);
    }

    template <class TYPE, class FLOAT>
    void nurbs_convert_bezier_type_(std::vector<TYPE>& p, std::vector<FLOAT>& w, int& nu, int& nv, std::vector<FLOAT>& uknots, std::vector<FLOAT>& vknots)
    {
        int nu_ = nu;
        int nv_ = nv;

        std::vector<FLOAT> uknots_ = uknots;
        std::vector<FLOAT> vknots_ = vknots;

        bspline_convert_bezier_type(p, nu, nv, uknots, vknots);
        bspline_convert_bezier_type(w, nu_, nv_, uknots_, vknots_);
    }

#define DEC_BSPLINE(TYPE, FLOAT)                                                                                                                          \
    void nurbs_convert_bezier_type(std::vector<TYPE>& p, std::vector<FLOAT>& w, std::vector<FLOAT>& knots)                                                \
    {                                                                                                                                                     \
        nurbs_convert_bezier_type_(p, w, knots);                                                                                                          \
    }                                                                                                                                                     \
    void nurbs_convert_bezier_type(std::vector<TYPE>& p, std::vector<FLOAT>& w, int& nu, int& nv, std::vector<FLOAT>& uknots, std::vector<FLOAT>& vknots) \
    {                                                                                                                                                     \
        nurbs_convert_bezier_type_(p, w, nu, nv, uknots, vknots);                                                                                         \
    }

    DEC_BSPLINE(float, float)
    DEC_BSPLINE(vector2f, float)
    DEC_BSPLINE(vector3f, float)
    DEC_BSPLINE(double, double)
    DEC_BSPLINE(vector2d, double)
    DEC_BSPLINE(vector3d, double)

#undef DEC_BSPLINE
}
