#include "bspline_ex.h"

#include <vector>

namespace kaze
{

    static void make_schoenberg_whitney_knots(std::vector<real>& knots, int psz, int nOrder)
    {
        int n = psz;
        int tsz = n + nOrder; //knots
        knots.resize(tsz);
        for (int i = 0; i < nOrder; i++)
            knots[i] = 0;
        for (int i = 0; i < n - nOrder; i++)
            knots[i + nOrder] = i + nOrder * real(0.5);
        for (int i = n; i < tsz; i++)
            knots[i] = n - 1;
        //for(int i = 0;i<tsz;i++)params[i]/=n-1;
    }

    bool make_interpolate_bspline_matrix(dynamic_matrix<real>& out, std::vector<real>& knots, int N, int nOrder)
    {
        make_schoenberg_whitney_knots(knots, N, nOrder);

        dynamic_matrix<real> m(N, N);
        std::vector<real> B(N);
        for (int i = 0; i < N; i++)
        {
            de_boor_cox(&B[0], N, &knots[0], (int)knots.size(), i);
            for (int j = 0; j < N; j++)
            {
                m.at(i, j) = B[j];
            }
        }

        return invert_lu(out, m);
    }
}
