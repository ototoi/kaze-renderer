#ifndef KAZE_AUTO_ARRAY_HPP
#define KAZE_AUTO_ARRAY_HPP

#include <cstdlib>

#include <memory>

namespace kaze
{

    template <class T>
    class auto_array
    {
    public:
        typedef T element_type;

    public:
        explicit auto_array(T* ptr) : ptr_(ptr) {}
        auto_array(auto_array& rhs) : ptr_(rhs.release()) {}
        ~auto_array()
        {
            if (ptr_)
            {
                delete[] ptr_;
            }
        }

        auto_array& operator=(auto_array& rhs)
        {
            reset(rhs.release());
            return *this;
        }

        T& operator*() const { return *ptr_; }
        T* operator->() const { return (&**this); }
        T* get() const { return ptr_; }
        T* release()
        {
            T* tmp = ptr_;
            ptr_ = 0;
            return tmp;
        }
        void reset(T* ptr = 0)
        {
            if (ptr_) delete[] ptr_;
            ptr_ = ptr;
        }
        T& operator[](std::size_t i) { return ptr_[i]; }
        const T& operator[](std::size_t i) const { return ptr_[i]; }
    private:
        T* ptr_;
    };
}

#endif