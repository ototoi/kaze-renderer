#ifndef KAZE_FP16_H
#define KAZE_FP16_H

namespace kaze
{
    typedef unsigned short fp16_t;

    union uint_float__union_
    {
        unsigned int i;
        float f;
    };

    /*
        bit15 ------- bit0
        s eeeee ffffffffff
        ------------------

        s = 1bit  sign
        e = 5bit  offset 15 ( 0�`31 )
        f = 10bit
        */
    //#if _DEBUG
    fp16_t f32to16(float fval);
    float f16to32(fp16_t ival);
    //#endif

    inline fp16_t f32to16(float fval)
    {
        uint_float__union_ uf;
        uf.f = fval;
        unsigned int ival = uf.i;

        if (!ival) return 0;

        int e = ((ival & 0x7f800000) >> 23) - 127 + 15;

        if (e < 0)
            return 0;
        else if (e > 31)
        {
            e = 31;
        }

        int s = ival & 0x80000000;
        int f = ival & 0x007fffff;

        fp16_t tmp = fp16_t(((s >> 16) & 0x8000) | ((e << 10) & 0x7c00) | ((f >> 13) & 0x03ff));
    #if _DEBUG
    //float fx = f16to32(tmp);
    #endif
        return tmp;
    }

    inline float f16to32(fp16_t ival)
    {
        if (!ival) return 0.0f;

        unsigned int s = ival & 0x8000;                      //1 00000 0000000000
        unsigned int e = ((ival & 0x7c00) >> 10) - 15 + 127; //0 11111 0000000000
        unsigned int f = ival & 0x03ff;                      //0 00000 1111111111
        unsigned int fval = (s << 16) | ((e << 23) & 0x7f800000) | (f << 13);

        uint_float__union_ uf;
        uf.i = fval;

        float tmp = uf.f;
        //#if _DEBUG
        //fp16_t ix = f32to16(tmp);
        //#endif
        return tmp;
    }

    inline fp16_t f16negate(fp16_t ival)
    {
        return ival ^ 0x8000;
    }
}

#endif