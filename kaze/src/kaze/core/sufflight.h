#ifndef KAZE_SUFFLIGHT_H
#define KAZE_SUFFLIGHT_H

/**
 * @file sufflight.h
 */

#include "types.h"
#include <vector>

namespace kaze
{

#define KAZE_SUFLIGHT_DEC_VALUE(NAME, a, b, c) \
    static const vector3 NAME##_default() { return vector3(a, b, c); }
#define KAZE_SUFLIGHT_ACCESSOR(NAME, member)             \
    const vector3& NAME() const { return member; }       \
    const vector3& get_##NAME() const { return member; } \
    void set_##NAME(const vector3& v) { member = v; }
#define KAZE_SUFLIGHT_ACCESSOR2(NAME, member)            \
    const vector3& get_##NAME() const { return member; } \
    void set_##NAME(const vector3& v) { member = v; }

    /**
	 * @class sufflight
	 * @brief Suffurring Light Point.
	 */
    class sufflight
    {
    public:
        KAZE_SUFLIGHT_DEC_VALUE(tangent, 1, 0, 0)
        KAZE_SUFLIGHT_DEC_VALUE(binormal, 0, 1, 0)
        KAZE_SUFLIGHT_DEC_VALUE(coord, 0, 0, 0)
        KAZE_SUFLIGHT_DEC_VALUE(normal, 0, 0, 1)
        KAZE_SUFLIGHT_DEC_VALUE(position, 0, 0, 0)
        KAZE_SUFLIGHT_DEC_VALUE(origin, 0, 0, -1)
        KAZE_SUFLIGHT_DEC_VALUE(direction, 0, 0, -1)
        KAZE_SUFLIGHT_DEC_VALUE(color, 0, 0, 0)
    public:
        sufflight(
            const vector3& o = origin_default(),    //origin
            const vector3& d = direction_default(), //direction
            const vector3& p = position_default(),  //position
            const vector3& g = normal_default(),    //geometric
            const vector3& n = normal_default(),    //normal
            const vector3& t = tangent_default(),   //tangent
            const vector3& b = binormal_default(),  //binormal
            const vector3& c = coord_default()      //coord
            ) : o_(o), d_(d), p_(p), g_(g),
                n_(n), t_(t), b_(b),
                c_(c), x_(c),
                c00_(coord_default()), c10_(coord_default()), c01_(coord_default()), c11_(coord_default()),
                col1_(color_default()), col2_(color_default()), col3_(color_default()),
                attr_(0), index_(0)
        {
        }

        sufflight(const sufflight& rhs) : o_(rhs.o_), d_(rhs.d_), p_(rhs.p_), g_(rhs.g_),
                                          n_(rhs.n_), t_(rhs.t_), b_(rhs.b_),
                                          c_(rhs.c_), x_(rhs.x_),
                                          c00_(rhs.c00_), c10_(rhs.c10_), c01_(rhs.c01_), c11_(rhs.c11_),
                                          col1_(rhs.col1_), col2_(rhs.col2_), col3_(rhs.col3_),
                                          attr_(rhs.attr_), index_(rhs.index_)
        {
        }

    public:
        KAZE_SUFLIGHT_ACCESSOR(origin, o_)
        KAZE_SUFLIGHT_ACCESSOR(direction, d_)
        KAZE_SUFLIGHT_ACCESSOR(position, p_)
        KAZE_SUFLIGHT_ACCESSOR(geometric, g_)
        KAZE_SUFLIGHT_ACCESSOR(normal, n_)
        KAZE_SUFLIGHT_ACCESSOR(tangent, t_)
        KAZE_SUFLIGHT_ACCESSOR(binormal, b_)
        KAZE_SUFLIGHT_ACCESSOR(coord, c_)
        KAZE_SUFLIGHT_ACCESSOR(param, c_)
        KAZE_SUFLIGHT_ACCESSOR(uv, c_)
        KAZE_SUFLIGHT_ACCESSOR(texture, x_)
        KAZE_SUFLIGHT_ACCESSOR(st, x_)
        KAZE_SUFLIGHT_ACCESSOR(stw, x_)

        KAZE_SUFLIGHT_ACCESSOR(coord00, c00_)
        KAZE_SUFLIGHT_ACCESSOR(coord10, c10_)
        KAZE_SUFLIGHT_ACCESSOR(coord01, c01_)
        KAZE_SUFLIGHT_ACCESSOR(coord11, c11_)
    public:
        KAZE_SUFLIGHT_ACCESSOR(bitangent, b_)
        KAZE_SUFLIGHT_ACCESSOR(uvw, c_)
        KAZE_SUFLIGHT_ACCESSOR(dPdu, t_)
        KAZE_SUFLIGHT_ACCESSOR(dPdv, b_)
        KAZE_SUFLIGHT_ACCESSOR(nu, t_)
        KAZE_SUFLIGHT_ACCESSOR(nv, b_)

        KAZE_SUFLIGHT_ACCESSOR(col1, col1_)
        KAZE_SUFLIGHT_ACCESSOR(col2, col2_)
        KAZE_SUFLIGHT_ACCESSOR(col3, col3_)
        KAZE_SUFLIGHT_ACCESSOR2(color1, col1_)
        KAZE_SUFLIGHT_ACCESSOR2(color2, col2_)
        KAZE_SUFLIGHT_ACCESSOR2(color3, col3_)
    public:
        size_t get_attr() const { return attr_; }
        void set_attr(size_t attr) { attr_ = attr; }
        size_t get_index() const { return index_; }
        void set_index(size_t index) { index_ = index; }
    public:
        sufflight& operator=(const sufflight& rhs)
        {
            o_ = rhs.o_;
            d_ = rhs.d_;
            p_ = rhs.p_;
            n_ = rhs.n_;
            g_ = rhs.g_;
            t_ = rhs.t_;
            b_ = rhs.b_;
            c_ = rhs.c_;
            x_ = rhs.x_;

            c00_ = rhs.c00_;
            c10_ = rhs.c10_;
            c01_ = rhs.c01_;
            c11_ = rhs.c11_;

            col1_ = rhs.col1_;
            col2_ = rhs.col2_;
            col3_ = rhs.col3_;

            attr_ = rhs.attr_;
            index_ = rhs.index_;

            return *this;
        }

    private:
        vector3 o_; //origin
        vector3 d_; //direction
        vector3 p_; //position
        vector3 g_; //geometric
        vector3 n_; //normal
        vector3 t_; //tangent  <- dPdu
        vector3 b_; //binormal <- dPdv
        vector3 c_; //parameter coord
        vector3 x_; //texture   coord

        vector3 c00_; //c00
        vector3 c10_; //c10
        vector3 c01_; //c01
        vector3 c11_; //c11

        vector3 col1_; //color_1
        vector3 col2_; //color_2
        vector3 col3_; //color_3

        size_t attr_; //face attribute
        size_t index_;
    };

#undef KAZE_SUFLIGHT_ACCESSOR2
#undef KAZE_SUFLIGHT_ACCESSOR
#undef KAZE_SUFLIGHT_DEC_VALUE
}

#endif