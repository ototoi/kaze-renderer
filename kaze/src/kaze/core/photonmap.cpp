#include "photonmap.h"

#include "types.h"
#include "values.h"

#include <functional>
#include <algorithm>
#include <queue>

namespace kaze
{

    enum
    {
        AXIS_MASK_X = 0,
        AXIS_MASK_Y = 1,
        AXIS_MASK_Z = 2,
        AXIS_MASK_ALL = 0 + 1 + 2
    };

    struct c_photon
    {
        vector3f position;
        vector3f power;
        vector3f direction;
        char data;
    };

    static inline vector3f tof(const vector3& v)
    {
        return vector3f(float(v[0]), float(v[1]), float(v[2]));
    }
    static inline vector3d tod(const vector3f& v)
    {
        return vector3d(double(v[0]), double(v[1]), double(v[2]));
    }

    static inline c_photon compress(const photon& p)
    {
        c_photon tmp;
        tmp.position = tof(p.position());
        tmp.direction = tof(p.direction());
        tmp.power = tof(convert(p.power()));
        return tmp;
    }

    static inline photon decompress(const c_photon& p)
    {
        return photon(vector3(p.position), vector3(p.direction), spectrum((color3)p.power));
    }

    struct pm_search_set
    {
    public:
        pm_search_set() {}
        pm_search_set(size_t idx_, real dist_) : index(idx_), dist(dist_) {}
    public:
        size_t index;
        real dist;
    };

    inline bool operator<(const pm_search_set& a, const pm_search_set& b) { return (a.dist < b.dist); }

    class pm_search_struct
    {
    public:
        explicit pm_search_struct(size_t nmax) : max(nmax) {}

        const pm_search_set& far() const { return mv_.top(); }
        const pm_search_set& top() const { return mv_.top(); }
        void pop() { mv_.pop(); }

        size_t size() const { return mv_.size(); }
        bool is_full() const { return (mv_.size() >= max); }

        bool add(size_t idx, real dist)
        {
            mv_.push(pm_search_set(idx, dist));
            if (mv_.size() > max)
            {
                mv_.pop();
            }

            return true;
        }

    private:
        size_t max;
        std::priority_queue<pm_search_set> mv_;
    };

    namespace
    {

        struct axis_sorter : public std::binary_function<c_photon*, c_photon*, bool>
        {
        public:
            axis_sorter(int axis) : axis_(axis) {}
        public:
            bool operator()(const c_photon* a, const c_photon* b) const
            {
                return (a->position[axis_] < b->position[axis_]);
            }

        private:
            int axis_;
        };

        template <class IT>
        inline IT next__(IT& it, size_t num = 1)
        {
            IT tmp = it;
            std::advance(tmp, num);
            return tmp;
        }

        inline char set_axis(char data, int axis)
        {
            return (char)((data & ~AXIS_MASK_ALL) | (axis & AXIS_MASK_ALL));
        }

        template <class IT>
        static void sort_kdtree(IT begin, IT end, const vector3& min, const vector3& max)
        {
            static const axis_sorter sorter[3] = {axis_sorter(0), axis_sorter(1), axis_sorter(2)};

            if (begin == end) return;
            vector3 wid = max - min;

            int max_axis = 0;
            if (wid[max_axis] < wid[1]) max_axis = 1;
            if (wid[max_axis] < wid[2]) max_axis = 2;

            std::sort(begin, end, sorter[max_axis]);
            size_t mid = std::distance(begin, end) >> 1;

            IT mid_it = next__(begin, mid);

            (*mid_it)->data = set_axis((*mid_it)->data, max_axis); //set mid

            real pos = (*mid_it)->position[max_axis];

            vector3 cmin, cmax;

            cmin = min;
            cmax = max;
            cmax[max_axis] = pos;
            sort_kdtree(begin, mid_it, cmin, cmax); //

            cmin = min;
            cmin[max_axis] = pos;
            cmax = max;
            sort_kdtree(next__(mid_it, 1), end, cmin, cmax); //
        }

        template <class IT>
        static void sort_kdtree(IT begin, IT end)
        {
            if (begin == end) return;

            //Get MinMax
            real fr = values::far();
            vector3 min(fr, fr, fr), max(-fr, -fr, -fr), wid;
            for (IT i = begin; i != end; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if ((*i)->position[j] < min[j]) min[j] = (*i)->position[j];
                    if ((*i)->position[j] > max[j]) max[j] = (*i)->position[j];
                }
            }
            sort_kdtree(begin, end, min, max);
        }

    } //end of namespace

    class photonmap_imp
    {
    public:
        void reserve(size_t sz) { mv_.reserve(sz); }
        void construct();
        size_t size() const { return mv_.size(); }
        void add(const photon& p);

        bool search(aggregator<photon>* ps, const vector3& pos, real radius, size_t nmax) const;

    private:
        bool search_internal(
            size_t begin, size_t end,
            pm_search_struct* p_pms,
            const vector3& pos, real r2) const;
        bool search_i(
            size_t i,
            pm_search_struct* p_pms,
            const vector3& pos, real r2) const;

    private:
        std::vector<c_photon> mv_;
    };
    //----------------------------------------------------

    void photonmap_imp::construct()
    { //kdtree construct
        size_t sz = mv_.size();
        std::vector<c_photon*> p_pmp(sz); // pointer of c_photon;
        for (size_t i = 0; i < sz; i++)
        {
            p_pmp[i] = &(mv_[i]);
        } //get pointer

        //
        sort_kdtree(p_pmp.begin(), p_pmp.end());
        //

        std::vector<c_photon> tmp;
        tmp.reserve(sz);
        for (size_t i = 0; i < sz; i++)
        {
            tmp.push_back(*p_pmp[i]);
        }
        mv_.swap(tmp);
    }

    void photonmap_imp::add(const photon& p)
    {
        mv_.push_back(compress(p));
    }

    bool photonmap_imp::search(aggregator<photon>* ps, const vector3& pos, real radius, size_t nmax) const
    {
        pm_search_struct pms(nmax);
        if (this->search_internal(0, mv_.size(), &pms, pos, radius * radius))
        {
            size_t psz = pms.size();
            for (size_t i = 0; i < psz; i++)
            {
                size_t index = pms.top().index;
                pms.pop();
                ps->add(decompress(mv_[index]));
            }
            return true;
        }
        return false;
    }

    bool photonmap_imp::search_i(
        size_t i,
        pm_search_struct* p_pms,
        const vector3& pos, real r2) const
    {
        vector3 diff = (vector3)(mv_[i].position) - pos;
        real l2 = diff.sqr_length();
        if (l2 <= r2)
        {
            p_pms->add(i, l2);
            return true;
        }
        return false;
    }

    bool photonmap_imp::search_internal(size_t begin, size_t end, pm_search_struct* p_pms, const vector3& pos, real r2) const
    {
        if (begin >= end) return false;
        size_t mid = begin + ((end - begin) >> 1);
        int axis = (mv_[mid].data & AXIS_MASK_ALL);

        bool bRet = false;
        real s = pos[axis] - mv_[mid].position[axis]; //axis_length
        real s2 = s * s;                              //axis_length2
        if (s < 0)
        { //search_left
            if (this->search_internal(begin, mid, p_pms, pos, r2)) bRet = true;
            if (s2 <= r2)
            {
                if (this->search_i(mid, p_pms, pos, r2)) bRet = true;
                if (this->search_internal(mid + 1, end, p_pms, pos, r2)) bRet = true;
            }
        }
        else
        {
            if (this->search_internal(mid + 1, end, p_pms, pos, r2)) bRet = true;
            if (s2 <= r2)
            {
                if (this->search_i(mid, p_pms, pos, r2)) bRet = true;
                if (this->search_internal(begin, mid, p_pms, pos, r2)) bRet = true;
            }
        }

        return bRet;
    }

    photonmap::photonmap()
    {
        imp_ = new photonmap_imp();
        imp_->reserve(500);
    }
    photonmap::~photonmap()
    {
        delete imp_;
    }

    void photonmap::reserve(size_t sz)
    {
        imp_->reserve(sz);
    }

    void photonmap::add(const photon& p)
    {
        imp_->add(p);
    }

    void photonmap::construct()
    {
        imp_->construct();
    }

    size_t photonmap::size() const
    {
        return imp_->size();
    }

    bool photonmap::search(aggregator<photon>* ps, const vector3& pos, real radius, size_t nmax) const
    {
        return imp_->search(ps, pos, radius, nmax);
    }

} //end of namespace
