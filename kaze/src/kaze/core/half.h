#ifndef KAZE_HALF_H
#define KAZE_HALF_H

#include "fp16.h"

namespace kaze
{

    class half
    {
    public:
        half() : m_e(0) {}
        half(fp16_t rhs) : m_e(rhs) {}
        half(const half& rhs) : m_e(rhs.m_e) {}
        half(float rhs) : m_e(f32to16(rhs)) {}
        half(double rhs) : m_e(f32to16((float)rhs)) {}
        half(long double rhs) : m_e(f32to16((float)rhs)) {}

        half& operator=(fp16_t rhs)
        {
            m_e = rhs;
            return *this;
        }
        half& operator=(const half& rhs)
        {
            m_e = rhs.m_e;
            return *this;
        }
        half& operator=(float rhs)
        {
            m_e = f32to16(rhs);
            return *this;
        }
        half& operator=(double rhs)
        {
            m_e = f32to16((float)rhs);
            return *this;
        }
        half& operator=(long double rhs)
        {
            m_e = f32to16((float)rhs);
            return *this;
        }

#define DEC_OP(OP)                                   \
    half& operator OP##=(const half& rhs)            \
    {                                                \
        m_e = f32to16((float)(*this)OP(float)(rhs)); \
        return *this;                                \
    }

        DEC_OP(+)
        DEC_OP(-)
        DEC_OP(*)
        DEC_OP(/ )

#undef DEC_OP

        operator fp16_t() const
        {
            return m_e;
        }
        operator float() const { return f16to32(m_e); }
        operator double() const { return (double)f16to32(m_e); }
        operator long double() const { return (long double)f16to32(m_e); }
    private:
        fp16_t m_e;
    };

#define DEC_BIN_OP(OP) \
    half operator OP(const half& lhs, const half& rhs) { return half((float)(lhs)OP(float)(rhs)); }

    DEC_BIN_OP(+)
    DEC_BIN_OP(-)
    DEC_BIN_OP(*)
    DEC_BIN_OP(/)

#undef DEC_OP
}

//--------------------------------------------
//from ILM's half
#if 0
#include <limits>
namespace std{
  template <>
  class numeric_limits <kaze::half>
  {
    public:

      static const bool is_specialized = true;

      static half min () throw () {return HALF_NRM_MIN;}
      static half max () throw () {return HALF_MAX;}

      static const int digits = HALF_MANT_DIG;
      static const int digits10 = HALF_DIG;
      static const bool is_signed = true;
      static const bool is_integer = false;
      static const bool is_exact = false;
      static const int radix = HALF_RADIX;
      static half epsilon () throw () {return HALF_EPSILON;}
      static half round_error () throw () {return HALF_EPSILON / 2;}

      static const int min_exponent = HALF_MIN_EXP;
      static const int min_exponent10 = HALF_MIN_10_EXP;
      static const int max_exponent = HALF_MAX_EXP;
      static const int max_exponent10 = HALF_MAX_10_EXP;

      static const bool has_infinity = true;
      static const bool has_quiet_NaN = true;
      static const bool has_signaling_NaN = true;
      static const float_denorm_style has_denorm = denorm_present;
      static const bool has_denorm_loss = false;
      static half infinity () throw () {return half::posInf();}
      static half quiet_NaN () throw () {return half::qNan();}
      static half signaling_NaN () throw () {return half::sNan();}
      static half denorm_min () throw () {return HALF_MIN;}

      static const bool is_iec559 = false;
      static const bool is_bounded = false;
      static const bool is_modulo = false;

      static const bool traps = true;
      static const bool tinyness_before = false;
      static const float_round_style round_style = round_to_nearest;
  };
#endif

#endif