#include "parameter_map.h"

#include <memory>

#include "mapset_selection.h"
#define MAP_TYPE KAZE_MAP_TYPE

#include "lexical_cast.hpp"

namespace kaze
{

    namespace
    {
        struct val_type
        {
            std::string val;
            int rest;
        };
    }

    class parameter_map_imp
    {
    public:
        typedef MAP_TYPE<std::string, val_type> map_type;
        typedef map_type::iterator iterator;
        typedef map_type::const_iterator const_iterator;

    public:
        parameter_map_imp();
        ~parameter_map_imp();
        parameter_map_imp(const parameter_map_imp& rhs);
        parameter_map_imp& operator=(const parameter_map_imp& rhs);

        void set(const char* key, const char* val, int rest);
        void set_int(const char* key, int val, int rest);
        void set_double(const char* key, double val, int rest);
        void set_string(const char* key, const char* val, int rest);

        const char* get(const char* key, int rest) const;
        int get_int(const char* key, int def) const;
        double get_double(const char* key, double def) const;
        const char* get_string(const char* key, const char* def) const;

        void get_keys(std::vector<const char*>& keys) const;

    private:
        map_type map_;
    };

    parameter_map_imp::parameter_map_imp() {}
    parameter_map_imp::~parameter_map_imp() {}

    parameter_map_imp::parameter_map_imp(const parameter_map_imp& rhs) : map_(rhs.map_) {}

    parameter_map_imp& parameter_map_imp::operator=(const parameter_map_imp& rhs)
    {
        map_ = rhs.map_;
        return *this;
    }

    void parameter_map_imp::set(const char* key, const char* val, int rest)
    {
        val_type v;
        v.val = val;
        v.rest = rest;
        map_.insert(map_type::value_type(std::string(key), v));
    }

    void parameter_map_imp::set_int(const char* key, int val, int rest)
    {
        val_type v;
        v.val = lexical_cast<std::string>(val);
        v.rest = rest;
        map_.insert(map_type::value_type(std::string(key), v));
    }

    void parameter_map_imp::set_double(const char* key, double val, int rest)
    {
        val_type v;
        v.val = lexical_cast<std::string>(val);
        v.rest = rest;
        map_.insert(map_type::value_type(std::string(key), v));
    }

    void parameter_map_imp::set_string(const char* key, const char* val, int rest)
    {
        val_type v;
        v.val = val;
        v.rest = rest;
        map_.insert(map_type::value_type(std::string(key), v));
    }

    const char* parameter_map_imp::get(const char* key, int rest) const
    {
        std::string skey(key);

        const_iterator i = map_.find(skey);

        if (i != map_.end())
        { //find;
            if (rest & i->second.rest)
            {
                return i->second.val.c_str();
            }
            else
            {
                return NULL;
            }
        }
        else
        {
            return NULL;
        }
    }

    int parameter_map_imp::get_int(const char* key, int def) const
    {
        const char* str = this->get(key, RESTRICTION_INT);
        if (str != NULL)
        {
            return lexical_cast<int>(str);
        }
        else
        {
            return def;
        }
    }
    double parameter_map_imp::get_double(const char* key, double def) const
    {
        const char* str = this->get(key, RESTRICTION_DOUBLE);
        if (str != NULL)
        {
            return lexical_cast<double>(str);
        }
        else
        {
            return def;
        }
    }
    const char* parameter_map_imp::get_string(const char* key, const char* def) const
    {
        const char* str = this->get(key, RESTRICTION_STRING);
        if (str != NULL)
        {
            return str;
        }
        else
        {
            return def;
        }
    }

    void parameter_map_imp::get_keys(std::vector<const char*>& keys) const
    {
        for (const_iterator i = map_.begin(); i != map_.end(); i++)
        {
            keys.push_back(i->first.c_str());
        }
    }

    //-----------------------------------------------------------------------------
    //-----------------------------------------------------------------------------

    parameter_map::parameter_map() { imp_ = new parameter_map_imp(); }
    parameter_map::~parameter_map() { delete imp_; }
    parameter_map::parameter_map(const parameter_map& rhs)
    {
        imp_ = new parameter_map_imp(*(rhs.imp_));
    }
    parameter_map& parameter_map::operator=(const parameter_map& rhs)
    {
        if(&rhs != this)
        {
            std::unique_ptr<parameter_map_imp> ap(new parameter_map_imp(*(rhs.imp_)));
            delete imp_;
            imp_ = ap.release();
        }
        return *this;
    }

    void parameter_map::set(const char* key, const char* val, int rest) { imp_->set(key, val, rest); }
    void parameter_map::set_int(const char* key, int val, int rest) { imp_->set_int(key, val, rest); }
    void parameter_map::set_double(const char* key, double val, int rest) { imp_->set_double(key, val, rest); }
    void parameter_map::set_string(const char* key, const char* val, int rest) { imp_->set_string(key, val, rest); }

    const char* parameter_map::get(const char* key, int rest) const { return imp_->get(key, rest); }
    int parameter_map::get_int(const char* key, int def) const { return imp_->get_int(key, def); }
    double parameter_map::get_double(const char* key, double def) const { return imp_->get_double(key, def); }
    const char* parameter_map::get_string(const char* key, const char* def) const { return imp_->get_string(key, def); }

    void parameter_map::get_keys(std::vector<const char*>& keys) const { imp_->get_keys(keys); }
}
