#ifndef KAZE_RAY_H
#define KAZE_RAY_H

#include "types.h"
#include "values.h"

namespace kaze
{

    typedef int32_t ray_id_t;

    class ray
    {
    public:
        //typedef ray_id_t ray_id_t;
        typedef ray_id_t id_t;

        static ray_id_t get_ray_id();
        static ray_id_t get_ray_data(const vector3& org, const vector3& dir);

        inline static int get_phase(const vector3& dir)
        {
            int phase = 0;
            if (dir[0] < 0) phase |= 1;
            if (dir[1] < 0) phase |= 2;
            if (dir[2] < 0) phase |= 4;
            return phase;
        }

        inline static real safe_invert(real x)
        {
            if (std::abs(x) < values::epsilon())
            {
                if (x < 0)
                    return -values::far();
                else
                    return +values::far();
            }
            else
            {
                return real(1) / x;
            }
        }
        inline static vector3 safe_invert3(const vector3& v)
        {
            return vector3(safe_invert(v[0]), safe_invert(v[1]), safe_invert(v[2]));
        }

    public:
        ray(const ray& rhs) : org_(rhs.org_), dir_(rhs.dir_), idir_(rhs.idir_), data_(rhs.data_), time_(rhs.time_) {}

        ray(const ray& rhs, real progress) : org_(rhs.org_ + rhs.dir_ * progress), dir_(rhs.dir_), idir_(rhs.idir_), time_(rhs.time_)
        {
            data_ = get_ray_id() | rhs.phase();
        }

        ray(const vector3& org, const vector3& dir) : org_(org), dir_(dir), idir_(safe_invert3(dir)), data_(get_ray_data(org, dir)), time_(0) {}
        ray(const vector3& org, const vector3& dir, const vector3& idir) : org_(org), dir_(dir), idir_(idir), data_(get_ray_data(org, dir)), time_(0) {}

        ray(const vector3& org, const vector3& dir, real tm) : org_(org), dir_(dir), idir_(safe_invert3(dir)), data_(get_ray_data(org, dir)), time_(tm) {}

        ray& operator=(const ray& rhs)
        {
            org_ = rhs.org_;
            dir_ = rhs.dir_;
            idir_ = rhs.idir_;
            data_ = rhs.data_;
            time_ = rhs.time_;
            return *this;
        }

        //vector3& origin()           {return org_;}
        const vector3& origin() const { return org_; }

        //vector3& direction()           {return dir_;}
        const vector3& direction() const { return dir_; }

        //vector3& inversed_direction()           {return idir_;}
        const vector3& inversed_direction() const { return idir_; }

        ray_id_t phase() const { return data_ & 0x7; } //000111
        ray_id_t id() const { return data_; }
        ray_id_t hasdiff() const { return data_ & 0x8; } //001000
        ray_id_t data() const { return data_; }

        real time() const;

    protected:
        vector3 org_;
        vector3 dir_;
        vector3 idir_;
        ray_id_t data_;
        real time_;
    };

    inline bool operator==(const ray& lhs, const ray& rhs)
    {
        return (
            (lhs.data() == rhs.data()) &&
            (lhs.origin() == rhs.origin()) &&
            (lhs.direction() == rhs.direction()));
    }
}

#endif
