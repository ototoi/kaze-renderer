#include "compressed_uv.h"
#include <limits>

namespace kaze
{

    compressed_uv::compressed_uv(const vector2& rhs)
    {
        static const real cof = (real)std::numeric_limits<uint16>::max() - 1;

        real sx, sy, u, v;

        u = rhs[0];
        v = rhs[1];

        sx = floor(u);
        sy = floor(v);

        u = u - sx;
        v = v - sy;

        if (u < 0.0) u = real(0.0);
        if (u >= 1.0) u = real(1.0);
        if (v < 0.0) v = real(0.0);
        if (v >= 1.0) v = real(1.0);

        mv_[0] = (uint16)(cof * u);
        mv_[1] = (uint16)(cof * v);
    }

    compressed_uv& compressed_uv::operator=(const vector2& rhs)
    {
        static const real cof = (real)std::numeric_limits<uint16>::max() - 1;

        real sx, sy, u, v;

        u = rhs[0];
        v = rhs[1];

        sx = floor(u);
        sy = floor(v);

        u = u - sx;
        v = v - sy;

        if (u < 0.0) u = real(0.0);
        if (u >= 1.0) u = real(1.0);
        if (v < 0.0) v = real(0.0);
        if (v >= 1.0) v = real(1.0);

        mv_[0] = (uint16)(cof * u);
        mv_[1] = (uint16)(cof * v);

        return *this;
    }

    vector2 barycentric_convert(real w, real u, real v, const compressed_uv& uv0, const compressed_uv& uv1, const compressed_uv& uv2)
    {
        static const real cof = real(1) / std::numeric_limits<compressed_uv::uint16>::max();
        return vector2(
            (w * uv0.mv_[0] + u * uv1.mv_[0] + v * uv2.mv_[0]) * cof,
            (w * uv0.mv_[1] + u * uv1.mv_[1] + v * uv2.mv_[1]) * cof);
    }
}
