#ifndef KAZE_FP16_VECTOR3_H
#define KAZE_FP16_VECTOR3_H

#include "fp16.h"
#include "types.h"

namespace kaze
{

    class fp16_vector3
    {
    public:
        fp16_vector3() {}

        fp16_vector3(const fp16_vector3& rhs)
        {
            mv_[0] = rhs.mv_[0];
            mv_[1] = rhs.mv_[1];
            mv_[2] = rhs.mv_[2];
        }

        explicit fp16_vector3(const vector3& rhs)
        {
            mv_[0] = f32to16((float)rhs[0]);
            mv_[1] = f32to16((float)rhs[1]);
            mv_[2] = f32to16((float)rhs[2]);
        }

#define DEF_CONS(TYPE)                       \
    fp16_vector3(TYPE a, TYPE b, TYPE c)     \
    {                                        \
        mv_[0] = f32to16((float)a);          \
        mv_[1] = f32to16((float)b);          \
        mv_[2] = f32to16((float)c);          \
    }                                        \
    explicit fp16_vector3(const TYPE rhs[3]) \
    {                                        \
        mv_[0] = f32to16((float)rhs[0]);     \
        mv_[1] = f32to16((float)rhs[1]);     \
        mv_[2] = f32to16((float)rhs[2]);     \
    }

        DEF_CONS(float)
        DEF_CONS(double)
        DEF_CONS(long double)

#undef DEF_CONS

        fp16_vector3& operator=(const fp16_vector3& rhs)
        {
            mv_[0] = rhs.mv_[0];
            mv_[1] = rhs.mv_[1];
            mv_[2] = rhs.mv_[2];

            return *this;
        }

        fp16_vector3& operator=(const vector3& rhs)
        {
            mv_[0] = f32to16((float)rhs[0]);
            mv_[1] = f32to16((float)rhs[1]);
            mv_[2] = f32to16((float)rhs[2]);

            return *this;
        }

        fp16_vector3& negate()
        {
            mv_[0] = f16negate(mv_[0]);
            mv_[1] = f16negate(mv_[1]);
            mv_[2] = f16negate(mv_[2]);

            return *this;
        }

        operator vector3() const
        {
            return vector3((real)f16to32(mv_[0]), (real)f16to32(mv_[1]), (real)f16to32(mv_[2]));
        }

        size_t size() const { return 3; }
        float operator[](std::size_t i) const { return f16to32(mv_[i]); }
    private:
        fp16_t mv_[3];
    };

    inline fp16_vector3 operator-(const fp16_vector3& rhs)
    {
        return fp16_vector3(rhs).negate();
    }
}

#endif