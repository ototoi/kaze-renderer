#include "newton_solver.h"
#include <vector>
#include <math.h>
#include <limits>
#include <algorithm>

#define MAX_FUNC 16
#define MAX_ITER 64

namespace kaze
{

    static const double DEF_EPS = std::numeric_limits<double>::epsilon();
    static const double PI = 3.14159265358979323846;

    static double root3(double v)
    {
        return ((v < 0) ? -1 : 1) * pow(fabs(v), 1.0 / 3.0);
    }

    static double calc(const double coeff[], int n, double x)
    {
        double r = 0;
        for (int i = 0; i < n; i++)
        {
            r += coeff[i];
            r *= x;
        }
        r += coeff[n];
        return r;
    }

    int newton_solver::solve_newton(double root[], const double coeff[], const double tcoeff[], int n, double x0, double x1) const
    {
        const double EPSILON = eps_;
        const int MAX_ITERATIONS = iter_;

        double f0 = calc(coeff, n, x0);
        double f1 = calc(coeff, n, x1);

        if ((f0 < -EPSILON && f1 < -EPSILON) || (f0 > EPSILON && f1 > EPSILON)) return 0;

        double xn = 0.5 * (x0 + x1);

        int c = 0;
        while (c++ < MAX_ITERATIONS)
        {
            double f = calc(coeff, n, xn);
            if (fabs(f) < EPSILON) break;
            double tf = calc(tcoeff, n - 1, xn);
            if (fabs(tf) < EPSILON)
            {
                return 0;
            }
            else
            {
                xn = xn - f / tf;
            }
        }
        *root = xn;
        return 1;
    }

    int newton_solver::solve2(double root[2], const double coeff[3], double x0, double x1) const
    {
        double rt[2];
        int nRet = solve2(rt, coeff);
        int k = 0;
        for (int i = 0; i < nRet; i++)
        {
            if (x0 <= rt[i] && rt[i] < x1)
            {
                root[k] = rt[i];
                k++;
            }
        }
        return k;
    }

    int newton_solver::solve3(double root[3], const double coeff[4], double x0, double x1) const
    {
        double rt[3];
        int nRet = solve3(rt, coeff);
        int k = 0;
        for (int i = 0; i < nRet; i++)
        {
            if (x0 <= rt[i] && rt[i] < x1)
            {
                root[k] = rt[i];
                k++;
            }
        }
        return k;
    }

    int newton_solver::solve2(double root[2], const double coeff[3]) const
    {
        double EPS = eps_;

        double A = coeff[0];
        double B = coeff[1];
        double C = coeff[2];

        if (fabs(A) <= EPS)
        {
            double x = -C / B;
            root[0] = x;
            return 1;
        }
        else
        {
            double D = B * B - 4 * A * C;
            if (D < 0)
            {
                return 0;
            }
            else if (D == 0)
            {
                double x = -0.5 * B / A;
                root[0] = x;
                return 1;
            }
            else
            {
#if 0
                double sqrD = sqrt(D);
                double invA = 0.5/A;
                double xa = invA*(-B - sqrD);
                double xb = invA*(-B + sqrD);
                if(xa>xb)std::swap(xa,xb);
                root[0] = xa;
                root[1] = xb;
                return 2;
#else
                double x1 = (fabs(B) + sqrt(D)) / (2.0 * A);
                if (B >= 0.0)
                {
                    x1 = -x1;
                }
                double x2 = C / (A * x1);
                if (x1 > x2) std::swap(x1, x2);
                root[0] = x1;
                root[1] = x2;
                return 2;
#endif
            }
        }
    }
    int newton_solver::solve3(double root[3], const double coeff[4]) const
    {
        double EPS = eps_;

        if (fabs(coeff[0]) <= EPS)
        {
            return solve2(root, coeff + 1);
        }
        else
        {
            double A = coeff[1] / coeff[0];
            double B = coeff[2] / coeff[0];
            double C = coeff[3] / coeff[0];

            if (fabs(C) <= EPS)
            {
                const double tcoeff[3] = {1, A, B};
                return solve2(root, tcoeff);
            }
            else
            {
                double Q = (3 * B - A * A) / 9;
                double R = (9 * A * B - 27 * C - 2 * A * A * A) / 54;
                double D = Q * Q * Q + R * R; // polynomial discriminant

                if (D > 0)
                { // complex or duplicate roots
                    double sqrtD = sqrt(D);
                    double S = root3(R + sqrtD);
                    double T = root3(R - sqrtD);
                    double x = (-A / 3 + (S + T)); // one real root
                    root[0] = x;
                    return 1;
                }
                else
                { // 3 real roots
                    double th = acos(R / sqrt(-(Q * Q * Q)));
                    double sqrtQ = sqrt(-Q);
                    double x = (2 * sqrtQ * cos(th / 3) - A / 3);
                    double y = (2 * sqrtQ * cos((th + 2 * PI) / 3) - A / 3);
                    double z = (2 * sqrtQ * cos((th + 4 * PI) / 3) - A / 3);
                    int nRet = 0;

                    root[nRet] = x;
                    nRet++;

                    root[nRet] = y;
                    nRet++;

                    root[nRet] = z;
                    nRet++;

                    return nRet;
                }
            }
        }
    }

    int newton_solver::solve_internal(double root[], const double coeff[], int n, double x0, double x1) const
    {
        double EPS = eps_;

        if (n == 2)
        {
            return solve2(root, coeff);
        }
        else if (n == 3)
        {
            return solve3(root, coeff);
        }
        else
        {
            if (fabs(coeff[0]) <= EPS)
            {
                return solve_internal(root, coeff + 1, n - 1, x0, x1);
            }
            else
            {
                double tcoeff[MAX_FUNC] = {0};
                double troot[MAX_FUNC + 1] = {0}; //4Ax3+3Bx2+2Cx + D
                for (int i = 0; i < n; i++)
                { // = {4.0*coeff[0],3.0*coeff[1],2.0*coeff[2],1.0*coeff[3]};
                    tcoeff[i] = (n - i) * coeff[i];
                }

                int k = solve_internal(troot + 1, tcoeff, n - 1, x0, x1);
                troot[0] = x0;
                troot[k + 1] = x1;
                std::sort(troot, troot + k + 2);
                int nRet = 0;
                for (int i = 0; i <= k; i++)
                {
                    double rt;
                    if (solve_newton(&rt, coeff, tcoeff, n, troot[i], troot[i + 1]))
                    {
                        root[nRet] = rt;
                        nRet++;
                    }
                }
                return nRet;
            }
        }
    }

    int newton_solver::solve(double root[], const double coeff[], int n, double x0, double x1) const
    {
        if (n == 2)
        {
            return solve2(root, coeff, x0, x1);
        }
        else if (n == 3)
        {
            return solve3(root, coeff, x0, x1);
        }
        else
        {
            double rt[MAX_FUNC + 1] = {0};
            int nRet = solve_internal(rt, coeff, n, x0, x1);
            int k = 0;
            for (int i = 0; i < nRet; i++)
            {
                if (x0 <= rt[i] && rt[i] < x1)
                {
                    root[k] = rt[i];
                    k++;
                }
            }
            return k;
        }
    }

    newton_solver::newton_solver() : eps_(DEF_EPS), iter_(MAX_ITER) {}
    newton_solver::newton_solver(double epsilon, int iteration) : eps_(epsilon), iter_(iteration) {}
}
