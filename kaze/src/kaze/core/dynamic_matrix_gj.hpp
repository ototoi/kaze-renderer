#ifndef KAZE_DYNAMIC_MATRIX_GJ_HPP
#define KAZE_DYNAMIC_MATRIX_GJ_HPP

#include "dynamic_matrix.hpp"

namespace kaze
{

    template <class T> //gauss joldan
    bool invert_gj(dynamic_matrix<T>& out, const dynamic_matrix<T>& rhs)
    { //invert_gj
        if (rhs.rows() != rhs.cols()) return false;
        int i, j, k;
        T t, u, det;

        int Sz = (int)rhs.rows();

        out = rhs;

        det = T(1);
        for (k = 0; k < Sz; k++)
        {
            t = out[k][k];
            det *= t;
            for (i = 0; i < Sz; i++)
                out[k][i] /= t;
            out.at(k, k) = T(1) / t;
            for (j = 0; j < Sz; j++)
            {
                if (j != k)
                {
                    u = out[j][k];
                    for (i = 0; i < Sz; i++)
                        if (i != k)
                            out[j][i] -= out[k][i] * u;
                        else
                            out[j][i] = -u / t;
                }
            }
        }

        if (det != T(0))
            return true;
        else
            return false;
    }
}

#endif
