#ifndef KAZE_PHOTONMAP_H
#define KAZE_PHOTONMAP_H

#include "types.h"
#include "photon.h"
#include "aggregator.hpp"

namespace kaze
{

    class photonmap_imp;

    class photonmap
    {
    public:
        photonmap();
        ~photonmap();
        //---------------
        void reserve(size_t sz);
        void add(const photon& p);
        void construct();
        size_t size() const;
        bool search(aggregator<photon>* reg, const vector3& pos, real radius, size_t nmax = 1) const;

    private:
        //nocopy
        photonmap(const photonmap&);
        photonmap& operator=(const photonmap&);

    private:
        photonmap_imp* imp_;
    };
}

#endif
