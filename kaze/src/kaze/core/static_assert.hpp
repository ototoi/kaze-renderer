#ifndef KAZE_STATIC_ASSERT_HPP
#define KAZE_STATIC_ASSERT_HPP

namespace kaze
{

    template <bool b>
    struct static_assert_struct;

    template <>
    struct static_assert_struct<true>
    {
    };
}

#endif