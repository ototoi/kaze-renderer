#ifndef KAZE_TEST_INFO_H
#define KAZE_TEST_INFO_H

#include "types.h"

#include <cstring>

namespace kaze
{
    class shader;
    class intersection;

    class test_info
    {
    public:
        test_info()
        {
            memset(this, 0, sizeof(test_info));
        }

        real get_distance() const { return distance; }
        void set_distance(real d) { distance = d; }

#define DEF_VEC(VEC)                                 \
    const vector3& get_##VEC() const { return VEC; } \
    void set_##VEC(const vector3& v) { VEC = v; }

        DEF_VEC(position)
        DEF_VEC(geometric)
        DEF_VEC(normal)
        DEF_VEC(tangent)
        DEF_VEC(binormal)
        DEF_VEC(coord)

#undef DEF_VEC

        const intersection* get_intersection() const
        {
            return p_intersection;
        }
        void set_intersection(const intersection* p) { p_intersection = p; }

        const shader* get_shader() const { return p_shader; }
        void set_shader(const shader* p) { p_shader = p; }

        const void* get_freearea() const { return (const void*)freearea; }
        void set_freearea(void* p, size_t sz) { memcpy(freearea, p, sz); }

    public:
        real distance; //8

        vector3 position;  //24
        vector3 geometric; //24 geometric_normal
        vector3 normal;    //24
        vector3 tangent;   //24
        vector3 binormal;  //24
        vector3 coord;     //24

        //plane
        bool b_planecoord; //
        vector3 p1;
        vector3 p2;
        vector3 p3;
        vector3 c1;
        vector3 c2;
        vector3 c3;
        //
        vector3 col1;
        vector3 col2;
        vector3 col3;

        const intersection* p_intersection; //4
        const shader* p_shader;             //4
        size_t i_attr;                      //4
        size_t index;                       //4

        unsigned char freearea[256]; //256 bytes free area !
    };
}

#endif
