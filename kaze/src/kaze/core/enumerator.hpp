#ifndef KAZE_ENUMERATOR_HPP
#define KAZE_ENUMERATOR_HPP

#include "types.h"

namespace kaze
{

    template <class T>
    class enumerator
    {
    public:
        typedef T value_type;

    public:
        virtual size_t size() const = 0;
        virtual const T& operator[](size_t idx) const { return this->get(idx); }
        virtual const T& get(size_t idx) const { return (*this)[idx]; }
    };
}

#endif