#ifndef KAZE_TRANSFORMER_H
#define KAZE_TRANSFORMER_H

#include "types.h"

namespace kaze
{

    class transformer
    {
    public:
        virtual vector3 transform_n(const vector3& v) const = 0;
        virtual vector3 transform_p(const vector3& v) const = 0;
        virtual vector3 transform_v(const vector3& v) const = 0;
        virtual ~transformer() {}
    };

    class no_transformer : public transformer
    {
    public:
        vector3 transform_n(const vector3& v) const;
        vector3 transform_p(const vector3& v) const;
        vector3 transform_v(const vector3& v) const;
    };

    class t_transformer : public transformer
    {
    public:
        explicit t_transformer(const vector3& t);
    public:
        vector3 transform_n(const vector3& v) const;
        vector3 transform_p(const vector3& v) const;
        vector3 transform_v(const vector3& v) const;

    private:
        vector3 t_;
    };

    class m3_transformer : public transformer
    {
    public:
        explicit m3_transformer(const matrix3& m);
    public:
        vector3 transform_n(const vector3& v) const;
        vector3 transform_p(const vector3& v) const;
        vector3 transform_v(const vector3& v) const;

    private:
        matrix3 m_;
        matrix3 im_;
    };

    class m3t_transformer : public transformer
    {
    public:
        m3t_transformer(const matrix3& m, const vector3& t);
        explicit m3t_transformer(const matrix4& m);
    public:
        vector3 transform_n(const vector3& v) const;
        vector3 transform_p(const vector3& v) const;
        vector3 transform_v(const vector3& v) const;

    private:
        matrix3 m_;
        matrix3 im_;
        vector3 t_;
        //vector3 it_;
    };

    class m4_transformer : public transformer
    {
    public:
        explicit m4_transformer(const matrix4& m);
    public:
        vector3 transform_n(const vector3& v) const;
        vector3 transform_p(const vector3& v) const;
        vector3 transform_v(const vector3& v) const;

    private:
        matrix4 m_;
        matrix4 im_;
    };

    class q_transformer : public transformer
    {
    public:
        explicit q_transformer(const quaternion& q) : m_(q), im_(~q) {}
    public:
        vector3 transform_n(const vector3& v) const;
        vector3 transform_p(const vector3& v) const;
        vector3 transform_v(const vector3& v) const;

    private:
        quaternion m_;
        quaternion im_;
    };
}

#endif
