#ifndef KAZE_PATH_STRING_H
#define KAZE_PATH_STRING_H

#ifdef _WIN32
#include <windows.h>
#include <mbstring.h>

namespace kaze
{

    std::string normalize_regalar_path(const std::string& str)
    {
#ifdef _WIN32
        DWORD sz;
        sz = (DWORD)(str.length() + 1);
        std::vector<char> buffer(sz);
        ::GetFullPathNameA(str.c_str(), sz, &(buffer[0]), NULL);
        return std::string(&(buffer[0]));
#else
        return str;
#endif
    }

    std::wstring normalize_regalar_path(const std::wstring& str)
    {
#ifdef _WIN32
        DWORD sz;
        sz = (DWORD)(str.length() + 1);
        std::vector<wchar_t> buffer(sz);
        ::GetFullPathNameW(str.c_str(), sz, &(buffer[0]), NULL);
        return std::wstring(&(buffer[0]));
#else
        return str;
#endif
    }

    //----------------------
    template <class CharT, class Traits = std::basic_traits<CharT> >
    class basic_path_string : public std::basic_string<CharT, Traits>
    {
    public:
        typedef std::basic_string<CharT, Traits> base_type;
        typedef basic_path_string<CharT, Traits> this_type;

    public:
        this_type& regular()
        {
            *this = normalize_regalar_path(*this);
            return *this;
        }
    };

    bool compare_path(const wchar_t* lhs, const wchar_t* rhs)
    {
#ifdef _WIN32
        return (0 == _wcsicmp(lhs, rhs)); //ignore
#else
        return (0 == wcscmp(lhs, rhs));
#endif
    }

    bool compare_path(const char* lhs, const char* rhs)
    {
#ifdef _WIN32
        return (0 == _mbsicmp((const unsigned char*)lhs, (const unsigned char*)rhs)); //ignore
#else
        return (0 == strcmp(lhs, rhs));
#endif
    }

    template <class C, class T>
    inline bool operator==(const basic_path_string<C, T>& lhs, const basic_path_string<C, T>& rhs)
    {
        return compare_path(lhs.c_str(), rhs.c_str());
    }

    typedef basic_path_string<char> path_string;
    typedef basic_path_string<wchar_t> path_wstring;
}
#endif

#endif
