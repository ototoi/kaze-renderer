#ifndef KAZE_MAP_SELECTION_H
#define KAZE_MAP_SELECTION_H

#ifdef _MSC_VER
#if _MSC_VER >= 1600
#define KAZE_USE_UNORDERED
#include <unordered_map>
#include <unordered_set>
#define KAZE_MAP_NAMESPACE std
#define KAZE_SET_NAMESPACE std
#else
#define KAZE_USE_HASH
#include <hash_map>
#include <hash_set>
#if _MSC_VER >= 1300 //vc7
#define KAZE_MAP_NAMESPACE stdext
#define KAZE_SET_NAMESPACE stdext
#else
#define KAZE_MAP_NAMESPACE std
#define KAZE_SET_NAMESPACE std
#endif
#endif
#else
#if defined(__GNUC__) && defined(__unix__)
#if defined(__GXX_EXPERIMENTAL_CXX0X__)
#define KAZE_USE_UNORDERED
#include <unordered_map>
#include <unordered_set>
#define KAZE_MAP_NAMESPACE std
#define KAZE_SET_NAMESPACE std
#elif !((__GNUC__ == 4 && __GNUC_MINOR__ >= 4) || (__GNUC__ >= 5))
#define KAZE_USE_HASH
#include <ext/hash_map>
#include <ext/hash_set>
#define KAZE_MAP_NAMESPACE __gnu_cxx
#define KAZE_SET_NAMESPACE __gnu_cxx
#endif
#endif
#endif

#if !(defined(KAZE_USE_HASH) || defined(KAZE_USE_UNORDERED))
#include <map>
#include <set>
#define KAZE_MAP_NAMESPACE std
#define KAZE_SET_NAMESPACE std
#endif

#ifdef KAZE_USE_HASH
#define KAZE_MAP_TYPE KAZE_MAP_NAMESPACE::hash_map
#define KAZE_SET_TYPE KAZE_MAP_NAMESPACE::hash_set
#elif defined(KAZE_USE_UNORDERED)
#define KAZE_MAP_TYPE KAZE_MAP_NAMESPACE::unordered_map
#define KAZE_SET_TYPE KAZE_MAP_NAMESPACE::unordered_set
#else
#define KAZE_MAP_TYPE KAZE_MAP_NAMESPACE::map
#define KAZE_SET_TYPE KAZE_MAP_NAMESPACE::set
#endif

#endif
