#ifndef KAZE_AGGREGATOR_HPP
#define KAZE_AGGREGATOR_HPP

/*
 * An Interface for aggregation
 */

namespace kaze
{
    template <class T>
    class aggregator
    {
    public:
        typedef T value_type;

    public:
        virtual ~aggregator() {}
        virtual void add(const T& rhs) = 0;
    };
}

#endif
