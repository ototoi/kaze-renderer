#ifndef KAZE_BIT_VECTOR_H
#define KAZE_BIT_VECTOR_H

/**
 *
 * @file   bit_vector.hpp
 * @author Toru Matsuoka
 *
 */

#include <iostream>
#include <sstream>

#include <bitset>

namespace kaze
{

    template <class T>
    class basic_bit_vector
    {
    public:
        typedef T block_t;
        typedef basic_bit_vector<T> this_type;
        static const int BLOCK_SIZE = sizeof(block_t) * 8;
        static const int BLOCK_SHIFT = sizeof(block_t) / 4 * 5;
        static const int BLOCK_MASK = (1 << BLOCK_SHIFT) - 1;

    public:
        basic_bit_vector(size_t sz = BLOCK_SIZE * 2)
            : sz_(sz), bsz_((sz + BLOCK_SIZE - 1) / BLOCK_SIZE), a_(0)
        {
            a_ = new block_t[bsz_]; //
            memset(a_, 0, sizeof(block_t) * bsz_);
        }

        basic_bit_vector(const this_type& rhs)
            : sz_(rhs.sz_), bsz_(rhs.bsz_), a_(0)
        {
            a_ = new block_t[bsz_]; //
            memcpy(a_, rhs.a_, sizeof(block_t) * bsz_);
        }

        template <size_t Bits>
        basic_bit_vector(const std::bitset<Bits>& rhs)
            : sz_(Bits), bsz_((Bits + BLOCK_SIZE - 1) / BLOCK_SIZE), a_(0)
        {
            a_ = new block_t[bsz_]; //
            memset(a_, 0, sizeof(block_t) * bsz_);
            for (size_t i = 0; i < Bits; i++)
            {
                if (rhs[i])
                    this->set1(i);
                else
                    this->set0(i);
            }
        }

        ~basic_bit_vector() { delete[] a_; }

        basic_bit_vector& operator=(const basic_bit_vector& rhs)
        {
            basic_bit_vector tmp(rhs);
            tmp.swap(*this);
            return *this;
        }

        //-------------------------------------
        void resize(size_t sz)
        {
            if (sz_ >= sz)
                return;
            basic_bit_vector tmp(sz);
            // block_t* pb = tmp.get_ptr();
            memcpy(tmp.get_ptr(), get_ptr(), sizeof(block_t) * bsz_);
            this->swap(tmp);
        }
        //-------------------------------------
        int get(size_t pos) const
        {
            size_t div = get_div(pos);
            size_t rem = get_rem(pos);
            return int((a_[div] >> rem) & 1);
        }
        void set(size_t pos, int bit)
        {
            if (bit)
                set1(pos);
            else
                set0(pos);
        }

        void set1(size_t pos)
        {
            if (pos > sz_)
                resize(pos + 1);
            size_t div = get_div(pos);
            size_t rem = get_rem(pos);
            set1(div, rem);
        }
        void set0(size_t pos)
        {
            if (pos > sz_)
                resize(pos + 1);
            size_t div = get_div(pos);
            size_t rem = get_rem(pos);
            set0(div, rem);
        }
        size_t graw_size(size_t pos)
        {
            size_t n = sz_;
            while (pos > n)
            {
                n *= 2;
            }
            return n;
        }

        int operator[](size_t pos) const { return get(pos); }

        size_t size() const { return sz_; }
        size_t block_size() const { return bsz_; }
        size_t allocation_size() const { return bsz_ * sizeof(block_t); }

        const block_t* get_ptr() const { return a_; }
        block_t* get_ptr() { return a_; }

        block_t& get_block(size_t pos) { return a_[pos]; }

        const block_t& get_block(size_t pos) const { return a_[pos]; }

    public:
        void swap(this_type& rhs)
        {
            std::swap(sz_, rhs.sz_);
            std::swap(bsz_, rhs.bsz_);
            std::swap(a_, rhs.a_);
        }

        basic_bit_vector& inv()
        {
            size_t bsz = bsz_;
            for (size_t i = 0; i < bsz; i++)
            {
                a_[i] = ~a_[i];
            }
            return *this;
        }

    protected:
        static size_t get_div(size_t pos)
        {
            // return pos/BLOCK_SIZE;
            return pos >> BLOCK_SHIFT;
        }
        static size_t get_rem(size_t pos)
        {
            // return pos%BLOCK_SIZE;
            return pos & BLOCK_MASK;
        }

        void set1(size_t div, size_t rem) { a_[div] |= block_t(1 << rem); }
        void set0(size_t div, size_t rem) { a_[div] &= block_t(~(1 << rem)); }

    protected:
        size_t sz_;  // size of bits
        size_t bsz_; // size of blocks
        block_t* a_; //
    };

    template <class T>
    basic_bit_vector<T> operator~(const basic_bit_vector<T>& rhs)
    {
        return basic_bit_vector<T>(rhs).inv();
    }

    template <class T, class CharT, class Traits>
    std::basic_ostream<CharT, Traits>&
    operator<<(std::basic_ostream<CharT, Traits>& os,
               const basic_bit_vector<T>& rhs)
    {
        std::basic_stringstream<CharT, Traits> s;

        s.flags(os.flags());
        s.imbue(os.getloc());
        s.precision(os.precision());

        size_t sz = rhs.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (rhs[i])
                s << '1';
            else
                s << '0';
        }

        return os << s.str();
    }

    template class basic_bit_vector<unsigned int>;
    template class basic_bit_vector<unsigned long long>;

    typedef basic_bit_vector<unsigned int> bit_vector;
}

#endif
