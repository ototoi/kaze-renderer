#include "spectrum.h"
#include "spectrum_component.h"

#include "color_convert.h"

#include "values.h"

#include <vector>
#include <memory>
#include <cassert>
#include <cstring>

namespace kaze
{

    const char* rgb_spectrum_component::IDSTRING = "rgb";
    const char* rgba_spectrum_component::IDSTRING = "rgba";
    const char* color_opacity_spectrum_component::IDSTRING = "color_opacity";
    const char* xyz_spectrum_component::IDSTRING = "xyz";
    const char* black_spectrum_component::IDSTRING = "black";
    const char* singlesample_spectrum_component::IDSTRING = "singlesample";
    const char* multisample_spectrum_component::IDSTRING = "multisample";
    const char* blackbody_spectrum_component::IDSTRING = "blackbody";

    const char* CiOi_spectrum_component::IDSTRING = "CiOi";

    //--------------------------------------------------------------------------------------

    rgb_spectrum_component::rgb_spectrum_component(real r, real g, real b)
    {
        c_[0] = r;
        c_[1] = g;
        c_[2] = b;
    }

    rgb_spectrum_component::rgb_spectrum_component(const color3& c) : c_(c) {}
    color3 rgb_spectrum_component::to_rgb() const { return c_; }
    spectrum_component* rgb_spectrum_component::clone() const { return new rgb_spectrum_component(c_); }
    const char* rgb_spectrum_component::id_string() const { return IDSTRING; }
    bool rgb_spectrum_component::equal(const spectrum_component* sc) const
    {
        return (static_cast<const rgb_spectrum_component*>(sc)->c_ == c_);
    }
    real rgb_spectrum_component::intensity() const
    {
        double R = c_[0];
        double G = c_[1];
        double B = c_[2];
        return real(0.2126 * R + 0.7152 * G + 0.0722 * B); //real(0.299*R + 0.587*G + 0.114*B);
    }
    void rgb_spectrum_component::add(const spectrum_component* sc) { add(sc->to_rgb()); }
    void rgb_spectrum_component::mul(const spectrum_component* sc) { mul(sc->to_rgb()); }
    void rgb_spectrum_component::mul(real rhs) { c_ *= rhs; }

    //--------------------------------------------------------------------------------------

    rgba_spectrum_component::rgba_spectrum_component(real r, real g, real b, real a)
    {
        c_[0] = r;
        c_[1] = g;
        c_[2] = b;
        c_[3] = a;
    }
    rgba_spectrum_component::rgba_spectrum_component(const color4& c) : c_(c) {}
    color3 rgba_spectrum_component::to_rgb() const { return color3(c_[0], c_[1], c_[2]); }
    spectrum_component* rgba_spectrum_component::clone() const { return new rgba_spectrum_component(c_); }
    const char* rgba_spectrum_component::id_string() const { return IDSTRING; }
    bool rgba_spectrum_component::equal(const spectrum_component* sc) const
    {
        return (static_cast<const rgba_spectrum_component*>(sc)->c_ == c_);
    }
    real rgba_spectrum_component::intensity() const
    {
        double R = c_[0];
        double G = c_[1];
        double B = c_[2];
        double A = c_[3];
        return real(A * (0.2126 * R + 0.7152 * G + 0.0722 * B)); //real(0.299*R + 0.587*G + 0.114*B);
    }
    void rgba_spectrum_component::add(const spectrum_component* sc) { add(sc->to_rgb()); }
    void rgba_spectrum_component::mul(const spectrum_component* sc) { mul(sc->to_rgb()); }
    void rgba_spectrum_component::mul(real rhs)
    {
        c_ *= color4(rhs, rhs, rhs, 1);
    }

    void rgba_spectrum_component::add(const color3& c)
    {
        c_ += color4(c[0], c[1], c[2], 0);
    }
    void rgba_spectrum_component::mul(const color3& c)
    {
        c_ *= color4(c[0], c[1], c[2], 1);
    }

    //--------------------------------------------------------------------------------------
    static color3 _get_color(const spectrum_component* sc)
    {
        return sc->to_rgb();
    }
    static color3 _get_opacity(const spectrum_component* sc)
    {
        if (strcmp(sc->id_string(), color_opacity_spectrum_component::IDSTRING) == 0)
        {
            return static_cast<const color_opacity_spectrum_component*>(sc)->opacity();
        }
        return color3(0, 0, 0);
    }

    color_opacity_spectrum_component::color_opacity_spectrum_component(const color3& c, const color3& o)
        : c_(c), o_(o)
    {
    }

    color3 color_opacity_spectrum_component::to_rgb() const { return c_; }
    spectrum_component* color_opacity_spectrum_component::clone() const { return new color_opacity_spectrum_component(c_, o_); }
    const char* color_opacity_spectrum_component::id_string() const { return IDSTRING; }
    bool color_opacity_spectrum_component::equal(const spectrum_component* sc) const
    {
        return (static_cast<const color_opacity_spectrum_component*>(sc)->c_ == c_) &&
               (static_cast<const color_opacity_spectrum_component*>(sc)->o_ == o_);
    }
    real color_opacity_spectrum_component::intensity() const
    {
        double R = c_[0] * o_[0];
        double G = c_[1] * o_[1];
        double B = c_[2] * o_[2];
        return real(0.2126 * R + 0.7152 * G + 0.0722 * B);
    }
    void color_opacity_spectrum_component::add(const spectrum_component* sc)
    {
        c_ += _get_color(sc);
        o_ += _get_opacity(sc);
    }
    void color_opacity_spectrum_component::mul(const spectrum_component* sc)
    {
        c_ *= _get_color(sc);
        o_ *= _get_opacity(sc);
    }
    void color_opacity_spectrum_component::mul(real rhs)
    {
        c_ *= rhs;
        o_ *= rhs;
    }

    color3 color_opacity_spectrum_component::opacity() const
    {
        return o_;
    }

    //--------------------------------------------------------------------------------------
    static color3 _get_Ci(const spectrum_component* sc)
    {
        if (strcmp(sc->id_string(), CiOi_spectrum_component::IDSTRING) == 0)
        {
            return static_cast<const CiOi_spectrum_component*>(sc)->Ci();
        }
        return sc->to_rgb();
    }
    static color3 _get_Oi(const spectrum_component* sc)
    {
        if (strcmp(sc->id_string(), CiOi_spectrum_component::IDSTRING) == 0)
        {
            return static_cast<const CiOi_spectrum_component*>(sc)->Oi();
        }
        return vector3(1, 1, 1); //
    }

    color3 CiOi_spectrum_component::get_Ci(const spectrum_component* sc) { return _get_Ci(sc); }
    color3 CiOi_spectrum_component::get_Oi(const spectrum_component* sc) { return _get_Oi(sc); }

    CiOi_spectrum_component::CiOi_spectrum_component(const color3& c, const color3& o)
        : Ci_(c), Oi_(o)
    {
    }

    static real _D(real a, real b)
    {
        if (b >= 1) return a;
        if (b <= 0) return 0;
        return a / b;
    }
    color3 CiOi_spectrum_component::to_rgb() const
    {
        return color3(_D(Ci_[0], Oi_[0]), _D(Ci_[1], Oi_[1]), _D(Ci_[2], Oi_[2]));
    }
    spectrum_component* CiOi_spectrum_component::clone() const { return new CiOi_spectrum_component(Ci_, Oi_); }
    const char* CiOi_spectrum_component::id_string() const { return IDSTRING; }
    bool CiOi_spectrum_component::equal(const spectrum_component* sc) const
    {
        return (static_cast<const CiOi_spectrum_component*>(sc)->Ci_ == Ci_) &&
               (static_cast<const CiOi_spectrum_component*>(sc)->Oi_ == Oi_);
    }
    real CiOi_spectrum_component::intensity() const
    {
        double R = Ci_[0];
        double G = Ci_[1];
        double B = Ci_[2];
        return real(0.2126 * R + 0.7152 * G + 0.0722 * B);
    }
    void CiOi_spectrum_component::add(const spectrum_component* sc)
    {
        Ci_ += _get_Ci(sc);
        Oi_ += _get_Oi(sc);
    }
    void CiOi_spectrum_component::mul(const spectrum_component* sc)
    {
        Ci_ *= _get_Ci(sc);
        Oi_ *= _get_Oi(sc);
    }
    void CiOi_spectrum_component::mul(real rhs)
    {
        Ci_ *= rhs;
        Oi_ *= rhs;
    }

    color3 CiOi_spectrum_component::Ci() const
    {
        return Ci_;
    }
    color3 CiOi_spectrum_component::Oi() const
    {
        return Oi_;
    }
    //--------------------------------------------------------------------------------------
    xyz_spectrum_component::xyz_spectrum_component(double x, double y, double z)
    {
        c_[0] = x;
        c_[1] = y;
        c_[2] = z;
    }
    xyz_spectrum_component::xyz_spectrum_component(const double* xyz)
    {
        memcpy(c_, xyz, sizeof(double) * 3);
    }
    xyz_spectrum_component::xyz_spectrum_component(const color3& c)
    {
        c_[0] = c[0];
        c_[1] = c[1];
        c_[2] = c[2];
    }
    color3 xyz_spectrum_component::to_rgb() const
    {
        double rgb[3];
        xyz_to_rgb(c_, rgb);
        return color3(rgb);
    }
    spectrum_component* xyz_spectrum_component::clone() const { return new xyz_spectrum_component(c_); }
    const char* xyz_spectrum_component::id_string() const { return IDSTRING; }
    bool xyz_spectrum_component::equal(const spectrum_component* sc) const
    {
        return (memcmp(static_cast<const xyz_spectrum_component*>(sc)->c_, c_, sizeof(double) * 3) == 0);
    }
    real xyz_spectrum_component::intensity() const
    {
        return real(c_[1]);
    }
    void xyz_spectrum_component::add(const spectrum_component* sc)
    {
        const double* c = static_cast<const xyz_spectrum_component*>(sc)->c_;
        c_[0] += c[0];
        c_[1] += c[1];
        c_[2] += c[2];
    }
    void xyz_spectrum_component::mul(const spectrum_component* sc)
    {
        const double* c = static_cast<const xyz_spectrum_component*>(sc)->c_;
        c_[0] *= c[0];
        c_[1] *= c[1];
        c_[2] *= c[2];
    }
    void xyz_spectrum_component::mul(real rhs)
    {
        c_[0] *= rhs;
        c_[1] *= rhs;
        c_[2] *= rhs;
    }

    //--------------------------------------------------------------------------------------

    black_spectrum_component::black_spectrum_component() {}
    color3 black_spectrum_component::to_rgb() const { return color3(0, 0, 0); }
    spectrum_component* black_spectrum_component::clone() const { return new black_spectrum_component(); }
    const char* black_spectrum_component::id_string() const { return IDSTRING; }
    bool black_spectrum_component::equal(const spectrum_component* sc) const { return true; }
    real black_spectrum_component::intensity() const { return 0; }
    void black_spectrum_component::add(const spectrum_component* sc) { ; }
    void black_spectrum_component::mul(const spectrum_component* sc) { ; }
    void black_spectrum_component::mul(real rhs) { ; }

    //--------------------------------------------------------------------------------------

    static inline bool operator==(
        const spectrum_pair& a,
        const spectrum_pair& b)
    {
        return a.wavelength == b.wavelength && a.value == b.value;
    }
    //--------------------------------------------------------------------------------------
    singlesample_spectrum_component::singlesample_spectrum_component(int w, float v)
    {
        pr_.wavelength = w;
        pr_.value = v;
    }
    singlesample_spectrum_component::singlesample_spectrum_component(const spectrum_pair& pr) : pr_(pr)
    {
    }

    color3 singlesample_spectrum_component::to_rgb() const
    {
        double xyz[3];
        wl_to_xyz(pr_.wavelength, xyz);
        xyz[0] *= pr_.value;
        xyz[1] *= pr_.value;
        xyz[2] *= pr_.value;
        double rgb[3];
        xyz_to_rgb(xyz, rgb);
        return color3(rgb);
    }
    spectrum_component* singlesample_spectrum_component::clone() const { return new singlesample_spectrum_component(pr_); }
    const char* singlesample_spectrum_component::id_string() const { return IDSTRING; }
    bool singlesample_spectrum_component::equal(const spectrum_component* sc) const
    {
        return (static_cast<const singlesample_spectrum_component*>(sc)->pr_ == pr_);
    }
    real singlesample_spectrum_component::intensity() const
    {
        double tmp[3];
        wl_to_xyz(pr_.wavelength, tmp);
        return tmp[1];
    }
    void singlesample_spectrum_component::add(const spectrum_component* sc)
    {
        const singlesample_spectrum_component* ssc = static_cast<const singlesample_spectrum_component*>(sc);
        if (pr_.wavelength == ssc->pr_.wavelength)
        {
            pr_.value += ssc->pr_.value;
        }
        else
        {
            assert(0);
        }
    }
    void singlesample_spectrum_component::mul(const spectrum_component* sc)
    {
        const singlesample_spectrum_component* ssc = static_cast<const singlesample_spectrum_component*>(sc);
        if (pr_.wavelength == ssc->pr_.wavelength)
        {
            pr_.value *= ssc->pr_.value;
        }
        else
        {
            pr_.value = 0;
        }
    }
    void singlesample_spectrum_component::mul(real rhs) { pr_.value *= (float)rhs; }

    bool singlesample_spectrum_component::is_addable(const singlesample_spectrum_component* ssc) const
    {
        return pr_.wavelength == ssc->pr_.wavelength;
    }

    const spectrum_pair& singlesample_spectrum_component::get_spectrum_pair() const
    {
        return pr_;
    }

    //--------------------------------------------------------------------------------------

    static void make_wavelengthes(std::vector<int>& wl, const std::vector<spectrum_pair>& a, const std::vector<spectrum_pair>& b)
    {
        size_t asz = a.size();
        size_t bsz = b.size();
        size_t sz = asz + bsz;
        wl.resize(sz);
        for (size_t i = 0; i < asz; i++)
            wl[i] = a[i].wavelength;
        for (size_t i = 0; i < bsz; i++)
            wl[asz + i] = b[i].wavelength;
        std::sort(wl.begin(), wl.end());
        wl.erase(std::unique(wl.begin(), wl.end()), wl.end());
    }
    static void expand_spectrum(std::vector<spectrum_pair>& mv, const std::vector<int>& wl, const std::vector<spectrum_pair>& old)
    {
        size_t sz = wl.size();
        size_t asz = old.size();
        mv.resize(sz);
        //-------------------------
        for (size_t i = 0; i < sz; i++)
        {
            mv[i].wavelength = wl[i];
            mv[i].value = 0;
        }
        //-------------------------
        size_t j = 0;
        for (size_t i = 0; i < sz; i++)
        {
            int w = wl[i];
            while (old[j].wavelength < w)
            {
                if (asz <= j) return;
                j++;
            }
            if (old[j].wavelength == w)
            {
                mv[i].value = old[j].value;
            }
        }
    }

    static void make_spectrum(
        std::vector<spectrum_pair>& aout,
        std::vector<spectrum_pair>& bout,
        const std::vector<spectrum_pair>& ain,
        const std::vector<spectrum_pair>& bin)
    {
        std::vector<int> wl;
        make_wavelengthes(wl, ain, bin);
        expand_spectrum(aout, wl, ain);
        expand_spectrum(bout, wl, bin);
    }

    static bool check_id(const spectrum_component* a, const spectrum_component* b)
    {
        return (strcmp(a->id_string(), b->id_string()) == 0);
    }

    bool multisample_spectrum_component::equal_wavelengthes(const multisample_spectrum_component* a, const multisample_spectrum_component* b)
    {
        size_t asz = a->mv_.size();
        size_t bsz = b->mv_.size();
        if (asz != bsz) return false;
        size_t sz = asz;
        for (size_t i = 0; i < sz; i++)
        {
            if (a->mv_[i].wavelength != b->mv_[i].wavelength) return false;
        }
        return true;
    }
    static const int START_WAVELENGTH = 360;
    static const int END_WAVELENGTH = 830;
    static const int SAMPLE_WIDTH = 10; //1,2,5,10,94,235:470=1*2*5*47:typeof((END_WAVELENGTH-START_WAVELENGTH)/SAMPLE_WIDTH) -> int
    static const int SAMPLE_NUMBERS = (END_WAVELENGTH - START_WAVELENGTH) / SAMPLE_WIDTH + 1;

    multisample_spectrum_component::multisample_spectrum_component()
    {
        int sz = SAMPLE_NUMBERS;
        int band = SAMPLE_WIDTH;
        mv_.resize(sz);
        for (int i = 0; i < sz; i++)
        {
            int w = 360 + i * band;
            mv_[i].wavelength = w;
            mv_[i].value = 0;
        }
    }
    multisample_spectrum_component::multisample_spectrum_component(const multisample_spectrum_component& rhs) : mv_(rhs.mv_)
    {
        ; //
    }
    multisample_spectrum_component::multisample_spectrum_component(const color3& c)
    {
        double rgb[3] = {c[0], c[1], c[2]};
        double xyz[3] = {0};
        rgb_to_xyz(rgb, xyz);
        int sz = SAMPLE_NUMBERS;
        int band = SAMPLE_WIDTH;
        mv_.resize(sz);
        for (int i = 0; i < sz; i++)
        {
            int w = 360 + i * band;
            mv_[i].wavelength = w;
            mv_[i].value = (float)(band * sample_wvalue(xyz, w));
        }
    }

    spectrum_component* multisample_spectrum_component::clone() const { return new multisample_spectrum_component(*this); }
    const char* multisample_spectrum_component::id_string() const { return IDSTRING; }
    bool multisample_spectrum_component::equal(const spectrum_component* sc) const
    {
        return (static_cast<const multisample_spectrum_component*>(sc)->mv_ == mv_);
    }
    color3 multisample_spectrum_component::to_rgb() const
    {
        size_t sz = mv_.size();
        double xyz[3] = {0};
        double tmp[3];
        for (size_t i = 0; i < sz; i++)
        {
            wl_to_xyz(mv_[i].wavelength, tmp);
            xyz[0] += tmp[0] * mv_[i].value;
            xyz[1] += tmp[1] * mv_[i].value;
            xyz[2] += tmp[2] * mv_[i].value;
        }
        double rgb[3];
        xyz_to_rgb(xyz, rgb);

        return color3(rgb); //
    }
    real multisample_spectrum_component::intensity() const
    {
        size_t sz = mv_.size();
        double Y = 0;
        double tmp[3];
        for (size_t i = 0; i < sz; i++)
        {
            wl_to_xyz(mv_[i].wavelength, tmp);
            Y += tmp[1] * mv_[i].value;
        }

        return Y;
    }

    void multisample_spectrum_component::add(const spectrum_component* sc)
    {
        assert(check_id(this, sc));
        const multisample_spectrum_component* ssc = static_cast<const multisample_spectrum_component*>(sc);
        if (equal_wavelengthes(this, ssc))
        { //equal
            size_t sz = this->mv_.size();
            for (size_t i = 0; i < sz; i++)
            {
                this->mv_[i].value += ssc->mv_[i].value;
            }
        }
        else
        {
            std::vector<spectrum_pair> amv;
            std::vector<spectrum_pair> bmv;
            make_spectrum(amv, bmv, this->mv_, ssc->mv_);
            size_t sz = amv.size();
            for (size_t i = 0; i < sz; i++)
            {
                amv[i].value += bmv[i].value;
            }
            this->mv_.swap(amv);
        }
    }

    void multisample_spectrum_component::mul(const spectrum_component* sc)
    {
        assert(check_id(this, sc));
        const multisample_spectrum_component* ssc = static_cast<const multisample_spectrum_component*>(sc);
        if (equal_wavelengthes(this, ssc))
        { //equal
            size_t sz = this->mv_.size();
            for (size_t i = 0; i < sz; i++)
            {
                this->mv_[i].value *= ssc->mv_[i].value;
            }
        }
        else
        {
            std::vector<spectrum_pair> amv;
            std::vector<spectrum_pair> bmv;
            make_spectrum(amv, bmv, this->mv_, ssc->mv_);
            size_t sz = amv.size();
            for (size_t i = 0; i < sz; i++)
            {
                amv[i].value *= bmv[i].value;
            }
            this->mv_.swap(amv);
        }
    }

    void multisample_spectrum_component::mul(real rhs)
    {
        size_t sz = mv_.size();
        for (size_t i = 0; i < sz; i++)
        {
            mv_[i].value *= (float)rhs;
        }
    }

    size_t multisample_spectrum_component::samples() const { return mv_.size(); }

    void multisample_spectrum_component::add(const color3& c)
    {
        multisample_spectrum_component tmp(c);
        this->add(&tmp);
    }

    void multisample_spectrum_component::mul(const color3& c)
    {
        multisample_spectrum_component tmp(c);
        this->mul(&tmp);
    }

    static int find_index(int w, const std::vector<spectrum_pair>& v)
    {
        int sz = (int)v.size();
        for (int i = 0; i < sz; i++)
        {
            if (v[i].wavelength == w) return i;
        }
        return -1;
    }

    void multisample_spectrum_component::add(const spectrum_pair& sp)
    {
        assert(0);
    }

    void multisample_spectrum_component::mul(const spectrum_pair& sp)
    {
        assert(0);
    }

    bool type_equal(const spectrum_component* a, const spectrum_component* b)
    {
        return (strcmp(a->id_string(), b->id_string()) == 0);
    }

    static bool is_black(const spectrum_component* a)
    {
        return (strcmp(a->id_string(), black_spectrum_component::IDSTRING) == 0);
    }
    static bool is_multi(const spectrum_component* a)
    {
        return (strcmp(a->id_string(), multisample_spectrum_component::IDSTRING) == 0);
    }

    static spectrum_component* spectrum_plus_othertype(const spectrum_component* a, const spectrum_component* b)
    {
        if (is_black(a))
        {
            return b->clone();
        }
        else if (is_black(b))
        {
            return a->clone();
        }
        else
        {
            multisample_spectrum_component* r = 0;
            if (is_multi(a))
            {
                r = reinterpret_cast<multisample_spectrum_component*>(a->clone());
            }
            else
            {
                r = new multisample_spectrum_component(a->to_rgb());
            }

            if (is_multi(b))
            {
                r->add(b);
            }
            else
            {
                r->add(b->to_rgb());
            }
            return r;
        }
    }

    spectrum_component* spectrum_plus(const spectrum_component* a, const spectrum_component* b)
    {
        if (type_equal(a, b))
        {
            spectrum_component* r = a->clone();
            r->add(b);
            return r;
        }
        else
        {
            return spectrum_plus_othertype(a, b);
        }
    }

    static spectrum_component* spectrum_multiply_othertype(const spectrum_component* a, const spectrum_component* b)
    {
        if (is_black(a))
        {
            return b->clone();
        }
        else if (is_black(b))
        {
            return a->clone();
        }
        else
        {
            multisample_spectrum_component* r = 0;
            if (is_multi(a))
            {
                r = reinterpret_cast<multisample_spectrum_component*>(a->clone());
            }
            else
            {
                r = new multisample_spectrum_component(a->to_rgb());
            }

            if (is_multi(b))
            {
                r->mul(b);
            }
            else
            {
                r->mul(b->to_rgb());
            }
            return r;
        }
    }

    spectrum_component* spectrum_multiply(const spectrum_component* a, const spectrum_component* b)
    {
        if (type_equal(a, b))
        {
            spectrum_component* r = a->clone();
            r->mul(b);
            return r;
        }
        else
        {
            return spectrum_multiply_othertype(a, b);
        }
    }

    //----------------------------------------------------------------------------------------------
    blackbody_spectrum_component::blackbody_spectrum_component() : t_(6500), v_(1) {}
    blackbody_spectrum_component::blackbody_spectrum_component(real t, real v) : t_(t), v_(v) {}

    color3 blackbody_spectrum_component::to_rgb() const
    {
        //static const double ro = 5.67e-8; //Stefan-Boltzmann law

        static const int wavelengths[] = {830, 800, 750, 700, 650, 600, 550, 500, 450, 400, 380};
        double xyz[3] = {0};
        double tmp[3];
        double vMax = 0;
        for (size_t i = 0; i < (sizeof(wavelengths) / sizeof(int)); i++)
        {
            wl_to_xyz(wavelengths[i], tmp);
            double v = sample(wavelengths[i]);
            if (vMax < v) vMax = v;
            //Ysum += tmp[1];
            //Ysum += v;
            xyz[0] += tmp[0] * v;
            xyz[1] += tmp[1] * v;
            xyz[2] += tmp[2] * v;
        }
        /*
		int n = (sizeof(wavelengths)/sizeof(int));
		vMax = 1.0/(vMax*n);
		xyz[0] *= vMax;
		xyz[1] *= vMax;
		xyz[2] *= vMax;	
		*/
        double rgb[3];
        xyz_to_rgb(xyz, rgb);

        double r = rgb[0];
        double g = rgb[1];
        double b = rgb[2];

        /*
		int n = (sizeof(wavelengths)/sizeof(int))-1;
		double I = ro*pow(t_,4);

		r = r*n*1e+6/I;
		g = g*n*1e+6/I;
		b = b*n*1e+6/I;
		*/
        //norm_rgb
        double max = std::max<double>(r, std::max<double>(g, b));
        r /= max;
        g /= max;
        b /= max;

        return v_ * color3(r, g, b);
    }

    spectrum_component* blackbody_spectrum_component::clone() const { return new blackbody_spectrum_component(t_, v_); }
    const char* blackbody_spectrum_component::id_string() const { return IDSTRING; }
    bool blackbody_spectrum_component::equal(const spectrum_component* sc) const
    {
        const blackbody_spectrum_component* ssc = static_cast<const blackbody_spectrum_component*>(sc);
        return (ssc->t_ == t_) && (ssc->v_ == v_);
    }
    real blackbody_spectrum_component::intensity() const
    {
        return rgb_spectrum_component(to_rgb()).intensity(); //TODO
    }
    void blackbody_spectrum_component::add(const spectrum_component* sc) {}
    void blackbody_spectrum_component::mul(const spectrum_component* sc) {}
    void blackbody_spectrum_component::mul(real rhs) { v_ *= rhs; }

    double blackbody_spectrum_component::sample(real l) const
    {
        static const double c = 299792458;      // speed of light
        static const double h = 6.62607095e-34; // Planck constant
        static const double k = 1.3807e-23;     // Boltzmann constant
        static const double pi = values::pi();

        static const double c1 = 2 * h * pi * c * c; //3.7418e-16
        static const double c2 = h * c / k;          //1.4388e-2

        const double lambda = l * 1e-9;

        //const double I = c1 * std::pow(lambda, -5.0) / (std::exp(c2 / (lambda * t_)) - 1.0);
        const double I = c1 * std::pow(lambda, -5.0) / (std::expm1(c2 / (lambda * t_)));
        
        return I;
    }
}
