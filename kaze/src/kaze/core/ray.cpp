#ifdef _MSC_VER
#pragma warning(disable : 4311)
#endif

#include "ray.h"

#include <mutex>

#ifdef _WIN32
#include <windows.h>
#endif

#define KAZE_RAY_HAVE_ID

namespace kaze
{
    namespace
    {

#if defined(_WIN32) && ((_WIN32_WINNT >= 0x0400) || (_WIN32_WINDOWS >= 0x0410))
        static volatile __declspec(align(32)) LONG id_seed = 0;
        LONG get_ray_id_()
        {
            return ::InterlockedExchangeAdd(&id_seed, (LONG)1); //return  (ray_id_t)::InterlockedIncrement(&id_seed)
        }
#elif(defined(__i686__) || defined(__APPLE__)) && defined(__GNUC__) && (__GNUC__ >= 4)
        static volatile uint32_t id_seed = 0;
        uint32_t get_ray_id_()
        {
            return __sync_fetch_and_add(&id_seed, 1);
        }
#elif(defined(__unix__) || defined(__APPLE__)) && defined(__i386__) && defined(__GNUC__)
        //from APR unix/atomic.c
        static uint32_t inline intel_atomic_add32(volatile uint32_t* mem, uint32_t val)
        {
            asm volatile("lock; xaddl %0,%1"
                         : "=r"(val), "=m"(*mem) /* outputs */
                         : "0"(val), "m"(*mem)   /* inputs */
                         : "memory", "cc");
            return val;
        }

        static volatile uint32_t id_seed = 0;
        uint32_t get_ray_id_()
        {
            return intel_atomic_add32(&id_seed, 1);
        }
#else
        static volatile int id_seed = 0;

        static std::mutex mtx; //
        std::mutex& get_mutex()
        {
            return mtx;
        }

        int get_ray_id_()
        {
            std::lock_guard<std::mutex> lck(get_mutex());
            int nRet = id_seed;
            id_seed++;
            return nRet;
        }
#endif
    }

    ray_id_t ray::get_ray_id()
    {
        return get_ray_id_() << 5;
    }

    ray_id_t ray::get_ray_data(const vector3& org, const vector3& dir)
    {
        return get_ray_id() | get_phase(dir);
    }

    real ray::time() const
    {
        return time_;
    }
}
