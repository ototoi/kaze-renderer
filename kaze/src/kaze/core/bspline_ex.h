#ifndef KAZE_BSPLINE_EX_H
#define KAZE_BSPLINE_EX_H

#include "types.h"
#include "bspline.h"
#include "dynamic_matrix.hpp"
#include "dynamic_matrix_gj.hpp"
#include "dynamic_matrix_lu.hpp"

#include <vector>

namespace kaze
{

    bool make_interpolate_bspline_matrix(dynamic_matrix<real>& out, std::vector<real>& knots, int N, int nOrder);
    template <class T>
    void make_interpolate_bspline_points(std::vector<T>& out, const dynamic_matrix<real>& m, const std::vector<T>& points)
    {
        int N = (int)points.size();
        std::vector<T> a(N);
        for (int i = 0; i < N; i++)
        {
            T tmp = m[i][0] * points[0];
            for (int j = 1; j < N; j++)
            {
                tmp += m[i][j] * points[j];
            }
            a[i] = tmp;
        }
        out.swap(a);
    }

    template <class T>
    void create_interpolate_bspline(std::vector<T>& cp, std::vector<real>& knots, const std::vector<T>& points, int nOrder)
    {
        int N = (int)points.size();
        dynamic_matrix<real> m(N, N);
        make_interpolate_bspline_matrix(m, knots, N, nOrder);
        make_interpolate_bspline_points(cp, m, points);
    }
}

#endif