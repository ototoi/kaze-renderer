#ifndef KAZE_DYNAMIC_MATRIX_HPP
#define KAZE_DYNAMIC_MATRIX_HPP

#include <algorithm>
#include <iostream>

namespace kaze
{

    template <class T>
    class dynamic_matrix
    {
    public:
        typedef T value_type;
        typedef dynamic_matrix<T> this_type;
        typedef T& ref_type;
        typedef const T& const_ref_type;

    public:
        dynamic_matrix(int rows, int cols)
            : ptr_(0), rows_(rows), cols_(cols)
        {
            alloc(rows_ * cols_);
        }

        dynamic_matrix(const this_type& rhs)
            : rows_(rhs.rows_), cols_(rhs.cols_)
        {
            alloc(rows_ * cols_);
            copy(rhs);
        }

        ~dynamic_matrix()
        {
            dealloc();
        }

        this_type& operator=(const this_type& rhs)
        {
            if (!(rows_ == rhs.rows_ && cols_ == rhs.cols_))
            {
                dealloc();
                rows_ = rhs.rows_;
                cols_ = rhs.cols_;
                alloc(rows_ * cols_);
            }
            copy(rhs);
            return *this;
        }

    public:
        int rows() const { return rows_; }
        int cols() const { return cols_; }
        T* operator[](int i) { return ptr_ + cols_ * i; }
        const T* operator[](int i) const { return ptr_ + cols_ * i; }
        T& at(int i, int j) { return ptr_[cols_ * i + j]; }
        const T& at(int i, int j) const { return ptr_[cols_ * i + j]; }
    protected:
        void alloc(size_t sz)
        {
            ptr_ = new T[sz];
        }
        void dealloc()
        {
            if (ptr_) delete ptr_;
        }
        void copy(const this_type& rhs)
        {
#ifdef _MSC_VER
#pragma warning(push)
#pragma warning(disable : 4996)
#endif
            std::copy(rhs.ptr_, rhs.ptr_ + rows_ * cols_, ptr_);
#ifdef _MSC_VER
#pragma warning(pop)
#endif
        }

    private:
        T* ptr_;
        int rows_;
        int cols_;
    };

    /** 
	 * ostream << 
	 */
    template <typename T, typename _CharT, class _Traits>
    std::basic_ostream<_CharT, _Traits>& operator<<(std::basic_ostream<_CharT, _Traits>& os, const dynamic_matrix<T>& rhs)
    {
        int RowSz = rhs.rows();
        int ColumnSz = rhs.cols();
        std::basic_ostringstream<_CharT, _Traits> s;
        s.flags(os.flags());
        s.imbue(os.getloc());
        s.precision(os.precision());
        s << "(";
        for (int i = 0; i < RowSz; i++)
        {
            s << "(";
            for (int j = 0; j < ColumnSz; ++j)
            {
                s << rhs.at(i, j);
                if (j != ColumnSz - 1)
                {
                    s << ",";
                }
            }
            s << ")";
            if (i != RowSz - 1)
            {
                s << ",";
            }
        }
        s << ")";
        return os << s.str();
    }
}

#endif