#include "socket_io.h"
#include "logger.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

#if defined(_MSC_VER)
#pragma comment(lib, "ws2_32.lib")
#else

#include <arpa/inet.h> // inet_addr()
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <errno.h>

#endif

namespace kaze
{
#ifdef _WIN32
    static void SockStartup()
    {
        WSADATA wsaData;
        ::WSAStartup(2, &wsaData);
    }
    static void SockCleanup()
    {
        ::WSACleanup();
    }

    socket_io::socket_io() : m_fd(0), m_port(0), m_addrString(), m_state(SOCKET_STATE_INIT)
    {
        SockStartup();
    }

    socket_io::socket_io(SOCKET fd) : m_fd(fd), m_port(0), m_addrString(), m_state(SOCKET_STATE_CONNECTED)
    {
        SockStartup();
    }

    socket_io::~socket_io()
    {
        if (m_state == SOCKET_STATE_CONNECTED)
        {
            this->close();
        }

        //assert(m_state == SOCKET_STATE_CLOSED);
        SockCleanup();
    }

    bool
    socket_io::connect(
        const char* addrString,
        int port)
    {

        SOCKET fd = socket(AF_INET, SOCK_STREAM, 0);
        if (fd == INVALID_SOCKET)
        {
            print_log("Invalid socket error.\n");
            return false;
        }

        struct sockaddr_in addr;
        memset((void*)&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        addr.sin_addr.S_un.S_addr = inet_addr(addrString);

        int ret = ::connect(fd, (struct sockaddr*)&addr, sizeof(addr));
        if (ret == SOCKET_ERROR)
        {
            print_log("Cannot establish a socket connection.\n");
            return false;
        }

        m_state = SOCKET_STATE_CONNECTED;
        m_fd = fd;

        return true;
    }

    bool
    socket_io::bind(
        int port)
    {
        SOCKET fd = socket(AF_INET, SOCK_STREAM, 0);
        if (fd == INVALID_SOCKET)
        {
            print_log("Invalid socket error.\n");
            return false;
        }

        struct sockaddr_in addr;
        memset((void*)&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        addr.sin_addr.s_addr = htonl(INADDR_ANY);

        int ret = ::bind(fd, (struct sockaddr*)&addr, sizeof(addr));
        if (ret < 0)
        {
            print_log("bind error. %d\n", WSAGetLastError());
            return false;
        }

        ret = ::listen(fd, 1);
        if (ret < 0)
        {
            print_log("listen error.\n");
            return false;
        }

        m_state = SOCKET_STATE_CONNECTED;
        m_fd = fd;

        return true;
    }

    SOCKET
    socket_io::accept(void)
    {
        struct sockaddr_in addr;
        int len = sizeof(addr);

        SOCKET fd = ::accept(m_fd, (struct sockaddr*)&addr, &len);

        if (fd == INVALID_SOCKET)
        {
            print_log("Invalid accept error.\n");
            return false;
        }
        return fd;
    }

    bool
    socket_io::close()
    {
        if (m_state != SOCKET_STATE_CONNECTED)
        {
            print_log("Invalid operation: socket_io::close().\n");
            return false; // Never come here.
        }

        ::closesocket(m_fd);

        m_state = SOCKET_STATE_CLOSED;

        return true;
    }

    bool
    socket_io::send(
        const void* buf,
        size_t len,
        int flags)
    {
        int size;
        int sent_size;
        int remainder;
        const char* ptr;

        remainder = (int)len;
        size = (int)len;
        ptr = (const char*)buf;

        if (m_state != SOCKET_STATE_CONNECTED)
        {
            return false;
        }

        while (1)
        {

            sent_size = ::send(m_fd, ptr, size, flags);
            remainder -= sent_size;
            if (remainder <= 0)
                break;

            size = remainder;
            ptr = (const char*)((unsigned char*)ptr + sent_size);
        }

        return true;
    }

    bool
    socket_io::recv(
        void* buf,
        size_t len,
        int flags)
    {
        int size;
        int recv_size;
        int remainder;
        char* ptr;

        remainder = (int)len;
        size = (int)len;
        ptr = (char*)buf;

        if (m_state != SOCKET_STATE_CONNECTED)
        {
            return false;
        }

        while (1)
        {

            recv_size = ::recv(m_fd, ptr, size, flags);
            remainder -= recv_size;
            if (remainder <= 0)
                break;

            size = remainder;
            ptr = (char*)((unsigned char*)ptr + recv_size);
        }

        return true;
    }

    bool socket_io::select()
    {
        fd_set fds;
        struct timeval tv = {0, 1000}; // 1 msec

        // TODO:
        FD_ZERO(&fds);
        FD_SET(m_fd, &fds);
        ::select((int)m_fd + 1, &fds, NULL, NULL, &tv);

        if (FD_ISSET(m_fd, &fds))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

#else

    socket_io::socket_io() : m_fd(0), m_port(0), m_addrString(), m_state(SOCKET_STATE_INIT)
    {
        ;
    }

    socket_io::socket_io(SOCKET fd) : m_fd(fd), m_port(0), m_addrString(), m_state(SOCKET_STATE_CONNECTED)
    {
        ;
    }

    socket_io::~socket_io()
    {
        if (m_state == SOCKET_STATE_CONNECTED)
        {
            this->close();
        }

        //assert(m_state == SOCKET_STATE_CLOSED);
    }

    bool
    socket_io::connect(
        const char* addrString,
        int port)
    {
        int fd = socket(AF_INET, SOCK_STREAM, 0);

        struct sockaddr_in addr;
        memset((void*)&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        addr.sin_addr.s_addr = inet_addr(addrString);

        int ret = ::connect(fd, (struct sockaddr*)&addr, sizeof(addr));
        if (ret != 0)
        {
            print_log("Cannot establish a socket connection: %s:%d %d\n", addrString, port, errno);
            return false;
        }

        m_state = SOCKET_STATE_CONNECTED;
        m_fd = fd;

        return true;
    }

    bool
    socket_io::bind(
        int port)
    {
        int fd = socket(AF_INET, SOCK_STREAM, 0);
        if (-1 == fd)
        {
            print_log("Invalid socket error. %d\n", errno);
            return false;
        }

        struct sockaddr_in addr;
        memset((void*)&addr, 0, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        addr.sin_addr.s_addr = htonl(INADDR_ANY);

        int ret = ::bind(fd, (struct sockaddr*)&addr, sizeof(addr));
        if (ret < 0)
        {
            print_log("bind error. %d\n", errno);
            return false;
        }

        ret = ::listen(fd, 1);
        if (ret < 0)
        {
            print_log("listen error.¥n");
            return false;
        }

        m_state = SOCKET_STATE_CONNECTED;
        m_fd = fd;

        return true;
    }

        bool
    socket_io::connect(
        const char* filename)
    {
        int fd = socket(AF_UNIX, SOCK_STREAM, 0);

        struct sockaddr_un addr;
        memset((void*)&addr, 0, sizeof(addr));
        addr.sun_family = AF_UNIX;
        strcpy(addr.sun_path, filename);

        int ret = ::connect(fd, (struct sockaddr*)&addr, sizeof(addr));
        if (ret != 0)
        {
            print_log("Cannot establish a socket connection: %s %d\n", filename, errno);
            return false;
        }

        m_state = SOCKET_STATE_CONNECTED;
        m_fd = fd;

        return true;
    }

    bool
    socket_io::bind(
        const char* filename)
    {
        int fd = socket(AF_UNIX, SOCK_STREAM, 0);
        if (-1 == fd)
        {
            print_log("Invalid socket error. %d\n", errno);
            return false;
        }

        struct sockaddr_un addr;
        memset((void*)&addr, 0, sizeof(addr));
        addr.sun_family = AF_UNIX;
        strcpy(addr.sun_path, filename);
        remove(addr.sun_path);

        int ret = ::bind(fd, (struct sockaddr*)&addr, sizeof(addr));
        if (ret < 0)
        {
            print_log("bind error. %d\n", errno);
            return false;
        }

        ret = ::listen(fd, 128);
        if (ret < 0)
        {
            print_log("listen error.¥n");
            return false;
        }

        m_state = SOCKET_STATE_CONNECTED;
        m_fd = fd;

        return true;
    }

    SOCKET
    socket_io::accept(void)
    {
        struct sockaddr_in addr;
        socklen_t len = sizeof(addr);

        int fd = ::accept(m_fd, (struct sockaddr*)&addr, &len);

        if (-1 == fd)
        {
            print_log("Invalid accept error. %d\n", errno);
            return false;
        }
        return fd;
    }

    bool
    socket_io::close()
    {
        if (m_state != SOCKET_STATE_CONNECTED)
        {
            print_log("Invalid operation: socket_io::close().\n");
            return false; // Never come here.
        }

        ::close(m_fd);

        m_state = SOCKET_STATE_CLOSED;

        return true;
    }

    bool
    socket_io::send(
        const void* buf,
        size_t len,
        int flags)
    {
        int size;
        int sent_size;
        int remainder;
        const void* ptr;

        remainder = len;
        size = len;
        ptr = buf;

        if (m_state != SOCKET_STATE_CONNECTED)
        {
            return false;
        }

        //
        // Since send() does not send all data in one send() call,
        // Invoke multiple send() call until all data was sent.
        //
        while (1)
        {

            sent_size = ::send(m_fd, ptr, size, flags);
            remainder -= sent_size;
            if (remainder <= 0)
                break;

            size = remainder;
            ptr = (const void*)((unsigned char*)ptr + sent_size);
        }

        return true;
    }

    bool
    socket_io::recv(
        void* buf,
        size_t len,
        int flags)
    {
        int size;
        int recv_size;
        int remainder;
        void* ptr;

        remainder = len;
        size = len;
        ptr = buf;

        if (m_state != SOCKET_STATE_CONNECTED)
        {
            return false;
        }

        while (1)
        {

            recv_size = ::recv(m_fd, ptr, size, flags);
            remainder -= recv_size;
            if (remainder <= 0)
                break;

            size = remainder;
            ptr = (void*)((unsigned char*)ptr + recv_size);
        }

        return true;
    }

    bool socket_io::select()
    {
        fd_set fds;
        struct timeval tv = {0, 1000}; // 1 msec

        // TODO: It'd be better to use Windows message event instead of select()
        FD_ZERO(&fds);
        FD_SET(m_fd, &fds);
        ::select(m_fd + 1, &fds, NULL, NULL, &tv);

        if (FD_ISSET(m_fd, &fds))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

#endif
}
