#ifndef KAZE_VALUES_H
#define KAZE_VALUES_H

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

#ifdef far
#undef far
#endif

#ifdef near
#undef near
#endif

#ifdef FAR
#undef FAR
#endif

#ifdef NEAR
#undef NEAR
#endif

#include <cstdlib>
#include <limits>

#include "types.h"

namespace kaze
{
    namespace values
    {
        inline real infinity() { return std::numeric_limits<real>::infinity(); }
        inline real maximum() { return std::numeric_limits<real>::max(); }
        inline real far() { return std::numeric_limits<real>::max() * real(1e-3); } //far != max
        inline real epsilon() { return std::numeric_limits<real>::epsilon(); }
        inline real near() { return std::numeric_limits<real>::epsilon() * real(1e+3); }
        inline real pi() { return (real)3.14159265358979323846; }
        inline real e() { return (real)2.71828183; }
        inline real less1() { return (real)1 - std::numeric_limits<real>::epsilon(); }

        inline real radians(real x) { return x * pi() / (real)180.0; }
        inline real degrees(real x) { return x * (real)180 / pi(); }

        inline size_t invalid_size() { return std::numeric_limits<size_t>::max(); }
    }
}

#endif
