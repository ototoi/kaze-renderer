#ifndef KAZE_RANDOM_H
#define KAZE_RANDOM_H

#include "types.h"
#include <cstdlib>

namespace kaze
{

    inline unsigned long xor128()
    {
        static unsigned long x = 123456789, y = 362436069, z = 521288629, w = 88675123;
        unsigned long t;
        t = (x ^ (x << 11));
        x = y;
        y = z;
        z = w;
        return (w = (w ^ (w >> 19)) ^ (t ^ (t >> 8)));
    }

    typedef unsigned long rand_t;

    class rand_generator
    {
    public:
        ~rand_generator() {}
        rand_t operator()() { return this->generate(); }

        virtual rand_t generate() = 0;
    };

    class xor128_generator : public rand_generator
    {
    public:
        xor128_generator(rand_t x = 123456789, rand_t y = 362436069, rand_t z = 521288629, rand_t w = 88675123) : x_(x), y_(y), z_(z), w_(w) {}

        virtual rand_t generate()
        {
            rand_t t;
            t = (x_ ^ (x_ << 11));
            x_ = y_;
            y_ = z_;
            z_ = w_;
            return (w_ = (w_ ^ (w_ >> 19)) ^ (t ^ (t >> 8)));
        }

    private:
        rand_t x_;
        rand_t y_;
        rand_t z_;
        rand_t w_;
    };
}

#endif