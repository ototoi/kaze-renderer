#include "QMC.h"

#include <cassert>
#include <vector>
#include <algorithm>

//from lucille
/* Prime numbers up to 100 dimension. */
static const int primes[] = {
    2, 3, 5, 7, 11, 13, 17, 19, 23, 29,               /*  10 */
    31, 37, 41, 43, 47, 53, 59, 61, 67, 71,           /*  20 */
    73, 79, 83, 89, 97, 101, 103, 107, 109, 113,      /*  30 */
    127, 131, 137, 139, 149, 151, 157, 163, 167, 173, /*  40 */
    179, 181, 191, 193, 197, 199, 211, 223, 227, 229, /*  50 */
    233, 239, 241, 251, 257, 263, 269, 271, 277, 281, /*  60 */
    283, 293, 307, 311, 313, 317, 331, 337, 347, 349, /*  70 */
    353, 359, 367, 373, 379, 383, 389, 397, 401, 409, /*  80 */
    419, 421, 431, 433, 439, 443, 449, 457, 461, 463, /*  90 */
    467, 479, 487, 491, 499, 503, 509, 521, 523, 541  /* 100 */
};

static const int nprimes = sizeof(primes) / sizeof(int);

static double halton_1(unsigned int base)
{
    double v = 0;
    double inv = 1.0 / 3;
    int n = base;
    for (double p = inv; n != 0; p *= inv, n /= 3)
    {
        int k = n % 3;
        v += (k * p);
    }
    return v;
}
static double halton_x(unsigned int d, unsigned int i)
{
    int base = primes[d];
    double v = 0;
    double inv = 1.0 / base;
    int n = i;
    for (double p = inv; n != 0; p *= inv, n /= base)
    {
        //v += sigma[n % base] * p;
    }
    return v;
}

namespace kaze
{

    int nextPrime(int lastPrime)
    {
        int newPrime = lastPrime + (lastPrime & 1) + 1;
        for (;;)
        {
            int dv = 3;
            bool ispr = true;
            while ((ispr) && (dv * dv <= newPrime))
            {
                ispr = ((newPrime % dv) != 0);
                dv += 2;
            }
            if (ispr) break;
            newPrime += 2;
        }
        return newPrime;
    }

    double halton(unsigned int i, unsigned int base)
    {
        switch (i)
        {
        case 0:
            return ri_vdC(base);
        case 1:
            return halton_1(base);
        default:
            return halton_x(i, base);
        }
    }

    void hammersley2(double* out, int n)
    {
        double p, t;
        int k, kk;

        for (k = 0; k < n; k++)
        {
            t = 0;
            for (p = 0.5, kk = k; kk; p *= 0.5, kk >>= 1)
            {
                if (kk & 1)
                { /* kk mod 2 = 1        */
                    t += p;
                }
            }

            out[2 * k + 0] = (double)k / (double)n;
            out[2 * k + 1] = t;
        }
    }

    void hammersley2_r(double* out, int n, unsigned int base)
    {
        double p, t;
        int k, kk;

        base %= n;

        for (k = 0; k < n; k++)
        {
            t = 0;
            for (p = 0.5, kk = k; kk; p *= 0.5, kk >>= 1)
            {
                if (kk & 1)
                { /* kk mod 2 = 1        */
                    t += p;
                }
            }

            int kb = (k + base); //ri_S_n(k,base)%n;//(k+base);
            kb = (kb >= n) ? kb - n : kb;
            //assert(0<=kb && kb<n);

            out[2 * k + 0] = (double)(kb) / (double)n;
            out[2 * k + 1] = t;
        }
    }

    void hammersley2_s(double* out, int n, int nLook)
    {
        double p, t;
        int k, kk;

        if (nLook < 1) nLook = 1;

        int n2 = 0;
        while ((n2 += nLook) < n)
            ;
        std::vector<int> ka(n2);
        for (int i = 0; i < n2; i++)
            ka[i] = i;

        int* a = &ka[0];
        for (int i = 0; i < n2; i += nLook)
        {
            std::random_shuffle(a + i, a + i + nLook);
        }

        for (k = 0; k < n; k++)
        {
            t = 0;
            for (p = 0.5, kk = k; kk; p *= 0.5, kk >>= 1)
            {
                if (kk & 1)
                { /* kk mod 2 = 1        */
                    t += p;
                }
            }

            out[2 * k + 0] = (double)ka[k] / (double)n;
            out[2 * k + 1] = t;
        }
    }

    double halton2(int i)
    {
        unsigned long h, f;

        h = i & 1;
        f = 2;
        i >>= 1;

        while (i)
        {
            h <<= 1;
            h += (i & 1);
            i >>= 1;
            f <<= 1;
            h <<= 1;
            h += (i & 1);
            i >>= 1;
            f <<= 1;
            h <<= 1;
            h += (i & 1);
            i >>= 1;
            f <<= 1;
            h <<= 1;
            h += (i & 1);
            i >>= 1;
            f <<= 1;
        }

        return (double)h / (double)f;
    }

    double generalized_vdC(int i, int base, int** p)
    {
        double h = 0.0, f, factor;
        int perm;
        int digit;

        f = factor = 1.0 / (double)base;

        while (i > 0)
        {
            digit = i % base;

            /* Lookup permutation table. */
            perm = p[base - 1][digit];

            h += (double)perm * factor;
            i /= base;
            factor *= f;
        }

        return h;
    }

    double generalized_scrambled_halton(int i, int offset, int dim, int** p)
    {
        int prim;
        double val;

        assert(dim >= 1);
        assert(dim <= nprimes);

        /* dim'th prime number. */
        prim = primes[dim];
        val = generalized_vdC(i + offset, prim, p);

        return val;
    }

    double generalized_scrambled_hammersley(int i, int offset, int n, int dim, int** p)
    {

        assert(dim >= 1);
        assert(dim <= nprimes);

        int prim;
        int j;
        double val;

        j = i + offset;
        if (j > n)
        {
            j = (i + offset) % n;
        }

        if (dim == 1)
        {
            val = (double)(i + offset) / (double)n;
        }
        else
        {
            /* (dim-1)'th prime number. */
            prim = primes[dim - 1];
            val = generalized_vdC(j, prim, p);
        }

        return val;
    }
}