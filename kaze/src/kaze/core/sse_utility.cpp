#include "sse_utility.h"

#include "memory_utility.h"

#if defined(_WIN32) | defined(__GNUC__)
#include <xmmintrin.h>
#endif

namespace kaze
{

    void* sse_malloc(size_t sz)
    {
        return aligned_malloc(sz, 16);
    }

    void sse_free(void* p)
    {
        aligned_free(p);
    }

#ifdef KAZE_HAVE_SSE

    float* sse_neg(float* out, const float* a)
    {
        __m128 ma = _mm_load_ps(a);
        _mm_store_ps(out, _mm_mul_ps(ma, _mm_set1_ps(-1.0f)));
        return out;
    }

    float* sse_rcp(float* out, const float* a)
    {
        __m128 ma = _mm_load_ps(a);
        __m128 r = _mm_rcp_ps(ma);
        _mm_store_ps(out, _mm_mul_ps(r, _mm_sub_ps(_mm_set1_ps(2.0f), _mm_mul_ps(ma, r))));
        return out;
    }

    float* sse_add(float* out, const float* a, const float* b)
    {
        __m128 ma = _mm_load_ps(a);
        __m128 mb = _mm_load_ps(b);
        _mm_store_ps(out, _mm_add_ps(ma, mb));
        return out;
    }
    float* sse_sub(float* out, const float* a, const float* b)
    {
        __m128 ma = _mm_load_ps(a);
        __m128 mb = _mm_load_ps(b);
        _mm_store_ps(out, _mm_sub_ps(ma, mb));
        return out;
    }
    float* sse_mul(float* out, const float* a, const float* b)
    {
        __m128 ma = _mm_load_ps(a);
        __m128 mb = _mm_load_ps(b);
        _mm_store_ps(out, _mm_mul_ps(ma, mb));
        return out;
    }
    float* sse_div(float* out, const float* a, const float* b)
    {
        __m128 ma = _mm_load_ps(a);
        __m128 mb = _mm_load_ps(b);
#if 1
        __m128 mr = _mm_rcp_ps(mb);
        _mm_store_ps(out, _mm_mul_ps(ma, _mm_mul_ps(mr, _mm_sub_ps(_mm_set1_ps(2), _mm_mul_ps(ma, mr)))));
#else
        _mm_store_ps(out, _mm_div_ps(ma, mb));
#endif
        return out;
    }

    float* sse_div_ss(float* out, const float* a, const float* b)
    {
        __m128 ma = _mm_load_ss(a);
        __m128 mb = _mm_load_ss(b);
        _mm_store_ss(out, _mm_div_ss(ma, mb));
        //__m128 mr = _mm_rcp_ps(mb);
        //_mm_store_ss(out,_mm_mul_ss(ma,_mm_mul_ss(mr,_mm_sub_ss(_mm_set_ss(2.0f),_mm_mul_ss(ma,mr)))));
        return out;
    }

    float* sse_sum(float* out, const float* a)
    {
        float t = 0.0f;
        t += a[0];
        t += a[1];
        t += a[2];
        t += a[3];
        out[0] = t;
        return out;
        /*
		__m128 r = _mm_load_ps(a);
		__m128 x = _mm_shuffle_ps(r,r,_MM_SHUFFLE(1,0,3,2));	// 2, 1, 4, 3 
		x = _mm_add_ps(r,x);									// 6, 4, 6, 4 
		r = _mm_shuffle_ps(x,x,_MM_SHUFFLE(2,3,0,1));			// 4, 6, 4, 6 
		x = _mm_add_ps(r,x);									//10,10,10,10 sum of
		x = _mm_shuffle_ps(x,x,0);
		_mm_store_ps(out,x);
		return out;
*/
    }

    float* sse_normalize(float* out, const float* a)
    {
        __m128 ma = _mm_load_ps(a);
        __m128 r = _mm_mul_ps(ma, ma);                            //a0 only
        __m128 x = _mm_shuffle_ps(r, r, _MM_SHUFFLE(1, 0, 3, 2)); // 2, 1, 4, 3
        x = _mm_add_ps(r, x);                                     // 6, 4, 6, 4
        r = _mm_shuffle_ps(x, x, _MM_SHUFFLE(2, 3, 0, 1));        // 4, 6, 4, 6
        x = _mm_add_ps(r, x);                                     //10,10,10,10 //sum of x*x

        r = _mm_rsqrt_ss(x);
        //r = r*(1.5f - 0.5f  * x*r*r)
        r = _mm_mul_ss(r, _mm_sub_ss(_mm_set_ss(1.5f), _mm_mul_ss(_mm_set_ss(0.5f), _mm_mul_ss(ma, _mm_mul_ss(r, r)))));

        r = _mm_shuffle_ps(r, r, 0);
        _mm_store_ps(out, _mm_mul_ps(ma, r));
        return out;
    }

    float* sse_ss2ps(float* out, const float* a)
    {
        __m128 x = _mm_set1_ps(a[0]);
        _mm_store_ps(out, x);
        return out;
    }

#endif
}