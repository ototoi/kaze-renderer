#ifndef KAZE_COEFFECTOR_HPP
#define KAZE_COEFFECTOR_HPP

#include <vector>
#include <algorithm>

namespace kaze
{

    template <class T, class X = double>
    class coeffector
    {
    public:
        struct value_type
        {
            value_type(T v, X x = X(1)) : value(v), coeff(x) {}
            T value;
            X coeff;
        };
        typedef coeffector<T, X> this_type;

        static inline value_type neg(const value_type& a)
        {
            return value_type(a.value, -a.coeff);
        }

    public:
        coeffector(T v, X x = X(1))
        {
            vec_.push_back(value_type(v, x));
        }

        coeffector(const this_type& rhs) : vec_(rhs.vec_) {}

        this_type& operator=(const this_type& rhs)
        {
            vec_ = rhs.vec_;
            return *this;
        }

        this_type& add(const this_type& rhs)
        {
            size_t sz = rhs.vec_.size();
            for (size_t i = 0; i < sz; i++)
            {
                vec_.push_back(rhs.vec_[i]);
            }
            return *this;
        }

        this_type& sub(const this_type& rhs)
        {
            size_t sz = rhs.vec_.size();
            for (size_t i = 0; i < sz; i++)
            {
                vec_.push_back(neg(rhs.vec_[i]));
            }
            return *this;
        }

        this_type& negate()
        {
            size_t sz = vec_.size();
            for (size_t i = 0; i < sz; i++)
            {
                vec_[i].coeff *= -1;
            }
            return *this;
        }

        this_type& mul(X a)
        {
            size_t sz = vec_.size();
            for (size_t i = 0; i < sz; i++)
            {
                vec_[i].coeff *= a;
            }
            return *this;
        }

        this_type& mul(const this_type& rhs)
        {
            size_t lsz = vec_.size();
            size_t rsz = rhs.vec_.size();
            for (size_t j = 0; j < lsz; j++)
            {
                X tmp = X(0);
                for (size_t i = 0; i < rsz; i++)
                {
                    tmp += (rhs.vec_[i].value * rhs.vec_[i].coeff);
                }
                vec_[j].coeff *= tmp;
            }
            return *this;
        }

        size_t size() const { return vec_.size(); }
        value_type& operator[](size_t i) { return vec_[i]; }
        const value_type& operator[](size_t i) const { return vec_[i]; }
    private:
        std::vector<value_type> vec_;
    };

    template <class T, class X>
    inline coeffector<T, X> operator-(const coeffector<T, X>& rhs)
    {
        return coeffector<T, X>(rhs).negate();
    }

    template <class T, class X>
    inline coeffector<T, X> operator+(const coeffector<T, X>& lhs, const coeffector<T, X>& rhs)
    {
        return coeffector<T, X>(lhs).add(rhs);
    }

    template <class T, class X>
    inline coeffector<T, X> operator-(const coeffector<T, X>& lhs, const coeffector<T, X>& rhs)
    {
        return coeffector<T, X>(lhs).sub(rhs);
    }

    template <class T, class X>
    inline coeffector<T, X> operator*(const coeffector<T, X>& lhs, const X& rhs)
    {
        return coeffector<T, X>(lhs).mul(rhs);
    }

    template <class T, class X>
    inline coeffector<T, X> operator*(const X& lhs, const coeffector<T, X>& rhs)
    {
        return coeffector<T, X>(rhs).mul(lhs);
    }

    template <class T, class X>
    inline coeffector<T, X> operator*(const coeffector<T, X>& lhs, const coeffector<T, X>& rhs)
    {
        return coeffector<T, X>(lhs).mul(rhs);
    }
}

#endif
