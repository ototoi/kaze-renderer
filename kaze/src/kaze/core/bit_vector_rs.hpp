#ifndef KAZE_BIT_VECTOR_RS_HPP
#define KAZE_BIT_VECTOR_RS_HPP

/**
 *
 * @file   bit_vector_rs.hpp
 * @author Toru Matsuoka
 *
 */

#include "bit_vector.hpp"

#if defined(__APPLE__) || defined(__GNUC__)
#include <x86intrin.h>
#else
#include <intrin.h>
#endif

#include <cassert>
#include <cstring>

namespace kaze
{

    class bit_vector_pop_counter_base
    {
    public:
        static int get_byte(int r)
        {
            static const int popCountArray[] = {
                0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
                1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
                1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
                2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
                1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
                2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
                2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
                3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8};
            return popCountArray[r];
        }
        static int get(unsigned int r)
        {
            int nRet = 0;
            for (int i = 0; i < sizeof(unsigned char); i++)
            {
                nRet += get_byte(int((r >> (i * 8)) & 0xFF));
            }
            return nRet;
        }
    };

    template <class T>
    class bit_vector_pop_counter
    {
    public:
        static int get(T r)
        {
            return bit_vector_pop_counter_base::get(r);
        }
    };

    template <>
    class bit_vector_pop_counter<unsigned int>
    {
    public:
        static int get(unsigned int r)
        {
#if defined(__SSE_4_2__)
            return _mm_pop_cnt_u32(r);
#else
            return bit_vector_pop_counter_base::get(r);
#endif
        }
    };

    template <>
    class bit_vector_pop_counter<unsigned long long>
    {
    public:
        static int get(unsigned long long r)
        {
#if defined(__SSE_4_2__)
            return _mm_pop_cnt_u64(r);
#else
            return bit_vector_pop_counter_base::get(r & 0xFFFFFFFFUL) + bit_vector_pop_counter_base::get((r >> 32) & 0xFFFFFFFFUL);
#endif
        }
    };

    template <class T>
    class basic_bit_vector_rs : public basic_bit_vector<T>
    {
    public:
        typedef basic_bit_vector<T> base_type;
        typedef basic_bit_vector_rs<T> this_type;

        typedef typename base_type::block_t block_t;
        using base_type::BLOCK_SIZE;
        using base_type::size;
        using base_type::get_block;
        using base_type::block_size;

    public:
        basic_bit_vector_rs(const base_type& rhs)
            : base_type(rhs)
        {
            init_cache();
            recache();
        }
        basic_bit_vector_rs(const this_type& rhs)
            : base_type(rhs)
        {
            init_cache();
            recache();
        }
        ~basic_bit_vector_rs()
        {
            destroy_cache();
        }

        basic_bit_vector_rs& operator=(const basic_bit_vector_rs<T>& rhs)
        {
            this_type tmp(rhs);
            tmp.swap(*this);
            return *this;
        }

        void swap(this_type& rhs)
        {
            base_type::swap(rhs);
            swap_cache(rhs);
        }

        //-------------------------------------------------
        size_t size(int bit) const
        {
            if (bit)
                return size1();
            else
                return size0();
        }

        /**
     * @return count of 1.
     */
        size_t size1() const
        {
            return get_count_cache(block_size());
        }

        /**
     * @return count of 0.
     */
        size_t size0() const
        {
            return size() - size1();
        }
        //------------------------------------------------

        size_t allocation_size() const { return base_type::allocation_size() + allocation_cache_size(); }

        /**
        * recache internal data.
        */
        void recache()
        {
            recalc_cache();
        }

        size_t rank(size_t pos, int bit)
        {
            if (bit)
                return rank1(pos);
            else
                return rank0(pos);
        }

        size_t rank1(size_t pos) const
        {
            return less1(pos + 1);
        }

        size_t rank0(size_t pos) const
        {
            return less0(pos + 1);
        }

        size_t less(size_t pos, int bit) const
        {
            if (bit)
                return less1(pos);
            else
                return less0(pos);
        }
        //----------------------------
        size_t less1(size_t pos) const
        {
            return less1_fast(pos);
        }
        size_t less0(size_t pos) const
        {
            return pos - less1(pos);
        }
        //----------------------------
        void set(size_t pos, int bit)
        {
            if (bit)
                set1(pos);
            else
                set0(pos);
        }

        void set1(size_t pos)
        {
            size_t div = base_type::get_div(pos);
            size_t rem = base_type::get_rem(pos);
            set1_cache(div, rem);
        }
        void set0(size_t pos)
        {
            size_t div = base_type::get_div(pos);
            size_t rem = base_type::get_rem(pos);
            set0_cache(div, rem);
        }

        void set1_raw(size_t pos)
        {
            base_type::set1(pos);
        }
        void set0_raw(size_t pos)
        {
            base_type::set0(pos);
        }
        //-----------------------------
        size_t select(size_t pos, int bit) const
        {
            if (bit)
                return select1(pos);
            else
                return select0(pos);
        }

        size_t select1(size_t pos) const
        {
            return select1_cache(pos);
        }

        size_t select0(size_t pos) const
        {
            return select0_cache(pos);
        }

    protected:
        size_t less1_fast(size_t pos) const
        { //no pos
            size_t div = base_type::get_div(pos);
            size_t rem = base_type::get_rem(pos);
            return get_count_cache(div) + pop_count(get_block(div) & ((1 << rem) - 1));
        }

        size_t less1_basic(size_t pos) const
        {
            size_t div = base_type::get_div(pos);
            size_t rem = base_type::get_rem(pos);
            size_t r = 0;
            for (size_t i = 0; i < div; i++)
            {
                r += pop_count(get_block(i));
            }
            return r + pop_count(get_block(div) & ((1 << rem) - 1));
        }

        static int pop_count_byte(int r)
        {
            return bit_vector_pop_counter_base::get_byte(r);
        }

        static int pop_count(block_t r)
        {
            return bit_vector_pop_counter<block_t>::get(r);
        }
        //-------------------------------------------------

        static int select_block(block_t r, int p)
        {
            if (pop_count(r) <= p)
                return -1;

            int nRet = 0;
            int i = 0;
            int b;
            while (i < sizeof(block_t))
            {
                b = int((r >> (i * 8)) & 0xFF);
                int t = pop_count_byte(b);
                if (p < t)
                    break;
                p -= t;
                i++;
            }
            //if(!(0<=p&&p<8))return -1;
            nRet += 8 * i;
            nRet += select_byte(b, p);
            return nRet;
        }
        static int select_byte(unsigned char byte, int p)
        {
            assert(p < pop_count_byte(byte));

            static const int selectPosArray[] = {
                4, 4, 4, 4, 0, 4, 4, 4, 1, 4, 4, 4, 0, 1, 4, 4,
                2, 4, 4, 4, 0, 2, 4, 4, 1, 2, 4, 4, 0, 1, 2, 4,
                3, 4, 4, 4, 0, 3, 4, 4, 1, 3, 4, 4, 0, 1, 3, 4,
                2, 3, 4, 4, 0, 2, 3, 4, 1, 2, 3, 4, 0, 1, 2, 3};

            int lo = (byte)&0xF;
            int hi = (byte >> 4) & 0xF;
            int ln = pop_count_byte(lo);
            if (ln <= p)
            { //hi
                return 4 + selectPosArray[hi * 4 + (p - ln)];
            }
            else
            {
                return selectPosArray[lo * 4 + p];
            }
        }

    protected:
        size_t cache_size() const
        {
            return block_size() + 1;
        }
        size_t select1_cache(size_t pos) const
        {
            if (pos >= size1())
                return size();

            size_t p = 0;
            //--------------------------------------
            {
                size_t a = 0;
                size_t b = block_size();
                while (a + 1 < b)
                {
                    size_t m = (a + b) >> 1;
                    size_t c = get_count_cache(m);
                    if (c <= pos)
                    {
                        a = m;
                    }
                    else
                    {
                        b = m;
                    }
                }
                p = a;
            }
            //--------------------------------------

            size_t OFFSET = p * BLOCK_SIZE;
            size_t cnt = get_count_cache(p);
            int cnt_rem;
            if (pos < cnt || (cnt_rem = (int)(pos - cnt)) >= sizeof(block_t) * 8)
                return size();
            int pos_rem = select_block(get_block(p), cnt_rem);
            if (pos_rem < 0)
                return size();
            return OFFSET + pos_rem;
        }

        size_t select0_cache(size_t pos) const
        {
            if (pos >= size0())
                return size();

            size_t p = 0;
            //--------------------------------------
            {
                size_t a = 0;
                size_t b = block_size();
                while (a + 1 < b)
                {
                    size_t m = (a + b) >> 1;
                    size_t c = m * BLOCK_SIZE - get_count_cache(m);
                    if (c <= pos)
                    {
                        a = m;
                    }
                    else
                    {
                        b = m;
                    }
                }
                p = a;
            }
            //--------------------------------------

            size_t OFFSET = p * BLOCK_SIZE;
            size_t cnt = OFFSET - get_count_cache(p);
            int cnt_rem;
            if (pos < cnt || (cnt_rem = (int)(pos - cnt)) >= sizeof(block_t) * 8)
                return size();
            int pos_rem = select_block(~get_block(p), cnt_rem);
            if (pos_rem < 0)
                return size();
            return OFFSET + pos_rem;
        }

        static const int OFFSET_SIZE = (1 << sizeof(unsigned char) * 8) / BLOCK_SIZE; //8
        static const int OFFSET_SHIFT = (sizeof(block_t)) / 4 * 3;                    //3
        static const int OFFSET_MASK = (1 << OFFSET_SHIFT) - 1;

        static size_t get_div2(size_t i)
        {
            return i >> OFFSET_SHIFT;
        }
        static size_t get_rem2(size_t i)
        {
            return i & OFFSET_MASK;
        }

        size_t delta_size() const
        {
            return ((cache_size() + OFFSET_SIZE - 1) >> OFFSET_SHIFT) * OFFSET_SIZE;
        }
        size_t offset_size() const
        {
            return ((cache_size() + OFFSET_SIZE - 1) >> OFFSET_SHIFT) + 1;
        }

        size_t allocation_cache_size() const
        {
            return delta_size() * sizeof(unsigned char) + offset_size() * sizeof(size_t);
        }

        void init_cache()
        {
            size_t dsz = delta_size();
            size_t osz = offset_size();
            d_ = new unsigned char[dsz]; //delta
            o_ = new size_t[osz];        //
            memset(d_, 0, sizeof(unsigned char) * dsz);
            memset(o_, 0, sizeof(size_t) * osz);
        }
        void destroy_cache()
        {
            delete[] d_;
            delete[] o_;
        }

        void recalc_cache()
        {
            size_t bsz = block_size();
            size_t osz = offset_size();

            d_[0] = 0;
            for (size_t i = 0; i < bsz; i++)
            {
                d_[i + 1] = pop_count(get_block(i));
            }
            size_t r = 0;
            o_[0] = 0;
            for (size_t i = 0; i < osz - 1; i++)
            {
                size_t d = OFFSET_SIZE * i;
                int sum = 0;
                for (int k = 0; k < OFFSET_SIZE; k++)
                {
                    sum += d_[d + k];
                    d_[d + k] = sum;
                }
                r += sum;
                o_[i + 1] = r;
            }
        }

        void set1_cache(size_t div, size_t rem)
        {
            block_t old_blk = get_block(div);
            base_type::set1(div, rem); //<-------------
            block_t new_blk = get_block(div);
            if (old_blk != new_blk)
            {
                recalc_cache(div, old_blk, new_blk);
            }
        }
        void set0_cache(size_t div, size_t rem)
        {
            block_t old_blk = get_block(div);
            base_type::set0(div, rem); //<-------------
            block_t new_blk = get_block(div);
            if (old_blk != new_blk)
            {
                recalc_cache(div, old_blk, new_blk);
            }
        }

        void recalc_cache(size_t div, block_t old_blk, block_t new_blk)
        {
            int old_num = pop_count(old_blk);
            int new_num = pop_count(new_blk);
            int diff = new_num - old_num;

            //-------------------------------
            size_t pos = div + 1;
            size_t i = get_div2(pos); //o_[i]
            size_t p = get_rem2(pos);
            size_t d = OFFSET_SIZE * i;
            for (size_t k = p; k < OFFSET_SIZE; k++)
            {
                d_[d + k] += diff;
            }
            size_t osz = offset_size();
            for (size_t k = i + 1; k < osz; k++)
            {
                o_[k] += diff;
            }
        }

        size_t get_count_cache(size_t pos) const
        {
            size_t oi = get_div2(pos);
            return o_[oi] + d_[pos];
        }

        void swap_cache(this_type& rhs)
        {
            std::swap(d_, rhs.d_);
            std::swap(o_, rhs.o_);
        }

        unsigned char* d_;
        size_t* o_;
    };

    template class basic_bit_vector_rs<unsigned int>;
    template class basic_bit_vector_rs<unsigned long long>;

    typedef basic_bit_vector_rs<bit_vector::block_t> bit_vector_rs;
}

#endif
