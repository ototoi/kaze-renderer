#include "spectrum.h"
#include "spectrum_component.h"

#include "color.h"
#include "values.h"

#include <cstring>
#include <vector>
#include <memory>
#include <cassert>

namespace kaze
{

    spectrum::spectrum()
    {
        sc_ = new rgb_spectrum_component(color3(0, 0, 0));
    }

    spectrum::~spectrum()
    {
        delete sc_;
    }

    spectrum::spectrum(spectrum_component* sc)
    {
        sc_ = sc;
    }

    spectrum::spectrum(const spectrum& rhs)
    {
        sc_ = rhs.sc_->clone();
    }

    spectrum& spectrum::operator=(const spectrum& rhs)
    {
        if(&rhs != this)
        {
            std::unique_ptr<spectrum_component> ap(rhs.sc_->clone());
            delete sc_;
            sc_ = ap.release();
        }
        return *this;
    }

    spectrum& spectrum::operator*=(real rhs)
    {
        sc_->mul(rhs);
        return *this;
    }

    spectrum& spectrum::operator+=(const spectrum& rhs)
    {
        std::unique_ptr<spectrum_component> ap(spectrum_plus((*this).sc_, rhs.sc_));
        delete sc_;
        sc_ = ap.release();
        return *this;
    }

    spectrum& spectrum::operator*=(const spectrum& rhs)
    {
        std::unique_ptr<spectrum_component> ap(spectrum_multiply((*this).sc_, rhs.sc_));
        delete sc_;
        sc_ = ap.release();
        return *this;
    }

    bool spectrum::equal(const spectrum& rhs) const
    {
        if (type_equal(sc_, rhs.sc_))
        {
            return rhs.sc_->equal(sc_);
        }
        else
        {
            return false;
        }
    }

    real spectrum::intensity() const
    {
        return sc_->intensity();
    }

    //---------------------------------------------
    spectrum::spectrum(const color3& rhs)
    {
        sc_ = new rgb_spectrum_component(rhs);
    }
    spectrum::spectrum(real f)
    {
        sc_ = new rgb_spectrum_component(color3(f, f, f));
    }

    color3 spectrum::to_rgb() const
    {
        return sc_->to_rgb();
    }
    //---------------------------------------------
    spectrum operator+(const spectrum& lhs, const spectrum& rhs)
    {
        return spectrum(spectrum_plus(lhs.sc(), rhs.sc()));
    }

    spectrum operator*(const spectrum& lhs, const spectrum& rhs)
    {
        return spectrum(spectrum_multiply(lhs.sc(), rhs.sc()));
    }

    spectrum convert(const color3& c, const color3& o)
    {
        return spectrum(new color_opacity_spectrum_component(c, o));
    }

    real coverage(const spectrum& s)
    {
        if (strcmp(s.sc()->id_string(), rgba_spectrum_component::IDSTRING) == 0)
        {
            return static_cast<const rgba_spectrum_component*>(s.sc())->alpha();
        }
        else if (strcmp(s.sc()->id_string(), color_opacity_spectrum_component::IDSTRING) == 0)
        {
            color3 o = static_cast<const color_opacity_spectrum_component*>(s.sc())->opacity();
            return std::min<real>(1, std::max<real>(o[0], std::max<real>(o[1], o[2])));
        }
        else if (strcmp(s.sc()->id_string(), CiOi_spectrum_component::IDSTRING) == 0)
        {
            color3 o = static_cast<const CiOi_spectrum_component*>(s.sc())->Oi();
            return std::min<real>(1, std::max<real>(o[0], std::max<real>(o[1], o[2])));
        }
        else
        {
            return 1;
        }
    }

    color3 opacity(const spectrum& s)
    {
        if (strcmp(s.sc()->id_string(), rgba_spectrum_component::IDSTRING) == 0)
        {
            real a = static_cast<const rgba_spectrum_component*>(s.sc())->alpha();
            return color3(a, a, a);
        }
        else if (strcmp(s.sc()->id_string(), color_opacity_spectrum_component::IDSTRING) == 0)
        {
            return static_cast<const color_opacity_spectrum_component*>(s.sc())->opacity();
        }
        else
        {
            return color3(1, 1, 1);
        }
    }

    color3 get_Ci(const spectrum& s)
    {
        return CiOi_spectrum_component::get_Ci(s.sc());
    }
    color3 get_Oi(const spectrum& s)
    {
        return CiOi_spectrum_component::get_Oi(s.sc());
    }
}
