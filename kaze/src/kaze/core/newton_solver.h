#ifndef KAZE_NEWTON_SOLVER_H
#define KAZE_NEWTON_SOLVER_H

namespace kaze
{

    class newton_solver
    {
    public:
        int solve2(double root[2], const double coeff[3], double x0, double x1) const;
        int solve3(double root[3], const double coeff[4], double x0, double x1) const;
        int solve2(double root[2], const double coeff[3]) const;
        int solve3(double root[3], const double coeff[4]) const;
        int solve(double root[], const double coeff[], int n, double x0, double x1) const;
        int solve_newton(double root[], const double coeff[], const double tcoeff[], int n, double x0, double x1) const;

    protected:
        int solve_internal(double root[], const double coeff[], int n, double x0, double x1) const;

    public:
        newton_solver();
        newton_solver(double epsilon, int iteration);

    private:
        double eps_;
        int iter_;
    };
}

#endif
