#ifndef KAZE_DIFFERENTIAL_RAY_H
#define KAZE_DIFFERENTIAL_RAY_H

#include "ray.h"

namespace kaze
{

    class differential_ray : public ray
    {
    public:
        enum
        {
            DIFFERENTIAL_RAY_RIGHTBOTTOM, //00 = 0
            DIFFERENTIAL_RAY_LEFTBOTTOM,  //01 = 1
            DIFFERENTIAL_RAY_RIGHTTOP,    //10 = 2
            DIFFERENTIAL_RAY_LEFTTOP      //11 = 3
        };

    public:
        differential_ray(
            const ray& r,
            const ray& lt, const ray& rt,
            const ray& lb, const ray& rb) : ray(r), lt_(lt), rt_(rt), lb_(lb), rb_(rb)
        {
            data_ |= 8;
        }

        differential_ray& operator=(const differential_ray& rhs)
        {
            ray::operator=(rhs);
            lt_ = rhs.lt_;
            rt_ = rhs.rt_;
            lb_ = rhs.lb_;
            rb_ = rhs.rb_;
            return *this;
        }

        const ray& diffrential(int nPos) const
        {
            switch (nPos & 3)
            {
            case DIFFERENTIAL_RAY_RIGHTBOTTOM:
                return right_bottom();
            case DIFFERENTIAL_RAY_LEFTBOTTOM:
                return left_bottom();
            case DIFFERENTIAL_RAY_RIGHTTOP:
                return right_top();
            default:
                return left_top();
            }
        }

        const ray& right_top() const { return rt_; }
        const ray& left_top() const { return lt_; }
        const ray& right_bottom() const { return lt_; }
        const ray& left_bottom() const { return lb_; }

    private:
        ray rt_;
        ray lt_;
        ray rb_;
        ray lb_;
    };

    typedef differential_ray dray;
}

#endif