
#include "transformer.h"
#include "values.h"

namespace kaze
{

    namespace
    {

        inline vector3 mul_normal(const matrix3& m, const vector3& v)
        {
            return vector3(
                m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
                m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
                m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
        }

        inline vector3 mul_normal(const matrix4& m, const vector3& v)
        {
            return vector3(
                m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
                m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
                m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
        }

        inline vector3 mul_vector(const matrix4& m, const vector3& v)
        {
            return vector3(
                m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2],
                m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2],
                m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2]);
        }

        inline vector3 mul_position(const matrix4& m, const vector3& v)
        {
            real iR = real(1) / (m[3][0] * v[0] + m[3][1] * v[1] + m[3][2] * v[2] + m[3][3]);
            return vector3(
                (m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2] + m[0][3]) * iR,
                (m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2] + m[1][3]) * iR,
                (m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2] + m[2][3]) * iR);
        }
    }

    //-------------------------------------------------------------------------

    vector3 no_transformer::transform_n(const vector3& v) const { return v; }
    vector3 no_transformer::transform_p(const vector3& v) const { return v; }
    vector3 no_transformer::transform_v(const vector3& v) const { return v; }

    //-------------------------------------------------------------------------

    t_transformer::t_transformer(const vector3& t) : t_(t) {}

    vector3 t_transformer::transform_n(const vector3& v) const { return v; }
    vector3 t_transformer::transform_p(const vector3& v) const { return v + t_; }
    vector3 t_transformer::transform_v(const vector3& v) const { return v; }

    //-------------------------------------------------------------------------

    m3_transformer::m3_transformer(const matrix3& m) : m_(m), im_(~m) {}

    vector3 m3_transformer::transform_n(const vector3& v) const
    {
        return mul_normal(im_, v);
    }
    vector3 m3_transformer::transform_p(const vector3& v) const
    {
        return m_ * v;
    }
    vector3 m3_transformer::transform_v(const vector3& v) const
    {
        return m_ * v;
    }

    //-------------------------------------------------------------------------

    m3t_transformer::m3t_transformer(const matrix3& m, const vector3& t) : m_(m), im_(~m), t_(t) {}
    m3t_transformer::m3t_transformer(const matrix4& m)
    {
        m_[0][0] = m[0][0];
        m_[0][1] = m[0][1];
        m_[0][2] = m[0][2];
        t_[0] = m[0][3];
        m_[1][0] = m[1][0];
        m_[1][1] = m[1][1];
        m_[1][2] = m[1][2];
        t_[1] = m[1][3];
        m_[2][0] = m[2][0];
        m_[2][1] = m[2][1];
        m_[2][2] = m[2][2];
        t_[2] = m[2][3];

        im_ = ~m_;
    }

    vector3 m3t_transformer::transform_n(const vector3& v) const
    {
        return mul_normal(im_, v);
    }
    vector3 m3t_transformer::transform_p(const vector3& v) const
    {
        return m_ * v + t_;
    }
    vector3 m3t_transformer::transform_v(const vector3& v) const
    {
        return m_ * v;
    }

    //-------------------------------------------------------------------------

    m4_transformer::m4_transformer(const matrix4& m) : m_(m), im_(~m) {}

    vector3 m4_transformer::transform_n(const vector3& v) const
    {
        return mul_normal(im_, v);
    }
    vector3 m4_transformer::transform_p(const vector3& v) const
    {
        return mul_position(m_, v);
    }

    vector3 m4_transformer::transform_v(const vector3& v) const
    {
        return mul_vector(m_, v);
    }

    //-------------------------------------------------------------------------

    vector3 q_transformer::transform_n(const vector3& v) const
    {
        return mul_normal(mat3_gen::convert(im_), v);
    }

    vector3 q_transformer::transform_p(const vector3& v) const
    {
        vector3 tmp;
        transform(&tmp, m_, v);
        return tmp;
    }

    vector3 q_transformer::transform_v(const vector3& v) const
    {
        vector3 tmp;
        transform(&tmp, m_, v);
        return tmp;
    }

    //-------------------------------------------------------------------------
}

#ifdef __UNIT_TEST__

#include <iostream>
#include <vector>

#include "types.h"
#include "timer.h"

using namespace kaze;
using namespace kaze::system;

inline real radian(real x) { return x * 3.14159265 * 2 / 360.0; }

void func3(std::vector<vector3>& vv, const transformer& t)
{

    size_t sz;
    sz = vv.size();

    for (int j = 0; j < 400; j++)
        for (size_t i = 0; i < sz; i++)
        {
            vv[i] = t.transform_p(vv[i]);
        }

    return;
}

void func4(std::vector<vector4>& vv, const transformer& t)
{

    size_t sz;
    sz = vv.size();

    for (int j = 0; j < 400; j++)
        for (size_t i = 0; i < sz; i++)
        {
            vv[i] = t.transform_p(vv[i]);
        }

    return;
}

int main()
{

    vector3 v3(1, 0, 0);
    vector4 v4(1, 0, 0, 1);

    quaternion q = tempest::pivot_angle(vector3(0, 1, 0), radian(45));

    q_transformer qt(q);

    std::cout << qt.transform_p(v3) << ":" << qt.transform_p(v4) << std::endl;

    matrix3 m3 = mat3_gen::rotation_y(radian(45));

    m3_transformer m3t(m3);

    std::cout << m3t.transform_p(v3) << ":" << m3t.transform_p(v4) << std::endl;

    matrix4 m4 = mat4_gen::rotation_y(radian(45));

    m4_transformer m4t(m4);

    std::cout << m4t.transform_p(v3) << ":" << m4t.transform_p(v4) << std::endl;

    m3t_transformer m3tt(m3, vector3(0, 0, 0));

    std::cout << m3tt.transform_p(v3) << ":" << m3tt.transform_p(v4) << std::endl;

    timer t1;
    std::vector<vector3> vv3(100000, vector3(1, 0, 0));
    std::vector<vector4> vv4(100000, vector4(1, 0, 0, 1));

    t1.start();
    func3(vv3, qt);
    t1.end();
    std::cout << t1.sec() << ":" << t1.msec() << std::endl;

    t1.start();
    func3(vv3, m3t);
    t1.end();
    std::cout << t1.sec() << ":" << t1.msec() << std::endl;

    t1.start();
    func3(vv3, m4t);
    t1.end();
    std::cout << t1.sec() << ":" << t1.msec() << std::endl;

    t1.start();
    func3(vv3, m3tt);
    t1.end();
    std::cout << t1.sec() << ":" << t1.msec() << std::endl;

    t1.start();
    func4(vv4, qt);
    t1.end();
    std::cout << t1.sec() << ":" << t1.msec() << std::endl;

    t1.start();
    func4(vv4, m3t);
    t1.end();
    std::cout << t1.sec() << ":" << t1.msec() << std::endl;

    t1.start();
    func4(vv4, m4t);
    t1.end();
    std::cout << t1.sec() << ":" << t1.msec() << std::endl;

    t1.start();
    func4(vv4, m3tt);
    t1.end();
    std::cout << t1.sec() << ":" << t1.msec() << std::endl;

    return 1;
}
#endif
