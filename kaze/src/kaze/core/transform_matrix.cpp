#include "transform_matrix.h"

namespace kaze
{

    matrix4 world2camera(const vector3& from, const vector3& to, const vector3& upper)
    {
#ifndef KAZE_USE_RH
        return world2camera_LH(from, to, upper);
#else
        return world2camera_RH(from, to, upper);
#endif
    }

    matrix4 world2camera_LH(const vector3& from, const vector3& to, const vector3& upper)
    {
        //T->R
        matrix4 T = mat4_gen::translation(-from[0], -from[1], -from[2]);

        vector3 cz = normalize(to - from);        //
        vector3 cx = normalize(cross(cz, upper)); //-y^z
        vector3 cy = cross(cx, cz);               //

        matrix4 R = matrix4(
            cx[0], cx[1], cx[2], 0,
            cy[0], cy[1], cy[2], 0,
            cz[0], cz[1], cz[2], 0,
            0, 0, 0, 1);

        return R * T;
    }

    matrix4 world2camera_RH(const vector3& from, const vector3& to, const vector3& upper)
    {
        //T->R
        matrix4 T = mat4_gen::translation(-from[0], -from[1], -from[2]);

        vector3 cz = normalize(from - to);        //z(0..-1)
        vector3 cx = normalize(cross(upper, cz)); //x = yz
        vector3 cy = cross(cz, cx);               //y = zx

        matrix4 R = matrix4(
            cx[0], cx[1], cx[2], 0,
            cy[0], cy[1], cy[2], 0,
            cz[0], cz[1], cz[2], 0,
            0, 0, 0, 1);

        return R * T;
    }

#ifndef KAZE_USE_CLIP_MP
    matrix4 camera2clip(real angle, real ratio, real Zn, real Zf)
    {
        return camera2clip_01(angle, ratio, Zn, Zf);
    }
    matrix4 camera2clip(real l, real r, real b, real t, real n, real f) { return camera2clip_01(l, r, b, t, n, f); }
#else
    matrix4 camera2clip(real angle, real ratio, real Zn, real Zf)
    {
        return camera2clip_MP(angle, ratio, Zn, Zf);
    }
    matrix4 camera2clip(real l, real r, real b, real t, real n, real f) { return camera2clip_MP(l, r, b, t, n, f); }
#endif
    /***************************************************
                       .
                      /|\
  -\-------------------+-------------------/-+--+-
    \                  |                  /  |  |
     \                 |                 /   |  |
      \                |                /    |  |
       \               |               /     |Zd|
        \         Vh   |              /      |  |
         \ |<----------+---------->| /       |  |
          \|           |           |/        |  |
           \-----------+-----------/-----+---+- |
            \          |          /      |      |Zf
             \       h |         /       |      |
              \ |<-----+----->| /        |      |
               \|      |      |/         |      |
                \------+------/--+-      |Zn    |
                 \     |     /   |       |      |
                  \    |    /    |       |      |
                   \   |   /     |1      |      |
                    \_ |_ /      |       |      |
                     \ |angle    |       |      |
                      \|/        |       |      |
                      Eye--------+-------+------+-
*****************************************************/

    /*
     * x:[-1,+1]
     * y:[-1,+1]
     * z:[ 0,+1]
     *
     * [Vw/2,V/h/2,Zn]:[1,1,1]
     *
     * 1 = W * (Vw/2)/Zn
     * 1 = H * (Vh/2)/Zn
     * W = 2Zn/Vw
     * H = 2Zn/Vh
     * Q = Zf/Zd
     * x' = W * x/z
     * y' = H * y/z
     * z' = (Zf/z)*(z-Zn)/Zd ...(Zf/Zd) /z * (z-Zn) ... Q/z*(z-Zn)
     *
     * x' = Wx
     * y' = Hy
     * z' = Q(z-Zn)
     * w' = z
     */
    matrix4 camera2clip_MP(real angle, real aspect, real Zn, real Zf)
    {
        real h = std::tan(angle / 2) * 2;
        real w = aspect * h;
        real Vh = h * Zn;
        real Vw = w * Zn;

        real Zd = Zf - Zn;

        real W = 2 * Zn / Vw;
        real H = 2 * Zn / Vh;
        real Q = Zf / Zd;

        return matrix4(
            W, 0, 0, 0,
            0, H, 0, 0,
            0, 0, Q, -Q * Zn,
            0, 0, 1, 0);
    }

    matrix4 camera2clip_MP(real l, real r, real b, real t, real n, real f)
    {
        real Zn = n;
        real Zf = f;

        real Vw = r - l;
        real Vh = t - b;
        real Zd = f - n;

        real W = 2 * Zn / Vw;
        real H = 2 * Zn / Vh;
        real Q = Zf / Zd;

        real TW = -(r + l) / Vw;
        real TH = -(t + b) / Vh;

        return matrix4(
            W, 0, TW, 0,
            0, H, TH, 0,
            0, 0, Q, -Q * Zn,
            0, 0, 1, 0);
    }

    matrix4 camera2clip_01(real angle, real aspect, real Zn, real Zf)
    {
        real h = std::tan(angle / 2) * 2;
        real w = aspect * h;
        real Vh = h * Zn;
        real Vw = w * Zn;

        real Zd = Zf - Zn;

        real W = Zn / Vw;
        real H = Zn / Vh;
        real Q = Zf / Zd;

        real TW = real(0.5);
        real TH = real(0.5);

        return
            //mat4_gen::translation(0,0,-(Zn+1))
            //*
            matrix4(
                W, 0, TW, 0,
                0, H, TH, 0,
                0, 0, Q, -Q * Zn,
                0, 0, 1, 0);
    }

    matrix4 camera2clip_01(real l, real r, real b, real t, real n, real f)
    {
        real Zn = n;
        real Zf = f;

        real Vw = r - l;
        real Vh = t - b;
        real Zd = f - n;

        real W = Zn / Vw;
        real H = Zn / Vh;
        real Q = Zf / Zd;

        real TW = -(r + l) / Vw + real(0.5);
        real TH = -(t + b) / Vh + real(0.5);

        return
            //mat4_gen::translation(0,0,-(Zn+1))
            //*
            matrix4(
                W, 0, TW, 0,
                0, H, TH, 0,
                0, 0, Q, -Q * Zn,
                0, 0, 1, 0);
    }

#ifndef KAZE_USE_CLIP_MP
    matrix4 clip2screen(real w, real h, real d)
    {
        return clip2screen_01(w, h, d);
    }
#else
    matrix4 clip2screen(real w, real h, real d)
    {
        return clip2screen_MP(w, h, d);
    }
#endif
    matrix4 clip2screen_MP(real w, real h, real d)
    {
        real hh = h / 2;
        real ww = w / 2;
        return mat4_gen::translation(ww, hh, 0) * mat4_gen::scaling(ww, -hh, d);
    }

    matrix4 clip2screen_01(real w, real h, real d)
    {
        return mat4_gen::translation(0, h, 0) * mat4_gen::scaling(w, -h, d);
    }

    //-------------------------------------------------------------------------------
    matrix4 camera2world(const vector3& from, const vector3& to, const vector3& upper)
    {
#ifndef KAZE_USE_RH
        return camera2world_LH(from, to, upper);
#else
        return camera2world_RH(from, to, upper);
#endif
    }

    matrix4 camera2world_LH(const vector3& from, const vector3& to, const vector3& upper)
    {
        return ~world2camera_LH(from, to, upper);
    }
    matrix4 camera2world_RH(const vector3& from, const vector3& to, const vector3& upper)
    {
        return ~world2camera_RH(from, to, upper);
    }
}
