#ifndef KAZE_PARAMETER_MAP_H
#define KAZE_PARAMETER_MAP_H

#include <vector>

namespace kaze
{

    enum
    {
        RESTRICTION_INT = 1,
        RESTRICTION_DOUBLE = 2,
        RESTRICTION_STRING = 4,
        RESTRICTION_ALL = 1 + 2 + 4
    };

    class parameter_map_imp;

    class parameter_map
    {
    public:
        parameter_map();
        ~parameter_map();
        parameter_map(const parameter_map& rhs);
        parameter_map& operator=(const parameter_map& rhs);

        void set(const char* key, const char* val, int rest = RESTRICTION_ALL);
        void set_int(const char* key, int val, int rest = RESTRICTION_INT);
        void set_double(const char* key, double val, int rest = RESTRICTION_DOUBLE);
        void set_string(const char* key, const char* val, int rest = RESTRICTION_STRING);

        const char* get(const char* key, int rest = RESTRICTION_ALL) const;
        int get_int(const char* key, int def = 0) const;
        double get_double(const char* key, double def = 0.0) const;
        const char* get_string(const char* key, const char* def = "") const;

        void get_keys(std::vector<const char*>& keys) const;

    private:
        parameter_map_imp* imp_;
    };
}

#endif