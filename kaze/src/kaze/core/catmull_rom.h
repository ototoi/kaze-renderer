#ifndef KAZE_CATMULL_ROM_H
#define KAZE_CATMULL_ROM_H

#include "values.h"

namespace kaze
{

#define DEF_SPLINE(TYPE)                                         \
    \
TYPE                                                      \
    catmull_rom_evaluate(const TYPE cp[], int sz, real x);       \
    \
TYPE                                                      \
    catmull_rom_evaluate_deriv(const TYPE cp[], int sz, real x); \
    \
TYPE                                                      \
    catmull_rom_evaluate_close(const TYPE cp[], int sz, real x);

    DEF_SPLINE(real)
    DEF_SPLINE(vector2)
    DEF_SPLINE(vector3)
    DEF_SPLINE(vector4)

#undef DEF_SPLINE
}

#endif