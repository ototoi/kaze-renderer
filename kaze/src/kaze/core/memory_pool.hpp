#ifndef KAZE_MEMORY_POOL_H_
#define KAZE_MEMORY_POOL_H_

/// from Efficient c++ section 6

#include <memory>

namespace kaze
{

    const int MEMPRY_POOL_DEFAULTEXPAND_SIZE = 32;

    template <class T, size_t Sz = MEMPRY_POOL_DEFAULTEXPAND_SIZE>
    class memory_pool
    {
    public:
        memory_pool()
        {
            expandTheFreeList(Sz);
        }
        ~memory_pool()
        {
            memory_pool<T, Sz>* nextPtr = next_;
            while (nextPtr)
            {
                memory_pool<T, Sz>* nowPtr = nextPtr;
                nextPtr = nextPtr->next_;
                delete[] reinterpret_cast<char*>(nowPtr);
            }
        }

        void* alloc(size_t)
        {
            if (!next_) expandTheFreeList(Sz);

            memory_pool<T, Sz>* head = next_;
            next_ = head->next_;

            return head;
        }
        void free(void* doomed)
        {
            memory_pool<T, Sz>* head = static_cast<memory_pool<T, Sz>*>(doomed);
            head->next_ = next_;
            next_ = head;
        }

    private:
        void expandTheFreeList(int howMany);

    private:
        memory_pool<T, Sz>* next_;
    };
    template <class T, size_t Sz>
    void memory_pool<T, Sz>::expandTheFreeList(int howMany)
    {
        size_t size = (sizeof(T) > sizeof(memory_pool<T, Sz>*))
                          ? sizeof(T)
                          : sizeof(memory_pool<T, Sz>*);

        memory_pool<T, Sz>* runner =
            reinterpret_cast<memory_pool<T, Sz>*>(new char[size]);

        next_ = runner;
        for (int i = 1; i < howMany; i++)
        {
            runner->next_ =
                reinterpret_cast<memory_pool<T, Sz>*>(new char[size]);
            runner = runner->next_;
        }
        runner->next_ = 0;
    }

    template <class T, size_t Sz = MEMPRY_POOL_DEFAULTEXPAND_SIZE>
    class memory_pool_holder__
    {
    public:
        memory_pool_holder__() : p_pool_(new memory_pool<T, Sz>()) {}
        ~memory_pool_holder__() { delete p_pool_; }
        void initialize()
        {
            std::unique_ptr<memory_pool<T, Sz> > ap(new memory_pool<T, Sz>());
            delete p_pool_;
            p_pool_ = ap.release();
        }
        memory_pool<T, Sz>* operator->() const throw() { return p_pool_; }
    private:
        memory_pool<T, Sz>* p_pool_;
    };

    template <class T, size_t Sz = MEMPRY_POOL_DEFAULTEXPAND_SIZE>
    class use_memory_pool
    {
    public:
        void* operator new(size_t size) { return use_memory_pool::get_instance()->alloc(size); }
        void operator delete(void* doomed, size_t) { use_memory_pool::get_instance()->free(doomed); }

        static void initialize() { use_memory_pool::get_instance().initialize(); }
    protected:
        static memory_pool_holder__<T, Sz>& get_instance()
        {
            static memory_pool_holder__<T, Sz> pool_;
            return pool_;
        }
    };
}

#endif // ! MEMORY_POOL_H_
