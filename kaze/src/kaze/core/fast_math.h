#ifndef KAZE_FAST_MATH_H
#define KAZE_FAST_MATH_H

namespace kaze
{

    inline float fast_isqrtf(float number)
    {
        static const float threehalfs = 1.5f;
        long i;
        float x2, y;

        x2 = number * 0.5F;
        y = number;
        i = *(long*)&y;
        i = 0x5f3759df - (i >> 1);
        y = *(float*)&i;
        y = y * (threehalfs - (x2 * y * y));
        //	y  = y * ( threehalfs - ( x2 * y * y ) );			// Da second iteration

        return y;
    }

    inline float fast_fabsf(float f)
    {
        return *(float*)(&((*(int*)&f) &= 0x7FFFFFFF));
    }

    inline double fast_fabs(double x)
    {
        return *(((int*)&x) + 1) &= 0x7fffffff;
    }
}

#endif
