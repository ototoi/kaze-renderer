#ifndef KAZE_SSE_UTILITY_H
#define KAZE_SSE_UTILITY_H

#include <cstdlib>

namespace kaze
{
    void* sse_malloc(size_t sz);
    void sse_free(void* p);

#if defined(_WIN32) | defined(__GNUC__)
#define KAZE_HAVE_SSE

    float* sse_neg(float* out, const float* a);
    float* sse_rcp(float* out, const float* a);
    float* sse_sum(float* out, const float* a);
    float* sse_add(float* out, const float* a, const float* b);
    float* sse_sub(float* out, const float* a, const float* b);
    float* sse_mul(float* out, const float* a, const float* b);
    float* sse_div(float* out, const float* a, const float* b);
    float* sse_div_ss(float* out, const float* a, const float* b);

    float* sse_ss2ps(float* out, const float* a);

    float* sse_normalize(float* out, const float* a);

#endif
}

#endif
