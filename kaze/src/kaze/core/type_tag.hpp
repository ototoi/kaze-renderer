#ifndef KAZE_TYPE_TAG_HPP
#define KAZE_TYPE_TAG_HPP

#include "types.h"

namespace kaze
{

    template <class T>
    class type_tag
    {
    };

    enum
    {
        TYPE_ENUM_bool,
        TYPE_ENUM_int8_t,
        TYPE_ENUM_int16_t,
        TYPE_ENUM_int32_t,
        TYPE_ENUM_int64_t,
        TYPE_ENUM_uint8_t,
        TYPE_ENUM_uint16_t,
        TYPE_ENUM_uint32_t,
        TYPE_ENUM_uint64_t,
        TYPE_ENUM_float,
        TYPE_ENUM_double
    };

#define DEF_KAZE_TYPE_TAG(TYPE)                    \
    template <>                                    \
    class type_tag<TYPE>                           \
    {                                              \
        static const int value = TYPE_ENUM_##TYPE; \
    };

    DEF_KAZE_TYPE_TAG(bool)
    DEF_KAZE_TYPE_TAG(int8_t)
    DEF_KAZE_TYPE_TAG(int16_t)
    DEF_KAZE_TYPE_TAG(int32_t)
    DEF_KAZE_TYPE_TAG(int64_t)
    DEF_KAZE_TYPE_TAG(uint8_t)
    DEF_KAZE_TYPE_TAG(uint16_t)
    DEF_KAZE_TYPE_TAG(uint32_t)
    DEF_KAZE_TYPE_TAG(uint64_t)
    DEF_KAZE_TYPE_TAG(float)
    DEF_KAZE_TYPE_TAG(double)

#undef DEF_KAZE_TYPE_TAG
}

#endif