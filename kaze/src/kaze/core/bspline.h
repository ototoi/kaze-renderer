#ifndef KAZE_BSPLINE_H
#define KAZE_BSPLINE_H

#include "types.h"
#include <vector>

namespace kaze
{

/*
    m:Order
    k:Knots size
    t:parameter
    */
#define DEF_BSPLINE_DE_BOOR_COX(FLOAT)                                                        \
    FLOAT de_boor_cox(const FLOAT knots[], int i, int m, FLOAT t);                            \
    FLOAT de_boor_cox_deriv(const FLOAT knots[], int i, int m, FLOAT t);                      \
    int de_boor_cox_offset(FLOAT params[], int m, const FLOAT knots[], int k, FLOAT t);       \
    int de_boor_cox_deriv_offset(FLOAT params[], int m, const FLOAT knots[], int k, FLOAT t); \
    void de_boor_cox(FLOAT params[], int p, const FLOAT knots[], int k, FLOAT t);             \
    void bspline_create_uniform_knots(FLOAT knots[], int k, int nOrder, bool bOpen0, bool bOpen1);

    DEF_BSPLINE_DE_BOOR_COX(float)
    DEF_BSPLINE_DE_BOOR_COX(double)

#undef DEF_BSPLINE_DE_BOOR_COX

#define DEF_BSPLINE_IS(FLOAT)                                 \
    bool bspline_is_open0(const FLOAT knots[], int k, int m); \
    bool bspline_is_open1(const FLOAT knots[], int k, int m); \
    bool bspline_is_uniform(const FLOAT knots[], int k);      \
    bool bspline_is_bezier_type(const FLOAT knots[], int k, int m);

    DEF_BSPLINE_IS(float)
    DEF_BSPLINE_IS(double)

#undef DEF_BSPLINE_IS

#define DEF_BSPLINE(TYPE, FLOAT) \
    TYPE bspline4_evaluate(const TYPE p[], int n, FLOAT t);

    DEF_BSPLINE(float, float)
    DEF_BSPLINE(vector2f, float)
    DEF_BSPLINE(vector3f, float)
    DEF_BSPLINE(vector4f, float)
    DEF_BSPLINE(double, double)
    DEF_BSPLINE(vector2, double)
    DEF_BSPLINE(vector3, double)
    DEF_BSPLINE(vector4, double)

#undef DEF_BSPLINE

#define DEF_BSPLINE(TYPE, FLOAT)                                                                                                                 \
    TYPE bspline_evaluate(const TYPE p[], int n, const FLOAT knots[], int k, FLOAT t);                                                           \
    TYPE bspline_evaluate_deriv(const TYPE p[], int n, const FLOAT knots[], int k, FLOAT t);                                                     \
    TYPE bspline_evaluate(const TYPE p[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v);         \
    TYPE bspline_evaluate_deriv_u(const TYPE p[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v); \
    TYPE bspline_evaluate_deriv_v(const TYPE p[], int nu, int nv, const FLOAT uknots[], int ku, const FLOAT vknots[], int kv, FLOAT u, FLOAT v);

    DEF_BSPLINE(float, float)
    DEF_BSPLINE(vector2f, float)
    DEF_BSPLINE(vector3f, float)
    DEF_BSPLINE(vector4f, float)
    DEF_BSPLINE(double, double)
    DEF_BSPLINE(vector2, double)
    DEF_BSPLINE(vector3, double)
    DEF_BSPLINE(vector4, double)

#undef DEF_BSPLINE

#define DEF_BSPLINE(TYPE, FLOAT)                                                                                                      \
    void bspline_convert_bezier_type(std::vector<TYPE>& p, std::vector<FLOAT>& knots);                                                \
    void bspline_convert_bezier_type(std::vector<TYPE>& p, int& nu, int& nv, std::vector<FLOAT>& uknots, std::vector<FLOAT>& vknots); \
    void bspline_convert_bezier_type(std::vector<TYPE>& p, std::vector<FLOAT>& w, std::vector<FLOAT>& knots);

    DEF_BSPLINE(float, float)
    DEF_BSPLINE(vector2f, float)
    DEF_BSPLINE(vector3f, float)
    DEF_BSPLINE(vector4f, float)
    DEF_BSPLINE(double, double)
    DEF_BSPLINE(vector2, double)
    DEF_BSPLINE(vector3, double)
    DEF_BSPLINE(vector4, double)

#undef DEF_BSPLINE

#define DEF_BSPLINE(TYPE, FLOAT)                                                      \
    void bspline_kill_periodic(std::vector<TYPE>& points, std::vector<FLOAT>& knots); \
    void bspline_kill_periodic(std::vector<TYPE>& points, int& nu, int& nv, std::vector<FLOAT>& uknots, std::vector<FLOAT>& vknots, bool periodicU, bool periodicV);

    DEF_BSPLINE(float, float)
    DEF_BSPLINE(vector2f, float)
    DEF_BSPLINE(vector3f, float)
    DEF_BSPLINE(vector4f, float)
    DEF_BSPLINE(double, double)
    DEF_BSPLINE(vector2, double)
    DEF_BSPLINE(vector3, double)
    DEF_BSPLINE(vector4, double)

#undef DEF_BSPLINE
}

#endif
