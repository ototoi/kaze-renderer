#ifndef KAZE_COMPRESSED_NORMAL_H
#define KAZE_COMPRESSED_NORMAL_H

#include "types.h"

namespace kaze
{

    class compressed_normal
    {
    public:
        typedef unsigned char value_type;

    public:
        compressed_normal() {}
        compressed_normal(const compressed_normal& rhs)
        {
            mv_[0] = rhs.mv_[0];
            mv_[1] = rhs.mv_[1];
            mv_[2] = rhs.mv_[2];
        }
        compressed_normal& operator=(const compressed_normal& rhs)
        {
            mv_[0] = rhs.mv_[0];
            mv_[1] = rhs.mv_[1];
            mv_[2] = rhs.mv_[2];
            return *this;
        }

        compressed_normal& negate();

        operator vector3() const;

    public:
        explicit compressed_normal(const vector3& rhs);
        compressed_normal& operator=(const vector3& rhs);

    public:
        friend vector3 barycentric_convert(real w, real u, real v, const compressed_normal& n0, const compressed_normal& n1, const compressed_normal& n2);

    protected:
        value_type mv_[3];
    };

    vector3 barycentric_convert(real w, real u, real v, const compressed_normal& n0, const compressed_normal& n1, const compressed_normal& n2);

    unsigned char compress_normal_element(real f);
    real decompress_normal_element(unsigned char c);
}

#endif
