#ifndef KAZE_PC2_LOADER_H
#define KAZE_PC2_LOADER_H

#include <vector>

#ifndef _WIN32
#include <stdint.h>
#endif

namespace kaze
{

    class pc2_loader
    {
    public:
        pc2_loader();
        ~pc2_loader();
        bool load(const char* szFileName);

    public:
        std::size_t get_frame() const;
        float get_sample_rate() const;
        float get_start_frame() const;
        int get_num_points() const;

    protected:
        std::vector<std::vector<float>*> points_;
    };
}

#endif
