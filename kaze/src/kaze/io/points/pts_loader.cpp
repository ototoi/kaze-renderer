#include "pts_loader.h"

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include <stdio.h>

namespace kaze
{

    pts_loader::pts_loader(const char* szFileName)
    {
        fp_ = fopen(szFileName, "rt");
    }
    pts_loader::~pts_loader()
    {
        if (fp_) fclose(fp_);
    }
    bool pts_loader::get(point* pt) const
    {
        if (!fp_) return false;
        if (feof(fp_)) return false;

        char buffer[512];
        if (!fgets(buffer, 512, fp_)) return false;
        float x, y, z, r, cr, cg, cb;
        int n = 0;
        n = sscanf(buffer, "%f, %f, %f, %f, %f, %f, %f", &x, &y, &z, &r, &cr, &cg, &cb);
        if (n == 1)
        {
            n = sscanf(buffer, "%f %f %f %f %f %f %f", &x, &y, &z, &r, &cr, &cg, &cb);
        }
        if (n >= 3)
        {
            pt->p[0] = x;
            pt->p[1] = y;
            pt->p[2] = z;
            if (n >= 4)
            {
                pt->p[3] = r;
                if (n >= 7)
                {
                    pt->p[4] = cr;
                    pt->p[5] = cg;
                    pt->p[6] = cb;
                }
                else
                {
                    pt->p[4] = 1.0f;
                    pt->p[5] = 1.0f;
                    pt->p[6] = 1.0f;
                }
            }
            else
            {
                pt->p[3] = 1.0f;
                pt->p[4] = 1.0f;
                pt->p[5] = 1.0f;
                pt->p[6] = 1.0f;
            }
            return true;
        }
        return true;
    }
}