#ifndef KAZE_PTS_LOADER_H
#define KAZE_PTS_LOADER_H

#include <cstdio>

namespace kaze
{

    class pts_loader
    {
    public:
        struct point
        {
            float p[7];
        };
        pts_loader(const char* szFileName);
        ~pts_loader();
        bool get(point* pt) const;

    private:
        FILE* fp_;
    };
}

#endif