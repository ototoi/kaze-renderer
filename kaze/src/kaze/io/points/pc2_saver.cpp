#include "pc2_saver.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct pc2_header
{
    char cacheSignature[12]; // Will be 'POINTCACHE2' followed by a trailing null character.
    int fileVersion;         // Currently 1
    int numPoints;           // Number of points per sample
    float startFrame;        // Corresponds to the UI value of the same name.
    float sampleRate;        // Corresponds to the UI value of the same name.
    int numSamples;          // Defines how many samples are stored in the
};

static bool WritePC2Header(FILE* fp)
{
    pc2_header header;
    strcpy(header.cacheSignature, "POINTCACHE2");
    header.fileVersion = 1;

    return true;
}

namespace kaze
{

    bool pc2_saver::save(const char* szFile)
    {

        return true;
    }
}
