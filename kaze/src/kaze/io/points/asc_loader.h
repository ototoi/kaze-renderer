#ifndef KAZE_ASC_LOADER_H
#define KAZE_ASC_LOADER_H

#include <cstdio>

namespace kaze
{

    class asc_loader
    {
    public:
        struct point
        {
            float p[4];
        };
        asc_loader(const char* szFileName);
        ~asc_loader();
        bool get(point* pt) const;

    private:
        FILE* fp_;
    };
}

#endif
