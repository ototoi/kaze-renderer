#include "pc2_loader.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

namespace kaze
{
    namespace
    {
        struct pc2_header
        {
            char cacheSignature[12]; // Will be 'POINTCACHE2' followed by a trailing null character.
            int fileVersion;         // Currently 1
            int numPoints;           // Number of points per sample
            float startFrame;        // Corresponds to the UI value of the same name.
            float sampleRate;        // Corresponds to the UI value of the same name.
            int numSamples;          // Defines how many samples are stored in the
        };
    }

    static bool LoadHeader(pc2_header* header, FILE* fp)
    {
        size_t nRead = ::fread(&header, sizeof(pc2_header), 1, fp);
        if (nRead == 0) return false;
        return true;
    }

    pc2_loader::pc2_loader()
    {
        ; //
    }

    pc2_loader::~pc2_loader()
    {
        size_t sz = points_.size();
        for (size_t i = 0; i < sz; i++)
        {
            delete points_[i];
        }
    }

    bool pc2_loader::load(const char* szFileName)
    {

        return false;
    }

    size_t pc2_loader::get_frame() const
    {
        return 0;
    }

    float pc2_loader::get_sample_rate() const
    {
        return 0;
    }

    float pc2_loader::get_start_frame() const
    {

        return 0;
    }

    int pc2_loader::get_num_points() const
    {

        return 0;
    }
}
