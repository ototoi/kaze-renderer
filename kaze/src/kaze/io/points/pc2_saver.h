#ifndef KAZE_PC2_SAVER_H
#define KAZE_PC2_SAVER_H

#include <vector>

namespace kaze
{

    class pc2_saver
    {
    public:
        bool save(const char* szFile);

    private:
        std::vector<float> points_;
        int numPoints;    // Number of points per sample
        float startFrame; // Corresponds to the UI value of the same name.
        float sampleRate; // Corresponds to the UI value of the same name.
        int numSamples;   // Defines how many samples are stored in the file.
    };
}

#endif