#include "asc_loader.h"

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include <stdio.h>

namespace kaze
{

    asc_loader::asc_loader(const char* szFileName)
    {
        fp_ = fopen(szFileName, "rt");
    }
    asc_loader::~asc_loader()
    {
        if (fp_) fclose(fp_);
    }
    bool asc_loader::get(point* pt) const
    {
        if (!fp_) return false;
        if (feof(fp_)) return false;

        char buffer[512];
        if (!fgets(buffer, 512, fp_)) return false;
        float x, y, z, r;
        int n = 0;
        n = sscanf(buffer, "%f, %f, %f, %f", &x, &y, &z, &r);
        if (n == 1)
        {
            n = sscanf(buffer, "%f %f %f %f", &x, &y, &z, &r);
        }
        if (n >= 3)
        {
            pt->p[0] = x;
            pt->p[1] = y;
            pt->p[2] = z;
            return true;
        }
        return true;
    }
}
