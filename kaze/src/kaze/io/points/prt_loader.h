#ifndef KAZE_PRT_LOADER_H
#define KAZE_PRT_LOADER_H

#include <string>
#include <vector>
#include <map>

namespace kaze
{

    class prt_channel
    {
    public:
        enum DATA_TYPE
        {
            INT32,
            INT64,
            UINT32,
            UINT64,
            FLOAT32,
            FLOAT64
        };

    private:
        DATA_TYPE type_;
        std::vector<char> data_;
    };

    class prt_data_set
    {
    public:
        prt_data_set();
        ~prt_data_set();
        void add(const char* key, prt_channel*);
        prt_channel* get(const char* key);

    private:
        std::map<std::string, prt_channel*> channels_;
    };

    class prt_loader
    {
    public:
        prt_loader();
        ~prt_loader();

    public:
        bool load(const char* szFile);

    private:
        prt_data_set* data_;
    };
}

#endif
