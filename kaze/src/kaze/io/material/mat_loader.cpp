#include "mat_loader.h"
#include <vector>
#include <map>
#include <cstdio>
#include <cstdlib>
#include <fstream>
#include <sstream>

#include <string.h>

namespace kaze
{
    namespace
    {
        class mat_material_imp : public mat_material
        {
        public:
            mat_material_imp() {}
            mat_material_imp(const std::string& str) : name_(str) {}
        public:
            std::string get(const char* szKey) const
            {
                std::map<std::string, std::string>::const_iterator i = map_.find(szKey);
                if (i != map_.end())
                {
                    return i->second;
                }
                return "";
            };
            void set(const char* szKey, const char* szValue)
            {
                map_[std::string(szKey)] = std::string(szValue);
            }

            const std::string& get_name() const { return name_; }
        private:
            std::string name_;
            std::map<std::string, std::string> map_;
        };

        class mat_loader_loader
        {
        public:
            mat_loader_loader(const char* szFilePath);
            ~mat_loader_loader();
            bool load();

        public:
            std::vector<mat_material_imp*> mats_;

        private:
            bool load_mtl();
            bool load_newmtl(const std::string& strName);

        private:
            std::ifstream ifs_;
        };

        mat_loader_loader::mat_loader_loader(const char* szFilePath)
        {
            ifs_.open(szFilePath);
        }

        mat_loader_loader::~mat_loader_loader()
        {
            size_t sz = mats_.size();
            for (size_t i = 0; i < sz; i++)
            {
                delete mats_[i];
            }
            if (ifs_) ifs_.close();
        }

        bool mat_loader_loader::load()
        {
            if (!ifs_) return false;
            return load_mtl();
        }

        static bool ieq(const std::string& str, const char* szKey)
        {
#ifdef _MSC_VER
            return (_strcmpi(str.c_str(), szKey) == 0);
#else
            return (strcasecmp(str.c_str(), szKey) == 0);
#endif
        }

        static const char* TABLE_COLORS[] = {
            "Fr", //Fresnel reflectance
            "Ft", //Fresnel transmittance
            "Ia", //ambient light
            "I",  //light intensity
            "Ir", //intensity from reflected direction
            "It", //intensity from transmitted direction
            "Ka", //ambient reflectance
            "Kd", //diffuse reflectance
            "Ks", //specular reflectance
            "Tf", //transmission filter
            NULL};

        static const char* TABLE_VALUES[] = {
            "Ns",        //
            "Ni",        //
            "d",         //
            "sharpness", //
            "illum",     //
            NULL};

        static const char* TABLE_TEXTURES[] = {
            "map_Ka", //
            "map_Kd", //
            "map_Ks", //
            "map_Ns", //
            "map_Ni", //
            "map_d",  //
            "disp",   //
            "decal",  //
            "bump",   //
            NULL};

        static bool is_attribute(const std::string& str)
        {
            int i;
            i = 0;
            while (TABLE_COLORS[i])
            {
                if (ieq(str, TABLE_COLORS[i])) return true;
            }
            i = 0;
            while (TABLE_VALUES[i])
            {
                if (ieq(str, TABLE_VALUES[i])) return true;
            }
            i = 0;
            while (TABLE_TEXTURES[i])
            {
                if (ieq(str, TABLE_TEXTURES[i])) return true;
            }

            return false;
        }

        static std::string revalue(std::istream& is)
        {
            std::string strTotal;
            std::string strToken;
            int i = 0;
            while ((is >> strToken))
            {
                if (i != 0) strTotal += " ";
                strTotal += strToken;
                i++;
            }
            return strTotal;
        }

        bool mat_loader_loader::load_mtl()
        {
            std::string strLine;
            std::string strToken;
            while (std::getline(ifs_, strLine))
            {
                if (strLine.empty()) continue;
                if (strLine[0] == '#') continue;
                std::stringstream ss(strLine);
                ss >> strToken;
                if (strToken.empty()) continue;
                if (strToken[0] == '#') continue;
                if (ieq(strToken, "newmtl"))
                {
                    ss >> strToken; //mat name
                    if (!load_newmtl(strToken)) return false;
                }
                else
                {
                    if (is_attribute(strToken))
                    {
                        if (mats_.empty()) return false;
                        mat_material_imp* pMat = mats_.back();
                        std::string strValue = revalue(ss);
                        pMat->set(strToken.c_str(), strValue.c_str());
                    }
                }
            }
            return true;
        }

        bool mat_loader_loader::load_newmtl(const std::string& strName)
        {
            mat_material_imp* pMat = new mat_material_imp(strName);
            mats_.push_back(pMat);
            return true;
        }
    }

    //---------------------------------------------

    class mat_loader_imp
    {
    public:
        mat_loader_imp();
        ~mat_loader_imp();
        bool load(const char* szFilePath);
        mat_material* get_material(size_t i) const;
        size_t get_size() const;

    public:
        std::vector<mat_material_imp*> mats_;
    };

    mat_loader_imp::mat_loader_imp()
    {
        ; //
    }

    mat_loader_imp::~mat_loader_imp()
    {
        size_t sz = mats_.size();
        for (size_t i = 0; i < sz; i++)
        {
            delete mats_[i];
        }
    }

    bool mat_loader_imp::load(const char* szFilePath)
    {
        //TODO:
        mat_loader_loader ld(szFilePath);
        if (!ld.load()) return false;
        size_t sz = ld.mats_.size();
        for (size_t i = 0; i < sz; i++)
        {
            mats_.push_back(ld.mats_[i]);
        }
        ld.mats_.clear();
        return false;
    }

    mat_material* mat_loader_imp::get_material(size_t i) const
    {
        return mats_[i];
    }

    size_t mat_loader_imp::get_size() const
    {
        return mats_.size();
    }

    //----------------------------------------------------------------------------

    mat_loader::mat_loader()
    {
        imp_ = new mat_loader_imp();
    }
    mat_loader::~mat_loader()
    {
        delete imp_;
    }
    bool mat_loader::load(const char* szFilePath)
    {
        return imp_->load(szFilePath);
    }
    mat_material* mat_loader::get_material(size_t i) const
    {
        return imp_->get_material(i);
    }
    size_t mat_loader::get_size() const
    {
        return imp_->get_size();
    }
}
