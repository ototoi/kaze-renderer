#ifndef KAZE_MAT_LOADER_H
#define KAZE_MAT_LOADER_H

#include <string>

namespace kaze
{

    class mat_material
    {
    public:
        virtual ~mat_material() { ; }
        virtual std::string get(const char* szKey) const = 0;
    };

    class mat_loader_imp;
    class mat_loader
    {
    public:
        mat_loader();
        ~mat_loader();
        bool load(const char* szFilePath);
        mat_material* get_material(size_t i) const;
        size_t get_size() const;

    private:
        mat_loader_imp* imp_;
    };
}

#endif