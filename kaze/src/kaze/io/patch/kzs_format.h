#ifndef KZS_FORMAT_H
#define KZS_FORMAT_H

typedef struct kzs_header_
{
    union
    {
        struct
        {
            char magic[4];   /*'K','Z','S','\0'*/
            unsigned int sn; /* 4 Byte */
            unsigned int version;
        };
        char reserve[256];
    };
} kzs_header;

/*
surface format

struct bezierpatch{
    int nu;
    int nv;
    std::vector<double> uknots;
    std::vector<double> vknots;
    std::vector<double> cps;//xyz xyz xyz....
    std::vector<bezierloop> loops;
    unsigned int nID;
    unsigned int nMat;
};

Write each surface

type #bezier:0
nu
nv
uknots.size()-1
vknots.size()-1
uknots...
vknots...
cps...
loops.size();
loops...
nID
nMat
*/

#endif