#ifndef KAZE_IO_BEZIER_PATCH_H
#define KAZE_IO_BEZIER_PATCH_H

#include <vector>

namespace kaze
{

    struct io_bezier_curve
    {
        std::vector<double> knots;
        std::vector<double> cps; //xyz xyz xyz....
    };

    struct io_bezier_loop
    {
        int nDirection;
        std::vector<io_bezier_curve> curves;
    };

    struct io_bezier_patch
    {
        int nu;
        int nv;
        std::vector<double> uknots;
        std::vector<double> vknots;
        std::vector<double> cps; //xyz xyz xyz....
        std::vector<io_bezier_loop> loops;
        unsigned int nID;
        unsigned int nMat;
    };
}

#endif
