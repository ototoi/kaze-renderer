#include "convert_bezier_patch.h"

namespace kaze
{

    typedef std::vector<multi_bezier_curve<vector2> > LoopType;

    static bool ConvertKnots(std::vector<real>& out, const std::vector<double>& in)
    {
        out.resize(in.size());
        for (size_t i = 0; i < in.size(); i++)
        {
            out[i] = (real)in[i];
        }
        return true;
    }

    static bool ConvertPatch(std::vector<real>& uknots, std::vector<real>& vknots, std::vector<bezier_patch<vector3> >& patches, const io_bezier_patch& in)
    {
        const std::vector<double>& cps = in.cps;

        if (!ConvertKnots(uknots, in.uknots)) return false;
        if (!ConvertKnots(vknots, in.vknots)) return false;

        int nu = in.nu;
        int nv = in.nv;
        int nPu = (int)in.uknots.size() - 1;
        int nPv = (int)in.vknots.size() - 1;

        unsigned int nID = in.nID;
        unsigned int nMat = in.nMat;

        int totalPatch = nPu * nPv;
        patches.reserve(totalPatch);
        for (int ipv = 0; ipv < nPv; ipv++)
        {
            for (int ipu = 0; ipu < nPu; ipu++)
            {
                //size_t index_patch = (ipu * nPv + ipv ) * nu * nv * 3;
                size_t index_patch = (ipv * nPu + ipu) * nu * nv * 3;
                std::vector<vector3> cp(nu * nv);
                for (int v = 0; v < nv; v++)
                {
                    for (int u = 0; u < nu; u++)
                    {
                        //size_t index = index_patch + (u * nv + v) * 3;
                        size_t index = index_patch + (v * nu + u) * 3;
                        cp[v * nu + u] = vector3(cps[index + 0], cps[index + 1], cps[index + 2]);
                    }
                }
                patches.push_back(bezier_patch<vector3>(nu, nv, cp));
            }
        }

        return true;
    }

    static bool ConvertCurve(multi_bezier_curve<vector2>& out, const io_bezier_curve& in)
    {
        int ns = (int)in.knots.size() - 1;
        int nc = (int)in.cps.size() / ns / 3;

        std::vector<bezier_curve<vector2> > curves;
        for (int j = 0; j < ns; j++)
        {
            std::vector<vector2> cp(nc);
            int indexCurve = j * nc;
            for (int k = 0; k < nc; k++)
            {
                int index = 3 * (k + indexCurve);
                cp[k] = vector2(in.cps[index + 0], in.cps[index + 1]);
            }
            curves.push_back(bezier_curve<vector2>(cp));
        }
        std::vector<real> knots(in.knots.size());
        ConvertKnots(knots, in.knots);
        out = multi_bezier_curve<vector2>(curves, knots);
        return true;
    }

    static bool ConvertLoop(std::vector<multi_bezier_curve<vector2> >& out, const io_bezier_loop& in)
    {
        size_t sz = in.curves.size();
        for (size_t i = 0; i < sz; i++)
        {
            multi_bezier_curve<vector2> tmp;
            if (!ConvertCurve(tmp, in.curves[i])) return false;
            out.push_back(tmp);
        }
        return true;
    }

    static bool ConvertLoops(std::vector<LoopType>& outer_loops, std::vector<LoopType>& inner_loops, const io_bezier_patch& in)
    {
        const std::vector<io_bezier_loop>& loops = in.loops;
        int nLoops = (int)loops.size();
        for (int j = 0; j < nLoops; j++)
        {
            int nDirection = loops[j].nDirection;
            LoopType tmp;
            if (!ConvertLoop(tmp, loops[j])) return false;
            if (nDirection > 0)
            {
                outer_loops.push_back(tmp);
            }
            else
            {
                inner_loops.push_back(tmp);
            }
        }

        return true;
    }
    //------------------------------------------------------------------------------------------------------------------
    static bool ConvertCurve(io_bezier_curve& out, const multi_bezier_curve<vector2>& in)
    {
        int ksz = in.get_knot_size();
        std::vector<double> knots(ksz);
        for (int i = 0; i < ksz; i++)
        {
            knots[i] = (double)in.get_knot_at(i);
        }
        int ns = ksz - 1;
        int nc = in.order();
        int csz = nc * ns * 3;
        out.knots.swap(knots);
        std::vector<double> cps(csz);
        size_t idx = 0;
        for (int i = 0; i < ns; i++)
        {
            const bezier_curve<vector2>& curve = in.get_curve_at(i);
            for (int j = 0; j < nc; j++)
            {
                vector2 cp = curve.get_cp_at(j);
                cps[idx++] = cp[0];
                cps[idx++] = cp[1];
                cps[idx++] = 0;
            }
        }
        out.cps.swap(cps);

        return true;
    }

    static bool ConvertLoop(io_bezier_loop& out, const LoopType& in)
    {
        size_t sz = in.size();
        for (size_t i = 0; i < sz; i++)
        {
            io_bezier_curve tmp;
            if (!ConvertCurve(tmp, in[i])) return false;
            out.curves.push_back(tmp);
        }
        return true;
    }

    static bool ConvertLoops(std::vector<io_bezier_loop>& out, const std::vector<LoopType>& outer_loops, const std::vector<LoopType>& inner_loops)
    {
        for (size_t i = 0; i < outer_loops.size(); i++)
        {
            io_bezier_loop tmp;
            if (!ConvertLoop(tmp, outer_loops[i])) return false;
            tmp.nDirection = 1; //
            out.push_back(tmp);
        }
        for (size_t i = 0; i < inner_loops.size(); i++)
        {
            io_bezier_loop tmp;
            if (!ConvertLoop(tmp, inner_loops[i])) return false;
            tmp.nDirection = -1; //
            out.push_back(tmp);
        }

        return true;
    }

    //------------------------------------------------------------------------------------------------------------------

    bool convert_bezier_patch(io_bezier_patch& out, const multi_bezier_patch<vector3>& in)
    {
        int nPu = in.get_patch_size_u();
        int nPv = in.get_patch_size_v();
        int nu = in.get_patch_at(0, 0).get_nu();
        int nv = in.get_patch_at(0, 0).get_nv();

        size_t total = nu * nv * 3 * nPu * nPv;

        std::vector<double> cps(total);
        size_t count = 0;
        for (int ipv = 0; ipv < nPv; ipv++)
        {
            for (int ipu = 0; ipu < nPu; ipu++)
            {
                const bezier_patch<vector3>& patch = in.get_patch_at(ipu, ipv);
                if (nu != patch.get_nu()) return false;
                if (nv != patch.get_nv()) return false;
                for (int v = 0; v < nv; v++)
                {
                    for (int u = 0; u < nu; u++)
                    {
                        vector3 p = patch.get_at(u, v);
                        cps[count + 0] = (double)p[0];
                        cps[count + 1] = (double)p[1];
                        cps[count + 2] = (double)p[2];
                        count += 3;
                    }
                }
            }
        }

        std::vector<double> uknots(in.get_knots_u().begin(), in.get_knots_u().end());
        std::vector<double> vknots(in.get_knots_v().begin(), in.get_knots_v().end());

        out.nu = nu;
        out.nv = nv;
        out.uknots.swap(uknots);
        out.vknots.swap(vknots);
        out.cps.swap(cps);
        out.nID = 0;
        out.nMat = 0;

        return true;
    }

    bool convert_bezier_patch(multi_bezier_patch<vector3>& out, const io_bezier_patch& in)
    {
        std::vector<bezier_patch<vector3> > patches;
        std::vector<real> uknots;
        std::vector<real> vknots;
        if (!ConvertPatch(uknots, vknots, patches, in)) return false;

        multi_bezier_patch<vector3> tmp(uknots, vknots, patches);
        out.swap(tmp);

        return true;
    }

    bool convert_bezier_patch(
        multi_bezier_patch<vector3>& out,
        std::vector<std::vector<multi_bezier_curve<vector2> > >& outer_loops,
        std::vector<std::vector<multi_bezier_curve<vector2> > >& inner_loops,
        const io_bezier_patch& in)
    {
        std::vector<bezier_patch<vector3> > patches;
        std::vector<real> uknots;
        std::vector<real> vknots;
        if (!ConvertPatch(uknots, vknots, patches, in)) return false;

        if (!ConvertLoops(outer_loops, inner_loops, in)) return false;

        multi_bezier_patch<vector3> tmp(uknots, vknots, patches);
        out.swap(tmp);

        return true;
    }

    bool convert_bezier_patch(trimmed_multi_bezier_patch<vector3, vector2>& out, const io_bezier_patch& in)
    {
        std::vector<bezier_patch<vector3> > patches;
        std::vector<real> uknots;
        std::vector<real> vknots;
        if (!ConvertPatch(uknots, vknots, patches, in)) return false;

        std::vector<LoopType> outer_loops;
        std::vector<LoopType> inner_loops;
        if (!ConvertLoops(outer_loops, inner_loops, in)) return false;

        trimmed_multi_bezier_patch<vector3, vector2> tmp(uknots, vknots, patches, outer_loops, inner_loops);
        out.swap(tmp);

        return true;
    }

    bool convert_bezier_patch(io_bezier_patch& out, const trimmed_multi_bezier_patch<vector3, vector2>& in)
    {
        int nPu = in.get_patch_size_u();
        int nPv = in.get_patch_size_v();
        int nu = in.get_patch_at(0, 0).get_nu();
        int nv = in.get_patch_at(0, 0).get_nv();

        size_t total = nu * nv * 3 * nPu * nPv;

        std::vector<double> cps(total);
        size_t count = 0;
        for (int ipv = 0; ipv < nPv; ipv++)
        {
            for (int ipu = 0; ipu < nPu; ipu++)
            {
                const bezier_patch<vector3>& patch = in.get_patch_at(ipu, ipv);
                if (nu != patch.get_nu()) return false;
                if (nv != patch.get_nv()) return false;
                for (int v = 0; v < nv; v++)
                {
                    for (int u = 0; u < nu; u++)
                    {
                        vector3 p = patch.get_at(u, v);
                        cps[count + 0] = (double)p[0];
                        cps[count + 1] = (double)p[1];
                        cps[count + 2] = (double)p[2];
                        count += 3;
                    }
                }
            }
        }

        std::vector<double> uknots(in.get_knots_u().begin(), in.get_knots_u().end());
        std::vector<double> vknots(in.get_knots_v().begin(), in.get_knots_v().end());

        std::vector<io_bezier_loop> loops;
        if (!ConvertLoops(loops, in.get_outer_loops(), in.get_inner_loops())) return false;

        out.nu = nu;
        out.nv = nv;
        out.uknots.swap(uknots);
        out.vknots.swap(vknots);
        out.cps.swap(cps);
        out.nID = 0;
        out.nMat = 0;
        out.loops.swap(loops);

        return true;
    }
}
