#include "memory_patch_saver.h"
#include "convert_bezier_patch.h"
#include <vector>

namespace kaze
{
    typedef memory_patch_saver::patch_type patch_type;

    memory_patch_saver::memory_patch_saver()
    {
    }

    memory_patch_saver::~memory_patch_saver()
    {
    }

    bool memory_patch_saver::set_patches(size_t sz)
    {
        mv_.reserve(sz);
        return true;
    }

    bool memory_patch_saver::set_patch(size_t i, io_bezier_patch* f)
    {
        patch_type tmp;
        if (!convert_bezier_patch(tmp, *f)) return false;
        mv_.push_back(tmp);

        return true;
    }

    size_t memory_patch_saver::size() const
    {
        return mv_.size();
    }

    const patch_type& memory_patch_saver::get(size_t i) const
    {
        return mv_[i];
    }

    patch_type& memory_patch_saver::get(size_t i)
    {
        return mv_[i];
    }
}
