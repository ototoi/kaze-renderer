#ifndef KAZE_BEZIER_PATCH_SAVER_H
#define KAZE_BEZIER_PATCH_SAVER_H

#include "types.h"
#include "io_bezier_patch.h"

namespace kaze
{

    class bezier_patch_loader;

    class bezier_patch_saver
    {
    public:
        virtual bool save(const bezier_patch_loader* ml);

    public:
        virtual bool set_patches(size_t sz);
        virtual bool set_patch(size_t i, io_bezier_patch* f);

    public:
        virtual ~bezier_patch_saver() {}
    };
}

#endif