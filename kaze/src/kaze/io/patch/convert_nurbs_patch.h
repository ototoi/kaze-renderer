#ifndef KAZE_CONVERT_NURBS_H
#define KAZE_CONVERT_NURBS_H

#include "io_nurbs_patch.h"
#include "bspline_patch.hpp"
#include "nurbs_patch.hpp"
#include "trimmed_nurbs_patch.hpp"

namespace kaze
{

    bool convert_nurbs_patch(io_nurbs_patch& out, const bspline_patch<vector3>& in);
    bool convert_nurbs_patch(bspline_patch<vector3>& out, const io_nurbs_patch& in);
    bool convert_nurbs_patch(io_nurbs_patch& out, const nurbs_patch<vector3>& in);
    bool convert_nurbs_patch(nurbs_patch<vector3>& out, const io_nurbs_patch& in);
    bool convert_nurbs_patch(trimmed_nurbs_patch<vector3, vector2>& out, const io_nurbs_patch& in);
    bool convert_nurbs_patch(io_nurbs_patch& out, const trimmed_nurbs_patch<vector3, vector2>& in);
}

#endif
