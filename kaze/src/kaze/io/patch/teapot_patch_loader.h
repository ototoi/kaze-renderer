#ifndef KAZE_TEAPOT_PATCH_LOADER_H
#define KAZE_TEAPOT_PATCH_LOADER_H

#include "bezier_patch_loader.h"

namespace kaze
{

    class teapot_patch_loader_imp;

    class teapot_patch_loader : public bezier_patch_loader
    {
    public:
        teapot_patch_loader();
        ~teapot_patch_loader();

    public:
        bool load(bezier_patch_saver* ms) const;
        bool get_patches(size_t& sz) const;
        bool get_patch(size_t i, io_bezier_patch* patch) const;

    private:
        teapot_patch_loader_imp* imp_;
    };
}

#endif