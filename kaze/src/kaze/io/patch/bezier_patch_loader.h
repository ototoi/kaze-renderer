#ifndef KAZE_BEZIER_PATCH_LOADER_H
#define KAZE_BEZIER_PATCH_LOADER_H

#include "types.h"
#include "io_bezier_patch.h"

namespace kaze
{

    class bezier_patch_saver;

    class bezier_patch_loader
    {
    public:
        virtual bool load(bezier_patch_saver* ms) const;

    public:
        virtual bool get_patches(size_t& sz) const;
        virtual bool get_patch(size_t i, io_bezier_patch* patch) const;

    public:
        virtual ~bezier_patch_loader() {}
    };
}

#endif
