#ifndef KAZE_KZS_BEZIERPATCH_SAVER_H
#define KAZE_KZS_BEZIERPATCH_SAVER_H

#include "bezier_patch_saver.h"

namespace kaze
{

    class kzs_patch_saver : public bezier_patch_saver
    {
    public:
        kzs_patch_saver(const char* file);
        ~kzs_patch_saver();
        bool save(const bezier_patch_loader* ml);

    private:
        FILE* fp_;
    };
}

#endif