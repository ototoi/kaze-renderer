#include "teapot_patch_loader.h"
#include "bezier_patch.hpp"
#include "multi_bezier_patch.hpp"
#include "values.h"
#include "transformer.h"
#include "convert_bezier_patch.h"
#include "bezier_patch_saver.h"
#include "bezier_patch_loader.h"

#include <string.h>
#include <map>
#include <vector>

namespace
{
    typedef int patch_t[16];
    typedef double vertex_t[3];

    static const patch_t Rim[] = {
        {102, 103, 104, 105, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
    };

    static const patch_t Body[] = {
        {12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27},
        {24, 25, 26, 27, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40},
    };

    static const patch_t Lid[] = {
        {96, 96, 96, 96, 97, 98, 99, 100, 101, 101, 101, 101, 0, 1, 2, 3},
        {0, 1, 2, 3, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117},
    };

    static const patch_t Handle[] = {
        {41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56},
        {53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 28, 65, 66, 67},
    };

    static const patch_t Spout[] = {
        {68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83},
        {80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95},
    };

    static const vertex_t TeapotVertices[] = {
        {0.2000, 0.0000, 2.70000}, {0.2000, -0.1120, 2.70000}, {0.1120, -0.2000, 2.70000}, {0.0000, -0.2000, 2.70000}, {1.3375, 0.0000, 2.53125}, {1.3375, -0.7490, 2.53125}, {0.7490, -1.3375, 2.53125}, {0.0000, -1.3375, 2.53125}, {1.4375, 0.0000, 2.53125}, {1.4375, -0.8050, 2.53125}, {0.8050, -1.4375, 2.53125}, {0.0000, -1.4375, 2.53125}, {1.5000, 0.0000, 2.40000}, {1.5000, -0.8400, 2.40000}, {0.8400, -1.5000, 2.40000}, {0.0000, -1.5000, 2.40000}, {1.7500, 0.0000, 1.87500}, {1.7500, -0.9800, 1.87500}, {0.9800, -1.7500, 1.87500}, {0.0000, -1.7500, 1.87500}, {2.0000, 0.0000, 1.35000}, {2.0000, -1.1200, 1.35000}, {1.1200, -2.0000, 1.35000}, {0.0000, -2.0000, 1.35000}, {2.0000, 0.0000, 0.90000}, {2.0000, -1.1200, 0.90000}, {1.1200, -2.0000, 0.90000}, {0.0000, -2.0000, 0.90000}, {-2.0000, 0.0000, 0.90000}, {2.0000, 0.0000, 0.45000}, {2.0000, -1.1200, 0.45000}, {1.1200, -2.0000, 0.45000}, {0.0000, -2.0000, 0.45000}, {1.5000, 0.0000, 0.22500}, {1.5000, -0.8400, 0.22500}, {0.8400, -1.5000, 0.22500}, {0.0000, -1.5000, 0.22500}, {1.5000, 0.0000, 0.15000}, {1.5000, -0.8400, 0.15000}, {0.8400, -1.5000, 0.15000}, {0.0000, -1.5000, 0.15000}, {-1.6000, 0.0000, 2.02500}, {-1.6000, -0.3000, 2.02500}, {-1.5000, -0.3000, 2.25000}, {-1.5000, 0.0000, 2.25000}, {-2.3000, 0.0000, 2.02500}, {-2.3000, -0.3000, 2.02500}, {-2.5000, -0.3000, 2.25000}, {-2.5000, 0.0000, 2.25000}, {-2.7000, 0.0000, 2.02500}, {-2.7000, -0.3000, 2.02500}, {-3.0000, -0.3000, 2.25000}, {-3.0000, 0.0000, 2.25000}, {-2.7000, 0.0000, 1.80000}, {-2.7000, -0.3000, 1.80000}, {-3.0000, -0.3000, 1.80000}, {-3.0000, 0.0000, 1.80000}, {-2.7000, 0.0000, 1.57500}, {-2.7000, -0.3000, 1.57500}, {-3.0000, -0.3000, 1.35000}, {-3.0000, 0.0000, 1.35000}, {-2.5000, 0.0000, 1.12500}, {-2.5000, -0.3000, 1.12500}, {-2.6500, -0.3000, 0.93750}, {-2.6500, 0.0000, 0.93750}, {-2.0000, -0.3000, 0.90000}, {-1.9000, -0.3000, 0.60000}, {-1.9000, 0.0000, 0.60000}, {1.7000, 0.0000, 1.42500}, {1.7000, -0.6600, 1.42500}, {1.7000, -0.6600, 0.60000}, {1.7000, 0.0000, 0.60000}, {2.6000, 0.0000, 1.42500}, {2.6000, -0.6600, 1.42500}, {3.1000, -0.6600, 0.82500}, {3.1000, 0.0000, 0.82500}, {2.3000, 0.0000, 2.10000}, {2.3000, -0.2500, 2.10000}, {2.4000, -0.2500, 2.02500}, {2.4000, 0.0000, 2.02500}, {2.7000, 0.0000, 2.40000}, {2.7000, -0.2500, 2.40000}, {3.3000, -0.2500, 2.40000}, {3.3000, 0.0000, 2.40000}, {2.8000, 0.0000, 2.47500}, {2.8000, -0.2500, 2.47500}, {3.5250, -0.2500, 2.49375}, {3.5250, 0.0000, 2.49375}, {2.9000, 0.0000, 2.47500}, {2.9000, -0.1500, 2.47500}, {3.4500, -0.1500, 2.51250}, {3.4500, 0.0000, 2.51250}, {2.8000, 0.0000, 2.40000}, {2.8000, -0.1500, 2.40000}, {3.2000, -0.1500, 2.40000}, {3.2000, 0.0000, 2.40000}, {0.0000, 0.0000, 3.15000}, {0.8000, 0.0000, 3.15000}, {0.8000, -0.4500, 3.15000}, {0.4500, -0.8000, 3.15000}, {0.0000, -0.8000, 3.15000}, {0.0000, 0.0000, 2.85000}, {1.4000, 0.0000, 2.40000}, {1.4000, -0.7840, 2.40000}, {0.7840, -1.4000, 2.40000}, {0.0000, -1.4000, 2.40000}, {0.4000, 0.0000, 2.55000}, {0.4000, -0.2240, 2.55000}, {0.2240, -0.4000, 2.55000}, {0.0000, -0.4000, 2.55000}, {1.3000, 0.0000, 2.55000}, {1.3000, -0.7280, 2.55000}, {0.7280, -1.3000, 2.55000}, {0.0000, -1.3000, 2.55000}, {1.3000, 0.0000, 2.40000}, {1.3000, -0.7280, 2.40000}, {0.7280, -1.3000, 2.40000}, {0.0000, -1.3000, 2.40000},
    };
}
namespace kaze
{
    namespace
    {
        vector3 conv(const double* v)
        {
            return vector3(v[0], v[1], v[2]);
        }

        bezier_patch<vector3> GetPatch(const int patch[16])
        {
            std::vector<vector3> tmp(16);
            for (int i = 0; i < 16; i++)
                tmp[i] = conv(TeapotVertices[patch[i]]);
            return bezier_patch<vector3>(4, 4, tmp);
        }

        //�ӂ�
        void GetRim(std::vector<bezier_patch<vector3> >& patches)
        {
            bezier_patch<vector3> patch = GetPatch(Rim[0]);
            for (int i = 0; i < 4; i++)
            {
                patches.push_back(mat4_gen::rotation_z(0.5 * i * values::pi()) * patch);
            }
        }

        //�{��
        void GetBody(std::vector<bezier_patch<vector3> >& patches)
        {
            bezier_patch<vector3> patch[2];
            patch[0] = GetPatch(Body[0]);
            patch[1] = GetPatch(Body[1]);
            for (int i = 0; i < 4; i++)
            {
                patches.push_back(mat4_gen::rotation_z(0.5 * i * values::pi()) * patch[0]);
                patches.push_back(mat4_gen::rotation_z(0.5 * i * values::pi()) * patch[1]);
            }
        }
        //�ӂ�
        void GetLid(std::vector<bezier_patch<vector3> >& patches)
        {
            bezier_patch<vector3> patch[2];
            patch[0] = GetPatch(Lid[0]);
            patch[1] = GetPatch(Lid[1]);
            for (int i = 0; i < 4; i++)
            {
                patches.push_back(mat4_gen::rotation_z(0.5 * i * values::pi()) * patch[0]);
                patches.push_back(mat4_gen::rotation_z(0.5 * i * values::pi()) * patch[1]);
            }
        }
        //�Ƃ���
        void GetHandle(std::vector<bezier_patch<vector3> >& patches)
        {
            bezier_patch<vector3> patch[2];
            patch[0] = GetPatch(Handle[0]);
            patch[1] = GetPatch(Handle[1]);
            matrix4 m = mat4_gen::scaling(1, -1, 1);

            {
                patches.push_back(patch[0]);
                patches.push_back(patch[1]);
            }
            {
                patches.push_back((m * patch[0]).swap_u());
                patches.push_back((m * patch[1]).swap_u());
            }
        }
        //������
        void GetSpout(std::vector<bezier_patch<vector3> >& patches)
        {
            bezier_patch<vector3> patch[2];
            patch[0] = GetPatch(Spout[0]);
            patch[1] = GetPatch(Spout[1]);
            matrix4 m = mat4_gen::scaling(1, -1, 1);

            {
                patches.push_back(patch[0]);
                patches.push_back(patch[1]);
            }
            {
                patches.push_back((m * patch[0]).swap_u());
                patches.push_back((m * patch[1]).swap_u());
            }
        }

        void GetTeaPot(std::vector<bezier_patch<vector3> >& patches)
        {
            GetRim(patches);
            GetBody(patches);
            GetLid(patches);
            GetHandle(patches);
            GetSpout(patches);
        }
    }
}

namespace kaze
{

    class teapot_patch_loader_imp
    {
    public:
        teapot_patch_loader_imp();
        bool load(bezier_patch_saver* ms) const;
        bool get_patches(size_t& sz) const;
        bool get_patch(size_t i, io_bezier_patch* patch) const;

    private:
        std::vector<bezier_patch<vector3> > mv_;
    };

    teapot_patch_loader_imp::teapot_patch_loader_imp()
    {
        GetTeaPot(mv_);
    }

    bool teapot_patch_loader_imp::load(bezier_patch_saver* ms) const
    {
        size_t sz = 0;
        if (!get_patches(sz)) return false;
        if (!ms->set_patches(sz)) return false;
        for (size_t i = 0; i < sz; i++)
        {
            io_bezier_patch tmp;
            if (!get_patch(i, &tmp)) return false;
            if (!ms->set_patch(i, &tmp)) return false;
        }
        return true;
    }

    bool teapot_patch_loader_imp::get_patches(size_t& sz) const
    {
        sz = mv_.size();
        return true;
    }

    bool teapot_patch_loader_imp::get_patch(size_t i, io_bezier_patch* patch) const
    {
        if (mv_.size() <= i) return false;
        multi_bezier_patch<vector3> mp(mv_[i]);
        if (!convert_bezier_patch(*patch, mp)) return false;
        return true;
    }

    teapot_patch_loader::teapot_patch_loader()
    {
        imp_ = new teapot_patch_loader_imp();
    }

    teapot_patch_loader::~teapot_patch_loader()
    {
        delete imp_;
    }

    bool teapot_patch_loader::load(bezier_patch_saver* ms) const
    {
        return imp_->load(ms);
    }

    bool teapot_patch_loader::get_patches(size_t& sz) const
    {
        return imp_->get_patches(sz);
    }

    bool teapot_patch_loader::get_patch(size_t i, io_bezier_patch* patch) const
    {
        return imp_->get_patch(i, patch);
    }
}
