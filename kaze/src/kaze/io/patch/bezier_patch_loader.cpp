#include "bezier_patch_loader.h"
#include "bezier_patch_saver.h"

namespace kaze
{

    bool bezier_patch_loader::load(bezier_patch_saver* ms) const
    {
        const bezier_patch_loader* ml = this;

        size_t sz = 0;
        if (!ml->get_patches(sz)) return false;
        if (!ms->set_patches(sz)) return false;
        for (size_t i = 0; i < sz; i++)
        {
            io_bezier_patch tmp;
            if (!ml->get_patch(i, &tmp)) return false;
            if (!ms->set_patch(i, &tmp)) return false;
        }
        return true;
    }

    bool bezier_patch_loader::get_patches(size_t& sz) const
    {
        return false;
    }

    bool bezier_patch_loader::get_patch(size_t i, io_bezier_patch* patch) const
    {
        return false;
    }
}
