#include "nurbs_patch_saver.h"
#include "nurbs_patch_loader.h"

namespace kaze
{

    bool nurbs_patch_saver::save(const nurbs_patch_loader* ml)
    {
        nurbs_patch_saver* ms = this;

        size_t sz = 0;
        if (!ml->get_patches(sz)) return false;
        if (!ms->set_patches(sz)) return false;
        for (size_t i = 0; i < sz; i++)
        {
            io_nurbs_patch tmp;
            if (!ml->get_patch(i, &tmp)) return false;
            if (!ms->set_patch(i, &tmp)) return false;
        }
        return true;
    }

    bool nurbs_patch_saver::set_patches(size_t sz)
    {
        return false;
    }

    bool nurbs_patch_saver::set_patch(size_t i, io_nurbs_patch* f)
    {
        return false;
    }
}