#ifndef KAZE_NURBS_PATCH_SAVER_H
#define KAZE_NURBS_PATCH_SAVER_H

#include "types.h"
#include "io_nurbs_patch.h"

namespace kaze
{

    class nurbs_patch_loader;

    class nurbs_patch_saver
    {
    public:
        virtual bool save(const nurbs_patch_loader* ml);

    public:
        virtual bool set_patches(size_t sz);
        virtual bool set_patch(size_t i, io_nurbs_patch* f);

    public:
        virtual ~nurbs_patch_saver() {}
    };
}

#endif
