#ifndef KAZE_NURBS_PATCH_LOADER_H
#define KAZE_NURBS_PATCH_LOADER_H

#include "io_nurbs_patch.h"
#include <cstdlib>

#ifndef _WIN32
#include <stdint.h>
#endif

namespace kaze
{

    class nurbs_patch_saver;

    class nurbs_patch_loader
    {
    public:
        virtual bool load(nurbs_patch_saver* ms) const;

    public:
        virtual bool get_patches(size_t& sz) const;
        virtual bool get_patch(size_t i, io_nurbs_patch* patch) const;

    public:
        virtual ~nurbs_patch_loader() {}
    };
}

#endif
