#include "bezier_patch_saver.h"
#include "bezier_patch_loader.h"

#include <stdlib.h>

namespace kaze
{

    bool bezier_patch_saver::save(const bezier_patch_loader* ml)
    {
        bezier_patch_saver* ms = this;

        size_t sz = 0;
        if (!ml->get_patches(sz)) return false;
        if (!ms->set_patches(sz)) return false;
        for (size_t i = 0; i < sz; i++)
        {
            io_bezier_patch tmp;
            if (!ml->get_patch(i, &tmp)) return false;
            if (!ms->set_patch(i, &tmp)) return false;
        }
        return true;
    }

    bool bezier_patch_saver::set_patches(size_t sz)
    {
        return false;
    }

    bool bezier_patch_saver::set_patch(size_t i, io_bezier_patch* f)
    {
        return false;
    }
}
