#include "jsp_patch_loader.h"
#include "nurbs_patch_saver.h"

#pragma warning(disable : 4800)

#include "picojson/picojson.h"

#include "bspline_patch.hpp"

#include "timer.h"
#include "logger.h"

#include <iostream>
#include <fstream>
#include <vector>

#define FLIP_V 0

namespace kaze
{
    typedef jsp_patch_loader::NURBSPatch NURBSPatch;
    typedef jsp_patch_loader::NURBSLoop NURBSLoop;
    typedef jsp_patch_loader::NURBSCurve NURBSCurve;

    static int get_int(const picojson::value& v, const char* szKey)
    {
        const picojson::value& c = v.get(szKey);
        if (!c.is<double>()) return 0;
        int nRet = (int)c.get<double>();
        return nRet;
    }

    static std::string get_string(const picojson::value& v, const char* szKey)
    {
        const picojson::value& c = v.get(szKey);
        if (!c.is<std::string>()) return "";
        std::string nRet = c.get<std::string>();
        return nRet;
    }

    static std::vector<double> get_double_array(const picojson::value& v, const char* szKey)
    {
        std::vector<double> tmp;
        const picojson::value& c = v.get(szKey);
        if (!c.is<picojson::array>()) return tmp;
        const picojson::array& a = c.get<picojson::array>();
        tmp.resize(a.size());
        for (size_t i = 0; i < a.size(); i++)
        {
            tmp[i] = a[i].get<double>();
        }
        return tmp;
    }

    static bool is_open0(const std::vector<double>& knots, int k, int m)
    {
        int nOrder = m;
        int nKnots = k;
        if (nKnots >= nOrder)
        {
            for (int i = 1; i < nOrder; i++)
            {
                if (knots[i] != knots[0]) return false;
            }
            return true;
        }
        return false;
    }

    static bool is_open1(const std::vector<double>& knots, int k, int m)
    {
        int nOrder = m;
        int nKnots = k;
        if (nKnots >= nOrder)
        {
            for (int i = 1; i < nOrder; i++)
            {
                if (knots[nKnots - 1 - i] != knots[nKnots - 1]) return false;
            }
            return true;
        }
        return false;
    }

    static bool is_open(const std::vector<double>& knots, int k, int m)
    {
        return is_open0(knots, k, m) && is_open1(knots, k, m);
    }

    static bool ValidateKnotVector(std::vector<double>& knots, int order)
    {
        const double eps = std::numeric_limits<double>::epsilon();
        const double feps = std::numeric_limits<float>::epsilon();

        if (knots.size() < 2)
        {
            return false;
        }

        // Check monotonicality
        {
            double prev = knots[0];
            for (int i = 1; i < knots.size(); i++)
            {
                if (prev > knots[i])
                {
                    return false;
                }
                else
                {
                    prev = knots[i];
                }
            }
        }

        // Check zero-size knots
        {
            double start = knots.front();
            double end = knots.back();
            if (start >= end) return false;
            if ((end - start) <= eps) return false;
        }

        // Check multiple knots.
        //if (!is_open(knots, knots.size(), order)) {
        //    return false;
        //}

        /*
        // Check zero-size knots [for float]...
        {
            float start = (float)knots.front();
            float end = (float)knots.back();
            if (start >= end) return false;
            if ((end - start) <= feps) return false;
        }
		*/

        return true;
    }

    static void Convert(bspline_patch<vector4d>& out, const NURBSPatch& in)
    {
        int nu = in.cvs_u;
        int nv = in.cvs_v;
        size_t tsz = in.cv.size() / 4;
        std::vector<vector4d> cps(tsz);
        for (size_t i = 0; i < tsz; i++)
        {
            double x = in.cv[4 * i + 0];
            double y = in.cv[4 * i + 1];
            double z = in.cv[4 * i + 2];
            double w = in.cv[4 * i + 3];
            cps[i] = vector4d(x, y, z, w);
        }
        size_t usz = in.knots_u.size();
        size_t vsz = in.knots_v.size();
        std::vector<double> uknots(usz);
        std::vector<double> vknots(vsz);
        for (size_t i = 0; i < usz; i++)
            uknots[i] = (double)in.knots_u[i];
        for (size_t i = 0; i < vsz; i++)
            vknots[i] = (double)in.knots_v[i];
        bspline_patch<vector4d> bs(nu, nv, cps, uknots, vknots);
        out.swap(bs);
    }

    static void Convert(NURBSPatch& out, const bspline_patch<vector4d>& in)
    {
        int nu = in.get_nu();
        int nv = in.get_nv();
        size_t tsz = in.get_cp_size();
        std::vector<double> cps(tsz * 4);
        for (size_t i = 0; i < tsz; i++)
        {
            vector4d p = in.get_cp()[i];
            cps[4 * i + 0] = p[0];
            cps[4 * i + 1] = p[1];
            cps[4 * i + 2] = p[2];
            cps[4 * i + 3] = p[3];
        }
        int usz = in.get_knot_size_u();
        int vsz = in.get_knot_size_v();
        std::vector<double> uknots(usz);
        std::vector<double> vknots(vsz);
        for (int i = 0; i < usz; i++)
            uknots[i] = (double)in.get_knot_at_u(i);
        for (int i = 0; i < vsz; i++)
            vknots[i] = (double)in.get_knot_at_v(i);

        out.cvs_u = nu;
        out.cvs_v = nv;
        out.cv.swap(cps);
        out.knots_u.swap(uknots);
        out.knots_v.swap(vknots);
    }

    static void KillPeriodic(NURBSPatch& out, const NURBSPatch& in)
    {
        bool bPU = false;
        bool bPV = false;
        if (in.form_u == "periodic") bPU = true;
        if (in.form_v == "periodic") bPV = true;
        if (bPU || bPV)
        {
            NURBSPatch tmp = in;
            bspline_patch<vector4d> bs;
            Convert(bs, tmp);
            bs.to_nonperiodic(bPU, bPV);
            Convert(tmp, bs);
            out = tmp;
        }
        else
        {
            out = in;
        }
    }

    static bool ConvertCurve(io_nurbs_curve& out, const NURBSCurve& in, const matrix3d& m)
    {
        int cps = (int)in.cv.size() / 2;
        std::vector<double> cp(cps * 4);
        for (int i = 0; i < cps; i++)
        {
            vector2d p(in.cv[2 * i + 0], in.cv[2 * i + 1]);
            p = m * p;

#if FLIP_V
            double v = p[1];
            v = 1 - v;
            //v *= 0.5;
            //v = 1-v;
            p[1] = v;
#endif
            //p = ~m*p;
            cp[4 * i + 0] = p[0];
            cp[4 * i + 1] = p[1];
            cp[4 * i + 2] = 0; //z
            cp[4 * i + 3] = 1; //w=1
        }
        assert(in.cvs == cps);
        std::vector<double> knots = in.knots;
        knots.resize(in.cvs + in.degree + 1);
        out.cps = cp;
        out.knots = knots;
        return true;
    }

    static bool ConvertLoop(io_nurbs_loop& out, const NURBSLoop& in, const matrix3d& m)
    {
        int nDirection = 1;
        if (in.loop_type == "inner")
        {
            nDirection = -1;
        }
        else
        {
            nDirection = +1;
        }
        out.nDirection = nDirection;
        for (size_t i = 0; i < in.curves.size(); i++)
        {
            io_nurbs_curve tmp;
            if (!ConvertCurve(tmp, in.curves[i], m)) return false;
            int nOrder = in.curves[i].degree + 1;
            if (ValidateKnotVector(tmp.knots, nOrder))
            {
                out.curves.push_back(tmp);
            }
            else
            {
                /*
                //create uniform knots.
                int sz = tmp.knots.size();
                for(int i=0;i<sz;i++){
                    tmp.knots[i] = (double)i;
                }
                out.curves.push_back(tmp); 
                */
            }
        }
        return true;
    }

    static bool ConvertLoops(std::vector<io_nurbs_loop>& out, const std::vector<NURBSLoop>& in, const matrix3d& m)
    {
        for (size_t i = 0; i < in.size(); i++)
        {
            io_nurbs_loop tmp;
            if (!ConvertLoop(tmp, in[i], m)) return false;
            out.push_back(tmp);
        }
        return true;
    }

    static std::vector<double> flip_cv(int nu, int nv, const std::vector<double>& cv)
    {
#if FLIP_V
        int sz = nu * nv * 4;
        std::vector<double> tmp(sz);
        for (int j = 0; j < nv; j++)
        {
            for (int i = 0; i < nu; i++)
            {
                int idx = j * nu + i;
                int idx2 = (nv - 1 - j) * nu + i;
                tmp[4 * idx + 0] = cv[4 * idx2 + 0];
                tmp[4 * idx + 1] = cv[4 * idx2 + 1];
                tmp[4 * idx + 2] = cv[4 * idx2 + 2];
                tmp[4 * idx + 3] = cv[4 * idx2 + 3];
            }
        }
        return tmp;
#else
        return cv;
#endif
    }

    static bool ConvertPatch(io_nurbs_patch* patch, const NURBSPatch& in)
    {
        NURBSPatch tmp;
        KillPeriodic(tmp, in);

        patch->cps = flip_cv(tmp.cvs_u, tmp.cvs_v, tmp.cv);
        patch->nu = tmp.cvs_u;
        patch->nv = tmp.cvs_v;
        patch->uknots = tmp.knots_u;
        patch->vknots = tmp.knots_v;
        patch->nID = 0;
        patch->nMat = 0;

        double dx = -tmp.domain_u[0];
        double dy = -tmp.domain_v[0];
        double sx = 1.0 / (tmp.domain_u[1] - tmp.domain_u[0]);
        double sy = 1.0 / (tmp.domain_v[1] - tmp.domain_v[0]);

        matrix3d s(
            sx, 0, 0,
            0, sy, 0,
            0, 0, 1);
        matrix3d t(
            1, 0, dx,
            0, 1, dy,
            0, 0, 1);

        matrix3d m = s * t;

        if (!ConvertLoops(patch->loops, in.loops, m)) return false;
        return true;
    }

    static bool ConvertCurve(NURBSCurve& curve, const picojson::value& v)
    {
        int degree = get_int(v, "degree");
        int cvs = get_int(v, "cvs");
        std::vector<double> cv = get_double_array(v, "cv");
        std::vector<double> domain = get_double_array(v, "knotDomain");
        std::vector<double> knots = get_double_array(v, "knot");

        std::string form = get_string(v, "form");

        curve.degree = degree;
        curve.cvs = cvs;
        curve.cv.swap(cv);
        curve.domain.swap(domain);
        curve.knots.swap(knots);
        curve.form = form;
        return true;
    }

    static bool ConvertLoop(NURBSLoop& loop, const picojson::value& v)
    {
        const picojson::value& clt = v.get("loop_type");
        if (!clt.is<std::string>()) return false;
        const std::string& lt = clt.get<std::string>();

        const picojson::value& ccv = v.get("curves");
        if (!ccv.is<picojson::array>()) return false;
        const picojson::array& cv = ccv.get<picojson::array>();
        std::vector<NURBSCurve> curves;
        for (int i = 0; i < cv.size(); i++)
        {
            NURBSCurve tmp;
            if (!ConvertCurve(tmp, cv[i])) return false;
            curves.push_back(tmp);
        }
        loop.loop_type = lt;
        loop.curves.swap(curves);
        return true;
    }

    static bool CovertLoops(std::vector<NURBSLoop>& loops, const picojson::value& v)
    {
        const picojson::value& c = v.get("loops");
        if (!c.is<picojson::array>()) return false;
        const picojson::array& a = c.get<picojson::array>();
        for (int i = 0; i < a.size(); i++)
        {
            NURBSLoop tmp;
            if (!ConvertLoop(tmp, a[i])) return false;
            loops.push_back(tmp);
        }
        return true;
    }

    static void PushPatch(std::vector<NURBSPatch>& mv, const picojson::value& v)
    {
        int degree_u = get_int(v, "degreeU");
        int degree_v = get_int(v, "degreeV");
        int cvs_u = get_int(v, "cvsU");
        int cvs_v = get_int(v, "cvsV");
        std::vector<double> cv = get_double_array(v, "cv");
        std::vector<double> domain_u = get_double_array(v, "knotDomainU");
        std::vector<double> domain_v = get_double_array(v, "knotDomainV");
        std::vector<double> knots_u = get_double_array(v, "knotU");
        std::vector<double> knots_v = get_double_array(v, "knotV");

        std::string form_u = get_string(v, "formU");
        std::string form_v = get_string(v, "formV");

        if (!cv.empty())
        {
            size_t tsz = cv.size() / 3;
            std::vector<double> cv2(tsz * 4);
            for (size_t i = 0; i < tsz; i++)
            {
                cv2[4 * i + 0] = cv[3 * i + 0];
                cv2[4 * i + 1] = cv[3 * i + 1];
                cv2[4 * i + 2] = cv[3 * i + 2];
                cv2[4 * i + 3] = 1.0;
            }
            cv.swap(cv2);
        }

        NURBSPatch tmp;
        tmp.degree_u = degree_u;
        tmp.degree_v = degree_v;
        tmp.cvs_u = cvs_u;
        tmp.cvs_v = cvs_v;
        tmp.cv.swap(cv);
        tmp.domain_u.swap(domain_u);
        tmp.domain_v.swap(domain_v);
        tmp.knots_u.swap(knots_u);
        tmp.knots_v.swap(knots_v);
        tmp.form_u = form_u;
        tmp.form_v = form_v;

        std::vector<NURBSLoop> loops;
        if (CovertLoops(loops, v))
        {
            tmp.loops.swap(loops);
        }
        mv.push_back(tmp);
    }

    jsp_patch_loader::jsp_patch_loader(const char* szFilePath)
    {
        timer t;
        print_log("jsp_patch_loader:pre-loading start...\n");
        t.start();
        std::ifstream is(szFilePath);
        if (!is) return;
        picojson::value v;
        picojson::parse(v, is);

        if (v.is<picojson::array>())
        {
            const picojson::array& a = v.get<picojson::array>();
            for (size_t i = 0; i < a.size(); i++)
            {
                PushPatch(mv_, a[i]);
            }
        }
        else
        {
            PushPatch(mv_, v);
        }
        t.end();
        print_log("jsp_patch_loader:pre-loading end %d ms.\n", t.msec());
    }

    jsp_patch_loader::~jsp_patch_loader()
    {
    }

    bool jsp_patch_loader::load(nurbs_patch_saver* ms) const
    {
        timer t;
        print_log("jsp_patch_loader:loading start...\n");
        t.start();

        size_t sz = mv_.size();
        if (!ms->set_patches(sz)) return false;
        for (size_t i = 0; i < sz; i++)
        {
            io_nurbs_patch tmp;
            if (!this->get_patch(i, &tmp)) return false;
            if (!ms->set_patch(i, &tmp)) return false;
        }

        t.end();
        print_log("jsp_patch_loader:loading end %d ms.\n", t.msec());

        return true;
    }

    bool jsp_patch_loader::get_patches(size_t& sz) const
    {
        sz = mv_.size();
        return true;
    }

    bool jsp_patch_loader::get_patch(size_t i, io_nurbs_patch* patch) const
    {
        if (i >= mv_.size()) return false;
        if (!ConvertPatch(patch, mv_[i])) return false;
        return true;
    }
}
