#include "memory_patch_loader.h"
#include "convert_bezier_patch.h"

#include <vector>

namespace kaze
{

    memory_patch_loader::memory_patch_loader()
    {
    }

    memory_patch_loader::~memory_patch_loader()
    {
    }

    bool memory_patch_loader::get_patches(size_t& sz) const
    {
        sz = mv_.size();
        return true;
    }

    bool memory_patch_loader::get_patch(size_t i, io_bezier_patch* patch) const
    {
        if (i >= mv_.size()) return true;
        if (!convert_bezier_patch(*patch, mv_[i])) return false;

        return true;
    }
    void memory_patch_loader::add(const patch_type& patch)
    {
        mv_.push_back(patch);
    }
}
