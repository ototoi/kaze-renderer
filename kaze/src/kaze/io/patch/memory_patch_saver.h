#ifndef KAZE_MEMORY_BEZIERPATCH_SAVER_H
#define KAZE_MEMORY_BEZIERPATCH_SAVER_H

#include "bezier_patch_saver.h"
#include "trimmed_multi_bezier_patch.hpp"

namespace kaze
{

    class memory_patch_saver : public bezier_patch_saver
    {
    public:
        typedef trimmed_multi_bezier_patch<vector3, vector2> patch_type;

    public:
        memory_patch_saver();
        ~memory_patch_saver();
        bool set_patches(size_t sz);
        bool set_patch(size_t i, io_bezier_patch* f);

    public:
        void clear() { mv_.clear(); }
        size_t size() const;
        const patch_type& get(size_t i) const;
        patch_type& get(size_t i);

    private:
        std::vector<patch_type> mv_;
    };
}

#endif