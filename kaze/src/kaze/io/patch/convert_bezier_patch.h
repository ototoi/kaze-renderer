#ifndef KAZE_CONVERT_BEZIER_PATCH_H
#define KAZE_CONVERT_BEZIER_PATCH_H

#include "io_bezier_patch.h"
#include "multi_bezier_patch.hpp"
#include "trimmed_multi_bezier_patch.hpp"

namespace kaze
{

    bool convert_bezier_patch(io_bezier_patch& out, const multi_bezier_patch<vector3>& in);
    bool convert_bezier_patch(multi_bezier_patch<vector3>& out, const io_bezier_patch& in);
    bool convert_bezier_patch(
        multi_bezier_patch<vector3>& out,
        std::vector<std::vector<multi_bezier_curve<vector2> > >& outer_loops,
        std::vector<std::vector<multi_bezier_curve<vector2> > >& inner_loops,
        const io_bezier_patch& in);
    bool convert_bezier_patch(trimmed_multi_bezier_patch<vector3, vector2>& out, const io_bezier_patch& in);
    bool convert_bezier_patch(io_bezier_patch& out, const trimmed_multi_bezier_patch<vector3, vector2>& in);
}

#endif
