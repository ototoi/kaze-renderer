#include "nurbs_patch_loader.h"
#include "nurbs_patch_saver.h"

namespace kaze
{

    bool nurbs_patch_loader::load(nurbs_patch_saver* ms) const
    {
        const nurbs_patch_loader* ml = this;

        size_t sz = 0;
        if (!ml->get_patches(sz)) return false;
        if (!ms->set_patches(sz)) return false;
        for (size_t i = 0; i < sz; i++)
        {
            io_nurbs_patch tmp;
            if (!ml->get_patch(i, &tmp)) return false;
            if (!ms->set_patch(i, &tmp)) return false;
        }
        return true;
    }

    bool nurbs_patch_loader::get_patches(size_t& sz) const
    {
        return false;
    }

    bool nurbs_patch_loader::get_patch(size_t i, io_nurbs_patch* patch) const
    {
        return false;
    }
}
