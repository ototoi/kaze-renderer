#include "kzs_patch_loader.h"
#include "bezier_patch_loader.h"
#include "bezier_patch_saver.h"
#include <stdio.h>
#include "kzs_format.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#pragma warning(disable : 4996)

namespace kaze
{

    static bool KZSReadHeader(FILE* fp, kzs_header& header)
    {
        if (1 != fread(&header, sizeof(kzs_header), 1, fp)) return false;
        if (memcmp(header.magic, "KZS", sizeof(char) * 4) != 0) return false;
        return true;
    }

    static bool KZSReadCurve(FILE* fp, io_bezier_curve& curve)
    {
        int ns;
        int nc;
        if (1 != fread(&nc, sizeof(int), 1, fp)) return false;
        if (1 != fread(&ns, sizeof(int), 1, fp)) return false;

        size_t szKnots = ns + 1;
        size_t szCps = nc * ns * 3;

        curve.knots.resize(szKnots);
        curve.cps.resize(szCps);

        if (1 != fread(&(curve.knots[0]), szKnots * sizeof(double), 1, fp)) return false;
        if (1 != fread(&(curve.cps[0]), szCps * sizeof(double), 1, fp)) return false;

        return true;
    }

    static bool KZSReadLoop(FILE* fp, io_bezier_loop& loop)
    {
        int nDirection;
        int nCurves;
        std::vector<io_bezier_curve> curves;

        if (1 != fread(&nDirection, sizeof(int), 1, fp)) return false;
        if (1 != fread(&nCurves, sizeof(int), 1, fp)) return false;

        curves.resize(nCurves);
        for (int i = 0; i < nCurves; i++)
        {
            if (!KZSReadCurve(fp, curves[i])) return false;
        }

        loop.nDirection = nDirection;
        loop.curves.swap(curves);

        return true;
    }

    static bool KZSReadPatch(FILE* fp, io_bezier_patch& patch)
    {
        int nType;
        int nu;
        int nv;
        int nPU;
        int nPV;
        unsigned int nID;
        unsigned int nMat;
        std::vector<double> uknots;
        std::vector<double> vknots;
        std::vector<double> cps;
        std::vector<io_bezier_loop> loops;

        if (1 != fread(&nType, sizeof(int), 1, fp)) return false;
        if (1 != fread(&nu, sizeof(int), 1, fp)) return false;
        if (1 != fread(&nv, sizeof(int), 1, fp)) return false;
        if (1 != fread(&nPU, sizeof(int), 1, fp)) return false;
        if (1 != fread(&nPV, sizeof(int), 1, fp)) return false;

        int szUKnots = nPU + 1;
        int szVKnots = nPV + 1;
        int szCPs = (3 * nu * nv * nPU * nPV);
        uknots.resize(szUKnots);
        vknots.resize(szVKnots);
        cps.resize(szCPs);

        if (1 != fread(&(uknots[0]), szUKnots * sizeof(double), 1, fp)) return false;
        if (1 != fread(&(vknots[0]), szVKnots * sizeof(double), 1, fp)) return false;
        if (1 != fread(&(cps[0]), szCPs * sizeof(double), 1, fp)) return false;

        int nLoops = 0;
        if (1 != fread(&(nLoops), sizeof(int), 1, fp)) return false;
        loops.resize(nLoops);
        for (int i = 0; i < nLoops; i++)
        {
            if (!KZSReadLoop(fp, loops[i])) return false;
        }

        if (1 != fread(&nID, sizeof(unsigned int), 1, fp)) return false;
        if (1 != fread(&nMat, sizeof(unsigned int), 1, fp)) return false;

        patch.nu = nu;
        patch.nv = nv;
        patch.uknots.swap(uknots);
        patch.vknots.swap(vknots);
        patch.cps.swap(cps);
        patch.loops.swap(loops);
        patch.nID = nID;
        patch.nMat = nMat;

        return true;
    }
}

namespace kaze
{

    kzs_patch_loader::kzs_patch_loader(const char* file)
    {
        fp_ = fopen(file, "rb");
    }

    kzs_patch_loader::~kzs_patch_loader()
    {
        if (fp_) fclose(fp_);
    }

    bool kzs_patch_loader::load(bezier_patch_saver* ms) const
    {
        FILE* fp = fp_;

        if (fseek(fp, 0, SEEK_SET) != 0) return false;
        kzs_header header;
        if (!KZSReadHeader(fp, header)) return false;

        int nSurface = header.sn;
        if (!ms->set_patches(nSurface)) return false;
        io_bezier_patch patch;
        for (int i = 0; i < nSurface; i++)
        {
            if (!KZSReadPatch(fp, patch)) return false;
            if (!ms->set_patch(i, &patch)) return false;
        }

        return true;
    }
}
