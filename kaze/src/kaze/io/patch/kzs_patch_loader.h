#ifndef KAZE_KZS_PATCH_LOADER_H
#define KAZE_KZS_PATCH_LOADER_H

#include "bezier_patch_loader.h"

#include <cstdio>
#include <cstdlib>

namespace kaze
{

    class kzs_patch_loader : public bezier_patch_loader
    {
    public:
        kzs_patch_loader(const char* file);
        ~kzs_patch_loader();
        bool load(bezier_patch_saver* ms) const;

    private:
        FILE* fp_;
    };
}

#endif
