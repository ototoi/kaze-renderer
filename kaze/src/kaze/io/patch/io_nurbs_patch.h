#ifndef KAZE_IO_NURBS_PATCH_H
#define KAZE_IO_NURBS_PATCH_H

#include <vector>

namespace kaze
{

    struct io_nurbs_curve
    {
        std::vector<double> knots;
        std::vector<double> cps; //xyzw xyzw xyzw...
    };

    struct io_nurbs_loop
    {
        int nDirection; //Outer or Inner
        std::vector<io_nurbs_curve> curves;
    };

    struct io_nurbs_patch
    {
        int nu;
        int nv;
        std::vector<double> uknots;
        std::vector<double> vknots;
        std::vector<double> cps; //xyzw xyzw xyzw....
        std::vector<io_nurbs_loop> loops;
        unsigned int nID;
        unsigned int nMat;
    };
}

#endif
