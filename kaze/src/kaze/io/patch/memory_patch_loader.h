#ifndef KAZE_MEMORY_PATCH_LOADER_H
#define KAZE_MEMORY_PATCH_LOADER_H

#include "types.h"
#include "bezier_patch_loader.h"
#include "trimmed_multi_bezier_patch.hpp"

#include <vector>
#include <cstdlib>

namespace kaze
{

    class memory_patch_loader : public bezier_patch_loader
    {
    public:
        typedef trimmed_multi_bezier_patch<vector3, vector2> patch_type;

    public:
        memory_patch_loader();
        ~memory_patch_loader();
        bool get_patches(size_t& sz) const;
        bool get_patch(size_t i, io_bezier_patch* patch) const;

    public:
        void clear() { mv_.clear(); }
        void add(const patch_type& patch);
        size_t size() const { return mv_.size(); }
    private:
        std::vector<patch_type> mv_;
    };
}

#endif
