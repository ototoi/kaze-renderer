#include "kzs_patch_saver.h"
#include "bezier_patch_loader.h"
#include "bezier_patch_saver.h"
#include <stdio.h>
#include "kzs_format.h"

#include <string.h>
#pragma warning(disable : 4996)

namespace kaze
{

    static bool KZSInitHeader(kzs_header& header, size_t nSurface)
    {
        strcpy(header.magic, "KZS");
        header.sn = (int)nSurface;
        header.version = 1;
        return true;
    }

    static bool KZSWriteHeader(FILE* fp, const kzs_header& header)
    {
        if (1 != fwrite(&header, sizeof(kzs_header), 1, fp)) return false;
        return true;
    }

    static bool KZSWriteCurve(FILE* fp, const io_bezier_curve& curve)
    {
        int ns = (int)curve.knots.size() - 1;
        int nc = (int)curve.cps.size() / 3 / ns;
        if (1 != fwrite(&nc, sizeof(int), 1, fp)) return false;
        if (1 != fwrite(&ns, sizeof(int), 1, fp)) return false;

        size_t szKnots = curve.knots.size();
        size_t szCps = curve.cps.size();

        if (1 != fwrite(&(curve.knots[0]), szKnots * sizeof(double), 1, fp)) return false;
        if (1 != fwrite(&(curve.cps[0]), szCps * sizeof(double), 1, fp)) return false;

        return true;
    }

    static bool KZSWriteLoop(FILE* fp, const io_bezier_loop& loop)
    {
        int nDirection = loop.nDirection;
        int nCurves = (int)loop.curves.size();
        if (1 != fwrite(&nDirection, sizeof(int), 1, fp)) return false;
        if (1 != fwrite(&nCurves, sizeof(int), 1, fp)) return false;
        for (int i = 0; i < nCurves; i++)
        {
            if (!KZSWriteCurve(fp, loop.curves[i])) return false;
        }

        return true;
    }

    static bool KZSWritePatch(FILE* fp, const io_bezier_patch& patch)
    {
        int nu = patch.nu;
        int nv = patch.nv;
        int nPU = (int)patch.uknots.size() - 1;
        int nPV = (int)patch.vknots.size() - 1;
        int szUKnots = (nPU + 1) * sizeof(double);
        int szVKnots = (nPV + 1) * sizeof(double);
        int szCP = (3 * nu * nv * nPU * nPV) * sizeof(double);
        int nType = 0;

        if (1 != fwrite(&nType, sizeof(int), 1, fp)) return false;
        if (1 != fwrite(&nu, sizeof(int), 1, fp)) return false;
        if (1 != fwrite(&nv, sizeof(int), 1, fp)) return false;
        if (1 != fwrite(&nPU, sizeof(int), 1, fp)) return false;
        if (1 != fwrite(&nPV, sizeof(int), 1, fp)) return false;

        if (1 != fwrite(&(patch.uknots[0]), szUKnots, 1, fp)) return false;
        if (1 != fwrite(&(patch.vknots[0]), szVKnots, 1, fp)) return false;
        if (1 != fwrite(&(patch.cps[0]), szCP, 1, fp)) return false;

        int nLoops = (int)patch.loops.size();
        if (1 != fwrite(&(nLoops), sizeof(int), 1, fp)) return false;
        for (int i = 0; i < nLoops; i++)
        {
            if (!KZSWriteLoop(fp, patch.loops[i])) return false;
        }

        if (1 != fwrite(&patch.nID, sizeof(unsigned int), 1, fp)) return false;
        if (1 != fwrite(&patch.nMat, sizeof(unsigned int), 1, fp)) return false;

        return true;
    }
}

namespace kaze
{

    kzs_patch_saver::kzs_patch_saver(const char* file)
    {
        fp_ = fopen(file, "wb+");
    }

    kzs_patch_saver::~kzs_patch_saver()
    {
        if (fp_) fclose(fp_);
    }

    bool kzs_patch_saver::save(const bezier_patch_loader* ml)
    {
        FILE* fp = fp_;

        if (fseek(fp, 0, SEEK_SET) != 0) return false;

        size_t nSurface = 0;
        if (!ml->get_patches(nSurface)) return false;

        kzs_header header;
        if (!KZSInitHeader(header, nSurface)) return false;
        if (!KZSWriteHeader(fp, header)) return false;

        io_bezier_patch patch;
        for (int i = 0; i < nSurface; i++)
        {
            if (!ml->get_patch(i, &patch)) return false;
            if (!KZSWritePatch(fp, patch)) return false;
        }

        return true;
    }
}
