#include "convert_nurbs_patch.h"

namespace kaze
{

    typedef std::vector<nurbs_curve<vector2> > LoopType;

    static bool ConvertKnots(std::vector<real>& out, const std::vector<double>& in)
    {
        out.resize(in.size());
        for (size_t i = 0; i < in.size(); i++)
        {
            out[i] = (real)in[i];
        }
        return true;
    }

    static bool ConvertCurve(nurbs_curve<vector2>& out, const io_nurbs_curve& in)
    {
        const std::vector<double>& cps = in.cps;
        size_t csz = cps.size() / 4; //xyzw

        std::vector<vector2> cp(csz);
        std::vector<real> w(csz);
        for (size_t i = 0; i < csz; i++)
        {
            cp[i][0] = (real)in.cps[4 * i + 0];
            cp[i][1] = (real)in.cps[4 * i + 1];
            //
            w[i] = (real)in.cps[4 * i + 3];
        }
        std::vector<real> knots(in.knots.size());
        if (!ConvertKnots(knots, in.knots)) return false;

        nurbs_curve<vector2> tmp(cp, w, knots);
        out.swap(tmp);

        return true;
    }

    static bool ConvertLoop(std::vector<nurbs_curve<vector2> >& out, const io_nurbs_loop& in)
    {
        size_t sz = in.curves.size();
        for (size_t i = 0; i < sz; i++)
        {
            nurbs_curve<vector2> tmp;
            if (!ConvertCurve(tmp, in.curves[i])) return false;
            out.push_back(tmp);
        }
        return true;
    }

    static bool ConvertLoops(std::vector<LoopType>& outer_loops, std::vector<LoopType>& inner_loops, const io_nurbs_patch& in)
    {
        const std::vector<io_nurbs_loop>& loops = in.loops;
        int nLoops = (int)loops.size();
        for (int j = 0; j < nLoops; j++)
        {
            int nDirection = loops[j].nDirection;
            LoopType tmp;
            if (!ConvertLoop(tmp, loops[j])) return false;
            if (nDirection > 0)
            {
                outer_loops.push_back(tmp);
            }
            else
            {
                inner_loops.push_back(tmp);
            }
        }

        return true;
    }
    //------------------------------------------------------------------------------------------------------------------

    bool convert_nurbs_patch(io_nurbs_patch& out, const bspline_patch<vector3>& in)
    {
        int nu = in.get_nu();
        int nv = in.get_nv();
        size_t sz = 4 * nu * nv;
        std::vector<double> cps(sz);
        for (int j = 0; j < nv; j++)
        {
            for (int i = 0; i < nu; i++)
            {
                vector3 p = in.get_cp_at(i, j);
                double x = p[0];
                double y = p[1];
                double z = p[2];
                double w = 1;

                size_t k = nu * j + i;
                cps[4 * k + 0] = x;
                cps[4 * k + 1] = y;
                cps[4 * k + 2] = z;
                cps[4 * k + 3] = w;
            }
        }
        int usz = in.get_knot_size_u();
        std::vector<double> uknots(usz);
        for (int i = 0; i < usz; i++)
            uknots[i] = in.get_knot_at_u(i);

        int vsz = in.get_knot_size_v();
        std::vector<double> vknots(vsz);
        for (int i = 0; i < vsz; i++)
            vknots[i] = in.get_knot_at_v(i);

        out.nu = nu;
        out.nv = nv;
        out.cps.swap(cps);
        out.uknots.swap(uknots);
        out.vknots.swap(vknots);
        out.nID = 0;
        out.nMat = 0;
        out.loops.clear();

        return true;
    }

    bool convert_nurbs_patch(bspline_patch<vector3>& out, const io_nurbs_patch& in)
    {
        int nu = in.nu;
        int nv = in.nv;
        size_t sz = in.cps.size() / 4;
        std::vector<vector3> cps(sz);
        for (size_t i = 0; i < sz; i++)
        {
            double x = in.cps[4 * i + 0];
            double y = in.cps[4 * i + 1];
            double z = in.cps[4 * i + 2];
            double w = in.cps[4 * i + 3];
            cps[i] = vector3(x, y, z);
        }
        size_t usz = in.uknots.size();
        std::vector<real> uknots(usz);
        for (size_t i = 0; i < usz; i++)
            uknots[i] = in.uknots[i];

        size_t vsz = in.vknots.size();
        std::vector<real> vknots(vsz);
        for (size_t i = 0; i < vsz; i++)
            vknots[i] = in.vknots[i];

        bspline_patch<vector3> tmp(nu, nv, cps, uknots, vknots);
        out.swap(tmp);

        return true;
    }

    bool convert_nurbs_patch(io_nurbs_patch& out, const nurbs_patch<vector3>& in)
    {
        int nu = in.get_nu();
        int nv = in.get_nv();
        size_t sz = 4 * nu * nv;
        std::vector<double> cps(sz);
        for (int j = 0; j < nv; j++)
        {
            for (int i = 0; i < nu; i++)
            {
                vector3 p = in.get_cp_at(i, j);
                double x = p[0];
                double y = p[1];
                double z = p[2];
                double w = in.get_w_at(i, j);

                size_t k = nu * j + i;
                cps[4 * k + 0] = x;
                cps[4 * k + 1] = y;
                cps[4 * k + 2] = z;
                cps[4 * k + 3] = w;
            }
        }
        int usz = in.get_knot_size_u();
        std::vector<double> uknots(usz);
        for (int i = 0; i < usz; i++)
            uknots[i] = in.get_knot_at_u(i);

        int vsz = in.get_knot_size_v();
        std::vector<double> vknots(vsz);
        for (int i = 0; i < vsz; i++)
            vknots[i] = in.get_knot_at_v(i);

        out.nu = nu;
        out.nv = nv;
        out.cps.swap(cps);
        out.uknots.swap(uknots);
        out.vknots.swap(vknots);
        out.nID = 0;
        out.nMat = 0;
        out.loops.clear();

        return true;
    }

    bool convert_nurbs_patch(nurbs_patch<vector3>& out, const io_nurbs_patch& in)
    {
        int nu = in.nu;
        int nv = in.nv;
        size_t sz = in.cps.size() / 4;
        std::vector<vector3> cps(sz);
        std::vector<real> ws(sz);
        for (size_t i = 0; i < sz; i++)
        {
            double x = in.cps[4 * i + 0];
            double y = in.cps[4 * i + 1];
            double z = in.cps[4 * i + 2];
            double w = in.cps[4 * i + 3];
            cps[i] = vector3(x, y, z);
            ws[i] = w;
        }
        size_t usz = in.uknots.size();
        std::vector<real> uknots(usz);
        for (size_t i = 0; i < usz; i++)
            uknots[i] = in.uknots[i];

        size_t vsz = in.vknots.size();
        std::vector<real> vknots(vsz);
        for (size_t i = 0; i < vsz; i++)
            vknots[i] = in.vknots[i];

        nurbs_patch<vector3> tmp(nu, nv, cps, ws, uknots, vknots);
        out.swap(tmp);

        return true;
    }

    bool convert_nurbs_patch(trimmed_nurbs_patch<vector3, vector2>& out, const io_nurbs_patch& in)
    {
        nurbs_patch<vector3> patch;
        if (!convert_nurbs_patch(patch, in)) return false;

        std::vector<LoopType> outer_loops;
        std::vector<LoopType> inner_loops;
        if (!ConvertLoops(outer_loops, inner_loops, in)) return false;

        trimmed_nurbs_patch<vector3, vector2> tmp(patch, outer_loops, inner_loops);
        out.swap(tmp);

        return true;
    }

    bool convert_nurbs_patch(io_nurbs_patch& out, const trimmed_nurbs_patch<vector3, vector2>& in)
    {
        /*TODO*/
        return false;
    }
}
