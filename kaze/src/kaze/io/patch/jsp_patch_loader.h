#ifndef KAZE_JSP_PATCH_LOADER_H
#define KAZE_JSP_PATCH_LOADER_H

#include <string>
#include <vector>
#include "nurbs_patch_loader.h"

namespace kaze
{

    class jsp_patch_loader : public nurbs_patch_loader
    {
    public:
        struct NURBSCurve
        {
            int degree;
            int cvs;
            std::vector<double> cv;
            std::vector<double> domain;
            std::vector<double> knots;
            std::string form;
        };
        struct NURBSLoop
        {
            std::string loop_type;
            std::vector<NURBSCurve> curves;
        };
        struct NURBSPatch
        {
            int degree_u;
            int degree_v;
            int cvs_u;
            int cvs_v;
            std::vector<double> cv;
            std::vector<double> domain_u;
            std::vector<double> domain_v;
            std::vector<double> knots_u;
            std::vector<double> knots_v;
            std::string form_u;
            std::string form_v;
            std::vector<NURBSLoop> loops;
        };

    public:
        jsp_patch_loader(const char* szFilePath);
        ~jsp_patch_loader();

    public:
        bool load(nurbs_patch_saver* ms) const;
        bool get_patches(size_t& sz) const;
        bool get_patch(size_t i, io_nurbs_patch* patch) const; //NURBS
    private:
        std::vector<NURBSPatch> mv_;
    };
}

#endif