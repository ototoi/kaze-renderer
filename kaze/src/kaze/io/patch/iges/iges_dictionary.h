#ifndef KAZE_IGES_DICTIONARY_H
#define KAZE_IGES_DICTIONARY_H

#include <string>
#include "iges_entity.h"

namespace kaze
{

    class iges_dictionary
    {
    public:
        iges_dictionary(const char data[]);
        ~iges_dictionary();

    public:
        virtual std::string to_str() const;

    public:
        char* get_data() { return data_; }
        const char* get_data() const { return data_; }
    public:
        iges_entity* create_entity(char delim, const char params[]);
        iges_entity* get_entity() const { return ent_; }
    public:
        int GetEntityTypeNumber() const;
        int GetParameterData() const;
        int GetStructure() const;
        int GetLineFontPattern() const;
        int GetLevel() const;
        int GetView() const;
        int GetXformationMatrix() const;
        int GetLabelDisplay() const;
        int GetStatusNumber() const;
        int GetSequenceNumber() const;
        int GetLineWeightNumber() const;
        int GetColorNumber() const;
        int GetParameterLineCount() const;
        int GetFormNumber() const;
        int GetReserve1() const;
        int GetReserve2() const;
        int GetEntityLabel() const;
        int GetEntitySubscriptNumber() const;

    protected:
        char data_[160 + 1];
        iges_entity* ent_;
    };
}

#endif