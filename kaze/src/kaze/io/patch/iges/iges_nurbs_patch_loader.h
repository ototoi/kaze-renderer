#ifndef KAZE_IGES_NURBS_PATCH_LOADER_H
#define KAZE_IGES_NURBS_PATCH_LOADER_H

#include "patch/nurbs_patch_loader.h"

namespace kaze
{

    class iges_nurbs_patch_loader_imp;
    class iges_nurbs_patch_loader : public nurbs_patch_loader
    {
    public:
        iges_nurbs_patch_loader(const char* szFileName);
        ~iges_nurbs_patch_loader();

    public:
        virtual bool get_patches(size_t& sz) const;
        virtual bool get_patch(size_t i, io_nurbs_patch* patch) const;

    protected:
        iges_nurbs_patch_loader_imp* imp_;
    };
}

#endif