#include "iges_dictionary.h"
#include "iges_entity.h"
#include <string>
#include <sstream>
#include <assert.h>
#include <string.h>
#include <memory>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

namespace kaze
{

    static int GetInteger(const char szToken[])
    {
        int nRet = 0;
        std::stringstream(std::string(szToken, 8)) >> std::skipws >> nRet;
        return nRet;
    }

    static int lnumber_to_epointer(int line_number)
    {
        if (line_number > 0)
        {
            line_number++;
            return (line_number >> 1);
        }
        else if (line_number < 0)
        {
            line_number--;
            return line_number / 2;
        }
        else
            return line_number; //0
    }

    iges_dictionary::iges_dictionary(const char data[])
        : ent_(NULL)
    {
        strcpy(data_, data);
    }

    iges_dictionary::~iges_dictionary()
    {
        if (ent_) delete ent_;
    }

    iges_entity* iges_dictionary::create_entity(char delim, const char params[])
    {
        std::stringstream ss(params);
        int typeNumber = 100;
        ss >> typeNumber;

        int mTypeNumber = GetEntityTypeNumber();
        assert(typeNumber == mTypeNumber);

        //std::string dummy;
        //std::getline(pdstream,dummy,delim);
        //assert(typeNumber==m_EntityTypeNumber);
        std::unique_ptr<iges_entity> ap;
        switch (typeNumber)
        {
        case 100:
            ap.reset(new iges_entity_100(delim, params));
            break;
        case 102:
            ap.reset(new iges_entity_102(delim, params));
            break;
        case 108:
            ap.reset(new iges_entity_108(delim, params));
            break;
        case 110:
            ap.reset(new iges_entity_110(delim, params));
            break;
        case 120:
            ap.reset(new iges_entity_120(delim, params));
            break;
        case 124:
            ap.reset(new iges_entity_124(delim, params));
            break;
        case 126:
            ap.reset(new iges_entity_126(delim, params));
            break;
        case 128:
            ap.reset(new iges_entity_128(delim, params));
            break;
        case 142:
            ap.reset(new iges_entity_142(delim, params));
            break;
        case 144:
            ap.reset(new iges_entity_144(delim, params));
            break;
        }

        ent_ = ap.release();

        return ent_;
    }

    std::string iges_dictionary::to_str() const
    {
        return "";
    }

    int iges_dictionary::GetEntityTypeNumber() const
    {
        return GetInteger(data_ + 8 * 0);
    }
    int iges_dictionary::GetParameterData() const
    {
        return GetInteger(data_ + 8 * 1);
    }
    int iges_dictionary::GetStructure() const
    {
        return GetInteger(data_ + 8 * 2);
    }
    int iges_dictionary::GetLineFontPattern() const
    {
        return GetInteger(data_ + 8 * 3);
    }
    int iges_dictionary::GetLevel() const
    {
        return GetInteger(data_ + 8 * 4);
    }
    int iges_dictionary::GetView() const
    {
        return lnumber_to_epointer(GetInteger(data_ + 8 * 5)); //
    }
    int iges_dictionary::GetXformationMatrix() const
    {
        return lnumber_to_epointer(GetInteger(data_ + 8 * 6)); //
    }
    int iges_dictionary::GetLabelDisplay() const
    {
        return lnumber_to_epointer(GetInteger(data_ + 8 * 7)); //
    }
    int iges_dictionary::GetStatusNumber() const
    {
        return GetInteger(data_ + 8 * 8);
    }
    int iges_dictionary::GetSequenceNumber() const
    {
        return GetInteger(data_ + 8 * 9);
    }
    int iges_dictionary::GetLineWeightNumber() const
    {
        return GetInteger(data_ + 8 * 10);
    }
    int iges_dictionary::GetColorNumber() const
    {
        int nRet = GetInteger(data_ + 8 * 11);
        if (nRet < 0)
        {
            nRet = -lnumber_to_epointer(-nRet);
        }
        return nRet;
    }
    int iges_dictionary::GetParameterLineCount() const
    {
        return GetInteger(data_ + 8 * 12);
    }
    int iges_dictionary::GetFormNumber() const
    {
        return GetInteger(data_ + 8 * 13);
    }
    int iges_dictionary::GetReserve1() const
    {
        return GetInteger(data_ + 8 * 14);
    }
    int iges_dictionary::GetReserve2() const
    {
        return GetInteger(data_ + 8 * 15);
    }
    int iges_dictionary::GetEntityLabel() const
    {
        return GetInteger(data_ + 8 * 16);
    }
    int iges_dictionary::GetEntitySubscriptNumber() const
    {
        return GetInteger(data_ + 8 * 17);
    }
}
