#include "iges_entity.h"
#include "iges_dictionary.h"
#include <string>
#include <sstream>
#include <vector>
#include <assert.h>

namespace kaze
{

    static int GetInteger(char delim, std::istream& is)
    {
        int nRet = 0;
        is >> nRet;
        std::string dummy;
        std::getline(is, dummy, delim);
        return nRet;
    }

    static double GetReal(char delim, std::istream& is)
    {
        double nRet = 0;
        is >> nRet;
        std::string dummy;
        std::getline(is, dummy, delim);
        return nRet;
    }
    //---------------------------------------------------------------------

    iges_entity_100::iges_entity_100(char delim, const char params[])
    {
        std::stringstream ss(params);
        int nTypeNumber = GetInteger(delim, ss);
        assert(nTypeNumber == 100);

        Zt_ = GetReal(delim, ss);
        X1_ = GetReal(delim, ss);
        Y1_ = GetReal(delim, ss);
        X2_ = GetReal(delim, ss);
        Y2_ = GetReal(delim, ss);
        X3_ = GetReal(delim, ss);
        Y3_ = GetReal(delim, ss);
    }

    //---------------------------------------------------------------------

    iges_entity_102::iges_entity_102(char delim, const char params[])
    {
        std::stringstream ss(params);
        int nTypeNumber = GetInteger(delim, ss);
        assert(nTypeNumber == 102);

        nCurves_ = GetInteger(delim, ss);
        for (int i = 0; i < nCurves_; i++)
        {
            CurvesNumbers_.push_back(GetInteger(delim, ss));
        }
    }

    //---------------------------------------------------------------------

    iges_entity_108::iges_entity_108(char delim, const char params[])
    {
        std::stringstream ss(params);
        int nTypeNumber = GetInteger(delim, ss);
        assert(nTypeNumber == 108);

        A_ = GetReal(delim, ss);
        B_ = GetReal(delim, ss);
        C_ = GetReal(delim, ss);
        D_ = GetReal(delim, ss);
        PTR_ = GetInteger(delim, ss);
        X_ = GetReal(delim, ss);
        Y_ = GetReal(delim, ss);
        Z_ = GetReal(delim, ss);
        SIZE_ = GetReal(delim, ss);
    }
    //---------------------------------------------------------------------

    iges_entity_110::iges_entity_110(char delim, const char params[])
    {
        std::stringstream ss(params);
        int nTypeNumber = GetInteger(delim, ss);
        assert(nTypeNumber == 110);

        startX_ = GetReal(delim, ss);
        startY_ = GetReal(delim, ss);
        startZ_ = GetReal(delim, ss);
        terminateX_ = GetReal(delim, ss);
        terminateY_ = GetReal(delim, ss);
        terminateZ_ = GetReal(delim, ss);
    }

    //---------------------------------------------------------------------

    iges_entity_120::iges_entity_120(char delim, const char params[])
        : pAxis_(0), pGeneratrix_(0)
    {
        std::stringstream ss(params);
        int nTypeNumber = GetInteger(delim, ss);
        assert(nTypeNumber == 120);

        nAxis_ = GetInteger(delim, ss);
        nGeneratrix_ = GetInteger(delim, ss);

        startAngle_ = GetReal(delim, ss);
        terminateAngle_ = GetReal(delim, ss);
    }

    iges_entity_120::~iges_entity_120()
    {
        if (pAxis_) delete pAxis_;
        if (pGeneratrix_) delete pGeneratrix_;
    }

    //---------------------------------------------------------------------

    iges_entity_124::iges_entity_124(char delim, const char params[])
    {
        std::stringstream ss(params);
        int nTypeNumber = GetInteger(delim, ss);
        assert(nTypeNumber == 124);

        for (int i = 0; i < 12; i++)
        {
            mat_[i] = GetReal(delim, ss);
        }
    }

    //---------------------------------------------------------------------

    iges_entity_126::iges_entity_126(char delim, const char params[])
    {
        std::stringstream ss(params);
        int nTypeNumber = GetInteger(delim, ss);
        assert(nTypeNumber == 126);

        int upper_index = GetInteger(delim, ss);
        int degree = GetInteger(delim, ss);
        int planer = GetInteger(delim, ss);
        int closed = GetInteger(delim, ss);

        int non_rational = GetInteger(delim, ss);
        int periodic = GetInteger(delim, ss);

        int nCtrl = upper_index + 1;
        int order = degree + 1;

        //Read knot vector.
        int nKnot = order + nCtrl;

        std::vector<double> knot(nKnot);
        for (int i = 0; i < nKnot; i++)
        {
            knot[i] = GetReal(delim, ss);
        }

        //Read weights.
        std::vector<double> weights(nCtrl);
        for (int i = 0; i < nCtrl; i++)
        {
            weights[i] = GetReal(delim, ss);
        }

        //Read control points.
        std::vector<double> ctrl_points(nCtrl * 3);
        for (int i = 0; i < nCtrl; i++)
        {
            ctrl_points[3 * i + 0] = GetReal(delim, ss);
            ctrl_points[3 * i + 1] = GetReal(delim, ss);
            ctrl_points[3 * i + 2] = GetReal(delim, ss);
        }

        double start_param = GetReal(delim, ss);
        double end_param = GetReal(delim, ss);

        //Read normal.
        std::vector<double> normal(3);
        normal[0] = GetReal(delim, ss);
        normal[1] = GetReal(delim, ss);
        normal[2] = GetReal(delim, ss);

        nCtrl_ = nCtrl;
        nDegree_ = degree;
        nPlaner_ = planer;
        nClosed_ = closed;
        nPeriodic_ = periodic;
        nNonRational_ = non_rational;
        knot_.swap(knot);
        weights_.swap(weights);
        ctrl_points_.swap(ctrl_points);
        start_ = start_param;
        end_ = end_param;
        normal_.swap(normal);
    }

    //---------------------------------------------------------------------

    iges_entity_128::iges_entity_128(char delim, const char params[])
    {
        std::stringstream ss(params);
        int nTypeNumber = GetInteger(delim, ss);
        assert(nTypeNumber == 128);

        int upper_indexU = GetInteger(delim, ss);
        int upper_indexV = GetInteger(delim, ss);
        int degreeU = GetInteger(delim, ss);
        int degreeV = GetInteger(delim, ss);
        int closedU = GetInteger(delim, ss);
        int closedV = GetInteger(delim, ss);
        int non_rational = GetInteger(delim, ss);
        int periodicU = GetInteger(delim, ss);
        int periodicV = GetInteger(delim, ss);

        int nCtrlU = upper_indexU + 1;
        int nCtrlV = upper_indexV + 1;

        int orderU = degreeU + 1;
        int orderV = degreeV + 1;

        //Read knot vector.
        int nKnotU = orderU + nCtrlU;
        int nKnotV = orderV + nCtrlV;
        std::vector<double> knotU(nKnotU);
        std::vector<double> knotV(nKnotV);
        for (int i = 0; i < nKnotU; i++)
        {
            knotU[i] = GetReal(delim, ss);
        }
        for (int i = 0; i < nKnotV; i++)
        {
            knotV[i] = GetReal(delim, ss);
        }

        //Read weights.
        std::vector<double> weights(nCtrlU * nCtrlV);
        for (int j = 0; j < nCtrlV; j++)
        {
            for (int i = 0; i < nCtrlU; i++)
            {
                weights[j * nCtrlU + i] = GetReal(delim, ss);
            }
        }

        std::vector<double> ctrl_points(nCtrlU * nCtrlV * 3);
        for (int j = 0; j < nCtrlV; j++)
        {
            for (int i = 0; i < nCtrlU; i++)
            {
                size_t index = j * nCtrlU + i;
                ctrl_points[3 * index + 0] = GetReal(delim, ss);
                ctrl_points[3 * index + 1] = GetReal(delim, ss);
                ctrl_points[3 * index + 2] = GetReal(delim, ss);
            }
        }

        double start_paramU = GetReal(delim, ss);
        double end_paramU = GetReal(delim, ss);
        double start_paramV = GetReal(delim, ss);
        double end_paramV = GetReal(delim, ss);

        nCtrlU_ = nCtrlU;
        nCtrlV_ = nCtrlV;
        nDegreeU_ = degreeU;
        nDegreeV_ = degreeV;
        nClosedU_ = closedU;
        nClosedV_ = closedV;
        nPeriodicU_ = periodicU;
        nPeriodicV_ = periodicV;
        nNonRational_ = non_rational;
        knotU_.swap(knotU);
        knotV_.swap(knotV);
        weights_.swap(weights);
        ctrl_points_.swap(ctrl_points);
        startU_ = start_paramU;
        endU_ = end_paramU;
        startV_ = start_paramV;
        endV_ = end_paramV;
    }

    //---------------------------------------------------------------------

    iges_entity_142::iges_entity_142(char delim, const char params[])
    {
        std::stringstream ss(params);
        int nTypeNumber = GetInteger(delim, ss);
        assert(nTypeNumber == 142);

        nCreatedWay_ = GetInteger(delim, ss);
        nSurface_ = GetInteger(delim, ss);
        nParamCurve_ = GetInteger(delim, ss);
        nModelCurve_ = GetInteger(delim, ss);
        nPrefered_ = GetInteger(delim, ss);
    }

    //---------------------------------------------------------------------
    iges_entity_144::iges_entity_144(char delim, const char params[])
        : pSurface_(NULL)
    {
        std::stringstream ss(params);
        int nTypeNumber = GetInteger(delim, ss);
        assert(nTypeNumber == 144);

        nSurface_ = GetInteger(delim, ss);
        nOuterBoundaryType_ = GetInteger(delim, ss);
        nNumInner_ = GetInteger(delim, ss);
        nOuterBoundary_ = GetInteger(delim, ss);
        for (int i = 0; i < nNumInner_; i++)
        {
            int nBoundary = GetInteger(delim, ss);
            nInnerBoundaries.push_back(nBoundary);
        }
    }

    iges_entity_144::~iges_entity_144()
    {
        if (pSurface_) delete pSurface_;
        for (size_t i = 0; i < Outers_.size(); i++)
        {
            if (Outers_[i]) delete Outers_[i];
        }
        for (size_t i = 0; i < Inners_.size(); i++)
        {
            if (Inners_[i]) delete Inners_[i];
        }
    }
}