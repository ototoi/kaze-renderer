#ifndef KAZE_IGES_LOADER_H
#define KAZE_IGES_LOADER_H

#include <cstdio>
#include <vector>
#include <string>
#include "iges_dictionary.h"

namespace kaze
{

    class iges_loader
    {
    public:
        iges_loader();
        ~iges_loader();

    public:
        bool load(const char* szFileName);

    public:
        const std::vector<iges_dictionary*>& get_dictionaries() const;
        std::vector<iges_dictionary*>& get_dictionaries();

    protected:
        bool load_Internal(const char* szFileName);
        bool load_Internal(FILE* fp);
        bool load_S(FILE* fp, char buffer[]);
        bool load_G(FILE* fp, char buffer[]);
        bool load_D(FILE* fp, char buffer[]);
        bool load_P(FILE* fp, char buffer[]);
        bool construct();
        bool construct_102(size_t i);
        bool construct_108(size_t i);
        bool construct_120(size_t i);
        bool construct_142(size_t i);
        bool construct_144(size_t i);

    protected:
        std::vector<std::string> start_sections_;
        std::vector<std::string> global_sections_;
        std::vector<iges_dictionary*> dictionaries_;
    };
}

#endif