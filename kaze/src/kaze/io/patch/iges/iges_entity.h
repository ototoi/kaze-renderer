#ifndef KAZE_IGES_ENTITY_H_
#define KAZE_IGES_ENTITY_H_

#include <vector>

namespace kaze
{
    /*
	100 //
	102
	106 //
	108 //
	110 //
	120 //
	124
	126
	142
	144
	*/

    class iges_dictionary;

    class iges_entity
    {
    public:
        virtual ~iges_entity() {}
        virtual int get_type_number() const { return 0; }
    };

    //100 Circular Arc Entity
    class iges_entity_100 : public iges_entity
    {
    public:
        iges_entity_100(char delim, const char params[]);
        virtual int get_type_number() const { return 100; }
    public:
        double get_Zt() const { return Zt_; }
        double get_arc_center_x() const { return X1_; }
        double get_arc_center_y() const { return Y1_; }
        double get_start_x() const { return X2_; }
        double get_start_y() const { return Y2_; }
        double get_terminate_x() const { return X3_; }
        double get_terminate_y() const { return Y3_; }
    protected:
        double Zt_;
        double X1_;
        double Y1_;
        double X2_;
        double Y2_;
        double X3_;
        double Y3_;
    };

    //102 composite curve
    class iges_entity_102 : public iges_entity
    {
    public:
        iges_entity_102(char delim, const char params[]);
        virtual int get_type_number() const { return 102; }
    public:
        int GetCurves() const { return nCurves_; }
        const std::vector<int>& GetCurveNumbers() const { return CurvesNumbers_; }
    public:
        void add_curve(iges_dictionary* crv) { curves_.push_back(crv); }
        const std::vector<iges_dictionary*>& get_curves() const { return curves_; }
    protected:
        int nCurves_;
        std::vector<int> CurvesNumbers_;

    protected:
        std::vector<iges_dictionary*> curves_;
    };

    //108 Plane Entity
    class iges_entity_108 : public iges_entity
    {
    public:
        iges_entity_108(char delim, const char params[]);
        virtual int get_type_number() const { return 108; }
    public:
        int GetPTR() const { return PTR_; }
    protected:
        double A_;
        double B_;
        double C_;
        double D_;
        int PTR_;
        double X_;
        double Y_;
        double Z_;
        double SIZE_;
    };

    //110 Line Entity
    class iges_entity_110 : public iges_entity
    {
    public:
        iges_entity_110(char delim, const char params[]);
        virtual int get_type_number() const { return 110; }
    public:
        double get_start_x() const { return startX_; }
        double get_start_y() const { return startY_; }
        double get_start_z() const { return startZ_; }
        double get_terminate_x() const { return terminateX_; }
        double get_terminate_y() const { return terminateY_; }
        double get_terminate_z() const { return terminateZ_; }
    protected:
        double startX_;
        double startY_;
        double startZ_;
        double terminateX_;
        double terminateY_;
        double terminateZ_;
    };

    //120 Surface of Revolution Entity
    class iges_entity_120 : public iges_entity
    {
    public:
        iges_entity_120(char delim, const char params[]);
        ~iges_entity_120();
        virtual int get_type_number() const { return 120; }
    public:
        int GetAxis() const { return nAxis_; }
        int GetGeneratrix() const { return nGeneratrix_; }
        double get_start_angle() const { return startAngle_; }
        double get_terminate_angle() const { return terminateAngle_; }
    public:
        void set_axis(iges_dictionary* ax) { pAxis_ = ax; }
        const iges_dictionary* get_axis() const { return pAxis_; }
        void set_generatrix(iges_dictionary* g) { pGeneratrix_ = g; }
        const iges_dictionary* get_generatrix() const { return pGeneratrix_; }
    protected:
        int nAxis_;
        int nGeneratrix_;
        double startAngle_;
        double terminateAngle_;

    protected:
        iges_dictionary* pAxis_;
        iges_dictionary* pGeneratrix_;
    };

    //124 Transformation Matrix Entity
    class iges_entity_124 : public iges_entity
    {
    public:
        iges_entity_124(char delim, const char params[]);
        virtual int get_type_number() const { return 124; }

    protected:
        double mat_[12];
    };

    //126 nurbs curve
    class iges_entity_126 : public iges_entity
    {
    public:
        iges_entity_126(char delim, const char params[]);
        virtual int get_type_number() const { return 126; }
    public:
        int get_n() const { return nCtrl_; }
        int get_degree() const { return nDegree_; }
        int get_planer() const { return nPlaner_; }
        int get_closed() const { return nClosed_; }
        int get_periodic() const { return nClosed_; }
        int get_non_rational() const { return nNonRational_; }
        const std::vector<double>& get_knot() const { return knot_; }
        const std::vector<double>& get_weights() const { return weights_; }
        const std::vector<double>& get_ctrl_points() const { return ctrl_points_; }
        double get_start() const { return start_; }
        double get_end() const { return end_; }
        const std::vector<double>& get_normal() const { return normal_; }
    protected:
        int nCtrl_;
        int nDegree_;
        int nPlaner_;
        int nClosed_;
        int nPeriodic_;
        int nNonRational_;
        std::vector<double> knot_;
        std::vector<double> weights_;
        std::vector<double> ctrl_points_;
        double start_;
        double end_;
        std::vector<double> normal_;
    };

    //128 nurbs surface
    class iges_entity_128 : public iges_entity
    {
    public:
        iges_entity_128(char delim, const char params[]);
        virtual int get_type_number() const { return 128; }
    public:
        int get_nu() const { return nCtrlU_; }
        int get_nv() const { return nCtrlV_; }
        int get_degree_u() const { return nDegreeU_; }
        int get_degree_v() const { return nDegreeV_; }
        int get_closed_u() const { return nClosedU_; }
        int get_closed_v() const { return nClosedV_; }
        int get_periodic_u() const { return nClosedU_; }
        int get_periodic_v() const { return nClosedV_; }
        int get_non_rational() const { return nNonRational_; }
        const std::vector<double>& get_knot_u() const { return knotU_; }
        const std::vector<double>& get_knot_v() const { return knotV_; }
        const std::vector<double>& get_weights() const { return weights_; }
        const std::vector<double>& get_ctrl_points() const { return ctrl_points_; }
        double get_start_u() const { return startU_; }
        double get_start_v() const { return startV_; }
        double get_end_u() const { return endU_; }
        double get_end_v() const { return endV_; }
    protected:
        int nCtrlU_;
        int nCtrlV_;
        int nDegreeU_;
        int nDegreeV_;
        int nClosedU_;
        int nClosedV_;
        int nPeriodicU_;
        int nPeriodicV_;
        int nNonRational_;
        std::vector<double> knotU_;
        std::vector<double> knotV_;
        std::vector<double> weights_;
        std::vector<double> ctrl_points_;
        double startU_;
        double endU_;
        double startV_;
        double endV_;
    };

    //142 Curve on a Parametric Surface
    class iges_entity_142 : public iges_entity
    {
    public:
        iges_entity_142(char delim, const char params[]);
        virtual int get_type_number() const { return 142; }
    public:
        int GetCreatedWay() const { return nCreatedWay_; }
        int GetSurface() const { return nSurface_; }
        int GetParamCurve() const { return nParamCurve_; }
        int GetModelCurve() const { return nModelCurve_; }
        int GetPrefered() const { return nPrefered_; }
    public:
        void add_curve(iges_dictionary* crv) { curves_.push_back(crv); }
        const std::vector<iges_dictionary*>& get_curves() const { return curves_; }
    protected:
        int nCreatedWay_;
        int nSurface_;
        int nParamCurve_;
        int nModelCurve_;
        int nPrefered_;

    protected:
        std::vector<iges_dictionary*> curves_;
    }; //

    //144 Trimmed (Parametric) Surface Entity
    class iges_entity_144 : public iges_entity
    {
    public:
        iges_entity_144(char delim, const char params[]);
        ~iges_entity_144();
        virtual int get_type_number() const { return 144; }
    public:
        int GetSurfaceNumber() const { return nSurface_; }
        int GetOuterBoundaryType() const { return nOuterBoundaryType_; }
        int GetNumInner() const { return nNumInner_; }
        int GetOuterBoundary() const { return nOuterBoundary_; }
        const std::vector<int>& GetInnerBoundaries() const { return nInnerBoundaries; }
    public:
        void SetSurface(iges_dictionary* pSurface) { pSurface_ = pSurface; }
        void AddOuter(iges_dictionary* outer) { Outers_.push_back(outer); }
        void AddInner(iges_dictionary* inner) { Inners_.push_back(inner); }
        iges_dictionary* GetSurface() const { return pSurface_; }
        const std::vector<iges_dictionary*>& get_outer_loops() const { return Outers_; }
        const std::vector<iges_dictionary*>& get_inner_loops() const { return Inners_; }
    protected:
        int nSurface_;
        int nOuterBoundaryType_;
        int nNumInner_;
        int nOuterBoundary_;
        std::vector<int> nInnerBoundaries;

    protected:
        iges_dictionary* pSurface_;
        std::vector<iges_dictionary*> Outers_;
        std::vector<iges_dictionary*> Inners_;
    };
}

#endif