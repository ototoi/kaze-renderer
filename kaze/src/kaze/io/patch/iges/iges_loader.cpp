#include "iges_loader.h"
#include "iges_dictionary.h"
#include <sstream>
#include <stdio.h>
#include <string.h>
#include <assert.h>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

namespace kaze
{

    static int lnumber_to_epointer(int line_number)
    {
        if (line_number > 0)
        {
            line_number++;
            return (line_number >> 1);
        }
        else if (line_number < 0)
        {
            line_number--;
            return line_number / 2;
        }
        else
            return line_number; //0
    }

    iges_loader::iges_loader()
    {
        ;
    }

    iges_loader::~iges_loader()
    {
        size_t sz = dictionaries_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (dictionaries_[i]) delete dictionaries_[i];
        }
    }

    bool iges_loader::load(const char* szFileName)
    {
        if (!load_Internal(szFileName)) return false;
        if (!construct()) return false;
        return true;
    }

    bool iges_loader::construct_102(size_t i)
    {
        iges_dictionary* dict = dictionaries_[i];
        iges_entity* ent = dict->get_entity();
        if (!ent) return false;
        iges_entity_102* ent102 = dynamic_cast<iges_entity_102*>(ent);
        if (!ent102) return false;

        const std::vector<int>& crvs = ent102->GetCurveNumbers();
        for (size_t i = 0; i < crvs.size(); i++)
        {
            int nCrv = crvs[i];
            iges_dictionary* crvBDE = dictionaries_[lnumber_to_epointer(nCrv) - 1];
            ent102->add_curve(crvBDE);
            dictionaries_[lnumber_to_epointer(nCrv) - 1] = 0;
        }
        return true;
    }

    bool iges_loader::construct_108(size_t i)
    {
        iges_dictionary* dict = dictionaries_[i];
        iges_entity* ent = dict->get_entity();
        if (!ent) return false;
        iges_entity_108* ent108 = dynamic_cast<iges_entity_108*>(ent);
        if (!ent108) return false;

        int nPtr = ent108->GetPTR();

        int eptr = lnumber_to_epointer(nPtr) - 1;
        if (eptr < 0) return true;
        iges_dictionary* pBound = dictionaries_[eptr];

        return true;
    }

    bool iges_loader::construct_120(size_t i)
    {
        iges_dictionary* dict = dictionaries_[i];
        iges_entity* ent = dict->get_entity();
        if (!ent) return false;
        iges_entity_120* ent120 = dynamic_cast<iges_entity_120*>(ent);
        if (!ent120) return false;

        int nAxis = ent120->GetAxis();
        int nGeneratrix = ent120->GetGeneratrix();

        int eptr = lnumber_to_epointer(nAxis) - 1;
        if (eptr < 0) return true;

        iges_dictionary* pAxis = dictionaries_[eptr];

        iges_dictionary* pGeneratrix = dictionaries_[lnumber_to_epointer(nGeneratrix) - 1];

        ent120->set_axis(pAxis);
        ent120->set_generatrix(pGeneratrix);

        dictionaries_[lnumber_to_epointer(nAxis) - 1] = 0;
        dictionaries_[lnumber_to_epointer(nGeneratrix) - 1] = 0;

        return true;
    }

    bool iges_loader::construct_142(size_t i)
    {
        iges_dictionary* dict = dictionaries_[i];
        iges_entity* ent = dict->get_entity();
        if (!ent)
        {
            return false;
        }
        iges_entity_142* ent142 = dynamic_cast<iges_entity_142*>(ent);
        if (!ent142)
        {
            return false;
        }

        int nCreatedWay = ent142->GetCreatedWay();
        int nParamCurve = ent142->GetParamCurve();
        int nModelCurve = ent142->GetModelCurve();

        int eptr = lnumber_to_epointer(nParamCurve) - 1;
        if (eptr < 0)
        {
            return true;
            eptr = lnumber_to_epointer(nModelCurve) - 1;
        }

        if (eptr < 0)
        {
            return true;
        }

        iges_dictionary* dCurve = dictionaries_[eptr];
        if (!dCurve) return true;

        ent142->add_curve(dCurve);
        dictionaries_[eptr] = 0;
        return true;
    }

    bool iges_loader::construct_144(size_t i)
    {
        iges_dictionary* dict = dictionaries_[i];
        iges_entity* ent = dict->get_entity();
        if (!ent) return false;
        iges_entity_144* ent144 = dynamic_cast<iges_entity_144*>(ent);
        if (!ent144) return false;

        int nSurface = ent144->GetSurfaceNumber();

        iges_dictionary* dSurf = dictionaries_[lnumber_to_epointer(nSurface) - 1];
        if (!dSurf)
        {
            //assert
            //TODO
            return true;
        }

        if (ent144->GetOuterBoundaryType())
        {
            int nOuterBoundary = ent144->GetOuterBoundary();
            iges_dictionary* outBDE = dictionaries_[lnumber_to_epointer(nOuterBoundary) - 1];
            assert(outBDE->GetEntityTypeNumber() == 142);
            ent144->AddOuter(outBDE);
            dictionaries_[lnumber_to_epointer(nOuterBoundary) - 1] = 0;
        }

        const std::vector<int>& inners = ent144->GetInnerBoundaries();
        for (size_t i = 0; i < inners.size(); i++)
        {
            int nInnerBoundary = inners[i];
            iges_dictionary* innerBDE = dictionaries_[lnumber_to_epointer(nInnerBoundary) - 1];
            assert(innerBDE->GetEntityTypeNumber() == 142);
            ent144->AddInner(innerBDE);
            dictionaries_[lnumber_to_epointer(nInnerBoundary) - 1] = 0;
        }

        ent144->SetSurface(dSurf);
        dictionaries_[lnumber_to_epointer(nSurface) - 1] = 0; //surface is zero

        return true;
    }

    bool iges_loader::construct()
    {
        size_t sz = dictionaries_.size();
        for (size_t i = 0; i < sz; i++)
        {
            iges_dictionary* dict = dictionaries_[i];
            if (dict)
            {
                int nType = dict->GetEntityTypeNumber();
                switch (nType)
                {
                case 102:
                    if (!construct_102(i))
                    {
                        return false;
                    }
                    break;
                case 108:
                    if (!construct_108(i))
                    {
                        return false;
                    }
                    break;
                case 120:
                    if (!construct_120(i))
                    {
                        return false;
                    }
                    break;
                case 142:
                    if (!construct_142(i))
                    {
                        return false;
                    }
                    break;
                case 144:
                    if (!construct_144(i))
                    {
                        return false;
                    }
                    break;
                }
            }
        }
        return true;
    }

    bool iges_loader::load_Internal(const char* szFileName)
    {
        FILE* fp = fopen(szFileName, "rb");
        if (!fp) return false;

        bool bRet = this->load_Internal(fp);

        if (fp) fclose(fp);

        return bRet;
    }

    bool iges_loader::load_Internal(FILE* fp)
    {
        assert(fp);

        bool bRet = true;
        char buffer[80 + 1 + 1 + 1] = {};
        while (!feof(fp))
        {
            char* s = fgets(buffer, 83, fp);
            if (!s) break;

            char id = buffer[72]; //
            switch (id)
            {
            case 'S':
                if (!load_S(fp, buffer)) return false;
                break;
            case 'G':
                if (!load_G(fp, buffer)) return false;
                break;
            case 'D':
                bRet = load_D(fp, buffer);
                assert(bRet);
                if (!bRet) return false;
                break;
            case 'P':
                bRet = load_P(fp, buffer);
                assert(bRet);
                if (!bRet) return false;
                break;
            }
        }
        return true;
    }

    bool iges_loader::load_S(FILE* fp, char buffer[])
    {
        char str[73] = {};
        memcpy(str, buffer, sizeof(char) * 72);
        str[72] = 0;
        start_sections_.push_back(str);
        return true;
    }
    bool iges_loader::load_G(FILE* fp, char buffer[])
    {
        char str[73] = {};
        memcpy(str, buffer, sizeof(char) * 72);
        str[72] = 0;
        global_sections_.push_back(str);
        return true;
    }
    bool iges_loader::load_D(FILE* fp, char buffer[])
    {
        char id = buffer[72]; //
        if (id != 'D')
        {
            return false;
        }
        char str[72 * 2 + 1] = {};
        memcpy(str, buffer, 72);
        char* s = fgets(buffer, 83, fp);
        if (!s) return false;
        id = buffer[72]; //
        if (id != 'D') return false;
        memcpy(str + 72, buffer, 72);
        iges_dictionary* ent = new iges_dictionary(str);
        dictionaries_.push_back(ent);
        return true;
    }

    bool iges_loader::load_P(FILE* fp, char buffer[])
    {
        int lnumber = 0;
        int epointer = 0;
        std::stringstream(std::string(buffer + 65, 8)) >> std::skipws >> lnumber;
        epointer = lnumber_to_epointer(lnumber) - 1;

        if (epointer >= dictionaries_.size()) return false;
        iges_dictionary* dict = dictionaries_[epointer];
        if (!dict) return false;
        int nLine = dict->GetParameterLineCount();
        std::string paramData(buffer, 64);
        for (int i = 1; i < nLine; i++)
        {
            char* s = fgets(buffer, 83, fp);
            if (!s) return false;
            int lnumber2 = 0;
            std::stringstream(std::string(buffer + 65, 8)) >> std::skipws >> lnumber2;
            if (lnumber != lnumber2)
            {
                return false;
            }
            paramData += std::string(buffer, 64);
        }

        if (global_sections_.empty()) return false;
        char delim = ','; //global_sections_[0][0];
        iges_entity* ent = dict->create_entity(delim, paramData.c_str());

        return true;
    }

    const std::vector<iges_dictionary*>& iges_loader::get_dictionaries() const
    {
        return dictionaries_;
    }

    std::vector<iges_dictionary*>& iges_loader::get_dictionaries()
    {
        return dictionaries_;
    }
}
