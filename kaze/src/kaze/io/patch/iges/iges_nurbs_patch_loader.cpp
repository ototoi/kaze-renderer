#define _USE_MATH_DEFINES
#include <math.h>

#include "iges_nurbs_patch_loader.h"
#include "iges_loader.h"
#include "iges_entity.h"
#include <vector>
#include <cmath>
#include <stdio.h>

#if 1
#include "types.h"
#else
#include "vector3.h"
#endif

#if 1
#define PRINT_ERR(i, s) printf("%d:%s\n", i, s)
#else
#include <fstream>
static void print__(int i, const char* s)
{
    std::ofstream ofs("log_iges.txt", std::ios_base::app);
    ofs << i << ":" << s << std::endl;
}

#define PRINT_ERR(i, s) print__(i, s);
#endif

namespace kaze
{

    /*
	struct io_nurbs_patch{
        int nu;
        int nv;
        std::vector<double> uknots;
        std::vector<double> vknots;
        std::vector<double> cps;//xyzw xyzw xyzw....
        std::vector<io_nurbs_loop> loops;
        unsigned int nID;
        unsigned int nMat;
    };
	*/

    struct uv_domain
    {
        double u0;
        double u1;
        double v0;
        double v1;
    };

    static double GetValue(double x, double x0, double x1)
    {
        return (x - x0) / (x1 - x0);
    }

    static bool ConvertCurve(io_nurbs_curve* out, const iges_entity_100* ent, const uv_domain& uv)
    {
        double center[2] = {ent->get_arc_center_x(), ent->get_arc_center_y()};
        double start[2] = {ent->get_start_x(), ent->get_start_y()};
        double term[2] = {ent->get_terminate_x(), ent->get_terminate_y()};

        vector2 cv = vector2(center[0], center[1]);
        vector2 sv = vector2(start[0], start[1]);
        vector2 tv = vector2(term[0], term[1]);

        vector2 ds = sv - cv;
        vector2 dt = tv - cv;

        double rad = length(ds);

        ds = normalize(ds);
        dt = normalize(dt);

        double cos_ = dot(ds, dt);
        double theta = acos(cos_);

        double theta0 = acos(ds[0]);

        std::vector<vector2> cp;
        static const int div = 16;
        double dc = theta / div;
        for (int i = 0; i <= div; i++)
        {
            double t = theta0 + dc * i;
            double x = rad * cos(t);
            double y = rad * sin(t);
            cp.push_back(cv + vector2(x, y));
        }

        int csz = (int)cp.size();

        std::vector<double> cps;
        for (int i = 0; i < csz; i++)
        {
            cps.push_back(cp[i][0]);
            cps.push_back(cp[i][1]);
            cps.push_back(0);
            cps.push_back(1);
        }

        std::vector<double> knots;
        knots.push_back(0);
        for (int i = 0; i < csz; i++)
        {
            knots.push_back(i);
        }
        knots.push_back(csz - 1);

        out->cps = cps;
        out->knots = knots;

        return true;
    }

    //110
    static bool ConvertCurve(io_nurbs_curve* out, const iges_entity_110* ent, const uv_domain& uv)
    {
        double points[6] = {
            ent->get_start_x(), ent->get_start_y(), ent->get_start_z(),
            ent->get_terminate_x(), ent->get_terminate_y(), ent->get_terminate_z()};

        double knots_[4] = {0, 0, 1, 1};

        int nPoints = 2;
        int nOrder = 2;
        std::vector<double> cps(4 * nPoints);
        std::vector<double> knots(knots_, knots_ + 4);

        for (int i = 0; i < nPoints; i++)
        {
            cps[4 * i + 0] = GetValue(points[3 * i + 0], uv.u0, uv.u1);
            cps[4 * i + 1] = GetValue(points[3 * i + 1], uv.v0, uv.v1);
            cps[4 * i + 2] = (points[3 * i + 2]);
            cps[4 * i + 3] = 1;
        }
        out->knots.swap(knots);
        out->cps.swap(cps);

        return true;
    }

    static bool ConvertCurve(io_nurbs_curve* out, const iges_entity_126* ent, const uv_domain& uv)
    {
        int nPoints = ent->get_n();
        int nOrder = ent->get_degree() + 1;
        std::vector<double> cps(4 * nPoints);
        std::vector<double> knots = ent->get_knot();
        std::vector<double> cc = ent->get_ctrl_points();
        const std::vector<double>& ww = ent->get_weights();
        for (int i = 0; i < nPoints; i++)
        {
            cps[4 * i + 0] = GetValue(cc[3 * i + 0], uv.u0, uv.u1);
            cps[4 * i + 1] = GetValue(cc[3 * i + 1], uv.v0, uv.v1);
            cps[4 * i + 2] = (cc[3 * i + 2]);
            cps[4 * i + 3] = ww[i];
        }
        out->knots.swap(knots);
        out->cps.swap(cps);

        return true;
    }

    static bool ConvertCurve(io_nurbs_curve* out, const iges_dictionary* dict, const uv_domain& uv)
    {
        int nType = dict->GetEntityTypeNumber();
        if (nType == 100)
        { //arc
            const iges_entity_100* ent100 = dynamic_cast<const iges_entity_100*>(dict->get_entity());
            return ConvertCurve(out, ent100, uv);
        }
        else if (nType == 110)
        { //line
            const iges_entity_110* ent110 = dynamic_cast<const iges_entity_110*>(dict->get_entity());
            return ConvertCurve(out, ent110, uv);
        }
        else if (nType == 126)
        { //nurbs
            const iges_entity_126* ent126 = dynamic_cast<const iges_entity_126*>(dict->get_entity());
            return ConvertCurve(out, ent126, uv);
        }
        else
        {
            PRINT_ERR(nType, "convert curve");
            return false;
        }
        return true;
    }

    //arc
    static bool ConvertLoop(io_nurbs_loop* out, const iges_entity_100* ent, const uv_domain& uv)
    {
        io_nurbs_curve crv;
        if (!ConvertCurve(&crv, ent, uv)) return false;
        out->curves.push_back(crv);
        return true;
    }

    //line
    static bool ConvertLoop(io_nurbs_loop* out, const iges_entity_110* ent, const uv_domain& uv)
    {
        io_nurbs_curve crv;
        if (!ConvertCurve(&crv, ent, uv)) return false;
        out->curves.push_back(crv);
        return true;
    }

    //nurbs
    static bool ConvertLoop(io_nurbs_loop* out, const iges_entity_126* ent, const uv_domain& uv)
    {
        io_nurbs_curve crv;
        if (!ConvertCurve(&crv, ent, uv)) return false;
        out->curves.push_back(crv);
        return true;
    }

    static bool ConvertLoop(io_nurbs_loop* out, const iges_entity_102* ent, const uv_domain& uv)
    {
        const std::vector<iges_dictionary*>& crvs = ent->get_curves();
        if (crvs.size() == 0) return false;
        for (size_t i = 0; i < crvs.size(); i++)
        {
            const iges_dictionary* crv = crvs[i];
            int nType = crv->GetEntityTypeNumber();
            if (nType == 100)
            {
                const iges_entity_100* ent100 = dynamic_cast<const iges_entity_100*>(crv->get_entity());
                if (!ConvertLoop(out, ent100, uv))
                {
                    return false;
                }
            }
            else if (nType == 110)
            {
                const iges_entity_110* ent110 = dynamic_cast<const iges_entity_110*>(crv->get_entity());
                if (!ConvertLoop(out, ent110, uv))
                {
                    return false;
                }
            }
            else if (nType == 126)
            {
                const iges_entity_126* ent126 = dynamic_cast<const iges_entity_126*>(crv->get_entity());
                if (!ConvertLoop(out, ent126, uv))
                {
                    return false;
                }
            }
            else
            {
                PRINT_ERR(nType, "convert loop");
            }
        }
        return true;
    }

    static bool ConvertLoop(io_nurbs_loop* out, const iges_entity_142* ent, const uv_domain& uv)
    {
        const std::vector<iges_dictionary*>& crvs = ent->get_curves();
        if (crvs.size() == 0)
        {
            return false;
        }
        for (size_t i = 0; i < crvs.size(); i++)
        {
            const iges_dictionary* crv = crvs[i];
            int nType = crv->GetEntityTypeNumber();
            if (nType == 102)
            {
                const iges_entity_102* ent102 = dynamic_cast<const iges_entity_102*>(crv->get_entity());
                if (!ConvertLoop(out, ent102, uv))
                {
                    return false;
                }
            }
            else if (nType == 100)
            {
                const iges_entity_100* ent100 = dynamic_cast<const iges_entity_100*>(crv->get_entity());
                if (!ConvertLoop(out, ent100, uv))
                {
                    return false;
                }
            }
            else if (nType == 110)
            {
                const iges_entity_110* ent110 = dynamic_cast<const iges_entity_110*>(crv->get_entity());
                if (!ConvertLoop(out, ent110, uv))
                {
                    return false;
                }
            }
            else if (nType == 126)
            {
                const iges_entity_126* ent126 = dynamic_cast<const iges_entity_126*>(crv->get_entity());
                if (!ConvertLoop(out, ent126, uv))
                {
                    return false;
                }
            }
            else
            {
                PRINT_ERR(nType, "convert loop");
                return false;
            }
        }
        return true;
    }

    static bool ConvertLoop(io_nurbs_loop* out, const iges_dictionary* dict, const uv_domain& uv)
    {
        int nType = dict->GetEntityTypeNumber();
        if (nType == 102)
        {
            const iges_entity_102* ent102 = dynamic_cast<const iges_entity_102*>(dict->get_entity());
            return ConvertLoop(out, ent102, uv);
        }
        else if (nType == 100)
        {
            const iges_entity_100* ent100 = dynamic_cast<const iges_entity_100*>(dict->get_entity());
            return ConvertLoop(out, ent100, uv);
        }
        else if (nType == 110)
        {
            const iges_entity_110* ent110 = dynamic_cast<const iges_entity_110*>(dict->get_entity());
            return ConvertLoop(out, ent110, uv);
        }
        else if (nType == 126)
        {
            const iges_entity_126* ent126 = dynamic_cast<const iges_entity_126*>(dict->get_entity());
            return ConvertLoop(out, ent126, uv);
        }
        else if (nType == 142)
        {
            const iges_entity_142* ent142 = dynamic_cast<const iges_entity_142*>(dict->get_entity());
            return ConvertLoop(out, ent142, uv);
        }
        else
        {
            PRINT_ERR(nType, "convert loop");
            return false;
        }
        return true;
    }

    //-----------------------------------------------------------------
    static io_nurbs_curve Transform(const matrix4& mat, const io_nurbs_curve& crv)
    {
        std::vector<double> cps(crv.cps);
        for (int i = 0; i < cps.size() / 4; i++)
        {
            double x = cps[4 * i + 0];
            double y = cps[4 * i + 1];
            double z = cps[4 * i + 2];
            vector3 p = mat * vector3(x, y, z);
            cps[4 * i + 0] = p[0];
            cps[4 * i + 1] = p[1];
            cps[4 * i + 2] = p[2];
        }

        io_nurbs_curve tmp;
        tmp.cps.swap(cps);
        tmp.knots = crv.knots;
        return tmp;
    }

    static double radians(double x)
    {
        return x / 360.0 * 2 * M_PI;
    }

    static bool ConvertPatch(io_nurbs_patch* out, const iges_entity_120* ent)
    {
        uv_domain uv;
        uv.u0 = 0;
        uv.u1 = 1;
        uv.v0 = 0;
        uv.v1 = 1;

        double start = ent->get_start_angle();
        double term = ent->get_terminate_angle();

        if (start >= term) return false;

        io_nurbs_curve axis;
        if (!ConvertCurve(&axis, ent->get_axis(), uv)) return false;

        io_nurbs_curve generatrix;
        if (!ConvertCurve(&generatrix, ent->get_generatrix(), uv)) return false;

        vector3 org = vector3(&axis.cps[0]);
        vector3 org_x = vector3(&generatrix.cps[0]);

        vector3 org_z = vector3(&axis.cps[4]);

        vector3 x = normalize(org_x - org);
        vector3 z = normalize(org_z - org);
        vector3 y = normalize(cross(z, x));
        x = cross(y, z);

        matrix4 rot(
            x[0], x[1], x[2], 0,
            y[0], y[1], y[2], 0,
            z[0], z[1], z[2], 0,
            0, 0, 0, 1);
        matrix4 irot = transpose(rot);

        matrix4 trns = matrix4(
            1, 0, 0, -org[0],
            0, 1, 0, -org[1],
            0, 0, 1, -org[2],
            0, 0, 0, 1);

        matrix4 itrns = matrix4(
            1, 0, 0, +org[0],
            0, 1, 0, +org[1],
            0, 0, 1, +org[2],
            0, 0, 0, 1);

        matrix4 mat = rot * trns;
        matrix4 imat = itrns * irot;

#if 0
		std::vector<io_nurbs_curve> crvs_;
		static const int NUM = 32;
		double range = term-start;
		for(int i=0;i<=NUM;i++){
			double rad = start+range*double(i)/NUM;
			matrix4 R   = mat4_gen::rotation_z(rad);
			matrix4 m = imat*R*mat;
			crvs_.push_back(Transform(m, generatrix));
		}

		std::vector<double> cps;
		size_t usz = crvs_[0].cps.size()/4;
		cps.reserve(usz*(NUM+1)*4);
		for(int i=0;i<=NUM;i++){
			for(int j=0;j<usz;j++){
				double x = crvs_[i].cps[4*j+0];
				double y = crvs_[i].cps[4*j+1];
				double z = crvs_[i].cps[4*j+2];
				double w = crvs_[i].cps[4*j+3];

				cps.push_back(x);
				cps.push_back(y);
				cps.push_back(z);
				cps.push_back(w);
			}
		}

		std::vector<double> knots;
		knots.push_back(0);
		for(int i=0;i<=NUM;i++){
			knots.push_back(i);
		}
		knots.push_back(NUM);

		out->nu = usz;
		out->nv = NUM+1;
		out->uknots = crvs_[0].knots;
		out->vknots = knots;
		out->cps = cps;
#else
        std::vector<io_nurbs_curve> crvs_;
        {
            double r2 = sqrt(2.0);
            double ws[] = {1, r2 / 2, 1, r2 / 2, 1, r2 / 2, 1, r2 / 2};
            double rs[] = {1, r2, 1, r2, 1, r2, 1, r2};
            double rads[] = {0, 45, 90, 135, 180, 225, 270, 315};
            for (int i = 0; i < 8; i++)
            {
                double rad = radians(rads[i]);
                double s = rs[i];
                matrix4 R = mat4_gen::rotation_z(rad);
                matrix4 S = mat4_gen::scaling(s, s, 1);
                matrix4 m = imat * R * S * mat;
                io_nurbs_curve crv = Transform(m, generatrix);
                for (int j = 0; j < crv.cps.size() / 4; j++)
                {
                    crv.cps[4 * j + 3] *= ws[i];
                }
                crvs_.push_back(crv);
            }
        }

        std::vector<double> cps;
        size_t usz = crvs_[0].cps.size() / 4;
        cps.reserve(usz * 9 * 4);
        for (int i = 0; i < 9; i++)
        {
            int ii = i % 8;
            for (int j = 0; j < usz; j++)
            {
                double x = crvs_[ii].cps[4 * j + 0];
                double y = crvs_[ii].cps[4 * j + 1];
                double z = crvs_[ii].cps[4 * j + 2];
                double w = crvs_[ii].cps[4 * j + 3];

                cps.push_back(x);
                cps.push_back(y);
                cps.push_back(z);
                cps.push_back(w);
            }
        }

        std::vector<double> knots;
        {
            double kk[] = {
                0, 0, 0,
                1, 1,
                2, 2,
                3, 3,
                4, 4, 4};
            knots.assign(kk, kk + 12);
        }

        out->nu = (int)usz;
        out->nv = 9;
        out->uknots = crvs_[0].knots;
        out->vknots = knots;
        out->cps = cps;
#endif

        return true;
    }

    static bool ConvertPatch(io_nurbs_patch* out, const iges_entity_124* ent)
    {

        return false;
    }

    static bool ConvertPatch(io_nurbs_patch* out, const iges_entity_128* ent)
    {
        int nu = ent->get_nu();
        int nv = ent->get_nv();

        const std::vector<double>& cc = ent->get_ctrl_points();
        const std::vector<double>& ww = ent->get_weights();
        std::vector<double> cps(nu * nv * 4);
        for (int j = 0; j < nv; j++)
        {
            for (int i = 0; i < nu; i++)
            {
                size_t index = nu * j + i;
                cps[4 * index + 0] = cc[3 * index + 0];
                cps[4 * index + 1] = cc[3 * index + 1];
                cps[4 * index + 2] = cc[3 * index + 2];
                cps[4 * index + 3] = ww[index];
            }
        }
        const std::vector<double>& uknots = ent->get_knot_u();
        const std::vector<double>& vknots = ent->get_knot_v();

        if (uknots.empty() || vknots.empty())
        {
            return false;
        }

        out->nu = nu;
        out->nv = nv;
        out->uknots = uknots;
        out->vknots = vknots;
        out->cps.swap(cps);
        out->nID = 0;
        out->nMat = 0;
        return true;
    }

    static bool ConvertPatch(io_nurbs_patch* out, const iges_entity_144* ent)
    {
        iges_dictionary* dSurface = ent->GetSurface();
        if (!dSurface) return false;
        int nType = dSurface->GetEntityTypeNumber();

        uv_domain uv;
        uv.u0 = 0;
        uv.u1 = 1;
        uv.v0 = 0;
        uv.v1 = 1;
        if (nType == 120)
        {
            const iges_entity_120* ent120 = dynamic_cast<const iges_entity_120*>(dSurface->get_entity());
            if (!ConvertPatch(out, ent120)) return false;

            uv.u0 = 0;
            uv.u1 = 1;

            uv.v0 = ent120->get_start_angle();
            uv.v1 = ent120->get_terminate_angle();
        }
        else if (nType == 128)
        {
            const iges_entity_128* ent128 = dynamic_cast<const iges_entity_128*>(dSurface->get_entity());
            if (!ConvertPatch(out, ent128)) return false;

            uv.u0 = ent128->get_start_u();
            uv.u1 = ent128->get_end_u();
            uv.v0 = ent128->get_start_v();
            uv.v1 = ent128->get_end_v();
        }
        else
        {
            PRINT_ERR(nType, "convert patch");
            return false;
        }

        //if(nType==120)return true;

        const std::vector<iges_dictionary*>& outers = ent->get_outer_loops();
        for (size_t i = 0; i < outers.size(); i++)
        {
            const iges_dictionary* loop = outers[i];
            int nType = loop->GetEntityTypeNumber();
            if (nType == 142)
            {
                const iges_entity_142* ent142 = dynamic_cast<const iges_entity_142*>(loop->get_entity());
                io_nurbs_loop lp;
                if (ConvertLoop(&lp, ent142, uv))
                {
                    lp.nDirection = 1;
                    out->loops.push_back(lp);
                }
            }
            else
            {
                PRINT_ERR(nType, "convert loops");
                return false;
            }
        }

        const std::vector<iges_dictionary*>& inners = ent->get_inner_loops();
        for (size_t i = 0; i < inners.size(); i++)
        {
            const iges_dictionary* loop = inners[i];
            int nType = loop->GetEntityTypeNumber();
            if (nType == 142)
            {
                const iges_entity_142* ent142 = dynamic_cast<const iges_entity_142*>(loop->get_entity());
                io_nurbs_loop lp;
                if (ConvertLoop(&lp, ent142, uv))
                {
                    lp.nDirection = -1;
                    out->loops.push_back(lp);
                }
            }
            else
            {
                PRINT_ERR(nType, "convert loops");
                return false;
            }
        }

        return true;
    }

    class iges_nurbs_patch_loader_imp
    {
    public:
        iges_nurbs_patch_loader_imp(const char* szFileName);
        ~iges_nurbs_patch_loader_imp();

    public:
        bool get_patches(size_t& sz) const;
        bool get_patch(size_t i, io_nurbs_patch* patch) const;

    private:
        iges_loader* ldr_;
        std::vector<const iges_entity*> nurbs_;
    };

    iges_nurbs_patch_loader_imp::iges_nurbs_patch_loader_imp(const char* szFileName)
    {
        ldr_ = new iges_loader();
        if (!ldr_->load(szFileName)) return;

        const std::vector<iges_dictionary*>& dicts = ldr_->get_dictionaries();
        for (size_t i = 0; i < dicts.size(); i++)
        {
            const iges_dictionary* dict = dicts[i];
            if (dict)
            {
                const iges_entity* ent = dict->get_entity();
                if (ent)
                {
                    int nType = ent->get_type_number();
                    switch (nType)
                    {
                    case 100:
                    case 102:
                    case 110:
                    case 126:
                        break;
                    case 120:
                    {
                        const iges_entity_120* ent120 = dynamic_cast<const iges_entity_120*>(ent);
                        if (ent120)
                        {
                            io_nurbs_patch tmp;
                            if (ConvertPatch(&tmp, ent120))
                            {
                                nurbs_.push_back(ent120);
                            }
                        }
                    }
                    break;
                    case 124:
                    {
                        const iges_entity_124* ent124 = dynamic_cast<const iges_entity_124*>(ent);
                        if (ent124)
                        {
                            io_nurbs_patch tmp;
                            if (ConvertPatch(&tmp, ent124))
                            {
                                nurbs_.push_back(ent124);
                            }
                        }
                    }
                    break;
                    case 128:
                    {
                        const iges_entity_128* ent128 = dynamic_cast<const iges_entity_128*>(ent);
                        if (ent128)
                        {
                            io_nurbs_patch tmp;
                            if (ConvertPatch(&tmp, ent128))
                            {
                                nurbs_.push_back(ent128);
                            }
                        }
                    }
                    break;
                    case 144:
                    {
                        const iges_entity_144* ent144 = dynamic_cast<const iges_entity_144*>(ent);
                        if (ent144)
                        {
                            io_nurbs_patch tmp;
                            if (ConvertPatch(&tmp, ent144))
                            {
                                nurbs_.push_back(ent144);
                            }
                        }
                    }
                    break;
                    default:
                        PRINT_ERR(nType, "convert patch");
                    }
                }
            }
        }
    }

    iges_nurbs_patch_loader_imp::~iges_nurbs_patch_loader_imp()
    {
        delete ldr_;
    }

    bool iges_nurbs_patch_loader_imp::get_patches(size_t& sz) const
    {
        sz = nurbs_.size();
        return true;
    }

    bool iges_nurbs_patch_loader_imp::get_patch(size_t i, io_nurbs_patch* patch) const
    {
        if (i >= nurbs_.size())
        {
            return false;
        }
        const iges_entity* ent = nurbs_[i];
        int nType = ent->get_type_number();
        bool bRet = true;
        switch (nType)
        {
        case 120:
            bRet = ConvertPatch(patch, dynamic_cast<const iges_entity_120*>(ent));
            if (!bRet)
            {
                return false;
            }
        case 128:
            bRet = ConvertPatch(patch, dynamic_cast<const iges_entity_128*>(ent));
            if (!bRet)
            {
                return false;
            }
        case 144:
            bRet = ConvertPatch(patch, dynamic_cast<const iges_entity_144*>(ent));
            if (!bRet)
            {
                return false;
            }
        }
        return true;
    }

    //----------------------------------------------------------------------

    iges_nurbs_patch_loader::iges_nurbs_patch_loader(const char* szFileName)
    {
        imp_ = new iges_nurbs_patch_loader_imp(szFileName);
    }

    iges_nurbs_patch_loader::~iges_nurbs_patch_loader()
    {
        delete imp_;
    }

    bool iges_nurbs_patch_loader::get_patches(size_t& sz) const
    {
        return imp_->get_patches(sz);
    }

    bool iges_nurbs_patch_loader::get_patch(size_t i, io_nurbs_patch* patch) const
    {
        return imp_->get_patch(i, patch);
    }
}
