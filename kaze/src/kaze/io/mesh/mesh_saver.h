#ifndef KAZE_MESH_SAVER_H
#define KAZE_MESH_SAVER_H

#include "io_mesh.h"

namespace kaze
{

    class mesh_loader;

    class mesh_saver
    {
    public:
        virtual bool save(const mesh_loader* ml);

    public:
        virtual bool set_vertices(size_t sz);
        virtual bool set_faces(size_t sz);
        virtual bool set_vertex(size_t i, io_mesh_vertex* v);
        virtual bool set_face(size_t i, io_mesh_face* f);

    public:
        virtual ~mesh_saver() {}
    };
}

#endif
