#define _LARGEFILE_SOURCE
#define _FILE_OFFSET_BITS 64
#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include "temporary_mesh_loader.h"
#include "file.h"

//#include "logger.h"

#include <stdio.h>
#include <vector>

#ifdef _MSC_VER
#define fseek64 _fseeki64
#define ftell64 _ftelli64
#else
#define fseek64 fseek
#define ftell64 ftell
#endif

namespace kaze
{

    class temporary_mesh_loader_imp
    {
    public:
        temporary_mesh_loader_imp();
        ~temporary_mesh_loader_imp();
        bool get_vertices(size_t& sz) const;
        bool get_faces(size_t& sz) const;
        bool get_vertex(size_t i, io_mesh_vertex* v) const;
        bool get_face(size_t i, io_mesh_face* f) const;

    public:
        bool add_vertices(const double vertices[], size_t sz);
        bool add_vertices(const std::vector<double>& vertices);
        bool add_vertices(const std::vector<vector3>& vertices);

    private:
        FILE* fp_;
        std::string strPath_;
        mutable size_t offset_;
    };
    //---------------------------------------------------------
    temporary_mesh_loader_imp::temporary_mesh_loader_imp()
    {
        char szPath[512];
        system::get_temppath_nodelete(szPath, 512, "TML");
        fp_ = fopen(szPath, "w+b");
        strPath_ = szPath;
        offset_ = 0;
    }
    temporary_mesh_loader_imp::~temporary_mesh_loader_imp()
    {
        if (fp_) fclose(fp_);
        system::delete_file(strPath_.c_str());
    }
    bool temporary_mesh_loader_imp::get_vertices(size_t& sz) const
    {
        if (fseek64(fp_, 0, SEEK_END) != 0) return false;
        size_t tsz = ftell64(fp_);
        if (tsz % 24 != 0) return false;
        tsz = tsz / 3 / sizeof(double);
        sz = tsz;
        return true;
    }
    bool temporary_mesh_loader_imp::get_faces(size_t& sz) const
    {
        size_t vsz;
        if (!get_vertices(vsz)) return false;
        sz = vsz / 3;
        return true;
    }
    bool temporary_mesh_loader_imp::get_vertex(size_t i, io_mesh_vertex* v) const
    {
        double vert[3];
        size_t offset = 3 * i * sizeof(double);
        if (offset_ != offset)
        {
            if (fseek64(fp_, offset, SEEK_SET) != 0) return false;
            offset_ = offset;
        }
        if (fread(vert, sizeof(double), 3, fp_) != 3) return false;
        offset_ += 3 * sizeof(double);
        memcpy(v->pos, vert, sizeof(double) * 3);
        v->rgba = 0;
        return true;
    }
    bool temporary_mesh_loader_imp::get_face(size_t i, io_mesh_face* f) const
    {
        f->i0 = 3 * i + 0;
        f->i1 = 3 * i + 1;
        f->i2 = 3 * i + 2;
        return true;
    }

    bool temporary_mesh_loader_imp::add_vertices(const double vertices[], size_t sz)
    {
        if (sz != fwrite(vertices, sizeof(double), sz, fp_)) return false;
        offset_ += sizeof(double) * sz;
        return true;
    }

    bool temporary_mesh_loader_imp::add_vertices(const std::vector<double>& vertices)
    {
        if (vertices.empty()) return true;
        return add_vertices(&vertices[0], vertices.size());
    }
    bool temporary_mesh_loader_imp::add_vertices(const std::vector<vector3>& vertices)
    {
        if (vertices.empty()) return true;
        if (sizeof(double) == sizeof(real))
        {
            return add_vertices((const double*)&vertices[0], vertices.size() * 3);
        }
        else
        {
            size_t sz = vertices.size();
            std::vector<double> v(vertices.size() * 3);
            for (size_t i = 0; i < sz; i++)
            {
                v[3 * i + 0] = vertices[i][0];
                v[3 * i + 1] = vertices[i][1];
                v[3 * i + 2] = vertices[i][2];
            }
            return add_vertices(v);
        }
    }

    //---------------------------------------------------------

    temporary_mesh_loader::temporary_mesh_loader()
    {
        imp_ = new temporary_mesh_loader_imp();
    }
    temporary_mesh_loader::~temporary_mesh_loader()
    {
        delete imp_;
    }
    bool temporary_mesh_loader::get_vertices(size_t& sz) const
    {
        return imp_->get_vertices(sz);
    }
    bool temporary_mesh_loader::get_faces(size_t& sz) const
    {
        return imp_->get_faces(sz);
    }
    bool temporary_mesh_loader::get_vertex(size_t i, io_mesh_vertex* v) const
    {
        return imp_->get_vertex(i, v);
    }
    bool temporary_mesh_loader::get_face(size_t i, io_mesh_face* f) const
    {
        return imp_->get_face(i, f);
    }
    bool temporary_mesh_loader::add_vertices(const std::vector<double>& vertices)
    {
        return imp_->add_vertices(vertices);
    }
    bool temporary_mesh_loader::add_vertices(const std::vector<vector3>& vertices)
    {
        return imp_->add_vertices(vertices);
    }
}
