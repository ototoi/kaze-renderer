#include "transformed_mesh_saver.h"
#include "transformed_mesh_loader.h"

#include "proxy_mesh_loader.h"

namespace kaze
{
    namespace
    {
        void set(double d[3], const vector3& p)
        {
            d[0] = p[0];
            d[1] = p[1];
            d[2] = p[2];
        }
    }

    transformed_mesh_saver::transformed_mesh_saver(
        const std::shared_ptr<mesh_saver>& ms, const std::shared_ptr<transformer>& tr) : ms_(ms), tr_(tr) {}

    bool transformed_mesh_saver::save(const mesh_loader* ml)
    {
        transformed_mesh_loader aml(
            std::shared_ptr<mesh_loader>(new proxy_mesh_loader(ml)), //no delete
            tr_);
        return ms_->save(&aml);
    }

    bool transformed_mesh_saver::set_vertices(size_t sz) { return ms_->set_vertices(sz); }
    bool transformed_mesh_saver::set_faces(size_t sz) { return ms_->set_faces(sz); }

    bool transformed_mesh_saver::set_vertex(size_t i, io_mesh_vertex* v)
    {
        set(v->pos, tr_->transform_p(vector3(v->pos)));
        return ms_->set_vertex(i, v);
    }

    bool transformed_mesh_saver::set_face(size_t i, io_mesh_face* f)
    {
        set(f->n0, tr_->transform_n(vector3(f->n0)));
        set(f->n1, tr_->transform_n(vector3(f->n1)));
        set(f->n2, tr_->transform_n(vector3(f->n2)));

        return ms_->set_face(i, f);
    }
}
