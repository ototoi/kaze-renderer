#ifndef KAZE_PROXY_MESH_LOADER_H
#define KAZE_PROXY_MESH_LOADER_H

#include "mesh_loader.h"

namespace kaze
{

    class proxy_mesh_loader : public mesh_loader
    {
    public:
        proxy_mesh_loader(const mesh_loader* ml) : ml_(ml) {}
        ~proxy_mesh_loader() { /**/}
    public:
        bool load(mesh_saver* ms) const { return ml_->load(ms); }
    public:
        bool get_vertices(size_t& sz) const { return ml_->get_vertices(sz); }
        bool get_faces(size_t& sz) const { return ml_->get_faces(sz); }
        bool get_vertex(size_t i, io_mesh_vertex* v) const { return ml_->get_vertex(i, v); }
        bool get_face(size_t i, io_mesh_face* f) const { return ml_->get_face(i, f); }
    private:
        const mesh_loader* ml_;
    };
}

#endif