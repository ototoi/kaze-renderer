#ifndef KAZE_IO_MESH_H
#define KAZE_IO_MESH_H

#include <cstdlib>

namespace kaze
{

    struct io_mesh_vertex
    {
        union
        {
            struct
            {
                double x;
                double y;
                double z;
            };
            double pos[3];
        };

        unsigned int rgba;
    };

    struct io_mesh_face
    {
        size_t i0;
        size_t i1;
        size_t i2;

        double n0[3];
        double n1[3];
        double n2[3];

        double uv0[2];
        double uv1[2];
        double uv2[2];

        const void* p_shader;
        size_t i_shader;
    };
}

#endif
