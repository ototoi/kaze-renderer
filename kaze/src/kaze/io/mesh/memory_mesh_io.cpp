#include "memory_mesh_io.h"
#include "io_mesh.h"

#include <string.h>

namespace kaze
{

    bool memory_mesh_io::get_vertices(size_t& sz) const
    {
        sz = mv_.size();
        return true;
    }
    bool memory_mesh_io::get_faces(size_t& sz) const
    {
        sz = mf_.size();
        return true;
    }
    bool memory_mesh_io::get_vertex(size_t i, io_mesh_vertex* v) const
    {
        memcpy(v, &mv_[i], sizeof(io_mesh_vertex));
        return true;
    }
    bool memory_mesh_io::get_face(size_t i, io_mesh_face* f) const
    {
        *f = mf_[i];
        return true;
    }
    bool memory_mesh_io::set_vertices(size_t sz)
    {
        mv_.resize(sz);
        return true;
    }
    bool memory_mesh_io::set_faces(size_t sz)
    {
        mf_.resize(sz);
        return true;
    }
    bool memory_mesh_io::set_vertex(size_t i, io_mesh_vertex* v)
    {
        mv_[i] = *v;
        return true;
    }
    bool memory_mesh_io::set_face(size_t i, io_mesh_face* f)
    {
        mf_[i] = *f;
        return true;
    }
}
