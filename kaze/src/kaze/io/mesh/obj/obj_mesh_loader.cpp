#include "types.h"
#include "obj_mesh_loader.h"
#include "mesh_saver.h"

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

#include <string.h>

#include "logger.h"

#if defined(_MSC_VER) && _MSC_VER >= 1300
#include <windows.h>
#pragma warning(disable : 4018)
#endif

namespace kaze
{
    namespace
    {
        typedef vector3 obj_vertex;

        template <class T>
        struct static_vector
        {
        public:
            T& operator[](size_t i) { return v_[i]; }
            const T& operator[](size_t i) const { return v_[i]; }
            void push_back(const T& t) { v_[n_++] = t; }
            size_t size() const { return n_; }
            void resize(size_t sz) { n_ = (char)sz; }
        protected:
            T v_[4];
            char n_;
        };

        struct obj_face
        {
            std::vector<int> v;
            std::vector<int> n;
            std::vector<int> t;
        };

        struct elem_size
        {
            size_t v;
            size_t n;
            size_t t;
            size_t f;
        };

        static void check_elem(elem_size& elem, std::istream& is)
        {
            std::string str;
            if (!(is >> str)) return;
            if (str.empty())
            {
                //�V���l
            }
            else if (str == "v")
            {
                elem.v++;
            }
            else if (str == "vn")
            {
                elem.n++;
            }
            else if (str == "vt")
            {
                elem.t++;
            }
            else if (str == "f")
            {
                elem.f++;
            }
        }

        int get_size(const obj_face& face)
        {
            return (int)face.v.size();
        }

        void split_face(const obj_face& face, obj_face& face1, obj_face& face2)
        {
            static const int map[] = {0, 1, 2, 3, 0, 1, 2, 3, 2};
            face1.v.resize(3);
            face1.n.resize(3);
            face1.t.resize(3);
            face2.v.resize(3);
            face2.n.resize(3);
            face2.t.resize(3);
            for (int i = 0; i < 3; i++)
            {
                face1.v[i] = face.v[i];
                face1.n[i] = face.n[i];
                face1.t[i] = face.t[i];
            }
            for (int i = 0; i < 3; i++)
            {
                int k = map[i + 2]; //2,3,0
                face2.v[i] = face.v[k];
                face2.n[i] = face.n[k];
                face2.t[i] = face.t[k];
            }
        }

        typedef std::istream istream;

        class obj_mesh_proxy
        {
        public:
            void add_vertex(double x, double y, double z) { vertices_.push_back(obj_vertex(x, y, z)); }
            void add_normal(double x, double y, double z) { normals_.push_back(obj_vertex(x, y, z)); }
            void add_texture(double x, double y, double z) { textures_.push_back(obj_vertex(x, y, z)); }

            void add_face(const obj_face& f) { faces_.push_back(f); }
        public:
            bool triangulate();

        public:
            std::vector<obj_vertex> vertices_;
            std::vector<obj_vertex> normals_;
            std::vector<obj_vertex> textures_;
            std::vector<obj_face> faces_;
        };

        bool obj_mesh_proxy::triangulate()
        {
            size_t fsz = faces_.size();
            for (size_t i = 0; i < fsz; i++)
            {
                obj_face& face = faces_[i];
                int nSz = (int)get_size(face);
                switch (nSz)
                {
                case 0:
                case 1:
                case 2:
                    return false;
                case 3:
                    //
                    break;
                case 4:
                {
                    obj_face face1, face2;
                    split_face(face, face1, face2);
                    faces_[i] = face1;
                    faces_.push_back(face2);
                }
                break;
                default:
                    return false;
                }
            }
            return true;
        }

        //struct obj_mesh_parser_error{};

        class obj_mesh_parser
        {
        public:
            obj_mesh_parser();
            ~obj_mesh_parser();

            bool load(const char* filename);

        protected:
            bool parse(istream& is);
            bool vertex(obj_mesh_proxy* mesh, istream& is);
            bool normal(obj_mesh_proxy* mesh, istream& is);
            bool texture(obj_mesh_proxy* mesh, istream& is);
            bool face(obj_mesh_proxy* mesh, istream& is);

        public:
            obj_mesh_proxy* get_mesh(int i) const { return prxy_[i]; }
            size_t size() const { return prxy_.size(); }
        private:
            std::vector<obj_mesh_proxy*> prxy_;
        };

        obj_mesh_parser::obj_mesh_parser()
        {
            ; //
        }

        obj_mesh_parser::~obj_mesh_parser()
        {
            size_t sz = prxy_.size();
            for (size_t i = 0; i < sz; i++)
            {
                delete prxy_[i];
            }
        }

        bool obj_mesh_parser::load(const char* filename)
        {
            {
                std::ifstream ifs;
#if defined(_MSC_VER) && _MSC_VER >= 1300
                char* str_loc = _strdup(setlocale(LC_CTYPE, NULL));
                setlocale(LC_CTYPE, "");
                ifs.open(filename);
                setlocale(LC_CTYPE, str_loc);
                free(str_loc);
#else
                ifs.open(filename);
#endif
                if (!ifs) return false;
                ifs.seekg(0);

                if (prxy_.empty())
                {
                    prxy_.push_back(new obj_mesh_proxy());
                }

                elem_size elem = {0, 0, 0, 0};
                size_t totalLine = 0;
                {
                    size_t nLine = 0;
                    std::string str;
                    while (std::getline(ifs, str))
                    {
                        std::stringstream ss(str);
                        check_elem(elem, ss);
                        if (nLine % 50000 == 0) print_progress("line = %d", nLine);
                        nLine++;
                    }
                    totalLine = nLine;
                }
                prxy_.back()->vertices_.reserve(elem.v);
                prxy_.back()->faces_.reserve(elem.f);
                prxy_.back()->normals_.reserve(elem.n);
                prxy_.back()->textures_.reserve(elem.t);
            }

            {
                std::ifstream ifs;
#if defined(_MSC_VER) && _MSC_VER >= 1300
                char* str_loc = _strdup(setlocale(LC_CTYPE, NULL));
                setlocale(LC_CTYPE, "");
                ifs.open(filename);
                setlocale(LC_CTYPE, str_loc);
                free(str_loc);
#else
                ifs.open(filename);
#endif
                if (!ifs) return false;
                ifs.seekg(0);

                size_t nLine = 0;
                std::string str;
                while (std::getline(ifs, str))
                {
                    if (str.empty()) continue;
                    std::stringstream ss(str);
                    if (!this->parse(ss)) return false;
                    if (nLine % 50000 == 0) print_progress("line = %d", nLine);
                    nLine++;
                }
            }
            {
                std::vector<obj_mesh_proxy*> tmp;
                size_t psz = prxy_.size();
                for (size_t i = 0; i < psz; i++)
                {
                    if (prxy_[i]->faces_.empty())
                    {
                        delete prxy_[i];
                    }
                    else
                    {
                        tmp.push_back(prxy_[i]);
                    }
                }
                prxy_.swap(tmp);
            }
            return true;
        }

        bool obj_mesh_parser::parse(istream& is)
        {
            if (prxy_.empty())
            {
                prxy_.push_back(new obj_mesh_proxy());
            }
            obj_mesh_proxy* mesh = prxy_.back();

            std::string str;
            if (!(is >> str)) return false;
            if (str.empty())
            {
                //�V���l
            }
            else if (str[0] == '#')
            {
                //�V���l
            }
            else if (str == "mtllib")
            {
                //�V���l
            }
            else if (str == "usemtl")
            {
                //�V���l
            }
            else if (str == "v")
            {
                if (!this->vertex(mesh, is)) return false;
            }
            else if (str == "vn")
            {
                if (!this->normal(mesh, is)) return false;
            }
            else if (str == "vt")
            {
                if (!this->texture(mesh, is)) return false;
            }
            else if (str == "f")
            {
                if (!this->face(mesh, is)) return false;
                //}else if(str == "g"){
                //    prxy_.push_back(new obj_mesh_proxy());
                //    mesh = prxy_.back();
            }
            else
            {
                //
            }
            return true;
        }

        bool obj_mesh_parser::vertex(obj_mesh_proxy* mesh, istream& is)
        {
            double x, y, z;
            is >> x >> y >> z;
            mesh->add_vertex(x, y, z);
            return true;
        }

        bool obj_mesh_parser::normal(obj_mesh_proxy* mesh, istream& is)
        {
            double x, y, z;
            is >> x >> y >> z;
            mesh->add_normal(x, y, z);
            return true;
        }

        bool obj_mesh_parser::texture(obj_mesh_proxy* mesh, istream& is)
        {
            double x, y, z;
            z = 0;
            is >> x >> y >> z;
            is.clear();
            mesh->add_texture(x, y, z);
            return true;
        }

        void split_index(const char* str, int* v, int* t, int* n)
        {
            char vt[64] = {0};
            char tt[64] = {0};
            char nt[64] = {0};

            //sscanf(str,"%s/%s/%s",vt,nt,tt);

            char* tx[] = {vt, tt, nt};
            const char* a = str;
            const char* b = a + strlen(str);
            const char* p = a;
            int i = 0;
            while (a < b && i < 3)
            {
                if (b <= p || *p == '/' || *p == ',')
                {
                    size_t sz = p - a;
                    if (sz > 63) sz = 63;
                    if (sz)
                        memcpy(tx[i], a, sz);
                    p++;
                    a = p;
                    i++;
                }
                else
                {
                    p++;
                }
            }

            if (isalnum(vt[0]))
            {
                *v = atoi(vt);
            }
            else
            {
                *v = -1;
            }
            if (isalnum(tt[0]))
            {
                *t = atoi(tt);
            }
            else
            {
                *t = -1;
            }
            if (isalnum(nt[0]))
            {
                *n = atoi(nt);
            }
            else
            {
                *n = -1;
            }
        }

        bool obj_mesh_parser::face(obj_mesh_proxy* mesh, istream& is)
        {
            //bool bN = bN_;
            //bool bT = bT_;

            //int nType = 0;
            //if(bN)nType |= 1;
            //if(bT)nType |= 2;

            obj_face face;
            int V, N, T;
            //char c;//for comma
            std::string str;
            int k = 0;
            while ((is >> str) && k < 16)
            {

                split_index(str.c_str(), &V, &T, &N);
                if (V > 0) V--;
                if (T > 0) T--;
                if (N > 0) N--;

                face.v.push_back(V);
                face.t.push_back(T);
                face.n.push_back(N);

                str.clear();
                k++;
            }

            mesh->add_face(face);
            return true;
        }
    }

    //-----------------------------------------------------

    class obj_mesh_loader_imp
    {
    public:
        obj_mesh_loader_imp(const char* filename);
        ~obj_mesh_loader_imp();

    public:
        bool load(mesh_saver* ms) const;

    protected:
        std::string str_;
    };

    obj_mesh_loader_imp::obj_mesh_loader_imp(const char* filename)
    {
        str_ = filename;
    }

    obj_mesh_loader_imp::~obj_mesh_loader_imp()
    {
        //
    }

    static inline void conv(double d[2], const vector2& v)
    {
        d[0] = v[0];
        d[1] = v[1];
    }
    static inline void conv(double d[3], const vector3& v)
    {
        d[0] = v[0];
        d[1] = v[1];
        d[2] = v[2];
    }
    static inline vector2 v3v2(const vector3& v) { return vector2(v[0], v[1]); }

    bool obj_mesh_loader_imp::load(mesh_saver* ms) const
    {
        obj_mesh_parser psr;

        if (!psr.load(str_.c_str())) return false;
        obj_mesh_proxy* mesh = psr.get_mesh(0);
        if (!mesh->triangulate()) return false;

        int vsz = (int)mesh->vertices_.size();
        int fsz = (int)mesh->faces_.size();
        int nsz = (int)mesh->normals_.size();
        int tsz = (int)mesh->textures_.size();

        if (!ms->set_vertices(vsz)) return false;
        if (!ms->set_faces(fsz)) return false;

        for (int i = 0; i < vsz; i++)
        {
            io_mesh_vertex tmp;
            memset(&tmp, 0, sizeof(io_mesh_vertex));
            tmp.pos[0] = mesh->vertices_[i][0];
            tmp.pos[1] = mesh->vertices_[i][1];
            tmp.pos[2] = mesh->vertices_[i][2];
            if (!ms->set_vertex(i, &tmp)) return false;
        }

        for (int i = 0; i < fsz; i++)
        {
            io_mesh_face tmp;
            memset(&tmp, 0, sizeof(io_mesh_face));
            obj_face& face = mesh->faces_[i];
            int v0 = face.v[0];
            int v1 = face.v[1];
            int v2 = face.v[2];

            //
            vector3 n0 = vector3(0, 0, 0);
            vector3 n1 = vector3(0, 0, 0);
            vector3 n2 = vector3(0, 0, 0);
            {
                int i0 = face.n[0];
                int i1 = face.n[1];
                int i2 = face.n[2];

                if (0 <= i0 && nsz > i0)
                {
                    n0 = mesh->normals_[i0];
                }
                if (0 <= i1 && nsz > i1)
                {
                    n1 = mesh->normals_[i1];
                }
                if (0 <= i2 && nsz > i2)
                {
                    n2 = mesh->normals_[i2];
                }
            }

            vector3 t0 = vector3(0, 0, 0);
            vector3 t1 = vector3(0, 0, 0);
            vector3 t2 = vector3(0, 0, 0);
            {
                int i0 = face.t[0];
                int i1 = face.t[1];
                int i2 = face.t[2];

                if (0 <= i0 && tsz > i0)
                {
                    t0 = mesh->textures_[i0];
                }
                if (0 <= i1 && tsz > i1)
                {
                    t1 = mesh->textures_[i1];
                }
                if (0 <= i2 && tsz > i2)
                {
                    t2 = mesh->textures_[i2];
                }
            }

            tmp.i0 = v0;
            tmp.i1 = v1;
            tmp.i2 = v2;

            conv(tmp.n0, n0);
            conv(tmp.n1, n1);
            conv(tmp.n2, n2);

            conv(tmp.uv0, v3v2(t0));
            conv(tmp.uv1, v3v2(t1));
            conv(tmp.uv2, v3v2(t2));

            if (!ms->set_face(i, &tmp)) return false;
        }
        return true;
    }

    //--------------------------------------------------------------
    //--------------------------------------------------------------

    obj_mesh_loader::obj_mesh_loader(const char* filename)
    {
        imp_ = new obj_mesh_loader_imp(filename);
    }

    obj_mesh_loader::~obj_mesh_loader()
    {
        delete imp_;
    }

    bool obj_mesh_loader::load(mesh_saver* ms) const
    {
        return imp_->load(ms);
    }
}
