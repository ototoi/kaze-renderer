#ifndef KAZE_OBJ_MESH_LOADER_H
#define KAZE_OBJ_MESH_LOADER_H

#include "mesh_loader.h"

namespace kaze
{

    class obj_mesh_loader_imp;
    class obj_mesh_loader : public mesh_loader
    {
    public:
        obj_mesh_loader(const char* filename);
        ~obj_mesh_loader();

    public:
        bool load(mesh_saver* ms) const;

    private:
        obj_mesh_loader_imp* imp_;
    };
}

#endif