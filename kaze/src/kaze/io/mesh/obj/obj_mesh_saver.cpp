#include "obj_mesh_saver.h"
#include "mesh_loader.h"
#include <stdio.h>
#include <time.h>
#include <string.h>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

namespace kaze
{

    class obj_mesh_saver_imp
    {
    public:
        obj_mesh_saver_imp(const char* filename);
        ~obj_mesh_saver_imp();
        bool save(const mesh_loader* ml);

    private:
        FILE* fp_;
    };
    //-------------------------------------------------

    obj_mesh_saver_imp::obj_mesh_saver_imp(const char* filename)
    {
        fp_ = fopen(filename, "wt");
    }

    obj_mesh_saver_imp::~obj_mesh_saver_imp()
    {
        if (fp_) fclose(fp_);
    }

    bool obj_mesh_saver_imp::save(const mesh_loader* ml)
    {
        if (!fp_) return false;
        FILE* fp = fp_;
        size_t vsz = 0;
        size_t fsz = 0;
        if (!ml->get_vertices(vsz)) return false;
        if (!ml->get_faces(fsz)) return false;

        fprintf(fp, "#\n");
        {
            char buffer[256];
            time_t t = time(NULL);
            struct tm* gt;
            gt = localtime(&t);
            sprintf(buffer, "%d/%d/%d %d:%d:%d", 1900 + gt->tm_year, gt->tm_mon, gt->tm_mday, gt->tm_hour, gt->tm_min, gt->tm_sec);
            fprintf(fp, "#created at %s\n", buffer);
        }

        fprintf(fp, "#\n");
        fprintf(fp, "g\n");
        for (size_t i = 0; i < vsz; i++)
        {
            io_mesh_vertex tmp;
            memset(&tmp, 0, sizeof(io_mesh_vertex));
            if (!ml->get_vertex(i, &tmp)) return false;
            fprintf(fp, "v %f %f %f\n", tmp.pos[0], tmp.pos[1], tmp.pos[2]);
        }
        for (size_t i = 0; i < fsz; i++)
        {
            io_mesh_face tmp;
            memset(&tmp, 0, sizeof(io_mesh_face));
            if (!ml->get_face(i, &tmp)) return false;
            fprintf(fp, "vn %f %f %f\n", tmp.n0[0], tmp.n0[1], tmp.n0[2]);
            fprintf(fp, "vn %f %f %f\n", tmp.n1[0], tmp.n1[1], tmp.n1[2]);
            fprintf(fp, "vn %f %f %f\n", tmp.n2[0], tmp.n2[1], tmp.n2[2]);
        }
        for (size_t i = 0; i < fsz; i++)
        {
            io_mesh_face tmp;
            memset(&tmp, 0, sizeof(io_mesh_face));
            if (!ml->get_face(i, &tmp)) return false;
            fprintf(fp, "vt %f %f %f\n", tmp.uv0[0], tmp.uv0[1], 0.0);
            fprintf(fp, "vt %f %f %f\n", tmp.uv1[0], tmp.uv1[1], 0.0);
            fprintf(fp, "vt %f %f %f\n", tmp.uv2[0], tmp.uv2[1], 0.0);
        }

        //-------------------------------------------------

        for (size_t i = 0; i < fsz; i++)
        {
            io_mesh_face tmp;
            memset(&tmp, 0, sizeof(io_mesh_face));
            if (!ml->get_face(i, &tmp)) return false;
            fprintf(fp, 
                "f %ld/%ld/%ld %ld/%ld/%ld %ld/%ld/%ld\n",
                tmp.i0 + 1, 3 * i + 1, 3 * i + 1,
                tmp.i1 + 1, 3 * i + 2, 3 * i + 2,
                tmp.i2 + 1, 3 * i + 3, 3 * i + 3
            );
        }

        return true;
    }

    //-------------------------------------------------

    obj_mesh_saver::obj_mesh_saver(const char* filename)
    {
        imp_ = new obj_mesh_saver_imp(filename);
    }

    obj_mesh_saver::~obj_mesh_saver()
    {
        delete imp_;
    }

    bool obj_mesh_saver::save(const mesh_loader* ml)
    {
        return imp_->save(ml);
        ;
    }
}
