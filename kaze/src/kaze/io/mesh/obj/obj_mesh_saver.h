#ifndef KAZE_OBJ_MESH_SAVER_H
#define KAZE_OBJ_MESH_SAVER_H

#include "mesh_saver.h"

namespace kaze
{

    class obj_mesh_saver_imp;
    class obj_mesh_saver : public mesh_saver
    {
    public:
        obj_mesh_saver(const char* filename);
        ~obj_mesh_saver();
        bool save(const mesh_loader* ml);

    private:
        obj_mesh_saver_imp* imp_;
    };
}

#endif
