#ifndef KAZE_TRANSFORMED_MESH_SAVER_H
#define KAZE_TRANSFORMED_MESH_SAVER_H

#include "mesh_saver.h"
#include "transformer.h"

namespace kaze
{

    class transformed_mesh_saver : public mesh_saver
    {
    public:
        transformed_mesh_saver(const std::shared_ptr<mesh_saver>& ms, const std::shared_ptr<transformer>& tr);

        bool save(const mesh_loader* ml);

    public:
        bool set_vertices(size_t sz);
        bool set_faces(size_t sz);
        bool set_vertex(size_t i, io_mesh_vertex* v);
        bool set_face(size_t i, io_mesh_face* f);

    private:
        std::shared_ptr<mesh_saver> ms_;
        std::shared_ptr<transformer> tr_;
    };
}

#endif
