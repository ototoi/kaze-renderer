#ifdef _MSC_VER
#pragma warning(disable : 4311)
#endif

#include "shadered_mesh_saver.h"
#include "shadered_mesh_loader.h"
#include "proxy_mesh_loader.h"

#include <vector>

namespace kaze
{

    typedef std::map<uint32_t, const shader*> itop_type;
    typedef std::map<const shader*, uint32_t> ptoi_type;

    shadered_mesh_saver::shadered_mesh_saver(const auto_count_ptr<mesh_saver>& ms, const std::vector<std::pair<uint32_t, const shader*> >& shaders)
        : ms_(ms)
    {
        size_t sz = shaders.size();
        for (size_t i = 0; i < sz; i++)
        {
            uint32_t id = shaders[i].first;
            const shader* s = shaders[i].second;
            itop_[id] = s;
            ptoi_[s] = id;
        }
    }

    shadered_mesh_saver::shadered_mesh_saver(
        const auto_count_ptr<mesh_saver>& ms,
        const std::map<uint32_t, const shader*>& itop,
        const std::map<const shader*, uint32_t>& ptoi) : ms_(ms), itop_(itop), ptoi_(ptoi)
    {
        ;
    }

    void shadered_mesh_saver::add(uint32_t id, const shader* s)
    {
        itop_[id] = s;
        ptoi_[s] = id;
    }

    bool shadered_mesh_saver::save(const mesh_loader* ml)
    {
        shadered_mesh_loader sml(new proxy_mesh_loader(ml), itop_, ptoi_);
        return ms_->save(&sml);
    }

    bool shadered_mesh_saver::set_vertices(size_t sz)
    {
        return ms_->set_vertices(sz);
    }

    bool shadered_mesh_saver::set_faces(size_t sz)
    {
        return ms_->set_faces(sz);
    }

    bool shadered_mesh_saver::set_vertex(size_t i, io_mesh_vertex* v)
    {
        return ms_->set_vertex(i, v);
    }

    bool shadered_mesh_saver::set_face(size_t i, io_mesh_face* f)
    {
        if (f->p_shader)
        {
            int ip = find_shader((const shader*)(f->p_shader));
            if (0 <= ip)
            {
                f->i_shader = (size_t)ip;
                return ms_->set_face(i, f);
            }
        }
        else
        {
            const shader* p = find_shader((uint32_t)f->i_shader);
            if (p)
            {
                f->p_shader = p;
                return ms_->set_face(i, f);
            }
        }

        return ms_->set_face(i, f);
    }

    int shadered_mesh_saver::find_shader(const shader* s) const
    {
        ptoi_type::const_iterator it = ptoi_.find(s);
        if (it != ptoi_.end())
        {
            return it->second;
        }
        return -1;
    }

    const shader* shadered_mesh_saver::find_shader(uint32_t i) const
    {
        itop_type::const_iterator it = itop_.find(i);
        if (it != itop_.end())
        {
            return it->second;
        }
        return NULL;
    }
}
