#ifndef KAZE_MINMAX_MESH_SAVER_H
#define KAZE_MINMAX_MESH_SAVER_H

#include "types.h"
#include "mesh_saver.h"
#include "mesh_loader.h"

namespace kaze
{

    class minmax_mesh_saver : public mesh_saver,
                              public mesh_loader
    {
    public:
        minmax_mesh_saver();
        size_t get_vertex_size() const;
        size_t get_face_size() const;
        vector3 get_min() const;
        vector3 get_max() const;

    public:
        vector3 min() const { return get_min(); }
        vector3 max() const { return get_max(); }
    public:
        bool load(mesh_saver* ms) const;
        bool get_vertices(size_t& sz) const
        {
            sz = get_vertex_size();
            return true;
        }
        bool get_faces(size_t& sz) const
        {
            sz = get_face_size();
            return true;
        }
        bool get_vertex(size_t i, io_mesh_vertex* v) const { return false; }
        bool get_face(size_t i, io_mesh_face* f) const { return false; }
    public:
        bool save(const mesh_loader* ml);
        bool set_vertices(size_t sz);
        bool set_faces(size_t sz);
        bool set_vertex(size_t i, io_mesh_vertex* v);
        bool set_face(size_t i, io_mesh_face* f);

    private:
        size_t vsz_;
        size_t fsz_;
        vector3 min_;
        vector3 max_;
    };
}

#endif
