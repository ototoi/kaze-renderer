#include "temporary_vn_mesh_loader.h"

namespace kaze
{

    temporary_vn_mesh_loader::temporary_vn_mesh_loader()
    {
        mv_.push_back(new temporary_mesh_loader());
        mv_.push_back(new temporary_mesh_loader());
    }
    temporary_vn_mesh_loader::~temporary_vn_mesh_loader()
    {
        delete mv_[0];
        delete mv_[1];
    }

    bool temporary_vn_mesh_loader::get_vertices(size_t& sz) const
    {
        return mv_[0]->get_vertices(sz);
    }
    bool temporary_vn_mesh_loader::get_faces(size_t& sz) const
    {
        return mv_[0]->get_faces(sz);
    }
    bool temporary_vn_mesh_loader::get_vertex(size_t i, io_mesh_vertex* v) const
    {
        if (!mv_[0]->get_vertex(i, v)) return false;
        return true;
    }
    bool temporary_vn_mesh_loader::get_face(size_t i, io_mesh_face* f) const
    {
        size_t i0 = 3 * i + 0;
        size_t i1 = 3 * i + 1;
        size_t i2 = 3 * i + 2;
        io_mesh_vertex v0;
        io_mesh_vertex v1;
        io_mesh_vertex v2;
        if (!mv_[1]->get_vertex(i0, &v0)) return false;
        if (!mv_[1]->get_vertex(i1, &v1)) return false;
        if (!mv_[1]->get_vertex(i2, &v2)) return false;

        memset(f, 0, sizeof(io_mesh_face));
        f->i0 = i0;
        f->i1 = i1;
        f->i2 = i2;
        memcpy(f->n0, v0.pos, sizeof(double) * 3);
        memcpy(f->n1, v1.pos, sizeof(double) * 3);
        memcpy(f->n2, v2.pos, sizeof(double) * 3);

        return true;
    }

    bool temporary_vn_mesh_loader::add_vertices(const std::vector<double>& vertices, const std::vector<double>& normals)
    {
        size_t vsz = vertices.size();
        size_t nsz = normals.size();
        if (vsz != nsz) return false;
        if (!mv_[0]->add_vertices(vertices)) return false;
        if (!mv_[1]->add_vertices(normals)) return false;

        return true;
    }

    bool temporary_vn_mesh_loader::add_vertices(const std::vector<vector3>& vertices, const std::vector<vector3>& normals)
    {
        size_t vsz = vertices.size();
        size_t nsz = normals.size();
        if (vsz != nsz) return false;
        if (!mv_[0]->add_vertices(vertices)) return false;
        if (!mv_[1]->add_vertices(normals)) return false;

        return true;
    }
}
