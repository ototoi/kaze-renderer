#include "smf_mesh_loader.h"
#include "mesh_saver.h"
#include "mesh_loader.h"
#include "smf_format.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

namespace kaze
{

    class smf_mesh_loader_imp
    {
    public:
        smf_mesh_loader_imp(const char* szFilePath);
        ~smf_mesh_loader_imp();
        bool load(mesh_saver* ms) const;

    protected:
        bool read_header(smf_header* pHeader) const;
        bool read_face(smf_face* face) const;

    private:
        FILE* fp_;
        smf_header header_;
    };

    bool smf_mesh_loader_imp::read_header(smf_header* pHeader) const
    {
        if (fseek(fp_, 0, SEEK_SET) != 0) return false;
        if (fread(pHeader, sizeof(smf_header), 1, fp_) != 1) return false;
        return true;
    }

    smf_mesh_loader_imp::smf_mesh_loader_imp(const char* szFilePath)
    {
        fp_ = 0;
        fp_ = fopen(szFilePath, "rb");
        if (!fp_) return;
        read_header(&header_);
    }

    smf_mesh_loader_imp::~smf_mesh_loader_imp()
    {
        if (fp_)
        {
            fclose(fp_);
        }
        fp_ = 0;
    }

    bool smf_mesh_loader_imp::read_face(smf_face* face) const
    {
        if (fread(face, sizeof(smf_face), 1, fp_) != 1) return false;
        return true;
    }

    static void convert(double* d, float* f)
    {
        d[0] = f[0];
        d[1] = f[1];
        d[2] = f[2];
    }

    bool smf_mesh_loader_imp::load(mesh_saver* ms) const
    {
        size_t i = 0;
        if (!fp_) return false;
        smf_header header;
        if (!read_header(&header)) return false;

        size_t fsz = 0;
        size_t vsz = 0;

        smf_face face;
        while (1)
        {
            if (!read_face(&face)) break;
            fsz++;
        }
        vsz = fsz * 3;

        if (!ms->set_vertices(vsz)) return false;
        if (!ms->set_faces(fsz)) return false;

        {
            io_mesh_vertex v;
            memset(&v, 0, sizeof(io_mesh_vertex));
            if (!read_header(&header)) return false;
            i = 0;
            while (1)
            {
                if (!read_face(&face)) break;
                for (int j = 0; j < 3; j++)
                {
                    v.pos[0] = face.vertices[3 * j + 0];
                    v.pos[1] = face.vertices[3 * j + 1];
                    v.pos[2] = face.vertices[3 * j + 2];
                    if (!ms->set_vertex(3 * i + j, &v)) return false;
                }
                i++;
            }
        }

        {
            io_mesh_face f;
            memset(&f, 0, sizeof(io_mesh_face));
            if (!read_header(&header)) return false;
            i = 0;
            while (1)
            {
                if (!read_face(&face)) break;
                f.i0 = 3 * i + 0;
                f.i1 = 3 * i + 1;
                f.i2 = 3 * i + 2;
                convert(f.n0, face.normals + 3 * 0);
                convert(f.n1, face.normals + 3 * 1);
                convert(f.n2, face.normals + 3 * 2);
                if (!ms->set_face(i, &f)) return false;
                i++;
            }
        }

        return true;
    }

    smf_mesh_loader::smf_mesh_loader(const char* szFilePath)
    {
        imp_ = new smf_mesh_loader_imp(szFilePath);
    }

    smf_mesh_loader::~smf_mesh_loader()
    {
        delete imp_;
    }

    bool smf_mesh_loader::load(mesh_saver* ms) const
    {
        return imp_->load(ms);
    }
}
