#include "smf_mesh_saver.h"
#include "smf_format.h"
#include <vector>
#include <stdio.h>
#include "types.h"

#include <string.h>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

namespace kaze
{

    class smf_mesh_saver_imp
    {
    public:
        smf_mesh_saver_imp(const char* szFilePath);
        ~smf_mesh_saver_imp();
        bool set_vertices(size_t sz);
        bool set_faces(size_t sz);
        bool set_vertex(size_t i, io_mesh_vertex* v);
        bool set_face(size_t i, io_mesh_face* f);

    private:
        FILE* fp_;
        std::vector<vector3> v_;
        std::vector<vector3> n_;
    };

    smf_mesh_saver_imp::smf_mesh_saver_imp(const char* szFilePath)
    {
        fp_ = 0;
        fp_ = fopen(szFilePath, "wb");
        if (!fp_) return;
        smf_header header;
        memset(&header, 0, sizeof(smf_header));
        memcpy(header.magic, "SMF", 4);
        fwrite(&header, sizeof(smf_header), 1, fp_);
    }

    smf_mesh_saver_imp::~smf_mesh_saver_imp()
    {
        v_.clear();
        n_.clear();
        if (fp_)
        {
            fflush(fp_);
            fclose(fp_);
        }
    }

    bool smf_mesh_saver_imp::set_vertices(size_t sz)
    {
        v_.resize(sz);
        n_.resize(sz);
        return true;
    }

    bool smf_mesh_saver_imp::set_faces(size_t sz)
    {
        return true;
    }

    bool smf_mesh_saver_imp::set_vertex(size_t i, io_mesh_vertex* v)
    {
        if (v_.size() <= i) return false;
        v_[i] = vector3(v->pos);
        return true;
    }

    bool smf_mesh_saver_imp::set_face(size_t i, io_mesh_face* f)
    {
        if (!fp_) return false;
        size_t indices[3];
        indices[0] = f->i0;
        indices[1] = f->i1;
        indices[2] = f->i2;
        if (v_.size() < indices[0]) return false;
        if (v_.size() < indices[1]) return false;
        if (v_.size() < indices[2]) return false;

        vector3 points[3];
        for (int i = 0; i < 3; i++)
        {
            points[i] = v_[indices[i]];
        }

        vector3 normals[3];
        normals[0] = vector3(f->n0);
        normals[1] = vector3(f->n1);
        normals[2] = vector3(f->n2);

        unsigned int nMat = (unsigned int)(f->i_shader);

        smf_face face;
        for (int i = 0; i < 3; i++)
        {
            face.vertices[3 * i + 0] = (float)points[i][0];
            face.vertices[3 * i + 1] = (float)points[i][1];
            face.vertices[3 * i + 2] = (float)points[i][2];
            face.normals[3 * i + 0] = (float)normals[i][0];
            face.normals[3 * i + 1] = (float)normals[i][1];
            face.normals[3 * i + 2] = (float)normals[i][2];
        }
        face.mat_id = nMat;

        if (fwrite(&face, sizeof(smf_face), 1, fp_) != 1) return false;
        return true;
    }

    smf_mesh_saver::smf_mesh_saver(const char* szFilePath)
    {
        imp_ = new smf_mesh_saver_imp(szFilePath);
    }

    smf_mesh_saver::~smf_mesh_saver()
    {
        delete imp_;
    }

    bool smf_mesh_saver::save(const mesh_loader* ml)
    {
        return mesh_saver::save(ml);
    }

    bool smf_mesh_saver::set_vertices(size_t sz)
    {
        return imp_->set_vertices(sz);
    }

    bool smf_mesh_saver::set_faces(size_t sz)
    {
        return imp_->set_faces(sz);
    }

    bool smf_mesh_saver::set_vertex(size_t i, io_mesh_vertex* v)
    {
        return imp_->set_vertex(i, v);
    }

    bool smf_mesh_saver::set_face(size_t i, io_mesh_face* f)
    {
        return imp_->set_face(i, f);
    }
}
