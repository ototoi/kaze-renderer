#ifndef KAZE_SMF_MESH_SAVER_H
#define KAZE_SMF_MESH_SAVER_H

#include "smf_format.h"
#include "mesh_saver.h"

namespace kaze
{

    class smf_mesh_saver_imp;
    class smf_mesh_saver : public mesh_saver
    {
    public:
        smf_mesh_saver(const char* szFilePath);
        ~smf_mesh_saver();

    public:
        bool save(const mesh_loader* ml);

    public:
        bool set_vertices(size_t sz);
        bool set_faces(size_t sz);
        bool set_vertex(size_t i, io_mesh_vertex* v);
        bool set_face(size_t i, io_mesh_face* f);

    private:
        smf_mesh_saver_imp* imp_;
    };
}

#endif
