#ifndef SMF_FORMAT_H
#define SMF_FORMAT_H

#include <stdio.h>
#include <stdlib.h>

struct smf_header
{
    char magic[4];
    unsigned long long face_num;
    char reserve[52];
};

struct smf_face
{
    float vertices[3 * 3];
    float normals[3 * 3];
    unsigned int mat_id;
};

#endif
