#ifndef KAZE_SMF_MESH_LOADER_H
#define KAZE_SMF_MESH_LOADER_H

#include "mesh_loader.h"

namespace kaze
{

    class smf_mesh_loader_imp;
    class smf_mesh_loader : public mesh_loader
    {
    public:
        smf_mesh_loader(const char* szFilePath);
        ~smf_mesh_loader();

    public:
        bool load(mesh_saver* ms) const;

    private:
        smf_mesh_loader_imp* imp_;
    };
}

#endif