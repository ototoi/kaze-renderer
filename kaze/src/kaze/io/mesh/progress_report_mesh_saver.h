#ifndef KAZE_PROGRESS_REPORT_MESH_SAVER_H
#define KAZE_PROGRESS_REPORT_MESH_SAVER_H

#include "mesh_saver.h"
#include <memory>

namespace kaze
{
    class progress_report_mesh_saver : public mesh_saver
    {
    public:
        progress_report_mesh_saver(const std::shared_ptr<mesh_saver>& ms);
        bool save(const mesh_loader* ml);
        bool set_vertices(size_t sz);
        bool set_faces(size_t sz);
        bool set_vertex(size_t i, io_mesh_vertex* v);
        bool set_face(size_t i, io_mesh_face* f);

    private:
        std::shared_ptr<mesh_saver> ms_;
        size_t vsz_;
        size_t fsz_;
    };
}

#endif
