#ifndef KAZE_MEMORY_MESH_IO_H
#define KAZE_MEMORY_MESH_IO_H

#include "mesh_loader.h"
#include "mesh_saver.h"

#include <vector>

namespace kaze
{

    class memory_mesh_io : public mesh_saver,
                           public mesh_loader
    {
    public:
        memory_mesh_io() {}

        bool get_vertices(size_t& sz) const;
        bool get_faces(size_t& sz) const;
        bool get_vertex(size_t i, io_mesh_vertex* v) const;
        bool get_face(size_t i, io_mesh_face* f) const;
        bool set_vertices(size_t sz);
        bool set_faces(size_t sz);
        bool set_vertex(size_t i, io_mesh_vertex* v);
        bool set_face(size_t i, io_mesh_face* f);

    public:
        std::vector<io_mesh_vertex> mv_;
        std::vector<io_mesh_face> mf_;
    };

    typedef memory_mesh_io memory_mesh_saver;
    typedef memory_mesh_io memory_mesh_loader;
}

#endif
