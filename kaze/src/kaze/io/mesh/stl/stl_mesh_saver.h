#ifndef KAZE_STL_MESH_SAVER_H
#define KAZE_STL_MESH_SAVER_H

#include "mesh_saver.h"

namespace kaze
{

    class stl_mesh_saver_imp;

    class stl_mesh_saver : public mesh_saver
    {
    public:
        stl_mesh_saver(const char* filename);
        ~stl_mesh_saver();

    public:
        bool save(const mesh_loader* ml);

    public:
        bool set_vertices(size_t sz);
        bool set_faces(size_t sz);
        bool set_vertex(size_t i, io_mesh_vertex* v);
        bool set_face(size_t i, io_mesh_face* f);

    private:
        stl_mesh_saver_imp* imp_;
    };
}

#endif
