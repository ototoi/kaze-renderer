#include "stl_mesh_saver.h"
#include "mesh_loader.h"

#include "types.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>
#include <vector>
#include <limits>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

namespace kaze
{
    namespace
    {
#pragma pack(push, 1)
        struct stl_face
        {
            float n[3];
            float p0[3];
            float p1[3];
            float p2[3];
            char res[2];
        };
#pragma pack(pop)
    }

    class stl_mesh_saver_imp
    {
    public:
        stl_mesh_saver_imp(const char* filename, bool bBin, const char* objectname);
        ~stl_mesh_saver_imp();

    public:
        bool save(const mesh_loader* ml);

    protected:
        bool save_binary(const mesh_loader* ml);
        bool save_ascii(const mesh_loader* ml);

    public:
        bool set_vertices(size_t sz);
        bool set_faces(size_t sz);
        bool set_vertex(size_t i, io_mesh_vertex* v);
        bool set_face(size_t i, io_mesh_face* f);

    private:
        bool bBinary;
        std::string filename_;
        std::string objectname_;
    };

    stl_mesh_saver_imp::stl_mesh_saver_imp(const char* filename, bool bBin, const char* objectname)
    {
        bBinary = bBin;
        filename_ = filename;
        objectname_ = objectname;
    }

    stl_mesh_saver_imp::~stl_mesh_saver_imp()
    {
        ; //
    }

    bool stl_mesh_saver_imp::save(const mesh_loader* ml)
    {
        if (bBinary)
        {
            return save_binary(ml);
        }
        else
        {
            return save_ascii(ml);
        }
    }

    static std::string get_objectname(const std::string& strFilePath)
    {
#if defined(_WIN32)
        //char drive[_MAX_DRIVE];
        //char dir[_MAX_DIR];
        char fname[_MAX_FNAME];
        //char ext[_MAX_EXT];
        _splitpath(strFilePath.c_str(), NULL, NULL, fname, NULL);
        return fname;
#else
        const char* fname = strrchr(strFilePath.c_str(), '\\');
        if (!fname) return strFilePath;
        fname++;
        char buffer[1024];
        strcpy(buffer, fname);
        char* ext = strrchr(buffer, '.');
        if (!ext)
        {
            return buffer;
        }
        *ext = '\0';
        return buffer;

#endif
    }
    
    namespace
    {
        class FPWrapper
        {
        public:
            FPWrapper(FILE* fp)
                :fp_(fp)
            {}
            ~FPWrapper()
            {
                fclose(fp_);
            }
            FILE* fp_;
        };
    }
    
    bool stl_mesh_saver_imp::save_binary(const mesh_loader* ml)
    {
        char header[80];
        unsigned int vsz;
        unsigned int fsz;

        FILE* fp = NULL;

        fp = fopen(filename_.c_str(), "wb");
        if (!fp) return false;
        FPWrapper fw(fp);

        size_t svsz;
        size_t sfsz;
        if (!ml->get_vertices(svsz)) return false;
        if (!ml->get_faces(sfsz)) return false;

        if (sfsz >= std::numeric_limits<unsigned int>::max() - 1) return false;

        vsz = (unsigned int)svsz;
        fsz = (unsigned int)sfsz;

        int nMAX = (int)objectname_.size();
        if (nMAX >= 79)
        {
            nMAX = 79;
        }

        memset(header, 0, 80);
        memcpy(header, objectname_.c_str(), nMAX);

        if (fwrite(header, 80, 1, fp) != 1) return false;

        if (fwrite((const char*)&fsz, 4, 1, fp) != 1) return false;

        std::vector<float> vv(vsz * 3);
        io_mesh_vertex v;
        for (unsigned int i = 0; i < vsz; i++)
        {
            memset(&v, 0, sizeof(io_mesh_vertex));
            if (!ml->get_vertex(i, &v)) return false;
            vv[3 * i + 0] = (float)v.x;
            vv[3 * i + 1] = (float)v.y;
            vv[3 * i + 2] = (float)v.z;
        }

        char face[50];
        stl_face* pface = (stl_face*)face;
        io_mesh_face f;
        for (unsigned int i = 0; i < fsz; i++)
        {
            memset(&f, 0, sizeof(io_mesh_face));
            if (!ml->get_face(i, &f)) return false;

            float p0[3];
            float p1[3];
            float p2[3];

            memcpy(p0, &vv[3 * f.i0], sizeof(float) * 3);
            memcpy(p1, &vv[3 * f.i1], sizeof(float) * 3);
            memcpy(p2, &vv[3 * f.i2], sizeof(float) * 3);

            vector3 v0(p0);
            vector3 v1(p1);
            vector3 v2(p2);

            vector3 nn = cross(v1 - v0, v2 - v0);
            if (nn.sqr_length() <= std::numeric_limits<float>::epsilon() * 32)
            {
                nn = vector3(0, 0, 0);
            }
            else
            {
                nn.normalize();
            }

            float n[3];
            n[0] = (float)nn[0];
            n[1] = (float)nn[1];
            n[2] = (float)nn[2];

            memcpy(pface->n, n, sizeof(float) * 3);
            memcpy(pface->p0, p0, sizeof(float) * 3);
            memcpy(pface->p1, p1, sizeof(float) * 3);
            memcpy(pface->p2, p2, sizeof(float) * 3);

            if (fwrite(face, 50, 1, fp) != 1)
            {
                return false;
            }
        }

        return true;
    }

    bool stl_mesh_saver_imp::save_ascii(const mesh_loader* ml)
    {
        return false;
    }

    bool stl_mesh_saver_imp::set_vertices(size_t sz)
    {
        return false;
    }

    bool stl_mesh_saver_imp::set_faces(size_t sz)
    {
        return false;
    }

    bool stl_mesh_saver_imp::set_vertex(size_t i, io_mesh_vertex* v)
    {
        return false;
    }

    bool stl_mesh_saver_imp::set_face(size_t i, io_mesh_face* f)
    {
        return false;
    }

    stl_mesh_saver::stl_mesh_saver(const char* filename)
    {
        std::string obj = get_objectname(filename);
        imp_ = new stl_mesh_saver_imp(filename, true, obj.c_str());
    }
    stl_mesh_saver::~stl_mesh_saver()
    {
        delete imp_;
    }

    bool stl_mesh_saver::save(const mesh_loader* ml)
    {
        return imp_->save(ml);
    }
    bool stl_mesh_saver::set_vertices(size_t sz)
    {
        return imp_->set_vertices(sz);
    }
    bool stl_mesh_saver::set_faces(size_t sz)
    {
        return imp_->set_faces(sz);
    }
    bool stl_mesh_saver::set_vertex(size_t i, io_mesh_vertex* v)
    {
        return imp_->set_vertex(i, v);
    }
    bool stl_mesh_saver::set_face(size_t i, io_mesh_face* f)
    {
        return imp_->set_face(i, f);
    }
}
