#include "stl_mesh_loader.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <vector>
//#include <sstream>

#ifdef _MSC_VER
#pragma warning(disable : 4311)
#pragma warning(disable : 4996)
#endif

namespace kaze
{

    namespace
    {
        struct stl_data
        {
            char name[1024];
            size_t face_size;
            float* faces;
        };

        const char* skip_space(const char* str)
        {
            const char* p = str;
            while (isspace(*p))
            {
                p++;
            }
            return p;
        }

        const char* kstrstr(const char* str, const char* search)
        {
            str = skip_space(str);
            const char* a = str;
            const char* b = search;
            while (*a && *b)
            {
                if (toupper(*a) != toupper(*b)) return NULL;
                a++;
                b++;
            }
            return str;
        }

        int stl_calc_facets(stl_data* data, FILE* fp)
        {
            char buffer[1024];
            size_t sz = 0;
            memset(buffer, 0, sizeof(char) * 1024);
            while (fgets(buffer, 1024, fp))
            {
                if (kstrstr(buffer, "facet "))
                {
                    sz++;
                }
            }
            if (sz == 0)
            {
                return -1;
            }
            else
            {
                if (data->faces) free(data->faces);
                if ((data->faces = (float*)malloc(sizeof(float) * 3 * 3 * sz)) == NULL)
                {
                    return -1;
                }
                else
                {
                    data->face_size = sz;
                    return 0;
                }
            }
        }

        int stl_convert_vertex(float v[3], const char* str)
        {
            if (sscanf(str, "%f %f %f", v + 0, v + 1, v + 2) == 3)
                return 0;
            else
                return -1;
            //std::stringstream ss(str);
            //ss >> v[0];
            //ss >> v[1];
            //ss >> v[2];
            //return 0;
        }

        int stl_load_vertex(float v[3], FILE* fp)
        {
            char buffer[1024];
            const char* p;
            memset(buffer, 0, sizeof(char) * 1024);
            fgets(buffer, 1024, fp);
            if ((p = kstrstr(buffer, "vertex ")))
            {
                p += strlen("vertex ");
                p = skip_space(p);

                return stl_convert_vertex(v, p);
            }
            else
            {
                return -1;
            }
        }

        int stl_load_facet(float f[3 * 3], FILE* fp)
        {
            char buffer[1024];
            float v[3];
            int count;

            memset(buffer, 0, sizeof(char) * 1024);
            while (fgets(buffer, 1024, fp))
            {
                if (kstrstr(buffer, "endloop"))
                {
                    break;
                }
                else if (kstrstr(buffer, "outer loop"))
                {
                    count = 0;
                    while (count < 3)
                    {
                        if (0 == stl_load_vertex(v, fp))
                        {
                            memcpy(f + count * 3, v, sizeof(float) * 3);
                            count++;
                        }
                    }
                    if (count == 3)
                    {
                        return 0;
                    }
                    else
                    {
                        return -1;
                    }
                }
            }

            return 0;
        }

        int stl_load_facets(stl_data* data, FILE* fp)
        {
            char buffer[1024];
            const char* p;
            float facet[3 * 3];
            size_t index;
            memset(buffer, 0, sizeof(char) * 1024);

            index = 0;
            while (fgets(buffer, 1024, fp))
            {
                if ((p = kstrstr(buffer, "endfacet")))
                {
                    continue;
                }
                else if ((p = kstrstr(buffer, "facet ")))
                {
                    if (0 == stl_load_facet(facet, fp))
                    {
                        memcpy(data->faces + index * 3 * 3, facet, sizeof(float) * 3 * 3);
                    }
                    else
                    {
                        return -1;
                    }
                    index++;
                }
            }

            return 0;
        }

        int stl_load_solid(stl_data* data, FILE* fp)
        {
            char buffer[1024];
            const char* p;
            //
            int save_point;
            int init = 0;

            memset(buffer, 0, sizeof(char) * 1024);
            while (fgets(buffer, 1024, fp))
            {
                if ((p = kstrstr(buffer, "solid ")))
                {
                    p += strlen("solid");
                    p = skip_space(p);
                    strncpy(data->name, p, 1024);

                    save_point = ftell(fp);

                    if (init == 0)
                    {
                        if (0 == stl_calc_facets(data, fp))
                        {
                            init = 1;
                            fseek(fp, 0, SEEK_SET);
                        }
                        else
                        {
                            return -1;
                        }
                    }
                    else
                    {
                        if (0 == stl_load_facets(data, fp))
                        {
                            return 0;
                        }
                        else
                        {
                            return -1;
                        }
                    }
                }
            }

            return -1;
        }

        //---------------------------------------------------------------
        int stl_load_ascii(stl_data* data, const char* str)
        {
            FILE* fp = NULL;

            fp = fopen(str, "rt");
            if (fp == NULL) return -1;

            data->faces = 0;

            return stl_load_solid(data, fp);
        }

        //---------------------------------------------------------------

        int stl_load_binary(stl_data* data, const char* str)
        {
            FILE* fp = NULL;

            char face[50];
            float* pf;
            float* pn;
            float* pp;

            char header[80];
            unsigned int fsz;

            fp = fopen(str, "rb");
            if (fp == NULL) return -1;

            memset(header, 0, 80);
            if (fread(header, 80, 1, fp) != 1)
            { 
                fclose(fp);
                return -1;
            }
            memcpy(data->name, header, 80);

            if (fread((char*)&fsz, 4, 1, fp) != 1)
            { 
                fclose(fp);
                return -1;
            }
            data->face_size = fsz;

            if ((data->faces = (float*)malloc(sizeof(float) * 3 * 3 * fsz)) == NULL)
            { 
                fclose(fp);
                return -1;
            }

            for (unsigned int i = 0; i < fsz; i++)
            {
                if (fread(face, 50, 1, fp) != 1)
                {
                    fclose(fp);
                    return -1;
                }
                pf = (float*)face;
                pn = pf + 0;
                pp = pf + 3;
                memcpy(data->faces + i * 3 * 3, pp, sizeof(float) * 3 * 3);
            }
            fclose(fp);
            return 0;
        }

        int stl_load(stl_data* data, const char* str, bool bBinary = false)
        {
            if (!bBinary)
            {
                return stl_load_ascii(data, str);
            }
            else
            {
                return stl_load_binary(data, str);
            }
        }

        int stl_free(stl_data* data)
        {
            if (data->faces) free(data->faces);
            return 0;
        }

        /*
		 * -1 file open error
		 * 0  ascii
		 * 1  binary
		 */
        int stl_judge_format(const char* str)
        {
            FILE* fp = NULL;
            char buffer[1024];
            int nRet = 1;

            fp = fopen(str, "rb");
            if (fp == NULL)
            {
                return -1;
            }

            if (fgets(buffer, 1024, fp))
            {
                buffer[1023] = 0;
                if (kstrstr(buffer, "solid"))
                { //ascii
                    nRet = 0;
                    goto FINAL;
                }
            }

        //binary
        FINAL:
            if (fp) fclose(fp);
            return nRet;
        }
    }

    //--------------------------------------------

    class stl_mesh_loader_imp
    {
    public:
        stl_mesh_loader_imp(const char* str);
        void initialize();

    public:
        bool load(mesh_saver* ms) const;

    public:
        bool get_vertices(size_t& sz) const;
        bool get_faces(size_t& sz) const;
        bool get_vertex(size_t i, io_mesh_vertex* v) const;
        bool get_face(size_t i, io_mesh_face* f) const;

    private:
        size_t fsz_;
        std::vector<float> vertices_;
    };

    stl_mesh_loader_imp::stl_mesh_loader_imp(const char* str)
    {
        int nRet = stl_judge_format(str);
        if (nRet == -1)
        {
            throw "stl_mesh_loader";
        }

        bool bBinary;
        if (nRet == 0) bBinary = false;
        if (nRet == 1) bBinary = true;

        stl_data data;
        data.face_size = 0;
        data.faces = 0;

        if (0 != stl_load(&data, str, bBinary))
        {
            stl_free(&data);
            throw "stl_mesh_loader";
        }
        fsz_ = data.face_size;
        vertices_.resize(fsz_ * 3 * 3);
        memcpy(&vertices_[0], data.faces, sizeof(float) * fsz_ * 3 * 3);
        stl_free(&data);
    }

    void stl_mesh_loader_imp::initialize()
    {
    }

    /*
	bool stl_mesh_loader_imp::load(mesh_saver* ms)const
	{
		return true;
	}
	*/

    bool stl_mesh_loader_imp::get_vertices(size_t& sz) const
    {
        sz = fsz_ * 3;
        return true;
    }

    bool stl_mesh_loader_imp::get_faces(size_t& sz) const
    {
        sz = fsz_;
        return true;
    }

    bool stl_mesh_loader_imp::get_vertex(size_t i, io_mesh_vertex* v) const
    {
        if (3 * i + 3 <= vertices_.size())
        {
            v->pos[0] = (double)vertices_[3 * i + 0];
            v->pos[1] = (double)vertices_[3 * i + 1];
            v->pos[2] = (double)vertices_[3 * i + 2];

            return true;
        }
        return false;
    }

    bool stl_mesh_loader_imp::get_face(size_t i, io_mesh_face* f) const
    {
        f->i0 = 3 * i + 0;
        f->i1 = 3 * i + 1;
        f->i2 = 3 * i + 2;

        return true;
    }

    //----------------------------------------------------

    stl_mesh_loader::stl_mesh_loader(const char* str)
    {
        imp_ = new stl_mesh_loader_imp(str);
    }

    stl_mesh_loader::~stl_mesh_loader()
    {
        delete imp_;
    }

    bool stl_mesh_loader::load(mesh_saver* ms) const
    {
        return mesh_loader::load(ms);
    }

    bool stl_mesh_loader::get_vertices(size_t& sz) const
    {
        return imp_->get_vertices(sz);
    }

    bool stl_mesh_loader::get_faces(size_t& sz) const
    {
        return imp_->get_faces(sz);
    }

    bool stl_mesh_loader::get_vertex(size_t i, io_mesh_vertex* v) const
    {
        return imp_->get_vertex(i, v);
    }

    bool stl_mesh_loader::get_face(size_t i, io_mesh_face* f) const
    {
        return imp_->get_face(i, f);
    }
}
