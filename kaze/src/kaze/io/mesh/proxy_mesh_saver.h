#ifndef KAZE_PROXY_MESH_SAVER_H
#define KAZE_PROXY_MESH_SAVER_H

#include "mesh_saver.h"

namespace kaze
{

    class proxy_mesh_saver : public mesh_saver
    {
    public:
        proxy_mesh_saver(mesh_saver* ms) : ms_(ms) {}
        ~proxy_mesh_saver() { /**/}
    public:
        bool save(const mesh_loader* ml) { return ms_->save(ml); }
    public:
        bool set_vertices(size_t sz) { return ms_->set_vertices(sz); }
        bool set_faces(size_t sz) { return ms_->set_faces(sz); }
        bool set_vertex(size_t i, io_mesh_vertex* v) { return ms_->set_vertex(i, v); }
        bool set_face(size_t i, io_mesh_face* f) { return ms_->set_face(i, f); }
    private:
        mesh_saver* ms_;
    };
}

#endif
