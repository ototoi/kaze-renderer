#include "transformed_mesh_loader.h"
#include "transformed_mesh_saver.h"

#include "proxy_mesh_saver.h"

namespace kaze
{

    namespace
    {
        void set(double d[3], const vector3& p)
        {
            d[0] = p[0];
            d[1] = p[1];
            d[2] = p[2];
        }
    }

    bool transformed_mesh_loader::load(mesh_saver* ms) const
    {
        transformed_mesh_saver ams(
            std::shared_ptr<mesh_saver>(new proxy_mesh_saver(ms)), //no delete
            tr_);
        return ml_->load(&ams);
    }

    transformed_mesh_loader::transformed_mesh_loader(const std::shared_ptr<mesh_loader>& ml, const std::shared_ptr<transformer>& tr)
    {
        this->add(ml, tr);
    }

    transformed_mesh_loader::transformed_mesh_loader(mesh_loader* ml, transformer* tr)
    {
        std::shared_ptr<mesh_loader> aml(ml);
        std::shared_ptr<transformer> atr(tr);
        this->add(aml, atr);
    }
    transformed_mesh_loader::transformed_mesh_loader(mesh_loader* ml, const matrix4& m)
    {
        std::shared_ptr<mesh_loader> aml(ml);
        std::shared_ptr<transformer> atr(new m4_transformer(m));
        this->add(aml, atr);
    }

    void transformed_mesh_loader::add(const std::shared_ptr<mesh_loader>& ml, const std::shared_ptr<transformer>& tr)
    {
        ml_ = ml;
        tr_ = tr;
    }

    bool transformed_mesh_loader::get_vertices(size_t& sz) const { return ml_->get_vertices(sz); }
    bool transformed_mesh_loader::get_faces(size_t& sz) const { return ml_->get_faces(sz); }
    bool transformed_mesh_loader::get_vertex(size_t i, io_mesh_vertex* v) const
    {
        if (ml_->get_vertex(i, v))
        {
            set(v->pos, tr_->transform_p(vector3(v->pos)));
            return true;
        }
        return false;
    }
    bool transformed_mesh_loader::get_face(size_t i, io_mesh_face* f) const
    {
        if (ml_->get_face(i, f))
        {
            set(f->n0, tr_->transform_n(vector3(f->n0)));
            set(f->n1, tr_->transform_n(vector3(f->n1)));
            set(f->n2, tr_->transform_n(vector3(f->n2)));
            return true;
        }
        return false;
    }
}
