#include "minmax_mesh_saver.h"
#include "values.h"

namespace kaze
{

    static inline vector3 far_vector()
    {
        static const real FAR = values::far();
        return vector3(FAR, FAR, FAR);
    }

    minmax_mesh_saver::minmax_mesh_saver()
        : vsz_(0), fsz_(0), min_(+far_vector()), max_(-far_vector())
    {
        ;
    }

    bool minmax_mesh_saver::load(mesh_saver* ms) const
    {
        if (!ms->set_vertices(vsz_)) return false;
        if (!ms->set_faces(fsz_)) return false;
        return false;
    }

    size_t minmax_mesh_saver::get_vertex_size() const
    {
        return vsz_;
    }

    size_t minmax_mesh_saver::get_face_size() const
    {
        return fsz_;
    }

    vector3 minmax_mesh_saver::get_min() const
    {
        return min_;
    }

    vector3 minmax_mesh_saver::get_max() const
    {
        return max_;
    }

    bool minmax_mesh_saver::save(const mesh_loader* ml)
    {
        vsz_ = 0;
        fsz_ = 0;
        min_ = +far_vector();
        max_ = -far_vector();
        return mesh_saver::save(ml);
    }

    bool minmax_mesh_saver::set_vertices(size_t sz)
    {
        vsz_ = sz;
        return true;
    }

    bool minmax_mesh_saver::set_faces(size_t sz)
    {
        fsz_ = sz;
        return true;
    }

    bool minmax_mesh_saver::set_vertex(size_t i, io_mesh_vertex* v)
    {
        for (int j = 0; j < 3; j++)
        {
            if (v->pos[j] < min_[j]) min_[j] = v->pos[j];
            if (v->pos[j] > max_[j]) max_[j] = v->pos[j];
        }
        return true;
    }

    bool minmax_mesh_saver::set_face(size_t i, io_mesh_face* f)
    {
        return true;
    }
}
