#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include <stdio.h>
#include <stdlib.h>

#include "kzm_mesh_loader.h"
#include "kzm_format.h"

#include "mesh_saver.h"

#include "types.h"

#include <limits>
#include <cstring>

using namespace std;

#define DEF_CPY(TYPE1, TYPE2)                            \
    static void copy_v3(TYPE1 out[3], const TYPE2 in[3]) \
    {                                                    \
        out[0] = (TYPE1)in[0];                           \
        out[1] = (TYPE1)in[1];                           \
        out[2] = (TYPE1)in[2];                           \
    }                                                    \
    static void copy_v2(TYPE1 out[2], const TYPE2 in[2]) \
    {                                                    \
        out[0] = (TYPE1)in[0];                           \
        out[1] = (TYPE1)in[1];                           \
    }

DEF_CPY(float, float)
DEF_CPY(float, double)
DEF_CPY(double, float)
DEF_CPY(double, double)

#undef DEF_CPY

namespace kaze
{

    class kzm_mesh_loader_imp
    {
    public:
        kzm_mesh_loader_imp(const char* fname);
        ~kzm_mesh_loader_imp();
        bool load(mesh_saver* setter) const;

    private:
        FILE* fp_;
    };

    kzm_mesh_loader_imp::kzm_mesh_loader_imp(const char* fname) : fp_(0)
    {
        FILE* fp = fopen(fname, "rb");
        if (!fp)
        {
            std::string s("kzm_mesh_loader:");
            s += fname;
            s += " open failed.";
            throw kzm_mesh_loader_exception(s.c_str());
        }
        fp_ = fp;
    }

    kzm_mesh_loader_imp::~kzm_mesh_loader_imp()
    {
        if (fp_) fclose(fp_);
    }

    bool kzm_mesh_loader_imp::load(mesh_saver* setter) const
    {
        //using namespace kzmega;
        if (fseek(fp_, 0, SEEK_SET) != 0) return false;

        kzmheader h;
        if (1 != fread(&h, sizeof(kzmheader), 1, fp_)) return false;
        if (h.vn > std::numeric_limits<size_t>::max()) return false;
        if (h.fn > std::numeric_limits<size_t>::max()) return false;

        size_t vn = (size_t)h.vn;
        size_t fn = (size_t)h.fn;

        if (!setter->set_vertices(vn)) return false;
        if (!setter->set_faces(fn)) return false;

        io_mesh_vertex ivert;
        kzmvertex overt;
        for (size_t i = 0; i < vn; i++)
        {
            //if(std::numeric_limits<size_t>::max()<i)return false;
            if (1 != fread(&overt, sizeof(kzmvertex), 1, fp_)) return false;

            copy_v3(ivert.pos, overt.pos);
            ivert.rgba = overt.attr;

            if (!setter->set_vertex((size_t)i, &ivert)) return false;
        }

        io_mesh_face iface;
        kzmface oface;
        for (size_t i = 0; i < fn; i++)
        {
            //if(std::numeric_limits<size_t>::max()<i)return false;
            if (1 != fread(&oface, sizeof(kzmface), 1, fp_)) return false;

            iface.i0 = (size_t)oface.i0;
            iface.i1 = (size_t)oface.i1;
            iface.i2 = (size_t)oface.i2;

            copy_v3(iface.n0, oface.n0);
            copy_v3(iface.n1, oface.n1);
            copy_v3(iface.n2, oface.n2);

            copy_v2(iface.uv0, oface.uv0);
            copy_v2(iface.uv1, oface.uv1);
            copy_v2(iface.uv2, oface.uv2);

            iface.i_shader = (size_t)oface.attr;

            if (!setter->set_face((size_t)i, &iface)) return false;
        }

        return true;
    }

    //-----------------------------------------------------
    kzm_mesh_loader::kzm_mesh_loader(const char* fname)
    {
        imp_ = new kzm_mesh_loader_imp(fname);
    }

    kzm_mesh_loader::~kzm_mesh_loader()
    {
        delete imp_;
    }

    bool kzm_mesh_loader::load(mesh_saver* setter) const
    {
        return imp_->load(setter);
    }
}
