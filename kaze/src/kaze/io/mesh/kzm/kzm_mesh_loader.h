#ifndef KAZE_KZM_MESH_LOADER
#define KAZE_KZM_MESH_LOADER

#include "mesh_loader.h"

#include <stdexcept>
#include <exception>
#include <string>

namespace kaze
{

    class kzm_mesh_loader_imp;

    class kzm_mesh_loader : public mesh_loader
    {
    public:
        kzm_mesh_loader(const char* fname);
        ~kzm_mesh_loader();

    public:
        bool load(mesh_saver* setter) const;

    private:
        kzm_mesh_loader_imp* imp_;
    };

    class kzm_mesh_loader_exception : public std::exception
    {
    public:
        kzm_mesh_loader_exception(const char* str) : str_(str) {}
        ~kzm_mesh_loader_exception() throw() { ; }
        const char* what() const throw() { return str_.c_str(); }
    private:
        std::string str_;
    };
}

#endif
