#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include <stdio.h>
#include <stdlib.h>

#include "kzm_mesh_saver.h"

#include <cstdio>
#include <iostream>
#include <cstring>
#include <string>

#include "kzm_format.h"

using namespace std;

namespace kaze
{

#define DEF_CPY(TYPE1, TYPE2)                            \
    static void copy_v3(TYPE1 out[3], const TYPE2 in[3]) \
    {                                                    \
        out[0] = (TYPE1)in[0];                           \
        out[1] = (TYPE1)in[1];                           \
        out[2] = (TYPE1)in[2];                           \
    }                                                    \
    static void copy_v2(TYPE1 out[2], const TYPE2 in[2]) \
    {                                                    \
        out[0] = (TYPE1)in[0];                           \
        out[1] = (TYPE1)in[1];                           \
    }

    DEF_CPY(float, float)
    DEF_CPY(float, double)
    DEF_CPY(double, float)
    DEF_CPY(double, double)

#undef DEF_CPY

    class kzm_mesh_saver_imp
    {
    public:
        kzm_mesh_saver_imp(const char* filename);
        ~kzm_mesh_saver_imp();
        bool set_vertices(size_t sz);
        bool set_faces(size_t sz);
        bool set_vertex(size_t i, io_mesh_vertex* v);
        bool set_face(size_t i, io_mesh_face* f);

    protected:
        bool write_header();

    private:
        FILE* fp_;
        int mode_;
        size_t vn_;
        size_t max_v_;
        size_t fn_;
        size_t max_f_;
    };

    kzm_mesh_saver_imp::kzm_mesh_saver_imp(const char* filename)
    {
        FILE* fp = fopen(filename, "wb");
        if (!fp) throw kzm_mesh_saver_exception("kzm_mesh_saver open failed.");
        fp_ = fp;
        vn_ = 0;
        fn_ = 0;
        max_v_ = 0;
        max_f_ = 0;
        mode_ = 0;
    }
    kzm_mesh_saver_imp::~kzm_mesh_saver_imp()
    {
        fclose(fp_);
    }
    bool kzm_mesh_saver_imp::set_vertices(size_t sz)
    {
        max_v_ = sz;
        mode_ |= 1;
        if ((mode_ & 3) == 3)
        {
            if (!write_header()) return false;
            mode_ &= ~3;
        }
        return true;
    }
    bool kzm_mesh_saver_imp::set_faces(size_t sz)
    {
        max_f_ = sz;
        mode_ |= 2;
        if ((mode_ & 3) == 3)
        {
            if (!write_header()) return false;
            mode_ &= ~3;
        }
        return true;
    }

    bool kzm_mesh_saver_imp::write_header()
    {
        kzmheader tmp;
        memset(&tmp, 0, sizeof(kzmheader));
        strcpy(tmp.magic, "KZM");
        tmp.vn = (kzmsize_t)max_v_;
        tmp.fn = (kzmsize_t)max_f_;

        bool bRet = (fwrite(&tmp, sizeof(kzmheader), 1, fp_) == 1);

        return bRet;
    }

    bool kzm_mesh_saver_imp::set_vertex(size_t i, io_mesh_vertex* v)
    {
        if (vn_ != i) return false;
        vn_++;

        kzmvertex tmp;
        memset(&tmp, 0, sizeof(kzmvertex));
        copy_v3(tmp.pos, v->pos);
        tmp.attr = (kzmattr_t)v->rgba;
        ; //(kzmattr_t)v->i_shader;
        //tmp.attr   = 0;//(kzmattr_t)v->i_shader;

        bool bRet = (fwrite(&tmp, sizeof(kzmvertex), 1, fp_) == 1);

        return bRet;
    }

    bool kzm_mesh_saver_imp::set_face(size_t i, io_mesh_face* f)
    {
        if (fn_ != i) return false;
        fn_++;

        kzmface tmp;
        memset(&tmp, 0, sizeof(kzmface));
        tmp.i0 = (kzmsize_t)f->i0;
        tmp.i1 = (kzmsize_t)f->i1;
        tmp.i2 = (kzmsize_t)f->i2;
        copy_v3(tmp.n0, f->n0);
        copy_v3(tmp.n1, f->n1);
        copy_v3(tmp.n2, f->n2);
        copy_v2(tmp.uv0, f->uv0);
        copy_v2(tmp.uv1, f->uv1);
        copy_v2(tmp.uv2, f->uv2);
        tmp.attr = (kzmattr_t)f->i_shader;

        bool bRet = (fwrite(&tmp, sizeof(kzmface), 1, fp_) == 1);

        return bRet;
    }

    //
    //----------------------------------------------------------------------------

    kzm_mesh_saver::kzm_mesh_saver(const char* filename) { imp_ = new kzm_mesh_saver_imp(filename); }
    kzm_mesh_saver::~kzm_mesh_saver() { delete imp_; }

    bool kzm_mesh_saver::set_vertices(size_t sz) { return imp_->set_vertices(sz); }
    bool kzm_mesh_saver::set_faces(size_t sz) { return imp_->set_faces(sz); }
    bool kzm_mesh_saver::set_vertex(size_t i, io_mesh_vertex* v) { return imp_->set_vertex(i, v); }
    bool kzm_mesh_saver::set_face(size_t i, io_mesh_face* f) { return imp_->set_face(i, f); }
}
