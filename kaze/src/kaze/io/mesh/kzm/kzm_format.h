#ifndef KAZE_KZM_FORMAT_H
#define KAZE_KZM_FORMAT_H

typedef double kzmreal_t;
typedef unsigned int kzmsize_t;
typedef unsigned int kzmattr_t;

/*__attribute__ ((packed));*/

#define FLTDUMMY
/*
#define FLTDUMMY float dummy_float
*/
/*
#ifdef __GNUC__
#define ATTR_PACK __attribute__ ((packed))
#else
#define ATTR_PACK
#endif
*/

#pragma pack(push, 4)
typedef struct kzmvertex_
{
    kzmreal_t pos[3];
    kzmattr_t attr;
    /*kzmattr_t dmmy;//8byte align*/
} kzmvertex;

typedef struct kzmface_
{
    kzmsize_t i0;
    kzmsize_t i1;
    kzmsize_t i2;

    kzmattr_t attr;

    kzmreal_t n0[3];
    kzmreal_t n1[3];
    kzmreal_t n2[3];

    kzmreal_t uv0[2];
    kzmreal_t uv1[2];
    kzmreal_t uv2[2];
    /*3*3+3*2 = 9+6= 15*/
    FLTDUMMY
} kzmface;

#pragma pack(pop)

#undef FLTDUMMY

enum KZM_FORMAT_FLAG
{
    KZM_FORMAT_VERTEX_FLOAT = 1,
    KZM_FORMAT_FACE_NONORMAL = 8,
    KZM_FORMAT_FACE_NOUV = 16,
    KZM_FORMAT_ATTR_SHADER = 32
};

#pragma pack(push, 4)
typedef struct kzmheader_
{
    union
    {
        struct
        {
            char magic[4]; /*'K','Z','M','\0'*/
            kzmsize_t vn;
            kzmsize_t fn;
            unsigned int version;
            unsigned int flag;
        };
        char reserve[256];
    };
} kzmheader;
#pragma pack(pop)

#endif