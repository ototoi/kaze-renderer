#ifndef KAZE_KZM_SAVER_H
#define KAZE_KZM_SAVER_H

#include "mesh_saver.h"

#include <stdexcept>
#include <string>

namespace kaze
{
    /*
		mesh_lopader->file
	*/
    class kzm_mesh_saver_imp;

    class kzm_mesh_saver : public mesh_saver
    {
    public:
        kzm_mesh_saver(const char* filename);
        ~kzm_mesh_saver();

    public:
        bool set_vertices(size_t sz);
        bool set_faces(size_t sz);
        bool set_vertex(size_t i, io_mesh_vertex* v);
        bool set_face(size_t i, io_mesh_face* f);

    private:
        kzm_mesh_saver_imp* imp_;
    };

    class kzm_mesh_saver_exception : public std::exception
    {
    public:
        kzm_mesh_saver_exception(const char* str) : str_(str) {}
        ~kzm_mesh_saver_exception() throw() {}
        const char* what() const throw() { return str_.c_str(); }
    private:
        std::string str_;
    };
}

#endif
