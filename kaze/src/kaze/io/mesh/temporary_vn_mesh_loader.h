#ifndef KAZE_TEMPORARY_VN_MESH_LOADER_H
#define KAZE_TEMPORARY_VN_MESH_LOADER_H

#include "temporary_mesh_loader.h"
#include <vector>

namespace kaze
{

    class temporary_vn_mesh_loader : public mesh_loader
    {
    public:
        temporary_vn_mesh_loader();
        ~temporary_vn_mesh_loader();
        bool get_vertices(size_t& sz) const;
        bool get_faces(size_t& sz) const;
        bool get_vertex(size_t i, io_mesh_vertex* v) const;
        bool get_face(size_t i, io_mesh_face* f) const;

    public:
        bool add_vertices(const std::vector<double>& vertices, const std::vector<double>& normals);
        bool add_vertices(const std::vector<vector3>& vertices, const std::vector<vector3>& normals);

    private:
        std::vector<temporary_mesh_loader*> mv_;
    };
}

#endif
