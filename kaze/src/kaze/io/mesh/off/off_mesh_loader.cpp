#include "off_mesh_loader.h"

#include "mesh_saver.h"

#include <fstream>
#include <string>
#include <sstream>

#include <cstring>

using namespace std;

namespace kaze
{

    class off_mesh_loader_imp
    {
    public:
        off_mesh_loader_imp(const char* str);
        bool load(mesh_saver* ms) const;

    protected:
        std::string str_;
    };

    off_mesh_loader_imp::off_mesh_loader_imp(const char* str)
    {
        str_ = str;
    }

    static bool get_size(size_t& vsz, size_t& fsz, size_t& nsz, const std::string& s)
    {
        std::stringstream ss(s);
        ss >> vsz >> fsz >> nsz;
        return !ss.bad();
    }

    static bool get_v(double e[3], const std::string& s)
    {
        std::stringstream ss(s);
        ss >> e[0] >> e[1] >> e[2];
        return !ss.bad();
    }

    static bool get_f(int& nsz, size_t& i0, size_t& i1, size_t& i2, const std::string& s)
    {
        std::stringstream ss(s);
        ss >> nsz >> i0 >> i1 >> i2;
        return !ss.bad();
    }

    bool off_mesh_loader_imp::load(mesh_saver* ms) const
    {
        std::ifstream ifs(str_.c_str());

        if (!ifs) return false;

        std::string line;
        if (!std::getline(ifs, line)) return false;
        if (line != "OFF") return false;

        size_t vsz;
        size_t fsz;
        size_t nsz;

        if (!std::getline(ifs, line)) return false;
        if (!get_size(vsz, fsz, nsz, line)) return false;

        if (!ms->set_vertices(vsz)) return false;
        if (!ms->set_faces(fsz)) return false;

        for (size_t i = 0; i < vsz; i++)
        {
            io_mesh_vertex tmp;
            memset(&tmp, 0, sizeof(io_mesh_vertex));
            if (!std::getline(ifs, line)) return false;
            if (!get_v(tmp.pos, line)) return false;
            if (!ms->set_vertex(i, &tmp)) return false;
        }

        for (size_t i = 0; i < fsz; i++)
        {
            io_mesh_face tmp;
            memset(&tmp, 0, sizeof(io_mesh_face));
            if (!std::getline(ifs, line)) return false;
            int nsz;
            if (!get_f(nsz, tmp.i0, tmp.i1, tmp.i2, line)) return false;
            if (tmp.i0 >= vsz) return false;
            if (tmp.i1 >= vsz) return false;
            if (tmp.i2 >= vsz) return false;
            if (nsz != 3) return false;
            if (!ms->set_face(i, &tmp)) return false;
        }
        return true;
    }

    //----------------------------------------------

    off_mesh_loader::off_mesh_loader(const char* str)
    {
        imp_ = new off_mesh_loader_imp(str);
    }

    off_mesh_loader::~off_mesh_loader()
    {
        delete imp_;
    }

    bool off_mesh_loader::load(mesh_saver* ms) const
    {
        return imp_->load(ms);
    }

    bool off_mesh_loader::get_vertices(size_t& sz) const { return false; }
    bool off_mesh_loader::get_faces(size_t& sz) const { return false; }
    bool off_mesh_loader::get_vertex(size_t i, io_mesh_vertex* v) const { return false; }
    bool off_mesh_loader::get_face(size_t i, io_mesh_face* f) const { return false; }
}
