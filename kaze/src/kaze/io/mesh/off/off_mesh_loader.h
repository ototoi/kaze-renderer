#ifndef KAZE_OFF_MESH_LOADER_H
#define KAZE_OFF_MESH_LOADER_H

#include "mesh_loader.h"

namespace kaze
{

    class off_mesh_loader_imp;

    class off_mesh_loader : public mesh_loader
    {
    public:
        off_mesh_loader(const char* str);
        ~off_mesh_loader();

    public:
        bool load(mesh_saver* ms) const;

    public:
        bool get_vertices(size_t& sz) const;
        bool get_faces(size_t& sz) const;
        bool get_vertex(size_t i, io_mesh_vertex* v) const;
        bool get_face(size_t i, io_mesh_face* f) const;

    private:
        off_mesh_loader_imp* imp_;
    };
}

#endif