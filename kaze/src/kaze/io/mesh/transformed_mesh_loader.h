#ifndef KAZE_TRANSFORMED_MESH_LOADER_H
#define KAZE_TRANSFORMED_MESH_LOADER_H

#include "mesh_loader.h"
#include "transformer.h"

namespace kaze
{

    class transformed_mesh_loader : public mesh_loader
    {
    public:
        transformed_mesh_loader(const std::shared_ptr<mesh_loader>& ml, const std::shared_ptr<transformer>& tr);
        transformed_mesh_loader(mesh_loader* ml, transformer* tr);
        transformed_mesh_loader(mesh_loader* ml, const matrix4& m);

        bool load(mesh_saver* ms) const;

    public:
        bool get_vertices(size_t& sz) const;
        bool get_faces(size_t& sz) const;
        bool get_vertex(size_t i, io_mesh_vertex* v) const;
        bool get_face(size_t i, io_mesh_face* f) const;

    protected:
        void add(const std::shared_ptr<mesh_loader>& ml, const std::shared_ptr<transformer>& tr);

    private:
        std::shared_ptr<mesh_loader> ml_;
        std::shared_ptr<transformer> tr_;
    };
}

#endif