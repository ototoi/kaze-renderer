#ifndef KAZE_TEMPORARY_MESH_LOADER_H
#define KAZE_TEMPORARY_MESH_LOADER_H

#include "types.h"
#include "mesh_loader.h"

#include <vector>

namespace kaze
{

    class temporary_mesh_loader_imp;
    class temporary_mesh_loader : public mesh_loader
    {
    public:
        temporary_mesh_loader();
        ~temporary_mesh_loader();

    public:
        bool get_vertices(size_t& sz) const;
        bool get_faces(size_t& sz) const;
        bool get_vertex(size_t i, io_mesh_vertex* v) const;
        bool get_face(size_t i, io_mesh_face* f) const;

    public:
        bool add_vertices(const std::vector<double>& vertices);
        bool add_vertices(const std::vector<vector3>& vertices);

    private:
        temporary_mesh_loader& operator=(const temporary_mesh_loader& imp); //no implement
    private:
        temporary_mesh_loader_imp* imp_;
    };
}

#endif