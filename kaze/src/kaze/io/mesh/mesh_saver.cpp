#include "mesh_saver.h"
#include "mesh_loader.h"

#include <string.h>

namespace kaze
{

    bool mesh_saver::save(const mesh_loader* ml)
    {
        const mesh_loader* in = ml;
        mesh_saver* out = this;
        size_t vsz, fsz;
        if (!in->get_vertices(vsz)) return false;
        if (!in->get_faces(fsz)) return false;
        if (!out->set_vertices(vsz)) return false;
        if (!out->set_faces(fsz)) return false;

        for (size_t i = 0; i < vsz; i++)
        {
            io_mesh_vertex v;
            memset(&v, 0, sizeof(io_mesh_vertex));
            if (!in->get_vertex(i, &v)) return false;
            if (!out->set_vertex(i, &v)) return false;
        }

        for (size_t i = 0; i < fsz; i++)
        {
            io_mesh_face f;
            memset(&f, 0, sizeof(io_mesh_face));
            if (!in->get_face(i, &f)) return false;
            if (!out->set_face(i, &f)) return false;
        }

        return true;
    }

    bool mesh_saver::set_vertices(size_t sz) { return true; }
    bool mesh_saver::set_faces(size_t sz) { return true; }
    bool mesh_saver::set_vertex(size_t i, io_mesh_vertex* v) { return true; }
    bool mesh_saver::set_face(size_t i, io_mesh_face* f) { return true; }
}