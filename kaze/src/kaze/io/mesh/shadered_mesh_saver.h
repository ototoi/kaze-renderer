#ifndef KAZE_SHADERED_MESH_SAVER_H
#define KAZE_SHADERED_MESH_SAVER_H

#include "mesh_saver.h"
#include "shader.h"
#include "count_ptr.hpp"

#include <vector>
#include <map>

namespace kaze
{
    class shadered_mesh_saver : public mesh_saver
    {
    public:
        shadered_mesh_saver(const auto_count_ptr<mesh_saver>& ms, const std::vector<std::pair<uint32_t, const shader*> >& shaders);
        shadered_mesh_saver(
            const auto_count_ptr<mesh_saver>& ms,
            const std::map<uint32_t, const shader*>& itop,
            const std::map<const shader*, uint32_t>& ptoi);

        void add(uint32_t id, const shader* s);

        bool save(const mesh_loader* ml);

    public:
        bool set_vertices(size_t sz);
        bool set_faces(size_t sz);
        bool set_vertex(size_t i, io_mesh_vertex* v);
        bool set_face(size_t i, io_mesh_face* f);

    protected:
        int find_shader(const shader* s) const;
        const shader* find_shader(uint32_t i) const;

    private:
        std::shared_ptr<mesh_saver> ms_;
        std::map<uint32_t, const shader*> itop_;
        std::map<const shader*, uint32_t> ptoi_;
    };
}

#endif
