#ifdef _MSC_VER
#pragma warning(disable : 4311)
#endif

#include "shadered_mesh_loader.h"
#include "shadered_mesh_saver.h"
#include "proxy_mesh_saver.h"

#include <vector>

namespace kaze
{

    typedef std::map<uint32_t, const shader*> itop_type;
    typedef std::map<const shader*, uint32_t> ptoi_type;

    shadered_mesh_loader::shadered_mesh_loader(const auto_count_ptr<mesh_loader>& ml, const std::vector<std::pair<uint32_t, const shader*> >& shaders)
        : ml_(ml)
    {
        size_t sz = shaders.size();
        for (size_t i = 0; i < sz; i++)
        {
            uint32_t id = shaders[i].first;
            const shader* s = shaders[i].second;
            itop_[id] = s;
            ptoi_[s] = id;
        }
    }

    shadered_mesh_loader::shadered_mesh_loader(
        const auto_count_ptr<mesh_loader>& ml,
        const std::map<uint32_t, const shader*>& itop,
        const std::map<const shader*, uint32_t>& ptoi) : ml_(ml), itop_(itop), ptoi_(ptoi)
    {
        ; //
    }

    void shadered_mesh_loader::add(uint32_t id, const shader* s)
    {
        itop_[id] = s;
        ptoi_[s] = id;
    }

    bool shadered_mesh_loader::load(mesh_saver* ms) const
    {
        shadered_mesh_saver sms(new proxy_mesh_saver(ms), itop_, ptoi_);
        return ml_->load(&sms);
    }

    bool shadered_mesh_loader::get_vertices(size_t& sz) const { return ml_->get_vertices(sz); }
    bool shadered_mesh_loader::get_faces(size_t& sz) const { return ml_->get_faces(sz); }
    bool shadered_mesh_loader::get_vertex(size_t i, io_mesh_vertex* v) const { return ml_->get_vertex(i, v); }
    bool shadered_mesh_loader::get_face(size_t i, io_mesh_face* f) const
    {
        if (ml_->get_face(i, f))
        {
            if (f->p_shader)
            {
                int ip = find_shader((const shader*)(f->p_shader));
                if (0 <= ip)
                {
                    f->i_shader = (size_t)i;
                }
            }
            else
            {
                const shader* p = find_shader((uint32_t)f->i_shader);
                if (p)
                {
                    f->p_shader = p;
                }
            }
            return true;
        }

        return false;
    }

    int shadered_mesh_loader::find_shader(const shader* s) const
    {
        ptoi_type::const_iterator it = ptoi_.find(s);
        if (it != ptoi_.end())
        {
            return it->second;
        }
        return -1;
    }

    const shader* shadered_mesh_loader::find_shader(uint32_t i) const
    {
        itop_type::const_iterator it = itop_.find(i);
        if (it != itop_.end())
        {
            return it->second;
        }
        return NULL;
    }
}
