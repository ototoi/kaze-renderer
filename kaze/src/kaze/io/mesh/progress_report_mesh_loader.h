#ifndef KAZE_PROGRESS_REPORT_MESH_LOADER_H
#define KAZE_PROGRESS_REPORT_MESH_LOADER_H

#include "mesh_loader.h"
#include <memory>

namespace kaze
{

    class progress_report_mesh_loader : public mesh_loader
    {
    public:
        progress_report_mesh_loader(const std::shared_ptr<mesh_loader>& ms);

    public:
        bool load(mesh_saver* ms) const;

    public:
        bool get_vertices(size_t& sz) const;
        bool get_faces(size_t& sz) const;
        bool get_vertex(size_t i, io_mesh_vertex* v) const;
        bool get_face(size_t i, io_mesh_face* f) const;

    private:
        std::shared_ptr<mesh_loader> ml_;
        mutable size_t vsz_;
        mutable size_t fsz_;
    };
}

#endif
