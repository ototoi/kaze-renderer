#include "progress_report_mesh_saver.h"
#include "progress_report_mesh_loader.h"

#include "proxy_mesh_loader.h"

#include "logger.h"

namespace kaze
{

    progress_report_mesh_saver::progress_report_mesh_saver(const std::shared_ptr<mesh_saver>& ms)
        : ms_(ms), vsz_(0), fsz_(0)
    {
    }

    bool progress_report_mesh_saver::save(const mesh_loader* ml)
    {
        vsz_ = 0;
        fsz_ = 0;

        progress_report_mesh_loader tm(std::shared_ptr<mesh_loader>(new proxy_mesh_loader(ml)));
        if (ms_->save(&tm))
        {
            print_progress("mesh loading...:%3.3f%%", 100.0);
            return true;
        }

        return false;
    }

    bool progress_report_mesh_saver::set_vertices(size_t sz)
    {
        if (ms_->set_vertices(sz))
        {
            vsz_ = sz;
            return true;
        }
        return false;
    }

    bool progress_report_mesh_saver::set_faces(size_t sz)
    {
        if (ms_->set_faces(sz))
        {
            fsz_ = sz;
            return true;
        }
        return false;
    }

    bool progress_report_mesh_saver::set_vertex(size_t i, io_mesh_vertex* v)
    {
        if (ms_->set_vertex(i, v))
        {
            if ((i & 1023) == 0)
            {
                if (vsz_) print_progress("mesh loading...:%3.3f%%", double(i) * 50.0 / vsz_);
            }
            return true;
        }
        return false;
    }

    bool progress_report_mesh_saver::set_face(size_t i, io_mesh_face* f)
    {
        if (ms_->set_face(i, f))
        {
            if ((i & 1023) == 0)
            {
                if (fsz_) print_progress("mesh loading...:%3.3f%%", 50.0 + double(i) * 50.0 / fsz_);
            }
            return true;
        }
        return false;
    }
}
