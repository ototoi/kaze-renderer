
#include "ply_mesh_loader.h"
#include "mesh_saver.h"

//----------------
#include "rply.h"
//----------------

#include "logger.h"
#include "timer.h"

#include <stdexcept>
#include <iostream>

#include <cstring>
#include <cassert>

namespace kaze
{

    struct ply_v_input
    {
        mesh_saver* ms;

        double vtx[3];

        unsigned int idx;
        int cnt;
        float isz;
    };

    struct ply_f_input
    {
        mesh_saver* ms;

        unsigned long fce[3];

        unsigned int idx;
        int cnt;
        float isz;
    };

    extern "C" {

    static int vertex_cb(p_ply_argument argument)
    {
        int32_t plane;

        ply_v_input* input;
        unsigned int idx;
        mesh_saver* ms;

        ply_get_argument_user_data(argument, (void**)&input, &plane);
        idx = input->idx;
        ms = input->ms;

        if (plane >= 3) return 0;

        if (plane == 2)
        {
            input->vtx[2] = (double)ply_get_argument_value(argument);

            io_mesh_vertex vv;
            memset(&vv, 0, sizeof(io_mesh_vertex));
            vv.pos[0] = input->vtx[0];
            vv.pos[1] = input->vtx[1];
            vv.pos[2] = input->vtx[2];
            ms->set_vertex(idx, &vv);

            input->idx = idx + 1;
        }
        else
        {
            input->vtx[plane] = (double)ply_get_argument_value(argument);
        }

        return 1;
    }

    static int face_cb(p_ply_argument argument)
    {
        int32_t length, plane;
        ply_f_input* input;
        unsigned int idx;
        mesh_saver* ms;

        ply_get_argument_user_data(argument, (void**)&input, NULL);
        idx = input->idx;
        ms = input->ms;

        ply_get_argument_property(argument, NULL, &length, &plane);

        if (plane >= 3) return 0;

        io_mesh_face ff;

        switch (plane)
        {
        case 0:
            input->fce[0] = (unsigned long)ply_get_argument_value(argument);
            break;
        case 1:
            input->fce[1] = (unsigned long)ply_get_argument_value(argument);
            break;
        case 2:
            input->fce[2] = (unsigned long)ply_get_argument_value(argument);

            memset(&ff, 0, sizeof(io_mesh_face));
            ff.i0 = input->fce[0];
            ff.i1 = input->fce[1];
            ff.i2 = input->fce[2];
            ms->set_face(idx, &ff);

            input->idx = idx + 1;
        default:
            break;
        }

        return 1;
    }

    static void error_cb(const char* message)
    {
        print_log(message);
    }
    }

    static int32_t ply_get_element_size(p_ply ply, const char* name)
    {
        p_ply_element element;
        int32_t size;
        const char* p_name;

        assert(ply && name);

        element = 0; //
        while ((element = ply_get_next_element(ply, element)))
        { //get next element!
            ply_get_element_info(element, &p_name, &size);
            if (!std::strcmp(p_name, name)) return size;
        }

        return 0;
    }

    class ply_mesh_loader_exception : public std::exception
    {
    public:
        ply_mesh_loader_exception(const char* str) : str_(str) {}
        virtual ~ply_mesh_loader_exception() throw() {}
        const char* what() const throw() { return str_.c_str(); } //"virtual" overloaded
    private:
        std::string str_;
    };

#define PLM_MKSTR(str) (std::string("ply_mesh_loader:") + str).c_str()

    class ply_mesh_loader_imp
    {
    public:
        ply_mesh_loader_imp(const char* name)
        {
            ply_ = ply_open(name, error_cb);
            if (!ply_)
            {
                //throw std::bad_alloc();
                throw ply_mesh_loader_exception(PLM_MKSTR(name));
            }
            if (!ply_read_header(ply_))
            {
                ply_close(ply_);

                //throw std::domain_error(name);
            }

            unsigned long nsz = 0;

            if (!(nsz = ply_get_element_size(ply_, "face")))
            {
                ply_close(ply_);
                //throw std::bad_alloc();
                throw ply_mesh_loader_exception(PLM_MKSTR(name));
            }
            nfaces_ = nsz;

            //nfaces_    = ply_set_read_cb(ply_, "face", "vertex_indices", NULL, NULL, 0);
            if (!(nsz = ply_get_element_size(ply_, "vertex")))
            {
                ply_close(ply_);
                //throw std::bad_alloc();
                throw ply_mesh_loader_exception(PLM_MKSTR(name));
            }
            nvertices_ = nsz;
            //nvertices_ = ply_set_read_cb(ply_, "vertex", "x", NULL, NULL, 0);
        }

        ~ply_mesh_loader_imp()
        {
            ply_close(ply_);
        }

        size_t get_f() const { return nfaces_; }
        size_t get_v() const { return nvertices_; }

        bool load(mesh_saver* ms) const
        {
            size_t fsize = this->get_f();
            size_t vsize = this->get_v();

            //print_log("face size = %d\n",fsize);
            //print_log("vrtx size = %d\n",vsize);

            ms->set_vertices(vsize);
            ms->set_faces(fsize);

            ply_v_input vinput;
            ply_f_input finput;

            vinput.ms = ms;
            vinput.idx = 0;
            vinput.cnt = 0;
            vinput.isz = 100.0f / vsize;
            ply_set_read_cb(ply_, "vertex", "x", vertex_cb, &vinput, 0);
            ply_set_read_cb(ply_, "vertex", "y", vertex_cb, &vinput, 1);
            ply_set_read_cb(ply_, "vertex", "z", vertex_cb, &vinput, 2);

            finput.ms = ms;
            finput.idx = 0;
            finput.cnt = 0;
            finput.isz = 100.0f / fsize;
            ply_set_read_cb(ply_, "face", "vertex_indices", face_cb, &finput, 0);

            if (!ply_read(ply_)) return false;
            return true;
        }

    private:
        p_ply ply_;
        size_t nfaces_;
        size_t nvertices_;
    };

#undef PLM_MKSTR

    ply_mesh_loader::ply_mesh_loader(const char* name)
    {
        p_imp_ = new ply_mesh_loader_imp(name);
    }

    ply_mesh_loader::~ply_mesh_loader()
    {
        delete p_imp_;
    }

    bool ply_mesh_loader::load(mesh_saver* ms) const
    {
        return p_imp_->load(ms);
    }

    bool ply_mesh_loader::get_vertices(size_t& sz) const
    {
        sz = p_imp_->get_v();
        return true;
    }

    bool ply_mesh_loader::get_faces(size_t& sz) const
    {
        sz = p_imp_->get_f();
        return true;
    }

    bool ply_mesh_loader::get_vertex(size_t i, io_mesh_vertex* v) const
    {
        //no imp
        return false;
    }

    bool ply_mesh_loader::get_face(size_t i, io_mesh_face* f) const
    {
        //no imp;
        return false;
    }
}
