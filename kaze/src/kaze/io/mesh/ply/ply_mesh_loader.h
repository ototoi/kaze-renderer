#ifndef KAZE_PLY_MESH_LOADER_H
#define KAZE_PLY_MESH_LOADER_H

#include "mesh_loader.h"

namespace kaze
{

    class ply_mesh_loader_imp;

    class ply_mesh_loader : public mesh_loader
    {
    public:
        ply_mesh_loader(const char* name);
        ~ply_mesh_loader();

    public:
        bool load(mesh_saver* mc) const;

    public:
        bool get_vertices(size_t& sz) const;
        bool get_faces(size_t& sz) const;
        bool get_vertex(size_t i, io_mesh_vertex* v) const;
        bool get_face(size_t i, io_mesh_face* f) const;

    private:
        ply_mesh_loader_imp* p_imp_;
    };
}

#endif
