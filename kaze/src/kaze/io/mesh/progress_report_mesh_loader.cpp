#include "progress_report_mesh_loader.h"
#include "progress_report_mesh_saver.h"

#include "proxy_mesh_saver.h"

#include "logger.h"

namespace kaze
{

    progress_report_mesh_loader::progress_report_mesh_loader(const std::shared_ptr<mesh_loader>& ml)
        : ml_(ml), vsz_(0), fsz_(0)
    {
    }

    bool progress_report_mesh_loader::load(mesh_saver* ms) const
    {
        vsz_ = 0;
        fsz_ = 0;

        progress_report_mesh_saver tm(std::shared_ptr<mesh_saver>(new proxy_mesh_saver(ms)));
        if (ml_->load(&tm))
        {
            print_progress("mesh loading...:%3.3f%%", 100.0);
            return true;
        }

        return false;
    }

    bool progress_report_mesh_loader::get_vertices(size_t& sz) const
    {
        if (ml_->get_vertices(sz))
        {
            vsz_ = sz;
            return true;
        }
        return false;
    }

    bool progress_report_mesh_loader::get_faces(size_t& sz) const
    {
        if (ml_->get_faces(sz))
        {
            fsz_ = sz;
            return true;
        }
        return false;
    }

    bool progress_report_mesh_loader::get_vertex(size_t i, io_mesh_vertex* v) const
    {
        if (ml_->get_vertex(i, v))
        {
            if ((i & 1023) == 0)
            {
                if (vsz_) print_progress("mesh loading...:%3.3f%%", double(i) * 50.0 / vsz_);
            }
            return true;
        }
        return false;
    }

    bool progress_report_mesh_loader::get_face(size_t i, io_mesh_face* f) const
    {
        if (ml_->get_face(i, f))
        {
            if ((i & 1023) == 0)
            {
                if (fsz_) print_progress("mesh loading...:%3.3f%%", 50.0 + double(i) * 50.0 / fsz_);
            }
            return true;
        }
        return false;
    }
}
