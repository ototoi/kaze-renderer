#include "mesh_loader.h"
#include "mesh_saver.h"

#include <string.h>

namespace kaze
{

    bool mesh_loader::load(mesh_saver* ms) const
    {
        const mesh_loader* in = this;
        mesh_saver* out = ms;
        size_t vsz, fsz;
        if (!in->get_vertices(vsz)) return false;
        if (!in->get_faces(fsz)) return false;
        if (!out->set_vertices(vsz)) return false;
        if (!out->set_faces(fsz)) return false;

        for (size_t i = 0; i < vsz; i++)
        {
            io_mesh_vertex v;
            memset(&v, 0, sizeof(io_mesh_vertex));
            if (!in->get_vertex(i, &v)) return false;
            if (!out->set_vertex(i, &v)) return false;
        }

        for (size_t i = 0; i < fsz; i++)
        {
            io_mesh_face f;
            memset(&f, 0, sizeof(io_mesh_face));
            if (!in->get_face(i, &f)) return false;
            if (!out->set_face(i, &f)) return false;
        }

        return true;
    }

    bool mesh_loader::get_vertices(size_t& sz) const { return true; }
    bool mesh_loader::get_faces(size_t& sz) const { return true; }
    bool mesh_loader::get_vertex(size_t i, io_mesh_vertex* v) const { return true; }
    bool mesh_loader::get_face(size_t i, io_mesh_face* f) const { return true; }
}