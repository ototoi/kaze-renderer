#ifndef KAZE_SHADERED_MESH_LOADER_H
#define KAZE_SHADERED_MESH_LOADER_H

#include "mesh_loader.h"
#include "shader.h"
#include "count_ptr.hpp"

#include <vector>
#include <map>

namespace kaze
{
    class shadered_mesh_loader : public mesh_loader
    {
    public:
        shadered_mesh_loader(const auto_count_ptr<mesh_loader>& ml, const std::vector<std::pair<uint32_t, const shader*> >& shaders);
        shadered_mesh_loader(
            const auto_count_ptr<mesh_loader>& ml,
            const std::map<uint32_t, const shader*>& itop,
            const std::map<const shader*, uint32_t>& ptoi);

        void add(uint32_t id, const shader* s);

        bool load(mesh_saver* ms) const;

    public:
        bool get_vertices(size_t& sz) const;
        bool get_faces(size_t& sz) const;
        bool get_vertex(size_t i, io_mesh_vertex* v) const;
        bool get_face(size_t i, io_mesh_face* f) const;

    protected:
        int find_shader(const shader* s) const;
        const shader* find_shader(uint32_t i) const;

    private:
        std::shared_ptr<mesh_loader> ml_;
        std::map<uint32_t, const shader*> itop_;
        std::map<const shader*, uint32_t> ptoi_;
    };
}

#endif
