#ifndef KAZE_MESH_LOADER_H
#define KAZE_MESH_LOADER_H

#include "io_mesh.h"

namespace kaze
{

    class mesh_saver;

    class mesh_loader
    {
    public:
        virtual bool load(mesh_saver* ms) const;

    public:
        virtual bool get_vertices(size_t& sz) const;
        virtual bool get_faces(size_t& sz) const;
        virtual bool get_vertex(size_t i, io_mesh_vertex* v) const;
        virtual bool get_face(size_t i, io_mesh_face* f) const;

    public:
        virtual ~mesh_loader() {}
    };
}

#endif