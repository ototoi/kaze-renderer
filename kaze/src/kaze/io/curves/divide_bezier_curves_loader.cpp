#include "divide_bezier_curves_loader.h"
#include "bezier.h"
#include <limits>

namespace kaze
{
    static const real FAR = std::numeric_limits<real>::max();
    static const real EPS = std::numeric_limits<real>::epsilon();

    static void get_minmax(vector3& min, vector3& max, const io_bezier_curve_strand& str)
    {
        min = vector3(+FAR, +FAR, +FAR);
        max = vector3(-FAR, -FAR, -FAR);

        size_t psz = str.points.size();
        for (size_t j = 0; j < psz; j++)
        {
            vector3 p = vector3(str.points[j].position);
            real r = (real)str.points[j].radius;
            for (int k = 0; k < 3; k++)
            {
                if (min[k] > p[k] - r) min[k] = p[k] - r;
                if (max[k] < p[k] + r) max[k] = p[k] + r;
            }
        }
        min -= vector3(EPS, EPS, EPS);
        max += vector3(EPS, EPS, EPS);
    }

    static void get_minmax(vector3& min, vector3& max, const bezier_curves_loader& loader)
    {
        min = vector3(+FAR, +FAR, +FAR);
        max = vector3(-FAR, -FAR, -FAR);
        size_t sz = 0;
        loader.get_strands(sz);
        io_bezier_curve_strand str;
        for (size_t i = 0; i < sz; i++)
        {
            if (loader.get_strand(i, &str))
            {
                vector3 cmin, cmax;
                get_minmax(cmin, cmax, str);
                for (int k = 0; k < 3; k++)
                {
                    if (min[k] > cmin[k]) min[k] = cmin[k];
                    if (max[k] < cmax[k]) max[k] = cmax[k];
                }
            }
        }
    }

    static int GetDivideNum(int a, int b)
    {
        return (int)ceil(floor((float)b) / a);
    }

    template <class T>
    static void DivideBezier(std::vector<T>& out, const std::vector<T>& in, int nDiv)
    {
        int nOrder = (int)in.size();
        out.resize((nOrder - 1) * nDiv + 1);
        std::vector<T> tmp0(nOrder);
        std::vector<T> tmp1(nOrder);
        if (nDiv == 2)
        {
            bezier_split(&tmp0[0], &tmp1[0], &in[0], nOrder, 0.5f);
            for (int k = 0; k < nOrder; k++)
            {
                out[k] = tmp0[k];
                out[nOrder - 1 + k] = tmp1[k];
            }
        }
        else
        {
            for (int k = 0; k < nDiv - 1; k++)
            {
                float t1 = float(k + 1) / nDiv;
                if (k == 0)
                {
                    bezier_split(&out[(nOrder - 1) * k], &out[(nOrder - 1) * (k + 1)], &in[0], nOrder, t1);
                }
                else
                {
                    float t0 = float(k) / nDiv;
                    bezier_split(&tmp0[0], &out[(nOrder - 1) * (k + 1)], &in[0], nOrder, t1);
                    bezier_split(&tmp1[0], &out[(nOrder - 1) * k], &tmp0[0], nOrder, t0 / t1);
                }
            }
        }
    }

    static void Copy(float f[3], const vector3f& v)
    {
        for (int k = 0; k < 3; k++)
        {
            f[k] = v[k];
        }
    }

    static void DivideStrand(io_bezier_curve_strand& out, const io_bezier_curve_strand& in, int nDiv)
    {
        out.points.clear();
        int nOrder = in.order;
        int psz = (int)in.points.size();
        int ssz = (psz - 1) / (in.order - 1);
        if (ssz >= nDiv)
        {
            out = in;
        }
        else
        {
            int nK = GetDivideNum(ssz, nDiv); //

            out.points.reserve(ssz * nK * (nOrder - 1) + 1);

            for (int j = 0; j < ssz; j++)
            {
                std::vector<vector3f> pos(nOrder);
                std::vector<vector3f> col1(nOrder);
                std::vector<vector3f> col2(nOrder);
                std::vector<vector3f> col3(nOrder);
                std::vector<float> rad(nOrder);

                std::vector<vector3f> nor(nOrder);

                for (int k = 0; k < nOrder; k++)
                {
                    pos[k] = vector3f(in.points[j * (nOrder - 1) + k].position);
                    col1[k] = vector3f(in.points[j * (nOrder - 1) + k].color1);
                    col2[k] = vector3f(in.points[j * (nOrder - 1) + k].color2);
                    col3[k] = vector3f(in.points[j * (nOrder - 1) + k].color3);
                    rad[k] = in.points[j * (nOrder - 1) + k].radius;
                    nor[k] = vector3f(in.points[j * (nOrder - 1) + k].normal);
                }
                std::vector<vector3f> opos((nOrder - 1) * nK + 1);
                std::vector<vector3f> ocol1((nOrder - 1) * nK + 1);
                std::vector<vector3f> ocol2((nOrder - 1) * nK + 1);
                std::vector<vector3f> ocol3((nOrder - 1) * nK + 1);
                std::vector<float> orad((nOrder - 1) * nK + 1);
                std::vector<vector3f> onor((nOrder - 1) * nK + 1);

                DivideBezier(opos, pos, nK);
                DivideBezier(ocol1, col1, nK);
                DivideBezier(ocol2, col2, nK);
                DivideBezier(ocol3, col3, nK);
                DivideBezier(orad, rad, nK);
                DivideBezier(onor, nor, nK);

                if (in.has_normal)
                {
                    for (size_t i = 0; i < onor.size(); i++)
                    {
                        onor[i].normalize();
                    }
                }

                int osz = (int)orad.size();

                if (j != ssz - 1)
                {
                    osz--; //last point is first point of next sgement. Do not output.
                }
                for (int k = 0; k < osz; k++)
                {
                    io_curve_point pt;
                    Copy(pt.position, opos[k]);
                    Copy(pt.color1, ocol1[k]);
                    Copy(pt.color2, ocol2[k]);
                    Copy(pt.color3, ocol3[k]);
                    pt.radius = orad[k];
                    Copy(pt.normal, onor[k]);
                    out.points.push_back(pt);
                }
                assert(out.points.size() == (ssz * nK * (nOrder - 1) + 1));
            }
        }
    }

    //AUTO
    divide_bezier_curves_loader::divide_bezier_curves_loader(const auto_count_ptr<bezier_curves_loader>& loader)
        : loader_(loader)
    {
        bAuto_ = true;
        nDiv_ = 64;
        if (!loader_->get_strands(sz_))
        {
            sz_ = 0;
        }
        get_minmax(min_, max_, *loader_);
    }

    bool divide_bezier_curves_loader::get_strands(size_t& sz) const
    {
        return loader_->get_strands(sz);
    }

    bool divide_bezier_curves_loader::get_strand(size_t i, io_bezier_curve_strand* str) const
    {
        if (sz_ >= 16)
        {
            io_bezier_curve_strand tmp;
            if (!loader_->get_strand(i, &tmp)) return false;
            if (bAuto_)
            {
                vector3 cmin, cmax;
                get_minmax(cmin, cmax, tmp);
                real fArea[3];
                for (int k = 0; k < 3; k++)
                {
                    fArea[k] = (cmax[k] - cmin[k]) / (max_[k] - min_[k]);
                }
                int nDiv[3] = {1, 1, 1};
                for (int k = 0; k < 3; k++)
                {
                    nDiv[k] = std::max<int>(1, std::min<int>(nDiv_, (int)floor(fabs(nDiv_ * fArea[k]))));
                }
                int div = std::max<int>(nDiv[0], std::max<int>(nDiv[1], nDiv[2]));
                DivideStrand(*str, tmp, div);
                str->order = tmp.order;
                return true;
            }
            return true;
        }
        else
        {
            if (sz_)
            {
                return loader_->get_strand(i, str);
            }
            else
            {
                return false;
            }
        }
    }
}