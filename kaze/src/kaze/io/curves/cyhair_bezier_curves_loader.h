#ifndef KAZE_CYHAIR_BEZIER_CURVES_LOADER_H
#define KAZE_CYHAIR_BEZIER_CURVES_LOADER_H

#include "curves/bezier_curves_loader.h"

namespace kaze
{

    class cyHair_loader;
    class cyhair_bezier_curves_loader : public bezier_curves_loader
    {
    public:
        cyhair_bezier_curves_loader(const char* szHairFile);
        ~cyhair_bezier_curves_loader();

    public:
        bool get_strands(size_t& sz) const;
        bool get_strand(size_t i, io_bezier_curve_strand* str) const;

    private:
        cyHair_loader* l_;
    };

    typedef cyhair_bezier_curves_loader cyhair_hair_loader;
}

#endif
