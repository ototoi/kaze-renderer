#ifndef KAZE_DIVIDE_BEZIER_CURVES_LOADER_H
#define KAZE_DIVIDE_BEZIER_CURVES_LOADER_H

#include "count_ptr.hpp"
#include "bezier_curves_loader.h"

namespace kaze
{

    class divide_bezier_curves_loader : public bezier_curves_loader
    {
    public:
        divide_bezier_curves_loader(const auto_count_ptr<bezier_curves_loader>& loader);
        virtual bool get_strands(size_t& sz) const;
        virtual bool get_strand(size_t i, io_bezier_curve_strand* str) const;

    protected:
        bool bAuto_;
        size_t sz_;
        int nDiv_;
        vector3 min_;
        vector3 max_;
        std::shared_ptr<bezier_curves_loader> loader_;
    };
}

#endif
