#ifndef KAZE_IO_CURVES_H
#define KAZE_IO_CURVES_H

#include "types.h"
#include "color.h"

#include <vector>

namespace kaze
{

    struct io_curve_point
    {
        float position[3];
        float radius;
        float color1[3];
        float color2[3];
        float color3[3];
        float normal[3];
    };

    struct io_bezier_curve_strand
    {
        int order;
        std::vector<io_curve_point> points;
        bool has_normal;
    };
}

#endif