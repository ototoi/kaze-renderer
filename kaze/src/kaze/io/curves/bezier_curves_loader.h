#ifndef KAZE_BEZIER_CURVES_LOADER_H
#define KAZE_BEZIER_CURVES_LOADER_H

#include "types.h"
#include "io_curves.h"

namespace kaze
{

    class bezier_curves_loader
    {
    public:
        virtual ~bezier_curves_loader() {}
        virtual bool get_strands(size_t& sz) const = 0;
        virtual bool get_strand(size_t i, io_bezier_curve_strand* str) const = 0;
    };
}

#endif
