#include "cyhair_bezier_curves_loader.h"
#include "./cyhair/cyhair_loader.h"

namespace kaze
{

    template <class T>
    struct zero_value
    {
        static inline T value() { return T(0); }
    };

    template <>
    struct zero_value<vector2f>
    {
        static inline vector2f value() { return vector2f(0, 0); }
    };

    template <>
    struct zero_value<vector3f>
    {
        static inline vector3f value() { return vector3f(0, 0, 0); }
    };

    template <class T>
    static void mul_matrix(T out[4], const matrix4f& mat, const T pt[4])
    {
        for (int i = 0; i < 4; i++)
        {
            out[i] = mat[i][0] * pt[0] + mat[i][1] * pt[1] + mat[i][2] * pt[2] + mat[i][3] * pt[3];
        }
    }

    template <class T>
    static void catmull_rom2bezier(std::vector<T>& out, const std::vector<T>& in)
    {
#if 0
        size_t sz = in.size();
        out.clear();
        out.reserve(sz*4);
        for(size_t i = 0;i<sz-1;i++)
        {
            for(int j = 0;j<4;j++){
                real t = real(j)/4.0;
                out.push_back(in[i]*(1-t) + in[i+1]*t);
            } 
        }
        //return;
#else
        static const matrix4f toC2B = float(1.0 / 6.0) * matrix4f(
                                                             0, 6, 0, 0,
                                                             -1, 6, 1, 0,
                                                             0, 1, 6, -1,
                                                             0, 0, 6, 0);
        static const matrix4f toC2B0 = float(1.0 / 6.0) * matrix4f(
                                                              0, 6, 0, 0,
                                                              0, 3, 4, -1,
                                                              0, 1, 6, -1,
                                                              0, 0, 6, 0);
        static const matrix4f toC2B1 = float(1.0 / 6.0) * matrix4f(
                                                              0, 6, 0, 0,
                                                              -1, 6, 1, 0,
                                                              -1, 4, 3, 0,
                                                              0, 0, 6, 0);
        out.clear();
        T P[4];
        T Q[4];
        int sz = (int)in.size();
        out.reserve(3 * sz - 2); //

        if (sz == 2)
        {
            Q[0] = in[0];
            Q[1] = in[0] * float(2.0 / 3.0) + in[1] * float(1.0 / 3.0);
            Q[2] = in[0] * float(1.0 / 3.0) + in[1] * float(2.0 / 3.0);
            Q[3] = in[1];

            out.push_back(Q[0]);
            out.push_back(Q[1]);
            out.push_back(Q[2]);
            out.push_back(Q[3]);
        }
        else
        {
            {
                int i = 0;
                P[0] = zero_value<T>::value();
                P[1] = in[i + 0];
                P[2] = in[i + 1];
                P[3] = in[i + 2];
                mul_matrix(Q, toC2B0, P);
                out.push_back(Q[0]);
                out.push_back(Q[1]);
                out.push_back(Q[2]);
            }
            {
                for (int i = 1; i < sz - 2; i++)
                {
                    P[0] = in[i - 1];
                    P[1] = in[i + 0];
                    P[2] = in[i + 1];
                    P[3] = in[i + 2];
                    mul_matrix(Q, toC2B, P);
                    out.push_back(Q[0]);
                    out.push_back(Q[1]);
                    out.push_back(Q[2]);
                }
            }
            {
                int i = sz - 2;
                P[0] = in[i - 1];
                P[1] = in[i + 0];
                P[2] = in[i + 1];
                P[3] = zero_value<T>::value();
                mul_matrix(Q, toC2B1, P);
                out.push_back(Q[0]);
                out.push_back(Q[1]);
                out.push_back(Q[2]);
                out.push_back(Q[3]);
            }
        }
#endif
    }

    cyhair_bezier_curves_loader::cyhair_bezier_curves_loader(const char* szHairFile)
    {
        l_ = new cyhair_loader(szHairFile);
    }

    cyhair_bezier_curves_loader::~cyhair_bezier_curves_loader()
    {
        delete l_;
    }

    bool cyhair_bezier_curves_loader::get_strands(size_t& sz) const
    {
        if (!l_->is_valid()) return false;
        sz = l_->get_strands();
        return true;
    }

    static bool convert_(
        cyhair_strand& from,
        std::vector<vector3f>& points,
        std::vector<float>& radius,
        std::vector<color3f>& col1,
        std::vector<color3f>& col2)
    {
        size_t psz = from.points_array.size();
        if (from.color_array.size() != psz) return false;
        if (from.thickness_array.size() != psz) return false;
        if (from.transparency_array.size() != psz) return false;

        points.resize(psz);
        radius.resize(psz);
        col1.resize(psz);
        col2.resize(psz);

        for (size_t i = 0; i < psz; i++)
        {
            radius[i] = from.thickness_array[i] * 0.5f;
            float opacity = 1.0f - from.transparency_array[i];

            points[i] = from.points_array[i];
            col1[i] = from.color_array[i];
            col2[i] = vector3f(opacity, opacity, opacity);
        }
        return true;
    }

    static bool convert(cyhair_strand& from, io_bezier_curve_strand& to)
    {
        std::vector<vector3f> points;
        std::vector<float> radius;
        std::vector<color3f> col1;
        std::vector<color3f> col2;
        if (!convert_(from, points, radius, col1, col2)) return false;

        //
        //Convert with catmull-rom
        //
        std::vector<vector3f> points_b;
        std::vector<float> radius_b;
        std::vector<color3f> col1_b;
        std::vector<color3f> col2_b;

        catmull_rom2bezier(points_b, points);
        catmull_rom2bezier(radius_b, radius);
        catmull_rom2bezier(col1_b, col1);
        catmull_rom2bezier(col2_b, col2);

        size_t psz = points_b.size();
        to.points.resize(psz);
        for (size_t i = 0; i < psz; i++)
        {
            to.points[i].radius = radius_b[i];
            for (int j = 0; j < 3; j++)
            {
                to.points[i].position[j] = points_b[i][j];
                to.points[i].color1[j] = col1_b[i][j];
                to.points[i].color2[j] = col2_b[i][j];
                to.points[i].color3[j] = 0;
            }
        }

        to.order = 4;
        return true;
    }

    bool cyhair_bezier_curves_loader::get_strand(size_t i, io_bezier_curve_strand* str) const
    {
        if (!l_->is_valid()) return false;
        cyhair_strand cy;
        if (!l_->get_strand(&cy)) return false;
        if (!str) return false;
        if (!convert(cy, *str)) return false;
        return true;
    }
}
