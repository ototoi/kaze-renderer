#ifndef CYHAIR_FORMAT_H
#define CYHAIR_FORMAT_H
/*
Bytes 0-3	Must be "HAIR" in ascii code (48 41 49 52)
Bytes 4-7	Number of hair strands as unsigned int
Bytes 8-11	Total number of points of all strands as unsigned int
Bytes 12-15	Bit array of data in the file
Bit-0 is 1 if the file has segments array.
Bit-1 is 1 if the file has points array (this bit must be 1).
Bit-2 is 1 if the file has thickness array.
Bit-3 is 1 if the file has transparency array.
Bit-4 is 1 if the file has color array.
Bit-5 to Bit-31 are reserved for future extension (must be 0).
Bytes 16-19	Default number of segments of hair strands as unsigned int
If the file does not have a segments array, this default value is used.
Bytes 20-23	Default thickness hair strands as float
If the file does not have a thickness array, this default value is used.
Bytes 24-27	Default transparency hair strands as float
If the file does not have a transparency array, this default value is used.
Bytes 28-39	Default color hair strands as float array of size 3
If the file does not have a thickness array, this default value is used.
Bytes 40-127	File information as char array of size 88 in ascii
*/

struct cyhair_header
{
    char magic[4];
    unsigned int strands;
    unsigned int total_points;
    unsigned int nFlags;
    unsigned int default_segments;
    float default_thickness;
    float default_transparency;
    float default_color[3];
    char infomation[88];
};

#endif
