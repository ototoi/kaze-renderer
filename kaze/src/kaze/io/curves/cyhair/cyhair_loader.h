#ifndef KAZE_CYHAIR_LOADER_H
#define KAZE_CYHAIR_LOADER_H

#include "types.h"
#include <vector>
#include "cyhair_format.h"

#include <cstdio>
#include <cstdlib>

namespace kaze
{

    struct cyhair_strand
    {
        std::vector<vector3f> points_array;
        std::vector<float> thickness_array;
        std::vector<float> transparency_array;
        std::vector<vector3f> color_array;

        void clear()
        {
            points_array.clear();
            thickness_array.clear();
            transparency_array.clear();
            color_array.clear();
        }
    };

    class cyHair_loader
    {
    public:
        cyHair_loader(const char* szFilePath);
        ~cyHair_loader();
        bool is_valid() const;
        size_t get_strands() const;
        bool get_strand(cyhair_strand* s, bool bStoreDefault = true);

    public:
        unsigned int get_default_segments() const;
        vector3f get_default_color() const;
        float get_default_thickness() const;
        float get_default_transparency() const;
        unsigned int get_flags() const;

    protected:
        void init();

    private:
        FILE* fp_;
        cyhair_header header_;
        std::vector<unsigned short> segment_;
        std::vector<vector3f> point_;
        std::vector<float> thickness_;
        std::vector<float> transparency_;
        std::vector<vector3f> color_;

        size_t used_;
        size_t i_;
    };

    typedef cyHair_loader cyhair_loader;
}

#endif
