#include "cyhair_loader.h"

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

namespace kaze
{

    cyHair_loader::cyHair_loader(const char* szFilePath)
    {
        used_ = 0;
        i_ = 0;

        fp_ = fopen(szFilePath, "rb");
        if (fp_)
        {
            if (1 != fread(&header_, 128, 1, fp_))
            {
                if (fp_) fclose(fp_);
                fp_ = 0;
                return;
            }
            if (memcmp(header_.magic, "HAIR", 4) != 0)
            {
                if (fp_) fclose(fp_);
                fp_ = 0;
                return;
            }
            init();
        }
        else
        {
            memset(&header_, 0, 128);
        }
    }

    cyHair_loader::~cyHair_loader()
    {
        if (fp_) fclose(fp_);
    }

    static bool ToBool(int x)
    {
        return x ? true : false;
    }

    static void GetFlags(
        bool& bHasSegments,
        bool& bHasPoints,
        bool& bHasThickness,
        bool& bHasTransparency,
        bool& bHasColor,
        int nFlags)
    {
        bHasSegments = ToBool(nFlags & 1);
        bHasPoints = ToBool(nFlags & 2);
        bHasThickness = ToBool(nFlags & 4);
        bHasTransparency = ToBool(nFlags & 8);
        bHasColor = ToBool(nFlags & 16);
    }

#define CLOSE()  \
    fclose(fp_); \
    fp_ = 0;     \
    return;

    void cyHair_loader::init()
    {
        bool bHasSegments = false;
        bool bHasPoints = true;
        bool bHasThickness = false;
        bool bHasTransparency = false;
        bool bHasColor = false;

        GetFlags(bHasSegments, bHasPoints, bHasThickness, bHasTransparency, bHasColor, header_.nFlags);

        unsigned int nStarands = header_.strands;
        unsigned int nCount = header_.total_points;

        if (bHasSegments)
        {
            segment_.resize(nStarands);
            if (1 != fread(&segment_[0], sizeof(unsigned short) * nStarands, 1, fp_))
            {
                CLOSE();
            }
        }

        if (bHasPoints)
        {
            point_.resize(nCount);
            float* f = (float*)&(point_[0]);
            if (1 != fread(f, nCount * sizeof(float) * 3, 1, fp_))
            {
                CLOSE();
            }
        }
        if (bHasThickness)
        {
            thickness_.resize(nCount);
            float* f = &(thickness_[0]);
            if (1 != fread(f, nCount * sizeof(float), 1, fp_))
            {
                CLOSE();
            }
        }

        if (bHasTransparency)
        {
            transparency_.resize(nCount);
            float* f = &(transparency_[0]);
            if (1 != fread(f, nCount * sizeof(float), 1, fp_))
            {
                CLOSE();
            }
        }

        if (bHasColor)
        {
            color_.resize(nCount);
            float* f = (float*)&(color_[0]);
            if (1 != fread(f, nCount * sizeof(float) * 3, 1, fp_))
            {
                CLOSE();
            }
        }
    }

    bool cyHair_loader::is_valid() const
    {
        if (fp_) return true;
        return false;
    }

    size_t cyHair_loader::get_strands() const
    {
        return header_.strands;
    }

    unsigned int cyHair_loader::get_default_segments() const
    {
        return header_.default_segments;
    }

    vector3f cyHair_loader::get_default_color() const
    {
        return vector3f(header_.default_color[0], header_.default_color[1], header_.default_color[2]);
    }

    float cyHair_loader::get_default_thickness() const
    {
        return header_.default_thickness;
    }

    float cyHair_loader::get_default_transparency() const
    {
        return header_.default_transparency;
    }

    unsigned int cyHair_loader::get_flags() const
    {
        return header_.nFlags;
    }

    bool cyHair_loader::get_strand(cyhair_strand* s, bool bStoreDefault)
    {
        if (!fp_) return false;
        if (i_ >= header_.strands) return false;

        s->clear();

        bool bHasSegments = false;
        bool bHasPoints = true;
        bool bHasThickness = false;
        bool bHasTransparency = false;
        bool bHasColor = false;

        GetFlags(bHasSegments, bHasPoints, bHasThickness, bHasTransparency, bHasColor, header_.nFlags);

        unsigned int nSegments = header_.default_segments;
        if (bHasSegments)
        {
            nSegments = segment_[i_];
        }

        unsigned int nPoints = nSegments + 1;

        if (bHasPoints)
        { //
            s->points_array.resize(nPoints);
            memcpy(&s->points_array[0], &point_[used_], sizeof(vector3f) * nPoints);
        }

        if (bHasThickness)
        {
            s->thickness_array.resize(nPoints);
            memcpy(&s->thickness_array[0], &thickness_[used_], sizeof(float) * nPoints);
        }
        else if (bStoreDefault)
        {
            s->thickness_array.resize(nPoints);
            for (size_t i = 0; i < nPoints; i++)
            {
                s->thickness_array[i] = header_.default_thickness;
            }
        }

        if (bHasTransparency)
        {
            s->transparency_array.resize(nPoints);
            memcpy(&s->transparency_array[0], &transparency_[used_], sizeof(float) * nPoints);
        }
        else if (bStoreDefault)
        {
            s->transparency_array.resize(nPoints);
            for (size_t i = 0; i < nPoints; i++)
            {
                s->transparency_array[i] = header_.default_transparency;
            }
        }

        if (bHasColor)
        {
            s->color_array.resize(nPoints);
            memcpy(&s->color_array[0], &color_[used_], sizeof(vector3f) * nPoints);
        }
        else if (bStoreDefault)
        {
            s->color_array.resize(nPoints);
            for (size_t i = 0; i < nPoints; i++)
            {
                s->color_array[i] = vector3f(header_.default_color);
            }
        }

        used_ += nPoints;
        i_++;
        return true;
    }
}
