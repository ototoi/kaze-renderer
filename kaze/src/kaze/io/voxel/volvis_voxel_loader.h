#ifndef KAZE_VOLVIS_VOXEL_LOADER_H
#define KAZE_VOLVIS_VOXEL_LOADER_H

namespace kaze
{

    class volvis_voxel_loader
    {
    public:
        volvis_voxel_loader(const char* szFilename, int dim[3]);
        ~volvis_voxel_loader();
        int dim(int i) const;
        unsigned char at_char(int i, int j, int k) const;
        const unsigned char* get_ptr() const { return ptr_; }
    protected:
        unsigned char* ptr_;
        int dim_[3];
    };
}

#endif