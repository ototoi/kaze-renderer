#ifndef KAZE_KZV_H
#define KAZE_KZV_H

namespace kaze
{

    struct kzv_header
    {
        char MAGIC[4]; //KZV\0
        int version;
        int dim[3];
    };
}

#endif