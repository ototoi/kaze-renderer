#ifndef KAZE_IO_DUAL_OCTREE_H
#define KAZE_IO_DUAL_OCTREE_H

#include "types.h"
#include <vector>
#include <string>

namespace kaze
{

    class io_dual_octree
    {
    public:
    public:
    };

    class io_dual_octree_node
    {
    public:
        enum type
        {
            LEAF = 1,
            BRANCH = 2,
            PSEUDO_BRANCH = 3,
        };
        typedef std::string hkey_t;

    public:
        virtual ~io_dual_octree_node() {}
        virtual bool is_branch() const { return !is_leaf(); }
        virtual bool is_leaf() const { return get_type() == LEAF; }
        virtual int get_type() const = 0;
        virtual io_dual_octree_node* get_at(int i) const { return 0; }
        virtual hkey_t get_hash() const { return ""; }
    };

    class leaf_io_dual_octree_node : public io_dual_octree_node
    {
    public:
        virtual int get_type() const()
        {
            return io_dual_octree_node::LEAF;
        }
        real get_value() const { return val_; }
        real get_gradient() const { return grad_; }
        virtual void set_hash(const hkey_t& h) { hash_ = h; }
        virtual hkey_t get_hash() const { return hash_; }
    protected:
        real val_;
        vector3 grad_;
        hkey_t hash_;
    };

    class branch_io_dual_octree_node : public io_dual_octree_node
    {
    public:
        virtual ~branch_io_dual_octree_node()
        {
            for (int i = 0; i < 8; i++)
            {
                if (children_[i]) delete children_[i];
            }
        }
        virtual int get_type() const()
        {
            return io_dual_octree_node::BRANCH;
        }
        virtual io_dual_octree_node* get_at(int i) const
        {
            return children_[0];
        }

    protected:
        io_dual_octree_node* children_[8];
    };

    class pseudo_branch_io_dual_octree_node : public io_dual_octree_node
    {
    public:
        virtual ~pseudo_branch_io_dual_octree_node()
        {
            for (int i = 0; i < 8; i++)
            {
                if (children_[i]) delete children_[i];
            }
        }
        virtual int get_type() const()
        {
            return io_dual_octree_node::PSEUDO_BRANCH;
        }
        virtual io_dual_octree_node* get_at(int i) const
        {
            return children_[0];
        }

    protected:
        io_dual_octree_node* children_[8];
    };
}

#endif
