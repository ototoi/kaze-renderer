#ifndef KAZE_DCF_H
#define KAZE_DCF_H

#include "io_dual_octree.h"

namespace kaze
{

    class io_dual_octree;

    int save_to_dcf(const char* szFile, const io_dual_octree* oct);
    int save_to_sog(const char* szFile, const io_dual_octree* oct);
}

#endif