#include "volvis_voxel_loader.h"
#include <stdio.h>
#include <string.h>

namespace kaze
{

    static unsigned char* CreateVoxel(const char* szFilename, int dim[3])
    {
        FILE* fp = fopen(szFilename, "rb");
        if (fp == 0) return 0;
        fseek(fp, 0, SEEK_END);
        int sz = ftell(fp);
        int w = dim[0];
        int h = dim[1];
        int d = dim[2];

        int rsz = w * h * d * sizeof(unsigned char);

        if (sz < rsz)
        {
            fclose(fp);
            return 0;
        }

        unsigned char* ptr = new unsigned char[rsz];
        fseek(fp, 0, SEEK_SET);
        fread(ptr, 1, sizeof(unsigned char) * rsz, fp);
        fclose(fp);
        return ptr;
    }

    volvis_voxel_loader::volvis_voxel_loader(const char* szFilename, int dim[3])
        : ptr_(0)
    {
        ptr_ = CreateVoxel(szFilename, dim);
        if (ptr_)
        {
            memcpy(dim_, dim, sizeof(int) * 3);
        }
        else
        {
            memset(dim_, 0, sizeof(int) * 3);
        }
    }

    volvis_voxel_loader::~volvis_voxel_loader()
    {
        if (ptr_) delete[] ptr_;
    }

    int volvis_voxel_loader::dim(int i) const
    {
        return dim_[i];
    }

    unsigned char volvis_voxel_loader::at_char(int i, int j, int k) const
    {
        int w = dim_[0];
        int h = dim_[1];
        size_t idx = (size_t)(i + (w * (j + h * (k))));
        return ptr_[idx];
    }
}
