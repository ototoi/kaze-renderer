#ifndef KAZE_BMP_IO_H
#define KAZE_BMP_IO_H

#include "image.hpp"
#include "color.h"

namespace kaze
{

    bool WriteBMPRGBAImage(const char* szFilePath, const unsigned char* bytes, int width, int height);
    std::shared_ptr<image<color3> > ReadBMPColor3Image(const char* szFilePath);
}

#endif