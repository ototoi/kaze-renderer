#ifndef KAZE_EXR_IO_H
#define KAZE_EXR_IO_H

#include "image.hpp"
#include "color.h"

namespace kaze
{

    bool WriteEXRRGBAImage(const char* szFilePath, const unsigned char* bytes, int width, int height);
    std::shared_ptr<image<color3> > ReadEXRColor3Image(const char* szFilePath);
    std::shared_ptr<image<color4> > ReadEXRColor4Image(const char* szFilePath);
}

#endif