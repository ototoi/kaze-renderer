#ifndef KAZE_HDR_IO_H
#define KAZE_HDR_IO_H

#include "image.hpp"
#include "color.h"

namespace kaze
{

    std::shared_ptr<image<color3> > ReadHDRColor3Image(const char* szFilePath);
    std::shared_ptr<image<color4> > ReadHDRColor4Image(const char* szFilePath);
    bool WriteHDRColor3Image(const char* szFilePath, const std::shared_ptr<image<color3> >& img);
    bool WriteHDRColor4Image(const char* szFilePath, const std::shared_ptr<image<color4> >& img);
}

#endif
