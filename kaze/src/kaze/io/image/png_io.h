#ifndef KAZE_PNG_IO_H
#define KAZE_PNG_IO_H

#include "image.hpp"
#include "color.h"

namespace kaze
{
    bool WritePNGRGBAImage(const char* szFilePath, const unsigned char* bytes, int width, int height);
    std::shared_ptr<image<color3> > ReadPNGColor3Image(const char* szFilePath);
    std::shared_ptr<image<color4> > ReadPNGColor4Image(const char* szFilePath);
}

#endif