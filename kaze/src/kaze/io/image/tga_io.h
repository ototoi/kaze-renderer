#ifndef KAZE_TGA_IO_H
#define KAZE_TGA_IO_H

#include "image.hpp"
#include "color.h"

namespace kaze
{

    bool WriteTGARGBAImage(const char* szFilePath, const unsigned char* bytes, int width, int height);
    std::shared_ptr<image<color3> > ReadTGAColor3Image(const char* szFilePath);
}

#endif