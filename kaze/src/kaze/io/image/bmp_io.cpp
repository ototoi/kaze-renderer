#include "bmp_io.h"

#include <assert.h>
#include <vector>
#include <string>

#include "memory_image.hpp"

#ifdef _WIN32
#include <shlobj.h>
#include <gdiplus.h>
#include <Gdiplusinit.h>
#include <windows.h>

#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "gdiplus.lib")

#else
#define STB_IMAGE_IMPLEMENTATION
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image.h"
#include "stb_image_write.h"
#endif

namespace kaze
{
//---------------------------------------------------------------------------------
#ifdef _WIN32
    using namespace Gdiplus;

    static BOOL GetEncoderClsid(LPCWSTR lpszFormat, CLSID* pClsid)
    {
        assert(lpszFormat);
        assert(pClsid);
        UINT nEncoders;
        UINT nSize;
        if (Gdiplus::GetImageEncodersSize(&nEncoders, &nSize) != Gdiplus::Ok)
        {
            return FALSE;
        }
        std::vector<BYTE> _scoped(nSize);
        Gdiplus::ImageCodecInfo* pEncoders = reinterpret_cast<Gdiplus::ImageCodecInfo*>(&_scoped[0]);
        if (Gdiplus::GetImageEncoders(nEncoders, nSize, pEncoders) != Gdiplus::Ok)
        {
            return FALSE;
        }
        std::wstring strFormat(lpszFormat);
        for (UINT i = 0; i < nEncoders; ++i)
        {
            if (strFormat == pEncoders[i].MimeType)
            {
                *pClsid = pEncoders[i].Clsid;
                return TRUE;
            }
        }
        return FALSE;
    }

    static BOOL SaveByBMP(LPCTSTR lpszFilePath, Gdiplus::Bitmap& img)
    {
        CLSID encoderClsid;
        GetEncoderClsid(L"image/bmp", &encoderClsid);
        return (img.Save(lpszFilePath, &encoderClsid, NULL) == Gdiplus::Ok);
    }

    static bool WriteBMPRGBAImage(LPCWSTR szFilePath, const BYTE* bytes, int width, int height)
    {
        GdiplusStartupInput gdiplusStartupInput;
        ULONG_PTR gdiplusToken = NULL;
        if (Gdiplus::Ok != GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL)) return false;

        const bool botttom_up = true;
        const SIZE size = {width, height};
        const int stride = ((32 * width + 31) / 32) * 4;
        std::vector<BYTE> buffer(stride * height);
        for (int y = 0; y < size.cy; ++y)
        {
            for (int x = 0; x < size.cx; ++x)
            {
                const int p = stride * (botttom_up ? (size.cy - 1) - y : y) + (4 * x);
                size_t index = y * width + x;
                buffer[p + 0] = bytes[4 * index + 2]; // B
                buffer[p + 1] = bytes[4 * index + 1]; // G
                buffer[p + 2] = bytes[4 * index + 0]; // R
                buffer[p + 3] = bytes[4 * index + 3]; // A
            }
        }

        bool bRet = true;
        {
            Gdiplus::Bitmap bmp(
                size.cx, size.cy,
                (botttom_up ? -stride : stride),
                PixelFormat32bppARGB,
                &buffer[0] + (botttom_up ? stride * (size.cy - 1) : 0));

            if (!SaveByBMP(szFilePath, bmp))
            {
                bRet = false;
            }
        }

        GdiplusShutdown(gdiplusToken);

        return bRet;
    }

    bool WriteBMPRGBAImage(const char* szFilePath, const BYTE* bytes, int width, int height)
    {
        WCHAR buffer[_MAX_PATH] = {};
        int nRet = ::MultiByteToWideChar(CP_OEMCP, 0, szFilePath, (int)strlen(szFilePath) + 1, buffer, _MAX_PATH);
        if (nRet == 0) return false;
        return WriteBMPRGBAImage(buffer, bytes, width, height);
    }

    static std::shared_ptr<image<color3> > ReadBMPColor3Image(LPCWSTR szFilePath)
    {
        std::shared_ptr<image<color3> > img;

        GdiplusStartupInput gdiplusStartupInput;
        ULONG_PTR gdiplusToken = NULL;
        if (Gdiplus::Ok != GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL)) return img;

        Bitmap* bmp = Bitmap::FromFile(szFilePath);
        if (bmp)
        {
            int width = bmp->GetWidth();
            int height = bmp->GetHeight();

            std::shared_ptr<image<color3> > timg(new memory_image<color3>(width, height));

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color c;
                    bmp->GetPixel(x, y, &c);
                    BYTE A = c.GetA();
                    BYTE R = c.GetR();
                    BYTE G = c.GetG();
                    BYTE B = c.GetB();
                    timg->set(x, y, color3(R / 255.0, G / 255.0, B / 255.0));
                }
            }

            img = timg;

            delete bmp;
            bmp = 0;
        }
        GdiplusShutdown(gdiplusToken);

        return img;
    }

    std::shared_ptr<image<color3> > ReadBMPColor3Image(const char* szFilePath)
    {
        WCHAR buffer[_MAX_PATH] = {};
        int nRet = ::MultiByteToWideChar(CP_OEMCP, 0, szFilePath, (int)strlen(szFilePath) + 1, buffer, _MAX_PATH);
        if (nRet == 0) return std::shared_ptr<image<color3> >();
        return ReadBMPColor3Image(buffer);
    }
#else

    bool WriteBMPRGBAImage(const char* szFilePath, const unsigned char* bytes, int width, int height)
    {
        int bpp = 4;
        int ret = stbi_write_bmp (szFilePath, width, height, bpp, bytes);
        return ret == 0;
    }

    std::shared_ptr<image<color3> > ReadBMPColor3Image(const char* szFilePath)
    {
        unsigned char* pixels = NULL ;
        int width;
        int height;
        int bpp;
        pixels = stbi_load (szFilePath, &width, &height, &bpp, 3);
        if(pixels != NULL)
        {
            std::shared_ptr<image<color3> > img(new memory_image<color3>(width, height));

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int index = y*width + x;
                    unsigned char R = pixels[3*index+0];
                    unsigned char G = pixels[3*index+1];
                    unsigned char B = pixels[3*index+2];
                    //unsigned char A = pixels[4*index+3];
                    img->set(x, y, color3(R / 255.0, G / 255.0, B / 255.0));
                }
            }
            
            stbi_image_free (pixels);
            return img;
        }
        return std::shared_ptr<image<color3> >();
    }
#endif
    //---------------------------------------------------------------------------------
}
