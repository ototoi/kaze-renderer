#ifndef KAZE_ZFILE_H
#define KAZE_ZFILE_H

#include <vector>

namespace kaze
{

    int write_zfile_image(const char* szFilePath, int width, int height, const double pixels[], const double w2c[16], const double w2s[16]);

    class zfile_image
    {
    public:
        int get_width() const;
        int get_height() const;
        bool load(const char* szFilePath);
        bool save(const char* szFilePath);

        const double* get_buffer() const;
        const double* get_w2c() const;
        const double* get_w2s() const;

    public:
        void set_buffer_raw(const std::vector<double>& buffer);
        const std::vector<double>& get_buffer_raw() const;

    private:
        std::vector<double> buffer_;
        int width_;
        int height_;
        double w2c_[16];
        double w2s_[16];
    };
}

#endif