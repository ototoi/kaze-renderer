#include "exr_io.h"
#include <vector>

#include <assert.h>
#include <vector>
#include <string>

#include "memory_image.hpp"

#ifdef __APPLE__
#include <OpenEXR/half.h>
#include <OpenEXR/ImfCRgbaFile.h>
#endif

namespace kaze
{

    template <class T>
    struct Col
    {
        static T get(real r, real g, real b, real a) { return T(r, g, b); }
    };

    template <>
    struct Col<color4>
    {
        static color4 get(real r, real g, real b, real a) { return color4(r, g, b, a); }
    };

#ifdef __APPLE__

    template <class T>
    static std::shared_ptr<image<T> > ReadEXRColorTImage(const char* szFilePath)
    {
        const ImfHeader* header;
        ImfInputFile* fp = NULL;

        fp = ImfOpenInputFile(szFilePath);
        if (!fp)
        {
            return std::shared_ptr<image<T> >();
        }

        header = ImfInputHeader((const ImfInputFile*)fp);
        //printf("channels = %d\n", ImfInputChannels(fp));

        int xmin, ymin, xmax, ymax;
        ImfHeaderDataWindow(header, &xmin, &ymin, &xmax, &ymax);

        //printf("data = %d, %d, %d, %d\n", xmin, ymin, xmax, ymax);
        int width, height;
        width = xmax - xmin + 1;
        height = ymax - ymin + 1;

        std::shared_ptr<image<T> > out(new memory_image<T>(width, height));

        std::vector<ImfRgba> buffer(width);
        ImfRgba* buf = &buffer[0];
        for (int y = 0; y < height; y++)
        {
            ImfInputSetFrameBuffer(fp, buf - (y * width), 1, width);
            ImfInputReadPixels(fp, y, y);
            for (int x = 0; x < width; x++)
            {
                float r = ImfHalfToFloat(buf[x].r);
                float g = ImfHalfToFloat(buf[x].g);
                float b = ImfHalfToFloat(buf[x].b);
                float a = ImfHalfToFloat(buf[x].a);
                out->set(x, y, Col<T>::get(r, g, b, a));
            }
        }
        ImfCloseInputFile(fp);
        return out;
    }
#else
    template <class T>
    static std::shared_ptr<image<T> > ReadEXRColorTImage(const char* szFilePath)
    {
        return std::shared_ptr<image<T> >();
    }
#endif

    bool WriteEXRRGBAImage(const char* szFilePath, const unsigned char* bytes, int width, int height)
    {
        return true;
    }
    std::shared_ptr<image<color3> > ReadEXRColor3Image(const char* szFilePath)
    {
        return ReadEXRColorTImage<color3>(szFilePath);
    }
    std::shared_ptr<image<color4> > ReadEXRColor4Image(const char* szFilePath)
    {
        return ReadEXRColorTImage<color4>(szFilePath);
    }
}