#ifndef KAZE_DDS_IO_H
#define KAZE_DDS_IO_H

#include "image.hpp"
#include "color.h"

namespace kaze
{

    std::shared_ptr<image<color3> > ReadDDSColor3Image(const char* szFilePath);
    std::shared_ptr<image<color4> > ReadDDSColor4Image(const char* szFilePath);
}

#endif