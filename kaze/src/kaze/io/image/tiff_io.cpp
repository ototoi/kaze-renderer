#include "tiff_io.h"

#include <assert.h>
#include <vector>
#include <string>

#include "memory_image.hpp"

#ifdef _WIN32
#include <shlobj.h>
#include <gdiplus.h>
#include <Gdiplusinit.h>
#include <windows.h>

#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "gdiplus.lib")

#else
#include <tiff.h>
#include <tiffio.h>
#endif

namespace kaze
{
//---------------------------------------------------------------------------------
#ifdef _WIN32
    using namespace Gdiplus;

    static BOOL GetEncoderClsid(LPCWSTR lpszFormat, CLSID* pClsid)
    {
        assert(lpszFormat);
        assert(pClsid);
        UINT nEncoders;
        UINT nSize;
        if (Gdiplus::GetImageEncodersSize(&nEncoders, &nSize) != Gdiplus::Ok)
        {
            return FALSE;
        }
        std::vector<BYTE> _scoped(nSize);
        Gdiplus::ImageCodecInfo* pEncoders = reinterpret_cast<Gdiplus::ImageCodecInfo*>(&_scoped[0]);
        if (Gdiplus::GetImageEncoders(nEncoders, nSize, pEncoders) != Gdiplus::Ok)
        {
            return FALSE;
        }
        std::wstring strFormat(lpszFormat);
        for (UINT i = 0; i < nEncoders; ++i)
        {
            if (strFormat == pEncoders[i].MimeType)
            {
                *pClsid = pEncoders[i].Clsid;
                return TRUE;
            }
        }
        return FALSE;
    }

    static BOOL SaveByTiff(LPCTSTR lpszFilePath, Gdiplus::Bitmap& img)
    {
        CLSID encoderClsid;
        GetEncoderClsid(L"image/tiff", &encoderClsid);
        return (img.Save(lpszFilePath, &encoderClsid, NULL) == Gdiplus::Ok);
    }

    static bool WriteTiffRGBAImage(LPCWSTR szFilePath, const BYTE* bytes, int width, int height)
    {
        GdiplusStartupInput gdiplusStartupInput;
        ULONG_PTR gdiplusToken = NULL;
        if (Gdiplus::Ok != GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL)) return false;

        const bool botttom_up = true;
        const SIZE size = {width, height};
        const int stride = ((32 * width + 31) / 32) * 4;
        std::vector<BYTE> buffer(stride * height);
        for (int y = 0; y < size.cy; ++y)
        {
            for (int x = 0; x < size.cx; ++x)
            {
                const int p = stride * (botttom_up ? (size.cy - 1) - y : y) + (4 * x);
                size_t index = y * width + x;
                buffer[p + 0] = bytes[4 * index + 2]; // B
                buffer[p + 1] = bytes[4 * index + 1]; // G
                buffer[p + 2] = bytes[4 * index + 0]; // R
                buffer[p + 3] = bytes[4 * index + 3]; // A
            }
        }

        bool bRet = true;
        {
            Gdiplus::Bitmap bmp(
                size.cx, size.cy,
                (botttom_up ? -stride : stride),
                PixelFormat32bppARGB,
                &buffer[0] + (botttom_up ? stride * (size.cy - 1) : 0));

            if (!SaveByTiff(szFilePath, bmp))
            {
                bRet = false;
            }
        }

        GdiplusShutdown(gdiplusToken);

        return bRet;
    }

    bool WriteTiffRGBAImage(const char* szFilePath, const BYTE* bytes, int width, int height)
    {
        WCHAR buffer[_MAX_PATH] = {};
        int nRet = ::MultiByteToWideChar(CP_OEMCP, 0, szFilePath, (int)strlen(szFilePath) + 1, buffer, _MAX_PATH);
        if (nRet == 0) return false;
        return WriteTiffRGBAImage(buffer, bytes, width, height);
    }

    static std::shared_ptr<image<color3> > ReadTiffColor3Image(LPCWSTR szFilePath)
    {
        std::shared_ptr<image<color3> > img;

        GdiplusStartupInput gdiplusStartupInput;
        ULONG_PTR gdiplusToken = NULL;
        if (Gdiplus::Ok != GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL)) return img;

        Bitmap* bmp = Bitmap::FromFile(szFilePath);
        if (bmp)
        {
            int width = bmp->GetWidth();
            int height = bmp->GetHeight();

            std::shared_ptr<image<color3> > timg(new memory_image<color3>(width, height));

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color c;
                    bmp->GetPixel(x, y, &c);
                    BYTE A = c.GetA();
                    BYTE R = c.GetR();
                    BYTE G = c.GetG();
                    BYTE B = c.GetB();
                    timg->set(x, y, color3(R / 255.0, G / 255.0, B / 255.0));
                }
            }

            img = timg;

            delete bmp;
            bmp = 0;
        }
        GdiplusShutdown(gdiplusToken);

        return img;
    }

    static std::shared_ptr<image<color4> > ReadTiffColor4Image(LPCWSTR szFilePath)
    {
        std::shared_ptr<image<color4> > img;

        GdiplusStartupInput gdiplusStartupInput;
        ULONG_PTR gdiplusToken = NULL;
        if (Gdiplus::Ok != GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL)) return img;

        Bitmap* bmp = Bitmap::FromFile(szFilePath);
        if (bmp)
        {
            int width = bmp->GetWidth();
            int height = bmp->GetHeight();

            std::shared_ptr<image<color4> > timg(new memory_image<color4>(width, height));

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color c;
                    bmp->GetPixel(x, y, &c);
                    BYTE A = c.GetA();
                    BYTE R = c.GetR();
                    BYTE G = c.GetG();
                    BYTE B = c.GetB();
                    timg->set(x, y, color4(R / 255.0, G / 255.0, B / 255.0, A / 255.0));
                }
            }

            img = timg;

            delete bmp;
            bmp = 0;
        }
        GdiplusShutdown(gdiplusToken);

        return img;
    }

    std::shared_ptr<image<color3> > ReadTiffColor3Image(const char* szFilePath)
    {
        WCHAR buffer[_MAX_PATH] = {};
        int nRet = ::MultiByteToWideChar(CP_OEMCP, 0, szFilePath, (int)strlen(szFilePath) + 1, buffer, _MAX_PATH);
        if (nRet == 0) return std::shared_ptr<image<color3> >();
        return ReadTiffColor3Image(buffer);
    }

    std::shared_ptr<image<color4> > ReadTiffColor4Image(const char* szFilePath)
    {
        WCHAR buffer[_MAX_PATH] = {};
        int nRet = ::MultiByteToWideChar(CP_OEMCP, 0, szFilePath, (int)strlen(szFilePath) + 1, buffer, _MAX_PATH);
        if (nRet == 0) return std::shared_ptr<image<color4> >();
        return ReadTiffColor4Image(buffer);
    }
#else

    bool WriteTiffRGBAImage(const char* szFilePath, const unsigned char* bytes, int width, int height)
    {
        TIFF* image = NULL;
        // Open the TIFF file
        if ((image = TIFFOpen(szFilePath, "w")) == NULL)
        {
            //printf("Could not open output.tif for writing");
            return false;
        }
        // We need to set some values for basic tags before we can add any data
        TIFFSetField(image, TIFFTAG_IMAGEWIDTH, width);
        TIFFSetField(image, TIFFTAG_IMAGELENGTH, height);
        TIFFSetField(image, TIFFTAG_BITSPERSAMPLE, 8);
        TIFFSetField(image, TIFFTAG_SAMPLESPERPIXEL, 4);
        //TIFFSetField(image, TIFFTAG_ROWSPERSTRIP, height);
        TIFFSetField(image, TIFFTAG_COMPRESSION, COMPRESSION_LZW);
        TIFFSetField(image, TIFFTAG_PHOTOMETRIC, PHOTOMETRIC_RGB);
        TIFFSetField(image, TIFFTAG_FILLORDER, FILLORDER_MSB2LSB);
        TIFFSetField(image, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
        TIFFSetField(image, TIFFTAG_XRESOLUTION, 150.0);
        TIFFSetField(image, TIFFTAG_YRESOLUTION, 150.0);
        TIFFSetField(image, TIFFTAG_RESOLUTIONUNIT, RESUNIT_INCH);
        // Write the information to the file
        TIFFWriteEncodedStrip(image, 0, (void*)bytes, width * height * 4);
        // Close the file
        TIFFClose(image);
        return true;
    }

    template <class T>
    struct Col
    {
        static T get(real r, real g, real b, real a) { return T(r, g, b); }
    };

    template <>
    struct Col<color4>
    {
        static color4 get(real r, real g, real b, real a) { return color4(r, g, b, a); }
    };

    template <class T>
    static std::shared_ptr<image<T> > ReadTiffColorTImage(const char* szFilePath)
    {
        std::shared_ptr<image<T> > out;
        TIFF* image = NULL;

        // Open the TIFF file
        if ((image = TIFFOpen(szFilePath, "r")) == NULL)
        {
            goto FINAL;
        }
        int width, height;
        int nBitsPerSample;
        int nRowsPerStrip;
        int nSamplePerPixel;
        int nPhotometric;
        // We need to set some values for basic tags before we can add any data
        if (TIFFGetField(image, TIFFTAG_IMAGEWIDTH, &width) != 1) goto FINAL;
        if (TIFFGetField(image, TIFFTAG_IMAGELENGTH, &height) != 1) goto FINAL;
        if (TIFFGetField(image, TIFFTAG_BITSPERSAMPLE, &nBitsPerSample) != 1) goto FINAL;
        if (TIFFGetField(image, TIFFTAG_SAMPLESPERPIXEL, &nSamplePerPixel) != 1) goto FINAL;
        if (TIFFGetField(image, TIFFTAG_ROWSPERSTRIP, &nRowsPerStrip) != 1)
        {
            nRowsPerStrip = 1;
        }
        if (TIFFGetField(image, TIFFTAG_PHOTOMETRIC, &nPhotometric) != 1) goto FINAL;

        if (nPhotometric == 3) // インデックスカラーならば
        {
            goto FINAL;
        }
        else
        {
            out.reset(new memory_image<T>(width, height));
            for (int y = 0; y < height; y++)
            {
                std::vector<unsigned char> buffer(width * nSamplePerPixel);
                /* バッファに1ストリップ分読み込み */
                if (TIFFReadScanline(image, &buffer[0], y, 0) == -1)
                {
                    ; //
                }
                else
                {
                    for (int x = 0; x < width; x++)
                    {
                        int r = buffer[x * nSamplePerPixel + 0];
                        int g = buffer[x * nSamplePerPixel + 1];
                        int b = buffer[x * nSamplePerPixel + 2];
                        int a = 255;
                        if (nSamplePerPixel == 4)
                        {
                            a = buffer[x * nSamplePerPixel + 3];
                        }

                        out->set(x, y, Col<T>::get(r / 255.0, g / 255.0, b / 255.0, a / 255.0));
                    }
                }
            }
        }

    FINAL:
        if (image)
        {
            TIFFClose(image);
        }
        return out;
    }

    std::shared_ptr<image<color3> > ReadTiffColor3Image(const char* szFilePath)
    {
        return ReadTiffColorTImage<color3>(szFilePath);
    }
    std::shared_ptr<image<color4> > ReadTiffColor4Image(const char* szFilePath)
    {
        return ReadTiffColorTImage<color4>(szFilePath);
    }
#endif
}
