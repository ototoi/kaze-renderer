#ifndef KAZE_TIFF_IO_H
#define KAZE_TIFF_IO_H

#include "image.hpp"
#include "color.h"

namespace kaze
{

    bool WriteTiffRGBAImage(const char* szFilePath, const unsigned char* bytes, int width, int height);
    std::shared_ptr<image<color3> > ReadTiffColor3Image(const char* szFilePath);
    std::shared_ptr<image<color4> > ReadTiffColor4Image(const char* szFilePath);
}

#endif