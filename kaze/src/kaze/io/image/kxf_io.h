#ifndef KAZE_KXF_IO_H
#define KAZE_KXF_IO_H

#include "color.h"
#include "image.hpp"
#include <vector>

namespace kaze
{
    struct kxf_header
    {
        char magic[4];
        int version;
        unsigned int width;
        unsigned int height;

        char swrap[16];
        char twrap[16];
        char filter[16];
        float swidth;
        float twidth;
        char projection[8];
        char application[8];

        unsigned int miplevel;
        unsigned int block;
        unsigned int pointers;
        char channels[8];
    };

    struct kxf_mip_header
    {
        unsigned int width;
        unsigned int height;
        unsigned int block_width;
        unsigned int block_height;
    };

    struct kxf_pointer
    {
        unsigned int level;
        unsigned int x0;
        unsigned int x1;
        unsigned int y0;
        unsigned int y1;
        unsigned int offset;
    };

    int read_kxf_header(const char* szTexFile, kxf_header* header);
    int save_to_kxf(const char* szTexFile, const std::shared_ptr<image<color4> >& img, const char* swrap, const char* twrap, const char* filer, float swidth, float twidth);
    int save_to_kxf(const char* szTexFile, const std::vector<std::shared_ptr<image<color4> > >& vecImg, const char* swrap, const char* twrap, const char* filer, float swidth, float twidth);
    int save_to_kxf_cube(const char* szTexFile, const std::vector<std::shared_ptr<image<color4> > >& vecImg, const char* swrap, const char* twrap, const char* filer, float swidth, float twidth);
    int save_to_kxf_latlong(const char* szTexFile, const std::shared_ptr<image<color4> >& img, const char* filer, float swidth, float twidth);

    int load_from_kxf(const char* szTexFile, std::shared_ptr<image<color3> >& img);
    int load_from_kxf(const char* szTexFile, std::vector<std::shared_ptr<image<color3> > >& vecImg);
}

#endif