#ifndef KAZE_JPEG_IO_H
#define KAZE_JPEG_IO_H

#include "image.hpp"
#include "color.h"

namespace kaze
{

    bool WriteJPEGRGBAImage(const char* szFilePath, const unsigned char* bytes, int width, int height);
    std::shared_ptr<image<color3> > ReadJPEGColor3Image(const char* szFilePath);
    std::shared_ptr<image<color4> > ReadJPEGColor4Image(const char* szFilePath);
}

#endif