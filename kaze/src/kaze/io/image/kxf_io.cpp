#include "kxf_io.h"

#ifndef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include "fp16.h"
#include "memory_image.hpp"

#include <stdio.h>
#include <vector>

#define KTX_VERSION_MAJOR 9
#define BLK 32

namespace kaze
{
    int read_kxf_header(const char* szTexFile, kxf_header* hdr)
    {
        FILE* fp = fopen(szTexFile, "rb");
        if (!fp) return -1;

        kxf_header header;
        int nRead = fread(&header, sizeof(kxf_header), 1, fp);

        if (nRead != 1)
        {
            fclose(fp);
            return -1;
        }

        if (strcmp(header.magic, "KTX") != 0)
        {
            fclose(fp);
            return -1;
        }

        int width = header.width;
        int height = header.height;

        if (header.version != KTX_VERSION_MAJOR)
        {
            fclose(fp);
            return -1;
        }

        memcpy(hdr, &header, sizeof(kxf_header));
        fclose(fp);

        return 0;
    }

    static int save_to_kxf(const char* szTexFile, const image<color3>& img, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth, const char* proj)
    {
        FILE* fp = fopen(szTexFile, "wb");
        if (!fp) return -1;

        int width = img.get_width();
        int height = img.get_height();
        kxf_header header;
        memset(&header, 0, sizeof(kxf_header));
        memcpy(header.magic, "KTX", sizeof(char) * 4);
        header.version = KTX_VERSION_MAJOR;
        header.width = width;
        header.height = height;

        strcpy(header.swrap, swrap);
        strcpy(header.twrap, twrap);
        strcpy(header.filter, filter);
        header.swidth = swidth;
        header.twidth = twidth;

        header.miplevel = 1;
        header.block = BLK;

        strcpy(header.projection, proj);
        strcpy(header.application, "color");
        strcpy(header.channels, "rgb");

        int bw = width / BLK;
        int bh = height / BLK;
        if (bw * BLK < width) bw++;
        if (bh * BLK < height) bh++;

        header.pointers = bw * bh;

        fwrite(&header, sizeof(kxf_header), 1, fp);
        size_t ptr_offset = (size_t)ftell(fp);

        std::vector<kxf_pointer> ptrs(bw * bh);
        for (int y = 0; y < bh; y++)
        {
            for (int x = 0; x < bw; x++)
            {
                int x0 = x * BLK;
                int x1 = std::min(width, x0 + BLK);
                int y0 = y * BLK;
                int y1 = std::min(height, y0 + BLK);
                int idx = y * bw + x;
                ptrs[idx].x0 = x0;
                ptrs[idx].x1 = x1;
                ptrs[idx].y0 = y0;
                ptrs[idx].y1 = y1;
            }
        }

        fwrite(&ptrs[0], sizeof(kxf_pointer) * bw * bh, 1, fp);

        for (size_t i = 0; i < ptrs.size(); i++)
        {
            ptrs[i].offset = (size_t)ftell(fp);
            for (unsigned int y = ptrs[i].y0; y < ptrs[i].y1; y++)
            {
                for (unsigned int x = ptrs[i].x0; x < ptrs[i].x1; x++)
                {
                    color3 c = img.get(x, y);

                    uint16_t srgb[3];
                    srgb[0] = f32to16((float)c[0]);
                    srgb[1] = f32to16((float)c[1]);
                    srgb[2] = f32to16((float)c[2]);
                    fwrite(srgb, sizeof(uint16_t), 3, fp);
                }
            }
        }

        fseek(fp, (long)ptr_offset, SEEK_SET);
        fwrite(&ptrs[0], sizeof(kxf_pointer) * bw * bh, 1, fp);

        fflush(fp);
        fclose(fp);
        return 0;
    }

    int save_to_kxf(const char* szTexFile, const image<color3>& img, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth)
    {
        return save_to_kxf(szTexFile, img, swrap, twrap, filter, swidth, twidth, "plane");
    }

    //------------------------------------------------------------------------------------------------------------------------------------------------------

    static int save_to_kxf(const char* szTexFile, const std::vector<std::shared_ptr<image<color4> > >& vecImg, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth, const char* proj)
    {
        FILE* fp = fopen(szTexFile, "wb");
        if (!fp) return -1;

        int width = vecImg[0]->get_width();
        int height = vecImg[0]->get_height();
        kxf_header header;
        memset(&header, 0, sizeof(kxf_header));
        memcpy(header.magic, "KTX", sizeof(char) * 4);
        header.version = KTX_VERSION_MAJOR;
        header.width = width;
        header.height = height;

        strcpy(header.swrap, swrap);
        strcpy(header.twrap, twrap);
        strcpy(header.filter, filter);
        header.swidth = swidth;
        header.twidth = twidth;

        header.miplevel = vecImg.size();
        header.block = BLK;
        strcpy(header.projection, proj);
        strcpy(header.application, "color");
        strcpy(header.channels, "rgba");

        {
            int bw = width / BLK;
            int bh = height / BLK;
            if (bw * BLK < width) bw++;
            if (bh * BLK < height) bh++;
            header.pointers = bw * bh;
        }

        fwrite(&header, sizeof(kxf_header), 1, fp);

        std::vector<std::vector<kxf_pointer> > vecPtrs(vecImg.size());
        std::vector<size_t> ptr_offsets(vecImg.size());
        for (size_t k = 0; k < vecImg.size(); k++)
        {
            int ww = vecImg[k]->get_width();
            int hh = vecImg[k]->get_height();

            int bw = ww / BLK;
            int bh = hh / BLK;
            if (bw * BLK < width) bw++;
            if (bh * BLK < height) bh++;

            kxf_mip_header mip_header;
            mip_header.width = ww;
            mip_header.height = hh;
            mip_header.block_width = bw;
            mip_header.block_height = bh;
            fwrite(&mip_header, sizeof(kxf_mip_header), 1, fp);

            size_t ptr_offset = (size_t)ftell(fp);
            ptr_offsets[k] = ptr_offset;

            std::vector<kxf_pointer>& ptrs = vecPtrs[k];
            ptrs.resize(bw * bh);

            for (int y = 0; y < bh; y++)
            {
                for (int x = 0; x < bw; x++)
                {
                    int x0 = x * BLK;
                    int x1 = std::min(ww, x0 + BLK);
                    int y0 = y * BLK;
                    int y1 = std::min(hh, y0 + BLK);
                    int idx = y * bw + x;

                    ptrs[idx].level = k;
                    ptrs[idx].x0 = x0;
                    ptrs[idx].x1 = x1;
                    ptrs[idx].y0 = y0;
                    ptrs[idx].y1 = y1;
                }
            }

            fwrite(&ptrs[0], sizeof(kxf_pointer) * bw * bh, 1, fp);
            const std::shared_ptr<image<color4> >& img = vecImg[k];

            for (size_t i = 0; i < ptrs.size(); i++)
            {
                ptrs[i].offset = (size_t)ftell(fp);
                for (unsigned int y = ptrs[i].y0; y < ptrs[i].y1; y++)
                {
                    for (unsigned int x = ptrs[i].x0; x < ptrs[i].x1; x++)
                    {
                        color4 c = img->get(x, y);

                        uint16_t srgb[4];
                        srgb[0] = f32to16((float)c[0]);
                        srgb[1] = f32to16((float)c[1]);
                        srgb[2] = f32to16((float)c[2]);
                        srgb[3] = f32to16((float)c[3]);
                        fwrite(srgb, sizeof(uint16_t), 4, fp);
                    }
                }
            }

            ptr_offset = (size_t)ftell(fp);
            fseek(fp, (long)ptr_offsets[k], SEEK_SET);
            fwrite(&ptrs[0], sizeof(kxf_pointer) * bw * bh, 1, fp);
            fseek(fp, (long)ptr_offset, SEEK_SET);
        }

        fflush(fp);
        fclose(fp);
        return 0;
    }

    int save_to_kxf(const char* szTexFile, const std::shared_ptr<image<color4> >& img, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth)
    {
        std::vector<std::shared_ptr<image<color4> > > tmp;
        tmp.push_back(img);
        return save_to_kxf(szTexFile, tmp, swrap, twrap, filter, swidth, twidth);
    }

    int save_to_kxf(const char* szTexFile, const std::vector<std::shared_ptr<image<color4> > >& vecImg, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth)
    {
        return save_to_kxf(szTexFile, vecImg, swrap, twrap, filter, swidth, twidth, "plane");
    }

    int save_to_kxf_cube(const char* szTexFile, const std::vector<std::shared_ptr<image<color4> > >& vecImg, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth)
    {
        return save_to_kxf(szTexFile, vecImg, swrap, twrap, filter, swidth, twidth, "cube");
    }

    int save_to_kxf_latlong(const char* szTexFile, const std::shared_ptr<image<color4> >& img, const char* filter, float swidth, float twidth)
    {
        std::vector<std::shared_ptr<image<color4> > > tmp;
        tmp.push_back(img);
        return save_to_kxf(szTexFile, tmp, "repeat", "clamp", filter, swidth, twidth, "latlong");
    }

    int load_from_kxf(const char* szTexFile, std::shared_ptr<image<color3> >& img)
    {
        std::vector<std::shared_ptr<image<color3> > > vecImg;
        int nRet = load_from_kxf(szTexFile, vecImg);
        if (nRet == 0 && vecImg.size())
        {
            img = vecImg[0];
        }
        return nRet;
    }

    class kxf_image_s3 : public image<color3>
    {
    public:
        kxf_image_s3(int width, int height, const std::vector<uint16_t>& vec)
            : width_(width), height_(height), vec_(vec)
        {
            ;
        }
        color3 get(int x, int y) const
        {
            int i = y * width_ + x;
            float r = f16to32(vec_[3 * i + 0]);
            float g = f16to32(vec_[3 * i + 1]);
            float b = f16to32(vec_[3 * i + 2]);
            return color3(r, g, b);
        }
        void set(int x, int y, const color3& c)
        {
        }
        int get_width() const { return width_; }
        int get_height() const { return height_; }
    private:
        int width_;
        int height_;
        std::vector<uint16_t> vec_;
    };

    class kxf_image_s4 : public image<color4>
    {
    public:
        kxf_image_s4(int width, int height, const std::vector<uint16_t>& vec)
            : width_(width), height_(height), vec_(vec)
        {
            ;
        }
        color4 get(int x, int y) const
        {
            int i = y * width_ + x;
            float r = f16to32(vec_[4 * i + 0]);
            float g = f16to32(vec_[4 * i + 1]);
            float b = f16to32(vec_[4 * i + 2]);
            float a = f16to32(vec_[4 * i + 3]);
            return color4(r, g, b, a);
        }
        void set(int x, int y, const color4& c)
        {
        }
        int get_width() const { return width_; }
        int get_height() const { return height_; }
    private:
        int width_;
        int height_;
        std::vector<uint16_t> vec_;
    };

    template<class T>
    struct sx_image_traits
    {
        typedef kxf_image_s3 image_type;
        static const int channels = 3;
    };

    template<>
    struct sx_image_traits<color4>
    {
        typedef kxf_image_s4 image_type;
        static const int channels = 4;
    };

    template<class T>
    int load_from_kxf_(const char* szTexFile, std::vector<std::shared_ptr<image<T> > >& vecImg)
    {
        static const int image_channels = sx_image_traits<T>::channels;

        FILE* fp = fopen(szTexFile, "rb");
        if (!fp) return -1;

        kxf_header header;
        int nRead = fread(&header, sizeof(kxf_header), 1, fp);

        if (nRead != 1 || strcmp(header.magic, "KTX") != 0)
        {
            fclose(fp);
            return -1;
        }

        int width = header.width;
        int height = header.height;

        if (header.version != KTX_VERSION_MAJOR)
        {
            fclose(fp);
            return -1;
        }

        if (width * height == 0)
        {
            fclose(fp);
            return -1;
        }

        int miplevel = header.miplevel;
        int channels = strlen(header.channels);

        //printf("%s:%d\n", header.channels, channels);

        for (int k = 0; k < miplevel; k++)
        {
            kxf_mip_header mip_header;
            fread(&mip_header, sizeof(kxf_mip_header), 1, fp);

            int ww = mip_header.width;
            int hh = mip_header.height;

            int bw = mip_header.block_width;
            int bh = mip_header.block_height;

            std::vector<kxf_pointer> ptrs(bw * bh);
            fread(&ptrs[0], sizeof(kxf_pointer) * bw * bh, 1, fp);

            std::vector<uint16_t> vec(image_channels * ww * hh);
            for (size_t i = 0; i < ptrs.size(); i++)
            {
                fseek(fp, (long)ptrs[i].offset, SEEK_SET);
                for (unsigned int y = ptrs[i].y0; y < ptrs[i].y1; y++)
                {
                    for (unsigned int x = ptrs[i].x0; x < ptrs[i].x1; x++)
                    {
                        uint16_t srgb[4] = {};
                        fread(srgb, sizeof(uint16_t), channels, fp);

                        int index = y * ww + x;
                        if(image_channels == 3)
                        {
                            vec[3 * index + 0] = srgb[0];
                            vec[3 * index + 1] = srgb[1];
                            vec[3 * index + 2] = srgb[2];
                        }
                        else if(image_channels == 4)
                        {
                            vec[4 * index + 0] = srgb[0];
                            vec[4 * index + 1] = srgb[1];
                            vec[4 * index + 2] = srgb[2];
                            if(channels == 4)
                            {
                                vec[4 * index + 3] = srgb[3];
                            }
                            else
                            {
                                vec[4 * index + 3] = f32to16(1);
                            }
                        }
                    }
                }
            }

            typedef typename sx_image_traits<T>::image_type image_type;
            vecImg.push_back( std::shared_ptr< image<T> >(new image_type(ww, hh, vec)) );
        }

        fclose(fp);

        return 0;
    }

    int load_from_kxf(const char* szTexFile, std::vector<std::shared_ptr<image<color3> > >& vecImg)
    {
        return load_from_kxf_(szTexFile, vecImg);
    }

    int load_from_kxf(const char* szTexFile, std::vector<std::shared_ptr<image<color4> > >& vecImg)
    {
        return load_from_kxf_(szTexFile, vecImg);
    }
}
