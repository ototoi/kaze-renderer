#include "png_io.h"
#include <vector>

#include <assert.h>
#include <vector>
#include <string>

#include "memory_image.hpp"

#ifdef _WIN32
#include <shlobj.h>
#include <gdiplus.h>
#include <Gdiplusinit.h>
#include <windows.h>

#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "gdiplus.lib")

#else
#include <png.h>
#endif

namespace kaze
{
    //---------------------------------------------------------------------------------
    static std::shared_ptr<image<color3> > ConvertChannel(const std::shared_ptr<image<color4> >& img)
    {
        int width = img->get_width();
        int height = img->get_height();
        std::shared_ptr<image<color3> > timg(new memory_image<color3>(width, height));
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                color4 c = img->get(x, y);
                timg->set(x, y, color3(c[0], c[1], c[2]));
            }
        }
        return timg;
    }

#ifdef _WIN32
    using namespace Gdiplus;

    static BOOL GetEncoderClsid(LPCWSTR lpszFormat, CLSID* pClsid)
    {
        assert(lpszFormat);
        assert(pClsid);
        UINT nEncoders;
        UINT nSize;
        if (Gdiplus::GetImageEncodersSize(&nEncoders, &nSize) != Gdiplus::Ok)
        {
            return FALSE;
        }
        std::vector<BYTE> _scoped(nSize);
        Gdiplus::ImageCodecInfo* pEncoders = reinterpret_cast<Gdiplus::ImageCodecInfo*>(&_scoped[0]);
        if (Gdiplus::GetImageEncoders(nEncoders, nSize, pEncoders) != Gdiplus::Ok)
        {
            return FALSE;
        }
        std::wstring strFormat(lpszFormat);
        for (UINT i = 0; i < nEncoders; ++i)
        {
            if (strFormat == pEncoders[i].MimeType)
            {
                *pClsid = pEncoders[i].Clsid;
                return TRUE;
            }
        }
        return FALSE;
    }

    static BOOL SaveByPNG(LPCTSTR lpszFilePath, Gdiplus::Bitmap& img)
    {
        CLSID encoderClsid;
        GetEncoderClsid(L"image/png", &encoderClsid);
        return (img.Save(lpszFilePath, &encoderClsid, NULL) == Gdiplus::Ok);
    }

    bool WritePNGRGBAImage(LPCWSTR szFilePath, const BYTE* bytes, int width, int height)
    {
        GdiplusStartupInput gdiplusStartupInput;
        ULONG_PTR gdiplusToken = NULL;
        if (Gdiplus::Ok != GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL)) return false;

        const bool botttom_up = true;
        const SIZE size = {width, height};
        const int stride = ((32 * width + 31) / 32) * 4;
        std::vector<BYTE> buffer(stride * height);
        for (int y = 0; y < size.cy; ++y)
        {
            for (int x = 0; x < size.cx; ++x)
            {
                const int p = stride * (botttom_up ? (size.cy - 1) - y : y) + (4 * x);
                size_t index = y * width + x;
                buffer[p + 0] = bytes[4 * index + 2]; // B
                buffer[p + 1] = bytes[4 * index + 1]; // G
                buffer[p + 2] = bytes[4 * index + 0]; // R
                buffer[p + 3] = bytes[4 * index + 3]; // A
            }
        }

        bool bRet = true;
        {
            Gdiplus::Bitmap bmp(
                size.cx, size.cy,
                (botttom_up ? -stride : stride),
                PixelFormat32bppARGB,
                &buffer[0] + (botttom_up ? stride * (size.cy - 1) : 0));

            if (!SaveByPNG(szFilePath, bmp))
            {
                bRet = false;
            }
        }

        GdiplusShutdown(gdiplusToken);

        return bRet;
    }

    bool WritePNGRGBAImage(const char* szFilePath, const BYTE* bytes, int width, int height)
    {
        WCHAR buffer[_MAX_PATH] = {};
        int nRet = ::MultiByteToWideChar(CP_OEMCP, 0, szFilePath, (int)strlen(szFilePath) + 1, buffer, _MAX_PATH);
        if (nRet == 0) return false;
        return WritePNGRGBAImage(buffer, bytes, width, height);
    }

    static std::shared_ptr<image<color4> > ReadPNGColor4Image(LPCWSTR szFilePath)
    {
        std::shared_ptr<image<color3> > img;

        GdiplusStartupInput gdiplusStartupInput;
        ULONG_PTR gdiplusToken = NULL;
        if (Gdiplus::Ok != GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL)) return img;

        Bitmap* bmp = Bitmap::FromFile(szFilePath);
        if (bmp)
        {
            int width = bmp->GetWidth();
            int height = bmp->GetHeight();

            std::shared_ptr<image<color4> > timg(new memory_image<color4>(width, height));

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    Color c;
                    bmp->GetPixel(x, y, &c);
                    BYTE A = c.GetA();
                    BYTE R = c.GetR();
                    BYTE G = c.GetG();
                    BYTE B = c.GetB();
                    timg->set(x, y, color4(R / 255.0, G / 255.0, B / 255.0, A / 255.0));
                }
            }

            img = timg;

            delete bmp;
            bmp = 0;
        }
        GdiplusShutdown(gdiplusToken);

        return img;
    }

    std::shared_ptr<image<color3> > ReadPNGColor3Image(const char* szFilePath)
    {
        WCHAR buffer[_MAX_PATH] = {};
        int nRet = ::MultiByteToWideChar(CP_OEMCP, 0, szFilePath, (int)strlen(szFilePath) + 1, buffer, _MAX_PATH);
        if (nRet == 0) return std::shared_ptr<image<color3> >();
        return ConvertChannel(ReadPNGColor4Image(buffer));
    }

    std::shared_ptr<image<color4> > ReadPNGColor4Image(const char* szFilePath)
    {
        WCHAR buffer[_MAX_PATH] = {};
        int nRet = ::MultiByteToWideChar(CP_OEMCP, 0, szFilePath, (int)strlen(szFilePath) + 1, buffer, _MAX_PATH);
        if (nRet == 0) return std::shared_ptr<image<color4> >();
        return ReadPNGColor4Image(buffer);
    }

#else
    static void write_row_callback(png_structp png_ptr, png_uint_32 row, int pass)
    {
        //printf("\r%3d%% saved", (row * 100) / png_ptr->height);
    }

    static bool write_png(const char* file_name, png_bytepp image, int width, int height)
    {
        FILE* fp;
        png_structp png_ptr;
        png_infop info_ptr;

        if ((fp = fopen(file_name, "wb")) == NULL) return false;
        png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
        if (png_ptr == NULL)
        {
            fclose(fp);
            return false;
        }
        info_ptr = png_create_info_struct(png_ptr);
        if (info_ptr == NULL)
        {
            png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
            fclose(fp);
            return false;
        }
        /*
		if (setjmp(png_ptr->jmpbuf)) {
			png_destroy_write_struct(&png_ptr,  &info_ptr);
			fclose(fp);
			return false;
		}
         */
        png_init_io(png_ptr, fp);
        png_set_write_status_fn(png_ptr, write_row_callback);
        png_set_filter(png_ptr, 0, PNG_ALL_FILTERS);
        //png_set_compression_level(png_ptr, Z_BEST_COMPRESSION);
        png_set_IHDR(png_ptr, info_ptr, width, height, 8, PNG_COLOR_TYPE_RGB_ALPHA, //rgba
                     PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
        png_set_gAMA(png_ptr, info_ptr, 1.0);

        {
            time_t gmt; // G.M.T.
            png_time mod_time;
            png_text text_ptr[2];

            time(&gmt);
            png_convert_from_time_t(&mod_time, gmt);
            png_set_tIME(png_ptr, info_ptr, &mod_time);

            text_ptr[0].key = "Generator";
            text_ptr[0].text = "Kaze-renderer";
            text_ptr[0].compression = PNG_TEXT_COMPRESSION_NONE;
            text_ptr[1].key = "Creation Time";
            text_ptr[1].text = (char*)png_convert_to_rfc1123(png_ptr, &mod_time);
            text_ptr[1].compression = PNG_TEXT_COMPRESSION_NONE;

            png_set_text(png_ptr, info_ptr, text_ptr, 2);
        }

        png_write_info(png_ptr, info_ptr);
        png_write_image(png_ptr, image);
        png_write_end(png_ptr, info_ptr);
        png_destroy_write_struct(&png_ptr, &info_ptr);
        fclose(fp);
        return true;
    }

    bool WritePNGRGBAImage(const char* szFilePath, const unsigned char* bytes, int width, int height)
    {
        unsigned char** image; // image[HEIGHT][WIDTH]
        int i, j;

        image = (png_bytepp)malloc(height * sizeof(png_bytep)); //
        for (j = 0; j < height; j++)
            image[j] = (png_bytep)malloc(width * 4 * sizeof(png_byte));
        for (j = 0; j < height; j++)
        {
            for (i = 0; i < width; i++)
            { //
                image[j][4 * i + 0] = bytes[4 * (j * width + i) + 0];
                image[j][4 * i + 1] = bytes[4 * (j * width + i) + 1];
                image[j][4 * i + 2] = bytes[4 * (j * width + i) + 2];
                image[j][4 * i + 3] = bytes[4 * (j * width + i) + 3];
            }
        }
        bool bRet = write_png(szFilePath, image, width, height);
        for (j = 0; j < height; j++)
            free(image[j]);
        free(image);
        return bRet;
    }

    static std::shared_ptr<image<color4> > ReadPNGColor4Image_(const char* szFilePath)
    {
        std::shared_ptr<image<color4> > img;

        png_byte header[8];
        FILE* fp;

        if ((fp = fopen(szFilePath, "rb")) == NULL) return img;

        // read the header
        fread(header, 1, 8, fp);

        if (png_sig_cmp(header, 0, 8))
        {
            fprintf(stderr, "error: %s is not a PNG.\n", szFilePath);
            fclose(fp);
            return img;
        }

        png_structp png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
        if (!png_ptr)
        {
            fprintf(stderr, "error: png_create_read_struct returned 0.\n");
            fclose(fp);
            return img;
        }

        // create png info struct
        png_infop info_ptr = png_create_info_struct(png_ptr);
        if (!info_ptr)
        {
            fprintf(stderr, "error: png_create_info_struct returned 0.\n");
            png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
            fclose(fp);
            return img;
        }

        // create png info struct
        png_infop end_info = png_create_info_struct(png_ptr);
        if (!end_info)
        {
            fprintf(stderr, "error: png_create_info_struct returned 0.\n");
            png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
            fclose(fp);
            return img;
        }

        // the code in this if statement gets called if libpng encounters an error
        if (setjmp(png_jmpbuf(png_ptr)))
        {
            fprintf(stderr, "error from libpng\n");
            png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
            fclose(fp);
            return img;
        }

        // init png reading
        png_init_io(png_ptr, fp);

        // let libpng know you already read the first 8 bytes
        png_set_sig_bytes(png_ptr, 8);

        // read all the info up to the image data
        png_read_info(png_ptr, info_ptr);

        // variables to pass to get info
        int bit_depth, color_type;
        png_uint_32 temp_width, temp_height;

        // get info about png
        png_get_IHDR(png_ptr, info_ptr, &temp_width, &temp_height, &bit_depth, &color_type,
                     NULL, NULL, NULL);

        switch (color_type)
        {
        case PNG_COLOR_TYPE_PALETTE:
            png_set_palette_to_rgb(png_ptr);
            break;
        case PNG_COLOR_TYPE_GRAY:
            if (bit_depth < 8)
            {
                png_set_expand_gray_1_2_4_to_8(png_ptr);
            }
            break;
        default:;
        }

        //if (width){ *width = temp_width; }
        //if (height){ *height = temp_height; }

        // Update the png info struct.
        png_read_update_info(png_ptr, info_ptr);
        // get info about png
        png_get_IHDR(png_ptr, info_ptr, &temp_width, &temp_height, &bit_depth, &color_type,
                     NULL, NULL, NULL);

        // Row size in bytes.
        int rowbytes = png_get_rowbytes(png_ptr, info_ptr);

        // glTexImage2d requires rows to be 4-byte aligned
        rowbytes += 3 - ((rowbytes - 1) % 4);

        // Allocate the image_data as a big block, to be given to opengl
        png_byte* image_data;
        image_data = (png_byte*)malloc(rowbytes * temp_height * sizeof(png_byte) + 15);
        if (image_data == NULL)
        {
            fprintf(stderr, "error: could not allocate memory for PNG image data\n");
            png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
            fclose(fp);
            return img;
        }

        // row_pointers is for pointing to image_data for reading the png with libpng
        png_bytep* row_pointers = (png_bytep*)malloc(temp_height * sizeof(png_bytep));
        if (row_pointers == NULL)
        {
            fprintf(stderr, "error: could not allocate memory for PNG row pointers\n");
            png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
            free(image_data);
            fclose(fp);
            return img;
        }

        // set the individual row_pointers to point at the correct offsets of image_data
        int i;
        for (i = 0; i < temp_height; i++)
        {
            row_pointers[temp_height - 1 - i] = image_data + i * rowbytes;
        }

        // read the png into image_data through row_pointers
        png_read_image(png_ptr, row_pointers);

        bool bUnknownFormat = false;
        std::shared_ptr<image<color4> > timg(new memory_image<color4>(temp_width, temp_height));
        {
            for (int y = 0; y < temp_height; y++)
            {
                const png_byte* line = row_pointers[y];
                for (int x = 0; x < temp_width; x++)
                {
                    if (color_type == PNG_COLOR_TYPE_GRAY)
                    {
                        unsigned char R = line[x];

                        timg->set(x, y, color4(R / 255.0, R / 255.0, R / 255.0, 1.0));
                    }
                    else if (color_type == PNG_COLOR_TYPE_RGB)
                    {
                        unsigned char R = line[3 * x + 0];
                        unsigned char G = line[3 * x + 1];
                        unsigned char B = line[3 * x + 2];
                        //unsigned char A = line[3*x+3];
                        timg->set(x, y, color4(R / 255.0, G / 255.0, B / 255.0, 1.0));
                    }
                    else if (color_type == PNG_COLOR_TYPE_RGB_ALPHA)
                    {
                        unsigned char R = line[4 * x + 0];
                        unsigned char G = line[4 * x + 1];
                        unsigned char B = line[4 * x + 2];
                        unsigned char A = line[4 * x + 3];
                        timg->set(x, y, color4(R / 255.0, G / 255.0, B / 255.0, A / 255.0));
                    }
                    else
                    {
                        bUnknownFormat = true;
                    }
                }
            }
        }
        img = timg;

        //if(bUnknownFormat)
        //{
        //    printf("UnknownFormat\n");
        //}

        // clean up
        png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
        free(image_data);
        free(row_pointers);
        fclose(fp);

        return img;
    }

    std::shared_ptr<image<color3> > ReadPNGColor3Image(const char* szFilePath)
    {
        return ConvertChannel(ReadPNGColor4Image_(szFilePath));
    }

    std::shared_ptr<image<color4> > ReadPNGColor4Image(const char* szFilePath)
    {
        return ReadPNGColor4Image_(szFilePath);
    }
#endif
}
