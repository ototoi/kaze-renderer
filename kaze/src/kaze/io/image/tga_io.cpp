#include "tga_io.h"
#include <stdio.h>
#include "memory_image.hpp"

//
//TGA
//from http://asura.iaigiri.com/OpenGL/gl5.html

namespace
{

    //typedef unsigned int uint32_t;
    //typedef unsigned char uint8_t;

    class TGAImage
    {
    public:
        uint32_t imageSize;
        uint8_t* imageData;
        //GLenum format;
        //uint32_t internalFormat;
        uint32_t width;
        uint32_t height;
        uint32_t bpp;

    public:
        uint32_t ID;
        TGAImage();
        ~TGAImage();
        bool ReadTGA(const char* filename);
    };

    //////////////////////////////////////////////////////////////////////////
    //�@�@TGAImage class
    //////////////////////////////////////////////////////////////////////////

    //-----------------------------------------------------------------------------------------------------
    //�@�@TGAImage
    //�@�@Desc : �R���X�g���N�^
    //-----------------------------------------------------------------------------------------------------
    TGAImage::TGAImage()
    {
        imageSize = 0;
        imageData = NULL;
        //format = GL_RGB;
        //internalFormat = GL_RGB;
        width = 0;
        height = 0;
        bpp = 0;
        ID = 0;
    }

    //-----------------------------------------------------------------------------------------------------
    //�@�@~TGAImage
    //�@�@Desc : �f�X�g���N�^
    //-----------------------------------------------------------------------------------------------------
    TGAImage::~TGAImage()
    {
        if (imageData)
        {
            delete[] imageData;
            imageData = NULL;
        }
    }

    //-----------------------------------------------------------------------------------------------------
    //�@�@ReadTGA
    //�@�@Desc : TGA�t�@�C���̓ǂݍ���
    //-----------------------------------------------------------------------------------------------------
    bool TGAImage::ReadTGA(const char* filename)
    {
        FILE* fp;
        uint8_t header[18];
        uint8_t bytePerPixel;
        uint32_t temp;

        //�@�t�@�C�����J��
        if ((fp = fopen(filename, "rb")) == NULL)
        {
            //cout << "Error : �w�肵���t�@�C�����J���܂����ł���\n";
            //cout << "File Name : " << filename << endl;
            return false;
        }

        //�@�w�b�_�[�����̓ǂݍ���
        fread(header, 1, sizeof(header), fp);

        //�@���ƍ��������߂�
        width = header[13] * 256 + header[12];
        height = header[15] * 256 + header[14];

        //�@�r�b�g�̐[��
        bpp = header[16];

        //�@24 bit
        if (bpp == 24)
        {
            //format = GL_RGB;
            //internalFormat = GL_RGB;
        }
        //�@32 bit
        else if (bpp = 32)
        {
            //format = GL_RGBA;
            //internalFormat = GL_RGBA;
        }

        //�@1�s�N�Z���������̃o�C�g��������
        bytePerPixel = bpp / 8;

        //�@�f�[�^�T�C�Y�̌���
        imageSize = width * height * bytePerPixel;

        //�@���������m��
        imageData = new uint8_t[imageSize];

        //�@�e�N�Z���f�[�^�����C�ɓǂݎ���
        fread(imageData, 1, imageSize, fp);

        //�@BGR(A)��RGB(A)�ɃR���o�[�g
        for (int i = 0; i < (int)imageSize; i += bytePerPixel)
        {
            temp = imageData[i];
            imageData[i + 0] = imageData[i + 2];
            imageData[i + 2] = temp;
        }

        //�@�t�@�C��������
        fclose(fp);

        return true;
    }
}

namespace kaze
{

    static std::shared_ptr<image<color3> > ConvertImage(const TGAImage& tga)
    {
        int width = tga.width;
        int height = tga.height;

        int bpp = tga.bpp;

        if (bpp == 24)
        {
            std::shared_ptr<image<color3> > img(new memory_image<color3>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    size_t index = (height - 1 - y) * width + x;
                    uint32_t r = tga.imageData[3 * index + 0];
                    uint32_t g = tga.imageData[3 * index + 1];
                    uint32_t b = tga.imageData[3 * index + 2];

                    color3 c = color3(r / 255.0, g / 255.0, b / 255.0);
                    img->set(x, y, c);
                }
            }
            return img;
        }
        else if (bpp == 32)
        {
            std::shared_ptr<image<color3> > img(new memory_image<color3>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    size_t index = (height - 1 - y) * width + x;
                    uint32_t r = tga.imageData[4 * index + 0];
                    uint32_t g = tga.imageData[4 * index + 1];
                    uint32_t b = tga.imageData[4 * index + 2];
                    uint32_t a = tga.imageData[4 * index + 3];

                    color3 c = color3(r / 255.0, g / 255.0, b / 255.0);
                    img->set(x, y, c);
                }
            }
            return img;
        }

        return std::shared_ptr<image<color3> >();
    }

    bool WriteTGARGBAImage(const char* szFilePath, const unsigned char* bytes, int width, int height)
    {
        return false;
    }

    std::shared_ptr<image<color3> > ReadTGAColor3Image(const char* szFilePath)
    {
        TGAImage tga;
        if (!tga.ReadTGA(szFilePath)) return std::shared_ptr<image<color3> >();
        return ConvertImage(tga);
    }
}