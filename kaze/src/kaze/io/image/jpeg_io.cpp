#include "jpeg_io.h"
#include <vector>

#include <assert.h>
#include <vector>
#include <string>
#include <stdio.h>

#include "memory_image.hpp"

#ifdef _WIN32
#include <shlobj.h>
#include <gdiplus.h>
#include <Gdiplusinit.h>
#include <windows.h>

#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "gdiplus.lib")

#else
#include <jpeglib.h>
#endif

namespace kaze
{
//---------------------------------------------------------------------------------
#ifdef _WIN32
    using namespace Gdiplus;

    static BOOL GetEncoderClsid(LPCWSTR lpszFormat, CLSID* pClsid)
    {
        assert(lpszFormat);
        assert(pClsid);
        UINT nEncoders;
        UINT nSize;
        if (Gdiplus::GetImageEncodersSize(&nEncoders, &nSize) != Gdiplus::Ok)
        {
            return FALSE;
        }
        std::vector<BYTE> _scoped(nSize);
        Gdiplus::ImageCodecInfo* pEncoders = reinterpret_cast<Gdiplus::ImageCodecInfo*>(&_scoped[0]);
        if (Gdiplus::GetImageEncoders(nEncoders, nSize, pEncoders) != Gdiplus::Ok)
        {
            return FALSE;
        }
        std::wstring strFormat(lpszFormat);
        for (UINT i = 0; i < nEncoders; ++i)
        {
            if (strFormat == pEncoders[i].MimeType)
            {
                *pClsid = pEncoders[i].Clsid;
                return TRUE;
            }
        }
        return FALSE;
    }

    static BOOL SaveByJPEG(LPCTSTR lpszFilePath, Gdiplus::Bitmap& img)
    {
        CLSID encoderClsid;
        GetEncoderClsid(L"image/jpeg", &encoderClsid);
        return (img.Save(lpszFilePath, &encoderClsid, NULL) == Gdiplus::Ok);
    }

    bool WriteJPEGRGBAImage(LPCWSTR szFilePath, const BYTE* bytes, int width, int height)
    {
        GdiplusStartupInput gdiplusStartupInput;
        ULONG_PTR gdiplusToken = NULL;
        if (Gdiplus::Ok != GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL)) return false;

        const bool botttom_up = true;
        const SIZE size = {width, height};
        const int stride = ((32 * width + 31) / 32) * 4;
        std::vector<BYTE> buffer(stride * height);
        for (int y = 0; y < size.cy; ++y)
        {
            for (int x = 0; x < size.cx; ++x)
            {
                const int p = stride * (botttom_up ? (size.cy - 1) - y : y) + (4 * x);
                size_t index = y * width + x;
                buffer[p + 0] = bytes[4 * index + 2]; // B
                buffer[p + 1] = bytes[4 * index + 1]; // G
                buffer[p + 2] = bytes[4 * index + 0]; // R
                buffer[p + 3] = bytes[4 * index + 3]; // A
            }
        }

        bool bRet = true;
        {
            Gdiplus::Bitmap bmp(
                size.cx, size.cy,
                (botttom_up ? -stride : stride),
                PixelFormat32bppARGB,
                &buffer[0] + (botttom_up ? stride * (size.cy - 1) : 0));

            if (!SaveByJPEG(szFilePath, bmp))
            {
                bRet = false;
            }
        }

        GdiplusShutdown(gdiplusToken);

        return bRet;
    }

    bool WriteJPEGRGBAImage(const char* szFilePath, const BYTE* bytes, int width, int height)
    {
        WCHAR buffer[_MAX_PATH] = {};
        int nRet = ::MultiByteToWideChar(CP_OEMCP, 0, szFilePath, (int)strlen(szFilePath) + 1, buffer, _MAX_PATH);
        if (nRet == 0) return false;
        return WriteJPEGRGBAImage(buffer, bytes, width, height);
    }

    static std::shared_ptr<image<color3> > ReadJPEGColor3Image(LPCWSTR szFilePath)
    {
        std::shared_ptr<image<color3> > img;

        GdiplusStartupInput gdiplusStartupInput;
        ULONG_PTR gdiplusToken = NULL;
        if (Gdiplus::Ok != GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL)) return img;

        Bitmap* bmp = Bitmap::FromFile(szFilePath);
        if (bmp)
        {
            int width = bmp->GetWidth();
            int height = bmp->GetHeight();

            if (width * height)
            {
                std::shared_ptr<image<color3> > timg(new memory_image<color3>(width, height));

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        Color c;
                        bmp->GetPixel(x, y, &c);
                        BYTE A = c.GetA();
                        BYTE R = c.GetR();
                        BYTE G = c.GetG();
                        BYTE B = c.GetB();
                        timg->set(x, y, color3(R / 255.0, G / 255.0, B / 255.0));
                    }
                }

                img = timg;
            }

            delete bmp;
            bmp = 0;
        }
        GdiplusShutdown(gdiplusToken);

        return img;
    }

    static std::shared_ptr<image<color4> > ReadJPEGColor4Image(LPCWSTR szFilePath)
    {
        std::shared_ptr<image<color4> > img;

        GdiplusStartupInput gdiplusStartupInput;
        ULONG_PTR gdiplusToken = NULL;
        if (Gdiplus::Ok != GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL)) return img;

        Bitmap* bmp = Bitmap::FromFile(szFilePath);
        if (bmp)
        {
            int width = bmp->GetWidth();
            int height = bmp->GetHeight();

            if (width * height)
            {
                std::shared_ptr<image<color4> > timg(new memory_image<color4>(width, height));

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        Color c;
                        bmp->GetPixel(x, y, &c);
                        BYTE A = c.GetA();
                        BYTE R = c.GetR();
                        BYTE G = c.GetG();
                        BYTE B = c.GetB();
                        timg->set(x, y, color4(R / 255.0, G / 255.0, B / 255.0, A / 255.0));
                    }
                }

                img = timg;
            }

            delete bmp;
            bmp = 0;
        }
        GdiplusShutdown(gdiplusToken);

        return img;
    }

    std::shared_ptr<image<color3> > ReadJPEGColor3Image(const char* szFilePath)
    {
        WCHAR buffer[_MAX_PATH] = {};
        int nRet = ::MultiByteToWideChar(CP_OEMCP, 0, szFilePath, (int)strlen(szFilePath) + 1, buffer, _MAX_PATH);
        if (nRet == 0) return std::shared_ptr<image<color3> >();
        return ReadJPEGColor3Image(buffer);
    }

    std::shared_ptr<image<color4> > ReadJPEGColor4Image(const char* szFilePath)
    {
        WCHAR buffer[_MAX_PATH] = {};
        int nRet = ::MultiByteToWideChar(CP_OEMCP, 0, szFilePath, (int)strlen(szFilePath) + 1, buffer, _MAX_PATH);
        if (nRet == 0) return std::shared_ptr<image<color4> >();
        return ReadJPEGColor4Image(buffer);
    }

#else

    static std::shared_ptr<image<color3> > ReadJPEGColor3Image_(const char* szFilePath)
    {
        FILE* fp = fopen(szFilePath, "rb");
        if (!fp) return std::shared_ptr<image<color3> >();

        struct jpeg_decompress_struct cinfo;
        struct jpeg_error_mgr jerr;

        cinfo.err = jpeg_std_error(&jerr);
        jpeg_create_decompress(&cinfo);
        jpeg_stdio_src(&cinfo, fp);
        jpeg_read_header(&cinfo, TRUE);

        jpeg_start_decompress(&cinfo);

        JSAMPARRAY buffer = (JSAMPARRAY)malloc(sizeof(JSAMPROW) * cinfo.output_height);
        for (int i = 0; i < cinfo.output_height; ++i)
        {
            buffer[i] = (JSAMPROW)calloc(sizeof(JSAMPLE), cinfo.output_width * cinfo.output_components);
        }

        while (cinfo.output_scanline < cinfo.output_height)
        {
            jpeg_read_scanlines(&cinfo,
                                buffer + cinfo.output_scanline,
                                cinfo.output_height - cinfo.output_scanline);
        }

        jpeg_finish_decompress(&cinfo);
        jpeg_destroy_decompress(&cinfo);

        fclose(fp);

        int width = cinfo.output_width;
        int height = cinfo.output_height;

        std::shared_ptr<image<color3> > img;

        if (width * height)
        {
            std::shared_ptr<image<color3> > timg(new memory_image<color3>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    unsigned char R = buffer[y][3 * x + 0];
                    unsigned char G = buffer[y][3 * x + 1];
                    unsigned char B = buffer[y][3 * x + 2];

                    timg->set(x, y, color3(R / 255.0, G / 255.0, B / 255.0));
                }
            }

            img = timg;
        }

        for (int i = 0; i < cinfo.output_height; ++i)
        {
            free(buffer[i]);
        }
        free(buffer);

        return img;
    }

    static std::shared_ptr<image<color4> > ReadJPEGColor4Image_(const char* szFilePath)
    {
        FILE* fp = fopen(szFilePath, "rb");
        if (!fp) return std::shared_ptr<image<color4> >();

        struct jpeg_decompress_struct cinfo;
        struct jpeg_error_mgr jerr;

        cinfo.err = jpeg_std_error(&jerr);
        jpeg_create_decompress(&cinfo);
        jpeg_stdio_src(&cinfo, fp);
        jpeg_read_header(&cinfo, TRUE);

        jpeg_start_decompress(&cinfo);

        JSAMPARRAY buffer = (JSAMPARRAY)malloc(sizeof(JSAMPROW) * cinfo.output_height);
        for (int i = 0; i < cinfo.output_height; ++i)
        {
            buffer[i] = (JSAMPROW)calloc(sizeof(JSAMPLE), cinfo.output_width * cinfo.output_components);
        }

        while (cinfo.output_scanline < cinfo.output_height)
        {
            jpeg_read_scanlines(&cinfo,
                                buffer + cinfo.output_scanline,
                                cinfo.output_height - cinfo.output_scanline);
        }

        jpeg_finish_decompress(&cinfo);
        jpeg_destroy_decompress(&cinfo);

        fclose(fp);

        int width = cinfo.output_width;
        int height = cinfo.output_height;

        std::shared_ptr<image<color4> > img;

        if (width * height)
        {
            std::shared_ptr<image<color4> > timg(new memory_image<color4>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    unsigned char R = buffer[y][3 * x + 0];
                    unsigned char G = buffer[y][3 * x + 1];
                    unsigned char B = buffer[y][3 * x + 2];

                    timg->set(x, y, color4(R / 255.0, G / 255.0, B / 255.0, 1));
                }
            }

            img = timg;
        }

        for (int i = 0; i < cinfo.output_height; ++i)
        {
            free(buffer[i]);
        }
        free(buffer);

        return img;
    }

    bool WriteJPEGRGBAImage(const char* szFilePath, const unsigned char* bytes, int width, int height)
    {
        /*TODO*/
        return true;
    }

    std::shared_ptr<image<color3> > ReadJPEGColor3Image(const char* szFilePath)
    {
        return ReadJPEGColor3Image_(szFilePath);
    }

    std::shared_ptr<image<color4> > ReadJPEGColor4Image(const char* szFilePath)
    {
        return ReadJPEGColor4Image_(szFilePath);
    }
#endif
}
