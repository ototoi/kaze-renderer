#include "zfile.h"

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

namespace kaze
{

    static const char ZMAGIC[4] = "KZZ";
    struct zfile_header
    {
        char magic[4];
        int width;
        int height;
        double w2c[16];
        double w2s[16];
        char reserve[64];
    };

    int write_zfile_image(const char* szFilePath, int width, int height, const double pixels[], const double w2c[16], const double w2s[16])
    {
        FILE* fp = fopen(szFilePath, "wb");
        if (!fp) return -1;

        zfile_header header;
        memset(&header, 0, sizeof(zfile_header));
        memcpy(&header.magic, ZMAGIC, sizeof(char) * 4);
        header.width = width;
        header.height = height;
        memcpy(header.w2c, w2c, sizeof(double) * 16);
        memcpy(header.w2s, w2s, sizeof(double) * 16);

        if (1 != fwrite(&header, sizeof(zfile_header), 1, fp))
        {
            fclose(fp);
            return -1;
        }

        for (int y = 0; y < height; y++)
        {
            if (1 != fwrite(pixels + y * width, sizeof(double) * width, 1, fp))
            {
                fclose(fp);
                return -1;
            }
        }

        fclose(fp);

        return 0;
    }

    int read_zfile_image(const char* szFilePath, int& width, int& height, std::vector<double>& pixels, double w2c[16], double w2s[16])
    {
        FILE* fp = fopen(szFilePath, "rb");
        if (!fp) return -1;

        zfile_header header;
        memset(&header, 0, sizeof(zfile_header));

        if (1 != fread(&header, sizeof(zfile_header), 1, fp))
        {
            fclose(fp);
            return -1;
        }

        width = header.width;
        height = header.height;
        pixels.resize(width * height);

        memcpy(w2c, header.w2c, sizeof(double) * 16);
        memcpy(w2s, header.w2s, sizeof(double) * 16);

        for (int y = 0; y < height; y++)
        {
            if (1 != fread(&pixels[0] + y * width, sizeof(double) * width, 1, fp))
            {
                fclose(fp);
                return -1;
            }
        }

        fclose(fp);

        return 0;
    }

    int zfile_image::get_width() const
    {
        return width_;
    }
    int zfile_image::get_height() const
    {
        return height_;
    }
    bool zfile_image::load(const char* szFilePath)
    {
        int nRet = read_zfile_image(szFilePath, width_, height_, buffer_, w2c_, w2s_);
        return (nRet == 0);
    }

    bool zfile_image::save(const char* szFilePath)
    {
        int nRet = write_zfile_image(szFilePath, get_width(), get_height(), get_buffer(), get_w2c(), get_w2s());
        return (nRet == 0);
    }

    const double* zfile_image::get_buffer() const
    {
        if (buffer_.empty()) return NULL;
        return &buffer_[0];
    }
    const double* zfile_image::get_w2c() const
    {
        return w2c_;
    }
    const double* zfile_image::get_w2s() const
    {
        return w2s_;
    }

    void zfile_image::set_buffer_raw(const std::vector<double>& buffer)
    {
        buffer_ = buffer;
    }

    const std::vector<double>& zfile_image::get_buffer_raw() const
    {
        return buffer_;
    }
}