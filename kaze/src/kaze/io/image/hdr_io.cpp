#include "hdr_io.h"

#include <assert.h>
#include <vector>
#include <string>

#include "memory_image.hpp"

#include <stdio.h>
#include <stdlib.h>

extern "C" {
#include "rgbe.h"
}

namespace kaze
{

    template <class T>
    struct Col
    {
        static T get(real r, real g, real b, real a) { return T(r, g, b); }
    };

    template <>
    struct Col<color4>
    {
        static color4 get(real r, real g, real b, real a) { return color4(r, g, b, a); }
    };

    template <class T>
    static std::shared_ptr<image<T> > ReadHDRColorTImage(const char* szFilePath)
    {
        std::shared_ptr<image<T> > out;
        float* image = NULL;
        FILE* f = fopen(szFilePath, "rb");
        int width = 1;
        int height = 1;
        int nRet = 0;
        if (f == NULL) goto FINALIZE;
        nRet = RGBE_ReadHeader(f, &width, &height, NULL);
        if (nRet != 0) goto FINALIZE;
        image = (float*)malloc(sizeof(float) * 3 * width * height);
        RGBE_ReadPixels_RLE(f, image, width, height);
        out.reset(new memory_image<T>(width, height));
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                size_t idx = y * width + x;
                float r = image[3 * idx + 0];
                float g = image[3 * idx + 1];
                float b = image[3 * idx + 2];
                float a = 1.0f;
                out->set(x, y, Col<T>::get(r, g, b, a));
            }
        }
    FINALIZE:
        if (f) fclose(f);
        if (image) free(image);
        return out;
    }

    static bool CheckHeader(rgbe_header_info* info, const char* szFilePath)
    {
        bool bRet = false;
        FILE* f = fopen(szFilePath, "rb");
        int width = 1;
        int height = 1;
        int nRet = 0;
        if (f == NULL) goto FINALIZE;
        nRet = RGBE_ReadHeader(f, &width, &height, info);
        if (nRet != 0) goto FINALIZE;
        bRet = true;
    FINALIZE:
        if (f) fclose(f);
        return bRet;
    }

    template <class T>
    static bool WriteHDRColorTImage(const char* szFilePath, const std::shared_ptr<image<T> >& img)
    {
        bool bRet = false;
        rgbe_header_info info;
        memset(&info, 0, sizeof(rgbe_header_info));
        bool bChecked = CheckHeader(&info, szFilePath);

        float* image = NULL;
        FILE* f = fopen(szFilePath, "wb");
        int width = img->get_width();
        int height = img->get_height();
        int nRet = 0;
        if (f == NULL) goto FINALIZE;
        if (bChecked)
        {
            nRet = RGBE_WriteHeader(f, width, height, &info);
        }
        else
        {
            strcpy(info.programtype, "RGBE");
            info.valid = RGBE_VALID_PROGRAMTYPE | RGBE_VALID_GAMMA | RGBE_VALID_EXPOSURE;
            info.gamma = 1.0f;
            info.exposure = 1.0f;
            nRet = RGBE_WriteHeader(f, width, height, &info);
        }
        image = (float*)malloc(sizeof(float) * 3 * width * height);
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                size_t idx = y * width + x;
                T c = img->get(x, y);
                image[3 * idx + 0] = (float)c[0];
                image[3 * idx + 1] = (float)c[1];
                image[3 * idx + 2] = (float)c[2];
            }
        }
        nRet = RGBE_WritePixels_RLE(f, image, width, height);
        if (nRet != 0) goto FINALIZE;
        bRet = true;
    FINALIZE:
        if (f) fclose(f);
        if (image) free(image);
        return bRet;
    }

    std::shared_ptr<image<color3> > ReadHDRColor3Image(const char* szFilePath)
    {
        return ReadHDRColorTImage<color3>(szFilePath);
    }
    std::shared_ptr<image<color4> > ReadHDRColor4Image(const char* szFilePath)
    {
        return ReadHDRColorTImage<color4>(szFilePath);
    }

    bool WriteHDRColor3Image(const char* szFilePath, const std::shared_ptr<image<color3> >& img)
    {
        return WriteHDRColorTImage<color3>(szFilePath, img);
    }

    bool WriteHDRColor4Image(const char* szFilePath, const std::shared_ptr<image<color4> >& img)
    {
        return WriteHDRColorTImage<color4>(szFilePath, img);
    }
}
