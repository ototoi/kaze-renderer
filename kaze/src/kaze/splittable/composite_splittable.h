#ifndef KAZE_COMPOSITE_SPLITTABLE_H
#define KAZE_COMPOSITE_SPLITTABLE_H

#include "splittable.h"
#include <vector>

namespace kaze
{

    class composite_splittable : public splittable
    {
    public:
        composite_splittable(splittable* c);
        ~composite_splittable();

    public:
        void add(splittable* c);

    public:
        void split_u(splittable* child[2]) const;
        void split_v(splittable* child[2]) const;
        void split(splittable* child[4]) const;

    protected:
        std::vector<splittable*> c_;
    };
}

#endif