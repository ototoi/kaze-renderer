#include "composite_splittable.h"

namespace kaze
{

    composite_splittable::composite_splittable(splittable* c)
    {
        c_.push_back(c);
    }

    composite_splittable::~composite_splittable()
    {
        size_t csz = c_.size();
        for (size_t i = 0; i < csz; i++)
        {
            delete c_[i];
        }
    }

    void composite_splittable::add(splittable* c)
    {
        c_.push_back(c);
    }

    void composite_splittable::split_u(splittable* child[2]) const
    {
        size_t csz = c_.size();
        composite_splittable* cx[2];
        splittable* cc[2];
        c_[0]->split_u(cc);
        cx[0] = new composite_splittable(cc[0]);
        cx[1] = new composite_splittable(cc[1]);
        for (size_t i = 1; i < csz; i++)
        {
            c_[i]->split_u(cc);
            cx[0]->add(cc[0]);
            cx[1]->add(cc[1]);
        }
        child[0] = cx[0];
        child[1] = cx[1];
    }
    void composite_splittable::split_v(splittable* child[2]) const
    {
        size_t csz = c_.size();
        composite_splittable* cx[2];
        splittable* cc[2];
        c_[0]->split_v(cc);
        cx[0] = new composite_splittable(cc[0]);
        cx[1] = new composite_splittable(cc[1]);
        for (size_t i = 1; i < csz; i++)
        {
            c_[i]->split_v(cc);
            cx[0]->add(cc[0]);
            cx[1]->add(cc[1]);
        }
        child[0] = cx[0];
        child[1] = cx[1];
    }
    void composite_splittable::split(splittable* child[4]) const
    {
        size_t csz = c_.size();
        composite_splittable* cx[4];
        splittable* cc[4];
        c_[0]->split(cc);
        cx[0] = new composite_splittable(cc[0]);
        cx[1] = new composite_splittable(cc[1]);
        cx[2] = new composite_splittable(cc[2]);
        cx[3] = new composite_splittable(cc[3]);
        for (size_t i = 1; i < csz; i++)
        {
            c_[i]->split(cc);
            cx[0]->add(cc[0]);
            cx[1]->add(cc[1]);
            cx[2]->add(cc[2]);
            cx[3]->add(cc[3]);
        }
        child[0] = cx[0];
        child[1] = cx[1];
        child[2] = cx[2];
        child[3] = cx[3];
    }
}