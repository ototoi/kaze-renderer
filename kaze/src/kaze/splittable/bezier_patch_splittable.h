#ifndef KAZE_BEZIER_PATCH_SPLITTABLE_H
#define KAZE_BEZIER_PATCH_SPLITTABLE_H

#include "splittable.h"
#include "bezier_patch.hpp"
#include <vector>

namespace kaze
{

    class bezier_patch_splittable : public splittable
    {
    public:
        bezier_patch_splittable(const bezier_patch<real>& patch);
        bezier_patch_splittable(const bezier_patch<vector2>& patch);
        bezier_patch_splittable(const bezier_patch<vector3>& patch);
        ~bezier_patch_splittable();

        void split_u(splittable* child[2]) const;
        void split_v(splittable* child[2]) const;
        void split(splittable* child[4]) const;

    protected:
        bezier_patch_splittable(splittable* P);

    protected:
        splittable* P_;
    };
}

#endif