#ifndef KAZE_SPLITTABLE_H
#define KAZE_SPLITTABLE_H

#include "types.h"

namespace kaze
{

    class splittable
    {
    public:
        virtual ~splittable() {}
        virtual void split_u(splittable* child[2]) const = 0;
        virtual void split_v(splittable* child[2]) const = 0;
        virtual void split(splittable* child[4]) const = 0;
    };
}

#endif