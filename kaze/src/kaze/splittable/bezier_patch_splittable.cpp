#include "bezier_patch_splittable.h"

namespace kaze
{

    template <class T>
    class bezier_patch_splittable_base : public splittable
    {
    public:
        bezier_patch_splittable_base(const bezier_patch<T>& patch) : patch_(patch) {}
    public:
        void split_u(splittable* child[2]) const
        {
            bezier_patch<T> c[2];
            patch_.split_u(c, 0.5);
            child[0] = new bezier_patch_splittable_base<T>(c[0]);
            child[1] = new bezier_patch_splittable_base<T>(c[1]);
        }
        void split_v(splittable* child[2]) const
        {
            bezier_patch<T> c[2];
            patch_.split_v(c, 0.5);
            child[0] = new bezier_patch_splittable_base<T>(c[0]);
            child[1] = new bezier_patch_splittable_base<T>(c[1]);
        }
        void split(splittable* child[4]) const
        {
            bezier_patch<T> c[4];
            patch_.split(c, 0.5, 0.5);
            child[0] = new bezier_patch_splittable_base<T>(c[0]);
            child[1] = new bezier_patch_splittable_base<T>(c[1]);
            child[2] = new bezier_patch_splittable_base<T>(c[2]);
            child[3] = new bezier_patch_splittable_base<T>(c[3]);
        }

    public:
        bezier_patch<T> patch_;
    };

    bezier_patch_splittable::bezier_patch_splittable(const bezier_patch<real>& patch)
    {
        P_ = new bezier_patch_splittable_base<real>(patch);
    }

    bezier_patch_splittable::bezier_patch_splittable(const bezier_patch<vector2>& patch)
    {
        P_ = new bezier_patch_splittable_base<vector2>(patch);
    }

    bezier_patch_splittable::bezier_patch_splittable(const bezier_patch<vector3>& patch)
    {
        P_ = new bezier_patch_splittable_base<vector3>(patch);
    }

    bezier_patch_splittable::~bezier_patch_splittable()
    {
        delete P_;
    }

    void bezier_patch_splittable::split_u(splittable* child[2]) const
    {
        splittable* Ps[2];
        P_->split_u(Ps);
        child[0] = new bezier_patch_splittable(Ps[0]);
        child[1] = new bezier_patch_splittable(Ps[1]);
    }

    void bezier_patch_splittable::split_v(splittable* child[2]) const
    {
        splittable* Ps[2];
        P_->split_v(Ps);
        child[0] = new bezier_patch_splittable(Ps[0]);
        child[1] = new bezier_patch_splittable(Ps[1]);
    }

    void bezier_patch_splittable::split(splittable* child[4]) const
    {
        splittable* Ps[4];
        P_->split(Ps);
        child[0] = new bezier_patch_splittable(Ps[0]);
        child[1] = new bezier_patch_splittable(Ps[1]);
        child[2] = new bezier_patch_splittable(Ps[2]);
        child[3] = new bezier_patch_splittable(Ps[3]);
    }

    bezier_patch_splittable::bezier_patch_splittable(splittable* P)
        : P_(P)
    {
        ;
    }
}
