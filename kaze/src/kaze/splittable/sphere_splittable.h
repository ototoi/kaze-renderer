#ifndef KAZE_SPHERE_SPLITTABLE_H
#define KAZE_SPHERE_SPLITTABLE_H

#include "splittable.h"

namespace kaze
{

    sphere_splittable_core;

    class sphere_splittable : public splittable
    {
    public:
        sphere_splittable(const matrix4& mat);
        ~sphere_splittable();

    public:
        void split_u(splittable* child[2]) const;
        void split_v(splittable* child[2]) const;
        void split(splittable* child[4]) const;

    protected:
        real u0;
        real u1;
        real v0;
        real v1;
        std::shared_ptr<sphere_splittable_core> core_;
    };
}

#endif