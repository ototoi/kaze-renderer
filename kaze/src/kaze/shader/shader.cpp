
#include "shader.h"
#include "values.h"

#include "color.h"

namespace kaze
{

    spectrum surface_shader::shade(const enumerator<radiance>& rads, const sufflight& sl, const tracer& tr, const mediation& md) const
    {
        return shade(rads, sl, md);
    }

    spectrum surface_shader::shade(const enumerator<radiance>& rads, const sufflight& sl, const mediation& md) const
    {
        return spectrum(color_black());
    }

    //nodef
    //void shader::set_tracer(const tracer* tr){}
    spectrum shadow_shader::shade(const radiance& rad, const sufflight& sl, const mediation& md) const
    {
        return spectrum(color_black());
    }

    spectrum shadow_shader::shade(const enumerator<radiance>& rads, const sufflight& sl, const mediation& md) const
    {
        spectrum tmp(color_black());
        size_t sz = rads.size();
        for (size_t i = 0; i < sz; i++)
        {
            tmp += shade(rads[i], sl, md);
        }
        return tmp;
    }
}
