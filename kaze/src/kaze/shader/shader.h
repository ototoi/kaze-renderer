#ifndef KAZE_SHADER_H
#define KAZE_SHADER_H

#include "ray.h"
#include "sufflight.h"
#include "mediation.h"

#include "radiance.h"
#include "tracer.h"

#include "enumerator.hpp"
#include "spectrum.h"

namespace kaze
{

    class surface_shader;
    class shadow_shader;
    class volume_shader;

    class shader
    {
    public:
        virtual ~shader() {}
    public:
        virtual const surface_shader* get_surface_shader() const { return NULL; }
        virtual const shadow_shader* get_shadow_shader() const { return NULL; }
        virtual const volume_shader* get_volume_shader() const { return NULL; }
    };

    class surface_shader : public shader
    {
    public:
        virtual const surface_shader* get_surface_shader() const { return this; }
    public:
        virtual spectrum shade(const enumerator<radiance>& rads, const sufflight& sl, const tracer& tr, const mediation& md) const;
        virtual spectrum shade(const enumerator<radiance>& rads, const sufflight& sl, const mediation& md) const;
    };

    class shadow_shader : public shader
    {
    public:
        virtual const shadow_shader* get_shadow_shader() const { return this; }
    public:
        virtual spectrum shade(const radiance& rad, const sufflight& sl, const mediation& md) const;
        virtual spectrum shade(const enumerator<radiance>& rads, const sufflight& sl, const mediation& md) const;
    };

    class volume_shader : public shader
    {
    public:
        virtual const volume_shader* get_volume_shader() const { return this; }
    };
}

#endif