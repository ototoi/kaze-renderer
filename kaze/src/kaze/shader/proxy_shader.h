#ifndef KAZE_PROXY_SHADER_H
#define KAZE_PROXY_SHADER_H

#include "shader.h"

namespace kaze
{

    class proxy_shader : public shader
    {
    public:
        proxy_shader(const shader* shdr) : shdr_(shdr) {}
    public:
        virtual const surface_shader* get_surface_shader() const { return shdr_->get_surface_shader(); }
        virtual const shadow_shader* get_shadow_shader() const { return shdr_->get_shadow_shader(); }
        virtual const volume_shader* get_volume_shader() const { return shdr_->get_olume_shader(); }
    private:
        const shader* shdr_;
    };
}

#endif