#include "tone_mapper.h"

namespace kaze
{

    void normalize_tone_mapper::convert(real* img, int width, int height) const
    {
        real min = 1;
        real max = 0;

        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                size_t idx = y * width + x;
                real R = img[3 * idx + 0];
                real G = img[3 * idx + 1];
                real B = img[3 * idx + 2];
                if (R < min) min = R;
                if (G < min) min = G;
                if (B < min) min = B;

                if (R > max) max = R;
                if (G > max) max = G;
                if (B > max) max = B;
            }
        }
        //
        real d = 1 / (max - min);

        //
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                size_t idx = y * width + x;
                real R = img[3 * idx + 0];
                real G = img[3 * idx + 1];
                real B = img[3 * idx + 2];
                R = (R - min) * d;
                G = (G - min) * d;
                B = (B - min) * d;
                img[3 * idx + 0] = R;
                img[3 * idx + 1] = G;
                img[3 * idx + 2] = B;
            }
        }
    }

    //--------------------------------------------------------------------
    //FROM
    /**
 * Copyright (C) 2003 Billy Biggs <vektor@dumbterm.net>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * Non-linear response compression function from this paper:
 *   G.M. Johnson and M.D. Fairchild.  Rendering HDR Images.
 *   IS&T/SID 11th Color Imaging Conference. 36-41 (2003)
 */
    static double calculate_fl_from_la_ciecam02(double la)
    {
        double la5 = la * 5.0;
        double k = 1.0 / (la5 + 1.0);

        /* Calculate k^4. */
        k = k * k;
        k = k * k;

        return (0.2 * k * la5) + (0.1 * (1.0 - k) * (1.0 - k) * pow(la5, 1.0 / 3.0));
    }

    static void xyz_to_srgb(double* r, double* g, double* b,
                            double x, double y, double z)
    {
        *r = (3.2406 * x) + (-1.5372 * y) + (-0.4986 * z);
        *g = (-0.9689 * x) + (1.8758 * y) + (0.0415 * z);
        *b = (0.0557 * x) + (-0.2040 * y) + (1.0570 * z);
    }

    static void srgb_to_xyz(double* x, double* y, double* z,
                            double r, double g, double b)
    {
        *x = (0.4124 * r) + (0.3576 * g) + (0.1805 * b);
        *y = (0.2126 * r) + (0.7152 * g) + (0.0722 * b);
        *z = (0.0193 * r) + (0.1192 * g) + (0.9505 * b);
    }

    static void lms_to_xyz(double* x, double* y, double* z,
                           double l, double m, double s)
    {
        *x = (1.85024 * l) - (1.13830 * m) + (0.23843 * s);
        *y = (0.36683 * l) + (0.64388 * m) - (0.01067 * s);
        *z = (1.08885 * s);
    }

    static void xyz_to_lms(double* l, double* m, double* s,
                           double x, double y, double z)
    {
        *l = (0.4002 * x) + (0.7075 * y) - (0.0807 * z);
        *m = (-0.2280 * x) + (1.1500 * y) + (0.0612 * z);
        *s = (0.9184 * z);
    }

    void icam_tone_mapper::convert(real* img, int width, int height) const
    {
    }
}
