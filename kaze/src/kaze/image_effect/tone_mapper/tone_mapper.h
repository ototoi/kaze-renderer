#ifndef KAZE_TONE_MAPPER_H
#define KAZE_TONE_MAPPER_H

#include "color.h"

namespace kaze
{

    class tone_mapper
    {
    public:
        virtual ~tone_mapper() {}
        //virtual color3 convert(const color3& c)const;
        virtual void convert(real* img, int width, int height) const = 0;
    };

    class normalize_tone_mapper : public tone_mapper
    {
    public:
        void convert(real* img, int width, int height) const;
    };

    class icam_tone_mapper : public tone_mapper
    {
    public:
        void convert(real* img, int width, int height) const;
    };
}

#endif
