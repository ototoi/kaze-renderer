#include "corresponder.h"

//#include "values.h"

#include <math.h>
#include <assert.h>
#include <limits>

namespace kaze
{

    static inline bool is_powerof2(unsigned int x)
    {
        return ((x - 1) & x) == 0;
    }

    using namespace std;

    double correspond_repeat(double x)
    {
        double i = floor(x);
        double f = x - i;
        return f;
    }

    double correspond_mirror(double x)
    {
        static const double EPSILON = std::numeric_limits<double>::epsilon();
        static const double LESS1 = 1.0 - EPSILON;
        /*
		double i = floor(x);//1 ->1
		int    d = (int)i;
		double f = (d&1)?ceil(x+EPSILON)-x:x-i;
		return f;
		*/
        double i = floor(x); //1 ->1
        int d = (int)i;
        double f = x - i;
        if (d & 1)
        {
            return LESS1 - f; //1-EPS-x+i//ceil(x)-EPS-x
        }
        else
        {
            return f;
        }
    }

    double correspond_clamp(double x)
    {
        static const double EPSILON = std::numeric_limits<double>::epsilon();
        static const double LESS1 = 1.0 - EPSILON;

        if (x < 0) return 0;
        if (1.0 <= x) return LESS1;
        return x;
    }

    //--------------------------------------
    //--------------------------------------

    //---------------------------------------------
    int repeat_corresponder::get(int x, int w) const { return static_get(x, w); }
    corresponder* repeat_corresponder::clone_powerof2() const { return new powerof2_repeat_corresponder(); }
    corresponder* repeat_corresponder::clone_general() const { return new repeat_corresponder(); }
    corresponder* repeat_corresponder::clone() const { return new repeat_corresponder(); }
    //int repeat_corresponder::static_get(int x, int w) { return correspond_repeat(x, w); }
    //---------------------------------------------
    int mirror_corresponder::get(int x, int w) const { return static_get(x, w); }
    corresponder* mirror_corresponder::clone_powerof2() const { return new powerof2_mirror_corresponder(); }
    corresponder* mirror_corresponder::clone_general() const { return new mirror_corresponder(); }
    corresponder* mirror_corresponder::clone() const { return new mirror_corresponder(); }
    //int mirror_corresponder::static_get(int x, int w) { return correspond_mirror(x, w); }
    //---------------------------------------------
    int clamp_corresponder::get(int x, int w) const { return static_get(x, w); }
    corresponder* clamp_corresponder::clone_powerof2() const { return new powerof2_clamp_corresponder(); }
    corresponder* clamp_corresponder::clone_general() const { return new clamp_corresponder(); }
    corresponder* clamp_corresponder::clone() const { return new clamp_corresponder(); }
    //int clamp_corresponder::static_get(int x, int w) { return correspond_clamp(x, w); }
    //---------------------------------------------

    //---------------------------------------------
    int powerof2_repeat_corresponder::get(int x, int w) const { return static_get(x, w); }
    corresponder* powerof2_repeat_corresponder::clone_powerof2() const { return new powerof2_repeat_corresponder(); }
    corresponder* powerof2_repeat_corresponder::clone_general() const { return new repeat_corresponder(); }
    corresponder* powerof2_repeat_corresponder::clone() const { return new powerof2_repeat_corresponder(); }
    //int powerof2_repeat_corresponder::static_get(int x, int w) { return correspond_repeat_powerof2(x, w); }
    //---------------------------------------------
    int powerof2_mirror_corresponder::get(int x, int w) const { return static_get(x, w); }
    corresponder* powerof2_mirror_corresponder::clone_powerof2() const { return new powerof2_mirror_corresponder(); }
    corresponder* powerof2_mirror_corresponder::clone_general() const { return new mirror_corresponder(); }
    corresponder* powerof2_mirror_corresponder::clone() const { return new powerof2_mirror_corresponder(); }
    //int powerof2_mirror_corresponder::static_get(int x, int w) { return correspond_mirror_powerof2(x, w); }
    //---------------------------------------------
    int powerof2_clamp_corresponder::get(int x, int w) const { return static_get(x, w); }
    corresponder* powerof2_clamp_corresponder::clone_powerof2() const { return new powerof2_clamp_corresponder(); }
    corresponder* powerof2_clamp_corresponder::clone_general() const { return new clamp_corresponder(); }
    corresponder* powerof2_clamp_corresponder::clone() const { return new powerof2_clamp_corresponder(); }
    //int powerof2_clamp_corresponder::static_get(int x, int w) { return correspond_clamp_powerof2(x, w); }
    //---------------------------------------------
}
