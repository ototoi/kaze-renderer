#include "phase_function.h"
#include "values.h"

namespace kaze
{

    hg_phase_function::hg_phase_function(real g) : g_(g) {}

    real hg_phase_function::f(const vector3& wi, const vector3& wo) const
    {
        static const real I4 = 1.0 / (4.0 * values::pi());
        real g = g_;
        real gg = g * g;
        return I4 * (1 - gg) / std::pow(1 + gg - 2 * g * dot(wi, wo), (real)1.5);
    }

    real kajiya_kay_specular_phase_function::f(const vector3& wi, const vector3& wo) const
    {
        /*
		Frame frame(mRec.orientation);
		if (mRec.orientation.length() == 0)
			return m_kd / (4*M_PI);

		Vector orientation = normalize(mRec.orientation);
		Vector reflectedLocal = frame.toLocal(wo);
		reflectedLocal.z = -dot(wi, orientation);
		Float a = std::sqrt((1-reflectedLocal.z*reflectedLocal.z) / 
			(reflectedLocal.x*reflectedLocal.x + reflectedLocal.y*reflectedLocal.y));
		reflectedLocal.y *= a;
		reflectedLocal.x *= a;
		Vector R = frame.toWorld(reflectedLocal);

		return (std::pow(std::max((Float) 0, dot(R, wo)), m_exponent))
			* m_normalization * m_ks + m_kd / (4*M_PI);
		*/
        return 1;
    }

    real isotropic_phase_function::f(const vector3& wi, const vector3& wo) const
    {
        static const real I4 = 1.0 / (4 * values::pi());
        return I4;
    }
}
