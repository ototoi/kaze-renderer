#include "distancer.h"
#include "values.h"

#include <cmath>
#include <cstdlib>

namespace kaze
{

    real euclidean_distancer::static_get(real x, real y) { return std::sqrt(x * x + y * y); }
    real euclidean_distancer::static_get(real x, real y, real z) { return std::sqrt(x * x + y * y + z * z); }

    real cityblock_distancer::static_get(real x, real y) { return fabs(x) + fabs(y); }
    real cityblock_distancer::static_get(real x, real y, real z) { return fabs(x) + fabs(y) + fabs(z); }

    real chessboard_distancer::static_get(real x, real y)
    {
        x = fabs(x);
        y = fabs(y);
        return std::max(x, y);
    }
    real chessboard_distancer::static_get(real x, real y, real z)
    {
        x = fabs(x);
        y = fabs(y);
        z = fabs(z);
        return std::max(std::max(x, y), z);
    }

    real octagonal_distancer::static_get(real x, real y)
    {
        static const real k1 = std::sqrt(values::pi() / (2 * std::sqrt(2.0)));
        static const real k2 = std::sqrt(2.0) - 1;

        x = fabs(x);
        y = fabs(y);
        return (x > y) ? (k1 * x + k2 * y) : (k2 * x + k1 * y);
    }
    real octagonal_distancer::static_get(real x, real y, real z)
    {
        static const real k1 = std::sqrt(values::pi() / (2 * std::sqrt(2.0)));
        static const real k2 = std::sqrt(2.0) - 1;

        x = fabs(x);
        y = fabs(y);
        z = fabs(z);

        real xy = (x > y) ? (k1 * x + k2 * y) : (k2 * x + k1 * y);
        return (xy > z) ? (k1 * xy + k2 * z) : (k2 * xy + k1 * z);
    }
}
