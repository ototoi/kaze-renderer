#ifndef KAZE_CORRESPONDER_H
#define KAZE_CORRESPONDER_H

#include <algorithm>
#include <functional>

namespace kaze
{

    inline int correspond_repeat(int x, int w){ return (x+w) % w; }
    inline int correspond_mirror(int x, int w){ return ((x / w) & 1) ? (w - 1 - correspond_repeat(x, w)) : (correspond_repeat(x, w)); }
    inline int correspond_clamp (int x, int w){ return std::max(0, std::min(x, w-1)); }
    inline int correspond_repeat_powerof2(int x, int w){ return x & (w - 1);}
    inline int correspond_mirror_powerof2(int x, int w){ return (x & w) ? (w - 1 - (x & (w - 1))) : (x & (w - 1)); }
    inline int correspond_clamp_powerof2 (int x, int w){ correspond_clamp(x, w); }

    class corresponder
    {
    public:
        virtual ~corresponder() {}
    public:
        virtual int get(int x, int w) const = 0;
        virtual corresponder* clone_powerof2() const { return this->clone_general(); }
        virtual corresponder* clone_general() const = 0;
        virtual corresponder* clone() const { return this->clone_general(); }
    };

    class powerof2_repeat_corresponder;
    class powerof2_mirror_corresponder;
    class powerof2_clamp_corresponder;

    class repeat_corresponder : public corresponder
    {
    public:
        int get(int x, int w) const;
        corresponder* clone_powerof2() const;
        corresponder* clone_general() const;
        corresponder* clone() const;

    public:
        static inline int static_get(int x, int w){ return correspond_repeat(x, w); }
    };

    class mirror_corresponder : public corresponder
    {
    public:
        int get(int x, int w) const;
        corresponder* clone_powerof2() const;
        corresponder* clone_general() const;
        corresponder* clone() const;

    public:
        static inline int static_get(int x, int w){ return correspond_mirror(x, w); }
    };

    class clamp_corresponder : public corresponder
    {
    public:
        int get(int x, int w) const;
        corresponder* clone_powerof2() const;
        corresponder* clone_general() const;
        corresponder* clone() const;

    public:
        static inline int static_get(int x, int w){ return correspond_clamp(x, w); }
    };

    class powerof2_repeat_corresponder : public corresponder
    {
    public:
        int get(int x, int w) const;
        corresponder* clone_powerof2() const;
        corresponder* clone_general() const;
        corresponder* clone() const;

    public:
        static inline int static_get(int x, int w){ return correspond_repeat_powerof2(x, w); }
    };

    class powerof2_mirror_corresponder : public corresponder
    {
    public:
        int get(int x, int w) const;
        corresponder* clone_powerof2() const;
        corresponder* clone_general() const;
        corresponder* clone() const;

    public:
        static inline int static_get(int x, int w){ return correspond_mirror_powerof2(x, w); }
    };

    class powerof2_clamp_corresponder : public corresponder
    {
    public:
        int get(int x, int w) const;
        corresponder* clone_powerof2() const;
        corresponder* clone_general() const;
        corresponder* clone() const;

    public:
        static inline int static_get(int x, int w){ return correspond_clamp_powerof2(x, w); }
    };
}

#endif