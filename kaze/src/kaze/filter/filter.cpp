#include "filter.h"
#include "values.h"

#include <cmath>
#include <algorithm>

namespace kaze
{

    using namespace std;

    namespace
    {
        inline real pow2(real x) { return x * x; }
        inline real pow3(real x) { return x * x * x; }
    }

    //----------------------------------------------------------------------------

    mix_filter::mix_filter(
        const auto_count_ptr<filter>& f1,
        const auto_count_ptr<filter>& f2) : f1_(f1), f2_(f2) {}

    real mix_filter::get(real x) const
    {
        return f1_->get(x) * f2_->get(x);
    }

    //---------------------------------------------------------------

    real none_filter::get(real x) const
    {
        return static_get(x);
    }

    real none_filter::static_get(real x)
    {
        (void)x;
        return real(1);
    }
    //---------------------------------------------------------------

    real box_filter::get(real x) const
    {
        return static_get(x);
    }

    real box_filter::static_get(real x)
    {
        if (fabs(x) <= 1)
        {
            return real(1);
        }
        else
        {
            return real(0);
        }
    }

    //---------------------------------------------------------------

    real triangle_filter::get(real x) const
    {
        return static_get(x);
    }

    real triangle_filter::static_get(real x)
    {
        return std::max<real>(0, real(1) - x); //0 .. 1 r...1;
    }

    //---------------------------------------------------------------

    real cos_filter::get(real x) const
    {
        return static_get(x);
    }

    real cos_filter::static_get(real x)
    {
        if (x <= 0)
        {
            return 1;
        }
        else if (x < 1)
        {
            return real(0.5) * cos(values::pi() * x) + real(0.5);
        }
        else
        {
            return 0;
        }
    }

    //---------------------------------------------------------------

    real f3_filter::get(real x) const
    {
        return static_get(x);
    }

    real f3_filter::static_get(real x)
    {
        if (x <= 0)
        {
            return 1;
        }
        else if (x < 1)
        {
            return (2 * x - 3) * x * x + 1;
        }
        else
        {
            return 0;
        }
    }

    //---------------------------------------------------------------

    real f4_filter::get(real x) const
    {
        return static_get(x);
    }

    real f4_filter::static_get(real x)
    {
        if (x <= 0)
        {
            return 1;
        }
        else if (x < 1)
        {
            return pow2(1 - pow2(x)); //(1-x^2)^2-> x^4-2x^2+1
        }
        else
        {
            return 0;
        }
    }

    //---------------------------------------------------------------
    //
    real f5_filter::get(real x) const
    {
        return static_get(x);
    }

    real f5_filter::static_get(real x)
    {
        if (x <= 0)
        {
            return 1;
        }
        else if (x < 1)
        {
            return 1 - x * x * x * (x * (x * 6 - 15) + 10);
        }
        else
        {
            return 0;
        }
    }

    //---------------------------------------------------------------

    real f6_filter::get(real x) const
    {
        return static_get(x);
    }

    real f6_filter::static_get(real x)
    {
        if (x <= 0)
        {
            return 1;
        }
        else if (x < 1)
        {
            real x2 = x * x;
            real x4 = x2 * x2;
            real x6 = x2 * x4;
            return -4.0 / 9.0 * x6 + 17.0 / 9.0 * x4 - 22.0 / 9.0 * x2 + 1;
        }
        else
        {
            return 0;
        }
    }

    //---------------------------------------------------------------
    gaussian_filter::gaussian_filter(real stddev, real w)
    {
        alpha_ = 1 / (2 * stddev * stddev);
        expv_ = std::exp(-alpha_ * w * w);
        w_ = w;
    }

    real gaussian_filter::get(real x) const
    {
        if (x <= 0) return 1;
        if (w_ <= x) return 0;

        return gaussian(x, alpha_, expv_);
    }

    real gaussian_filter::static_get(real x)
    {
        static const real stddev = 0.5;
        static const real w = 2.0;
        static const real alpha = 1 / (2 * stddev * stddev);
        static const real expv = std::exp(-alpha * w * w);

        if (x <= 0) return 1;
        if (w <= x) return 0;

        return gaussian(x, alpha, expv);
    }

    real gaussian_filter::gaussian(real x, real alpha, real expv)
    {
        return std::max<real>(0.0, std::exp(-alpha * x * x) - expv);
    }
    //---------------------------------------------------------------

    static real get_1st_cubic(real x)
    {
        //(x^3-x^2-x)+(-x^2+x+1)->x^3-2*x^2+1
        //return (x-1)*(x*x-x-1);
        //static const real c3 = 1;
        //static const real c2 = -2.0;
        return x * x * (x - 2) + 1;
    }
    static real get_2nd_cubic(real x)
    {
        //4-8*x+5*x^2-x^3
        //return -(x-1)*(x-2)*(x-2);
        static const real c3 = -1;
        static const real c2 = 5;
        static const real c1 = -8;
        static const real c0 = 4;

        return x * (x * (x * c3 + c2) + c1) + c0;
    }

    real cubic_filter::get(real x) const
    {
        return static_get(x);
    }

    real cubic_filter::static_get(real x)
    {
        if (x <= 0)
        {
            return 1;
        }
        else if (x < 1)
        {
            return get_1st_cubic(x);
        }
        else if (x < 2)
        {
            return get_2nd_cubic(x);
        }
        else
        {
            return 0;
        }
    }

    //---------------------------------------------------------------
    static real get_1st_mitchell(real x)
    {
        //7/6*x^3-2*x^2+8/9
        //(7/6+x-2)*(x^2)+8/9
        static const real c3 = 7.0 / 6.0;
        static const real c2 = -2.0;
        //static const real c1 = 0;
        static const real c0 = 8.0 / 9.0;

        return x * (x * (x * c3 + c2)) + c0;
    }
    static real get_2nd_mitchell(real x)
    {
        //2*x^2-10/3*x-7/18*x^3+16/9
        //-7/18*x^3 + 2*x^2 -10/3*x + 16/9
        static const real c3 = -7.0 / 18.0;
        static const real c2 = 2.0;
        static const real c1 = -10.0 / 3.0;
        static const real c0 = 16.0 / 9.0;

        return x * (x * (x * c3 + c2) + c1) + c0;
    }
    real mitchell_filter::get(real x) const
    {
        return static_get(x);
    }

    real mitchell_filter::static_get(real x)
    {
        if (x <= 0)
        {
            return 1;
        }
        else if (x < 1)
        {
            return get_1st_mitchell(x);
        }
        else if (x < 2)
        {
            return get_2nd_mitchell(x);
        }
        else
        {
            return 0;
        }
    }
//---------------------------------------------------------------
#ifndef M_PI
#define M_PI (values::pi())
#endif
    //---------------------------------------------------------------
    static inline real sinc(real x)
    {
        if (fabs(x) <= values::epsilon())
            return 1.0 + x * x * (-1.0 / 6.0 + x * x * 1.0 / 120.0);
        else
            return sin(x) / x;
    }

    real sinc_filter::get(real x) const
    {
        return static_get(x);
    }

    real sinc_filter::static_get(real x)
    {
        if (x >= 2) return 0;
        return sinc(M_PI * x);
    }

    lanczos_filter::lanczos_filter(real f)
    {
        tau_ = 1 / f;
    }

    real lanczos_filter::get(real x) const
    {
        return static_get(x, tau_);
    }

    real lanczos_filter::static_get(real x)
    {
        static const real TAU = real(0.5);
        return static_get(x, TAU);
    }

    real lanczos_filter::static_get(real x, real tau)
    {
        if (x >= 2) return 0;
        return sinc(M_PI * x) * sinc(M_PI * x * tau);
    }

    //---------------------------------------------------------------
    /*
	real bell_filter::get(real x)const{
		return static_get(x);
	}

	real bell_filter::static_get(real x){
		if(x<real(0.5)){
			return real(3.0/4.0)-x*x;			
		}else if(x<real(1.5)){
			real x2 = x- real(2.0/3);
			return x2*x2*real(0.5);
		}else{
			return 0;
		}
	}
*/
    //---------------------------------------------------------------

    real lagrange_filter::get(real x) const
    {
        return static_get(x);
    }

    real lagrange_filter::static_get(real x)
    {
        if (x <= 0)
        {
            return 1;
        }
        else if (x < 1)
        {
            return real(0.5) * ((x - 2) * (x + 1) * (x - 1));
        }
        else if (x < 2)
        {
            return real(-1.0 / 6) * ((x - 3) * (x - 2) * (x - 1));
        }
        else
        {
            return 0;
        }
    }

    //---------------------------------------------------------------

    real bspline_filter::get(real x) const
    {
        return static_get(x);
    }

    real bspline_filter::static_get(real x)
    {
        if (x <= 0)
        {
            return 1;
        }
        else if (x < 2)
        {
            real xp2 = x + 2;
            real xp1 = x + 1;
            real xm1 = std::max<real>(0, x - 1);
            return real(4.0 / 6.0) * real(1.0 / 4) * (pow3(xp2) - 4 * pow3(xp1) + 6 * pow3(x) - 4 * pow3(xm1)); //3/(2*6)
        }
        else
        {
            return 0;
        }
    }
}
