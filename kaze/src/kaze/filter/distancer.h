#ifndef KAZE_DISTANCER_H
#define KAZE_DISTANCER_H

#include "types.h"

#include <functional>

namespace kaze
{

    class distancer : public std::binary_function<real, real, real>
    {
    public:
        virtual ~distancer() {}
    public:
        virtual real get(real x, real y) const = 0;
        virtual real get(real x, real y, real z) const = 0;

    public:
        real operator()(real x, real y) const { return this->get(x, y); }
        real operator()(real x, real y, real z) const { return this->get(x, y, z); }
    };

    class euclidean_distancer : public distancer
    {
    public:
        real get(real x, real y) const { return static_get(x, y); }
        real get(real x, real y, real z) const { return static_get(x, y, z); }
    public:
        static real static_get(real x, real y);
        static real static_get(real x, real y, real z);
    };

    class cityblock_distancer : public distancer
    {
    public:
        real get(real x, real y) const { return static_get(x, y); }
        real get(real x, real y, real z) const { return static_get(x, y, z); }
    public:
        static real static_get(real x, real y);
        static real static_get(real x, real y, real z);
    };
    typedef cityblock_distancer city_block_distancer;

    class chessboard_distancer : public distancer
    {
    public:
        real get(real x, real y) const { return static_get(x, y); }
        real get(real x, real y, real z) const { return static_get(x, y, z); }
    public:
        static real static_get(real x, real y);
        static real static_get(real x, real y, real z);
    };
    typedef chessboard_distancer chess_board_distancer;

    class octagonal_distancer : public distancer
    {
    public:
        real get(real x, real y) const { return static_get(x, y); }
        real get(real x, real y, real z) const { return static_get(x, y, z); }
    public:
        static real static_get(real x, real y);
        static real static_get(real x, real y, real z);
    };
}

#endif
