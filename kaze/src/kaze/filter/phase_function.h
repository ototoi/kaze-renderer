#ifndef KAZE_PHASE_FUNCTION_H
#define KAZE_PHASE_FUNCTION_H

#include "types.h"
//#include "spectrum.h"

namespace kaze
{

    class phase_function
    {
    public:
        virtual ~phase_function() {}
        virtual real f(const vector3& wi, const vector3& wo) const = 0;
    };

    class hg_phase_function : public phase_function
    {
    public:
        hg_phase_function(real g);
        real f(const vector3& wi, const vector3& wo) const;

    private:
        real g_;
    };

    class kajiya_kay_specular_phase_function : public phase_function
    {
    public:
        kajiya_kay_specular_phase_function(const vector3& T) : T_(T) {}
    public:
        real f(const vector3& wi, const vector3& wo) const;

    private:
        vector3 T_;
    };

    class isotropic_phase_function : public phase_function
    {
    public:
        real f(const vector3& wi, const vector3& wo) const;
    };

    typedef isotropic_phase_function iso_phase_function;
}

#endif
