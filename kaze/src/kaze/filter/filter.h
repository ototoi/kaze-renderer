#ifndef KAZE_FILTER_H
#define KAZE_FILTER_H

#include "types.h"
#include "distancer.h"
#include "count_ptr.hpp"

#include <functional>
#include <vector>

namespace kaze
{

    /*
     *---------|1------------
     *         |\
     *         |  \
     *         |    \
     *         |      \
     *         |        \ R
     *--------------------\-X
     *
     */

    /*
	inline real smoothstep(real x){return x * x * (real(3.0) - (real(2.0) * x));}
	*/
    //---------------------------------------------------------------

    class filter : public std::unary_function<real, real>
    {
    public:
        virtual real get(real x) const = 0;

    public:
        virtual ~filter() {}
    public:
        real operator()(real x) const { return this->get(x); }
    };

    //---------------------------------------------------------------

    class proxy_filter : public filter
    {
    public:
        proxy_filter(const filter* f) { f_ = f; }
        real get(real x) const { return f_->get(x); }
    private:
        const filter* f_;
    };

    //---------------------------------------------------------------

    class mix_filter : public filter
    {
    public:
        mix_filter(
            const auto_count_ptr<filter>& f1,
            const auto_count_ptr<filter>& f2);

    public:
        real get(real x) const;

    private:
        std::shared_ptr<filter> f1_;
        std::shared_ptr<filter> f2_;
    };

    //---------------------------------------------------------------

    class none_filter : public filter
    {
    public:
        real get(real x) const;

    public:
        static real static_get(real x);
    };

    //---------------------------------------------------------------

    class box_filter : public filter
    {
    public:
        real get(real x) const;

    public:
        static real static_get(real x);
    };

    //---------------------------------------------------------------

    class triangle_filter : public filter
    {
    public:
        real get(real x) const;

    public:
        static real static_get(real x);
    };
    typedef triangle_filter pyramid_filter;
    typedef triangle_filter cone_filter;
    //typedef bilinear_filter cone_filter;

    //---------------------------------------------------------------

    class gaussian_filter : public filter
    {
    public:
        gaussian_filter(real stddev = real(0.5), real w = real(2.0));

    public:
        real get(real x) const;

    public:
        static real static_get(real x);
        static real gaussian(real x, real alpha, real expv);

    private:
        real alpha_;
        real expv_;
        real w_;
    };

    class cos_filter : public filter
    {
    public:
        real get(real x) const;

    public:
        static real static_get(real x);
    };

    //---------------------------------------------------------------
    //2x^3 - 3x^2 + 1
    class f3_filter : public filter
    {
    public:
        real get(real x) const;

    public:
        static real static_get(real x);
    };
    typedef f3_filter hermite_filter;

    //---------------------------------------------------------------
    //x^4 - 2x^2 + 1
    class f4_filter : public filter
    {
    public:
        real get(real x) const;

    public:
        static real static_get(real x);
    };

    //---------------------------------------------------------------
    //-6x^5 + 15x^4 - 10x^3 + 1
    class f5_filter : public filter
    {
    public:
        real get(real x) const;

    public:
        static real static_get(real x);
    };

    //---------------------------------------------------------------
    //-4.0/9.0*x^6 + 17.0/9.0*x^4 -22.0/9.0*x^2 + 1;
    class f6_filter : public filter
    {
    public:
        real get(real x) const;

    public:
        static real static_get(real x);
    };

    //---------------------------------------------------------------
    class cubic_filter : public filter
    {
    public:
        real get(real x) const;

    public:
        static real static_get(real x);
    };
    typedef cubic_filter bicubic_filter;

    //---------------------------------------------------------------
    class mitchell_filter : public filter
    {
    public:
        real get(real x) const;

    public:
        static real static_get(real x);
    };
    //---------------------------------------------------------------

    class sinc_filter : public filter
    {
    public:
        real get(real x) const;

    public:
        static real static_get(real x);
    };

    //---------------------------------------------------------------

    class lanczos_filter : public filter
    {
    public:
        lanczos_filter(real f = real(2.0));

    public:
        real get(real x) const;

    public:
        static real static_get(real x);
        static real static_get(real x, real tau);

    private:
        real tau_;
    };

    //---------------------------------------------------------------

    /*
	class bell_filter:public filter{
	public:
		real get(real x)const;
	public:
		static real static_get(real x);
	};
	*/

    //---------------------------------------------------------------

    class lagrange_filter : public filter
    {
    public:
        real get(real x) const;

    public:
        static real static_get(real x);
    };

    //---------------------------------------------------------------

    class bspline_filter : public filter
    {
    public:
        real get(real x) const;

    public:
        static real static_get(real x);
    };
}

#endif
