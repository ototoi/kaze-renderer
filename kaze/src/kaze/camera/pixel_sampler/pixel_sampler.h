#ifndef KAZE_PIXEL_SAMPLER_H
#define KAZE_PIXEL_SAMPLER_H

#include "types.h"
#include "pixel_sample.h"
#include <vector>

namespace kaze
{

    class pixel_sampler
    {
    public:
        virtual ~pixel_sampler() {}
        virtual void get(std::vector<pixel_sample>& samples, int x0, int y0, int x1, int y1) const = 0;
    };

    class uniform_pixel_sampler : public pixel_sampler
    {
    public:
        virtual void get(std::vector<pixel_sample>& samples, int x0, int y0, int x1, int y1) const;
    };
}

#endif
