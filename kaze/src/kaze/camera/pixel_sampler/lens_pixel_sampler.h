#ifndef KAZE_LENS_PIXEL_SAMPLER_H
#define KAZE_LENS_PIXEL_SAMPLER_H

#include "pixel_sampler.h"

namespace kaze
{

    class lds_lens_pixel_sampler : public pixel_sampler
    {
    public:
        lds_lens_pixel_sampler();

    public:
        virtual void get(std::vector<pixel_sample>& samples, int x0, int y0, int x1, int y1) const;
    };
}

#endif