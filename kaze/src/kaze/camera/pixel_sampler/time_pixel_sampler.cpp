#include "time_pixel_sampler.h"
#include "QMC.h"

namespace
{
//using FNV1-a
#define FNV1_32_INIT ((Fnv32_t)0x811c9dc5)
#define FNV1_32A_INIT FNV1_32_INIT
/*
 * 32 bit magic FNV-1a prime
 */
#define FNV_32_PRIME ((Fnv32_t)0x01000193)

/*
 * 32 bit FNV-0 hash type
 */
    typedef uint32_t Fnv32_t;

/*
 * fnv_32a_buf - perform a 32 bit Fowler/Noll/Vo FNV-1a hash on a buffer
 *
 * input:
 *	buf	- start of buffer to hash
 *	len	- length of buffer in octets
 *	hval	- previous hash value or 0 if first call
 *
 * returns:
 *	32 bit hash as a static hash type
 *
 * NOTE: To use the recommended 32 bit FNV-1a hash, use FNV1_32A_INIT as the
 * 	 hval arg on the first call to either fnv_32a_buf() or fnv_32a_str().
 */
    Fnv32_t fnv_32a_buf(void* buf, size_t len, Fnv32_t hval)
    {
        unsigned char* bp = (unsigned char*)buf; /* start of buffer */
        unsigned char* be = bp + len;            /* beyond end of buffer */

        /*
     * FNV-1a hash each octet in the buffer
     */
        while (bp < be)
        {

            /* xor the bottom with the current octet */
            hval ^= (Fnv32_t)*bp++;

/* multiply by the 32 bit FNV magic prime mod 2^32 */
#if defined(NO_FNV_GCC_OPTIMIZATION)
            hval *= FNV_32_PRIME;
#else
            hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
#endif
        }

        /* return our new hash value */
        return hval;
    }
}

namespace kaze
{

    lds_time_pixel_sampler::lds_time_pixel_sampler()
    {
        off_ = 0;
    }

    void lds_time_pixel_sampler::get(std::vector<pixel_sample>& samples, int x0, int y0, int x1, int y1) const
    {
        (void)x0;
        (void)x1;
        (void)y0;
        (void)y1;

        int px = 0;
        int py = 0;
        int k = 0;
        int off = off_;
        int kx = fnv_32a_buf(&off, 4, FNV1_32A_INIT);
        for (size_t i = 0; i < samples.size(); i++)
        {
            int x = samples[i].x;
            int y = samples[i].y;
            unsigned int hx = fnv_32a_buf(&x, 4, FNV1_32A_INIT);
            unsigned int hy = fnv_32a_buf(&y, 4, FNV1_32A_INIT);
            //unsigned int hh = hx^hy;
            unsigned int hh = fnv_32a_buf(&hx, 4, hy);

            float time = ri_vdC(kx + k, hh);
            samples[i].time = time;
            if (x != px || y != py)
            {
                k = 0;
            }
            else
            {
                k++;
            }

            px = x;
            py = y;
        }
        off_ = off + 1;
    }
}
