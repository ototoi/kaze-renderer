#ifndef KAZE_PIXEL_SAMPLE_H
#define KAZE_PIXEL_SAMPLE_H

namespace kaze
{

    struct pixel_sample
    {
        int x;      //
        int y;      //
        float dx;   //subpixel delta x;
        float dy;   //subpixel delta y;
        float u;    //
        float v;    //
        float time; //[0,1)
        int i;      //subpixel index;
        int n;      //subpixel total samples
    };
}

#endif
