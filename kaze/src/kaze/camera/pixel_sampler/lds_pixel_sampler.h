#ifndef KAZE_LDS_PIXEL_SAMPLER_H
#define KAZE_LDS_PIXEL_SAMPLER_H

#include "pixel_sampler.h"

namespace kaze
{

    class lds_pixel_sampler : public pixel_sampler
    {
    public:
        lds_pixel_sampler(int sx, int sy);
        void get(std::vector<pixel_sample>& samples, int x0, int y0, int x1, int y1) const;

    private:
        int sx_;
        int sy_;
        mutable int off_;
    };
}

#endif
