#include "composite_pixel_sampler.h"

namespace kaze
{

    composite_pixel_sampler::composite_pixel_sampler(
        const auto_count_ptr<pixel_sampler>& jitter,
        const auto_count_ptr<pixel_sampler>& time,
        const auto_count_ptr<pixel_sampler>& lens) : jitter_(jitter), time_(time), lens_(lens)
    {
    }

    void composite_pixel_sampler::get(std::vector<pixel_sample>& samples, int x0, int y0, int x1, int y1) const
    {
        if (jitter_.get()) jitter_->get(samples, x0, y0, x1, y1);
        if (time_.get()) time_->get(samples, x0, y0, x1, y1);
        if (lens_.get()) lens_->get(samples, x0, y0, x1, y1);
    }
}
