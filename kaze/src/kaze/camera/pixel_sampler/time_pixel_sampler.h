#ifndef KAZE_TIME_PIXEL_SAMPLER_H
#define KAZE_TIME_PIXEL_SAMPLER_H

#include "pixel_sampler.h"

namespace kaze
{

    class lds_time_pixel_sampler : public pixel_sampler
    {
    public:
        lds_time_pixel_sampler();

    public:
        virtual void get(std::vector<pixel_sample>& samples, int x0, int y0, int x1, int y1) const;

    protected:
        mutable int off_;
    };
}

#endif