#include "lds_pixel_sampler.h"
#include "QMC.h"
#include <algorithm>

namespace
{
//using FNV1-a
#define FNV1_32_INIT ((Fnv32_t)0x811c9dc5)
#define FNV1_32A_INIT FNV1_32_INIT
/*
 * 32 bit magic FNV-1a prime
 */
#define FNV_32_PRIME ((Fnv32_t)0x01000193)

/*
 * 32 bit FNV-0 hash type
 */
    typedef uint32_t Fnv32_t;

/*
 * fnv_32a_buf - perform a 32 bit Fowler/Noll/Vo FNV-1a hash on a buffer
 *
 * input:
 *	buf	- start of buffer to hash
 *	len	- length of buffer in octets
 *	hval	- previous hash value or 0 if first call
 *
 * returns:
 *	32 bit hash as a static hash type
 *
 * NOTE: To use the recommended 32 bit FNV-1a hash, use FNV1_32A_INIT as the
 * 	 hval arg on the first call to either fnv_32a_buf() or fnv_32a_str().
 */
    Fnv32_t fnv_32a_buf(void* buf, size_t len, Fnv32_t hval)
    {
        unsigned char* bp = (unsigned char*)buf; /* start of buffer */
        unsigned char* be = bp + len;            /* beyond end of buffer */

        /*
     * FNV-1a hash each octet in the buffer
     */
        while (bp < be)
        {

            /* xor the bottom with the current octet */
            hval ^= (Fnv32_t)*bp++;

/* multiply by the 32 bit FNV magic prime mod 2^32 */
#if defined(NO_FNV_GCC_OPTIMIZATION)
            hval *= FNV_32_PRIME;
#else
            hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
#endif
        }

        /* return our new hash value */
        return hval;
    }
}

namespace kaze
{

    lds_pixel_sampler::lds_pixel_sampler(int sx, int sy)
        : sx_(sx), sy_(sy)
    {
        off_ = 0;
    }

    void lds_pixel_sampler::get(std::vector<pixel_sample>& samples, int x0, int y0, int x1, int y1) const
    {
        int sx = sx_;
        int sy = sy_;
        int n = sx * sy;
        int picsz = (x1 - x0) * (y1 - y0);
        samples.resize(n * picsz);

        int kk = off_ + 12345;
        unsigned int h = fnv_32a_buf(&kk, 4, FNV1_32A_INIT);

        int c = 0;
        for (int y = y0; y < y1; y++)
        {
            for (int x = x0; x < x1; x++)
            {
                //unsigned int hx = fnv_32a_buf(&x,4,FNV1_32A_INIT);
                //unsigned int hy = fnv_32a_buf(&y,4,FNV1_32A_INIT);
                //unsigned int hh = hx^hy;
                //unsigned int hh = fnv_32a_buf(&cnt,4,hy);

                for (int j = 0; j < sy; j++)
                {
                    for (int i = 0; i < sx; i++)
                    {
                        float u = ri_vdC(c, h);
                        float v = ri_S(c, h);
                        samples[c].x = x;
                        samples[c].y = y;
                        samples[c].dx = u;
                        samples[c].dy = v;
                        samples[c].n = n;
                        c++;
                    }
                }
            }
        }
        off_ += c;
    }
}
