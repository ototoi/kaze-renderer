#include "pixel_sampler.h"

namespace kaze
{

    void uniform_pixel_sampler::get(std::vector<pixel_sample>& samples, int x0, int y0, int x1, int y1) const
    {
        int picsz = (x1 - x0) * (y1 - y0);
        samples.resize(picsz);

        int cnt = 0;
        for (int y = y0; y < y1; y++)
        {
            for (int x = x0; x < x1; x++)
            {
                samples[cnt].x = x;
                samples[cnt].y = y;

                samples[cnt].dx = 0.5f;
                samples[cnt].dy = 0.5f;

                samples[cnt].time = 0;
                samples[cnt].u = 0;
                samples[cnt].v = 0;

                samples[cnt].n = 1;

                cnt++;
            }
        }
    };
}
