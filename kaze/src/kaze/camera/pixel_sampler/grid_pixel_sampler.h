#ifndef KAZE_GRID_PIXEL_SAMPLER_H
#define KAZE_GRID_PIXEL_SAMPLER_H

#include "pixel_sampler.h"

namespace kaze
{

    class grid_pixel_sampler : public pixel_sampler
    {
    public:
        grid_pixel_sampler(int sx, int sy);
        void get(std::vector<pixel_sample>& samples, int x0, int y0, int x1, int y1) const;

    private:
        int sx_;
        int sy_;
    };
}

#endif
