#ifndef KAZE_COMPOSITE_PIXEL_SAMPLER_H
#define KAZE_COMPOSITE_PIXEL_SAMPLER_H

#include "pixel_sampler.h"
#include "count_ptr.hpp"

namespace kaze
{

    class composite_pixel_sampler : public pixel_sampler
    {
    public:
        composite_pixel_sampler(
            const auto_count_ptr<pixel_sampler>& jitter,
            const auto_count_ptr<pixel_sampler>& time,
            const auto_count_ptr<pixel_sampler>& lens);

    public:
        virtual void get(std::vector<pixel_sample>& samples, int x0, int y0, int x1, int y1) const;

    private:
        auto_count_ptr<pixel_sampler> jitter_;
        auto_count_ptr<pixel_sampler> time_;
        auto_count_ptr<pixel_sampler> lens_;
    };
}

#endif