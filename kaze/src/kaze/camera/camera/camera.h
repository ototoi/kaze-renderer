#ifndef KAZE_CAMERA_H
#define KAZE_CAMERA_H

#include "ray_shooter.h"
#include "pixel_sample.h"

namespace kaze
{

    class camera
    {
    public:
        virtual ~camera() {}
        virtual ray shoot(real i, real j) const = 0;
        virtual ray shoot(const pixel_sample& sample, int width, int height) const;
    };

    class perspective_camera : public camera
    {
    public:
        perspective_camera(const vector3& from, const vector3& to, const vector3& upper, real angle, real aspect);
        ~perspective_camera();

    public:
        ray shoot(real i, real j) const;

    private:
        ray_shooter* rs_;
    };

    class orthogonal_camera : public camera
    {
    public:
        orthogonal_camera(const vector3& from, const vector3& to, const vector3& upper, real width, real height);
        ~orthogonal_camera();

    public:
        ray shoot(real i, real j) const;

    private:
        ray_shooter* rs_;
    };

    class spherical_camera : public camera
    {
    public:
        spherical_camera(const vector3& from, const vector3& to, const vector3& upper, real angle_h, real angle_v);
        ~spherical_camera();

    public:
        ray shoot(real i, real j) const;

    private:
        ray_shooter* rs_;
    };

    class cylindrical_camera : public camera
    {
    public:
        cylindrical_camera(const vector3& from, const vector3& to, const vector3& upper, real angle, real height);
        ~cylindrical_camera();

    public:
        ray shoot(real i, real j) const;

    private:
        ray_shooter* rs_;
    };

    class cylindrical2_camera : public camera
    {
    public:
        cylindrical2_camera(const vector3& from, const vector3& to, const vector3& upper, real angle, real height);
        ~cylindrical2_camera();

    public:
        ray shoot(real i, real j) const;

    private:
        ray_shooter* rs_;
    };

    typedef cylindrical2_camera perspective_cylindrical_camera;

    class fisheye_camera : public camera
    {
    public:
        fisheye_camera(const vector3& from, const vector3& to, const vector3& upper);
        ~fisheye_camera();

    public:
        ray shoot(real i, real j) const;

    private:
        ray_shooter* rs_;
    };
}

#endif
