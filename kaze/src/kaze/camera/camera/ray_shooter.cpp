#include "ray_shooter.h"
#include "logger.h"
#include <cmath>
#include <cassert>

namespace kaze
{

    perspective_ray_shooter::perspective_ray_shooter(const vector3& from, const vector3& to, const vector3& upper, real angle, real aspect)
    {
        from_ = from;
        /*
		vector3 az = normalize(to - from);
		vector3 up = normalize(upper);
		vector3 ay = normalize(up - dot(up,az)*az);
		vector3 ax = cross(az,ay);
		*/
        vector3 az = normalize(to - from);        //
        vector3 ax = normalize(cross(az, upper)); //-y^z
        vector3 ay = cross(ax, az);               //

        axis_z_ = az;
        axis_y_ = ay;
        axis_x_ = ax;

        //tangent_ = std::tan(angle*0.5);
        //aspect_ = aspect;
        real tangent = std::tan(angle * 0.5);
        cof_x_ = aspect * tangent;
        cof_y_ = tangent;
    }

    ray perspective_ray_shooter::shoot(real i, real j) const
    {
        //assert(is_valid_range(i,j));

        real WW = cof_x_ * i;
        real HH = cof_y_ * j;

        return ray(from_, normalize(WW * axis_x_ + HH * axis_y_ + axis_z_));
    }

    orthogonal_ray_shooter::orthogonal_ray_shooter(const vector3& from, const vector3& to, const vector3& upper, real width, real height)
    {

        from_ = from;
        vector3 az = normalize(to - from);        //
        vector3 ax = normalize(cross(az, upper)); //-y^z
        vector3 ay = cross(ax, az);               //

        axis_z_ = az;
        axis_y_ = ay * (height / 2);
        axis_x_ = ax * (width / 2);
    }

    ray orthogonal_ray_shooter::shoot(real i, real j) const
    {
        //assert(is_valid_range(i,j));

        return ray(from_ + (i * axis_x_) + (j * axis_y_), axis_z_);
    }
    //------------------------------------------------------------------

    spherical_ray_shooter::spherical_ray_shooter(const vector3& from, const vector3& to, const vector3& upper, real angle_h, real angle_v)
    {
        from_ = from;
        vector3 az = normalize(to - from);        //
        vector3 ax = normalize(cross(az, upper)); //-y^z
        vector3 ay = cross(ax, az);               //

        axis_z_ = az;
        axis_y_ = ay;
        axis_x_ = ax;

        angle_h_ = angle_h / 2;
        angle_v_ = angle_v / 2;
    }

    ray spherical_ray_shooter::shoot(real i, real j) const
    {
        //assert(is_valid_range(i,j));

        real theta = angle_h_ * i;
        real phi = angle_v_ * j;

        real cos_theta = std::cos(theta);
        real sin_theta = std::sin(theta);

        real cos_phi = std::cos(phi);
        real sin_phi = std::sin(phi);

        return ray(from_, cos_phi * sin_theta * axis_x_ + sin_phi * axis_y_ + cos_phi * cos_theta * axis_z_);
    }

    //------------------------------------------------------------------

    cylindrical_ray_shooter::cylindrical_ray_shooter(const vector3& from, const vector3& to, const vector3& upper, real angle, real height)
    {
        from_ = from;

        vector3 az = normalize(to - from);        //
        vector3 ax = normalize(cross(az, upper)); //-y^z
        vector3 ay = cross(ax, az);               //

        axis_z_ = az;
        axis_y_ = ay;
        axis_x_ = ax;

        angle_ = angle / 2;
        h_ = height / 2;
    }

    ray cylindrical_ray_shooter::shoot(real i, real j) const
    {
        //assert(is_valid_range(i,j));

        real theta = angle_ * i;
        real cos = std::cos(theta);
        real sin = std::sin(theta);
        return ray(from_ + (j * h_ * axis_y_), sin * axis_x_ + cos * axis_z_);
    }

    //------------------------------------------------------------------
    cylindrical2_ray_shooter::cylindrical2_ray_shooter(const vector3& from, const vector3& to, const vector3& upper, real angle, real height)
    {
        from_ = from;

        vector3 az = normalize(to - from);        //
        vector3 ax = normalize(cross(az, upper)); //-y^z
        vector3 ay = cross(ax, az);               //

        axis_z_ = az;
        axis_y_ = ay;
        axis_x_ = ax;

        angle_ = angle / 2;
        h_ = height / 2;
    }

    ray cylindrical2_ray_shooter::shoot(real i, real j) const
    {
        //assert(is_valid_range(i,j));

        real theta = angle_ * i;
        real cos = std::cos(theta);
        real sin = std::sin(theta);
        return ray(from_, normalize(sin * axis_x_ + cos * axis_z_ + (j * h_ * axis_y_)));
    }

    //------------------------------------------------------------------

    fisheye_ray_shooter::fisheye_ray_shooter(const vector3& from, const vector3& to, const vector3& upper)
    {
        from_ = from;

        vector3 az = normalize(to - from);        //
        vector3 ax = normalize(cross(az, upper)); //-y^z
        vector3 ay = cross(ax, az);               //

        axis_z_ = az;
        axis_y_ = ay;
        axis_x_ = ax;
    }

    ray fisheye_ray_shooter::shoot(real i, real j) const
    {
        //assert(is_valid_range(i,j));

        real k;
        real L2 = i * i + j * j;
        if (L2 > 1)
        {
            /*
			real cof = std::sqrt((2-L2)/L2);
			i *= cof;
			j *= cof;
			k = - std::sqrt(L2 - real(1));
			*/

            real cof = std::sqrt(1 / L2);
            i *= cof;
            j *= cof;
            k = 0;
        }
        else
        {
            k = std::sqrt(real(1) - L2);
        }

        return ray(from_, i * axis_x_ + j * axis_y_ + k * axis_z_);
    }
}
