#include "camera.h"
#include "logger.h"
#include <cmath>
#include <cassert>

namespace kaze
{

    ray camera::shoot(const pixel_sample& sample, int width, int height) const
    {
        real xx = sample.x + sample.dx;
        real yy = sample.y + sample.dy;
        real i = xx / width;
        real j = 1 - (yy / height);

        i = 2 * i - 1;
        j = 2 * j - 1;

        ray r = this->shoot(i, j);
        return ray(r.origin(), r.direction(), sample.time);
    }

    perspective_camera::perspective_camera(const vector3& from, const vector3& to, const vector3& upper, real angle, real aspect)
    {
        rs_ = new perspective_ray_shooter(from, to, upper, angle, aspect);
    }
    perspective_camera::~perspective_camera()
    {
        delete rs_;
    }
    ray perspective_camera::shoot(real i, real j) const
    {
        return rs_->shoot(i, j);
    }
    //------------------------------------------------------------------
    orthogonal_camera::orthogonal_camera(const vector3& from, const vector3& to, const vector3& upper, real width, real height)
    {
        rs_ = new orthogonal_ray_shooter(from, to, upper, width, height);
    }
    orthogonal_camera::~orthogonal_camera()
    {
        delete rs_;
    }
    ray orthogonal_camera::shoot(real i, real j) const
    {
        return rs_->shoot(i, j);
    }
    //------------------------------------------------------------------
    spherical_camera::spherical_camera(const vector3& from, const vector3& to, const vector3& upper, real angle_h, real angle_v)
    {
        rs_ = new spherical_ray_shooter(from, to, upper, angle_h, angle_v);
    }
    spherical_camera::~spherical_camera()
    {
        delete rs_;
    }
    ray spherical_camera::shoot(real i, real j) const
    {
        return rs_->shoot(i, j);
    }
    //------------------------------------------------------------------
    cylindrical_camera::cylindrical_camera(const vector3& from, const vector3& to, const vector3& upper, real angle, real height)
    {
        rs_ = new cylindrical_ray_shooter(from, to, upper, angle, height);
    }
    cylindrical_camera::~cylindrical_camera()
    {
        delete rs_;
    }
    ray cylindrical_camera::shoot(real i, real j) const
    {
        return rs_->shoot(i, j);
    }
    //------------------------------------------------------------------
    cylindrical2_camera::cylindrical2_camera(const vector3& from, const vector3& to, const vector3& upper, real angle, real height)
    {
        rs_ = new cylindrical2_ray_shooter(from, to, upper, angle, height);
    }
    cylindrical2_camera::~cylindrical2_camera()
    {
        delete rs_;
    }
    ray cylindrical2_camera::shoot(real i, real j) const
    {
        return rs_->shoot(i, j);
    }
    //------------------------------------------------------------------
    fisheye_camera::fisheye_camera(const vector3& from, const vector3& to, const vector3& upper)
    {
        rs_ = new fisheye_ray_shooter(from, to, upper);
    }
    fisheye_camera::~fisheye_camera()
    {
        delete rs_;
    }
    ray fisheye_camera::shoot(real i, real j) const
    {
        return rs_->shoot(i, j);
    }
    //------------------------------------------------------------------
}
