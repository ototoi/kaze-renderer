#ifndef KAZE_RAY_SHOOTER_H
#define KAZE_RAY_SHOOTER_H

#include "types.h"
#include "ray.h"
#include "aggregator.hpp"

namespace kaze
{

    class ray_shooter
    {
    public:
        virtual ray shoot(real i, real j) const = 0;

    public:
        virtual ~ray_shooter() {}
    };

    class perspective_ray_shooter : public ray_shooter
    {
    public:
        perspective_ray_shooter(const vector3& from, const vector3& to, const vector3& upper, real angle, real aspect);

    public:
        ray shoot(real i, real j) const;

    public:
        vector3 origin() const { return from_; }
    private:
        vector3 from_;
        vector3 axis_x_;
        vector3 axis_y_;
        vector3 axis_z_;

        real cof_x_;
        real cof_y_;
    };

    class orthogonal_ray_shooter : public ray_shooter
    {
    public:
        orthogonal_ray_shooter(const vector3& from, const vector3& to, const vector3& upper, real width, real height);

    public:
        ray shoot(real i, real j) const;

    public:
        vector3 origin() const { return from_; }
    private:
        vector3 from_;
        vector3 axis_x_;
        vector3 axis_y_;
        vector3 axis_z_;
    };

    class spherical_ray_shooter : public ray_shooter
    {
    public:
        spherical_ray_shooter(const vector3& from, const vector3& to, const vector3& upper, real angle_h, real angle_v);

    public:
        ray shoot(real i, real j) const;

    public:
        vector3 origin() const { return from_; }
    private:
        vector3 from_;
        vector3 axis_x_;
        vector3 axis_y_;
        vector3 axis_z_;

        real angle_h_;
        real angle_v_;
    };

    class cylindrical_ray_shooter : public ray_shooter
    {
    public:
        cylindrical_ray_shooter(const vector3& from, const vector3& to, const vector3& upper, real angle, real height);

    public:
        ray shoot(real i, real j) const;

    public:
        vector3 origin() const { return from_; }
    private:
        vector3 from_;
        vector3 axis_x_;
        vector3 axis_y_;
        vector3 axis_z_;

        real angle_;
        real h_;
    };

    class cylindrical2_ray_shooter : public ray_shooter
    {
    public:
        cylindrical2_ray_shooter(const vector3& from, const vector3& to, const vector3& upper, real angle, real height);

    public:
        ray shoot(real i, real j) const;

    public:
        vector3 origin() const { return from_; }
    private:
        vector3 from_;
        vector3 axis_x_;
        vector3 axis_y_;
        vector3 axis_z_;

        real angle_;
        real h_;
    };

    typedef cylindrical2_ray_shooter perspective_cylindrical_ray_shooter;

    class fisheye_ray_shooter : public ray_shooter
    {
    public:
        fisheye_ray_shooter(const vector3& from, const vector3& to, const vector3& upper);

    public:
        ray shoot(real i, real j) const;

    public:
        vector3 origin() const { return from_; }
    private:
        vector3 from_;
        vector3 axis_x_;
        vector3 axis_y_;
        vector3 axis_z_;
    };
}

#endif