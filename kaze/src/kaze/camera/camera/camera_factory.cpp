#include "camera_factory.h"

#include "camera.h"

#include "transform_matrix.h"
#include "values.h"

#include <cmath>
#include <memory>

namespace kaze
{

    enum
    {
        CAMERA_PERSPECTIVE,
        CAMERA_ORTHOGONAL,
        CAMERA_SPHERICAL,
        CAMERA_CYLINDRICAL,
        CAMERA_FISHEYE
    };

    static const int MATRIX_WORLD2CAMERA = camera_factory::MATRIX_WORLD2CAMERA;
    static const int MATRIX_CAMERA2CLIP = camera_factory::MATRIX_CAMERA2CLIP;
    static const int MATRIX_CLIP2SCREEN = camera_factory::MATRIX_CLIP2SCREEN;

    class camera_factory_imp
    {
    public:
        void init();
        void set_origin(const vector3& p);
        void set_target(const vector3& p);
        void set_upper(const vector3& v);

        void set_near(real n);
        void set_far(real f);
        void set_horizontal_angle(real angle);
        void set_vertical_angle(real angle);

        void set_camera_width(real width, real height);
        void set_pixel_width(int width, int height);

    public:
        bool is_rasterizable() const;

    public:
        camera* create_camera() const;
        matrix4 create_matrix(int m0, int m1) const;

    protected:
        camera* create_perspective_camera() const;
        camera* create_orthogonal_camera() const;
        camera* create_spherical_camera() const;
        camera* create_cylindrical_camera() const;
        camera* create_fisheye_camera() const;

    protected:
        matrix4 create_perspective_matrix() const;
        matrix4 create_orthogonal_matrix() const;

    protected:
    private:
        real n_;
        real f_;
        vector3 org_;
        vector3 trg_;
        vector3 upr_;

        real hang_;
        real vang_;

        real cw_;
        real ch_;
        int w_; //width
        int h_; //height

        int nType;
    };

    using namespace std;

    void camera_factory_imp::init()
    {
        org_ = vector3(0, 0, 0);
        trg_ = vector3(0, 0, 100);
        upr_ = vector3(0, 1, 0);

        n_ = 10.0;
        f_ = 1000.0;

        hang_ = values::radians(45.0);
        vang_ = values::radians(45.0);

        cw_ = 10.0;
        ch_ = 10.0;

        w_ = 600;
        h_ = 600;

        nType = CAMERA_PERSPECTIVE;
    }

    void camera_factory_imp::set_origin(const vector3& p)
    {
        org_ = p;
    }

    void camera_factory_imp::set_target(const vector3& p)
    {
        trg_ = p;
    }

    void camera_factory_imp::set_upper(const vector3& v)
    {
        upr_ = v;
    }

    void camera_factory_imp::set_near(real n)
    {
        n_ = n;
    }

    void camera_factory_imp::set_far(real f)
    {
        f_ = f;
    }

    void camera_factory_imp::set_horizontal_angle(real angle)
    {
        hang_ = angle;
        cw_ = n_ * tan(angle / 2);
    }

    void camera_factory_imp::set_vertical_angle(real angle)
    {
        vang_ = angle;
        ch_ = n_ * tan(angle / 2);
    }

    void camera_factory_imp::set_camera_width(real width, real height)
    {
        cw_ = width;
        ch_ = height;
    }

    void camera_factory_imp::set_pixel_width(int width, int height)
    {
        w_ = width;
        h_ = height;
    }

    bool camera_factory_imp::is_rasterizable() const
    {
        return (nType == CAMERA_PERSPECTIVE || nType == CAMERA_ORTHOGONAL);
    }

    //-----------------------------------------------------------------

    camera* camera_factory_imp::create_perspective_camera() const
    {
        std::unique_ptr<camera> ap;

        real v = std::tan(vang_ * 0.5);
        real u = std::tan(hang_ * 0.5);
        real aspect = v / u;

        ap.reset(new perspective_camera(org_, trg_, upr_, vang_, aspect));

        return ap.release();
    }

    camera* camera_factory_imp::create_orthogonal_camera() const
    {
        std::unique_ptr<camera> ap;

        real aspect = real(cw_) / ch_;

        ap.reset(new orthogonal_camera(org_, trg_, upr_, aspect, 1));

        return ap.release();
    }

    camera* camera_factory_imp::create_spherical_camera() const
    {
        std::unique_ptr<camera> ap;

        real aspect = real(cw_) / ch_;

        ap.reset(new spherical_camera(org_, trg_, upr_, hang_, vang_));

        return ap.release();
    }

    camera* camera_factory_imp::create_cylindrical_camera() const
    {
        std::unique_ptr<camera> ap;

        real aspect = real(cw_) / ch_;

        ap.reset(new cylindrical_camera(org_, trg_, upr_, hang_, ch_));

        return ap.release();
    }

    camera* camera_factory_imp::create_fisheye_camera() const
    {
        std::unique_ptr<camera> ap;

        real aspect = real(cw_) / ch_;

        ap.reset(new fisheye_camera(org_, trg_, upr_));

        return ap.release();
    }

    //-----------------------------------------------------------------

    camera* camera_factory_imp::create_camera() const
    {
        switch (nType)
        {
        case CAMERA_PERSPECTIVE:
            return create_perspective_camera();
        case CAMERA_ORTHOGONAL:
            return create_orthogonal_camera();
        case CAMERA_SPHERICAL:
            return create_spherical_camera();
        case CAMERA_CYLINDRICAL:
            return create_cylindrical_camera();
        case CAMERA_FISHEYE:
            return create_fisheye_camera();
        }
        return NULL;
    }

    //-----------------------------------------------------------------

    matrix4 camera_factory_imp::create_perspective_matrix() const
    {
        real v = std::tan(vang_ * 0.5);
        real u = std::tan(hang_ * 0.5);
        real aspect = v / u;

        return camera2clip(vang_, aspect, n_, f_);
    }

    matrix4 camera_factory_imp::create_orthogonal_matrix() const
    {
        real hw = cw_ / 2;
        real hh = ch_ / 2;

        return camera2clip(-hw, +hw, -hh, +hh, n_, f_);
    }
    //-----------------------------------------------------------------

    matrix4 camera_factory_imp::create_matrix(int n0, int n1) const
    {
        matrix4 w2c = world2camera(org_, trg_, upr_); //,
        matrix4 clp = mat4_gen::identity();
        matrix4 scn = clip2screen(w_, h_);

        switch (nType)
        {
        case CAMERA_PERSPECTIVE:
            clp = create_perspective_matrix();
            break;
        case CAMERA_ORTHOGONAL:
            clp = create_orthogonal_matrix();
            break;
        }

        matrix4 tbl[3] = {w2c, clp, scn};

        matrix4 mat = mat4_gen::identity();

        /*
			MATRIX_WORLD2CAMERA,
			MATRIX_CAMERA2CLIP,
			MATRIX_CLIP2SCREEN
		*/
        if (n0 < MATRIX_WORLD2CAMERA)
        {
            n0 = MATRIX_WORLD2CAMERA;
        }
        else if (MATRIX_CLIP2SCREEN < n0)
        {
            n0 = MATRIX_CLIP2SCREEN;
        }
        if (n1 < MATRIX_WORLD2CAMERA)
        {
            n1 = MATRIX_WORLD2CAMERA;
        }
        else if (MATRIX_CLIP2SCREEN < n1)
        {
            n1 = MATRIX_CLIP2SCREEN;
        }

        for (int i = n0; i <= n1; i++)
        {
            mat *= tbl[i];
        }

        return mat;
    }

    //----------------------------------

    camera_factory::camera_factory()
    {
        imp_ = new camera_factory_imp();
    }

    camera_factory::~camera_factory()
    {
        delete imp_;
    }

    void camera_factory::init()
    {
        imp_->init();
    }

    void camera_factory::set_origin(const vector3& p)
    {
        imp_->set_origin(p);
    }

    void camera_factory::set_target(const vector3& p)
    {
        imp_->set_target(p);
    }

    void camera_factory::set_upper(const vector3& v)
    {
        imp_->set_upper(v);
    }

    void camera_factory::set_near(real n)
    {
        imp_->set_near(n);
    }

    void camera_factory::set_far(real f)
    {
        imp_->set_far(f);
    }

    void camera_factory::set_horizontal_angle(real angle)
    {
        imp_->set_horizontal_angle(angle);
    }

    void camera_factory::set_vertical_angle(real angle)
    {
        imp_->set_vertical_angle(angle);
    }

    void camera_factory::set_camera_width(real width, real height)
    {
        imp_->set_camera_width(width, height);
    }

    void camera_factory::set_pixel_width(int width, int height)
    {
        imp_->set_pixel_width(width, height);
    }

    bool camera_factory::is_rasterizable() const
    {
        return imp_->is_rasterizable();
    }

    camera* camera_factory::create_camera() const
    {
        return imp_->create_camera();
    }

    matrix4 camera_factory::create_matrix(int m0, int m1) const
    {
        return imp_->create_matrix(m0, m1);
    }

    void camera_factory::set_camera_perspective() {}
    void camera_factory::set_camera_orthogonal() {}
    void camera_factory::set_camera_spherical() {}
    void camera_factory::set_camera_cylidrical() {}
    void camera_factory::set_camera_fisheye() {}
}
