#ifndef KAZE_CAMERA_FACTORY_H
#define KAZE_CAMERA_FACTORY_H

#include "camera.h"

namespace kaze
{

    class camera_factory_imp;
    class camera_factory
    {
    public:
        enum
        {
            MATRIX_WORLD2CAMERA,
            MATRIX_CAMERA2CLIP,
            MATRIX_CLIP2SCREEN
        };

    public:
        camera_factory();
        ~camera_factory();

    public:
        void init();
        void set_origin(const vector3& p);
        void set_target(const vector3& p);
        void set_upper(const vector3& v);

    public:
        void set_origin_position(const vector3& p) { set_origin(p); }
        void set_target_position(const vector3& p) { set_target(p); }

        //void set_horizontal_vector(const vector3& v);
        //void set_vertical_vector(const vector3& v);

        void set_upper_vector(const vector3& v) { set_upper(v); }

        //

        void set_near(real n);
        void set_far(real f);

        void set_near_distance(real n) { set_near(n); }
        void set_far_distance(real f) { set_far(f); }

        void set_horizontal_angle(real angle);
        void set_vertical_angle(real angle);

        void set_camera_width(real width, real height);
        void set_pixel_width(int width, int height);

    public:
        void set_camera_perspective();
        void set_camera_orthogonal();
        void set_camera_spherical();
        void set_camera_cylidrical();
        void set_camera_fisheye();

    public:
        bool is_rasterizable() const;

    public:
        camera* create_camera() const;
        matrix4 create_matrix(int m0 = MATRIX_WORLD2CAMERA, int m1 = MATRIX_CAMERA2CLIP) const;

    private:
        camera_factory_imp* imp_;
    };
}

#endif