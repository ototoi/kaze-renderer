#define _USE_MATH_DEFINES
#include <math.h>
#include "types.h"
#include "parameter_map.h"
#include "memory_image.hpp"

namespace kaze
{

    static void CreateLensDistanceMap(std::shared_ptr<memory_image<double> >& img, const parameter_map& param)
    {
        int blades = param.get_int("blades", 6);                      //Blade
        double roundness = param.get_double("roundness", 0);          //
        double notching = param.get_double("notching", 0);            //
        double notching_pos = param.get_double("notching_pos", 0.85); //

        int width = img->get_width();
        int height = img->get_height();

        double deltaAngle = 2 * M_PI / blades;
        double deltaCos = cos(0.5 * deltaAngle);

        for (int y = 0; y < height; y++)
        {
            double yy = 2 * ((y + 0.5) / height) - 1;
            for (int x = 0; x < width; x++)
            {
                double xx = 2 * ((x + 0.5) / width) - 1;

                double rotMap = (atan2(xx, yy) + M_PI) / (2 * M_PI);
                double rotMapFun = rotMap * blades - floor(rotMap * blades);

                //circle
                double circleShape = sqrt(xx * xx + yy * yy);

                //polygon
                double polygonShape = circleShape * cos(deltaAngle * (fabs(rotMapFun - 0.5))) / deltaCos;

                //roundness
                double roundnessShape = polygonShape + (circleShape - polygonShape) * roundness;

                //notch
                double lv = (roundnessShape - rotMapFun / notching_pos * notching * roundnessShape);
                double rv = (roundnessShape - (1 - (rotMapFun - notching_pos) / (1 - notching_pos)) * notching * roundnessShape);
                double shape = ((rotMapFun < notching_pos) * lv + (rotMapFun >= notching_pos) * rv);

                //
                double brigtness = std::min<double>(shape, 1);
                //
                img->set(x, y, brigtness);
            }
        }
    }

    static void CreateLensBrightnessMap(const std::shared_ptr<memory_image<double> >& img, const std::shared_ptr<memory_image<double> >& dist, const parameter_map& param)
    {
        int width = img->get_width();
        int height = img->get_height();

        for (int y = 0; y < height; y++)
        {
            //double yy = 2*((y+0.5)/height)-1;
            for (int x = 0; x < width; x++)
            {
                //double xx = 2*((x+0.5)/width)-1;

                double d = dist->get(x, y);
                if (d > 1)
                    img->set(x, y, 0);
                else
                    img->set(x, y, 1);
            }
        }
    }
}
