#define _USE_MATH_DEFINES
#include <math.h>
#include "types.h"
#include "parameter_map.h"
#include "memory_image.hpp"

namespace kaze
{
    /*
    float thickness = param.GetFloat("thickness", 0.2f) / 100;//�㉺�̃X�P�[��
        float symmetry = param.GetFloat("symmetry", 1.0f) / 100;//�Е��̐L�т��}����0�`1
        float fanEnds   = (param.GetFloat("fan_ends", 0.4f)+0.001) / 100+1;//�H�̌`��
        float fanFeathering = param.GetFloat("fan_feathering", 0.1f) / 100;//�H���኱�\�t�g�ɂ���
        
        int width  = base_resolution;
        int height = base_resolution;
        int reHeight = height;// /1.5 * thickness * fanEnds;

        //�����I�ɁA�A���J�[�|�C���g�A���[�e�[�V����,�O���[�̔��a���w�肵�ďo�����悤�Ɏ������Ă���
        float frontGain = 1.0f;
        float backGain = 5.0f;
        float gammaV = 0.5f;
        float glowRadius = param.GetFloat("core_intensity", 0.5f) / 1000;
        
        std::shared_ptr<Image> img(new Image(width, reHeight));
        memset(img->GetPtr(), 0, sizeof(float)*width*reHeight );

        //timeStamp();
        for(int y=0;y<reHeight;y++)
        {

            if(y < reHeight/2){
                continue;
            }

            for(int x=0;x<width;x++)
            {   

                float result = 0.0f;
                int wb = 0;
                float shape = 0.0f;
                float color = 0.0f;
                float circleMap = 0.0f;
                float streakMap = 0.0f;
                float map = 0.0f;

                float coordW = (x+0.5-width/2)/width*2;
                float coordH = (y+0.5-reHeight/2)/height*2;

                coordH = coordH / thickness;
  
                if(coordW > 0.0f){
                    coordW = coordW / symmetry;
                }

                //�s�̒l�𐳂̒l�ɂɕϊ�
                coordW = abs(coordW);

                //�v�Z���@���ς��镔���̃}�X�N���쐬�B
                wb = atan2(coordH,coordW)/M_PI*2.0f < atan(fanEnds/1.0f)/M_PI*2.0f;

                //�`�����ό`�����邽�߂ɁA���W���ϊ�
                shape = ((coordH/fanEnds*wb*(thickness>0.0f) + (coordH-coordW*(fanEnds-1.0f))*(1-wb)));

                //���W�����ɃO���f�[�V�������쐬
                circleMap = sqrt(coordW * coordW + coordH * coordH);    //�~�̌`�����\�������}�b�v
                streakMap = sqrt(coordW * coordW + shape * shape);      //�H�̌`�����\�������}�b�v

                //���̃}�b�v�̃u�����h���s��
                map = circleMap*fanFeathering + streakMap*(1-fanFeathering);

                //���̌������\������
                color = attenuationOfLight
                        (
                            map,
                            glowRadius,
                            frontGain,
                            gammaV,
                            backGain
                        );
                
                //�ŏI�I�Ȓl���N�����v
                result = result + max(0.0f, color);

                //�l���Z�b�g
                img->Set(x, y, result);
                img->Set(x, reHeight-y-1, result);
            }

        }


    */

    static void CreateLens(std::shared_ptr<memory_image<double> >& img, const parameter_map& param)
    {
        double thickness = param.get_double("thickness", 0.2);          //
        double symmetry = param.get_double("symmetry", 1.0);            //
        double fanEnds = param.get_double("fan_ends", 0.4) + 1;         //
        double fanFeathering = param.get_double("fan_feathering", 0.1); //

        int width = img->get_width();
        int height = img->get_height();

        for (int y = 0; y < height; y++)
        {
            double yy = 2 * ((y + 0.5) / width) - 1;
            for (int x = 0; x < width; x++)
            {
                //double xx = 2 * ((x + 0.5) / width) - 1;

                //double shape = 1;
                //
                double brigtness = std::min<double>(0, 1);
                //
                img->set(x, y, brigtness);
            }
        }
    }
}
