#define _USE_MATH_DEFINES
#include <math.h>
#include "types.h"
#include "parameter_map.h"
#include "memory_image.hpp"

namespace kaze
{
    /*
        float frontGain = 1.0f;
        float backGain = 1.0f;
        float gammaV = 1.0f;
        float glowRadius  = param.GetFloat("thickness", 0.5f) / 100;

        int width  = base_resolution ;
        int height = base_resolution /2;

        std::shared_ptr<Image> img(new Image(width, height));
        memset(img->GetPtr(), 0, sizeof(float)*width*height );

        for(int y=0;y<height;y++)
        {
            for(int x=0; x<width; x++)
            {  
                //X���𒆐S�ɔ��Α��̓R�s�[���邽�߁A�v�Z�����Ȃ�
                //if(x < width/2){ continue;}

                //���^�[������
                float result = 0.0f;

                //���W�����`
                float harfWidth = (float)width/2.0f;
                float coordW = ((float)x + 0.5 - harfWidth) / height;//((float)height / ((float)width / 2.0f));
                float coordH = float(y) / float(height);

                //���C�����̌���
                float lineColor = attenuationOfLight
                        (
                            coordW,
                            glowRadius,
                            frontGain,
                            gammaV,
                            backGain
                        );

                //�񌹂̌��̌����̃}�b�v���쐬
                float distanceMap = sqrt(coordW * coordW + coordH * coordH);

                //�񌹂����̃}�b�v�ɑ΂��A���̌������ǉ�����
                float distanceColor = attenuationOfLight
                        (
                            distanceMap,
                            glowRadius,
                            frontGain,
                            gammaV,
                            backGain
                        );

                //�O�����g�̋؂Ɍ񌹂����̌��������Z
                result = distanceColor * lineColor;

    */

    static void CreateLens(std::shared_ptr<memory_image<double> >& img, const parameter_map& param)
    {
        int blades = param.get_int("blades", 6);                      //Blade
        double roundness = param.get_double("roundness", 0);          //
        double notching = param.get_double("notching", 0);            //
        double notching_pos = param.get_double("notching_pos", 0.85); //

        int width = img->get_width();
        int height = img->get_height();

        double deltaAngle = 2 * M_PI / blades;
        double deltaCos = cos(0.5 * deltaAngle);

        for (int y = 0; y < height; y++)
        {
            double yy = 2 * ((y + 0.5) / height) - 1;
            for (int x = 0; x < width; x++)
            {
                double xx = 2 * ((x + 0.5) / width) - 1;

                double rotMap = (atan2(xx, yy) + M_PI) / (2 * M_PI);
                double rotMapFun = rotMap * blades - floor(rotMap * blades);

                //circle
                double circleShape = sqrt(xx * xx + yy * yy);

                //polygon
                double polygonShape = circleShape * cos(deltaAngle * (fabs(rotMapFun - 0.5))) / deltaCos;

                //roundness
                double roundnessShape = polygonShape + (circleShape - polygonShape) * roundness;

                //notch
                double lv = (roundnessShape - rotMapFun / notching_pos * notching * roundnessShape);
                double rv = (roundnessShape - (1 - (rotMapFun - notching_pos) / (1 - notching_pos)) * notching * roundnessShape);
                double shape = ((rotMapFun < notching_pos) * lv + (rotMapFun >= notching_pos) * rv);

                //
                double brigtness = std::min<double>(shape, 1);
                //
                img->set(x, y, brigtness);
            }
        }
    }
}
