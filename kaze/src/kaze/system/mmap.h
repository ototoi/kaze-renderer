#ifndef KAZE_SYSTEM_MMAP_H
#define KAZE_SYSTEM_MMAP_H

#include "file.h"

#include <cerrno>

#ifdef _WIN32
#include <windows.h>
#define KAZE_USE_WIN_MMAP
#endif

#if defined(__unix__) || defined(__APPLE__)
#include <unistd.h>
#ifdef _POSIX_MAPPED_FILES
#if _POSIX_MAPPED_FILES > 0
#include <sys/mman.h>
#include <syscall.h>
#define KAZE_USE_POSIX_MMAP
#endif //
#endif //_POSIX_MAPPED_FILES
#endif //__unix__

namespace kaze
{
    namespace system
    {

        enum
        {
            MMAP_OPEN_READ = 0x1,
            MMAP_OPEN_WRITE = 0x2
        };

#ifdef KAZE_USE_WIN_MMAP
        enum
        {
            MMAP_SHARE_PUBLIC,
            MMAP_SHARE_PRIVATE
        };

        typedef DWORD mmap_mode_t;

        typedef struct mmap_handle_struct
        {
            void* pointer;
            size_t size;
            HANDLE handle;
        } mmap_handle;

#endif

#ifdef KAZE_USE_POSIX_MMAP
        enum
        {
            MMAP_SHARE_PUBLIC = MAP_SHARED,
            MMAP_SHARE_PRIVATE = MAP_PRIVATE
        };

        typedef int mmap_mode_t;

        typedef struct mmap_handle_struct
        {
            void* pointer;
            size_t size;
        } mmap_handle;

#endif

        inline void* get_pointer(const mmap_handle& hdl)
        {
            return hdl.pointer;
        }

//----------------------------------------------------------------
//----------------------------------------------------------------

#ifdef KAZE_USE_WIN_MMAP

        enum
        {
            MMAP_ACCESS_READ = PAGE_READONLY,
            MMAP_ACCESS_WRITE = PAGE_READWRITE,
            MMAP_ACCESS_RW = PAGE_READWRITE,
        };

#define KAZE_L_DWD(x) ((DWORD)(0xffffffff & x))
//#define KAZE_H_DWD(x) ((DWORD)(0xffffffff&(x>>32)))
#define KAZE_H_DWD(x) ((sizeof(size_t) <= 4) ? 0 : (DWORD)(0xffffffff & (Int64ShrlMod32(x, 32))))

        inline bool create_mmap(mmap_handle* mmh, file_handle fd, mmap_mode_t open_flag, mmap_mode_t share_flag, size_t sz)
        {

            HANDLE mmap_h;
            void* p_tmp;
            DWORD mmv_flag;

            (mmap_mode_t) share_flag;

            if (sz < 1) return false;

            int access_flag = 0;

            if (open_flag & MMAP_OPEN_READ)
            {
                if (open_flag & MMAP_OPEN_WRITE)
                {
                    access_flag = MMAP_ACCESS_RW;
                    mmv_flag = FILE_MAP_WRITE;
                }
                else
                {
                    //read only
                    access_flag = MMAP_ACCESS_READ;
                    mmv_flag = FILE_MAP_READ;
                }
            }
            else
            {
                return false;
            }

            //DWORD hhh = KAZE_H_DWD(sz);
            //DWORD lll = KAZE_L_DWD(sz);

            mmap_h = ::CreateFileMapping(fd, NULL, access_flag, KAZE_H_DWD(sz), KAZE_L_DWD(sz), NULL);

            if (mmap_h == NULL)
            {
                return false;
            }

            p_tmp = ::MapViewOfFile(mmap_h, mmv_flag, 0, 0, (SIZE_T)sz);

            if (p_tmp == NULL)
            {
                CloseHandle(mmap_h);
                return false;
            }

            mmh->pointer = p_tmp;
            mmh->size = sz;
            mmh->handle = mmap_h;

            return true;
        }

        inline bool close_mmap(const mmap_handle& mmh)
        {
            BOOL b_success = TRUE;
            b_success &= ::UnmapViewOfFile(mmh.pointer);
            b_success &= ::CloseHandle(mmh.handle);
            if (b_success)
                return true;
            else
                return false;
        }

        inline bool flush_mmap(const mmap_handle& mmh)
        {
            if (::FlushViewOfFile(mmh.pointer, mmh.size))
                return true;
            else
                return false;
        }

#undef KAZE_L_DWD
#undef KAZE_H_DWD

#endif //KAZE_USE_WIN_MMAP

#ifdef KAZE_USE_POSIX_MMAP

        enum
        {
            MMAP_ACCESS_READ = PROT_READ,
            MMAP_ACCESS_WRITE = PROT_WRITE,
            MMAP_ACCESS_RW = PROT_READ | PROT_WRITE,
        };

        inline bool create_mmap(mmap_handle* mmh, file_handle fd, mmap_mode_t open_flag, mmap_mode_t share_flag, size_t sz)
        {

            if (sz < 1) return false;

            bool nofile = false;
            std::string tmp_str;

            if (fd < 0)
            {
                char tmp_path[500];
                if (0 > get_temppath_nodelete(tmp_path, 500, "kzm")) return false;
                if (!open_file(&fd, tmp_path, FILE_OPEN_READ | FILE_OPEN_WRITE | FILE_OPEN_CREATE)) return false;
                nofile = true;
                tmp_str = tmp_path;
            }

#ifdef BSD
            off_t psize = getpagesize();
#else
            off_t psize = sysconf(_SC_PAGE_SIZE);
#endif
            int access_flag = 0;

            if (open_flag & MMAP_OPEN_READ)
            {
                if (open_flag & MMAP_OPEN_WRITE)
                {
                    access_flag = MMAP_ACCESS_RW;
                }
                else
                {
                    //read only
                    access_flag = MMAP_ACCESS_READ;
                }
            }
            else
            {
                if (open_flag & MMAP_OPEN_WRITE)
                {
                    access_flag = MMAP_ACCESS_WRITE;
                }
                else
                {
                    return false;
                }
            }

            size_t size = (((sz - 1) / (psize)) + 1) * psize;
            //size_t size=((sz/psize)+ 1)*psize;

            {
                struct stat buf;
                fstat(fd, &buf); //get file size.

                if (buf.st_size < (off_t)size)
                {
                    if (access_flag & PROT_WRITE)
                    {
                        if (lseek(fd, size, SEEK_SET) < 0)
                        {
                            std::perror("create_mmap");
                            return false;
                        }
                        char c = 0;
                        if (write(fd, &c, sizeof(char)) == -1)
                        {
                            std::perror("create_mmap");
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            //#ifndef _LARGEFILE64_SOURCE
            void* p_tmp = mmap(0, size, access_flag, share_flag, fd, 0);
            //#else
            //void* p_tmp = mmap64(0,size,access_flag,share_flag,fd,0);
            //#endif
            if (p_tmp == MAP_FAILED)
            {
                std::perror("create_mmap");
                return false;
            }

            if (nofile)
            {
                unlink(tmp_str.c_str()); //
            }

            mmh->pointer = p_tmp;
            mmh->size = size;
            return true;
        }

        inline bool close_mmap(const mmap_handle& mmh)
        {
            return (0 == munmap(mmh.pointer, mmh.size));
        }

        inline bool flush_mmap(const mmap_handle& mmh)
        {
            return (0 == msync(mmh.pointer, mmh.size, 0));
        }

#endif //KAZE_USE_POSIX_MMAP
    }
}

#endif
