#ifndef KAZE_OS_VERSION_H
#define KAZE_OS_VERSION_H

#ifdef _WIN32
#include <windows.h>
#endif

#if defined(__unix__) || defined(__APPLE__)
#include <sys/utsname.h>
#endif

#include <cstdio>
#include <string>
#include <cstring>

#ifdef _MSC_VER
//'_splitpath': This function or variable may be unsafe.
#pragma warning(disable : 4996)
#endif

namespace kaze
{
    namespace system
    {

#ifdef _WIN32
        inline std::string os_version()
        {
            using namespace std;
            char verbuf[256];

            OSVERSIONINFO VersionInfo;

            VersionInfo.dwOSVersionInfoSize = sizeof(VersionInfo);
            if (::GetVersionEx(&VersionInfo))
            {
                //if(::_tcslen(VersionInfo.szCSDVersion) > 200)VersionInfo.szCSDVersion[100] = 0;

                sprintf(verbuf, "Windows %d.%d build%d PlatformId %d SP=\"%s\"",
                        VersionInfo.dwMajorVersion,
                        VersionInfo.dwMinorVersion,
                        VersionInfo.dwBuildNumber,
                        VersionInfo.dwPlatformId,
                        VersionInfo.szCSDVersion);
            }
            else
            {
                strcpy(verbuf, "WINDOWS UNKNOWN");
            }

            return std::string(verbuf);
        }

#elif defined(__unix__)

        inline std::string os_version()
        {
            using namespace std;
            char verbuf[4 * SYS_NMLN + 4];

            struct utsname ubuf;

            if (uname(&ubuf))
            {
                strcpy(verbuf, "UNKNOWN");
            }
            else
            {
                sprintf(verbuf, "%s %s %s %s",
                        ubuf.sysname,
                        ubuf.release,
                        ubuf.version,
                        ubuf.machine);
            }

            return std::string(verbuf);
        }

#else
        inline std::string os_version()
        {
            return "";
        }
#endif
    }
}

#endif
