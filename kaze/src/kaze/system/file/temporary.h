#ifndef KAZE_SYSTEM_TEMPORARY
#define KAZE_SYSTEM_TEMPORARY
//KAZE_SYSTEM_TEMP

#ifdef _WIN32
#include <windows.h> //
#endif

#ifdef __unix__
#include <cstdlib> //int mkstemp(char *template);
#endif

#include <vector>
#include <string>
#include <cstdlib>

namespace kaze
{
    namespace system
    {

#ifdef _WIN32

        inline int get_tempdir(char* buffer, size_t size)
        {
            DWORD nsz = ::GetTempPathA((DWORD)size, buffer);
            if (nsz == 0) return -1;
            return nsz;
        }

        inline int get_tempfile(char* buffer, size_t size, const char* tmpdir, const char* prefix)
        {

            if (std::strlen(tmpdir) + 15 > size) return -1;

            if (0 == ::GetTempFileNameA(tmpdir, prefix, 0, buffer)) return -1;

            DeleteFileA(buffer);

            return (int)(std::strlen(buffer) + 1);
        }

        inline int get_tempfile_nodelete(char* buffer, size_t size, const char* tmpdir, const char* prefix)
        {

            if (std::strlen(tmpdir) + 15 > size) return -1;

            if (0 == ::GetTempFileNameA(tmpdir, prefix, 0, buffer)) return -1;

            //DeleteFileA(buffer);

            return (int)(std::strlen(buffer) + 1);
        }

#endif

#if defined(__unix__) | __APPLE__

        static bool test_tempdir(const char* temp_dir)
        {
            std::string tmp(temp_dir);
            tmp += std::string("/kz_tmp.XXXXXX");

            std::vector<char> buffer(tmp.size() + 1);

            std::strcpy(&buffer[0], tmp.c_str());

            int fd = mkstemp(&buffer[0]);

            if (fd == -1) return false;

            unlink(&buffer[0]);
            close(fd);
            return true;
        }

        inline int get_tempdir(char* buffer, size_t size)
        {
            static const char* try_envs[] = {"TMP", "TEMP", "TMPDIR"};
            static const char* try_dirs[] = {"/tmp", "/usr/tmp", "/var/tmp"};

            const char* cp = NULL;

            for (std::size_t i = 0; i < (sizeof(try_envs) / sizeof(const char*)); i++)
            {
                cp = getenv(try_envs[i]);
                if (cp != NULL) break;
            }

            if (cp == NULL)
            {
                for (std::size_t i = 0; i < (sizeof(try_dirs) / sizeof(const char*)); i++)
                {
                    if (test_tempdir(try_dirs[i]))
                    {
                        cp = try_dirs[i];
                        break;
                    }
                }
            }

            if (cp == NULL) return -1;

            size_t sz = std::strlen(cp) + 1;
            if (size < sz) return -1;
            std::strcpy(buffer, cp);
            return sz;
        }

        inline int get_tempfile(char* buffer, size_t size, const char* tmpdir, const char* prefix)
        {
            std::string temp(tmpdir);

            char lc = temp[temp.size() - 1];
            if (lc != '/')
            {
                if (lc != '\\')
                {
                    temp.push_back('/');
                }
            }

            temp += std::string(prefix);
            temp += std::string("XXXXXX");

            size_t sz = temp.size() + 1;

            if (sz > size) return -1;

            std::strcpy(buffer, temp.c_str());

            int fd = mkstemp(buffer);

            if (fd == -1) return -1;

            close(fd);
            unlink(buffer);
            return sz;
        }

        inline int get_tempfile_nodelete(char* buffer, size_t size, const char* tmpdir, const char* prefix)
        {
            std::string temp(tmpdir);

            char lc = temp[temp.size() - 1];
            if (lc != '/')
            {
                if (lc != '\\')
                {
                    temp.push_back('/');
                }
            }

            temp += std::string(prefix);
            temp += std::string("XXXXXX");

            size_t sz = temp.size() + 1;

            if (sz > size) return -1;

            std::strcpy(buffer, temp.c_str());

            int fd = mkstemp(buffer);

            if (fd == -1) return -1;

            close(fd);
            //unlink(buffer);
            return sz;
        }

#endif

        inline int get_temppath(char* buffer, size_t size, const char* prefix)
        {
            int sz;
            if (0 > get_tempdir(buffer, size)) return -1;
            std::string tmp(buffer);
            if (0 > (sz = get_tempfile(buffer, size, tmp.c_str(), prefix))) return -1;
            return sz;
        }

        inline int get_temppath_nodelete(char* buffer, size_t size, const char* prefix)
        {
            int sz;
            if (0 > get_tempdir(buffer, size)) return -1;
            std::string tmp(buffer);
            if (0 > (sz = get_tempfile_nodelete(buffer, size, tmp.c_str(), prefix))) return -1;
            return sz;
        }
    }
}

#endif
