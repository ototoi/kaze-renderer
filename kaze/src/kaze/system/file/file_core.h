#ifndef KAZE_FILE_CORE_H
#define KAZE_FILE_CORE_H

#ifdef _WIN32
#include <windows.h>
#endif

#if defined(__unix__) || defined(__APPLE__)
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <fcntl.h>
#endif

#include <cstdlib>

namespace kaze
{
    namespace system
    {

        enum
        {
            FILE_OPEN_READ = 0x1,
            FILE_OPEN_WRITE = 0x2,
            FILE_OPEN_CREATE = 0x4,
            FILE_OPEN_APPEND = 0x8,
            FILE_OPEN_TRUNCATE = 0x10, //write & append

            FILE_OPEN_BINARY = 0x20,
            FILE_OPEN_EXCL = 0x40,

            FILE_OPEN_LARGEFILE = 0x4000,

            FILE_OPEN_NOSHARE = 0x8000
        };

        enum
        {
            FILE_MODE_DEFAULT = 0x0666,
            FILE_MODE_TEMP = 0x0666,

            FILE_MODE_UREAD = 0x0400,
            FILE_MODE_UWRITE = 0x0200,
            FILE_MODE_UEXECUTE = 0x0100,

            FILE_MODE_GREAD = 0x0040,
            FILE_MODE_GWRITE = 0x0020,
            FILE_MODE_GEXECUTE = 0x0010,

            FILE_MODE_WREAD = 0x0004,
            FILE_MODE_WWRITE = 0x0002,
            FILE_MODE_WEXECUTE = 0x0001
        };

#ifdef _WIN32

        typedef HANDLE file_handle; //�n���h��
        typedef DWORD file_mode_t;

        inline file_handle invalid_file_handle() { return INVALID_HANDLE_VALUE; }

        inline bool open_file(file_handle* fh, const char* filepath, file_mode_t flag, file_mode_t mode = FILE_MODE_DEFAULT)
        {
            file_handle tfh;

            DWORD oflags = 0;
            DWORD createflags = 0;
            DWORD attributes = FILE_ATTRIBUTE_NORMAL;
            DWORD sharemode = FILE_SHARE_READ | FILE_SHARE_WRITE;

            if (flag & FILE_OPEN_READ)
            {
                oflags |= GENERIC_READ;
            }
            if (flag & FILE_OPEN_WRITE)
            {
                oflags |= GENERIC_WRITE;
            }

            if (flag & FILE_OPEN_CREATE)
            {
                if (flag & FILE_OPEN_EXCL)
                {
                    /* only create new if file does not already exist */
                    createflags = CREATE_NEW;
                }
                else if (flag & FILE_OPEN_TRUNCATE)
                {
                    /* truncate existing file or create new */
                    createflags = CREATE_ALWAYS;
                }
                else
                {
                    /* open existing but create if necessary */
                    createflags = OPEN_ALWAYS;
                }
            }
            else if (flag & FILE_OPEN_TRUNCATE)
            {
                /* only truncate if file already exists */
                createflags = TRUNCATE_EXISTING;
            }
            else
            {
                /* only open if file already exists */
                createflags = OPEN_EXISTING;
            }

            if ((flag & FILE_OPEN_EXCL) && !(flag & FILE_OPEN_CREATE))
            {
                return false;
            }

#if (_WIN32_WINNT >= 0x0400)
            sharemode |= FILE_SHARE_DELETE;
#endif

            if (flag & FILE_OPEN_NOSHARE)
            {
                sharemode = 0;
            }

            tfh = ::CreateFileA(filepath, oflags, sharemode, NULL, createflags, attributes, NULL);

            if (tfh == INVALID_HANDLE_VALUE)
            {
                return false;
            }
            else
            {
                *fh = tfh;
            }

            if (flag & FILE_OPEN_APPEND)
            {
                ::SetFilePointer(tfh, 0, NULL, FILE_END);
            }

            return true;
        }

        template <bool b64bit = true>
        struct file_util__
        {
            static BOOL seek_offset(file_handle h, std::size_t offset)
            {
                LARGE_INTEGER li;
                li.QuadPart = offset;
                return ::SetFilePointer(h, li.LowPart, &li.HighPart, FILE_BEGIN);
            }
        };
        template <>
        struct file_util__<false>
        {
            static BOOL seek_offset(file_handle h, std::size_t offset)
            {
                return ::SetFilePointer(h, (LONG)offset, NULL, FILE_BEGIN);
            }
        };

        inline void seek_offset(file_handle h, std::size_t offset)
        {
            file_util__<sizeof(std::size_t) == 8>::seek_offset(h, offset);
        }

        inline bool close_file(file_handle h)
        {
            if (::CloseHandle(h))
                return true;
            else
                return false;
        }

        inline bool flush_file(file_handle h)
        {
            if (::FlushFileBuffers(h))
                return true;
            else
                return false;
        }

        inline bool remove_file(const char* filepath)
        {
            if (::DeleteFileA(filepath))
                return true;
            else
                return false;
        }

        inline bool delete_file(const char* filepath)
        {
            return remove_file(filepath);
        }

        inline size_t maxpath() { return (size_t)MAX_PATH; }

#endif

#if defined(__unix__) || defined(__APPLE__)

        typedef int file_handle;
        typedef int file_mode_t;

        inline file_handle invalid_file_handle() { return (-1); }

        inline file_mode_t parms2mode(file_mode_t perms)
        {
            int mode = 0;

            if (perms & FILE_MODE_UREAD)
                mode |= S_IRUSR;
            if (perms & FILE_MODE_UWRITE)
                mode |= S_IWUSR;
            if (perms & FILE_MODE_UEXECUTE)
                mode |= S_IXUSR;

            if (perms & FILE_MODE_GREAD)
                mode |= S_IRGRP;
            if (perms & FILE_MODE_GWRITE)
                mode |= S_IWGRP;
            if (perms & FILE_MODE_GEXECUTE)
                mode |= S_IXGRP;

            if (perms & FILE_MODE_WREAD)
                mode |= S_IROTH;
            if (perms & FILE_MODE_WWRITE)
                mode |= S_IWOTH;
            if (perms & FILE_MODE_WEXECUTE)
                mode |= S_IXOTH;

            return mode;
        }

        inline bool open_file(file_handle* fh, const char* filepath, file_mode_t flag, file_mode_t mode = FILE_MODE_DEFAULT)
        {
            file_handle tfh;

            int oflags = 0;

            if ((flag & FILE_OPEN_READ) && (flag & FILE_OPEN_WRITE))
            {
                oflags = O_RDWR;
            }
            else if (flag & FILE_OPEN_READ)
            {
                oflags = O_RDONLY;
            }
            else if (flag & FILE_OPEN_WRITE)
            {
                oflags = O_WRONLY;
            }
            else
            {
                return false;
            }

            if (flag & FILE_OPEN_CREATE)
            {
                oflags |= O_CREAT;
                if (flag & FILE_OPEN_EXCL)
                {
                    oflags |= O_EXCL;
                }
            }
            if ((flag & FILE_OPEN_EXCL) && !(flag & FILE_OPEN_CREATE))
            {
                return false;
            }

            if (flag & FILE_OPEN_APPEND)
            {
                oflags |= O_APPEND;
            }
            if (flag & FILE_OPEN_TRUNCATE)
            {
                oflags |= O_TRUNC;
            }

#ifdef O_BINARY
            if (flag & FILE_OPEN_BINARY)
            {
                oflags |= O_BINARY;
            }
#endif

#if defined(_LARGEFILE64_SOURCE)
            oflags |= O_LARGEFILE;
#elif defined(O_LARGEFILE)
            if (flag & FILE_OPEN_LARGEFILE)
            {
                oflags |= O_LARGEFILE;
            }
#endif

            tfh = open(filepath, oflags, parms2mode(mode));

            if (tfh < 0)
            {
                return false;
            }
            else
            {
                *fh = tfh;
                return true;
            }
        }

        template <class T>
        struct seek_selecter
        {
            static inline T seek(int fd, T offset, int origin) { return lseek(fd, (off_t)offset, origin); }
        };
#ifndef __APPLE__
        template <>
        struct seek_selecter<unsigned long long>
        {
            static unsigned long long seek(int fd, unsigned long long offset, int origin) { return lseek64(fd, offset, origin); }
        };
#endif

        inline void seek_offset(file_handle h, std::size_t offset)
        {
            seek_selecter<std::size_t>::seek(h, offset, SEEK_SET);
        }

        inline bool close_file(file_handle h)
        {
            return (0 == ::close(h));
        }

        inline bool flush_file(file_handle h)
        {
            return (0 == ::fsync(h));
        }

        inline bool remove_file(const char* filepath)
        {
            return (0 == ::unlink(filepath));
        }

        inline bool delete_file(const char* filepath)
        {
            return remove_file(filepath);
        }

        inline size_t maxpath() { return (size_t)1024; }
#endif
    }
}

#if 0	
	
inline int conv_utf8_to_ucs2(const char *in, size_t *inbytes, wchar_t *out, size_t *outwords)
{
    __int64 newch, mask;
    size_t expect, eating;
    int ch;
    
    while (*inbytes && *outwords) 
    {
        ch = (unsigned char)(*in++);
        if (!(ch & 0200)) {
            /* US-ASCII-7 plain text
             */
            --*inbytes;
            --*outwords;
            *(out++) = ch;
        }
        else
        {
            if ((ch & 0300) != 0300) { 
                /* Multibyte Continuation is out of place
                 */
                return 1;
            }
            else
            {
                /* Multibyte Sequence Lead Character
                 *
                 * Compute the expected bytes while adjusting
                 * or lead byte and leading zeros mask.
                 */
                mask = 0340;
                expect = 1;
                while ((ch & mask) == mask) {
                    mask |= mask >> 1;
                    if (++expect > 3) /* (truly 5 for ucs-4) */
                        return 1;
                }
                newch = ch & ~mask;
                eating = expect + 1;
                if (*inbytes <= expect)
                    return 2;
                /* Reject values of excessive leading 0 bits
                 * utf-8 _demands_ the shortest possible byte length
                 */
                if (expect == 1) {
                    if (!(newch & 0036))
                        return 1;
                }
                else {
                    /* Reject values of excessive leading 0 bits
                     */
                    if (!newch && !((unsigned char)*in & 0077 & (mask << 1)))
                        return 1;
                    if (expect == 2) {
                        /* Reject values D800-DFFF when not utf16 encoded
                         * (may not be an appropriate restriction for ucs-4)
                         */
                        if (newch == 0015 && ((unsigned char)*in & 0040))
                            return 1;
                    }
                    else if (expect == 3) {
                        /* Short circuit values > 110000
                         */
                        if (newch > 4)
                            return 1;
                        if (newch == 4 && ((unsigned char)*in & 0060))
                            return 1;
                    }
                }
                /* Where the boolean (expect > 2) is true, we will need
                 * an extra word for the output.
                 */
                if (*outwords < (size_t)(expect > 2) + 1) 
                    break; /* buffer full */
                while (expect--)
                {
                    /* Multibyte Continuation must be legal */
                    if (((ch = (unsigned char)*(in++)) & 0300) != 0200)
                        return 1;
                    newch <<= 6;
                    newch |= (ch & 0077);
                }
                *inbytes -= eating;
                /* newch is now a true ucs-4 character
                 *
                 * now we need to fold to ucs-2
                 */
                if (newch < 0x10000) 
                {
                    --*outwords;
                    *(out++) = (wchar_t) newch;
                }
                else 
                {
                    *outwords -= 2;
                    newch -= 0x10000;
                    *(out++) = (wchar_t) (0xD800 | (newch >> 10));
                    *(out++) = (wchar_t) (0xDC00 | (newch & 0x03FF));                    
                }
            }
        }
    }
    /* Buffer full 'errors' aren't errors, the client must inspect both
     * the inbytes and outwords values
     */
    return 0;
}
	
	inline int utf8_to_unicode_path(wchar_t* retstr, size_t retlen, const char* srcstr){
	
		size_t srcremains = strlen(srcstr) + 1;
		wchar_t *t = retstr;
		int rv;

		if (srcremains > MAX_PATH) {
			if (srcstr[1] == ':' && (srcstr[2] == '/' || srcstr[2] == '\\')) {
				wcscpy (retstr, L"\\\\?\\");
				retlen -= 4;
				t += 4;
			}
			else if ((srcstr[0] == '/' || srcstr[0] == '\\')
				  && (srcstr[1] == '/' || srcstr[1] == '\\')
				  && (srcstr[2] != '?')) {
				/* Skip the slashes */
				srcstr += 2;
				srcremains -= 2;
				wcscpy (retstr, L"\\\\?\\UNC\\");
				retlen -= 8;
				t += 8;
			}
		}

		if (rv = conv_utf8_to_ucs2(srcstr, &srcremains, t, &retlen)) {
			return false;
		}
		if (srcremains) {
			return false;
		}
		for (; *t; ++t)
			if (*t == L'/')*t = L'\\';
		
		
		return true;
	}
#endif

#endif
