#ifndef KAZE_FILE_READ_WRITE_H
#define KAZE_FILE_READ_WRITE_H

#include "file_core.h"

namespace kaze
{

#ifdef _WIN32
    typedef int ssize_t;
#endif

    namespace system
    {

#ifdef _WIN32
        inline ssize_t read(file_handle h, void* buffer, size_t sz)
        {
            DWORD nRead = 0;
            if (::ReadFile(h, buffer, (DWORD)sz, &nRead, NULL))
            {
                return (int)nRead;
            }
            return -1;
        }

        inline ssize_t write(file_handle h, const void* buffer, size_t sz)
        {
            DWORD nRead = 0;
            if (::WriteFile(h, buffer, (DWORD)sz, &nRead, NULL))
            {
                return (int)nRead;
            }
            return -1;
        }

#endif

#if defined(__unix__) || defined(__APPLE__)
        inline ssize_t read(file_handle h, void* buffer, size_t sz)
        {
            return ::read(h, buffer, sz);
        }
        inline ssize_t write(file_handle h, const void* buffer, size_t sz) { return ::write(h, buffer, sz); }
#endif
    }
}

#endif
