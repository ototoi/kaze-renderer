#ifndef KAZE_SYSTEM_BEGIN_H
#define KAZE_SYSTEM_BEGIN_H

#ifdef _WIN32

#ifndef _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES
#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1
#endif

#include <windows.h>

#endif

#endif
