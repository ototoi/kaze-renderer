#ifndef KAZE_SYSTEM_H
#define KAZE_SYSTEM_H

#include "begin.h"

#include "marker.h"
#include "os_version.h"
#include "thread.h"
#include "file.h"
#include "timer.h"

#include "end.h"

#endif
