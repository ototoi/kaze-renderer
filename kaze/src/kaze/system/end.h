#ifndef KAZE_SYSTEM_END_H
#define KAZE_SYSTEM_END_H

#ifdef _WIN32
#include <windef.h>

#ifndef KAZE_NO_KILL_WIN_DEF
#ifdef min
#undef min
#endif

#ifdef max
#undef max
#endif

#ifdef near
#undef near
#endif

#ifdef far
#undef far
#endif

#ifdef _near
#undef _near
#endif

#ifdef _far
#undef _far
#endif

#ifdef __near
#undef __near
#endif

#ifdef __far
#undef __far
#endif

#endif
#endif //_WiN32

#endif
