#ifndef KAZE_SYSTEM_MARKER_H
#define KAZE_SYSTEM_MARKER_H

#include <string>

#ifdef _WIN32
#include <windows.h>
#endif

#define KAZE_MARKER_USE_OPENMUTEX

namespace kaze
{
    namespace system
    {

#ifdef _WIN32 //windows version

        inline HANDLE set_marker(const char* cstr)
        {
            std::string str(cstr);
            std::size_t sz = str.size();
            if (sz > MAX_PATH) return false;

            for (std::size_t i = 0; i < sz; i++)
            {
                if (str[i] == '\\')
                {
                    str[i] = '_';
                }
            }

            return ::CreateMutexA(NULL, FALSE, str.c_str());
        }

        inline void unset_marker(HANDLE h)
        {
            try
            {
                ::CloseHandle(h);
            }
            catch (...)
            {
                //Do not do!
            }
        }
        //----------------------------------------------------
        //----------------------------------------------------
        class marker
        {
        public:
            marker(const char* cstr) : h(set_marker(cstr)) {}
            ~marker() { unset_marker(h); }
        private:
            HANDLE h;
        };

        inline bool check_marker(const char* cstr, bool check_str = true)
        {
            HANDLE h;
            if (check_str)
            {
                std::string str(cstr);
                std::size_t sz = str.size();
                if (sz > MAX_PATH) return false;

                for (std::size_t i = 0; i < sz; i++)
                {
                    if (str[i] == '\\')
                    {
                        str[i] = '_';
                    }
                }
#ifdef KAZE_MARKER_USE_OPENMUTEX
                h = ::OpenMutexA(MUTEX_ALL_ACCESS, FALSE, str.c_str());
            }
            else
            {
                h = ::OpenMutexA(MUTEX_ALL_ACCESS, FALSE, cstr);
            }

            if (h != NULL)
            {
                CloseHandle(h);
                return true;
            }
            return false;
#else
                h = ::CreateMutexA(NULL, FALSE, str.c_str());
            }
            else
            {
                h = ::CreateMutexA(NULL, FALSE, cstr);
            }

            if (::GetLastError() == ERROR_ALREADY_EXISTS)
            {
                ::CloseHandle(h);
                return true;
            }
            else
            {
                ::CloseHandle(h);
                return false;
            }
#endif
        }

#else //end of _WIN32

#endif
    }
}

#endif
