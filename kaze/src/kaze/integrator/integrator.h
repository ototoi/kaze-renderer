#ifndef KAZE_INTEGRATOR_H
#define KAZE_INTEGRATOR_H

#include "sufflight.h"
#include "mediation.h"
#include "radiance.h"
#include "aggregator.hpp"

namespace kaze
{
    class integrator
    {
    public:
        virtual ~integrator() {}
        virtual void illuminate(aggregator<radiance>* rads, const sufflight& sl, const mediation& media) const = 0;
    };

    class null_integrator : public integrator
    {
    public:
        void illuminate(aggregator<radiance>* rads, const sufflight& sl, const mediation& media) const { ; }
    };
}

#endif