#ifndef KAZE_PHOTONMAP_INTEGRATOR_H
#define KAZE_PHOTONMAP_INTEGRATOR_H

#include "integrator.h"
#include "photonmap.h"

#include "count_ptr.hpp"

namespace kaze
{
    class photonmap_integrator : public integrator
    {
    public:
        photonmap_integrator(const auto_count_ptr<photonmap>& pm, real rad);
        ~photonmap_integrator();
        void illuminate(aggregator<radiance>* rads, const sufflight& sl, const mediation& media) const;

    protected:
        std::shared_ptr<photonmap> pp_;
        real rad_;
    };
}

#endif