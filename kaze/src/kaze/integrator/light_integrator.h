#ifndef KAZE_LIGHT_INTEGRATOR_H
#define KAZE_LIGHT_INTEGRATOR_H

#include "integrator.h"
#include "light.h"
#include "count_ptr.hpp"

namespace kaze
{
    class light_integrator : public integrator
    {
    public:
        light_integrator(const auto_count_ptr<light>& pl);
        ~light_integrator();
        void illuminate(aggregator<radiance>* rads, const sufflight& sl, const mediation& media) const;

    private:
        std::shared_ptr<light> pl_;
    };
}

#endif
