#include "photonmap_integrator.h"

namespace kaze
{

    namespace
    {
        class photon_aggregator : public aggregator<photon>
        {
        public:
            void add(const photon& p) { p_ = p; }
            const photon& get_photon() const { return p_; }
        private:
            photon p_;
        };

        inline radiance p2i(const photon& p)
        {
            return radiance(p.direction(), p.power());
        }
    }

    photonmap_integrator::photonmap_integrator(const auto_count_ptr<photonmap>& pm, real rad) : pp_(pm), rad_(rad) {}
    photonmap_integrator::~photonmap_integrator() {}

    void photonmap_integrator::illuminate(aggregator<radiance>* rads, const sufflight& sl, const mediation& media) const
    {
        photon_aggregator pa;
        if (pp_->search(&pa, sl.position(), rad_))
        {
            rads->add(p2i(pa.get_photon()));
        }
    }
}
