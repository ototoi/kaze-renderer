#include "light_integrator.h"
#include "light.h"

#include "aggregator.hpp"

namespace kaze
{
    namespace
    {
        inline radiance l2i(const lightpath& lp)
        {
            return radiance(normalize(lp.to() - lp.from()), lp.power());
        }

        class light_converter : public aggregator<lightpath>
        {
        public:
            light_converter(aggregator<radiance>* rads) : rads_(rads) {}
            void add(const lightpath& lp)
            {
                rads_->add(l2i(lp));
            }

        private:
            aggregator<radiance>* rads_;
        };
    }

    light_integrator::light_integrator(const auto_count_ptr<light>& pl) : pl_(pl) {}
    light_integrator::~light_integrator() {}

    void light_integrator::illuminate(aggregator<radiance>* rads, const sufflight& sl, const mediation& media) const
    {
        light_converter lpf(rads);
        pl_->cast(&lpf, sl);
    }
}
