#ifndef KAZE_CORRESPONDED_IMAGE_HPP
#define KAZE_CORRESPONDED_IMAGE_HPP

#include "image.hpp"
#include "corresponder.h"
#include "count_ptr.hpp"

namespace kaze
{

    template <class T>
    class corresponded_image : public image<T>
    {
    public:
        corresponded_image(
            const auto_count_ptr<image<T> >& img,
            const auto_count_ptr<corresponder>& cor_w,
            const auto_count_ptr<corresponder>& cor_h) : img_(img), cor_w_(cor_w), cor_h_(cor_h)
        {
            init();
        }

        int get_width() const { return img_->get_width(); }
        int get_height() const { return img_->get_height(); }
        T get(int x, int y) const
        {
            x = cor_w_->get(x, get_width());
            y = cor_h_->get(y, get_height());
            return img_->get(x, y);
        }

        void set(int x, int y, const T& t)
        {
            x = cor_w_->get(x, get_width());
            y = cor_h_->get(y, get_height());
            return img_->set(x, y, t);
        }

    protected:
        void init()
        {
            if (!this->is_powerof2(img_->get_width()))
            {
                cor_w_.reset(cor_w_->clone_general());
            }
            if (!this->is_powerof2(img_->get_height()))
            {
                cor_h_.reset(cor_h_->clone_general());
            }
        }

    public:
        const corresponder* get_corresponder() const { return cor_w_.get(); }
        const corresponder* get_width_corresponder() const { return cor_w_.get(); }
        const corresponder* get_height_corresponder() const { return cor_h_.get(); }

    protected:
        static bool is_powerof2(int x) { return ((x ^ (x - 1)) == 0); }
        static bool is_powerof2(const image<T>* img) { return is_powerof2(img->get_width()) && is_powerof2(img->get_height()); }

        static bool is_powerof2_w(const image<T>* img) { return is_powerof2(img->get_width()); }
        static bool is_powerof2_h(const image<T>* img) { return is_powerof2(img->get_height()); }

    protected:
        std::shared_ptr<image<T> > img_;
        std::shared_ptr<corresponder> cor_w_;
        std::shared_ptr<corresponder> cor_h_;
    };

    template <class T, class A, class B>
    class static_corresponded_image : public image<T>
    {
    public:
        static_corresponded_image(const auto_count_ptr<image<T> >& img) : img_(img) {}

        int get_width() const { return img_->get_width(); }
        int get_height() const { return img_->get_height(); }
        T get(int x, int y) const
        {
            x = A::static_get(x, get_width());
            y = B::static_get(y, get_height());
            return img_->get(x, y);
        }

        void set(int x, int y, const T& t)
        {
            x = A::static_get(x, get_width());
            y = B::static_get(y, get_height());
            return img_->set(x, y, t);
        }

    protected:
        std::shared_ptr<image<T> > img_;
    };

    template <class T, class A1, class A2, class B1 = A1, class B2 = A2>
    class base_corresponded_image : public corresponded_image<T>
    {
    private:
        static corresponder* get_width_corresponder(const image<T>* img)
        {
            if (corresponded_image<T>::is_powerof2_w(img))
            {
                return new A2();
            }
            else
            {
                return new A1();
            }
        }
        static corresponder* get_height_corresponder(const image<T>* img)
        {
            if (corresponded_image<T>::is_powerof2_h(img))
            {
                return new B2();
            }
            else
            {
                return new B1();
            }
        }

    public:
        base_corresponded_image(const auto_count_ptr<image<T> >& img)
            : corresponded_image<T>(img, get_width_corresponder(img.get()), get_height_corresponder(img.get())) {}
    };

#define DEF_CORRESPONDED_IMAGE(TYPE)                                                                                                                              \
    template <class T>                                                                                                                                            \
    class TYPE##_corresponded_image : public base_corresponded_image<T, TYPE##_corresponder, powerof2_##TYPE##_corresponder>                                      \
    {                                                                                                                                                             \
    public:                                                                                                                                                       \
        TYPE##_corresponded_image(const auto_count_ptr<image<T> >& img) : base_corresponded_image<T, TYPE##_corresponder, powerof2_##TYPE##_corresponder>(img) {} \
    };                                                                                                                                                            \
    template <class T>                                                                                                                                            \
    class TYPE##_image : public base_corresponded_image<T, TYPE##_corresponder, powerof2_##TYPE##_corresponder>                                                   \
    {                                                                                                                                                             \
    public:                                                                                                                                                       \
        TYPE##_image(const auto_count_ptr<image<T> >& img) : base_corresponded_image<T, TYPE##_corresponder, powerof2_##TYPE##_corresponder>(img) {}              \
    };

    DEF_CORRESPONDED_IMAGE(repeat)
    DEF_CORRESPONDED_IMAGE(mirror)
    DEF_CORRESPONDED_IMAGE(clamp)

#undef DEF_CORRESPONDED_IMAGE

#define DEF_CORRESPONDED_IMAGE(A, B)                                                                                                                                                                          \
    template <class T>                                                                                                                                                                                        \
    class A##_##B##_corresponded_image : public base_corresponded_image<T, A##_corresponder, powerof2_##A##_corresponder, B##_corresponder, powerof2_##B##_corresponder>                                      \
    {                                                                                                                                                                                                         \
    public:                                                                                                                                                                                                   \
        A##_##B##_corresponded_image(const auto_count_ptr<image<T> >& img) : base_corresponded_image<T, A##_corresponder, powerof2_##A##_corresponder, B##_corresponder, powerof2_##B##_corresponder>(img) {} \
    };                                                                                                                                                                                                        \
    template <class T>                                                                                                                                                                                        \
    class A##_##B##_image : public base_corresponded_image<T, A##_corresponder, powerof2_##A##_corresponder, B##_corresponder, powerof2_##B##_corresponder>                                                   \
    {                                                                                                                                                                                                         \
    public:                                                                                                                                                                                                   \
        A##_##B##_image(const auto_count_ptr<image<T> >& img) : base_corresponded_image<T, A##_corresponder, powerof2_##A##_corresponder, B##_corresponder, powerof2_##B##_corresponder>(img) {}              \
    };

    DEF_CORRESPONDED_IMAGE(repeat, repeat)
    DEF_CORRESPONDED_IMAGE(repeat, mirror)
    DEF_CORRESPONDED_IMAGE(repeat, clamp)
    DEF_CORRESPONDED_IMAGE(mirror, repeat)
    DEF_CORRESPONDED_IMAGE(mirror, mirror)
    DEF_CORRESPONDED_IMAGE(mirror, clamp)
    DEF_CORRESPONDED_IMAGE(clamp, repeat)
    DEF_CORRESPONDED_IMAGE(clamp, mirror)
    DEF_CORRESPONDED_IMAGE(clamp, clamp)

#undef DEF_CORRESPONDED_IMAGE
}

#endif
