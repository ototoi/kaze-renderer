#ifndef KAZE_COLOR_CONVERT_IMAGE_HPP
#define KAZE_COLOR_CONVERT_IMAGE_HPP

#include "image.hpp"
#include "color_traits.hpp"

#include <vector>

namespace kaze
{
    namespace ns_conv_img
    {
        template <class FROM, class TO>
        struct conv_traits
        {
            static inline TO convert(const FROM& x) { return color_traits<TO>::convert_from(x); }
            static inline FROM convert(const TO& x) { return color_traits<FROM>::convert_from(x); }
        };
        template <class FROM>
        struct conv_traits<FROM, FROM>
        {
            static inline const FROM& convert(const FROM& x) { return x; }
        };
    }

    template <class FROM, class TO, class CVTR = ns_conv_img::conv_traits<FROM, TO> >
    class color_convert_image : public image<TO>
    {
    public:
        color_convert_image(const std::shared_ptr<image<FROM> >& img) : img_(img) {}
    public:
        TO get(int x, int y) const
        {
            return CVTR::convert(img_->get(x, y));
        }

        void set(int x, int y, const TO& t)
        {
            img_->set(x, y, CVTR::convert(t));
        }
        int get_width() const { return img_->get_width(); }
        int get_height() const { return img_->get_height(); }

    protected:
        std::shared_ptr<image<FROM> > img_;
    };
}

#endif
