#ifndef KAZE_MEMORY_IMAGE_HPP
#define KAZE_MEMORY_IMAGE_HPP

#include "types.h"
#include "color.h"
#include "image.hpp"
#include <cassert>

namespace kaze
{

    template <class T>
    struct memory_image_traits
    {
        typedef T value_type;
        static const T& convert(const T& v) { return v; }
    };
    template <class T>
    class memory_image : public image<T>
    {
    public:
        typedef typename memory_image_traits<T>::value_type value_type;

    public:
        memory_image(const memory_image<T>& img) : w_(img.w_), h_(img.h_)
        {
            int w = w_;
            int h = h_;
            vec_ = new value_type[w * h];
            memcpy(vec_, img.vec_, sizeof(T) * w * h);
        }

        memory_image(int w, int h) : w_(w), h_(h)
        {
            vec_ = new value_type[w * h];
            memset(vec_, 0, sizeof(value_type) * w * h);
        }
        ~memory_image()
        {
            delete[] vec_;
        }
        int get_width() const { return w_; }
        int get_height() const { return h_; }

        size_t get_size() const
        {
            return w_ * h_;
        }

        T get(int x, int y) const
        {
            size_t idx = y * w_ + x;
            assert(idx < get_size());
            return memory_image_traits<T>::convert(vec_[idx]);
        }

        void set(int x, int y, const T& t)
        {
            size_t idx = y * w_ + x;
            assert(idx < get_size());
            vec_[idx] = memory_image_traits<T>::convert(t);
        }

    public:
        T& operator[](size_t i) { return vec_[i]; }
        const T& operator[](size_t i) const { return vec_[i]; }

        value_type* get_ptr() { return vec_; }
        const value_type* get_ptr() const { return vec_; }
    protected:
        int w_;
        int h_;
        value_type* vec_;
    };
    /*
	template class memory_image<real>;
	template class memory_image<color1>;
	template class memory_image<color2>;
	template class memory_image<color3>;
	template class memory_image<color4>;
*/
    typedef memory_image<real> real_memory_image;
    typedef memory_image<color1> color1_memory_image;
    typedef memory_image<color2> color2_memory_image;
    typedef memory_image<color3> color3_memory_image;
    typedef memory_image<color3> color4_memory_image;
}

#endif
