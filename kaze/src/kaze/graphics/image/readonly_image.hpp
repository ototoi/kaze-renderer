#ifndef KAZE_READONLY_IMAGE_HPP
#define KAZE_READONLY_IMAGE_HPP

#include "image.hpp"
#include <cassert>

namespace kaze
{

    template <class T>
    class readonly_image : public image<T>
    {
    public:
        void set(int x, int y, const T& t) 
        {
            (void)x;
            (void)y;
            (void)t;
            assert(0); 
        }
    };
}

#endif
