#ifndef KAZE_IMAGE_INTERPOLATOR_HPP
#define KAZE_IMAGE_INTERPOLATOR_HPP

#include "image.hpp"
#include "image_interpolation.hpp"
#include "corresponder.h"
#include "filter.h"

namespace kaze
{
    template <class T>
    class image_interpolator
    {
    public:
        virtual ~image_interpolator() {}
    public:
        virtual T get(const image<T>& img, real x, real y) const = 0;
        virtual T get(const image<T>& img, const corresponder& cor, real x, real y) const = 0;
        virtual T get(const image<T>& img, const corresponder& corx, const corresponder& cory, real x, real y) const = 0;
    };

    template <class T>
    class c0_image_interpolator : public image_interpolator<T>
    {
    public:
        T get(const image<T>& img, real x, real y) const { return static_get(img, x, y); }
        T get(const image<T>& img, const corresponder& cor, real x, real y) const { return static_get(img, cor, x, y); }
        T get(const image<T>& img, const corresponder& corx, const corresponder& cory, real x, real y) const { return static_get(img, corx, cory, x, y); }
        static T static_get(const image<T>& img, real x, real y) { return c0_interpolation(img, x, y); }
        static T static_get(const image<T>& img, const corresponder& cor, real x, real y) { return c0_interpolation(img, cor, x, y); }
        static T static_get(const image<T>& img, const corresponder& corx, const corresponder& cory, real x, real y) { return c0_interpolation(img, corx, cory, x, y); }
    };

    template <class T, class F>
    class c1_image_interpolator : public image_interpolator<T>
    {
    public:
        T get(const image<T>& img, real x, real y) const { return static_get(img, x, y); }
        T get(const image<T>& img, const corresponder& cor, real x, real y) const { return static_get(img, cor, x, y); }
        T get(const image<T>& img, const corresponder& corx, const corresponder& cory, real x, real y) const { return static_get(img, corx, cory, x, y); }
        static T static_get(const image<T>& img, real x, real y) { return c1_interpolation<T, F>(img, x, y); }
        static T static_get(const image<T>& img, const corresponder& cor, real x, real y) { return c1_interpolation<T, F>(img, cor, x, y); }
        static T static_get(const image<T>& img, const corresponder& corx, const corresponder& cory, real x, real y) { return c1_interpolation<T, F>(img, corx, cory, x, y); }
    };

    template <class T, class F>
    class c2_image_interpolator : public image_interpolator<T>
    {
    public:
        T get(const image<T>& img, real x, real y) const { return static_get(img, x, y); }
        T get(const image<T>& img, const corresponder& cor, real x, real y) const { return static_get(img, cor, x, y); }
        T get(const image<T>& img, const corresponder& corx, const corresponder& cory, real x, real y) const { return static_get(img, corx, cory, x, y); }
        static T static_get(const image<T>& img, real x, real y) { return c2_interpolation<T, F>(img, x, y); }
        static T static_get(const image<T>& img, const corresponder& cor, real x, real y) { return c2_interpolation<T, F>(img, cor, x, y); }
        static T static_get(const image<T>& img, const corresponder& corx, const corresponder& cory, real x, real y) { return c2_interpolation<T, F>(img, corx, cory, x, y); }
    };

    template <class T>
    class nearest_image_interpolator : public c0_image_interpolator<T>
    {
    };

    template <class T>
    class bilinear_image_interpolator : public c1_image_interpolator<T, triangle_filter>
    {
    };

    template <class T>
    class hermit_image_interpolator : public c1_image_interpolator<T, hermite_filter>
    {
    };

    template <class T>
    class cubic_image_interpolator : public c2_image_interpolator<T, cubic_filter>
    {
    };

    template <class T>
    class bspline_image_interpolator : public c2_image_interpolator<T, bspline_filter>
    {
    };

    template <class T>
    class mitchell_image_interpolator : public c2_image_interpolator<T, mitchell_filter>
    {
    };

    template <class T>
    class lagrange_image_interpolator : public c2_image_interpolator<T, lagrange_filter>
    {
    };

    template <class T>
    class gaussian_image_interpolator : public c2_image_interpolator<T, gaussian_filter>
    {
    };

    template <class T>
    class sinc_image_interpolator : public c2_image_interpolator<T, sinc_filter>
    {
    };

    template <class T>
    class lanczos_image_interpolator : public c2_image_interpolator<T, lanczos_filter>
    {
    };
}

#endif
