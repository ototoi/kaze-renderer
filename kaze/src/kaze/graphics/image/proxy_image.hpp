#ifndef KAZE_PROXY_IMAGE_HPP
#define KAZE_PROXY_IMAGE_HPP

#include "image.hpp"

namespace kaze
{

    template <class T>
    class proxy_image : public image<T>
    {
    public:
        proxy_image(image<T>* img) : img_(img) {}
        //~proxy_image(){}
        T get(int x, int y) const { return img_->get(x, y); }
        int get_width() const { return img_->get_width(); }
        int get_height() const { return img_->get_height(); }
    private:
        image<T>* img_;
    };
}

#endif