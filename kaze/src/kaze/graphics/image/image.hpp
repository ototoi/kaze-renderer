#ifndef KAZE_IMAGE_HPP
#define KAZE_IMAGE_HPP

#include "color.h"

namespace kaze
{

    template <class T>
    class image
    {
    public:
        typedef T value_type;

    public:
        virtual ~image() {}
        virtual T get(int x, int y) const = 0;
        virtual void set(int x, int y, const T& t) = 0;
        virtual int get_width() const = 0;
        virtual int get_height() const = 0;
    };

    typedef image<color1> color1_image;
    typedef image<color2> color2_image;
    typedef image<color3> color3_image;
    typedef image<color4> color4_image;
}

#endif