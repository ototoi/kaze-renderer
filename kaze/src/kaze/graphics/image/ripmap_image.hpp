#ifndef KAZE_RIPMAP_IAMGE_HPP
#define KAZE_RIPMAP_IAMGE_HPP

#include "image.hpp"
#include "readonly_image.hpp"
#include "memory_image.hpp"
#include "image_interpolator.hpp"
#include <vector>
#include <memory>

#include "count_ptr.hpp"

#if defined(_MSC_VER) && defined(_M_X64)
#include <intrin.h>
#endif

namespace kaze
{

    template <class T>
    class ripmap_image : public readonly_image<T>
    {
    public:
        typedef memory_image<T> IMG;

    public:
        class ref_image : public readonly_image<T>
        { //clamp
        public:
            ref_image(const image<T>* img) : img_(img) {}
            ~ref_image() { /*no delete*/}
            int get_width() const { return img_->get_width(); }
            int get_height() const { return img_->get_height(); }
            T get(int x, int y) const
            {
                int w = img_->get_width();
                int h = img_->get_height();

                if (x < 0) x = 0;
                if (y < 0) y = 0;
                if (x >= w) x = w - 1;
                if (y >= h) y = h - 1;
                return img_->get(x, y);
            }

        private:
            const image<T>* img_;
        };

        class scan_image
        {
        public:
            scan_image(image<T>* img)
            {
                initialize(img); //caution
            }
            scan_image(const std::vector<image<T>*>& images)
                : mv_(images)
            {
                //assert(check_ripmap());
            }
            ~scan_image()
            {
                destroy();
            }

            scan_image(const scan_image* scan)
            { //
                shift(scan);
            }

        public:
            image<T>* last_image() const { return mv_[mv_.size() - 1]; }
            const image<T>* get_image(size_t index) const { return mv_[index]; }
            size_t size() const { return mv_.size(); }

            int get_width() const { return last_image()->get_width(); }
            int get_height() const { return last_image()->get_height(); }
        protected:
            void initialize(image<T>* img)
            {
                try
                {
                    int W = img->get_width();
                    int H = img->get_height();
                    int G = get_powerof2(W);
                    assert(G == H);
                    assert(G == W);
                    std::unique_ptr<image<T> > ap(img);

                    add(ap.get());
                    mv_.push_back(ap.release());
                }
                catch (...)
                {
                    destroy();
                }
            }

            void destroy()
            {
                size_t sz = mv_.size();
                for (size_t i = 0; i < sz; i++)
                {
                    delete mv_[i];
                }
                mv_.clear();
            }

            void add(const image<T>* img)
            {
                int W = img->get_width();
                int H = img->get_height();

                int Sz = W;
                if (Sz <= 1)
                {
                    //Don't doing
                }
                else
                {
                    int CW = W >> 1;
                    //
                    std::unique_ptr<IMG> ap(new IMG(CW, H));
                    T* ptr = ap->get_ptr();

                    for (int y = 0; y < H; y++)
                    { //
                        for (int x = 0; x < CW; x++)
                        { //
                            T col = real(0.5) * (img->get(2 * x, y) + img->get(2 * x + 1, y));
                            *ptr = col;
                            ptr++;
                        }
                    }
                    add(ap.get());
                    mv_.push_back(ap.release());
                }
            }

            void shift(const scan_image* scan)
            {
                try
                {
                    size_t sz = scan->size();
                    for (size_t i = 0; i < sz; i++)
                    {
                        const image<T>* img = scan->get_image(i);
                        int W = img->get_width();
                        int H = img->get_height();
                        int CH = H >> 1;
                        //
                        std::unique_ptr<IMG> ap(new IMG(W, CH));
                        T* ptr = ap->get_ptr();

                        for (int y = 0; y < CH; y++)
                        { //
                            for (int x = 0; x < W; x++)
                            { //
                                T col = real(0.5) * (img->get(x, 2 * y) + img->get(x, 2 * y + 1));
                                *ptr = col;
                                ptr++;
                            }
                        }

                        mv_.push_back(ap.release());
                    }
                }
                catch (...)
                {
                    destroy();
                }
            }

        private:
            std::vector<image<T>*> mv_;
        };

    public:
        ripmap_image(const image<T>& img)
        {
            initialize(&img);
        }
        ripmap_image(const auto_count_ptr<image<T> >& img)
        {
            initialize(img.get());
        }

        ~ripmap_image()
        {
            destroy();
        }

    public:
        int get_width() const { return last_image()->get_width(); }
        int get_height() const { return last_image()->get_height(); }
        T get(int x, int y) const { return last_image()->get(x, y); }
    protected:
        image<T>* last_image() const { return mv_[mv_.size() - 1]->last_image(); }
    public:
        const image<T>* get_image(int x, int y) const { return mv_[y]->get_image(x); }
        size_t size() const { return mv_.size(); }
    protected:
        /**
		 *  @param const image<T>* img : an original image 
		 */
        void initialize(const image<T>* img)
        {
            try
            {
                int W = img->get_width();
                int H = img->get_height();
                int G = std::max<int>(get_powerof2(W), get_powerof2(H));
                std::unique_ptr<IMG> ap(new IMG(G, G));
                T* ptr = ap->get_ptr();

                if (W == G && H == G)
                { //no interpolation
                    for (int y = 0; y < G; y++)
                    {
                        for (int x = 0; x < G; x++)
                        {
                            *ptr = img->get(x, y);
                            ptr++;
                        }
                    }
                }
                else
                {
                    ref_image rimg(img);

                    real c = real(1.0) / G;
                    for (int y = 0; y < G; y++)
                    {
                        for (int x = 0; x < G; x++)
                        {
                            *ptr = bilinear_image_interpolator<T>::static_get(rimg, c * (x + 0.5), c * (y + 0.5)); //
                            ptr++;
                        }
                    }
                }
                //-----------------------------------------------------

                std::unique_ptr<scan_image> sap(new scan_image(ap.release()));
                add(sap.get());
                mv_.push_back(sap.release());
            }
            catch (...)
            {
                destroy();
            }
        }

        void add(const scan_image* img)
        {
            if (img->get_height() > 1)
            {
                std::unique_ptr<scan_image> sap(new scan_image(img));
                add(sap.get());
                mv_.push_back(sap.release());
            }
        }

        void destroy()
        {
            size_t sz = mv_.size();
            for (size_t i = 0; i < sz; i++)
            {
                delete mv_[i];
            }
            mv_.clear();
        }

    public:
        static int get_powerof2(int x)
        {
            x--;
            x |= x >> 1;
            x |= x >> 2;
            x |= x >> 4;
            x |= x >> 8;
            x |= x >> 16;
            x++;
            return x;
        }

    private:
        std::vector<scan_image*> mv_;
    };
}

#endif
