#ifndef KAZE_B3_IMAGE_H
#define KAZE_B3_IMAGE_H

#include "image.hpp"
#include <vector>

namespace kaze
{
    class b3_image : public image<color3>
    {
    public:
        typedef uint32_t raw_value_type;
    public:
        b3_image(int w, int h);
        color3 get(int x, int y) const;
        void set(int x, int y, const color3& t);
        int get_width() const;
        int get_height() const;

    public:
        uint32_t* get_ptr();
        const uint32_t* get_ptr() const;

    private:
        int w_;
        int h_;
        std::vector<raw_value_type> vec_;
    };
}

#endif