#ifndef KAZE_DXT_IMAGE_H
#define KAZE_DXT_IMAGE_H

#include <vector>
#include <string.h>

#include "image.hpp"
#include "color_convert.h"
#include "fp16.h"

#ifdef _DEBUG
#include "logger.h"
#endif

namespace kaze
{

    class DXT1_image : public image<color4>
    {
    public:
#pragma pack(push, 1)
        struct block
        {
            uint16_t c0;
            uint16_t c1;
            uint32_t bits; //for bit mask 32/8
        };
#pragma pack(pop)
        DXT1_image(int width, int height, const std::vector<unsigned char>& buffer)
            : width_(width), height_(height), buffer_(0)
        {
            int bw = width >> 2;
            int bh = height >> 2;
#ifdef _DEBUG
            assert(sizeof(block) == 8);
#endif
            size_t sz = bw * bh * sizeof(block); //(64)bit / 8 bit
            buffer_ = new block[bw * bh];
            memcpy(buffer_, &buffer[0], sz);
            bw_ = bw;
        }
        ~DXT1_image()
        {
            if (buffer_) delete[] buffer_;
        }

    public:
        color4 get(int x, int y) const
        {
            int bx = x >> 2;
            int by = y >> 2;
            int ix = x & 0x3;
            int iy = y & 0x3;
            const block& blk = buffer_[by * bw_ + bx];

            int ii = 4 * iy + ix;
            int p = (blk.bits >> (2 * ii)) & 0x3;

            color4 c[4];
            if (blk.c0 > blk.c1)
            {
                c[0] = from565(blk.c0);
                c[1] = from565(blk.c1);
                c[2] = c[0] * 2.0 / 3.0 + c[1] * 1.0 / 3.0;
                c[3] = c[0] * 1.0 / 3.0 + c[1] * 2.0 / 3.0;
            }
            else
            {
                c[0] = from565(blk.c0);
                c[1] = from565(blk.c1);
                c[2] = c[0] * 1.0 / 2.0 + c[1] * 1.0 / 2.0;
                c[3] = color4(0, 0, 0, 0); //color4(c[2][0], c[2][1], c[2][2], 0);
            }
            return c[p];
        }
        void set(int x, int y, const color4& t) {}
        int get_width() const { return width_; }
        int get_height() const { return height_; }
    protected:
        static inline color4 from565(uint16_t packed)
        {
            int R0 = (packed >> 11) & 0x1F;
            int G0 = (packed >> 5) & 0x3F;
            int B0 = (packed)&0x1F;
            return color4(R0 / 31.0, G0 / 63.0, B0 / 31.0, 1.0);
        }

    protected:
        int width_;
        int height_;
        int bw_;
        block* buffer_;
    };

    class DXT3_image : public image<color4>
    {
    public:
#pragma pack(push, 1)
        struct block
        {
            uint64_t alpha; //for bit mask 32/8
            uint16_t c0;
            uint16_t c1;
            uint32_t bits; //for bit mask 32/8
        };
#pragma pack(pop)
        DXT3_image(int width, int height, const std::vector<unsigned char>& buffer)
            : width_(width), height_(height), buffer_(0)
        {
            int bw = width >> 2;
            int bh = height >> 2;
#ifdef _DEBUG
            assert(sizeof(block) == 16);
#endif
            size_t sz = bw * bh * sizeof(block); //(64)bit / 8 bit
            buffer_ = new block[bw * bh];
            memcpy(buffer_, &buffer[0], sz);
            bw_ = bw;
        }
        ~DXT3_image()
        {
            if (buffer_) delete[] buffer_;
        }

    public:
        color4 get(int x, int y) const
        {
            int bx = x >> 2;
            int by = y >> 2;
            int ix = x & 0x3;
            int iy = y & 0x3;
            const block& blk = buffer_[by * bw_ + bx];

            int ii = 4 * iy + ix;
            int p = (blk.bits >> (2 * ii)) & 0x3;
            int a = (blk.alpha >> (4 * ii)) & 0xF; //TODO

            color3f c[4];
            c[0] = from565(blk.c0);
            c[1] = from565(blk.c1);
            c[2] = c[0] * 2.0f / 3.0f + c[1] * 1.0f / 3.0f;
            c[3] = c[0] * 1.0f / 3.0f + c[1] * 2.0f / 3.0f;

            return color4(c[p][0], c[p][1], c[p][2], a / 15.0);
        }
        void set(int x, int y, const color4& t) {}
        int get_width() const { return width_; }
        int get_height() const { return height_; }
    protected:
        static inline color3f from565(uint16_t packed)
        {
            int R0 = (packed >> 11) & 0x1F;
            int G0 = (packed >> 5) & 0x3F;
            int B0 = (packed)&0x1F;
            return color3f(R0 / 31.0f, G0 / 63.0f, B0 / 31.0f);
        }

    protected:
        int width_;
        int height_;
        int bw_;
        block* buffer_;
    };

    class DXT5_image : public image<color4>
    {
    public:
#pragma pack(push, 1)
        struct block
        {
            uint8_t a0;
            uint8_t a1;
            uint8_t abits[6]; //48 bit
            uint16_t c0;
            uint16_t c1;
            uint32_t bits; //for bit mask 32/8
        };
#pragma pack(pop)
        DXT5_image(int width, int height, const std::vector<unsigned char>& buffer)
            : width_(width), height_(height), buffer_(0)
        {
            int bw = width >> 2;
            int bh = height >> 2;
#ifdef _DEBUG
            assert(sizeof(block) == 16);
#endif
            size_t sz = bw * bh * sizeof(block); //(64)bit / 8 bit
            buffer_ = new block[bw * bh];
            memcpy(buffer_, &buffer[0], sz);
            bw_ = bw;
        }
        ~DXT5_image()
        {
            if (buffer_) delete[] buffer_;
        }

    public:
        color4 get(int x, int y) const
        {
            using namespace std;
            int bx = x >> 2;
            int by = y >> 2;
            int ix = x & 0x3;
            int iy = y & 0x3;
            const block& blk = buffer_[by * bw_ + bx];

            int ii = 4 * iy + ix;
            int p = (blk.bits >> (2 * ii)) & 0x3;
            //unsigned char buf[8] = {};
            //memcpy(buf, blk.abits, 8);
            uint64_t abits = *((uint64_t*)((void*)blk.abits));
            int ap = (abits >> (3 * ii)) & 0x7;

            color3f c[4];
            c[0] = from565(blk.c0);
            c[1] = from565(blk.c1);
            c[2] = c[0] * 2.0f / 3.0f + c[1] * 1.0f / 3.0f;
            c[3] = c[0] * 1.0f / 3.0f + c[1] * 2.0f / 3.0f;

            float a[8];
            if (blk.a0 > blk.a1)
            {
                a[0] = blk.a0 / 255.0f;
                a[1] = blk.a1 / 255.0f;
                a[2] = a[0] * 6.0f / 7.0f + a[1] * 1.0f / 7.0f;
                a[3] = a[0] * 5.0f / 7.0f + a[1] * 2.0f / 7.0f;
                a[4] = a[0] * 4.0f / 7.0f + a[1] * 3.0f / 7.0f;
                a[5] = a[0] * 3.0f / 7.0f + a[1] * 4.0f / 7.0f;
                a[6] = a[0] * 2.0f / 7.0f + a[1] * 5.0f / 7.0f;
                a[7] = a[0] * 1.0f / 7.0f + a[1] * 6.0f / 7.0f;
            }
            else
            {
                a[0] = blk.a0 / 255.0f;
                a[1] = blk.a1 / 255.0f;
                a[2] = a[0] * 4.0f / 5.0f + a[1] * 1.0f / 5.0f;
                a[3] = a[0] * 3.0f / 5.0f + a[1] * 2.0f / 5.0f;
                a[4] = a[0] * 2.0f / 5.0f + a[1] * 3.0f / 5.0f;
                a[5] = a[0] * 1.0f / 5.0f + a[1] * 4.0f / 5.0f;
                a[6] = 0.0f;
                a[7] = 1.0f;
            }

            return color4(c[p][0], c[p][1], c[p][2], a[ap]);
        }
        void set(int x, int y, const color4& t) {}
        int get_width() const { return width_; }
        int get_height() const { return height_; }
    protected:
        static inline color3f from565(uint16_t packed)
        {
            int R0 = (packed >> 11) & 0x1F;
            int G0 = (packed >> 5) & 0x3F;
            int B0 = (packed)&0x1F;
            return color3f(R0 / 31.0f, G0 / 63.0f, B0 / 31.0f);
        }

    protected:
        int width_;
        int height_;
        int bw_;
        block* buffer_;
    };

    typedef DXT1_image dxt1_image;
    typedef DXT3_image dxt3_image;
    typedef DXT5_image dxt5_image;

    typedef DXT1_image BC1_image;
    typedef DXT3_image BC2_image;
    typedef DXT5_image BC3_image;

    struct packA8R8G8B8
    {
        static color4 unpack(uint32_t packed)
        {
            int A = (packed >> 24) & 0xFF;
            int R = (packed >> 16) & 0xFF;
            int G = (packed >> 8) & 0xFF;
            int B = (packed)&0xFF;
            return color4(R / 255.0, G / 255.0, B / 255.0, A / 255.0);
        }
    };

    struct packX8R8G8B8
    {
        static color4 unpack(uint32_t packed)
        {
            int A = 255;
            int R = (packed >> 16) & 0xFF;
            int G = (packed >> 8) & 0xFF;
            int B = (packed)&0xFF;
            return color4(R / 255.0, G / 255.0, B / 255.0, A / 255.0);
        }
    };

    struct packA8B8G8R8
    {
        static color4 unpack(uint32_t packed)
        {
            int A = (packed >> 24) & 0xFF;
            int B = (packed >> 16) & 0xFF;
            int G = (packed >> 8) & 0xFF;
            int R = (packed)&0xFF;
            return color4(R / 255.0, G / 255.0, B / 255.0, A / 255.0);
        }
    };

    struct packX8B8G8R8
    {
        static color4 unpack(uint32_t packed)
        {
            int A = 255;
            int B = (packed >> 16) & 0xFF;
            int G = (packed >> 8) & 0xFF;
            int R = (packed)&0xFF;
            return color4(R / 255.0, G / 255.0, B / 255.0, A / 255.0);
        }
    };

    struct packA2R10G10B10
    {
        static color4 unpack(uint32_t packed)
        {
            int A = (packed >> 30) & 0x3;
            int R = (packed >> 20) & 0x3FF;
            int G = (packed >> 10) & 0x3FF;
            int B = (packed)&0x3FF;
            return color4(R / 1023.0, G / 1023.0, B / 1023.0, A / 4.0);
        }
    };

    struct packA2B10G10R10
    {
        static color4 unpack(uint32_t packed)
        {
            int A = (packed >> 30) & 0x3;
            int B = (packed >> 20) & 0x3FF;
            int G = (packed >> 10) & 0x3FF;
            int R = (packed)&0x3FF;
            return color4(R / 1023.0, G / 1023.0, B / 1023.0, A / 4.0);
        }
    };

    struct packR10G10B10A2
    {
        static color4 unpack(uint32_t packed)
        {
            /*
			int R = (packed >> 22) & 0x3FF;
			int G = (packed >> 12) & 0x3FF;
			int B = (packed >> 2) & 0x3FF;
			int A = (packed) & 0x3;
			return color4(R / 1023.0, G / 1023.0, B / 1023.0, A / 4.0);
			*/
            int A = (packed >> 30) & 0x3;
            int B = (packed >> 20) & 0x3FF;
            int G = (packed >> 10) & 0x3FF;
            int R = (packed)&0x3FF;
            return color4(R / 1023.0, G / 1023.0, B / 1023.0, A / 4.0);
        }
    };

    struct packR8G8B8A8_UNORM
    {
        static color4 unpack(uint32_t packed)
        {
            return packA8B8G8R8::unpack(packed);
            //color4 c = packA8B8G8R8::unpack(packed);
            //return c;
            //double g[3] = {  };
            //double l[3] = { c[0], c[1], c[2] };
            //linear_to_gamma_sRGB(l, g);
            //return color4(g[0], g[1], g[2], c[3]);
        }
    };

    template <class T>
    class pack32_image : public image<color4>
    {
    public:
        pack32_image(int width, int height, const std::vector<unsigned char>& buffer)
            : width_(width), height_(height), buffer_(0)
        {
            int w = width;
            int h = height;
            buffer_ = new uint32_t[w * h];
            memcpy(buffer_, &buffer[0], sizeof(uint32_t) * w * h);
        }
        ~pack32_image()
        {
            if (buffer_) delete[] buffer_;
        }

    public:
        color4 get(int x, int y) const
        {
            uint32_t packed = buffer_[y * width_ + x];
            return T::unpack(packed);
        }
        void set(int x, int y, const color4& t) {}
        int get_width() const { return width_; }
        int get_height() const { return height_; }
    protected:
        int width_;
        int height_;
        uint32_t* buffer_;
    };

    typedef pack32_image<packA8R8G8B8> A8R8G8B8_image;
    typedef pack32_image<packX8R8G8B8> X8R8G8B8_image;
    typedef pack32_image<packA8B8G8R8> A8B8G8R8_image;
    typedef pack32_image<packX8B8G8R8> X8B8G8R8_image;
    typedef pack32_image<packA2R10G10B10> A2R10G10B10_image;
    typedef pack32_image<packA2B10G10R10> A2B10G10R10_image;
    typedef pack32_image<packR10G10B10A2> R10G10B10A2_image;

    typedef pack32_image<packR8G8B8A8_UNORM> R8G8B8A8_UNORM_image;
    typedef pack32_image<packA8B8G8R8> R8G8B8A8_UNORM_SRGB_image;

    template <class T>
    class pack24_image : public image<color4>
    {
    public:
        pack24_image(int width, int height, const std::vector<unsigned char>& buffer)
            : width_(width), height_(height), buffer_(0)
        {
            int w = width;
            int h = height;
            buffer_ = new uint8_t[w * h * 3];
            memcpy(buffer_, &buffer[0], w * h * 3);
        }
        ~pack24_image()
        {
            if (buffer_) delete[] buffer_;
        }

    public:
        color4 get(int x, int y) const
        {
            uint32_t packed = 0xFF000000 | (*((uint32_t*)&buffer_[3 * (y * width_ + x)]));
            return T::unpack(packed);
        }
        void set(int x, int y, const color4& t) {}
        int get_width() const { return width_; }
        int get_height() const { return height_; }
    protected:
        int width_;
        int height_;
        uint8_t* buffer_;
    };

    typedef pack24_image<packX8R8G8B8> R8G8B8_image;
    typedef pack24_image<packX8B8G8R8> B8G8R8_image;

    struct packA16B16G16R16F
    {
        static color4 unpack(uint64_t packed)
        {
            unsigned int A = (packed >> 48) & 0xFFFF;
            unsigned int R = (packed >> 32) & 0xFFFF;
            unsigned int G = (packed >> 16) & 0xFFFF;
            unsigned int B = (packed)&0xFFFF;
            return color4(f16to32(R), f16to32(G), f16to32(B), f16to32(A));
        }
    };

    template <class T>
    class pack64_image : public image<color4>
    {
    public:
        pack64_image(int width, int height, const std::vector<unsigned char>& buffer)
            : width_(width), height_(height), buffer_(0)
        {
            int w = width;
            int h = height;
            buffer_ = new uint64_t[w * h];
            memcpy(buffer_, &buffer[0], w * h * sizeof(uint64_t));
        }
        ~pack64_image()
        {
            if (buffer_) delete[] buffer_;
        }

    public:
        color4 get(int x, int y) const
        {
            uint64_t packed = buffer_[y * width_ + x];
            return T::unpack(packed);
        }
        void set(int x, int y, const color4& t) {}
        int get_width() const { return width_; }
        int get_height() const { return height_; }
    protected:
        int width_;
        int height_;
        uint64_t* buffer_;
    };

    typedef pack64_image<packA16B16G16R16F> A16B16G16R16F_image;

    class BC6H_image : public image<color4>
    {
    public:
#pragma pack(push, 1)
        struct block
        {
            unsigned char bits[16];
        };
#pragma pack(pop)
    public:
        BC6H_image(int width, int height, const std::vector<unsigned char>& buffer, bool bSigned = false);
        ~BC6H_image();

    public:
        color4 get(int x, int y) const;
        void set(int x, int y, const color4& t) {}
        int get_width() const { return width_; }
        int get_height() const { return height_; }
    protected:
        int width_;
        int height_;
        block* buffer_;
        int bw_;
        bool bSigned_;
    };
}

#endif