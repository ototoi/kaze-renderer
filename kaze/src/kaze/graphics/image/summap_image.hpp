#ifndef KAZE_SUMMAP_IAMGE_HPP
#define KAZE_SUMMAP_IAMGE_HPP

#include "memory_image.hpp"
#include "color_traits.hpp"

#include <memory>

#include "count_ptr.hpp"

namespace kaze
{

    template <class T>
    class summap_image : public memory_image<T>
    {
    public:
        summap_image(const image<T>& img) : memory_image<T>(img.get_width(), img.get_height())
        {
            int w = this->w_;
            int h = this->h_;
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    this->vec_[w * y + x] = img.get(x, y);
                }
            }
            initialize();
        }
        summap_image(const memory_image<T>& img) : memory_image<T>(img)
        {
            initialize();
        }
        summap_image(const summap_image<T>& img) : memory_image<T>(img) {}

        summap_image(const auto_count_ptr<image<T> >& img)
            : memory_image<T>(img->get_width(), img->get_height())
        {
            int w = this->w_;
            int h = this->h_;
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    this->vec_[w * y + x] = img->get(x, y);
                }
            }
            initialize();
        }

        void initialize()
        {
            int w = this->w_;
            int h = this->h_;
            for (int y = 0; y < h; y++)
            {
                for (int x = 1; x < w; x++)
                {
                    this->vec_[w * y + x] += this->vec_[w * y + x - 1];
                }
            }
            for (int x = 0; x < w; x++)
            {
                for (int y = 1; y < h; y++)
                {
                    this->vec_[w * y + x] += this->vec_[w * (y - 1) + x];
                }
            }
        }

        T get_avg(int x0, int y0, int x1, int y1) const
        {
            return (get_raw(x1 - 1, y1 - 1) - get_raw(x0 - 1, y1 - 1) - get_raw(x1 - 1, y0 - 1) + get_raw(x0 - 1, y0 - 1)) * (real(1) / ((x1 - x0) * (y1 - y0)));
        }

        T get_raw(int x, int y) const
        {
            if (x < 0 || y < 0) return color_traits<T>::zero();
            return this->vec_[this->w_ * y + x];
        }

        T get(int x, int y) const
        {
            return get_avg(x, y, x + 1, y + 1);
        }
    };
}

#endif
