#include "b3_image.h"

namespace kaze
{

    typedef color3::value_type value_type;
    typedef b3_image::raw_value_type raw_value_type;
    typedef raw_value_type byte4;

    static inline value_type b2r(int x)
    {
        return (x * (1.0 / 255.0));
    }

    static inline color3 b2r(int a, int b, int c)
    {
        return color3(b2r(a), b2r(b), b2r(c));
    }

    static inline int r2b(value_type x)
    {
        return int(x * 255);
    }

    static inline byte4 r2b(value_type a, value_type b, value_type c)
    {
        return 0 | (r2b(a) << 16) | (r2b(b) << 8) | (r2b(c));
    }

    //------------------------------------------------------------------

    b3_image::b3_image(int w, int h)
        : w_(w), h_(h)
    {
        vec_.resize(w * h);
    }

    color3 b3_image::get(int x, int y) const
    {
        byte4 val = vec_[w_ * y + x];
        return b2r((val >> 16) & 0xFF, (val >> 8) & 0xFF, val & 0xFF);
    }

    void b3_image::set(int x, int y, const color3& t)
    {
        vec_[w_ * y + x] = r2b(t[0], t[1], t[2]);
    }

    int b3_image::get_width() const
    {
        return w_;
    }

    int b3_image::get_height() const
    {
        return h_;
    }

    //------------------------------------------------------------------

    uint32_t* b3_image::get_ptr()
    {
        return &vec_[0];
    }
    const uint32_t* b3_image::get_ptr() const
    {
        return &vec_[0];
    }
}
