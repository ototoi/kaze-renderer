#ifndef KAZE_IMAGE_INTERPOLATION_HPP
#define KAZE_IMAGE_INTERPOLATION_HPP

#include "types.h"
#include "image.hpp"
#include "corresponder.h"
#include <cmath>

namespace kaze
{

    template <class T>
    T c0_interpolation(const image<T>& tex, real x, real y)
    {
        int w = tex.get_width();
        int h = tex.get_height();

        real rx = x * w;
        real ry = y * h;

        real ix = floor(rx);
        real iy = floor(ry);

        int xx = (int)ix;
        int yy = (int)iy;

        return tex.get(xx, yy);
    }

    template <class T>
    T c0_interpolation(const image<T>& tex, const corresponder& corx, const corresponder& cory, real x, real y)
    {
        int w = tex.get_width();
        int h = tex.get_height();

        real rx = x * w;
        real ry = y * h;

        real ix = floor(rx);
        real iy = floor(ry);

        int xx = (int)ix;
        int yy = (int)iy;

        xx = corx.get(xx, w);
        yy = cory.get(yy, h);

        return tex.get(xx, yy);
    }

    template <class T>
    T c0_interpolation(const image<T>& tex, const corresponder& cor, real x, real y)
    {
        return c0_interpolation(tex, cor, cor, x, y);
    }

    template <class T, class F>
    T c1_interpolation(const image<T>& tex, real x, real y)
    {
        int w = tex.get_width();
        int h = tex.get_height();

        real rx = x * w;
        real ry = y * h;

        rx -= real(0.5); //ex. 0.2-> -1 ... 0
        ry -= real(0.5);

        real ix = floor(rx);
        real iy = floor(ry);

        real u = rx - ix;
        real v = ry - iy;

        real iu = 1 - u;
        real iv = 1 - v;

        int xx = (int)ix;
        int yy = (int)iy;
        int xp = xx + 1;
        int yp = yy + 1;

        u = F::static_get(u);
        v = F::static_get(v);

        iu = F::static_get(iu);
        iv = F::static_get(iv);

        real a = u * v;
        real b = iu * v;
        real c = u * iv;
        real d = iu * iv;

        return a * tex.get(xx, yy) +
               b * tex.get(xp, yy) +
               c * tex.get(xx, yp) +
               d * tex.get(xp, yp) ;
    }

    template <class T, class F>
    T c1_interpolation(const image<T>& tex, const corresponder& corx, const corresponder& cory, real x, real y)
    {
        int w = tex.get_width();
        int h = tex.get_height();

        real rx = x * w;
        real ry = y * h;

        rx -= real(0.5); //ex. 0.2-> -1 ... 0
        ry -= real(0.5);

        real ix = floor(rx);
        real iy = floor(ry);

        real u = rx - ix;
        real v = ry - iy;

        real iu = 1 - u;
        real iv = 1 - v;

        int xx = (int)ix;
        int yy = (int)iy;
        int xp = xx + 1;
        int yp = yy + 1;

        xx = corx.get(xx, w);
        yy = cory.get(yy, h);
        xp = corx.get(xp, w);
        yp = cory.get(yp, h);

        u = F::static_get(u);
        v = F::static_get(v);

        iu = F::static_get(iu);
        iv = F::static_get(iv);

        real a = u * v;
        real b = iu * v;
        real c = u * iv;
        real d = iu * iv;

        return a * tex.get(xx, yy) +
               b * tex.get(xp, yy) +
               c * tex.get(xx, yp) +
               d * tex.get(xp, yp) ;
    }

    template <class T, class F>
    T c1_interpolation(const image<T>& tex, const corresponder& cor, real x, real y)
    {
        return c1_interpolation<T, F>(tex, cor, cor, x, y);
    }

    /*
		[a , b, c, d]
		[0,1,2,3]
		1.2
		[0,1,2,3]
	*/
    template <class T>
    inline T sum_image_pixel(const T in[4][4])
    {
#if 1
        return in[0][0] + in[0][1] + in[0][2] + in[0][3] +
               in[1][0] + in[1][1] + in[1][2] + in[1][3] +
               in[2][0] + in[2][1] + in[2][2] + in[2][3] +
               in[3][0] + in[3][1] + in[3][2] + in[3][3];
#else
        T tmp = in[0][0];

        tmp += in[0][1];
        tmp += in[0][2];
        tmp += in[0][3];

        for (int x = 1; x < 4; x++)
        {
            for (int y = 0; y < 4; y++)
            {
                tmp += in[x][y];
            }
        }

        return tmp;
#endif
    }

    template <class T, class F>
    T c2_interpolation(const image<T>& tex, real x, real y)
    {
        int w = tex.get_width();
        int h = tex.get_height();

        real rx = x * w;
        real ry = y * h;

        rx -= real(0.5);
        ry -= real(0.5);

        real ix = floor(rx);
        real iy = floor(ry);

        //real u = rx-ix;
        //real v = ry-iy;

        int xx = (int)ix; //-1
        int yy = (int)iy;

        int xd[4];
        int yd[4];
        for (int i = 0; i < 4; i++)
        {
            xd[i] = xx - 1 + i; //cor.get(xx-1+i,w);
            yd[i] = yy - 1 + i; //cor.get(yy-1+i,h);
        }

        real weight_x[4];
        real weight_y[4];

        for (int i = 0; i < 4; i++)
        {
            int p = i - 1;
            real sx = xx + p;
            real sy = yy + p;
            real wx = fabs(rx - sx); //
            real wy = fabs(ry - sy); //abs(rx - kx)
            wx = F::static_get(wx);
            wy = F::static_get(wy);

            weight_x[i] = wx;
            weight_y[i] = wy;
        }

        T tmp[4][4];

        for (int j = 0; j < 4; j++)
        { //y
            for (int i = 0; i < 4; i++)
            {                                                                    ////x
                tmp[j][i] = (weight_x[i] * weight_y[j]) * tex.get(xd[i], yd[j]); //-1 ,  0,  1,  2
            }
        }

        return sum_image_pixel<T>(tmp);
    }

    template <class T, class F>
    T c2_interpolation(const image<T>& tex, const corresponder& corx, const corresponder& cory, real x, real y)
    {
        int w = tex.get_width();
        int h = tex.get_height();

        real rx = x * w;
        real ry = y * h;

        //real rx = x;
        //real ry = y;

        rx -= real(0.5);
        ry -= real(0.5);

        real ix = floor(rx);
        real iy = floor(ry);

        //real u = rx-ix;
        //real v = ry-iy;

        int xx = (int)ix; //-1
        int yy = (int)iy;

        int xd[4];
        int yd[4];
        for (int i = 0; i < 4; i++)
        {
            xd[i] = corx.get(xx - 1 + i, w);
            yd[i] = cory.get(yy - 1 + i, h);
        }

        real weight_x[4];
        real weight_y[4];

        for (int i = 0; i < 4; i++)
        {
            int p = i - 1;
            real sx = xx + p;
            real sy = yy + p;
            real wx = fabs(rx - sx); //
            real wy = fabs(ry - sy); //abs(rx - kx)
            wx = F::static_get(wx);
            wy = F::static_get(wy);

            weight_x[i] = wx;
            weight_y[i] = wy;
        }

        T tmp[4][4];

        for (int j = 0; j < 4; j++)
        { //y
            for (int i = 0; i < 4; i++)
            {                                                                    //x
                tmp[j][i] = (weight_x[i] * weight_y[j]) * tex.get(xd[i], yd[j]); //-1 ,  0,  1,  2
            }
        }

        return sum_image_pixel<T>(tmp);
    }

    template <class T, class F>
    T c2_interpolation(const image<T>& tex, const corresponder& cor, real x, real y)
    {
        return c2_interpolation<T, F>(tex, cor, cor, x, y);
    }
}

#endif
