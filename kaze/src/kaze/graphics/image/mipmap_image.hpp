#ifndef KAZE_MIPMAP_IMAGE_H
#define KAZE_MIPMAP_IMAGE_H

/**
 * @file   mipmap_image.hpp
 * @auther Toru Matsuoka
 * @date   2008/4/2
 */
#include "image.hpp"
#include "memory_image.hpp"
#include "readonly_image.hpp"
#include "image_interpolator.hpp"
#include <vector>
#include <memory>
#include <cassert>

#include "count_ptr.hpp"

#if defined(_MSC_VER) && defined(_M_X64)
#include <intrin.h>
#endif

namespace kaze
{

    /**
	* @class mipmap_image
	* @brief This class is mipmap as an image.
	*/

    template <class T>
    class mipmap_image : public readonly_image<T>
    {
    public:
        typedef memory_image<T> IMG;

    public:
        class ref_image : public readonly_image<T>
        { //clamp
        public:
            ref_image(const image<T>* img) : img_(img) {}
            ~ref_image() { /*no delete*/}
            int get_width() const { return img_->get_width(); }
            int get_height() const { return img_->get_height(); }
            T get(int x, int y) const
            {
                int w = img_->get_width();
                int h = img_->get_height();

                if (x < 0) x = 0;
                if (y < 0) y = 0;
                if (x >= w) x = w - 1;
                if (y >= h) y = h - 1;
                return img_->get(x, y);
            }

        private:
            const image<T>* img_;
        };

    public:
        mipmap_image(const image<T>& img)
        {
            initialize(&img);
        }
        mipmap_image(const auto_count_ptr<image<T> >& img)
        {
            initialize(img.get());
        }
        mipmap_image(const std::vector<std::shared_ptr<image<T> > >& images)
            : mv_(images)
        {
            assert(check_mipmap());
        }

    public:
        int get_width() const { return last_image()->get_width(); }
        int get_height() const { return last_image()->get_height(); }
        T get(int x, int y) const { return last_image()->get(x, y); }
    public:
        const image<T>* get_image(size_t index) const { return mv_[index].get(); }
        size_t size() const { return mv_.size(); }
    protected:
        bool check_mipmap()
        {
            size_t sz = mv_.size();
            for (size_t i = 0; i < sz - 1; i++)
            {
                int w = mv_[i]->get_width();
                int h = mv_[i]->get_height();
                if (get_powerof2(w) != w) return false;
                if (get_powerof2(h) != h) return false;

                int nw = mv_[i + 1]->get_width();
                int nh = mv_[i + 1]->get_height();

                if (w != 1 && 2 * w != nw) return false;
                if (h != 1 && 2 * h != nh) return false;
            }
            return true;
        }
        /**
		 *  @param const image<T>* img : an original image
		 */
        void initialize(const image<T>* img)
        {
            int W = img->get_width();
            int H = img->get_height();
            int WG = get_powerof2(W);
            int HG = get_powerof2(H);
            std::unique_ptr<IMG> ap(new IMG(WG, HG));
            T* ptr = ap->get_ptr();

            if (W == WG && H == HG)
            { //no interpolation
                for (int y = 0; y < HG; y++)
                {
                    for (int x = 0; x < WG; x++)
                    {
                        *ptr = img->get(x, y);
                        ptr++;
                    }
                }
            }
            else
            {
                ref_image rimg(img);

                real cw = real(1.0) / WG;
                real ch = real(1.0) / HG;
                for (int y = 0; y < HG; y++)
                {
                    for (int x = 0; x < WG; x++)
                    {
                        *ptr = bilinear_image_interpolator<T>::static_get(rimg, cw * (x + 0.5), ch * (y + 0.5)); //
                        ptr++;
                    }
                }
            }
            add(ap.get());
            mv_.push_back(std::shared_ptr<image<T> >(ap.release()));
        }

        void add(const image<T>* img)
        {
            int W = img->get_width();
            int H = img->get_height();

            int nCase = 0;
            if (W == 1) nCase |= 1;
            if (H == 1) nCase |= 2;

            switch (nCase)
            {
            case 0:
            {
                int CW = std::max<int>(1, W >> 1);
                int CH = std::max<int>(1, H >> 1);
                std::unique_ptr<IMG> ap(new IMG(CW, CH));
                T* ptr = ap->get_ptr();

                for (int y = 0; y < CH; y++)
                { //
                    for (int x = 0; x < CW; x++)
                    { //
                        T col = real(1.0 / 4.0) *
                                (img->get(2 * x, 2 * y) + img->get(2 * x + 1, 2 * y) + img->get(2 * x, 2 * y + 1) + img->get(2 * x + 1, 2 * y + 1));
                        *ptr = col;
                        ptr++;
                    }
                }
                add(ap.get());
                mv_.push_back(std::shared_ptr<image<T> >(ap.release()));
            }
            break;
            case 1:
            {
                int CW = std::max<int>(1, W >> 1); //1
                int CH = std::max<int>(1, H >> 1);
                std::unique_ptr<IMG> ap(new IMG(CW, CH));
                T* ptr = ap->get_ptr();

                for (int y = 0; y < CH; y++)
                {
                    T col = real(1.0 / 2.0) *
                            (img->get(0, 2 * y + 0) + img->get(0, 2 * y + 1));
                    *ptr = col;
                    ptr++;
                }
                add(ap.get());
                mv_.push_back(std::shared_ptr<image<T> >(ap.release()));
            }
            break;
            case 2:
            {
                int CW = std::max<int>(1, W >> 1);
                int CH = std::max<int>(1, H >> 1); //1
                std::unique_ptr<IMG> ap(new IMG(CW, CH));
                T* ptr = ap->get_ptr();

                for (int x = 0; x < CW; x++)
                {
                    T col = real(1.0 / 2.0) *
                            (img->get(2 * x + 0, 0) + img->get(2 * x + 1, 0));
                    *ptr = col;
                    ptr++;
                }

                add(ap.get());
                mv_.push_back(std::shared_ptr<image<T> >(ap.release()));
            }
            break;
            default:
            {
            }
            break;
            }
        }

        image<T>* last_image() const { return mv_[mv_.size() - 1].get(); }
    protected:
        static int get_powerof2(int x)
        {
            x--;
            x |= x >> 1;
            x |= x >> 2;
            x |= x >> 4;
            x |= x >> 8;
            x |= x >> 16;
            x++;
            return x;
        }

    private:
        std::vector<std::shared_ptr<image<T> > > mv_;
    };
}

#endif
