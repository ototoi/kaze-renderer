#ifndef KAZE_COLOR_TRAITS_HPP
#define KAZE_COLOR_TRAITS_HPP

#include "color.h"

#ifdef _MSC_VER
#pragma warning(disable : 4244)
#endif

namespace kaze
{
    //---------------------------------------------------------------------
    //color_zero_traits

    template <class T>
    class color_zero_traits
    {
    public:
        static T zero()
        {
            static const T ZERO = (T() - T());
            return ZERO;
        }
    };

    template <>
    class color_zero_traits<float>
    {
    public:
        static float zero() { return 0.f; }
    };

    template <>
    class color_zero_traits<double>
    {
    public:
        static double zero() { return 0.0; }
    };

    template <>
    class color_zero_traits<long double>
    {
    public:
        static long double zero() { return 0.0; }
    };

    template <>
    class color_zero_traits<color3f>
    {
    public:
        static color3f zero() { return color3f(0, 0, 0); }
    };

    template <>
    class color_zero_traits<color4f>
    {
    public:
        static color4f zero() { return color4f(0, 0, 0, 0); }
    };

    template <>
    class color_zero_traits<color3d>
    {
    public:
        static color3d zero() { return color3d(0, 0, 0); }
    };

    template <>
    class color_zero_traits<color4d>
    {
    public:
        static color4d zero() { return color4d(0, 0, 0, 0); }
    };

    //---------------------------------------------------------------------
    //color_element_traits
    template <class T>
    class color_element_traits
    {
    public:
        static const int element_number = 1;
        typedef T element_type;
    };

    template <>
    class color_element_traits<color3f>
    {
    public:
        static const int element_number = 3;
        typedef float element_type;
    };
    template <>
    class color_element_traits<color3d>
    {
    public:
        static const int element_number = 3;
        typedef double element_type;
    };

    template <>
    class color_element_traits<color4f>
    {
    public:
        static const int element_number = 4;
        typedef float element_type;
    };
    template <>
    class color_element_traits<color4d>
    {
    public:
        static const int element_number = 4;
        typedef double element_type;
    };

    //---------------------------------------------------------------------
    //color_convert_traits
    template <class T>
    class color_convert_traits
    {
    public:
        template <class X>
        static T convert_from(const X& x)
        {
            return (T)x;
        }
    };

    template <>
    class color_convert_traits<float>
    {
    public:
        template <class X>
        static float convert_from(const X& x)
        {
            return (float)x;
        }
        static float convert_from(float x) { return float(x); }
        static float convert_from(double x) { return float(x); }
        static float convert_from(long double x) { return float(x); }
    };

    template <>
    class color_convert_traits<double>
    {
    public:
        template <class X>
        static double convert_from(const X& x)
        {
            return (double)x;
        }
        static double convert_from(float x) { return double(x); }
        static double convert_from(double x) { return double(x); }
        static double convert_from(long double x) { return double(x); }
    };

    template <>
    class color_convert_traits<long double>
    {
    public:
        template <class X>
        static long double convert_from(const X& x)
        {
            return (long double)x;
        }
        static long double convert_from(float x) { return (long double)(x); }
        static long double convert_from(double x) { return (long double)(x); }
        static long double convert_from(long double x) { return (long double)(x); }
    };

    template <>
    class color_convert_traits<vector3f>
    {
    public:
        template <class X>
        static vector3f convert_from(const X& x)
        {
            return (vector3f)x;
        }
        static vector3f convert_from(float x) { return vector3f(x, x, x); }
        static vector3f convert_from(double x) { return vector3f(x, x, x); }
        static vector3f convert_from(long double x) { return vector3f(x, x, x); }
        static vector3f convert_from(const vector3f& x) { return x; }
        static vector3f convert_from(const vector3d& x) { return vector3f(x[0], x[1], x[2]); }
    };

    template <>
    class color_convert_traits<vector3d>
    {
    public:
        template <class X>
        static vector3d convert_from(const X& x)
        {
            return (vector3d)x;
        }
        static vector3d convert_from(float x) { return vector3d(x, x, x); }
        static vector3d convert_from(double x) { return vector3d(x, x, x); }
        static vector3d convert_from(long double x) { return vector3d(x, x, x); }
        static vector3d convert_from(const vector3f& x) { return vector3d(x[0], x[1], x[2]); }
        static vector3d convert_from(const vector3d& x) { return x; }
    };

    template <>
    class color_convert_traits<vector4f>
    {
    public:
        template <class X>
        static vector4f convert_from(const X& x)
        {
            return (vector4f)x;
        }
        static vector4f convert_from(float x) { return vector4f(x, x, x); }
        static vector4f convert_from(double x) { return vector4f(x, x, x); }
        static vector4f convert_from(long double x) { return vector4f(x, x, x, x); }
        static vector4f convert_from(const vector3f& x) { return vector4f(x[0], x[1], x[2], 1.f); }
        static vector4f convert_from(const vector3d& x) { return vector4f(x[0], x[1], x[2], 1.f); }
        static vector4f convert_from(const vector4f& x) { return x; }
        static vector4f convert_from(const vector4d& x) { return vector4f(x[0], x[1], x[2], x[3]); }
    };

    template <>
    class color_convert_traits<vector4d>
    {
    public:
        template <class X>
        static vector4d convert_from(const X& x)
        {
            return (vector4d)x;
        }
        static vector4d convert_from(float x) { return vector4d(x, x, x); }
        static vector4d convert_from(double x) { return vector4d(x, x, x); }
        static vector4d convert_from(long double x) { return vector4d(x, x, x); }
        static vector4d convert_from(const vector3f& x) { return vector4d(x[0], x[1], x[2], 1.0); }
        static vector4d convert_from(const vector3d& x) { return vector4d(x[0], x[1], x[2], 1.0); }
        static vector4d convert_from(const vector4f& x) { return vector4d(x[0], x[1], x[2], x[3]); }
        static vector4d convert_from(const vector4d& x) { return x; }
    };

    template <class T>
    class color_traits : public color_zero_traits<T>,
                         public color_element_traits<T>,
                         public color_convert_traits<T>
    {
    };
}

#endif
