#ifndef KAZE_COLOR_CONVERT_H
#define KAZE_COLOR_CONVERT_H

#include "color.h"

namespace kaze
{

    //------------------------------------------------
    color3 HSVtoRGB(const color3& c);
    color3 RGBtoHSV(const color3& c);
    color3 HLStoRGB(const color3& c);
    color3 RGBtoHLS(const color3& c);
    color3 YCrCbtoRGB(const color3& c);
    color3 RGBtoYCrCb(const color3& c);
    color3 RGBtoCMY(const color3& c);
    color3 CMYtoRGB(const color3& c);
    //------------------------------------------------
    color3 XYZtoRGB(const color3& c);
    color3 RGBtoXYZ(const color3& c);
    //------------------------------------------------
    color3 CIEtoRGB(double w);

    //------------------------------------------------
    void rgb_to_xyz(const double rgb[3], double xyz[3]);
    void xyz_to_rgb(const double xyz[3], double rgb[3]);
    void xyz_to_rgb_black(const double xyz[3], double rgb[3]);
    void wl_to_xyz(int w, double xyz[3]);
    void xyz_to_wvalue(const double xyz[3], double cie[3]); //at value of 700,546,439
    void rgb_to_wvalue(const double rgb[3], double cie[3]); //at value of 700,546,439
    double sample_wvalue(const double xyz[3], int w);
    double sample_wvalue_bee(const double rgb[3], int w);
    //------------------------------------------------
    void xvYCC_lrgb_to_hrgb(const double lrgb[3], double hrgb[3]);
    void xvYCC_hrgb_to_lrgb(const double hrgb[3], double lrgb[3]);
    //------------------------------------------------
    void xyz_to_AdobeRGB(const double xyz[3], double rgb[3]);
    void liner_to_gamma_AdobeRGB(const double lrgb[3], double grgb[3]);
    //------------------------------------------------
    void gamma_to_linear_sRGB(const double grgb[3], double lrgb[3]);
    void linear_to_gamma_sRGB(const double lrgb[3], double grgb[3]);

    //------------------------------------------------
    unsigned int color3toRGBA(const color3& c);
    unsigned int color3toARGB(const color3& c);
    unsigned int color4toRGBA(const color4& c);
    unsigned int color4toARGB(const color4& c);
    //------------------------------------------------
    color3 RGBAtocolor3(unsigned int C);
    color3 ARGBtocolor3(unsigned int C);
    color4 RGBAtocolor4(unsigned int C);
    color4 ARGBtocolor4(unsigned int C);
    //------------------------------------------------
    unsigned int color3toRGBE(const color3& c);
    unsigned int color4toRGBE(const color4& c);
    //------------------------------------------------
    color3 RGBEtocolor3(unsigned int C);
    color4 RGBEtocolor4(unsigned int C);
    //------------------------------------------------
}

#endif
