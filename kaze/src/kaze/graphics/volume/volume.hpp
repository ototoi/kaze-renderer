#ifndef KAZE_VOLUME_HPP
#define KAZE_VOLUME_HPP

namespace kaze
{
    template <class T>
    class volume
    {
    public:
        typedef T value_type;
    public:
        virtual ~volume() {}
        virtual T get(int x, int y, int z) const = 0;
        virtual int get_width() const = 0;
        virtual int get_height() const = 0;
        virtual int get_depth() const = 0;
    };
}

#endif