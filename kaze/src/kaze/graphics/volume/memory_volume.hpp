#ifndef KAZE_MEMORY_VOLUME_HPP
#define KAZE_MEMORY_VOLUME_HPP

#include "volume.hpp"

namespace kaze
{

    template <class T>
    class memory_volume : public volume<T>
    {
    public:
        memory_volume(int w, int h, int d) : w_(w), h_(h), d_(d), vec_(w * h * d) {}
        int get_width() const { return w_; }
        int get_height() const { return h_; }
        int get_depth() const { return d_; }
        T get(int x, int y, int z) const
        {
            size_t idx = (size_t)(x + (w_ * (y + h_ * (z))));
            assert(idx < vec_.size());
            return vec_[idx];
        }

        void set(int x, int y, int z, const T& t)
        {
            size_t idx = (size_t)(x + (w_ * (y + h_ * (z))));
            assert(idx < vec_.size());
            vec_[idx] = t;
        }

        T* ptr() { return &(vec_[0]); }
        const T* ptr() const { return &(vec_[0]); }
    private:
        int w_;
        int h_;
        int d_;
        std::vector<T> vec_;
    };

    typedef memory_volume<real> real_memory_volume;
    typedef memory_volume<color1> color1_memory_volume;
    typedef memory_volume<color2> color2_memory_volume;
    typedef memory_volume<color3> color3_memory_volume;
    typedef memory_volume<color3> color4_memory_volume;
}

#endif