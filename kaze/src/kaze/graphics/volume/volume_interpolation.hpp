#ifndef KAZE_VOLUME_INTERPOLATION_HPP
#define KAZE_VOLUME_INTERPOLATION_HPP

#include "types.h"
#include "volume.hpp"
#include "corresponder.h"
#include <cmath>

namespace kaze
{

    template <class T>
    T c0_interpolation(const volume<T>& vox, real x, real y, real z)
    {
        int ww = vox.get_width();
        int hh = vox.get_height();
        int dd = vox.get_depth();

        real rx = x * ww;
        real ry = y * hh;
        real rz = z * dd;

        real ix = floor(rx);
        real iy = floor(ry);
        real iz = floor(rz);

        int xx = (int)ix;
        int yy = (int)iy;
        int zz = (int)iz;

        return vox.get(xx, yy, zz);
    }

    template <class T>
    T c0_interpolation(const volume<T>& vox, const corresponder& corx, const corresponder& cory, const corresponder& corz, real x, real y, real z)
    {
        int ww = vox.get_width();
        int hh = vox.get_height();
        int dd = vox.get_depth();

        real rx = x * ww;
        real ry = y * hh;
        real rz = z * dd;

        real ix = floor(rx);
        real iy = floor(ry);
        real iz = floor(rz);

        int xx = (int)ix;
        int yy = (int)iy;
        int zz = (int)iz;

        xx = corx.get(xx, ww);
        yy = cory.get(yy, hh);
        zz = cory.get(zz, dd);

        return vox.get(xx, yy, zz);
    }

    template <class T>
    T c0_interpolation(const volume<T>& vox, const corresponder& cor, real x, real y, real z)
    {
        return c0_interpolation(vox, cor, cor, cor, x, y, z);
    }

    template <class T, class F>
    T c1_interpolation(const volume<T>& vox, real x, real y, real z)
    {
        int ww = vox.get_width();
        int hh = vox.get_height();
        int dd = vox.get_depth();

        real rx = x * ww;
        real ry = y * hh;
        real rz = z * dd;

        rx -= real(0.5);
        ry -= real(0.5);
        rz -= real(0.5);

        real ix = floor(rx);
        real iy = floor(ry);
        real iz = floor(rz);

        real u = rx - ix;
        real v = ry - iy;
        real w = rz - iz;

        real iu = 1 - u;
        real iv = 1 - v;
        real iw = 1 - w;

        int x0 = (int)ix;
        int y0 = (int)iy;
        int z0 = (int)iz;
        int x1 = x0+1;
        int y1 = y0+1;
        int z1 = z0+1;

        u = F::static_get(u);
        v = F::static_get(v);
        w = F::static_get(w);

        iu = F::static_get(iu);
        iv = F::static_get(iv);
        iw = F::static_get(iw);

        return ( u *  v *  w) * vox.get(x0, y0, z0) +
               (iu *  v *  w) * vox.get(x1, y0, z0) +
               ( u * iv *  w) * vox.get(x0, y1, z0) +
               (iu * iv *  w) * vox.get(x1, y1, z0) +
               ( u *  v * iw) * vox.get(x0, y0, z1) +
               (iu *  v * iw) * vox.get(x1, y0, z1) +
               ( u * iv * iw) * vox.get(x0, y1, z1) +
               (iu * iv * iw) * vox.get(x1, y1, z1);
    }

    template <class T, class F>
    T c1_interpolation(const volume<T>& vox, const corresponder& corx, const corresponder& cory, const corresponder& corz, real x, real y, real z)
    {
        int ww = vox.get_width();
        int hh = vox.get_height();
        int dd = vox.get_depth();

        real rx = x * ww;
        real ry = y * hh;
        real rz = z * dd;

        rx -= real(0.5);
        ry -= real(0.5);
        rz -= real(0.5);

        real ix = floor(rx);
        real iy = floor(ry);
        real iz = floor(rz);

        real u = rx - ix;
        real v = ry - iy;
        real w = rz - iz;

        real iu = 1 - u;
        real iv = 1 - v;
        real iw = 1 - w;

        int xx = (int)ix;
        int yy = (int)iy;
        int zz = (int)iz;

        int x0 = corx.get(xx+0, ww);
        int y0 = cory.get(yy+0, hh);
        int z0 = cory.get(zz+0, dd);
        int x1 = corx.get(xx+1, ww);
        int y1 = cory.get(yy+1, hh);
        int z1 = cory.get(zz+1, dd);

        u = F::static_get(u);
        v = F::static_get(v);
        w = F::static_get(w);

        iu = F::static_get(iu);
        iv = F::static_get(iv);
        iw = F::static_get(iw);

        return ( u *  v *  w) * vox.get(x0, y0, z0) +
               (iu *  v *  w) * vox.get(x1, y0, z0) +
               ( u * iv *  w) * vox.get(x0, y1, z0) +
               (iu * iv *  w) * vox.get(x1, y1, z0) +
               ( u *  v * iw) * vox.get(x0, y0, z1) +
               (iu *  v * iw) * vox.get(x1, y0, z1) +
               ( u * iv * iw) * vox.get(x0, y1, z1) +
               (iu * iv * iw) * vox.get(x1, y1, z1);
    }

    template <class T, class F>
    T c1_interpolation(const volume<T>& vox, const corresponder& cor, real x, real y, real z)
    {
        return c1_interpolation<T, F>(vox, cor, cor, cor, x, y, z);
    }

    template <class T>
    inline T sum_z(
        const real wz[4],
        const int x, const int y, const int dz[4],
        const volume<T>& vox)
    {
        return wz[0] * vox.get(x, y, dz[0]) +
               wz[1] * vox.get(x, y, dz[1]) +
               wz[2] * vox.get(x, y, dz[2]) +
               wz[3] * vox.get(x, y, dz[3]);
    }

    template <class T>
    inline T sum_y(
        const real wy[4], const real wz[4],
        const int x, const int dy[4], const int dz[4],
        const volume<T>& vox)
    {
        return wy[0] * sum_z(wz, x, dy[0], dz, vox) +
               wy[1] * sum_z(wz, x, dy[1], dz, vox) +
               wy[2] * sum_z(wz, x, dy[2], dz, vox) +
               wy[3] * sum_z(wz, x, dy[3], dz, vox);
    }

    template <class T>
    T sum_x(
        const real wx[4], const real wy[4], const real wz[4],
        const int dx[4], const int dy[4], const int dz[4],
        const volume<T>& vox)
    {
        return wx[0] * sum_y(wy, wz, dx[0], dy, dz, vox) +
               wx[1] * sum_y(wy, wz, dx[1], dy, dz, vox) +
               wx[2] * sum_y(wy, wz, dx[2], dy, dz, vox) +
               wx[3] * sum_y(wy, wz, dx[3], dy, dz, vox);
    }

    template <class T, class F>
    T c2_interpolation(const volume<T>& vox, real x, real y, real z)
    {
        int ww = vox.get_width();
        int hh = vox.get_height();
        int dd = vox.get_depth();

        real rx = x * ww;
        real ry = y * hh;
        real rz = z * dd;

        rx -= real(0.5);
        ry -= real(0.5);
        rz -= real(0.5);

        real ix = floor(rx);
        real iy = floor(ry);
        real iz = floor(rz);

        int xx = (int)ix;
        int yy = (int)iy;
        int zz = (int)iz;

        int xd[4];
        int yd[4];
        int zd[4];
        for (int i = 0; i < 4; i++)
        {
            xd[i] = xx - 1 + i;
            yd[i] = yy - 1 + i;
            zd[i] = zz - 1 + i;
        }

        real weight_x[4];
        real weight_y[4];
        real weight_z[4];

        for (int i = 0; i < 4; i++)
        {
            int p = i - 1;
            real sx = xx + p;
            real sy = yy + p;
            real sz = zz + p;
            real wx = fabs(rx - sx);
            real wy = fabs(ry - sy);
            real wz = fabs(rz - sz);
            wx = F::static_get(wx);
            wy = F::static_get(wy);
            wz = F::static_get(wz);
            weight_x[i] = wx;
            weight_y[i] = wy;
            weight_z[i] = wz;
        }

        return sum_x(weight_x, weight_y, weight_z, xd, yd, zd, vox);
    }

    template <class T, class F>
    T c2_interpolation(const volume<T>& vox, const corresponder& corx, const corresponder& cory, const corresponder& corz, real x, real y, real z)
    {
        int ww = vox.get_width();
        int hh = vox.get_height();
        int dd = vox.get_depth();

        real rx = x * ww;
        real ry = y * hh;
        real rz = z * dd;

        rx -= real(0.5);
        ry -= real(0.5);
        rz -= real(0.5);

        real ix = floor(rx);
        real iy = floor(ry);
        real iz = floor(rz);

        int xx = (int)ix; //-1
        int yy = (int)iy;
        int zz = (int)iz;

        int xd[4];
        int yd[4];
        int zd[4];
        for (int i = 0; i < 4; i++)
        {
            xd[i] = corx.get(xx - 1 + i, ww);
            yd[i] = cory.get(yy - 1 + i, hh);
            zd[i] = corz.get(zz - 1 + i, dd);
        }

        real weight_x[4];
        real weight_y[4];
        real weight_z[4];

        for (int i = 0; i < 4; i++)
        {
            int p = i - 1;
            real sx = xx + p;
            real sy = yy + p;
            real sz = zz + p;
            real wx = fabs(rx - sx);
            real wy = fabs(ry - sy);
            real wz = fabs(rz - sz);
            wx = F::static_get(wx);
            wy = F::static_get(wy);
            wz = F::static_get(wz);
            weight_x[i] = wx;
            weight_y[i] = wy;
            weight_z[i] = wz;
        }

        return sum_x(weight_x, weight_y, weight_z, xd, yd, zd, vox);
    }

    template <class T, class F>
    T c2_interpolation(const volume<T>& vox, const corresponder& cor, real x, real y, real z)
    {
        return c2_interpolation<T, F>(vox, cor, cor, cor, x, y, z);
    }
}

#endif