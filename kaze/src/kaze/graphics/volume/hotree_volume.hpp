#ifndef KAZE_HOCTREE_VOLUME_HPP
#define KAZE_HOCTREE_VOLUME_HPP

#include "octree_hierarchy.hpp"
#include "volume.hpp"

namespace kaze
{

    template <class T>
    class hotree_volume : public volume<T>
    {
    public:
        hotree_volume(int w, int h, int d)
            : w_(w), h_(h), d_(d)
        {
            pOctree_ = new octree_hierarchy<T>(w, h, d);
        }
        hotree_volume(const hotree_volume<T>& rhs)
            : w_(rhs.w_), h_(rhs.h_), d_(rhs.d_)
        {
            pOctree_ = rhs.pOctree_->clone();
        }
        ~hotree_volume()
        {
            delete pOctree_;
        }
        T get(int x, int y, int z) const
        {
            return pOctree_->get(x, y, z);
        }
        void set(int x, int y, int z, const T& v)
        {
            pOctree_->set(x, y, z, v);
        }
        int get_width() const { return w_; }
        int get_height() const { return h_; }
        int get_depth() const { return d_; }
    public:
        void swap(hotree_volume<T>& rhs)
        {
            std::swap(pOctree_, rhs.pOctree_);
            std::swap(w_, rhs.w_);
            std::swap(h_, rhs.h_);
            std::swap(d_, rhs.d_);
        }
        hotree_volume<T>& operator=(const hotree_volume<T>& rhs)
        {
            hotree_volume<T> tmp(rhs);
            swap(tmp);
            return *this;
        }

    private:
        octree_hierarchy<T>* pOctree_;
        int w_;
        int h_;
        int d_;
    };
}

#endif
