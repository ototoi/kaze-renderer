#ifndef KAZE_VOLUME_INTERPOLATOR_HPP
#define KAZE_VOLUME_INTERPOLATOR_HPP

#include "volume.hpp"
#include "volume_interpolation.hpp"
#include "corresponder.h"
#include "filter.h"

namespace kaze
{
    template <class T>
    class volume_interpolator
    {
    public:
        virtual ~volume_interpolator() {}
    public:
        virtual T get(const volume<T>& vox, real x, real y, real z) const = 0;
        virtual T get(const volume<T>& vox, const corresponder& cor, real x, real y, real z) const = 0;
        virtual T get(const volume<T>& vox, const corresponder& corx, const corresponder& cory, const corresponder& corz, real x, real y, real z) const = 0;
    };

    template <class T>
    class c0_volume_interpolator : public volume_interpolator<T>
    {
    public:
        T get(const volume<T>& vox, real x, real y, real z) const { return static_get(vox, x, y, z); }
        T get(const volume<T>& vox, const corresponder& cor, real x, real y, real z) const { return static_get(vox, cor, x, y, z); }
        T get(const volume<T>& vox, const corresponder& corx, const corresponder& cory, const corresponder& corz, real x, real y, real z) const { return static_get(vox, corx, cory, corz, x, y, z); }
        static T static_get(const volume<T>& vox, real x, real y, real z) { return c0_interpolation(vox, x, y, z); }
        static T static_get(const volume<T>& vox, const corresponder& cor, real x, real y, real z) { return c0_interpolation(vox, cor, x, y, z); }
        static T static_get(const volume<T>& vox, const corresponder& corx, const corresponder& cory, const corresponder& corz, real x, real y, real z) { return c0_interpolation(vox, corx, cory, corz, x, y, z); }
    };

    template <class T, class F>
    class c1_volume_interpolator : public volume_interpolator<T>
    {
    public:
        T get(const volume<T>& vox, real x, real y, real z) const { return static_get(vox, x, y, z); }
        T get(const volume<T>& vox, const corresponder& cor, real x, real y, real z) const { return static_get(vox, cor, x, y, z); }
        T get(const volume<T>& vox, const corresponder& corx, const corresponder& cory, const corresponder& corz, real x, real y, real z) const { return static_get(vox, corx, cory, corz, x, y, z); }
        static T static_get(const volume<T>& vox, real x, real y, real z) { return c1_interpolation<T, F>(vox, x, y, z); }
        static T static_get(const volume<T>& vox, const corresponder& cor, real x, real y, real z) { return c1_interpolation<T, F>(vox, cor, x, y, z); }
        static T static_get(const volume<T>& vox, const corresponder& corx, const corresponder& cory, const corresponder& corz, real x, real y, real z) { return c1_interpolation<T, F>(vox, corx, cory, corz, x, y, z); }
    };

    template <class T, class F>
    class c2_volume_interpolator : public volume_interpolator<T>
    {
    public:
        T get(const volume<T>& vox, real x, real y, real z) const { return static_get(vox, x, y, z); }
        T get(const volume<T>& vox, const corresponder& cor, real x, real y, real z) const { return static_get(vox, cor, x, y, z); }
        T get(const volume<T>& vox, const corresponder& corx, const corresponder& cory, const corresponder& corz, real x, real y, real z) const { return static_get(vox, corx, cory, corz, x, y, z); }
        static T static_get(const volume<T>& vox, real x, real y, real z) { return c2_interpolation<T, F>(vox, x, y, z); }
        static T static_get(const volume<T>& vox, const corresponder& cor, real x, real y, real z) { return c2_interpolation<T, F>(vox, cor, x, y, z); }
        static T static_get(const volume<T>& vox, const corresponder& corx, const corresponder& cory, const corresponder& corz, real x, real y, real z) { return c2_interpolation<T, F>(vox, corx, cory, corz, x, y, z); }
    };

    template <class T>
    class nearest_volume_interpolator : public c0_volume_interpolator<T>
    {
    };

    template <class T>
    class bilinear_volume_interpolator : public c1_volume_interpolator<T, triangle_filter>
    {
    };

    template <class T>
    class hermit_volume_interpolator : public c1_volume_interpolator<T, hermite_filter>
    {
    };

    template <class T>
    class cubic_volume_interpolator : public c2_volume_interpolator<T, cubic_filter>
    {
    };

    template <class T>
    class bspline_volume_interpolator : public c2_volume_interpolator<T, bspline_filter>
    {
    };

    template <class T>
    class mitchell_volume_interpolator : public c2_volume_interpolator<T, mitchell_filter>
    {
    };

    template <class T>
    class lagrange_volume_interpolator : public c2_volume_interpolator<T, lagrange_filter>
    {
    };

    template <class T>
    class gaussian_volume_interpolator : public c2_volume_interpolator<T, gaussian_filter>
    {
    };

    template <class T>
    class sinc_volume_interpolator : public c2_volume_interpolator<T, sinc_filter>
    {
    };

    template <class T>
    class lanczos_volume_interpolator : public c2_volume_interpolator<T, lanczos_filter>
    {
    };
}

#endif
