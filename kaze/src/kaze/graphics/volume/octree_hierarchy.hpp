#ifndef KAZE_OCTREE_HIERARCHY_HPP
#define KAZE_OCTREE_HIERARCHY_HPP

#include <algorithm>

namespace kaze
{

    template <class T>
    class octree_hierarchy
    {
    public:
        static const int N = 64;
        static const int N_LEVEL = 6; //0->1
        struct octree_node
        {
        };
        struct octree_node_branch : public octree_node
        {
            octree_node_branch()
            {
                memset(nodes, 0, sizeof(octree_node*) * 8);
            }
            ~octree_node_branch()
            {
                for (int i = 0; i < 8; i++)
                {
                    if (nodes[i])
                    {
                        delete nodes[i];
                    }
                }
            }
            octree_node* nodes[8];
        };
        struct octree_node_leaf : public octree_node
        {
            octree_node_leaf()
            {
                for (int i = 0; i < N * N * N; i++)
                {
                    values[i] = T();
                }
            }
            T values[N * N * N];
        };
        static int powerof2(int x)
        {
            int i = 1;
            while (i < x)
            {
                i <<= 1;
            }
            return i;
        }
        static int get_level(int x)
        {
            int r = 0;
            while (x > 1)
            {
                r++;
                x >>= 1;
            }
            return r;
        }

    public:
        octree_hierarchy(int w, int h, int d)
        {
            w = powerof2(w);
            h = powerof2(h);
            d = powerof2(d);
            int k = std::max<int>(w, std::max<int>(h, d));
            if (k <= N)
            {
                level_ = N_LEVEL;
                root_ = new octree_node_leaf();
            }
            else
            {
                level_ = get_level(k);
                root_ = new octree_node_branch();
            }
        }
        octree_hierarchy(octree_node* root, int level)
            : root_(root), level_(level)
        {
        }

        ~octree_hierarchy()
        {
            destroy(level_, root_);
        }

        int get_level() const { return level_; }
        int get_grid_size() const { return 1 << level_; }

        T get(int x, int y, int z) const
        {
            return get(level_, root_, x, y, z);
        }

        void set(int x, int y, int z, const T& v)
        {
            set(level_, root_, x, y, z, v);
        }

        octree_hierarchy* clone() const
        {

            return new octree_hierarchy(clone(level_, root_), level_);
        }

    protected:
        static octree_node* clone(int level, const octree_node* node)
        {
            if (level <= N_LEVEL)
            {
                const octree_node_leaf* pSrc = static_cast<const octree_node_leaf*>(node);

                octree_node_leaf* pNode = new octree_node_leaf();
                *pNode = *pSrc;
                return pNode;
            }
            else
            {
                const octree_node_branch* pSrc = static_cast<const octree_node_branch*>(node);

                octree_node_branch* pNode = new octree_node_branch();
                for (int i = 0; i < 8; i++)
                {
                    if (pSrc->nodes[i])
                    {
                        pNode->nodes[i] = clone(level - 1, pSrc->nodes[i]);
                    }
                }
                return pNode;
            }
        }
        static void destroy(int level, octree_node* node)
        {
            if (!node) return;
            if (level <= N_LEVEL)
            {
                delete node;
            }
            else
            {
                const octree_node_branch* pNode = static_cast<const octree_node_branch*>(node);
                destroy(level - 1, pNode->nodes[0]);
                destroy(level - 1, pNode->nodes[1]);
                destroy(level - 1, pNode->nodes[2]);
                destroy(level - 1, pNode->nodes[3]);
                destroy(level - 1, pNode->nodes[4]);
                destroy(level - 1, pNode->nodes[5]);
                destroy(level - 1, pNode->nodes[6]);
                destroy(level - 1, pNode->nodes[7]);
                delete node;
            }
        }

        static void set(int level, octree_node* node, int x, int y, int z, const T& v)
        {
            if (level <= N_LEVEL)
            {
                octree_node_leaf* pNode = static_cast<octree_node_leaf*>(node);
                pNode->values[(z * N + y) * N + x] = v;
            }
            else
            {
                octree_node_branch* pNode = static_cast<octree_node_branch*>(node);

                int w = 1 << level;
                int m = w >> 1;
                int nFlag = 0;
                if (m <= x)
                {
                    nFlag |= 1;
                    x -= m;
                }
                if (m <= y)
                {
                    nFlag |= 2;
                    y -= m;
                }
                if (m <= z)
                {
                    nFlag |= 4;
                    z -= m;
                }

                octree_node* pChild = pNode->nodes[nFlag];
                if (!pChild)
                {
                    if (level - 1 <= N_LEVEL)
                    {
                        pNode->nodes[nFlag] = pChild = new octree_node_leaf();
                    }
                    else
                    {
                        pNode->nodes[nFlag] = pChild = new octree_node_branch();
                    }
                }
                set(level - 1, pChild, x, y, z, v);
            }
        }

        static T get(int level, const octree_node* node, int x, int y, int z)
        {
            if (level <= N_LEVEL)
            {
                const octree_node_leaf* pNode = static_cast<const octree_node_leaf*>(node);

                return pNode->values[(z * N + y) * N + x];
            }
            else
            {
                const octree_node_branch* pNode = static_cast<const octree_node_branch*>(node);

                int w = 1 << level;
                int m = w >> 1;
                int nFlag = 0;
                if (m <= x)
                {
                    nFlag |= 1;
                    x -= m;
                }
                if (m <= y)
                {
                    nFlag |= 2;
                    y -= m;
                }
                if (m <= z)
                {
                    nFlag |= 4;
                    z -= m;
                }

                const octree_node* pChild = pNode->nodes[nFlag];
                if (!pChild)
                {
                    return T();
                }
                else
                {
                    return get(level - 1, pChild, x, y, z);
                }
            }
        }

    private:
        octree_node* root_;
        int level_; //max level
    };
}

#endif
