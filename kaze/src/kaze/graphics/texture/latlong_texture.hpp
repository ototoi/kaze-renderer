#ifndef KAZE_LATLONG_TEXTURE_HPP
#define KAZE_LATLONG_TEXTURE_HPP

#include "types.h"
#include "values.h"
#include "texture.hpp"
#include "count_ptr.hpp"

#include <vector>
#include <cmath>

namespace kaze
{

    template <class T>
    class latlong_texture : public texture<T>
    {
    public:
        latlong_texture(
            const std::vector<std::shared_ptr<texture<T> > >& vt)
        {
            vt_ = vt[0];
        }

        latlong_texture(
            const auto_count_ptr<texture<T> >& vt) : vt_(vt)
        {
            ;
        }

        ~latlong_texture()
        {
            ;
        }

    public:
        T get(const sufflight& suf) const
        {
            static const real PI = values::pi();
            static const real PI2 = 2 * PI;
            vector3 d1 = normalize(suf.direction());
            real x = d1[0];
            real y = d1[1];
            real z = d1[2];

            real phi = atan2(z, x);
            if (phi < 0) phi += PI2;
            real l = sqrt(x * x + z * z);
            real theta = atan2(y, l);

            real s = phi / PI2;
            real t = 1.0 - (theta / PI + 1) * 0.5;
            sufflight tsl(suf);
            tsl.set_stw(color3(s, t, 0));
            return vt_->get(tsl);
        }

    protected:
        std::shared_ptr<texture<T> > vt_;
    };
}

#endif
