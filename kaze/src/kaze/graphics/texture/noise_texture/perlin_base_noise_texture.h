#ifndef KAZE_PERLRIN_BASE_NOISE_TEXTURE_H
#define KAZE_PERLRIN_BASE_NOISE_TEXTURE_H

#include "texture.hpp"
#include "base_noise_texture.h"

namespace kaze
{

    class perlin_base_noise_texture : public base_noise_texture
    {
    public:
        perlin_base_noise_texture();
        perlin_base_noise_texture(real frequency, real persistence = 1.0);

        virtual real get_value(real u, real v, real w) const;

    protected:
        real frequency_;
        real persistence_;
    };
}

#endif