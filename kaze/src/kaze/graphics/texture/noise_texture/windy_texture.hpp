#ifndef KAZE_WINDY_TEXTURE_HPP
#define KAZE_WINDY_TEXTURE_HPP

#include "texture.hpp"
#include "base_windy_texture.h"

#include "count_ptr.hpp"

namespace kaze
{

    template <class T>
    class windy_texture : public texture<T>
    {
    public:
        windy_texture(const auto_count_ptr<texture<T> >& tex) : tex_(tex) {}
    public:
        T get(const sufflight& suf) const
        {
            real k = nz_.get(suf);
            return k * tex_->get(suf);
        }

    private:
        base_windy_texture nz_;
        std::shared_ptr<texture<T> > tex_;
    };

    template <>
    class windy_texture<real> : public base_windy_texture
    {
    };
}

#endif