#ifndef KAZE_BASE_WINDY_TEXTURE_H
#define KAZE_BASE_WINDY_TEXTURE_H

#include "base_noise_texture.h"

namespace kaze
{

    class base_windy_texture : public base_noise_texture
    {
    public:
        base_windy_texture();
        real get_value(real u, real v, real w) const;
    };
}

#endif
