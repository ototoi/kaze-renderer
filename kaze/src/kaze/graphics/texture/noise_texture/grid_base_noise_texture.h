#ifndef KAZE_GRID_BASE_NOISE_TEXTURE_H
#define KAZE_GRID_BASE_NOISE_TEXTURE_H

#include "texture.hpp"
#include "base_noise_texture.h"

namespace kaze
{

    class grid_base_noise_texture : public base_noise_texture
    {
    public:
        grid_base_noise_texture(real freq = 16.0);
        real get_value(real u, real v, real w) const;

    private:
        real freq_;
    };
}

#endif