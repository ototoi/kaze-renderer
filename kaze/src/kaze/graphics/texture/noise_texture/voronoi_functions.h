#ifndef VORONOI_FUNCTIONS_H
#define VORONOI_FUNCTIONS_H

void voronoi(double x, double y, double z, double* da, double* pa, double me, int dtype);
double voronoi_F1(double x, double y, double z);
double voronoi_F2(double x, double y, double z);
double voronoi_F3(double x, double y, double z);
double voronoi_F4(double x, double y, double z);
double voronoi_F1F2(double x, double y, double z);
double voronoi_Cr(double x, double y, double z);

#endif