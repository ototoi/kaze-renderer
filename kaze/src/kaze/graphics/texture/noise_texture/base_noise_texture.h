#ifndef KAZE_BASE_NOISE_TEXTURE_H
#define KAZE_BASE_NOISE_TEXTURE_H

#include "texture.hpp"
#include "hyper_texture.h"
#include "texture.hpp"

namespace kaze
{

    class base_noise_texture : public texture<real>,
                               public hyper_texture
    {
    public:
        real get(const sufflight& suf) const;
        virtual real get_value(real u, real v, real w) const;
    };
}

#endif