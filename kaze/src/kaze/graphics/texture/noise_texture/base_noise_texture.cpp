#include "base_noise_texture.h"

#include <vector>

namespace kaze
{

    real base_noise_texture::get(const sufflight& suf) const
    {
        vector3 c = suf.stw();
        return this->get_value(c[0], c[1], c[2]);
    }

    real base_noise_texture::get_value(real u, real v, real w) const
    {
        static const int f = 16;
        return fabs(noise(f * u, f * v, f * w));
    }
}