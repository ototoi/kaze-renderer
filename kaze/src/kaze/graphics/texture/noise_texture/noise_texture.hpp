#ifndef KAZE_NOISE_TEXTURE_HPP
#define KAZE_NOISE_TEXTURE_HPP

#include "base_noise_texture.h"
#include "count_ptr.hpp"

#include "perlin_base_noise_texture.h"
#include "fBm_base_noise_texture.h"
#include "turbulence_base_noise_texture.h"
#include "grid_base_noise_texture.h"

namespace kaze
{

    template <class T>
    class noise_texure : public texture<T>
    {
    public:
        noise_texure(const auto_count_ptr<base_noise_texture>& nz, const auto_count_ptr<texture<T> >& tex)
            : nz_(nz), tex_(tex) {}
    public:
        T get(const sufflight& suf) const
        {
            return nz_->get(suf) * tex_->get(suf);
        }

    protected:
        std::shared_ptr<base_noise_texture> nz_;
        std::shared_ptr<texture<T> > tex_;
    };

    template <class T>
    class perlin_noise_texture : public noise_texure<T>
    {
    public:
        perlin_noise_texture(real frequency, real persistence, const auto_count_ptr<texture<T> >& tex)
            : noise_texure<T>(new perlin_base_noise_texture(frequency, persistence), tex) {}
        perlin_noise_texture(const auto_count_ptr<texture<T> >& tex)
            : noise_texure<T>(new perlin_base_noise_texture(), tex) {}
    };

    template <>
    class perlin_noise_texture<real> : public perlin_base_noise_texture
    {
    public:
        perlin_noise_texture(real frequency, real persistence) : perlin_base_noise_texture(frequency, persistence) {}
    };

    template <class T>
    class fBm_noise_texture : public noise_texure<T>
    {
    public:
        fBm_noise_texture(real frequency, real persistence, int octave, const auto_count_ptr<texture<T> >& tex)
            : noise_texure<T>(new fBm_base_noise_texture(frequency, persistence, octave), tex) {}
        fBm_noise_texture(const auto_count_ptr<texture<T> >& tex)
            : noise_texure<T>(new fBm_base_noise_texture(), tex) {}
    };

    template <>
    class fBm_noise_texture<real> : public fBm_base_noise_texture
    {
    public:
        fBm_noise_texture(real frequency, real persistence, int octave) : fBm_base_noise_texture(frequency, persistence, octave) {}
    };

    template <class T>
    class turbulence_noise_texture : public noise_texure<T>
    {
    public:
        turbulence_noise_texture(real frequency, real persistence, int octave, const auto_count_ptr<texture<T> >& tex)
            : noise_texure<T>(new turbulence_base_noise_texture(frequency, persistence, octave), tex) {}
        turbulence_noise_texture(const auto_count_ptr<texture<T> >& tex)
            : noise_texure<T>(new turbulence_base_noise_texture(), tex) {}
    };

    template <>
    class turbulence_noise_texture<real> : public turbulence_base_noise_texture
    {
    public:
        turbulence_noise_texture(real frequency, real persistence, int octave) : turbulence_base_noise_texture(frequency, persistence, octave) {}
    };

    template <class T>
    class grid_noise_texture : public noise_texure<T>
    {
    public:
        grid_noise_texture(real frequency, const auto_count_ptr<texture<T> >& tex)
            : noise_texure<T>(new grid_base_noise_texture(frequency), tex) {}

        grid_noise_texture(const auto_count_ptr<texture<T> >& tex)
            : noise_texure<T>(new grid_base_noise_texture(), tex) {}
    };

    template <>
    class grid_noise_texture<real> : public grid_base_noise_texture
    {
    public:
        grid_noise_texture(real frequency) : grid_base_noise_texture(frequency) {}
    };
}

#endif
