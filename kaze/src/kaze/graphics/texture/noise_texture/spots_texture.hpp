#ifndef KAZE_SPOTS_TEXTURE_HPP
#define KAZE_SPOTS_TEXTURE_HPP

#include "texture.hpp"
#include "base_spots_texture.h"
#include "count_ptr.hpp"

namespace kaze
{

    template <class T>
    class spots_texture : public texture<T>
    {
    public:
        spots_texture(const auto_count_ptr<texture<T> >& a, const auto_count_ptr<texture<T> >& b)
            : a_(a), b_(b), pred_(std::shared_ptr<texture<bool> >(new base_spots_texture())) {}
        spots_texture(const auto_count_ptr<texture<T> >& a, const auto_count_ptr<texture<T> >& b, const auto_count_ptr<texture<bool> >& pred)
            : a_(a), b_(b), pred_(pred) {}
    public:
        T get(const sufflight& suf) const
        {
            if (pred_->get(suf))
            {
                return a_->get(suf);
            }
            else
            {
                return b_->get(suf);
            }
        }

    protected:
        std::shared_ptr<texture<T> > a_;
        std::shared_ptr<texture<T> > b_;
        std::shared_ptr<texture<bool> > pred_;
    };

    template <>
    class spots_texture<bool> : public base_spots_texture
    {
    public:
        spots_texture(real grid, real rad, real shift) : base_spots_texture(grid, rad, shift) {}
        spots_texture(const auto_count_ptr<texture_clipper>& clp, real grid, real rad, real shift) : base_spots_texture(clp, grid, rad, shift) {}
    };
}

#endif