#include "base_windy_texture.h"

namespace kaze
{

    base_windy_texture::base_windy_texture()
    {
        ; //
    }

    real base_windy_texture::get_value(real u, real v, real w) const
    {
        u *= 16;
        v *= 16;
        w *= 16;

        real coef = real(0.1);
        real windStrength = 0.5 * windy(coef * u, coef * v, coef * w, real(1.0), real(0.5), 3) + 0.5;
        real waveHeight = 4 * windy(u, v, w, real(1.0), real(0.5), 6);
        return 0.5 * (fabs(windStrength) * waveHeight) + 0.5;
    }
}
