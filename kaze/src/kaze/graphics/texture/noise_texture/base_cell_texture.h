#ifndef KAZE_BASE_CELL_TEXTURE_H
#define KAZE_BASE_CELL_TEXTURE_H

#include "texture.hpp"

namespace kaze
{

    class base_cell_texture : public texture<real>
    {
    public:
        real get(const sufflight& suf) const;
        real get_value(real u, real v, real w) const;
    };
}

#endif