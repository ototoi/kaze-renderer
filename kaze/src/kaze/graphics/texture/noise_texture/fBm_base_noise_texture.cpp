#include "fBm_base_noise_texture.h"

#include "hyper_texture.h"

namespace kaze
{

    fBm_base_noise_texture::fBm_base_noise_texture()
    {
        frequency_ = 16;
        persistence_ = 0.5;
        octave_ = 8;
    }

    fBm_base_noise_texture::fBm_base_noise_texture(real frequency, real persistence, int octave)
    {
        frequency_ = frequency;
        persistence_ = persistence;
        octave_ = octave;
    }

    real fBm_base_noise_texture::get_value(real u, real v, real w) const
    {
        real val = fBm(u, v, w, frequency_, persistence_, octave_);
        return 0.5 * (val) + 0.5;
    }
}