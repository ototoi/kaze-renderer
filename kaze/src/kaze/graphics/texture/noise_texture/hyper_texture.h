#ifndef KAZE_HYPER_TEXTURE_H
#define KAZE_HYPER_TEXTURE_H

#include "types.h"
#include <algorithm>
#include <vector>

namespace kaze
{

    class hyper_texture
    {
    public:
        static real noise(real u, real v, real w);
        static real turb(real u, real v, real w, real frequency, real persistence, int octave = 8);
        static real fBm(real u, real v, real w, real frequency, real persistence, int octave = 8);
        static real windy(real u, real v, real w, real frequency, real persistence, int octave = 8);
        static real grid(real u, real v, real w, real frequency = 8);
    };
}

#endif