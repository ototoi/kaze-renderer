#ifndef KAZE_VORONOI_TEXTURE_H
#define KAZE_VORONOI_TEXTURE_H

#include "texture.hpp"
#include "base_voronoi_texture.h"

namespace kaze
{

    template <class T>
    class voronoi_texture : public texture<T>
    {
    public:
        voronoi_texture(const auto_count_ptr<texture<T> >& tex) : tex_(tex) {}

        T get(const sufflight& suf) const
        {
            return v_.get(suf) * tex_->get(suf);
        }

    protected:
        base_voronoi_texture v_;
        std::shared_ptr<texture<T> > tex_;
    };

    template <>
    class voronoi_texture<real> : public base_voronoi_texture
    {
    };
}

#endif