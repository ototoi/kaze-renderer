#ifndef KAZE_BASE_SPOTS_TEXTURE_H
#define KAZE_BASE_SPOTS_TEXTURE_H

#include "base_noise_texture.h"
#include "count_ptr.hpp"
#include "texture_clipper.h"

namespace kaze
{

    class base_spots_texture : public texture<bool>
    {
    public:
        base_spots_texture(real grid = 16, real rad = 0.35, real shift = 0.25);
        base_spots_texture(const auto_count_ptr<texture_clipper>& clp, real grid = 16, real rad = 0.35, real shift = 0.25);
        bool get(const sufflight& suf) const;

        bool get_value(real u, real v, real w) const;

    private:
        base_noise_texture nz_;
        real gsz_;
        real r_;
        real shift_;
        std::shared_ptr<texture_clipper> clp_;
    };
}

#endif