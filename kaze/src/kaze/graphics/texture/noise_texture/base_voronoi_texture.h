#ifndef KAZE_BASE_VORONOI_TEXTURE_H
#define KAZE_BASE_VORONOI_TEXTURE_H

#include "texture.hpp"

namespace kaze
{

    class base_voronoi_texture_imp;
    class base_voronoi_texture : public texture<real>
    {
    public:
        enum
        {
            VORONOI_F1,
            VORONOI_F2,
            VORONOI_F3,
            VORONOI_F4,
            VORONOI_F1F2,
            VORONOI_CRACK
        };

    public:
        base_voronoi_texture(int nType = 0);
        ~base_voronoi_texture();
        real get(const sufflight& suf) const;

    protected:
        real get_value(real x, real y, real z) const;

    private:
        base_voronoi_texture_imp* imp_;
    };
}

#endif