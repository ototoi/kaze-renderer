#include "grid_base_noise_texture.h"

namespace kaze
{

    grid_base_noise_texture::grid_base_noise_texture(real freq)
        : freq_(freq)
    {
    }

    real grid_base_noise_texture::get_value(real u, real v, real w) const
    {
        real freq = freq_;
        return 0.5 * grid(u, v, w, freq) + 0.5;
    }
}
