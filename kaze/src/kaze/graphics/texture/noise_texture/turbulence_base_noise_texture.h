#ifndef KAZE_TURBULENCE_BASE_NOISE_TEXTURE_H
#define KAZE_TURBULENCE_BASE_NOISE_TEXTURE_H

#include "texture.hpp"
#include "base_noise_texture.h"

namespace kaze
{

    class turbulence_base_noise_texture : public base_noise_texture
    {
    public:
        turbulence_base_noise_texture();
        turbulence_base_noise_texture(real frequency, real persistence, int octave = 8);

        virtual real get_value(real u, real v, real w) const;

    protected:
        real frequency_;
        real persistence_;
        int octave_;
    };
}

#endif