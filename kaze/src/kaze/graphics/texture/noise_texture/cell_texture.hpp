#ifndef KAZE_CELL_TEXTURE_HPP
#define KAZE_CELL_TEXTURE_HPP

#include "texture.hpp"
#include "base_cell_texture.h"

namespace kaze
{

    template <class T>
    class cell_texture : public texture<T>
    {
    public:
        T get(const sufflight& suf) const
        {
            vector3 c = suf.coord();
            return get_value(freq * c[0], freq * c[1], freq * c[2]);
        }

    protected:
        T get_value(real u, real v, real w) const
        {
            return T() * nz_.get_value(u, v, w); //
        }

    private:
        real freq_;
        base_cell_texture nz_;
    };
}

#endif
