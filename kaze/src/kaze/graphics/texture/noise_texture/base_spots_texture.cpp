#include "base_spots_texture.h"
#include "count_ptr.hpp"

namespace kaze
{
    namespace
    {
        class s_spot_texture_clipper : public texture_clipper
        {
        public:
            s_spot_texture_clipper() {}
        public:
            bool get(real u, real v) const
            {
                real du = u - 0.5;
                real dv = v - 0.5;
                return (du * du + dv * dv) < 0.25; //0.5*0.5
            }
            vector2 min() const { return vector2(0, 0); }
            vector2 max() const { return vector2(1, 1); }
        };
    }

    base_spots_texture::base_spots_texture(real grid, real rad, real shift)
        : gsz_(grid), clp_(std::shared_ptr<texture_clipper>(new s_spot_texture_clipper()))
    {
        r_ = std::max<real>(0.00001, std::min<real>(rad, 0.5));
        shift_ = shift;
    }

    base_spots_texture::base_spots_texture(const auto_count_ptr<texture_clipper>& clp, real grid, real rad, real shift)
        : gsz_(grid), clp_(clp)
    {
        r_ = std::max<real>(0.00001, std::min<real>(rad, 0.5));
        shift_ = shift;
    }

    bool base_spots_texture::get(const sufflight& suf) const
    {
        vector3 c = suf.coord();
        return get_value(gsz_ * c[0], gsz_ * c[1], gsz_ * c[2]);
    }

    static int f2i(real x)
    {
        return (int)floor(x);
    }

    bool base_spots_texture::get_value(real u, real v, real w) const
    {
        int nu = f2i(u + 0.5);
        int nv = f2i(v + 0.5);
        if (nz_.noise(nu + 0.5, nv + 0.5, 0) > shift_)
        {
            //real max_shift = std::max<real>(0,0.5-r_);
            real max_shift = (0.5 - r_);

            real cu = nu + max_shift * nz_.noise(nu + 1.5, nv + 2.8, 0);
            real cv = nv + max_shift * nz_.noise(nu + 4.5, nv + 9.8, 0);

            real du = (u - cu) / (r_ * 2);
            real dv = (v - cv) / (r_ * 2);

            real uu = du + 0.5;
            real vv = dv + 0.5;

            if (clp_->get(uu, vv))
            {
                return true;
            }
        }

        return false;
    }
}