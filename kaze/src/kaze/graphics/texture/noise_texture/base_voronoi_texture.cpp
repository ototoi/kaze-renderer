#include "base_voronoi_texture.h"

#include "voronoi_functions.h"

namespace kaze
{

    class base_voronoi_texture_imp
    {
    public:
        virtual ~base_voronoi_texture_imp() {}
        virtual real get(real x, real y, real z) const = 0;
    };

#define DEF_VORONOI(F)                                                \
    class F##_imp : public base_voronoi_texture_imp                   \
    {                                                                 \
    public:                                                           \
        real get(real x, real y, real z) const { return F(x, y, z); } \
    };

    namespace
    {
        DEF_VORONOI(voronoi_F1)
        DEF_VORONOI(voronoi_F2)
        DEF_VORONOI(voronoi_F3)
        DEF_VORONOI(voronoi_F4)
        DEF_VORONOI(voronoi_F1F2)
        DEF_VORONOI(voronoi_Cr)
    }

#undef DEF_VORONOI

    base_voronoi_texture::base_voronoi_texture(int nType)
    {
        /*
      VORONOI_F1,
      VORONOI_F2,
      VORONOI_F3,
      VORONOI_F1F2,
      VORONOI_CRACK
    */
        switch (nType)
        {
        case VORONOI_F1:
            imp_ = new voronoi_F1_imp();
            break;
        case VORONOI_F2:
            imp_ = new voronoi_F2_imp();
            break;
        case VORONOI_F3:
            imp_ = new voronoi_F3_imp();
            break;
        case VORONOI_F4:
            imp_ = new voronoi_F4_imp();
            break;
        case VORONOI_F1F2:
            imp_ = new voronoi_F1F2_imp();
            break;
        case VORONOI_CRACK:
            imp_ = new voronoi_Cr_imp();
            break;
        default:
            imp_ = new voronoi_F1_imp();
            break;
        }
    }

    base_voronoi_texture::~base_voronoi_texture()
    {
        delete imp_;
    }

    real base_voronoi_texture::get(const sufflight& suf) const
    {
        real f = 16;
        vector3 c = suf.stw();
        return get_value(f * c[0], f * c[1], f * c[2]);
    }

    real base_voronoi_texture::get_value(real x, real y, real z) const
    {
        return imp_->get(x, y, z);
    }
}