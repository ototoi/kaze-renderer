#include "turbulence_base_noise_texture.h"

#include "hyper_texture.h"

namespace kaze
{

    turbulence_base_noise_texture::turbulence_base_noise_texture()
    {
        frequency_ = 16;
        persistence_ = 0.5;
        octave_ = 8;
    }

    turbulence_base_noise_texture::turbulence_base_noise_texture(real frequency, real persistence, int octave)
    {
        frequency_ = frequency;
        persistence_ = persistence;
        octave_ = octave;
    }

    real turbulence_base_noise_texture::get_value(real u, real v, real w) const
    {
        real val = turb(u, v, w, frequency_, persistence_, octave_);
        return val;
    }
}