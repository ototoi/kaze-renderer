#include "perlin_base_noise_texture.h"

#include "hyper_texture.h"

namespace kaze
{

    perlin_base_noise_texture::perlin_base_noise_texture()
    {
        frequency_ = 16;
        persistence_ = 0.5;
    }

    perlin_base_noise_texture::perlin_base_noise_texture(real frequency, real persistence)
    {
        frequency_ = frequency;
        persistence_ = persistence;
    }

    real perlin_base_noise_texture::get_value(real u, real v, real w) const
    {
        real val = 0.5 * noise(u * frequency_, v * frequency_, w * frequency_) + 0.5;
        return val;
    }
}