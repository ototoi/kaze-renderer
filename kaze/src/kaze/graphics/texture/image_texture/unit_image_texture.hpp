#ifndef KAZE_UNIT_IMAGE_TEXTURE_HPP
#define KAZE_UNIT_IMAGE_TEXTURE_HPP

#include "image_texture.hpp"
#include "image_interpolator.hpp"

#include "count_ptr.hpp"

namespace kaze
{

    template <class T>
    class unit_image_texture : public image_texture<T>
    {
    protected:
        void init()
        {
            if (!this->is_powerof2(img_->get_width()))
            {
                corx_.reset(corx_->clone_general());
            }
            if (!this->is_powerof2(img_->get_height()))
            {
                cory_.reset(cory_->clone_general());
            }
        }
        vector2 convert(const vector3& v) const
        {
            vector3 xyz = tr_->transform_p(v);
            return vector2(xyz[0], xyz[1]);
        }

    public:
        unit_image_texture(
            const auto_count_ptr<image<T> >& img,
            const auto_count_ptr<corresponder>& corx = image_texture<T>::default_corresponder(),
            const auto_count_ptr<corresponder>& cory = image_texture<T>::default_corresponder(),
            const auto_count_ptr<image_interpolator<T> >& inp = image_texture<T>::default_image_interpolator(),
            const auto_count_ptr<transformer>& tr = image_texture<T>::default_transformer()) : img_(img), corx_(corx), cory_(cory), inp_(inp), tr_(tr)
        {
            init();
        }
        ~unit_image_texture() {}

        T get(const sufflight& suf) const
        {
            vector2 xy = convert(suf.get_stw());
            return inp_->get(*img_, *corx_, *cory_, xy[0], xy[1]);
        }

        T get(real x, real y) const
        {
            return inp_->get(*img_, *corx_, *cory_, x, y);
        }

    private:
        std::shared_ptr<image<T> > img_;
        std::shared_ptr<corresponder> corx_;
        std::shared_ptr<corresponder> cory_;
        std::shared_ptr<image_interpolator<T> > inp_;
        std::shared_ptr<transformer> tr_;
    };
}

#endif
