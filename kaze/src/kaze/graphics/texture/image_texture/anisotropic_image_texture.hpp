#ifndef KAZE_ANISOTROPIC_IMAGE_TEXTURE_HPP
#define KAZE_ANISOTROPIC_IMAGE_TEXTURE_HPP

#include "texture.hpp"
#include "mipmap_image.hpp"
#include "corresponder.h"
#include "image_interpolator.hpp"

#include "image_texture.hpp"

#include "filter.h"

#include "count_ptr.hpp"

#include <vector>
#include <cmath>

/*
 *
 * Anisotropic Multi Sample Image Texture
 *
 */

namespace kaze
{

    template <class T>
    class anisotropic_image_texture : public image_texture<T>
    {
    public:
        anisotropic_image_texture(
            const auto_count_ptr<mipmap_image<T> >& mip,
            const auto_count_ptr<corresponder>& cor = image_texture<T>::default_corresponder(),
            const auto_count_ptr<image_interpolator<T> >& inp = image_texture<T>::default_image_interpolator(),
            const auto_count_ptr<transformer>& tr = image_texture<T>::default_transformer(),
            int sample = 4) : mip_(mip), cor_(cor), inp_(inp), tr_(tr), sample_(sample)
        {
            initialize();
        }
        ~anisotropic_image_texture()
        {
            destroy();
        }

    protected:
        void initialize()
        {
            try
            {
                int sz = (int)mip_->size();
                vc_.resize(sz);
                for (int i = 0; i < sz; i++)
                {
                    vc_[i] = new ref_correspond_image<T>(mip_->get_image(i), cor_.get());
                }
            }
            catch (...)
            {
                destroy();
            }
        }

        void destroy()
        {
            size_t sz = vc_.size();
            for (size_t i = 0; i < sz; i++)
            {
                delete vc_[i];
            }
        }

    public:
        T get(const sufflight& suf) const
        {
            vector2 coords[4];
            coords[0] = convert(suf.coord00());
            coords[1] = convert(suf.coord10());
            coords[2] = convert(suf.coord01());
            coords[3] = convert(suf.coord11());
            return pick_with_box(coords);
        }

    protected:
        int get_mex_level() const
        {
            return (int)mip_->size() - 1;
        }

    protected:
        struct color_set
        {
            int n; //count of
            T c;   //color
        };

        T pick_with_box(
            const vector2 coords[4]) const
        {
            static const real FAR = values::far();
            int sample = sample_;

            vector2 min, max;
            min = max = coords[0];
            for (int i = 1; i < 4; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (min[j] > coords[i][j]) min[j] = coords[i][j];
                    if (max[j] < coords[i][j]) max[j] = coords[i][j];
                }
            }
            vector2 wid = max - min;
            real w = std::max<real>(wid[0], wid[1]);
            //vector2 cnt = (cmax+cmin)*real(0.5);
            //real w2 = real(0.5)*w;

            //cmin = cnt - vector2(w2,w2);
            //cmax = cnt + vector2(w2,w2);

            w /= sample;
            w *= 2;

            real Lv = -log2(w);
            Lv = std::max<real>(0, Lv);
            int n = (int)floor(Lv);

            int max_level = get_mex_level();
            if (n >= max_level)
            {
                return pick_sample(max_level, sample, min, max, coords);
            }
            else
            {
                real a = Lv - n;
                return (1 - a) * pick_sample(n, sample, min, max, coords) + a * pick_sample(n + 1, sample, min, max, coords);
            }
        }

        T pick_sample(
            int level,
            int smp,
            const vector2& cmin, const vector2& cmax,
            const vector2 coords[4]) const
        {
            vector2 dif = (cmax - cmin) * (real(1) / smp);

            color_set cs;
            cs.c = image_texture<T>::zero_color();
            cs.n = 0;

            for (int y = 0; y < smp; y++)
            {
                real dy = dif[1] * (y + 0.5);
                for (int x = 0; x < smp; x++)
                {
                    real dx = dif[0] * (x + 0.5);
                    vector2 c = cmin + vector2(dx, dy);
                    pick_sub(&cs, level, c, coords);
                }
            }

            if (cs.n < 1)
            {
                vector2 c = real(0.25) * (coords[0] + coords[1] + coords[2] + coords[3]);
                real u = c[0];
                real v = c[1];
                return pick_level(u, v, level);
            }
            else
            {
                return cs.c * (real(1) / cs.n);
            }
        }

        void pick_sub(
            color_set* cs,
            int level,
            const vector2& c,
            const vector2 coords[4]) const
        {
            if (is_contain_recrangle_point(coords, c))
            {
                real u = c[0];
                real v = c[1];
                cs->n++;
                cs->c += pick_level(u, v, level);
            }
        }

        T pick_level(real u, real v, int lv) const
        {
            return inp_->get(*vc_[lv], u, v);
        }

    protected:
        vector2 convert(const vector3& v) const
        {
            vector3 xyz = tr_->transform_p(v);
            return vector2(xyz[0], xyz[1]);
        }
        static real min4(real a, real b, real c, real d)
        {
            return std::min<real>(std::min<real>(a, b), std::min<real>(c, d));
        }
        static real max4(real a, real b, real c, real d)
        {
            return std::max<real>(std::max<real>(a, b), std::max<real>(c, d));
        }
        static inline real h_cross(const vector2& v1, const vector2& v2)
        {
            real a = v1[0];
            real b = v1[1];
            real c = v2[0];
            real d = v2[1];
            return a * d - b * c;
        }
        static inline bool sgn3(real a, real b, real c)
        {
            if (a < 0)
            {
                if (b < 0 && c < 0) return true;
            }
            else
            {
                if (b >= 0 && c >= 0) return true;
            }
            return false;
        }

        static inline bool is_contain_triangle_point(
            const vector2& a, const vector2& b, const vector2& c,
            const vector2& p)
        {
            vector2 ap = p - a;
            vector2 bp = p - b;
            vector2 cp = p - c;

            return sgn3(h_cross(ap, -bp), h_cross(bp, -cp), h_cross(cp, -ap));
        }

        static inline bool is_contain_recrangle_point(
            const vector2& a, const vector2& b, const vector2& c, const vector2& d,
            const vector2& p)
        {
            //bool b1 = is_contain_triangle_point(a,b,c,p);
            //bool b2 = is_contain_triangle_point(c,b,d,p);
            return is_contain_triangle_point(a, b, c, p) || is_contain_triangle_point(c, d, b, p);
        }

        static inline bool is_contain_recrangle_point(
            const vector2 v[4],
            const vector2& p)
        {
            return is_contain_recrangle_point(v[0], v[1], v[2], v[3], p);
        }

    protected:
        std::shared_ptr<mipmap_image<T> > mip_;
        std::shared_ptr<corresponder> cor_;
        std::shared_ptr<image_interpolator<T> > inp_;
        std::shared_ptr<transformer> tr_;
        std::vector<ref_correspond_image<T>*> vc_;
        int sample_;
    };
}

#endif
