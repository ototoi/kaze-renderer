#ifndef KAZE_SUMMAP_IMAGE_TEXTURE_HPP
#define KAZE_SUMMAP_IMAGE_TEXTURE_HPP

#include "image_texture.hpp"
#include "summap_image.hpp"

#include "corresponder.h"
#include "image_interpolator.hpp"

#include "count_ptr.hpp"

#include <vector>

namespace kaze
{

    //SummedAreaTable

    template <class T>
    class summap_image_texture : public image_texture<T>
    {
        struct rng
        {
            int i0, i1;
        };
        struct pix
        {
            int x0, x1, y0, y1;
        };

    protected:
        vector2 convert(const vector3& v) const
        {
            vector3 xyz = tr_->transform_p(v);
            return vector2(xyz[0], xyz[1]);
        }
        void init()
        {
            if (!this->is_powerof2(img_->get_width()) || !this->is_powerof2(img_->get_height()))
            {
                cor_.reset(cor_->clone_general());
            }
        }

    public:
        summap_image_texture(
            const auto_count_ptr<summap_image<T> >& img,
            const auto_count_ptr<corresponder>& cor = image_texture<T>::default_corresponder(),
            const auto_count_ptr<image_interpolator<T> >& inp = image_texture<T>::default_image_interpolator(),
            const auto_count_ptr<transformer>& tr = image_texture<T>::default_transformer()) : img_(img), cor_(cor), inp_(inp), tr_(tr)
        {
            init();
        }

        ~summap_image_texture() { ; }

        T get(const sufflight& suf) const
        {
            vector2 coords[4];
            coords[0] = convert(suf.coord00());
            coords[1] = convert(suf.coord10());
            coords[2] = convert(suf.coord01());
            coords[3] = convert(suf.coord11());

            vector2 min, max;
            min = max = coords[0];
            for (int i = 1; i < 4; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (min[j] > coords[i][j]) min[j] = coords[i][j];
                    if (max[j] < coords[i][j]) max[j] = coords[i][j];
                }
            }

            vector2 cnt = (max - min) * 0.5;
            vector3 cc = suf.get_stw();
            vector2 dif = vector2(cc[0] - cnt[0], cc[1] - cnt[1]);

            min += dif;
            max += dif;

            int w = img_->get_width();
            int h = img_->get_height();

            min[0] = min[0] * w;
            min[1] = min[1] * h;
            max[0] = max[0] * w;
            max[1] = max[1] * h;

            //innner
            int imin[2];
            int imax[2];
            //ounter
            int omin[2];
            int omax[2];

            imin[0] = (int)(ceil(min[0]));
            imin[1] = (int)(ceil(min[1]));
            imax[0] = (int)(floor(max[0]));
            imax[1] = (int)(floor(max[1]));
            omin[0] = (int)(floor(min[0]));
            omin[1] = (int)(floor(min[1]));
            omax[0] = (int)(ceil(max[0]));
            omax[1] = (int)(ceil(max[1]));

            int szx = imax[0] - imin[0];
            int szy = imax[1] - imin[1];
            int sz = 0;

            T tmp = color_traits<T>::zero();
            if (!(szx <= 0 || szy <= 0))
            {
                int csz = szx * szy;
                tmp += real(csz) * pick(imin[0], imin[1], imax[0], imax[1]);
                sz += csz;
            }
            real u0 = min[0] - omin[0];
            real u1 = max[0] - imin[0];
            real v0 = min[1] - omin[1];
            real v1 = max[1] - imin[1];

            if (!(szy <= 0))
            {
                { //left
                    int l0 = (int)floor(u0 - 0.5);
                    real s = min[0] - l0 + real(0.5);
                    int csz = szy;
                    tmp += real(csz) * ((1 - s) * pick(l0, imin[1], l0 + 1, imax[1]) + (s)*pick(l0 + 1, imin[1], l0 + 2, imax[1]));
                    sz += csz;
                }
                { //right
                    int l0 = (int)floor(u1 - 0.5);
                    real s = max[0] - l0 + real(0.5);
                    int csz = szy;
                    tmp += real(csz) * ((1 - s) * pick(l0, imin[1], l0 + 1, imax[1]) + (s)*pick(l0 + 1, imin[1], l0 + 2, imax[1]));
                    sz += csz;
                }
            }
            if (!(szx <= 0))
            {
                { //top
                    int l0 = (int)floor(v0 - 0.5);
                    real s = min[1] - l0 + real(0.5);
                    int csz = szx;
                    tmp += real(csz) * ((1 - s) * pick(imin[0], l0, imax[0], l0 + 1) + (s)*pick(imin[0], l0 + 1, imax[0], l0 + 2));
                    sz += csz;
                }
                { //bottom
                    int l0 = (int)floor(v1 - 0.5);
                    real s = max[1] - l0 + real(0.5);
                    int csz = szx;
                    tmp += real(csz) * ((1 - s) * pick(imin[0], l0, imax[0], l0 + 1) + (s)*pick(imin[0], l0 + 1, imax[0], l0 + 2));
                    sz += csz;
                }
            }

            { //4 points
                tmp += pick_point(u0, v0, omin[0], omin[1]);
                tmp += pick_point(u1, v0, imin[0], omin[1]);
                tmp += pick_point(u0, v1, omin[0], imin[1]);
                tmp += pick_point(u1, v1, imin[0], imin[1]);
                sz += 4;
            }

            return (real(1) / sz) * tmp;
        }

        T pick_point(real u, real v, int x, int y) const
        {
            int x0 = x;
            int x1 = x + 1;
            int y0 = y;
            int y1 = y + 1;
            real s = u;
            real t = v;
            if (u < 0.5)
            {
                x0 = x - 1;
                x1 = x;
                s = u + 0.5;
            }
            else
            {
                x0 = x;
                x1 = x + 1;
                s = u - 0.5;
            }

            if (v < 0.5)
            {
                y0 = y - 1;
                y1 = y;
                t = v + 0.5;
            }
            else
            {
                y0 = y;
                y1 = y + 1;
                t = v - 0.5;
            }

            return (1 - t) * ((1 - s) * img_->get(x0, y0) + (s)*img_->get(x1, y0)) +
                   (t) * ((1 - s) * img_->get(x0, y1) + (s)*img_->get(x1, y1));
        }

        static void getdiv(std::vector<rng>& ranges, int x0, int x1, int w)
        {
            std::vector<int> v;
            int s = w * (int)floor(double(x0) / w);
            int e = w * (int)ceil(double(x1) / w);
            v.push_back(x0);
            for (int i = s; i < e; i += w)
            {
                if (x0 < i && i < x1)
                {
                    v.push_back(i);
                }
            }
            v.push_back(x1);
            int sz = (int)v.size();
            for (int i = 1; i < sz; i++)
            {
                rng r = {v[i - 1], v[i]};
                ranges.push_back(r);
            }
        }

        static void getsub(std::vector<pix>& pixes, int x0, int y0, int x1, int y1, int w, int h)
        {
            std::vector<rng> xranges;
            std::vector<rng> yranges;
            getdiv(xranges, x0, x1, w);
            getdiv(yranges, y0, y1, h);
            size_t xsz = xranges.size();
            size_t ysz = yranges.size();
            for (size_t y = 0; y < ysz; y++)
            {
                int cy0 = yranges[y].i0;
                int cy1 = yranges[y].i1;
                for (size_t x = 0; x < xsz; x++)
                {
                    int cx0 = xranges[x].i0;
                    int cx1 = xranges[x].i1;
                    pix px = {cx0, cy0, cx1, cy1};
                    pixes.push_back(px);
                }
            }
        }

        T pick(int x0, int y0, int x1, int y1) const
        {
            int w = img_->get_width();
            int h = img_->get_height();

            if (0 <= x0 && x1 <= w && 0 <= y0 && y1 <= h)
            {
                return pick_final(x0, y0, x1, y1);
            }
            else
            {
                double total = 1.0 / ((x1 - x0) * (y1 - y0));
                std::vector<pix> pixes;
                getsub(pixes, x0, y0, x1, y1, w, h);
                T tmp = color_traits<T>::zero();
                size_t sz = pixes.size();
                for (size_t i = 0; i < sz; i++)
                {
                    int x0 = pixes[i].x0;
                    int y0 = pixes[i].y0;
                    int x1 = pixes[i].x1;
                    int y1 = pixes[i].y1;
                    x0 = cor_->get(x0, w);
                    y0 = cor_->get(y0, h);
                    x1 = cor_->get(x1 - 1, w);
                    y1 = cor_->get(y1 - 1, h);
                    if (x0 > x1) std::swap(x0, x1);
                    if (y0 > y1) std::swap(y0, y1);
                    x1++;
                    y1++;
                    int dx = x1 - x0;
                    int dy = y1 - y0;
                    tmp += (T)(((dx * dy) * total) * pick_final(x0, y0, x1, y1));
                }
                return tmp;
            }
        }

        T pick_final(int x0, int y0, int x1, int y1) const
        {
            return img_->get_avg(x0, y0, x1, y1);
        }

    protected:
        std::shared_ptr<summap_image<T> > img_;
        std::shared_ptr<image_interpolator<T> > inp_;
        std::shared_ptr<corresponder> cor_;
        std::shared_ptr<transformer> tr_;
    };
}

#endif
