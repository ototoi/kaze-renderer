#ifndef KAZE_IMAGE_XEXXURE_HPP
#define KAZE_IMAGE_XEXXURE_HPP

#include "image.hpp"
#include "texture.hpp"
#include "corresponder.h"
#include "readonly_image.hpp"
#include "transformer.h"

#include "color_traits.hpp"

#include <cmath>

namespace kaze
{

    template <class X>
    class ref_correspond_image : public readonly_image<X>
    {
    public:
        ref_correspond_image(const image<X>* img, const corresponder* cor) : img_(img), corx_(cor), cory_(cor) {}
        ref_correspond_image(const image<X>* img, const corresponder* corx, const corresponder* cory) : img_(img), corx_(corx), cory_(cory) {}
        //~correspond_image(){/*don't doing*/}
    public:
        X get(int x, int y) const
        {
            x = corx_->get(x, get_width());
            y = cory_->get(y, get_height());
            return img_->get(x, y);
        }

        int get_width() const { return img_->get_width(); }
        int get_height() const { return img_->get_height(); }
    private:
        const image<X>* img_;
        const corresponder* corx_;
        const corresponder* cory_;
    };

    template <class T>
    class image_texture : public texture<T>
    {
    public:
        static T zero_color()
        {
            return color_traits<T>::zero();
        }
        static inline bool is_powerof2(int x)
        {
            return ((x - 1) & x) == 0;
        }
        static double log2(double x)
        {
            using namespace std;
            static const double iLOG2 = 1.0 / log(2.0);
            return log(x) * iLOG2;
        }

        class coord_to_image_transformer : public transformer
        {
        public:
            vector3 transform_n(const vector3& v) const { return tr_(v); }
            vector3 transform_p(const vector3& v) const { return tr_(v); }
            vector3 transform_v(const vector3& v) const { return tr_(v); }
        protected:
            inline vector3 tr_(const vector3& v) const { return vector3(v[0], v[1], v[2]); }
        };

        static image_interpolator<T>* default_image_interpolator() { return new mitchell_image_interpolator<T>(); }
        static corresponder* default_corresponder() { return new powerof2_repeat_corresponder(); }
        static transformer* default_transformer() { return new coord_to_image_transformer(); }
    };
}

#endif
