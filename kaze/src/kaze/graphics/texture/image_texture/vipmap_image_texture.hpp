#ifndef KAZE_VIPMAP_IMAGE_TEXTURE_HPP
#define KAZE_VIPMAP_IMAGE_TEXTURE_HPP

#include "texture.hpp"
#include "mipmap_image.hpp"
#include "corresponder.h"
#include "image_interpolator.hpp"

#include "readonly_image.hpp"

#include "image_texture.hpp"

#include <vector>
#include <cmath>

namespace kaze
{

    /*
	 * On the fly - Convoluted Image Texture
	 * vipmap_image_texture 
	 * 
	 */

    template <class T>
    class vipmap_image_texture : public image_texture<T>
    {
    public:
        vipmap_image_texture(
            const auto_count_ptr<mipmap_image<T> >& mip,
            const auto_count_ptr<corresponder>& cor = image_texture<T>::default_corresponder(),
            const auto_count_ptr<image_interpolator<T> >& inp = image_texture<T>::default_image_interpolator(),
            const auto_count_ptr<transformer>& tr = image_texture<T>::default_transformer()) : mip_(mip), corx_(cor), cory_(cor), inp_(inp), tr_(tr)
        {
            initialize();
        }
        vipmap_image_texture(
            const auto_count_ptr<mipmap_image<T> >& mip,
            const auto_count_ptr<corresponder>& corx = image_texture<T>::default_corresponder(),
            const auto_count_ptr<corresponder>& cory = image_texture<T>::default_corresponder(),
            const auto_count_ptr<image_interpolator<T> >& inp = image_texture<T>::default_image_interpolator(),
            const auto_count_ptr<transformer>& tr = image_texture<T>::default_transformer()) : mip_(mip), corx_(corx), cory_(cory), inp_(inp), tr_(tr)
        {
            initialize();
        }
        ~vipmap_image_texture()
        {
            ;
        }

        void initialize()
        {
            if (!this->is_powerof2(mip_->get_width()))
            {
                corx_.reset(corx_->clone_general());
            }
            if (!this->is_powerof2(mip_->get_height()))
            {
                cory_.reset(cory_->clone_general());
            }
        }

    public:
        T get(const sufflight& suf) const;

    protected:
        T pick(real u, real v, real wx, real wy) const;
        T pick_wx_wy_expand(real u, real v, real wx, real wy) const;
        T pick_wx_wy(real u, real v, real wx, real wy) const;
        T pick_wx_ly(real u, real v, real wx, int ly) const;
        T pick_lx_ly(real u, real v, int lx, int ly) const;

    protected:
        vector2 convert(const vector3& v) const
        {
            vector3 xyz = tr_->transform_p(v);
            return vector2(xyz[0], xyz[1]);
        }

        class convoluted_image : public readonly_image<T>
        {
        public:
            convoluted_image(
                const mipmap_image<T>* img,
                const corresponder* corx,
                const corresponder* cory,
                real u, real v,
                int lx, int ly)
            {
                int w = img->get_width();
                int h = img->get_height();

                //assert(w == h);//mipmap is width equal height!
                assert(lx >= 0);
                assert(ly >= 0);

                int vw = 1 << lx; // virtual ripmap width
                int vh = 1 << ly; // virtual ripmap height

                assert(w >= vw);
                assert(h >= vh);

                real rx = u * vw;
                real ry = v * vh;

                int ix = (int)floor(rx - 0.5);
                int iy = (int)floor(ry - 0.5);

                int dx = ix - 1;
                int dy = iy - 1;

                //--------------------------------
                int levels[] = {lx, ly};
                int sample_layer = (lx < ly) ? 0 : 1; //���x�����������ق��������傫���̂ŃT���v��
                int base_layer = 1 - sample_layer;    //�
                //--------------------------------
                int samples[2];
                int diff = levels[base_layer] - levels[sample_layer];
                assert(diff >= 0);
                samples[sample_layer] = 1 << diff;
                samples[base_layer] = 1;
                //--------------------------------
                int base_level = levels[base_layer];
                //int base_w     = 1<<base_level;
                //int base_h     = 1<<base_level;
                //--------------------------------
                const image<T>* pimg = img->get_image(base_level);
                int base_w = pimg->get_width();
                int base_h = pimg->get_height();
                //--------------------------------
                int x0[4];
                int x1[4];
                int y0[4];
                int y1[4];
                for (int i = 0; i < 4; i++)
                {
                    int x = i + dx;
                    x0[i] = x * samples[0];
                    x1[i] = x0[i] + samples[0];
                    int y = i + dy;
                    y0[i] = y * samples[1];
                    y1[i] = y0[i] + samples[1];
                }

                for (int j = 0; j < 4; j++)
                {
                    for (int i = 0; i < 4; i++)
                    {
                        T tmp = zero_color();
                        for (int yy = y0[j]; yy < y1[j]; yy++)
                        {
                            int yyy = cory->get(yy, base_h);
                            for (int xx = x0[i]; xx < x1[i]; xx++)
                            {
                                int xxx = corx->get(xx, base_w);
                                tmp += pimg->get(xxx, yyy);
                            }
                        }
                        tmp *= real(1.0 / (samples[0] * samples[1]));

                        mv_[j * 4 + i] = tmp;
                    }
                }

                //---------------
                dx_ = dx;
                dy_ = dy;
                //---------------
                w_ = vw;
                h_ = vh;
            }

            T get(int x, int y) const
            {
                int i, j;

                i = x - dx_;
                j = y - dy_;

                assert(!(i < 0 || 4 <= i));
                assert(!(j < 0 || 4 <= j));

                return mv_[j * 4 + i];
            }

            int get_width() const { return w_; }
            int get_height() const { return h_; }
        public:
            static T zero_color() { return image_texture<T>::zero_color(); }

        protected:
            T mv_[4 * 4];
            int dx_;
            int dy_;
            int w_;
            int h_;
        };

    protected:
        std::shared_ptr<mipmap_image<T> > mip_;
        std::shared_ptr<corresponder> corx_;
        std::shared_ptr<corresponder> cory_;
        std::shared_ptr<image_interpolator<T> > inp_;
        std::shared_ptr<transformer> tr_;
    };

    //----------------------------------------------------------------------------------
    template <class T>
    T vipmap_image_texture<T>::get(const sufflight& suf) const
    {
        using namespace std;

        vector2 coords[4];
        coords[0] = convert(suf.coord00());
        coords[1] = convert(suf.coord10());
        coords[2] = convert(suf.coord01());
        coords[3] = convert(suf.coord11());

        vector2 min, max;
        min = max = coords[0];
        for (int i = 1; i < 4; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                if (min[j] > coords[i][j]) min[j] = coords[i][j];
                if (max[j] < coords[i][j]) max[j] = coords[i][j];
            }
        }

        real ww = max[0] - min[0];
        real wh = max[1] - min[1];

        vector3 c = suf.get_stw();
        real u = c[0];
        real v = c[1];

        return pick(u, v, ww, wh);
    }

    //----------------------------------------------------------------------------------

    template <class T>
    T vipmap_image_texture<T>::pick(real u, real v, real wx, real wy) const
    {
#if 0
		return pick_wx_wy(u, v, wx, wy);
#else
        return pick_wx_wy_expand(u, v, wx, wy);
#endif
    }

    //----------------------------------------------------------------------------------
    template <class T>
    T vipmap_image_texture<T>::pick_wx_wy_expand(real u, real v, real wx, real wy) const
    {
        int max_level = (int)mip_->size() - 1;
        int max_size = 1 << max_level;

        if (wy >= 1)
        {
            if (wx >= 1) return pick_lx_ly(u, v, 0, 0);
            if (wx * max_size <= 1) return pick_lx_ly(u, v, max_level, 0);

            real Lv = -log2(wx);
            int n = (int)Lv;
            real a = Lv - n;
            return (1 - a) * pick_lx_ly(u, v, n, 0) + a * pick_lx_ly(u, v, n + 1, 0);
        }
        else if (wy * max_size <= 1)
        {
            if (wx >= 1) return pick_lx_ly(u, v, 0, max_level);
            if (wx * max_size <= 1) return pick_lx_ly(u, v, max_level, max_level);

            real Lv = -log2(wx);
            int n = (int)Lv;
            real a = Lv - n;
            return (1 - a) * pick_lx_ly(u, v, n, max_level) + a * pick_lx_ly(u, v, n + 1, max_level);
        }
        else
        {
            real Lvy = -log2(wy);
            int ny = (int)Lvy;
            real ay = Lvy - ny;

            if (wx >= 1) return (1 - ay) * pick_lx_ly(u, v, 0, ny) + (ay)*pick_lx_ly(u, v, 0, ny + 1);
            if (wx * max_size <= 1) return (1 - ay) * pick_lx_ly(u, v, max_level, ny) + (ay)*pick_lx_ly(u, v, max_level, ny + 1);

            real Lvx = -log2(wx);
            int nx = (int)Lvx;
            real ax = Lvx - nx;

            return (1 - ay) * ((1 - ax) * pick_lx_ly(u, v, nx, ny) + (ax)*pick_lx_ly(u, v, nx + 1, ny)) + (ay) * ((1 - ax) * pick_lx_ly(u, v, nx, ny + 1) + (ax)*pick_lx_ly(u, v, nx + 1, ny + 1));
        }
    }

    //----------------------------------------------------------------------------------
    template <class T>
    T vipmap_image_texture<T>::pick_wx_wy(real u, real v, real wx, real wy) const
    {
        int max_level = mip_->size() - 1;
        int max_size = 1 << max_level;

        if (wy >= 1) return pick_wx_ly(u, v, wx, 0);
        if (wy * max_size <= 1) return pick_wx_ly(u, v, wx, max_level);

        real Lv = -log2(wy);
        int n = (int)Lv;
        real a = Lv - n;

        return (1 - a) * pick_wx_ly(u, v, wx, n) + a * pick_wx_ly(u, v, wx, n + 1);
    }

    template <class T>
    T vipmap_image_texture<T>::pick_wx_ly(real u, real v, real wx, int ly) const
    {
        int max_level = mip_->size() - 1;
        int max_size = 1 << max_level;

        if (wx >= 1) return pick_lx_ly(u, v, 0, ly);
        if (wx * max_size <= 1) return pick_lx_ly(u, v, max_level, ly);

        real Lv = -log2(wx);
        int n = (int)Lv;
        real a = Lv - n;
        return (1 - a) * pick_lx_ly(u, v, n, ly) + a * pick_lx_ly(u, v, n + 1, ly);
    }

    template <class T>
    T vipmap_image_texture<T>::pick_lx_ly(real u, real v, int lx, int ly) const
    {
        convoluted_image cnv_img(mip_.get(), corx_.get(), cory_.get(), u, v, lx, ly);
        return inp_->get(cnv_img, u, v);
    }
    //----------------------------------------------------------------------------------
}

#endif
