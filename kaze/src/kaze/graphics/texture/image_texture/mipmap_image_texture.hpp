#ifndef KAZE_MIPMAP_IMAGE_TEXTURE_HPP
#define KAZE_MIPMAP_IMAGE_TEXTURE_HPP

#include "image_texture.hpp"
#include "mipmap_image.hpp"
#include "corresponder.h"
#include "image_interpolator.hpp"

#include "count_ptr.hpp"

#include <vector>
#include <cmath>

namespace kaze
{

    template <class T>
    class mipmap_image_texture : public image_texture<T>
    {
    public:
        mipmap_image_texture(
            const auto_count_ptr<mipmap_image<T> >& mip,
            const auto_count_ptr<corresponder>& cor = image_texture<T>::default_corresponder(),
            const auto_count_ptr<image_interpolator<T> >& inp = image_texture<T>::default_image_interpolator(),
            const auto_count_ptr<transformer>& tr = image_texture<T>::default_transformer()) : mip_(mip), corx_(cor), cory_(cor), inp_(inp), tr_(tr)
        {
            initialize();
        }

        mipmap_image_texture(
            const auto_count_ptr<mipmap_image<T> >& mip,
            const auto_count_ptr<corresponder>& corx = image_texture<T>::default_corresponder(),
            const auto_count_ptr<corresponder>& cory = image_texture<T>::default_corresponder(),
            const auto_count_ptr<image_interpolator<T> >& inp = image_texture<T>::default_image_interpolator(),
            const auto_count_ptr<transformer>& tr = image_texture<T>::default_transformer()) : mip_(mip), corx_(corx), cory_(cory), inp_(inp), tr_(tr)
        {
            initialize();
        }

        ~mipmap_image_texture()
        {
            destroy();
        }

    protected:
        void initialize()
        {
            if (!this->is_powerof2(mip_->get_width()))
            {
                corx_.reset(corx_->clone_general());
            }
            if (!this->is_powerof2(mip_->get_height()))
            {
                cory_.reset(cory_->clone_general());
            }
            try
            {
                int sz = (int)mip_->size();
                vc_.reserve(sz);
                for (int i = 0; i < sz; i++)
                {
                    std::unique_ptr<ref_correspond_image<T> > ap(new ref_correspond_image<T>(mip_->get_image(i), corx_.get(), cory_.get()));
                    vc_.push_back(ap.release());
                }
            }
            catch (...)
            {
                destroy();
                throw;
            }
        }

        void destroy()
        {
            size_t sz = vc_.size();
            for (size_t i = 0; i < sz; i++)
            {
                delete vc_[i];
            }
        }

    public:
        T get(const sufflight& suf) const
        {
            vector2 coords[4];
            coords[0] = convert(suf.coord00());
            coords[1] = convert(suf.coord10());
            coords[2] = convert(suf.coord01());
            coords[3] = convert(suf.coord11());

            vector2 min, max;
            min = max = coords[0];
            for (int i = 1; i < 4; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (min[j] > coords[i][j]) min[j] = coords[i][j];
                    if (max[j] < coords[i][j]) max[j] = coords[i][j];
                }
            }

            //real t0 = (coords[0]-coords[3]).sqr_length();
            //real t1 = (coords[1]-coords[2]).sqr_length();

            real w = std::max<real>(max[0] - min[0], max[1] - min[1]);
            //real w = std::sqrt(std::max<real>(t0,t1));

            vector3 c = suf.get_stw();
            //vector2 c = real(0.25)*(coords[0]+coords[1]+coords[2]+coords[3]);

            real u = c[0];
            real v = c[1];

            return pick(u, v, w);
        }

    protected:
        T pick(real u, real v, real w) const
        {
            int max_level = (int)mip_->size() - 1;
            int max_size = 1 << max_level;

            if (w >= 1) return pick_level(u, v, 0);
            if (w * max_size <= 1) return pick_level(u, v, max_level);

            real Lv = -log2(w);
            int n = (int)Lv;
            real a = Lv - n;
            return (1 - a) * pick_level(u, v, n) + a * pick_level(u, v, n + 1);
        }

        T pick_level(real u, real v, int lv) const
        {
            return inp_->get(*vc_[lv], u, v);
        }

    protected:
        vector2 convert(const vector3& v) const
        {
            vector3 xyz = tr_->transform_p(v);
            return vector2(xyz[0], xyz[1]);
        }

    protected:
        std::shared_ptr<mipmap_image<T> > mip_;
        std::shared_ptr<corresponder> corx_;
        std::shared_ptr<corresponder> cory_;
        std::shared_ptr<image_interpolator<T> > inp_;
        std::shared_ptr<transformer> tr_;
        std::vector<ref_correspond_image<T>*> vc_;
    };
}

#endif
