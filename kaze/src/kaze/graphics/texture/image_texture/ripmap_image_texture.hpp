#ifndef KAZE_RIPMAP_IMAGE_TEXTURE_HPP
#define KAZE_RIPMAP_IMAGE_TEXTURE_HPP

#include "texture.hpp"
#include "ripmap_image.hpp"
#include "corresponder.h"
#include "image_interpolator.hpp"

#include "count_ptr.hpp"

#include <vector>
#include <cmath>

namespace kaze
{

    template <class T>
    class ripmap_image_texture : public image_texture<T>
    {
    public:
        ripmap_image_texture(
            const auto_count_ptr<ripmap_image<T> >& rip,
            const auto_count_ptr<corresponder>& cor = image_texture<T>::default_corresponder(),
            const auto_count_ptr<image_interpolator<T> >& inp = image_texture<T>::default_image_interpolator(),
            const auto_count_ptr<transformer>& tr = image_texture<T>::default_transformer()) : rip_(rip), corx_(cor), cory_(cor), inp_(inp), tr_(tr)
        {
            initialize();
        }

        ripmap_image_texture(
            const auto_count_ptr<ripmap_image<T> >& rip,
            const auto_count_ptr<corresponder>& corx = image_texture<T>::default_corresponder(),
            const auto_count_ptr<corresponder>& cory = image_texture<T>::default_corresponder(),
            const auto_count_ptr<image_interpolator<T> >& inp = image_texture<T>::default_image_interpolator(),
            const auto_count_ptr<transformer>& tr = image_texture<T>::default_transformer()) : rip_(rip), corx_(corx), cory_(cory), inp_(inp), tr_(tr)
        {
            initialize();
        }

        ~ripmap_image_texture()
        {
            destroy();
        }

    protected:
        void initialize();
        void destroy();

    public:
        T get(const sufflight& suf) const;

    protected:
        T pick(real u, real v, real wx, real wy) const;
        T pick_wx_wy_expand(real u, real v, real wx, real wy) const;
        T pick_wx_wy(real u, real v, real wx, real wy) const;
        T pick_wx_ly(real u, real v, real wx, int ly) const;
        T pick_lx_ly(real u, real v, int lx, int ly) const;

        const image<T>* get_image(int xLevel, int yLevel) const
        {
            return vc_[yLevel * rip_->size() + xLevel];
        }

    protected:
        vector2 convert(const vector3& v) const
        {
            vector3 xyz = tr_->transform_p(v);
            return vector2(xyz[0], xyz[1]);
        }

    protected:
        std::shared_ptr<ripmap_image<T> > rip_;
        std::shared_ptr<corresponder> corx_;
        std::shared_ptr<corresponder> cory_;
        std::shared_ptr<image_interpolator<T> > inp_;
        std::shared_ptr<transformer> tr_;
        std::vector<ref_correspond_image<T>*> vc_;
    };

    //-------------------------------------------------------------------
    //implement
    //-------------------------------------------------------------------
    template <class T>
    void ripmap_image_texture<T>::initialize()
    {
        if (!this->is_powerof2(rip_->get_width()))
        {
            corx_.reset(corx_->clone_general());
        }
        if (!this->is_powerof2(rip_->get_height()))
        {
            cory_.reset(cory_->clone_general());
        }
        try
        {
            size_t sz = rip_->size();
            vc_.reserve(sz * sz);
            for (size_t j = 0; j < sz; j++)
            {
                for (size_t i = 0; i < sz; i++)
                {
                    std::unique_ptr<ref_correspond_image<T> > ap(new ref_correspond_image<T>(rip_->get_image((int)i, (int)j), corx_.get(), cory_.get()));
                    vc_.push_back(ap.release());
                }
            }
        }
        catch (...)
        {
            destroy();
            throw; //re-throw
        }
    }

    //----------------------------------------------------------------------------------
    template <class T>
    void ripmap_image_texture<T>::destroy()
    {
        size_t sz = vc_.size();
        for (size_t i = 0; i < sz; i++)
        {
            delete vc_[i];
        }
    }

    //----------------------------------------------------------------------------------
    template <class T>
    T ripmap_image_texture<T>::get(const sufflight& suf) const
    {
        vector2 coords[4];
        coords[0] = convert(suf.coord00());
        coords[1] = convert(suf.coord10());
        coords[2] = convert(suf.coord01());
        coords[3] = convert(suf.coord11());

        vector2 min, max;
        min = max = coords[0];
        for (int i = 1; i < 4; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                if (min[j] > coords[i][j]) min[j] = coords[i][j];
                if (max[j] < coords[i][j]) max[j] = coords[i][j];
            }
        }

        real wx = max[0] - min[0];
        real wy = max[1] - min[1];

        vector3 c = suf.get_stw();
        //vector2 c = real(0.25)*(coords[0]+coords[1]+coords[2]+coords[3]);
        real u = c[0];
        real v = c[1];

        return pick(u, v, wx, wy);
    }
    //----------------------------------------------------------------------------------
    template <class T>
    T ripmap_image_texture<T>::pick(real u, real v, real wx, real wy) const
    {
#if 0
		return pick_wx_wy(u, v, wx, wy);
#else
        return pick_wx_wy_expand(u, v, wx, wy);
#endif
    }

    //----------------------------------------------------------------------------------
    template <class T>
    T ripmap_image_texture<T>::pick_wx_wy_expand(real u, real v, real wx, real wy) const
    {
        int max_level = (int)rip_->size() - 1;
        int max_size = 1 << max_level;

        if (wy >= 1)
        {
            if (wx >= 1) return pick_lx_ly(u, v, 0, 0);
            if (wx * max_size <= 1) return pick_lx_ly(u, v, max_level, 0);

            real Lv = -log2(wx);
            int n = (int)Lv;
            real a = Lv - n;
            return (1 - a) * pick_lx_ly(u, v, n, 0) + a * pick_lx_ly(u, v, n + 1, 0);
        }
        else if (wy * max_size <= 1)
        {
            if (wx >= 1) return pick_lx_ly(u, v, 0, max_level);
            if (wx * max_size <= 1) return pick_lx_ly(u, v, max_level, max_level);

            real Lv = -log2(wx);
            int n = (int)Lv;
            real a = Lv - n;
            return (1 - a) * pick_lx_ly(u, v, n, max_level) + a * pick_lx_ly(u, v, n + 1, max_level);
        }
        else
        {
            real Lvy = -log2(wy);
            int ny = (int)Lvy;
            real ay = Lvy - ny;

            if (wx >= 1) return (1 - ay) * pick_lx_ly(u, v, 0, ny) + (ay)*pick_lx_ly(u, v, 0, ny + 1);
            if (wx * max_size <= 1) return (1 - ay) * pick_lx_ly(u, v, max_level, ny) + (ay)*pick_lx_ly(u, v, max_level, ny + 1);

            real Lvx = -log2(wx);
            int nx = (int)Lvx;
            real ax = Lvx - nx;

            return (1 - ay) * ((1 - ax) * pick_lx_ly(u, v, nx, ny) + (ax)*pick_lx_ly(u, v, nx + 1, ny)) + (ay) * ((1 - ax) * pick_lx_ly(u, v, nx, ny + 1) + (ax)*pick_lx_ly(u, v, nx + 1, ny + 1));
        }
    }

    //----------------------------------------------------------------------------------
    template <class T>
    T ripmap_image_texture<T>::pick_wx_wy(real u, real v, real wx, real wy) const
    {
        int max_level = rip_->size() - 1;
        int max_size = 1 << max_level;

        if (wy >= 1) return pick_wx_ly(u, v, wx, 0);
        if (wy * max_size <= 1) return pick_wx_ly(u, v, wx, max_level);

        real Lv = -log2(wy);
        int n = (int)Lv;
        real a = Lv - n;

        return (1 - a) * pick_wx_ly(u, v, wx, n) + a * pick_wx_ly(u, v, wx, n + 1);
    }

    template <class T>
    T ripmap_image_texture<T>::pick_wx_ly(real u, real v, real wx, int ly) const
    {
        int max_level = rip_->size() - 1;
        int max_size = 1 << max_level;

        if (wx >= 1) return pick_lx_ly(u, v, 0, ly);
        if (wx * max_size <= 1) return pick_lx_ly(u, v, max_level, ly);

        real Lv = -log2(wx);
        int n = (int)Lv;
        real a = Lv - n;
        return (1 - a) * pick_lx_ly(u, v, n, ly) + a * pick_lx_ly(u, v, n + 1, ly);
    }

    template <class T>
    T ripmap_image_texture<T>::pick_lx_ly(real u, real v, int lx, int ly) const
    {
        return inp_->get(*get_image(lx, ly), u, v);
    }
    //----------------------------------------------------------------------------------
}

#endif
