#ifndef KAZE_CHECK_TEXTURE_HPP
#define KAZE_CHECK_TEXTURE_HPP

#include "texture.hpp"
#include <cmath>

namespace kaze
{

    template <class T>
    class check_texture : public texture<T>
    {
    public:
        check_texture(const auto_count_ptr<texture<T> >& a, const auto_count_ptr<texture<T> >& b, int nu = 2, int nv = 2, int nw = 2)
            : a_(a), b_(b), nu_(nu), nv_(nv), nw_(nw) {}
    public:
        T get(const sufflight& suf) const
        {
            using namespace std;

            real u = suf.stw()[0];
            real v = suf.stw()[1];
            real w = suf.stw()[2];
            int nu = int(floor(nu_ * u));
            int nv = int(floor(nv_ * v));
            int nw = int(floor(nw_ * w));
            if ((nu + nv + nw) & 1)
            {
                return b_->get(suf);
            }
            else
            {
                return a_->get(suf);
            }
        }

    protected:
        std::shared_ptr<texture<T> > a_;
        std::shared_ptr<texture<T> > b_;
        int nu_;
        int nv_;
        int nw_;
    };
}

#endif
