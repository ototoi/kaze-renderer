#ifndef KAZE_CONSTANT_TEXTURE_HPP
#define KAZE_CONSTANT_TEXTURE_HPP

#include "texture.hpp"

namespace kaze
{

    template <class T>
    class constant_texture : public texture<T>
    {
    public:
        constant_texture(const T& t) : t_(t) {}
    public:
        T get(real u) const
        {
            return t_;
        }
        T get(real u, real v) const
        {
            return t_;
        }
        T get(real u, real v, real w) const
        {
            return t_;
        }
        T get(const sufflight& suf) const
        {
            return t_;
        }

    protected:
        T t_;
    };
}

#endif
