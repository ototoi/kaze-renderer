#ifndef KAZE_LINES_TEXTURE_CLIPPER_H
#define KAZE_LINES_TEXTURE_CLIPPER_H

#include "texture_clipper.h"
#include "lines_loop_segment.h"

#include <vector>

namespace kaze
{

    class lines_texture_clipper : public texture_clipper
    {
    public:
        lines_texture_clipper(const vector2& a, const vector2& b, const vector2& c);
        lines_texture_clipper(const std::vector<vector2>& cp);
        ~lines_texture_clipper();

        bool get(real u, real v) const;

        vector2 min() const { return min_; }
        vector2 max() const { return max_; }
    private:
        std::vector<lines_loop_segment*> segs_;
        vector2 min_;
        vector2 max_;
    };
}

#endif
