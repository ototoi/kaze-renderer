#include "arc_loop_segment.h"

namespace kaze
{

    /*
   *  -rad (|) +rad
   */

    arc_loop_segment::arc_loop_segment(const vector2& a, const vector2& b, real rad)
    {
        int sgn = (rad < 0) ? -1 : 1;
        real phi = 0.5 * fabs(rad);
        real theta = 0.5 * values::pi() - phi;
        vector2 L = b - a;
        vector2 Lyx = vector2(L[1], L[0]);
        vector2 C = 0.5 * (L + sgn * tan(theta) * Lyx);

        real R = length(a - C);

        vector2 CA = normalize(a - C);
        vector2 CB = normalize(b - C);

        int nSgn[2] = {0};
        if (CA[0] < 0) nSgn[0] |= 1;
        if (CB[0] < 0) nSgn[0] |= 2;
        if (CA[1] < 0) nSgn[1] |= 1;
        if (CB[1] < 0) nSgn[1] |= 2;

        vector2 min, max;
        for (int i = 0; i < 2; i++)
        {
            if (a[i] < b[i])
            {
                min[i] = a[i];
                max[i] = b[i];
            }
        }
        if (nSgn[1] == 1 || nSgn[1] == 2)
        { //�c�ɈقȂ�
            if (nSgn[0] & 1)
            {
                min[0] = C[0] - R;
            }
            else
            {
                max[0] = C[0] + R;
            }
        }

        if (nSgn[0] == 1 || nSgn[0] == 2)
        { //���ɈقȂ�
            if (nSgn[1] & 1)
            {
                min[1] = C[1] - R;
            }
            else
            {
                max[1] = C[1] + R;
            }
        }

        r_ = R;
        c_ = C;
        min_ = min;
        max_ = max;
    }

    int arc_loop_segment::clip_u(std::vector<real>& vs, real u) const
    {
        if (min_[0] <= u && u <= max_[0])
        {
            real r = r_;
            real dx = u - c_[0];
            real dy = r * r - dx * dx;
            if (dy <= 0) return 0;
            dy = sqrt(dy);
            real y[2] = {c_[1] - dy, c_[1] + dy};
            int nRet = 0;
            for (int i = 0; i < 2; i++)
            {
                if (min_[1] <= y[i] && y[i] <= max_[1])
                {
                    vs.push_back(y[i]);
                    nRet++;
                }
            }
            return nRet;
        }
        return 0;
    }

    int arc_loop_segment::clip_v(std::vector<real>& us, real v) const
    {
        if (min_[1] <= v && v <= max_[1])
        {
            real r = r_;
            real dy = v - c_[1];
            real dx = r * r - dy * dy;
            if (dx <= 0) return 0;
            dx = sqrt(dx);
            real x[2] = {c_[0] - dx, c_[0] + dx};
            int nRet = 0;
            for (int i = 0; i < 2; i++)
            {
                if (min_[0] <= x[i] && x[i] <= max_[0])
                {
                    us.push_back(x[i]);
                    nRet++;
                }
            }
            return nRet;
        }
        return 0;
    }

    const vector2& arc_loop_segment::min() const
    {
        return min_;
    }

    const vector2& arc_loop_segment::max() const
    {
        return max_;
    }

    size_t arc_loop_segment::size() const
    {
        return 3; //
    }
}
