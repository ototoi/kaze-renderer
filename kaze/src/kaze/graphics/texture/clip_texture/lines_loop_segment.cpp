#include "lines_loop_segment.h"

namespace kaze
{

    static void get_minmax(vector2& min, vector2& max, const std::vector<vector2>& p)
    {
        min = p[0];
        max = p[0];
        size_t sz = p.size();
        for (size_t i = 1; i < sz; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                if (min[j] > p[i][j]) min[j] = p[i][j];
                if (max[j] < p[i][j]) max[j] = p[i][j];
            }
        }
    }

    static bool clip_line_u(real& v, const vector2& p0, const vector2& p1, real u)
    {
        vector2 a = p0;
        vector2 b = p1;
        if (a[0] == b[0]) return false;
        if (a[0] > b[0]) std::swap(a, b);
        //--------------------------
        if (a[0] <= u && u <= b[0])
        {
            real k = (u - a[0]) / (b[0] - a[0]);
            v = (1 - k) * a[1] + k * b[1];
            return true;
        }
        else
        {
            return false;
        }
    }

    static bool clip_line_v(real& u, const vector2& p0, const vector2& p1, real v)
    {
        vector2 a = p0;
        vector2 b = p1;
        if (a[1] == b[1]) return false;
        if (a[1] > b[1]) std::swap(a, b);
        //--------------------------
        if (a[1] <= v && v <= b[1])
        {
            real k = (v - a[1]) / (b[1] - a[1]);
            u = (1 - k) * a[0] + k * b[0];
            return true;
        }
        else
        {
            return false;
        }
    }

    lines_loop_segment::lines_loop_segment(const std::vector<vector2>& cp)
        : cp_(cp)
    {
        get_minmax(min_, max_, cp_);
    }

    int lines_loop_segment::clip_u(std::vector<real>& vs, real u) const
    {
        int sz = (int)cp_.size();
        if (2 <= sz)
        {
            if (min_[0] <= u && u <= max_[0])
            {
                int nRet = 0;
                for (int i = 0; i < sz - 1; i++)
                {
                    real val;
                    if (clip_line_u(val, cp_[i], cp_[i + 1], u))
                    {
                        vs.push_back(val);
                        nRet++;
                    }
                }
                return nRet;
            }
        }
        return 0;
    }

    int lines_loop_segment::clip_v(std::vector<real>& us, real v) const
    {
        int sz = (int)cp_.size();
        if (2 <= sz)
        {
            if (min_[1] <= v && v <= max_[1])
            {
                int nRet = 0;
                for (int i = 0; i < sz - 1; i++)
                {
                    real val;
                    if (clip_line_v(val, cp_[i], cp_[i + 1], v))
                    {
                        us.push_back(val);
                        nRet++;
                    }
                }
                return nRet;
            }
        }
        return 0;
    }

    const vector2& lines_loop_segment::min() const
    {
        return min_;
    }

    const vector2& lines_loop_segment::max() const
    {
        return max_;
    }

    size_t lines_loop_segment::size() const
    {
        return cp_.size();
    }
}
