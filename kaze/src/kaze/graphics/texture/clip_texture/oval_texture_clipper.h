#ifndef KAZE_OVAL_TEXTURE_CLIPPER_H
#define KAZE_OVAL_TEXTURE_CLIPPER_H

#include "texture_clipper.h"

namespace kaze
{

    class oval_texture_clipper : public texture_clipper
    {
    public:
        oval_texture_clipper(const vector2& c, real rad);
        bool get(real u, real v) const;
        vector2 min() const { return c_ - vector2(rad_, rad_); }
        vector2 max() const { return c_ + vector2(rad_, rad_); }
    private:
        vector2 c_;
        real rad_;
    };
}

#endif