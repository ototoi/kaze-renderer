#include "bezier_loop_segment.h"
#include "bezier.h"

#include <vector>
#include <algorithm>
#include <memory>
#include <cmath>

#define TOLERANCE 1e-12

namespace kaze
{
    namespace
    {

        static void get_minmax(vector2& min, vector2& max, const std::vector<vector2>& p)
        {
            min = p[0];
            max = p[0];
            size_t sz = p.size();
            for (size_t i = 1; i < sz; i++)
            {
                for (int j = 0; j < 2; j++)
                {
                    if (min[j] > p[i][j]) min[j] = p[i][j];
                    if (max[j] < p[i][j]) max[j] = p[i][j];
                }
            }
        }

        static inline real cross(real u1, real u2, real v1, real v2)
        {
            return -v1 * u2 + u1 * v2;
        }

        static inline real check_line(const vector2& a, const vector2& b, const vector2& p)
        {
            vector2 ab = b - a;
            vector2 ap = p - a;

            return cross(ab[0], ab[1], ap[0], ap[1]);
        }

        static void push_upper_chain(std::vector<vector2>& chain, const vector2& p)
        {
            int sz = (int)chain.size();
            if (sz < 2)
            {
                chain.push_back(p);
            }
            else
            {
                vector2 a = chain[sz - 2]; //
                vector2 b = chain[sz - 1]; //
                if (0 <= check_line(a, b, p))
                {
                    chain.pop_back();
                    push_upper_chain(chain, p);
                }
                else
                {
                    chain.push_back(p);
                }
            }
        }

        static void push_lower_chain(std::vector<vector2>& chain, const vector2& p)
        {
            int sz = (int)chain.size();
            if (sz < 2)
            {
                chain.push_back(p);
            }
            else
            {
                vector2 a = chain[sz - 2]; //
                vector2 b = chain[sz - 1]; //
                if (0 >= check_line(a, b, p))
                {
                    chain.pop_back();
                    push_lower_chain(chain, p);
                }
                else
                {
                    chain.push_back(p);
                }
            }
        }

        /*
    struct x_sorter{
        inline bool operator()(const vector2& a, const vector2& b)const{
            return a[0] < b[0];
            //return a[1] < b[1];
        }
    };
    */

        static void convex_hull(std::vector<vector2>& upper_chain, std::vector<vector2>& lower_chain, const std::vector<vector2>& points)
        {
            using namespace std;
            int sz = (int)points.size();
            if (sz < 2) return;
            upper_chain.reserve(sz >> 1);
            lower_chain.reserve(sz >> 1);

            //std::vector<vector2> tp(points);
            //std::sort(tp.begin(), tp.end(), x_sorter());

            //
            upper_chain.push_back(points[0]);
            upper_chain.push_back(points[1]);
            for (int i = 2; i < sz; i++)
            {
                push_upper_chain(upper_chain, points[i]);
            }

            //
            lower_chain.push_back(points[0]);
            lower_chain.push_back(points[1]);
            for (int i = 2; i < sz; i++)
            {
                push_lower_chain(lower_chain, points[i]);
            }
        }

        int scan_chain(real t[], const std::vector<vector2>& chain)
        {
            int n = 0;
            size_t sz = chain.size();
            if (sz < 1) return 0;
            for (size_t i = 0; i < sz - 1; i++)
            {
                if (chain[i][1] * chain[i + 1][1] <= 0)
                {
                    real a = fabs(chain[i][1]);
                    real b = fabs(chain[i + 1][1]);
                    if (a + b)
                    {
                        real k = chain[i][0] + a / (a + b) * (chain[i + 1][0] - chain[i][0]);
                        t[n] = k;
                        n++;
                    }
                }
            }
            return n;
        }

        static void split_bezier_range(vector2 te0[], const vector2 e[], int n, double t0, double t1)
        {
            double a = t0 / t1;
            double b = t1;
            std::vector<vector2> te1(n);
            std::vector<vector2> te2(n);
            //vector2 te1[8];
            //vector2 te2[8];
            bezier_split(&te1[0], &te2[0], e, n, b);
            bezier_split(&te2[0], &te0[0], &te1[0], n, a);
        }

        template <int UV>
        static int clip_bezier_uv(std::vector<real>& out, const std::vector<vector2>& p, real tol)
        {
            static const int X = 1 - UV; //
            static const int Y = UV;     //

            vector2 min, max;
            get_minmax(min, max, p);
            if (min[Y] * max[Y] > 0) return 0;

            real wid = max[X] - min[X];
            if (wid < tol)
            {
                real ret = (min[X] + max[X]) * 0.5;
                out.push_back(ret);
                return 1;
            }
            else
            {
                size_t sz = p.size();
                std::vector<vector2> tmp(sz);
                real delta = real(1.0) / (sz - 1);
                for (size_t i = 0; i < sz; i++)
                {
                    tmp[i][0] = delta * i;
                    tmp[i][1] = p[i][Y];
                }
                std::vector<vector2> upper_chain;
                upper_chain.reserve(sz);
                std::vector<vector2> lower_chain;
                lower_chain.reserve(sz);
                convex_hull(upper_chain, lower_chain, tmp);

                int n = 0;
                real ts[4] = {0};
                n += scan_chain(&ts[n], upper_chain);
                n += scan_chain(&ts[n], lower_chain);
                if (n == 2)
                {
                    std::vector<real> px(sz);
                    for (size_t i = 0; i < sz; i++)
                        px[i] = p[i][X];
                    real px0 = bezier_evaluate(&px[0], (int)sz, ts[0]);
                    real px1 = bezier_evaluate(&px[0], (int)sz, ts[1]);
                    if (fabs(px0 - px1) < tol)
                    {
                        real ret = (px0 + px1) * 0.5;
                        out.push_back(ret);
                        return 1;
                    }
                    else
                    {
                        if (ts[0] > ts[1]) std::swap(ts[0], ts[1]);
                        real tw = ts[1] - ts[0];
                        if (tw > 0.5)
                        {
                            std::vector<vector2> pl(sz);
                            std::vector<vector2> pr(sz);
                            bezier_split(&pl[0], &pr[0], &p[0], (int)sz, real(0.5)); //split

                            int nRet = 0;
                            nRet += clip_bezier_uv<UV>(out, pl, tol);
                            nRet += clip_bezier_uv<UV>(out, pr, tol);
                            return nRet;
                        }
                        else
                        {
                            std::vector<vector2> pp(sz);
                            split_bezier_range(&pp[0], &p[0], (int)sz, ts[0], ts[1]);

                            return clip_bezier_uv<UV>(out, pp, tol);
                        }
                    }
                }
            }
            return 0;
        }

        static int clip_bezier_u(std::vector<real>& out, const std::vector<vector2>& p, real u, real tol)
        {
            std::vector<vector2> tmp(p);
            size_t sz = tmp.size();
            for (size_t i = 0; i < sz; i++)
            {
                tmp[i][0] -= u;
            }
            return clip_bezier_uv<0>(out, tmp, tol);
        }

        static int clip_bezier_v(std::vector<real>& out, const std::vector<vector2>& p, real v, real tol)
        {
            std::vector<vector2> tmp(p);
            size_t sz = tmp.size();
            for (size_t i = 0; i < sz; i++)
            {
                tmp[i][1] -= v;
            }
            return clip_bezier_uv<1>(out, tmp, tol);
        }
    }
}
//---------------------------------------------------------------------

namespace kaze
{

    bezier_loop_segment::bezier_loop_segment(const std::vector<vector2>& cp)
        : cp_(cp)
    {
        get_minmax(min_, max_, cp_);
    }

    int bezier_loop_segment::clip_u(std::vector<real>& vs, real u) const
    {
        return clip_bezier_u(vs, cp_, u, TOLERANCE);
    }

    int bezier_loop_segment::clip_v(std::vector<real>& us, real v) const
    {
        return clip_bezier_v(us, cp_, v, TOLERANCE);
    }

    const vector2& bezier_loop_segment::min() const
    {
        return min_;
    }

    const vector2& bezier_loop_segment::max() const
    {
        return max_;
    }

    size_t bezier_loop_segment::size() const
    {
        return cp_.size();
    }
}
