#ifndef KAZE_LINE_LOOP_SEGMENT_H
#define KAZE_LINE_LOOP_SEGMENT_H

#include "loop_segment.h"

namespace kaze
{

    class line_loop_segment : public loop_segment
    {
    public:
        line_loop_segment(const vector2& a, const vector2& b);
        int clip_u(std::vector<real>& vs, real u) const;
        int clip_v(std::vector<real>& us, real v) const;
        const vector2& min() const;
        const vector2& max() const;
        size_t size() const;

    private:
        vector2 a_;
        vector2 b_;
        vector2 min_;
        vector2 max_;
    };
}

#endif
