#ifndef KAZE_GRID_LOOP_TEXTURE_CLIPPER_H
#define KAZE_GRID_LOOP_TEXTURE_CLIPPER_H

#include "texture_clipper.h"
#include "loop_segment.h"
#include "loop_segment_tree.h"
#include <vector>
#include <memory>

namespace kaze
{

    class grid_loop_texture_clipper : public texture_clipper
    {
    public:
        grid_loop_texture_clipper(
            std::vector<const loop_segment*>& v,
            int nDIV_U = 256, int nDIV_V = 256);
        bool get(real u, real v) const;
        vector2 min() const { return min_; }
        vector2 max() const { return max_; }
    protected:
        bool get_internal(real u, real v) const;
        int check_grid(real u, real v) const;

    private:
        std::vector<char> mv_;
        int nDIV_U_;
        int nDIV_V_;
        std::unique_ptr<loop_segment_tree> v_tree_;
        vector2 min_;
        vector2 max_;
    };
}

#endif