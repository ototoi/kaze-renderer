#ifndef KAZE_LAYERED_TEXTURE_CLIPPER_H
#define KAZE_LAYERED_TEXTURE_CLIPPER_H

#include "texture_clipper.h"
#include "count_ptr.hpp"
#include <vector>

namespace kaze
{

    class layered_texture_clipper : public texture_clipper
    {
    public:
        layered_texture_clipper();
        void add_inner(const auto_count_ptr<texture_clipper>& p);
        void add_outer(const auto_count_ptr<texture_clipper>& p);
        void construct();

    protected:
        void set_minmax(const vector2& min, const vector2& max);

    public:
        bool get(real u, real v) const;

    public:
        vector2 min() const { return min_; }
        vector2 max() const { return max_; }
    protected:
        std::vector<std::shared_ptr<texture_clipper> > loop_;
        std::vector<bool> binner_;
        vector2 min_;
        vector2 max_;
    };
}

#endif