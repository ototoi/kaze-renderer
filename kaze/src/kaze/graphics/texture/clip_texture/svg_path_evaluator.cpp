#include <iostream>
#include <string>
#include <cstring>
#include <vector>
#include <sstream>

#include "values.h"
#include "types.h"

#include "svg_path_evaluator.h"

using namespace kaze;
using namespace kaze::svg;

struct SVGPATHCMD
{
    char cmd;
    std::vector<vector2> p;
};

static const char* test_path =
    "M 60 60 L 120 60 120 120 60 120 60 60 Z \
M 60 60 Q 90 30 120 60 Z M 60 120 Q 90 150 120 120 Z \
M 60 60 Q 30 90 60 120 Z M 120 60 Q 150 90 120 120 Z";

static bool convert_svg_path(std::vector<path_segment>& path, const std::vector<SVGPATHCMD>& cmds)
{
    size_t sz = cmds.size();
    vector2 current = vector2(0, 0);
    for (size_t i = 0; i < sz; i++)
    {
        switch (cmds[i].cmd)
        {
        case 'z':
        case 'Z':
        {
        }
        break;
        case 'M':
        {
            if (!cmds[i].p.empty())
            {
                size_t sz = cmds[i].p.size();
                current = cmds[i].p.back();
                if (sz >= 2)
                {
                    path.push_back(path_segment(SVG_PATH_LINES, cmds[i].p));
                }
            }
        }
        break;
        case 'm':
        {
            if (!cmds[i].p.empty())
            {
                size_t sz = cmds[i].p.size();
                std::vector<vector2> tmp(sz);
                for (size_t j = 0; j < sz; j++)
                {
                    tmp[j] = current + cmds[i].p[j];
                }
                current = tmp.back();
                if (sz >= 2)
                {
                    path.push_back(path_segment(SVG_PATH_LINES, tmp));
                }
            }
        }
        break;
        case 'L':
        {
            if (!cmds[i].p.empty())
            {
                size_t sz = cmds[i].p.size();
                std::vector<vector2> tmp(sz + 1);
                tmp[0] = current;
                for (size_t j = 0; j < sz; j++)
                {
                    tmp[j + 1] = cmds[i].p[j];
                }
                current = tmp.back();
                path.push_back(path_segment(SVG_PATH_LINES, tmp));
            }
        }
        break;
        case 'Q':
        {
            if (!cmds[i].p.empty())
            {
                size_t sz = cmds[i].p.size();
                if (sz > 2) break;
                std::vector<vector2> tmp(3);
                tmp[0] = current;
                for (size_t j = 0; j < 2; j++)
                {
                    tmp[j + 1] = cmds[i].p[j];
                }
                current = tmp.back();
                path.push_back(path_segment(SVG_PATH_QBEZIER, tmp));
            }
        }
        break;
        case 'C':
        {
            if (!cmds[i].p.empty())
            {
                size_t sz = cmds[i].p.size();
                if (sz > 3) break;
                std::vector<vector2> tmp(4);
                tmp[0] = current;
                for (size_t j = 0; j < 3; j++)
                {
                    tmp[j + 1] = cmds[i].p[j];
                }
                current = tmp.back();
                path.push_back(path_segment(SVG_PATH_CBEZIER, tmp));
            }
        }
        break;
        }
    }

    return true;
}

static void get_svg_command(char cmd, std::vector<SVGPATHCMD>& cmds, std::istream& is)
{
    std::vector<double> pos;

    std::string str;
    while (true)
    {
        str.clear();
        is >> str;
        if (!str.empty() && !isalpha(str[0]))
        {
            pos.push_back(atof(str.c_str()));
        }
        else
        {
            if (cmd == 'a' || cmd == 'A')
            {
            }
            else
            {
                std::vector<vector2> p;
                if (!pos.empty())
                {
                    p.resize(pos.size() >> 1);
                }
                for (size_t i = 0; i < p.size(); i++)
                {
                    p[i][0] = pos[2 * i + 0];
                    p[i][1] = pos[2 * i + 1];
                }
                pos.clear();
                SVGPATHCMD tmp;
                tmp.cmd = cmd;
                cmds.push_back(tmp);
                cmds.back().p.swap(p);
            }

            if (str.empty())
            {
                break;
            }
            else
            {
                cmd = str[0];
                continue;
            }
        }
    }
}

static void get_svg_command(std::vector<SVGPATHCMD>& cmds, const char* svg_str)
{
    std::stringstream ss(svg_str);

    std::string str;
    while (true)
    {
        str.clear();
        ss >> str;
        if (str.empty()) break;
        if (isalpha(str[0]))
        {
            get_svg_command(str[0], cmds, ss);
        }
    }
}
//-------------------------------------------------------------------------------------
namespace kaze
{
    namespace svg
    {
        bool evaluate_path_segement(std::vector<path_segment>& pathes, const char* svg_str)
        {
            std::vector<SVGPATHCMD> cmds;
            get_svg_command(cmds, svg_str);
            convert_svg_path(pathes, cmds);
            return true;
        }
    }
}
