#ifndef KAZE_LOOP_SEGMENT_TREE_H
#define KAZE_LOOP_SEGMENT_TREE_H

#include "loop_segment.h"
#include <vector>

namespace kaze
{

    class loop_segment_tree_node;

    class loop_segment_tree
    {
    public:
        loop_segment_tree(std::vector<const loop_segment*>& in, int sort_uv);
        ~loop_segment_tree();
        bool test(std::vector<const loop_segment*>& out, real p) const;
        real min() const;
        real max() const;

    protected:
        loop_segment_tree_node* node_;
    };
}

#endif