#include "composite_loop_texture_clipper.h"

#include "loop_segment.h"
#include "bezier_loop_segment.h"
#include "lines_loop_segment.h"

#include <vector>
#include <algorithm>
#include <memory>
#include <cmath>

#define TOLERANCE 1e-12

namespace kaze
{

    composite_loop_texture_clipper::composite_loop_texture_clipper(int du, int dv)
        : grid_(0)
    {
        du_ = du;
        dv_ = dv;
    }

    composite_loop_texture_clipper::~composite_loop_texture_clipper()
    {
        if (grid_) delete grid_;
        size_t sz = clips_.size();
        for (size_t i = 0; i < sz; i++)
        {
            delete clips_[i];
        }
    }

    void composite_loop_texture_clipper::add_lines(const std::vector<vector2>& p)
    {
        clips_.push_back(new lines_loop_segment(p));
    }

    void composite_loop_texture_clipper::add_bezier(const std::vector<vector2>& p)
    {
        clips_.push_back(new bezier_loop_segment(p));
    }

    void composite_loop_texture_clipper::add_arc(const vector2& a, const vector2& b, real rad)
    {
        //
    }

    void composite_loop_texture_clipper::construct()
    {
        if (grid_) delete grid_;
        grid_ = 0;

        if (clips_.empty()) return;

        size_t sz = clips_.size();
        std::vector<const loop_segment*> tmp(sz);
        for (size_t i = 0; i < sz; i++)
        {
            tmp[i] = clips_[i];
        }
        grid_ = new grid_loop_texture_clipper(tmp, du_, dv_); //v_sort
    }

    bool composite_loop_texture_clipper::get(real u, real v) const
    {
        if (grid_)
        {
            return grid_->get(u, v);
        }
        else
        {
            return false;
        }
    }

    static const real FAR = values::far();

    vector2 composite_loop_texture_clipper::min() const
    {
        if (grid_) return grid_->min();
        return +vector2(FAR, FAR);
    }

    vector2 composite_loop_texture_clipper::max() const
    {
        if (grid_) return grid_->max();
        return -vector2(FAR, FAR);
    }
}
