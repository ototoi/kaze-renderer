#ifndef KAZE_DOTS_TEXTURE_CLIPPER_H
#define KAZE_DOTS_TEXTURE_CLIPPER_H

#include "texture_clipper.h"

#include <vector>

namespace kaze
{

    class dots_texture_clipper : public texture_clipper
    {
    public:
        struct kd_struct
        {
            char plane;
            vector2 pos;
        };

    public:
        dots_texture_clipper(const std::vector<vector2>& v, real radius = 0.01);
        bool get(real u, real v) const;

        void construct();

    public:
        vector2 min() const;
        vector2 max() const;

    private:
        std::vector<kd_struct> mv_;
        real radius_;
        vector2 min_;
        vector2 max_;
    };
}

#endif