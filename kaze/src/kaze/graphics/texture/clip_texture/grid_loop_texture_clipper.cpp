#include "grid_loop_texture_clipper.h"

#include <vector>
#include <algorithm>
#include <memory>
#include <cmath>

#define TOLERANCE 1e-12

namespace kaze
{

    typedef loop_segment LS;
    typedef const LS* CPLS;
    typedef std::vector<CPLS>::const_iterator CPLS_iterator;

    enum
    {
        INTR_OUT, //OUT
        INTR_CRS, //CROSS
        INTR_IN,  //IN
    };

    static void get_clip_u(std::vector<real>& out, const std::vector<const loop_segment*>& ls, real u)
    {
        size_t sz = ls.size();
        for (size_t i = 0; i < sz; i++)
        {
            ls[i]->clip_u(out, u);
        }
    }

    static void get_clip_v(std::vector<real>& out, const std::vector<const loop_segment*>& ls, real v)
    {
        size_t sz = ls.size();
        for (size_t i = 0; i < sz; i++)
        {
            ls[i]->clip_v(out, v);
        }
    }

    static int check_intersection(const std::vector<real>& inter, real a, real b)
    {
        int sz = (int)inter.size();
        if (sz < 1) return INTR_OUT;
        for (int i = 0; i < sz; i++)
        {
            if (a < inter[i] && inter[i] < b)
            {
                return INTR_CRS;
            }
        }
        for (int i = 0; i < sz - 1; i += 2)
        {
            if (inter[i] <= a && b <= inter[i + 1])
            {
                return INTR_IN;
            }
        }
        return INTR_OUT;
    }

    static int total_intersection(int a, int b, int c, int d)
    {
        int ar[] = {a, b, c, d};
        for (int i = 0; i < 4; i++)
        {
            if (ar[i] == INTR_CRS) return INTR_CRS;
        }
        if (!(a == b && b == c && c == d)) return INTR_CRS;
        return a;
    }

    static int get_divide_number(const std::vector<const loop_segment*>& v)
    {
        int nRet = 0;
        size_t sz = v.size();
        for (size_t i = 0; i < sz; i++)
        {
            nRet += (int)v[i]->size();
        }
        return nRet;
    }

    struct tol_equal
    {
        tol_equal(real tol) : tol_(tol) {}
        bool operator()(real a, real b) const
        {
            return fabs(b - a) < tol_;
        }
        real tol_;
    };
}

namespace kaze
{

    grid_loop_texture_clipper::grid_loop_texture_clipper(std::vector<const loop_segment*>& v, int nDIV_U, int nDIV_V)
    {
        static const real EPSILON = values::epsilon() * 1024;
        //static const real GRIDWIDE = 1.0/512;
        //----------------------------------------------

        //----------------------------------------------
        std::unique_ptr<loop_segment_tree> u_tree(new loop_segment_tree(v, 0));
        std::unique_ptr<loop_segment_tree> v_tree(new loop_segment_tree(v, 1));

        vector2 min(u_tree->min(), v_tree->min());
        vector2 max(u_tree->max(), v_tree->max());
        min -= vector2(EPSILON, EPSILON);
        max += vector2(EPSILON, EPSILON);

        vector2 wid = max - min;
        int nDIV = 4 * get_divide_number(v);
        if (nDIV_U <= 2 || nDIV_V <= 2)
        {
            nDIV_U = 2;
            nDIV_V = 2;
            if (wid[0] == wid[1])
            {
                nDIV_U = nDIV;
                nDIV_V = nDIV;
            }
            else if (wid[0] < wid[1])
            {
                nDIV_U = nDIV;
                nDIV_V = (int)ceil(nDIV * wid[1] / wid[0]);
            }
            else
            {
                nDIV_U = (int)ceil(nDIV * wid[0] / wid[1]);
                nDIV_V = nDIV;
            }
        }
        nDIV_U = std::min<int>(1024, std::max<int>(2, nDIV_U));
        nDIV_V = std::min<int>(1024, std::max<int>(2, nDIV_V));

        std::vector<CPLS> out;
        std::vector<std::vector<real> > u_inters(nDIV_V + 1);
        for (int i = 0; i <= nDIV_V; i++)
        { //V
            real t = min[1] + wid[1] * i / nDIV_V;

            out.clear();
            if (v_tree->test(out, t))
            {
                get_clip_v(u_inters[i], out, t);
                if (!u_inters[i].empty())
                {
                    std::sort(u_inters[i].begin(), u_inters[i].end());
                    u_inters[i].erase(std::unique(u_inters[i].begin(), u_inters[i].end(), tol_equal(TOLERANCE)), u_inters[i].end());
                }
            }
        }
        std::vector<std::vector<real> > v_inters(nDIV_U + 1);
        for (int i = 0; i <= nDIV_U; i++)
        { //U
            real t = min[0] + wid[0] * i / nDIV_U;
            out.clear();
            if (u_tree->test(out, t))
            {
                get_clip_u(v_inters[i], out, t);
                if (!v_inters[i].empty())
                {
                    std::sort(v_inters[i].begin(), v_inters[i].end());
                    v_inters[i].erase(std::unique(v_inters[i].begin(), v_inters[i].end(), tol_equal(TOLERANCE)), v_inters[i].end());
                }
            }
        }
        //----------------------------------------------
        std::vector<int> u_line(nDIV_U * (nDIV_V + 1));
        for (int j = 0; j <= nDIV_V; j++)
        { //�c��
            for (int i = 0; i < nDIV_U; i++)
            {
                real a = min[0] + wid[0] * (i) / nDIV_U;
                real b = min[0] + wid[0] * (i + 1) / nDIV_U;
                int nIxt = check_intersection(u_inters[j], a, b);
                u_line[nDIV_U * j + i] = nIxt;
            }
        }
        std::vector<int> v_line(nDIV_V * (nDIV_U + 1));
        for (int j = 0; j <= nDIV_U; j++)
        { //����
            for (int i = 0; i < nDIV_V; i++)
            {
                real a = min[1] + wid[1] * (i) / nDIV_V;
                real b = min[1] + wid[1] * (i + 1) / nDIV_V;
                int nIxt = check_intersection(v_inters[j], a, b);
                v_line[nDIV_V * j + i] = nIxt;
            }
        }
        //----------------------------------------------
        mv_.resize(nDIV_U * nDIV_V);
        for (int y = 0; y < nDIV_V; y++)
        {
            for (int x = 0; x < nDIV_U; x++)
            {
                int a = u_line[nDIV_U * (y + 0) + x];
                int b = u_line[nDIV_U * (y + 1) + x];
                int c = v_line[nDIV_V * (x + 0) + y];
                int d = v_line[nDIV_V * (x + 1) + y];
                mv_[y * nDIV_U + x] = total_intersection(a, b, c, d);
            }
        }
        //----------------------------------------------
        nDIV_U_ = nDIV_U;
        nDIV_V_ = nDIV_V;
        //----------------------------------------------

        min_ = min;
        max_ = max;
        //----------------------------------------------
        v_tree_.reset(v_tree.release());
    }

    bool grid_loop_texture_clipper::get(real u, real v) const
    {
        int n = check_grid(u, v);
        switch (n)
        {
        case INTR_OUT:
            return false;
        case INTR_IN:
            return true;
        default:
            return get_internal(u, v);
        }
    }

    bool grid_loop_texture_clipper::get_internal(real u, real v) const
    {
        std::vector<real> us;

        std::vector<CPLS> out;
        if (v_tree_->test(out, v))
        {
            size_t sz = out.size();
            for (size_t i = 0; i < sz; i++)
            {
                out[i]->clip_v(us, v);
            }
        }

        if (!us.empty())
        {
            std::sort(us.begin(), us.end());
            us.erase(std::unique(us.begin(), us.end(), tol_equal(TOLERANCE)), us.end());
            int usz = (int)us.size();
            for (int i = 0; i < usz - 1; i += 2)
            {
                if (us[i] <= u && u <= us[i + 1])
                {                //
                    return true; //inner is true
                }
            }
        }
        return false;
    }

    int grid_loop_texture_clipper::check_grid(real u, real v) const
    {
        int ix = (int)floor(nDIV_U_ * (u - min_[0]) / (max_[0] - min_[0]));
        int iy = (int)floor(nDIV_V_ * (v - min_[1]) / (max_[1] - min_[1]));
        if (ix < 0 || nDIV_U_ <= ix) return INTR_OUT;
        if (iy < 0 || nDIV_V_ <= iy) return INTR_OUT;

        return mv_[iy * nDIV_U_ + ix];
    }
}
