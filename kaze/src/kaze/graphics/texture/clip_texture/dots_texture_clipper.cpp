#include "dots_texture_clipper.h"

#include <vector>

#include "values.h"

namespace kaze
{
    typedef dots_texture_clipper::kd_struct kd_struct;
    typedef std::vector<kd_struct>::iterator iter;

    dots_texture_clipper::dots_texture_clipper(const std::vector<vector2>& v, real radius)
        : radius_(radius)
    {
        for (size_t i = 0; i < v.size(); i++)
        {
            kd_struct tmp;
            tmp.pos = v[i];
            tmp.plane = 0;
            mv_.push_back(tmp);
        }
        construct();
    }

    struct dist_info
    {
        real d2;
        vector2 pos;
    };

    static bool find(size_t a, size_t b, const std::vector<kd_struct>& v, dist_info& info, const vector2& p, real d2)
    {
        size_t sz = b - a;
        if (sz <= 0) return false;
        if (sz <= 8)
        {
            bool bRet = false;
            for (size_t i = a; i < b; i++)
            {
                real l2 = (v[i].pos - p).sqr_length();
                if (l2 < d2)
                {
                    d2 = l2;
                    info.d2 = d2;
                    info.pos = v[i].pos;
                    bRet = true;
                }
            }
            return bRet;
        }
        else
        {
            size_t c = a + (sz >> 1);
            int plane = v[c].plane;
            vector2 pos = v[c].pos;

            real s2 = pos[plane] - p[plane];
            s2 *= s2;

            bool bRet = false;
            if (p[plane] < pos[plane])
            {
                if (find(a, c, v, info, p, d2))
                {
                    d2 = info.d2;
                    bRet = true;
                }
                if (s2 <= d2)
                {
                    real l2 = (pos - p).sqr_length();
                    if (l2 < d2)
                    {
                        d2 = l2;
                        info.d2 = d2;
                        info.pos = pos;
                        bRet = true;
                    }
                    if (find(c + 1, b, v, info, p, d2))
                    {
                        d2 = info.d2;
                        bRet = true;
                    }
                }
            }
            else
            {
                if (find(c + 1, b, v, info, p, d2))
                {
                    d2 = info.d2;
                    bRet = true;
                }
                if (s2 <= d2)
                {
                    real l2 = (pos - p).sqr_length();
                    if (l2 < d2)
                    {
                        d2 = l2;
                        info.d2 = d2;
                        info.pos = pos;
                        bRet = true;
                    }
                    if (find(a, c, v, info, p, d2))
                    {
                        d2 = info.d2;
                        bRet = true;
                    }
                }
            }
            return bRet;
        }
    }

    static bool find(const std::vector<kd_struct>& v, dist_info& info, const vector2& p)
    {
        static const real FAR = values::far();
        size_t sz = v.size();
        if (sz == 0) return false;
        if (find(0, sz, v, info, p, FAR))
        {
            return true;
        }
        return false;
    }

    bool dots_texture_clipper::get(real u, real v) const
    {
        vector2 pos(u, v);
#if 1
        dist_info info;
        if (find(mv_, info, pos))
        {
            real r2 = radius_ * radius_;
            if (info.d2 < r2)
            {
                return true;
            }
        }
        return false;
#else
        real r2 = radius_ * radius_;
        for (size_t i = 0; i < mv_.size(); i++)
        {
            real d2 = (mv_[i].pos - pos).sqr_length();
            if (d2 < r2) return true;
        }
        return false;
#endif
    }

    struct kd_less
    {
        kd_less(int p) : plane(p) {}
        bool operator()(const kd_struct& a, const kd_struct& b)
        {
            return a.pos[plane] < b.pos[plane];
        }
        int plane;
    };

    static void sort_kdtree(iter a, iter b, const vector2& min, const vector2& max)
    {
        if (a == b) return;
        size_t sz = b - a;
        vector2 wid = max - min;
        int plane = (wid[0] < wid[1]) ? 1 : 0;
        std::sort(a, b, kd_less(plane));
        iter c = a + (sz >> 1);
        vector2 cmin, cmax;

        cmin = min;
        cmax = max;
        cmax[plane] = (max[plane] + min[plane]) * 0.5;
        sort_kdtree(a, c, cmin, cmax);

        cmin = min;
        cmin[plane] = (max[plane] + min[plane]) * 0.5;
        cmax = max;
        sort_kdtree(c + 1, b, cmin, cmax);

        c->plane = plane;
    }

    void dots_texture_clipper::construct()
    {
        if (mv_.empty()) return;
        vector2 min = mv_[0].pos;
        vector2 max = mv_[0].pos;
        size_t sz = mv_.size();
        for (size_t i = 1; i < sz; i++)
        {
            for (int j = 0; j < 2; j++)
            {
                if (min[j] > mv_[i].pos[j]) min[j] = mv_[i].pos[j];
                if (max[j] < mv_[i].pos[j]) max[j] = mv_[i].pos[j];
            }
        }
        sort_kdtree(mv_.begin(), mv_.end(), min, max);
        min_ = min - vector2(radius_, radius_);
        max_ = max + vector2(radius_, radius_);
    }

    vector2 dots_texture_clipper::min() const
    {
        return min_;
    }

    vector2 dots_texture_clipper::max() const
    {
        return max_;
    }
}
