#include "layered_texture_clipper.h"

#include "values.h"

namespace kaze
{

    static const real FAR = values::far();

    layered_texture_clipper::layered_texture_clipper()
        : min_(FAR, FAR), max_(FAR, FAR)
    {
    }

    void layered_texture_clipper::add_inner(const auto_count_ptr<texture_clipper>& p)
    {
        loop_.push_back(p);
        binner_.push_back(true);
        set_minmax(p->min(), p->max());
    }
    //
    void layered_texture_clipper::add_outer(const auto_count_ptr<texture_clipper>& p)
    {
        loop_.push_back(p);
        binner_.push_back(false);
        set_minmax(p->min(), p->max());
    }

    void layered_texture_clipper::construct()
    {
        ; //
    }

    void layered_texture_clipper::set_minmax(const vector2& min, const vector2& max)
    {
        for (int i = 0; i < 2; i++)
        {
            if (min[i] < min_[i]) min_[i] = min[i];
            if (max[i] > max_[i]) max_[i] = max[i];
        }
    }

    bool layered_texture_clipper::get(real u, real v) const
    {
        if (loop_.empty()) return true;
        int sz = (int)loop_.size();
        for (int i = sz - 1; 0 <= i; i--)
        {
            if (binner_[i])
            {
                if (loop_[i]->get(u, v)) return false;
            }
            else
            { //clip
                if (loop_[i]->get(u, v)) return true;
            }
        }
        return false;
    }
}
