#ifndef KAZE_BASE_CLIP_TEXTURE_H
#define KAZE_BASE_CLIP_TEXTURE_H

#include "texture.hpp"
#include "texture_clipper.h"
#include "count_ptr.hpp"

namespace kaze
{

    class base_clip_texture : public texture<bool>
    {
    public:
        base_clip_texture(const auto_count_ptr<texture_clipper>& clp)
            : clipper_(clp) {}
        bool get(const sufflight& suf) const
        {
            vector3 c(suf.coord());
            real u = c[0];
            real v = c[1];
            vector2 min = clipper_->min();
            vector2 max = clipper_->max();
            if (min[0] <= u && u <= max[0] && min[1] <= v && v <= max[1])
            {
                return clipper_->get(u, v);
            }
            return false;
        }

    protected:
        std::shared_ptr<texture_clipper> clipper_;
    };
}

#endif
