#ifndef KAZE_LOOP_SEGMENT_H
#define KAZE_LOOP_SEGMENT_H

#include <vector>
#include "values.h"

namespace kaze
{

    class loop_segment
    {
    public:
        virtual ~loop_segment() {}
        virtual int clip_u(std::vector<real>& vs, real u) const = 0;
        virtual int clip_v(std::vector<real>& us, real v) const = 0;
        virtual const vector2& min() const = 0;
        virtual const vector2& max() const = 0;
        virtual size_t size() const = 0;
    };
}

#endif