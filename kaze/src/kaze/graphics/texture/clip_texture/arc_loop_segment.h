#ifndef KAZE_ARC_LOOP_SEGMENT_H
#define KAZE_ARC_LOOP_SEGMENT_H

#include "loop_segment.h"
#include <vector>

namespace kaze
{

    class arc_loop_segment : public loop_segment
    {
    public:
        arc_loop_segment(const vector2& a, const vector2& b, real rad);
        int clip_u(std::vector<real>& vs, real u) const;
        int clip_v(std::vector<real>& us, real v) const;
        const vector2& min() const;
        const vector2& max() const;
        size_t size() const;

    private:
        ; //
        ; //
        real r_;
        vector2 c_;
        vector2 min_;
        vector2 max_;
    };
}

#endif