#ifndef KAZE_TEXTURE_CLIPPER_H
#define KAZE_TEXTURE_CLIPPER_H

#include "types.h"

namespace kaze
{

    class texture_clipper
    {
    public:
        virtual ~texture_clipper() {}
        virtual bool get(real u, real v) const { return true; }
        virtual vector2 min() const = 0;
        virtual vector2 max() const = 0;
    };
}

#endif