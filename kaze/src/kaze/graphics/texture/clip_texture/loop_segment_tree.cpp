#include "loop_segment_tree.h"

#include <vector>
#include <algorithm>
#include <memory>
#include <cmath>

namespace kaze
{

    typedef loop_segment LS;
    typedef const LS* CPLS;
    typedef std::vector<CPLS>::const_iterator CPLS_iterator;

    class loop_segment_tree_node
    {
    public:
        virtual ~loop_segment_tree_node() {}
        virtual bool test(std::vector<CPLS>& out, real v) const = 0;
        virtual real min() const = 0;
        virtual real max() const = 0;
    };

    template <int UV>
    class loop_segment_tree_node_leaf : public loop_segment_tree_node
    {
    public:
        loop_segment_tree_node_leaf(CPLS p) : p_(p) {}
        bool test(std::vector<CPLS>& out, real v) const
        {
            if (p_->min()[UV] <= v && v <= p_->max()[UV])
            {
                out.push_back(p_);
                return true;
            }
            else
            {
                return false;
            }
        }
        real min() const { return p_->min()[UV]; }
        real max() const { return p_->max()[UV]; }
    private:
        CPLS p_;
    };

    template <int UV>
    class loop_segment_tree_node_branch : public loop_segment_tree_node
    {
    public:
        loop_segment_tree_node_branch(CPLS_iterator a, CPLS_iterator b)
        {
            size_t sz = b - a;

            size_t lsz = sz >> 1;
            size_t rsz = sz - lsz;

            nodes_[0] = 0;
            nodes_[1] = 0;

            if (lsz)
            {
                if (lsz == 1)
                {
                    nodes_[0] = new loop_segment_tree_node_leaf<UV>(*a);
                }
                else
                {
                    nodes_[0] = new loop_segment_tree_node_branch<UV>(a, a + lsz);
                }
            }

            if (rsz)
            {
                if (rsz == 1)
                {
                    nodes_[1] = new loop_segment_tree_node_leaf<UV>(*(a + lsz));
                }
                else
                {
                    nodes_[1] = new loop_segment_tree_node_branch<UV>(a + lsz, b);
                }
            }

            vrange_[0] = +values::far();
            vrange_[1] = -values::far();

            if (nodes_[0])
            {
                vrange_[0] = std::min<real>(vrange_[0], nodes_[0]->min());
                vrange_[1] = std::max<real>(vrange_[1], nodes_[0]->max());
            }
            if (nodes_[1])
            {
                vrange_[0] = std::min<real>(vrange_[0], nodes_[1]->min());
                vrange_[1] = std::max<real>(vrange_[1], nodes_[1]->max());
            }
        }
        ~loop_segment_tree_node_branch()
        {
            if (nodes_[0]) delete nodes_[0];
            if (nodes_[1]) delete nodes_[1];
        }
        bool test(std::vector<CPLS>& out, real v) const
        {
            if (vrange_[0] <= v && v <= vrange_[1])
            {
                bool bRet = false;
                if (nodes_[0] && nodes_[0]->test(out, v)) bRet = true;
                if (nodes_[1] && nodes_[1]->test(out, v)) bRet = true;
                return bRet;
            }
            else
            {
                return false;
            }
        }

        real min() const { return vrange_[0]; }
        real max() const { return vrange_[1]; }
    private:
        loop_segment_tree_node* nodes_[2];
        real vrange_[2];
    };

    struct pred_CPLS
    {
        pred_CPLS(int p) : plane_(p) {}
        bool operator()(CPLS a, CPLS b) const
        {
            return (a->min()[plane_] + a->max()[plane_]) < (b->min()[plane_] + b->max()[plane_]);
        }
        int plane_;
    };
}

namespace kaze
{

    loop_segment_tree::loop_segment_tree(std::vector<const loop_segment*>& in, int uv)
    {
        std::sort(in.begin(), in.end(), pred_CPLS(uv));
        if (uv == 0)
            node_ = new loop_segment_tree_node_branch<0>(in.begin(), in.end()); //u sort
        else
            node_ = new loop_segment_tree_node_branch<1>(in.begin(), in.end()); //v sort
    }

    loop_segment_tree::~loop_segment_tree()
    {
        delete node_;
    }

    bool loop_segment_tree::test(std::vector<const loop_segment*>& out, real p) const
    {
        return node_->test(out, p);
    }

    real loop_segment_tree::min() const
    {
        return node_->min();
    }

    real loop_segment_tree::max() const
    {
        return node_->max();
    }
}