#include "lines_texture_clipper.h"

#include "lines_loop_segment.h"

namespace kaze
{

    static void get_minmax(vector2& min, vector2& max, const std::vector<vector2>& cp)
    {
        min = max = cp[0];
        for (size_t i = 1; i < cp.size(); i++)
        {
            for (int j = 0; j < 2; j++)
            {
                if (min[j] > cp[i][j]) min[j] = cp[i][j];
                if (max[j] < cp[i][j]) max[j] = cp[i][j];
            }
        }
    }

    lines_texture_clipper::lines_texture_clipper(const vector2& a, const vector2& b, const vector2& c)
    {
        std::vector<vector2> vec;
        vec.push_back(a);
        vec.push_back(b);
        vec.push_back(c);
        vec.push_back(a);
        get_minmax(min_, max_, vec);
        segs_.push_back(new lines_loop_segment(vec));
    }

    lines_texture_clipper::lines_texture_clipper(const std::vector<vector2>& cp)
    {
        if (cp.size() < 3) return;
        std::vector<vector2> vec(cp);
        if (vec.front() != vec.back())
        {
            vec.push_back(vec.front());
        }
        get_minmax(min_, max_, vec);
        segs_.push_back(new lines_loop_segment(vec));
    }

    lines_texture_clipper::~lines_texture_clipper()
    {
        size_t sz = segs_.size();
        for (size_t i = 0; i < sz; i++)
        {
            delete segs_[i];
        }
    }

    bool lines_texture_clipper::get(real u, real v) const
    {
        if (min_[0] <= u && u <= max_[0] && min_[1] <= v && v <= max_[1])
        {
            std::vector<real> us;
            size_t sz = segs_.size();
            for (size_t i = 0; i < sz; i++)
            {
                segs_[i]->clip_v(us, v);
            }

            if (!us.empty())
            {
                std::sort(us.begin(), us.end());
                us.erase(std::unique(us.begin(), us.end()), us.end());
                int usz = (int)us.size();
                for (int i = 0; i < usz - 1; i += 2)
                {
                    if (us[i] <= u && u <= us[i + 1])
                    {                //
                        return true; //inner is true
                    }
                }
            }
        }
        return false;
    }
}
