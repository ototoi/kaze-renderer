#ifndef KAZE_CLIP_TEXTURE_HPP
#define KAZE_CLIP_TEXTURE_HPP

#include "texture.hpp"
#include "count_ptr.hpp"
#include "base_clip_texture.h"
#include "values.h"
#include <vector>

namespace kaze
{
    namespace ns_clip_texture
    {
        template <class T>
        class colored_texture_clipper : public texture<T>
        {
        public:
            colored_texture_clipper(const auto_count_ptr<texture<T> >& tex, const auto_count_ptr<texture_clipper>& clp)
                : tex_(tex), clp_(clp) {}
        public:
            T get(const sufflight& suf) const
            {
                return tex_->get(suf);
            }

        public:
            bool is(real u, real v) const { return clp_->get(u, v); }
            vector2 min() const { return clp_->min(); }
            vector2 max() const { return clp_->max(); }
        private:
            std::shared_ptr<texture<T> > tex_;
            std::shared_ptr<texture_clipper> clp_;
        };

        template <class T>
        class texture_clipper_tree_node
        {
        public:
            virtual ~texture_clipper_tree_node() {}
            virtual const texture<T>* get(real u, real v) const = 0;
        };

        template <class T>
        class texture_clipper_tree_node_leaf : public texture_clipper_tree_node<T>
        {
        public:
            texture_clipper_tree_node_leaf(const colored_texture_clipper<T>* ctc) : ctc_(ctc) {}
            const texture<T>* get(real u, real v) const
            {
                if (ctc_->is(u, v))
                {
                    return ctc_;
                }
                return NULL;
            }

        private:
            const colored_texture_clipper<T>* ctc_;
        };

        template <class T>
        class texture_clipper_tree_node_branch : public texture_clipper_tree_node<T>
        {
            typedef const colored_texture_clipper<T>* PCTC;
            static void get_minmax(vector2& min, vector2& max, PCTC* a, PCTC* b)
            {
                static const real FAR = values::far();
                min = vector2(FAR, FAR);
                max = vector2(-FAR, -FAR);
                for (PCTC* i = a; i != b; i++)
                {
                    vector2 cmin = (*i)->min();
                    vector2 cmax = (*i)->max();
                    for (int j = 0; j < 2; j++)
                    {
                        if (min[j] > cmin[j]) min[j] = cmin[j];
                        if (max[j] < cmax[j]) max[j] = cmax[j];
                    }
                }
            }
            struct sorter
            {
                sorter(int pl) : plane(pl) {}
                bool operator()(PCTC a, PCTC b) const
                {
                    vector2 ac = a->min() + a->max();
                    vector2 bc = b->min() + b->max();
                    return ac[plane] < bc[plane];
                }
                int plane;
            };

        public:
            texture_clipper_tree_node_branch(PCTC* a, PCTC* b)
            {
                size_t sz = b - a;
                size_t m = sz >> 1;
                get_minmax(min_, max_, a, b);
                vector2 wid = max_ - min_;
                int pl = (wid[0] > wid[1]) ? 0 : 1;
                std::sort(a, b, sorter(pl));
                PCTC* c = a + m;
                nodes_[0] = 0;
                nodes_[1] = 0;
                if (m)
                {
                    if (m == 1)
                    {
                        nodes_[0] = new texture_clipper_tree_node_leaf<T>(*a);
                    }
                    else
                    {
                        nodes_[0] = new texture_clipper_tree_node_branch<T>(a, c);
                    }
                }
                m = sz - m;
                if (m)
                {
                    if (m == 1)
                    {
                        nodes_[1] = new texture_clipper_tree_node_leaf<T>(*c);
                    }
                    else
                    {
                        nodes_[1] = new texture_clipper_tree_node_branch<T>(c, b);
                    }
                }
            }

            ~texture_clipper_tree_node_branch()
            {
                if (nodes_[0]) delete nodes_[0];
                if (nodes_[1]) delete nodes_[1];
            }

            const texture<T>* get(real u, real v) const
            {
                if (min_[0] <= u && u <= max_[0] && min_[1] <= v && v <= max_[1])
                {
                    const texture<T>* pTex = NULL;
                    if (nodes_[0] && (pTex = nodes_[0]->get(u, v))) return pTex;
                    if (nodes_[1] && (pTex = nodes_[1]->get(u, v))) return pTex;
                }
                return NULL;
            }

        private:
            texture_clipper_tree_node<T>* nodes_[2];
            vector2 min_;
            vector2 max_;
        };
    }

    template <class T>
    class clip_texture : public texture<T>
    {
    public:
        typedef ns_clip_texture::colored_texture_clipper<T> colored_texture_clipper;
        typedef ns_clip_texture::texture_clipper_tree_node<T> tree_type;
        typedef ns_clip_texture::texture_clipper_tree_node_branch<T> branch_type;

    public:
        clip_texture(const auto_count_ptr<texture<T> >& a)
            : tex_(a), tree_(NULL)
        {
            ;
        }
        ~clip_texture()
        {
            if (tree_) delete tree_;
        }

        void add(const auto_count_ptr<texture<T> >& tex, const auto_count_ptr<texture_clipper>& pred)
        {
            std::shared_ptr<colored_texture_clipper> p(new colored_texture_clipper(tex, pred));
            v_.push_back(p);
        }

        void construct()
        {
            typedef const colored_texture_clipper* PCTC;

            if (tree_) delete tree_;
            tree_ = NULL;
            size_t sz = v_.size();
            if (sz <= 4) return;

            std::vector<PCTC> vec(sz);
            for (size_t i = 0; i < sz; i++)
            {
                vec[i] = v_[i].get();
            }
            tree_ = new branch_type((&vec[0]), (&vec[0]) + sz);
        }

    public:
        T get(const sufflight& suf) const
        {
            real u = suf.coord()[0];
            real v = suf.coord()[1];
            if (tree_)
            {
                const texture<T>* pct = tree_->get(u, v);
                if (pct)
                {
                    return pct->get(suf);
                }
            }
            else
            {
                int vsz = (int)v_.size();
                for (int i = vsz - 1; i >= 0; i--)
                {
                    if (v_[i]->is(u, v))
                    {
                        return v_[i]->get(suf);
                    }
                }
            }

            return tex_->get(suf);
        }

    protected:
        std::vector<std::shared_ptr<colored_texture_clipper> > v_;
        std::shared_ptr<texture<T> > tex_;
        tree_type* tree_;
    };
}

#endif
