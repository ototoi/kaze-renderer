#ifndef KAZE_SVG_PATH_EVALUATOR_H
#define KAZE_SVG_PATH_EVALUATOR_H

#include <vector>
#include "types.h"

namespace kaze
{
    namespace svg
    {

        enum SVG_PATH_TYPE
        {
            SVG_PATH_LINES,
            SVG_PATH_QBEZIER,
            SVG_PATH_CBEZIER,
            SVG_PATH_ARC
        };

        struct path_segment
        {
            path_segment(SVG_PATH_TYPE type_, const std::vector<vector2>& p_) : type(type_), p(p_), radians(0) {}

            SVG_PATH_TYPE type;
            std::vector<vector2> p;
            real radians; //for arc
        };

        bool evaluate_path_segement(std::vector<path_segment>& pathes, const char* svg_str);
    }
}

#endif