#ifndef KAZE_BEZIER_LOOP_SEGMENT_H
#define KAZE_BEZIER_LOOP_SEGMENT_H

#include "loop_segment.h"
#include <vector>

namespace kaze
{

    class bezier_loop_segment : public loop_segment
    {
    public:
        bezier_loop_segment(const std::vector<vector2>& cp);
        int clip_u(std::vector<real>& vs, real u) const;
        int clip_v(std::vector<real>& us, real v) const;
        const vector2& min() const;
        const vector2& max() const;
        size_t size() const;

    private:
        std::vector<vector2> cp_;
        vector2 min_;
        vector2 max_;
    };
}

#endif