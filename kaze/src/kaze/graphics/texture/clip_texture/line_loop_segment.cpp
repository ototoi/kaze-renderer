#include "line_loop_segment.h"

namespace kaze
{

    line_loop_segment::line_loop_segment(const vector2& a, const vector2& b)
        : a_(a), b_(b)
    {
        ; //
    }

    int line_loop_segment::clip_u(std::vector<real>& vs, real u) const
    {
        return 0;
    }
    int line_loop_segment::clip_v(std::vector<real>& us, real v) const
    {
        return 0;
    }
    const vector2& line_loop_segment::min() const { return min_; }
    const vector2& line_loop_segment::max() const { return max_; }
    size_t line_loop_segment::size() const { return 2; }
}
