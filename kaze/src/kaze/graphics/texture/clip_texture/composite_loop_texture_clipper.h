#ifndef KAZE_COMPOSITE_LOOP_TEXTURE_CLIPPER_H
#define KAZE_COMPOSITE_LOOP_TEXTURE_CLIPPER_H

#include "texture_clipper.h"
#include "grid_loop_texture_clipper.h"
#include <vector>

namespace kaze
{

    class composite_loop_texture_clipper : public texture_clipper
    {
    public:
        composite_loop_texture_clipper(int du = -1, int dv = -1);
        ~composite_loop_texture_clipper();

    public:
        void add(const std::vector<vector2>& p)
        {
            add_bezier(p);
        }

        void add_lines(const std::vector<vector2>& p);
        void add_bezier(const std::vector<vector2>& p);
        void add_arc(const vector2& a, const vector2& b, real rad);

        void construct();

    public:
        vector2 min() const;
        vector2 max() const;

    public:
        bool get(real u, real v) const;

    protected:
        std::vector<loop_segment*> clips_;
        grid_loop_texture_clipper* grid_;
        int dv_;
        int du_;
    };
}

#endif