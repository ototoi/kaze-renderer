#include "oval_texture_clipper.h"

namespace kaze
{

    oval_texture_clipper::oval_texture_clipper(const vector2& c, real rad)
        : c_(c), rad_(rad)
    {
    }

    bool oval_texture_clipper::get(real u, real v) const
    {
        return ((c_ - vector2(u, v)).sqr_length() <= rad_ * rad_);
    }
}
