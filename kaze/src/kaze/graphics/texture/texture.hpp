#ifndef KAZE_TEXTURE_HPP
#define KAZE_TEXTURE_HPP

#include "sufflight.h"

namespace kaze
{
    template <class T>
    class texture
    {
    public:
        typedef T value_type;

    public:
        virtual ~texture() {}
        virtual T get(const sufflight& suf) const = 0;
        virtual T get(real u) const
        {
            return get(u, 0);
        }
        virtual T get(real u, real v) const
        {
            return get(u, v, 0);
        }
        virtual T get(real u, real v, real w) const
        {
            sufflight suf;
            suf.set_coord(vector3(u, v, w));
            return get(suf);
        }
    };
}

#endif