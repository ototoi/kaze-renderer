#ifndef KAZE_CUBEMAP_TEXTURE_HPP
#define KAZE_CUBEMAP_TEXTURE_HPP

#include "types.h"
#include "values.h"
#include "texture.hpp"
#include "count_ptr.hpp"

#include <vector>
#include <cmath>

namespace kaze
{

    template <class T>
    class cubemap_texture : public texture<T>
    {
    public:
        enum
        {
            PX,
            NX,
            PY,
            NY,
            PZ,
            NZ
        };

    public:
        cubemap_texture(
            const std::vector<std::shared_ptr<texture<T> > >& vt) : vt_(vt)
        {
            ;
        }

        ~cubemap_texture()
        {
            ;
        }

    public:
        T get(const sufflight& suf) const
        {
            //static const real PI_2 = 2.0 / values::pi();
            vector3 d1 = normalize(suf.direction());
            int cls = classify(d1);
            vector3 d2 = get_mat(cls) * d1;
            real x = d2[0];
            real y = d2[1];
            real z = d2[2];
            real s = (x / z + 1) * 0.5;
            real t = (y / z + 1) * 0.5;
            sufflight tsl(suf);
            tsl.set_stw(color3(s, t, 0));
            return vt_[cls]->get(tsl);
        }

    protected:
        static inline int classify(const vector3& v)
        {
            int n = 0;
            if (fabs(v[n]) <= fabs(v[1])) n = 1;
            if (fabs(v[n]) <= fabs(v[2])) n = 2;
            return 2 * n + sgn(v[n]);
        }

        static inline int sgn(real v)
        {
            return (v > 0) ? 0 : 1;
        }

        static const matrix3& get_mat(int i)
        {
            static const matrix3 mats[6] =
                {
                    mat3_gen::scaling(+1, -1, +1) * mat3_gen::rotation_y(values::radians(-90)), //PX
                    mat3_gen::scaling(+1, -1, +1) * mat3_gen::rotation_y(values::radians(+90)),
                    mat3_gen::scaling(+1, -1, +1) * mat3_gen::rotation_x(values::radians(+90)),
                    mat3_gen::scaling(+1, -1, +1) * mat3_gen::rotation_x(values::radians(-90)),
                    mat3_gen::scaling(+1, +1, +1) * mat3_gen::rotation_y(values::radians(180)),
                    mat3_gen::scaling(+1, +1, +1) * mat3_gen::rotation_y(values::radians(0))};
            return mats[i];
        }

    protected:
        std::vector<std::shared_ptr<texture<T> > > vt_;
    };
}

#endif
