#ifndef KAZE_AVERAGE_TEXTURE_HPP
#define KAZE_AVERAGE_TEXTURE_HPP

#include "texture.hpp"
#include "count_ptr.hpp"

#include <vector>

namespace kaze
{

    template <class T>
    class average_texture : public texture<T>
    {
    public:
        average_texture() {}
        void add(const auto_count_ptr<texture<T> >& t)
        {
            mv_.push_back(t);
        }
        size_t size() const
        {
            return mv_.size();
        }

    public:
        T get(const sufflight& suf) const
        {
            if (mv_.empty())
            {
                return T();
            }
            else
            {
                size_t sz = mv_.size();
                T tmp = mv_[0]->get(suf);
                for (size_t i = 1; i < sz; i++)
                {
                    tmp += mv_[i]->get(suf);
                }
                return tmp * real(1.0 / sz);
            }
        }

    protected:
        std::vector<std::shared_ptr<texture<T> > > mv_;
    };
}

#endif
