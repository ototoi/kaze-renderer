#ifndef KAZE_UVW_TEXTURE_HPP
#define KAZE_UVW_TEXTURE_HPP

#include "texture.hpp"
#include "constant_texture.hpp"
#include "count_ptr.hpp"

namespace kaze
{

    template <class T>
    class uvw_texture : public texture<T>
    {
    public:
        uvw_texture(
            const auto_count_ptr<texture<T> >& uv000,
            const auto_count_ptr<texture<T> >& uv100,
            const auto_count_ptr<texture<T> >& uv010,
            const auto_count_ptr<texture<T> >& uv110,
            const auto_count_ptr<texture<T> >& uv001,
            const auto_count_ptr<texture<T> >& uv101,
            const auto_count_ptr<texture<T> >& uv011,
            const auto_count_ptr<texture<T> >& uv111, ) : uv000_(uv000), uv100_(uv100), uv010_(uv010), uv110_(uv110),
                                                          uv001_(uv001), uv101_(uv101), uv011_(uv011), uv111_(uv111) {}

        uvw_texture(
            const T& uv000,
            const T& uv100,
            const T& uv010,
            const T& uv110,
            const T& uv001,
            const T& uv101,
            const T& uv011,
            const T& uv111) : uv000_(new constant_texture<T>(uv000)),
                              uv100_(new constant_texture<T>(uv100)),
                              uv010_(new constant_texture<T>(uv010)),
                              uv110_(new constant_texture<T>(uv110)),
                              uv001_(new constant_texture<T>(uv001)),
                              uv101_(new constant_texture<T>(uv101)),
                              uv011_(new constant_texture<T>(uv011)),
                              uv111_(new constant_texture<T>(uv111))
        {
        }

        T get(const sufflight& suf) const
        {
            vector3 c = suf.coord();
            real u = c[0];
            real v = c[1];

            u = u - floor(u);
            v = v - floor(v);
            w = w - floor(w);

            T tmp[8];
            tmp[0] = uv000_->get(suf);
            tmp[1] = uv100_->get(suf);
            tmp[2] = uv010_->get(suf);
            tmp[3] = uv110_->get(suf);
            tmp[4] = uv001_->get(suf);
            tmp[5] = uv101_->get(suf);
            tmp[6] = uv011_->get(suf);
            tmp[7] = uv111_->get(suf);

            return mix(u, v, w, tmp);
        }

    protected:
        T mix(real u, real v, real w, const T tmp[8])
        {
            real iu = 1 - u;
            real iv = 1 - v;
            real iw = 1 - w;

            T ret = iw * (iv * (iu * tmp[0] + u * tmp[1]) + v * (iu * tmp[2] + u * tmp[3])) +
                    w * (iv * (iu * tmp[4] + u * tmp[5]) + v * (iu * tmp[6] + u * tmp[7]));
            return ret;
        }

    protected:
        std::shared_ptr<texture<T> > uv000_;
        std::shared_ptr<texture<T> > uv100_;
        std::shared_ptr<texture<T> > uv010_;
        std::shared_ptr<texture<T> > uv110_;
        std::shared_ptr<texture<T> > uv001_;
        std::shared_ptr<texture<T> > uv101_;
        std::shared_ptr<texture<T> > uv011_;
        std::shared_ptr<texture<T> > uv111_;
    };
}

#endif