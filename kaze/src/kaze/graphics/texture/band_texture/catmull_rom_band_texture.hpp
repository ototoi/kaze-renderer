#ifndef KAZE_CATMULL_ROM_BAND_TEXTURE_HPP
#define KAZE_CATMULL_ROM_BAND_TEXTURE_HPP

#include "band_texture.hpp"
#include "catmull_rom.h"

#include <vector>

namespace kaze
{

    template <class T>
    class catmull_rom_band_texture : public band_texture<T>
    {
    public:
        catmull_rom_band_texture() {}
        catmull_rom_band_texture(const std::vector<T>& v) : mv_(v) {}
        catmull_rom_band_texture(const T v[], int nsz) : mv_(v, v + nsz) {}

        void add(const T& rhs) { mv_.push_back(rhs); }
        size_t size() const { return mv_.size(); }
    public:
        T get(real u) const
        {
            return catmull_rom_evaluate(&mv_[0], (int)mv_.size(), u);
        }

    private:
        std::vector<T> mv_;
    };
}

#endif