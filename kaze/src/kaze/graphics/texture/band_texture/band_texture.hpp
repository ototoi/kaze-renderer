#ifndef KAZE_BAND_TEXTURE_HPP
#define KAZE_BAND_TEXTURE_HPP

#include "types.h"
#include "texture.hpp"

namespace kaze
{

    template <class T>
    class band_texture : public texture<T>
    {
    public:
        static inline bool is_powerof2(int x)
        {
            return ((x - 1) & x) == 0;
        }

    public:
        T get(const sufflight& suf) const
        {
            return get(suf.coord()[0]);
        }
        virtual T get(real u) const = 0;
    };
}

#endif