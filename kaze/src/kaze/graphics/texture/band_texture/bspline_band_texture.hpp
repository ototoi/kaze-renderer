#ifndef KAZE_BSPLINE_BAND_TEXTURE_HPP
#define KAZE_BSPLINE_BAND_TEXTURE_HPP

#include "band_texture.hpp"
#include "bspline.h"

#include <vector>

namespace kaze
{

    template <class T, int Sz>
    struct bspline_band_texture_evaluator
    {
        static inline T evaluate(const T knot[], int nknots, real x)
        {
            return bspline_evaluate(knot, nknots, x);
        }
    };

    template <class T>
    struct bspline_band_texture_evaluator<T, 4>
    {
        static inline T evaluate(const T knot[], int nknots, real x)
        {
            return bspline4_evaluate(knot, nknots, x);
        }
    };

    template <class T, int Sz = 4>
    class bspline_band_texture : public band_texture<T>
    {
    public:
        bspline_band_texture() {}
        bspline_band_texture(const std::vector<T>& v) : mv_(v) {}
        bspline_band_texture(const T v[], int nsz) : mv_(v, v + nsz) {}

        void add(const T& rhs) { mv_.push_back(rhs); }
        size_t size() const { return mv_.size(); }
    public:
        T get(real u) const
        {
            assert(mv_.size() >= 3); //
            return bspline_band_texture_evaluator<T, Sz>::evaluate(&mv_[0], mv_.size(), u);
        }

    private:
        std::vector<T> mv_;
    };
}

#endif