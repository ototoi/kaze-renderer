#ifndef KAZE_UNIT_BAND_TEXTURE_HPP
#define KAZE_UNIT_BAND_TEXTURE_HPP

#include "band_texture.hpp"
#include "count_ptr.hpp"
#include "band.hpp"
#include "memory_band.hpp"
#include "corresponder.h"
#include "band_interpolator.hpp"

#include <vector>

namespace kaze
{

    template <class T>
    class unit_band_texture : public band_texture<T>
    {
    public:
        static band_interpolator<T>* default_interpolator() { return new mitchell_band_interpolator<T>(); }
        static corresponder* default_corresponder() { return new powerof2_repeat_corresponder(); }
    public:
        unit_band_texture(
            const auto_count_ptr<band<T> >& bnd,
            const auto_count_ptr<corresponder>& cor = default_corresponder(),
            const auto_count_ptr<band_interpolator<T> >& inp = default_interpolator()) : bnd_(bnd), cor_(cor), inp_(inp)
        {
            init();
        }
        unit_band_texture(
            const std::vector<T>& v,
            const auto_count_ptr<corresponder>& cor = default_corresponder(),
            const auto_count_ptr<band_interpolator<T> >& inp = default_interpolator()) : bnd_(std::shared_ptr<band<T> >(new memory_band<T>(v))),
                                                                                         cor_(cor),
                                                                                         inp_(inp)
        {
            init();
        }

    public:
        size_t size() const { return bnd_.get_width(); }
    public:
        T get(real u) const
        {
            return inp_->get(*bnd_, *cor_, u);
        }

    protected:
        void init()
        {
            if (!is_powerof2(bnd_->get_width()))
            {
                cor_.reset(cor_->clone_powerof2());
            }
        }

    private:
        std::shared_ptr<band<T> > bnd_;
        std::shared_ptr<corresponder> cor_;
        std::shared_ptr<band_interpolator<T> > inp_;
    };
}

#endif
