#ifndef KAZE_DEBUG_TEXTURE_HPP
#define KAZE_DEBUG_TEXTURE_HPP

#include "texture.hpp"
#include "color_traits.hpp"

#include "count_ptr.hpp"

#include "logger.h"

#include <string>

namespace kaze
{

    template <class T>
    class debug_texture : public texture<T>
    {
    public:
        T get(const sufflight& suf) const
        {
            if (ptr_.get())
            {
                return ptr_->get(suf);
            }
            else
            {
                return color_traits<T>::zero();
            }
        }
        //-----------------------------------------------------------------
        void print_constructor()
        {
#ifdef _DEBUG
            print_debug("debug_texture::debug_texture - %s , %d\n", name_.c_str(), ptr_.count());
#else
            print_debug("debug_texture::debug_texture - %s\n", name_.c_str());
#endif
        }
        void print_destructor()
        {
#ifdef _DEBUG
            print_debug("debug_texture::~debug_texture - %s , %d\n", name_.c_str(), ptr_.count());
#else
            print_debug("debug_texture::~debug_texture - %s\n", name_.c_str());
#endif
        }

        //-----------------------------------------------------------------

        debug_texture()
        {
            print_constructor();
        }
        debug_texture(const char* name)
            : name_(name)
        {
            print_constructor();
        }
        debug_texture(const auto_count_ptr<texture<T> >& p)
            : ptr_(p)
        {
            print_constructor();
        }
        debug_texture(const char* name, const auto_count_ptr<texture<T> >& p)
            : name_(name), ptr_(p)
        {
            print_constructor();
        }
        //-----------------------------------------------------------------
        ~debug_texture()
        {
            print_destructor();
        }

    private:
        std::string name_;
        std::shared_ptr<texture<T> > ptr_;
    };
}

#endif