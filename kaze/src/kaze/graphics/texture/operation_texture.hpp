#ifndef KAZE_OPERATION_TEXTURE_HPP
#define KAZE_OPERATION_TEXTURE_HPP

#include "texture.hpp"
#include "count_ptr.hpp"

#include <vector>
#include <functional>
#include <algorithm>

namespace kaze
{

    template <class T, class F = std::plus<T> >
    class operation_texture : public texture<T>
    {
    public:
        operation_texture() {}
        void add(const auto_count_ptr<texture<T> >& t)
        {
            mv_.push_back(t);
        }
        size_t size() const
        {
            return mv_.size();
        }

    public:
        T get(const sufflight& suf) const
        {
            if (mv_.empty())
            {
                return T();
            }
            else
            {
                size_t sz = mv_.size();
                T tmp = mv_[0]->get(suf);
                for (size_t i = 1; i < sz; i++)
                {
                    tmp = f_(tmp, mv_[i]->get(suf));
                }
                return tmp;
            }
        }

    protected:
        std::vector<std::shared_ptr<texture<T> > > mv_;
        F f_;
    };

#define DEF_TEXTURE(NAME, OP)                                                                                                                                             \
    \
template<class T> \
class NAME##_texture : public operation_texture<T, OP>{\
public :                                                                                                                                                                  \
                                                           NAME##_texture(){} NAME##_texture(const auto_count_ptr<texture<T> >& a, const auto_count_ptr<texture<T> >& b){ \
                                                               add(a);                                                                                                    \
    add(b);                                                                                                                                                               \
    }                                                                                                                                                                     \
    \
}                                                                                                                                                                  \
    ;

    DEF_TEXTURE(add, std::plus<T>)
    DEF_TEXTURE(sub, std::minus<T>)
    DEF_TEXTURE(mul, std::multiplies<T>)
//DEF_TEXTURE(div,std::divides<T>)
//DEF_TEXTURE(min,std::min<T>)
//DEF_TEXTURE(max,std::max<T>)

#undef DEF_TEXTURE
}

#endif
