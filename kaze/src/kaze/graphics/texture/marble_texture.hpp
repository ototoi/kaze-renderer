#ifndef KAZE_MARBLE_TEXTURE_HPP
#define KAZE_MARBLE_TEXTURE_HPP

#include "texture.hpp"
#include "base_noise_texture.h"
#include "catmull_rom_band_texture.hpp"
#include "count_ptr.hpp"

#include <vector>
#include <algorithm>

namespace kaze
{

    template <class T>
    class marble_texture : public texture<T>
    {
    public:
        marble_texture(const T v[], int nsz, real freq = 8, int nOctave = 4) : bnd_(std::shared_ptr<band_texture<T> >(new catmull_rom_band_texture<T>(v, nsz))),
                                                                               freq_(freq), nOctave_(nOctave) {}
        marble_texture(const std::vector<T>& v, real freq = 8, int nOctave = 4) : bnd_(std::shared_ptr<band_texture<T> >(new catmull_rom_band_texture<T>(v))),
                                                                                  freq_(freq), nOctave_(nOctave) {}
        marble_texture(const auto_count_ptr<band_texture<T> >& bnd, real freq = 8, int nOctave = 4) : bnd_(bnd),
                                                                                                      freq_(freq), nOctave_(nOctave) {}

        T get(const sufflight& suf) const
        {
            vector3 c = suf.coord();
            real freq = freq_;
            return get_value(freq * c[0], freq * c[1], freq * c[2]);
        }

    protected:
        T get_value(real u, real v, real w) const
        {
            int nOct = nOctave_;
            real marble = 0;
            real f = 1;
            for (int i = 0; i < nOct; i++)
            {
                marble += noise_marble(u, v, w, f);
                f *= real(2.17);
            }
            return marble_color(marble);
        }
        real noise_marble(real u, real v, real w, real f) const
        {
            return nz_.noise(f * u, f * v, f * w) / f;
        }
        T marble_color(real m) const
        {
            m = clamp___(0, 2 * m + 0.5, 1);
            return bnd_->get(m);
        }

    private:
        static real clamp___(real a, real x, real b)
        {
            return std::max<real>(a, std::min<real>(x, b));
        }

    private:
        base_noise_texture nz_;
        std::shared_ptr<band_texture<T> > bnd_;
        real freq_;
        int nOctave_;
    };
}

#endif