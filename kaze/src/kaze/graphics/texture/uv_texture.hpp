#ifndef KAZE_UV_TEXTURE_HPP
#define KAZE_UV_TEXTURE_HPP

#include "texture.hpp"
#include "constant_texture.hpp"
#include "count_ptr.hpp"

namespace kaze
{

    template <class T>
    class uv_texture : public texture<T>
    {
    public:
        uv_texture(
            const auto_count_ptr<texture<T> >& uv00,
            const auto_count_ptr<texture<T> >& uv10,
            const auto_count_ptr<texture<T> >& uv01,
            const auto_count_ptr<texture<T> >& uv11) : uv00_(uv00), uv10_(uv10), uv01_(uv01), uv11_(uv11) {}

        uv_texture(
            const T& uv00,
            const T& uv10,
            const T& uv01,
            const T& uv11) : uv00_(new constant_texture<T>(uv00)),
                             uv10_(new constant_texture<T>(uv10)),
                             uv01_(new constant_texture<T>(uv01)),
                             uv11_(new constant_texture<T>(uv11)) {}

        T get(const sufflight& suf) const
        {
            vector3 c = suf.coord();
            real u = c[0];
            real v = c[1];

            u = u - floor(u);
            v = v - floor(v);

            return mix(u, v, uv00_->get(suf), uv10_->get(suf), uv01_->get(suf), uv11_->get(suf));
        }

    protected:
        T mix(real u, real v, const T& c00, const T& c10, const T& c01, const T& c11)
        {
            real iu = 1 - u;
            real iv = 1 - v;

            return iv * (iu * c00 + u * c10) + v * (iu * c01 + u * c11);
        }

    protected:
        std::shared_ptr<texture<T> > uv00_;
        std::shared_ptr<texture<T> > uv10_;
        std::shared_ptr<texture<T> > uv01_;
        std::shared_ptr<texture<T> > uv11_;
    };
}

#endif