#ifndef KAZE_MIX_TEXTURE_HPP
#define KAZE_MIX_TEXTURE_HPP

#include "texture.hpp"
#include "count_ptr.hpp"
#include "constant_texture.hpp"

namespace kaze
{

    template <class T>
    class mix_texture : public texture<T>
    {
    public:
        mix_texture(const auto_count_ptr<texture<T> >& a, const auto_count_ptr<texture<T> >& b, double alpha = 0.5)
            : a_(a), b_(b), alpha_(new constant_texture<real>(alpha)) {}
        mix_texture(const auto_count_ptr<texture<T> >& a, const auto_count_ptr<texture<T> >& b, const auto_count_ptr<texture<real> >& alpha)
            : a_(a), b_(b), alpha_(alpha) {}

        T get(const sufflight& suf) const
        {
            return mix(
                a_->get(suf),
                b_->get(suf),
                alpha_->get(suf));
        }

    protected:
        T mix(const T& a, const T& b, real alpha) const
        {
            return (1 - alpha) * a + (alpha)*b;
        }

    protected:
        std::shared_ptr<texture<T> > a_;
        std::shared_ptr<texture<T> > b_;
        std::shared_ptr<texture<real> > alpha_;
    };
}

#endif