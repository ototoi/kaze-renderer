#ifndef KAZE_BAND_HPP
#define KAZE_BAND_HPP

namespace kaze
{

    template <class T>
    class band
    {
    public:
        typedef T value_type;

    public:
        virtual ~band() {}
        virtual T get(int x) const = 0;
        virtual void set(int x, const T& t) = 0;
        virtual int get_width() const = 0;
    };
}

#endif