#ifndef KAZE_MIPMAP_BAND_HPP
#define KAZE_MIPMAP_BAND_HPP

#include "readonly_band.hpp"

namespace kaze
{

    template <class T>
    class mipmap_band : public read_only_band<T>
    {
    public:
    };
}

#endif