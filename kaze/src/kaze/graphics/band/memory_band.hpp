#ifndef KAZE_MEMORY_BAND_HPP
#define KAZE_MEMORY_BAND_HPP

#include "types.h"
#include "color.h"
#include "band.hpp"
#include <vector>
#include <cassert>

namespace kaze
{

    template <class T>
    class memory_band : public band<T>
    {
    public:
        memory_band(int w) : v_(w) {}
        memory_band(const std::vector<T>& v) : v_(v) {}
        int get_width() const { return (int)v_.size(); }
        T get(int x) const
        {
            size_t idx = (size_t)x;
            assert(idx < v_.size());
            return v_[idx];
        }

        void set(int x, const T& t)
        {
            size_t idx = (size_t)x;
            assert(idx < v_.size());
            v_[idx] = t;
        }

        T* ptr() { return &(v_[0]); }
        const T* ptr() const { return &(v_[0]); }
    private:
        std::vector<T> v_;
    };

    typedef memory_band<real> real_memory_band;
    typedef memory_band<color1> color1_memory_band;
    typedef memory_band<color2> color2_memory_band;
    typedef memory_band<color3> color3_memory_band;
    typedef memory_band<color3> color4_memory_band;
}

#endif
