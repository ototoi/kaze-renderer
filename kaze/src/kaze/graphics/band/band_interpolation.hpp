#ifndef KAZE_BAND_INTERPOLATION_HPP
#define KAZE_BAND_INTERPOLATION_HPP

#include "types.h"
#include "band.hpp"
#include "corresponder.h"
#include <cmath>

namespace kaze
{

    template <class T>
    T c0_interpolation(const band<T>& tex, real x)
    {
        int w = tex.get_width();

        real rx = x * w;
        real ix = floor(rx);
        int xx = (int)ix;

        return tex.get(xx);
    }

    template <class T>
    T c0_interpolation(const band<T>& tex, const corresponder& cor, real x)
    {
        int w = tex.get_width();

        real rx = x * w;
        real ix = floor(rx);
        int xx = (int)ix;

        xx = cor.get(xx, w);

        return tex.get(xx);
    }

    template <class T, class F>
    T c1_interpolation(const band<T>& tex, real x)
    {
        int w = tex.get_width();

        real rx = x * w;
        rx -= real(0.5); //ex. 0.2-> -1 ... 0

        real ix = floor(rx);

        real u = rx - ix;
        real iu = 1 - u;

        int xx = (int)ix;

        T tmp[2];

        tmp[0] = tex.get(xx);
        tmp[1] = tex.get(xx + 1);

        u = F::static_get(u);
        iu = F::static_get(iu);

        return u * tmp[0] + iu * tmp[1];
    }

    template <class T, class F>
    T c1_interpolation(const band<T>& tex, const corresponder& cor, real x)
    {
        int w = tex.get_width();

        real rx = x * w;
        rx -= real(0.5); //ex. 0.2-> -1 ... 0

        real ix = floor(rx);

        real u = rx - ix;
        real iu = 1 - u;

        int xx = (int)ix;

        int xp = xx + 1;

        xx = cor.get(xx, w);
        xp = cor.get(xp, w);

        T tmp[2];

        tmp[0] = tex.get(xx);
        tmp[1] = tex.get(xp);

        u = F::static_get(u);
        iu = F::static_get(iu);

        return u * tmp[0] + iu * tmp[1];
    }

    template <class T>
    inline T sum_band_pixel(const T in[4])
    {
        return in[0] + in[1] + in[2] + in[3];
    }

    template <class T, class F>
    T c2_interpolation(const band<T>& tex, real x)
    {
        int w = tex.get_width();

        real rx = x * w;
        rx -= real(0.5);

        real ix = floor(rx);

        int xx = (int)ix; //-1

        int xd[4];
        for (int i = 0; i < 4; i++)
        {
            xd[i] = xx - 1 + i; //cor.get(xx-1+i,w);
        }

        real weight_x[4];
        for (int i = 0; i < 4; i++)
        {
            int p = i - 1;
            real sx = xx + p;
            real wx = fabs(rx - sx); //
            wx = F::static_get(wx);

            weight_x[i] = wx;
        }

        T tmp[4];

        for (int i = 0; i < 4; i++)
        {                                          //x
            tmp[i] = weight_x[i] * tex.get(xd[i]); //-1 ,  0,  1,  2
        }

        return sum_band_pixel<T>(tmp);
    }

    template <class T, class F>
    T c2_interpolation(const band<T>& tex, const corresponder& cor, real x)
    {
        int w = tex.get_width();

        real rx = x * w;
        rx -= real(0.5);

        real ix = floor(rx);

        int xx = (int)ix; //-1

        int xd[4];
        for (int i = 0; i < 4; i++)
        {
            xd[i] = cor.get(xx - 1 + i, w);
        }

        real weight_x[4];
        for (int i = 0; i < 4; i++)
        {
            int p = i - 1;
            real sx = xx + p;
            real wx = fabs(rx - sx); //
            wx = F::static_get(wx);

            weight_x[i] = wx;
        }

        T tmp[4];

        for (int i = 0; i < 4; i++)
        {                                          //x
            tmp[i] = weight_x[i] * tex.get(xd[i]); //-1 ,  0,  1,  2
        }

        return sum_band_pixel<T>(tmp);
    }
}

#endif
