#ifndef KAZE_BAND_INTERPOLATOR_HPP
#define KAZE_BAND_INTERPOLATOR_HPP

#include "band.hpp"
#include "band_interpolation.hpp"
#include "corresponder.h"
#include "filter.h"

namespace kaze
{

    template <class T>
    class band_interpolator
    {
    public:
        virtual ~band_interpolator() {}
    public:
        virtual T get(const band<T>& bnd, real x) const = 0;
        virtual T get(const band<T>& bnd, const corresponder& cor, real x) const = 0;
    };

    template <class T>
    class c0_band_interpolator : public band_interpolator<T>
    {
    public:
        T get(const band<T>& bnd, real x) const { return static_get(bnd, x); }
        T get(const band<T>& bnd, const corresponder& cor, real x) const { return static_get(bnd, cor, x); }
        static T static_get(const band<T>& bnd, real x) { return c0_interpolation(bnd, x); }
        static T static_get(const band<T>& bnd, const corresponder& cor, real x) { return c0_interpolation(bnd, cor, x); }
    };

    template <class T, class F>
    class c1_band_interpolator : public band_interpolator<T>
    {
    public:
        T get(const band<T>& bnd, real x) const { return static_get(bnd, x); }
        T get(const band<T>& bnd, const corresponder& cor, real x) const { return static_get(bnd, cor, x); }
        static T static_get(const band<T>& bnd, real x) { return c1_interpolation<T, F>(bnd, x); }
        static T static_get(const band<T>& bnd, const corresponder& cor, real x) { return c1_interpolation<T, F>(bnd, cor, x); }
    };

    template <class T, class F>
    class c2_band_interpolator : public band_interpolator<T>
    {
    public:
        T get(const band<T>& bnd, real x) const { return static_get(bnd, x); }
        T get(const band<T>& bnd, const corresponder& cor, real x) const { return static_get(bnd, cor, x); }
        static T static_get(const band<T>& bnd, real x) { return c2_interpolation<T, F>(bnd, x); }
        static T static_get(const band<T>& bnd, const corresponder& cor, real x) { return c2_interpolation<T, F>(bnd, cor, x); }
    };

    template <class T>
    class nearest_band_interpolator : public c0_band_interpolator<T>
    {
    };

    template <class T>
    class bilenear_band_interpolator : public c1_band_interpolator<T, triangle_filter>
    {
    };

    template <class T>
    class hermit_band_interpolator : public c1_band_interpolator<T, hermite_filter>
    {
    };

    template <class T>
    class bicubic_band_interpolator : public c2_band_interpolator<T, bicubic_filter>
    {
    };

    template <class T>
    class bspline_band_interpolator : public c2_band_interpolator<T, bspline_filter>
    {
    };

    template <class T>
    class mitchell_band_interpolator : public c2_band_interpolator<T, mitchell_filter>
    {
    };

    template <class T>
    class lagrange_band_interpolator : public c2_band_interpolator<T, lagrange_filter>
    {
    };
}

#endif
