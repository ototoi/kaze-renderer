#ifndef KAZE_READONLY_BAND_HPP
#define KAZE_READONLY_BAND_HPP

#include "band.hpp"
#include <cassert>

namespace kaze
{

    template <class T>
    class readonly_band
    {
    public:
        void set(int x, const T& t) { assert(0); };
    };
}

#endif
