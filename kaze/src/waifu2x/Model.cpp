#include "Model.h"

#include "picojson.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include <iostream>
#include <fstream>

namespace waifu2x
{
    namespace
    {
        struct TmpModel
        {
            int nInputPlanes;
            int nOutputPlanes;
            std::vector<std::shared_ptr<Mat> > weights;
            std::vector<float> biases;
        };
    }

    //
    Mat::Mat(int row, int col)
        : row_(row), col_(col)
    {
        mat_.resize(row * col);
    }

    float* Mat::get_ptr()
    {
        return &mat_[0];
    }

    static bool loadModelFromJSONObject_(TmpModel& model, picojson::object& obj)
    {
        int nInputPlanes = static_cast<int>(obj["nInputPlane"].get<double>());
        int nOutputPlanes = static_cast<int>(obj["nOutputPlane"].get<double>());
        int kW = static_cast<int>(obj["kW"].get<double>());
        int kH = static_cast<int>(obj["kH"].get<double>());
        if (kW != kH) return false;
        if (kW != 3) return false;
        if (kH != 3) return false;

        std::vector<std::shared_ptr<Mat> > weights;
        weights.reserve(nOutputPlanes * nInputPlanes);
        std::vector<float> biases(nOutputPlanes);

        picojson::array& wOutputPlane = obj["weight"].get<picojson::array>();

        assert(wOutputPlane.size() == nOutputPlanes);

        for (size_t i = 0; i < nOutputPlanes; i++)
        {
            picojson::array& wInputPlane = wOutputPlane[i].get<picojson::array>();
            for (size_t j = 0; j < nInputPlanes; j++)
            {
                std::vector<float> vecData(kW * kH);
                picojson::array& weightMat = wInputPlane[j].get<picojson::array>();
                for (int row = 0; row < kH; row++)
                {
                    picojson::array& weightMatRow = weightMat[row].get<picojson::array>();
                    for (int col = 0; col < kW; col++)
                    {
                        vecData[row * kW + col] = (float)weightMatRow[col].get<double>();
                    }
                }
                std::shared_ptr<Mat> mat(new Mat(kH, kW));
                memcpy(mat->get_ptr(), &vecData[0], sizeof(float) * kH * kW);
                weights.push_back(mat);
            }
        }

        picojson::array& biasesData = obj["bias"].get<picojson::array>();
        for (size_t i = 0; i < nOutputPlanes; i++)
        {
            biases[i] = (float)biasesData[i].get<double>();
        }

        model.nInputPlanes = nInputPlanes;
        model.nOutputPlanes = nOutputPlanes;
        model.weights.swap(weights);
        model.biases.swap(biases);

        return true;
    }

    bool ModelBuilder::LoadModelFromJSONObject(std::vector<std::shared_ptr<Model> >& models, const char* szFileName)
    {
        std::ifstream ifs(szFileName);
        if (!ifs) return false;

        picojson::value jsonValue;
        ifs >> jsonValue;
        std::string errMsg = picojson::get_last_error();
        if (!errMsg.empty()) return false;
        picojson::array& objectArray = jsonValue.get<picojson::array>();
        for (int i = 0; i < objectArray.size(); i++)
        {
            TmpModel tmpModel;
            if (loadModelFromJSONObject_(tmpModel, objectArray[i].get<picojson::object>()))
            {
                std::shared_ptr<Model> pModel(new Model());
                pModel->nInputPlanes = tmpModel.nInputPlanes;
                pModel->nOutputPlanes = tmpModel.nOutputPlanes;
                pModel->weights.swap(tmpModel.weights);
                pModel->biases.swap(tmpModel.biases);
                models.push_back(pModel);
            }
        }

        return true;
    }

    bool ModelBuilder::LoadModelFromBinObject(std::vector<std::shared_ptr<Model> >& models, const char* szFileName)
    {
        static const int kWidthSize = 3;
        static const int kHeightSize = 3;
        std::ifstream ifs(szFileName, std::ios::in | std::ios::binary);
        if (!ifs) return false;
        int nInputPlanes = 1;
        int nOutputPlanes = 1;
        int nModelSize = 7;
        for (int m = 0; m < nModelSize; m++)
        {
            TmpModel tmpModel;
            ifs.read(reinterpret_cast<char*>(&nInputPlanes), sizeof(int));
            ifs.read(reinterpret_cast<char*>(&nOutputPlanes), sizeof(int));

            //printf("%d, %d \n", nInputPlanes, nOutputPlanes);

            tmpModel.nInputPlanes = nInputPlanes;
            tmpModel.nOutputPlanes = nOutputPlanes;
            for (int o = 0; o < nOutputPlanes; o++)
            {
                for (int i = 0; i < nInputPlanes; i++)
                {
                    float matbuffer[kWidthSize * kHeightSize] = {};
                    ifs.read(reinterpret_cast<char*>(matbuffer), sizeof(float) * kWidthSize * kHeightSize);
                    std::shared_ptr<Mat> pMat(new Mat(kWidthSize, kHeightSize));
                    memcpy(pMat->GetPtr(), matbuffer, sizeof(float) * kWidthSize * kHeightSize);
                    tmpModel.weights.push_back(pMat);
                }
            }
            tmpModel.biases.resize(nOutputPlanes);
            ifs.read(reinterpret_cast<char*>(&(tmpModel.biases[0])), sizeof(float) * nOutputPlanes);

            std::shared_ptr<Model> pModel(new Model());
            pModel->nInputPlanes = tmpModel.nInputPlanes;
            pModel->nOutputPlanes = tmpModel.nOutputPlanes;
            pModel->weights.swap(tmpModel.weights);
            pModel->biases.swap(tmpModel.biases);
            models.push_back(pModel);
        }

        return true;
    }
}
