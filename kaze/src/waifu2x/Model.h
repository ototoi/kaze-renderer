#ifndef WAIFU2X_MODEL_H
#define WAIFU2X_MODEL_H

#include <vector>

namespace waifu2x
{
    class Mat
    {
    public:
        Mat(int row, int col);
        float* get_ptr();
        float* GetPtr() { return &mat_[0]; }
        const float* GetPtr() const { return &mat_[0]; }
    protected:
        std::vector<float> mat_;
        int row_;
        int col_;
    };

    class Model
    {
    public:
        friend class ModelBuilder;

        int GetInputPlanes() const { return nInputPlanes; }
        int GetOutputPlanes() const { return nOutputPlanes; }
        const std::vector<std::shared_ptr<Mat> >& GetWeights() const { return weights; }
        const std::vector<float>& GetBiases() const { return biases; }
    private:
        int nInputPlanes;
        int nOutputPlanes;
        std::vector<std::shared_ptr<Mat> > weights;
        std::vector<float> biases;
    };

    class ModelBuilder
    {
    public:
        static bool LoadModelFromJSONObject(std::vector<std::shared_ptr<Model> >& models, const char* szFileName);
        static bool LoadModelFromBinObject(std::vector<std::shared_ptr<Model> >& models, const char* szFileName);
    };
}

#endif