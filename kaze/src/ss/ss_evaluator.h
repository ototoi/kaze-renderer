#ifndef SS_EVALUATOR_H
#define SS_EVALUATOR_H

#include "ss_scene.h"
#include "ss_context.h"

namespace ss
{
    class ss_evaluator
    {
    public:
        static int run(ss_scene* scn, const ss_context* ctx);
    };
}

#endif
