#ifndef SS_COLORPOOL_H
#define SS_COLORPOOL_H

#include <vector>
#include "ss_random.h"

namespace ss
{
    class ss_colorpool_strategy;
    class ss_colorpool
    {
    public:
        ss_colorpool();
        ~ss_colorpool();

    public:
        void set_scheme(const std::string& scheme);
        void get_color(float col[3], const ss_random* rnd) const;

    protected:
        ss_colorpool_strategy* strategy_;
    };
}

#endif