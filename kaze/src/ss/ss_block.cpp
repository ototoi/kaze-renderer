#include "ss_block.h"
#include "ss_action.h"

namespace ss
{
    ss_block::ss_block()
    {
        times_ = 1;
    }

    ss_block::ss_block(int times)
    {
        times_ = times;
    }

    ss_block::~ss_block()
    {
        ;
    }

    void ss_block::add_action(const ss_action* act)
    {
        actions_.push_back(act);
    }

    void ss_blocklist::add_block(const ss_block* blk)
    {
        blocks_.push_back(blk);
    }
}

