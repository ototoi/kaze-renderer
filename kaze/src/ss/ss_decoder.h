
#ifndef SS_DECODER_H
#define SS_DECODER_H

#include <iostream>
#include <sstream>

namespace ss
{
    class ss_decoder
    {
    public:
        ss_decoder(std::istream& is);

    public:
        int read(char* buffer, unsigned int size);

    protected:
        std::istream& is_;
    };
}

#endif
