#ifndef SS_PREPROCESSOR_H
#define SS_PREPROCESSOR_H

#include <vector>
#include <string>
#include <iostream>

namespace ss
{
    class ss_preprocessor
    {
    public:
        static int run(std::ostream& os, std::istream& is);
    };
}

#endif
