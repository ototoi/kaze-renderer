#include "ss_options.h"


namespace ss
{
    ss_options::ss_options()
    {
        maxdepth_ = 1000;
        maxobjects_ = 10000;
        minsize_ = 0.01f;
        maxsize_ = 10000.0f;
        seed_ = 123456;
        static const float col_[3] = {0, 0, 0};
        memcpy(background_, col_, sizeof(float) * 3);
    }

    ss_options::ss_options(const ss_options& opt)
    {
        maxdepth_ = opt.maxdepth_;
        maxobjects_ = opt.maxobjects_;
        minsize_ = opt.minsize_;
        maxsize_ = opt.maxsize_;
        seed_ = opt.seed_;
        memcpy(background_, opt.background_, sizeof(float) * 3);
    }

    void ss_options::set_background(const float v[3])
    {
        float r = v[0];
        float g = v[1];
        float b = v[2];
        set_background(r, g, b);
    }

    void ss_options::set_background(unsigned int nRGB)
    {
        float r = (nRGB & 0xFF0000) / 255.0f;
        float g = (nRGB & 0x00FF00) / 255.0f;
        float b = (nRGB & 0x0000FF) / 255.0f;
        set_background(r, g, b);
    }

    void ss_options::set_background(float r, float g, float b)
    {
        background_[0] = r;
        background_[1] = g;
        background_[2] = b;
    }
}
