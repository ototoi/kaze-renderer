#ifndef SS_BLOCK_H
#define SS_BLOCK_H

#include <vector>
#include "ss_object.h"

namespace ss
{
    class ss_action;
    class ss_block : public ss_object
    {
    public:
        ss_block();
        ss_block(int times);
        ~ss_block();
        void add_action(const ss_action* act);
        const std::vector<const ss_action*>& get_actions() const { return actions_; }
    public:
        void set_times(int times) { times_ = times; }
        int get_times() const { return times_; }
    protected:
        int times_;
        std::vector<const ss_action*> actions_;
    };

    class ss_blocklist : public ss_object
    {
    public:
        void add_block(const ss_block* blk);
        const std::vector<const ss_block*>& get_list() const { return blocks_; }
    protected:
        std::vector<const ss_block*> blocks_;
    };
}


#endif
