#include "ss_context_loader.h"

namespace ss
{
    int ss_context_loader::run(ss_context* ctx, ss_decoder* dec)
    {
        return load_structure_synth(ctx, dec);
    }
}

