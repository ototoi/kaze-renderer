#ifndef SS_COLOR_H
#define SS_COLOR_H

namespace ss
{
    class ss_color
    {
    public:
        static int eval_code(const char* szText);
        static int eval_name(const char* szText);

    public:
        static int eval_random(unsigned int r);

    public:
        static void int_to_float(float rgb[3], unsigned int c);

    public:
        static void hsv_to_rgb(float rgb[3], const float hsv[3]);
        static void rgb_to_hsv(float rgb[3], const float hsv[3]);
    };
}


#endif