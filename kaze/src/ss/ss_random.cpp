#include "ss_random.h"
#include <time.h>
#include <limits>

namespace ss
{
    static const unsigned int DEFAULT_SEED = 2463534242;

    static unsigned int xor32(unsigned int y)
    {
        y = y ^ (y << 13);
        y = y ^ (y >> 17);
        return y = y ^ (y << 5);
    }

    ss_random::ss_random()
    {
        seed_ = DEFAULT_SEED ^ (unsigned int)time(NULL);
    }

    ss_random::ss_random(unsigned int seed)
    {
        seed_ = seed;
    }

    void ss_random::set_seed(unsigned int seed)
    {
        seed_ = seed;
    }

    unsigned int ss_random::get() const
    {
        return seed_ = xor32(seed_);
    }

    float ss_random::get_float() const
    {
        return float(this->get()) / std::numeric_limits<unsigned int>::max();
    }
}
