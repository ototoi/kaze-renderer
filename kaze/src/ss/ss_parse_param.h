#ifndef SS_PARSE_PARAM_H
#define SS_PARSE_PARAM_H

namespace ss
{
    class ss_context;
    class ss_decoder;

    struct ss_parse_param
    {
        ss_context* ctx;
        ss_decoder* dec;
        int is_in_block;
        int line_number;
    };
}

#endif
