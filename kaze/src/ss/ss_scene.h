#ifndef SS_SCENE_H
#define SS_SCENE_H

#include "ss_geometry.h"
#include <vector>

namespace ss
{
    class ss_scene
    {
    public:
        ss_scene();
        ~ss_scene();
        void add_geometry(ss_geometry* geo);
        size_t get_geometry_size() const;
        const ss_geometry* get_geometry_at(int i) const;

    protected:
        std::vector<ss_geometry*> geos_;
    };
}

#endif
