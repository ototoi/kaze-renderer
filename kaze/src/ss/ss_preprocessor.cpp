#include "ss_preprocessor.h"

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <sstream>
#include <string.h>

namespace ss
{
    namespace
    {
        static std::string TrimLeft(const std::string& s)
        {
            const char* c = s.c_str();
            while (*c)
            {
                if (!isspace(*c))
                {
                    return std::string(c);
                }
                c++;
            }
            return "";
        }

        static std::string TrimRight(const std::string& s)
        {
            std::vector<char> v(s.begin(), s.end());
            v.push_back(0);
            int ci = (int)v.size() - 1;
            while (0 <= ci)
            {
                if (!isspace(v[ci]))
                {
                    v[ci + 1] = 0;
                    return std::string(&v[0]);
                }
                ci--;
            }
            return "";
        }

        static std::string Trim(const std::string& s)
        {
            return TrimLeft(TrimRight(s));
        }

        static std::string Replace_(const std::string& s, const std::string& k, const std::string& v)
        {
            if (s.empty()) return s;

            std::string::size_type pos = s.find(k);
            if (pos != std::string::npos)
            {
                return s.substr(0, pos) + v + Replace_(s.substr(pos + k.size()), k, v);
            }
            else
            {
                return s;
            }
        }

        class PragmaManager
        {
        public:
            bool Define(const std::string& line)
            {
                const char* def = line.c_str();
                while (*def && isspace(*def))
                    def++;
                if ((def = strstr(line.c_str(), "#")) != NULL)
                {
                    //std::cout << "@1:" << def << std::endl;

                    def++;
                    while (*def && isspace(*def))
                        def++;
                    if ((def = strstr(def, "define")) != NULL)
                    {
                        //std::cout << "@2:" << def << std::endl;

                        def = def + strlen("define") + 1;
                        if (*def != 0)
                        {
                            //std::cout << "@3:" << def << std::endl;

                            std::string strKey;
                            std::stringstream ss(def);
                            ss >> strKey;
                            if (!strKey.empty())
                            {
                                //std::cout << "@4:" << strKey << std::endl;

                                if ((def = strstr(def, strKey.c_str())) != NULL)
                                {
                                    def = def + strlen(strKey.c_str()) + 1;
                                    if (*def != 0)
                                    {
                                        //std::cout << "@5:" << def << std::endl;

                                        std::string strValue = Trim(def); //clip
                                        if (!strValue.empty())
                                        {
                                            kv_.push_back(std::make_pair(strKey, strValue));
                                            //std::cout << strKey << ":" << strValue << std::endl;
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                return false;
            }
            std::string Replace(const std::string& line)
            {
                std::string s = line;
                typedef std::vector<std::pair<std::string, std::string> > map_type;
                for (map_type::const_reverse_iterator it = kv_.rbegin(); it != kv_.rend(); ++it)
                {
                    if (strstr(s.c_str(), it->first.c_str()) != NULL)
                    {
                        s = Replace_(s, it->first, it->second);
                    }
                }
                return s;
            }

        protected:
            std::vector<std::pair<std::string, std::string> > kv_;
        };
    }

    int ss_preprocessor::run(std::ostream& os, std::istream& is)
    {
        PragmaManager pm;

        std::string line;
        while (std::getline(is, line))
        {
            if (!pm.Define(line))
            {
                line = pm.Replace(line);
                os << line << std::endl;
            }
            else
            {
                os << std::endl;
            }
        }
        return 0;
    }
}

