#ifndef SS_CONTEXT_LOADER_H
#define SS_CONTEXT_LOADER_H

#include "ss_context.h"
#include "ss_decoder.h"

namespace ss
{
    int load_structure_synth(ss_context* ctx, ss_decoder* dec);

    class ss_context_loader
    {
    public:
        static int run(ss_context* ctx, ss_decoder* dec);
    };
}

#endif
