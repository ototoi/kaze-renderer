#include "ss_action.h"
#include "ss_block.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sstream>

namespace ss
{
    ss_x_action::ss_x_action(float v)
        : v_(v)
    {
    }

    ss_y_action::ss_y_action(float v)
        : v_(v)
    {
    }

    ss_z_action::ss_z_action(float v)
        : v_(v)
    {
    }

    //------------------------------------------------

    ss_rx_action::ss_rx_action(float v)
        : v_(v)
    {
    }

    ss_ry_action::ss_ry_action(float v)
        : v_(v)
    {
    }

    ss_rz_action::ss_rz_action(float v)
        : v_(v)
    {
    }

    //------------------------------------------------

    ss_s_action::ss_s_action(float v)
    {
        v_[0] = v;
        v_[1] = v;
        v_[2] = v;
    }

    ss_s_action::ss_s_action(float x, float y, float z)
    {
        v_[0] = x;
        v_[1] = y;
        v_[2] = z;
    }

    //------------------------------------------------

    ss_m_action::ss_m_action(const float m[9])
    {
        memcpy(m_, m, sizeof(float) * 9);
    }

    //------------------------------------------------

    ss_fx_action::ss_fx_action(float v)
        : v_(v)
    {
    }

    ss_fy_action::ss_fy_action(float v)
        : v_(v)
    {
    }

    ss_fz_action::ss_fz_action(float v)
        : v_(v)
    {
    }

    //------------------------------------------------

    ss_hue_action::ss_hue_action(float v)
        : v_(v)
    {
    }

    ss_sat_action::ss_sat_action(float v)
        : v_(v)
    {
    }

    ss_brightness_action::ss_brightness_action(float v)
        : v_(v)
    {
    }

    ss_color_action::ss_color_action(float r, float g, float b)
    {
        col_[0] = r;
        col_[1] = g;
        col_[2] = b;
    }

    ss_alpha_action::ss_alpha_action(float v)
        : v_(v)
    {
    }

    ss_blend_action::ss_blend_action(float r, float g, float b, float strength)
    {
        col_[0] = r;
        col_[1] = g;
        col_[2] = b;
        s_ = strength;
    }

    ss_color_random_action::ss_color_random_action()
    {
    }
    //------------------------------------------------

    ss_call_rule_action::ss_call_rule_action(const std::string& name)
        : name_(name)
    {
    }

    //------------------------------------------------

    ss_geometry_action* ss_geometry_action_creator::create(const std::string& name)
    {
        if (strstr(name.c_str(), "box") != NULL)
        {
            return new ss_box_action();
        }
        else if (strstr(name.c_str(), "grid") != NULL)
        {
            return new ss_grid_action();
        }
        else if (strstr(name.c_str(), "sphere") != NULL)
        {
            return new ss_sphere_action();
        }
        else if (strstr(name.c_str(), "line") != NULL)
        {
            return new ss_line_action();
        }
        else if (strstr(name.c_str(), "point") != NULL)
        {
            return new ss_point_action();
        }
        else if (strstr(name.c_str(), "triangle") != NULL)
        {
            return new ss_triangle_action();
        }
        else if (strstr(name.c_str(), "cylinder") != NULL)
        {
            return new ss_cylinder_action();
        }
        else if (strstr(name.c_str(), "tube") != NULL)
        {
            return new ss_tube_action();
        }
        return NULL;
    }

    //------------------------------------------------
    ss_blocked_action::ss_blocked_action(const ss_blocklist* bl, const ss_action* act)
        : act_(act)
    {
        blocks_ = bl->get_list();
    }

    ss_blocked_action::ss_blocked_action(const std::vector<const ss_block*>& blocks, const ss_action* act)
        : blocks_(blocks), act_(act)
    {
    }

    //------------------------------------------------
    ss_modify_maxdepth_action::ss_modify_maxdepth_action(int depth)
        : depth_(depth)
    {
    }

    ss_modify_maxdepth_action::ss_modify_maxdepth_action(int depth, const std::string& extra)
        : depth_(depth), extra_(extra)
    {
    }

    int ss_modify_maxdepth_action::get_maxdepth() const
    {
        return depth_;
    }

    ss_modify_weight_action::ss_modify_weight_action(float w)
        : v_(w)
    {
    }

    float ss_modify_weight_action::get_weight() const
    {
        return v_;
    }
    //------------------------------------------------
    static std::string ToString(float f)
    {
        char buffer[128] = {};
        sprintf(buffer, "%f", f);
        return buffer;
    }

    static int ToInteger(const std::string& s)
    {
        int v = 0;
        std::stringstream ss(s);
        ss >> v;
        return v;
    }

    static float ToFloat(const std::string& s)
    {
        float v = 0;
        std::stringstream ss(s);
        ss >> v;
        return v;
    }

    ss_set_option_action::ss_set_option_action(const std::string& key, int v)
    {
        key_ = key;
        char buffer[128] = {};
        sprintf(buffer, "%d", v);
        value_ = buffer;
    }

    ss_set_option_action::ss_set_option_action(const std::string& key, float v)
    {
        key_ = key;
        value_ = ToString(v);
    }

    ss_set_option_action::ss_set_option_action(const std::string& key, const float v[], int n)
    {
        key_ = key;
        std::string val;
        for (int i = 0; i < n; i++)
        {
            if (i != 0) val += " ";
            val += ToString(v[i]);
        }
        value_ = val;
    }

    ss_set_option_action::ss_set_option_action(const std::string& key, const std::string& v)
    {
        key_ = key;
        value_ = v;
    }

    const std::string& ss_set_option_action::get_key() const
    {
        return key_;
    }

    int ss_set_option_action::get_value_as_integer() const
    {
        return ToInteger(value_);
    }

    float ss_set_option_action::get_value_as_float() const
    {
        return ToFloat(value_);
    }

    const std::string& ss_set_option_action::get_value_as_string() const
    {
        return value_;
    }
}

