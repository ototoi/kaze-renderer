#ifndef SS_ATTRIBUTES_H
#define SS_ATTRIBUTES_H

#include <string>

namespace ss
{
    class ss_attributes
    {
    public:
        ss_attributes();
        ss_attributes(const ss_attributes& attr);

    public:
        void action_x(float v);
        void action_y(float v);
        void action_z(float v);
        void action_rx(float v);
        void action_ry(float v);
        void action_rz(float v);
        void action_s(float x, float y, float z);
        void action_sx(float v);
        void action_sy(float v);
        void action_sz(float v);
        void action_fx(float v);
        void action_fy(float v);
        void action_fz(float v);
        void action_m(const float m[9]);

    public:
        void action_hue(float v);
        void action_sat(float v);
        void action_brightness(float v);
        void action_alpha(float v);
        void action_color(const float c[3]);
        void action_blend(const float c[3], float s);
        void action_color_random();
        void action_set_color_scheme(const std::string& scheme);

    public:
        const float* get_transform_ptr() const;
        const float* get_color_ptr() const;

    public:
        bool is_minmax(float min, float max) const;

    protected:
        void concat(const float mat[4 * 4]);
        void translate(float x, float y, float z);
        void scale(float x, float y, float z);
        void rotate(float angle, float dx, float dy, float dz);

        void apply(float vec[3]) const;

    protected:
        float mat_[16];
        float rgba_[4];
    };
}


#endif
