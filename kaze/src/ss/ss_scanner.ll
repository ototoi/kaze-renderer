%option noyywrap nounput batch
%option never-interactive
%option noyyget_out
%option noyy_push_state
%option noyy_pop_state
%option noyy_top_state
%option noyy_scan_buffer
%option noyy_scan_bytes
%option noyy_scan_string
%option noyyget_extra
%option noyyget_leng
%option noyyget_text
%option noyyget_lineno
%option noyyset_lineno
%option noyyget_in
%option noyyset_in
%option noyyget_out
%option noyyset_out
%option noyyget_lval
%option noyyset_lval
%option noyyget_lloc
%option noyyset_lloc
%option noyyget_debug
%option noyyset_debug
%option noinput
%option 8bit
%option reentrant
%option prefix="_ss_"
%option nounistd

%{

#ifdef _WIN32
#pragma warning(disable : 4018)
#pragma warning(disable : 4102)
#pragma warning(disable : 4244)
#pragma warning(disable : 4267)
#pragma warning(disable : 4786)
#pragma warning(disable : 4996)

extern "C" int isatty(int);

#endif

#include <stdio.h>
#include <string>
#include <string.h>
#include <memory>

#include "ss_parser.hpp"
#include "ss_parse_param.h"
#include "ss_decoder.h"
#include "ss_color.h"

typedef yy::ss_parser::token token;


#define YY_EXTRA_TYPE ss::ss_parse_param

#define PARAM() ((ss::ss_parse_param*)(yyscanner))


#define	YY_DECL											\
	int							\
	yylex(yy::ss_parser::semantic_type* yylval,		\
	     yy::ss_parser::location_type* yyloc,          \
		 void* yyscanner)

#ifdef YY_INPUT
#undef YY_INPUT
#endif

#define YY_INPUT(buf, retval, maxlen)	if ( (retval = PARAM()->dec->read(buf,maxlen)) < 0) 			\
											YY_FATAL_ERROR( "input in flex scanner failed" );



%}

D			[0-9]
L			[a-zA-Z_-]

delim				[ \t\\]
whitespace		{delim}+
letter				[A-Za-z]
digit					[0-9]
bin                 [0-9a-fA-F]
float				[+\-]?[1-9]?[0-9]+\.{digit}+[f]?
integer			[+\-]?{digit}+
colbin3             #{bin}{bin}{bin}
colbin6             #{bin}{bin}{bin}{bin}{bin}{bin}
string				\"[^"]*\"
eol					\r\n|\r|\n

%s COMMENT
%s COMMENTLINE

/* %option lex-compat */
%option noreject

%%


<INITIAL>"/*"   { BEGIN COMMENT; }
<COMMENT>"*/"   { BEGIN INITIAL; }
<COMMENT>.      { }

<INITIAL>"//"   { BEGIN COMMENTLINE; }
<COMMENTLINE>{eol}  { PARAM()->line_number++; BEGIN INITIAL; }
<COMMENTLINE>.     { }

{whitespace}    {  }
{eol}			{ PARAM()->line_number++; }

<INITIAL>{integer}		{ yylval->itype = atoi(yytext); return token::INTEGER_TOKEN; }
<INITIAL>{float}	    { yylval->ftype = (float)atof(yytext); return token::FLOAT_TOKEN; }
<INITIAL>{colbin3}       { yylval->itype = ss::ss_color::eval_code(yytext); return token::COLCODE_TOKEN; }
<INITIAL>{colbin6}       { yylval->itype = ss::ss_color::eval_code(yytext); return token::COLCODE_TOKEN; }

<INITIAL>"true"              { return token::TOKEN_TRUE; }
<INITIAL>"false"             { return token::TOKEN_FALSE; }

<INITIAL>"Rule"              { return token::TOKEN_RULE; }
<INITIAL>"rule"              { return token::TOKEN_RULE; }
<INITIAL>"set"               { return token::TOKEN_SET; }
<INITIAL>"initial"           { return token::TOKEN_INITIAL; }
<INITIAL>"random"            { return token::TOKEN_RANDOM; }

<INITIAL>"md"                { return token::TOKEN_MAXDEPTH; }
<INITIAL>"weight"            { return token::TOKEN_WEIGHT; }
<INITIAL>"w"                 { return token::TOKEN_WEIGHT; }

<INITIAL>"x"             { return token::TOKEN_X; }
<INITIAL>"y"             { return token::TOKEN_Y; }
<INITIAL>"z"             { return token::TOKEN_Z; }
<INITIAL>"rx"            { return token::TOKEN_RX; }
<INITIAL>"ry"            { return token::TOKEN_RY; }
<INITIAL>"rz"            { return token::TOKEN_RZ; }
<INITIAL>"m"             { if(PARAM()->is_in_block){return token::TOKEN_M;}else{std::string temp(yytext); yylval->stype = new char[temp.size()+1];  strcpy(yylval->stype, temp.c_str()); return token::IDENTIFIER;} }
<INITIAL>"s"             { return token::TOKEN_S; }
<INITIAL>"fx"            { return token::TOKEN_FX; }
<INITIAL>"fy"            { return token::TOKEN_FY; }
<INITIAL>"fz"            { return token::TOKEN_FZ; }

<INITIAL>"hue"           { return token::TOKEN_HUE; }
<INITIAL>"h"             { return token::TOKEN_HUE; }
<INITIAL>"sat"           { return token::TOKEN_SAT; }
<INITIAL>"brightness"    { return token::TOKEN_BRIGHTNESS; }
<INITIAL>"b"             { if(PARAM()->is_in_block){return token::TOKEN_BRIGHTNESS;}else{std::string temp(yytext); yylval->stype = new char[temp.size()+1];  strcpy(yylval->stype, temp.c_str()); return token::IDENTIFIER;} }
<INITIAL>"color"         { return token::TOKEN_COLOR; }
<INITIAL>"blend"         { return token::TOKEN_BLEND; }
<INITIAL>"alpha"         { return token::TOKEN_ALPHA; }
<INITIAL>"a"             { if(PARAM()->is_in_block){return token::TOKEN_ALPHA;}else{std::string temp(yytext); yylval->stype = new char[temp.size()+1];  strcpy(yylval->stype, temp.c_str()); return token::IDENTIFIER;} }

<INITIAL>"box"           { return token::TOKEN_BOX; }
<INITIAL>"Box"           { return token::TOKEN_BOX; }
<INITIAL>"grid"          { return token::TOKEN_GRID; }
<INITIAL>"Grid"          { return token::TOKEN_GRID; }
<INITIAL>"line"          { return token::TOKEN_LINE; }
<INITIAL>"Line"          { return token::TOKEN_LINE; }
<INITIAL>"sphere"        { return token::TOKEN_SPHERE; }
<INITIAL>"Sphere"        { return token::TOKEN_SPHERE; }
<INITIAL>"point"         { return token::TOKEN_POINT; }
<INITIAL>"Point"         { return token::TOKEN_POINT; }
<INITIAL>"triangle"      { return token::TOKEN_TRIANGLE; }
<INITIAL>"Triangle"      { return token::TOKEN_TRIANGLE; }
<INITIAL>"cylinder"      { return token::TOKEN_CYLINDER; }
<INITIAL>"Cylinder"      { return token::TOKEN_CYLINDER; }
<INITIAL>"tube"          { return token::TOKEN_TUBE; }
<INITIAL>"Tube"          { return token::TOKEN_TUBE; }

<INITIAL>"::"            { return token::TOKEN_DOUBLE_COLON; }

<INITIAL>{L}({L}|{D})*	{ std::string temp(yytext); yylval->stype = new char[temp.size()+1];  strcpy(yylval->stype, temp.c_str()); return token::IDENTIFIER;}

.				{ return yytext[0]; }
<<EOF>>			{ yyterminate(); }


%%
