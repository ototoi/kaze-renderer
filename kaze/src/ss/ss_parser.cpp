/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison LALR(1) parsers in C++

   Copyright (C) 2002, 2003, 2004, 2005, 2006 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


#include "ss_parser.hpp"

/* User implementation prologue.  */
#line 45 "ss_parser.yy"


#define yyerror(m) error(yylloc,m)

using namespace ss;

static
char* dup_(const char* szText)
{
	int l = strlen(szText);
	char* buffer = new char[l+1];
	buffer[l] = 0;
	strcpy(buffer,szText);
	return buffer;
}

#define DUP(s) dup_(s)

#define PARAM() ((ss_parse_param*)scanner)
#define GET_CTX() (((ss_parse_param*)scanner)->ctx)
#define GET_OPT() (GET_CTX()->get_global_options())
#define DEBUG(s)  (GET_CTX()->print(s))
#define LINENUMBER() (((ss_parse_param*)scanner)->line_number)

static
void release(const char* v){delete[] v;v=NULL;}

#define	YY_DECL	1

extern int _ss_lex(yy::ss_parser::semantic_type* yylval,
	 yy::ss_parser::location_type* yylloc,
	 void* scanner);

static
int yylex(yy::ss_parser::semantic_type* yylval,
	 yy::ss_parser::location_type* yylloc,
	 void* scanner)
{
	return _ss_lex(yylval, yylloc, scanner);
}





/* Line 317 of lalr1.cc.  */
#line 87 "ss_parser.cpp"

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* FIXME: INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#define YYUSE(e) ((void) (e))

/* A pseudo ostream that takes yydebug_ into account.  */
# define YYCDEBUG							\
  for (bool yydebugcond_ = yydebug_; yydebugcond_; yydebugcond_ = false)	\
    (*yycdebug_)

/* Enable debugging if requested.  */
#if YYDEBUG

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)	\
do {							\
  if (yydebug_)						\
    {							\
      *yycdebug_ << Title << ' ';			\
      yy_symbol_print_ ((Type), (Value), (Location));	\
      *yycdebug_ << std::endl;				\
    }							\
} while (false)

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug_)				\
    yy_reduce_print_ (Rule);		\
} while (false)

# define YY_STACK_PRINT()		\
do {					\
  if (yydebug_)				\
    yystack_print_ ();			\
} while (false)

#else /* !YYDEBUG */

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_REDUCE_PRINT(Rule)
# define YY_STACK_PRINT()

#endif /* !YYDEBUG */

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab

namespace yy
{
#if YYERROR_VERBOSE

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
  ss_parser::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr = "";
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              /* Fall through.  */
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }

#endif

  /// Build a parser object.
  ss_parser::ss_parser (void * scanner_yyarg)
    : yydebug_ (false),
      yycdebug_ (&std::cerr),
      scanner (scanner_yyarg)
  {
  }

  ss_parser::~ss_parser ()
  {
  }

#if YYDEBUG
  /*--------------------------------.
  | Print this symbol on YYOUTPUT.  |
  `--------------------------------*/

  inline void
  ss_parser::yy_symbol_value_print_ (int yytype,
			   const semantic_type* yyvaluep, const location_type* yylocationp)
  {
    YYUSE (yylocationp);
    YYUSE (yyvaluep);
    switch (yytype)
      {
         default:
	  break;
      }
  }


  void
  ss_parser::yy_symbol_print_ (int yytype,
			   const semantic_type* yyvaluep, const location_type* yylocationp)
  {
    *yycdebug_ << (yytype < yyntokens_ ? "token" : "nterm")
	       << ' ' << yytname_[yytype] << " ("
	       << *yylocationp << ": ";
    yy_symbol_value_print_ (yytype, yyvaluep, yylocationp);
    *yycdebug_ << ')';
  }
#endif /* ! YYDEBUG */

  void
  ss_parser::yydestruct_ (const char* yymsg,
			   int yytype, semantic_type* yyvaluep, location_type* yylocationp)
  {
    YYUSE (yylocationp);
    YYUSE (yymsg);
    YYUSE (yyvaluep);

    YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

    switch (yytype)
      {
  
	default:
	  break;
      }
  }

  void
  ss_parser::yypop_ (unsigned int n)
  {
    yystate_stack_.pop (n);
    yysemantic_stack_.pop (n);
    yylocation_stack_.pop (n);
  }

  std::ostream&
  ss_parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  ss_parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  ss_parser::debug_level_type
  ss_parser::debug_level () const
  {
    return yydebug_;
  }

  void
  ss_parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }


  int
  ss_parser::parse ()
  {
    /// Look-ahead and look-ahead in internal form.
    int yychar = yyempty_;
    int yytoken = 0;

    /* State.  */
    int yyn;
    int yylen = 0;
    int yystate = 0;

    /* Error handling.  */
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// Semantic value of the look-ahead.
    semantic_type yylval;
    /// Location of the look-ahead.
    location_type yylloc;
    /// The locations where the error started and ended.
    location yyerror_range[2];

    /// $$.
    semantic_type yyval;
    /// @$.
    location_type yyloc;

    int yyresult;

    YYCDEBUG << "Starting parse" << std::endl;


    /* Initialize the stacks.  The initial state will be pushed in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystate_stack_ = state_stack_type (0);
    yysemantic_stack_ = semantic_stack_type (0);
    yylocation_stack_ = location_stack_type (0);
    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yylloc);

    /* New state.  */
  yynewstate:
    yystate_stack_.push (yystate);
    YYCDEBUG << "Entering state " << yystate << std::endl;
    goto yybackup;

    /* Backup.  */
  yybackup:

    /* Try to take a decision without look-ahead.  */
    yyn = yypact_[yystate];
    if (yyn == yypact_ninf_)
      goto yydefault;

    /* Read a look-ahead token.  */
    if (yychar == yyempty_)
      {
	YYCDEBUG << "Reading a token: ";
	yychar = yylex (&yylval, &yylloc, scanner);
      }


    /* Convert token to internal form.  */
    if (yychar <= yyeof_)
      {
	yychar = yytoken = yyeof_;
	YYCDEBUG << "Now at end of input." << std::endl;
      }
    else
      {
	yytoken = yytranslate_ (yychar);
	YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
      }

    /* If the proper action on seeing token YYTOKEN is to reduce or to
       detect an error, take that action.  */
    yyn += yytoken;
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yytoken)
      goto yydefault;

    /* Reduce or error.  */
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
	if (yyn == 0 || yyn == yytable_ninf_)
	goto yyerrlab;
	yyn = -yyn;
	goto yyreduce;
      }

    /* Accept?  */
    if (yyn == yyfinal_)
      goto yyacceptlab;

    /* Shift the look-ahead token.  */
    YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

    /* Discard the token being shifted unless it is eof.  */
    if (yychar != yyeof_)
      yychar = yyempty_;

    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yylloc);

    /* Count tokens shifted since error; after three, turn off error
       status.  */
    if (yyerrstatus_)
      --yyerrstatus_;

    yystate = yyn;
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystate];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    /* If YYLEN is nonzero, implement the default value of the action:
       `$$ = $1'.  Otherwise, use the top of the stack.

       Otherwise, the following line sets YYVAL to garbage.
       This behavior is undocumented and Bison
       users should not rely upon it.  */
    if (yylen)
      yyval = yysemantic_stack_[yylen - 1];
    else
      yyval = yysemantic_stack_[0];

    {
      slice<location_type, location_stack_type> slice (yylocation_stack_, yylen);
      YYLLOC_DEFAULT (yyloc, slice, yylen);
    }
    YY_REDUCE_PRINT (yyn);
    switch (yyn)
      {
	  case 2:
#line 177 "ss_parser.yy"
    { ; ;}
    break;

  case 3:
#line 179 "ss_parser.yy"
    { ; ;}
    break;

  case 4:
#line 184 "ss_parser.yy"
    { ; ;}
    break;

  case 5:
#line 186 "ss_parser.yy"
    { ; ;}
    break;

  case 6:
#line 191 "ss_parser.yy"
    { ; ;}
    break;

  case 7:
#line 193 "ss_parser.yy"
    { ; ;}
    break;

  case 8:
#line 195 "ss_parser.yy"
    { ; ;}
    break;

  case 9:
#line 200 "ss_parser.yy"
    {
                GET_OPT()->set_maxdepth((yysemantic_stack_[(3) - (3)].itype));
				ss_set_option_action * act = new ss_set_option_action("maxdepth", (yysemantic_stack_[(3) - (3)].itype));
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
            ;}
    break;

  case 10:
#line 207 "ss_parser.yy"
    {
                ss_set_option_action * act = new ss_set_option_action((yysemantic_stack_[(3) - (2)].stype), (yysemantic_stack_[(3) - (3)].itype));
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release((yysemantic_stack_[(3) - (2)].stype));
            ;}
    break;

  case 11:
#line 214 "ss_parser.yy"
    {
                ss_set_option_action * act = new ss_set_option_action((yysemantic_stack_[(3) - (2)].stype), (yysemantic_stack_[(3) - (3)].itype));
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release((yysemantic_stack_[(3) - (2)].stype));
                DEBUG("SetOption OptionKey Integer\n");
            ;}
    break;

  case 12:
#line 222 "ss_parser.yy"
    {
                ss_set_option_action * act = new ss_set_option_action((yysemantic_stack_[(3) - (2)].stype), (yysemantic_stack_[(3) - (3)].ftype));
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release((yysemantic_stack_[(3) - (2)].stype));
                
                DEBUG("SetOption OptionKey Float\n");
            ;}
    break;

  case 13:
#line 231 "ss_parser.yy"
    {
                ss_array_object* ary = (ss_array_object*)(yysemantic_stack_[(3) - (3)].ntype);
                float f[2] = {};
                memcpy(f, ary->get_ptr(), sizeof(float)*2);
                
                ss_set_option_action * act = new ss_set_option_action((yysemantic_stack_[(3) - (2)].stype), f, 2);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release((yysemantic_stack_[(3) - (2)].stype));
                GET_CTX()->erase_object(ary);
            ;}
    break;

  case 14:
#line 243 "ss_parser.yy"
    {
                ss_array_object* ary = (ss_array_object*)(yysemantic_stack_[(3) - (3)].ntype);
                float f[3] = {};
                memcpy(f, ary->get_ptr(), sizeof(float)*3);
                
                ss_set_option_action * act = new ss_set_option_action((yysemantic_stack_[(3) - (2)].stype), f, 3);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release((yysemantic_stack_[(3) - (2)].stype));
                GET_CTX()->erase_object(ary);
            ;}
    break;

  case 15:
#line 255 "ss_parser.yy"
    {
                ss_array_object* ary = (ss_array_object*)(yysemantic_stack_[(3) - (3)].ntype);
                float f[9] = {};
                memcpy(f, ary->get_ptr(), sizeof(float)*9);
            
                ss_set_option_action * act = new ss_set_option_action((yysemantic_stack_[(3) - (2)].stype), f, 9);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release((yysemantic_stack_[(3) - (2)].stype));
                GET_CTX()->erase_object(ary);
            ;}
    break;

  case 16:
#line 267 "ss_parser.yy"
    {
                ss_array_object* ary = (ss_array_object*)(yysemantic_stack_[(3) - (3)].ntype);
                float f[3] = {};
                memcpy(f, ary->get_ptr(), sizeof(float)*3);
                
                ss_set_option_action * act = new ss_set_option_action((yysemantic_stack_[(3) - (2)].stype), f, 3);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release((yysemantic_stack_[(3) - (2)].stype));
                GET_CTX()->erase_object(ary);
            ;}
    break;

  case 17:
#line 279 "ss_parser.yy"
    {
                ss_set_option_action * act = new ss_set_option_action((yysemantic_stack_[(3) - (2)].stype), (yysemantic_stack_[(3) - (3)].stype));
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release((yysemantic_stack_[(3) - (2)].stype));
                release((yysemantic_stack_[(3) - (3)].stype));
            ;}
    break;

  case 18:
#line 287 "ss_parser.yy"
    {
                ss_set_option_action * act = new ss_set_option_action((yysemantic_stack_[(3) - (2)].stype), (yysemantic_stack_[(3) - (3)].stype));
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release((yysemantic_stack_[(3) - (2)].stype));
                release((yysemantic_stack_[(3) - (3)].stype));
            ;}
    break;

  case 19:
#line 295 "ss_parser.yy"
    {
                ss_set_option_action * act = new ss_set_option_action((yysemantic_stack_[(3) - (2)].stype), "initial");
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release((yysemantic_stack_[(3) - (2)].stype));
            ;}
    break;

  case 20:
#line 305 "ss_parser.yy"
    {
            (yyval.stype) = (yysemantic_stack_[(1) - (1)].stype);
        ;}
    break;

  case 21:
#line 309 "ss_parser.yy"
    {
            std::string s;
			s += (yysemantic_stack_[(3) - (1)].stype);
			s += "::";
			s += (yysemantic_stack_[(3) - (3)].stype);
			(yyval.stype) = DUP(s.c_str());
            release((yysemantic_stack_[(3) - (1)].stype));
            release((yysemantic_stack_[(3) - (3)].stype));
        ;}
    break;

  case 22:
#line 322 "ss_parser.yy"
    {
            ;
        ;}
    break;

  case 23:
#line 326 "ss_parser.yy"
    {
            ;
        ;}
    break;

  case 24:
#line 333 "ss_parser.yy"
    {
            ss_rule* r = new ss_rule((yysemantic_stack_[(3) - (2)].stype));
            r->set_maxdepth(GET_OPT()->get_maxdepth());

			GET_CTX()->add_object(r);
            GET_CTX()->push_rule(r);

			release((yysemantic_stack_[(3) - (2)].stype));
        ;}
    break;

  case 25:
#line 343 "ss_parser.yy"
    {
            ss_rule* r = new ss_rule((yysemantic_stack_[(4) - (2)].stype));
			ss_modify_maxdepth_action* modm = (ss_modify_maxdepth_action*)(yysemantic_stack_[(4) - (3)].ntype);

			r->set_maxdepth(modm->get_maxdepth());
			r->set_extra_rule_name(modm->get_extra_rule_name());

			GET_CTX()->add_object(r);
            GET_CTX()->push_rule(r);

			GET_CTX()->erase_object(modm);

			release((yysemantic_stack_[(4) - (2)].stype));
        ;}
    break;

  case 26:
#line 358 "ss_parser.yy"
    {
            ss_rule* r = new ss_rule((yysemantic_stack_[(4) - (2)].stype));
			ss_modify_weight_action* modw = (ss_modify_weight_action*)(yysemantic_stack_[(4) - (3)].ntype);

			r->set_weight(modw->get_weight());

			GET_CTX()->add_object(r);
            GET_CTX()->push_rule(r);

			GET_CTX()->erase_object(modw);

			release((yysemantic_stack_[(4) - (2)].stype));
        ;}
    break;

  case 27:
#line 372 "ss_parser.yy"
    {
            ss_rule* r = new ss_rule((yysemantic_stack_[(5) - (2)].stype));
			ss_modify_maxdepth_action* modm = (ss_modify_maxdepth_action*)(yysemantic_stack_[(5) - (3)].ntype);
			ss_modify_weight_action* modw = (ss_modify_weight_action*)(yysemantic_stack_[(5) - (4)].ntype);

			r->set_maxdepth(modm->get_maxdepth());
			r->set_extra_rule_name(modm->get_extra_rule_name());
			r->set_weight(modw->get_weight());

			GET_CTX()->add_object(r);
            GET_CTX()->push_rule(r);

			GET_CTX()->erase_object(modm);
			GET_CTX()->erase_object(modw);

			release((yysemantic_stack_[(5) - (2)].stype));
        ;}
    break;

  case 28:
#line 390 "ss_parser.yy"
    {
            ss_rule* r = new ss_rule((yysemantic_stack_[(5) - (2)].stype));
			ss_modify_weight_action* modw = (ss_modify_weight_action*)(yysemantic_stack_[(5) - (3)].ntype);
			ss_modify_maxdepth_action* modm = (ss_modify_maxdepth_action*)(yysemantic_stack_[(5) - (4)].ntype);

			r->set_maxdepth(modm->get_maxdepth());
			r->set_extra_rule_name(modm->get_extra_rule_name());
			r->set_weight(modw->get_weight());

			GET_CTX()->add_object(r);
            GET_CTX()->push_rule(r);

			GET_CTX()->erase_object(modm);
			GET_CTX()->erase_object(modw);

			release((yysemantic_stack_[(5) - (2)].stype));
        ;}
    break;

  case 29:
#line 411 "ss_parser.yy"
    {
			ss_action* act = new ss_modify_maxdepth_action((yysemantic_stack_[(2) - (2)].itype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;

			DEBUG("ModifyMaxDepth\n");
		;}
    break;

  case 30:
#line 419 "ss_parser.yy"
    {
			ss_action* act = new ss_modify_maxdepth_action((yysemantic_stack_[(4) - (2)].itype), (yysemantic_stack_[(4) - (4)].stype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;

			DEBUG("ModifyMaxDepth\n");
			release((yysemantic_stack_[(4) - (4)].stype));
		;}
    break;

  case 31:
#line 431 "ss_parser.yy"
    {
			ss_action* act = new ss_modify_weight_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;

			DEBUG("ModifyWeight\n");
		;}
    break;

  case 32:
#line 442 "ss_parser.yy"
    {
            GET_CTX()->pop_rule();
        ;}
    break;

  case 33:
#line 449 "ss_parser.yy"
    { ; ;}
    break;

  case 34:
#line 451 "ss_parser.yy"
    { ; ;}
    break;

  case 35:
#line 456 "ss_parser.yy"
    { ; ;}
    break;

  case 36:
#line 458 "ss_parser.yy"
    { ; ;}
    break;

  case 37:
#line 463 "ss_parser.yy"
    { ; ;}
    break;

  case 38:
#line 465 "ss_parser.yy"
    { ; ;}
    break;

  case 39:
#line 470 "ss_parser.yy"
    {
			ss_blocklist* bl = new ss_blocklist();
			GET_CTX()->add_object(bl);

			ss_call_rule_action* act = new ss_call_rule_action((yysemantic_stack_[(1) - (1)].stype));
			GET_CTX()->add_object(act);

			ss_blocked_action* bact = new ss_blocked_action(bl, act);
			GET_CTX()->add_object(bact);

			GET_CTX()->get_top_rule()->add_action(bact);

			DEBUG("RuleSymbol\n");
			release((yysemantic_stack_[(1) - (1)].stype));
		;}
    break;

  case 40:
#line 486 "ss_parser.yy"
    {
			ss_blocklist* bl = (ss_blocklist*)(yysemantic_stack_[(2) - (1)].ntype);

			ss_call_rule_action* act = new ss_call_rule_action((yysemantic_stack_[(2) - (2)].stype));
			GET_CTX()->add_object(act);

			ss_blocked_action* bact = new ss_blocked_action(bl, act);
			GET_CTX()->add_object(bact);

			GET_CTX()->get_top_rule()->add_action(bact);

			DEBUG("BlockList RuleSymbol\n");
			release((yysemantic_stack_[(2) - (2)].stype));
		;}
    break;

  case 41:
#line 504 "ss_parser.yy"
    {
			ss_blocklist* bl = new ss_blocklist();
			GET_CTX()->add_object(bl);

			ss_geometry_action* act = ss_geometry_action_creator::create((yysemantic_stack_[(1) - (1)].stype));
			GET_CTX()->add_object(act);

			ss_blocked_action* bact = new ss_blocked_action(bl, act);
			GET_CTX()->add_object(bact);

			GET_CTX()->get_top_rule()->add_action(bact);

			DEBUG("GeometrySymbol\n");
			release((yysemantic_stack_[(1) - (1)].stype));
		;}
    break;

  case 42:
#line 520 "ss_parser.yy"
    {
			ss_blocklist* bl = (ss_blocklist*)(yysemantic_stack_[(2) - (1)].ntype);

			ss_geometry_action* act = ss_geometry_action_creator::create((yysemantic_stack_[(2) - (2)].stype));
			GET_CTX()->add_object(act);

			ss_blocked_action* bact = new ss_blocked_action(bl, act);
			GET_CTX()->add_object(bact);

			GET_CTX()->get_top_rule()->add_action(bact);

			DEBUG("BlockList GeometrySymbol\n");
			release((yysemantic_stack_[(2) - (2)].stype));
		;}
    break;

  case 43:
#line 535 "ss_parser.yy"
    {
            ss_blocklist* bl = (ss_blocklist*)(yysemantic_stack_[(3) - (1)].ntype);
            
            ss_array_object* ary = (ss_array_object*)(yysemantic_stack_[(3) - (3)].ntype);
			float f[9] = {};
			memcpy(f, ary->get_ptr(), sizeof(float)*9);

			ss_triangle_action* act = (ss_triangle_action*)ss_geometry_action_creator::create((yysemantic_stack_[(3) - (2)].stype));
			act->set_positions(f);
            GET_CTX()->add_object(act);  

			ss_blocked_action* bact = new ss_blocked_action(bl, act);
			GET_CTX()->add_object(bact);

			GET_CTX()->get_top_rule()->add_action(bact);

			DEBUG("BlockList TriangleSymbol TriangleList\n");
			release((yysemantic_stack_[(3) - (2)].stype));
            GET_CTX()->erase_object((yysemantic_stack_[(3) - (3)].ntype));
        ;}
    break;

  case 44:
#line 559 "ss_parser.yy"
    {
		 	(yyval.stype) = (yysemantic_stack_[(1) - (1)].stype);
		;}
    break;

  case 45:
#line 566 "ss_parser.yy"
    {
			(yyval.stype) = DUP("box");
		;}
    break;

  case 46:
#line 570 "ss_parser.yy"
    {
			(yyval.stype) = DUP("grid");
		;}
    break;

  case 47:
#line 574 "ss_parser.yy"
    {
			(yyval.stype) = DUP("sphere");
		;}
    break;

  case 48:
#line 578 "ss_parser.yy"
    {
			(yyval.stype) = DUP("line");
		;}
    break;

  case 49:
#line 582 "ss_parser.yy"
    {
			(yyval.stype) = DUP("point");
		;}
    break;

  case 50:
#line 586 "ss_parser.yy"
    {
			(yyval.stype) = DUP("cylinder");
		;}
    break;

  case 51:
#line 590 "ss_parser.yy"
    {
			(yyval.stype) = DUP("tube");
		;}
    break;

  case 52:
#line 594 "ss_parser.yy"
    {
			std::string s = "";
			s += (yysemantic_stack_[(3) - (1)].stype);
			s += "::";
			s += (yysemantic_stack_[(3) - (3)].stype);
			(yyval.stype) = DUP(s.c_str());
		 	DEBUG("GeometrySymbol\n");
			release((yysemantic_stack_[(3) - (1)].stype));
			release((yysemantic_stack_[(3) - (3)].stype));
		;}
    break;

  case 53:
#line 608 "ss_parser.yy"
    {
			(yyval.stype) = DUP("triangle");
		;}
    break;

  case 54:
#line 612 "ss_parser.yy"
    {
			std::string s = "";
			s += (yysemantic_stack_[(3) - (1)].stype);
			s += "::";
			s += (yysemantic_stack_[(3) - (3)].stype);
			(yyval.stype) = DUP(s.c_str());
		 	DEBUG("TriangleSymbol\n");
			release((yysemantic_stack_[(3) - (1)].stype));
			release((yysemantic_stack_[(3) - (3)].stype));
		;}
    break;

  case 55:
#line 626 "ss_parser.yy"
    {
		 	ss_blocklist* bl = new ss_blocklist();
			GET_CTX()->add_object(bl);
			bl->add_block((ss_block*)(yysemantic_stack_[(1) - (1)].ntype));
			(yyval.ntype) = bl;
			DEBUG("BlockList\n");
		;}
    break;

  case 56:
#line 634 "ss_parser.yy"
    {
			ss_blocklist* bl = (ss_blocklist*)(yysemantic_stack_[(2) - (1)].ntype);
			bl->add_block((ss_block*)(yysemantic_stack_[(2) - (2)].ntype));
			(yyval.ntype) = bl;
		;}
    break;

  case 57:
#line 643 "ss_parser.yy"
    {
			ss_block* blk = (ss_block*)(yysemantic_stack_[(3) - (1)].ntype);
			(yyval.ntype) = blk;
		 	DEBUG("Block\n");
		;}
    break;

  case 58:
#line 649 "ss_parser.yy"
    {
			ss_block* blk = (ss_block*)(yysemantic_stack_[(5) - (3)].ntype);
			blk->set_times((yysemantic_stack_[(5) - (1)].itype));
			(yyval.ntype) = blk;
			DEBUG("Block/Times\n");
		;}
    break;

  case 59:
#line 656 "ss_parser.yy"
    {
			ss_block* blk = (ss_block*)(yysemantic_stack_[(2) - (1)].ntype);
			(yyval.ntype) = blk;
		 	DEBUG("Block\n");
		;}
    break;

  case 60:
#line 662 "ss_parser.yy"
    {
			ss_block* blk = (ss_block*)(yysemantic_stack_[(4) - (3)].ntype);
            blk->set_times((yysemantic_stack_[(4) - (1)].itype));
			(yyval.ntype) = blk;
		 	DEBUG("Block\n");
		;}
    break;

  case 61:
#line 672 "ss_parser.yy"
    {
			PARAM()->is_in_block = 1;
			ss_block* blk = new ss_block();
			GET_CTX()->add_object(blk);
			GET_CTX()->push_block(blk);
			(yyval.ntype) = blk;
			DEBUG("BlockStart\n");
		;}
    break;

  case 62:
#line 684 "ss_parser.yy"
    {
			GET_CTX()->pop_block();
			PARAM()->is_in_block = 0;
			DEBUG("BlockEnd\n");
		;}
    break;

  case 63:
#line 693 "ss_parser.yy"
    {
		 	GET_CTX()->get_top_block()->add_action((ss_action*)(yysemantic_stack_[(1) - (1)].ntype));
		;}
    break;

  case 64:
#line 697 "ss_parser.yy"
    {
		 	GET_CTX()->get_top_block()->add_action((ss_action*)(yysemantic_stack_[(2) - (2)].ntype));
		;}
    break;

  case 65:
#line 703 "ss_parser.yy"
    {
			ss_action* act = new ss_x_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 66:
#line 709 "ss_parser.yy"
    {
			ss_action* act = new ss_y_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 67:
#line 715 "ss_parser.yy"
    {
			ss_action* act = new ss_z_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 68:
#line 721 "ss_parser.yy"
    {
			ss_action* act = new ss_rx_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 69:
#line 727 "ss_parser.yy"
    {
			ss_action* act = new ss_ry_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 70:
#line 733 "ss_parser.yy"
    {
			ss_action* act = new ss_rz_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 71:
#line 739 "ss_parser.yy"
    {
			ss_action* act = new ss_s_action((yysemantic_stack_[(2) - (2)].ftype),(yysemantic_stack_[(2) - (2)].ftype),(yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 72:
#line 745 "ss_parser.yy"
    {
			ss_array_object* ary = (ss_array_object*)(yysemantic_stack_[(2) - (2)].ntype);
			float f[3] = {ary->get_at(0), ary->get_at(1), ary->get_at(2)};
			ss_action* act = new ss_s_action(f[0],f[1],f[2]);//TODO
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;

			GET_CTX()->erase_object(ary);
		;}
    break;

  case 73:
#line 755 "ss_parser.yy"
    {
			ss_array_object* ary = (ss_array_object*)(yysemantic_stack_[(2) - (2)].ntype);
			float f[9] = {};
			memcpy(f, ary->get_ptr(), sizeof(float)*9);
			ss_action* act = new ss_m_action(f);
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;

			GET_CTX()->erase_object(ary);
		;}
    break;

  case 74:
#line 766 "ss_parser.yy"
    {
			ss_action* act = new ss_fx_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 75:
#line 772 "ss_parser.yy"
    {
			ss_action* act = new ss_fy_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 76:
#line 778 "ss_parser.yy"
    {
			ss_action* act = new ss_fz_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 77:
#line 784 "ss_parser.yy"
    {
			ss_action* act = new ss_hue_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 78:
#line 790 "ss_parser.yy"
    {
			ss_action* act = new ss_sat_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 79:
#line 796 "ss_parser.yy"
    {
			ss_action* act = new ss_brightness_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 80:
#line 802 "ss_parser.yy"
    {
			ss_action* act = new ss_alpha_action((yysemantic_stack_[(2) - (2)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 81:
#line 808 "ss_parser.yy"
    {
			ss_array_object* ary = (ss_array_object*)(yysemantic_stack_[(2) - (2)].ntype);
			float f[3] = {ary->get_at(0), ary->get_at(1), ary->get_at(2)};
			ss_action* act = new ss_color_action(f[0],f[1],f[2]);
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;

			GET_CTX()->erase_object(ary);
		;}
    break;

  case 82:
#line 818 "ss_parser.yy"
    {
			ss_action* act = new ss_color_random_action();
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;
		;}
    break;

  case 83:
#line 824 "ss_parser.yy"
    {
			ss_array_object* ary = (ss_array_object*)(yysemantic_stack_[(3) - (2)].ntype);
			float f[3] = {ary->get_at(0), ary->get_at(1), ary->get_at(2)};
			ss_action* act = new ss_blend_action(f[0],f[1],f[2], (yysemantic_stack_[(3) - (3)].ftype));
			GET_CTX()->add_object(act);
			(yyval.ntype) = act;

			GET_CTX()->erase_object(ary);
		;}
    break;

  case 84:
#line 837 "ss_parser.yy"
    {
			(yyval.stype) = (yysemantic_stack_[(1) - (1)].stype);
		;}
    break;

  case 85:
#line 844 "ss_parser.yy"
    {
			std::string s;
			s += (yysemantic_stack_[(3) - (1)].stype);
			s += ":";
			s += (yysemantic_stack_[(3) - (3)].stype);
			(yyval.stype) = DUP(s.c_str());
			release((yysemantic_stack_[(3) - (1)].stype));
			release((yysemantic_stack_[(3) - (3)].stype));
		;}
    break;

  case 86:
#line 854 "ss_parser.yy"
    {
			std::string s;
			s += (yysemantic_stack_[(3) - (1)].stype);
			s += ",";
			s += (yysemantic_stack_[(3) - (3)].stype);
			(yyval.stype) = DUP(s.c_str());
			release((yysemantic_stack_[(3) - (1)].stype));
			release((yysemantic_stack_[(3) - (3)].stype));
		;}
    break;

  case 87:
#line 867 "ss_parser.yy"
    {
            (yyval.itype) = (yysemantic_stack_[(1) - (1)].itype);
        ;}
    break;

  case 88:
#line 874 "ss_parser.yy"
    {
			(yyval.ftype) = (yysemantic_stack_[(1) - (1)].ftype);
		;}
    break;

  case 89:
#line 878 "ss_parser.yy"
    {
			(yyval.ftype) = (yysemantic_stack_[(3) - (1)].ftype) + (yysemantic_stack_[(3) - (3)].ftype);
		;}
    break;

  case 90:
#line 882 "ss_parser.yy"
    {
			(yyval.ftype) = (yysemantic_stack_[(3) - (1)].ftype) - (yysemantic_stack_[(3) - (3)].ftype);
		;}
    break;

  case 91:
#line 889 "ss_parser.yy"
    {
			(yyval.ftype) = (yysemantic_stack_[(1) - (1)].ftype);
		;}
    break;

  case 92:
#line 893 "ss_parser.yy"
    {
			(yyval.ftype) = (yysemantic_stack_[(3) - (1)].ftype) * (yysemantic_stack_[(3) - (3)].ftype);
		;}
    break;

  case 93:
#line 897 "ss_parser.yy"
    {
		 	(yyval.ftype) = (yysemantic_stack_[(3) - (1)].ftype) / (yysemantic_stack_[(3) - (3)].ftype);
		;}
    break;

  case 94:
#line 904 "ss_parser.yy"
    {
			(yyval.ftype) = (yysemantic_stack_[(1) - (1)].ftype);
            DEBUG("Float\n");
		;}
    break;

  case 95:
#line 909 "ss_parser.yy"
    {
			(yyval.ftype) = (float)(yysemantic_stack_[(1) - (1)].itype);
		;}
    break;

  case 96:
#line 916 "ss_parser.yy"
    {
			float f[2] = {(yysemantic_stack_[(2) - (1)].ftype), (yysemantic_stack_[(2) - (2)].ftype)};
			ss_array_object* ary = new ss_array_object(f, 2);
			GET_CTX()->add_object(ary);
			(yyval.ntype) = ary;
		;}
    break;

  case 97:
#line 923 "ss_parser.yy"
    {
			float f[2] = {(yysemantic_stack_[(4) - (2)].ftype), (yysemantic_stack_[(4) - (3)].ftype)};
			ss_array_object* ary = new ss_array_object(f, 2);
			GET_CTX()->add_object(ary);
			(yyval.ntype) = ary;
		;}
    break;

  case 98:
#line 930 "ss_parser.yy"
    {
			float f[2] = {(yysemantic_stack_[(5) - (2)].ftype), (yysemantic_stack_[(5) - (4)].ftype)};
			ss_array_object* ary = new ss_array_object(f, 2);
			GET_CTX()->add_object(ary);
			(yyval.ntype) = ary;
		;}
    break;

  case 99:
#line 940 "ss_parser.yy"
    {
			float f[3] = {(yysemantic_stack_[(3) - (1)].ftype), (yysemantic_stack_[(3) - (2)].ftype), (yysemantic_stack_[(3) - (3)].ftype)};
			ss_array_object* ary = new ss_array_object(f, 3);
			GET_CTX()->add_object(ary);
			(yyval.ntype) = ary;
		;}
    break;

  case 100:
#line 947 "ss_parser.yy"
    {
			float f[3] = {(yysemantic_stack_[(5) - (2)].ftype), (yysemantic_stack_[(5) - (3)].ftype), (yysemantic_stack_[(5) - (4)].ftype)};
			ss_array_object* ary = new ss_array_object(f, 3);
			GET_CTX()->add_object(ary);
			(yyval.ntype) = ary;
		;}
    break;

  case 101:
#line 954 "ss_parser.yy"
    {
			float f[3] = {(yysemantic_stack_[(7) - (2)].ftype), (yysemantic_stack_[(7) - (4)].ftype), (yysemantic_stack_[(7) - (6)].ftype)};
			ss_array_object* ary = new ss_array_object(f, 3);
			GET_CTX()->add_object(ary);
			(yyval.ntype) = ary;
		;}
    break;

  case 102:
#line 964 "ss_parser.yy"
    {
			float f[9] = {
				(yysemantic_stack_[(9) - (1)].ftype), (yysemantic_stack_[(9) - (2)].ftype), (yysemantic_stack_[(9) - (3)].ftype),
				(yysemantic_stack_[(9) - (4)].ftype), (yysemantic_stack_[(9) - (5)].ftype), (yysemantic_stack_[(9) - (6)].ftype),
				(yysemantic_stack_[(9) - (7)].ftype), (yysemantic_stack_[(9) - (8)].ftype), (yysemantic_stack_[(9) - (9)].ftype),
			};
			ss_array_object* ary = new ss_array_object(f, 9);
			GET_CTX()->add_object(ary);
			(yyval.ntype) = ary;
		;}
    break;

  case 103:
#line 975 "ss_parser.yy"
    {
			float f[9] = {
				(yysemantic_stack_[(11) - (2)].ftype), (yysemantic_stack_[(11) - (3)].ftype), (yysemantic_stack_[(11) - (4)].ftype),
				(yysemantic_stack_[(11) - (5)].ftype), (yysemantic_stack_[(11) - (6)].ftype), (yysemantic_stack_[(11) - (7)].ftype),
				(yysemantic_stack_[(11) - (8)].ftype), (yysemantic_stack_[(11) - (9)].ftype), (yysemantic_stack_[(11) - (10)].ftype),
			};
			ss_array_object* ary = new ss_array_object(f, 9);
			GET_CTX()->add_object(ary);
			(yyval.ntype) = ary;
		;}
    break;

  case 104:
#line 989 "ss_parser.yy"
    {
            float f[9] = {
				(yysemantic_stack_[(19) - (2)].ftype), (yysemantic_stack_[(19) - (4)].ftype), (yysemantic_stack_[(19) - (6)].ftype),
				(yysemantic_stack_[(19) - (8)].ftype), (yysemantic_stack_[(19) - (10)].ftype), (yysemantic_stack_[(19) - (12)].ftype),
				(yysemantic_stack_[(19) - (14)].ftype), (yysemantic_stack_[(19) - (16)].ftype), (yysemantic_stack_[(19) - (18)].ftype),
			};
			ss_array_object* ary = new ss_array_object(f, 9);
			GET_CTX()->add_object(ary);
			(yyval.ntype) = ary;
        ;}
    break;

  case 105:
#line 1004 "ss_parser.yy"
    {
			float f[3] = {1,1,1};
			GET_CTX()->get_color_from_code(f,(yysemantic_stack_[(1) - (1)].itype));
			ss_array_object* ary = new ss_array_object(f, 3);
			GET_CTX()->add_object(ary);
			(yyval.ntype) = ary;
		 	DEBUG("Color/COLCODE_TOKEN\n");
		;}
    break;

  case 106:
#line 1013 "ss_parser.yy"
    {
			float f[3] = {1,1,1};
			GET_CTX()->get_color_from_name(f,(yysemantic_stack_[(1) - (1)].stype));
			ss_array_object* ary = new ss_array_object(f, 3);
			GET_CTX()->add_object(ary);
			(yyval.ntype) = ary;
		 	DEBUG("Color/IDENTIFIER\n");
		;}
    break;

  case 107:
#line 1022 "ss_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
			DEBUG("Color/Float3\n");
		;}
    break;

  case 108:
#line 1030 "ss_parser.yy"
    {
		 	(yyval.itype) = 1;
		;}
    break;

  case 109:
#line 1034 "ss_parser.yy"
    {
			(yyval.itype) = 0;
		;}
    break;


    /* Line 675 of lalr1.cc.  */
#line 1513 "ss_parser.cpp"
	default: break;
      }
    YY_SYMBOL_PRINT ("-> $$ =", yyr1_[yyn], &yyval, &yyloc);

    yypop_ (yylen);
    yylen = 0;
    YY_STACK_PRINT ();

    yysemantic_stack_.push (yyval);
    yylocation_stack_.push (yyloc);

    /* Shift the result of the reduction.  */
    yyn = yyr1_[yyn];
    yystate = yypgoto_[yyn - yyntokens_] + yystate_stack_[0];
    if (0 <= yystate && yystate <= yylast_
	&& yycheck_[yystate] == yystate_stack_[0])
      yystate = yytable_[yystate];
    else
      yystate = yydefgoto_[yyn - yyntokens_];
    goto yynewstate;

  /*------------------------------------.
  | yyerrlab -- here on detecting error |
  `------------------------------------*/
  yyerrlab:
    /* If not already recovering from an error, report this error.  */
    if (!yyerrstatus_)
      {
	++yynerrs_;
	error (yylloc, yysyntax_error_ (yystate, yytoken));
      }

    yyerror_range[0] = yylloc;
    if (yyerrstatus_ == 3)
      {
	/* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

	if (yychar <= yyeof_)
	  {
	  /* Return failure if at end of input.  */
	  if (yychar == yyeof_)
	    YYABORT;
	  }
	else
	  {
	    yydestruct_ ("Error: discarding", yytoken, &yylval, &yylloc);
	    yychar = yyempty_;
	  }
      }

    /* Else will try to reuse look-ahead token after shifting the error
       token.  */
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;

    yyerror_range[0] = yylocation_stack_[yylen - 1];
    /* Do not reclaim the symbols of the rule which action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    yystate = yystate_stack_[0];
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;	/* Each real token shifted decrements this.  */

    for (;;)
      {
	yyn = yypact_[yystate];
	if (yyn != yypact_ninf_)
	{
	  yyn += yyterror_;
	  if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
	    {
	      yyn = yytable_[yyn];
	      if (0 < yyn)
		break;
	    }
	}

	/* Pop the current state because it cannot handle the error token.  */
	if (yystate_stack_.height () == 1)
	YYABORT;

	yyerror_range[0] = yylocation_stack_[0];
	yydestruct_ ("Error: popping",
		     yystos_[yystate],
		     &yysemantic_stack_[0], &yylocation_stack_[0]);
	yypop_ ();
	yystate = yystate_stack_[0];
	YY_STACK_PRINT ();
      }

    if (yyn == yyfinal_)
      goto yyacceptlab;

    yyerror_range[1] = yylloc;
    // Using YYLLOC is tempting, but would change the location of
    // the look-ahead.  YYLOC is available though.
    YYLLOC_DEFAULT (yyloc, (yyerror_range - 1), 2);
    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yyloc);

    /* Shift the error token.  */
    YY_SYMBOL_PRINT ("Shifting", yystos_[yyn],
		   &yysemantic_stack_[0], &yylocation_stack_[0]);

    yystate = yyn;
    goto yynewstate;

    /* Accept.  */
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    /* Abort.  */
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (yychar != yyeof_ && yychar != yyempty_)
      yydestruct_ ("Cleanup: discarding lookahead", yytoken, &yylval, &yylloc);

    /* Do not reclaim the symbols of the rule which action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (yystate_stack_.height () != 1)
      {
	yydestruct_ ("Cleanup: popping",
		   yystos_[yystate_stack_[0]],
		   &yysemantic_stack_[0],
		   &yylocation_stack_[0]);
	yypop_ ();
      }

    return yyresult;
  }

  // Generate an error message.
  std::string
  ss_parser::yysyntax_error_ (int yystate, int tok)
  {
    std::string res;
    YYUSE (yystate);
#if YYERROR_VERBOSE
    int yyn = yypact_[yystate];
    if (yypact_ninf_ < yyn && yyn <= yylast_)
      {
	/* Start YYX at -YYN if negative to avoid negative indexes in
	   YYCHECK.  */
	int yyxbegin = yyn < 0 ? -yyn : 0;

	/* Stay within bounds of both yycheck and yytname.  */
	int yychecklim = yylast_ - yyn + 1;
	int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
	int count = 0;
	for (int x = yyxbegin; x < yyxend; ++x)
	  if (yycheck_[x + yyn] == x && x != yyterror_)
	    ++count;

	// FIXME: This method of building the message is not compatible
	// with internationalization.  It should work like yacc.c does it.
	// That is, first build a string that looks like this:
	// "syntax error, unexpected %s or %s or %s"
	// Then, invoke YY_ on this string.
	// Finally, use the string as a format to output
	// yytname_[tok], etc.
	// Until this gets fixed, this message appears in English only.
	res = "syntax error, unexpected ";
	res += yytnamerr_ (yytname_[tok]);
	if (count < 5)
	  {
	    count = 0;
	    for (int x = yyxbegin; x < yyxend; ++x)
	      if (yycheck_[x + yyn] == x && x != yyterror_)
		{
		  res += (!count++) ? ", expecting " : " or ";
		  res += yytnamerr_ (yytname_[x]);
		}
	  }
      }
    else
#endif
      res = YY_("syntax error");
    return res;
  }


  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
  const signed char ss_parser::yypact_ninf_ = -53;
  const short int
  ss_parser::yypact_[] =
  {
       206,   -53,   -53,    76,    62,   -53,   -53,   -53,   -53,   -53,
     -53,   -53,   -53,    15,   174,   -53,   -53,   -53,   186,   -53,
     -53,   -53,   -53,     2,   218,   -53,   247,    23,    46,   -53,
      85,    19,   -53,   -53,   -53,   -53,   -53,   -53,   186,   -53,
     -53,    90,   -53,   -53,     2,    16,   -53,    98,    98,    98,
      98,    98,    98,    42,    47,    98,    98,    98,    98,    98,
      98,    31,    36,    98,   -53,   -53,   247,   -53,    63,    85,
      98,   -53,    10,    52,   -53,    89,   104,   -53,    73,   -53,
     113,   -53,   -53,    98,   -53,    74,   -53,    98,   -53,   -53,
     -53,   -53,   -53,   -53,   -53,   -53,   115,    98,   -53,   -53,
     -53,    61,    54,   -53,    61,    61,    61,    61,    61,    98,
      98,   -53,    98,    61,    98,   -53,    61,    61,    61,    61,
      61,    61,   -53,   -53,    98,   -53,   -53,    98,    61,   -53,
     -53,   247,    80,   -53,   -53,    82,   -53,    83,   121,   -53,
      26,   124,    98,   -53,    86,    98,    98,    98,    98,    98,
      98,    44,    98,    61,   -53,   247,    76,   -53,   -53,   -53,
      98,    12,   -53,    98,    98,    61,    61,    54,    54,    98,
      98,    98,    98,   -53,   -53,   -53,    34,   -53,    14,    98,
      87,    98,    92,    88,    98,   -53,   -53,    98,    98,    98,
      91,    98,    98,    79,   -53,    98,    98,    98,    98,    98,
      94,    98,   -53,    98,    96,    95,   -53,    98,    93,    98,
     102,    98,   105,    98,    99,   -53
  };

  /* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
     doesn't specify something else to do.  Zero means the default is an
     error.  */
  const unsigned char
  ss_parser::yydefact_[] =
  {
         0,    87,    44,     0,     0,    45,    46,    48,    47,    49,
      50,    51,    61,     0,     2,     4,     6,     7,     0,     8,
      37,    38,    39,    41,     0,    55,     0,     0,     0,    20,
       0,     0,     1,     3,     5,    32,    36,    23,     0,    33,
      35,     0,    53,    40,    42,     0,    56,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    62,    59,     0,    63,     0,     0,
       0,    24,     0,     0,     9,    12,    87,   105,    84,    19,
       0,   109,   108,     0,    17,    18,    11,     0,    13,    14,
      15,    16,    10,    22,    34,    52,     0,     0,    43,    94,
      95,    65,    88,    91,    66,    67,    68,    69,    70,     0,
       0,    73,     0,    71,    91,    72,    74,    75,    76,    77,
      78,    79,   106,    82,     0,   107,    81,     0,    80,    57,
      64,     0,    29,    31,    25,     0,    26,     0,     0,    21,
       0,     0,    96,    54,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    83,    60,     0,     0,    27,    28,    85,
       0,     0,    86,    99,     0,    89,    90,    92,    93,     0,
       0,     0,     0,    99,    58,    30,     0,    97,     0,     0,
       0,     0,     0,     0,     0,    98,   100,     0,     0,     0,
       0,     0,     0,     0,   101,     0,     0,     0,     0,     0,
       0,     0,   102,     0,     0,     0,   103,     0,     0,     0,
       0,     0,     0,     0,     0,   104
  };

  /* YYPGOTO[NTERM-NUM].  */
  const short int
  ss_parser::yypgoto_[] =
  {
       -53,   -53,   -53,   148,    39,   -53,   -53,   -53,   100,    97,
     125,   -53,   133,    50,   -53,   -53,     3,   150,   -53,   -53,
     151,   111,   -49,    56,   -52,   -53,   -53,     5,   -47,   -33,
     -31,   -53,    33,   130,   -53,    55,   -53
  };

  /* YYDEFGOTO[NTERM-NUM].  */
  const signed char
  ss_parser::yydefgoto_[] =
  {
        -1,    13,    14,    15,    16,    31,    17,    18,    72,    73,
      37,    38,    39,    19,    20,    21,    22,    23,    45,    24,
      25,    26,    65,    66,    67,    84,    85,    27,   101,   102,
     103,    88,   125,    90,    98,    91,    92
  };

  /* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule which
     number is the opposite.  If zero, do what YYDEFACT says.  */
  const signed char ss_parser::yytable_ninf_ = -96;
  const short int
  ss_parser::yytable_[] =
  {
        87,   104,   105,   106,   107,   108,    28,   113,   116,   117,
     118,   119,   120,   121,   130,    32,   128,   129,    99,   100,
      99,   100,   110,   114,    70,    75,    76,    43,    77,    78,
     124,   124,    99,   100,    79,    74,    86,    99,   100,   133,
      77,   122,    99,   100,    41,    77,   122,   123,    99,   100,
      99,   100,   140,    99,   100,   134,   142,    36,    96,    69,
      70,    80,    81,    82,    89,    69,   144,   177,    40,   186,
      97,    68,    29,    83,   132,    30,   160,    36,   149,   150,
     153,   151,   154,   152,   184,   112,     2,   115,    40,   185,
     112,    71,     1,   152,   171,   -94,   109,   136,   165,   166,
      95,   112,   147,   130,    99,   100,   174,   148,    12,   161,
     -95,   163,   145,   146,   167,   168,   126,   127,   169,   170,
     172,   173,   138,   139,   141,   143,   156,   157,   158,   176,
     178,   159,   179,   180,   162,   197,   164,   189,   181,   179,
     182,   183,   184,   186,   203,   207,   194,   187,   188,   209,
     187,   206,   211,   190,   215,   213,   191,   192,   193,   175,
     195,   196,    34,    93,   198,   199,   200,   201,   202,   135,
     204,    94,   205,   137,    44,    46,   208,    33,   210,   131,
     212,     1,   214,   111,     2,     3,     4,   155,     0,     0,
       0,     0,     0,     1,     0,     0,     2,     0,     4,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     5,     6,
       7,     8,     9,     1,    10,    11,     2,     3,     4,    12,
       5,     6,     7,     8,     9,     1,    10,    11,     2,     0,
       0,    12,     0,    35,     0,     0,     0,     0,     0,     0,
       5,     6,     7,     8,     9,     0,    10,    11,     0,     0,
       0,    12,     5,     6,     7,     8,     9,    42,    10,    11,
       0,     0,     0,    12,    47,    48,    49,    50,    51,    52,
      53,    54,    55,    56,    57,    58,    59,    60,    61,    62,
      63,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    64
  };

  /* YYCHECK.  */
  const short int
  ss_parser::yycheck_[] =
  {
        31,    48,    49,    50,    51,    52,     3,    54,    55,    56,
      57,    58,    59,    60,    66,     0,    63,    66,     6,     7,
       6,     7,    53,    54,    14,     6,     7,    24,     9,    10,
      61,    62,     6,     7,    15,    30,    31,     6,     7,    70,
       9,    10,     6,     7,    42,     9,    10,    16,     6,     7,
       6,     7,    83,     6,     7,    45,    87,    18,    42,    13,
      14,    42,    43,    44,    31,    13,    97,    55,    18,    55,
      54,    48,    10,    54,    69,    13,    50,    38,   109,   110,
     127,   112,   131,   114,    50,    54,    10,    54,    38,    55,
      54,    45,     7,   124,    50,     6,    54,    45,   145,   146,
      10,    54,    48,   155,     6,     7,   155,    53,    45,   140,
       6,   142,    51,    52,   147,   148,    61,    62,   149,   150,
     151,   152,    49,    10,    50,    10,    46,    45,    45,   160,
     161,    10,   163,   164,    10,    56,    50,    50,   169,   170,
     171,   172,    50,    55,    50,    50,    55,   178,   179,    56,
     181,    55,    50,   184,    55,    50,   187,   188,   189,   156,
     191,   192,    14,    38,   195,   196,   197,   198,   199,    72,
     201,    38,   203,    73,    24,    24,   207,     3,   209,    68,
     211,     7,   213,    53,    10,    11,    12,   131,    -1,    -1,
      -1,    -1,    -1,     7,    -1,    -1,    10,    -1,    12,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    34,    35,
      36,    37,    38,     7,    40,    41,    10,    11,    12,    45,
      34,    35,    36,    37,    38,     7,    40,    41,    10,    -1,
      -1,    45,    -1,    47,    -1,    -1,    -1,    -1,    -1,    -1,
      34,    35,    36,    37,    38,    -1,    40,    41,    -1,    -1,
      -1,    45,    34,    35,    36,    37,    38,    39,    40,    41,
      -1,    -1,    -1,    45,    17,    18,    19,    20,    21,    22,
      23,    24,    25,    26,    27,    28,    29,    30,    31,    32,
      33,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    47
  };

  /* STOS_[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
  const unsigned char
  ss_parser::yystos_[] =
  {
         0,     7,    10,    11,    12,    34,    35,    36,    37,    38,
      40,    41,    45,    58,    59,    60,    61,    63,    64,    70,
      71,    72,    73,    74,    76,    77,    78,    84,    73,    10,
      13,    62,     0,     3,    60,    47,    61,    67,    68,    69,
      70,    42,    39,    73,    74,    75,    77,    17,    18,    19,
      20,    21,    22,    23,    24,    25,    26,    27,    28,    29,
      30,    31,    32,    33,    47,    79,    80,    81,    48,    13,
      14,    45,    65,    66,    84,     6,     7,     9,    10,    15,
      42,    43,    44,    54,    82,    83,    84,    87,    88,    89,
      90,    92,    93,    67,    69,    10,    42,    54,    91,     6,
       7,    85,    86,    87,    85,    85,    85,    85,    85,    54,
      87,    90,    54,    85,    87,    89,    85,    85,    85,    85,
      85,    85,    10,    16,    87,    89,    92,    92,    85,    79,
      81,    78,    84,    87,    45,    66,    45,    65,    49,    10,
      87,    50,    87,    10,    87,    51,    52,    48,    53,    87,
      87,    87,    87,    85,    79,    80,    46,    45,    45,    10,
      50,    87,    10,    87,    50,    85,    85,    86,    86,    87,
      87,    50,    87,    87,    79,    73,    87,    55,    87,    87,
      87,    87,    87,    87,    50,    55,    55,    87,    87,    50,
      87,    87,    87,    87,    55,    87,    87,    56,    87,    87,
      87,    87,    87,    50,    87,    87,    55,    50,    87,    56,
      87,    50,    87,    50,    87,    55
  };

#if YYDEBUG
  /* TOKEN_NUMBER_[YYLEX-NUM] -- Internal symbol number corresponding
     to YYLEX-NUM.  */
  const unsigned short int
  ss_parser::yytoken_number_[] =
  {
         0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   123,    62,   125,    42,    58,
      44,    43,    45,    47,    91,    93,    59
  };
#endif

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
  const unsigned char
  ss_parser::yyr1_[] =
  {
         0,    57,    58,    58,    59,    59,    60,    60,    60,    61,
      61,    61,    61,    61,    61,    61,    61,    61,    61,    61,
      62,    62,    63,    63,    64,    64,    64,    64,    64,    65,
      65,    66,    67,    68,    68,    69,    69,    70,    70,    71,
      71,    72,    72,    72,    73,    74,    74,    74,    74,    74,
      74,    74,    74,    75,    75,    76,    76,    77,    77,    77,
      77,    78,    79,    80,    80,    81,    81,    81,    81,    81,
      81,    81,    81,    81,    81,    81,    81,    81,    81,    81,
      81,    81,    81,    81,    82,    83,    83,    84,    85,    85,
      85,    86,    86,    86,    87,    87,    88,    88,    88,    89,
      89,    89,    90,    90,    91,    92,    92,    92,    93,    93
  };

  /* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
  const unsigned char
  ss_parser::yyr2_[] =
  {
         0,     2,     1,     2,     1,     2,     1,     1,     1,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       1,     3,     3,     2,     3,     4,     4,     5,     5,     2,
       4,     2,     1,     1,     2,     1,     1,     1,     1,     1,
       2,     1,     2,     3,     1,     1,     1,     1,     1,     1,
       1,     1,     3,     1,     3,     1,     2,     3,     5,     2,
       4,     1,     1,     1,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     3,     1,     3,     3,     1,     1,     3,
       3,     1,     3,     3,     1,     1,     2,     4,     5,     3,
       5,     7,     9,    11,    19,     1,     1,     1,     1,     1
  };

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
  /* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
     First, the terminals, then, starting at \a yyntokens_, nonterminals.  */
  const char*
  const ss_parser::yytname_[] =
  {
    "$end", "error", "$undefined", "EOF_TOKEN", "UNKNOWN_TOKEN",
  "INVALID_VALUE", "FLOAT_TOKEN", "INTEGER_TOKEN", "STRING_TOKEN",
  "COLCODE_TOKEN", "IDENTIFIER", "TOKEN_RULE", "TOKEN_SET",
  "TOKEN_MAXDEPTH", "TOKEN_WEIGHT", "TOKEN_INITIAL", "TOKEN_RANDOM",
  "TOKEN_X", "TOKEN_Y", "TOKEN_Z", "TOKEN_RX", "TOKEN_RY", "TOKEN_RZ",
  "TOKEN_M", "TOKEN_S", "TOKEN_FX", "TOKEN_FY", "TOKEN_FZ", "TOKEN_HUE",
  "TOKEN_SAT", "TOKEN_BRIGHTNESS", "TOKEN_COLOR", "TOKEN_BLEND",
  "TOKEN_ALPHA", "TOKEN_BOX", "TOKEN_GRID", "TOKEN_LINE", "TOKEN_SPHERE",
  "TOKEN_POINT", "TOKEN_TRIANGLE", "TOKEN_CYLINDER", "TOKEN_TUBE",
  "TOKEN_DOUBLE_COLON", "TOKEN_FALSE", "TOKEN_TRUE", "'{'", "'>'", "'}'",
  "'*'", "':'", "','", "'+'", "'-'", "'/'", "'['", "']'", "';'", "$accept",
  "File", "Requests", "Request", "SetOption", "OptionKey", "DefineRule",
  "RuleBegin", "ModifyMaxDepth", "ModifyWeight", "RuleEnd", "RuleContents",
  "RuleContent", "CallAction", "CallRule", "CallGeometry", "RuleSymbol",
  "GeometrySymbol", "TriangleSymbol", "BlockList", "Block", "BlockStart",
  "BlockEnd", "BlockContents", "CallTrans", "Symbol", "SymbolList",
  "Integer", "Expression", "Primary", "Float", "Float2", "Float3",
  "Float9", "TriangleList", "Color", "Boolean", 0
  };
#endif

#if YYDEBUG
  /* YYRHS -- A `-1'-separated list of the rules' RHS.  */
  const ss_parser::rhs_number_type
  ss_parser::yyrhs_[] =
  {
        58,     0,    -1,    59,    -1,    59,     3,    -1,    60,    -1,
      59,    60,    -1,    61,    -1,    63,    -1,    70,    -1,    12,
      13,    84,    -1,    12,    62,    93,    -1,    12,    62,    84,
      -1,    12,    62,     6,    -1,    12,    62,    88,    -1,    12,
      62,    89,    -1,    12,    62,    90,    -1,    12,    62,    92,
      -1,    12,    62,    82,    -1,    12,    62,    83,    -1,    12,
      62,    15,    -1,    10,    -1,    62,    42,    10,    -1,    64,
      68,    67,    -1,    64,    67,    -1,    11,    73,    45,    -1,
      11,    73,    65,    45,    -1,    11,    73,    66,    45,    -1,
      11,    73,    65,    66,    45,    -1,    11,    73,    66,    65,
      45,    -1,    13,    84,    -1,    13,    84,    46,    73,    -1,
      14,    87,    -1,    47,    -1,    69,    -1,    68,    69,    -1,
      70,    -1,    61,    -1,    71,    -1,    72,    -1,    73,    -1,
      76,    73,    -1,    74,    -1,    76,    74,    -1,    76,    75,
      91,    -1,    10,    -1,    34,    -1,    35,    -1,    37,    -1,
      36,    -1,    38,    -1,    40,    -1,    41,    -1,    74,    42,
      10,    -1,    39,    -1,    75,    42,    10,    -1,    77,    -1,
      76,    77,    -1,    78,    80,    79,    -1,    84,    48,    78,
      80,    79,    -1,    78,    79,    -1,    84,    48,    78,    79,
      -1,    45,    -1,    47,    -1,    81,    -1,    80,    81,    -1,
      17,    85,    -1,    18,    85,    -1,    19,    85,    -1,    20,
      85,    -1,    21,    85,    -1,    22,    85,    -1,    24,    85,
      -1,    24,    89,    -1,    23,    90,    -1,    25,    85,    -1,
      26,    85,    -1,    27,    85,    -1,    28,    85,    -1,    29,
      85,    -1,    30,    85,    -1,    33,    85,    -1,    31,    92,
      -1,    31,    16,    -1,    32,    92,    85,    -1,    10,    -1,
      10,    49,    10,    -1,    83,    50,    10,    -1,     7,    -1,
      86,    -1,    85,    51,    85,    -1,    85,    52,    85,    -1,
      87,    -1,    86,    48,    86,    -1,    86,    53,    86,    -1,
       6,    -1,     7,    -1,    87,    87,    -1,    54,    87,    87,
      55,    -1,    54,    87,    50,    87,    55,    -1,    87,    87,
      87,    -1,    54,    87,    87,    87,    55,    -1,    54,    87,
      50,    87,    50,    87,    55,    -1,    87,    87,    87,    87,
      87,    87,    87,    87,    87,    -1,    54,    87,    87,    87,
      87,    87,    87,    87,    87,    87,    55,    -1,    54,    87,
      50,    87,    50,    87,    56,    87,    50,    87,    50,    87,
      56,    87,    50,    87,    50,    87,    55,    -1,     9,    -1,
      10,    -1,    89,    -1,    44,    -1,    43,    -1
  };

  /* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
     YYRHS.  */
  const unsigned short int
  ss_parser::yyprhs_[] =
  {
         0,     0,     3,     5,     8,    10,    13,    15,    17,    19,
      23,    27,    31,    35,    39,    43,    47,    51,    55,    59,
      63,    65,    69,    73,    76,    80,    85,    90,    96,   102,
     105,   110,   113,   115,   117,   120,   122,   124,   126,   128,
     130,   133,   135,   138,   142,   144,   146,   148,   150,   152,
     154,   156,   158,   162,   164,   168,   170,   173,   177,   183,
     186,   191,   193,   195,   197,   200,   203,   206,   209,   212,
     215,   218,   221,   224,   227,   230,   233,   236,   239,   242,
     245,   248,   251,   254,   258,   260,   264,   268,   270,   272,
     276,   280,   282,   286,   290,   292,   294,   297,   302,   308,
     312,   318,   326,   336,   348,   368,   370,   372,   374,   376
  };

  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
  const unsigned short int
  ss_parser::yyrline_[] =
  {
         0,   176,   176,   178,   183,   185,   190,   192,   194,   199,
     206,   213,   221,   230,   242,   254,   266,   278,   286,   294,
     304,   308,   321,   325,   332,   342,   357,   371,   389,   410,
     418,   430,   441,   448,   450,   455,   457,   462,   464,   469,
     485,   503,   519,   534,   558,   565,   569,   573,   577,   581,
     585,   589,   593,   607,   611,   625,   633,   642,   648,   655,
     661,   671,   683,   692,   696,   702,   708,   714,   720,   726,
     732,   738,   744,   754,   765,   771,   777,   783,   789,   795,
     801,   807,   817,   823,   836,   843,   853,   866,   873,   877,
     881,   888,   892,   896,   903,   908,   915,   922,   929,   939,
     946,   953,   963,   974,   988,  1003,  1012,  1021,  1029,  1033
  };

  // Print the state stack on the debug stream.
  void
  ss_parser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (state_stack_type::const_iterator i = yystate_stack_.begin ();
	 i != yystate_stack_.end (); ++i)
      *yycdebug_ << ' ' << *i;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  ss_parser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    /* Print the symbols being reduced, and their result.  */
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
	       << " (line " << yylno << "), ";
    /* The symbols being reduced.  */
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
		       yyrhs_[yyprhs_[yyrule] + yyi],
		       &(yysemantic_stack_[(yynrhs) - (yyi + 1)]),
		       &(yylocation_stack_[(yynrhs) - (yyi + 1)]));
  }
#endif // YYDEBUG

  /* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
  ss_parser::token_number_type
  ss_parser::yytranslate_ (int t)
  {
    static
    const token_number_type
    translate_table[] =
    {
           0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,    48,    51,    50,    52,     2,    53,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    49,    56,
       2,     2,    46,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    54,     2,    55,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    45,     2,    47,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44
    };
    if ((unsigned int) t <= yyuser_token_number_max_)
      return translate_table[t];
    else
      return yyundef_token_;
  }

  const int ss_parser::yyeof_ = 0;
  const int ss_parser::yylast_ = 294;
  const int ss_parser::yynnts_ = 37;
  const int ss_parser::yyempty_ = -2;
  const int ss_parser::yyfinal_ = 32;
  const int ss_parser::yyterror_ = 1;
  const int ss_parser::yyerrcode_ = 256;
  const int ss_parser::yyntokens_ = 57;

  const unsigned int ss_parser::yyuser_token_number_max_ = 299;
  const ss_parser::token_number_type ss_parser::yyundef_token_ = 2;

} // namespace yy

#line 1039 "ss_parser.yy"


void yy::ss_parser::error(const yy::ss_parser::location_type&, const std::string& m)
{
	int line_number = LINENUMBER();
	fprintf(stderr, "parser error %s, L.%d\n", m.c_str(), line_number);
}

extern int _ss_lex_init(void** p);
extern int _ss_lex_destroy(void* p);

namespace ss
{
	int load_structure_synth(ss_context* ctx, ss_decoder* dec)
	{
		void* scanner;
		_ss_lex_init(&scanner);

		ss_parse_param* param = (ss_parse_param*)(scanner);
		param->ctx = ctx;
		param->dec = dec;
		param->is_in_block = 0;
		param->line_number = 1;

		yy::ss_parser parser(scanner);

		int nRet = parser.parse();

		_ss_lex_destroy(scanner);
		return nRet;
	}
}

#ifdef SS_UNIT_TEST

#include <iostream>
#include <fstream>
#include <sstream>

#include "ss_decoder.h"
#include "ss_preprocessor.h"
#include "ss_evaluator.h"

int main(int argc, char** argv)
{
	if(argc < 2)return -1;
	std::ifstream is(argv[1]);
	if(!is)return -1;

	int nRet = 0;

	std::stringstream ss;
	nRet = ss::ss_preprocessor::run(ss, is);
	if(nRet != 0)return nRet;

	ss_context ctx;
	ss_decoder dec(ss);

	nRet = ss::load_structure_synth(&ctx, &dec);
	if(nRet != 0)return nRet;

	ss_scene scn;
	nRet = ss::ss_evaluator::run(&scn, &ctx);
	if(nRet != 0)return nRet;

	printf("Geometry Size: %d\n", scn.get_geometry_size());

	return nRet;
}

#endif

