%skeleton "lalr1.cc"
%define "parser_class_name" "ss_parser"
%defines

%{
#ifdef _WIN32
#pragma warning(disable : 4786)
#pragma warning(disable : 4102)
#pragma warning(disable : 4996)
#pragma warning(disable : 4065)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define YYDEBUG 1
#define YYERROR_VERBOSE 1

#include <algorithm>
#include <iterator>
#include "ss_parse_param.h"
#include "ss_context.h"
#include "ss_array_object.h"

%}

%union{
	int   itype;
	float ftype;
	char  ctype;
	char* stype;
	ss::ss_object* ntype;
}

%pure-parser

// The parsing context.
%parse-param { void * scanner }
%lex-param   { void * scanner }

%locations
// %debug
%error-verbose

%{

#define yyerror(m) error(yylloc,m)

using namespace ss;

static
char* dup_(const char* szText)
{
	int l = strlen(szText);
	char* buffer = new char[l+1];
	buffer[l] = 0;
	strcpy(buffer,szText);
	return buffer;
}

#define DUP(s) dup_(s)

#define PARAM() ((ss_parse_param*)scanner)
#define GET_CTX() (((ss_parse_param*)scanner)->ctx)
#define GET_OPT() (GET_CTX()->get_global_options())
#define DEBUG(s)  (GET_CTX()->print(s))
#define LINENUMBER() (((ss_parse_param*)scanner)->line_number)

static
void release(const char* v){delete[] v;v=NULL;}

#define	YY_DECL	1

extern int _ss_lex(yy::ss_parser::semantic_type* yylval,
	 yy::ss_parser::location_type* yylloc,
	 void* scanner);

static
int yylex(yy::ss_parser::semantic_type* yylval,
	 yy::ss_parser::location_type* yylloc,
	 void* scanner)
{
	return _ss_lex(yylval, yylloc, scanner);
}



%}

/*tokens*/

%token <ctype> EOF_TOKEN
%token <ctype> UNKNOWN_TOKEN INVALID_VALUE
%token <ftype> FLOAT_TOKEN
%token <itype> INTEGER_TOKEN
%token <stype> STRING_TOKEN
%token <itype> COLCODE_TOKEN

%token <stype> IDENTIFIER

%token <ctype> TOKEN_RULE
%token <ctype> TOKEN_SET
%token <ctype> TOKEN_MAXDEPTH
%token <ctype> TOKEN_WEIGHT
%token <ctype> TOKEN_INITIAL
%token <ctype> TOKEN_RANDOM

%token <ctype> TOKEN_X
%token <ctype> TOKEN_Y
%token <ctype> TOKEN_Z
%token <ctype> TOKEN_RX
%token <ctype> TOKEN_RY
%token <ctype> TOKEN_RZ
%token <ctype> TOKEN_M
%token <ctype> TOKEN_S
%token <ctype> TOKEN_FX
%token <ctype> TOKEN_FY
%token <ctype> TOKEN_FZ

%token <ctype> TOKEN_HUE
%token <ctype> TOKEN_SAT
%token <ctype> TOKEN_BRIGHTNESS
%token <ctype> TOKEN_COLOR
%token <ctype> TOKEN_BLEND
%token <ctype> TOKEN_ALPHA

%token <ctype> TOKEN_BOX
%token <ctype> TOKEN_GRID
%token <ctype> TOKEN_LINE
%token <ctype> TOKEN_SPHERE
%token <ctype> TOKEN_POINT
%token <ctype> TOKEN_TRIANGLE
%token <ctype> TOKEN_CYLINDER
%token <ctype> TOKEN_TUBE

%token <ctype> TOKEN_DOUBLE_COLON

%token <ctype> TOKEN_FALSE
%token <ctype> TOKEN_TRUE

%type <ntype> ModifyWeight
%type <ntype> ModifyMaxDepth

%type <ntype> BlockList

%type <stype> RuleSymbol
%type <stype> GeometrySymbol
%type <stype> TriangleSymbol

%type <ntype> CallTrans

%type <ntype> Block
%type <ntype> BlockStart

%type <stype> Symbol
%type <stype> SymbolList

%type <ftype> Expression
%type <ftype> Primary

%type <stype> OptionKey

%type <itype> Integer
%type <ftype> Float
%type <itype> Boolean

%type <ntype> Color
%type <ntype> Float2
%type <ntype> Float3
%type <ntype> Float9
%type <ntype> TriangleList

%%

File
    : Requests
			{ ; }
	| Requests EOF_TOKEN
			{ ; }
	;

Requests
    : Request
            { ; }
	| Requests Request
            { ; }
	;

Request
    : SetOption
            { ; }
    | DefineRule
            { ; }
    | CallAction
            { ; }
    ;

SetOption
    : TOKEN_SET TOKEN_MAXDEPTH Integer
            {
                GET_OPT()->set_maxdepth($3);
				ss_set_option_action * act = new ss_set_option_action("maxdepth", $3);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
            }
	| TOKEN_SET OptionKey Boolean
            {
                ss_set_option_action * act = new ss_set_option_action($2, $3);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release($2);
            }
    | TOKEN_SET OptionKey Integer
            {
                ss_set_option_action * act = new ss_set_option_action($2, $3);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release($2);
                DEBUG("SetOption OptionKey Integer\n");
            }
    | TOKEN_SET OptionKey FLOAT_TOKEN
            {
                ss_set_option_action * act = new ss_set_option_action($2, $3);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release($2);
                
                DEBUG("SetOption OptionKey Float\n");
            }
    | TOKEN_SET OptionKey Float2
            {
                ss_array_object* ary = (ss_array_object*)$3;
                float f[2] = {};
                memcpy(f, ary->get_ptr(), sizeof(float)*2);
                
                ss_set_option_action * act = new ss_set_option_action($2, f, 2);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release($2);
                GET_CTX()->erase_object(ary);
            }
    | TOKEN_SET OptionKey Float3
            {
                ss_array_object* ary = (ss_array_object*)$3;
                float f[3] = {};
                memcpy(f, ary->get_ptr(), sizeof(float)*3);
                
                ss_set_option_action * act = new ss_set_option_action($2, f, 3);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release($2);
                GET_CTX()->erase_object(ary);
            }
    | TOKEN_SET OptionKey Float9
            {
                ss_array_object* ary = (ss_array_object*)$3;
                float f[9] = {};
                memcpy(f, ary->get_ptr(), sizeof(float)*9);
            
                ss_set_option_action * act = new ss_set_option_action($2, f, 9);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release($2);
                GET_CTX()->erase_object(ary);
            }
    | TOKEN_SET OptionKey Color
            {
                ss_array_object* ary = (ss_array_object*)$3;
                float f[3] = {};
                memcpy(f, ary->get_ptr(), sizeof(float)*3);
                
                ss_set_option_action * act = new ss_set_option_action($2, f, 3);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release($2);
                GET_CTX()->erase_object(ary);
            }
    | TOKEN_SET OptionKey Symbol
            {
                ss_set_option_action * act = new ss_set_option_action($2, $3);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release($2);
                release($3);
            }
    | TOKEN_SET OptionKey SymbolList
            {
                ss_set_option_action * act = new ss_set_option_action($2, $3);
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release($2);
                release($3);
            }
	| TOKEN_SET OptionKey TOKEN_INITIAL
            {
                ss_set_option_action * act = new ss_set_option_action($2, "initial");
                GET_CTX()->add_object(act);
                GET_CTX()->get_top_rule()->add_action(act);
                release($2);
            }
    ;

OptionKey
    : IDENTIFIER
        {
            $$ = $1;
        }
    | OptionKey TOKEN_DOUBLE_COLON IDENTIFIER
        {
            std::string s;
			s += $1;
			s += "::";
			s += $3;
			$$ = DUP(s.c_str());
            release($1);
            release($3);
        }
    ;

DefineRule
    : RuleBegin RuleContents RuleEnd
        {
            ;
        }
	| RuleBegin RuleEnd
        {
            ;
        }
    ;

RuleBegin
    : TOKEN_RULE RuleSymbol '{'
        {
            ss_rule* r = new ss_rule($2);
            r->set_maxdepth(GET_OPT()->get_maxdepth());

			GET_CTX()->add_object(r);
            GET_CTX()->push_rule(r);

			release($2);
        }
    | TOKEN_RULE RuleSymbol ModifyMaxDepth '{'
        {
            ss_rule* r = new ss_rule($2);
			ss_modify_maxdepth_action* modm = (ss_modify_maxdepth_action*)$3;

			r->set_maxdepth(modm->get_maxdepth());
			r->set_extra_rule_name(modm->get_extra_rule_name());

			GET_CTX()->add_object(r);
            GET_CTX()->push_rule(r);

			GET_CTX()->erase_object(modm);

			release($2);
        }
    | TOKEN_RULE RuleSymbol ModifyWeight '{'
        {
            ss_rule* r = new ss_rule($2);
			ss_modify_weight_action* modw = (ss_modify_weight_action*)$3;

			r->set_weight(modw->get_weight());

			GET_CTX()->add_object(r);
            GET_CTX()->push_rule(r);

			GET_CTX()->erase_object(modw);

			release($2);
        }
	| TOKEN_RULE RuleSymbol ModifyMaxDepth ModifyWeight '{'
        {
            ss_rule* r = new ss_rule($2);
			ss_modify_maxdepth_action* modm = (ss_modify_maxdepth_action*)$3;
			ss_modify_weight_action* modw = (ss_modify_weight_action*)$4;

			r->set_maxdepth(modm->get_maxdepth());
			r->set_extra_rule_name(modm->get_extra_rule_name());
			r->set_weight(modw->get_weight());

			GET_CTX()->add_object(r);
            GET_CTX()->push_rule(r);

			GET_CTX()->erase_object(modm);
			GET_CTX()->erase_object(modw);

			release($2);
        }
	| TOKEN_RULE RuleSymbol ModifyWeight ModifyMaxDepth '{'
        {
            ss_rule* r = new ss_rule($2);
			ss_modify_weight_action* modw = (ss_modify_weight_action*)$3;
			ss_modify_maxdepth_action* modm = (ss_modify_maxdepth_action*)$4;

			r->set_maxdepth(modm->get_maxdepth());
			r->set_extra_rule_name(modm->get_extra_rule_name());
			r->set_weight(modw->get_weight());

			GET_CTX()->add_object(r);
            GET_CTX()->push_rule(r);

			GET_CTX()->erase_object(modm);
			GET_CTX()->erase_object(modw);

			release($2);
        }
    ;

ModifyMaxDepth
	: TOKEN_MAXDEPTH Integer
		{
			ss_action* act = new ss_modify_maxdepth_action($2);
			GET_CTX()->add_object(act);
			$$ = act;

			DEBUG("ModifyMaxDepth\n");
		}
	| TOKEN_MAXDEPTH Integer '>' RuleSymbol
		{
			ss_action* act = new ss_modify_maxdepth_action($2, $4);
			GET_CTX()->add_object(act);
			$$ = act;

			DEBUG("ModifyMaxDepth\n");
			release($4);
		}
	;

ModifyWeight
	: TOKEN_WEIGHT Float
		{
			ss_action* act = new ss_modify_weight_action($2);
			GET_CTX()->add_object(act);
			$$ = act;

			DEBUG("ModifyWeight\n");
		}
	;

RuleEnd
    : '}'
        {
            GET_CTX()->pop_rule();
        }
    ;

RuleContents
    : RuleContent
        { ; }
    | RuleContents RuleContent
        { ; }
    ;

RuleContent
	: CallAction
        { ; }
    | SetOption
        { ; }
    ;

CallAction
    : CallRule
        { ; }
    | CallGeometry
        { ; }
    ;

CallRule
    : RuleSymbol
        {
			ss_blocklist* bl = new ss_blocklist();
			GET_CTX()->add_object(bl);

			ss_call_rule_action* act = new ss_call_rule_action($1);
			GET_CTX()->add_object(act);

			ss_blocked_action* bact = new ss_blocked_action(bl, act);
			GET_CTX()->add_object(bact);

			GET_CTX()->get_top_rule()->add_action(bact);

			DEBUG("RuleSymbol\n");
			release($1);
		}
    | BlockList RuleSymbol
        {
			ss_blocklist* bl = (ss_blocklist*)$1;

			ss_call_rule_action* act = new ss_call_rule_action($2);
			GET_CTX()->add_object(act);

			ss_blocked_action* bact = new ss_blocked_action(bl, act);
			GET_CTX()->add_object(bact);

			GET_CTX()->get_top_rule()->add_action(bact);

			DEBUG("BlockList RuleSymbol\n");
			release($2);
		}
    ;

CallGeometry
    : GeometrySymbol
        {
			ss_blocklist* bl = new ss_blocklist();
			GET_CTX()->add_object(bl);

			ss_geometry_action* act = ss_geometry_action_creator::create($1);
			GET_CTX()->add_object(act);

			ss_blocked_action* bact = new ss_blocked_action(bl, act);
			GET_CTX()->add_object(bact);

			GET_CTX()->get_top_rule()->add_action(bact);

			DEBUG("GeometrySymbol\n");
			release($1);
		}
    | BlockList GeometrySymbol
        {
			ss_blocklist* bl = (ss_blocklist*)$1;

			ss_geometry_action* act = ss_geometry_action_creator::create($2);
			GET_CTX()->add_object(act);

			ss_blocked_action* bact = new ss_blocked_action(bl, act);
			GET_CTX()->add_object(bact);

			GET_CTX()->get_top_rule()->add_action(bact);

			DEBUG("BlockList GeometrySymbol\n");
			release($2);
		}
    |  BlockList TriangleSymbol TriangleList
        {
            ss_blocklist* bl = (ss_blocklist*)$1;
            
            ss_array_object* ary = (ss_array_object*)$3;
			float f[9] = {};
			memcpy(f, ary->get_ptr(), sizeof(float)*9);

			ss_triangle_action* act = (ss_triangle_action*)ss_geometry_action_creator::create($2);
			act->set_positions(f);
            GET_CTX()->add_object(act);  

			ss_blocked_action* bact = new ss_blocked_action(bl, act);
			GET_CTX()->add_object(bact);

			GET_CTX()->get_top_rule()->add_action(bact);

			DEBUG("BlockList TriangleSymbol TriangleList\n");
			release($2);
            GET_CTX()->erase_object($3);
        }
    ;

RuleSymbol
    : IDENTIFIER
        {
		 	$$ = $1;
		}
    ;

GeometrySymbol
	: TOKEN_BOX
		{
			$$ = DUP("box");
		}
	| TOKEN_GRID
		{
			$$ = DUP("grid");
		}
	| TOKEN_SPHERE
		{
			$$ = DUP("sphere");
		}
	| TOKEN_LINE
		{
			$$ = DUP("line");
		}
	| TOKEN_POINT
		{
			$$ = DUP("point");
		}
	| TOKEN_CYLINDER
		{
			$$ = DUP("cylinder");
		}
	| TOKEN_TUBE
		{
			$$ = DUP("tube");
		}
	| GeometrySymbol TOKEN_DOUBLE_COLON IDENTIFIER
		{
			std::string s = "";
			s += $1;
			s += "::";
			s += $3;
			$$ = DUP(s.c_str());
		 	DEBUG("GeometrySymbol\n");
			release($1);
			release($3);
		}
	;
    
TriangleSymbol
    : TOKEN_TRIANGLE
		{
			$$ = DUP("triangle");
		}
	| TriangleSymbol TOKEN_DOUBLE_COLON IDENTIFIER
		{
			std::string s = "";
			s += $1;
			s += "::";
			s += $3;
			$$ = DUP(s.c_str());
		 	DEBUG("TriangleSymbol\n");
			release($1);
			release($3);
		}
	;

BlockList
    : Block
        {
		 	ss_blocklist* bl = new ss_blocklist();
			GET_CTX()->add_object(bl);
			bl->add_block((ss_block*)$1);
			$$ = bl;
			DEBUG("BlockList\n");
		}
    | BlockList Block
        {
			ss_blocklist* bl = (ss_blocklist*)$1;
			bl->add_block((ss_block*)$2);
			$$ = bl;
		}
    ;

Block
    : BlockStart BlockContents BlockEnd
        {
			ss_block* blk = (ss_block*)$1;
			$$ = blk;
		 	DEBUG("Block\n");
		}
	| Integer '*' BlockStart BlockContents BlockEnd
		{
			ss_block* blk = (ss_block*)$3;
			blk->set_times($1);
			$$ = blk;
			DEBUG("Block/Times\n");
		}
    | BlockStart BlockEnd
        {
			ss_block* blk = (ss_block*)$1;
			$$ = blk;
		 	DEBUG("Block\n");
		}
    | Integer '*' BlockStart BlockEnd
        {
			ss_block* blk = (ss_block*)$3;
            blk->set_times($1);
			$$ = blk;
		 	DEBUG("Block\n");
		}
    ;

BlockStart
	: '{'
		{
			PARAM()->is_in_block = 1;
			ss_block* blk = new ss_block();
			GET_CTX()->add_object(blk);
			GET_CTX()->push_block(blk);
			$$ = blk;
			DEBUG("BlockStart\n");
		}
	;

BlockEnd
	: '}'
		{
			GET_CTX()->pop_block();
			PARAM()->is_in_block = 0;
			DEBUG("BlockEnd\n");
		}
	;

BlockContents
    : CallTrans
        {
		 	GET_CTX()->get_top_block()->add_action((ss_action*)$1);
		}
    | BlockContents CallTrans
        {
		 	GET_CTX()->get_top_block()->add_action((ss_action*)$2);
		}

CallTrans
    : TOKEN_X Expression
        {
			ss_action* act = new ss_x_action($2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_Y Expression
		{
			ss_action* act = new ss_y_action($2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_Z Expression
		{
			ss_action* act = new ss_z_action($2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_RX Expression
		{
			ss_action* act = new ss_rx_action($2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_RY Expression
		{
			ss_action* act = new ss_ry_action($2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_RZ Expression
		{
			ss_action* act = new ss_rz_action($2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_S Expression
		{
			ss_action* act = new ss_s_action($2,$2,$2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_S Float3
		{
			ss_array_object* ary = (ss_array_object*)$2;
			float f[3] = {ary->get_at(0), ary->get_at(1), ary->get_at(2)};
			ss_action* act = new ss_s_action(f[0],f[1],f[2]);//TODO
			GET_CTX()->add_object(act);
			$$ = act;

			GET_CTX()->erase_object(ary);
		}
    | TOKEN_M Float9
		{
			ss_array_object* ary = (ss_array_object*)$2;
			float f[9] = {};
			memcpy(f, ary->get_ptr(), sizeof(float)*9);
			ss_action* act = new ss_m_action(f);
			GET_CTX()->add_object(act);
			$$ = act;

			GET_CTX()->erase_object(ary);
		}
    | TOKEN_FX Expression
		{
			ss_action* act = new ss_fx_action($2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_FY Expression
		{
			ss_action* act = new ss_fy_action($2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_FZ Expression
		{
			ss_action* act = new ss_fz_action($2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_HUE Expression
        {
			ss_action* act = new ss_hue_action($2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_SAT Expression
        {
			ss_action* act = new ss_sat_action($2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_BRIGHTNESS Expression
		{
			ss_action* act = new ss_brightness_action($2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_ALPHA Expression
        {
			ss_action* act = new ss_alpha_action($2);
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_COLOR Color
        {
			ss_array_object* ary = (ss_array_object*)$2;
			float f[3] = {ary->get_at(0), ary->get_at(1), ary->get_at(2)};
			ss_action* act = new ss_color_action(f[0],f[1],f[2]);
			GET_CTX()->add_object(act);
			$$ = act;

			GET_CTX()->erase_object(ary);
		}
    | TOKEN_COLOR TOKEN_RANDOM
        {
			ss_action* act = new ss_color_random_action();
			GET_CTX()->add_object(act);
			$$ = act;
		}
    | TOKEN_BLEND Color Expression
        {
			ss_array_object* ary = (ss_array_object*)$2;
			float f[3] = {ary->get_at(0), ary->get_at(1), ary->get_at(2)};
			ss_action* act = new ss_blend_action(f[0],f[1],f[2], $3);
			GET_CTX()->add_object(act);
			$$ = act;

			GET_CTX()->erase_object(ary);
		}
    ;

Symbol
    : IDENTIFIER
		{
			$$ = $1;
		}
    ;

SymbolList
    : IDENTIFIER ':' IDENTIFIER
		{
			std::string s;
			s += $1;
			s += ":";
			s += $3;
			$$ = DUP(s.c_str());
			release($1);
			release($3);
		}
	| SymbolList ',' IDENTIFIER
		{
			std::string s;
			s += $1;
			s += ",";
			s += $3;
			$$ = DUP(s.c_str());
			release($1);
			release($3);
		}
	;

Integer
    : INTEGER_TOKEN
        {
            $$ = $1;
        }
    ;

Expression
	: Primary
		{
			$$ = $1;
		}
	| Expression '+' Expression
		{
			$$ = $1 + $3;
		}
	| Expression '-' Expression
		{
			$$ = $1 - $3;
		}
	;

Primary
	: Float
		{
			$$ = $1;
		}
	| Primary '*' Primary
		{
			$$ = $1 * $3;
		}
	| Primary '/' Primary
		{
		 	$$ = $1 / $3;
		}
	;

Float
	: FLOAT_TOKEN
		{
			$$ = $1;
            DEBUG("Float\n");
		}
	| INTEGER_TOKEN
		{
			$$ = (float)$1;
		}
	;

Float2
    : Float Float
        {
			float f[2] = {$1, $2};
			ss_array_object* ary = new ss_array_object(f, 2);
			GET_CTX()->add_object(ary);
			$$ = ary;
		}
    | '[' Float Float ']'
        {
			float f[2] = {$2, $3};
			ss_array_object* ary = new ss_array_object(f, 2);
			GET_CTX()->add_object(ary);
			$$ = ary;
		}
	| '[' Float ',' Float ']'
        {
			float f[2] = {$2, $4};
			ss_array_object* ary = new ss_array_object(f, 2);
			GET_CTX()->add_object(ary);
			$$ = ary;
		}
    ;

Float3
    : Float Float Float
        {
			float f[3] = {$1, $2, $3};
			ss_array_object* ary = new ss_array_object(f, 3);
			GET_CTX()->add_object(ary);
			$$ = ary;
		}
    | '[' Float Float Float ']'
        {
			float f[3] = {$2, $3, $4};
			ss_array_object* ary = new ss_array_object(f, 3);
			GET_CTX()->add_object(ary);
			$$ = ary;
		}
	| '[' Float ',' Float ',' Float ']'
        {
			float f[3] = {$2, $4, $6};
			ss_array_object* ary = new ss_array_object(f, 3);
			GET_CTX()->add_object(ary);
			$$ = ary;
		}
    ;

Float9
    : Float Float Float Float Float Float Float Float Float
        {
			float f[9] = {
				$1, $2, $3,
				$4, $5, $6,
				$7, $8, $9,
			};
			ss_array_object* ary = new ss_array_object(f, 9);
			GET_CTX()->add_object(ary);
			$$ = ary;
		}
    | '[' Float Float Float Float Float Float Float Float Float ']'
        {
			float f[9] = {
				$2, $3, $4,
				$5, $6, $7,
				$8, $9, $10,
			};
			ss_array_object* ary = new ss_array_object(f, 9);
			GET_CTX()->add_object(ary);
			$$ = ary;
		}
    ;
    
TriangleList
    : '[' Float ',' Float ',' Float ';' Float ',' Float ',' Float ';' Float ',' Float ',' Float ']'
        {
            float f[9] = {
				$2, $4, $6,
				$8, $10, $12,
				$14, $16, $18,
			};
			ss_array_object* ary = new ss_array_object(f, 9);
			GET_CTX()->add_object(ary);
			$$ = ary;
        }
    ;
    

Color
    : COLCODE_TOKEN
        {
			float f[3] = {1,1,1};
			GET_CTX()->get_color_from_code(f,$1);
			ss_array_object* ary = new ss_array_object(f, 3);
			GET_CTX()->add_object(ary);
			$$ = ary;
		 	DEBUG("Color/COLCODE_TOKEN\n");
		}
    | IDENTIFIER
        {
			float f[3] = {1,1,1};
			GET_CTX()->get_color_from_name(f,$1);
			ss_array_object* ary = new ss_array_object(f, 3);
			GET_CTX()->add_object(ary);
			$$ = ary;
		 	DEBUG("Color/IDENTIFIER\n");
		}
    | Float3
        {
			$$ = $1;
			DEBUG("Color/Float3\n");
		}
    ;

Boolean
    : TOKEN_TRUE
        {
		 	$$ = 1;
		}
	| TOKEN_FALSE
		{
			$$ = 0;
		}
	;

%%

void yy::ss_parser::error(const yy::ss_parser::location_type&, const std::string& m)
{
	int line_number = LINENUMBER();
	fprintf(stderr, "parser error %s, L.%d\n", m.c_str(), line_number);
}

extern int _ss_lex_init(void** p);
extern int _ss_lex_destroy(void* p);

namespace ss
{
	int load_structure_synth(ss_context* ctx, ss_decoder* dec)
	{
		void* scanner;
		_ss_lex_init(&scanner);

		ss_parse_param* param = (ss_parse_param*)(scanner);
		param->ctx = ctx;
		param->dec = dec;
		param->is_in_block = 0;
		param->line_number = 1;

		yy::ss_parser parser(scanner);

		int nRet = parser.parse();

		_ss_lex_destroy(scanner);
		return nRet;
	}
}

#ifdef SS_UNIT_TEST

#include <iostream>
#include <fstream>
#include <sstream>

#include "ss_decoder.h"
#include "ss_preprocessor.h"
#include "ss_evaluator.h"

int main(int argc, char** argv)
{
	if(argc < 2)return -1;
	std::ifstream is(argv[1]);
	if(!is)return -1;

	int nRet = 0;

	std::stringstream ss;
	nRet = ss::ss_preprocessor::run(ss, is);
	if(nRet != 0)return nRet;

	ss_context ctx;
	ss_decoder dec(ss);

	nRet = ss::load_structure_synth(&ctx, &dec);
	if(nRet != 0)return nRet;

	ss_scene scn;
	nRet = ss::ss_evaluator::run(&scn, &ctx);
	if(nRet != 0)return nRet;

	printf("Geometry Size: %d\n", scn.get_geometry_size());

	return nRet;
}

#endif
