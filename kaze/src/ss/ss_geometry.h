
#ifndef SS_GEOMETRY_H
#define SS_GEOMETRY_H

#include "ss_object.h"
#include "ss_attributes.h"

namespace ss
{
    enum
    {
        GEOMETRY_UNKNOWN = 0,
        GEOMETRY_BOX,
        GEOMETRY_GRID,
        GEOMETRY_SPHERE,
        GEOMETRY_LINE,
        GEOMETRY_POINT,
        GEOMETRY_TRIANGLE,
        GEOMETRY_CYLINDER,
        GEOMETRY_TUBE,
    };

    class ss_geometry : public ss_object
    {
    public:
        virtual ~ss_geometry() {}
        virtual int get_type() const = 0;
        void set_attributes(const ss_attributes& attr) { attr_ = attr; }
        const ss_attributes& get_attributes() const { return attr_; }
    protected:
        ss_attributes attr_;
    };

    class ss_box_geometry : public ss_geometry
    {
    public:
        virtual int get_type() const { return GEOMETRY_BOX; }
    };

    class ss_grid_geometry : public ss_geometry
    {
    public:
        virtual int get_type() const { return GEOMETRY_GRID; }
    };

    class ss_sphere_geometry : public ss_geometry
    {
    public:
        virtual int get_type() const { return GEOMETRY_SPHERE; }
    };

    class ss_line_geometry : public ss_geometry
    {
    public:
        virtual int get_type() const { return GEOMETRY_LINE; }
    };

    class ss_point_geometry : public ss_geometry
    {
    public:
        virtual int get_type() const { return GEOMETRY_POINT; }
    };

    class ss_triangle_geometry : public ss_geometry
    {
    public:
        ss_triangle_geometry(const float pos[9]);
        virtual int get_type() const { return GEOMETRY_TRIANGLE; }
    public:
        const float* get_positions() const;

    private:
        float pos_[9];
    };

    class ss_cylinder_geometry : public ss_geometry
    {
    public:
        virtual int get_type() const { return GEOMETRY_CYLINDER; }
    };

    class ss_tube_geometry : public ss_geometry
    {
    public:
        virtual int get_type() const { return GEOMETRY_TUBE; }
    };
}


#endif
