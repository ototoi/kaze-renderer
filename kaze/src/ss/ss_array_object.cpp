#include "ss_array_object.h"

namespace ss
{
    ss_array_object::ss_array_object(int n)
        : v_(n)
    {
    }

    ss_array_object::ss_array_object(const float* f, int n)
        : v_(f, f + n)
    {
    }

    void ss_array_object::set_at(int i, float f)
    {
        v_[i] = f;
    }

    float ss_array_object::get_at(int i) const
    {
        return v_[i];
    }

    int ss_array_object::get_size() const
    {
        return (int)v_.size();
    }

    float* ss_array_object::get_ptr()
    {
        return &v_[0];
    }

    const float* ss_array_object::get_ptr() const
    {
        return &v_[0];
    }
}

