#include "ss_rule.h"

namespace ss
{
    ss_rule::ss_rule(const std::string& name)
        : name_(name)
    {
        weight_ = 1.0f;
        maxdepth_ = 1000;
    }

    ss_rule::~ss_rule()
    {
        ;
    }

    void ss_rule::add_action(const ss_action* act)
    {
        actions_.push_back(act);
    }

    const std::vector<const ss_action*>& ss_rule::get_actions() const
    {
        return actions_;
    }
}

