#include "ss_context.h"
#include "ss_color.h"
#include <stdio.h>

namespace ss
{
    ss_context::ss_context()
    {
        gOpt_ = new ss_options();
        gRule_ = new ss_rule("__global__");
        this->add_object(gOpt_);
        this->add_object(gRule_);

        gRule_->set_maxdepth(gOpt_->get_maxdepth());
        gRule_->set_weight(1.0f);

        this->push_rule(gRule_);
    }

    ss_context::~ss_context()
    {
        typedef std::set<ss_object*>::iterator iterator;
        for (iterator it = objSet_.begin(); it != objSet_.end(); it++)
        {
            if (*it)
            {
                delete *it;
            }
        }
    }

    ss_options* ss_context::get_global_options()
    {
        return gOpt_;
    }

    ss_rule* ss_context::get_global_rule()
    {
        return gRule_;
    }

    const ss_options* ss_context::get_global_options() const
    {
        return gOpt_;
    }

    const ss_rule* ss_context::get_global_rule() const
    {
        return gRule_;
    }

    ss_rule* ss_context::get_top_rule()
    {
        if (!ruleStack_.empty())
        {
            return ruleStack_.back();
        }
        return gRule_;
    }

    void ss_context::push_rule(ss_rule* r)
    {
        std::string name = r->get_name();
        ruleMap_[name].push_back(r);
        ruleStack_.push_back(r);
    }

    void ss_context::pop_rule()
    {
        if (!ruleStack_.empty())
        {
            ruleStack_.pop_back();
        }
    }

    ss_block* ss_context::get_top_block()
    {
        if (!blockStack_.empty())
        {
            return blockStack_.back();
        }
        return NULL;
    }

    void ss_context::push_block(ss_block* r)
    {
        blockStack_.push_back(r);
    }

    void ss_context::pop_block()
    {
        if (!blockStack_.empty())
        {
            blockStack_.pop_back();
        }
    }

    ss_object* ss_context::add_object(ss_object* obj)
    {
        objSet_.insert(obj);
        return obj;
    }

    void ss_context::erase_object(ss_object* obj)
    {
        objSet_.erase(obj);
        delete obj;
    }

    void ss_context::get_color_from_code(float f[3], unsigned int c) const
    {
        float r = (0xFF & (c >> 16)) / 255.0f;
        float g = (0xFF & (c >> 8)) / 255.0f;
        float b = (0xFF & (c)) / 255.0f;
        f[0] = r;
        f[1] = g;
        f[2] = b;
    }

    void ss_context::get_color_from_name(float f[3], const std::string& name) const
    {
        int code = ss_color::eval_name(name.c_str());
        if (code > 0)
        {
            this->get_color_from_code(f, code);
        }
        else
        {
            return this->get_color_from_code(f, 0xFFFFFF);
        }
    }

    typedef ss_context::RuleMapType RuleMapType;

    const RuleMapType& ss_context::get_rule_map() const
    {
        return ruleMap_;
    }

    void ss_context::print(const char* szText)
    {
        //printf(szText);
    }
}

