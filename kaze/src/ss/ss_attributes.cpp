#include "ss_attributes.h"
#include "ss_color.h"

#define _USE_MATH_DEFINES
#include <math.h>

#include <limits>

#define PI M_PI
#define MAT(m, i, j) (m[4 * i + j])

namespace ss
{
    static const float IDENTITY[] = {
        1, 0, 0, 0,
        0, 1, 0, 0,
        0, 0, 1, 0,
        0, 0, 0, 1,
    };

    static const float INITCOL[] = {
        1, 0, 0, 1,
    };

    static float fract(float x)
    {
        return x - floor(x);
    }

    static float clamp(float x)
    {
        return std::min(std::max(0.0f, x), 1.0f);
    }

    static void mul_matrix(float* out, const float* lhs, const float* rhs)
    {
        out[0] = lhs[0] * rhs[0] + lhs[1] * rhs[4] + lhs[2] * rhs[8] + lhs[3] * rhs[12];
        out[1] = lhs[0] * rhs[1] + lhs[1] * rhs[5] + lhs[2] * rhs[9] + lhs[3] * rhs[13];
        out[2] = lhs[0] * rhs[2] + lhs[1] * rhs[6] + lhs[2] * rhs[10] + lhs[3] * rhs[14];
        out[3] = lhs[0] * rhs[3] + lhs[1] * rhs[7] + lhs[2] * rhs[11] + lhs[3] * rhs[15];

        out[4] = lhs[4] * rhs[0] + lhs[5] * rhs[4] + lhs[6] * rhs[8] + lhs[7] * rhs[12];
        out[5] = lhs[4] * rhs[1] + lhs[5] * rhs[5] + lhs[6] * rhs[9] + lhs[7] * rhs[13];
        out[6] = lhs[4] * rhs[2] + lhs[5] * rhs[6] + lhs[6] * rhs[10] + lhs[7] * rhs[14];
        out[7] = lhs[4] * rhs[3] + lhs[5] * rhs[7] + lhs[6] * rhs[11] + lhs[7] * rhs[15];

        out[8] = lhs[8] * rhs[0] + lhs[9] * rhs[4] + lhs[10] * rhs[8] + lhs[11] * rhs[12];
        out[9] = lhs[8] * rhs[1] + lhs[9] * rhs[5] + lhs[10] * rhs[9] + lhs[11] * rhs[13];
        out[10] = lhs[8] * rhs[2] + lhs[9] * rhs[6] + lhs[10] * rhs[10] + lhs[11] * rhs[14];
        out[11] = lhs[8] * rhs[3] + lhs[9] * rhs[7] + lhs[10] * rhs[11] + lhs[11] * rhs[15];

        out[12] = lhs[12] * rhs[0] + lhs[13] * rhs[4] + lhs[14] * rhs[8] + lhs[15] * rhs[12];
        out[13] = lhs[12] * rhs[1] + lhs[13] * rhs[5] + lhs[14] * rhs[9] + lhs[15] * rhs[13];
        out[14] = lhs[12] * rhs[2] + lhs[13] * rhs[6] + lhs[14] * rhs[10] + lhs[15] * rhs[14];
        out[15] = lhs[12] * rhs[3] + lhs[13] * rhs[7] + lhs[14] * rhs[11] + lhs[15] * rhs[15];
    }

    static void apply_matrix(float* out, const float lhs[], const float* rhs)
    {
        out[0] = lhs[0] * rhs[0] + lhs[1] * rhs[4] + lhs[2] * rhs[8] + 1 * rhs[12];
        out[1] = lhs[0] * rhs[1] + lhs[1] * rhs[5] + lhs[2] * rhs[9] + 1 * rhs[13];
        out[2] = lhs[0] * rhs[2] + lhs[1] * rhs[6] + lhs[2] * rhs[10] + 1 * rhs[14];
        out[3] = lhs[0] * rhs[3] + lhs[1] * rhs[7] + lhs[2] * rhs[11] + 1 * rhs[15];
    }

    static float length(const float a[3], const float b[3])
    {
        float c[] = {b[0] - a[0], b[1] - a[1], b[2] - a[2]};
        return sqrt(c[0] * c[0] + c[1] * c[1] + c[2] * c[2]);
    }

    ss_attributes::ss_attributes()
    {
        memcpy(mat_, IDENTITY, sizeof(float) * 16);
        memcpy(rgba_, INITCOL, sizeof(float) * 4);
    }

    ss_attributes::ss_attributes(const ss_attributes& attr)
    {
        memcpy(mat_, attr.mat_, sizeof(float) * 16);
        memcpy(rgba_, attr.rgba_, sizeof(float) * 4);
    }

    void ss_attributes::action_x(float v)
    {
        translate(v, 0, 0);
    }

    void ss_attributes::action_y(float v)
    {
        translate(0, v, 0);
    }

    void ss_attributes::action_z(float v)
    {
        translate(0, 0, v);
    }

    void ss_attributes::action_rx(float v)
    {
        rotate(v, 1, 0, 0);
    }

    void ss_attributes::action_ry(float v)
    {
        rotate(v, 0, 1, 0);
    }

    void ss_attributes::action_rz(float v)
    {
        rotate(v, 0, 0, 1);
    }

    void ss_attributes::action_s(float x, float y, float z)
    {
        scale(x, y, z);
    }

    void ss_attributes::action_sx(float v)
    {
        scale(v, 1, 1);
    }

    void ss_attributes::action_sy(float v)
    {
        scale(1, v, 1);
    }

    void ss_attributes::action_sz(float v)
    {
        scale(1, 1, v);
    }

    void ss_attributes::action_fx(float v)
    {
    }

    void ss_attributes::action_fy(float v)
    {
    }

    void ss_attributes::action_fz(float v)
    {
    }

    void ss_attributes::action_m(const float m[9])
    {
        float tmp[16] = {
            m[0], m[1], m[2], 0,
            m[3], m[4], m[5], 0,
            m[6], m[7], m[8], 0,
            0, 0, 0, 1,
        };
        concat(tmp);
    }

    void ss_attributes::action_hue(float v)
    {
        float hsv[3];
        ss_color::rgb_to_hsv(hsv, rgba_);
        hsv[0] = fract(hsv[0] + v / 360.0f);
        ss_color::hsv_to_rgb(rgba_, hsv);
    }

    void ss_attributes::action_sat(float v)
    {
        float hsv[3];
        ss_color::rgb_to_hsv(hsv, rgba_);
        hsv[1] = clamp(hsv[1] * v);
        ss_color::hsv_to_rgb(rgba_, hsv);
    }

    void ss_attributes::action_brightness(float v)
    {
        float hsv[3];
        ss_color::rgb_to_hsv(hsv, rgba_);
        hsv[2] = clamp(hsv[2] * v);
        ss_color::hsv_to_rgb(rgba_, hsv);
    }

    void ss_attributes::action_alpha(float v)
    {
        rgba_[3] = clamp(rgba_[3] * v);
    }

    void ss_attributes::action_color(const float c[3])
    {
        memcpy(rgba_, c, sizeof(float) * 3);
    }

    void ss_attributes::action_blend(const float c[3], float s)
    {
        s = clamp(s);
        for (int i = 0; i < 3; i++)
            rgba_[i] = s * c[i] + (1 - s) * rgba_[i];
    }

    //void ss_attributes::action_color_random()
    //{}

    //void ss_attributes::action_set_color_scheme(const std::string& scheme)
    //{}

    //
    void ss_attributes::concat(const float mat[4 * 4])
    {
        float tmp[16];
        mul_matrix(tmp, mat, mat_);
        memcpy(mat_, tmp, sizeof(float) * 16);
    }

    void ss_attributes::translate(float x, float y, float z)
    {
        float tmp[16] = {
            1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            x, y, z, 1,
        };
        concat(tmp);
    }

    void ss_attributes::scale(float x, float y, float z)
    {
        float tmp[16] = {
            x, 0, 0, 0,
            0, y, 0, 0,
            0, 0, z, 0,
            0, 0, 0, 1,
        };
        concat(tmp);
    }

    static float radians(float x)
    {
        return (float)PI / 180.0f * x;
    }
    void ss_attributes::rotate(float angle, float dx, float dy, float dz)
    {
        float rad = radians(angle);
        float s = sin(rad);
        float c = cos(rad);
        float t = 1.0f - c;

        float x = dx;
        float y = dy;
        float z = dz;

        float m00 = t * x * x + c;
        float m11 = t * y * y + c;
        float m22 = t * z * z + c;
        float m33 = 1.0f;

        float txy = t * x * y;
        float sz = s * z;

        float txz = t * x * z;
        float sy = s * y;

        float tyz = t * y * z;
        float sx = s * x;

        float tmp[16] = {0};

        MAT(tmp, 0, 0) = m00; //00
        MAT(tmp, 1, 1) = m11; //11
        MAT(tmp, 2, 2) = m22; //22
        MAT(tmp, 3, 3) = m33; //33

        MAT(tmp, 0, 1) = txy + sz; //01
        MAT(tmp, 1, 0) = txy - sz; //10

        MAT(tmp, 0, 2) = txz - sy; //02
        MAT(tmp, 2, 0) = txz + sy; //20

        MAT(tmp, 1, 2) = tyz + sx; //12
        MAT(tmp, 2, 1) = tyz - sx; //21

        concat(tmp);
    }

    const float* ss_attributes::get_transform_ptr() const
    {
        return mat_;
    }

    const float* ss_attributes::get_color_ptr() const
    {
        return rgba_;
    }

    void ss_attributes::apply(float vec[3]) const
    {
        float out[] = {};
        apply_matrix(out, vec, mat_);
        memcpy(vec, out, sizeof(float) * 3);
    }

    bool ss_attributes::is_minmax(float a, float b) const
    {
        float vecs[24] = {
            -0.5, -0.5, -0.5,
            -0.5, -0.5, +0.5,
            -0.5, +0.5, -0.5,
            -0.5, +0.5, +0.5,
            +0.5, -0.5, -0.5,
            +0.5, -0.5, +0.5,
            +0.5, +0.5, -0.5,
            +0.5, +0.5, +0.5,
        };
        for (int i = 0; i < 8; i++)
        {
            this->apply(vecs + 3 * i);
        }
        float aa = std::numeric_limits<float>::max();
        float bb = std::numeric_limits<float>::min();
        for (int i = 0; i < 8 - 1; i++)
        {
            for (int j = i + 1; j < 8; j++)
            {
                float l = length(vecs + 3 * i, vecs + 3 * j);
                aa = std::min(aa, l);
                bb = std::max(bb, l);
            }
        }
        if (aa < a) return true;
        if (b < bb) return true;
        return false;
    }
}

