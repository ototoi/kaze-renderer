#include "ss_color.h"
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <algorithm>

#if defined(_WIN32)
#define STRCOMP(A, B) _stricmp(A, B)
#else
#define STRCOMP(A, B) strcasecmp(A, B)
#endif

namespace ss
{
    static int code(char c)
    {
        if ('0' <= c && c <= '9')
        {
            return c - '0';
        }
        else if ('a' <= c && c <= 'f')
        {
            return 10 + c - 'a';
        }
        else if ('A' <= c && c <= 'F')
        {
            return 10 + c - 'A';
        }
        return 0xF;
    }

#define DEFCOLOR(NAME, CODE) \
{                        \
    NAME, CODE           \
}

    struct _tag_color
    {
        const char* name;
        unsigned int code;
    } COLORS[] =
        {
            DEFCOLOR("white", 0xFFFFFF),
            DEFCOLOR("whitesmoke", 0xF5F5F5),
            DEFCOLOR("gainsboro", 0xDCDCDC),
            DEFCOLOR("lightgrey", 0xD3D3D3),
            DEFCOLOR("silver", 0xC0C0C0),
            DEFCOLOR("darkgray", 0xA9A9A9),
            DEFCOLOR("gray", 0x808080),
            DEFCOLOR("dimgray", 0x696969),
            DEFCOLOR("black", 0x000000),
            DEFCOLOR("snow", 0xFFFAFA),
            DEFCOLOR("lightcoral", 0xF08080),
            DEFCOLOR("rosybrown", 0xBC8F8F),
            DEFCOLOR("indianred", 0xCD5C5C),
            DEFCOLOR("red", 0xFF0000),
            DEFCOLOR("firebrick", 0xB22222),
            DEFCOLOR("brown", 0xA52A2A),
            DEFCOLOR("darkred", 0x8B0000),
            DEFCOLOR("maroon", 0x800000),
            DEFCOLOR("mistyrose", 0xFFE4E1),
            DEFCOLOR("salmon", 0xFA8072),
            DEFCOLOR("tomato", 0xFF6347),
            DEFCOLOR("darksalmon", 0xE9967A),
            DEFCOLOR("lightsalmon", 0xFFA07A),
            DEFCOLOR("coral", 0xFF7F50),
            DEFCOLOR("orangered", 0xFF4500),
            DEFCOLOR("sienna", 0xA0522D),
            DEFCOLOR("seashell", 0xFFF5EE),
            DEFCOLOR("chocolate", 0xD2691E),
            DEFCOLOR("saddlebrown", 0x8B4513),
            DEFCOLOR("sandybrown", 0xF4A460),
            DEFCOLOR("peachpuff", 0xFFDAB9),
            DEFCOLOR("linen", 0xFAF0E6),
            DEFCOLOR("peru", 0xCD853F),
            DEFCOLOR("bisque", 0xFFE4C4),
            DEFCOLOR("darkorange", 0xFF8C00),
            DEFCOLOR("antiquewhite", 0xFAEBD7),
            DEFCOLOR("burlywood", 0xDEB887),
            DEFCOLOR("tan", 0xD2B48C),
            DEFCOLOR("blanchedalmond", 0xFFEBCD),
            DEFCOLOR("navajowhite", 0xFFDEAD),
            DEFCOLOR("papayawhip", 0xFFEFD5),
            DEFCOLOR("moccasin", 0xFFE4B5),
            DEFCOLOR("oldlace", 0xFDF5E6),
            DEFCOLOR("wheat", 0xF5DEB3),
            DEFCOLOR("orange", 0xFFA500),
            DEFCOLOR("floralwhite", 0xFFFAF0),
            DEFCOLOR("darkgoldenrod", 0xB8860B),
            DEFCOLOR("goldenrod", 0xDAA520),
            DEFCOLOR("cornsilk", 0xFFF8DC),
            DEFCOLOR("gold", 0xFFD700),
            DEFCOLOR("lemonchiffon", 0xFFFACD),
            DEFCOLOR("palegoldenrod", 0xEEE8AA),
            DEFCOLOR("khaki", 0xF0E68C),
            DEFCOLOR("darkkhaki", 0xBDB76B),
            DEFCOLOR("ivory", 0xFFFFF0),
            DEFCOLOR("lightyellow", 0xFFFFE0),
            DEFCOLOR("beige", 0xF5F5DC),
            DEFCOLOR("lightgoldenrodyellow", 0xFAFAD2),
            DEFCOLOR("yellow", 0xFFFF00),
            DEFCOLOR("olive", 0x808000),
            DEFCOLOR("yellowgreen", 0x9ACD32),
            DEFCOLOR("olivedrab", 0x6B8E23),
            DEFCOLOR("darkolivegreen", 0x556B2F),
            DEFCOLOR("greenyellow", 0xADFF2F),
            DEFCOLOR("chartreuse", 0x7FFF00),
            DEFCOLOR("lawngreen", 0x7CFC00),
            DEFCOLOR("honeydew", 0xF0FFF0),
            DEFCOLOR("palegreen", 0x98FB98),
            DEFCOLOR("lightgreen", 0x90EE90),
            DEFCOLOR("darkseagreen", 0x8FBC8F),
            DEFCOLOR("lime", 0x00FF00),
            DEFCOLOR("limegreen", 0x32CD32),
            DEFCOLOR("forestgreen", 0x228B22),
            DEFCOLOR("green", 0x008000),
            DEFCOLOR("darkgreen", 0x006400),
            DEFCOLOR("mediumseagreen", 0x3CB371),
            DEFCOLOR("seagreen", 0x2E8B57),
            DEFCOLOR("mintcream", 0xF5FFFA),
            DEFCOLOR("springgreen", 0x00FF7F),
            DEFCOLOR("mediumaquamarine", 0x66CDAA),
            DEFCOLOR("turquoise", 0x40E0D0),
            DEFCOLOR("lightseagreen", 0x20B2AA),
            DEFCOLOR("mediumturquoise", 0x48D1CC),
            DEFCOLOR("mediumspringgreen", 0x00FA9A),
            DEFCOLOR("aquamarine", 0x7FFFD4),
            DEFCOLOR("azure", 0xF0FFFF),
            DEFCOLOR("lightcyan", 0xE0FFFF),
            DEFCOLOR("paleturquoise", 0xAFEEEE),
            DEFCOLOR("cyan", 0x00FFFF),
            DEFCOLOR("aqua", 0x00FFFF),
            DEFCOLOR("darkcyan", 0x008B8B),
            DEFCOLOR("teal", 0x008080),
            DEFCOLOR("darkslategray", 0x2F4F4F),
            DEFCOLOR("cadetblue", 0x5F9EA0),
            DEFCOLOR("darkturquoise", 0x00CED1),
            DEFCOLOR("powderblue", 0xB0E0E6),
            DEFCOLOR("lightblue", 0xADD8E6),
            DEFCOLOR("deepskyblue", 0x00BFFF),
            DEFCOLOR("skyblue", 0x87CEEB),
            DEFCOLOR("lightskyblue", 0x87CEFA),
            DEFCOLOR("steelblue", 0x4682B4),
            DEFCOLOR("aliceblue", 0xF0F8FF),
            DEFCOLOR("dodgerblue", 0x1E90FF),
            DEFCOLOR("lightslategray", 0x778899),
            DEFCOLOR("slategray", 0x708090),
            DEFCOLOR("lightsteelblue", 0xB0C4DE),
            DEFCOLOR("cornflowerblue", 0x6495ED),
            DEFCOLOR("royalblue", 0x4169E1),
            DEFCOLOR("ghostwhite", 0xF8F8FF),
            DEFCOLOR("lavender", 0xE6E6FA),
            DEFCOLOR("blue", 0x0000FF),
            DEFCOLOR("mediumblue", 0x0000CD),
            DEFCOLOR("darkblue", 0x00008B),
            DEFCOLOR("midnightblue", 0x191970),
            DEFCOLOR("navy", 0x000080),
            DEFCOLOR("mediumslateblue", 0x7B68EE),
            DEFCOLOR("slateblue", 0x6A5ACD),
            DEFCOLOR("darkslateblue", 0x483D8B),
            DEFCOLOR("mediumpurple", 0x9370DB),
            DEFCOLOR("blueviolet", 0x8A2BE2),
            DEFCOLOR("indigo", 0x4B0082),
            DEFCOLOR("darkorchid", 0x9932CC),
            DEFCOLOR("darkviolet", 0x9400D3),
            DEFCOLOR("mediumorchid", 0xBA55D3),
            DEFCOLOR("thistle", 0xD8BFD8),
            DEFCOLOR("plum", 0xDDA0DD),
            DEFCOLOR("violet", 0xEE82EE),
            DEFCOLOR("magenta", 0xFF00FF),
            DEFCOLOR("fuchsia", 0xFF00FF),
            DEFCOLOR("darkmagenta", 0x8B008B),
            DEFCOLOR("purple", 0x800080),
            DEFCOLOR("orchid", 0xDA70D6),
            DEFCOLOR("mediumvioletred", 0xC71585),
            DEFCOLOR("deeppink", 0xFF1493),
            DEFCOLOR("hotpink", 0xFF69B4),
            DEFCOLOR("lavenderblush", 0xFFF0F5),
            DEFCOLOR("palevioletred", 0xDB7093),
            DEFCOLOR("crimson", 0xDC143C),
            DEFCOLOR("pink", 0xFFC0CB),
            DEFCOLOR("lightpink", 0xFFB6C1),
            DEFCOLOR(NULL, 0xFFFFFF),
    };

    static float fract(float x)
    {
        return x - floor(x);
    }

    static float clamp(float x)
    {
        return std::min(std::max(0.0f, x), 1.0f);
    }

    static void Hue(float rgb2[3], float hue)
    {
        float rgb[3] = {fract(hue + 0.0f), fract(hue + 2 / 3.0f), fract(hue + 1 / 3.0f)};
        for (int i = 0; i < 3; i++)
            rgb2[i] = clamp(3 * fabs(2 * rgb[i] - 1) - 1);
    }

    void HSVtoRGB(float rgb[3], const float hsv[3])
    {
        float hue[3];
        Hue(hue, hsv[0]);
        for (int i = 0; i < 3; i++)
            rgb[i] = ((hue[i] - 1) * hsv[1] + 1) * hsv[2];
    }

    void RGBtoHSV(float hsv[3], const float c[3])
    {
        float r = c[0];
        float g = c[1];
        float b = c[2];

        int nMAX = 0;
        int nMIN = 0;

        if (c[nMAX] < c[1]) nMAX = 1;
        if (c[nMAX] < c[2]) nMAX = 2;

        if (c[nMIN] > c[1]) nMIN = 1;
        if (c[nMIN] > c[2]) nMIN = 2;

        float min = c[nMIN];
        float max = c[nMAX];

        float h = max - min;
        if (h > 0.0f)
        {
            if (nMAX == 0)
            {
                h = (g - b) / h;
                if (h < 0.0f)
                {
                    h += 6.0f;
                }
            }
            else if (nMAX == 1)
            {
                h = 2.0f + (b - r) / h;
            }
            else
            {
                h = 4.0f + (r - g) / h;
            }
        }
        else
        {
            h = 0.0f;
        }
        h /= 6.0f;
        float s = (max - min);
        if (max != 0.0f)
            s /= max;
        float v = max;

        hsv[0] = h;
        hsv[1] = s;
        hsv[2] = v;
    }

    int ss_color::eval_code(const char* text)
    {
        int sz = strlen(text);
        if (sz == 4)
        {
            int nRet = 0;
            nRet |= (code(text[1]) * 17) << 16;
            nRet |= (code(text[2]) * 17) << 8;
            nRet |= (code(text[3]) * 17);
            return nRet;
        }
        else if (sz == 7)
        {
            int nRet = 0;
            nRet |= code(text[1]) << 20;
            nRet |= code(text[2]) << 16;
            nRet |= code(text[3]) << 12;
            nRet |= code(text[4]) << 8;
            nRet |= code(text[5]) << 4;
            nRet |= code(text[6]);
            return nRet;
        }
        else
        {
            return 0xFFFFFF;
        }
    }

    int ss_color::eval_name(const char* szText)
    {
        int i = 0;
        while (COLORS[i].name)
        {
            if (STRCOMP(COLORS[i].name, szText) == 0)
            {
                return COLORS[i].code;
            }
            i++;
        }
        return -1;
    }

    int ss_color::eval_random(unsigned int r)
    {
        return COLORS[r % 240].code;
    }

    void ss_color::int_to_float(float f[3], unsigned int c)
    {
        float r = (0xFF & (c >> 16)) / 255.0f;
        float g = (0xFF & (c >> 8)) / 255.0f;
        float b = (0xFF & (c)) / 255.0f;
        f[0] = r;
        f[1] = g;
        f[2] = b;
    }

    void ss_color::hsv_to_rgb(float rgb[3], const float hsv[3])
    {
        HSVtoRGB(rgb, hsv);
    }

    void ss_color::rgb_to_hsv(float hsv[3], const float rgb[3])
    {
        RGBtoHSV(hsv, rgb);
    }
}

