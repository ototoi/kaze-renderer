#ifndef SS_RANDOM_H
#define SS_RANDOM_H

namespace ss
{
    class ss_random
    {
    public:
        ss_random();
        ss_random(unsigned int seed);
    public:
        void set_seed(unsigned int seed);
        unsigned int get() const;
        float get_float() const;
    private:
        mutable unsigned int seed_;
    };
}


#endif