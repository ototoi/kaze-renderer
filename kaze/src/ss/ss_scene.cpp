#include "ss_scene.h"
#include "ss_geometry.h"

namespace ss
{
    ss_scene::ss_scene()
    {
    }

    ss_scene::~ss_scene()
    {
        size_t sz = geos_.size();
        for (size_t i = 0; i < sz; i++)
        {
            delete geos_[i];
        }
    }

    void ss_scene::add_geometry(ss_geometry* geo)
    {
        geos_.push_back(geo);
    }

    size_t ss_scene::get_geometry_size() const
    {
        return geos_.size();
    }

    const ss_geometry* ss_scene::get_geometry_at(int i) const
    {
        return geos_[i];
    }
}
