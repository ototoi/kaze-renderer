#include "ss_geometry.h"
#include <string.h>

namespace ss
{
    int ss_geometry::get_type() const
    {
        return GEOMETRY_UNKNOWN;
    }

    ss_triangle_geometry::ss_triangle_geometry(const float pos[9])
    {
        memcpy(pos_, pos, sizeof(float) * 9);
    }

    const float* ss_triangle_geometry::get_positions() const
    {
        return pos_;
    }
}

