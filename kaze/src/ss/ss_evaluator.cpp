#include "ss_evaluator.h"
#include "ss_rule.h"
#include "ss_attributes.h"
#include "ss_options.h"
#include "ss_random.h"
#include "ss_colorpool.h"
#include <stdlib.h>
#include <vector>
#include <map>
#include <string>

namespace ss
{
    typedef ss_context::RuleMapType RuleMapType;

    namespace
    {
        class SymbolManager
        {
        public:
            typedef std::pair<float, const ss_rule*> PairType;
            typedef std::vector<PairType> ListType;
            typedef std::map<std::string, ListType> MapType;

            SymbolManager(const RuleMapType& m)
            {
                for (RuleMapType::const_iterator it = m.begin(); it != m.end(); it++)
                {
                    std::string s = it->first;
                    ListType v;
                    typedef std::vector<const ss_rule*> RuleList;
                    for (RuleList::const_iterator j = it->second.begin(); j != it->second.end(); j++)
                    {
                        const ss_rule* r = *j;
                        v.push_back(std::make_pair(r->get_weight(), r));
                    }
                    regularize(v);
                    map_[s] = v;
                }
            }
            const ss_rule* Get(const std::string& name, float w)
            {
                MapType::const_iterator it = map_.find(name);
                if (it != map_.end())
                {
                    return Get(it->second, w);
                }
                else
                {
                    return NULL;
                }
            }

        protected:
            const ss_rule* Get(const ListType& l, float w)
            {
                for (ListType::const_iterator it = l.begin(); it != l.end(); it++)
                {
                    if (w <= it->first) return it->second;
                }
                return l.back().second;
            }

        private:
            struct Sorter
            {
                bool operator()(const PairType& a, const PairType& b) const
                {
                    return a.first < b.first;
                }
            };
            void regularize(ListType& l)
            {
                float sum = 0.0f;
                for (ListType::iterator it = l.begin(); it != l.end(); it++)
                {
                    sum += it->first;
                }
                if (sum != 0.0f)
                {
                    sum = 1.0f / sum;
                    for (ListType::iterator it = l.begin(); it != l.end(); it++)
                    {
                        it->first *= sum;
                    }
                }
                else
                {
                    sum = 1.0f / l.size();
                    for (ListType::iterator it = l.begin(); it != l.end(); it++)
                    {
                        it->first = sum;
                    }
                }
                std::sort(l.begin(), l.end(), Sorter());
            }

        private:
            MapType map_;
        };

        class AttrManager
        {
        public:
            AttrManager()
            {
                attrs_.push_back(new ss_attributes());
            }
            void Push()
            {
                attrs_.push_back(new ss_attributes(*attrs_.back()));
            }
            void Pop()
            {
                ss_attributes* attr = attrs_.back();
                attrs_.pop_back();
                delete attr;
            }
            ss_attributes* Top()
            {
                return attrs_.back();
            }

        private:
            std::vector<ss_attributes*> attrs_;
        };

#define SCN() (scn_)
#define OPT() (opt_)
#define RND() (rnd_)
#define COL() (col_)

        static bool cmp(const char* szA, const char* szB)
        {
            return strcmp(szA, szB) == 0;
        }

        class Evaluator
        {
        public:
            Evaluator(ss_scene* scn, const ss_context* ctx)
                : scn_(scn), ctx_(ctx)
            {
                sm_ = new SymbolManager(ctx->get_rule_map());
                am_ = new AttrManager();
                opt_ = new ss_options();
                rnd_ = new ss_random();
                col_ = new ss_colorpool();
            }
            ~Evaluator()
            {
                delete sm_;
                delete am_;
                delete opt_;
                delete rnd_;
                delete col_;
            }
            int Run()
            {
                return Eval(ctx_->get_global_rule(), 1);
            }

        public:
            int Eval(const ss_rule* r, int depth)
            {
                if (depth <= r->get_maxdepth())
                {
                    const std::vector<const ss_action*>& actions = r->get_actions();
                    size_t sz = actions.size();
                    for (size_t i = 0; i < sz; i++)
                    {
                        Eval(actions[i], depth);
                    }
                }
                else if (!(r->get_extra_rule_name().empty()))
                {
                    float w = Rand();
                    const ss_rule* r2 = sm_->Get(r->get_extra_rule_name(), w);
                    if (r2)
                    {
                        return Eval(r2, depth);
                    }
                }
                return 0;
            }
            int Eval(const ss_action* act, int depth)
            {
                int nType = act->get_type();
                switch (nType)
                {
                case ACTION_BLOCKED:
                    return Eval((const ss_blocked_action*)act, depth);
                case ACTION_CALL_RULE:
                    return Eval((const ss_call_rule_action*)act, depth);

                case ACTION_X:
                    return Eval((const ss_x_action*)act, depth);
                case ACTION_Y:
                    return Eval((const ss_y_action*)act, depth);
                case ACTION_Z:
                    return Eval((const ss_z_action*)act, depth);
                case ACTION_RX:
                    return Eval((const ss_rx_action*)act, depth);
                case ACTION_RY:
                    return Eval((const ss_ry_action*)act, depth);
                case ACTION_RZ:
                    return Eval((const ss_rz_action*)act, depth);
                case ACTION_S:
                    return Eval((const ss_s_action*)act, depth);
                case ACTION_M:
                    return Eval((const ss_m_action*)act, depth);
                case ACTION_FX:
                    return Eval((const ss_fx_action*)act, depth);
                case ACTION_FY:
                    return Eval((const ss_fy_action*)act, depth);
                case ACTION_FZ:
                    return Eval((const ss_fz_action*)act, depth);

                case ACTION_HUE:
                    return Eval((const ss_hue_action*)act, depth);
                case ACTION_SAT:
                    return Eval((const ss_sat_action*)act, depth);
                case ACTION_BRIGHTNESS:
                    return Eval((const ss_brightness_action*)act, depth);
                case ACTION_ALPHA:
                    return Eval((const ss_alpha_action*)act, depth);
                case ACTION_COLOR:
                    return Eval((const ss_color_action*)act, depth);
                case ACTION_COLOR_RANDOM:
                    return Eval((const ss_color_random_action*)act, depth);
                case ACTION_BLEND:
                    return Eval((const ss_blend_action*)act, depth);

                case ACTION_BOX:
                    return Eval((const ss_box_action*)act, depth);
                case ACTION_GRID:
                    return Eval((const ss_grid_action*)act, depth);
                case ACTION_SPHERE:
                    return Eval((const ss_sphere_action*)act, depth);
                case ACTION_LINE:
                    return Eval((const ss_line_action*)act, depth);
                case ACTION_POINT:
                    return Eval((const ss_point_action*)act, depth);
                case ACTION_TRIANGLE:
                    return Eval((const ss_triangle_action*)act, depth);
                case ACTION_CYLINDER:
                    return Eval((const ss_cylinder_action*)act, depth);
                case ACTION_TUBE:
                    return Eval((const ss_tube_action*)act, depth);

                case ACTION_SET_OPTION:
                    return Eval((const ss_set_option_action*)act, depth);
                }
                return 0;
            }

        protected:
            int Eval(const ss_blocked_action* act, int depth)
            {
                const std::vector<const ss_block*>& blocks = act->get_blocks();
                std::vector<const ss_block*> bl(blocks.begin(), blocks.end());
                return Eval(bl, act->get_action(), depth);
            }
            int Eval(const std::vector<const ss_block*>& bl, const ss_action* act, int depth)
            {
                if (bl.empty())
                {
                    return Eval(act, depth);
                }
                else
                {
                    const ss_block* b = bl.front();
                    std::vector<const ss_block*> bl2(bl.begin() + 1, bl.end());
                    int nTimes = b->get_times();
                    am_->Push();
                    for (int i = 0; i < nTimes; i++)
                    {
                        const std::vector<const ss_action*>& actions = b->get_actions();
                        size_t asz = actions.size();
                        for (size_t j = 0; j < asz; j++)
                        {
                            Eval(actions[j], depth);
                        }
                        am_->Push();
                        Eval(bl2, act, depth);
                        am_->Pop();
                    }
                    am_->Pop();
                    return 0;
                }
            }
            int Eval(const ss_call_rule_action* act, int depth)
            {
                float w = Rand();
                const ss_rule* r = sm_->Get(act->get_name(), w);
                if (r)
                {
                    am_->Push();
                    Eval(r, depth + 1);
                    am_->Pop();
                }
                return 0;
            }
            int Eval(const ss_x_action* act, int depth)
            {
                am_->Top()->action_x(act->get_value());
                return 0;
            }
            int Eval(const ss_y_action* act, int depth)
            {
                am_->Top()->action_y(act->get_value());
                return 0;
            }
            int Eval(const ss_z_action* act, int depth)
            {
                am_->Top()->action_z(act->get_value());
                return 0;
            }
            int Eval(const ss_rx_action* act, int depth)
            {
                am_->Top()->action_rx(act->get_value());
                return 0;
            }
            int Eval(const ss_ry_action* act, int depth)
            {
                am_->Top()->action_ry(act->get_value());
                return 0;
            }
            int Eval(const ss_rz_action* act, int depth)
            {
                am_->Top()->action_rz(act->get_value());
                return 0;
            }
            int Eval(const ss_s_action* act, int depth)
            {
                const float* f = act->get_value();
                am_->Top()->action_s(f[0], f[1], f[2]);
                return 0;
            }
            int Eval(const ss_m_action* act, int depth)
            {
                const float* f = act->get_value();
                am_->Top()->action_m(f);
                return 0;
            }
            int Eval(const ss_fx_action* act, int depth)
            {
                am_->Top()->action_fx(act->get_value());
                return 0;
            }
            int Eval(const ss_fy_action* act, int depth)
            {
                am_->Top()->action_fy(act->get_value());
                return 0;
            }
            int Eval(const ss_fz_action* act, int depth)
            {
                am_->Top()->action_fz(act->get_value());
                return 0;
            }

        protected:
            int Eval(const ss_hue_action* act, int depth)
            {
                am_->Top()->action_hue(act->get_value());
                return 0;
            }
            int Eval(const ss_sat_action* act, int depth)
            {
                am_->Top()->action_sat(act->get_value());
                return 0;
            }
            int Eval(const ss_brightness_action* act, int depth)
            {
                am_->Top()->action_brightness(act->get_value());
                return 0;
            }
            int Eval(const ss_alpha_action* act, int depth)
            {
                am_->Top()->action_alpha(act->get_value());
                return 0;
            }
            int Eval(const ss_color_action* act, int depth)
            {
                am_->Top()->action_color(act->get_color());
                return 0;
            }
            int Eval(const ss_color_random_action* act, int depth)
            {
                float col[3] = {};
                COL()
                    ->get_color(col, RND());
                am_->Top()->action_color(col);
                return 0;
            }
            int Eval(const ss_blend_action* act, int depth)
            {
                am_->Top()->action_blend(act->get_color(), act->get_value());
                return 0;
            }

        protected:
            bool IsMaxObjects()
            {
                int gsz = SCN()->get_geometry_size();
                int msz = OPT()->get_maxobjects();
                return msz <= gsz;
            }

            bool IsMinMaxSize()
            {
                float min = OPT()->get_minsize();
                float max = OPT()->get_maxsize();
                return (am_->Top())->is_minmax(min, max);
            }

            int Eval(const ss_box_action* act, int depth)
            {
                if (IsMaxObjects()) return 0;
                ss_geometry* geo = new ss_box_geometry();
                geo->set_attributes(*(am_->Top()));
                scn_->add_geometry(geo);
                return 0;
            }
            int Eval(const ss_grid_action* act, int depth)
            {
                if (IsMaxObjects()) return 0;
                ss_geometry* geo = new ss_grid_geometry();
                geo->set_attributes(*(am_->Top()));
                scn_->add_geometry(geo);
                return 0;
            }
            int Eval(const ss_sphere_action* act, int depth)
            {
                if (IsMaxObjects()) return 0;
                ss_geometry* geo = new ss_sphere_geometry();
                geo->set_attributes(*(am_->Top()));
                scn_->add_geometry(geo);
                return 0;
            }
            int Eval(const ss_line_action* act, int depth)
            {
                if (IsMaxObjects()) return 0;
                ss_geometry* geo = new ss_line_geometry();
                geo->set_attributes(*(am_->Top()));
                scn_->add_geometry(geo);
                return 0;
            }
            int Eval(const ss_point_action* act, int depth)
            {
                if (IsMaxObjects()) return 0;
                ss_geometry* geo = new ss_point_geometry();
                geo->set_attributes(*(am_->Top()));
                scn_->add_geometry(geo);
                return 0;
            }
            int Eval(const ss_triangle_action* act, int depth)
            {
                if (IsMaxObjects()) return 0;
                ss_geometry* geo = new ss_triangle_geometry(act->get_positions());
                geo->set_attributes(*(am_->Top()));
                scn_->add_geometry(geo);
                return 0;
            }
            int Eval(const ss_cylinder_action* act, int depth)
            {
                if (IsMaxObjects()) return 0;
                ss_geometry* geo = new ss_cylinder_geometry();
                geo->set_attributes(*(am_->Top()));
                scn_->add_geometry(geo);
                return 0;
            }
            int Eval(const ss_tube_action* act, int depth)
            {
                if (IsMaxObjects()) return 0;
                ss_geometry* geo = new ss_tube_geometry();
                geo->set_attributes(*(am_->Top()));
                scn_->add_geometry(geo);
                return 0;
            }

        public:
            int Eval(const ss_set_option_action* act, int depth)
            {
                std::string key = act->get_key();
                if (cmp(key.c_str(), "maxdepth"))
                {
                    OPT()
                        ->set_maxdepth(act->get_value_as_integer());
                }
                else if (cmp(key.c_str(), "maxobjects"))
                {
                    OPT()
                        ->set_maxobjects(act->get_value_as_integer());
                }
                else if (cmp(key.c_str(), "minsize"))
                {
                    OPT()
                        ->set_minsize(act->get_value_as_float());
                }
                else if (cmp(key.c_str(), "maxsize"))
                {
                    OPT()
                        ->set_maxsize(act->get_value_as_float());
                }
                else if (cmp(key.c_str(), "seed"))
                {
                    std::string val = act->get_value_as_string();
                    if (val == "initial")
                    {
                        int seed = OPT()->get_seed();
                        RND()
                            ->set_seed(seed);
                    }
                    else
                    {
                        int seed = act->get_value_as_integer();
                        OPT()
                            ->set_seed(seed);
                        RND()
                            ->set_seed(seed);
                    }
                }
                else if (cmp(key.c_str(), "colorpool"))
                {
                    COL()
                        ->set_scheme(act->get_value_as_string());
                }

                return 0;
            }

        public:
            float Rand()
            {
                return RND()->get_float();
            }

        protected:
            ss_scene* scn_;
            const ss_context* ctx_;
            SymbolManager* sm_;
            AttrManager* am_;
            ss_options* opt_;
            ss_random* rnd_;
            ss_colorpool* col_;
        };
    }

    int ss_evaluator::run(ss_scene* scn, const ss_context* ctx)
    {
        Evaluator ev(scn, ctx);
        return ev.Run();
    }
}

