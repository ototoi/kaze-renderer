#include "ss_colorpool.h"
#include "ss_color.h"

#include <string.h>
#include <vector>
#include <string>

namespace ss
{
    class ss_colorpool_strategy
    {
    public:
        virtual ~ss_colorpool_strategy() {}
        virtual void get(float col[3], const ss_random* rnd) const = 0;
    };

    class ss_randomhue_strategy : public ss_colorpool_strategy
    {
    public:
        void get(float col[3], const ss_random* rnd) const
        {
            float hsv[3] =
                {
                    rnd->get_float(),
                    1,
                    1};
            ss_color::hsv_to_rgb(col, hsv);
        }
    };

    class ss_randomcolor_strategy : public ss_colorpool_strategy
    {
    public:
        void get(float col[3], const ss_random* rnd) const
        {
            int nCode = ss_color::eval_random(rnd->get());
            ss_color::int_to_float(col, nCode);
        }
    };

    class ss_randomrgb_strategy : public ss_colorpool_strategy
    {
    public:
        void get(float col[3], const ss_random* rnd) const
        {
            col[0] = rnd->get_float();
            col[1] = rnd->get_float();
            col[2] = rnd->get_float();
        }
    };

    class ss_grayscale_strategy : public ss_colorpool_strategy
    {
    public:
        void get(float col[3], const ss_random* rnd) const
        {
            float f = rnd->get_float();
            col[0] = f;
            col[1] = f;
            col[2] = f;
        }
    };

    class ss_listcolor_strategy : public ss_colorpool_strategy
    {
    public:
        ss_listcolor_strategy(const std::vector<float>& colors)
            : colors_(colors)
        {
        }

        ss_listcolor_strategy(const float* colors, int n)
            : colors_(n)
        {
            memcpy(&colors_[0], colors, n);
        }

    public:
        void get(float col[3], const ss_random* rnd) const
        {
            int n = (int)(colors_.size() / 3);
            int i = rnd->get() % n;
            memcpy(col, &colors_[3 * i + 0], sizeof(float) * 3);
        }

    private:
        std::vector<float> colors_;
    };

    static const char* skip_ws(const char* s)
    {
        while (*s)
        {
            if (!isspace(*s))
            {
                return s;
            }
            s++;
        }
        return NULL;
    }

    static std::string TrimRight(const std::string& s)
    {
        std::vector<char> v(s.begin(), s.end());
        v.push_back(0);
        int ci = (int)v.size() - 1;
        while (0 <= ci)
        {
            if (!isspace(v[ci]))
            {
                v[ci + 1] = 0;
                return std::string(&v[0]);
            }
            ci--;
        }
        return "";
    }

    static bool GetColorNames(std::vector<std::string>& names, const std::string& scheme)
    {
        const char* s = scheme.c_str();
        s = strstr(s, "list");
        if (!s) return false;
        if (strlen(s) < 4) return false;
        s += 4;
        s = strstr(s, ":");
        if (!s) return false;
        if (strlen(s) < 1) return false;
        s += 1;

        while (s && strlen(s) >= 1)
        {
            s = skip_ws(s);
            if (!s) return false;
            if (strlen(s) < 1) return false;
            std::string token;
            const char* s2 = strstr(s, ",");
            if (s2 != NULL)
            {
                token = TrimRight(std::string(s, s2));
                s = s2 + 1;
            }
            else
            {
                token = TrimRight(std::string(s));
                s = NULL;
            }
            names.push_back(token);
        }

        return !names.empty();
    }

    static ss_colorpool_strategy* GetListStrategy(const std::string& scheme)
    {
        std::vector<std::string> names;
        if (!GetColorNames(names, scheme)) return NULL;

        std::vector<float> colors;
        for (size_t i = 0; i < names.size(); i++)
        {
            int nCode = ss_color::eval_name(names[i].c_str());
            if (nCode >= 0)
            {
                float f[3];
                ss_color::int_to_float(f, nCode);
                colors.push_back(f[0]);
                colors.push_back(f[1]);
                colors.push_back(f[2]);
            }
        }
        return new ss_listcolor_strategy(colors);
    }

    static ss_colorpool_strategy* GetImageStrategy(const std::string& scheme)
    {
        return NULL;
    }

    static ss_colorpool_strategy* GetStrategy(const std::string& scheme)
    {
        if (strstr(scheme.c_str(), "randomcolor") != NULL)
        {
            return new ss_randomcolor_strategy();
        }
        else if (strstr(scheme.c_str(), "randomhue") != NULL)
        {
            return new ss_randomhue_strategy();
        }
        else if (strstr(scheme.c_str(), "randomrgb") != NULL)
        {
            return new ss_randomrgb_strategy();
        }
        else if (strstr(scheme.c_str(), "greyscale") != NULL || strstr(scheme.c_str(), "grayscale") != NULL)
        {
            return new ss_grayscale_strategy();
        }
        else if (strstr(scheme.c_str(), "list") != NULL)
        {
            return GetListStrategy(scheme);
        }
        else if (strstr(scheme.c_str(), "image") != NULL)
        {
            return GetImageStrategy(scheme);
        }
        return new ss_randomcolor_strategy();
    }

    ss_colorpool::ss_colorpool()
    {
        strategy_ = GetStrategy("randomcolor");
    }

    ss_colorpool::~ss_colorpool()
    {
        if (strategy_)
        {
            delete strategy_;
        }
    }

    void ss_colorpool::set_scheme(const std::string& scheme)
    {
        if (strategy_)
        {
            delete strategy_;
        }
        strategy_ = GetStrategy(scheme);
    }

    void ss_colorpool::get_color(float col[3], const ss_random* rnd) const
    {
        if (strategy_)
        {
            strategy_->get(col, rnd);
        }
    }
}
