#ifndef SS_ACTION_H
#define SS_ACTION_H

#include <string>
#include <vector>
#include <map>
#include "ss_object.h"

namespace ss
{
    enum
    {
        ACTION_UNKOWN = 0,
        ACTION_X = 1,
        ACTION_Y,
        ACTION_Z,
        ACTION_RX,
        ACTION_RY,
        ACTION_RZ,
        ACTION_S,
        ACTION_M,
        ACTION_FX,
        ACTION_FY,
        ACTION_FZ,
        ACTION_HUE,
        ACTION_SAT,
        ACTION_BRIGHTNESS,
        ACTION_ALPHA,
        ACTION_COLOR,
        ACTION_COLOR_RANDOM,
        ACTION_BLEND,
        ACTION_CALL_RULE,
        ACTION_BOX,
        ACTION_GRID,
        ACTION_SPHERE,
        ACTION_LINE,
        ACTION_POINT,
        ACTION_TRIANGLE,
        ACTION_CYLINDER,
        ACTION_TUBE,
        ACTION_BLOCKED,
        ACTION_MODIFY_MAXDEPTH,
        ACTION_MODIFY_WEIGHT,
        ACTION_SET_OPTION,
    };

    //------------------------------------------------

    class ss_action : public ss_object
    {
    public:
        virtual ~ss_action() {}
        virtual int get_type() const = 0;
    };

    class ss_transformation_action : public ss_action
    {
    };

    class ss_call_action : public ss_action
    {
    };

    class ss_geometry_action : public ss_call_action
    {
    };

    //------------------------------------------------

    class ss_x_action : public ss_transformation_action
    {
    public:
        ss_x_action(float v);
        virtual int get_type() const { return ACTION_X; }
        float get_value() const { return v_; }
    protected:
        float v_;
    };

    class ss_y_action : public ss_transformation_action
    {
    public:
        ss_y_action(float v);
        virtual int get_type() const { return ACTION_Y; }
        float get_value() const { return v_; }
    protected:
        float v_;
    };

    class ss_z_action : public ss_transformation_action
    {
    public:
        ss_z_action(float v);
        virtual int get_type() const { return ACTION_Z; }
        float get_value() const { return v_; }
    protected:
        float v_;
    };

    //------------------------------------------------

    class ss_rx_action : public ss_transformation_action
    {
    public:
        ss_rx_action(float v);
        virtual int get_type() const { return ACTION_RX; }
        float get_value() const { return v_; }
    protected:
        float v_;
    };

    class ss_ry_action : public ss_transformation_action
    {
    public:
        ss_ry_action(float v);
        virtual int get_type() const { return ACTION_RY; }
        float get_value() const { return v_; }
    protected:
        float v_;
    };

    class ss_rz_action : public ss_transformation_action
    {
    public:
        ss_rz_action(float v);
        virtual int get_type() const { return ACTION_RZ; }
        float get_value() const { return v_; }
    protected:
        float v_;
    };

    //------------------------------------------------

    class ss_s_action : public ss_transformation_action
    {
    public:
        ss_s_action(float v);
        ss_s_action(float x, float y, float z);
        virtual int get_type() const { return ACTION_S; }
        const float* get_value() const { return v_; }
    protected:
        float v_[3];
    };

    //------------------------------------------------

    class ss_m_action : public ss_transformation_action
    {
    public:
        ss_m_action(const float m[9]);
        virtual int get_type() const { return ACTION_M; }
        const float* get_value() const { return m_; }
    protected:
        float m_[9];
    };

    //------------------------------------------------

    class ss_fx_action : public ss_transformation_action
    {
    public:
        ss_fx_action(float v);
        virtual int get_type() const { return ACTION_FX; }
        float get_value() const { return v_; }
    protected:
        float v_;
    };

    class ss_fy_action : public ss_transformation_action
    {
    public:
        ss_fy_action(float v);
        virtual int get_type() const { return ACTION_FY; }
        float get_value() const { return v_; }
    protected:
        float v_;
    };

    class ss_fz_action : public ss_transformation_action
    {
    public:
        ss_fz_action(float v);
        virtual int get_type() const { return ACTION_FZ; }
        float get_value() const { return v_; }
    protected:
        float v_;
    };

    //------------------------------------------------

    class ss_hue_action : public ss_transformation_action
    {
    public:
        ss_hue_action(float v);
        virtual int get_type() const { return ACTION_HUE; }
        float get_value() const { return v_; }
    protected:
        float v_;
    };

    class ss_sat_action : public ss_transformation_action
    {
    public:
        ss_sat_action(float v);
        virtual int get_type() const { return ACTION_SAT; }
        float get_value() const { return v_; }
    protected:
        float v_;
    };

    class ss_brightness_action : public ss_transformation_action
    {
    public:
        ss_brightness_action(float v);
        virtual int get_type() const { return ACTION_BRIGHTNESS; }
        float get_value() const { return v_; }
    protected:
        float v_;
    };

    class ss_color_action : public ss_transformation_action
    {
    public:
        ss_color_action(float r, float g, float b);
        virtual int get_type() const { return ACTION_COLOR; }
        const float* get_color() const { return col_; }
    protected:
        float col_[3];
    };

    class ss_alpha_action : public ss_transformation_action
    {
    public:
        ss_alpha_action(float v);
        virtual int get_type() const { return ACTION_ALPHA; }
        float get_value() const { return v_; }
    protected:
        float v_;
    };

    class ss_blend_action : public ss_transformation_action
    {
    public:
        ss_blend_action(float r, float g, float b, float strength);
        virtual int get_type() const { return ACTION_BLEND; }
        const float* get_color() const { return col_; }
        float get_value() const { return s_; }
    protected:
        float col_[3];
        float s_;
    };

    class ss_color_random_action : public ss_transformation_action
    {
    public:
        ss_color_random_action();
        virtual int get_type() const { return ACTION_COLOR_RANDOM; }
    };

    //------------------------------------------------

    class ss_call_rule_action : public ss_call_action
    {
    public:
        ss_call_rule_action(const std::string& name);
        virtual int get_type() const { return ACTION_CALL_RULE; }
        const std::string& get_name() const { return name_; }
    protected:
        std::string name_;
    };

    //------------------------------------------------

    class ss_box_action : public ss_geometry_action
    {
    public:
        ss_box_action() {}
        virtual int get_type() const { return ACTION_BOX; }
    };

    class ss_grid_action : public ss_geometry_action
    {
    public:
        ss_grid_action() {}
        virtual int get_type() const { return ACTION_GRID; }
    };

    class ss_sphere_action : public ss_geometry_action
    {
    public:
        ss_sphere_action() {}
        virtual int get_type() const { return ACTION_SPHERE; }
    };

    class ss_line_action : public ss_geometry_action
    {
    public:
        ss_line_action() {}
        virtual int get_type() const { return ACTION_LINE; }
    };

    class ss_point_action : public ss_geometry_action
    {
    public:
        ss_point_action() {}
        virtual int get_type() const { return ACTION_POINT; }
    };

    class ss_triangle_action : public ss_geometry_action
    {
    public:
        ss_triangle_action() {}
        virtual int get_type() const { return ACTION_TRIANGLE; }
    public:
        void set_positions(const float pos[9]) { memcpy(pos_, pos, sizeof(float) * 9); }
        const float* get_positions() const { return pos_; }
    private:
        float pos_[9];
    };

    class ss_cylinder_action : public ss_geometry_action
    {
    public:
        ss_cylinder_action() {}
        virtual int get_type() const { return ACTION_CYLINDER; }
    };

    class ss_tube_action : public ss_geometry_action
    {
    public:
        ss_tube_action() {}
        virtual int get_type() const { return ACTION_TUBE; }
    };

    class ss_geometry_action_creator
    {
    public:
        static ss_geometry_action* create(const std::string& name);
    };

    //------------------------------------------------

    class ss_block;
    class ss_blocklist;
    class ss_blocked_action : public ss_action
    {
    public:
        ss_blocked_action(const ss_blocklist* bl, const ss_action* act);
        ss_blocked_action(const std::vector<const ss_block*>& blocks, const ss_action* act);
        virtual int get_type() const { return ACTION_BLOCKED; }
    public:
        const ss_action* get_action() const { return act_; }
        const std::vector<const ss_block*>& get_blocks() const { return blocks_; }
    protected:
        std::vector<const ss_block*> blocks_;
        const ss_action* act_;
    };

    //------------------------------------------------

    class ss_modify_maxdepth_action : public ss_action
    {
    public:
        ss_modify_maxdepth_action(int depth);
        ss_modify_maxdepth_action(int depth, const std::string& extra);
        virtual int get_type() const { return ACTION_MODIFY_MAXDEPTH; }
    public:
        int get_maxdepth() const;
        const std::string& get_extra_rule_name() const { return extra_; }
    protected:
        int depth_;
        std::string extra_;
    };

    class ss_modify_weight_action : public ss_action
    {
    public:
        ss_modify_weight_action(float w);
        virtual int get_type() const { return ACTION_MODIFY_WEIGHT; }
    public:
        float get_weight() const;
    protected:
        float v_;
    };

    //------------------------------------------------

    class ss_set_option_action : public ss_action
    {
    public:
        ss_set_option_action(const std::string& key, int v);
        ss_set_option_action(const std::string& key, float v);
        ss_set_option_action(const std::string& key, const float v[], int n);
        ss_set_option_action(const std::string& key, const std::string& v);
        virtual int get_type() const { return ACTION_SET_OPTION; }
    public:
        const std::string& get_key() const;
        int get_value_as_integer() const;
        float get_value_as_float() const;
        const std::string& get_value_as_string() const;

    protected:
        std::string key_;
        std::string value_;
    };
}


#endif
