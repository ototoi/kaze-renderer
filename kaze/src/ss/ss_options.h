#ifndef SS_OPTIONS_H
#define SS_OPTIONS_H

#include <string>
#include <map>
#include "ss_object.h"

namespace ss
{
    class ss_options : public ss_object
    {
    public:
        ss_options();
        ss_options(const ss_options& opt);

    public:
        int get_maxdepth() const { return maxdepth_; }
        void set_maxdepth(int maxdepth) { maxdepth_ = maxdepth; }
        int get_maxobjects() const { return maxobjects_; }
        void set_maxobjects(int maxobjects) { maxobjects_ = maxobjects; }
        float get_minsize() const { return minsize_; }
        void set_minsize(int minsize) { minsize_ = minsize; }
        float get_maxsize() const { return maxsize_; }
        void set_maxsize(int maxsize) { maxsize_ = maxsize; }
        int get_seed() const { return seed_; }
        void set_seed(int seed) { seed_ = seed; }
        const float* get_background() const { return background_; }
        void set_background(const float r[3]);
        void set_background(unsigned int nRGB);
        void set_background(float r, float g, float b);

    protected:
        int maxdepth_;
        int maxobjects_;
        float minsize_;
        float maxsize_;
        int seed_;
        float background_[3];
        std::string random_scheme_;
    };
}


#endif
