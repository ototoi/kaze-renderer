#ifndef SS_RULE_H
#define SS_RULE_H

#include <vector>
#include "ss_object.h"
#include "ss_action.h"
#include "ss_block.h"

namespace ss
{
    class ss_rule : public ss_object
    {
    public:
        ss_rule(const std::string& name);
        ~ss_rule();

    public:
        const std::string& get_name() const { return name_; }
    public:
        int get_maxdepth() const { return maxdepth_; }
        void set_maxdepth(int maxdepth) { maxdepth_ = maxdepth; }
        float get_weight() const { return weight_; }
        void set_weight(float weight) { weight_ = weight; }
        const std::string& get_extra_rule_name() const { return extra_; }
        void set_extra_rule_name(const std::string& extra) { extra_ = extra; }
    public:
        void add_action(const ss_action* act);
        const std::vector<const ss_action*>& get_actions() const;

    protected:
        std::string name_;
        float weight_;
        int maxdepth_;
        std::string extra_;
        std::vector<const ss_action*> actions_;
    };
}


#endif
