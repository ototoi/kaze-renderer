#ifndef SS_CONTEXT_H
#define SS_CONTEXT_H

#include "ss_options.h"
#include "ss_action.h"
#include "ss_rule.h"

#include <set>
#include <map>
#include <vector>

namespace ss
{
    class ss_context
    {
    public:
        ss_context();
        ~ss_context();

    public:
        ss_options* get_global_options();
        ss_rule* get_global_rule();
        const ss_options* get_global_options() const;
        const ss_rule* get_global_rule() const;

    public:
        ss_rule* get_top_rule();
        void push_rule(ss_rule* r);
        void pop_rule();

    public:
        ss_block* get_top_block();
        void push_block(ss_block* r);
        void pop_block();

    public:
        ss_object* add_object(ss_object* obj);
        void erase_object(ss_object* obj);

    public:
        void get_color_from_code(float f[3], unsigned int c) const;
        void get_color_from_name(float f[3], const std::string& name) const;

    public:
        void print(const char* szText);

    public:
        typedef std::map<std::string, std::vector<ss_rule*> > RuleMapType;
        const RuleMapType& get_rule_map() const;

    protected:
        typedef std::vector<ss_rule*> ruleStackType;
        ruleStackType ruleStack_;
        RuleMapType ruleMap_;

    protected:
        typedef std::vector<ss_block*> blockStackType;
        blockStackType blockStack_;

    protected:
        std::set<ss_object*> objSet_;
        ss_options* gOpt_;
        ss_rule* gRule_;
    };
}

#endif
