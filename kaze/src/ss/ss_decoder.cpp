#include "ss_decoder.h"

namespace ss
{
    ss_decoder::ss_decoder(std::istream& is)
        : is_(is)
    {
    }

    int ss_decoder::read(char* buffer, unsigned int size)
    {
        is_.read(buffer, size);
        if (is_)
            return size;
        else
            return is_.gcount();
    }
}

