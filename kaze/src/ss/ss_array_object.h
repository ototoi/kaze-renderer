#ifndef SS_ARRAY_OBJECT_H
#define SS_ARRAY_OBJECT_H

#include "ss_object.h"
#include <vector>

namespace ss
{
    class ss_array_object : public ss_object
    {
    public:
        ss_array_object(int n);
        ss_array_object(const float* f, int n);
        void set_at(int i, float f);
        float get_at(int i) const;
        int get_size() const;
        float* get_ptr();
        const float* get_ptr() const;

    protected:
        std::vector<float> v_;
    };
}

#endif
