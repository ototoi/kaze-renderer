//See 
//https://gcc.gnu.org/onlinedocs/cpp/Traditional-lexical-analysis.html#Traditional-lexical-analysis

#if foo/**/bar

#else 

#endif

#define m This macro's fine and has an unmatched quote
"/* This is not a comment.  */
/* This is a comment.  The following #include directive
is ill-formed.  */
#include <stdio.h

#define a/*1
*2
*3
*/m