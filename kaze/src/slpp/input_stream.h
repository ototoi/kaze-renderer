#ifndef SLPP_INPUT_STREAM_H
#define SLPP_INPUT_STREAM_H

#include <memory>
#include <fstream>
#include <vector>

namespace slpp
{
    
    class input_stream
    {
    public:
        virtual ~input_stream(){}
        virtual int get() = 0;
        virtual void unget() = 0;
        virtual std::string get_path()const = 0;
    };

    class console_input_stream : public input_stream
    {
    public:
        console_input_stream(std::istream& istream);
        virtual int get();
        virtual void unget();
        virtual std::string get_path()const;
    private:
        std::istream& istream_;
    };

    class file_input_stream : public input_stream
    {
    public:
        file_input_stream(const char* szPath);
        ~file_input_stream();
        virtual int get();
        virtual void unget();
        virtual std::string get_path()const;
    private:
        int i_;
        int sz_;
        FILE* fp_;
        std::vector<char> buffer_;
        //std::unique_ptr<std::istream> istream_;
        std::string path_;
    };

}

#endif
