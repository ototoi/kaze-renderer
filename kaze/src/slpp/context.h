#ifndef SLPP_CONTEXT_H
#define SLPP_CONTEXT_H

#include <vector>
#include <set>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <memory>
#include <iostream>
#include <string>

#include "input_stream.h"
#include "output_stream.h"
#include "macro.h"

namespace slpp
{
    struct context_options
    {
        bool is_output_comment;
        std::vector<std::string> include_dirs;
    };

    enum 
    {
        SLPP_CONTEXT_NORMAL = 0,
        SLPP_CONTEXT_COMMENT,
        SLPP_CONTEXT_COMMENT_ONELINE,
        SLPP_CONTEXT_DIRECTIVE
    };

    struct context_state
    {
        context_state()
        {
            mode = SLPP_CONTEXT_NORMAL;
            line = 1;
            line_total = 1;
            colmn = 1;
        }
        int mode;
        int line;
        int line_total;
        int colmn;
        std::string file_name;
    };

    struct include_guard
    {
        bool has_pragma_once;
        std::string inc_guard_key;
    };

    class context
    {
    public:
        typedef std::unordered_map<std::string, std::shared_ptr<macro_def> > macro_def_type;
        typedef std::unordered_set<std::string> hide_set_type;
    public:
        context(
            const context_options& opt,
            const std::shared_ptr<input_stream>& is,
            const std::shared_ptr<output_stream>& os
        );
        ~context();
    public:
        int run();
    protected:
        int get();
        void unget();
    protected:
        int skip_directive_comment(std::vector<char>& buffer);
    protected:
        int eval(char* szLine);
    protected:
        int eval_comment(const char* szLine);
    protected:
        int eval_macro(char* szLine);
        int eval_macro_defined (std::string& strOut, const std::string& str);
        std::string eval_macro_detail  (const std::string& str);
        std::string eval_macro_detail  (const std::string& str, const hide_set_type& hs);
        int eval_macro_variable(std::string& strOut, const macro_def* macro, const hide_set_type& hs);
        int eval_macro_function(std::string& strOut, const macro_def* macro, const std::vector<std::string>& args, const hide_set_type& hs);
    protected:
        int eval_directive(const char* szLine);
        int eval_directive_define(const char* szLine);
        int eval_directive_undef(const char* szLine);
        int eval_directive_include_core(const char* szLine, size_t len);
        int eval_directive_include(const char* szLine);
        int eval_directive_include_next(const char* szLine);
        int eval_directive_if(const char* szLine);
        int eval_directive_else(const char* szLine);
        int eval_directive_elif(const char* szLine);
        int eval_directive_endif(const char* szLine);
        int eval_directive_ifdef(const char* szLine);
        int eval_directive_ifndef(const char* szLine);
        int eval_directive_pragma(const char* szLine);
        int eval_directive_version(const char* szLine);
        int eval_directive_line(const char* szLine);
        int eval_directive_error(const char* szLine);
        int eval_directive_warning(const char* szLine);
        int eval_directive_import(const char* szLine);
        int eval_directive_using(const char* szLine);
    protected:
        int write(const char* szLine);
    protected:
        int print_error  (const char* szMessage);
        int print_warning(const char* szMessage);
    protected:
        bool is_output_comment()const;
    protected:
        bool is_in_normal ()const;
        bool is_in_comment()const;
        bool is_in_comment_oneline()const;
        bool is_in_directive()const;
        bool is_if_true()const;
        std::string get_current_input_path()const;
    protected:
        bool register_macro  (const std::shared_ptr<macro_def>& def);
        bool unregister_macro(const char* name);
        bool find_macro      (const char* name);
    protected:
        bool is_already_included(const std::string& str);
    private:
        std::vector< std::shared_ptr<input_stream> > input_streams_;
        std::vector< std::shared_ptr<output_stream> > output_streams_;
        std::vector< int > condition_;
        context_options options_;
        std::vector< context_state > states_;
        macro_def_type macro_defs_;
        std::map<std::string, include_guard > includes_;
    };
}

#endif
