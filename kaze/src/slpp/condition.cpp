#include "condition.h"
#include "char_func.h"
#include <vector>
#include <memory>

namespace slpp
{
    enum SLPP_COND_TYPE
    {
        SLPP_COND_BINARY_ADD = '+',
        SLPP_COND_BINARY_SUB = '-',
        SLPP_COND_BINARY_MUL = '*',
        SLPP_COND_BINARY_DIV = '/',
        SLPP_COND_BINARY_MOD = '%',

        SLPP_COND_BINARY_AND = '&',
        SLPP_COND_BINARY_OR  = '|',
        SLPP_COND_BINARY_XOR  = '^',

        SLPP_COND_COND = '?',

        SLPP_COND_LESS = '<',
        SLPP_COND_GREATER = '>',

        SLPP_COND_UNARY_NEG = '~',
        SLPP_COND_UNARY_NOT = '!',

        SLPP_COND_NOSINGLE = 256,

        SLPP_COND_BOOLEAN,  //8
        SLPP_COND_INTEGER,  //259
        SLPP_COND_FLOAT,    //
        SLPP_COND_STRING,   //

        SLPP_COND_VALUE,

        SLPP_COND_UNARY_MINUS,

        SLPP_COND_EQUAL,    //==
        SLPP_COND_NEQ,      //!=

        SLPP_COND_LESS_EQ,      //<=
        SLPP_COND_GREATER_EQ,   //>=

        SLPP_COND_LSHIFT,   //<<
        SLPP_COND_RSHIFT,   //>>

        SLPP_COND_LOGAND,   //&&
        SLPP_COND_LOGOR,    //||

        SLPP_COND_IDENTIFIER,//

        SLPP_COND_EOL,
        SLPP_COND_EMPTY,
        SLPP_COND_UNKONOWN,
    };

    struct cond_token
    {
        cond_token(){}
        cond_token(int t, const std::string& v):type(t), value(v){}
        int type;
        std::string value;
    };

    struct cond_node
    {
        cond_node(){}
        cond_node(int t, int v = 0):type(t), value(v){}
        int type;
        int value;
        std::vector< std::shared_ptr<cond_node> > children;
    };

    static
    cond_token get_token(const char* s)
    {
        if(*s == 0)return cond_token(SLPP_COND_EMPTY, "");

        char c = *s;
        char c2 = *(s+1);

        switch(c)
        {
        case '"':
            {
                const char* s0 = s;
                bool next_ignore = false;
                s++;
                while(*s != 0)
                {
                    if(*s == '\\')
                    {
                        next_ignore = true;
                    }
                    else
                    {
                        if(*s == '"')
                        {
                            if(!next_ignore)
                            {
                                s++;
                                break;
                            }
                        }
                        next_ignore = false;
                    }
                    s++;
                }
                return cond_token(SLPP_COND_STRING, std::string(s0, s));
            }
        case '(':
        case ')':
        case '+':
        case '-':
        case '*':
        case '/':
        case '%':
        case '^':   //xor
        case '~':   //
        case '?':
        case ':':
            {
                char buffer[] = {c, 0};
                return cond_token(c, buffer);
            }
        case '&':
        case '|':
            {
                if(c2 == c)
                {
                    char buffer[] = {c, c2, 0};
                    if(c == '&')
                        return cond_token(SLPP_COND_LOGAND, buffer);
                    else
                        return cond_token(SLPP_COND_LOGOR,  buffer);
                }
                else
                {
                    char buffer[] = {c, 0};
                    return cond_token(c, buffer);
                }
            }
        case '=':
        case '!':
            {
                if(c2 == '=')
                {
                    char buffer[] = {c, c2, 0};
                    if(c == '=')
                        return cond_token(SLPP_COND_EQUAL, buffer);
                    else
                        return cond_token(SLPP_COND_NEQ,  buffer);
                }
                else
                {
                    char buffer[] = {c, 0};
                    return cond_token(c, buffer);
                }
            }
        case '>':
        case '<':
            {
                if(c2 == c)     //>>, <<
                {
                    char buffer[] = {c, c2, 0};
                    if(c == '<')
                        return cond_token(SLPP_COND_LSHIFT, buffer);
                    else
                        return cond_token(SLPP_COND_RSHIFT,  buffer);
                }
                else if(c2 == '=')
                {
                    char buffer[] = {c, c2, 0};
                    if(c == '<')
                        return cond_token(SLPP_COND_LESS_EQ, buffer);
                    else
                        return cond_token(SLPP_COND_GREATER_EQ,  buffer);
                }
                else
                {
                    char buffer[] = {c, 0};
                    return cond_token(c, buffer);
                }
            }
        case '0':
            {
                if(c2 == 'x' || c2 == 'X')
                {
                    s += 2;
                    char c3 = *s;
                    if(!ishex_(c3))
                    {
                        char buffer[] = {c, 0};
                        return cond_token(SLPP_COND_INTEGER, buffer);//TODO
                    }
                    else
                    {
                        std::string buffer;
                        buffer += c;
                        buffer += c2;
                        while(ishex_(*s))
                        {
                            buffer += *s;
                            s++;
                        }
                        return cond_token(SLPP_COND_INTEGER, buffer);
                    }
                }
                else
                {
                    s += 1;
                    std::string buffer;
                    buffer += c;
                    while(isoct_(*s))
                    {
                        buffer += *s;
                        s++;
                    }
                    return cond_token(SLPP_COND_INTEGER, buffer);
                }
            }
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
            {
                std::string buffer;
                while(*s != 0)
                {
                    c = *s;
                    if(isdigit_(c))
                    {
                        buffer += c;
                        s++;
                    }
                    else
                    {
                        break;
                    }
                }
                c = *s;
                if(c == 'u' || c == 'U')
                {
                    buffer += c;
                }
                return cond_token(SLPP_COND_INTEGER, buffer);
            }
        default:
            {
                std::string buffer;
                if(isalpha_(c) || c == '_')
                {
                    buffer += c;
                    s++;
                }
                else
                {
                    char buffer[] = {c, 0};
                    return cond_token(c, buffer);
                }

                while(*s != 0)
                {
                    c = *s;
                    if(isalnum_(c) || c == '_')
                    {
                        buffer += c;
                        s++;
                    }
                    else
                    {
                        if(buffer == "true") return cond_token(SLPP_COND_BOOLEAN, "true");
                        if(buffer == "false")return cond_token(SLPP_COND_BOOLEAN, "false");
                        return cond_token(SLPP_COND_IDENTIFIER, buffer);
                    }
                }
                if(buffer == "true") return cond_token(SLPP_COND_BOOLEAN, "true");
                if(buffer == "false")return cond_token(SLPP_COND_BOOLEAN, "false");
                return cond_token(SLPP_COND_IDENTIFIER, buffer);
            }
        }
        return cond_token(SLPP_COND_EMPTY, "");
    }

    static
    inline int str2int(const char* s)
    {
        if(*s == '0')
        {
            if(s[1] == 'x' || s[1] == 'X')return (int)strtol(s+2, NULL, 16);
            else return (int)strtol(s+1, NULL, 8);
        }
        else
        {
            return std::atoi(s);
        }
    }

    class cond_parser
    {
    public:
        cond_parser(const char* s)
        {
            while(isspace_(*s))
            {
                s++;
            }
            while(true)
            {
                cond_token tk = slpp::get_token(s);
                if(tk.type == SLPP_COND_EMPTY)
                {
                    break;
                }
                tokens_.push_back(tk);
                s += tk.value.size();
                while(isspace_(*s))
                {
                    s++;
                }
            }
            i_ = 0;
        }
        std::shared_ptr<cond_node> eval()
        {
            return this->expression();
        }

        std::shared_ptr<cond_node> expression()
        {
            return this->logical();
        }

        std::shared_ptr<cond_node> logical()
        {
            int i0 = i_;
            std::shared_ptr<cond_node> lnode = this->relation_u();
            int i1 = i_;
            if(lnode.get())
            {
                cond_token tkop = get_token();
                switch(tkop.type)
                {
                    case SLPP_COND_LOGAND:
                    case SLPP_COND_LOGOR:
                        {
                            std::shared_ptr<cond_node> rnode = this->relation_u();
                            if(rnode.get())
                            {
                                std::shared_ptr<cond_node> node(new cond_node(tkop.type));
                                node->children.push_back(lnode);
                                node->children.push_back(rnode);
                                return node;
                            }
                        }
                }
                i_ = i1;
                return lnode;
            }
            i_ = i0;
            return std::shared_ptr<cond_node>();
        }

        std::shared_ptr<cond_node> relation_u()
        {
            int i0 = i_;
            cond_token token = get_token();
            switch(token.type)
            {
            case '!':
                {
                    std::shared_ptr<cond_node> base = relation();
                    if(base.get())
                    {
                        std::shared_ptr<cond_node> node(new cond_node(token.type));
                        node->children.push_back(base);
                        return node;
                    }
                }
            }
            i_ = i0;
            return relation();
        }

        std::shared_ptr<cond_node> relation()
        {
            int i0 = i_;
            std::shared_ptr<cond_node> lnode = this->cond();
            int i1 = i_;
            if(lnode.get())
            {
                cond_token tkop = get_token();
                switch(tkop.type)
                {
                    case '<':
                    case '>':
                    case SLPP_COND_LESS_EQ:
                    case SLPP_COND_GREATER_EQ:
                    case SLPP_COND_EQUAL:
                    case SLPP_COND_NEQ:
                        {
                            std::shared_ptr<cond_node> rnode = this->cond();
                            if(rnode.get())
                            {
                                std::shared_ptr<cond_node> node(new cond_node(tkop.type));
                                node->children.push_back(lnode);
                                node->children.push_back(rnode);
                                return node;
                            }
                        }
                }
                i_ = i1;
                return lnode;
            }
            i_ = i0;
            return std::shared_ptr<cond_node>();
        }

        std::shared_ptr<cond_node> cond()
        {
            int i0 = i_;
            std::shared_ptr<cond_node> n0 = this->log_arith_u();
            int i1 = i_;
            if(n0.get())
            {
                cond_token tk0 = get_token();
                if(tk0.type == '?')
                {
                    std::shared_ptr<cond_node> n1 = this->log_arith_u();
                    if(n1.get())
                    {
                        cond_token tk1 = get_token();
                        if(tk1.type == ':')
                        {
                            std::shared_ptr<cond_node> n2 = this->cond();
                            if(n2.get())
                            {
                                std::shared_ptr<cond_node> node(new cond_node(SLPP_COND_COND));
                                node->children.push_back(n0);
                                node->children.push_back(n1);
                                node->children.push_back(n2);
                                return node;
                            }
                        }
                    }
                }
                i_ = i1;
                return n0;
            }
            i_ = i0;
            return std::shared_ptr<cond_node>();
        }

        std::shared_ptr<cond_node> log_arith_u()
        {
            int i0 = i_;
            cond_token token = get_token();
            switch(token.type)
            {
            case '~':
                {
                    std::shared_ptr<cond_node> base = log_arith();
                    if(base.get())
                    {
                        std::shared_ptr<cond_node> node(new cond_node(SLPP_COND_UNARY_NEG));
                        node->children.push_back(base);
                        return node;
                    }
                }
            }
            i_ = i0;
            return log_arith();
        }

        std::shared_ptr<cond_node> log_arith()
        {
            int i0 = i_;
            std::shared_ptr<cond_node> lnode = this->arithmetic();
            int i1 = i_;
            if(lnode.get())
            {
                cond_token tkop = get_token();
                switch(tkop.type)
                {
                    case '&':
                    case '|':
                    case '^':
                    case SLPP_COND_LSHIFT:
                    case SLPP_COND_RSHIFT:
                        {
                            std::shared_ptr<cond_node> rnode = this->arithmetic();
                            if(rnode.get())
                            {
                                std::shared_ptr<cond_node> node(new cond_node(tkop.type));
                                node->children.push_back(lnode);
                                node->children.push_back(rnode);
                                return node;
                            }
                        }
                }
                i_ = i1;
                return lnode;
            }
            i_ = i0;
            return std::shared_ptr<cond_node>();
        }

        std::shared_ptr<cond_node> arithmetic()
        {
            int i0 = i_;
            std::shared_ptr<cond_node> lnode = this->term();
            int i1 = i_;
            if(lnode.get())
            {
                cond_token tkop = get_token();
                switch(tkop.type)
                {
                    case '+':
                    case '-':
                        {
                            std::shared_ptr<cond_node> rnode = this->term();
                            if(rnode.get())
                            {
                                std::shared_ptr<cond_node> node(new cond_node(tkop.type));
                                node->children.push_back(lnode);
                                node->children.push_back(rnode);
                                return node;
                            }
                        }
                }
                i_ = i1;
                return lnode;
            }
            i_ = i0;
            return std::shared_ptr<cond_node>();
        }

        std::shared_ptr<cond_node> term()
        {
            int i0 = i_;
            std::shared_ptr<cond_node> lnode = this->primary_u();
            int i1 = i_;
            if(lnode.get())
            {
                cond_token tkop = get_token();
                switch(tkop.type)
                {
                    case '*':
                    case '/':
                    case '%':
                        {
                            std::shared_ptr<cond_node> rnode = this->primary_u();
                            if(rnode.get())
                            {
                                std::shared_ptr<cond_node> node(new cond_node(tkop.type));
                                node->children.push_back(lnode);
                                node->children.push_back(rnode);
                                return node;
                            }
                        }
                }
                i_ = i1;
                return lnode;
            }
            i_ = i0;
            return std::shared_ptr<cond_node>();
        }

        std::shared_ptr<cond_node> primary_u()
        {
            int i0 = i_;
            cond_token token = get_token();
            switch(token.type)
            {
            case '+':
            case '-':
                {
                    std::shared_ptr<cond_node> base = primary();
                    if(base.get())
                    {
                        if(token.type == '-')
                        {
                            std::shared_ptr<cond_node> node(new cond_node(SLPP_COND_UNARY_MINUS));
                            node->children.push_back(base);
                            return node;
                        }
                        return base;
                    }
                }
            }
            i_ = i0;
            return primary();
        }

        std::shared_ptr<cond_node> primary()
        {
            std::shared_ptr<cond_node> node = this->literal();
            if(node.get())
            {
                return node;
            }
            else
            {
                cond_token token = get_token();
                if(token.type == '(')
                {
                    node = this->expression();
                    token = get_token();
                    if(token.type == ')')
                    {
                        return node;
                    }
                    else
                    {
                        unget_token();
                        return std::shared_ptr<cond_node>();
                    }
                }
                else
                {
                    unget_token();
                    return std::shared_ptr<cond_node>();
                }
            }
        }

        std::shared_ptr<cond_node> literal()
        {
            int i0 = i_;
            cond_token token = get_token();
            switch(token.type)
            {
            case SLPP_COND_BOOLEAN:
                {
                    int v = (token.value == "true") ? 1 : 0;
                    std::shared_ptr<cond_node> node(new cond_node(SLPP_COND_VALUE, v));
                    return node;
                }
            case SLPP_COND_INTEGER:
                {
                    int v = str2int(token.value.c_str());
                    std::shared_ptr<cond_node> node(new cond_node(SLPP_COND_VALUE, v));
                    return node;
                }
            }
            i_ = i0;
            return std::shared_ptr<cond_node>();
        }
    private:
        cond_token get_token()
        {
            if(i_ < tokens_.size())
            {
                return tokens_[i_++];
            }
            else
            {
                return cond_token(SLPP_COND_EMPTY, "");
            }
        }
        void unget_token()
        {
            i_ = std::max<int>(i_-1, 0);
        }
    private:
        std::vector<cond_token> tokens_;
        int i_;
    };

    static
    int fold_data(const cond_node* node)
    {
        switch(node->type)
        {
            case SLPP_COND_VALUE:
                {
                    return node->value;
                }
            case SLPP_COND_BINARY_ADD:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return a + b;
                }
            case SLPP_COND_BINARY_SUB:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return a - b;
                }
            case SLPP_COND_BINARY_MUL:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return a * b;
                }
            case SLPP_COND_BINARY_DIV:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    if(b != 0)
                    {
                        return a / b;
                    }
                    else
                    {
                        return 0;
                    }
                }
            case SLPP_COND_BINARY_MOD:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return a % b;
                }
            case SLPP_COND_UNARY_MINUS:
                {
                    int a = fold_data(node->children[0].get());
                    return - a;
                }
            case SLPP_COND_BINARY_AND:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return a & b;
                }
            case SLPP_COND_BINARY_OR:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return a | b;
                }
            case SLPP_COND_BINARY_XOR:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return a ^ b;
                }
            case SLPP_COND_LSHIFT:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return a << b;
                }
            case SLPP_COND_RSHIFT:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return a >> b;
                }
            case SLPP_COND_LOGAND:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return int(a && b);
                }
            case SLPP_COND_LOGOR:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return int(a || b);
                }
            case SLPP_COND_LESS:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return int(a < b);
                }
            case SLPP_COND_GREATER:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return int(a > b);
                }
            case SLPP_COND_LESS_EQ:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return int(a <= b);
                }
            case SLPP_COND_GREATER_EQ:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return int(a >= b);
                }
            case SLPP_COND_EQUAL:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return int(a == b);
                }
            case SLPP_COND_NEQ:
                {
                    int a = fold_data(node->children[0].get());
                    int b = fold_data(node->children[1].get());
                    return int(a != b);
                }
            case SLPP_COND_UNARY_NOT:
                {
                    int a = fold_data(node->children[0].get());
                    return int(!a);
                }
            case SLPP_COND_COND:
                {
                    int a = fold_data(node->children[0].get());
                    if(a)
                    {
                        return fold_data(node->children[1].get());
                    }
                    else
                    {
                        return fold_data(node->children[2].get());
                    }
                }
        }
        return 0;
    }

    static
    void print_cond_node(const cond_node* node, int level = 0)
    {
        for(int i=0;i<level;i++)printf("    ");
        if(node->type < 256)
        {
            printf("type:%c, token:%d\n", (char)node->type, node->value);
        }
        else
        {
            printf("type:%d, token:%d\n", node->type, node->value);
        }
        for(int i=0;i<node->children.size();i++)print_cond_node(node->children[i].get(), level+1);
    }

    condition::condition(const char* szCondition)
        :is_valid_(false), is_result_(false)
    {
        cond_parser parser(szCondition);
        std::shared_ptr<cond_node> node = parser.eval();
        if(node.get())
        {
            int data = fold_data(node.get());
            is_valid_ = true;
            is_result_ = data ? true : false;
        }
    }

    condition::~condition()
    {

    }

    bool condition::is_valid()const
    {
        return is_valid_;
    }

    bool condition::eval_bool()const
    {
        return is_result_;
    }

}
