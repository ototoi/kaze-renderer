#include "input_stream.h"
#include <fstream>

namespace slpp
{
    console_input_stream::console_input_stream(std::istream& istream)
        :istream_(istream)
    {
        ;
    }

    int console_input_stream::get()
    {
        return istream_.get();
    }

    void console_input_stream::unget()
    {
        istream_.unget();
    }

    std::string console_input_stream::get_path()const
    {
        return "";
    }

#define MAX_READ 4096
    file_input_stream::file_input_stream(const char* szPath)
    {
        //istream_.reset(new std::ifstream(szPath));
        fp_ = fopen(szPath, "rb");
        buffer_.resize(MAX_READ);
        sz_ = fread(&buffer_[0], 1, MAX_READ, fp_);
        i_ = 0;
        path_ = szPath;
    }

    file_input_stream::~file_input_stream()
    {
        if(fp_)fclose(fp_);
    }

    int file_input_stream::get()
    {
        if(sz_<=0)return EOF;
        if(sz_-i_<=0)
        {
            sz_ = fread(&buffer_[0], 1, MAX_READ, fp_);
            i_ = 0;
            if(sz_<=0)return EOF;
        }
        return buffer_[i_++];
    }

    void file_input_stream::unget()
    {
        i_--;
        //istream_->unget();
    }

    std::string file_input_stream::get_path()const
    {
        return path_;
    }
}
