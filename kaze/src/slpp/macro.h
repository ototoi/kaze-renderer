#ifndef SLPP_MACRO_H
#define SLPP_MACRO_H

#include <string>
#include <vector>

namespace slpp
{
    enum SLPP_MACRO_TYPE
    {
        SLPP_MACRO_BUILTIN_VARIABLE,
        SLPP_MACRO_BUILTIN_FUNCTION,
        SLPP_MACRO_VARIABLE,
        SLPP_MACRO_FUNCTION
    };

    struct macro_def
    {
        int type;
        std::string name;
        std::vector<std::string> args;
        std::string body;
    };
}

#endif
