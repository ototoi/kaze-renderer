#ifndef SLPP_OUTPUT_STREAM_H
#define SLPP_OUTPUT_STREAM_H

#include <memory>
#include <fstream>

namespace slpp
{

    class output_stream
    {
    public:
        virtual ~output_stream(){}
        virtual void put(char c) = 0;
        virtual int write(const char* szLine) = 0;
    };

    class console_output_stream : public output_stream
    {
    public:
        console_output_stream(std::ostream& os);
        virtual void put(char c);
        virtual int write(const char* szLine);
    private:
        std::ostream& os_;
    };

    class file_output_stream : public output_stream
    {
    public:
        file_output_stream(const char* szPath);
        ~file_output_stream();
        virtual void put(char c);
        virtual int write(const char* szLine);
    private:
        std::unique_ptr<std::ostream> os_;
    };


}

#endif
