#ifndef SLPP_CHAR_FUNC_H
#define SLPP_CHAR_FUNC_H

#include <cstring>
#include <cstdlib>

//#define SLPP_USE_ORG_FUNC

namespace slpp
{
    enum SLPP_CHAR_MASK
    {
        SLPP_IS_SPACE = 1,
        SLPP_IS_ALPHA = 2,
        SLPP_IS_DIGIT = 4,
        SLPP_IS_HEX   = 8,
        SLPP_IS_OCT   = 16,
    };

    static const int CHAR_MASK_TABLE[256] =
    {
        0,0,0,0,0,0,0,0,0,1,1,1,1,1,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        28,28,28,28,28,28,28,28,12,12,0,0,0,0,0,0,
        0,10,10,10,10,10,10,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0,
        0,10,10,10,10,10,10,2,2,2,2,2,2,2,2,2,
        2,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
    };

#ifndef SLPP_USE_ORG_FUNC
    inline int isspace_(int c)
    {
        return CHAR_MASK_TABLE[c] & SLPP_IS_SPACE;
    }

    inline int isalpha_(int c)
    {
        return CHAR_MASK_TABLE[c] & SLPP_IS_ALPHA;
    }

    inline int isalnum_(int c)
    {
        return CHAR_MASK_TABLE[c] & (SLPP_IS_ALPHA|SLPP_IS_DIGIT);
    }

    inline int isdigit_(int c)
    {
        return CHAR_MASK_TABLE[c] & SLPP_IS_DIGIT;
    }
#else
    inline int isspace_(int c)
    {
        return std::isspace(c);
    }

    inline int isalpha_(int c)
    {
        return std::isalpha(c);
    }

    inline int isalnum_(int c)
    {
        return std::isalnum(c);
    }

    inline int isdigit_(int c)
    {
        return std::isdigit(c);
    }
#endif

    inline int ishex_(int c)
    {
        return CHAR_MASK_TABLE[c] & SLPP_IS_HEX;
    }

    inline int isoct_(int c)
    {
        return CHAR_MASK_TABLE[c] & SLPP_IS_OCT;
    }
}

#endif
