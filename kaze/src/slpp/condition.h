#ifndef SLPP_CONDITION_H
#define SLPP_CONDITION_H

#include <string>

namespace slpp
{
    class condition
    {
    public:
        condition(const char* szCondition);
        ~condition();
        bool is_valid()const;
        bool eval_bool()const;
    private:
        bool is_valid_;
        bool is_result_;
    };
}

#endif
