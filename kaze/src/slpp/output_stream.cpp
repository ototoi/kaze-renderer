#include "output_stream.h"
#include <fstream>

namespace slpp
{
    console_output_stream::console_output_stream(std::ostream& os)
        :os_(os)
    {
        ;
    }

    void console_output_stream::put(char c)
    {
        os_.put(c);
    }

    int console_output_stream::write(const char* szLine)
    {
        size_t sz = strlen(szLine);
        if(sz)
        {
            os_.write(szLine, sz);
        }
        os_.flush();
        return 0;
    }

    file_output_stream::file_output_stream(const char* szPath)
    {
        os_.reset(new std::ofstream(szPath));
    }

    file_output_stream::~file_output_stream()
    {
        os_->flush();
    }

    void file_output_stream::put(char c)
    {
        os_->put(c);
    }

    int file_output_stream::write(const char* szLine)
    {
        os_->write(szLine, strlen(szLine));
        return 0;
    }
}
