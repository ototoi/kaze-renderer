#include "context.h"
#include "condition.h"
#include "char_func.h"

#include <string.h>
//#include <ctype.h>
#include <time.h>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>

#include <cassert>

#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif

namespace slpp
{
    static const char* DEFAULT_DEFINED_MACRO_VALUE = "  ";

    enum SLPP_DIRECTIVE_ID
    {
        SLPP_DIRECTIVE_IF = 1,
        SLPP_DIRECTIVE_ELSE,
        SLPP_DIRECTIVE_ELIF,
        SLPP_DIRECTIVE_ENDIF,
        SLPP_DIRECTIVE_IFDEF,
        SLPP_DIRECTIVE_IFNDEF,
        SLPP_DIRECTIVE_DEFINE,
        SLPP_DIRECTIVE_UNDEF,
        SLPP_DIRECTIVE_INCLUDE,
        SLPP_DIRECTIVE_INCLUDE_NEXT,
        SLPP_DIRECTIVE_PRAGMA,
        SLPP_DIRECTIVE_LINE,
        SLPP_DIRECTIVE_ERROR,
        SLPP_DIRECTIVE_WARNING,
        SLPP_DIRECTIVE_VERSION,
        SLPP_DIRECTIVE_IMPORT,
        SLPP_DIRECTIVE_USING,
    };

    struct SLPP_TOKEN_INT
    {
        const char* name;
        int id;
    };

    static const SLPP_TOKEN_INT SLPP_DIRECTVES[] =
    {
        {"include_next", SLPP_DIRECTIVE_INCLUDE_NEXT},
        {"include", SLPP_DIRECTIVE_INCLUDE},
        {"define",  SLPP_DIRECTIVE_DEFINE},
        
        {"ifndef", SLPP_DIRECTIVE_IFNDEF},
        {"ifdef", SLPP_DIRECTIVE_IFDEF},
        {"undef",  SLPP_DIRECTIVE_UNDEF},
        {"endif", SLPP_DIRECTIVE_ENDIF}, 
        {"else", SLPP_DIRECTIVE_ELSE},
        {"elif", SLPP_DIRECTIVE_ELIF},
        {"if", SLPP_DIRECTIVE_IF},
        {"line", SLPP_DIRECTIVE_LINE},
        {"pragma",  SLPP_DIRECTIVE_PRAGMA},
        {"error",  SLPP_DIRECTIVE_ERROR},
        {"warning",  SLPP_DIRECTIVE_WARNING},
        {"version", SLPP_DIRECTIVE_VERSION},
        {"import", SLPP_DIRECTIVE_IMPORT},
        {"using", SLPP_DIRECTIVE_USING},
        {NULL, -1}
    };

    static SLPP_TOKEN_INT SLPP_BUILTIN_MACRO_NAMES[] = 
    {
        {"__LINE__", SLPP_MACRO_BUILTIN_VARIABLE},
        {"__DATE__", SLPP_MACRO_BUILTIN_VARIABLE},
        {"__TIME__", SLPP_MACRO_BUILTIN_VARIABLE},
        {"__FILE__", SLPP_MACRO_BUILTIN_VARIABLE},
        {"_Pragma",  SLPP_MACRO_BUILTIN_FUNCTION},
        //"__FUNCTION__",         //gcc, msvc
        //"__PRETTY_FUNCTION__",  //gcc
        //"__FUNCDNAME__",        //msvc
        //"__FUNCSIG__",          //msvc
        {NULL, -1}
    };

    ///<summary>
    ///return directive_id
    ///</summary>
    static
    int get_directive_id(const char* szDirective)
    {
        int i = 0;
        while(SLPP_DIRECTVES[i].name != NULL)
        {
            if(memcmp(szDirective, SLPP_DIRECTVES[i].name, strlen(SLPP_DIRECTVES[i].name)) == 0)
            {
                return SLPP_DIRECTVES[i].id;
            }
            i++;
        }
        return -1;
    }

    static
    inline std::string dirname(const std::string& path)
    {
        return path.substr(0, path.find_last_of('/'));
    }

    static
    inline bool is_exist(const std::string& path)
    {
        const char* szFilePath = path.c_str();
#ifdef _WIN32
        struct _stat s;
        int nRet = _stat(szFilePath, &s);
        return (nRet == 0);
#else
        struct stat s;
        int nRet = stat(szFilePath, &s);
        return (nRet == 0);
#endif
    }

    static std::string fullpath(const std::string& path)
    {
#if defined(_WIN32)
        char szFullPath[_MAX_PATH];
        _fullpath(szFullPath, path.c_str(), _MAX_PATH);
        return szFullPath;
#else
        char szFullPath[1024];
        realpath(path.c_str(), szFullPath);
        return szFullPath;
#endif
    }

    static
    inline const char* skip_ws(const char* s)
    {
        while(isspace_(*s))s++;
        return s;
    }

    static
    inline const char* skip_nl(const char* s)
    {
        while((*s != 0) && (*s != '\r') && (*s != '\n'))s++;
        return s;
    }

    class string_range
    {
    public:
        string_range(const char* s0, const char* s1)
            :s0_(s0), s1_(s1){}
        operator std::string() const
        {
            return std::string(s0_, s1_);
        }
        char operator[](int i)const{return s0_[i];}
        size_t size()const{return s1_ - s0_;}
    protected:
        const char* s0_;
        const char* s1_;
    };

    static 
    inline string_range get_number(const char* szLine)
    {
        const char* s = szLine;
        char c = *s;
        if(isdigit_(c) && c != '0')
        {
            s++;
            while(isdigit_(*s))
            {
                s++;
            }
        }
        return string_range(szLine, s);
    }

    static
    inline string_range get_token(const char* szLine)
    {
        const char* s = szLine;
        char c = *s;
        if(c == 0)return string_range(s, s);

        if(c == '"')
        {
            bool next_ignore = false;
            s++;
            while(*s != 0)
            {
                if(*s == '\\')
                {
                    next_ignore = true;
                }
                else
                {
                    if(*s == '"')
                    {
                        if(!next_ignore)
                        {
                            s++;
                            break;
                        }
                    }
                    next_ignore = false;
                }
                s++;
            }
            return string_range(szLine, s);
        }

        if(isalpha_(c) || c == '_')
        {
            s++;
        }
        else
        {
            if(c == '#')
            {
                char c2 = *(s+1);
                if(c2 == '#')
                {
                    return string_range(szLine, s+2);
                }
                else
                {
                    return string_range(szLine, s+1);
                }
            }
            else
            {
                return string_range(szLine, s+1);
            }
        }

        while(*s != 0)
        {
            c = *s;
            if(isalnum_(c) || c == '_')
            {
                s++;
            }
            else
            {
                return string_range(szLine, s);
            }
        }
        return string_range(szLine, s);
    }

    class inc_guard_detector
    {
    public:
        inc_guard_detector(const std::string& strFile)
        {
            ifs_.reset(new std::ifstream(strFile));
        }

        std::string get_begin_guard_key()
        {
            std::string key = "";
            int state = 0;
            std::string strLine;
            while(std::getline(*ifs_, strLine))
            {
                const char* s = strLine.c_str();
                s = skip_ws(s);
                if(*s == '#')
                {
                    s++;
                    std::string token = get_token(s);
                    if(token == "pragma")
                    {
                        s += token.size();
                        s = skip_ws(s);
                        token = get_token(s);
                        if(token == "once")
                        {
                            state |= 1;
                        }
                        else
                        {
                            state |= 8;
                        }
                    }
                    else if(token == "ifndef")
                    {
                        if(state <= 1)
                        {
                            s += token.size();
                            s = skip_ws(s);
                            key = get_token(s);
                            state |= 2;
                        }
                        else
                        {
                            state |= 8;
                        }
                    }
                    else if(token == "define")
                    {
                        if((state <= 3) && (state & 2))
                        {
                            s += token.size();
                            s = skip_ws(s);
                            token = get_token(s);
                            if(key == token)
                            {
                                state |= 4;
                            }
                            else
                            {
                                state |= 8;
                            }
                        }
                        else
                        {
                            state |= 8;
                        }
                    }
                    else
                    {
                        state |= 8;
                    }
                }
                else if(*s == '\0')
                {
                    ;
                }
                else
                {
                    state |= 8;
                }

                if(state & 8)
                {
                    return "";
                }
                else if((state & 6) == 6)
                {
                    return key;
                }
            }
            return "";
        }

        std::string get_inc_guard_key()
        {
            std::string key = get_begin_guard_key();
            if(key.empty())return key;
            bool isEndif = false; 
            std::string strLine;
            while(std::getline(*ifs_, strLine))
            {
                const char* s = strLine.c_str();
                s = skip_ws(s);
                if(*s == '#')
                {
                    s++;
                    std::string token = get_token(s);
                    if(token == "endif")
                    {
                        isEndif = true; 
                    }
                    else
                    {
                        isEndif = false;
                    }
                }
                else
                {
                    isEndif = false;
                }
            }

            if(isEndif)
            {
                return key;
            }
            else return "";
        }
    protected:
        std::shared_ptr<std::ifstream> ifs_;
    };

    typedef context::macro_def_type macro_def_type;
    typedef context::hide_set_type hide_set_type;

    ///<summary>
    ///
    ///</summary>
    context::context(
        const context_options& opt,
        const std::shared_ptr<input_stream>& is,
        const std::shared_ptr<output_stream>& os
    )
        :options_(opt)
    {
        input_streams_.push_back(is);
        output_streams_.push_back(os);
        condition_.push_back(true);
        states_.push_back(context_state());

        {
            int i = 0;
            while(SLPP_BUILTIN_MACRO_NAMES[i].name)
            {
                std::shared_ptr<macro_def> def(new macro_def());
                def->type = SLPP_BUILTIN_MACRO_NAMES[i].id;
                def->name = SLPP_BUILTIN_MACRO_NAMES[i].name;
                if(def->type == SLPP_MACRO_BUILTIN_FUNCTION)
                {
                    def->args.push_back("s");
                }
                this->register_macro(def);
                i++;
            }
        }
    }

    ///<summary>
    ///
    ///</summary>
    context::~context()
    {
        ;
    }

    ///<summary>
    /// Run is main
    ///</summary>
    int context::run()
    {
        bool is_in_s_quoted = false;
        bool is_in_d_quoted = false;
        std::vector<char> buffer;
        buffer.reserve(1024);

        states_.back().mode = SLPP_CONTEXT_NORMAL;
        states_.back().line = 1;
        states_.back().line_total = 1;
        states_.back().file_name = "\"" + this->get_current_input_path() + "\"";

        {
            std::string s;
            s += "#line ";
            s += "1";
            s += " ";
            s += states_.back().file_name;
            s += "\n";
            this->write(s.c_str());
        }

        while(true)
        {
            int ch = this->get();
            if (ch == EOF)
            {
                if(!buffer.empty())
                {
                    buffer.push_back('\0');
                    int nRet = 0;
                    if( (nRet = this->eval(&buffer[0])) != 0)
                    {
                        return nRet;
                    }
                    buffer.clear();
                    continue;
                }
                
                if(this->input_streams_.size() >= 2)
                {
                    this->states_.pop_back();
                    this->input_streams_.pop_back();
                    continue;
                }
                else
                {
                    break;
                }
            }

            {
                switch(ch)
                {
                case '#':
                    {
                        if(this->is_in_normal())
                        {
                            if(!is_in_s_quoted && !is_in_d_quoted)
                            {
                                if(!buffer.empty())
                                {
                                    buffer.push_back('\0');
                                    int nRet = 0;
                                    if( (nRet = this->eval(&buffer[0])) != 0)
                                    {
                                        return nRet;
                                    }
                                    buffer.clear();
                                }
                                states_.back().mode = SLPP_CONTEXT_DIRECTIVE;
                            }
                        }
                        buffer.push_back(ch);
                        break;
                    }
                case '/':
                    {
                        if(!this->is_in_comment() && !this->is_in_comment_oneline()) 
                        {
                            if(!is_in_d_quoted && !is_in_s_quoted)
                            {
                                int ch2 = this->get();
                                if(ch2 == '*')              // as in "/*"
                                {
                                    if(this->is_in_directive())
                                    {
                                        this->unget();
                                        this->unget();
                                        this->skip_directive_comment(buffer);
                                    }
                                    else
                                    {
                                        buffer.push_back('\0');
                                        this->eval(&buffer[0]);
                                        buffer.clear();

                                        buffer.push_back(ch);
                                        buffer.push_back(ch2);
                                        states_.back().mode = SLPP_CONTEXT_COMMENT;
                                    }
                                }
                                else if(ch2 == '/')         // as in "//"
                                {
                                    buffer.push_back('\0');
                                    int nRet = 0;
                                    if( (nRet = this->eval(&buffer[0])) != 0)
                                    {
                                        return nRet;
                                    }
                                    buffer.clear();
                                    
                                    buffer.push_back(ch);
                                    buffer.push_back(ch2);
                                    states_.back().mode = SLPP_CONTEXT_COMMENT_ONELINE;
                                }
                                else
                                {
                                    this->unget();
                                    buffer.push_back(ch);
                                }
                            }
                            else
                            {
                                buffer.push_back(ch);
                            }
                        }
                        else
                        {
                            buffer.push_back(ch);
                        }
                    }
                    break;
                case '\\':
                    {
                        if(!this->is_in_comment() && !this->is_in_comment_oneline()) 
                        {
                            int ch2 = this->get();
                            if(ch2 == '\r')
                            {
                                int ch3 = this->get();
                                if(ch3 == '\n')
                                {
                                    states_.back().line_total++;
                                }
                                else
                                {
                                    this->unget();
                                    buffer.push_back(ch);
                                    buffer.push_back(ch2);
                                }
                            }
                            else if(ch2 == '\n')
                            {
                                states_.back().line_total++;
                            }
                            else if(ch2 == '"')
                            {
                                if(!is_in_d_quoted && !is_in_s_quoted)
                                {
                                    is_in_d_quoted = !is_in_d_quoted;
                                }
                                buffer.push_back(ch);
                                buffer.push_back(ch2);
                            }
                            else if(ch2 == '\'')
                            {
                                if(!is_in_s_quoted && !is_in_d_quoted)
                                {
                                    is_in_s_quoted = !is_in_s_quoted;
                                }
                                buffer.push_back(ch);
                                buffer.push_back(ch2);
                            }
                            else
                            {
                                this->unget();
                                buffer.push_back(ch);
                            }
                        }
                        else
                        {
                            buffer.push_back(ch);
                        }
                    }
                    break;
                case '\n':
                    {
                        int state_index = states_.size()-1;
                        if(this->is_in_directive() || this->is_in_comment_oneline())
                        {
                            buffer.push_back(ch);
                            buffer.push_back('\0');
                            int nRet = 0;
                            if( (nRet = this->eval(&buffer[0])) != 0)
                            {
                                return nRet;
                            }
                            buffer.clear();

                            states_[state_index].mode = SLPP_CONTEXT_NORMAL;
                            states_[state_index].line_total++;
                            states_[state_index].line = states_[state_index].line_total;
                        }
                        else
                        {
                            buffer.push_back(ch);
                            states_[state_index].line_total++;
                        }
                    }
                    break;
                case '"':
                    {
                        if(!this->is_in_comment() && !this->is_in_comment_oneline())
                        {
                            if(!is_in_s_quoted) 
                            {
                                is_in_d_quoted = !is_in_d_quoted;
                            }
                            buffer.push_back(ch);
                        }
                        else
                        {
                            buffer.push_back(ch);
                        }
                    }
                    break;
                case '\'':
                    {
                        if(!this->is_in_comment() && !this->is_in_comment_oneline())
                        {
                            if(!is_in_d_quoted) 
                            {
                                is_in_s_quoted = !is_in_s_quoted;
                            }
                            buffer.push_back(ch);
                        }
                        else
                        {
                            buffer.push_back(ch);
                        }
                    }
                    break;
                case '*':
                    {
                        if(this->is_in_comment())
                        {
                            int ch2 = this->get();
                            if(ch2 == '/')
                            {                           
                                buffer.push_back(ch);
                                buffer.push_back(ch2);
                                buffer.push_back('\0');
                                states_.back().line = states_.back().line_total;
                                int nRet = 0;
                                if( (nRet = this->eval(&buffer[0])) != 0)
                                {
                                    return nRet;
                                }
                                buffer.clear();
                                states_.back().mode = SLPP_CONTEXT_NORMAL;
                            }
                            else
                            {
                                this->unget();
                                buffer.push_back(ch);
                            }
                        }
                        else
                        {
                            buffer.push_back(ch);
                        }
                    }
                    break;
                default:
                    {
                        buffer.push_back(ch);
                    }
                    break;
                }
            }
        }

        return 0;
    }

    int context::skip_directive_comment(std::vector<char>& buffer)
    {
        bool is_in_comment = false;
        while(true)
        {
            int ch = this->get();
            switch(ch)
            {
            case EOF:
                {
                    this->unget();
                    return 0;
                }
            case '/':
                {
                    int ch2 = this->get();
                    if(ch2 == '*')
                    {
                        is_in_comment = true;
                    }
                    else if(ch2 == '/')
                    {
                        this->unget();
                        this->unget();
                        return 0;
                    }
                    else
                    {
                        this->unget();
                        buffer.push_back(ch);
                    }
                }
                break;
            case '*':
                {
                    int ch2 = this->get();
                    if(ch2 == '/')
                    {
                        buffer.push_back(' ');
                        is_in_comment = false;
                    }
                    else
                    {
                        this->unget();
                        buffer.push_back(ch);
                    }
                }
                break;
            case '\\':
                {
                    this->unget();
                    return 0;
                }
                break;
            case '\r':
                {
                    this->unget();
                    return 0;
                }
                break;
            case '\n':
                {
                    this->unget();
                    return 0;
                }
                break;
            default:
                {
                    if(!is_in_comment)
                    {
                        buffer.push_back(ch);
                    }
                }
                break;
            }
        }
        return 0;
    }

    int context::get()
    {
        return input_streams_.back()->get();
    }

    void context::unget()
    {
        input_streams_.back()->unget();
    }

    bool context::is_output_comment()const
    {
        return options_.is_output_comment;
    }

    bool context::is_in_normal()const    
    {
        return states_.back().mode == SLPP_CONTEXT_NORMAL;
    }

    bool context::is_in_comment()const
    {
        return states_.back().mode == SLPP_CONTEXT_COMMENT;
    }

    bool context::is_in_comment_oneline()const
    {
        return states_.back().mode == SLPP_CONTEXT_COMMENT_ONELINE;
    }

    bool context::is_in_directive()const
    {
        return states_.back().mode == SLPP_CONTEXT_DIRECTIVE;
    }

    int context::eval(char* szLine)
    {
        if(this->is_in_comment())
        {
            return this->eval_comment(szLine);
        }
        else if(this->is_in_comment_oneline())
        {
            if(this->is_output_comment())
            {
                return this->write(szLine);
            }
            else
            {
                return this->write(skip_nl(szLine));
            }
        }
        else if(this->is_in_directive())
        {
            return this->eval_directive(szLine);
        }
        else
        {
            return this->eval_macro(szLine);
        }
    }

    int context::eval_comment(const char* szLine)
    {
         if(this->is_output_comment())
        {
            return this->write(szLine);
        }
        else
        {
            std::string str;
            const char* s = szLine;
            while(*s)
            {
                if(*s == '\r')
                {
                    str += *s;
                }
                else if(*s == '\n')
                {
                    str += *s;
                }
                s++;
            }

            if(str.empty())
            {
                str = " ";
            }

            return this->write(str.c_str());
        }
    }

    int context::eval_directive(const char* szLine)
    {
        const char* s = szLine;
        s = skip_ws(s);
        assert(*s == '#');
        s++;
        s = skip_ws(s);

        int directive_id = get_directive_id(s);
        if(directive_id > 0)
        {
            switch(directive_id)
            {
            case SLPP_DIRECTIVE_IF:
                return this->eval_directive_if(s);
            case SLPP_DIRECTIVE_ELSE:
                return this->eval_directive_else(s);
            case SLPP_DIRECTIVE_ELIF:
                return this->eval_directive_elif(s);
            case SLPP_DIRECTIVE_ENDIF:
                return this->eval_directive_endif(s);
            case SLPP_DIRECTIVE_IFDEF:
                return this->eval_directive_ifdef(s);
            case SLPP_DIRECTIVE_IFNDEF:
                return this->eval_directive_ifndef(s);
            }

            if(!this->is_if_true())
            {
                return this->write(skip_nl(s));
            }

            switch(directive_id)
            {
            case SLPP_DIRECTIVE_DEFINE:
                return this->eval_directive_define(s);
            case SLPP_DIRECTIVE_UNDEF:
                return this->eval_directive_undef(s);
            case SLPP_DIRECTIVE_INCLUDE:
                return this->eval_directive_include(s);
            case SLPP_DIRECTIVE_INCLUDE_NEXT:
                return this->eval_directive_include_next(s);
            case SLPP_DIRECTIVE_PRAGMA:
                return this->eval_directive_pragma(szLine);
            case SLPP_DIRECTIVE_VERSION:
                return this->eval_directive_version(szLine);
            case SLPP_DIRECTIVE_LINE:
                return this->eval_directive_line(szLine);
            case SLPP_DIRECTIVE_ERROR:
                return this->eval_directive_error(s);
            case SLPP_DIRECTIVE_WARNING:
                return this->eval_directive_warning(s);
            case SLPP_DIRECTIVE_IMPORT:
                return this->eval_directive_import(szLine);
            case SLPP_DIRECTIVE_USING:
                return this->eval_directive_using(szLine);
            }
        }
        return this->write(skip_nl(s));
    }

    static
    inline const char* define_find_param_name(const char* s)
    {
        while((*s != 0) && (!isspace_(*s)) && (*s != '\r') && (*s != '\n') && (*s != '('))s++;
        return s;
    }

    static
    inline const char* define_find_param_last(const char* s)
    {
        int nest = 1;
        while((*s != 0) && (*s != '\r') && (*s != '\n') && nest >= 1)
        {
            if(*s == '(')nest++;
            if(*s == ')')nest--;
            s++;
        }
        s--;
        return s;
    }

    static
    inline void split_macro_args(std::vector<std::string>& v, const std::string& str)
    {
        const char* s = str.c_str();
        std::vector<char> buffer;
        buffer.reserve(str.size());
        s = skip_ws(s);

        bool is_in_string = false;
        int nest = 0;
        while(*s != 0)
        {
            if(!is_in_string)
            {
                s = skip_ws(s);
            }
            if(*s == '(')
            {
                nest++;
                buffer.push_back(*s);
            }
            else if(*s == ')')
            {
                nest--;
                buffer.push_back(*s);
            }
            else if(*s == ',' && nest <= 0)
            {
                buffer.push_back(0);
                v.push_back(std::string(&buffer[0]));
                buffer.clear();
            }
            else
            {
                if(*s == '"' || *s == '\'')
                {
                    is_in_string = !is_in_string;
                }
                buffer.push_back(*s);
            }
            s++;
        }
        if(!buffer.empty())
        {
            buffer.push_back(0);
            v.push_back(std::string(&buffer[0]));
        }
    }

    int context::eval_directive_define(const char* szLine)
    {
        static const int DIRECTIVE_NAME_LEN = strlen("define");

        const char* s = szLine;
        s += DIRECTIVE_NAME_LEN;//strlen("define");
        s = skip_ws(s);
        const char* sEnd = define_find_param_name(s);
        std::string name = std::string(s, sEnd);
        std::string body = "";
        std::vector<std::string> args;
        if(name.empty())return -1;
        s = skip_ws(sEnd);
        bool isFunc = (*s == '(');
        if(isFunc)
        {
            const char* paramStart = skip_ws(s+1);
            const char* paramEnd = define_find_param_last(paramStart);
            if(*paramEnd != ')')return -1;
            if(paramEnd-paramStart>0)
            {
                split_macro_args(args, std::string(paramStart, paramEnd));
            }
            s = skip_ws(paramEnd+1);
            body = std::string(s, skip_nl(s));
        }
        else
        {
            body = std::string(s, skip_nl(s));
            if(body.empty())
            {
                body = DEFAULT_DEFINED_MACRO_VALUE;
            }
        }

        std::shared_ptr<macro_def> def(new macro_def());
        def->type = isFunc ? SLPP_MACRO_FUNCTION : SLPP_MACRO_VARIABLE;
        def->name = name;
        def->args = args;
        def->body = body;

        if(!this->register_macro(def))return -1;
        return this->write(skip_nl(s));
    }

    int context::eval_directive_undef(const char* szLine)
    {
        static const int DIRECTIVE_NAME_LEN = strlen("undef");

        const char* s = szLine;
        s += DIRECTIVE_NAME_LEN;//strlen("undeef");
        s = skip_ws(s);
        std::string name = get_token(s);
        if(name.empty())return -1;
        this->unregister_macro(name.c_str());
        return this->write(skip_nl(s));
    }

    static 
    std::string get_include_file_path(const std::vector<std::string>& include_dirs, const std::string& path)
    {
        for(size_t i = 0;i<include_dirs.size();i++)
        {
            std::string realpath = include_dirs[i] + "/" + path;
            if(is_exist(realpath))
            {
                return realpath;
            }
        }
        return "";
    }

    int context::eval_directive_include_core(const char* szLine, size_t len)
    {
        const char* s = szLine;
        s += len;
        s = skip_ws(s);

        std::string str = eval_macro_detail(s);
        s = str.c_str();

        char cEnd = '\"';
        if (*s == '\"')
        {
            cEnd = '\"';
        }
        else if (*s == '<')
        {
            cEnd = '>';
        }
        s++;
        std::string path = "";
        while(*s != 0 && *s != cEnd)
        {
            path += *s;
            s++;
        }

        std::vector<std::string> include_dirs;
        if(cEnd == '\"')
        {
            std::string dir = dirname(get_current_input_path());
            include_dirs.push_back(dir);
        }
        if(!options_.include_dirs.empty())
        {
            include_dirs.insert(include_dirs.end(), options_.include_dirs.begin(), options_.include_dirs.end());
        }
        std::string inc_path = get_include_file_path(include_dirs, path);
        if(!inc_path.empty())
        {
            std::string fpath = fullpath(inc_path);
            if(!this->is_already_included(fpath))
            {
                includes_[fpath].has_pragma_once = false;
                {
                    inc_guard_detector detect(fpath);
                    includes_[fpath].inc_guard_key = detect.get_inc_guard_key();
                }
                states_.push_back(context_state());
                input_streams_.push_back(std::shared_ptr<input_stream>( new file_input_stream(inc_path.c_str())));
            }
        }
        else
        {
            char buffer[255] = {};
            sprintf(buffer, "'%s' file not found\n", path.c_str() );
            return this->print_error(buffer);
        }
        return 0;
    }

    int context::eval_directive_include(const char* szLine)
    {
        static const int DIRECTIVE_NAME_LEN = strlen("include");
        return eval_directive_include_core(szLine, DIRECTIVE_NAME_LEN);
    }

    int context::eval_directive_include_next(const char* szLine)
    {
        static const int DIRECTIVE_NAME_LEN = strlen("eval_directive_include_next");
        return eval_directive_include_core(szLine, DIRECTIVE_NAME_LEN);
    }

    int context::eval_directive_if(const char* szLine)
    {
        static const int DIRECTIVE_NAME_LEN = strlen("if");

        const char* s = szLine;
        s += DIRECTIVE_NAME_LEN;
        s = skip_ws(s);

        std::string str = s;
        std::string strOut;
        if(this->eval_macro_defined(strOut, str) != 0)
        {
            return -1;
        }
        else
        {
            str.swap(strOut);
        }
        str = this->eval_macro_detail(str);
        if(str.empty())return -1;
        if(this->eval_macro_defined(strOut, str) != 0)
        {
            return -1;
        }
        else
        {
            str.swap(strOut);
        }
        condition cond(str.c_str());
        bool bRet = false;
        if(cond.is_valid())
        {
            bRet = cond.eval_bool();
        }
        else
        {
            return -1;
        }
        condition_.push_back(true);
        condition_.push_back(bRet);

        return this->write(skip_nl(s));
    }

    int context::eval_directive_else(const char* szLine)
    {
        if(condition_.size() < 2)return -1;
        condition_.back() = !condition_.back();
        return this->write(skip_nl(szLine));
    }

    int context::eval_directive_elif(const char* szLine)
    {
        static const int DIRECTIVE_NAME_LEN = strlen("elif");

        if(condition_.size() < 2)return -1;

        const char* s = szLine;
        s += DIRECTIVE_NAME_LEN;
        s = skip_ws(s);

        if(condition_.back())
        {
            condition_[condition_.size()-2] = false;
            condition_.back() = false;
        }
        else
        {
            std::string str = s;
            std::string strOut;
            if(this->eval_macro_defined(strOut, str) != 0)
            {
                return -1;
            }
            else
            {
                str.swap(strOut);
            }
            str = this->eval_macro_detail(str);
            if(str.empty())return -1;
            if(this->eval_macro_defined(strOut, str) != 0)
            {
                return -1;
            }
            else
            {
                str.swap(strOut);
            }
            condition cond(str.c_str());
            bool bRet = false;
            if(cond.is_valid())
            {
                bRet = cond.eval_bool();
            }
            condition_.back() = bRet;
        }

        return this->write(skip_nl(s));
    }

    int context::eval_directive_endif(const char* szLine)
    {
        if(condition_.size() < 2)return -1;

        condition_.pop_back();
        condition_.pop_back();
        return this->write(skip_nl(szLine));
    }

    int context::eval_directive_ifdef(const char* szLine)
    {
        static const int DIRECTIVE_NAME_LEN = strlen("ifdef");

        const char* s = szLine;
        s += DIRECTIVE_NAME_LEN;
        s = skip_ws(s);

        std::string token = get_token(s);
        bool bRet = this->find_macro(token.c_str());
        condition_.push_back(true);
        condition_.push_back(bRet);
        return this->write(skip_nl(s));
    }

    int context::eval_directive_ifndef(const char* szLine)
    {
        static const int DIRECTIVE_NAME_LEN = strlen("ifndef");

        const char* s = szLine;
        s += DIRECTIVE_NAME_LEN;
        s = skip_ws(s);

        std::string token = get_token(s);
        bool bRet = !this->find_macro(token.c_str());
        condition_.push_back(true);
        condition_.push_back(bRet);
        return this->write(skip_nl(s));
    }

    int context::eval_directive_pragma(const char* szLine)
    {
        static const int DIRECTIVE_NAME_LEN = strlen("pragma");

        const char* s = szLine;
        s = skip_ws(s);
        assert(*s == '#');
        s++;
        s = skip_ws(s);
        s += DIRECTIVE_NAME_LEN;
        s = skip_ws(s);

        std::string token = get_token(s);
        if (token == "once")
        {
            std::string fpath = fullpath(get_current_input_path());
            includes_[fpath].has_pragma_once = true;
            return this->write(skip_nl(szLine));
        }
        else
        {
            return this->write(szLine);
        }
    }

    int context::eval_directive_version(const char* szLine)
    {
        return this->write(szLine);
    }

    int context::eval_directive_line(const char* szLine)
    {
        static const int DIRECTIVE_NAME_LEN = strlen("line");

        const char* s = szLine;
        s = skip_ws(s);
        assert(*s == '#');
        s++;
        s = skip_ws(s);
        s += DIRECTIVE_NAME_LEN;
        s = skip_ws(s);

        std::string line_total = get_number(s);
        if(line_total.empty())return -1;
        s += line_total.size();
        s = skip_ws(s);
        std::string file_name = get_token(s);

        states_.back().line_total = atoi(line_total.c_str());
        states_.back().line = states_.back().line_total;
        if(!file_name.empty())
        {
            states_.back().file_name = file_name;
        }

        return this->write(szLine);
    }

    int context::eval_directive_error(const char* szLine)
    {
        static const int DIRECTIVE_NAME_LEN = strlen("error");

        const char* s = szLine;
        s += DIRECTIVE_NAME_LEN;
        s = skip_ws(s);

        std::string strMessage = get_token(s);

        return this->print_error(strMessage.c_str());
    }

    int context::eval_directive_warning(const char* szLine)
    {
        static const int DIRECTIVE_NAME_LEN = strlen("warning");

        const char* s = szLine;
        s += DIRECTIVE_NAME_LEN;
        s = skip_ws(s);

        std::string strMessage = get_token(s);

        return this->print_warning(strMessage.c_str());
    }

    int context::eval_directive_import(const char* szLine)
    {
        return this->write(szLine);
    }

    int context::eval_directive_using(const char* szLine)
    {
        return this->write(szLine);
    }

    int context::eval_macro(char* szLine)
    {
        if(this->is_if_true())
        {
            return this->write(this->eval_macro_detail(szLine).c_str());
        }
        else
        {
            return 0;
        }
    }

    static
    inline std::string eval_macro_concat(const std::string& str)
    {
        if(str.size()>=4)
        {
            const char* s = str.c_str();
            std::string buffer;
            char c = 0;
            while((c = *s) != 0)
            {
                if(c == '#' && *(s+1) == '#')
                {
                    if(*(s+2) == '#')return "";

                    while(isspace_(buffer.back()))buffer.pop_back();
                    s+=2;
                    while(isspace_(*s))s++;
                }
                else
                {
                    buffer += c;
                    s++;
                }
            }
            return buffer;
        }
        else
        {
            return str;
        }
    }

    struct SLPP_ARG_RANGE
    {
        std::string name;
        size_t start;
        size_t end;
        bool isstr;
        int index;
    };

    static
    inline int get_arg_ranges(std::vector<SLPP_ARG_RANGE>& ranges, const std::string& body, const std::vector<std::string>& args)
    {
        const char* szLine = body.c_str();
        size_t asz = args.size();
        size_t i = 0;
        bool isstr = false;
        int i_str = i;
        while(*(szLine+i) != 0)
        {
            std::string token = get_token(szLine+i);
            if(token == "#")
            {
                isstr = true;
                i_str = i;
                i++;
                while(isspace_(*(szLine+i)))i++;
            }
            else
            {
                bool bFound = false;
                for(size_t j = 0; j < asz; j++)
                {
                    if(token == args[j])
                    {
                        SLPP_ARG_RANGE r;
                        r.name = token;
                        r.index = j;
                        r.isstr = isstr;
                        r.start = isstr ? i_str : i;
                        r.end = i+token.size();
                        ranges.push_back(r);
                        i += token.size();
                        bFound = true;
                        break;
                    }
                }

                if(!bFound)
                {
                    SLPP_ARG_RANGE r;
                    r.name = "";
                    r.start = i;
                    r.end = i+token.size();
                    ranges.push_back(r);
                    i += (r.end - r.start);
                }
                isstr = false;
            }
        }
        return 0;
    }

    static
    std::string replace_macro_stringize(const std::string& str)
    {
        const char* s = str.c_str();
        std::string buffer;
        buffer.reserve(str.size());
        while(*s != 0)
        {
            if(*s == '\"')
            {
                buffer += '\\';
                buffer += '"';
            }
            else
            {
                buffer += *s;
            }
            s++;
        }
        return buffer;
    }

    static
    std::string replace_macro_body(const macro_def* macro, const std::vector<std::string>& args_new)
    {
        std::string body = macro->body;
        const std::vector<std::string>& args = macro->args;
        const char* szLine = body.c_str();
        std::vector<SLPP_ARG_RANGE> ranges;
        int nRet = get_arg_ranges(ranges, body, args);
        if(nRet != 0)return "";
        size_t sz = ranges.size();
        std::string buffer;
        for(size_t i = 0; i < sz; i++)
        {
            if(ranges[i].name.empty())
            {
                buffer.append(szLine+ranges[i].start, szLine+ranges[i].end);
            }
            else
            {
                if(!ranges[i].isstr)
                {
                    buffer.append(args_new[ranges[i].index]);
                }
                else
                {

                    buffer.append("\"" + replace_macro_stringize(args_new[ranges[i].index]) + "\"");
                }
            }
        }

        return buffer;
    }

    int context::eval_macro_defined(std::string& strOut, const std::string& str)
    {
        const char* szLine = str.c_str();
        std::string buffer;
        size_t i = 0;
        while(*(szLine+i) != 0)
        {
            string_range rtoken = get_token(szLine+i);
            if(rtoken.size() == 7)
            {
                std::string token = rtoken;
                if(token == "defined")
                {
                    const char* s = szLine+i;
                    s += token.size();
                    s = skip_ws(s);
                    if(*s == '(')
                    {
                        s++;
                        if(*s == 0)return -1;
                        s = skip_ws(s);
                        std::string macroname = get_token(s);
                        s += macroname.size();
                        if(*s == 0)return -1;
                        s = skip_ws(s);
                        if(*s != ')')return -1;
                        s++;
                        bool bRet = this->find_macro(macroname.c_str());
                        if (bRet)
                        {
                            buffer += "1";
                        }
                        else
                        {
                            buffer += "0";
                        }
                        i = s - szLine;
                    }
                    else
                    {
                        std::string macroname = get_token(s);
                        s += macroname.size();
                        bool bRet = this->find_macro(macroname.c_str());
                        if (bRet)
                        {
                            buffer += "1";
                        }
                        else
                        {
                            buffer += "0";
                        }
                        i = s - szLine;
                    }
                }
                else
                {
                    size_t start = i;
                    size_t end = i+token.size();
                    buffer.append(szLine+start, szLine+end);
                    i += (end - start);
                }
            }
            else
            {
                size_t start = i;
                size_t end = i+rtoken.size();
                buffer.append(szLine+start, szLine+end);
                i += (end - start);
            }
        }
        strOut.swap(buffer);
        return 0;
    }

    std::string context::eval_macro_detail  (const std::string& str)
    {
        hide_set_type hs;
        return this->eval_macro_detail(str, hs);
    }

    static
    inline std::string ToString(int x)
    {
        static char buffer[64];
        sprintf(buffer, "%d", x);
        return buffer;
    }

    static
    inline bool is_identifier(char c)
    {
        if(isalpha_(c) || c == '_')
        {
            return true;
        }
        return false;
    }

    std::string context::eval_macro_detail(const std::string& strOrg, const hide_set_type& hs)
    {
        typedef macro_def_type::const_iterator const_iterator;
        const macro_def_type& macros = this->macro_defs_;
        std::string str = strOrg;
        const char* szLine = str.c_str();
        std::string buffer;
        buffer.reserve(str.size());
        size_t i = 0;
        while(*(szLine+i) != 0)
        {
            string_range rtoken = get_token(szLine+i);
            if(is_identifier(rtoken[0]))
            {
                std::string token = rtoken;
                const_iterator it;
                if(
                    (it = macros.find(token)) != macros.end()
                    &&
                    (hs.find(token) == hs.end()) 
                )
                {
                    const macro_def* macro = it->second.get();
                    if(macro->type == SLPP_MACRO_VARIABLE || macro->type == SLPP_MACRO_BUILTIN_VARIABLE)
                    {
                        size_t start = i;
                        size_t end = i+token.size();
                        std::string str;
                        this->eval_macro_variable(str, macro, hs);
                        buffer.append(str);
                        i += (end - start);
                    }
                    else if(macro->type == SLPP_MACRO_FUNCTION || macro->type == SLPP_MACRO_BUILTIN_FUNCTION)
                    {
                        const char* s = szLine+i+token.size();
                        s = skip_ws(s);
                        if(*s != '(')
                        {
                            return "";//ERROR
                        }
                        s++;

                        if(*s == 0)
                        {
                            size_t start = i;
                            size_t end = i+token.size();
                            buffer.append(szLine+start, szLine+end);
                            i += (end - start);
                            continue;
                        }

                        const char* argStart = s;
                        int nest = 1;
                        bool nested = false;
                        while(*s != 0 && nest >= 1)
                        {
                            if( *s == '(')
                            {
                                nest++;
                                nested = true;
                            }
                            if( *s == ')')
                            {
                                nest--;
                            }
                            s++;
                        }
                        
                        std::string arg = std::string(argStart, s-1);
                        if(nested)
                        {
                            arg = eval_macro_detail(arg, hs);
                        }

                        std::vector<std::string> args;
                        split_macro_args(args, arg);

                        const char* argEnd = s;
                        if(macro->args.size() == args.size())
                        {
                            size_t start = i;
                            size_t end = argEnd - szLine;
                            std::string str;
                            this->eval_macro_function(str, macro, args, hs);
                            buffer.append(str);
                            i += (end - start);
                        }
                        else
                        {
                            //error
                            size_t start = i;
                            size_t end = i+token.size();
                            std::string str = std::string(szLine+start, szLine+end);
                            buffer.append(str);
                            i += (end - start);
                        }
                    }
                }
                else
                {
                    
                    size_t start = i;
                    size_t end = i+token.size();
                    buffer.append(szLine+start, szLine+end);
                    i += (end - start);
                }
            }
            else
            {
                if(rtoken.size() == 1)
                {
                    if(rtoken[0] == '\n')
                    {
                        states_.back().line++;
                    }
                }
                size_t start = i;
                size_t end = i+rtoken.size();
                buffer.append(szLine+start, szLine+end);
                i += (end - start);
            }
        }
        return buffer;
    }

    int context::eval_macro_variable(std::string& strOut, const macro_def* macro, const hide_set_type& hs)
    {
        if(macro->type == SLPP_MACRO_BUILTIN_VARIABLE)
        {
            if(macro->name == "__LINE__")
            {
                strOut = ToString(states_.back().line);
                return 0;
            }
            else if(macro->name == "__DATE__")
            {
                time_t t = time(NULL);
                struct tm* date = localtime(&t);
                char sbuf[64] = {};
                strftime(sbuf, 63, "\"%b %d %Y\"", date);
                strOut = sbuf;
                return 0;
            }
            else if(macro->name == "__TIME__")
            {
                time_t t = time(NULL);
                struct tm* date = localtime(&t);
                char sbuf[64] = {};
                strftime(sbuf, 63, "\"%H:%M:%S\"", date);
                strOut = sbuf;
                return 0;
            }
            else if(macro->name == "__FILE__")
            {
                strOut = states_.back().file_name;
                return 0;
            }
            strOut = macro->name;
            return 0;
        }
        else
        {
            hide_set_type hs2 = hs;
            hs2.insert(macro->name);
            strOut = eval_macro_detail(eval_macro_concat(macro->body), hs2);
            return 0;
        }
    }

    int context::eval_macro_function(std::string& strOut, const macro_def* macro, const std::vector<std::string>& args, const hide_set_type& hs)
    {
        if(macro->type == SLPP_MACRO_BUILTIN_FUNCTION)
        {
            if(macro->name == "_Pragma" && args.size() == 1)
            {
                std::string str = get_token(args[0].c_str());
                if(str.size() >= 3)
                {
                    strOut = "#pragma " + str.substr(1, str.size()-2);
                    return 0;
                }
            }
            return -1;
        }
        else
        {
            hide_set_type hs2 = hs;
            hs2.insert(macro->name);
            strOut = eval_macro_detail(eval_macro_concat(replace_macro_body(macro, args)), hs2);
            return 0;
        }
    }

    int context::write(const char* szLine)
    {
        return output_streams_.back()->write(szLine);
    }

    int context::print_error  (const char* szMessage)
    {
    #if defined(__APPLE__) || defined(unix)
        fprintf(stderr, "\x1b[1m%s:%d:%d: \x1b[31merror:\x1b[39m %s\x1b[0m\n", get_current_input_path().c_str(), states_.back().line, states_.back().colmn, szMessage);
    #else
        fprintf(stderr, "%s:%d:%d: error: %s\n", get_current_input_path().c_str(), states_.back().line, states_.back().colmn, szMessage);
    #endif
        return 0;
    }
        
    int context::print_warning(const char* szMessage)
    {
    #if defined(__APPLE__) || defined(unix)
        fprintf(stderr, "\x1b[1m%s:%d:%d: \x1b[35mwarning:\x1b[39m %s\x1b[0m\n", get_current_input_path().c_str(), states_.back().line, states_.back().colmn, szMessage);
    #else
        fprintf(stderr, "%s:%d:%d: warning: %s\n", get_current_input_path().c_str(), states_.back().line, states_.back().colmn, szMessage);
    #endif
        return 0;
    }

    std::string context::get_current_input_path()const
    {
        return input_streams_.back()->get_path();
    }

    bool context::is_if_true()const
    {
        const int* conds = &condition_[0];
        size_t sz = condition_.size();
        for(size_t i = 0; i < sz; i++)
        {
            if(!conds[i])return false;
        }
        return true;
    }

    bool context::register_macro(const std::shared_ptr<macro_def>& def)
    {
        typedef macro_def_type::iterator iterator;
        iterator it = macro_defs_.find(def->name);
        if(it != macro_defs_.end())
        {
            it->second = def;
            return false;
        }
        else
        {
            macro_defs_.insert(std::make_pair(def->name,def));
            return true;
        }
    }

    bool context::unregister_macro(const char* name)
    {
        {
            typedef std::map<std::string, include_guard > include_map_type;
            typedef include_map_type::iterator iterator;
            for(iterator it = includes_.begin();it!=includes_.end();++it)
            {
                if(it->second.inc_guard_key == name)
                {
                    it->second.inc_guard_key = "";
                }
            }
        }

        typedef macro_def_type::const_iterator const_iterator;
        const_iterator it = macro_defs_.find(name);
        if(it != macro_defs_.end())
        {
            if(
                it->second->type == SLPP_MACRO_BUILTIN_VARIABLE 
                ||
                it->second->type == SLPP_MACRO_BUILTIN_FUNCTION 
            ) 
            {
                this->print_warning("undefining builtin macro");
            }
            macro_defs_.erase(it);
            return true;
        }
        return false;
    }

    bool context::find_macro(const char* name)
    {
        typedef macro_def_type::const_iterator const_iterator;
        const_iterator it = macro_defs_.find(name);
        return (it != macro_defs_.end());
    }

    bool context::is_already_included(const std::string& str)
    {
        typedef std::map<std::string, include_guard > include_map_type;
        include_map_type::iterator it = includes_.find(str);
        if(it != includes_.end())
        {
            if(it->second.has_pragma_once)
            {
                return true;
            }
            if(!it->second.inc_guard_key.empty())
            {
                return true;
            }
        } 
        return false;
    }
}
