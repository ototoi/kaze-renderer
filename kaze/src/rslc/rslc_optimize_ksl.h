#ifndef RSLC_OPTIMIZE_KSL_H
#define RSLC_OPTIMIZE_KSL_H

namespace rslc
{
    class rslc_context;
    class rslc_node;
    int rslc_optimize_ksl(rslc_context* ctx, rslc_node* node);
}

#endif
