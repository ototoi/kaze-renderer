#include "rslc_symbol_manager.h"

#include <iostream>

namespace rslc
{
    bool rslc_symbol_table::add_symbol(const std::shared_ptr<rslc_symbol>& s)
    {
        sm_[s->get_name()].push_back(s);
        return true;
    }

    void rslc_symbol_table::debug_print()const
    {
        typedef rslc_symbol_table::map_type map_type;
        for(map_type::const_iterator it = sm_.begin(); it != sm_.end(); it++)
        {
            std::cout << it->first << ":" << it->second[0]->get_symbol_type() << std::endl;
        }
    }

    bool rslc_symbol_table::find_types    (std::vector<const rslc_symbol*>& symbols, const std::string& name)const
    {
        typedef rslc_symbol_table::map_type map_type;
        map_type::const_iterator it = sm_.find(name);
        if(it != sm_.end())
        {
            bool bRet = false;
            const std::vector< std::shared_ptr<rslc_symbol> >& v = it->second;
            size_t sz = v.size();
            for(size_t i = 0;i<sz;i++)
            {
                if(v[i]->get_symbol_type() == rslc_symbol::RSLC_SYMBOL_TYPE)
                {
                    symbols.push_back(v[i].get());
                    bRet = true;
                }
            }
            return bRet;
        }
        if(parent_)
        {
            return parent_->find_types(symbols, name);
        }
        return false;
    }

    bool rslc_symbol_table::find_variables(std::vector<const rslc_symbol*>& symbols, const std::string& name)const
    {
        typedef rslc_symbol_table::map_type map_type;
        map_type::const_iterator it = sm_.find(name);
        if(it != sm_.end())
        {
            bool bRet = false;
            const std::vector< std::shared_ptr<rslc_symbol> >& v = it->second;
            size_t sz = v.size();
            for(size_t i = 0;i<sz;i++)
            {
                if(v[i]->get_symbol_type() == rslc_symbol::RSLC_SYMBOL_VARIABLE)
                {
                    symbols.push_back(v[i].get());
                    bRet = true;
                }
            }
            return bRet;
        }
        if(parent_)
        {
            return parent_->find_variables(symbols, name);
        }
        return false;
    }

    bool rslc_symbol_table::find_functions(std::vector<const rslc_symbol*>& symbols, const std::string& name)const
    {
        typedef rslc_symbol_table::map_type map_type;
        map_type::const_iterator it = sm_.find(name);
        if(it != sm_.end())
        {
            bool bRet = false;
            const std::vector< std::shared_ptr<rslc_symbol> >& v = it->second;
            size_t sz = v.size();

            for(size_t i = 0;i<sz;i++)
            {
                if(v[i]->get_symbol_type() == rslc_symbol::RSLC_SYMBOL_FUNCTION)
                {
                    symbols.push_back(v[i].get());
                    bRet = true;
                }
            }
            return bRet;
        }
        if(parent_)
        {
            return parent_->find_functions(symbols, name);
        }
        return false;
    }

    //--------------------------------------------------------

    rslc_symbol_manager::rslc_symbol_manager()
    {
        std::shared_ptr<rslc_symbol_table> t( new rslc_symbol_table() );
        objects_.push_back(t);
        tables_.push_back(t.get());
    }

    rslc_symbol_manager::~rslc_symbol_manager()
    {
        ;
    }

    bool rslc_symbol_manager::push_symbol_table()
    {
        std::shared_ptr<rslc_symbol_table> t( new rslc_symbol_table(this->top_symbol_table()) );
        objects_.push_back(t);
        tables_.push_back(t.get());
        return true;
    }

    bool rslc_symbol_manager::pop_symbol_table()
    {
        if(tables_.size() < 2)
        {
            return false;
        }
        else
        {
            tables_.pop_back();
            return true;
        }
    }

    rslc_symbol_table* rslc_symbol_manager::top_symbol_table()const
    {
        if(tables_.size())
        {
            return tables_.back();
        }
        else
        {
            return NULL;
        }
    }

    bool rslc_symbol_manager::add_symbol(const std::shared_ptr<rslc_symbol>& s)
    {
        rslc_symbol_table* t = this->top_symbol_table();
        if(t)
        {
            return t->add_symbol(s);
        }
        else
        {
            return false;
        }
    }

    bool rslc_symbol_manager::find_types    (std::vector<const rslc_symbol*>& symbols, const std::string& name)const
    {
        rslc_symbol_table* t = this->top_symbol_table();
        if(t)
        {
            return t->find_types(symbols, name);
        }
        else
        {
            return false;
        }
    }

    bool rslc_symbol_manager::find_variables(std::vector<const rslc_symbol*>& symbols, const std::string& name)const
    {
        rslc_symbol_table* t = this->top_symbol_table();
        if(t)
        {
            return t->find_variables(symbols, name);
        }
        else
        {
            return false;
        }
    }

    bool rslc_symbol_manager::find_functions(std::vector<const rslc_symbol*>& symbols, const std::string& name)const
    {
        rslc_symbol_table* t = this->top_symbol_table();
        if(t)
        {
            return t->find_functions(symbols, name);
        }
        else
        {
            return false;
        }
    }

    void rslc_symbol_manager::clear_symbol()
    {
        tables_.clear();
        objects_.clear();
        
        std::shared_ptr<rslc_symbol_table> t( new rslc_symbol_table() );
        objects_.push_back(t);
        tables_.push_back(t.get());
    }

    void rslc_symbol_manager::debug_print()const
    {
        for(size_t i=0;i<objects_.size();i++)
        {
            objects_[i]->debug_print();
        }
    }
   
}
