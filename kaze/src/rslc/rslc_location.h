#ifndef RSLC_LOCATION_H
#define RSLC_LOCATION_H

#include <string>

namespace rslc
{
    class rslc_location
    {
    public:
        rslc_location()
        {
            line_ = 0;
            column_ = 0;
        }
        rslc_location(const std::string& file, int line, int column)
            :file_(file), line_(line), column_(column)
        {
            ;
        }
        const std::string& get_filename()const{return file_;}
        int get_line()const{return line_;}
        int get_column()const{return column_;}
    private:
        std::string file_;
        int line_;
        int column_;
    };
}

#endif