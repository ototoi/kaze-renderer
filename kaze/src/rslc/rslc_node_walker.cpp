#include "rslc_node_walker.h"
#include "rslc_node.h"
#include "rslc_context.h"

#include <vector>
#include <memory>
#include <cassert>
#include <iostream>

#define CAST(T, n) check_cast<T>(n)

namespace rslc
{
    template<class T, class B>
    T check_cast(B b)
    {
        T t = dynamic_cast<T>(b);
        assert(t);
        return t;
    }

    int rslc_node_walker::walk(rslc_node* node)
    {
        return this->walk_(node);
    }

    int rslc_node_walker::walk_(rslc_node* node)
    {
        if(node)
        {
            int nType = node->get_node_type();
            switch(nType)
            {
                case RSLC_NODE_IDENTIFIER:
                    return this->walk(CAST(rslc_identifier_node*,node));
                case RSLC_NODE_INTEGER:
                    return this->walk(CAST(rslc_integer_node*,node));
                case RSLC_NODE_FLOAT:
                    return this->walk(CAST(rslc_float_node*,node));
                case RSLC_NODE_STRING:
                    return this->walk(CAST(rslc_string_node*,node));
                case RSLC_NODE_TEXTURENAME:
                    return this->walk(CAST(rslc_texturename_node*,node));

                case RSLC_NODE_LIST:
                    return this->walk(CAST(rslc_list_node*,node));
                case RSLC_NODE_TUPLE:
                    return this->walk(CAST(rslc_tuple_node*,node));
                
                case RSLC_NODE_TYPE:
                    return this->walk(CAST(rslc_type_node*,node));
                case RSLC_NODE_VARIABLE_REF:
                    return this->walk(CAST(rslc_variable_ref_node*,node));
                case RSLC_NODE_VARIABLE_ARRAY_REF:
                    return this->walk(CAST(rslc_variable_array_ref_node*,node));
                case RSLC_NODE_FUNCTION_CALL:
                    return this->walk(CAST(rslc_function_call_node*,node));
                
                case RSLC_NODE_NEG:
                    return this->walk(CAST(rslc_neg_node*,node));
                case RSLC_NODE_NOT:
                    return this->walk(CAST(rslc_not_node*,node));
                case RSLC_NODE_ADD:
                    return this->walk(CAST(rslc_add_node*,node));
                case RSLC_NODE_SUB:
                    return this->walk(CAST(rslc_sub_node*,node));
                case RSLC_NODE_MUL:
                    return this->walk(CAST(rslc_mul_node*,node));
                case RSLC_NODE_DIV:
                    return this->walk(CAST(rslc_div_node*,node));
                case RSLC_NODE_CRS:
                    return this->walk(CAST(rslc_crs_node*,node));
                case RSLC_NODE_DOT:
                    return this->walk(CAST(rslc_dot_node*,node));

                case RSLC_NODE_LSS:
                    return this->walk(CAST(rslc_lss_node*,node));
                case RSLC_NODE_LEQ:
                    return this->walk(CAST(rslc_leq_node*,node));
                case RSLC_NODE_GTR:
                    return this->walk(CAST(rslc_gtr_node*,node));
                case RSLC_NODE_GEQ:
                    return this->walk(CAST(rslc_geq_node*,node));
                case RSLC_NODE_EQ:
                    return this->walk(CAST(rslc_eq_node*,node));
                case RSLC_NODE_NE:
                    return this->walk(CAST(rslc_ne_node*,node));

                case RSLC_NODE_AND:
                    return this->walk(CAST(rslc_and_node*,node));
                case RSLC_NODE_OR:
                    return this->walk(CAST(rslc_or_node*,node));

                case RSLC_NODE_ASSIGN:
                    return this->walk(CAST(rslc_assign_node*,node));
                case RSLC_NODE_ADD_ASSIGN:
                    return this->walk(CAST(rslc_add_assign_node*,node));
                case RSLC_NODE_SUB_ASSIGN:
                    return this->walk(CAST(rslc_sub_assign_node*,node));
                case RSLC_NODE_MUL_ASSIGN:
                    return this->walk(CAST(rslc_mul_assign_node*,node));
                case RSLC_NODE_DIV_ASSIGN:
                    return this->walk(CAST(rslc_div_assign_node*,node));

                case RSLC_NODE_COND:
                    return this->walk(CAST(rslc_cond_node*,node));
                case RSLC_NODE_CAST:
                    return this->walk(CAST(rslc_cast_node*,node));

                case RSLC_NODE_STATEMENT:
                    return this->walk(CAST(rslc_statement_node*,node));

                case RSLC_NODE_BLOCK:
                    return this->walk(CAST(rslc_block_node*,node));

                case RSLC_NODE_WHILE:
                    return this->walk(CAST(rslc_while_node*,node));
                case RSLC_NODE_FOR:
                    return this->walk(CAST(rslc_for_node*,node));
                case RSLC_NODE_SOLAR:
                    return this->walk(CAST(rslc_solar_node*,node));
                case RSLC_NODE_ILLUMINATE:
                    return this->walk(CAST(rslc_illuminate_node*,node));
                case RSLC_NODE_ILLUMINANCE:
                    return this->walk(CAST(rslc_illuminance_node*,node));
                case RSLC_NODE_GATHER:
                    return this->walk(CAST(rslc_gather_node*,node));

                case RSLC_NODE_IF:
                    return this->walk(CAST(rslc_if_node*,node));

                case RSLC_NODE_RETURN:
                    return this->walk(CAST(rslc_return_node*,node));
                case RSLC_NODE_BREAK:
                    return this->walk(CAST(rslc_break_node*,node));
                case RSLC_NODE_CONTINUE:
                    return this->walk(CAST(rslc_continue_node*,node));
        
                case RSLC_NODE_VARIABLE_DEF:
                    return this->walk(CAST(rslc_variable_def_node*,node));
                case RSLC_NODE_VARIABLE_ARRAY_DEF:
                    return this->walk(CAST(rslc_variable_array_def_node*,node));
                case RSLC_NODE_TYPE_VARIABLES_DEF:
                    return this->walk(CAST(rslc_type_variables_def_node*,node));

                case RSLC_NODE_FUNCTION_DEF:
                    return this->walk(CAST(rslc_function_def_node*,node));
                case RSLC_NODE_SHADER_DEF:
                    return this->walk(CAST(rslc_shader_def_node*,node));

                case RSLC_NODE_DEFINITIONS:
                    return this->walk(CAST(rslc_definitions_node*,node));

                case RSLC_NODE_PAREN:
                    return this->walk(CAST(rslc_paren_node*,node));
                default:
                    return 0; 
            }
        }
        return 0; 
    }

    int rslc_node_walker::walk(rslc_identifier_node* node)
    { 
        return 0; 
    }
    int rslc_node_walker::walk(rslc_integer_node* node)
    { 
        return 0; 
    }
    int rslc_node_walker::walk(rslc_float_node* node)
    { 
        return 0; 
    }
    int rslc_node_walker::walk(rslc_string_node* node)
    { 
        return 0; 
    }
    int rslc_node_walker::walk(rslc_texturename_node* node)
    { 
        return 0; 
    }
    int rslc_node_walker::walk(rslc_type_node* node)
    { 
        return 0; 
    }
    int rslc_node_walker::walk(rslc_variable_ref_node* node)
    { 
        return 0; 
    }
    int rslc_node_walker::walk(rslc_variable_array_ref_node* node)
    { 
        {
            int nRet = this->walk_(node->get_index_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_function_call_node* node)
    { 
        {
            int nRet = this->walk_(node->get_arguments_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_not_node* node)
    { 
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_neg_node* node)
    { 
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_add_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_sub_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_mul_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_div_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_crs_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_dot_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_lss_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_leq_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_gtr_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_geq_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_eq_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_ne_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_and_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_or_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_assign_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_add_assign_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_sub_assign_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_mul_assign_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_div_assign_node* node)
    { 
        {
            int nRet = this->walk_(node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }

    int rslc_node_walker::walk(rslc_cond_node* node)
    { 
        {
            int nRet = this->walk_(node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_expression1_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_expression2_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }

    int rslc_node_walker::walk(rslc_cast_node* node)
    { 
        {
            int nRet = this->walk_(node->get_type_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_expression_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_list_node* node)
    { 
        size_t sz = node->get_nodes_size();
        for(size_t i = 0; i < sz; i++)
        {
            int nRet = this->walk_(node->get_node_at(i));
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_tuple_node* node)
    { 
        size_t sz = node->get_nodes_size();
        for(size_t i = 0; i < sz; i++)
        {
            int nRet = this->walk_(node->get_node_at(i));
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_block_node* node)
    { 
        size_t sz = node->get_nodes_size();
        for(size_t i = 0; i < sz; i++)
        {
            int nRet = this->walk_(node->get_node_at(i));
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_definitions_node* node)
    { 
        size_t sz = node->get_nodes_size();
        for(size_t i = 0; i < sz; i++)
        {
            int nRet = this->walk_(node->get_node_at(i));
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_paren_node* node)
    { 
        {
            int nRet = this->walk_(node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_statement_node* node)
    { 
        {
            int nRet = this->walk_(node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }

    int rslc_node_walker::walk(rslc_while_node* node)
    { 
        {
            int nRet = this->walk_(node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_for_node* node)
    { 
        {
            int nRet = this->walk_(node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_solar_node* node)
    { 
        {
            int nRet = this->walk_(node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_illuminate_node* node)
    { 
        {
            int nRet = this->walk_(node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_illuminance_node* node)
    { 
        {
            int nRet = this->walk_(node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_gather_node* node)
    { 
        {
            int nRet = this->walk_(node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_if_node* node)
    { 
        {
            int nRet = this->walk_(node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_else_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }

    int rslc_node_walker::walk(rslc_return_node* node)
    { 
        {
            int nRet = this->walk_(node->get_expression_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_break_node* node)
    { 
        return 0; 
    }
    int rslc_node_walker::walk(rslc_continue_node* node)
    {
        return 0; 
    }

    int rslc_node_walker::walk(rslc_function_def_node* node)
    { 
        {
            int nRet = this->walk_(node->get_arguments_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_shader_def_node* node)
    { 
        {
            int nRet = this->walk_(node->get_arguments_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_variable_def_node* node)
    {
        {
            int nRet = this->walk_(node->get_definition_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_variable_array_def_node* node)
    {
        {
            int nRet = this->walk_(node->get_definition_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
    int rslc_node_walker::walk(rslc_type_variables_def_node* node)
    {
        {
            int nRet = this->walk_(node->get_type_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = this->walk_(node->get_variables_node());
            if(nRet != 0)return nRet;
        }
        return 0; 
    }
}

