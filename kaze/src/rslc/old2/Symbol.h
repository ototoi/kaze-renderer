#ifndef SLC_SYMBOL_H
#define SLC_SYMBOL_H

#include <string>
#include "Type.h"
#include "Token.h"

namespace slc
{

    class Symbol
    {
    public:
        Symbol(const Type& type, const std::string& name);
        virtual ~Symbol();
        const Type& GetType() const;
        const std::string& GetName() const;

    protected:
        Type type_;
        std::string name_;
    };
}

#endif