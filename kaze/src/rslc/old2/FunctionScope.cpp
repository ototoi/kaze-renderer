#include "FunctionScope.h"

namespace slc
{

    FunctionScope::FunctionScope(const shared_ptr<Scope>& parent, const Symbol& s)
        : Scope(parent), s_(s)
    {
    }

    void FunctionScope::AddVariableSymbol(const Symbol& s) const
    {
        vals_->Add(s);
    }

    const std::shared_ptr<SymbolTable>& FunctionScope::GetVariableSymbolTables() const;
    {
        return vals_;
    }
}