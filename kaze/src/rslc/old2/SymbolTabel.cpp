

namespace slc
{
    typedef std::map<std::string, Symbol> MapType;

    bool SymbolTable::IsContain(const std::string& s) const
    {
        MapType::const_iterator it = tbl_.find(s);
        if (it != tbl_.end())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    std::shared_ptr<Symbol> SymbolTable::Find(const std::string& s) const
    {
        MapType::const_iterator it = tbl_.find(s);
        if (it != tbl_.end())
        {
            return it->second();
        }
        else
        {
            return std::shared_ptr<Symbol>();
        }
    }

    void SymbolTable::Add(const std::shared_ptr<Symbol>& s)
    {
        tbl_.insert(MapType::value_type(s->GetName(), s));
    }
}