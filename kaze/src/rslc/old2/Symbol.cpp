
namespace slc
{

    Symbol::Symbol(const Type& type, const std::string& name)
        : type_(type), name_(name)
    {
    }

    Symbol::~Symbol()
    {
    }

    const Type& Symbol::GetType() const
    {
        return type_;
    }

    const std::string& Symbol::GetName() const
    {
        return name_;
    }
}