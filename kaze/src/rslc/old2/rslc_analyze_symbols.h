#ifndef RSLC_ANALYZE_SYMBOLS_H
#define RSLC_ANALYZE_SYMBOLS_H 

namespace rslc
{
    class rslc_context;
    class rslc_node;

    int rslc_analyze_symbols(rslc_context* ctx, const rslc_node* node);
}

#endif
