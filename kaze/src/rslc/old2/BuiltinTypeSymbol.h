#ifndef SCL_BUILTIN_TYPE_SYMBOL_H
#define SCL_BUILTIN_TYPE_SYMBOL_H

#include "BuiltinTypeSymbol.h"

namespace slc
{

    class BuiltinTypeSymbol : public Symbol
    {
    public:
        BuiltinTypeSymbol(const std::string& s)
            : Symbol(Type("__def"), s)
        {
        }
    };
}

#endif