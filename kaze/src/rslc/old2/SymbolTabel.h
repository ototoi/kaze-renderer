#ifndef SLC_STMBOL_TABLE_H
#define SLC_STMBOL_TABLE_H

#include "Symbol.h"
#include <map>

namespace slc
{

    class SymbolTable
    {
    public:
        bool IsContain(const std::string& s) const;
        std::shared_ptr<Symbol> Find(const std::string& s) const;
        void Add(const std::shared_ptr<Symbol>& s);

    private:
        std::map<std::string, std::shared_ptr<Symbol> > tbl_;
    };
}

#endif