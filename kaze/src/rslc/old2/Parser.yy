%skeleton "lalr1.cc"
%define "parser_class_name" "Parser"
%defines

%{
#ifdef _WIN32
#pragma warning(disable : 4786)
#pragma warning(disable : 4102)
#pragma warning(disable : 4996)
#pragma warning(disable : 4065)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define YYDEBUG 1
#define YYERROR_VERBOSE 1

#include <algorithm>
#include <iterator>
#include <memory>

#include "Context.h"
#include "InputDecoder.h"
#include "ParseParam.h"
#include "Node.h"

%}

%union{
	int itype;
	float ftype;
	char ctype;
	char* stype;
	slc::Node* ntype;
}

%pure_parser

// The parsing context.
%parse-param { void * scanner }
%lex-param   { void * scanner }

%locations
// %debug
%error-verbose

%{

#define yyerror(m) error(yylloc,m)

using namespace slc;

#define GET_CTX() (((ParseParam*)(scanner))->ctx)
#define GET_FNAME() (((ParseParam*)(scanner))->filename)
#define GET_LINE() (((ParseParam*)(scanner))->line_number)
#define PRINT(s) printf(s)

static
char* dups_(const char* s)
{
	int l = strlen(s);
	char* sRet = new char[l+1];
	strcpy(sRet, s);
	return sRet;
}

typedef kaze::ri::Node* PNODE;
typedef char* LPCSTR;

static
void release_value(LPCSTR v){delete[] v;v=NULL;}
//static
//void release_value(PNODE& v){delete v;v=NULL;}



#define	YY_DECL											\
	int						\
	yylex(yy::Parser::semantic_type* yylval,		\
		 yy::Parser::location_type* yylloc,		\
		 void* scanner)

YY_DECL;


%}

/*tokens*/

%token <stype>	IDENTIFIER

/*types*/
%token <ctype> TOKEN_FLOAT
%token <ctype> TOKEN_COLOR
%token <ctype> TOKEN_POINT
%token <ctype> TOKEN_VECTOR
%token <ctype> TOKEN_NORMAL
%token <ctype> TOKEN_MATRIX
%token <ctype> TOKEN_STRING
%token <ctype> TOKEN_VOID

/*strage class*/
%token <ctype> TOKEN_UNIFORM
%token <ctype> TOKEN_VARYING

%token <ctype> TOKEN_OUTPUT
%token <ctype> TOKEN_EXTERN

/*shader type*/
%token <ctype> TOKEN_SURFACE
%token <ctype> TOKEN_LIGHT
%token <ctype> TOKEN_ATMOSPHERE
%token <ctype> TOKEN_VOLUME
%token <ctype> TOKEN_DISPLACEMENT
%token <ctype> TOKEN_IMAGER
%token <ctype> TOKEN_TRANSFORMATION

/*block statement constructs*/
%token <ctype> TOKEN_IF
%token <ctype> TOKEN_ELSE
%token <ctype> TOKEN_WHILE
%token <ctype> TOKEN_FOR
%token <ctype> TOKEN_CONTINUE
%token <ctype> TOKEN_BREAK
%token <ctype> TOKEN_RETURN

%token <ctype> TOKEN_ILLUMINATE
%token <ctype> TOKEN_ILLUMINANCE
%token <ctype> TOKEN_SOLAR
%token <ctype> TOKEN_GATHER
%token <ctype> TOKEN_OCCLUSION

%token <ctype> TOKEN_LIGHTSOURCE
%token <ctype> TOKEN_INCIDENT
%token <ctype> TOKEN_OPPOSITE
%token <ctype> TOKEN_ATTRIBUTE
%token <ctype> TOKEN_OPTION
%token <ctype> TOKEN_RENDERERINFO


/* NOTE: These are priorities in ascending precedence, operators on the same line have the same precedence. */
%right <ctype> '='
%right <ctype> TOKEN_ADD_ASSIGN
%right <ctype> TOKEN_SUB_ASSIGN
%right <ctype> TOKEN_MUL_ASSIGN
%right <ctype> TOKEN_DIV_ASSIGN

%left <ctype> TOKEN_OR
%left <ctype> TOKEN_AND

%left <ctype>	'<'
%left <ctype>	'>'
%left <ctype>	TOKEN_LE
%left <ctype>	TOKEN_GE
%left <ctype>	TOKEN_EQ
%left <ctype>	TOKEN_NE

%right <ctype>	'?' ':'

%left <ctype>	'+' '-'
%left <ctype>	'^'
%left <ctype>	'/' '*'
%left <ctype>	'.'
%right <ctype>	'!' NEG
%left <ctype>	'(' ')'

%token <ftype>	FLOAT_LITERAL
%token <itype>	INTEGER_LITERAL
%token <stype>	STRING_LITERAL

%type  <ntype>	definitions
%type  <ntype>	definition
%type  <ntype>	shader_definition
%type  <ntype>	function_definition
%type  <ntype>	shader_type
%type  <ntype>	function_type
%type  <ntype>	formals
%type  <ntype>	variable_definitions
%type  <ntype>	formal_variable_definitions
%type  <ntype>	typespec
%type  <ntype>	def_expressions
%type  <ntype>	def_expression
%type  <ntype>	def_init
%type  <ntype>	array_initialisers
%type  <ntype>	def_array_initialisers
%type  <ntype>	type
%type  <ntype>	statements
%type  <ntype>	statement

%type  <ntype>	function_call_statement
%type  <ntype>	assign_statement
%type  <ntype>	definition_statement
%type  <ntype>	return_statement
%type  <ntype>	block_statement
%type  <ntype>	if_statement
%type  <ntype>	loop_statement
%type  <ntype>	while_statement
%type  <ntype>	for_statement
%type  <ntype>	solar_statement
%type  <ntype>	illuminate_statement
%type  <ntype>	illuminance_statement
%type  <ntype>  gather_statement
%type  <ntype>	loop_mod_statement


%type  <ntype>	expression
%type  <ntype>	primary
%type  <ntype>	triple
%type  <ntype>	sixteentuple
%type  <ntype>	relation
%type  <ntype>	assign_expression
%type  <ntype>	cast_expression
%type  <ntype>  function_call
%type  <stype>  function_name
%type  <ntype>	function_arguments

%type  <ntype>	texture_argument

%type  <ftype>	number
%type  <itype>	channel

%start file
%%

file
	: definitions
		{
			if($1 != NULL)
			{
				$1 = NULL;
			}
		}

definitions
	: definition
		{
            std::shared_ptr<Node> pNode( new Node("definitions") );
            pNode->Addhild(GET_CTX()->GetNode($1));
            GET_CTX()->AddNode(pNode);
			$$ = pNode.get();
            $1 = NULL;
		}
	| definitions definition
		{
            std::shared_ptr<Node> pNode = GET_CTX()->GetNode($1);
			pNode->Addhild(GET_CTX()->GetNode($2));
			//GET_CTX()->AddNode(pNode);
			$$ = pNode.get();
            $1 = NULL;
			$2 = NULL;
		}
	;

definition
	:	shader_definition
		{
			$$ = $1;
			$1 = NULL;
		}
	|	function_definition
		{
			$$ = $1;
			$1 = NULL;
		}
	;

shader_definition
	:	shader_type IDENTIFIER '(' formals ')' '{' statements '}'
			{
				std::shared_ptr<Node> pNode( new Node("shader_definition") );
				pNode->SetAttribute("name", $2);
				pNode->AddChild(GET_CTX()->GetNode($1));
				pNode->AddChild(GET_CTX()->GetNode($4));
				pNode->AddChild(GET_CTX()->GetNode($7));
				
				GET_CTX()->AddNode(pNode);
				
				$$ = pNode.get();
				$1 = NULL;
				release_value($2);
				$4 = NULL;
				$7 = NULL;
			}
	|	shader_type IDENTIFIER '(' ')' '{' statements '}'
			{
				std::shared_ptr<Node> pNode( new Node("shader_definition") );
				std::shared_ptr<Node> pFormals( new Node("formals")  );
				pNode->SetAttribute("name", $2);
				pNode->AddChild(GET_CTX()->GetNode($1));
				pNode->AddChild(pFormals);
				pNode->AddChild(GET_CTX()->GetNode($6));
				
				GET_CTX()->AddNode(pNode);
				GET_CTX()->AddNode(pFormals);
				
				$$ = pNode.get();
				$1 = NULL;
				release_value($2);
				$6 = NULL;
			}
	;

function_definition
	:	function_type IDENTIFIER '(' formals ')' '{' statements '}'
							{
								std::shared_ptr<Node> pNode( new Node("function_definition") );
                                pNode->SetAttribute("name", $2);
                                pNode->AddChild(GET_CTX()->GetNode($1));
                                pNode->AddChild(GET_CTX()->GetNode($4));
                                pNode->AddChild(GET_CTX()->GetNode($7));
                                
                                GET_CTX()->AddNode(pNode);
                                
                                $$ = pNode.get();
								$1 = NULL;
								release_value($2);
								$4 = NULL;
								$7 = NULL;
							}
	|	function_type IDENTIFIER '(' ')' '{' statements '}'
							{
								std::shared_ptr<Node> pNode( new Node("function_definition") );
                                std::shared_ptr<Node> pFormals( new Node("formals")  );
								pNode->SetAttribute("name", $2);
                                pNode->AddChild(GET_CTX()->GetNode($1));
                                pNode->AddChild(pFormals);
                                pNode->AddChild(GET_CTX()->GetNode($6));
                                
                                GET_CTX()->AddNode(pNode);
                                GET_CTX()->AddNode(pFormals);
                                
                                $$ = pNode.get();
								$1 = NULL;
								release_value($2);
								$6 = NULL;
							}
	;

shader_type
	:	TOKEN_LIGHT
						{
                            std::shared_ptr<Node> pNode( new Node("shader_type") );
                            pNode->SetAttribute("type", "light");
                            
                            GET_CTX()->AddNode(pNode);
                            
							$$ = pNode.get();
							$1 = 0;
						}
	|	TOKEN_SURFACE
						{
							std::shared_ptr<Node> pNode( new Node("shader_type") );
                            pNode->SetAttribute("type", "surface");
                            
                            GET_CTX()->AddNode(pNode);
                            
							$$ = pNode.get();
							$1 = 0;
						}
	|	TOKEN_VOLUME	{
							std::shared_ptr<Node> pNode( new Node("shader_type") );
                            pNode->SetAttribute("type", "volume");
                            
                            GET_CTX()->AddNode(pNode);
                            
							$$ = pNode.get();
							$1 = 0;
						}
	|	TOKEN_DISPLACEMENT
						{
                            std::shared_ptr<Node> pNode( new Node("shader_type") );
                            pNode->SetAttribute("type", "displacement");
                            
                            GET_CTX()->AddNode(pNode);
                            
							$$ = pNode.get();
							$1 = 0;
						}
	|	TOKEN_TRANSFORMATION
						{
                            std::shared_ptr<Node> pNode( new Node("shader_type") );
                            pNode->SetAttribute("type", "transformation");
                            
                            GET_CTX()->AddNode(pNode);
                            
							$$ = pNode.get();
							$1 = 0;
						}
	|	TOKEN_IMAGER	{
                            std::shared_ptr<Node> pNode( new Node("shader_type") );
                            pNode->SetAttribute("type", "imager");
                            
                            GET_CTX()->AddNode(pNode);
                            
							$$ = pNode.get();
							$1 = 0;
						}
	;

formals
	:	formal_variable_definitions
						{
                            std::shared_ptr<Node> pNode( new Node("formals") );
                            pNode->AddChild(GET_CTX()->GetNode($1));
							
                            GET_CTX()->AddNode(pNode);
                            
                            $$ = pNode.get();
							$1 = NULL;
						}
	|	formals ';' formal_variable_definitions
						{
                            std::shared_ptr<Node> pNode = GET_CTX()->GetNode($1);
			                pNode->AddChild(GET_CTX()->GetNode($3));
                            
							$$ = pNode.get();
							$1 = NULL;
							$3 = NULL;
						}
	|	formals ';'
						{
                            std::shared_ptr<Node> pNode = GET_CTX()->GetNode($1);
                            
                            $$ = pNode.get();
							$1 = NULL;
						}
	;

formal_variable_definitions
	:	typespec def_expressions
							{
								std::shared_ptr<Node> pNode( new Node("variable_definitions") );
                                pNode->AddChild(GET_CTX()->GetNode($1));
                                pNode->AddChild(GET_CTX()->GetNode($3));
                                
                                GET_CTX()->AddNode(pNode);
                            
								$$ = pNode.get();
								$1 = NULL;
								$2 = NULL;
							}
	|	TOKEN_OUTPUT typespec def_expressions
							{
								$2->SetAttribute("extern", "out");
								
                                std::shared_ptr<Node> pNode( new Node("variable_definitions") );
                                pNode->AddChild(GET_CTX()->GetNode($2));
                                pNode->AddChild(GET_CTX()->GetNode($3));
                                
                                GET_CTX()->AddNode(pNode);
                            
								$$ = pNode.get();
                                $1 = 0;
								$2 = NULL;
								$3 = NULL;
							}
	;


variable_definitions
	:	typespec def_expressions
							{
                                std::shared_ptr<Node> pNode( new Node("variable_definitions") );
                                pNode->AddChild(GET_CTX()->GetNode($1));
                                pNode->AddChild(GET_CTX()->GetNode($3));
                                
                                GET_CTX()->AddNode(pNode);
                            
								$$ = pNode.get();
								$1 = NULL;
								$2 = NULL;
							}
	|	TOKEN_EXTERN typespec def_expressions
							{
								$2->SetAttribute("extern", "extern");
								
                                std::shared_ptr<Node> pNode( new Node("variable_definitions") );
                                pNode->AddChild(GET_CTX()->GetNode($2));
                                pNode->AddChild(GET_CTX()->GetNode($3));
                                
                                GET_CTX()->AddNode(pNode);
                            
								$$ = pNode.get();
                                $1 = 0;
								$2 = NULL;
								$3 = NULL;
							}
	;

typespec
	:	TOKEN_UNIFORM type
							{
								$2->SetAttribute("storage", "uniform");
                                
                                $$ = $2;
								$1 = 0;
								$2 = NULL;
							}
    |	TOKEN_VARYING type
							{
								$2->SetAttribute("storage", "varying");
                                
                                $$ = $2;
								$1 = 0;
								$2 = NULL;
							}
	|	type
							{
								$$ = $1;
								$1 = NULL;
							}
	;

function_type
	:	type
							{
								$$ = $1;
								$1 = NULL;
							}
	;

def_expressions
	:	def_expression		{
								;
							}
	|	def_expressions ',' def_expression
							{
								;
							}
	;

def_expression
    :	IDENTIFIER
							{
								std::shared_ptr<Node> pNode( new Node("def") );
                                std::shared_ptr<Node> pVal ( new Node("valref") );
                                
                                pVal->SetAttribute("name", $1);
                                
                                pNode->AddChild(pVal);
                                
                                GET_CTX()->AddNode(pNode);
                                GET_CTX()->AddNode(pVal);
                                
								$$ = pNode.get();
								release_value($1);
							}
	|	IDENTIFIER def_init
							{
                                std::shared_ptr<Node> pNode( new Node("def") );
                                std::shared_ptr<Node> pVal ( new Node("valref") );
                                
                                pVal->SetAttribute("name", $1);
                                
                                pNode->AddChild(pVal);
                                pNode->AddChild(GET_CTX()->GetNode($2));
                                
                                GET_CTX()->AddNode(pNode);
                                GET_CTX()->AddNode(pVal);
                                
								$$ = pNode.get();
								release_value($1);
								$2 = NULL;
							}
	|	IDENTIFIER '[' number ']'
							{
								std::shared_ptr<Node> pNode( new Node("def") );
                                std::shared_ptr<Node> pVal ( new Node("arrayref") );
                                
                                pVal->SetAttribute("name", $1);
                                pVal->SetAttribute("size", $3);
                                
                                pNode->AddChild(pVal);
                                
                                GET_CTX()->AddNode(pNode);
                                GET_CTX()->AddNode(pVal);
                                
								$$ = pNode.get();
								release_value($1);
                                $3 = 0;
							}
	|	IDENTIFIER '[' number ']' def_array_initialisers
							{
								std::shared_ptr<Node> pNode( new Node("def") );
                                std::shared_ptr<Node> pVal ( new Node("arrayref") );
                                
                                pVal->SetAttribute("name", $1);
                                pVal->SetAttribute("size", $3);
                                
                                pNode->AddChild(pVal);
                                pNode->AddChild(GET_CTX()->GetNode($5));
                                
                                GET_CTX()->AddNode(pNode);
                                GET_CTX()->AddNode(pVal);
                                
								$$ = pNode.get();
								release_value($1);
                                $3 = 0;
                                $5 = NULL;
							}
	|	IDENTIFIER '[' ']' def_array_initialisers
							{
								std::shared_ptr<Node> pNode( new Node("def") );
                                std::shared_ptr<Node> pVal ( new Node("arrayref") );
                                
                                pVal->SetAttribute("name", $1);
                                pVal->SetAttribute("size", 0);
                                
                                pNode->AddChild(pVal);
                                pNode->AddChild(GET_CTX()->GetNode($4));
                                
                                GET_CTX()->AddNode(pNode);
                                GET_CTX()->AddNode(pVal);
                                
								$$ = pNode.get();
								release_value($1);
                                $4 = NULL;
							}
	;

def_init
	:	'=' expression
							{
								$$ = $2;
							}
	;

def_array_initialisers
	:	'=' '{' array_initialisers '}'
							{
								$$ = $3;
								$3 = NULL;
							}
	;

array_initialisers
	:	expression			{
								$$ = new Node("array_initialisers");
								$$->add_child($1);
								$1 = NULL;
							}
	|	array_initialisers ',' expression
							{
								$$ = $1;
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	;

type
	:	TOKEN_FLOAT
							{
								$$ = new Node("type");
								$$->set("type", "float");
							}
	|	TOKEN_STRING
							{
								$$ = new Node("type");
								$$->set("type", "string");
							}
	|	TOKEN_COLOR
							{
								$$ = new Node("type");
								$$->set("type", "color");
							}
	|	TOKEN_POINT
							{
								$$ = new Node("type");
								$$->set("type", "point");
							}
	|	TOKEN_VECTOR
							{
								$$ = new Node("type");
								$$->set("type", "vector");
							}
	|	TOKEN_NORMAL
							{
								$$ = new Node("type");
								$$->set("type", "normal");
							}
	|	TOKEN_MATRIX
							{
								$$ = new Node("type");
								$$->set("type", "matrix");
							}
	|	TOKEN_VOID
							{
								$$ = new Node("type");
								$$->set("type", "void");
							}
	;

statements
	:	statement
							{
								$$ = new Node("statements");
								$$->add_child($1);
								$1 = NULL;
							}
	|	statements statement
							{
								$$ = $1;
								$$->add_child($2);
								$1 = NULL;
								$2 = NULL;
							}
	;

statement
	:	assign_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	function_call_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	return_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	loop_mod_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	definition_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	block_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	loop_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	if_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	;


assign_statement
	:	assign_expression ';'
							{
								$$ = $1;
								$1 = NULL;
							}
	;

function_call_statement
	:	function_call ';'
							{
								$$ = $1;
								$1 = NULL;
							}
	;

return_statement
	:	TOKEN_RETURN expression ';'
							{
								$$ = new Node("return");
								$$->add_child($2);
								$2 = NULL;
							}
	;

definition_statement
	:	variable_definitions ';'
							{
								$$ = $1;
								$1 = NULL;
							}
	|	function_definition
							{
								$$ = $1;
								$1 = NULL;
							}
	;

block_statement
	:	'{' statements '}'
							{
								$$ = new Node("block");
								$$->add_child($2);
								$2 = NULL;
							}
	|	'{' '}'
							{
								$$ = new Node("block");
							}
	|	';'
							{
								$$ = new Node("block");
							}
	;


loop_statement
	:	while_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	for_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	solar_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	illuminate_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	illuminance_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	gather_statement
							{
								$$ = $1;
								$1 = NULL;
							}

while_statement
	:	TOKEN_WHILE relation statement
							{
								$$ = new Node("while");
								$$->add_child($2);
								$$->add_child($3);
								$2 = NULL;
								$3 = NULL;
							}
	|	TOKEN_WHILE expression statement
							{
								$$ = new Node("while");
								$$->add_child($2);
								$$->add_child($3);
								$2 = NULL;
								$3 = NULL;
							}
	;

for_statement
	:	TOKEN_FOR '(' expression ';' relation ';' expression ')' statement
							{
								$$ = new Node("for");
								$$->add_child($3);
								$$->add_child($5);
								$$->add_child($7);
								$$->add_child($9);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
								$9 = NULL;
							}
	|	TOKEN_FOR '(' expression ';' expression ';' expression ')' statement
							{
								$$ = new Node("for");
								$$->add_child($3);
								$$->add_child($5);
								$$->add_child($7);
								$$->add_child($9);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
								$9 = NULL;
							}
	;

solar_statement
	:	TOKEN_SOLAR '(' ')' statement
							{
								Node* args = new Node("function_arguments");
								$$ = new Node("solar");
								$$->add_child(args);
								$$->add_child($4);
								$4 = NULL;
							}
	|	TOKEN_SOLAR '(' expression ',' expression ')' statement
							{
								Node* args = new Node("function_arguments");
								args->add_child($3);
								args->add_child($5);
								$$ = new Node("solar");
								$$->add_child(args);
								$$->add_child($7);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
							}
	;

illuminate_statement
	:	TOKEN_ILLUMINATE '(' expression ')' statement
							{
								Node* args = new Node("function_arguments");
								args->add_child($3);
								$$ = new Node("illuminate");
								$$->add_child(args);
								$$->add_child($5);
								$3 = NULL;
								$5 = NULL;
							}
	|	TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement
							{
								Node* args = new Node("function_arguments");
								args->add_child($3);
								args->add_child($5);
								args->add_child($7);
								$$ = new Node("illuminate");
								$$->add_child(args);
								$$->add_child($9);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
								$9 = NULL;
							}
	;

illuminance_statement
	:	TOKEN_ILLUMINANCE '(' expression ')' statement
							{
								Node* args = new Node("function_arguments");
								args->add_child($3);
								$$ = new Node("illuminance");
								$$->add_child(args);
								$$->add_child($5);
								$3 = NULL;
								$5 = NULL;
							}
	|	TOKEN_ILLUMINANCE '(' expression ',' expression ')' statement
							{
								Node* args = new Node("function_arguments");
								args->add_child($3);
								args->add_child($5);
								$$ = new Node("illuminance");
								$$->add_child(args);
								$$->add_child($7);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
							}
	|	TOKEN_ILLUMINANCE '(' expression ',' expression ',' expression ')' statement
							{
								Node* args = new Node("function_arguments");
								args->add_child($3);
								args->add_child($5);
								args->add_child($7);
								$$ = new Node("illuminance");
								$$->add_child(args);
								$$->add_child($9);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
								$9 = NULL;
							}
	|	TOKEN_ILLUMINANCE '(' expression ',' expression ',' expression ',' expression ')' statement
							{
								Node* args = new Node("function_arguments");
								args->add_child($3);
								args->add_child($5);
								args->add_child($7);
								args->add_child($9);
								$$ = new Node("illuminance");
								$$->add_child(args);
								$$->add_child($11);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
								$9 = NULL;
								$11 = NULL;
							}
	;

gather_statement
	:	TOKEN_GATHER '(' expression ',' expression ',' expression ',' expression ',' expression ')' statement
							{
								Node* args = new Node("function_arguments");
								args->add_child($3);
								args->add_child($5);
								args->add_child($7);
								args->add_child($9);
								args->add_child($11);
								$$ = new Node("gather");
								$$->add_child(args);
								$$->add_child($13);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
								$9 = NULL;
								$11 = NULL;
								$13 = NULL;
							}




if_statement
	: TOKEN_IF relation statement
							{
								$$ = new Node("if");
								$$->set("haselse",0);
								$$->add_child($2);
								$$->add_child($3);
								$2 = NULL;
								$3 = NULL;
							}
	| TOKEN_IF expression statement
							{
								$$ = new Node("if");
								$$->set("haselse",0);
								$$->add_child($2);
								$$->add_child($3);
								$2 = NULL;
								$3 = NULL;
							}
	| TOKEN_IF relation statement TOKEN_ELSE statement
							{
								$$ = new Node("if");
								$$->set("haselse",1);
								$$->add_child($2);
								$$->add_child($3);
								$$->add_child($5);
								$2 = NULL;
								$3 = NULL;
								$5 = NULL;
							}
	| TOKEN_IF expression statement TOKEN_ELSE statement
							{
								$$ = new Node("if");
								$$->set("haselse",1);
								$$->add_child($2);
								$$->add_child($3);
								$$->add_child($5);
								$2 = NULL;
								$3 = NULL;
								$5 = NULL;
							}
	;

loop_mod_statement
	:	TOKEN_BREAK number ';'
							{
								$$ = new Node("break");
								$$->set("label", $2);
							}
	|	TOKEN_BREAK ';'
							{
								$$ = new Node("break");
							}
	|	TOKEN_CONTINUE number ';'
							{
								$$ = new Node("continue");
								$$->set("label", $2);
							}
	|	TOKEN_CONTINUE ';'
							{
								$$ = new Node("continue");
							}
	;



expression
	:	primary
							{
								$$ = $1;
								$1 = NULL;
							}
	|	expression '.' expression
							{
								$$ = new Node(".");
								$$->set("args", 2);
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression '/' expression
							{
								$$ = new Node("/");
								$$->set("args", 2);
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression '*' expression
							{
								$$ = new Node("*");
								$$->set("args", 2);
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression '^' expression
							{
								$$ = new Node("^");
								$$->set("args", 2);
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression '+' expression
							{
								$$ = new Node("+");
								$$->set("args", 2);
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression '-' expression
							{
								$$ = new Node("-");
								$$->set("args", 2);
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	'-' expression	%prec NEG
							{
								$$ = new Node("-");
								$$->set("args", 1);
								$$->add_child($2);
								$2 = NULL;
							}
	|	relation '?' expression ':' expression
							{
								$$ = new Node("?");
								$$->add_child($1);
								$$->add_child($3);
								$$->add_child($5);
								$1 = NULL;
								$3 = NULL;
								$5 = NULL;
							}
	|	cast_expression %prec '('
							{
								$$ = $1;
								$1 = NULL;
							}

primary
	:	number
							{
								$$ = new Node("number");
								$$->set("value", $1);
							}
	|	STRING_LITERAL
							{
								$$ = new Node("string");
								$$->set("value", $1);
								release_value($1);
							}
	|	IDENTIFIER
							{
								$$ = new Node("valref");
								$$->set("name", $1);
								release_value($1);
							}
	|	IDENTIFIER '[' expression ']'
							{
								$$ = new Node("arrayref");
								$$->set("name", $1);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	function_call
							{
								$$ = $1;
								$1 = NULL;
							}
	|	assign_expression
							{
								$$ = $1;
								$1 = NULL;
							}
	|	'(' expression ')'
							{
								$$ = $2;
								$2 = NULL;
							}
	|	triple
							{
								$$ = $1;
								$1 = NULL;
							}
	|	sixteentuple
							{
								$$ = $1;
								$1 = NULL;
							}
	;

triple
	:	'(' expression ',' expression ',' expression ')'
							{
								$$ = new Node("tuple3");
								$$->add_child($2);
								$$->add_child($4);
								$$->add_child($6);
								$2 = NULL;
								$4 = NULL;
								$6 = NULL;
							}
	;

sixteentuple
	:	'(' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ')'
							{
								$$ = new Node("tuple16");
								$$->add_child($2);
								$$->add_child($4);
								$$->add_child($6);
								$$->add_child($8);
								$$->add_child($10);
								$$->add_child($12);
								$$->add_child($14);
								$$->add_child($16);
								$$->add_child($18);
								$$->add_child($20);
								$$->add_child($22);
								$$->add_child($24);
								$$->add_child($26);
								$$->add_child($28);
								$$->add_child($30);
								$$->add_child($32);

								$2 = NULL;
								$4 = NULL;
								$6 = NULL;
								$8 = NULL;//4
								$10 = NULL;
								$12 = NULL;
								$14 = NULL;
								$16 = NULL;//8
								$18 = NULL;
								$20 = NULL;
								$22 = NULL;
								$24 = NULL;//12
								$26 = NULL;
								$28 = NULL;
								$30 = NULL;
								$32 = NULL;//16
							}
	;

relation
	:	'(' relation ')'
							{
								$$ = $2;
								$2 = NULL;
							}
	|	expression '>' expression
							{
								$$ = new Node(">");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression TOKEN_GE expression
							{
								$$ = new Node(">=");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression '<' expression
							{
								$$ = new Node("<");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression TOKEN_LE expression
							{
								$$ = new Node("<=");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression TOKEN_EQ expression
							{
								$$ = new Node("==");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression TOKEN_NE expression
							{
								$$ = new Node("!=");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	relation TOKEN_AND relation
							{
								$$ = new Node("&&");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	relation TOKEN_OR relation
							{
								$$ = new Node("||");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	'!' relation		{
								$$ = new Node("!");
								$$->add_child($2);
								$2 = NULL;
							}
	;

assign_expression
	:	IDENTIFIER '=' expression
							{
								Node* left = new Node("valref");
								left->set("name", $1);
								$$ = new Node("=");
								$$->add_child(left);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	IDENTIFIER TOKEN_ADD_ASSIGN expression
							{
								Node* left = new Node("valref");
								left->set("name", $1);
								$$ = new Node("+=");
								$$->add_child(left);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	IDENTIFIER TOKEN_SUB_ASSIGN expression
							{
								Node* left = new Node("valref");
								left->set("name", $1);
								$$ = new Node("-=");
								$$->add_child(left);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	IDENTIFIER TOKEN_MUL_ASSIGN expression
							{
								Node* left = new Node("valref");
								left->set("name", $1);
								$$ = new Node("*=");
								$$->add_child(left);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	IDENTIFIER TOKEN_DIV_ASSIGN expression
							{
								Node* left = new Node("valref");
								left->set("name", $1);
								$$ = new Node("/=");
								$$->add_child(left);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	IDENTIFIER '[' expression ']' '=' expression
							{
								Node* left = new Node("arrayref");
								left->set("name", $1);
								left->add_child($3);
								$$ = new Node("=");
								$$->add_child(left);
								$$->add_child($6);
								release_value($1);
								$3 = NULL;
								$6 = NULL;
							}
	|	IDENTIFIER  '[' expression ']' TOKEN_ADD_ASSIGN expression
							{
								Node* left = new Node("arrayref");
								left->set("name", $1);
								left->add_child($3);
								$$ = new Node("+=");
								$$->add_child(left);
								$$->add_child($6);
								release_value($1);
								$3 = NULL;
								$6 = NULL;
							}
	|	IDENTIFIER  '[' expression ']' TOKEN_SUB_ASSIGN expression
							{
								Node* left = new Node("arrayref");
								left->set("name", $1);
								left->add_child($3);
								$$ = new Node("-=");
								$$->add_child(left);
								$$->add_child($6);
								release_value($1);
								$3 = NULL;
								$6 = NULL;
							}
	|	IDENTIFIER  '[' expression ']' TOKEN_MUL_ASSIGN expression
							{
								Node* left = new Node("arrayref");
								left->set("name", $1);
								left->add_child($3);
								$$ = new Node("*=");
								$$->add_child(left);
								$$->add_child($6);
								release_value($1);
								$3 = NULL;
								$6 = NULL;
							}
	|	IDENTIFIER  '[' expression ']' TOKEN_DIV_ASSIGN expression
							{
								Node* left = new Node("arrayref");
								left->set("name", $1);
								left->add_child($3);
								$$ = new Node("/=");
								$$->add_child(left);
								$$->add_child($6);
								release_value($1);
								$3 = NULL;
								$6 = NULL;
							}
	;

cast_expression
	:	TOKEN_FLOAT	expression
							{
								$$ = new Node("cast");
								$$->set("type", "float");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_STRING expression
							{
								$$ = new Node("cast");
								$$->set("type", "string");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_COLOR expression
							{
								$$ = new Node("cast");
								$$->set("type", "color");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_POINT expression
							{
								$$ = new Node("cast");
								$$->set("type", "point");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_VECTOR expression
							{
								$$ = new Node("cast");
								$$->set("type", "vector");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_NORMAL expression
							{
								$$ = new Node("cast");
								$$->set("type", "normal");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_MATRIX expression
							{
								$$ = new Node("cast");
								$$->set("type", "matrix");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_COLOR STRING_LITERAL expression
							{
								$$ = new Node("cast");
								$$->set("type", "color");
								$$->set("space", $2);
								$$->add_child($3);
								release_value($2);
								$3 = NULL;
							}
	|	TOKEN_POINT STRING_LITERAL expression
							{
								$$ = new Node("cast");
								$$->set("type", "point");
								$$->set("space", $2);
								$$->add_child($3);
								release_value($2);
								$3 = NULL;
							}
	|	TOKEN_VECTOR STRING_LITERAL expression
							{
								$$ = new Node("cast");
								$$->set("type", "vector");
								$$->set("space", $2);
								$$->add_child($3);
								release_value($2);
								$3 = NULL;
							}
	|	TOKEN_NORMAL STRING_LITERAL expression
							{
								$$ = new Node("cast");
								$$->set("type", "normal");
								$$->set("space", $2);
								$$->add_child($3);
								release_value($2);
								$3 = NULL;
							}
	|	TOKEN_MATRIX STRING_LITERAL expression
							{
								$$ = new Node("cast");
								$$->set("type", "matrix");
								$$->set("space", $2);
								$$->add_child($3);
								release_value($2);
								$3 = NULL;
							}
	;


function_call
	:	function_name '(' texture_argument ',' function_arguments ')'
							{
								Node* args = $5;
								args->add_front($3);
								$$ = new Node("function_call");
								$$->set("name", $1);
								$$->add_child(args);
								release_value($1);
								$3 = NULL;
								$5 = NULL;
							}
	|	function_name '(' texture_argument ')'
							{
								Node* args = new Node("function_arguments");
								args->add_child($3);
								$$ = new Node("function_call");
								$$->set("name", $1);
								$$->add_child(args);
								release_value($1);
								$3 = NULL;
							}
	|	function_name '(' function_arguments ')'
							{
								$$ = new Node("function_call");
								$$->set("name", $1);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	function_name '(' ')'
							{
								$$ = new Node("function_call");
								$$->set("name", $1);
								release_value($1);
							}
	;

function_name
	:	IDENTIFIER
							{
								$$ = $1;
							}
	|	TOKEN_ATMOSPHERE
							{
								$$ = dups_("atmosphere");
							}
	|	TOKEN_DISPLACEMENT
							{
								$$ = dups_("displacement");
							}
	|	TOKEN_LIGHTSOURCE
							{
								$$ = dups_("lightsource");
							}
	|	TOKEN_SURFACE
							{
								$$ = dups_("surface");
							}
	;

function_arguments
	:	expression			{
								$$ = new Node("function_arguments");
								$$->add_child($1);
								$1 = NULL;
							}
	|	function_arguments ',' expression
							{
								$$ = $1;
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	;

texture_argument
	:	STRING_LITERAL
							{
								$$ = new Node("texture_argument");
                                $$->SetAttribute("name", $1);
								release_value($1);
							}
	|	texture_argument '[' channel ']'
							{
                                $1->SetAttribute("channel", $3);
								$$ = $1;
								$1 = NULL;
							}
	;


number
	:	INTEGER_LITERAL
							{
								$$ = (float)$1;
							}
	|	FLOAT_LITERAL
							{
								$$ = (float)$1;
							}
	;

channel
	:	INTEGER_LITERAL
							{
								$$ = $1;
							}
	;


%%

void yy::Parser::error (const location_type& loc, const std::string& msg)
{
	std::string fname = GET_FNAME();
	int lineno = GET_LINE();

	fprintf(stderr, "parser error %s :%s(%d)\n", msg.c_str(), fname.c_str(), lineno);
}


extern int yylex_init(void** p);
extern int yylex_destroy(void* p);

namespace slc
{
	int CompileToNode(FILE* in, Context* ctx)
	{
		InputDecoder dec(in);

		void* scanner;
		yylex_init(&scanner);

		ParseParam* param = (ParseParam*)(scanner);
		param->ctx = ctx;
		param->dec = &dec;
		param->line_number = 1;

		yy::Parser parser(scanner);

		int nRet = parser.parse();

		if(1){
			printf("%s(%d)\n", GET_FNAME().c_str(), GET_LINE());
		}

		yylex_destroy(scanner);

		return nRet;

		return 0;
	}
}
