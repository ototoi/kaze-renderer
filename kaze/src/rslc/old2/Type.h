#ifndef SLC_TYPE_H
#define SLC_TYPE_H

#include <string>
#include "Token.h"

namespace slc
{

    class Type
    {
    public:
        Type(const std::string& s);
        const std::string& ToString() const;

    private:
        std::string s_;
    };
}

#endif