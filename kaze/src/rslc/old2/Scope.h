#ifndef SLC_SCOPE_H
#define SLC_SCOPE_H

#include <memory>
#include "SymbolTable.h"

namespace slc
{

    class Scope
    {
    public:
        Scope();
        Scope(const std::shared_ptr<Scope>& parent);
        virtual ~Scope() {}
        bool IsRoot() const;
        void AddChild(const std::shared_ptr<Scope>& c);
        void SetParent(const std::shared_ptr<Scope>& parent);

    public:
        std::shared_ptr<Symbol> Find(const std::string& s) const;
        void Add(const std::shared_ptr<Symbol>& s);

    protected:
        std::weak_ptr<Scope> parent_;
        std::vector<std::shared_ptr<Scope> > children_;
        std::shared_ptr<SymbolTable> tbl_;
    };
}

#endif