#ifndef SLC_FUNCTION_SCOPE_H
#define SLC_FUNCTION_SCOPE_H

#include "Scope.h"
#include "SymbolTable.h"

namespace slc
{

    class FunctionScope : public Scope
    {
    public:
        FunctionScope(const shared_ptr<Scope>& parent, const Symbol& s);

    public:
        virtual void AddVariableSymbol(const Symbol& s) const;
        virtual const std::shared_ptr<SymbolTable>& GetVariableSymbolTables() const;

    protected:
        Symbol s_;
        std::shared_ptr<SymbolTable> vals_;
    };
}

#endif