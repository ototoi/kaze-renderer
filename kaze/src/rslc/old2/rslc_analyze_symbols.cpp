#include "rslc_optimize_opt1.h"
#include "rslc_context.h"
#include "rslc_node.h"
#include "rslc_location.h"

#include <iostream>
#include <vector>
#include <cassert>

#define CAST(T, n) check_cast<T>(n)
#define ERROR(ctx, node, er) error(ctx, node, er) 

namespace rslc
{
    template<class T, class B>
    T check_cast(B b)
    {
        T t = dynamic_cast<T>(b);
        if(t == NULL)
        {
            std::cerr << "error" << std::endl;
        }
        return t;
    }

    static const int ERROR_NOT_FOUND_SYMBOL = 2000;
    static const int ERROR_ADD_SYMBOL = 2001;

    static
    int error(rslc_context* ctx, const rslc_node* node, int nError)
    {
        rslc_location loc = node->get_location();
        char buffer[512] = {};
        sprintf(buffer, "analyze error %s :%s(%d)\n", "A symbol was not found", loc.get_filename().c_str(), loc.get_line());
        ctx->print_error(buffer);
        return 0;
    }

    typedef struct shader_variable_def
    {
        const char* name;
        const char* type;
        const char* attribute;
    } shader_variable_def;

    static
    const shader_variable_def GLOBAL_VARIABLES[] = 
    {
        {"PI", "float", "uniform"},
        {NULL, NULL, NULL},
    };

    static
    const shader_variable_def  SURFACE_SHADER_VARIABLES[] = 
    {
        {"Cs", "color", "varying"},  
        {"Os", "color", "varying"},  
        {"P", "point", "varying"},  
        {"dPdu", "vector", "varying"},  
        {"dPdv", "vector", "varying"},  
        {"N", "normal", "varying"},  
        {"Ng", "normal", "varying"},  
        {"u", "float", "varying"},  
        {"v", "float", "varying"},  
        {"du", "float", "varying"},  
        {"dv", "float", "varying"},  
        {"s", "float", "varying"},  
        {"t", "float", "varying"},  
        {"L", "vector", "varying"},  
        {"Cl", "color", "varying"},  
        {"Ol", "color", "varying"},  
        {"E", "point", "uniform"},  
        {"I", "vector", "varying"},  
        {"ncomps", "float", "uniform"},  
        {"time", "float", "uniform"},  
        {"dtime", "float", "uniform"},  
        {"dPdtime", "vector", "varying"},  
        {"Ci", "color", "varying"},
        {"Oi", "color", "varying"}, 

        {NULL, NULL, NULL},  
    };

    static
    const shader_variable_def  LIGHT_SHADER_VARIABLES[] = 
    { 
        {"P", "point", "varying"},  
        {"dPdu", "vector", "varying"},  
        {"dPdv", "vector", "varying"},  
        {"N", "normal", "varying"},  
        {"Ng", "normal", "varying"},  
        {"u", "float", "varying"},  
        {"v", "float", "varying"},  
        {"du", "float", "varying"},  
        {"dv", "float", "varying"},  
        {"s", "float", "varying"},  
        {"t", "float", "varying"},  
        {"L", "vector", "varying"}, 

        {"Cl", "color", "varying"},  
        {"Ol", "color", "varying"},  
        
        {"Ps", "point", "varying"}, 

        {"E", "point", "uniform"},  
       
        {"ncomps", "float", "uniform"},  
        {"time", "float", "uniform"},  
        {"dtime", "float", "uniform"},  
      
        {"Ci", "color", "varying"},
        {"Oi", "color", "varying"}, 

        {NULL, NULL, NULL},  
    };

    static
    const shader_variable_def  VOLUME_SHADER_VARIABLES[] = 
    {
        {"P", "point", "varying"},

        {"E", "point", "uniform"},  
        {"I", "vector", "varying"},  
        {"ncomps", "float", "uniform"},  
        {"time", "float", "uniform"},  
        {"dtime", "float", "uniform"},  
       
        {"Ci", "color", "varying"},
        {"Oi", "color", "varying"}, 

        {NULL, NULL, NULL},  
    };

    static
    const shader_variable_def  DISPLACEMENT_SHADER_VARIABLES[] = 
    {
        //{"Cs", "color", "varying"},  
        //{"Os", "color", "varying"},  
        {"P", "point", "varying"},  
        {"dPdu", "vector", "varying"},  
        {"dPdv", "vector", "varying"},  
        {"N", "normal", "varying"},  
        {"Ng", "normal", "varying"},  
        {"E", "point", "uniform"},  
        {"I", "vector", "varying"},
        {"u", "float", "varying"},  
        {"v", "float", "varying"},  
        {"du", "float", "varying"},  
        {"dv", "float", "varying"},  
        {"s", "float", "varying"},  
        {"t", "float", "varying"},  
        //{"L", "vector", "varying"},  
        //{"Cl", "color", "varying"},  
        //{"Ol", "color", "varying"},  
        {"ncomps", "float", "uniform"},  
        {"time", "float", "uniform"},  
        {"dtime", "float", "uniform"},  
        {"dPdtime", "vector", "varying"},
        //{"Ci", "color", "varying"},
        //{"Oi", "color", "varying"},  

        {NULL, NULL, NULL},  
    };

    static
    const shader_variable_def  IMAGER_SHADER_VARIABLES[] = 
    { 
        {"P", "point", "varying"},  
        {"alpha", "float", "uniform"},  
        {"ncomps", "float", "uniform"},  
        {"time", "float", "uniform"},
        {"dtime", "float", "uniform"},  
        {"Ci", "color", "varying"},
        {"Oi", "color", "varying"}, 

        {NULL, NULL, NULL},  
    };



    static
    int rslc_add_shader_symbol_(rslc_context* ctx, const shader_variable_def& def)
    {
        rslc_type t(def.type);
        t.add_attribute(def.attribute);

        rslc_symbol sym(t, def.name);
        if(!ctx->add_symbol(sym))
        {
            return ERROR_ADD_SYMBOL;
        }
        return 0;
    }

    static
    int rslc_initialize_shader_symbols_(rslc_context* ctx, const rslc_shader_def_node* node)
    {
        const rslc_type_node* tnode = CAST(const rslc_type_node*, node->get_type_node());
        std::string type = tnode->get_type().get_name();
        if(type == "surface")
        {
            int i = 0;
            while(SURFACE_SHADER_VARIABLES[i].name)
            {
                int nRet = rslc_add_shader_symbol_(ctx, SURFACE_SHADER_VARIABLES[i]);
                if(nRet != 0)
                {
                    return ERROR(ctx, node, nRet);
                }
                i++;
            }
        }
        else if(type == "light")
        {
            int i = 0;
            while(LIGHT_SHADER_VARIABLES[i].name)
            {
                int nRet = rslc_add_shader_symbol_(ctx, LIGHT_SHADER_VARIABLES[i]);
                if(nRet != 0)
                {
                    return ERROR(ctx, node, nRet);
                }
                i++;
            }
        }
        else if(type == "volume")
        {
            int i = 0;
            while(VOLUME_SHADER_VARIABLES[i].name)
            {
                int nRet = rslc_add_shader_symbol_(ctx, VOLUME_SHADER_VARIABLES[i]);
                if(nRet != 0)
                {
                    return ERROR(ctx, node, nRet);
                }
                i++;
            }
        }
        else if(type == "displacement")
        {
            int i = 0;
            while(DISPLACEMENT_SHADER_VARIABLES[i].name)
            {
                int nRet = rslc_add_shader_symbol_(ctx, DISPLACEMENT_SHADER_VARIABLES[i]);
                if(nRet != 0)
                {
                    return ERROR(ctx, node, nRet);
                }
                i++;
            }
        }
        else if(type == "imager")
        {
            int i = 0;
            while(IMAGER_SHADER_VARIABLES[i].name)
            {
                int nRet = rslc_add_shader_symbol_(ctx, IMAGER_SHADER_VARIABLES[i]);
                if(nRet != 0)
                {
                    return ERROR(ctx, node, nRet);
                }
                i++;
            }
        }
        return 0;
    }

    static
    int rslc_initialize_global_symbols_(rslc_context* ctx, const rslc_definitions_node* node)
    {
        int i = 0;
        while(GLOBAL_VARIABLES[i].name)
        {
            int nRet = rslc_add_shader_symbol_(ctx, GLOBAL_VARIABLES[i]);
            if(nRet != 0)
            {
                return ERROR(ctx, node, nRet);
            }
            i++;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_node* node);

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_identifier_node* node)
    {
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_integer_node* node)
    {
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_float_node* node)
    {
        return 0;
    }   

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_string_node* node)
    {
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_texturename_node* node)
    {
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_list_node* node)
    {
        int sz = (int)node->get_size();
        for(int i = 0;i<sz;i++)
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_at(i));
            if(nRet != 0)
            {
                return nRet; 
            }
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_block_node* node)
    {
        if(!ctx->push_symbol_table())
        {
            return -1;
        }

        {
            int sz = (int)node->get_size();
            for(int i = 0;i<sz;i++)
            {
                int nRet = rslc_analyze_symbols_(ctx, node->get_at(i));
                if(nRet != 0)
                {
                    return nRet; 
                }
            }
        }

        if(!ctx->pop_symbol_table())
        {
            return -1;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_tuple_node* node)
    {
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_type_node* node)
    {
        return 0;
    }

    static
    int rslc_analyze_symbols_(
        rslc_context* ctx, 
        const rslc_type_node* tnode, const rslc_variable_def_node* vnode)
    {
        std::string name = vnode->get_name();
        rslc_type t = tnode->get_type();
        rslc_symbol sym(t, name);
        if(!ctx->add_symbol(sym))
        {
            //std::cout << ":" << name << ":" << std::endl;
            ctx->debug_print();
            return ERROR(ctx, vnode, ERROR_ADD_SYMBOL);
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(
        rslc_context* ctx,
        const rslc_type_node* tnode, const rslc_variable_array_def_node* vnode)
    {
        std::string name = vnode->get_name();
        rslc_type t = tnode->get_type();
        rslc_symbol sym(t, name);
        if(!ctx->add_symbol(sym))
        {
            //std::cout << ":" << name << ":" << std::endl;
            ctx->debug_print();
            return ERROR(ctx, vnode, ERROR_ADD_SYMBOL);
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_variable_def_node* node)
    {
        return ERROR_ADD_SYMBOL;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_variable_array_def_node* node)
    {
        return ERROR_ADD_SYMBOL;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_type_variables_def_node* node)
    {
        const rslc_type_variables_def_node* tvnode = node;
        const rslc_type_node* tnode = CAST(const rslc_type_node*, tvnode->get_type_node());
        const rslc_list_node* vsnode = CAST(const rslc_list_node*, tvnode->get_variables_node());
        std::string type = tnode->get_type().get_name();
        size_t jsz = vsnode->get_size();
        for(size_t j=0;j<jsz;j++)
        {
            const rslc_node* vnode = vsnode->get_at(j);
            int nType = vnode->get_node_type();
            if(nType == RSLC_NODE_VARIABLE_DEF)
            {
                int nRet = rslc_analyze_symbols_(ctx, tnode, CAST(const rslc_variable_def_node*,vnode));
                if(nRet != 0)
                {
                    return nRet;
                }
            }
            else if(nType == RSLC_NODE_VARIABLE_ARRAY_DEF)
            {
                int nRet = rslc_analyze_symbols_(ctx, tnode, CAST(const rslc_variable_array_def_node*,vnode));
                if(nRet != 0)
                {
                    return nRet;
                }
            }
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_variable_ref_node* node)
    {
        std::string name = node->get_name();
        if(!ctx->exist_symbol(name))
        {
            std::cout << ":" << name << ":" << std::endl;
            //ctx->debug_print();
            return ERROR(ctx, node, ERROR_NOT_FOUND_SYMBOL);
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_variable_array_ref_node* node)
    {
        std::string name = node->get_name();
        if(!ctx->exist_symbol(name))
        {
            std::cout << ":" << name << ":" << std::endl;
            //ctx->debug_print();
            return ERROR(ctx, node, ERROR_NOT_FOUND_SYMBOL);
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_function_call_node* node)
    {
        std::string name = node->get_name();
        //if(!ctx->exist_symbol(name))
        //{
        //    std::cout << ":" << name << ":" << std::endl;
            //ctx->debug_print();
            //return ERROR(ctx, node, ERROR_NOT_FOUND_SYMBOL);
        //}

        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_arguments_node());
            if(nRet != 0)return nRet;
        }

        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_neg_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_not_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_add_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_sub_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_mul_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_div_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_crs_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_dot_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    //---------------------------
    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_lss_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_leq_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_gtr_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_geq_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_eq_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_ne_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_and_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_or_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }
    //---------------------------

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_assign_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_add_assign_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_sub_assign_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_mul_assign_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_div_assign_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_lhs_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_rhs_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    //---------------------------

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_cond_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_expression1_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_expression2_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_cast_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_expression_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_paren_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_statement_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_while_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_for_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_solar_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_illuminate_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_illuminance_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_gather_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_if_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_condition_node());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_contents_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    //---------------------------

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_return_node* node)
    {
        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_expression_node());
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_break_node* node)
    {
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_continue_node* node)
    {
        return 0;
    }

    //---------------------------
    
    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_function_def_node* node)
    {
        std::string name = node->get_name();
        rslc_symbol func_sym(name);
        if(!ctx->add_symbol(func_sym))
        {
            return -1;
        }

        if(!ctx->push_symbol_table())
        {
            return -1;
        }

        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_arguments_node());
            if(nRet != 0)return nRet;
        }

        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_contents_node());
            if(nRet != 0)return nRet;
        }

        if(!ctx->pop_symbol_table())
        {
            return -1;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_shader_def_node* node)
    {
        std::string name = node->get_name();
        rslc_symbol func_sym(name);
        if(!ctx->add_symbol(func_sym))
        {
            return -1;
        }

        if(!ctx->push_symbol_table())
        {
            return -1;
        }

        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_arguments_node());
            if(nRet != 0)return nRet;
        }

        {
            int nRet = rslc_initialize_shader_symbols_(ctx, node);
            if(nRet != 0)return nRet;
        }

        {
            int nRet = rslc_analyze_symbols_(ctx, node->get_contents_node());
            if(nRet != 0)return nRet;
        }

        if(!ctx->pop_symbol_table())
        {
            return -1;
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_definitions_node* node)
    {
        {
            int nRet = rslc_initialize_global_symbols_(ctx, node);
            if(nRet != 0)return nRet;
        }
        size_t sz = node->get_size();
        for(size_t i=0;i<sz;i++)
        {
            rslc_node* n = node->get_at(i);
            int nType = n->get_node_type();
            if(nType == RSLC_NODE_FUNCTION_DEF)
            {
                int nRet = rslc_analyze_symbols_(ctx, (rslc_function_def_node*)n);
                if(nRet != 0)return nRet;
            }
            else if(nType == RSLC_NODE_SHADER_DEF)
            {
                int nRet = rslc_analyze_symbols_(ctx, (rslc_shader_def_node*)n);
                if(nRet != 0)return nRet;
            }
        }
        return 0;
    }

    static
    int rslc_analyze_symbols_(rslc_context* ctx, const rslc_node* node)
    {
        int nType = node->get_node_type();
        switch(nType)
        {
        case RSLC_NODE_IDENTIFIER:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_identifier_node*,node));
        case RSLC_NODE_INTEGER:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_integer_node*,node));
        case RSLC_NODE_FLOAT:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_float_node*,node));
        case RSLC_NODE_STRING:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_string_node*,node));
        case RSLC_NODE_TEXTURENAME:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_texturename_node*,node));

        case RSLC_NODE_LIST:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_list_node*,node));
        case RSLC_NODE_TUPLE:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_tuple_node*,node));
        
        case RSLC_NODE_TYPE:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_type_node*,node));
        case RSLC_NODE_VARIABLE_REF:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_variable_ref_node*,node));
        case RSLC_NODE_VARIABLE_ARRAY_REF:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_variable_array_ref_node*,node));
        case RSLC_NODE_FUNCTION_CALL:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_function_call_node*,node));
        
        case RSLC_NODE_NEG:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_neg_node*,node));
        case RSLC_NODE_NOT:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_not_node*,node));
        case RSLC_NODE_ADD:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_add_node*,node));
        case RSLC_NODE_SUB:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_sub_node*,node));
        case RSLC_NODE_MUL:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_mul_node*,node));
        case RSLC_NODE_DIV:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_div_node*,node));
        case RSLC_NODE_CRS:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_crs_node*,node));
        case RSLC_NODE_DOT:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_dot_node*,node));

        case RSLC_NODE_LSS:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_lss_node*,node));
        case RSLC_NODE_LEQ:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_leq_node*,node));
        case RSLC_NODE_GTR:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_gtr_node*,node));
        case RSLC_NODE_GEQ:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_geq_node*,node));
        case RSLC_NODE_EQ:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_eq_node*,node));
        case RSLC_NODE_NE:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_ne_node*,node));

        case RSLC_NODE_AND:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_and_node*,node));
        case RSLC_NODE_OR:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_or_node*,node));

        case RSLC_NODE_ASSIGN:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_assign_node*,node));
        case RSLC_NODE_ADD_ASSIGN:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_add_assign_node*,node));
        case RSLC_NODE_SUB_ASSIGN:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_sub_assign_node*,node));
        case RSLC_NODE_MUL_ASSIGN:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_mul_assign_node*,node));
        case RSLC_NODE_DIV_ASSIGN:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_div_assign_node*,node));

        case RSLC_NODE_COND:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_cond_node*,node));
        case RSLC_NODE_CAST:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_cast_node*,node));

        case RSLC_NODE_STATEMENT:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_statement_node*,node));

        case RSLC_NODE_BLOCK:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_block_node*,node));

        case RSLC_NODE_WHILE:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_while_node*,node));
        case RSLC_NODE_FOR:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_for_node*,node));
        case RSLC_NODE_SOLAR:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_solar_node*,node));
        case RSLC_NODE_ILLUMINATE:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_illuminate_node*,node));
        case RSLC_NODE_ILLUMINANCE:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_illuminance_node*,node));
        case RSLC_NODE_GATHER:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_gather_node*,node));

        case RSLC_NODE_IF:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_if_node*,node));

        case RSLC_NODE_RETURN:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_return_node*,node));
        case RSLC_NODE_BREAK:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_break_node*,node));
        case RSLC_NODE_CONTINUE:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_continue_node*,node));
    
        case RSLC_NODE_VARIABLE_DEF:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_variable_def_node*,node));
        case RSLC_NODE_VARIABLE_ARRAY_DEF:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_variable_array_def_node*,node));
        case RSLC_NODE_TYPE_VARIABLES_DEF:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_type_variables_def_node*,node));

        case RSLC_NODE_FUNCTION_DEF:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_function_def_node*,node));
        case RSLC_NODE_SHADER_DEF:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_shader_def_node*,node));
            
        case RSLC_NODE_DEFINITIONS:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_definitions_node*,node));

        case RSLC_NODE_PAREN:
            return rslc_analyze_symbols_(ctx, CAST(const rslc_paren_node*,node));
        default:
            break;
        }
        return 0;
    }

    int rslc_analyze_symbols(rslc_context* ctx, const rslc_node* node)
    {
        int nRet = 0;
        nRet = rslc_analyze_symbols_(ctx, node);
        if(nRet != 0)return nRet;
        return nRet;
    }
}
