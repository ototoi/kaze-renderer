#ifndef SLC_ATTRIBUTE_H
#define SLC_ATTRIBUTE_H

#include <vector>

namespace slc
{

    class Attribute
    {
    public:
        Attribute(int v);
        Attribute(float v);
        Attribute(const std::string& v);

    protected:
        std::vector<int> ival_;
        std::vector<float> fval_;
        std::vector<std::string> sval_;
    };
}

#endif
