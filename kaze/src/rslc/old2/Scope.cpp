#include "Scope.h"

namespace slc
{

    Scope::Scope()
    {
        tbl_.reset(new SymbolTable());
    }

    Scope::Scope(const std::shared_ptr<Scope>& parent)
        : parent_(parent)
    {
        ;
    }

    void Scope::SetParent(const std::shared_ptr<Scope>& parent)
    {
        parent_ = parent;
    }

    bool Scope::IsRoot() const
    {
        return parent_.get() == NULL;
    }

    void Scope::AddChild(const std::shared_ptr<Scope>& c)
    {
        children_.push_back(c);
    }

    std::shared_ptr<Symbol> Scope::Find(const std::string& s) const
    {
        return tbl_->Find(s);
    }

    void Scope::Add(const std::shared_ptr<Symbol>& s)
    {
        tbl_->Add(s);
    }
}