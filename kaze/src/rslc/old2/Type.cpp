#include "Type.h"

namespace slc
{

    Type::Type(const std::string& s)
    {
        s_ = s;
    }

    const std::string& Type::ToString() const
    {
        return s_;
    }
}