#ifndef SLC_NODE_H
#define SLC_NODE_H

#include <string>
#include <memory>
#include <vector>

#include "Attribute.h"

namespace slc
{
    class Node
    {
    public:
        Node(const std::string& name);
        ~Node();

    protected:
        std::string name_;
        std::vector<std::shared_ptr<Node> > children_;
    };
}

#endif