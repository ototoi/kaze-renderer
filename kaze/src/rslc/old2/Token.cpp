#include "Token.h"

namespace slc
{

    Token::Token(const char* szName)
        : s_(szName)
    {
    }

    Token::Token(const std::string& s)
        : s_(s)
    {
    }

    bool Token::IsNull() const
    {
        return s_ != "";
    }

    const std::string& Token::ToString() const
    {
        return s_;
    }
}