#ifndef SLC_NAMESPACE_SCOPE_H
#define SLC_NAMESPACE_SCOPE_H

#include <memory>
#include "Scope.h"

namespace slc
{

    class NamespaceScope : public Scope
    {
    public:
        NamespaceScope(const shared_ptr<Scope>& parent, const Token& s);

    public:
        virtual void AddVariableSymbol(const Symbol& s) const;
        virtual void AddFunctionSymbol(const Symbol& s) const;
        virtual void AddTypeSymbol(const Symbol& s) const;

    protected:
        Token token_;
        std::shared_ptr<SymbolTable> vals_;
        std::shared_ptr<SymbolTable> funcs_;
        std::shared_ptr<SymbolTable> types_;
    };
}

#endif