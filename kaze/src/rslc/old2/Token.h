#ifndef SLC_TOKEN_H
#define SLC_TOKEN_H

#include <string>

namespace slc
{

    class Token
    {
    public:
        Token(const char* szName);
        Token(const std::string& s);
        bool IsNull() const;
        const std::string& ToString() const;

    private:
        std::string s_;
    };
}

#endif