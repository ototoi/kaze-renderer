#ifndef RSLC_PARSE_PARAM_H
#define RSLC_PARSE_PARAM_H

#include <string>


namespace rslc
{

    class rslc_context;
    class rslc_decoder;

    typedef struct rslc_parse_param
    {
        rslc_context* ctx;
        rslc_decoder* dec;

        std::string filename;
        int line_number;
    } rslc_parse_param;
}


#endif
