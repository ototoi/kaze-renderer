#ifndef RSLC_SYMBOL_MANAGER_H
#define RSLC_SYMBOL_MANAGER_H

#include "rslc_symbol.h"
#include <vector>
#include <map>
#include <unordered_map>
#include <memory>

namespace rslc
{
    class rslc_symbol_table
    {
    public:
        typedef std::unordered_map<std::string, std::vector< std::shared_ptr<rslc_symbol> > > map_type;
    public:
        rslc_symbol_table()
            :parent_(NULL)
        {}
        explicit rslc_symbol_table(rslc_symbol_table* parent)
            :parent_(parent)
        {
        }
        bool add_symbol    (const std::shared_ptr<rslc_symbol>& s);
        bool find_types    (std::vector<const rslc_symbol*>& symbols, const std::string& name)const;
        bool find_variables(std::vector<const rslc_symbol*>& symbols, const std::string& name)const;
        bool find_functions(std::vector<const rslc_symbol*>& symbols, const std::string& name)const;
    public:
        void debug_print()const;
    private:
        rslc_symbol_table* parent_;  
        std::unordered_map<std::string, std::vector< std::shared_ptr<rslc_symbol> > > sm_;
    };

    class rslc_symbol_manager
    {
    public:
        rslc_symbol_manager();
        ~rslc_symbol_manager();
        bool push_symbol_table();
        bool pop_symbol_table();
        rslc_symbol_table* top_symbol_table()const;
        bool add_symbol    (const std::shared_ptr<rslc_symbol>& s);
        bool find_types    (std::vector<const rslc_symbol*>& symbols, const std::string& name)const;
        bool find_variables(std::vector<const rslc_symbol*>& symbols, const std::string& name)const;
        bool find_functions(std::vector<const rslc_symbol*>& symbols, const std::string& name)const;
        void clear_symbol();
    public:
        void debug_print()const;
    protected:
        std::vector<rslc_symbol_table*> tables_;
        std::vector< std::shared_ptr<rslc_symbol_table> > objects_;
    };
}

#endif
