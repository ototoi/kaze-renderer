#include "rslc_write_ksl.h"
#include "rslc_node.h"

//#include "rslc_ksl_array.h"



#define CAST(T, n) check_cast<T>(n)

namespace rslc
{
    template<class T, class B>
    T check_cast(B b)
    {
        T t = dynamic_cast<T>(b);
        if(t == NULL)
        {
            std::cerr << "error" << std::endl;
        }
        return t;
    }

    static
    std::string tabs(int n)
    {
        typedef const char* SZCS;
#if 1
        static 
        SZCS szTABLE[] = 
        {
            "",
            "    ",
            "        ",
            "            ",
            "                ",
            "                    ",
            "                        ",
            "                            ",
            "                                ",
            "                                    ",
            "                                        ",
        };

        if(n < 10)
        {
            return szTABLE[n];
        }
        else
        {
            static char buffer[512] = {};
            for(int i=0;i<n;i++)
            {
                for(int j=0;j<4;j++)
                {
                    buffer[4*i+j] = ' ';
                }
            }
            buffer[4*n] = 0;
            return buffer;
        }
#else
        static 
        SZCS szTABLE[] = 
        {
            "",
            "\t",
            "\t\t",
            "\t\t\t",
            "\t\t\t\t",
            "\t\t\t\t\t",
            "\t\t\t\t\t\t",
            "\t\t\t\t\t\t\t",
            "\t\t\t\t\t\t\t\t",
            "\t\t\t\t\t\t\t\t\t",
            "\t\t\t\t\t\t\t\t\t\t",
        };
        if(n < 10)
        {
            return szTABLE[n];
        }
        else
        {
            static char buffer[512] = {};
            for(int i=0;i<n;i++)
            {
                buffer[i] = '\t';
            }
            buffer[n] = 0;
            return buffer;
        }
#endif
    }

    typedef struct func_names{
        const char* name;
        const char* mangled_name;
    } func_names;

    static
    const func_names RSL_STD_FUNCS[] = 
    {
        {"radians", "radians_f1_f1"},
        {"degrees", "degrees_f1_f1"},
        {"sin", "sin_f1_f1"},
        {"asin", "asin_f1_f1"},
        {"cos", "cos_f1_f1"},
        {"acos", "acos_f1_f1"},
        {"tan", "tan_f1_f1"},
        {"atan", "atan_f1_f1"},
        {"atan", "atan_f1_f1f1"},
        {"pow", "pow_f1_f1f1"},
        {"exp", "exp_f1_f1"},
        {"sqrt", "sqrt_f1_f1"},
        {"inversesqrt", "inversesqrt_f1_f1"},
        {"log", "log_f1_f1"},
        {"log", "log_f1_f1f1"},
        {"mod", "mod_f1_f1f1"},
        {"abs", "abs_f1_f1"},
        {"sign", "sign_f1_f1"},
        {"min", "min_f1_f1f1f1"},
        {"min", "min_f3_f3f3f3"},
        {"max", "max_f1_f1f1f1"},
        {"max", "max_f3_f3f3f3"},
        {"clamp", "clamp_f1_f1f1f1"},
        {"clamp", "clamp_f3_f3f3f3"},
        {"mix", "mix_f1_f1f1f1"},
        {"mix", "mix_f3_f3f3f1"},
        {"floor", "floor_f1_f1"},
        {"ceil", "ceil_f1_f1"},
        {"round", "round_f1_f1"},
        {"step", "step_f1_f1f1"},
        {"smoothstep", "smoothstep_f1_f1f1f1"},
        {"Du", "Du_f1_f1"},
        {"Dv", "Dv_f1_f1"},
        {"Deriv", "Deriv_f1_f1f1"},
        {"Du", "Du_f3_f3"},
        {"Dv", "Dv_f3_f3"},
        {"Deriv", "Deriv_f3_f3f1"},
        {"random", "random_f1_z0"},
        {"random", "random_f3_z0"},
        {"noise", "noise_f1_f1"},
        {"noise", "noise_f1_f1f1"},
        {"noise", "noise_f1_f3"},
        {"noise", "noise_f1_f3f1"},
        {"noise", "noise_f3_f1"},
        {"noise", "noise_f3_f1f1"},
        {"noise", "noise_f3_f3"},
        {"noise", "noise_f3_f3f1"},
        {"pnoise", "pnoise_f1_f1f1"},
        {"pnoise", "pnoise_f1_f1f1f1f1"},
        {"pnoise", "pnoise_f1_f3f3"},
        {"pnoise", "pnoise_f1_f3f1f3f1"},
        {"pnoise", "pnoise_f3_f1f1"},
        {"pnoise", "pnoise_f3_f1f1f1f1"},
        {"pnoise", "pnoise_f3_f3f3"},
        {"pnoise", "pnoise_f3_f3f1f3f1"},
        {"cellnoise", "cellnoise_f1_f1"},
        {"cellnoise", "cellnoise_f1_f1f1"},
        {"cellnoise", "cellnoise_f1_f3"},
        {"cellnoise", "cellnoise_f1_f3f1"},
        {"cellnoise", "cellnoise_f3_f1"},
        {"cellnoise", "cellnoise_f3_f1f1"},
        {"cellnoise", "cellnoise_f3_f3"},
        {"cellnoise", "cellnoise_f3_f3f1"},
        //{"xcomp", "xcomp_f1_f3"},
        //{"ycomp", "ycomp_f1_f3"},
        //{"zcomp", "zcomp_f1_f3"},
        //{"setxcomp", "setxcomp_z0_f3f1"},
        //{"setycomp", "setycomp_z0_f3f1"},
        //{"setzcomp", "setzcomp_z0_f3f1"},
        //{"length", "length_f1_f3"},
        //{"normalize", "normalize_f3_f3"},
        {"distance", "distance_f1_f3f3"},
        {"ptlined", "ptlined_f1_f3f3f3"},
        {"rotate", "rotate_f1_f3f1f3f3"},
        {"area", "area_f1_f3"},
        {"reflect", "reflect_f3_f3f3"},
        {"refract", "refract_f3_f3f3f1"},
        {"transform", "transform_f3_sxf3"},
        {"transform", "transform_f3_sxsxf3"},
        {"transform", "transform_f3_m4f3"},
        {"transform", "transform_f3_sxm4f3"},
        {"vtransform", "vtransform_f3_sxf3"},
        {"vtransform", "vtransform_f3_sxsxf3"},
        {"vtransform", "vtransform_f3_m4f3"},
        {"vtransform", "vtransform_f3_sxm4f3"},
        {"ntransform", "ntransform_f3_sxf3"},
        {"ntransform", "ntransform_f3_sxsxf3"},
        {"ntransform", "ntransform_f3_m4f3"},
        {"ntransform", "ntransform_f3_sxm4f3"},
        {"depth", "depth_f1_f3"},
        {"calculatenormal", "calculatenormal_f3_f3"},
        {"comp", "comp_f1_f3f1"},
        {"setcomp", "setcomp_z0_f3f1f1"},
        {"ctransform", "ctransform_f3_sxf3"},
        {"ctransform", "ctransform_f3_sxsxf3"},
        {"comp", "comp_f1_m4f1f1"},
        {"setcomp", "setcomp_z0_m4f1f1f1"},
        {"determinant", "determinant_f1_m4"},
        {"translate", "translate_m4_m4f3"},
        {"rotate", "rotate_m4_m4f1f3"},
        {"scale", "scale_m4_m4f3"},
        {"concat", "concat_sx_sxsxsx"},
        {"printf", "printf_z0_sxsxsxsxsx"},
        {"format", "format_sx_sxsxsxsxsx"},
        {"match", "match_f1_sxsx"},
        {"ambient", "ambient_f3_z0"},
        {"diffuse", "diffuse_f3_f3"},
        {"specular", "specular_f3_f3f3f1"},
        {"specularbrdf", "specularbrdf_f3_f3f3f3f1"},
        {"phong", "phong_f3_f3f3f1"},
        {"trace", "trace_f3_f3f3"},
        {"atmosphere", "atmosphere_f1_sxf1"},
        {"atmosphere", "atmosphere_f1_sxf3"},
        {"displacement", "displacement_f1_sxf1"},
        {"displacement", "displacement_f1_sxf3"},
        {"lightsource", "lightsource_f1_sxf1"},
        {"lightsource", "lightsource_f1_sxf3"},
        {"surface", "surface_f1_sxf1"},
        {"surface", "surface_f1_sxf3"},
        {"incident", "incident_f1_sxf1"},
        {"incident", "incident_f1_sxf3"},
        {"opposite", "opposite_f1_sxf1"},
        {"opposite", "opposite_f1_sxf3"},
        {"attribute", "attribute_f1_sxf1"},
        {"attribute", "attribute_f1_sxf3"},
        {"rendererinfo", "rendererinfo_f1_sxf1"},
        {"rendererinfo", "rendererinfo_f1_sxf3"},
        {"shadername", "shadername_sx_z0"},
        {"shadername", "shadername_sx_sx"},
        {NULL,NULL},
    };

    static
    bool is_ksl_std_funcs(const std::string& name)
    {
        int i = 0;
        while(RSL_STD_FUNCS[i].name)
        {
            if(RSL_STD_FUNCS[i].name == name)return true;
            i++;
        }
        return false;
    }

    static
    const char* INPUT_VARIABLES[] = 
    {
        "Cs", 
        "Os",
        "P",
        "dPdu", 
        "dPdv",
        "N",  
        "Ng", 
        "u", 
        "v", 
        "du", 
        "dv",  
        "s",
        "t",
        "E",
        "I",
        "P",
        "ncomps", 
        "time",
        "dtime", 
        "dPdtime",
        "PI",
        NULL
    };

    static
    const char* OUTPUT_VARIABLES[] = 
    {
        "Ci", 
        "Oi",
        "P", 
        "N",
        "Cl",
        "Ol",
        NULL
    };

    static
    bool is_input_variable(const std::string& s)
    {
        int i = 0;
        while(INPUT_VARIABLES[i])
        {
            if(s == INPUT_VARIABLES[i])return true;
            i++;
        }
        return false;
    }

    static
    bool is_output_variable(const std::string& s)
    {
        int i = 0;
        while(OUTPUT_VARIABLES[i])
        {
            if(s == OUTPUT_VARIABLES[i])return true;
            i++;
        }
        return false;
    }

    static
    int write_node(int nTab, std::ostream& os, const rslc_node* node);

    static
    void write_node(int nTab, std::ostream& os, const rslc_identifier_node* node)
    {
        os << tabs(nTab) << (const std::string&)(node->get_value());
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_integer_node* node)
    {
        os << tabs(nTab) << (int)(node->get_value());
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_float_node* node)
    {
        os << tabs(nTab) << (float)(node->get_value());
    }   

    static
    void write_node(int nTab, std::ostream& os, const rslc_string_node* node)
    {
        os << tabs(nTab) << "\"" << (const std::string&)(node->get_value()) << "\"";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_texturename_node* node)
    {
        rslc_texturename tex = node->get_value();
        os << tabs(nTab) << "\"" << (const std::string&)(tex.get_name()) << "\"";
        int ch = tex.get_index();
        if(ch >= 0)
        {
            os << "[" << ch << "]";
        }
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_list_node* node)
    {
        int sz = node->get_nodes_size();
        os << tabs(nTab) << "array({";
        for(int i = 0;i<sz;i++)
        {
            write_node(0, os, node->get_node_at(i) );
            if(i != sz-1)
            {
                os << ", ";
            }
        }
        os << "})";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_tuple_node* node)
    {
        int sz = node->get_nodes_size();
        if(sz == 3)
        {
            os << tabs(nTab) << "vec3(";
            for(int i = 0;i<sz;i++)
            {
                write_node(0, os, node->get_node_at(i) );
                if(i != sz-1)
                {
                    os << ", ";
                }
            }
            os << ")";
        }
        else if(sz == 16)
        {
            os << tabs(nTab) << "mat4(";
            for(int i = 0;i<sz;i++)
            {
                write_node(0, os, node->get_node_at(i) );
                if(i != sz-1)
                {
                    os << ", ";
                }
            }
            os << ")";
        }
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_type_node* node)
    {
        std::string tname = (std::string)((node->get_type()).get_name());
        os << tabs(nTab) << tname;
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_variable_def_node* node)
    {
        std::string name = (std::string)node->get_name();
        os << tabs(nTab) << name;
        const rslc_node* dnode = node->get_definition_node();
        if(dnode != NULL)
        {
            os << " = ";
            write_node(0, os, dnode);
        }
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_variable_array_def_node* node)
    {
        std::string name = (const std::string&)node->get_name();
        int size = node->get_size();
        os << tabs(nTab) << name << "[" << size << "]";
        const rslc_node* dnode = node->get_definition_node();
        if(dnode != NULL)
        {
            os << " = ";
            write_node(0, os, dnode);
        }
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_type_variables_def_node* node)
    {
        const rslc_type_variables_def_node* tvnode = node;
        const rslc_type_node* tnode = CAST(const rslc_type_node*, tvnode->get_type_node());
        const rslc_node*      vnode = CAST(const rslc_node*,      tvnode->get_variable_node());

        rslc_type type = tnode->get_type();
        std::string tname = type.get_name();
        
        if(type.find_attribute("extern"))
        {
            os << tabs(nTab) << "--extern";
        }
        else if(vnode)
        {
            int nType = vnode->get_node_type();
            if(nType == RSLC_NODE_VARIABLE_DEF)
            {
                //os << tabs(nTab) << "--" << type << "\n";

                os << tabs(nTab);
                os << "local" << " ";

                const rslc_variable_def_node* vvnode = CAST(const rslc_variable_def_node*,vnode);
                os << (std::string)vvnode->get_name() << " = ";
                if(vvnode->get_definition_node())
                {
                    write_node(0, os, vvnode->get_definition_node());
                }
                else
                {
                    if(tname == "float")
                    {
                        os << "0.0";
                    }
                    else if(tname == "vec3")
                    {
                        os << "vec3(0.0, 0.0, 0.0)";
                    }
                }  
            }
            else if(nType == RSLC_NODE_VARIABLE_ARRAY_DEF)
            {
                //os << tabs(nTab) << "--" << type << "[]\n";

                os << tabs(nTab);
                os << "local" << " ";

                const rslc_variable_array_def_node* vvnode = CAST(const rslc_variable_array_def_node*,vnode);
                os << (std::string)vvnode->get_name() << " = ";
                if(vvnode->get_definition_node())
                {
                    write_node(0, os, vvnode->get_definition_node());
                }
                else
                {
                    size_t sz = vvnode->get_nodes_size();
                    if(tname == "float")
                    {
                        os << "array({";
                        for(size_t i = 0;i<sz;i++)
                        {
                            os << "0.0";
                            if(i != sz-1)
                            {
                                os << ", ";
                            }
                        }
                        os << "})";
                    }
                    else if(tname == "vec3")
                    {
                        os << "array({";
                        for(size_t i = 0;i<sz;i++)
                        {
                            os << "vec3(0.0, 0.0, 0.0)";
                            if(i != sz-1)
                            {
                                os << ", ";
                            }
                        }
                        os << "})";
                    }
                }
            }
        }
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_variable_ref_node* node)
    {
        std::string name = (const std::string&)node->get_name();
        if(is_input_variable(name))
        {
            os << tabs(nTab) << "input:" << name << "()";
        }
        else
        {
            os << tabs(nTab) << name;
        }
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_variable_array_ref_node* node)
    {
        std::string name = (const std::string&)node->get_name();
        os << tabs(nTab) << name << ":get_node_at(";
        write_node(0, os, node->get_index_node());
        os << ")"; 
    }

    static 
    void write_node_args(std::ostream& os, const rslc_list_node* node)
    {
        int sz = (int)node->get_nodes_size();
        for(int i = 0;i<sz;i++)
        {
            write_node(0, os, node->get_node_at(i));
            if(i != sz-1)
            {
                os << ", ";
            }
        }
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_function_call_node* node)
    {
        std::string name = (const std::string&)(node->get_name());
        if(is_ksl_std_funcs(name))
        {
            name = "input:" + name;
        }
        os << tabs(nTab) << name << "(";
           write_node_args(os, (const rslc_list_node*) node->get_arguments_node() );
        os << ")";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_neg_node* node)
    {
        os << tabs(nTab);
        os << "-";
        //os << "(";
        write_node(0, os, node->get_rhs_node());
        //os << ")";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_not_node* node)
    {
        os << tabs(nTab);
        os << "not";
        os << "(";
        write_node(0, os, node->get_rhs_node());
        os << ")";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_add_node* node)
    {
        os << tabs(nTab);
        write_node(0, os, node->get_lhs_node());
        os << " + ";
        write_node(0, os, node->get_rhs_node());
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_sub_node* node)
    {
        os << tabs(nTab);
        write_node(0, os, node->get_lhs_node());
        os << " - ";
        write_node(0, os, node->get_rhs_node());
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_mul_node* node)
    {
        os << tabs(nTab);
        write_node(0, os, node->get_lhs_node());
        os << "*";
        write_node(0, os, node->get_rhs_node());
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_div_node* node)
    {
        os << tabs(nTab);
        write_node(0, os, node->get_lhs_node());
        os << "/";
        write_node(0, os, node->get_rhs_node());
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_crs_node* node)
    {
        os << tabs(nTab);
        os << "cross(";
        write_node(0, os, node->get_lhs_node());
        os << ", ";
        write_node(0, os, node->get_rhs_node());
        os << ")";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_dot_node* node)
    {
        os << tabs(nTab);
        os << "dot(";
        write_node(0, os, node->get_lhs_node());
        os << ", ";
        write_node(0, os, node->get_rhs_node());
        os << ")";
    }

    //---------------------------
    static
    void write_node(int nTab, std::ostream& os, const rslc_lss_node* node)
    {
        os << tabs(nTab);
        write_node(0, os, node->get_lhs_node());
        os << " < ";
        write_node(0, os, node->get_rhs_node());
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_leq_node* node)
    {
        os << tabs(nTab);
        write_node(0, os, node->get_lhs_node());
        os << " <= ";
        write_node(0, os, node->get_rhs_node());
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_gtr_node* node)
    {
        os << tabs(nTab);
        write_node(0, os, node->get_lhs_node());
        os << " > ";
        write_node(0, os, node->get_rhs_node());
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_geq_node* node)
    {
        os << tabs(nTab);
        write_node(0, os, node->get_lhs_node());
        os << " >= ";
        write_node(0, os, node->get_rhs_node());
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_eq_node* node)
    {
        os << tabs(nTab);
        write_node(0, os, node->get_lhs_node());
        os << " == ";
        write_node(0, os, node->get_rhs_node());
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_ne_node* node)
    {
        os << tabs(nTab);
        write_node(0, os, node->get_lhs_node());
        os << " ~= ";
        write_node(0, os, node->get_rhs_node());
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_and_node* node)
    {
        os << tabs(nTab);
        write_node(0, os, node->get_lhs_node());
        os << " and ";
        write_node(0, os, node->get_rhs_node());
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_or_node* node)
    {
        os << tabs(nTab);
        write_node(0, os, node->get_lhs_node());
        os << " or ";
        write_node(0, os, node->get_rhs_node());
    }
    //---------------------------

    static
    void write_node(int nTab, std::ostream& os, const rslc_assign_node* node)
    {
        const rslc_node* ln = node->get_lhs_node();
        const rslc_node* rn = node->get_rhs_node();

        if(ln->get_node_type() == RSLC_NODE_VARIABLE_REF)
        {
            const rslc_variable_ref_node* l = CAST(const rslc_variable_ref_node*, ln);
            std::string name = l->get_name();
            if(is_output_variable(name))
            {
                os << tabs(nTab);
                os << "output:" << name << "(";
                write_node(0, os, rn);
                os << ")";
            }
            else
            {
                os << tabs(nTab);
                write_node(0, os, ln);
                os << " = ";
                write_node(0, os, rn);
            }
        }
        else if(ln->get_node_type() == RSLC_NODE_VARIABLE_ARRAY_REF)
        {
            const rslc_variable_array_ref_node* l = CAST(const rslc_variable_array_ref_node*, ln);

            os << tabs(nTab);
            os << (std::string)l->get_name();
            os << ":set_node_at(";
            write_node(0, os, l->get_index_node());
            os << ", ";
            write_node(0, os, rn);
            os << ")";
        }
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_add_assign_node* node)
    {
        const rslc_node* ln = node->get_lhs_node();
        const rslc_node* rn = node->get_rhs_node();

        if(ln->get_node_type() == RSLC_NODE_VARIABLE_REF)
        {
            const rslc_variable_ref_node* l = CAST(const rslc_variable_ref_node*, ln);
            std::string name = l->get_name();
            if(is_output_variable(name))
            {
                os << tabs(nTab);
                os << "output:" << name << "(";
                write_node(0, os, rn);
                os << ")";
            }
            else
            {
                os << tabs(nTab);
                write_node(0, os, ln);
                os << " = ";
                write_node(0, os, rn);
            }
        }
        else if(ln->get_node_type() == RSLC_NODE_VARIABLE_ARRAY_REF)
        {
            const rslc_variable_array_ref_node* l = CAST(const rslc_variable_array_ref_node*, node->get_lhs_node());
            os << tabs(nTab);
            os << (std::string)l->get_name();
            os << ":add_at(";
            write_node(0, os, l->get_index_node());
            os << ", ";
            write_node(0, os, rn);
            os << ")";
        }
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_sub_assign_node* node)
    {
        const rslc_node* ln = node->get_lhs_node();
        const rslc_node* rn = node->get_rhs_node();

        if(ln->get_node_type() == RSLC_NODE_VARIABLE_REF)
        {
            const rslc_variable_ref_node* l = CAST(const rslc_variable_ref_node*, ln);
            std::string name = l->get_name();
            if(is_output_variable(name))
            {
                os << tabs(nTab);
                os << "output:" << name << "(";
                write_node(0, os, rn);
                os << ")";
            }
            else
            {
                os << tabs(nTab);
                write_node(0, os, ln);
                os << " = ";
                write_node(0, os, rn);
            }
        }
        else if(ln->get_node_type() == RSLC_NODE_VARIABLE_ARRAY_REF)
        {
            const rslc_variable_array_ref_node* l = CAST(const rslc_variable_array_ref_node*, node->get_lhs_node());
            os << tabs(nTab);
            os << (std::string)l->get_name();
            os << ":sub_at(";
            write_node(0, os, l->get_index_node());
            os << ", ";
            write_node(0, os, rn);
            os << ")";
        }
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_mul_assign_node* node)
    {
        const rslc_node* ln = node->get_lhs_node();
        const rslc_node* rn = node->get_rhs_node();

        if(ln->get_node_type() == RSLC_NODE_VARIABLE_REF)
        {
            const rslc_variable_ref_node* l = CAST(const rslc_variable_ref_node*, ln);
            std::string name = l->get_name();
            if(is_output_variable(name))
            {
                os << tabs(nTab);
                os << "output:" << name << "(";
                write_node(0, os, rn);
                os << ")";
            }
            else
            {
                os << tabs(nTab);
                write_node(0, os, ln);
                os << " = ";
                write_node(0, os, rn);
            }
        }
        else if(ln->get_node_type() == RSLC_NODE_VARIABLE_ARRAY_REF)
        {
            const rslc_variable_array_ref_node* l = CAST(const rslc_variable_array_ref_node*, node->get_lhs_node());
            os << tabs(nTab);
            os << (std::string)l->get_name();
            os << ":mul_at(";
            write_node(0, os, l->get_index_node());
            os << ", ";
            write_node(0, os, rn);
            os << ")";
        }
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_div_assign_node* node)
    {
        const rslc_node* ln = node->get_lhs_node();
        const rslc_node* rn = node->get_rhs_node();

        if(ln->get_node_type() == RSLC_NODE_VARIABLE_REF)
        {
            const rslc_variable_ref_node* l = CAST(const rslc_variable_ref_node*, ln);
            std::string name = l->get_name();
            if(is_output_variable(name))
            {
                os << tabs(nTab);
                os << "output:" << name << "(";
                write_node(0, os, rn);
                os << ")";
            }
            else
            {
                os << tabs(nTab);
                write_node(0, os, ln);
                os << " = ";
                write_node(0, os, rn);
            }
        }
        else if(ln->get_node_type() == RSLC_NODE_VARIABLE_ARRAY_REF)
        {
            const rslc_variable_array_ref_node* l = CAST(const rslc_variable_array_ref_node*, node->get_lhs_node());
            os << tabs(nTab);
            os << (std::string)l->get_name();
            os << ":div_at(";
            write_node(0, os, l->get_index_node());
            os << ", ";
            write_node(0, os, rn);
            os << ")";
        }
    }

    //---------------------------

    static
    void write_node(int nTab, std::ostream& os, const rslc_cond_node* node)
    {
        os << tabs(nTab);
        os << "(";
        write_node(0, os, node->get_condition_node());
        os << ")";
        os << " and ";
        os << "(";
        write_node(0, os, node->get_expression1_node());
        os << ")";
        os << " or ";
        os << "(";
        write_node(0, os, node->get_expression2_node());
        os << ")";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_cast_node* node)
    {
        std::string type = (const std::string&)(node->get_type_node()->get_type().get_name());
        std::string ns = node->get_namespace();
        if(ns == "")
        {
            os << tabs(nTab) << type << "(";
            write_node(0, os, node->get_expression_node());
            os << ")";
        }
        else
        {
            if(type == "vec3")
            {
                os << tabs(nTab) << "input:" << "transform" << "(" << "\"" << ns << "\"" << ", ";
                write_node(0, os, node->get_expression_node());
                os << ")";
            }
        }
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_paren_node* node)
    {
        os << tabs(nTab) << "(";
        write_node(0, os, node->get_contents_node());
        os << ")";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_statement_node* node)
    {
        write_node(nTab, os, node->get_contents_node());
        os << "\n";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_block_node* node)
    {
        os << tabs(nTab) << "do\n";
        int sz = node->get_nodes_size();
        for(int i = 0;i<sz;i++)
        {
            write_node(nTab+1, os, node->get_node_at(i) );
        }
        os << tabs(nTab) << "end\n";
    }
    
    //---------------------------

    static
    void write_node_condition(int nTab, std::ostream& os, const rslc_list_node* node)
    {
        os << tabs(nTab);
        int sz = node->get_nodes_size();
        for(int i = 0;i<sz;i++)
        {
            write_node(0, os, node->get_node_at(i) );
            if(i != sz-1)
            {
                os << ", ";
            }
        }
    }

    static
    void write_node_block(int nTab, std::ostream& os, const rslc_node* node)
    {
        if(node->get_node_type() == RSLC_NODE_BLOCK)
        {
            const rslc_block_node* l = CAST(const rslc_block_node*, node);
            //os << tabs(nTab) << "{\n";
            size_t sz = l->get_nodes_size();
            for(size_t i = 0;i<sz;i++)
            {
                write_node(nTab+1, os, l->get_node_at(i));
            }
            //os << tabs(nTab) << "}";
        }
        else
        {
            //os << tabs(nTab) << "{\n";
            write_node(nTab+1, os, node);
            //os << tabs(nTab) << "}";
        }
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_while_node* node)
    {
        os << tabs(nTab);
        os << "while" << " ";
        write_node_condition(0, os, node->get_condition_node());
        os << " ";
        os << "do";
        os << "\n";
        const rslc_node* c = node->get_contents_node();
        write_node_block(nTab, os, c);
        os << tabs(nTab) << "end";
        os << "\n";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_for_node* node)
    {
        
        if(false)
        {
            /*
            os << tabs(nTab);
            os << "for" << " ";
                os << "i = 0, sz-1";
                //write_node_condition(0, os, node->get_condition_node());
            os << " ";
            os << "do";
            os << "\n";
            const rslc_node* c = node->get_contents_node();
            write_node_block(nTab, os, c);
            os << tabs(nTab) << "end";
            os << "\n";
            */
        }
        else
        {
            os << tabs(nTab) << "do\n";
                const rslc_node* cond = node->get_condition_node();
                write_node(nTab+1, os, cond->get_node_at(0));
                os << "\n";
                os << tabs(nTab+1) << "while" << " ";
                write_node(0, os, cond->get_node_at(1));
                os << " " << "do\n";

                const rslc_node* c = node->get_contents_node();
                write_node_block(nTab+1, os, c);

                write_node(nTab+2, os, cond->get_node_at(2));
                os << "\n";
                os << tabs(nTab+1) << "end\n";
            os << tabs(nTab) << "end\n";
        }
        
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_solar_node* node)
    {
        os << tabs(nTab);
        os << "solar" << " ";
            write_node_condition(0, os, node->get_condition_node());
        os << " ";
        os << "do";
        os << "\n";
        const rslc_node* c = node->get_contents_node();
        write_node_block(nTab, os, c);
        os << tabs(nTab) << "end";
        os << "\n";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_illuminate_node* node)
    {
        os << tabs(nTab);
        os << "illuminate" << " ";
            write_node_condition(0, os, node->get_condition_node());
        os << " ";
        os << "do";
        os << "\n";
        const rslc_node* c = node->get_contents_node();
        write_node_block(nTab, os, c);
        os << tabs(nTab) << "end";
        os << "\n";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_illuminance_node* node)
    {
        os << tabs(nTab);
        os << "illuminance" << " ";
            write_node_condition(0, os, node->get_condition_node());
        os << " ";
        os << "do";
        os << "\n";
        const rslc_node* c = node->get_contents_node();
        write_node_block(nTab, os, c);
        os << tabs(nTab) << "end";
        os << "\n";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_gather_node* node)
    {
        os << tabs(nTab);
        os << "gather" << " ";
            write_node_condition(0, os, node->get_condition_node());
        os << " ";
        os << "do";
        os << "\n";
        const rslc_node* c = node->get_contents_node();
        write_node_block(nTab, os, c);
        os << tabs(nTab) << "end";
        os << "\n";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_if_node* node)
    {
        os << tabs(nTab);
        os << "if" << " ";
            write_node(0, os, node->get_condition_node());
        os << " ";
        os << "then";
        os << "\n";
        const rslc_node* c = node->get_contents_node();
        const rslc_node* e = node->get_else_node();
        if(!c && !e)
        {
            os << tabs(nTab) << "end";
            os << "\n";
        }
        else if(c && !e)
        {
            write_node_block(nTab, os, c);
            os << tabs(nTab) << "end";
            os << "\n";
        }
        else
        {
            write_node_block(nTab, os, c);
            os << tabs(nTab) << "else";
            os << "\n";
            write_node_block(nTab, os, e);
            os << tabs(nTab) << "end";
            os << "\n";
        }
    }

    //---------------------------

    static
    void write_node(int nTab, std::ostream& os, const rslc_return_node* node)
    {
        os << tabs(nTab) << "return";
        if(node->get_expression_node())
        {
            os << " ";
            write_node(0, os, node->get_expression_node());
        }
        os << "\n";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_break_node* node)
    {
        os << tabs(nTab) << "break\n";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_continue_node* node)
    {
        os << tabs(nTab) << "continue\n";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_shader_def_node* node)
    {
        const rslc_type_node* tnode = CAST(const rslc_type_node*, node->get_type_node());
        std::string ftype = tnode->get_type().get_name();
        std::string fname = node->get_name();
        os << tabs(nTab) << "--" << ftype << " " << fname << "\n";
        os << tabs(nTab) << "function" << " " << ftype << "(";
            os << "output, input";
        os << ")" << "\n";

        {
            const rslc_list_node* pnode = CAST(const rslc_list_node*, node->get_arguments_node());
            size_t sz = pnode->get_nodes_size();
            if(sz)
            {
                for(size_t i = 0;i < sz; i++)
                {
                    const rslc_type_variables_def_node* tvnode = CAST(const rslc_type_variables_def_node*, pnode->get_node_at(i));
                    const rslc_type_node* tnode = CAST(const rslc_type_node*, tvnode->get_type_node());
                    const rslc_variable_def_node* vnode = CAST(const rslc_variable_def_node*, tvnode->get_variable_node());
                    //assert(vnode != NULL);
                    if(vnode)
                    {
                        rslc_type t = tnode->get_type();
                        os << tabs(nTab+1) << "local " << (std::string)vnode->get_name() << " = ";
                        os << "input:get_" << t.get_name() << "(";
                        os << "\"" << (std::string)vnode->get_name() << "\"" << ", ";
                        if(vnode->get_definition_node())
                        {
                            write_node(0, os, vnode->get_definition_node());
                        }
                        os << ")";
                        os << "\n";
                    }
                }
                os << tabs(nTab+1) << "\n";
            }
        }

        {
            const rslc_list_node* pnode = CAST(const rslc_list_node*, node->get_contents_node());
            size_t sz = pnode->get_nodes_size();
            for(size_t i = 0; i < sz; i++)
            {
                write_node(nTab+1, os, pnode->get_node_at(i));
            }
        }

        os << tabs(nTab) << "end\n";
    }

    static
    void write_node(int nTab, std::ostream& os, const rslc_function_def_node* node)
    {
        const rslc_type_node* tnode = CAST(const rslc_type_node*, node->get_type_node());
        std::string ftype = tnode->get_type().get_name();
        std::string fname = node->get_name();
        os << tabs(nTab) << "--" << ftype << " " << fname << "\n";
        os << tabs(nTab) << "function" << " " << fname << "(";
        {
            const rslc_list_node* pnode = CAST(const rslc_list_node*, node->get_arguments_node());
            size_t sz = pnode->get_nodes_size();
            if(sz)
            {
                for(size_t i = 0;i < sz; i++)
                {
                    const rslc_type_variables_def_node* tvnode = CAST(const rslc_type_variables_def_node*, pnode->get_node_at(i));
                    const rslc_type_node* tnode = CAST(const rslc_type_node*, tvnode->get_type_node());
                    const rslc_variable_def_node* vnode = CAST(const rslc_variable_def_node*, tvnode->get_variable_node());
                    if(vnode)
                    {
                        rslc_type t = tnode->get_type();
                        os << (std::string)vnode->get_name();
                        if(i != sz-1)
                        {
                            os << ", ";
                        }
                    }
                }
            }
        }
        os << ")" << "\n";
        {
            const rslc_list_node* pnode = CAST(const rslc_list_node*, node->get_contents_node());
            size_t sz = pnode->get_nodes_size();
            for(size_t i = 0; i < sz; i++)
            {
                write_node(nTab+1, os, pnode->get_node_at(i));
            }
        }

        os << tabs(nTab) << "end\n";
    }

    //---------------------------

    static
    int write_node(int nTab, std::ostream& os, const rslc_definitions_node* node)
    {
        int sz = node->get_nodes_size();
        for(int i = 0;i<sz;i++)
        {
            write_node(nTab, os, node->get_node_at(i) );
            os << tabs(nTab) << "\n";
        }
        return 0;
    }

    static
    int write_node(int nTab, std::ostream& os, const rslc_node* node)
    {
        int nType = node->get_node_type();
        switch(nType)
        {
            case RSLC_NODE_IDENTIFIER:
                write_node(nTab, os, CAST(const rslc_identifier_node*,node));break;
            case RSLC_NODE_INTEGER:
                write_node(nTab, os, CAST(const rslc_integer_node*,node));break;
            case RSLC_NODE_FLOAT:
                write_node(nTab, os, CAST(const rslc_float_node*,node));break;
            case RSLC_NODE_STRING:
                write_node(nTab, os, CAST(const rslc_string_node*,node));break;
            case RSLC_NODE_TEXTURENAME:
                write_node(nTab, os, CAST(const rslc_texturename_node*,node));break;

            case RSLC_NODE_LIST:
                write_node(nTab, os, CAST(const rslc_list_node*,node));break;
            case RSLC_NODE_TUPLE:
                write_node(nTab, os, CAST(const rslc_tuple_node*,node));break;
            
            case RSLC_NODE_TYPE:
                write_node(nTab, os, CAST(const rslc_type_node*,node));break;
            case RSLC_NODE_VARIABLE_REF:
                write_node(nTab, os, CAST(const rslc_variable_ref_node*,node));break;
            case RSLC_NODE_VARIABLE_ARRAY_REF:
                write_node(nTab, os, CAST(const rslc_variable_array_ref_node*,node));break;
            case RSLC_NODE_FUNCTION_CALL:
                write_node(nTab, os, CAST(const rslc_function_call_node*,node));break;
            
            case RSLC_NODE_NEG:
                write_node(nTab, os, CAST(const rslc_neg_node*,node));break;
            case RSLC_NODE_NOT:
                write_node(nTab, os, CAST(const rslc_not_node*,node));break;
            case RSLC_NODE_ADD:
                write_node(nTab, os, CAST(const rslc_add_node*,node));break;
            case RSLC_NODE_SUB:
                write_node(nTab, os, CAST(const rslc_sub_node*,node));break;
            case RSLC_NODE_MUL:
                write_node(nTab, os, CAST(const rslc_mul_node*,node));break;
            case RSLC_NODE_DIV:
                write_node(nTab, os, CAST(const rslc_div_node*,node));break;
            case RSLC_NODE_CRS:
                write_node(nTab, os, CAST(const rslc_crs_node*,node));break;
            case RSLC_NODE_DOT:
                write_node(nTab, os, CAST(const rslc_dot_node*,node));break;

            case RSLC_NODE_LSS:
                write_node(nTab, os, CAST(const rslc_lss_node*,node));break;
            case RSLC_NODE_LEQ:
                write_node(nTab, os, CAST(const rslc_leq_node*,node));break;
            case RSLC_NODE_GTR:
                write_node(nTab, os, CAST(const rslc_gtr_node*,node));break;
            case RSLC_NODE_GEQ:
                write_node(nTab, os, CAST(const rslc_geq_node*,node));break;
            case RSLC_NODE_EQ:
                write_node(nTab, os, CAST(const rslc_eq_node*,node));break;
            case RSLC_NODE_NE:
                write_node(nTab, os, CAST(const rslc_ne_node*,node));break;

            case RSLC_NODE_AND:
                write_node(nTab, os, CAST(const rslc_and_node*,node));break;
            case RSLC_NODE_OR:
                write_node(nTab, os, CAST(const rslc_or_node*,node));break;

            case RSLC_NODE_ASSIGN:
                write_node(nTab, os, CAST(const rslc_assign_node*,node));break;
            case RSLC_NODE_ADD_ASSIGN:
                write_node(nTab, os, CAST(const rslc_add_assign_node*,node));break;
            case RSLC_NODE_SUB_ASSIGN:
                write_node(nTab, os, CAST(const rslc_sub_assign_node*,node));break;
            case RSLC_NODE_MUL_ASSIGN:
                write_node(nTab, os, CAST(const rslc_mul_assign_node*,node));break;
            case RSLC_NODE_DIV_ASSIGN:
                write_node(nTab, os, CAST(const rslc_div_assign_node*,node));break;

            case RSLC_NODE_COND:
                write_node(nTab, os, CAST(const rslc_cond_node*,node));break;
            case RSLC_NODE_CAST:
                write_node(nTab, os, CAST(const rslc_cast_node*,node));break;

            case RSLC_NODE_STATEMENT:
                write_node(nTab, os, CAST(const rslc_statement_node*,node));break;

            case RSLC_NODE_BLOCK:
                write_node(nTab, os, CAST(const rslc_block_node*,node));break;

            case RSLC_NODE_WHILE:
                write_node(nTab, os, CAST(const rslc_while_node*,node));break;
            case RSLC_NODE_FOR:
                write_node(nTab, os, CAST(const rslc_for_node*,node));break;
            case RSLC_NODE_SOLAR:
                write_node(nTab, os, CAST(const rslc_solar_node*,node));break;
            case RSLC_NODE_ILLUMINATE:
                write_node(nTab, os, CAST(const rslc_illuminate_node*,node));break;
            case RSLC_NODE_ILLUMINANCE:
                write_node(nTab, os, CAST(const rslc_illuminance_node*,node));break;
            case RSLC_NODE_GATHER:
                write_node(nTab, os, CAST(const rslc_gather_node*,node));break;

            case RSLC_NODE_IF:
                write_node(nTab, os, CAST(const rslc_if_node*,node));break;

            case RSLC_NODE_RETURN:
                write_node(nTab, os, CAST(const rslc_return_node*,node));break;
            case RSLC_NODE_BREAK:
                write_node(nTab, os, CAST(const rslc_break_node*,node));break;
            case RSLC_NODE_CONTINUE:
                write_node(nTab, os, CAST(const rslc_continue_node*,node));break;
     
            case RSLC_NODE_VARIABLE_DEF:
                write_node(nTab, os, CAST(const rslc_variable_def_node*,node));break;
            case RSLC_NODE_VARIABLE_ARRAY_DEF:
                write_node(nTab, os, CAST(const rslc_variable_array_def_node*,node));break;
            case RSLC_NODE_TYPE_VARIABLES_DEF:
                write_node(nTab, os, CAST(const rslc_type_variables_def_node*,node));break;

            case RSLC_NODE_FUNCTION_DEF:
                write_node(nTab, os, CAST(const rslc_function_def_node*,node));break;
            case RSLC_NODE_SHADER_DEF:
                write_node(nTab, os, CAST(const rslc_shader_def_node*,node));break;

            case RSLC_NODE_DEFINITIONS:
                write_node(nTab, os, CAST(const rslc_definitions_node*,node));break;

            case RSLC_NODE_PAREN:
                write_node(nTab, os, CAST(const rslc_paren_node*,node));break;

            default:
                break;
        }
        return 0;
    }

    static
    int write_header_comment(std::ostream& os, const rslc_node* node)
    {
        os << "-----------------------------------" << "\n";
        os << "--\"Generator\" : \"KzRSLCompiler\"" << "\n";
        os << "--\"GeneratorVersion\" : " << "1.0" << "\n";
        os << "--\"CodeType\" : " << "\"ksl\"" << "\n";
        os << "--\"GenerateDate\" : " << 12345678 << "\n";
        os << "-----------------------------------" << "\n";
        os << "\n";
        return 0;
    }

    static
    int write_ksl_module(std::ostream& os, const rslc_node* node)
    {
        //os << RSLC_KSL_ARRAY_STRING;
        os << "\n";

        return 0;
    }
    
    static
    int write_header(std::ostream& os, const rslc_node* node)
    {
        {
            int nRet = write_header_comment(os, node);
            if(nRet != 0)return nRet;
        }
        {
            int nRet = write_ksl_module(os, node);
            if(nRet != 0)return nRet;
        }
        return 0;
    }

    static
    int write_footer(std::ostream& os, const rslc_node* node)
    {
        os << "\n";
        return 0;
    }

    int rslc_write_ksl(std::ostream& os, const rslc_node* node)
    {
        {
            int nRet = write_header(os, node);
            if(nRet != 0)return nRet;
        }
        {
            int nRet = write_node(0, os, node);
            if(nRet != 0)return nRet;
        }
        {
            int nRet = write_footer(os, node);
            if(nRet != 0)return nRet;
        }
        return 0;
    }

}
