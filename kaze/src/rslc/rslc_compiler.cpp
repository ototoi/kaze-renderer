#include "rslc_compiler.h"
#include "rslc_context.h"
#include "rslc_decoder.h"

#include <memory>
#include "rslc_optimize_opt1.h"
#include "rslc_optimize_ksl.h"
#include "rslc_write_clike.h"
#include "rslc_write_ksl.h"


namespace rslc
{
    extern int load_rsl(rslc_context* ctx, rslc_decoder* dec, const char* szFileName);

    int rslc_compiler::compile(const char* szFile)
    {
        int nRet = 0;
        std::shared_ptr<rslc_context> ctx(new rslc_context());
        {
            FILE* fp = fopen(szFile, "rt");
            if(fp == NULL)return -1;
            std::shared_ptr<rslc_decoder> dec(new rslc_decoder(fp));
            nRet = load_rsl(ctx.get(), dec.get(), szFile);
            if(fp)fclose(fp);
            if(nRet != 0)return nRet;
        }
        {
            nRet = analyze(ctx.get());
            if(nRet != 0)return nRet;
        }
        {
            nRet = optimize(ctx.get());
            if(nRet != 0)return nRet;
        }
        {
            nRet = output(ctx.get());
            if(nRet != 0)return nRet;
        }
        return nRet;
    }

    int rslc_compiler::analyze(rslc_context* ctx)
    {
        rslc_node* root = ctx->get_root_node();
        if(!root)return -1;
        {
            //nRet = rslc_analyze_symbols(ctx, root);
            //if(nRet != 0)return nRet;
        }

        return 0;
    }

    int rslc_compiler::optimize(rslc_context* ctx)
    {
        rslc_node* root = ctx->get_root_node();
        if(!root)return -1;

        //rslc_write_clike(std::cout, root);
        rslc_optimize_func_args(ctx, root);
        //rslc_write_clike(std::cout, root);
        rslc_optimize_ksl(ctx, root);
        rslc_write_clike(std::cout, root);

        return 0;
    }

    int rslc_compiler::output(rslc_context* ctx)
    {
        rslc_node* root = ctx->get_root_node();
        if(!root)return -1;
        {
            int nRet = rslc_write_ksl(std::cout, root);
            if(nRet != 0)return nRet;
        }
        return 0;
    }

}

