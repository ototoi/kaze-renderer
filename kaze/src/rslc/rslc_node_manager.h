#ifndef RSLC_NODE_MANAGER_H
#define RSLC_NODE_MANAGER_H

#include "rslc_node.h"
#include <vector>
#include <map>

namespace rslc
{
    class rslc_node_manager
    {
    public:
        typedef std::map<rslc_node*, std::shared_ptr<rslc_node> > map_type;
    public:
        void add_node(const std::shared_ptr<rslc_node>& node);
        void remove_node(rslc_node* node);
        std::shared_ptr<rslc_node> find_node(rslc_node* node)const;
    protected:
        map_type nodes_;
    };
}

#endif
