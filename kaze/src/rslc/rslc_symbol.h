#ifndef RSLC_SYMBOL_H
#define RSLC_SYMBOL_H

#include "rslc_type.h"
#include "rslc_variable.h"
#include "rslc_function.h"

#include <string>

namespace rslc
{
    class rslc_symbol_base
    {
    public:
        enum 
        {
            RSLC_SYMBOL_TYPE = 1,
            RSLC_SYMBOL_VARIABLE = 2,
            RSLC_SYMBOL_FUNCTION = 3,
        };
    public:
        virtual ~rslc_symbol_base(){}
        virtual int get_symbol_type()const = 0;
        virtual const rslc_type& get_type()const = 0;
        virtual const std::string& get_name()const = 0;
    };

    class rslc_symbol_type : public rslc_symbol_base
    {
    public:
        rslc_symbol_type(const rslc_type& type)
            :type_(type)
        {}
        virtual int get_symbol_type()const{ return RSLC_SYMBOL_TYPE; }
        virtual const rslc_type& get_type()const{ return type_; }
        virtual const std::string& get_name()const{ return type_.get_name(); }
    protected:
        rslc_type type_;
    };

    class rslc_symbol_variable : public rslc_symbol_base
    {
    public:
        rslc_symbol_variable(const rslc_type& type, const std::string& name)
            :var_(rslc_variable(type, name))
        {}
        rslc_symbol_variable(const rslc_variable& var)
            :var_(var)
        {}
        virtual int get_symbol_type()const{ return RSLC_SYMBOL_VARIABLE; }
        virtual const rslc_type& get_type()const{ return var_.get_type(); }
        virtual const std::string& get_name()const{ return var_.get_name(); }
    protected:
        rslc_variable var_;
    };

    class rslc_symbol_function : public rslc_symbol_base
    {
    public:
        rslc_symbol_function(const rslc_function& fun)
            :fun_(fun)
        {}
        virtual int get_symbol_type()const{ return RSLC_SYMBOL_FUNCTION; }
        virtual const rslc_type& get_type()const{ return fun_.get_type(); }
        virtual const std::string& get_name()const{ return fun_.get_name(); }
    public:
        const rslc_function& get_function()const{return fun_; }
    protected:
        rslc_function fun_;
    };

    class rslc_symbol
    {
    public:
        enum 
        {
            RSLC_SYMBOL_TYPE = 1,
            RSLC_SYMBOL_VARIABLE = 2,
            RSLC_SYMBOL_FUNCTION = 3,
        };
    public:
        rslc_symbol(const rslc_type& type)
        {
            base_.reset( new rslc_symbol_type(type) );
        }
        rslc_symbol(const rslc_type& type, const std::string& name)
        {
            base_.reset( new rslc_symbol_variable(type, name) );
        }
        rslc_symbol(const rslc_variable& var)
        {
            base_.reset( new rslc_symbol_variable(var) );
        }
        rslc_symbol(const rslc_function& fun)
        {
            base_.reset( new rslc_symbol_function(fun) );
        }

        int get_symbol_type()const
        {
            return base_->get_symbol_type();
        }

        const rslc_type& get_type()const
        {
            return base_->get_type();
        }

        const std::string& get_name()const
        {
            return base_->get_name();
        }

        const rslc_symbol_base* get_ptr()const
        {
            return base_.get();
        }
    private:
        std::shared_ptr<rslc_symbol_base> base_;
    };
}

#endif