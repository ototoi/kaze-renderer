#include "rslc_optimize_ksl.h"
#include "rslc_context.h"
#include "rslc_node.h"
#include "rslc_node_walker.h"
#include "rslc_symbol_manager.h"

#include <iostream>
#include <vector>
#include <cassert>

#include <iostream>

#define CAST(T, n) check_cast<T>(n)

namespace rslc
{
    template<class T, class B>
    T check_cast(B b)
    {
        T t = dynamic_cast<T>(b);
        return t;
    }

namespace
{
    typedef struct shader_variable_def
    {
        const char* name;
        const char* type;
        const char* attribute;
    } shader_variable_def;

    static
    const shader_variable_def GLOBAL_VARIABLES[] = 
    {
        {"PI", "float", "uniform"},
        {NULL, NULL, NULL},
    };

    static
    const shader_variable_def  SURFACE_SHADER_VARIABLES[] = 
    {
        {"Cs", "color", "varying"},  
        {"Os", "color", "varying"},  
        {"P", "point", "varying"},  
        {"dPdu", "vector", "varying"},  
        {"dPdv", "vector", "varying"},  
        {"N", "normal", "varying"},  
        {"Ng", "normal", "varying"},  
        {"u", "float", "varying"},  
        {"v", "float", "varying"},  
        {"du", "float", "varying"},  
        {"dv", "float", "varying"},  
        {"s", "float", "varying"},  
        {"t", "float", "varying"},  
        {"L", "vector", "varying"},  
        {"Cl", "color", "varying"},  
        {"Ol", "color", "varying"},  
        {"E", "point", "uniform"},  
        {"I", "vector", "varying"},  
        {"ncomps", "float", "uniform"},  
        {"time", "float", "uniform"},  
        {"dtime", "float", "uniform"},  
        {"dPdtime", "vector", "varying"},  
        {"Ci", "color", "varying"},
        {"Oi", "color", "varying"}, 

        {NULL, NULL, NULL},  
    };

    static
    const shader_variable_def  LIGHT_SHADER_VARIABLES[] = 
    { 
        {"P", "point", "varying"},  
        {"dPdu", "vector", "varying"},  
        {"dPdv", "vector", "varying"},  
        {"N", "normal", "varying"},  
        {"Ng", "normal", "varying"},  
        {"u", "float", "varying"},  
        {"v", "float", "varying"},  
        {"du", "float", "varying"},  
        {"dv", "float", "varying"},  
        {"s", "float", "varying"},  
        {"t", "float", "varying"},  
        {"L", "vector", "varying"}, 

        {"Cl", "color", "varying"},  
        {"Ol", "color", "varying"},  
        
        {"Ps", "point", "varying"}, 

        {"E", "point", "uniform"},  
       
        {"ncomps", "float", "uniform"},  
        {"time", "float", "uniform"},  
        {"dtime", "float", "uniform"},  
      
        {"Ci", "color", "varying"},
        {"Oi", "color", "varying"}, 

        {NULL, NULL, NULL},  
    };

    static
    const shader_variable_def  VOLUME_SHADER_VARIABLES[] = 
    {
        {"P", "point", "varying"},

        {"E", "point", "uniform"},  
        {"I", "vector", "varying"},  
        {"ncomps", "float", "uniform"},  
        {"time", "float", "uniform"},  
        {"dtime", "float", "uniform"},  
       
        {"Ci", "color", "varying"},
        {"Oi", "color", "varying"}, 

        {NULL, NULL, NULL},  
    };

    static
    const shader_variable_def  DISPLACEMENT_SHADER_VARIABLES[] = 
    {
        //{"Cs", "color", "varying"},  
        //{"Os", "color", "varying"},  
        {"P", "point", "varying"},  
        {"dPdu", "vector", "varying"},  
        {"dPdv", "vector", "varying"},  
        {"N", "normal", "varying"},  
        {"Ng", "normal", "varying"},  
        {"E", "point", "uniform"},  
        {"I", "vector", "varying"},
        {"u", "float", "varying"},  
        {"v", "float", "varying"},  
        {"du", "float", "varying"},  
        {"dv", "float", "varying"},  
        {"s", "float", "varying"},  
        {"t", "float", "varying"},  
        //{"L", "vector", "varying"},  
        //{"Cl", "color", "varying"},  
        //{"Ol", "color", "varying"},  
        {"ncomps", "float", "uniform"},  
        {"time", "float", "uniform"},  
        {"dtime", "float", "uniform"},  
        {"dPdtime", "vector", "varying"},
        //{"Ci", "color", "varying"},
        //{"Oi", "color", "varying"},  

        {NULL, NULL, NULL},  
    };

    static
    const shader_variable_def  IMAGER_SHADER_VARIABLES[] = 
    { 
        {"P", "point", "varying"},  
        {"alpha", "float", "uniform"},  
        {"ncomps", "float", "uniform"},  
        {"time", "float", "uniform"},
        {"dtime", "float", "uniform"},  
        {"Ci", "color", "varying"},
        {"Oi", "color", "varying"}, 

        {NULL, NULL, NULL},  
    };

    static
    std::string replace_type(const std::string& name)
    {
        if(
            name == "point" ||
            name == "vector" || 
            name == "normal" || 
            name == "color" 
        )
        {
            return "vec3";
        }
        else if
        (
            name == "matrix"
        )
        {
            return "mat4";
        }
        return name;
    }

    static
    int add_variable_symbol(rslc_symbol_manager* sm, const shader_variable_def& def)
    {
        rslc_type t(def.type);
        t.add_attribute(def.attribute);

        std::shared_ptr<rslc_symbol> sym( new rslc_symbol(rslc_variable(t, def.name)) );
        if(!sm->add_symbol(sym))return -1;
        return 0;
    }

    static
    int add_shader_variable_symbols(rslc_symbol_manager* sm, const rslc_shader_def_node* node)
    {
        const rslc_type_node* tnode = CAST(const rslc_type_node*, node->get_type_node());
        std::string type = tnode->get_type().get_name();
        if(type == "surface")
        {
            int i = 0;
            while(SURFACE_SHADER_VARIABLES[i].name)
            {
                int nRet = add_variable_symbol(sm, SURFACE_SHADER_VARIABLES[i]);
                if(nRet != 0)return nRet;
                i++;
            }
        }
        else if(type == "light")
        {
            int i = 0;
            while(LIGHT_SHADER_VARIABLES[i].name)
            {
                int nRet = add_variable_symbol(sm, LIGHT_SHADER_VARIABLES[i]);
                if(nRet != 0)return nRet;
                i++;
            }
        }
        else if(type == "volume")
        {
            int i = 0;
            while(VOLUME_SHADER_VARIABLES[i].name)
            {
                int nRet = add_variable_symbol(sm, VOLUME_SHADER_VARIABLES[i]);
                if(nRet != 0)return nRet;
                i++;
            }
        }
        else if(type == "displacement")
        {
            int i = 0;
            while(DISPLACEMENT_SHADER_VARIABLES[i].name)
            {
                int nRet = add_variable_symbol(sm, DISPLACEMENT_SHADER_VARIABLES[i]);
                if(nRet != 0)return nRet;
                i++;
            }
        }
        else if(type == "imager")
        {
            int i = 0;
            while(IMAGER_SHADER_VARIABLES[i].name)
            {
                int nRet = add_variable_symbol(sm, IMAGER_SHADER_VARIABLES[i]);
                if(nRet != 0)return nRet;
                i++;
            }
        }
        return 0;
    }

    static
    int add_global_variable_symbols(rslc_symbol_manager* sm)
    {
        int i = 0;
        while(GLOBAL_VARIABLES[i].name)
        {
            int nRet = add_variable_symbol(sm, GLOBAL_VARIABLES[i]);
            if(nRet != 0)return nRet;
            i++;
        }
        return 0;
    }

    static
    int add_std_functions(rslc_symbol_manager* sm)
    {
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "radians", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "degrees", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "sin", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "asin", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "cos", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "acos", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "tan", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "atan", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            vars.push_back(rslc_variable(rslc_type("float"), "x1"));
            rslc_function fun(rslc_type("float"), "atan", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            vars.push_back(rslc_variable(rslc_type("float"), "x1"));
            rslc_function fun(rslc_type("float"), "pow", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "exp", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "sqrt", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "inversesqrt", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "log", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            vars.push_back(rslc_variable(rslc_type("float"), "x1"));
            rslc_function fun(rslc_type("float"), "log", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            vars.push_back(rslc_variable(rslc_type("float"), "x1"));
            rslc_function fun(rslc_type("float"), "mod", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "abs", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "sign", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("array<float>"), "x0"));
            rslc_function fun(rslc_type("float"), "min", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            vars.push_back(rslc_variable(rslc_type("float"), "x1"));
            rslc_function fun(rslc_type("float"), "min", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            rslc_function fun(rslc_type("vector"), "min", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            vars.push_back(rslc_variable(rslc_type("float"), "x1"));
            rslc_function fun(rslc_type("float"), "max", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            rslc_function fun(rslc_type("vector"), "max", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            vars.push_back(rslc_variable(rslc_type("float"), "x1"));
            vars.push_back(rslc_variable(rslc_type("float"), "x2"));
            rslc_function fun(rslc_type("float"), "clamp", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x2"));
            rslc_function fun(rslc_type("vector"), "clamp", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            vars.push_back(rslc_variable(rslc_type("float"), "x1"));
            vars.push_back(rslc_variable(rslc_type("float"), "x2"));
            rslc_function fun(rslc_type("float"), "mix", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            vars.push_back(rslc_variable(rslc_type("float"), "x2"));
            rslc_function fun(rslc_type("vector"), "mix", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "floor", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "ceil", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            rslc_function fun(rslc_type("float"), "round", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            vars.push_back(rslc_variable(rslc_type("float"), "x1"));
            rslc_function fun(rslc_type("float"), "step", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("float"), "x0"));
            vars.push_back(rslc_variable(rslc_type("float"), "x1"));
            vars.push_back(rslc_variable(rslc_type("float"), "x2"));
            rslc_function fun(rslc_type("float"), "smoothstep", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            rslc_function fun(rslc_type("float"), "xcomp", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            rslc_function fun(rslc_type("float"), "ycomp", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            rslc_function fun(rslc_type("float"), "zcomp", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            rslc_function fun(rslc_type("vector"), "faceforward", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x2"));
            rslc_function fun(rslc_type("vector"), "faceforward", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            rslc_function fun(rslc_type("vector"), "reflect", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            vars.push_back(rslc_variable(rslc_type("float"), "x2"));
            rslc_function fun(rslc_type("vector"), "refract", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            vars.push_back(rslc_variable(rslc_type("float"), "x2"));
            vars.push_back(rslc_variable(rslc_type("float"), "x3"));
            vars.push_back(rslc_variable(rslc_type("float"), "x4"));
            rslc_function fun(rslc_type("void"), "fresnel", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("string"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            rslc_function fun(rslc_type("vector"), "transform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("string"), "x0"));
            vars.push_back(rslc_variable(rslc_type("string"), "x1"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x2"));
            rslc_function fun(rslc_type("vector"), "transform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("matrix"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            rslc_function fun(rslc_type("vector"), "transform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("string"), "x0"));
            vars.push_back(rslc_variable(rslc_type("matrix"), "x1"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x2"));
            rslc_function fun(rslc_type("vector"), "transform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("string"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            rslc_function fun(rslc_type("vector"), "vtransform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("string"), "x0"));
            vars.push_back(rslc_variable(rslc_type("string"), "x1"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x2"));
            rslc_function fun(rslc_type("vector"), "vtransform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("matrix"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            rslc_function fun(rslc_type("vector"), "vtransform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("string"), "x0"));
            vars.push_back(rslc_variable(rslc_type("matrix"), "x1"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x2"));
            rslc_function fun(rslc_type("vector"), "vtransform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("string"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            rslc_function fun(rslc_type("vector"), "ntransform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("string"), "x0"));
            vars.push_back(rslc_variable(rslc_type("string"), "x1"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x2"));
            rslc_function fun(rslc_type("vector"), "ntransform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("matrix"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            rslc_function fun(rslc_type("vector"), "ntransform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("string"), "x0"));
            vars.push_back(rslc_variable(rslc_type("matrix"), "x1"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x2"));
            rslc_function fun(rslc_type("vector"), "ntransform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            rslc_function fun(rslc_type("float"), "depth", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            rslc_function fun(rslc_type("vector"), "calculatenormal", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("float"), "x1"));
            rslc_function fun(rslc_type("float"), "comp", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("string"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            rslc_function fun(rslc_type("vector"), "ctransform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("string"), "x0"));
            vars.push_back(rslc_variable(rslc_type("string"), "x1"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x2"));
            rslc_function fun(rslc_type("vector"), "ctransform", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("array<string>"), "x0"));
            rslc_function fun(rslc_type("string"), "concat", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("string"), "x0"));
            vars.push_back(rslc_variable(rslc_type("string"), "x1"));
            rslc_function fun(rslc_type("float"), "match", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            rslc_function fun(rslc_type("vector"), "ambient", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            rslc_function fun(rslc_type("vector"), "diffuse", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            vars.push_back(rslc_variable(rslc_type("float"), "x2"));
            rslc_function fun(rslc_type("vector"), "specular", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x2"));
            vars.push_back(rslc_variable(rslc_type("float"), "x3"));
            rslc_function fun(rslc_type("vector"), "specularbrdf", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            vars.push_back(rslc_variable(rslc_type("float"), "x2"));
            rslc_function fun(rslc_type("vector"), "phong", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            rslc_function fun(rslc_type("string"), "shadername", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("string"), "x0"));
            rslc_function fun(rslc_type("string"), "shadername", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }

        //-------------------------------------------------------------
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            rslc_function fun(rslc_type("vector"), "normalize", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            rslc_function fun(rslc_type("float"), "length", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            rslc_function fun(rslc_type("float"), "dot", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }
        {
            std::vector<rslc_variable> vars;
            vars.push_back(rslc_variable(rslc_type("vector"), "x0"));
            vars.push_back(rslc_variable(rslc_type("vector"), "x1"));
            rslc_function fun(rslc_type("vector"), "cross", vars);
            std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
            sm->add_symbol(s);
        }

        return 0;
    }

    static
    int create_types(std::vector<rslc_type>& types, const rslc_symbol_manager* sm, const rslc_list_node* args);

    static
    bool equal_vars(const std::vector<rslc_variable>& v, const std::vector<rslc_type>& t)
    {
        if(v.size() != t.size())
        {
            return false;
        }
        size_t sz = v.size();
        for(size_t i= 0;i<sz;i++)
        {
            std::string a = replace_type(v[i].get_type().get_name());
            std::string b = replace_type(t[i].get_name());
            if(a != b)
            {
                return false;
            }
        }
        return true;
    }

    static 
    const rslc_symbol* predict_function_call(const rslc_symbol_manager* sm, const rslc_function_call_node* node)
    {
        std::string name = node->get_name();
        const rslc_list_node* args = CAST(const rslc_list_node*, node->get_arguments_node());

        std::vector<const rslc_symbol*> symbols;
        if(sm->find_functions(symbols, name))
        {
            if(symbols.size())
            {
                std::vector<rslc_type> types;
                int nRet = create_types(types, sm, args);
                if(nRet != 0)
                {
                    //std::cout << "error:B:"  << std::endl;
                    return NULL;
                }
                size_t sz = symbols.size();
                for(size_t i = 0;i<sz;i++)
                {
                    const rslc_symbol_function* sfun = CAST(const rslc_symbol_function*, symbols[i]->get_ptr());
                    const rslc_function& f = sfun->get_function();
                    const std::vector<rslc_variable>& vars = f.get_arguments();
                    if(equal_vars(vars, types))
                    {
                        return symbols[i];
                    }
                    else
                    {
                        //std::cout << "error:C:"  << std::endl;
                    }
                }
            }
        }
        else
        {
            sm->debug_print();
        }
        //std::cout << "error:A:" << name << std::endl;
        return NULL;
    }

    static
    std::string predict_type(const rslc_symbol_manager* sm, const rslc_node* node)
    {
        std::string t("");
        int nType = node->get_node_type();
        switch(nType)
        {
        case RSLC_NODE_FLOAT:  
            return std::string("float");
        case RSLC_NODE_INTEGER:
            return std::string("float");
        case RSLC_NODE_STRING:
            return std::string("string");
        case RSLC_NODE_TEXTURENAME:
            return std::string("texturename");
        case RSLC_NODE_TUPLE:
            {
                int sz = node->get_nodes_size();
                if (sz == 3)return std::string("vec3");
                if (sz == 16)return std::string("mat4");
                std::cout << "error:001:"  << std::endl;
                return std::string("");
            }
        case RSLC_NODE_ADD:
        case RSLC_NODE_SUB:
        case RSLC_NODE_MUL:
        case RSLC_NODE_DIV:
            {
                std::string lt = predict_type(sm, node->get_node_at(0));
                std::string rt = predict_type(sm, node->get_node_at(1));
                if(lt == rt)
                {
                    return lt;
                }
                else
                {
                    if(lt == "string" || rt == "string")return std::string("");
                    if(lt == "float")
                    {
                        return rt;
                    }
                    if(rt == "float")
                    {
                        return lt;
                    }
                }
                std::cout << "error:002:"  << std::endl;
                return std::string("");
            }
        case RSLC_NODE_NEG:
        case RSLC_NODE_NOT:
            return predict_type(sm, node->get_node_at(0));
        case RSLC_NODE_ASSIGN:
        case RSLC_NODE_ADD_ASSIGN:
        case RSLC_NODE_SUB_ASSIGN:
        case RSLC_NODE_MUL_ASSIGN:
        case RSLC_NODE_DIV_ASSIGN:
            return predict_type(sm, node->get_node_at(0));
        case RSLC_NODE_LSS:
        case RSLC_NODE_LEQ:
        case RSLC_NODE_GTR:
        case RSLC_NODE_GEQ:
        case RSLC_NODE_EQ:
        case RSLC_NODE_NE:
        case RSLC_NODE_AND:
        case RSLC_NODE_OR:
            return std::string("float");//bool->float
        case RSLC_NODE_VARIABLE_REF:
            {
                const rslc_variable_ref_node* vnode = CAST(const rslc_variable_ref_node*, node);
                std::string name = vnode->get_name();
                std::vector<const rslc_symbol*> symbols;
                if(sm->find_variables(symbols, name) && symbols.size())
                {
                    assert(symbols.size()==1);
                    const rslc_symbol* s = symbols.back();
                    std::string tname = s->get_type().get_name();
                    return tname;
                }
                std::cout << "error:2:" << name << std::endl;
                return std::string("");
            }
        case RSLC_NODE_VARIABLE_ARRAY_REF:
            {
                const rslc_variable_array_ref_node* vnode = CAST(const rslc_variable_array_ref_node*, node);
                std::string name = vnode->get_name();
                std::vector<const rslc_symbol*> symbols;
                if(sm->find_variables(symbols, name) && symbols.size())
                {
                    assert(symbols.size()==1);
                    const rslc_symbol* s = symbols.back();
                    std::string tname = s->get_type().get_name();
                    return tname;
                }
                std::cout << "error:003:"  << std::endl;
                return std::string("");
            }
        case RSLC_NODE_FUNCTION_CALL:
            {
                const rslc_function_call_node* vnode = CAST(const rslc_function_call_node*, node);
                const rslc_symbol* s = predict_function_call(sm,vnode);
                if(s != NULL)
                {
                    return s->get_type().get_name();
                }
                std::cout << "error:004:"  << std::endl;
                return std::string("");
            }
        case RSLC_NODE_PAREN:
            {
                return predict_type(sm, node->get_node_at(0));
            }
        case RSLC_NODE_CAST:
            {
                const rslc_cast_node* vnode = CAST(const rslc_cast_node*, node);
                return vnode->get_type_node()->get_type().get_name();
            }
        }
        std::cout << "error:005:" << nType << std::endl;
        return t;
    }

    static
    int create_types(std::vector<rslc_type>& types, const rslc_symbol_manager* sm, const rslc_list_node* args)
    {
        size_t sz = args->get_nodes_size();
        for(size_t i=0;i<sz;i++)
        {
            std::string type = predict_type(sm, args->get_node_at(i));
            if(type == "")return -1;
            types.push_back(rslc_type(type));
        }
        return 0;
    }

    class call_maker : public rslc_node_walker
    {
    public:
        typedef rslc_node_walker base;
    public:
        call_maker(rslc_context* ctx, rslc_symbol_manager* sm)
            :ctx_(ctx), sm_(sm)
        {}
        int walk(rslc_node* node)
        {
            return rslc_node_walker::walk(node);
        }
        
        int walk(rslc_block_node* node)
        {
            sm_->push_symbol_table();
            int nRet = base::walk(node);
            sm_->pop_symbol_table();
            return nRet;
        }
        
        int walk(rslc_shader_def_node* node)
        {
            sm_->push_symbol_table();
            {
                int nRet = add_shader_variable_symbols(sm_, node);
                if(nRet)return -1;
            }
            {
                int nRet = base::walk(node);
                sm_->pop_symbol_table();
                return nRet;
            }
        }

        int walk(rslc_type_variables_def_node* node)
        {
            const rslc_type_variables_def_node* tvnode = CAST(const rslc_type_variables_def_node*, node);
            const rslc_type_node* tnode  = CAST(const rslc_type_node*, tvnode->get_type_node());

            rslc_type   t = tnode->get_type();
            std::string vname;
            if(tvnode->get_variable_node()->get_node_type() == RSLC_NODE_VARIABLE_DEF)
            {
                const rslc_variable_def_node* vnode = CAST(const rslc_variable_def_node*, tvnode->get_variable_node());
                vname = vnode->get_name();
            }
            else if(tvnode->get_variable_node()->get_node_type() == RSLC_NODE_VARIABLE_DEF)
            {
                const rslc_variable_array_def_node* vnode = CAST(const rslc_variable_array_def_node*, tvnode->get_variable_node());
                vname = vnode->get_name();
            }
            if(vname == "")
            {
                return -1;
            }
            std::shared_ptr<rslc_symbol> s(new rslc_symbol(t,vname));
            if(!sm_->add_symbol(s))
            {
                return -1;
            }
            return base::walk(node);
        }


        int walk(rslc_function_def_node* node)
        {
            std::string fname = node->get_name();
            const rslc_list_node* args = CAST(const rslc_list_node*, node->get_arguments_node());
            std::vector<rslc_variable> vargs;
            for(size_t i = 0;i<args->get_nodes_size();i++)
            {
                const rslc_type_variables_def_node* tvnode = CAST(const rslc_type_variables_def_node*, args->get_node_at(i));
                const rslc_type_node* tnode  = CAST(const rslc_type_node*, tvnode->get_type_node());
                
                rslc_type   t = tnode->get_type();
                std::string vname;
                if(tvnode->get_variable_node()->get_node_type() == RSLC_NODE_VARIABLE_DEF)
                {
                    const rslc_variable_def_node* vnode = CAST(const rslc_variable_def_node*, tvnode->get_variable_node());
                    vname = vnode->get_name();
                }
                else if(tvnode->get_variable_node()->get_node_type() == RSLC_NODE_VARIABLE_DEF)
                {
                    const rslc_variable_array_def_node* vnode = CAST(const rslc_variable_array_def_node*, tvnode->get_variable_node());
                    vname = vnode->get_name();
                }
                if(vname == "")
                {
                    return -1;
                }

                vargs.push_back(rslc_variable(t, vname));
            }
            const rslc_type_node* tnode  = CAST(const rslc_type_node*, node->get_type_node());
            rslc_type t = tnode->get_type();
            std::shared_ptr<rslc_symbol> s(new rslc_symbol(rslc_function(t, fname, vargs)));
            if(!sm_->add_symbol(s))
            {
                return -1;
            }
            
            sm_->push_symbol_table();
            int nRet = base::walk(node);
            sm_->pop_symbol_table();
            return nRet;
        }

        //-------------------------------------
        int walk(rslc_function_call_node* node)
        {
            std::string fname = node->get_name();
            const rslc_symbol* s = predict_function_call(sm_, node);
            if(s != NULL)
            {

            }
            else
            {
                std::cout << "error:" << fname << std::endl;
            }
            return base::walk(node);
        }
    protected:
        rslc_context* ctx_;
        rslc_symbol_manager* sm_;
    };

    static
    int mangling_functions(rslc_context* ctx, rslc_node* node)
    {
        std::shared_ptr<rslc_symbol_manager> sm(new rslc_symbol_manager());
        {
            int nRet = add_global_variable_symbols(sm.get());
            if(nRet != 0)return nRet;
        }
        {
            int nRet = add_std_functions(sm.get());
            if(nRet != 0)return nRet;
        }
        
        {
            call_maker w(ctx, sm.get());
            int nRet = w.walk(node);
            if(nRet != 0)return nRet;
        }

        return 0;
    }

    class type_replacer : public rslc_node_walker
    {
    public:
        type_replacer(rslc_context* ctx)
            :ctx_(ctx)
        {}
        int walk(rslc_node* node)
        {
            return rslc_node_walker::walk(node);
        }
        int walk(rslc_type_node* node)
        {
            std::string name = (std::string)(node->get_type().get_name());
            if(
                name == "point" ||
                name == "vector" || 
                name == "normal" || 
                name == "color" 
            )
            {
                node->set_name("vec3");
            }
            else if
            (
                name == "matrix"
            )
            {
                node->set_name("mat4");
            }

            return rslc_node_walker::walk(node);
        }
        int walk(rslc_function_call_node* node)
        {
            std::string name = (std::string)(node->get_name());
            if(
                name == "point" ||
                name == "vector" || 
                name == "normal" || 
                name == "color" 
            )
            {
                node->set_name("vec3");
            }
            return rslc_node_walker::walk(node);
        }
    protected:
        rslc_context* ctx_;
    };

    static
    int replace_type(rslc_context* ctx, rslc_node* node)
    {
        type_replacer w(ctx);
        return w.walk(node);
    }

    class inivalue_inserter : public rslc_node_walker
    {
    public:
        inivalue_inserter(rslc_context* ctx)
            :ctx_(ctx)
        {}
        int walk(rslc_node* node)
        {
            return rslc_node_walker::walk(node);
        }
        int walk(rslc_type_variables_def_node* node)
        {
            rslc_type_node* tnode = CAST(rslc_type_node*, node->get_type_node());
            rslc_type t = tnode->get_type();
            if(node->get_variable_node()->get_node_type() == RSLC_NODE_VARIABLE_DEF)
            {
                rslc_variable_def_node* vnode = CAST(rslc_variable_def_node*, node->get_variable_node());
                if(vnode->get_definition_node() == NULL)
                {
                    if(t.get_name() == "float")
                    {
                        std::shared_ptr<rslc_node> val(new rslc_float_node(0.0));
                        ctx_->add_node(val);
                        vnode->set_definition_node(val.get());
                    }
                    else if(t.get_name() == "vec3")
                    {
                        std::shared_ptr<rslc_node> args(new rslc_list_node());
                        {
                            rslc_list_node* lnode = (rslc_list_node*)args.get();
                            for(int j = 0; j < 3; j++)
                            {
                                std::shared_ptr<rslc_node> val(new rslc_float_node(0.0f));
                                ctx_->add_node(val);
                                lnode->add_node(val.get());
                            }
                        }
                        std::shared_ptr<rslc_node> func(new rslc_function_call_node("vec3", args.get()));
                        ctx_->add_node(func);
                        vnode->set_definition_node(func.get());
                    }
                    else if(t.get_name() == "matrix")
                    {
                        std::shared_ptr<rslc_node> args(new rslc_list_node());
                        {
                            rslc_list_node* lnode = (rslc_list_node*)args.get();
                            for(int j = 0; j < 16; j++)
                            {
                                std::shared_ptr<rslc_node> val(new rslc_float_node(0.0f));
                                ctx_->add_node(val);
                                lnode->add_node(val.get());
                            }
                        }
                        std::shared_ptr<rslc_node> func(new rslc_function_call_node("mat4", args.get()));
                        ctx_->add_node(func);
                        vnode->set_definition_node(func.get());
                    }
                }
            }
            else if(node->get_variable_node()->get_node_type() == RSLC_NODE_VARIABLE_ARRAY_DEF)
            {

            }
            return rslc_node_walker::walk(node);
        }
    protected:
        rslc_context* ctx_;
    };

    static
    int insert_inivalue(rslc_context* ctx, rslc_node* node)
    {
        inivalue_inserter w(ctx);
        return w.walk(node);
    }

    static
    int replace_assign(rslc_context* ctx, rslc_node* node)
    {
        size_t sz = node->get_nodes_size();
        for(size_t i = 0;i<sz;i++)
        {
            rslc_node* n = node->get_node_at(i);
            if(n)
            {
                int nRet = replace_assign(ctx, n);
                if(nRet != 0)return nRet;
                int nType = n->get_node_type();
                if(
                    nType == RSLC_NODE_ADD_ASSIGN ||
                    nType == RSLC_NODE_SUB_ASSIGN ||
                    nType == RSLC_NODE_MUL_ASSIGN ||
                    nType == RSLC_NODE_DIV_ASSIGN
                )
                {
                    rslc_node* ln = n->get_node_at(0);
                    rslc_node* rn = n->get_node_at(1);
                    if(ln->get_node_type() == RSLC_NODE_VARIABLE_REF)
                    {
                        std::shared_ptr<rslc_node> ln_(new rslc_variable_ref_node((const rslc_variable_ref_node&)*ln));
                        ctx->add_node(ln_);

                        rslc_node* op = NULL;
                        {
                            if(nType == RSLC_NODE_ADD_ASSIGN)
                            {
                                std::shared_ptr<rslc_node> op_(new rslc_add_node(ln_.get(), rn));
                                ctx->add_node(op_);
                                op = op_.get();
                            }
                            else if(nType == RSLC_NODE_SUB_ASSIGN)
                            {
                                std::shared_ptr<rslc_node> op_(new rslc_sub_node(ln_.get(), rn));
                                ctx->add_node(op_);
                                op = op_.get();
                            }
                            else if(nType == RSLC_NODE_MUL_ASSIGN)
                            {
                                std::shared_ptr<rslc_node> op_(new rslc_mul_node(ln_.get(), rn));
                                ctx->add_node(op_);
                                op = op_.get();
                            }
                            else if(nType == RSLC_NODE_DIV_ASSIGN)
                            {
                                std::shared_ptr<rslc_node> op_(new rslc_div_node(ln_.get(), rn));
                                ctx->add_node(op_);
                                op = op_.get();
                            }
                        }

                        if(op)
                        {
                            std::shared_ptr<rslc_node> an_(new rslc_assign_node(ln, op));
                            ctx->add_node(an_);

                            node->set_node_at(i, an_.get());
                        }
                    }
                } 
            }
        }
        return 0;
    }
}


    int rslc_optimize_ksl(rslc_context* ctx, rslc_node* node)
    {
        {
            int nRet = replace_type(ctx, node);
            if(nRet != 0)return nRet;
        }
        {
            int nRet = mangling_functions(ctx, node);
            if(nRet != 0)return nRet;
        }
        {
            int nRet = insert_inivalue(ctx, node);
            if(nRet != 0)return nRet;
        }
        {
            int nRet = replace_assign(ctx, node);
            if(nRet != 0)return nRet;
        }
        return 0;
    }
}
