#ifndef RSLC_COMPILER_H
#define RSLC_COMPILER_H

namespace rslc
{
    class rslc_context;
    class rslc_compiler
    {
    public:
        int compile(const char* szFile);
    protected:
        int analyze(rslc_context* ctx);
        int optimize(rslc_context* ctx);
        int output(rslc_context* ctx);
    };
}

#endif
