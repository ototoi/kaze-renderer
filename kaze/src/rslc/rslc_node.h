#ifndef RSLC_NODE_H
#define RSLC_NODE_H

#include <cassert>
#include <vector>
#include "rslc_type.h"
#include "rslc_texturename.h"
#include "rslc_location.h"

namespace rslc
{
    enum rslc_node_type
    {
        RSLC_NODE_UNKOWON = -1,
        RSLC_NODE_IDENTIFIER = 0,
        RSLC_NODE_INTEGER,
        RSLC_NODE_FLOAT,
        RSLC_NODE_STRING,
        RSLC_NODE_TEXTURENAME,
        RSLC_NODE_TYPE,
        RSLC_NODE_VARIABLE_REF,
        RSLC_NODE_VARIABLE_ARRAY_REF,
        RSLC_NODE_FUNCTION_CALL,
        RSLC_NODE_NEG,
        RSLC_NODE_NOT,
        RSLC_NODE_ADD,
        RSLC_NODE_SUB,
        RSLC_NODE_MUL,
        RSLC_NODE_DIV,
        RSLC_NODE_CRS,
        RSLC_NODE_DOT,
        RSLC_NODE_LSS,
        RSLC_NODE_LEQ,
        RSLC_NODE_GTR,
        RSLC_NODE_GEQ,
        RSLC_NODE_EQ,
        RSLC_NODE_NE,
        RSLC_NODE_AND,
        RSLC_NODE_OR,
        RSLC_NODE_ASSIGN,
        RSLC_NODE_ADD_ASSIGN,
        RSLC_NODE_SUB_ASSIGN,
        RSLC_NODE_MUL_ASSIGN,
        RSLC_NODE_DIV_ASSIGN,
        RSLC_NODE_COND,
        RSLC_NODE_CAST,
        RSLC_NODE_LIST,
        RSLC_NODE_TUPLE,
        RSLC_NODE_BLOCK,
        RSLC_NODE_WHILE,
        RSLC_NODE_FOR,
        RSLC_NODE_SOLAR,
        RSLC_NODE_ILLUMINATE,
        RSLC_NODE_ILLUMINANCE,
        RSLC_NODE_GATHER,
        RSLC_NODE_IF,
        RSLC_NODE_RETURN,
        RSLC_NODE_BREAK,
        RSLC_NODE_CONTINUE,
        RSLC_NODE_VARIABLE_DEF,
        RSLC_NODE_VARIABLE_ARRAY_DEF,
        RSLC_NODE_TYPE_VARIABLES_DEF,
        RSLC_NODE_FUNCTION_DEF,
        RSLC_NODE_SHADER_DEF,
        RSLC_NODE_DEFINITIONS,
        RSLC_NODE_STATEMENT,
        RSLC_NODE_PAREN,

        RSLC_NODE_ONOTHER = 1000,
    };

    class rslc_node
    {
    public: 
        virtual ~rslc_node(){}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_UNKOWON;}
        void  set_location(const rslc_location& loc){loc_ = loc;}
        const rslc_location& get_location()const{return loc_;}
    public:
        void add_node(rslc_node* node)
        {
            nodes_.push_back(node);
        }
        void clear_nodes()
        {
            nodes_.clear();
        }
        const std::vector<rslc_node*>& get_nodes()const
        {
            return nodes_;
        }
        size_t get_nodes_size()const
        {
            return nodes_.size();
        }
        rslc_node* get_node_at(size_t i)const
        {
            return nodes_[i];
        }
        void set_node_at(size_t i, rslc_node* node)
        {
            nodes_[i] = node;
        }
    protected:
        rslc_location loc_;
        std::vector<rslc_node*> nodes_;
    };
    //-------------------------------------------------
    class rslc_identifier_node : public rslc_node
    {
    public:
        explicit rslc_identifier_node(const std::string& v)
            :v_(v)
        {}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_IDENTIFIER;}
        const std::string& get_value()const{return v_;}
    private:
        std::string v_;
    };
    //-------------------------------------------------
    class rslc_integer_node : public rslc_node
    {
    public:
        explicit rslc_integer_node(int v)
            :v_(v)
        {}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_INTEGER;}
        const int& get_value()const{return v_;}
    private:
        int v_;
    };

    class rslc_float_node : public rslc_node
    {
    public:
        explicit rslc_float_node(float v)
            :v_(v)
        {}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_FLOAT;}
        const float& get_value()const{return v_;}
    private:
        float v_;
    };

    class rslc_string_node : public rslc_node
    {
    public:
        explicit rslc_string_node(const std::string& v)
            :v_(v)
        {}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_STRING;}
        const std::string& get_value()const{return v_;}
    private:
        std::string v_;
    };

    class rslc_texturename_node : public rslc_node
    {
    public:
        rslc_texturename_node(const std::string& name, int index)
            :v_(name, index)
        {}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_TEXTURENAME;}
        const rslc_texturename& get_value()const{return v_;}
    private:
        rslc_texturename v_;
    };
    //-------------------------------------------------
    class rslc_type_node : public rslc_node
    {
    public:
        explicit rslc_type_node(const std::string& name)
            :type_(name)
        {}
        explicit rslc_type_node(const rslc_type& type)
            :type_(type)
        {}
        explicit rslc_type_node(const rslc_type_node& n)
            :type_(n.type_)
        {}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_TYPE;}
        void add_attribute(const std::string& attr)
        {
            type_.add_attribute(attr);
        }
        const rslc_type& get_type()const
        {
            return type_;
        }
        void set_array(bool bArray)
        {
            type_.set_array(bArray);
        }
        void set_name(const std::string& name)
        {
            type_.set_name(name);
        }
    private:
        rslc_type type_;
    };
    //-------------------------------------------------
    class rslc_expression_node : public rslc_node
    {
    };
    //-------------------------------------------------

    class rslc_variable_ref_node : public rslc_expression_node
    {
    public:
        explicit rslc_variable_ref_node(
            const std::string& name
        )
            :name_(name)
        {}
        rslc_variable_ref_node(
            const rslc_variable_ref_node& rhs
        )
            :name_(rhs.name_)
        {}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_VARIABLE_REF;}
        const std::string& get_name()const{return name_;}
    private:
        std::string name_;
    };

    class rslc_variable_array_ref_node : public rslc_expression_node
    {
    public:
        rslc_variable_array_ref_node(
            const std::string& name,
            rslc_node* index
        )
            :name_(name)
        {
            this->add_node(index);
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_VARIABLE_ARRAY_REF;}
        const std::string& get_name()const{return name_;}
        rslc_node* get_index_node()const{return this->get_node_at(0);}
    private:
        std::string name_;
    };

    class rslc_function_call_node : public rslc_expression_node
    {
    public:
        rslc_function_call_node(
            const std::string& name, 
            rslc_node* args
        )
            :name_(name)
        {
            this->add_node(args);
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_FUNCTION_CALL;}
        const std::string& get_name()const{return name_;}
        rslc_node* get_arguments_node()const{return this->get_node_at(0);}
    public:
        void set_name(const std::string& name)
        {
            name_ = name;
        }
    private:
        std::string name_;
    };

    template<int TYPE>
    class rslc_una_base_node : public rslc_expression_node
    {
    public:
        rslc_una_base_node(rslc_node* exp)
        {
            this->add_node(exp);
        }
        virtual rslc_node_type get_node_type()const{return (rslc_node_type)TYPE;}
        rslc_node* get_rhs_node()const{return this->get_node_at(0);}
    };

    typedef rslc_una_base_node<RSLC_NODE_NEG> rslc_neg_node;
    typedef rslc_una_base_node<RSLC_NODE_NOT> rslc_not_node;

    template<int TYPE>
    class rslc_bin_base_node : public rslc_expression_node
    {
    public:
        rslc_bin_base_node(rslc_node* a, rslc_node* b)
        {
            this->add_node(a);
            this->add_node(b);
        }
        virtual rslc_node_type get_node_type()const{return (rslc_node_type)TYPE;}
        rslc_node* get_lhs_node()const{return this->get_node_at(0);}
        rslc_node* get_rhs_node()const{return this->get_node_at(1);}
    };

    typedef rslc_bin_base_node<RSLC_NODE_ADD> rslc_add_node;
    typedef rslc_bin_base_node<RSLC_NODE_SUB> rslc_sub_node;
    typedef rslc_bin_base_node<RSLC_NODE_MUL> rslc_mul_node;
    typedef rslc_bin_base_node<RSLC_NODE_DIV> rslc_div_node;
    typedef rslc_bin_base_node<RSLC_NODE_CRS> rslc_crs_node;
        typedef rslc_crs_node rslc_cross_node;
    typedef rslc_bin_base_node<RSLC_NODE_DOT> rslc_dot_node;

    typedef rslc_bin_base_node<RSLC_NODE_LSS>  rslc_lss_node;
        typedef rslc_lss_node rslc_less_node;
    typedef rslc_bin_base_node<RSLC_NODE_LEQ>  rslc_leq_node;
        typedef rslc_leq_node rslc_less_equal_node;
    typedef rslc_bin_base_node<RSLC_NODE_GTR>  rslc_gtr_node;
        typedef rslc_gtr_node rslc_grater_node;
    typedef rslc_bin_base_node<RSLC_NODE_GEQ>  rslc_geq_node;
    typedef rslc_bin_base_node<RSLC_NODE_EQ>   rslc_eq_node;
    typedef rslc_bin_base_node<RSLC_NODE_NE>   rslc_ne_node;

    typedef rslc_bin_base_node<RSLC_NODE_AND>  rslc_and_node;
    typedef rslc_bin_base_node<RSLC_NODE_OR>   rslc_or_node;

    template<int TYPE>
    class rslc_assign_base_node : public rslc_expression_node
    {
    public:
        rslc_assign_base_node(rslc_node* a, rslc_node* b)
        {
            this->add_node(a);
            this->add_node(b);
        }
        virtual rslc_node_type get_node_type()const{return (rslc_node_type)TYPE;}
        rslc_node* get_lhs_node()const{return this->get_node_at(0);}
        rslc_node* get_rhs_node()const{return this->get_node_at(1);}
    };

    typedef rslc_assign_base_node<RSLC_NODE_ASSIGN> rslc_assign_node;
    typedef rslc_assign_base_node<RSLC_NODE_ADD_ASSIGN> rslc_add_assign_node;
    typedef rslc_assign_base_node<RSLC_NODE_SUB_ASSIGN> rslc_sub_assign_node;
    typedef rslc_assign_base_node<RSLC_NODE_MUL_ASSIGN> rslc_mul_assign_node;
    typedef rslc_assign_base_node<RSLC_NODE_DIV_ASSIGN> rslc_div_assign_node;

    class rslc_cond_node : public rslc_expression_node
    {
    public:
        rslc_cond_node(rslc_node* cond, rslc_node* exp0, rslc_node* exp1)
        {
            this->add_node(cond);
            this->add_node(exp0);
            this->add_node(exp1);
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_COND;}
        rslc_node* get_condition_node()const{return this->get_node_at(0);}
        rslc_node* get_expression1_node()const{return this->get_node_at(1);}
        rslc_node* get_expression2_node()const{return this->get_node_at(2);}
    };

    class rslc_cast_node : public rslc_expression_node
    {
    public:
        rslc_cast_node(rslc_type_node* type, rslc_node* exp)
        {
            ns_ = "";
            this->add_node(type);
            this->add_node(exp);
        }
        rslc_cast_node(rslc_type_node* type, const std::string& ns, rslc_node* exp)
        {
            ns_   = ns;
            this->add_node(type);
            this->add_node(exp);
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_CAST;}

        const std::string& get_namespace()const{return ns_;}
        rslc_type_node* get_type_node()const {return (rslc_type_node*)this->get_node_at(0);}
        rslc_node* get_expression_node()const{return this->get_node_at(1);}
    private:
        std::string ns_;
    };

    //-------------------------------------------------

    class rslc_list_node : public rslc_expression_node
    {
    public:
        rslc_list_node(){}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_LIST;}
    };

    class rslc_tuple_node : public rslc_list_node
    {
    public:
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_TUPLE;}
    };

    class rslc_block_node : public rslc_list_node
    {
    public:
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_BLOCK;}
    };

    class rslc_definitions_node : public rslc_list_node
    {
    public:
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_DEFINITIONS;}
    };

    //-------------------------------------------------

    class rslc_paren_node : public rslc_expression_node
    {
    public:
        explicit rslc_paren_node(rslc_node* node)
        {
            this->add_node(node);
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_PAREN;}
        virtual rslc_node* get_contents_node()const{return this->get_node_at(0);}
    };

    //-------------------------------------------------
    class rslc_statement_base_node : public rslc_node
    {};

    class rslc_statement_node : public rslc_statement_base_node
    {
    public:
        explicit rslc_statement_node(rslc_node* node)
        {
            this->add_node(node);
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_STATEMENT;}
        virtual rslc_node* get_contents_node()const{return this->get_node_at(0);}
    };

    template<class Derive>
    class rlsc_loop_base_node : public rslc_statement_base_node
    {
    public:
        rlsc_loop_base_node(rslc_list_node* cond, rslc_node* stat)
        {
            this->add_node(cond);
            this->add_node(stat);
        }
        virtual rslc_list_node* get_condition_node()const{return (rslc_list_node*)this->get_node_at(0);}
        virtual rslc_node* get_contents_node()const{return this->get_node_at(1);}
        virtual void set_condition_node(rslc_list_node* cond){this->set_node_at(0, cond);}
    };

    class rslc_while_node : public rlsc_loop_base_node<rslc_while_node>
    {
    public:
        typedef rlsc_loop_base_node<rslc_while_node> base_type;
    public:
        rslc_while_node(rslc_list_node* cond, rslc_node* stat)
            :base_type(cond, stat)
        {}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_WHILE;}
    };

    class rslc_for_node : public rlsc_loop_base_node<rslc_for_node>
    {
    public:
        typedef rlsc_loop_base_node<rslc_for_node> base_type;
    public:
        rslc_for_node(rslc_list_node* cond, rslc_node* stat)
            :base_type(cond, stat)
        {}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_FOR;}
    };

    class rslc_solar_node : public rlsc_loop_base_node<rslc_solar_node>
    {
    public:
        typedef rlsc_loop_base_node<rslc_solar_node> base_type;
    public:
        rslc_solar_node(rslc_list_node* cond, rslc_node* stat)
            :base_type(cond, stat)
        {}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_SOLAR;}
    };

    class rslc_illuminate_node : public rlsc_loop_base_node<rslc_illuminate_node>
    {
    public:
        typedef rlsc_loop_base_node<rslc_illuminate_node> base_type;
    public:
        rslc_illuminate_node(rslc_list_node* cond, rslc_node* stat)
            :base_type(cond, stat)
        {}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_ILLUMINATE;}
    };

    class rslc_illuminance_node : public rlsc_loop_base_node<rslc_illuminance_node>
    {
    public:
        typedef rlsc_loop_base_node<rslc_illuminance_node> base_type;
    public:
        rslc_illuminance_node(rslc_list_node* cond, rslc_node* stat)
            :base_type(cond, stat)
        {}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_ILLUMINANCE;}
    };

    class rslc_gather_node : public rlsc_loop_base_node<rslc_gather_node>
    {
    public:
        typedef rlsc_loop_base_node<rslc_gather_node> base_type;
    public:
        rslc_gather_node(rslc_list_node* cond, rslc_node* stat)
            :base_type(cond, stat)
        {}
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_GATHER;}
    };
    //-------------------------------------------------
    class rslc_if_node : public rslc_statement_base_node
    {
    public:
        rslc_if_node(rslc_node* cond, rslc_node* stat, rslc_node* els = NULL)
        {
            this->add_node(cond);
            this->add_node(stat);
            this->add_node(els);
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_IF;}
        rslc_node* get_condition_node()const{return this->get_node_at(0);}
        rslc_node* get_contents_node()const{return this->get_node_at(1);}
        rslc_node* get_else_node()const{return this->get_node_at(2);} 

        void set_condition_node(rslc_node* cond){this->set_node_at(0, cond);}
    };
    //-------------------------------------------------
    class rslc_return_node : public rslc_statement_base_node
    {
    public:
        rslc_return_node()
        {
           this->add_node(NULL);
        }
        explicit rslc_return_node(rslc_node* exp)
        {
            this->add_node(exp);
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_RETURN;}
        rslc_node* get_expression_node()const{return this->get_node_at(0);}
    };

    //-------------------------------------------------
    class rslc_break_node : public rslc_statement_base_node
    {
    public:
        rslc_break_node(int label = -1)
        {
            label_ = label;
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_BREAK;}
    protected:
        int label_;
    };

    //-------------------------------------------------
    class rslc_continue_node : public rslc_statement_base_node
    {
    public:
        explicit rslc_continue_node(int label = -1)
        {
            label_ = label;
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_CONTINUE;}
    protected:
        int label_;
    };

    //-------------------------------------------------
    class rslc_function_def_node : public rslc_expression_node
    {
    public:
        rslc_function_def_node(
            rslc_node* type,
            const std::string& name, 
            rslc_node* args,
            rslc_node* stat
        )
            :name_(name)
        {
            this->add_node(type);
            this->add_node(args);
            this->add_node(stat);
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_FUNCTION_DEF;}
        const std::string& get_name()const{return name_;}

        rslc_node* get_type_node()const{return this->get_node_at(0);} 
        rslc_node* get_arguments_node()const{return this->get_node_at(1);} 
        rslc_node* get_contents_node()const{return this->get_node_at(2);}
    private:
        std::string name_;
    };

    //-------------------------------------------------
    class rslc_shader_def_node : public rslc_expression_node
    {
    public:
        rslc_shader_def_node(
            rslc_node* type,
            const std::string& name, 
            rslc_node* args,
            rslc_node* stat
        )
            :name_(name)
        {
            this->add_node(type);
            this->add_node(args);
            this->add_node(stat);
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_SHADER_DEF;}
        const std::string& get_name()const{return name_;}

        rslc_node* get_type_node()const{return this->get_node_at(0);} 
        rslc_node* get_arguments_node()const{return this->get_node_at(1);} 
        rslc_node* get_contents_node()const{return this->get_node_at(2);}
    private:
        std::string name_;
    };

    //-------------------------------------------------

    class rslc_variable_def_node : public rslc_expression_node
    {
    public:
        explicit rslc_variable_def_node(
            const std::string& name,
            rslc_node* def = NULL
        )
            :name_(name)
        {
            this->add_node(def);
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_VARIABLE_DEF;}
        const std::string& get_name()const{return name_;}
        rslc_node* get_definition_node()const{return this->get_node_at(0);}
        void set_definition_node(rslc_node* n){ this->set_node_at(0, n); }
    private:
        std::string name_;
    };

    class rslc_variable_array_def_node : public rslc_expression_node
    {
    public:
        rslc_variable_array_def_node(
            const std::string& name,
            int size,
            rslc_node* def = NULL
        )
            :name_(name), size_(size)
        {
            this->add_node(def);
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_VARIABLE_ARRAY_DEF;}
        const std::string& get_name()const{return name_;}
        int get_size()const{return size_;}
        rslc_node* get_definition_node()const{return this->get_node_at(0);}
        void set_definition_node(rslc_node* n){ this->set_node_at(0, n); }
    private:
        std::string name_;
        int size_;
    };

    class rslc_type_variables_def_node : public rslc_expression_node
    {
    public:
        rslc_type_variables_def_node(
            rslc_type_node* type,
            rslc_list_node* defs
        )
        {
            this->add_node(type);
            this->add_node(defs);
        }
        virtual rslc_node_type get_node_type()const{return RSLC_NODE_TYPE_VARIABLES_DEF;}
        rslc_type_node* get_type_node()const{return (rslc_type_node*)this->get_node_at(0);}
        rslc_list_node* get_variables_node()const{return (rslc_list_node*)this->get_node_at(1);}
    public:
        rslc_node* get_variable_node()const
        {
            rslc_list_node* l = this->get_variables_node();
            if(l->get_nodes_size() >= 1)
            {
                return l->get_node_at(0);
            }
            return NULL;
        }
    };


}

#endif
