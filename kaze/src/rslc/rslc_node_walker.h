#ifndef RSLC_NODE_WALKER_H
#define RSLC_NODE_WALKER_H

#include "rslc_node.h"

namespace rslc
{
    class rslc_node_walker
    {
    public:
        virtual ~rslc_node_walker(){}
        virtual int walk(rslc_node* node);
    public:
        virtual int walk_(rslc_node* node);

        virtual int walk(rslc_identifier_node* node);
        virtual int walk(rslc_integer_node* node);
        virtual int walk(rslc_float_node* node);
        virtual int walk(rslc_string_node* node);
        virtual int walk(rslc_texturename_node* node);
        virtual int walk(rslc_type_node* node);
        virtual int walk(rslc_variable_ref_node* node);
        virtual int walk(rslc_variable_array_ref_node* node);
        virtual int walk(rslc_function_call_node* node);

        virtual int walk(rslc_not_node* node);
        virtual int walk(rslc_neg_node* node);

        virtual int walk(rslc_add_node* node);
        virtual int walk(rslc_sub_node* node);
        virtual int walk(rslc_mul_node* node);
        virtual int walk(rslc_div_node* node);
        virtual int walk(rslc_crs_node* node);
        virtual int walk(rslc_dot_node* node);
        virtual int walk(rslc_lss_node* node);
        virtual int walk(rslc_leq_node* node);
        virtual int walk(rslc_gtr_node* node);
        virtual int walk(rslc_geq_node* node);
    
        virtual int walk(rslc_eq_node* node);
        virtual int walk(rslc_ne_node* node);
        virtual int walk(rslc_and_node* node);
        virtual int walk(rslc_or_node* node);
        virtual int walk(rslc_assign_node* node);
        virtual int walk(rslc_add_assign_node* node);
        virtual int walk(rslc_sub_assign_node* node);
        virtual int walk(rslc_mul_assign_node* node);
        virtual int walk(rslc_div_assign_node* node);

        virtual int walk(rslc_cond_node* node);
        virtual int walk(rslc_cast_node* node);
        virtual int walk(rslc_list_node* node);
        virtual int walk(rslc_tuple_node* node);
        virtual int walk(rslc_block_node* node);
        virtual int walk(rslc_definitions_node* node);
        virtual int walk(rslc_paren_node* node);
        virtual int walk(rslc_statement_node* node);

        virtual int walk(rslc_while_node* node);
        virtual int walk(rslc_for_node* node);
        virtual int walk(rslc_solar_node* node);

        virtual int walk(rslc_illuminate_node* node);
        virtual int walk(rslc_illuminance_node* node);
        virtual int walk(rslc_gather_node* node);
        virtual int walk(rslc_if_node* node);

        virtual int walk(rslc_return_node* node);
        virtual int walk(rslc_break_node* node);
        virtual int walk(rslc_continue_node* node);

        virtual int walk(rslc_function_def_node* node);
        virtual int walk(rslc_shader_def_node* node);

        virtual int walk(rslc_variable_def_node* node);
        virtual int walk(rslc_variable_array_def_node* node);
        virtual int walk(rslc_type_variables_def_node* node);
    };
}

#endif
