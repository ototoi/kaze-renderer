#ifndef RSLC_EXPRESSION_H
#define RSLC_EXPRESSION_H

#include "std::string.h"
#include "rslc_symbol.h"
#include <vector>

namespace rslc
{
    class rslc_function_call
    {
    public:
        rslc_function_call(const std::string& name, )
            :name_("")
        {}
    private:
        std::string name_;
    };
}

#endif
