#ifndef RSLC_KSL_ARRAY_H
#define RSLC_KSL_ARRAY_H

namespace rslc
{
    static const char* RSLC_KSL_ARRAY_STRING = 
        "--Array\n"
        "array_meta_ = {}\n"
        "array_meta_.l__ = {}\n"
        "array_meta_.get_at = function(self, i) return self.l__[i+1] end\n"
        "array_meta_.set_at = function(self, i, x) self.l__[i+1] = x end\n"
        "array_meta_.length = function(self) return #self.l__ end\n"
        "array_meta_.add_at = function(self, i, x) self.l__[i+1] = self.l__[i+1] + x end\n"
        "array_meta_.sub_at = function(self, i, x) self.l__[i+1] = self.l__[i+1] - x end\n"
        "array_meta_.mul_at = function(self, i, x) self.l__[i+1] = self.l__[i+1] * x end\n"
        "array_meta_.div_at = function(self, i, x) self.l__[i+1] = self.l__[i+1] / x end\n"
        "function array(l)\n"
        "    local obj = {}\n"
        "    obj.l__ = l\n"
        "    setmetatable(obj, {__index=array_meta_})\n"
        "	return obj\n"
        "end\n"
        "--Array\n"
    ;
}

#endif
