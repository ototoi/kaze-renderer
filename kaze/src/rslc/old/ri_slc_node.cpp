#include "ri_slc_node.h"
#include <string>
#include <set>

namespace kaze
{
    namespace ri
    {

        class node_type_manager
        {

        public:
            static node_type_manager& instance()
            {
                static node_type_manager inst;
                return inst;
            }
            void set(const std::string& type)
            {
                types_.insert(type);
            }

        protected:
            node_type_manager() {}
        protected:
            std::set<std::string> types_;
        };

        typedef ri_slc_node::LPCSTR LPCSTR;

        ri_slc_node::ri_slc_node(const std::string& type)
            : type_(type)
        {
            printf("%s\n", type.c_str());
            node_type_manager::instance().set(type);
            params_ = new ri_slc_parameters();
        }
        ri_slc_node::~ri_slc_node()
        {
            delete params_;
        }
        const std::string& ri_slc_node::type() const
        {
            return type_;
        }
        std::shared_ptr<ri_slc_node> ri_slc_node::clone() const
        {
            return std::shared_ptr<ri_slc_node>();
        }

        ri_slc_node::ri_slc_node(const ri_slc_node& node)
            : type_(node.type_)
        {
            params_ = new ri_slc_parameters(*(node.params_));
            children_ = node.clone_children();
        }

        void ri_slc_node::add_front(const auto_count_ptr<ri_slc_node>& c)
        {
            std::vector<std::shared_ptr<ri_slc_node> > tmp;
            tmp.push_back(c);
            for (size_t i = 0; i < children_.size(); i++)
                tmp.push_back(children_[i]);
            children_.swap(tmp);
        }
        void ri_slc_node::add_back(const auto_count_ptr<ri_slc_node>& c)
        {
            children_.push_back(c);
        }
        void ri_slc_node::add_child(const auto_count_ptr<ri_slc_node>& c)
        {
            children_.push_back(c);
        }

        size_t ri_slc_node::get_children_size() const
        {
            return children_.size();
        }
        const std::shared_ptr<ri_slc_node>& ri_slc_node::get_child_at(size_t i) const
        {
            return children_[i];
        }
        std::vector<std::shared_ptr<ri_slc_node> > ri_slc_node::clone_children() const
        {
            std::vector<std::shared_ptr<ri_slc_node> > ret;
            for (size_t i = 0; i < children_.size(); i++)
            {
                std::shared_ptr<ri_slc_node> c = children_[i]->clone();
                ret.push_back(c);
            }
            return ret;
        }

        bool ri_slc_node::set(const char* key, const ri_slc_parameters::value_type& val)
        {
            return params_->set(key, val);
        }
        bool ri_slc_node::set(const char* key, int val)
        {
            return params_->set(key, val);
        }
        bool ri_slc_node::set(const char* key, float val)
        {
            return params_->set(key, val);
        }
        bool ri_slc_node::set(const char* key, const char* val)
        {
            return params_->set(key, val);
        }
        bool ri_slc_node::set(const char* key, const int* val, int n)
        {
            return params_->set(key, val, n);
        }
        bool ri_slc_node::set(const char* key, const float* val, int n)
        {
            return params_->set(key, val, n);
        }
        bool ri_slc_node::set(const char* key, const LPCSTR* val, int n)
        {
            return params_->set(key, val, n);
        }
        bool ri_slc_node::set(const char* key, const std::string& val)
        {
            return params_->set(key, val);
        }
        bool ri_slc_node::set(const char* key, const std::string* val, int n)
        {
            return params_->set(key, val, n);
        }

        const ri_slc_parameters::value_type* ri_slc_node::get(const char* key) const
        {
            return params_->get(key);
        }
    }
}
