/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison LALR(1) parsers in C++

   Copyright (C) 2002, 2003, 2004, 2005, 2006 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#include "rsl_parser.hpp"

/* User implementation prologue.  */
#line 48 "rsl_parser.yy"

#define yyerror(m) error(yylloc, m)

using namespace kaze::ri;

#define GET_CTX() (((ri_rsl_parse_param*)(scanner))->ctx)
#define GET_FNAME() (((ri_rsl_parse_param*)(scanner))->filename)
#define GET_LINE() (((ri_rsl_parse_param*)(scanner))->line_number)
#define PRINT(s) printf(s)

static char* dups_(const char* s)
{
    int l = strlen(s);
    char* sRet = new char[l + 1];
    strcpy(sRet, s);
    return sRet;
}

typedef kaze::ri::ri_slc_node* PNODE;
typedef char* LPCSTR;

static void release_value(LPCSTR v)
{
    delete[] v;
    v = NULL;
}
//static
//void release_value(PNODE& v){delete v;v=NULL;}

#define YY_DECL                                  \
    int                                          \
    yylex(yy::rsl_parser::semantic_type* yylval, \
          yy::rsl_parser::location_type* yylloc, \
          void* scanner)

YY_DECL;

/* Line 317 of lalr1.cc.  */
#line 83 "rsl_parser.cpp"

#ifndef YY_
#if defined YYENABLE_NLS && YYENABLE_NLS
#if ENABLE_NLS
#include <libintl.h> /* FIXME: INFRINGES ON USER NAME SPACE */
#define YY_(msgid) dgettext("bison-runtime", msgid)
#endif
#endif
#ifndef YY_
#define YY_(msgid) msgid
#endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#define YYUSE(e) ((void)(e))

/* A pseudo ostream that takes yydebug_ into account.  */
#define YYCDEBUG                                                           \
    for (bool yydebugcond_ = yydebug_; yydebugcond_; yydebugcond_ = false) \
    (*yycdebug_)

/* Enable debugging if requested.  */
#if YYDEBUG

#define YY_SYMBOL_PRINT(Title, Type, Value, Location)      \
    do                                                     \
    {                                                      \
        if (yydebug_)                                      \
        {                                                  \
            *yycdebug_ << Title << ' ';                    \
            yy_symbol_print_((Type), (Value), (Location)); \
            *yycdebug_ << std::endl;                       \
        }                                                  \
    } while (false)

#define YY_REDUCE_PRINT(Rule)       \
    do                              \
    {                               \
        if (yydebug_)               \
            yy_reduce_print_(Rule); \
    } while (false)

#define YY_STACK_PRINT()      \
    do                        \
    {                         \
        if (yydebug_)         \
            yystack_print_(); \
    } while (false)

#else /* !YYDEBUG */

#define YY_SYMBOL_PRINT(Title, Type, Value, Location)
#define YY_REDUCE_PRINT(Rule)
#define YY_STACK_PRINT()

#endif /* !YYDEBUG */

#define YYACCEPT goto yyacceptlab
#define YYABORT goto yyabortlab
#define YYERROR goto yyerrorlab

namespace yy
{
#if YYERROR_VERBOSE

    /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
    std::string
    rsl_parser::yytnamerr_(const char* yystr)
    {
        if (*yystr == '"')
        {
            std::string yyr = "";
            char const* yyp = yystr;

            for (;;)
                switch (*++yyp)
                {
                case '\'':
                case ',':
                    goto do_not_strip_quotes;

                case '\\':
                    if (*++yyp != '\\')
                        goto do_not_strip_quotes;
                /* Fall through.  */
                default:
                    yyr += *yyp;
                    break;

                case '"':
                    return yyr;
                }
        do_not_strip_quotes:;
        }

        return yystr;
    }

#endif

    /// Build a parser object.
    rsl_parser::rsl_parser(void* scanner_yyarg)
        : yydebug_(false),
          yycdebug_(&std::cerr),
          scanner(scanner_yyarg)
    {
    }

    rsl_parser::~rsl_parser()
    {
    }

#if YYDEBUG
    /*--------------------------------.
  | Print this symbol on YYOUTPUT.  |
  `--------------------------------*/

    inline void
    rsl_parser::yy_symbol_value_print_(int yytype,
                                       const semantic_type* yyvaluep, const location_type* yylocationp)
    {
        YYUSE(yylocationp);
        YYUSE(yyvaluep);
        switch (yytype)
        {
        default:
            break;
        }
    }

    void
    rsl_parser::yy_symbol_print_(int yytype,
                                 const semantic_type* yyvaluep, const location_type* yylocationp)
    {
        *yycdebug_ << (yytype < yyntokens_ ? "token" : "nterm")
                   << ' ' << yytname_[yytype] << " ("
                   << *yylocationp << ": ";
        yy_symbol_value_print_(yytype, yyvaluep, yylocationp);
        *yycdebug_ << ')';
    }
#endif /* ! YYDEBUG */

    void
    rsl_parser::yydestruct_(const char* yymsg,
                            int yytype, semantic_type* yyvaluep, location_type* yylocationp)
    {
        YYUSE(yylocationp);
        YYUSE(yymsg);
        YYUSE(yyvaluep);

        YY_SYMBOL_PRINT(yymsg, yytype, yyvaluep, yylocationp);

        switch (yytype)
        {

        default:
            break;
        }
    }

    void
    rsl_parser::yypop_(unsigned int n)
    {
        yystate_stack_.pop(n);
        yysemantic_stack_.pop(n);
        yylocation_stack_.pop(n);
    }

    std::ostream&
    rsl_parser::debug_stream() const
    {
        return *yycdebug_;
    }

    void
    rsl_parser::set_debug_stream(std::ostream& o)
    {
        yycdebug_ = &o;
    }

    rsl_parser::debug_level_type
    rsl_parser::debug_level() const
    {
        return yydebug_;
    }

    void
    rsl_parser::set_debug_level(debug_level_type l)
    {
        yydebug_ = l;
    }

    int
    rsl_parser::parse()
    {
        /// Look-ahead and look-ahead in internal form.
        int yychar = yyempty_;
        int yytoken = 0;

        /* State.  */
        int yyn;
        int yylen = 0;
        int yystate = 0;

        /* Error handling.  */
        int yynerrs_ = 0;
        int yyerrstatus_ = 0;

        /// Semantic value of the look-ahead.
        semantic_type yylval;
        /// Location of the look-ahead.
        location_type yylloc;
        /// The locations where the error started and ended.
        location yyerror_range[2];

        /// $$.
        semantic_type yyval;
        /// @$.
        location_type yyloc;

        int yyresult;

        YYCDEBUG << "Starting parse" << std::endl;

        /* Initialize the stacks.  The initial state will be pushed in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
        yystate_stack_ = state_stack_type(0);
        yysemantic_stack_ = semantic_stack_type(0);
        yylocation_stack_ = location_stack_type(0);
        yysemantic_stack_.push(yylval);
        yylocation_stack_.push(yylloc);

    /* New state.  */
    yynewstate:
        yystate_stack_.push(yystate);
        YYCDEBUG << "Entering state " << yystate << std::endl;
        goto yybackup;

    /* Backup.  */
    yybackup:

        /* Try to take a decision without look-ahead.  */
        yyn = yypact_[yystate];
        if (yyn == yypact_ninf_)
            goto yydefault;

        /* Read a look-ahead token.  */
        if (yychar == yyempty_)
        {
            YYCDEBUG << "Reading a token: ";
            yychar = yylex(&yylval, &yylloc, scanner);
        }

        /* Convert token to internal form.  */
        if (yychar <= yyeof_)
        {
            yychar = yytoken = yyeof_;
            YYCDEBUG << "Now at end of input." << std::endl;
        }
        else
        {
            yytoken = yytranslate_(yychar);
            YY_SYMBOL_PRINT("Next token is", yytoken, &yylval, &yylloc);
        }

        /* If the proper action on seeing token YYTOKEN is to reduce or to
       detect an error, take that action.  */
        yyn += yytoken;
        if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yytoken)
            goto yydefault;

        /* Reduce or error.  */
        yyn = yytable_[yyn];
        if (yyn <= 0)
        {
            if (yyn == 0 || yyn == yytable_ninf_)
                goto yyerrlab;
            yyn = -yyn;
            goto yyreduce;
        }

        /* Accept?  */
        if (yyn == yyfinal_)
            goto yyacceptlab;

        /* Shift the look-ahead token.  */
        YY_SYMBOL_PRINT("Shifting", yytoken, &yylval, &yylloc);

        /* Discard the token being shifted unless it is eof.  */
        if (yychar != yyeof_)
            yychar = yyempty_;

        yysemantic_stack_.push(yylval);
        yylocation_stack_.push(yylloc);

        /* Count tokens shifted since error; after three, turn off error
       status.  */
        if (yyerrstatus_)
            --yyerrstatus_;

        yystate = yyn;
        goto yynewstate;

    /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
    yydefault:
        yyn = yydefact_[yystate];
        if (yyn == 0)
            goto yyerrlab;
        goto yyreduce;

    /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
    yyreduce:
        yylen = yyr2_[yyn];
        /* If YYLEN is nonzero, implement the default value of the action:
       `$$ = $1'.  Otherwise, use the top of the stack.

       Otherwise, the following line sets YYVAL to garbage.
       This behavior is undocumented and Bison
       users should not rely upon it.  */
        if (yylen)
            yyval = yysemantic_stack_[yylen - 1];
        else
            yyval = yysemantic_stack_[0];

        {
            slice<location_type, location_stack_type> slice(yylocation_stack_, yylen);
            YYLLOC_DEFAULT(yyloc, slice, yylen);
        }
        YY_REDUCE_PRINT(yyn);
        switch (yyn)
        {
        case 2:
#line 231 "rsl_parser.yy"
        {
            if ((yysemantic_stack_[(1) - (1)].ntype) != NULL)
            {
                GET_CTX()
                    ->set_ast_node((yysemantic_stack_[(1) - (1)].ntype));
                (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            };
        }
        break;

        case 3:
#line 241 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("definitions");
            (yyval.ntype)->add_child((yysemantic_stack_[(1) - (1)].ntype));
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 4:
#line 247 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(2) - (1)].ntype);
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            (yysemantic_stack_[(2) - (1)].ntype) = NULL;
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 5:
#line 257 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 6:
#line 262 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 7:
#line 270 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("shader_definition");
            (yyval.ntype)->set("name", (yysemantic_stack_[(8) - (2)].stype));
            (yyval.ntype)->add_child((yysemantic_stack_[(8) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(8) - (4)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(8) - (7)].ntype));
            (yysemantic_stack_[(8) - (1)].ntype) = NULL;
            release_value((yysemantic_stack_[(8) - (2)].stype));
            (yysemantic_stack_[(8) - (4)].ntype) = NULL;
            (yysemantic_stack_[(8) - (7)].ntype) = NULL;
            ;
        }
        break;

        case 8:
#line 282 "rsl_parser.yy"
        {
            ri_slc_node* formals = new ri_slc_node("formals");

            (yyval.ntype) = new ri_slc_node("shader_definition");
            (yyval.ntype)->set("name", (yysemantic_stack_[(7) - (2)].stype));
            (yyval.ntype)->add_child((yysemantic_stack_[(7) - (1)].ntype));
            (yyval.ntype)->add_child(formals);
            (yyval.ntype)->add_child((yysemantic_stack_[(7) - (6)].ntype));
            (yysemantic_stack_[(7) - (1)].ntype) = NULL;
            release_value((yysemantic_stack_[(7) - (2)].stype));
            (yysemantic_stack_[(7) - (6)].ntype) = NULL;
            ;
        }
        break;

        case 9:
#line 298 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("function_definition");
            (yyval.ntype)->set("name", (yysemantic_stack_[(8) - (2)].stype));
            (yyval.ntype)->add_child((yysemantic_stack_[(8) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(8) - (4)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(8) - (7)].ntype));
            (yysemantic_stack_[(8) - (1)].ntype) = NULL;
            release_value((yysemantic_stack_[(8) - (2)].stype));
            (yysemantic_stack_[(8) - (4)].ntype) = NULL;
            (yysemantic_stack_[(8) - (7)].ntype) = NULL;
            ;
        }
        break;

        case 10:
#line 310 "rsl_parser.yy"
        {
            ri_slc_node* formals = new ri_slc_node("formals");

            (yyval.ntype) = new ri_slc_node("function_definition");
            (yyval.ntype)->set("name", (yysemantic_stack_[(7) - (2)].stype));
            (yyval.ntype)->add_child((yysemantic_stack_[(7) - (1)].ntype));
            (yyval.ntype)->add_child(formals);
            (yyval.ntype)->add_child((yysemantic_stack_[(7) - (6)].ntype));
            (yysemantic_stack_[(7) - (1)].ntype) = NULL;
            release_value((yysemantic_stack_[(7) - (2)].stype));
            (yysemantic_stack_[(7) - (6)].ntype) = NULL;
            ;
        }
        break;

        case 11:
#line 326 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("shader_type");
            (yyval.ntype)->set("type", "light");
            ;
        }
        break;

        case 12:
#line 331 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("shader_type");
            (yyval.ntype)->set("type", "surface");
            ;
        }
        break;

        case 13:
#line 335 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("shader_type");
            (yyval.ntype)->set("type", "volume");
            ;
        }
        break;

        case 14:
#line 340 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("shader_type");
            (yyval.ntype)->set("type", "displacement");
            ;
        }
        break;

        case 15:
#line 345 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("shader_type");
            (yyval.ntype)->set("type", "transformation");
            ;
        }
        break;

        case 16:
#line 349 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("shader_type");
            (yyval.ntype)->set("type", "imager");
            ;
        }
        break;

        case 17:
#line 357 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("formals");
            (yyval.ntype)->add_child((yysemantic_stack_[(1) - (1)].ntype));
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 18:
#line 363 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(3) - (1)].ntype);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 19:
#line 370 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(2) - (1)].ntype);
            (yysemantic_stack_[(2) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 20:
#line 378 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("variable_definitions");
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            (yysemantic_stack_[(2) - (1)].ntype) = NULL;
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 21:
#line 386 "rsl_parser.yy"
        {
            (yysemantic_stack_[(3) - (2)].ntype)->set("extern", "out");
            (yyval.ntype) = new ri_slc_node("variable_definitions");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (2)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (2)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 22:
#line 399 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("variable_definitions");
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            (yysemantic_stack_[(2) - (1)].ntype) = NULL;
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 23:
#line 407 "rsl_parser.yy"
        {
            (yysemantic_stack_[(3) - (2)].ntype)->set("extern", "extern");
            (yyval.ntype) = new ri_slc_node("variable_definitions");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (2)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (2)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 24:
#line 419 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(2) - (2)].ntype);
            (yyval.ntype)->set("storage", (yysemantic_stack_[(2) - (1)].stype));
            release_value((yysemantic_stack_[(2) - (1)].stype));
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 25:
#line 426 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 26:
#line 434 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 27:
#line 441 "rsl_parser.yy"
        {
            ;
            ;
        }
        break;

        case 28:
#line 445 "rsl_parser.yy"
        {
            ;
            ;
        }
        break;

        case 29:
#line 452 "rsl_parser.yy"
        {
            ri_slc_node* ref = new ri_slc_node("valref");
            (yyval.ntype) = new ri_slc_node("def");
            (yyval.ntype)->add_child(ref);
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            release_value((yysemantic_stack_[(2) - (1)].stype));
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 30:
#line 461 "rsl_parser.yy"
        {
            ri_slc_node* ref = new ri_slc_node("valref");
            (yyval.ntype) = new ri_slc_node("def");
            (yyval.ntype)->add_child(ref);
            release_value((yysemantic_stack_[(1) - (1)].stype));
            ;
        }
        break;

        case 31:
#line 468 "rsl_parser.yy"
        {
            ri_slc_node* ref = new ri_slc_node("arrayref");
            ref->set("size", (yysemantic_stack_[(4) - (3)].ftype));
            (yyval.ntype) = new ri_slc_node("def");
            (yyval.ntype)->add_child(ref);
            release_value((yysemantic_stack_[(4) - (1)].stype));
            ;
        }
        break;

        case 32:
#line 476 "rsl_parser.yy"
        {
            ri_slc_node* ref = new ri_slc_node("arrayref");
            ref->set("size", (yysemantic_stack_[(5) - (3)].ftype));
            (yyval.ntype) = new ri_slc_node("def");
            (yyval.ntype)->add_child(ref);
            (yyval.ntype)->add_child((yysemantic_stack_[(5) - (5)].ntype));
            release_value((yysemantic_stack_[(5) - (1)].stype));
            (yysemantic_stack_[(5) - (5)].ntype) = NULL;
            ;
        }
        break;

        case 33:
#line 486 "rsl_parser.yy"
        {
            ri_slc_node* ref = new ri_slc_node("arrayref");
            ref->set("size", (int)((yysemantic_stack_[(4) - (4)].ntype)->get_children_size()));
            (yyval.ntype) = new ri_slc_node("def");
            (yyval.ntype)->add_child(ref);
            (yyval.ntype)->add_child((yysemantic_stack_[(4) - (4)].ntype));
            release_value((yysemantic_stack_[(4) - (1)].stype));
            (yysemantic_stack_[(4) - (4)].ntype) = NULL;
            ;
        }
        break;

        case 34:
#line 499 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(2) - (2)].ntype);
            ;
        }
        break;

        case 35:
#line 506 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(4) - (3)].ntype);
            (yysemantic_stack_[(4) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 36:
#line 513 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("array_initialisers");
            (yyval.ntype)->add_child((yysemantic_stack_[(1) - (1)].ntype));
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 37:
#line 519 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(3) - (1)].ntype);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 38:
#line 529 "rsl_parser.yy"
        {
            (yyval.stype) = dups_("uniform");
            ;
        }
        break;

        case 39:
#line 534 "rsl_parser.yy"
        {
            (yyval.stype) = dups_("varying");
            ;
        }
        break;

        case 40:
#line 542 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("type");
            (yyval.ntype)->set("type", "float");
            ;
        }
        break;

        case 41:
#line 547 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("type");
            (yyval.ntype)->set("type", "string");
            ;
        }
        break;

        case 42:
#line 552 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("type");
            (yyval.ntype)->set("type", "color");
            ;
        }
        break;

        case 43:
#line 557 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("type");
            (yyval.ntype)->set("type", "point");
            ;
        }
        break;

        case 44:
#line 562 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("type");
            (yyval.ntype)->set("type", "vector");
            ;
        }
        break;

        case 45:
#line 567 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("type");
            (yyval.ntype)->set("type", "normal");
            ;
        }
        break;

        case 46:
#line 572 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("type");
            (yyval.ntype)->set("type", "matrix");
            ;
        }
        break;

        case 47:
#line 577 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("type");
            (yyval.ntype)->set("type", "void");
            ;
        }
        break;

        case 48:
#line 585 "rsl_parser.yy"
        {
            (yyval.ctype) = (yysemantic_stack_[(1) - (1)].ctype);
            ;
        }
        break;

        case 49:
#line 592 "rsl_parser.yy"
        {
            (yyval.ctype) = (yysemantic_stack_[(1) - (1)].ctype);
            ;
        }
        break;

        case 50:
#line 599 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("statements");
            (yyval.ntype)->add_child((yysemantic_stack_[(1) - (1)].ntype));
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 51:
#line 605 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(2) - (1)].ntype);
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            (yysemantic_stack_[(2) - (1)].ntype) = NULL;
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 52:
#line 615 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 53:
#line 620 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 54:
#line 625 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 55:
#line 630 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 56:
#line 635 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 57:
#line 640 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 58:
#line 645 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 59:
#line 650 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 60:
#line 659 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(2) - (1)].ntype);
            (yysemantic_stack_[(2) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 61:
#line 667 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(2) - (1)].ntype);
            (yysemantic_stack_[(2) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 62:
#line 675 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("return");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (2)].ntype));
            (yysemantic_stack_[(3) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 63:
#line 684 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(2) - (1)].ntype);
            (yysemantic_stack_[(2) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 64:
#line 689 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 65:
#line 697 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("block");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (2)].ntype));
            (yysemantic_stack_[(3) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 66:
#line 703 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("block");
            ;
        }
        break;

        case 67:
#line 707 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("block");
            ;
        }
        break;

        case 68:
#line 715 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 69:
#line 720 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 70:
#line 725 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 71:
#line 730 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 72:
#line 735 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 73:
#line 740 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 74:
#line 747 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("while");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (2)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (2)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 75:
#line 755 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("while");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (2)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (2)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 76:
#line 766 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("for");
            (yyval.ntype)->add_child((yysemantic_stack_[(9) - (3)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(9) - (5)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(9) - (7)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(9) - (9)].ntype));
            (yysemantic_stack_[(9) - (3)].ntype) = NULL;
            (yysemantic_stack_[(9) - (5)].ntype) = NULL;
            (yysemantic_stack_[(9) - (7)].ntype) = NULL;
            (yysemantic_stack_[(9) - (9)].ntype) = NULL;
            ;
        }
        break;

        case 77:
#line 778 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("for");
            (yyval.ntype)->add_child((yysemantic_stack_[(9) - (3)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(9) - (5)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(9) - (7)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(9) - (9)].ntype));
            (yysemantic_stack_[(9) - (3)].ntype) = NULL;
            (yysemantic_stack_[(9) - (5)].ntype) = NULL;
            (yysemantic_stack_[(9) - (7)].ntype) = NULL;
            (yysemantic_stack_[(9) - (9)].ntype) = NULL;
            ;
        }
        break;

        case 78:
#line 793 "rsl_parser.yy"
        {
            ri_slc_node* args = new ri_slc_node("function_arguments");
            (yyval.ntype) = new ri_slc_node("solar");
            (yyval.ntype)->add_child(args);
            (yyval.ntype)->add_child((yysemantic_stack_[(4) - (4)].ntype));
            (yysemantic_stack_[(4) - (4)].ntype) = NULL;
            ;
        }
        break;

        case 79:
#line 801 "rsl_parser.yy"
        {
            ri_slc_node* args = new ri_slc_node("function_arguments");
            args->add_child((yysemantic_stack_[(7) - (3)].ntype));
            args->add_child((yysemantic_stack_[(7) - (5)].ntype));
            (yyval.ntype) = new ri_slc_node("solar");
            (yyval.ntype)->add_child(args);
            (yyval.ntype)->add_child((yysemantic_stack_[(7) - (7)].ntype));
            (yysemantic_stack_[(7) - (3)].ntype) = NULL;
            (yysemantic_stack_[(7) - (5)].ntype) = NULL;
            (yysemantic_stack_[(7) - (7)].ntype) = NULL;
            ;
        }
        break;

        case 80:
#line 816 "rsl_parser.yy"
        {
            ri_slc_node* args = new ri_slc_node("function_arguments");
            args->add_child((yysemantic_stack_[(5) - (3)].ntype));
            (yyval.ntype) = new ri_slc_node("illuminate");
            (yyval.ntype)->add_child(args);
            (yyval.ntype)->add_child((yysemantic_stack_[(5) - (5)].ntype));
            (yysemantic_stack_[(5) - (3)].ntype) = NULL;
            (yysemantic_stack_[(5) - (5)].ntype) = NULL;
            ;
        }
        break;

        case 81:
#line 826 "rsl_parser.yy"
        {
            ri_slc_node* args = new ri_slc_node("function_arguments");
            args->add_child((yysemantic_stack_[(9) - (3)].ntype));
            args->add_child((yysemantic_stack_[(9) - (5)].ntype));
            args->add_child((yysemantic_stack_[(9) - (7)].ntype));
            (yyval.ntype) = new ri_slc_node("illuminate");
            (yyval.ntype)->add_child(args);
            (yyval.ntype)->add_child((yysemantic_stack_[(9) - (9)].ntype));
            (yysemantic_stack_[(9) - (3)].ntype) = NULL;
            (yysemantic_stack_[(9) - (5)].ntype) = NULL;
            (yysemantic_stack_[(9) - (7)].ntype) = NULL;
            (yysemantic_stack_[(9) - (9)].ntype) = NULL;
            ;
        }
        break;

        case 82:
#line 843 "rsl_parser.yy"
        {
            ri_slc_node* args = new ri_slc_node("function_arguments");
            args->add_child((yysemantic_stack_[(5) - (3)].ntype));
            (yyval.ntype) = new ri_slc_node("illuminance");
            (yyval.ntype)->add_child(args);
            (yyval.ntype)->add_child((yysemantic_stack_[(5) - (5)].ntype));
            (yysemantic_stack_[(5) - (3)].ntype) = NULL;
            (yysemantic_stack_[(5) - (5)].ntype) = NULL;
            ;
        }
        break;

        case 83:
#line 853 "rsl_parser.yy"
        {
            ri_slc_node* args = new ri_slc_node("function_arguments");
            args->add_child((yysemantic_stack_[(7) - (3)].ntype));
            args->add_child((yysemantic_stack_[(7) - (5)].ntype));
            (yyval.ntype) = new ri_slc_node("illuminance");
            (yyval.ntype)->add_child(args);
            (yyval.ntype)->add_child((yysemantic_stack_[(7) - (7)].ntype));
            (yysemantic_stack_[(7) - (3)].ntype) = NULL;
            (yysemantic_stack_[(7) - (5)].ntype) = NULL;
            (yysemantic_stack_[(7) - (7)].ntype) = NULL;
            ;
        }
        break;

        case 84:
#line 865 "rsl_parser.yy"
        {
            ri_slc_node* args = new ri_slc_node("function_arguments");
            args->add_child((yysemantic_stack_[(9) - (3)].ntype));
            args->add_child((yysemantic_stack_[(9) - (5)].ntype));
            args->add_child((yysemantic_stack_[(9) - (7)].ntype));
            (yyval.ntype) = new ri_slc_node("illuminance");
            (yyval.ntype)->add_child(args);
            (yyval.ntype)->add_child((yysemantic_stack_[(9) - (9)].ntype));
            (yysemantic_stack_[(9) - (3)].ntype) = NULL;
            (yysemantic_stack_[(9) - (5)].ntype) = NULL;
            (yysemantic_stack_[(9) - (7)].ntype) = NULL;
            (yysemantic_stack_[(9) - (9)].ntype) = NULL;
            ;
        }
        break;

        case 85:
#line 879 "rsl_parser.yy"
        {
            ri_slc_node* args = new ri_slc_node("function_arguments");
            args->add_child((yysemantic_stack_[(11) - (3)].ntype));
            args->add_child((yysemantic_stack_[(11) - (5)].ntype));
            args->add_child((yysemantic_stack_[(11) - (7)].ntype));
            args->add_child((yysemantic_stack_[(11) - (9)].ntype));
            (yyval.ntype) = new ri_slc_node("illuminance");
            (yyval.ntype)->add_child(args);
            (yyval.ntype)->add_child((yysemantic_stack_[(11) - (11)].ntype));
            (yysemantic_stack_[(11) - (3)].ntype) = NULL;
            (yysemantic_stack_[(11) - (5)].ntype) = NULL;
            (yysemantic_stack_[(11) - (7)].ntype) = NULL;
            (yysemantic_stack_[(11) - (9)].ntype) = NULL;
            (yysemantic_stack_[(11) - (11)].ntype) = NULL;
            ;
        }
        break;

        case 86:
#line 898 "rsl_parser.yy"
        {
            ri_slc_node* args = new ri_slc_node("function_arguments");
            args->add_child((yysemantic_stack_[(13) - (3)].ntype));
            args->add_child((yysemantic_stack_[(13) - (5)].ntype));
            args->add_child((yysemantic_stack_[(13) - (7)].ntype));
            args->add_child((yysemantic_stack_[(13) - (9)].ntype));
            args->add_child((yysemantic_stack_[(13) - (11)].ntype));
            (yyval.ntype) = new ri_slc_node("gather");
            (yyval.ntype)->add_child(args);
            (yyval.ntype)->add_child((yysemantic_stack_[(13) - (13)].ntype));
            (yysemantic_stack_[(13) - (3)].ntype) = NULL;
            (yysemantic_stack_[(13) - (5)].ntype) = NULL;
            (yysemantic_stack_[(13) - (7)].ntype) = NULL;
            (yysemantic_stack_[(13) - (9)].ntype) = NULL;
            (yysemantic_stack_[(13) - (11)].ntype) = NULL;
            (yysemantic_stack_[(13) - (13)].ntype) = NULL;
            ;
        }
        break;

        case 87:
#line 921 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("if");
            (yyval.ntype)->set("haselse", 0);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (2)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (2)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 88:
#line 930 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("if");
            (yyval.ntype)->set("haselse", 0);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (2)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (2)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 89:
#line 939 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("if");
            (yyval.ntype)->set("haselse", 1);
            (yyval.ntype)->add_child((yysemantic_stack_[(5) - (2)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(5) - (3)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(5) - (5)].ntype));
            (yysemantic_stack_[(5) - (2)].ntype) = NULL;
            (yysemantic_stack_[(5) - (3)].ntype) = NULL;
            (yysemantic_stack_[(5) - (5)].ntype) = NULL;
            ;
        }
        break;

        case 90:
#line 950 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("if");
            (yyval.ntype)->set("haselse", 1);
            (yyval.ntype)->add_child((yysemantic_stack_[(5) - (2)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(5) - (3)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(5) - (5)].ntype));
            (yysemantic_stack_[(5) - (2)].ntype) = NULL;
            (yysemantic_stack_[(5) - (3)].ntype) = NULL;
            (yysemantic_stack_[(5) - (5)].ntype) = NULL;
            ;
        }
        break;

        case 91:
#line 964 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("break");
            (yyval.ntype)->set("label", (yysemantic_stack_[(3) - (2)].ftype));
            ;
        }
        break;

        case 92:
#line 969 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("break");
            ;
        }
        break;

        case 93:
#line 973 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("continue");
            (yyval.ntype)->set("label", (yysemantic_stack_[(3) - (2)].ftype));
            ;
        }
        break;

        case 94:
#line 978 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("continue");
            ;
        }
        break;

        case 95:
#line 987 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 96:
#line 992 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node(".");
            (yyval.ntype)->set("args", 2);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 97:
#line 1001 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("/");
            (yyval.ntype)->set("args", 2);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 98:
#line 1010 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("*");
            (yyval.ntype)->set("args", 2);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 99:
#line 1019 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("^");
            (yyval.ntype)->set("args", 2);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 100:
#line 1028 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("+");
            (yyval.ntype)->set("args", 2);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 101:
#line 1037 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("-");
            (yyval.ntype)->set("args", 2);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 102:
#line 1046 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("-");
            (yyval.ntype)->set("args", 1);
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 103:
#line 1053 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("?");
            (yyval.ntype)->add_child((yysemantic_stack_[(5) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(5) - (3)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(5) - (5)].ntype));
            (yysemantic_stack_[(5) - (1)].ntype) = NULL;
            (yysemantic_stack_[(5) - (3)].ntype) = NULL;
            (yysemantic_stack_[(5) - (5)].ntype) = NULL;
            ;
        }
        break;

        case 104:
#line 1063 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 105:
#line 1070 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("number");
            (yyval.ntype)->set("value", (yysemantic_stack_[(1) - (1)].ftype));
            ;
        }
        break;

        case 106:
#line 1075 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("string");
            (yyval.ntype)->set("value", (yysemantic_stack_[(1) - (1)].stype));
            release_value((yysemantic_stack_[(1) - (1)].stype));
            ;
        }
        break;

        case 107:
#line 1081 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("valref");
            (yyval.ntype)->set("name", (yysemantic_stack_[(1) - (1)].stype));
            release_value((yysemantic_stack_[(1) - (1)].stype));
            ;
        }
        break;

        case 108:
#line 1087 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("arrayref");
            (yyval.ntype)->set("name", (yysemantic_stack_[(4) - (1)].stype));
            (yyval.ntype)->add_child((yysemantic_stack_[(4) - (3)].ntype));
            release_value((yysemantic_stack_[(4) - (1)].stype));
            (yysemantic_stack_[(4) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 109:
#line 1095 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 110:
#line 1100 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 111:
#line 1105 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(3) - (2)].ntype);
            (yysemantic_stack_[(3) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 112:
#line 1110 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 113:
#line 1115 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 114:
#line 1123 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("tuple3");
            (yyval.ntype)->add_child((yysemantic_stack_[(7) - (2)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(7) - (4)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(7) - (6)].ntype));
            (yysemantic_stack_[(7) - (2)].ntype) = NULL;
            (yysemantic_stack_[(7) - (4)].ntype) = NULL;
            (yysemantic_stack_[(7) - (6)].ntype) = NULL;
            ;
        }
        break;

        case 115:
#line 1136 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("tuple16");
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (2)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (4)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (6)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (8)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (10)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (12)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (14)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (16)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (18)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (20)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (22)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (24)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (26)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (28)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (30)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(33) - (32)].ntype));

            (yysemantic_stack_[(33) - (2)].ntype) = NULL;
            (yysemantic_stack_[(33) - (4)].ntype) = NULL;
            (yysemantic_stack_[(33) - (6)].ntype) = NULL;
            (yysemantic_stack_[(33) - (8)].ntype) = NULL; //4
            (yysemantic_stack_[(33) - (10)].ntype) = NULL;
            (yysemantic_stack_[(33) - (12)].ntype) = NULL;
            (yysemantic_stack_[(33) - (14)].ntype) = NULL;
            (yysemantic_stack_[(33) - (16)].ntype) = NULL; //8
            (yysemantic_stack_[(33) - (18)].ntype) = NULL;
            (yysemantic_stack_[(33) - (20)].ntype) = NULL;
            (yysemantic_stack_[(33) - (22)].ntype) = NULL;
            (yysemantic_stack_[(33) - (24)].ntype) = NULL; //12
            (yysemantic_stack_[(33) - (26)].ntype) = NULL;
            (yysemantic_stack_[(33) - (28)].ntype) = NULL;
            (yysemantic_stack_[(33) - (30)].ntype) = NULL;
            (yysemantic_stack_[(33) - (32)].ntype) = NULL; //16
            ;
        }
        break;

        case 116:
#line 1176 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(3) - (2)].ntype);
            (yysemantic_stack_[(3) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 117:
#line 1181 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node(">");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 118:
#line 1189 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node(">=");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 119:
#line 1197 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("<");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 120:
#line 1205 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("<=");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 121:
#line 1213 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("==");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 122:
#line 1221 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("!=");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 123:
#line 1229 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("&&");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 124:
#line 1237 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("||");
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (1)].ntype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 125:
#line 1244 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("!");
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 126:
#line 1253 "rsl_parser.yy"
        {
            ri_slc_node* left = new ri_slc_node("valref");
            left->set("name", (yysemantic_stack_[(3) - (1)].stype));
            (yyval.ntype) = new ri_slc_node("=");
            (yyval.ntype)->add_child(left);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            release_value((yysemantic_stack_[(3) - (1)].stype));
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 127:
#line 1263 "rsl_parser.yy"
        {
            ri_slc_node* left = new ri_slc_node("valref");
            left->set("name", (yysemantic_stack_[(3) - (1)].stype));
            (yyval.ntype) = new ri_slc_node("+=");
            (yyval.ntype)->add_child(left);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            release_value((yysemantic_stack_[(3) - (1)].stype));
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 128:
#line 1273 "rsl_parser.yy"
        {
            ri_slc_node* left = new ri_slc_node("valref");
            left->set("name", (yysemantic_stack_[(3) - (1)].stype));
            (yyval.ntype) = new ri_slc_node("-=");
            (yyval.ntype)->add_child(left);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            release_value((yysemantic_stack_[(3) - (1)].stype));
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 129:
#line 1283 "rsl_parser.yy"
        {
            ri_slc_node* left = new ri_slc_node("valref");
            left->set("name", (yysemantic_stack_[(3) - (1)].stype));
            (yyval.ntype) = new ri_slc_node("*=");
            (yyval.ntype)->add_child(left);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            release_value((yysemantic_stack_[(3) - (1)].stype));
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 130:
#line 1293 "rsl_parser.yy"
        {
            ri_slc_node* left = new ri_slc_node("valref");
            left->set("name", (yysemantic_stack_[(3) - (1)].stype));
            (yyval.ntype) = new ri_slc_node("/=");
            (yyval.ntype)->add_child(left);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            release_value((yysemantic_stack_[(3) - (1)].stype));
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 131:
#line 1303 "rsl_parser.yy"
        {
            ri_slc_node* left = new ri_slc_node("arrayref");
            left->set("name", (yysemantic_stack_[(6) - (1)].stype));
            left->add_child((yysemantic_stack_[(6) - (3)].ntype));
            (yyval.ntype) = new ri_slc_node("=");
            (yyval.ntype)->add_child(left);
            (yyval.ntype)->add_child((yysemantic_stack_[(6) - (6)].ntype));
            release_value((yysemantic_stack_[(6) - (1)].stype));
            (yysemantic_stack_[(6) - (3)].ntype) = NULL;
            (yysemantic_stack_[(6) - (6)].ntype) = NULL;
            ;
        }
        break;

        case 132:
#line 1315 "rsl_parser.yy"
        {
            ri_slc_node* left = new ri_slc_node("arrayref");
            left->set("name", (yysemantic_stack_[(6) - (1)].stype));
            left->add_child((yysemantic_stack_[(6) - (3)].ntype));
            (yyval.ntype) = new ri_slc_node("+=");
            (yyval.ntype)->add_child(left);
            (yyval.ntype)->add_child((yysemantic_stack_[(6) - (6)].ntype));
            release_value((yysemantic_stack_[(6) - (1)].stype));
            (yysemantic_stack_[(6) - (3)].ntype) = NULL;
            (yysemantic_stack_[(6) - (6)].ntype) = NULL;
            ;
        }
        break;

        case 133:
#line 1327 "rsl_parser.yy"
        {
            ri_slc_node* left = new ri_slc_node("arrayref");
            left->set("name", (yysemantic_stack_[(6) - (1)].stype));
            left->add_child((yysemantic_stack_[(6) - (3)].ntype));
            (yyval.ntype) = new ri_slc_node("-=");
            (yyval.ntype)->add_child(left);
            (yyval.ntype)->add_child((yysemantic_stack_[(6) - (6)].ntype));
            release_value((yysemantic_stack_[(6) - (1)].stype));
            (yysemantic_stack_[(6) - (3)].ntype) = NULL;
            (yysemantic_stack_[(6) - (6)].ntype) = NULL;
            ;
        }
        break;

        case 134:
#line 1339 "rsl_parser.yy"
        {
            ri_slc_node* left = new ri_slc_node("arrayref");
            left->set("name", (yysemantic_stack_[(6) - (1)].stype));
            left->add_child((yysemantic_stack_[(6) - (3)].ntype));
            (yyval.ntype) = new ri_slc_node("*=");
            (yyval.ntype)->add_child(left);
            (yyval.ntype)->add_child((yysemantic_stack_[(6) - (6)].ntype));
            release_value((yysemantic_stack_[(6) - (1)].stype));
            (yysemantic_stack_[(6) - (3)].ntype) = NULL;
            (yysemantic_stack_[(6) - (6)].ntype) = NULL;
            ;
        }
        break;

        case 135:
#line 1351 "rsl_parser.yy"
        {
            ri_slc_node* left = new ri_slc_node("arrayref");
            left->set("name", (yysemantic_stack_[(6) - (1)].stype));
            left->add_child((yysemantic_stack_[(6) - (3)].ntype));
            (yyval.ntype) = new ri_slc_node("/=");
            (yyval.ntype)->add_child(left);
            (yyval.ntype)->add_child((yysemantic_stack_[(6) - (6)].ntype));
            release_value((yysemantic_stack_[(6) - (1)].stype));
            (yysemantic_stack_[(6) - (3)].ntype) = NULL;
            (yysemantic_stack_[(6) - (6)].ntype) = NULL;
            ;
        }
        break;

        case 136:
#line 1366 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("cast");
            (yyval.ntype)->set("type", "float");
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 137:
#line 1373 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("cast");
            (yyval.ntype)->set("type", "string");
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 138:
#line 1380 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("cast");
            (yyval.ntype)->set("type", "color");
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 139:
#line 1387 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("cast");
            (yyval.ntype)->set("type", "point");
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 140:
#line 1394 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("cast");
            (yyval.ntype)->set("type", "vector");
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 141:
#line 1401 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("cast");
            (yyval.ntype)->set("type", "normal");
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 142:
#line 1408 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("cast");
            (yyval.ntype)->set("type", "matrix");
            (yyval.ntype)->add_child((yysemantic_stack_[(2) - (2)].ntype));
            (yysemantic_stack_[(2) - (2)].ntype) = NULL;
            ;
        }
        break;

        case 143:
#line 1415 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("cast");
            (yyval.ntype)->set("type", "color");
            (yyval.ntype)->set("space", (yysemantic_stack_[(3) - (2)].stype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            release_value((yysemantic_stack_[(3) - (2)].stype));
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 144:
#line 1424 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("cast");
            (yyval.ntype)->set("type", "point");
            (yyval.ntype)->set("space", (yysemantic_stack_[(3) - (2)].stype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            release_value((yysemantic_stack_[(3) - (2)].stype));
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 145:
#line 1433 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("cast");
            (yyval.ntype)->set("type", "vector");
            (yyval.ntype)->set("space", (yysemantic_stack_[(3) - (2)].stype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            release_value((yysemantic_stack_[(3) - (2)].stype));
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 146:
#line 1442 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("cast");
            (yyval.ntype)->set("type", "normal");
            (yyval.ntype)->set("space", (yysemantic_stack_[(3) - (2)].stype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            release_value((yysemantic_stack_[(3) - (2)].stype));
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 147:
#line 1451 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("cast");
            (yyval.ntype)->set("type", "matrix");
            (yyval.ntype)->set("space", (yysemantic_stack_[(3) - (2)].stype));
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            release_value((yysemantic_stack_[(3) - (2)].stype));
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 148:
#line 1464 "rsl_parser.yy"
        {
            ri_slc_node* args = (yysemantic_stack_[(6) - (5)].ntype);
            args->add_front((yysemantic_stack_[(6) - (3)].ntype));
            (yyval.ntype) = new ri_slc_node("function_call");
            (yyval.ntype)->set("name", (yysemantic_stack_[(6) - (1)].stype));
            (yyval.ntype)->add_child(args);
            release_value((yysemantic_stack_[(6) - (1)].stype));
            (yysemantic_stack_[(6) - (3)].ntype) = NULL;
            (yysemantic_stack_[(6) - (5)].ntype) = NULL;
            ;
        }
        break;

        case 149:
#line 1475 "rsl_parser.yy"
        {
            ri_slc_node* args = new ri_slc_node("function_arguments");
            args->add_child((yysemantic_stack_[(4) - (3)].ntype));
            (yyval.ntype) = new ri_slc_node("function_call");
            (yyval.ntype)->set("name", (yysemantic_stack_[(4) - (1)].stype));
            (yyval.ntype)->add_child(args);
            release_value((yysemantic_stack_[(4) - (1)].stype));
            (yysemantic_stack_[(4) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 150:
#line 1485 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("function_call");
            (yyval.ntype)->set("name", (yysemantic_stack_[(4) - (1)].stype));
            (yyval.ntype)->add_child((yysemantic_stack_[(4) - (3)].ntype));
            release_value((yysemantic_stack_[(4) - (1)].stype));
            (yysemantic_stack_[(4) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 151:
#line 1493 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("function_call");
            (yyval.ntype)->set("name", (yysemantic_stack_[(3) - (1)].stype));
            release_value((yysemantic_stack_[(3) - (1)].stype));
            ;
        }
        break;

        case 152:
#line 1502 "rsl_parser.yy"
        {
            (yyval.stype) = (yysemantic_stack_[(1) - (1)].stype);
            ;
        }
        break;

        case 153:
#line 1506 "rsl_parser.yy"
        {
            (yyval.stype) = dups_("atmosphere");
            ;
        }
        break;

        case 154:
#line 1510 "rsl_parser.yy"
        {
            (yyval.stype) = dups_("displacement");
            ;
        }
        break;

        case 155:
#line 1514 "rsl_parser.yy"
        {
            (yyval.stype) = dups_("lightsource");
            ;
        }
        break;

        case 156:
#line 1518 "rsl_parser.yy"
        {
            (yyval.stype) = dups_("surface");
            ;
        }
        break;

        case 157:
#line 1524 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("function_arguments");
            (yyval.ntype)->add_child((yysemantic_stack_[(1) - (1)].ntype));
            (yysemantic_stack_[(1) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 158:
#line 1530 "rsl_parser.yy"
        {
            (yyval.ntype) = (yysemantic_stack_[(3) - (1)].ntype);
            (yyval.ntype)->add_child((yysemantic_stack_[(3) - (3)].ntype));
            (yysemantic_stack_[(3) - (1)].ntype) = NULL;
            (yysemantic_stack_[(3) - (3)].ntype) = NULL;
            ;
        }
        break;

        case 159:
#line 1540 "rsl_parser.yy"
        {
            (yyval.ntype) = new ri_slc_node("texture_argument");
            release_value((yysemantic_stack_[(1) - (1)].stype));
            ;
        }
        break;

        case 160:
#line 1545 "rsl_parser.yy"
        {
            (yysemantic_stack_[(4) - (1)].ntype)->set("channel", (yysemantic_stack_[(4) - (3)].itype));
            (yyval.ntype) = (yysemantic_stack_[(4) - (1)].ntype);
            (yysemantic_stack_[(4) - (1)].ntype) = NULL;
            ;
        }
        break;

        case 161:
#line 1555 "rsl_parser.yy"
        {
            (yyval.ftype) = (float)(yysemantic_stack_[(1) - (1)].itype);
            ;
        }
        break;

        case 162:
#line 1559 "rsl_parser.yy"
        {
            (yyval.ftype) = (float)(yysemantic_stack_[(1) - (1)].ftype);
            ;
        }
        break;

        case 163:
#line 1566 "rsl_parser.yy"
        {
            (yyval.itype) = (yysemantic_stack_[(1) - (1)].itype);
            ;
        }
        break;

/* Line 675 of lalr1.cc.  */
#line 2108 "rsl_parser.cpp"
        default:
            break;
        }
        YY_SYMBOL_PRINT("-> $$ =", yyr1_[yyn], &yyval, &yyloc);

        yypop_(yylen);
        yylen = 0;
        YY_STACK_PRINT();

        yysemantic_stack_.push(yyval);
        yylocation_stack_.push(yyloc);

        /* Shift the result of the reduction.  */
        yyn = yyr1_[yyn];
        yystate = yypgoto_[yyn - yyntokens_] + yystate_stack_[0];
        if (0 <= yystate && yystate <= yylast_ && yycheck_[yystate] == yystate_stack_[0])
            yystate = yytable_[yystate];
        else
            yystate = yydefgoto_[yyn - yyntokens_];
        goto yynewstate;

    /*------------------------------------.
  | yyerrlab -- here on detecting error |
  `------------------------------------*/
    yyerrlab:
        /* If not already recovering from an error, report this error.  */
        if (!yyerrstatus_)
        {
            ++yynerrs_;
            error(yylloc, yysyntax_error_(yystate, yytoken));
        }

        yyerror_range[0] = yylloc;
        if (yyerrstatus_ == 3)
        {
            /* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

            if (yychar <= yyeof_)
            {
                /* Return failure if at end of input.  */
                if (yychar == yyeof_)
                    YYABORT;
            }
            else
            {
                yydestruct_("Error: discarding", yytoken, &yylval, &yylloc);
                yychar = yyempty_;
            }
        }

        /* Else will try to reuse look-ahead token after shifting the error
       token.  */
        goto yyerrlab1;

    /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
    yyerrorlab:

        /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
        if (false)
            goto yyerrorlab;

        yyerror_range[0] = yylocation_stack_[yylen - 1];
        /* Do not reclaim the symbols of the rule which action triggered
       this YYERROR.  */
        yypop_(yylen);
        yylen = 0;
        yystate = yystate_stack_[0];
        goto yyerrlab1;

    /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
    yyerrlab1:
        yyerrstatus_ = 3; /* Each real token shifted decrements this.  */

        for (;;)
        {
            yyn = yypact_[yystate];
            if (yyn != yypact_ninf_)
            {
                yyn += yyterror_;
                if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
                {
                    yyn = yytable_[yyn];
                    if (0 < yyn)
                        break;
                }
            }

            /* Pop the current state because it cannot handle the error token.  */
            if (yystate_stack_.height() == 1)
                YYABORT;

            yyerror_range[0] = yylocation_stack_[0];
            yydestruct_("Error: popping",
                        yystos_[yystate],
                        &yysemantic_stack_[0], &yylocation_stack_[0]);
            yypop_();
            yystate = yystate_stack_[0];
            YY_STACK_PRINT();
        }

        if (yyn == yyfinal_)
            goto yyacceptlab;

        yyerror_range[1] = yylloc;
        // Using YYLLOC is tempting, but would change the location of
        // the look-ahead.  YYLOC is available though.
        YYLLOC_DEFAULT(yyloc, (yyerror_range - 1), 2);
        yysemantic_stack_.push(yylval);
        yylocation_stack_.push(yyloc);

        /* Shift the error token.  */
        YY_SYMBOL_PRINT("Shifting", yystos_[yyn],
                        &yysemantic_stack_[0], &yylocation_stack_[0]);

        yystate = yyn;
        goto yynewstate;

    /* Accept.  */
    yyacceptlab:
        yyresult = 0;
        goto yyreturn;

    /* Abort.  */
    yyabortlab:
        yyresult = 1;
        goto yyreturn;

    yyreturn:
        if (yychar != yyeof_ && yychar != yyempty_)
            yydestruct_("Cleanup: discarding lookahead", yytoken, &yylval, &yylloc);

        /* Do not reclaim the symbols of the rule which action triggered
       this YYABORT or YYACCEPT.  */
        yypop_(yylen);
        while (yystate_stack_.height() != 1)
        {
            yydestruct_("Cleanup: popping",
                        yystos_[yystate_stack_[0]],
                        &yysemantic_stack_[0],
                        &yylocation_stack_[0]);
            yypop_();
        }

        return yyresult;
    }

    // Generate an error message.
    std::string
    rsl_parser::yysyntax_error_(int yystate, int tok)
    {
        std::string res;
        YYUSE(yystate);
#if YYERROR_VERBOSE
        int yyn = yypact_[yystate];
        if (yypact_ninf_ < yyn && yyn <= yylast_)
        {
            /* Start YYX at -YYN if negative to avoid negative indexes in
	   YYCHECK.  */
            int yyxbegin = yyn < 0 ? -yyn : 0;

            /* Stay within bounds of both yycheck and yytname.  */
            int yychecklim = yylast_ - yyn + 1;
            int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
            int count = 0;
            for (int x = yyxbegin; x < yyxend; ++x)
                if (yycheck_[x + yyn] == x && x != yyterror_)
                    ++count;

            // FIXME: This method of building the message is not compatible
            // with internationalization.  It should work like yacc.c does it.
            // That is, first build a string that looks like this:
            // "syntax error, unexpected %s or %s or %s"
            // Then, invoke YY_ on this string.
            // Finally, use the string as a format to output
            // yytname_[tok], etc.
            // Until this gets fixed, this message appears in English only.
            res = "syntax error, unexpected ";
            res += yytnamerr_(yytname_[tok]);
            if (count < 5)
            {
                count = 0;
                for (int x = yyxbegin; x < yyxend; ++x)
                    if (yycheck_[x + yyn] == x && x != yyterror_)
                    {
                        res += (!count++) ? ", expecting " : " or ";
                        res += yytnamerr_(yytname_[x]);
                    }
            }
        }
        else
#endif
            res = YY_("syntax error");
        return res;
    }

    /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
    const signed char rsl_parser::yypact_ninf_ = -57;
    const short int
        rsl_parser::yypact_[] =
            {
                1623, -57, -57, -57, -57, -57, -57, -57, -57, -57,
                -57, -57, -57, -57, -57, 17, 1623, -57, -57, -57,
                60, 103, -57, -57, -57, -3, 59, 2, 23, -57,
                -57, -57, 39, 10, -57, 107, 347, -57, 375, 53,
                78, 914, 72, 219, -21, 70, -57, -57, 107, 914,
                81, 28, -57, -57, -57, -57, 983, 983, 99, -26,
                100, 983, 101, 104, 105, 114, -57, 608, -57, -57,
                109, 107, -57, 375, 641, -57, -57, -57, -57, -57,
                -57, -57, -57, -57, -57, -57, -57, -57, -57, -57,
                111, 112, 123, 914, -57, 983, -48, -57, 107, 70,
                710, 914, 983, 983, 983, 983, 983, 983, 117, 983,
                1026, 1049, 1069, 1092, 1135, 983, 983, 983, 983, -57,
                -57, -57, 390, -57, -57, -57, 556, -57, -57, -57,
                -57, 390, 556, 983, -57, 118, -57, 121, 1533, 58,
                983, 983, 893, 983, -57, 743, -57, 70, 107, -57,
                -57, -57, -57, 959, 812, 548, 147, 119, -57, -57,
                845, 548, 548, 548, 548, 548, 438, 983, 548, 983,
                548, 983, 548, 983, 548, 983, 548, 983, 548, 548,
                -57, 548, -57, 632, 49, 983, 983, 983, 983, 983,
                983, 983, 983, 983, 983, 983, 983, 170, 983, 983,
                983, 171, -57, -57, 1549, -57, -57, -57, 734, 1115,
                914, 1210, 1227, -57, 70, -57, 124, 548, 26, 55,
                -57, 132, -57, 147, -57, 288, 475, 548, 548, 548,
                548, 548, -57, 983, -57, 548, 548, 548, 548, 548,
                548, 94, 94, 113, 143, 143, -57, 914, 46, 152,
                1724, 914, 983, 914, 983, 914, 983, -57, 983, 983,
                -57, 983, -57, 983, 141, 983, -57, 983, 983, 983,
                983, 983, 288, 1244, -57, 983, -57, 1565, 36, -57,
                1261, -57, 1156, 1598, 1278, 548, 44, -57, 136, -49,
                548, 548, 548, 548, 548, 548, 983, 264, 983, 983,
                983, 914, 983, 914, 983, -57, -57, -57, 983, 1174,
                1616, 1634, 1652, -57, 1192, -57, 1295, 548, -57, 983,
                914, 914, 914, 914, 983, 983, 1312, -57, -57, -57,
                -57, 1670, 1329, 983, 914, 983, 1346, -57, 1688, 983,
                914, 1363, -57, 983, 1380, 983, 1397, 983, 1414, 983,
                1431, 983, 1448, 983, 1465, 983, 1482, 983, 1499, 983,
                1516, 983, 1706, -57};

    /* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
     doesn't specify something else to do.  Zero means the default is an
     error.  */
    const unsigned char
        rsl_parser::yydefact_[] =
            {
                0, 40, 42, 43, 44, 45, 46, 41, 47, 12,
                11, 13, 14, 16, 15, 0, 2, 3, 5, 6,
                0, 0, 26, 1, 4, 0, 0, 0, 0, 38,
                39, 48, 0, 0, 17, 0, 0, 25, 0, 0,
                0, 0, 0, 19, 30, 20, 27, 24, 0, 0,
                0, 152, 49, 156, 153, 154, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 155, 0, 67, 64,
                0, 0, 25, 0, 0, 50, 52, 53, 54, 56,
                57, 58, 68, 69, 70, 71, 72, 73, 59, 55,
                0, 0, 0, 0, 18, 0, 0, 29, 0, 21,
                0, 0, 0, 0, 0, 0, 0, 0, 107, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 162,
                161, 106, 0, 95, 112, 113, 0, 110, 104, 109,
                105, 0, 0, 0, 94, 0, 92, 0, 0, 0,
                0, 0, 0, 0, 66, 0, 63, 22, 0, 8,
                51, 60, 61, 0, 0, 34, 0, 0, 28, 10,
                0, 126, 127, 128, 129, 130, 0, 0, 136, 106,
                138, 106, 139, 106, 140, 106, 141, 106, 142, 137,
                102, 0, 125, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 88, 0, 0,
                0, 87, 75, 74, 0, 93, 91, 62, 0, 0,
                0, 0, 0, 65, 23, 151, 106, 157, 0, 0,
                7, 0, 33, 31, 9, 0, 0, 143, 144, 145,
                146, 147, 111, 0, 116, 119, 117, 120, 118, 121,
                122, 100, 101, 99, 97, 98, 96, 0, 124, 123,
                0, 0, 0, 0, 0, 0, 0, 78, 0, 0,
                150, 0, 149, 0, 0, 0, 32, 0, 0, 0,
                0, 0, 108, 0, 90, 0, 89, 0, 0, 80,
                0, 82, 0, 0, 0, 158, 0, 163, 0, 0,
                36, 131, 132, 133, 134, 135, 0, 103, 0, 0,
                0, 0, 0, 0, 0, 148, 160, 35, 0, 0,
                0, 0, 0, 83, 0, 79, 0, 37, 114, 0,
                0, 0, 0, 0, 0, 0, 0, 77, 76, 81,
                84, 0, 0, 0, 0, 0, 0, 85, 0, 0,
                0, 0, 86, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 115};

    /* YYPGOTO[NTERM-NUM].  */
    const short int
        rsl_parser::yypgoto_[] =
            {
                -57, -57, -57, 200, -57, 22, -57, 189, 175, -57,
                51, -57, -46, 122, -57, -1, -57, -57, 148, -57,
                -57, -25, 182, -57, -57, -57, -57, -57, -57, -57,
                -57, -57, -57, -57, -57, -57, -57, -56, -57, -57,
                -57, -53, 25, -57, 218, -57, -29, -57, -16, -57};

    /* YYDEFGOTO[NTERM-NUM].  */
    const short int
        rsl_parser::yydefgoto_[] =
            {
                -1, 15, 16, 17, 18, 69, 20, 33, 34, 70,
                71, 21, 45, 46, 97, 222, 289, 36, 72, 38,
                73, 74, 75, 76, 77, 78, 79, 80, 81, 82,
                83, 84, 85, 86, 87, 88, 89, 181, 123, 124,
                125, 139, 127, 128, 129, 92, 218, 219, 130, 288};

    /* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule which
     number is the opposite.  If zero, do what YYDEFACT says.  */
    const short int rsl_parser::yytable_ninf_ = -160;
    const short int
        rsl_parser::yytable_[] =
            {
                122, 131, 99, 126, 132, 138, 1, 2, 3, 4,
                5, 6, 7, 8, 29, 30, 31, 23, 119, 120,
                95, 307, 19, 308, 100, 147, 156, 1, 2, 3,
                4, 5, 6, 7, 8, 29, 30, 31, 19, 155,
                119, 120, 145, 135, 137, 134, 161, 162, 163, 164,
                165, 166, 96, 168, 170, 172, 174, 176, 178, 179,
                180, 27, 183, 25, 182, 184, 90, 32, 154, 102,
                103, 104, 105, 106, 90, 42, 160, 204, 35, 35,
                157, 43, 198, 199, 208, 209, 211, 212, 39, 48,
                200, 260, 90, 199, 35, 198, 199, 217, 261, 90,
                200, 107, 214, 200, 198, 199, 26, 299, 41, 305,
                44, 226, 200, 227, 234, 228, 261, 229, 90, 230,
                262, 231, 49, 28, 148, 90, 90, 263, 264, 235,
                236, 237, 238, 239, 240, 241, 242, 243, 244, 245,
                246, 93, 98, 50, 250, 248, 249, 90, 22, 43,
                101, 90, 193, 194, 195, 196, 90, 90, 102, 103,
                104, 105, 106, 133, 22, 140, 119, 120, 141, 142,
                90, 136, 194, 195, 196, 37, 37, 273, 143, 90,
                146, -152, 151, 152, 47, 90, 37, 153, 221, 205,
                167, 37, 206, 223, 247, 251, 277, -159, 280, 278,
                282, 265, 283, 284, 196, 285, 200, 217, 287, 290,
                306, 291, 292, 293, 294, 295, 24, 40, 94, 297,
                158, 37, 266, 1, 2, 3, 4, 5, 6, 7,
                8, 29, 30, 31, 286, 90, 0, 0, 0, 0,
                309, 0, 310, 311, 312, 0, 314, 0, 316, 0,
                0, 0, 317, 0, 0, 0, 150, 0, 0, 91,
                0, 0, 0, 326, 0, 0, 0, 91, 331, 332,
                0, 0, 90, 0, 0, 0, 90, 336, 90, 338,
                90, 0, 150, 341, 0, 91, 0, 344, 0, 346,
                0, 348, 91, 350, 0, 352, 0, 354, 0, 356,
                0, 358, 0, 360, 197, 362, 0, 0, 201, 0,
                0, 91, 0, 202, 203, 0, 0, 0, 91, 91,
                191, 192, 193, 194, 195, 196, 90, 150, 90, 267,
                268, 269, 270, 271, 0, 0, 150, 0, 0, 0,
                91, 0, 150, 0, 91, 90, 90, 90, 90, 91,
                91, 1, 2, 3, 4, 5, 6, 7, 8, 90,
                0, 0, 0, 91, 0, 90, 0, 0, 0, 0,
                0, 0, 91, 0, 0, 0, 0, 0, 91, 1,
                2, 3, 4, 5, 6, 7, 8, 29, 30, 0,
                0, 0, 257, 51, 1, 2, 3, 4, 5, 6,
                7, 8, 29, 30, 0, 52, 53, 0, 54, 0,
                55, 0, 0, 56, 0, 57, 58, 59, 60, 61,
                62, 63, 64, 65, 0, 66, 0, 0, 91, 274,
                0, 0, 0, 276, 0, 279, 0, 281, 185, 186,
                187, 188, 189, 190, 0, 0, 191, 192, 193, 194,
                195, 196, 0, 0, 0, 0, 0, 0, 0, 67,
                0, 68, 0, 0, 0, 91, 0, 0, 0, 91,
                0, 91, 0, 91, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 313, 0, 315, 185, 186, 187, 188,
                189, 190, 0, 0, 191, 192, 193, 194, 195, 196,
                0, 0, 327, 328, 329, 330, 0, 0, 0, 0,
                0, 0, 225, 0, 0, 0, 337, 0, 0, 91,
                0, 91, 342, 185, 186, 187, 188, 189, 190, 0,
                0, 191, 192, 193, 194, 195, 196, 0, 91, 91,
                91, 91, 0, 0, 0, 0, 0, 0, 0, 272,
                0, 0, 91, 0, 0, 0, 0, 0, 91, 51,
                1, 2, 3, 4, 5, 6, 7, 8, 29, 30,
                0, 52, 53, 0, 54, 0, 55, 0, 0, 56,
                0, 57, 58, 59, 60, 61, 62, 63, 64, 65,
                0, 66, 0, 0, 0, 0, 185, 186, 187, 188,
                189, 190, 198, 199, 191, 192, 193, 194, 195, 196,
                200, 51, 1, 2, 3, 4, 5, 6, 7, 8,
                29, 30, 0, 52, 53, 67, 54, 68, 55, 0,
                0, 56, 0, 57, 58, 59, 60, 61, 62, 63,
                64, 65, 0, 66, 51, 1, 2, 3, 4, 5,
                6, 7, 8, 29, 30, 0, 52, 53, 0, 54,
                0, 55, 0, 0, 56, 0, 57, 58, 59, 60,
                61, 62, 63, 64, 65, 0, 66, 67, 144, 68,
                185, 186, 187, 188, 189, 190, 0, 0, 191, 192,
                193, 194, 195, 196, 0, 0, 0, 232, 0, 0,
                0, 0, 0, 0, 233, 0, 0, 0, 0, 0,
                67, 149, 68, 51, 1, 2, 3, 4, 5, 6,
                7, 8, 29, 30, 0, 52, 53, 0, 54, 0,
                55, 0, 0, 56, 0, 57, 58, 59, 60, 61,
                62, 63, 64, 65, 0, 66, 51, 1, 2, 3,
                4, 5, 6, 7, 8, 29, 30, 0, 52, 53,
                0, 54, 0, 55, 0, 0, 56, 0, 57, 58,
                59, 60, 61, 62, 63, 64, 65, 0, 66, 67,
                159, 68, 185, 186, 187, 188, 189, 190, 0, 0,
                191, 192, 193, 194, 195, 196, 0, 0, 0, 253,
                0, 0, 0, 0, 0, 0, 254, 0, 0, 0,
                0, 0, 67, 213, 68, 51, 1, 2, 3, 4,
                5, 6, 7, 8, 29, 30, 0, 52, 53, 0,
                54, 0, 55, 0, 0, 56, 0, 57, 58, 59,
                60, 61, 62, 63, 64, 65, 0, 66, 51, 1,
                2, 3, 4, 5, 6, 7, 8, 29, 30, 0,
                52, 53, 0, 54, 0, 55, 0, 0, 56, 0,
                57, 58, 59, 60, 61, 62, 63, 64, 65, 0,
                66, 67, 220, 68, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 108, 109, 110, 111,
                112, 113, 114, 115, 0, 0, 0, 0, 0, 53,
                0, 54, 0, 55, 67, 224, 68, 51, 1, 2,
                3, 4, 5, 6, 7, 8, 29, 30, 66, 52,
                53, 0, 54, 0, 55, 0, 0, 56, 0, 57,
                58, 59, 60, 61, 62, 63, 64, 65, 0, 66,
                116, 0, 0, 0, 0, 117, 0, 118, 210, 119,
                120, 121, 108, 109, 110, 111, 112, 113, 114, 115,
                0, 0, 0, 0, 0, 53, 0, 54, 0, 55,
                0, 0, 0, 67, 0, 68, 108, 109, 110, 111,
                112, 113, 114, 115, 66, 0, 0, 0, 0, 53,
                0, 54, 0, 55, 0, 0, 0, 0, 0, 0,
                0, 0, 0, 0, 0, 0, 116, 0, 66, 0,
                0, 117, 0, 118, 215, 119, 120, 216, 0, 108,
                109, 110, 111, 112, 113, 114, 115, 0, 0, 0,
                116, 0, 53, 0, 54, 117, 55, 118, 0, 119,
                120, 121, 108, 109, 110, 111, 112, 113, 114, 115,
                0, 66, 0, 0, 0, 53, 0, 54, 0, 55,
                0, 0, 108, 109, 110, 111, 112, 113, 114, 115,
                0, 0, 0, 116, 66, 53, 0, 54, 117, 55,
                118, 0, 119, 120, 169, 108, 109, 110, 111, 112,
                113, 114, 115, 0, 66, 0, 116, 0, 53, 0,
                54, 117, 55, 118, 0, 119, 120, 171, 0, 0,
                0, 0, 0, 0, 0, 0, 116, 66, 0, 0,
                0, 117, 0, 118, 0, 119, 120, 173, 108, 109,
                110, 111, 112, 113, 114, 115, 0, 0, 0, 116,
                0, 53, 0, 54, 117, 55, 118, 0, 119, 120,
                175, 0, 0, 185, 186, 187, 188, 189, 190, 0,
                66, 191, 192, 193, 194, 195, 196, 0, 0, 0,
                255, 0, 0, 0, 0, 0, 0, 256, 0, 0,
                0, 0, 116, 0, 0, 0, 0, 117, 0, 118,
                0, 119, 120, 177, 185, 186, 187, 188, 189, 190,
                0, 0, 191, 192, 193, 194, 195, 196, 0, 0,
                0, 301, 185, 186, 187, 188, 189, 190, 302, 0,
                191, 192, 193, 194, 195, 196, 0, 0, 0, 318,
                185, 186, 187, 188, 189, 190, 319, 0, 191, 192,
                193, 194, 195, 196, 0, 0, 0, 323, 185, 186,
                187, 188, 189, 190, 324, 0, 191, 192, 193, 194,
                195, 196, 0, 0, 0, 185, 186, 187, 188, 189,
                190, 0, 258, 191, 192, 193, 194, 195, 196, 0,
                0, 0, 185, 186, 187, 188, 189, 190, 0, 259,
                191, 192, 193, 194, 195, 196, 0, 0, 0, 185,
                186, 187, 188, 189, 190, 0, 296, 191, 192, 193,
                194, 195, 196, 0, 0, 0, 185, 186, 187, 188,
                189, 190, 0, 300, 191, 192, 193, 194, 195, 196,
                0, 0, 0, 185, 186, 187, 188, 189, 190, 0,
                304, 191, 192, 193, 194, 195, 196, 0, 0, 0,
                185, 186, 187, 188, 189, 190, 0, 325, 191, 192,
                193, 194, 195, 196, 0, 0, 0, 185, 186, 187,
                188, 189, 190, 0, 333, 191, 192, 193, 194, 195,
                196, 0, 0, 0, 185, 186, 187, 188, 189, 190,
                0, 335, 191, 192, 193, 194, 195, 196, 0, 0,
                0, 185, 186, 187, 188, 189, 190, 0, 339, 191,
                192, 193, 194, 195, 196, 0, 0, 0, 185, 186,
                187, 188, 189, 190, 0, 343, 191, 192, 193, 194,
                195, 196, 0, 0, 0, 185, 186, 187, 188, 189,
                190, 0, 345, 191, 192, 193, 194, 195, 196, 0,
                0, 0, 185, 186, 187, 188, 189, 190, 0, 347,
                191, 192, 193, 194, 195, 196, 0, 0, 0, 185,
                186, 187, 188, 189, 190, 0, 349, 191, 192, 193,
                194, 195, 196, 0, 0, 0, 185, 186, 187, 188,
                189, 190, 0, 351, 191, 192, 193, 194, 195, 196,
                0, 0, 0, 185, 186, 187, 188, 189, 190, 0,
                353, 191, 192, 193, 194, 195, 196, 0, 0, 0,
                185, 186, 187, 188, 189, 190, 0, 355, 191, 192,
                193, 194, 195, 196, 0, 0, 0, 185, 186, 187,
                188, 189, 190, 0, 357, 191, 192, 193, 194, 195,
                196, 0, 0, 0, 185, 186, 187, 188, 189, 190,
                0, 359, 191, 192, 193, 194, 195, 196, 0, 0,
                0, 185, 186, 187, 188, 189, 190, 0, 361, 191,
                192, 193, 194, 195, 196, 0, 0, 185, 186, 187,
                188, 189, 190, 0, 207, 191, 192, 193, 194, 195,
                196, 0, 0, 185, 186, 187, 188, 189, 190, 0,
                252, 191, 192, 193, 194, 195, 196, 1, 2, 3,
                4, 5, 6, 7, 8, 0, 298, 0, 0, 9,
                10, 0, 11, 12, 13, 14, 185, 186, 187, 188,
                189, 190, 0, 0, 191, 192, 193, 194, 195, 196,
                0, 0, 0, 303, 185, 186, 187, 188, 189, 190,
                0, 0, 191, 192, 193, 194, 195, 196, 0, 0,
                0, 320, 185, 186, 187, 188, 189, 190, 0, 0,
                191, 192, 193, 194, 195, 196, 0, 0, 0, 321,
                185, 186, 187, 188, 189, 190, 0, 0, 191, 192,
                193, 194, 195, 196, 0, 0, 0, 322, 185, 186,
                187, 188, 189, 190, 0, 0, 191, 192, 193, 194,
                195, 196, 0, 0, 0, 334, 185, 186, 187, 188,
                189, 190, 0, 0, 191, 192, 193, 194, 195, 196,
                0, 0, 0, 340, 185, 186, 187, 188, 189, 190,
                0, 0, 191, 192, 193, 194, 195, 196, 0, 0,
                0, 363, 185, 186, 187, 188, 189, 190, 0, 275,
                191, 192, 193, 194, 195, 196};

    /* YYCHECK.  */
    const short int
        rsl_parser::yycheck_[] =
            {
                56, 57, 48, 56, 57, 61, 4, 5, 6, 7,
                8, 9, 10, 11, 12, 13, 14, 0, 66, 67,
                41, 70, 0, 72, 49, 71, 74, 4, 5, 6,
                7, 8, 9, 10, 11, 12, 13, 14, 16, 95,
                66, 67, 67, 59, 60, 71, 102, 103, 104, 105,
                106, 107, 73, 109, 110, 111, 112, 113, 114, 115,
                116, 64, 118, 3, 117, 118, 41, 65, 93, 41,
                42, 43, 44, 45, 49, 65, 101, 133, 27, 28,
                96, 71, 46, 47, 140, 141, 142, 143, 65, 38,
                54, 65, 67, 47, 43, 46, 47, 153, 72, 74,
                54, 73, 148, 54, 46, 47, 3, 71, 69, 65,
                3, 167, 54, 169, 65, 171, 72, 173, 93, 175,
                65, 177, 69, 64, 73, 100, 101, 72, 73, 185,
                186, 187, 188, 189, 190, 191, 192, 193, 194, 195,
                196, 69, 72, 65, 200, 198, 199, 122, 0, 71,
                69, 126, 58, 59, 60, 61, 131, 132, 41, 42,
                43, 44, 45, 64, 16, 64, 66, 67, 64, 64,
                145, 71, 59, 60, 61, 27, 28, 233, 64, 154,
                71, 64, 71, 71, 36, 160, 38, 64, 41, 71,
                73, 43, 71, 74, 24, 24, 252, 73, 254, 252,
                256, 69, 258, 259, 61, 261, 54, 263, 67, 265,
                74, 267, 268, 269, 270, 271, 16, 28, 43, 275,
                98, 73, 223, 4, 5, 6, 7, 8, 9, 10,
                11, 12, 13, 14, 263, 210, -1, -1, -1, -1,
                296, -1, 298, 299, 300, -1, 302, -1, 304, -1,
                -1, -1, 308, -1, -1, -1, 74, -1, -1, 41,
                -1, -1, -1, 319, -1, -1, -1, 49, 324, 325,
                -1, -1, 247, -1, -1, -1, 251, 333, 253, 335,
                255, -1, 100, 339, -1, 67, -1, 343, -1, 345,
                -1, 347, 74, 349, -1, 351, -1, 353, -1, 355,
                -1, 357, -1, 359, 122, 361, -1, -1, 126, -1,
                -1, 93, -1, 131, 132, -1, -1, -1, 100, 101,
                56, 57, 58, 59, 60, 61, 301, 145, 303, 41,
                42, 43, 44, 45, -1, -1, 154, -1, -1, -1,
                122, -1, 160, -1, 126, 320, 321, 322, 323, 131,
                132, 4, 5, 6, 7, 8, 9, 10, 11, 334,
                -1, -1, -1, 145, -1, 340, -1, -1, -1, -1,
                -1, -1, 154, -1, -1, -1, -1, -1, 160, 4,
                5, 6, 7, 8, 9, 10, 11, 12, 13, -1,
                -1, -1, 210, 3, 4, 5, 6, 7, 8, 9,
                10, 11, 12, 13, -1, 15, 16, -1, 18, -1,
                20, -1, -1, 23, -1, 25, 26, 27, 28, 29,
                30, 31, 32, 33, -1, 35, -1, -1, 210, 247,
                -1, -1, -1, 251, -1, 253, -1, 255, 48, 49,
                50, 51, 52, 53, -1, -1, 56, 57, 58, 59,
                60, 61, -1, -1, -1, -1, -1, -1, -1, 69,
                -1, 71, -1, -1, -1, 247, -1, -1, -1, 251,
                -1, 253, -1, 255, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, 301, -1, 303, 48, 49, 50, 51,
                52, 53, -1, -1, 56, 57, 58, 59, 60, 61,
                -1, -1, 320, 321, 322, 323, -1, -1, -1, -1,
                -1, -1, 74, -1, -1, -1, 334, -1, -1, 301,
                -1, 303, 340, 48, 49, 50, 51, 52, 53, -1,
                -1, 56, 57, 58, 59, 60, 61, -1, 320, 321,
                322, 323, -1, -1, -1, -1, -1, -1, -1, 74,
                -1, -1, 334, -1, -1, -1, -1, -1, 340, 3,
                4, 5, 6, 7, 8, 9, 10, 11, 12, 13,
                -1, 15, 16, -1, 18, -1, 20, -1, -1, 23,
                -1, 25, 26, 27, 28, 29, 30, 31, 32, 33,
                -1, 35, -1, -1, -1, -1, 48, 49, 50, 51,
                52, 53, 46, 47, 56, 57, 58, 59, 60, 61,
                54, 3, 4, 5, 6, 7, 8, 9, 10, 11,
                12, 13, -1, 15, 16, 69, 18, 71, 20, -1,
                -1, 23, -1, 25, 26, 27, 28, 29, 30, 31,
                32, 33, -1, 35, 3, 4, 5, 6, 7, 8,
                9, 10, 11, 12, 13, -1, 15, 16, -1, 18,
                -1, 20, -1, -1, 23, -1, 25, 26, 27, 28,
                29, 30, 31, 32, 33, -1, 35, 69, 70, 71,
                48, 49, 50, 51, 52, 53, -1, -1, 56, 57,
                58, 59, 60, 61, -1, -1, -1, 65, -1, -1,
                -1, -1, -1, -1, 72, -1, -1, -1, -1, -1,
                69, 70, 71, 3, 4, 5, 6, 7, 8, 9,
                10, 11, 12, 13, -1, 15, 16, -1, 18, -1,
                20, -1, -1, 23, -1, 25, 26, 27, 28, 29,
                30, 31, 32, 33, -1, 35, 3, 4, 5, 6,
                7, 8, 9, 10, 11, 12, 13, -1, 15, 16,
                -1, 18, -1, 20, -1, -1, 23, -1, 25, 26,
                27, 28, 29, 30, 31, 32, 33, -1, 35, 69,
                70, 71, 48, 49, 50, 51, 52, 53, -1, -1,
                56, 57, 58, 59, 60, 61, -1, -1, -1, 65,
                -1, -1, -1, -1, -1, -1, 72, -1, -1, -1,
                -1, -1, 69, 70, 71, 3, 4, 5, 6, 7,
                8, 9, 10, 11, 12, 13, -1, 15, 16, -1,
                18, -1, 20, -1, -1, 23, -1, 25, 26, 27,
                28, 29, 30, 31, 32, 33, -1, 35, 3, 4,
                5, 6, 7, 8, 9, 10, 11, 12, 13, -1,
                15, 16, -1, 18, -1, 20, -1, -1, 23, -1,
                25, 26, 27, 28, 29, 30, 31, 32, 33, -1,
                35, 69, 70, 71, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, 3, 4, 5, 6,
                7, 8, 9, 10, -1, -1, -1, -1, -1, 16,
                -1, 18, -1, 20, 69, 70, 71, 3, 4, 5,
                6, 7, 8, 9, 10, 11, 12, 13, 35, 15,
                16, -1, 18, -1, 20, -1, -1, 23, -1, 25,
                26, 27, 28, 29, 30, 31, 32, 33, -1, 35,
                57, -1, -1, -1, -1, 62, -1, 64, 65, 66,
                67, 68, 3, 4, 5, 6, 7, 8, 9, 10,
                -1, -1, -1, -1, -1, 16, -1, 18, -1, 20,
                -1, -1, -1, 69, -1, 71, 3, 4, 5, 6,
                7, 8, 9, 10, 35, -1, -1, -1, -1, 16,
                -1, 18, -1, 20, -1, -1, -1, -1, -1, -1,
                -1, -1, -1, -1, -1, -1, 57, -1, 35, -1,
                -1, 62, -1, 64, 65, 66, 67, 68, -1, 3,
                4, 5, 6, 7, 8, 9, 10, -1, -1, -1,
                57, -1, 16, -1, 18, 62, 20, 64, -1, 66,
                67, 68, 3, 4, 5, 6, 7, 8, 9, 10,
                -1, 35, -1, -1, -1, 16, -1, 18, -1, 20,
                -1, -1, 3, 4, 5, 6, 7, 8, 9, 10,
                -1, -1, -1, 57, 35, 16, -1, 18, 62, 20,
                64, -1, 66, 67, 68, 3, 4, 5, 6, 7,
                8, 9, 10, -1, 35, -1, 57, -1, 16, -1,
                18, 62, 20, 64, -1, 66, 67, 68, -1, -1,
                -1, -1, -1, -1, -1, -1, 57, 35, -1, -1,
                -1, 62, -1, 64, -1, 66, 67, 68, 3, 4,
                5, 6, 7, 8, 9, 10, -1, -1, -1, 57,
                -1, 16, -1, 18, 62, 20, 64, -1, 66, 67,
                68, -1, -1, 48, 49, 50, 51, 52, 53, -1,
                35, 56, 57, 58, 59, 60, 61, -1, -1, -1,
                65, -1, -1, -1, -1, -1, -1, 72, -1, -1,
                -1, -1, 57, -1, -1, -1, -1, 62, -1, 64,
                -1, 66, 67, 68, 48, 49, 50, 51, 52, 53,
                -1, -1, 56, 57, 58, 59, 60, 61, -1, -1,
                -1, 65, 48, 49, 50, 51, 52, 53, 72, -1,
                56, 57, 58, 59, 60, 61, -1, -1, -1, 65,
                48, 49, 50, 51, 52, 53, 72, -1, 56, 57,
                58, 59, 60, 61, -1, -1, -1, 65, 48, 49,
                50, 51, 52, 53, 72, -1, 56, 57, 58, 59,
                60, 61, -1, -1, -1, 48, 49, 50, 51, 52,
                53, -1, 72, 56, 57, 58, 59, 60, 61, -1,
                -1, -1, 48, 49, 50, 51, 52, 53, -1, 72,
                56, 57, 58, 59, 60, 61, -1, -1, -1, 48,
                49, 50, 51, 52, 53, -1, 72, 56, 57, 58,
                59, 60, 61, -1, -1, -1, 48, 49, 50, 51,
                52, 53, -1, 72, 56, 57, 58, 59, 60, 61,
                -1, -1, -1, 48, 49, 50, 51, 52, 53, -1,
                72, 56, 57, 58, 59, 60, 61, -1, -1, -1,
                48, 49, 50, 51, 52, 53, -1, 72, 56, 57,
                58, 59, 60, 61, -1, -1, -1, 48, 49, 50,
                51, 52, 53, -1, 72, 56, 57, 58, 59, 60,
                61, -1, -1, -1, 48, 49, 50, 51, 52, 53,
                -1, 72, 56, 57, 58, 59, 60, 61, -1, -1,
                -1, 48, 49, 50, 51, 52, 53, -1, 72, 56,
                57, 58, 59, 60, 61, -1, -1, -1, 48, 49,
                50, 51, 52, 53, -1, 72, 56, 57, 58, 59,
                60, 61, -1, -1, -1, 48, 49, 50, 51, 52,
                53, -1, 72, 56, 57, 58, 59, 60, 61, -1,
                -1, -1, 48, 49, 50, 51, 52, 53, -1, 72,
                56, 57, 58, 59, 60, 61, -1, -1, -1, 48,
                49, 50, 51, 52, 53, -1, 72, 56, 57, 58,
                59, 60, 61, -1, -1, -1, 48, 49, 50, 51,
                52, 53, -1, 72, 56, 57, 58, 59, 60, 61,
                -1, -1, -1, 48, 49, 50, 51, 52, 53, -1,
                72, 56, 57, 58, 59, 60, 61, -1, -1, -1,
                48, 49, 50, 51, 52, 53, -1, 72, 56, 57,
                58, 59, 60, 61, -1, -1, -1, 48, 49, 50,
                51, 52, 53, -1, 72, 56, 57, 58, 59, 60,
                61, -1, -1, -1, 48, 49, 50, 51, 52, 53,
                -1, 72, 56, 57, 58, 59, 60, 61, -1, -1,
                -1, 48, 49, 50, 51, 52, 53, -1, 72, 56,
                57, 58, 59, 60, 61, -1, -1, 48, 49, 50,
                51, 52, 53, -1, 71, 56, 57, 58, 59, 60,
                61, -1, -1, 48, 49, 50, 51, 52, 53, -1,
                71, 56, 57, 58, 59, 60, 61, 4, 5, 6,
                7, 8, 9, 10, 11, -1, 71, -1, -1, 16,
                17, -1, 19, 20, 21, 22, 48, 49, 50, 51,
                52, 53, -1, -1, 56, 57, 58, 59, 60, 61,
                -1, -1, -1, 65, 48, 49, 50, 51, 52, 53,
                -1, -1, 56, 57, 58, 59, 60, 61, -1, -1,
                -1, 65, 48, 49, 50, 51, 52, 53, -1, -1,
                56, 57, 58, 59, 60, 61, -1, -1, -1, 65,
                48, 49, 50, 51, 52, 53, -1, -1, 56, 57,
                58, 59, 60, 61, -1, -1, -1, 65, 48, 49,
                50, 51, 52, 53, -1, -1, 56, 57, 58, 59,
                60, 61, -1, -1, -1, 65, 48, 49, 50, 51,
                52, 53, -1, -1, 56, 57, 58, 59, 60, 61,
                -1, -1, -1, 65, 48, 49, 50, 51, 52, 53,
                -1, -1, 56, 57, 58, 59, 60, 61, -1, -1,
                -1, 65, 48, 49, 50, 51, 52, 53, -1, 55,
                56, 57, 58, 59, 60, 61};

    /* STOS_[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
    const unsigned char
        rsl_parser::yystos_[] =
            {
                0, 4, 5, 6, 7, 8, 9, 10, 11, 16,
                17, 19, 20, 21, 22, 76, 77, 78, 79, 80,
                81, 86, 93, 0, 78, 3, 3, 64, 64, 12,
                13, 14, 65, 82, 83, 85, 92, 93, 94, 65,
                82, 69, 65, 71, 3, 87, 88, 93, 85, 69,
                65, 3, 15, 16, 18, 20, 23, 25, 26, 27,
                28, 29, 30, 31, 32, 33, 35, 69, 71, 80,
                84, 85, 93, 95, 96, 97, 98, 99, 100, 101,
                102, 103, 104, 105, 106, 107, 108, 109, 110, 111,
                117, 119, 120, 69, 83, 41, 73, 89, 72, 87,
                96, 69, 41, 42, 43, 44, 45, 73, 3, 4,
                5, 6, 7, 8, 9, 10, 57, 62, 64, 66,
                67, 68, 112, 113, 114, 115, 116, 117, 118, 119,
                123, 112, 116, 64, 71, 123, 71, 123, 112, 116,
                64, 64, 64, 64, 70, 96, 71, 87, 85, 70,
                97, 71, 71, 64, 96, 112, 74, 123, 88, 70,
                96, 112, 112, 112, 112, 112, 112, 73, 112, 68,
                112, 68, 112, 68, 112, 68, 112, 68, 112, 112,
                112, 112, 116, 112, 116, 48, 49, 50, 51, 52,
                53, 56, 57, 58, 59, 60, 61, 97, 46, 47,
                54, 97, 97, 97, 112, 71, 71, 71, 112, 112,
                65, 112, 112, 70, 87, 65, 68, 112, 121, 122,
                70, 41, 90, 74, 70, 74, 112, 112, 112, 112,
                112, 112, 65, 72, 65, 112, 112, 112, 112, 112,
                112, 112, 112, 112, 112, 112, 112, 24, 116, 116,
                112, 24, 71, 65, 72, 65, 72, 97, 72, 72,
                65, 72, 65, 72, 73, 69, 90, 41, 42, 43,
                44, 45, 74, 112, 97, 55, 97, 112, 116, 97,
                112, 97, 112, 112, 112, 112, 121, 67, 124, 91,
                112, 112, 112, 112, 112, 112, 72, 112, 71, 71,
                72, 65, 72, 65, 72, 65, 74, 70, 72, 112,
                112, 112, 112, 97, 112, 97, 112, 112, 65, 72,
                65, 65, 65, 65, 72, 72, 112, 97, 97, 97,
                97, 112, 112, 72, 65, 72, 112, 97, 112, 72,
                65, 112, 97, 72, 112, 72, 112, 72, 112, 72,
                112, 72, 112, 72, 112, 72, 112, 72, 112, 72,
                112, 72, 112, 65};

#if YYDEBUG
    /* TOKEN_NUMBER_[YYLEX-NUM] -- Internal symbol number corresponding
     to YYLEX-NUM.  */
    const unsigned short int
        rsl_parser::yytoken_number_[] =
            {
                0, 256, 257, 258, 259, 260, 261, 262, 263, 264,
                265, 266, 267, 268, 269, 270, 271, 272, 273, 274,
                275, 276, 277, 278, 279, 280, 281, 282, 283, 284,
                285, 286, 287, 288, 289, 290, 291, 292, 293, 294,
                295, 61, 296, 297, 298, 299, 300, 301, 60, 62,
                302, 303, 304, 305, 63, 58, 43, 45, 94, 47,
                42, 46, 33, 306, 40, 41, 307, 308, 309, 123,
                125, 59, 44, 91, 93};
#endif

    /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
    const unsigned char
        rsl_parser::yyr1_[] =
            {
                0, 75, 76, 77, 77, 78, 78, 79, 79, 80,
                80, 81, 81, 81, 81, 81, 81, 82, 82, 82,
                83, 83, 84, 84, 85, 85, 86, 87, 87, 88,
                88, 88, 88, 88, 89, 90, 91, 91, 92, 92,
                93, 93, 93, 93, 93, 93, 93, 93, 94, 95,
                96, 96, 97, 97, 97, 97, 97, 97, 97, 97,
                98, 99, 100, 101, 101, 102, 102, 102, 103, 103,
                103, 103, 103, 103, 104, 104, 105, 105, 106, 106,
                107, 107, 108, 108, 108, 108, 109, 110, 110, 110,
                110, 111, 111, 111, 111, 112, 112, 112, 112, 112,
                112, 112, 112, 112, 112, 113, 113, 113, 113, 113,
                113, 113, 113, 113, 114, 115, 116, 116, 116, 116,
                116, 116, 116, 116, 116, 116, 117, 117, 117, 117,
                117, 117, 117, 117, 117, 117, 118, 118, 118, 118,
                118, 118, 118, 118, 118, 118, 118, 118, 119, 119,
                119, 119, 120, 120, 120, 120, 120, 121, 121, 122,
                122, 123, 123, 124};

    /* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
    const unsigned char
        rsl_parser::yyr2_[] =
            {
                0, 2, 1, 1, 2, 1, 1, 8, 7, 8,
                7, 1, 1, 1, 1, 1, 1, 1, 3, 2,
                2, 3, 2, 3, 2, 1, 1, 1, 3, 2,
                1, 4, 5, 4, 2, 4, 1, 3, 1, 1,
                1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
                1, 2, 1, 1, 1, 1, 1, 1, 1, 1,
                2, 2, 3, 2, 1, 3, 2, 1, 1, 1,
                1, 1, 1, 1, 3, 3, 9, 9, 4, 7,
                5, 9, 5, 7, 9, 11, 13, 3, 3, 5,
                5, 3, 2, 3, 2, 1, 3, 3, 3, 3,
                3, 3, 2, 5, 1, 1, 1, 1, 4, 1,
                1, 3, 1, 1, 7, 33, 3, 3, 3, 3,
                3, 3, 3, 3, 3, 2, 3, 3, 3, 3,
                3, 6, 6, 6, 6, 6, 2, 2, 2, 2,
                2, 2, 2, 3, 3, 3, 3, 3, 6, 4,
                4, 3, 1, 1, 1, 1, 1, 1, 3, 1,
                4, 1, 1, 1};

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
    /* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
     First, the terminals, then, starting at \a yyntokens_, nonterminals.  */
    const char* const rsl_parser::yytname_[] =
        {
            "$end", "error", "$undefined", "IDENTIFIER", "TOKEN_FLOAT",
            "TOKEN_COLOR", "TOKEN_POINT", "TOKEN_VECTOR", "TOKEN_NORMAL",
            "TOKEN_MATRIX", "TOKEN_STRING", "TOKEN_VOID", "TOKEN_UNIFORM",
            "TOKEN_VARYING", "TOKEN_OUTPUT", "TOKEN_EXTERN", "TOKEN_SURFACE",
            "TOKEN_LIGHT", "TOKEN_ATMOSPHERE", "TOKEN_VOLUME", "TOKEN_DISPLACEMENT",
            "TOKEN_IMAGER", "TOKEN_TRANSFORMATION", "TOKEN_IF", "TOKEN_ELSE",
            "TOKEN_WHILE", "TOKEN_FOR", "TOKEN_CONTINUE", "TOKEN_BREAK",
            "TOKEN_RETURN", "TOKEN_ILLUMINATE", "TOKEN_ILLUMINANCE", "TOKEN_SOLAR",
            "TOKEN_GATHER", "TOKEN_OCCLUSION", "TOKEN_LIGHTSOURCE", "TOKEN_INCIDENT",
            "TOKEN_OPPOSITE", "TOKEN_ATTRIBUTE", "TOKEN_OPTION",
            "TOKEN_RENDERERINFO", "'='", "TOKEN_ADD_ASSIGN", "TOKEN_SUB_ASSIGN",
            "TOKEN_MUL_ASSIGN", "TOKEN_DIV_ASSIGN", "TOKEN_OR", "TOKEN_AND", "'<'",
            "'>'", "TOKEN_LE", "TOKEN_GE", "TOKEN_EQ", "TOKEN_NE", "'?'", "':'",
            "'+'", "'-'", "'^'", "'/'", "'*'", "'.'", "'!'", "NEG", "'('", "')'",
            "FLOAT_LITERAL", "INTEGER_LITERAL", "STRING_LITERAL", "'{'", "'}'",
            "';'", "','", "'['", "']'", "$accept", "file", "definitions",
            "definition", "shader_definition", "function_definition", "shader_type",
            "formals", "formal_variable_definitions", "variable_definitions",
            "typespec", "function_type", "def_expressions", "def_expression",
            "def_init", "def_array_initialisers", "array_initialisers", "detail",
            "type", "outputspec", "externspec", "statements", "statement",
            "assign_statement", "function_call_statement", "return_statement",
            "definition_statement", "block_statement", "loop_statement",
            "while_statement", "for_statement", "solar_statement",
            "illuminate_statement", "illuminance_statement", "gather_statement",
            "if_statement", "loop_mod_statement", "expression", "primary", "triple",
            "sixteentuple", "relation", "assign_expression", "cast_expression",
            "function_call", "function_name", "function_arguments",
            "texture_argument", "number", "channel", 0};
#endif

#if YYDEBUG
    /* YYRHS -- A `-1'-separated list of the rules' RHS.  */
    const rsl_parser::rhs_number_type
        rsl_parser::yyrhs_[] =
            {
                76, 0, -1, 77, -1, 78, -1, 77, 78, -1,
                79, -1, 80, -1, 81, 3, 64, 82, 65, 69,
                96, 70, -1, 81, 3, 64, 65, 69, 96, 70,
                -1, 86, 3, 64, 82, 65, 69, 96, 70, -1,
                86, 3, 64, 65, 69, 96, 70, -1, 17, -1,
                16, -1, 19, -1, 20, -1, 22, -1, 21, -1,
                83, -1, 82, 71, 83, -1, 82, 71, -1, 85,
                87, -1, 94, 85, 87, -1, 85, 87, -1, 95,
                85, 87, -1, 92, 93, -1, 93, -1, 93, -1,
                88, -1, 87, 72, 88, -1, 3, 89, -1, 3,
                -1, 3, 73, 123, 74, -1, 3, 73, 123, 74,
                90, -1, 3, 73, 74, 90, -1, 41, 112, -1,
                41, 69, 91, 70, -1, 112, -1, 91, 72, 112,
                -1, 12, -1, 13, -1, 4, -1, 10, -1, 5,
                -1, 6, -1, 7, -1, 8, -1, 9, -1, 11,
                -1, 14, -1, 15, -1, 97, -1, 96, 97, -1,
                98, -1, 99, -1, 100, -1, 111, -1, 101, -1,
                102, -1, 103, -1, 110, -1, 117, 71, -1, 119,
                71, -1, 29, 112, 71, -1, 84, 71, -1, 80,
                -1, 69, 96, 70, -1, 69, 70, -1, 71, -1,
                104, -1, 105, -1, 106, -1, 107, -1, 108, -1,
                109, -1, 25, 116, 97, -1, 25, 112, 97, -1,
                26, 64, 112, 71, 116, 71, 112, 65, 97, -1,
                26, 64, 112, 71, 112, 71, 112, 65, 97, -1,
                32, 64, 65, 97, -1, 32, 64, 112, 72, 112,
                65, 97, -1, 30, 64, 112, 65, 97, -1, 30,
                64, 112, 72, 112, 72, 112, 65, 97, -1, 31,
                64, 112, 65, 97, -1, 31, 64, 112, 72, 112,
                65, 97, -1, 31, 64, 112, 72, 112, 72, 112,
                65, 97, -1, 31, 64, 112, 72, 112, 72, 112,
                72, 112, 65, 97, -1, 33, 64, 112, 72, 112,
                72, 112, 72, 112, 72, 112, 65, 97, -1, 23,
                116, 97, -1, 23, 112, 97, -1, 23, 116, 97,
                24, 97, -1, 23, 112, 97, 24, 97, -1, 28,
                123, 71, -1, 28, 71, -1, 27, 123, 71, -1,
                27, 71, -1, 113, -1, 112, 61, 112, -1, 112,
                59, 112, -1, 112, 60, 112, -1, 112, 58, 112,
                -1, 112, 56, 112, -1, 112, 57, 112, -1, 57,
                112, -1, 116, 54, 112, 55, 112, -1, 118, -1,
                123, -1, 68, -1, 3, -1, 3, 73, 112, 74,
                -1, 119, -1, 117, -1, 64, 112, 65, -1, 114,
                -1, 115, -1, 64, 112, 72, 112, 72, 112, 65,
                -1, 64, 112, 72, 112, 72, 112, 72, 112, 72,
                112, 72, 112, 72, 112, 72, 112, 72, 112, 72,
                112, 72, 112, 72, 112, 72, 112, 72, 112, 72,
                112, 72, 112, 65, -1, 64, 116, 65, -1, 112,
                49, 112, -1, 112, 51, 112, -1, 112, 48, 112,
                -1, 112, 50, 112, -1, 112, 52, 112, -1, 112,
                53, 112, -1, 116, 47, 116, -1, 116, 46, 116,
                -1, 62, 116, -1, 3, 41, 112, -1, 3, 42,
                112, -1, 3, 43, 112, -1, 3, 44, 112, -1,
                3, 45, 112, -1, 3, 73, 112, 74, 41, 112,
                -1, 3, 73, 112, 74, 42, 112, -1, 3, 73,
                112, 74, 43, 112, -1, 3, 73, 112, 74, 44,
                112, -1, 3, 73, 112, 74, 45, 112, -1, 4,
                112, -1, 10, 112, -1, 5, 112, -1, 6, 112,
                -1, 7, 112, -1, 8, 112, -1, 9, 112, -1,
                5, 68, 112, -1, 6, 68, 112, -1, 7, 68,
                112, -1, 8, 68, 112, -1, 9, 68, 112, -1,
                120, 64, 122, 72, 121, 65, -1, 120, 64, 122,
                65, -1, 120, 64, 121, 65, -1, 120, 64, 65,
                -1, 3, -1, 18, -1, 20, -1, 35, -1, 16,
                -1, 112, -1, 121, 72, 112, -1, 68, -1, 122,
                73, 124, 74, -1, 67, -1, 66, -1, 67, -1};

    /* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
     YYRHS.  */
    const unsigned short int
        rsl_parser::yyprhs_[] =
            {
                0, 0, 3, 5, 7, 10, 12, 14, 23, 31,
                40, 48, 50, 52, 54, 56, 58, 60, 62, 66,
                69, 72, 76, 79, 83, 86, 88, 90, 92, 96,
                99, 101, 106, 112, 117, 120, 125, 127, 131, 133,
                135, 137, 139, 141, 143, 145, 147, 149, 151, 153,
                155, 157, 160, 162, 164, 166, 168, 170, 172, 174,
                176, 179, 182, 186, 189, 191, 195, 198, 200, 202,
                204, 206, 208, 210, 212, 216, 220, 230, 240, 245,
                253, 259, 269, 275, 283, 293, 305, 319, 323, 327,
                333, 339, 343, 346, 350, 353, 355, 359, 363, 367,
                371, 375, 379, 382, 388, 390, 392, 394, 396, 401,
                403, 405, 409, 411, 413, 421, 455, 459, 463, 467,
                471, 475, 479, 483, 487, 491, 494, 498, 502, 506,
                510, 514, 521, 528, 535, 542, 549, 552, 555, 558,
                561, 564, 567, 570, 574, 578, 582, 586, 590, 597,
                602, 607, 611, 613, 615, 617, 619, 621, 623, 627,
                629, 634, 636, 638};

    /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
    const unsigned short int
        rsl_parser::yyrline_[] =
            {
                0, 230, 230, 240, 246, 256, 261, 269, 281, 297,
                309, 325, 330, 335, 339, 344, 349, 356, 362, 369,
                377, 385, 398, 406, 418, 425, 433, 441, 444, 451,
                460, 467, 475, 485, 498, 505, 513, 518, 528, 533,
                541, 546, 551, 556, 561, 566, 571, 576, 584, 591,
                598, 604, 614, 619, 624, 629, 634, 639, 644, 649,
                658, 666, 674, 683, 688, 696, 702, 706, 714, 719,
                724, 729, 734, 739, 746, 754, 765, 777, 792, 800,
                815, 825, 842, 852, 864, 878, 897, 920, 929, 938,
                949, 963, 968, 972, 977, 986, 991, 1000, 1009, 1018,
                1027, 1036, 1045, 1052, 1062, 1069, 1074, 1080, 1086, 1094,
                1099, 1104, 1109, 1114, 1122, 1135, 1175, 1180, 1188, 1196,
                1204, 1212, 1220, 1228, 1236, 1244, 1252, 1262, 1272, 1282,
                1292, 1302, 1314, 1326, 1338, 1350, 1365, 1372, 1379, 1386,
                1393, 1400, 1407, 1414, 1423, 1432, 1441, 1450, 1463, 1474,
                1484, 1492, 1501, 1505, 1509, 1513, 1517, 1524, 1529, 1539,
                1544, 1554, 1558, 1565};

    // Print the state stack on the debug stream.
    void
    rsl_parser::yystack_print_()
    {
        *yycdebug_ << "Stack now";
        for (state_stack_type::const_iterator i = yystate_stack_.begin();
             i != yystate_stack_.end(); ++i)
            *yycdebug_ << ' ' << *i;
        *yycdebug_ << std::endl;
    }

    // Report on the debug stream that the rule \a yyrule is going to be reduced.
    void
    rsl_parser::yy_reduce_print_(int yyrule)
    {
        unsigned int yylno = yyrline_[yyrule];
        int yynrhs = yyr2_[yyrule];
        /* Print the symbols being reduced, and their result.  */
        *yycdebug_ << "Reducing stack by rule " << yyrule - 1
                   << " (line " << yylno << "), ";
        /* The symbols being reduced.  */
        for (int yyi = 0; yyi < yynrhs; yyi++)
            YY_SYMBOL_PRINT("   $" << yyi + 1 << " =",
                            yyrhs_[yyprhs_[yyrule] + yyi],
                            &(yysemantic_stack_[(yynrhs) - (yyi + 1)]),
                            &(yylocation_stack_[(yynrhs) - (yyi + 1)]));
    }
#endif // YYDEBUG

    /* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
    rsl_parser::token_number_type
    rsl_parser::yytranslate_(int t)
    {
        static const token_number_type
            translate_table[] =
                {
                    0, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 62, 2, 2, 2, 2, 2, 2,
                    64, 65, 60, 56, 72, 57, 61, 59, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 55, 71,
                    48, 41, 49, 54, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 73, 2, 74, 58, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 69, 2, 70, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
                    2, 2, 2, 2, 2, 2, 1, 2, 3, 4,
                    5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
                    15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
                    25, 26, 27, 28, 29, 30, 31, 32, 33, 34,
                    35, 36, 37, 38, 39, 40, 42, 43, 44, 45,
                    46, 47, 50, 51, 52, 53, 63, 66, 67, 68};
        if ((unsigned int)t <= yyuser_token_number_max_)
            return translate_table[t];
        else
            return yyundef_token_;
    }

    const int rsl_parser::yyeof_ = 0;
    const int rsl_parser::yylast_ = 1785;
    const int rsl_parser::yynnts_ = 50;
    const int rsl_parser::yyempty_ = -2;
    const int rsl_parser::yyfinal_ = 23;
    const int rsl_parser::yyterror_ = 1;
    const int rsl_parser::yyerrcode_ = 256;
    const int rsl_parser::yyntokens_ = 75;

    const unsigned int rsl_parser::yyuser_token_number_max_ = 309;
    const rsl_parser::token_number_type rsl_parser::yyundef_token_ = 2;

} // namespace yy

#line 1572 "rsl_parser.yy"

void yy::rsl_parser::error(const location_type& loc, const std::string& msg)
{
    std::string fname = GET_FNAME();
    int lineno = GET_LINE();

    fprintf(stderr, "parser error %s :%s(%d)\n", msg.c_str(), fname.c_str(), lineno);
}

extern int yylex_init(void** p);
extern int yylex_destroy(void* p);

namespace kaze
{
    namespace ri
    {
        int load_rsl(FILE* in, ri_rsl_context* ctx)
        {
            ri_rsl_decoder dec(in);

            void* scanner;
            yylex_init(&scanner);

            ri_rsl_parse_param* param = (ri_rsl_parse_param*)(scanner);
            param->ctx = ctx;
            param->dec = &dec;
            param->line_number = 1;

            yy::rsl_parser parser(scanner);

            int nRet = parser.parse();

            if (1)
            {
                printf("%s(%d)\n", GET_FNAME().c_str(), GET_LINE());
            }

            yylex_destroy(scanner);

            return nRet;

            return 0;
        }
    }
}
