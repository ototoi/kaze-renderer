%skeleton "lalr1.cc"
%define "parser_class_name" "rsl_parser"
%defines

%{
#ifdef _WIN32
#pragma warning(disable : 4786)
#pragma warning(disable : 4102)
#pragma warning(disable : 4996)
#pragma warning(disable : 4065)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define YYDEBUG 1
#define YYERROR_VERBOSE 1

#include <algorithm>
#include <iterator>

#include "ri_rsl_context.h"
#include "ri_rsl_decoder.h"
#include "ri_rsl_parse_param.h"

#include "ri_slc_node.h"

%}

%union{
	int itype;
	float ftype;
	char ctype;
	char* stype;
	kaze::ri::ri_slc_node* ntype;
}

%pure_parser

// The parsing context.
%parse-param { void * scanner }
%lex-param   { void * scanner }

%locations
// %debug
%error-verbose

%{

#define yyerror(m) error(yylloc,m)

using namespace kaze::ri;

#define GET_CTX() (((ri_rsl_parse_param*)(scanner))->ctx)
#define GET_FNAME() (((ri_rsl_parse_param*)(scanner))->filename)
#define GET_LINE() (((ri_rsl_parse_param*)(scanner))->line_number)
#define PRINT(s) printf(s)

static
char* dups_(const char* s)
{
	int l = strlen(s);
	char* sRet = new char[l+1];
	strcpy(sRet, s);
	return sRet;
}

typedef kaze::ri::ri_slc_node* PNODE;
typedef char* LPCSTR;

static
void release_value(LPCSTR v){delete[] v;v=NULL;}
//static
//void release_value(PNODE& v){delete v;v=NULL;}



#define	YY_DECL											\
	int						\
	yylex(yy::rsl_parser::semantic_type* yylval,		\
		 yy::rsl_parser::location_type* yylloc,		\
		 void* scanner)

YY_DECL;


%}

/*tokens*/

%token <stype>	IDENTIFIER

/*types*/
%token <ctype> TOKEN_FLOAT
%token <ctype> TOKEN_COLOR
%token <ctype> TOKEN_POINT
%token <ctype> TOKEN_VECTOR
%token <ctype> TOKEN_NORMAL
%token <ctype> TOKEN_MATRIX
%token <ctype> TOKEN_STRING
%token <ctype> TOKEN_VOID

/*strage class*/
%token <ctype> TOKEN_UNIFORM
%token <ctype> TOKEN_VARYING

%token <ctype> TOKEN_OUTPUT
%token <ctype> TOKEN_EXTERN

/*shader type*/
%token <ctype> TOKEN_SURFACE
%token <ctype> TOKEN_LIGHT
%token <ctype> TOKEN_ATMOSPHERE
%token <ctype> TOKEN_VOLUME
%token <ctype> TOKEN_DISPLACEMENT
%token <ctype> TOKEN_IMAGER
%token <ctype> TOKEN_TRANSFORMATION

/*block statement constructs*/
%token <ctype> TOKEN_IF
%token <ctype> TOKEN_ELSE
%token <ctype> TOKEN_WHILE
%token <ctype> TOKEN_FOR
%token <ctype> TOKEN_CONTINUE
%token <ctype> TOKEN_BREAK
%token <ctype> TOKEN_RETURN

%token <ctype> TOKEN_ILLUMINATE
%token <ctype> TOKEN_ILLUMINANCE
%token <ctype> TOKEN_SOLAR
%token <ctype> TOKEN_GATHER
%token <ctype> TOKEN_OCCLUSION

%token <ctype> TOKEN_LIGHTSOURCE
%token <ctype> TOKEN_INCIDENT
%token <ctype> TOKEN_OPPOSITE
%token <ctype> TOKEN_ATTRIBUTE
%token <ctype> TOKEN_OPTION
%token <ctype> TOKEN_RENDERERINFO


/* NOTE: These are priorities in ascending precedence, operators on the same line have the same precedence. */
%right <ctype> '='
%right <ctype> TOKEN_ADD_ASSIGN
%right <ctype> TOKEN_SUB_ASSIGN
%right <ctype> TOKEN_MUL_ASSIGN
%right <ctype> TOKEN_DIV_ASSIGN

%left <ctype> TOKEN_OR
%left <ctype> TOKEN_AND

%left <ctype>	'<'
%left <ctype>	'>'
%left <ctype>	TOKEN_LE
%left <ctype>	TOKEN_GE
%left <ctype>	TOKEN_EQ
%left <ctype>	TOKEN_NE

%right <ctype>	'?' ':'

%left <ctype>	'+' '-'
%left <ctype>	'^'
%left <ctype>	'/' '*'
%left <ctype>	'.'
%right <ctype>	'!' NEG
%left <ctype>	'(' ')'

%token <ftype>	FLOAT_LITERAL
%token <itype>	INTEGER_LITERAL
%token <stype>	STRING_LITERAL

%type  <ntype>	definitions
%type  <ntype>	definition
%type  <ntype>	shader_definition
%type  <ntype>	function_definition
%type  <ntype>	shader_type
%type  <ntype>	function_type
%type  <ntype>	formals
%type  <ntype>	variable_definitions
%type  <ntype>	formal_variable_definitions
%type  <ntype>	typespec
%type  <ctype>	outputspec
%type  <ctype>	externspec
%type  <ntype>	def_expressions
%type  <ntype>	def_expression
%type  <ntype>	def_init
%type  <ntype>	array_initialisers
%type  <ntype>	def_array_initialisers
%type  <stype>	detail
%type  <ntype>	type
%type  <ntype>	statements
%type  <ntype>	statement

%type  <ntype>	function_call_statement
%type  <ntype>	assign_statement
%type  <ntype>	definition_statement
%type  <ntype>	return_statement
%type  <ntype>	block_statement
%type  <ntype>	if_statement
%type  <ntype>	loop_statement
%type  <ntype>	while_statement
%type  <ntype>	for_statement
%type  <ntype>	solar_statement
%type  <ntype>	illuminate_statement
%type  <ntype>	illuminance_statement
%type  <ntype>  gather_statement
%type  <ntype>	loop_mod_statement


%type  <ntype>	expression
%type  <ntype>	primary
%type  <ntype>	triple
%type  <ntype>	sixteentuple
%type  <ntype>	relation
%type  <ntype>	assign_expression
%type  <ntype>	cast_expression
%type  <ntype>  function_call
%type  <stype>  function_name
%type  <ntype>	function_arguments

%type  <ntype>	texture_argument

%type  <ftype>	number
%type  <itype>	channel

%start file
%%

file
	: definitions
		{
			if($1 != NULL)
			{
				GET_CTX()->set_ast_node($1);
				$1 = NULL;
			}
		}

definitions
	: definition
		{
			$$ = new ri_slc_node("definitions");
			$$->add_child($1);
			$1 = NULL;
		}
	| definitions definition
		{
			$$ = $1;
			$$->add_child($2);
			$1 = NULL;
			$2 = NULL;
		}
	;

definition
	:	shader_definition
		{
			$$ = $1;
			$1 = NULL;
		}
	|	function_definition
		{
			$$ = $1;
			$1 = NULL;
		}
	;

shader_definition
	:	shader_type IDENTIFIER '(' formals ')' '{' statements '}'
							{
								$$ = new ri_slc_node("shader_definition");
								$$->set("name", $2);
								$$->add_child($1);
								$$->add_child($4);
								$$->add_child($7);
								$1 = NULL;
								release_value($2);
								$4 = NULL;
								$7 = NULL;
							}
	|	shader_type IDENTIFIER '(' ')' '{' statements '}'
							{
								ri_slc_node* formals = new ri_slc_node("formals");

								$$ = new ri_slc_node("shader_definition");
								$$->set("name", $2);
								$$->add_child($1);
								$$->add_child( formals );
								$$->add_child($6);
								$1 = NULL;
								release_value($2);
								$6 = NULL;
							}
	;

function_definition
	:	function_type IDENTIFIER '(' formals ')' '{' statements '}'
							{
								$$ = new ri_slc_node("function_definition");
								$$->set("name", $2);
								$$->add_child($1);
								$$->add_child($4);
								$$->add_child($7);
								$1 = NULL;
								release_value($2);
								$4 = NULL;
								$7 = NULL;
							}
	|	function_type IDENTIFIER '(' ')' '{' statements '}'
							{
								ri_slc_node* formals = new ri_slc_node("formals");

								$$ = new ri_slc_node("function_definition");
								$$->set("name", $2);
								$$->add_child($1);
								$$->add_child( formals );
								$$->add_child($6);
								$1 = NULL;
								release_value($2);
								$6 = NULL;
							}
	;

shader_type
	:	TOKEN_LIGHT
						{
							$$ = new ri_slc_node("shader_type");
							$$->set("type", "light");
						}
	|	TOKEN_SURFACE
						{
							$$ = new ri_slc_node("shader_type");
							$$->set("type", "surface");
						}
	|	TOKEN_VOLUME	{
							$$ = new ri_slc_node("shader_type");
							$$->set("type", "volume");
						}
	|	TOKEN_DISPLACEMENT
						{
							$$ = new ri_slc_node("shader_type");
							$$->set("type", "displacement");
						}
	|	TOKEN_TRANSFORMATION
						{
							$$ = new ri_slc_node("shader_type");
							$$->set("type", "transformation");
						}
	|	TOKEN_IMAGER	{
							$$ = new ri_slc_node("shader_type");
							$$->set("type", "imager");
						}
	;

formals
	:	formal_variable_definitions
						{
							$$ = new ri_slc_node("formals");
							$$->add_child($1);
							$1 = NULL;
						}
	|	formals ';' formal_variable_definitions
						{
							$$ = $1;
							$$->add_child($3);
							$1 = NULL;
							$3 = NULL;
						}
	|	formals ';'
						{
							$$ = $1;
							$1 = NULL;
						}
	;

formal_variable_definitions
	:	typespec def_expressions
							{
								$$ = new ri_slc_node("variable_definitions");
								$$->add_child($1);
								$$->add_child($2);
								$1 = NULL;
								$2 = NULL;
							}
	|	outputspec typespec def_expressions
							{
								$2->set("extern", "out");
								$$ = new ri_slc_node("variable_definitions");
								$$->add_child($2);
								$$->add_child($3);
								$2 = NULL;
								$3 = NULL;
							}
	;


variable_definitions
	:	typespec def_expressions
							{
								$$ = new ri_slc_node("variable_definitions");
								$$->add_child($1);
								$$->add_child($2);
								$1 = NULL;
								$2 = NULL;
							}
	|	externspec typespec def_expressions
							{
								$2->set("extern", "extern");
								$$ = new ri_slc_node("variable_definitions");
								$$->add_child($2);
								$$->add_child($3);
								$2 = NULL;
								$3 = NULL;
							}
	;

typespec
	:	detail type
							{
								$$ = $2;
								$$->set("storage", $1);
								release_value($1);
								$2 = NULL;
							}
	|	type
							{
								$$ = $1;
								$1 = NULL;
							}
	;

function_type
	:	type
							{
								$$ = $1;
								$1 = NULL;
							}
	;

def_expressions
	:	def_expression		{
								;
							}
	|	def_expressions ',' def_expression
							{
								;
							}
	;

def_expression
	:	IDENTIFIER def_init
							{
								ri_slc_node* ref = new ri_slc_node("valref");
								$$ = new ri_slc_node("def");
								$$->add_child(ref);
								$$->add_child($2);
								release_value($1);
								$2 = NULL;
							}
	|	IDENTIFIER
							{
								ri_slc_node* ref = new ri_slc_node("valref");
								$$ = new ri_slc_node("def");
								$$->add_child(ref);
								release_value($1);
							}
	|	IDENTIFIER '[' number ']'
							{
								ri_slc_node* ref = new ri_slc_node("arrayref");
								ref->set("size", $3);
								$$ = new ri_slc_node("def");
								$$->add_child(ref);
								release_value($1);
							}
	|	IDENTIFIER '[' number ']' def_array_initialisers
							{
								ri_slc_node* ref = new ri_slc_node("arrayref");
								ref->set("size", $3);
								$$ = new ri_slc_node("def");
								$$->add_child(ref);
								$$->add_child($5);
								release_value($1);
								$5 = NULL;
							}
	|	IDENTIFIER '[' ']' def_array_initialisers
							{
								ri_slc_node* ref = new ri_slc_node("arrayref");
								ref->set("size", (int)($4->get_children_size()));
								$$ = new ri_slc_node("def");
								$$->add_child(ref);
								$$->add_child($4);
								release_value($1);
								$4 = NULL;
							}
	;

def_init
	:	'=' expression
							{
								$$ = $2;
							}
	;

def_array_initialisers
	:	'=' '{' array_initialisers '}'
							{
								$$ = $3;
								$3 = NULL;
							}
	;

array_initialisers
	:	expression			{
								$$ = new ri_slc_node("array_initialisers");
								$$->add_child($1);
								$1 = NULL;
							}
	|	array_initialisers ',' expression
							{
								$$ = $1;
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	;

detail
	:	TOKEN_UNIFORM
							{
								$$ = dups_("uniform");
							}
	;
	|	TOKEN_VARYING
							{
								$$ = dups_("varying");
							}
	;


type
	:	TOKEN_FLOAT
							{
								$$ = new ri_slc_node("type");
								$$->set("type", "float");
							}
	|	TOKEN_STRING
							{
								$$ = new ri_slc_node("type");
								$$->set("type", "string");
							}
	|	TOKEN_COLOR
							{
								$$ = new ri_slc_node("type");
								$$->set("type", "color");
							}
	|	TOKEN_POINT
							{
								$$ = new ri_slc_node("type");
								$$->set("type", "point");
							}
	|	TOKEN_VECTOR
							{
								$$ = new ri_slc_node("type");
								$$->set("type", "vector");
							}
	|	TOKEN_NORMAL
							{
								$$ = new ri_slc_node("type");
								$$->set("type", "normal");
							}
	|	TOKEN_MATRIX
							{
								$$ = new ri_slc_node("type");
								$$->set("type", "matrix");
							}
	|	TOKEN_VOID
							{
								$$ = new ri_slc_node("type");
								$$->set("type", "void");
							}
	;

outputspec
	:	TOKEN_OUTPUT
							{
								$$ = $1;
							}
	;

externspec
	:	TOKEN_EXTERN
							{
								$$ = $1;
							}
	;

statements
	:	statement
							{
								$$ = new ri_slc_node("statements");
								$$->add_child($1);
								$1 = NULL;
							}
	|	statements statement
							{
								$$ = $1;
								$$->add_child($2);
								$1 = NULL;
								$2 = NULL;
							}
	;

statement
	:	assign_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	function_call_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	return_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	loop_mod_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	definition_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	block_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	loop_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	if_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	;


assign_statement
	:	assign_expression ';'
							{
								$$ = $1;
								$1 = NULL;
							}
	;

function_call_statement
	:	function_call ';'
							{
								$$ = $1;
								$1 = NULL;
							}
	;

return_statement
	:	TOKEN_RETURN expression ';'
							{
								$$ = new ri_slc_node("return");
								$$->add_child($2);
								$2 = NULL;
							}
	;

definition_statement
	:	variable_definitions ';'
							{
								$$ = $1;
								$1 = NULL;
							}
	|	function_definition
							{
								$$ = $1;
								$1 = NULL;
							}
	;

block_statement
	:	'{' statements '}'
							{
								$$ = new ri_slc_node("block");
								$$->add_child($2);
								$2 = NULL;
							}
	|	'{' '}'
							{
								$$ = new ri_slc_node("block");
							}
	|	';'
							{
								$$ = new ri_slc_node("block");
							}
	;


loop_statement
	:	while_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	for_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	solar_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	illuminate_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	illuminance_statement
							{
								$$ = $1;
								$1 = NULL;
							}
	|	gather_statement
							{
								$$ = $1;
								$1 = NULL;
							}

while_statement
	:	TOKEN_WHILE relation statement
							{
								$$ = new ri_slc_node("while");
								$$->add_child($2);
								$$->add_child($3);
								$2 = NULL;
								$3 = NULL;
							}
	|	TOKEN_WHILE expression statement
							{
								$$ = new ri_slc_node("while");
								$$->add_child($2);
								$$->add_child($3);
								$2 = NULL;
								$3 = NULL;
							}
	;

for_statement
	:	TOKEN_FOR '(' expression ';' relation ';' expression ')' statement
							{
								$$ = new ri_slc_node("for");
								$$->add_child($3);
								$$->add_child($5);
								$$->add_child($7);
								$$->add_child($9);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
								$9 = NULL;
							}
	|	TOKEN_FOR '(' expression ';' expression ';' expression ')' statement
							{
								$$ = new ri_slc_node("for");
								$$->add_child($3);
								$$->add_child($5);
								$$->add_child($7);
								$$->add_child($9);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
								$9 = NULL;
							}
	;

solar_statement
	:	TOKEN_SOLAR '(' ')' statement
							{
								ri_slc_node* args = new ri_slc_node("function_arguments");
								$$ = new ri_slc_node("solar");
								$$->add_child(args);
								$$->add_child($4);
								$4 = NULL;
							}
	|	TOKEN_SOLAR '(' expression ',' expression ')' statement
							{
								ri_slc_node* args = new ri_slc_node("function_arguments");
								args->add_child($3);
								args->add_child($5);
								$$ = new ri_slc_node("solar");
								$$->add_child(args);
								$$->add_child($7);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
							}
	;

illuminate_statement
	:	TOKEN_ILLUMINATE '(' expression ')' statement
							{
								ri_slc_node* args = new ri_slc_node("function_arguments");
								args->add_child($3);
								$$ = new ri_slc_node("illuminate");
								$$->add_child(args);
								$$->add_child($5);
								$3 = NULL;
								$5 = NULL;
							}
	|	TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement
							{
								ri_slc_node* args = new ri_slc_node("function_arguments");
								args->add_child($3);
								args->add_child($5);
								args->add_child($7);
								$$ = new ri_slc_node("illuminate");
								$$->add_child(args);
								$$->add_child($9);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
								$9 = NULL;
							}
	;

illuminance_statement
	:	TOKEN_ILLUMINANCE '(' expression ')' statement
							{
								ri_slc_node* args = new ri_slc_node("function_arguments");
								args->add_child($3);
								$$ = new ri_slc_node("illuminance");
								$$->add_child(args);
								$$->add_child($5);
								$3 = NULL;
								$5 = NULL;
							}
	|	TOKEN_ILLUMINANCE '(' expression ',' expression ')' statement
							{
								ri_slc_node* args = new ri_slc_node("function_arguments");
								args->add_child($3);
								args->add_child($5);
								$$ = new ri_slc_node("illuminance");
								$$->add_child(args);
								$$->add_child($7);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
							}
	|	TOKEN_ILLUMINANCE '(' expression ',' expression ',' expression ')' statement
							{
								ri_slc_node* args = new ri_slc_node("function_arguments");
								args->add_child($3);
								args->add_child($5);
								args->add_child($7);
								$$ = new ri_slc_node("illuminance");
								$$->add_child(args);
								$$->add_child($9);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
								$9 = NULL;
							}
	|	TOKEN_ILLUMINANCE '(' expression ',' expression ',' expression ',' expression ')' statement
							{
								ri_slc_node* args = new ri_slc_node("function_arguments");
								args->add_child($3);
								args->add_child($5);
								args->add_child($7);
								args->add_child($9);
								$$ = new ri_slc_node("illuminance");
								$$->add_child(args);
								$$->add_child($11);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
								$9 = NULL;
								$11 = NULL;
							}
	;

gather_statement
	:	TOKEN_GATHER '(' expression ',' expression ',' expression ',' expression ',' expression ')' statement
							{
								ri_slc_node* args = new ri_slc_node("function_arguments");
								args->add_child($3);
								args->add_child($5);
								args->add_child($7);
								args->add_child($9);
								args->add_child($11);
								$$ = new ri_slc_node("gather");
								$$->add_child(args);
								$$->add_child($13);
								$3 = NULL;
								$5 = NULL;
								$7 = NULL;
								$9 = NULL;
								$11 = NULL;
								$13 = NULL;
							}




if_statement
	: TOKEN_IF relation statement
							{
								$$ = new ri_slc_node("if");
								$$->set("haselse",0);
								$$->add_child($2);
								$$->add_child($3);
								$2 = NULL;
								$3 = NULL;
							}
	| TOKEN_IF expression statement
							{
								$$ = new ri_slc_node("if");
								$$->set("haselse",0);
								$$->add_child($2);
								$$->add_child($3);
								$2 = NULL;
								$3 = NULL;
							}
	| TOKEN_IF relation statement TOKEN_ELSE statement
							{
								$$ = new ri_slc_node("if");
								$$->set("haselse",1);
								$$->add_child($2);
								$$->add_child($3);
								$$->add_child($5);
								$2 = NULL;
								$3 = NULL;
								$5 = NULL;
							}
	| TOKEN_IF expression statement TOKEN_ELSE statement
							{
								$$ = new ri_slc_node("if");
								$$->set("haselse",1);
								$$->add_child($2);
								$$->add_child($3);
								$$->add_child($5);
								$2 = NULL;
								$3 = NULL;
								$5 = NULL;
							}
	;

loop_mod_statement
	:	TOKEN_BREAK number ';'
							{
								$$ = new ri_slc_node("break");
								$$->set("label", $2);
							}
	|	TOKEN_BREAK ';'
							{
								$$ = new ri_slc_node("break");
							}
	|	TOKEN_CONTINUE number ';'
							{
								$$ = new ri_slc_node("continue");
								$$->set("label", $2);
							}
	|	TOKEN_CONTINUE ';'
							{
								$$ = new ri_slc_node("continue");
							}
	;



expression
	:	primary
							{
								$$ = $1;
								$1 = NULL;
							}
	|	expression '.' expression
							{
								$$ = new ri_slc_node(".");
								$$->set("args", 2);
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression '/' expression
							{
								$$ = new ri_slc_node("/");
								$$->set("args", 2);
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression '*' expression
							{
								$$ = new ri_slc_node("*");
								$$->set("args", 2);
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression '^' expression
							{
								$$ = new ri_slc_node("^");
								$$->set("args", 2);
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression '+' expression
							{
								$$ = new ri_slc_node("+");
								$$->set("args", 2);
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression '-' expression
							{
								$$ = new ri_slc_node("-");
								$$->set("args", 2);
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	'-' expression	%prec NEG
							{
								$$ = new ri_slc_node("-");
								$$->set("args", 1);
								$$->add_child($2);
								$2 = NULL;
							}
	|	relation '?' expression ':' expression
							{
								$$ = new ri_slc_node("?");
								$$->add_child($1);
								$$->add_child($3);
								$$->add_child($5);
								$1 = NULL;
								$3 = NULL;
								$5 = NULL;
							}
	|	cast_expression %prec '('
							{
								$$ = $1;
								$1 = NULL;
							}

primary
	:	number
							{
								$$ = new ri_slc_node("number");
								$$->set("value", $1);
							}
	|	STRING_LITERAL
							{
								$$ = new ri_slc_node("string");
								$$->set("value", $1);
								release_value($1);
							}
	|	IDENTIFIER
							{
								$$ = new ri_slc_node("valref");
								$$->set("name", $1);
								release_value($1);
							}
	|	IDENTIFIER '[' expression ']'
							{
								$$ = new ri_slc_node("arrayref");
								$$->set("name", $1);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	function_call
							{
								$$ = $1;
								$1 = NULL;
							}
	|	assign_expression
							{
								$$ = $1;
								$1 = NULL;
							}
	|	'(' expression ')'
							{
								$$ = $2;
								$2 = NULL;
							}
	|	triple
							{
								$$ = $1;
								$1 = NULL;
							}
	|	sixteentuple
							{
								$$ = $1;
								$1 = NULL;
							}
	;

triple
	:	'(' expression ',' expression ',' expression ')'
							{
								$$ = new ri_slc_node("tuple3");
								$$->add_child($2);
								$$->add_child($4);
								$$->add_child($6);
								$2 = NULL;
								$4 = NULL;
								$6 = NULL;
							}
	;

sixteentuple
	:	'(' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ')'
							{
								$$ = new ri_slc_node("tuple16");
								$$->add_child($2);
								$$->add_child($4);
								$$->add_child($6);
								$$->add_child($8);
								$$->add_child($10);
								$$->add_child($12);
								$$->add_child($14);
								$$->add_child($16);
								$$->add_child($18);
								$$->add_child($20);
								$$->add_child($22);
								$$->add_child($24);
								$$->add_child($26);
								$$->add_child($28);
								$$->add_child($30);
								$$->add_child($32);

								$2 = NULL;
								$4 = NULL;
								$6 = NULL;
								$8 = NULL;//4
								$10 = NULL;
								$12 = NULL;
								$14 = NULL;
								$16 = NULL;//8
								$18 = NULL;
								$20 = NULL;
								$22 = NULL;
								$24 = NULL;//12
								$26 = NULL;
								$28 = NULL;
								$30 = NULL;
								$32 = NULL;//16
							}
	;

relation
	:	'(' relation ')'
							{
								$$ = $2;
								$2 = NULL;
							}
	|	expression '>' expression
							{
								$$ = new ri_slc_node(">");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression TOKEN_GE expression
							{
								$$ = new ri_slc_node(">=");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression '<' expression
							{
								$$ = new ri_slc_node("<");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression TOKEN_LE expression
							{
								$$ = new ri_slc_node("<=");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression TOKEN_EQ expression
							{
								$$ = new ri_slc_node("==");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	expression TOKEN_NE expression
							{
								$$ = new ri_slc_node("!=");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	relation TOKEN_AND relation
							{
								$$ = new ri_slc_node("&&");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	relation TOKEN_OR relation
							{
								$$ = new ri_slc_node("||");
								$$->add_child($1);
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	|	'!' relation		{
								$$ = new ri_slc_node("!");
								$$->add_child($2);
								$2 = NULL;
							}
	;

assign_expression
	:	IDENTIFIER '=' expression
							{
								ri_slc_node* left = new ri_slc_node("valref");
								left->set("name", $1);
								$$ = new ri_slc_node("=");
								$$->add_child(left);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	IDENTIFIER TOKEN_ADD_ASSIGN expression
							{
								ri_slc_node* left = new ri_slc_node("valref");
								left->set("name", $1);
								$$ = new ri_slc_node("+=");
								$$->add_child(left);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	IDENTIFIER TOKEN_SUB_ASSIGN expression
							{
								ri_slc_node* left = new ri_slc_node("valref");
								left->set("name", $1);
								$$ = new ri_slc_node("-=");
								$$->add_child(left);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	IDENTIFIER TOKEN_MUL_ASSIGN expression
							{
								ri_slc_node* left = new ri_slc_node("valref");
								left->set("name", $1);
								$$ = new ri_slc_node("*=");
								$$->add_child(left);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	IDENTIFIER TOKEN_DIV_ASSIGN expression
							{
								ri_slc_node* left = new ri_slc_node("valref");
								left->set("name", $1);
								$$ = new ri_slc_node("/=");
								$$->add_child(left);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	IDENTIFIER '[' expression ']' '=' expression
							{
								ri_slc_node* left = new ri_slc_node("arrayref");
								left->set("name", $1);
								left->add_child($3);
								$$ = new ri_slc_node("=");
								$$->add_child(left);
								$$->add_child($6);
								release_value($1);
								$3 = NULL;
								$6 = NULL;
							}
	|	IDENTIFIER  '[' expression ']' TOKEN_ADD_ASSIGN expression
							{
								ri_slc_node* left = new ri_slc_node("arrayref");
								left->set("name", $1);
								left->add_child($3);
								$$ = new ri_slc_node("+=");
								$$->add_child(left);
								$$->add_child($6);
								release_value($1);
								$3 = NULL;
								$6 = NULL;
							}
	|	IDENTIFIER  '[' expression ']' TOKEN_SUB_ASSIGN expression
							{
								ri_slc_node* left = new ri_slc_node("arrayref");
								left->set("name", $1);
								left->add_child($3);
								$$ = new ri_slc_node("-=");
								$$->add_child(left);
								$$->add_child($6);
								release_value($1);
								$3 = NULL;
								$6 = NULL;
							}
	|	IDENTIFIER  '[' expression ']' TOKEN_MUL_ASSIGN expression
							{
								ri_slc_node* left = new ri_slc_node("arrayref");
								left->set("name", $1);
								left->add_child($3);
								$$ = new ri_slc_node("*=");
								$$->add_child(left);
								$$->add_child($6);
								release_value($1);
								$3 = NULL;
								$6 = NULL;
							}
	|	IDENTIFIER  '[' expression ']' TOKEN_DIV_ASSIGN expression
							{
								ri_slc_node* left = new ri_slc_node("arrayref");
								left->set("name", $1);
								left->add_child($3);
								$$ = new ri_slc_node("/=");
								$$->add_child(left);
								$$->add_child($6);
								release_value($1);
								$3 = NULL;
								$6 = NULL;
							}
	;

cast_expression
	:	TOKEN_FLOAT	expression
							{
								$$ = new ri_slc_node("cast");
								$$->set("type", "float");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_STRING expression
							{
								$$ = new ri_slc_node("cast");
								$$->set("type", "string");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_COLOR expression
							{
								$$ = new ri_slc_node("cast");
								$$->set("type", "color");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_POINT expression
							{
								$$ = new ri_slc_node("cast");
								$$->set("type", "point");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_VECTOR expression
							{
								$$ = new ri_slc_node("cast");
								$$->set("type", "vector");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_NORMAL expression
							{
								$$ = new ri_slc_node("cast");
								$$->set("type", "normal");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_MATRIX expression
							{
								$$ = new ri_slc_node("cast");
								$$->set("type", "matrix");
								$$->add_child($2);
								$2 = NULL;
							}
	|	TOKEN_COLOR STRING_LITERAL expression
							{
								$$ = new ri_slc_node("cast");
								$$->set("type", "color");
								$$->set("space", $2);
								$$->add_child($3);
								release_value($2);
								$3 = NULL;
							}
	|	TOKEN_POINT STRING_LITERAL expression
							{
								$$ = new ri_slc_node("cast");
								$$->set("type", "point");
								$$->set("space", $2);
								$$->add_child($3);
								release_value($2);
								$3 = NULL;
							}
	|	TOKEN_VECTOR STRING_LITERAL expression
							{
								$$ = new ri_slc_node("cast");
								$$->set("type", "vector");
								$$->set("space", $2);
								$$->add_child($3);
								release_value($2);
								$3 = NULL;
							}
	|	TOKEN_NORMAL STRING_LITERAL expression
							{
								$$ = new ri_slc_node("cast");
								$$->set("type", "normal");
								$$->set("space", $2);
								$$->add_child($3);
								release_value($2);
								$3 = NULL;
							}
	|	TOKEN_MATRIX STRING_LITERAL expression
							{
								$$ = new ri_slc_node("cast");
								$$->set("type", "matrix");
								$$->set("space", $2);
								$$->add_child($3);
								release_value($2);
								$3 = NULL;
							}
	;


function_call
	:	function_name '(' texture_argument ',' function_arguments ')'
							{
								ri_slc_node* args = $5;
								args->add_front($3);
								$$ = new ri_slc_node("function_call");
								$$->set("name", $1);
								$$->add_child(args);
								release_value($1);
								$3 = NULL;
								$5 = NULL;
							}
	|	function_name '(' texture_argument ')'
							{
								ri_slc_node* args = new ri_slc_node("function_arguments");
								args->add_child($3);
								$$ = new ri_slc_node("function_call");
								$$->set("name", $1);
								$$->add_child(args);
								release_value($1);
								$3 = NULL;
							}
	|	function_name '(' function_arguments ')'
							{
								$$ = new ri_slc_node("function_call");
								$$->set("name", $1);
								$$->add_child($3);
								release_value($1);
								$3 = NULL;
							}
	|	function_name '(' ')'
							{
								$$ = new ri_slc_node("function_call");
								$$->set("name", $1);
								release_value($1);
							}
	;

function_name
	:	IDENTIFIER
							{
								$$ = $1;
							}
	|	TOKEN_ATMOSPHERE
							{
								$$ = dups_("atmosphere");
							}
	|	TOKEN_DISPLACEMENT
							{
								$$ = dups_("displacement");
							}
	|	TOKEN_LIGHTSOURCE
							{
								$$ = dups_("lightsource");
							}
	|	TOKEN_SURFACE
							{
								$$ = dups_("surface");
							}
	;

function_arguments
	:	expression			{
								$$ = new ri_slc_node("function_arguments");
								$$->add_child($1);
								$1 = NULL;
							}
	|	function_arguments ',' expression
							{
								$$ = $1;
								$$->add_child($3);
								$1 = NULL;
								$3 = NULL;
							}
	;

texture_argument
	:	STRING_LITERAL
							{
								$$ = new ri_slc_node("texture_argument");
								release_value($1);
							}
	|	texture_argument '[' channel ']'
							{
								$1->set("channel", $3);
								$$ = $1;
								$1 = NULL;
							}
	;


number
	:	INTEGER_LITERAL
							{
								$$ = (float)$1;
							}
	|	FLOAT_LITERAL
							{
								$$ = (float)$1;
							}
	;

channel
	:	INTEGER_LITERAL
							{
								$$ = $1;
							}
	;


%%

void yy::rsl_parser::error (const location_type& loc, const std::string& msg)
{
	std::string fname = GET_FNAME();
	int lineno = GET_LINE();

	fprintf(stderr, "parser error %s :%s(%d)\n", msg.c_str(), fname.c_str(), lineno);
}


extern int yylex_init(void** p);
extern int yylex_destroy(void* p);

namespace kaze{
namespace ri{
	int load_rsl(FILE* in, ri_rsl_context* ctx)
	{
		ri_rsl_decoder dec(in);

		void* scanner;
		yylex_init(&scanner);

		ri_rsl_parse_param* param = (ri_rsl_parse_param*)(scanner);
		param->ctx = ctx;
		param->dec = &dec;
		param->line_number = 1;

		yy::rsl_parser parser(scanner);

		int nRet = parser.parse();

		if(1){
			printf("%s(%d)\n", GET_FNAME().c_str(), GET_LINE());
		}

		yylex_destroy(scanner);

		return nRet;

		return 0;
	}
}
}
