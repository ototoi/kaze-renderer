%option noyywrap nounput batch
%option never-interactive
%option noyy_scan_buffer
%option noyy_scan_bytes
%option noyy_scan_string
%option 8bit reentrant
%option nounistd

%{
#ifdef _WIN32
#pragma warning(disable : 4018)
#pragma warning(disable : 4102)
#pragma warning(disable : 4244)
#pragma warning(disable : 4267)
#pragma warning(disable : 4786)
#pragma warning(disable : 4996)

extern "C" int isatty(int);

#endif

#include <stdio.h>
#include <string>
#include <string.h>
#include <memory>
#ifdef HAVE_ZLIB
#include <zlib.h>
#endif

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>

#include "rsl_parser.hpp"
#include "ri_rsl_parse_param.h"

#define YY_EXTRA_TYPE kaze::ri::ri_rsl_parse_param

#define PARAM() ((kaze::ri::ri_rsl_parse_param*)(yyscanner))

#define	YY_DECL											\
	int							\
	yylex(yy::rsl_parser::semantic_type* yylval,		\
	     yy::rsl_parser::location_type* yyloc,          \
		 void* yyscanner)

#define YY_NO_INPUT 1
#undef YY_INPUT
#define YY_INPUT(buf, retval, maxlen)	if ( (retval = PARAM()->dec->read(buf,maxlen)) < 0) 			\
											YY_FATAL_ERROR( "input in flex scanner failed" );

typedef yy::rsl_parser::token token;

static
char* strip_string(const char* szText)
{
	std::string temp(szText);
	char* s = new char[temp.size()-1];
	strcpy(s, temp.substr(1, temp.size()-2).c_str());
	return s;
}

static
const std::string strip_string(const std::string& temp)
{
	char* s = new char[temp.size()-1];
	strcpy(s, temp.substr(1, temp.size()-2).c_str());
	return s;
}

static
void proc_hashline(const char* szText, int& line, std::string& filename)
{
	int line_ = 1;
	std::string filename_;

	std::string tmp(szText);
	std::string s = tmp.substr(tmp.find("line")+5);
	std::stringstream ss(s);

	ss >> line_;
	ss >> filename_;

	//printf("%d , %s\n", line_, filename_.c_str());
	line = line_;
	filename = strip_string(filename_);
}

%}

D			[0-9]
L			[a-zA-Z_]
H			[a-fA-F0-9]
E			[Ee][+-]?{D}+
FS			(f|F|l|L)
IS			(u|U|l|L)*
WS			[ \t]

eol			(\r\n)|(\r)|(\n)
whitespace  {WS}+

hashline	#{WS}?(line|{D}+)
hash		#{WS}?((pragma)).*{eol}

string		\"(\\.|[^\\"])*\"

/* %option lex-compat */
%option noreject

%%

{whitespace}		{  }
{eol}				{ PARAM()->line_number++; }
{hashline}.*{eol}	{ proc_hashline(yytext, PARAM()->line_number, PARAM()->filename);}
{hash}				{ PARAM()->line_number++; }

"float"			{ return token::TOKEN_FLOAT; }
"color"			{ return token::TOKEN_COLOR; }
"point"			{ return token::TOKEN_POINT; }
"vector"		{ return token::TOKEN_VECTOR; }
"normal"		{ return token::TOKEN_NORMAL; }
"matrix"		{ return token::TOKEN_MATRIX; }
"string"		{ return token::TOKEN_STRING; }
"void"			{ return token::TOKEN_VOID; }

"uniform"		{ return token::TOKEN_UNIFORM; }
"varying"		{ return token::TOKEN_VARYING; }

"output"		{ return token::TOKEN_OUTPUT; }
"extern"		{ return token::TOKEN_EXTERN; }

"surface"		 { return token::TOKEN_SURFACE; }
"light"			 { return token::TOKEN_LIGHT; }
"atmosphere"	 { return token::TOKEN_ATMOSPHERE; }
"volume"		 { return token::TOKEN_VOLUME; }
"displacement"	 { return token::TOKEN_DISPLACEMENT; }
"imager"		 { return token::TOKEN_IMAGER; }
"transformation" { return token::TOKEN_TRANSFORMATION; }

"if"			{ return token::TOKEN_IF; }
"else"			{ return token::TOKEN_ELSE; }
"while"			{ return token::TOKEN_WHILE; }
"for"			{ return token::TOKEN_FOR; }
"break"			{ return token::TOKEN_BREAK; }
"continue"		{ return token::TOKEN_CONTINUE; }
"return"		{ return token::TOKEN_RETURN; }

"illuminate"	{ return token::TOKEN_ILLUMINATE;}
"illuminance"	{ return token::TOKEN_ILLUMINANCE;}
"solar"			{ return token::TOKEN_SOLAR;}
"gather"		{ return token::TOKEN_GATHER;}


{L}({L}|{D})*	{ std::string temp(yytext); yylval->stype = new char[temp.size()+1];  strcpy(yylval->stype, temp.c_str()); return token::IDENTIFIER;}

0[xX]{H}+{IS}?	        { yylval->itype = (float)atoi(yytext);return token::INTEGER_LITERAL; }
0{D}+{IS}?		        { yylval->itype = (float)atoi(yytext);return token::INTEGER_LITERAL; }
{D}+{IS}?		        { yylval->itype = (float)atoi(yytext);return token::INTEGER_LITERAL; }

'(\\.|[^\\'])+'	        { yylval->ftype = (float)atof(yytext);return token::FLOAT_LITERAL; }
{D}+{E}{eol}?	        { yylval->ftype = (float)atof(yytext);return token::FLOAT_LITERAL; }
{D}*"."{D}+({FS})?{eol}?	{ yylval->ftype = (float)atof(yytext);return token::FLOAT_LITERAL; }
{D}+"."{D}*({FS})?{eol}?	{ yylval->ftype = (float)atof(yytext);return token::FLOAT_LITERAL; }
{D}*"."{D}+({E})?{eol}?	{ yylval->ftype = (float)atof(yytext);return token::FLOAT_LITERAL; }
{D}+"."{D}*({E})?{eol}?	{ yylval->ftype = (float)atof(yytext);return token::FLOAT_LITERAL; }

{string}		        { yylval->stype = strip_string(yytext);return token::STRING_LITERAL; }

"="				{ return '='; }
"+="			{ return token::TOKEN_ADD_ASSIGN; }
"-="			{ return token::TOKEN_SUB_ASSIGN; }
"*="			{ return token::TOKEN_MUL_ASSIGN; }
"/="			{ return token::TOKEN_DIV_ASSIGN; }

"&&"			{ return token::TOKEN_AND; }
"||"			{ return token::TOKEN_OR; }

"<"				{ return '<'; }
">"				{ return '>'; }
"<="			{ return token::TOKEN_LE; }
">="			{ return token::TOKEN_GE; }
"=="			{ return token::TOKEN_EQ; }
"!="			{ return token::TOKEN_NE; }

";"				{ return ';'; }
"{"				{ return '{'; }
"}"				{ return '}'; }
","				{ return ','; }
":"				{ return ':'; }
"("				{ return '('; }
")"				{ return ')'; }
"["				{ return '['; }
"]"				{ return ']'; }
"."				{ return '.'; }
"&"				{ return '&'; }
"!"				{ return '!'; }
"~"				{ return '~'; }
"-"				{ return '-'; }
"+"				{ return '+'; }
"*"				{ return '*'; }
"/"				{ return '/'; }
"%"				{ return '%'; }
"^"				{ return '^'; }
"|"				{ return '|'; }
"?"				{ return '?'; }

.			{ /* ignore bad characters */ }

%%
namespace kaze{
namespace ri{

	void ri_rsl_parse_param::scan_begin()
	{
		;//
	}

	void ri_rsl_parse_param::scan_end()
	{
		;//
	}

}
}
