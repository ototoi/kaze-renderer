#include "ri_slc_compile.h"
#include "ri_rsl_context.h"

namespace kaze
{
    namespace ri
    {

        extern int load_rsl(FILE* in, ri_rsl_context* ctx);

        int ri_slc_compile(const char* in, const char* out)
        {
            int nRet = 0;
            //FILE check
            FILE* fp = fopen(in, "rt");
            if (fp == NULL) return -1;

            ri_rsl_context ctx;
            nRet = load_rsl(fp, &ctx);

            fclose(fp);
            return nRet;
        }
    }
}