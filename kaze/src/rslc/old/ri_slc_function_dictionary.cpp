#include "ri_slc_function_dictionary.h"

#include <sstream>

namespace kaze
{
    namespace ri
    {

        typedef ri_slc_function_dictionary::variable_entry variable_entry;
        typedef ri_slc_function_dictionary::func_entry func_entry;

        variable_entry::variable_entry(const std::string& klass, const std::string& type, const std::string& name)
        {
            type_name_ = type;
            name_ = name;
        }

        variable_entry::variable_entry(const std::string& type, const std::string& name)
        {
            type_name_ = type;
            name_ = name;
        }

        bool variable_entry::equals(const variable_entry& b) const
        {
            if (type_name_ != b.type_name_) return false;
            return true;
        }

        struct TokenMangle
        {
            const char* token;
            const char* value;
        };

        static TokenMangle MANGLE_TABLE[] =
            {
                {"int", "i1"},
                {"integer", "i1"},
                {"float", "f1"},
                {"string", "s1"},
                {"color", "f3"},
                {"point", "f3"},
                {"vector", "f3"},
                {"normal", "f3"},
                {"hpoint", "f4"},
                {"mpoint", "fx"},
                {"matrix", "fx"},
                {"void", "nn"},
                {NULL, "nn"}};

        static std::string ToS(int i)
        {
            char buffer[64];
            sprintf(buffer, "%d", i);
            return buffer;
        }

        static std::string TypeToMangle(const std::string& type)
        {
            int i = 0;
            while (MANGLE_TABLE[i].token)
            {
                if (type == MANGLE_TABLE[i].token)
                {
                    return MANGLE_TABLE[i].value;
                }
                i++;
            }
            return "nn";
        }

        static std::string MangleFuncName(const std::string& name, const std::vector<variable_entry>& output, const std::vector<variable_entry>& input)
        {
            std::string sRet = name;
            sRet += "_";

            //sRet += ToS(output.size());
            for (size_t i = 0; i < output.size(); i++)
            {
                sRet += TypeToMangle(output[i].get_type_name());
            }

            sRet += "_";
            //sRet += ToS(input.size());
            for (size_t i = 0; i < input.size(); i++)
            {
                sRet += TypeToMangle(input[i].get_type_name());
            }
            return sRet;
        }

        func_entry::func_entry(const func_entry& e)
            : name_(e.name_), output_(e.output_), input_(e.input_)
        {
            mangled_name_ = e.mangled_name_;
        }

        func_entry::func_entry(const std::string& name, const std::vector<variable_entry>& output, const std::vector<variable_entry>& input)
            : name_(name), output_(output), input_(input)
        {
            mangled_name_ = MangleFuncName(name, output, input);
        }

        bool func_entry::equals(const func_entry& b) const
        {
            if (name_ != b.name_) return false;
            if (mangled_name_ != b.mangled_name_) return false;
            return true;
        }

        //------------------------------------------------------
        typedef ri_slc_function_dictionary::func_list func_list;
        typedef ri_slc_function_dictionary::entry_map entry_map;

        static void Destroy(func_list& list)
        {
            typedef func_list::const_iterator iter;
            for (iter it = list.begin(); it != list.end(); ++it)
            {
                delete *it;
            }
        }

        const char* TYPE_TABLE[] =
            {
                "integer",
                "float",
                "color",
                "point",
                "vector",
                "normal",
                "hpoint",
                "matrix",
                "bound",
                NULL};

        static std::vector<std::string> SplitTokens(const std::vector<std::string>& tRet, const std::string& tok)
        {
            size_t toklen = tok.length();
            std::vector<std::string> sRet;
            for (size_t i = 0; i < tRet.size(); i++)
            {
                std::string s = tRet[i];
                std::string::size_type b1 = s.find(tok);
                if (b1 != std::string::npos)
                {
                    std::string a = s.substr(0, b1);
                    std::string b = s.substr(b1 + toklen);
                    if (!a.empty()) sRet.push_back(a);
                    sRet.push_back(tok);
                    if (!b.empty()) sRet.push_back(b);
                }
                else
                {
                    sRet.push_back(s);
                }
            }
            return sRet;
        }

        static void SplitTokens(std::vector<std::string>& tokens, const std::vector<std::string>& desc)
        {
            std::vector<std::string> sRet = desc;
            sRet = SplitTokens(sRet, "[");
            sRet = SplitTokens(sRet, "]");
            sRet = SplitTokens(sRet, ",");
            sRet = SplitTokens(sRet, ";");
            sRet = SplitTokens(sRet, "(");
            sRet = SplitTokens(sRet, ")");
            int i = 0;
            while (TYPE_TABLE[i])
            {
                sRet = SplitTokens(sRet, TYPE_TABLE[i]);
                i++;
            }
            tokens = sRet;
        }

        static void SplitTokens(std::vector<std::string>& tokens, const std::string& desc)
        {
            std::stringstream ss(desc);

            std::vector<std::string> descs;
            std::string tok;
            while (ss)
            {
                tok.clear();
                ss >> tok;
                if (!tok.empty())
                {
                    descs.push_back(tok);
                }
            }
            SplitTokens(tokens, descs);
        }

        static std::string Trim(const std::string& s)
        {
            std::string sRet;
            std::string l;
            std::stringstream ss(s);
            while (ss)
            {
                l.clear();
                ss >> l;
                if (!l.empty())
                {
                    if (sRet.empty())
                        sRet = l;
                    else
                        sRet += " " + l;
                }
            }
            return sRet;
        }

        static variable_entry DescToVariableEntry(const std::string& desc)
        {
            std::vector<std::string> tokens;
            SplitTokens(tokens, desc);
            if (tokens.size() == 1)
            {
                return variable_entry(tokens[0], "dummy_");
            }
            else if (tokens.size() == 2)
            {
                return variable_entry(tokens[0], tokens[1]);
            }
            else if (tokens.size() == 3)
            {
                return variable_entry(tokens[0], tokens[1], tokens[2]);
            }
            return variable_entry("void", "dummy_");
        }

        static std::vector<variable_entry> DescToVariableEntry(const std::vector<std::string>& desc)
        {
            std::vector<variable_entry> vRet;
            for (size_t i = 0; i < desc.size(); i++)
            {
                vRet.push_back(DescToVariableEntry(desc[i]));
            }
            return vRet;
        }

        static func_entry DescToFuncEntry(const std::string& desc)
        {
            std::vector<std::string> tokens;
            SplitTokens(tokens, desc);

            std::string name = "";
            std::vector<std::string> output;
            std::vector<std::string> input;

            std::string tmp = "";
            int mode = 0;
            for (int i = 0; i < tokens.size(); i++)
            {
                std::string s = tokens[i];
                switch (mode)
                {
                case 0:
                {
                    tmp += s;
                    if (i + 1 < tokens.size())
                    {
                        std::string s1 = tokens[i + 1];
                        if (s1 == "[")
                        {
                            tmp += "[]";
                        }
                    }
                    output.push_back(tmp);
                    tmp = "";
                    mode++;
                }
                break;
                case 1:
                {
                    if (s == "(")
                    {
                        name = Trim(tmp);
                        tmp = "";
                        mode++;
                    }
                    else if (s == "]")
                    {
                        tmp = "";
                    }
                    else
                    {
                        tmp += s + " ";
                    }
                }
                break;
                case 2:
                {
                    if (s == ")" || i == (tokens.size() - 1))
                    {
                        input.push_back(Trim(tmp));
                        tmp = "";
                        mode++;
                    }
                    else if (s == "," || s == ";")
                    {
                        input.push_back(tmp);
                        tmp = "";
                    }
                    else
                    {
                        tmp += s + " ";
                    }
                }
                break;
                case 3:
                {
                    ; //Nothing TODO
                }
                break;
                }
            }
            /*
            for(int k=0;k<output.size();k++)
            {
                printf("%s", output[k].c_str());
            }
            for(int k=0;k<input.size();k++)
            {
                printf("%s", input[k].c_str());
            }
            */

            std::vector<variable_entry> oe = DescToVariableEntry(output);
            std::vector<variable_entry> ie = DescToVariableEntry(input);
            return func_entry(name, oe, ie);
        }

        ri_slc_function_dictionary::ri_slc_function_dictionary()
            : p_parent_(NULL)
        {
        }

        ri_slc_function_dictionary::~ri_slc_function_dictionary()
        {
            typedef entry_map::iterator iter;
            for (iter it = entries_.begin(); it != entries_.end(); ++it)
            {
                Destroy(it->second);
            }
        }

        func_entry* ri_slc_function_dictionary::add(const std::string& desc)
        {
            func_entry entry = DescToFuncEntry(desc);
            func_entry* pFind = this->find(entry);
            if (pFind != NULL) return pFind;

            typedef entry_map::iterator map_iter;
            map_iter mit = entries_.find(entry.get_name());
            if (mit != entries_.end())
            {
                mit->second.push_back(new func_entry(entry));
                return mit->second.back();
            }
            else
            {
                func_entry* e = new func_entry(entry);
                func_list list;
                list.push_back(e);
                entries_.insert(entry_map::value_type(entry.get_name(), list));
                return e;
            }
        }
        func_entry* ri_slc_function_dictionary::find(const std::string& desc) const
        {
            func_entry entry = DescToFuncEntry(desc);
            return this->find(entry);
        }
        func_entry* ri_slc_function_dictionary::find(const func_entry& entry) const
        {
            typedef entry_map::const_iterator map_iter;
            map_iter mit = entries_.find(entry.get_name());
            if (mit != entries_.end())
            {
                typedef func_list::const_iterator list_iter;
                const func_list& list = mit->second;
                for (list_iter it = list.begin(); it != list.end(); it++)
                {
                    const func_entry* e = *it;
                    if (*e == entry)
                    {
                        return (func_entry*)e;
                    }
                }
            }
            if (p_parent_)
            {
                return p_parent_->find(entry);
            }
            return NULL;
        }

        static std::string DumpString(const std::vector<variable_entry>& input)
        {
            std::string sRet;
            if (!input.empty())
            {
                for (size_t i = 0; i < input.size(); i++)
                {
                    const variable_entry& v = input[i];
                    sRet += v.get_type_name() + " " + v.get_name();
                    if (i != (int)(input.size() - 1))
                    {
                        sRet += ", ";
                    }
                }
            }
            else
            {
                sRet = "void";
            }
            return sRet;
        }

        static void Dump(const func_entry* e)
        {
            std::string oname = e->output_[0].get_type_name();
            std::string fname = e->get_name();
            std::string mname = e->get_mangled_name();
            std::string iname = DumpString(e->input_);

            printf("O(%s) F(%s:%s) I(%s)\n", oname.c_str(), fname.c_str(), mname.c_str(), iname.c_str());
        }

        void ri_slc_function_dictionary::dump() const
        {
            typedef entry_map::const_iterator map_iter;
            for (map_iter mit = entries_.begin(); mit != entries_.end(); ++mit)
            {
                typedef func_list::const_iterator list_iter;
                const func_list& list = mit->second;
                for (list_iter it = list.begin(); it != list.end(); it++)
                {
                    const func_entry* e = *it;
                    Dump(e);
                }
            }
        }

        void ri_slc_function_dictionary::set_parent(ri_slc_function_dictionary* parent)
        {
            p_parent_ = parent;
        }
    }
}
