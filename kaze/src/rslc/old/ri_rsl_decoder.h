#ifndef KAZE_RI_RSL_DECODER_H
#define KAZE_RI_RSL_DECODER_H

#include <cstdio>
#include <string>
#include <vector>
#include <map>

namespace kaze
{
    namespace ri
    {

        class ri_rsl_decoder
        {
        public:
            ri_rsl_decoder(FILE* fp);
            ~ri_rsl_decoder();
            int read(char* buffer, unsigned int size);

        private:
            FILE* fp_;
        };
    }
}

#endif