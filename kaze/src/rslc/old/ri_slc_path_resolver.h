#ifndef KAZE_RI_SLC_PATH_RESOLVER_H
#define KAZE_RI_SLC_PATH_RESOLVER_H

#include <string>

namespace kaze
{
    namespace ri
    {

        class ri_slc_path_resolver
        {
        public:
            static std::string get_bin_path();
            static std::string get_temp_path();
            static std::string replace_ext(const std::string& s, const std::string& ext);
            static std::string get_full_path(const std::string& s);
        };
    }
}

#endif