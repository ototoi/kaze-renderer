#ifndef KAZE_RI_SLC_PARAMETERS_H
#define KAZE_RI_SLC_PARAMETERS_H

#include <vector>
#include <map>
#include <string>

namespace kaze
{
    namespace ri
    {

        class ri_slc_parameters
        {
        public:
            typedef const char* LPCSTR;
            enum
            {
                TYPE_INTEGER,
                TYPE_FLOAT,
                TYPE_STRING
            };
            struct value_type
            {
                value_type(const value_type& val)
                    : nType(val.nType), integer_values(val.integer_values), float_values(val.float_values), string_values(val.string_values) {}
                value_type(const value_type* val)
                    : nType(val->nType), integer_values(val->integer_values), float_values(val->float_values), string_values(val->string_values) {}
                value_type(int val)
                {
                    integer_values.push_back(val);
                    nType = TYPE_INTEGER;
                }
                value_type(float val)
                {
                    float_values.push_back(val);
                    nType = TYPE_FLOAT;
                }
                value_type(const char* val)
                {
                    string_values.push_back(val);
                    nType = TYPE_STRING;
                }
                value_type(const int* a, int n)
                {
                    integer_values.resize(n);
                    for (int i = 0; i < n; i++)
                    {
                        integer_values[i] = a[i];
                    }
                    nType = TYPE_INTEGER;
                }
                value_type(const float* a, int n)
                {
                    float_values.resize(n);
                    for (int i = 0; i < n; i++)
                    {
                        float_values[i] = a[i];
                    }
                    nType = TYPE_FLOAT;
                }
                value_type(const LPCSTR* a, int n)
                {
                    string_values.resize(n);
                    for (int i = 0; i < n; i++)
                    {
                        string_values[i] = a[i];
                    }
                    nType = TYPE_STRING;
                }

                value_type(const std::string& val)
                {
                    string_values.push_back(val);
                    nType = TYPE_STRING;
                }

                value_type(const std::string* a, int n)
                {
                    string_values.resize(n);
                    for (int i = 0; i < n; i++)
                    {
                        string_values[i] = a[i];
                    }
                    nType = TYPE_STRING;
                }

                int nType;
                std::vector<int> integer_values;
                std::vector<float> float_values;
                std::vector<std::string> string_values;
            };
            typedef std::map<std::string, value_type*> map_type;

        public:
            ri_slc_parameters();
            ~ri_slc_parameters();
            ri_slc_parameters(const ri_slc_parameters& rhs);
            ri_slc_parameters& operator=(const ri_slc_parameters& rhs);

            bool set(const char* key, const value_type& val);
            bool set(const char* key, int val);
            bool set(const char* key, float val);
            bool set(const char* key, const char* val);
            bool set(const char* key, const int* val, int n);
            bool set(const char* key, const float* val, int n);
            bool set(const char* key, const LPCSTR* val, int n);

        public:
            bool set(const char* key, const std::string& val);
            bool set(const char* key, const std::string* val, int n);

        public:
            const value_type* get(const char* key) const;

        public:
            void swap(ri_slc_parameters& rhs);

        protected:
            map_type map_;
        };
    }
}

#endif
