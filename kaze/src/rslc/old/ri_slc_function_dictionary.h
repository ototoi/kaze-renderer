#ifndef KAZE_RI_SLC_FUNCTION_DICTIONARY_H
#define KAZE_RI_SLC_FUNCTION_DICTIONARY_H

#include <string>
#include <vector>
#include <list>
#include <map>

namespace kaze
{
    namespace ri
    {

        class ri_slc_function_dictionary
        {
        public:
            enum ENUM_CLASS
            {
                CONSTANT = 1,
                UNIFORM = 2,
                EXTERN = 3,
                OUTPUT = 4
            };
            enum ENUM_TYPE
            {
                VOID,
                INTEGER,
                FLOAT,
                STRING,
                COLOR,
                POINT,
                VECTOR,
                NORMAL,
                HPOINT,
                MATRIX,
                BASIS,
                BOUND
            };
            struct variable_entry
            {
                int class_;
                int type_;
                std::string type_name_;
                std::string name_;

                variable_entry(const std::string& klass, const std::string& type, const std::string& name);
                variable_entry(const std::string& type, const std::string& name);

                bool equals(const variable_entry& b) const;
                int get_class() const { return class_; }
                int get_type() const { return type_; }
                std::string get_type_name() const { return type_name_; }
                std::string get_name() const { return name_; }
            };

            class func_entry
            {
            public:
                std::string name_;
                std::string mangled_name_;
                std::vector<variable_entry> output_;
                std::vector<variable_entry> input_;

            public:
                func_entry(const func_entry& e);
                func_entry(const std::string& name, const std::vector<variable_entry>& output, const std::vector<variable_entry>& input);

                bool equals(const func_entry& b) const;
                std::string get_name() const { return name_; }
                std::string get_mangled_name() const { return mangled_name_; }
                const std::vector<variable_entry>& get_output_types() const { return output_; }
                const std::vector<variable_entry>& get_input_types() const { return input_; }
            };

            ri_slc_function_dictionary();
            ~ri_slc_function_dictionary();

            func_entry* add(const std::string& desc);
            func_entry* find(const std::string& desc) const;
            func_entry* find(const func_entry& entry) const;

        public:
            void dump() const;

        public:
            void set_parent(ri_slc_function_dictionary* parent);
            typedef std::list<func_entry*> func_list;
            typedef std::map<std::string, func_list> entry_map;

        protected:
            entry_map entries_;
            ri_slc_function_dictionary* p_parent_;
        };

        inline bool operator==(const ri_slc_function_dictionary::variable_entry& a, const ri_slc_function_dictionary::variable_entry& b)
        {
            return a.equals(b);
        }

        inline bool operator==(const ri_slc_function_dictionary::func_entry& a, const ri_slc_function_dictionary::func_entry& b)
        {
            return a.equals(b);
        }
    }
}

#endif
