#ifndef KAZE_RI_RSL_AST_NODE_H
#define KAZE_RI_RSL_AST_NODE_H

#include <vector>
#include <string>

namespace kaze
{
    namespace ri
    {

        enum
        {
            RI_RSL_AST_NODE_UNKNOWN = 0,
            RI_RSL_AST_NODE_DIFINITIONS,
            RI_RSL_AST_NODE_SHADER,
            RI_RSL_AST_NODE_SHADER_TYPE,
            RI_RSL_AST_NODE_SHADER_ARGUMENTS,
            RI_RSL_AST_NODE_SHADER_STATEMENTS,
            RI_RSL_AST_NODE_FUNCTION,
            RI_RSL_AST_NODE_TYPE,
            RI_RSL_AST_NODE_WHILE_STATEMENT,
            RI_RSL_AST_NODE_FOR_STATEMENT,
            RI_RSL_AST_NODE_IF_STATEMENT,
            RI_RSL_AST_NODE_BREAK_STATEMENT,
            RI_RSL_AST_NODE_CONTINUE_STATEMENT,
            RI_RSL_AST_NODE_ASSIGN,
            RI_RSL_AST_NODE_CMP,
            RI_RSL_AST_NODE_DOT,
            RI_RSL_AST_NODE_CROSS,
            RI_RSL_AST_NODE_ADD,
            RI_RSL_AST_NODE_SUB,
            RI_RSL_AST_NODE_MUL,
            RI_RSL_AST_NODE_DIV,
            RI_RSL_AST_NODE_NEG,
            RI_RSL_AST_NODE_CAST,
            RI_RSL_AST_NODE_COND,
            RI_RSL_AST_NODE_FLOAT,
            RI_RSL_AST_NODE_STRING,
            RI_RSL_AST_NODE_VECTOR,
            RI_RSL_AST_NODE_MATRIX,
            RI_RSL_AST_NODE_VARIABLE_REFERENCE,
            RI_RSL_AST_NODE_VARIABLE_INDEXING,
            RI_RSL_AST_NODE_TEXTURE_ARGUMENT,
            RI_RSL_AST_NODE_FUNCTION_ARGUMENTS,
            RI_RSL_AST_NODE_FUNCTION_CALL,
        };

        class ri_rsl_ast_node;
        class ri_rsl_difinitions_node;
        class ri_rsl_shader_node;
        class ri_rsl_shader_type_node;
        class ri_rsl_shader_arguments_node;
        class ri_rsl_shader_statements_node;
        class ri_rsl_function_node;
        class ri_rsl_function_type_node;
        class ri_rsl_variable_type_node;
        class ri_rsl_variable_class_node;
        class ri_rsl_environment_node;
        class ri_rsl_block_statement_node;

        class ri_rsl_function_call_node;

        class ri_rsl_ast_node
        {
        public:
            ri_rsl_ast_node() {}
            ri_rsl_ast_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : children_(children) {}
            virtual ~ri_rsl_ast_node() {}
            virtual int typeN() const { return RI_RSL_AST_NODE_UNKNOWN; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const = 0;

        public:
            void add_front(const std::shared_ptr<ri_rsl_ast_node>& c)
            {
                std::vector<std::shared_ptr<ri_rsl_ast_node> > tmp;
                tmp.push_back(c);
                for (size_t i = 0; i < children_.size(); i++)
                    tmp.push_back(children_[i]);
                children_.swap(tmp);
            }
            void add_back(const std::shared_ptr<ri_rsl_ast_node>& c) { children_.push_back(c); }
            void add_child(const std::shared_ptr<ri_rsl_ast_node>& c) { children_.push_back(c); }
            size_t get_child_size() const { return children_.size(); }
            const std::shared_ptr<ri_rsl_ast_node>& get_child(size_t i) const { return children_[i]; }
            virtual std::vector<std::shared_ptr<ri_rsl_ast_node> > clone_children() const
            {
                std::vector<std::shared_ptr<ri_rsl_ast_node> > ret;
                for (size_t i = 0; i < children_.size(); i++)
                {
                    std::shared_ptr<ri_rsl_ast_node> c = children_[i]->clone();
                    ret.push_back(c);
                }
                return ret;
            }

        protected:
            std::vector<std::shared_ptr<ri_rsl_ast_node> > children_;
        };

        class ri_rsl_difinitions_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_difinitions_node() {}
            ri_rsl_difinitions_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_DIFINITIONS; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_difinitions_node(clone_children())); }
        };

        class ri_rsl_shader_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_shader_node(ri_rsl_shader_type_node* t, ri_rsl_shader_arguments_node* a, ri_rsl_shader_statements_node* s)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)t)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //1
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)s)); //2
            }
            ri_rsl_shader_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_SHADER; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_shader_node(clone_children())); }
        public:
            const ri_rsl_shader_type_node* get_shader_type_node() const { return (const ri_rsl_shader_type_node*)(children_[0].get()); }
            const ri_rsl_shader_arguments_node* get_arguments_node() const { return (const ri_rsl_shader_arguments_node*)(children_[1].get()); }
            const ri_rsl_shader_statements_node* get_statements_node() const { return (const ri_rsl_shader_statements_node*)(children_[2].get()); }
        };

        class ri_rsl_shader_type_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_shader_type_node(int nType) : nType_(nType) {}
            ri_rsl_shader_type_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_SHADER_TYPE; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_shader_type_node(clone_children())); }
        public:
            int get_shader_type() const { return nType_; }
            int get_shader_class() const { return nType_; }
        protected:
            int nType_;
        };
        //---------------------------------------------------
        class ri_rsl_type_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_type_node(const std::string& type) : type_(type) {}
            ri_rsl_type_node(const std::string& type, const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children), type_(type) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_TYPE; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_type_node(type_, clone_children())); }
        public:
            const std::string& get_type() const { return type_; }
        protected:
            std::string type_;
        };

        //---------------------------------------------------
        class ri_rsl_while_statement_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_while_statement_node() {}
            ri_rsl_while_statement_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_WHILE_STATEMENT; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_while_statement_node(clone_children())); }
        };

        class ri_rsl_for_statement_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_for_statement_node() {}
            ri_rsl_for_statement_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_FOR_STATEMENT; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_for_statement_node(clone_children())); }
        };

        class ri_rsl_if_statement_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_if_statement_node() {}
            ri_rsl_if_statement_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_BREAK_STATEMENT; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_if_statement_node(clone_children())); }
        };

        class ri_rsl_break_statement_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_break_statement_node() {}
            ri_rsl_break_statement_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_BREAK_STATEMENT; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_break_statement_node(clone_children())); }
        };

        class ri_rsl_continue_statement_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_continue_statement_node() {}
            ri_rsl_continue_statement_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_CONTINUE_STATEMENT; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_continue_statement_node(clone_children())); }
        };

        //---------------------------------------------------
        class ri_rsl_dot_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_dot_node(ri_rsl_ast_node* a, ri_rsl_ast_node* b)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)b)); //0
            }
            ri_rsl_dot_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_DOT; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_dot_node(clone_children())); }
        };

        class ri_rsl_cross_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_cross_node(ri_rsl_ast_node* a, ri_rsl_ast_node* b)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)b)); //0
            }
            ri_rsl_cross_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_CROSS; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_cross_node(clone_children())); }
        };

        class ri_rsl_add_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_add_node(ri_rsl_ast_node* a, ri_rsl_ast_node* b)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)b)); //0
            }
            ri_rsl_add_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_ADD; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_add_node(clone_children())); }
        };

        class ri_rsl_sub_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_sub_node(ri_rsl_ast_node* a, ri_rsl_ast_node* b)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)b)); //0
            }
            ri_rsl_sub_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_SUB; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_sub_node(clone_children())); }
        };

        class ri_rsl_mul_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_mul_node(ri_rsl_ast_node* a, ri_rsl_ast_node* b)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)b)); //0
            }
            ri_rsl_mul_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_MUL; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_mul_node(clone_children())); }
        };

        class ri_rsl_div_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_div_node(ri_rsl_ast_node* a, ri_rsl_ast_node* b)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)b)); //0
            }
            ri_rsl_div_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_DIV; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_div_node(clone_children())); }
        };

        class ri_rsl_neg_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_neg_node(ri_rsl_ast_node* a)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
            }
            ri_rsl_neg_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_NEG; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_neg_node(clone_children())); }
        };

        class ri_rsl_cast_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_cast_node(ri_rsl_ast_node* type, ri_rsl_ast_node* a)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)type)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a));    //0
            }
            ri_rsl_cast_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_CAST; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_cast_node(clone_children())); }
        };

        class ri_rsl_cond_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_cond_node(ri_rsl_ast_node* r, ri_rsl_ast_node* a, ri_rsl_ast_node* b)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)r)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)b)); //0
            }
            ri_rsl_cond_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_COND; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_cond_node(clone_children())); }
        };

        //---------------------------------------------------
        class ri_rsl_cmp_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_cmp_node(const std::string& op, ri_rsl_ast_node* a)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
            }
            ri_rsl_cmp_node(const std::string& op, ri_rsl_ast_node* a, ri_rsl_ast_node* b)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)b)); //0
            }
            ri_rsl_cmp_node(const std::string& op, const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children), op_(op) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_CMP; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_cmp_node(op_, clone_children())); }
        public:
            const std::string& get_operator() const { return op_; }
        protected:
            std::string op_;
        };

        class ri_rsl_assign_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_assign_node(const std::string& op, ri_rsl_ast_node* a)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
            }
            ri_rsl_assign_node(const std::string& op, ri_rsl_ast_node* a, ri_rsl_ast_node* b)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)b)); //0
            }
            ri_rsl_assign_node(const std::string& op, const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children), op_(op) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_ASSIGN; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_assign_node(op_, clone_children())); }
        public:
            const std::string& get_operator() const { return op_; }
        protected:
            std::string op_;
        };

        //---------------------------------------------------
        class ri_rsl_float_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_float_node(float f) : f_(f) {}
            ri_rsl_float_node(float f, const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children), f_(f) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_FLOAT; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_float_node(f_, clone_children())); }
        public:
            float get_float() const { return f_; }
        protected:
            float f_;
        };

        class ri_rsl_string_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_string_node(const std::string& str) : str_(str) {}
            ri_rsl_string_node(const std::string& str, const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children), str_(str) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_STRING; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_string_node(str_, clone_children())); }
        public:
            const std::string& get_string() const { return str_; }
        protected:
            std::string str_;
        };

        class ri_rsl_vector_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_vector_node(ri_rsl_ast_node* a, ri_rsl_ast_node* b, ri_rsl_ast_node* c)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)b)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)c)); //0
            }
            ri_rsl_vector_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_VECTOR; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_vector_node(clone_children())); }
        };

        class ri_rsl_matrix_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_matrix_node(
                ri_rsl_ast_node* m1,
                ri_rsl_ast_node* m2,
                ri_rsl_ast_node* m3,
                ri_rsl_ast_node* m4,
                ri_rsl_ast_node* m5,
                ri_rsl_ast_node* m6,
                ri_rsl_ast_node* m7,
                ri_rsl_ast_node* m8,
                ri_rsl_ast_node* m9,
                ri_rsl_ast_node* m10,
                ri_rsl_ast_node* m11,
                ri_rsl_ast_node* m12,
                ri_rsl_ast_node* m13,
                ri_rsl_ast_node* m14,
                ri_rsl_ast_node* m15,
                ri_rsl_ast_node* m16)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m1));  //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m2));  //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m3));  //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m4));  //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m5));  //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m6));  //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m7));  //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m8));  //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m9));  //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m10)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m11)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m12)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m13)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m14)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m15)); //0
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)m16)); //0
            }
            ri_rsl_matrix_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_VECTOR; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_matrix_node(clone_children())); }
        };

        class ri_rsl_variable_reference_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_variable_reference_node(const std::string& name) : name_(name) {}
            ri_rsl_variable_reference_node(const std::string& name, const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children), name_(name) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_VARIABLE_REFERENCE; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_variable_reference_node(name_, clone_children())); }
        public:
            const std::string& get_name() const { return name_; }
        protected:
            std::string name_;
        };

        class ri_rsl_variable_indexing_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_variable_indexing_node(const std::string& name) : name_(name) {}
            ri_rsl_variable_indexing_node(const std::string& name, ri_rsl_ast_node* a) : name_(name)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>((ri_rsl_ast_node*)a)); //0
            }
            ri_rsl_variable_indexing_node(const std::string& name, const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children), name_(name) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_VARIABLE_INDEXING; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_variable_indexing_node(name_, clone_children())); }
        public:
            const std::string& get_name() const { return name_; }
        protected:
            std::string name_;
        };

        //---------------------------------------------------
        class ri_rsl_texture_argument_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_texture_argument_node(const std::string& name) : name_(name), ch_(-1) {}
            ri_rsl_texture_argument_node(const std::string& name, int ch) : name_(name), ch_(ch) {}
            ri_rsl_texture_argument_node(const std::string& name, int ch, const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children), name_(name), ch_(ch) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_TEXTURE_ARGUMENT; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_texture_argument_node(name_, ch_, clone_children())); }
        public:
            const std::string& get_name() const { return name_; }
            void set_channel(int ch) { ch_ = ch; }
        protected:
            std::string name_;
            int ch_;
        };

        class ri_rsl_function_arguments_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_function_arguments_node() {}
            ri_rsl_function_arguments_node(const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_FUNCTION_ARGUMENTS; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_function_arguments_node(clone_children())); }
        };

        class ri_rsl_function_call_node : public ri_rsl_ast_node
        {
        public:
            ri_rsl_function_call_node(const std::string& name, ri_rsl_function_arguments_node* a) : name_(name)
            {
                add_child(std::shared_ptr<ri_rsl_ast_node>(a));
            }
            ri_rsl_function_call_node(const std::string& name, const std::vector<std::shared_ptr<ri_rsl_ast_node> >& children) : ri_rsl_ast_node(children), name_(name) {}
            virtual int typeN() const { return RI_RSL_AST_NODE_FUNCTION_CALL; }
            virtual std::shared_ptr<ri_rsl_ast_node> clone() const { return std::shared_ptr<ri_rsl_ast_node>(new ri_rsl_function_call_node(name_, clone_children())); }
        public:
            const std::string& get_name() const { return name_; }
        protected:
            std::string name_;
        };
    }
}

#endif