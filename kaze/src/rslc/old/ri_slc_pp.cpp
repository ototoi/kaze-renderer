#include "ri_slc_pp.h"
#include "ri_slc_path_resolver.h"

#include "timer.h"
#include "logger.h"

#ifndef _WIN32
#include <errno.h>
#endif

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif

namespace kaze
{
    namespace ri
    {

        static std::string MakeFullPath(const char* szFilePath)
        {
#ifdef _WIN32
            char szFullPath[_MAX_PATH] = "";
            _fullpath(szFullPath, szFilePath, _MAX_PATH);
            return szFullPath;
#else
            char szFullPath[512];
            realpath(szFilePath, szFullPath);
            return szFullPath;
#endif
        }

#ifdef _WIN32
        static int ExecuteProcess(const char* szCmd, const char* szArg)
        {
            PROCESS_INFORMATION pi;
            STARTUPINFOA si;

            ZeroMemory(&si, sizeof(si));
            si.cb = sizeof(si);

            si.cb = sizeof(STARTUPINFO);
            si.dwFlags = STARTF_USESHOWWINDOW;
            si.wShowWindow = SW_SHOWNORMAL;

#if 0
            int nWINDOW = CREATE_NEW_CONSOLE;
#else
            int nWINDOW = CREATE_NO_WINDOW;
#endif
            std::string strCmdLine;
            strCmdLine += szCmd;
            strCmdLine += " ";
            strCmdLine += szArg;

            std::vector<char> s_strCmdLine(strCmdLine.size() + 1);
            strcpy(&s_strCmdLine[0], strCmdLine.c_str());
            s_strCmdLine.back() = 0;

            BOOL bRet = ::CreateProcessA(NULL, (char*)(&s_strCmdLine[0]), NULL, NULL, FALSE, nWINDOW, NULL, NULL, &si, &pi);

            if (bRet)
            {
                DWORD nExitCode = 0;
                ::CloseHandle(pi.hThread);
                ::WaitForSingleObject(pi.hProcess, INFINITE);
                ::GetExitCodeProcess(pi.hProcess, &nExitCode);
                ::CloseHandle(pi.hProcess);
                return nExitCode;
            }

            if (!bRet) return -1;

            return 0;
        }
#else
        static int ExecuteProcess(const char* szCmd, const char* szArg)
        {
            std::string cmdLine;
            cmdLine += szCmd;
            cmdLine += " ";
            cmdLine += szArg;

            //printf("%s\n", cmdLine.c_str());
            int nRet = ::system(cmdLine.c_str());
            if (nRet == 127 || nRet == -1)
            {
                print_log("%s\n", strerror(errno));
                return -1;
            }
            return nRet;
        }
#endif

        static bool IsExistFile(const char* szFilePath)
        {
#ifdef _WIN32
            struct _stat s;
            int nRet = _stat(szFilePath, &s);
            return (nRet == 0);
#else
            struct stat s;
            int nRet = stat(szFilePath, &s);
            return (nRet == 0);
#endif
        }

        static size_t GetLastModifiedTime(const char* szFilePath)
        {
#ifdef _WIN32
            struct _stat s;
            int nRet = _stat(szFilePath, &s);
            return s.st_mtime; //mtime
#else
            struct stat s;
            int nRet = stat(szFilePath, &s);
            return s.st_mtime; //mtime
#endif
        }

        int ri_slc_pp(const char* szIn, const char* szOut)
        {
            std::string strSLCmp = ri_slc_path_resolver::get_bin_path() + "kzrislpp";

#ifdef _WIN32
            strSLCmp += ".exe";
#endif
            std::string strFullIn = MakeFullPath(szIn);
            std::string strFullOut = MakeFullPath(szOut);

            if (!IsExistFile(strFullIn.c_str())) return -1;

            std::string strCmd;
            strCmd += "\"";
            strCmd += strSLCmp;
            strCmd += "\"";

            std::string strArg;
            strArg += "\"";
            strArg += strFullIn;
            strArg += "\"";

            strArg += " ";
            strArg += ">";

            strArg += " ";
            strArg += "\"";
            strArg += strFullOut;
            strArg += "\"";

            int nRet = ExecuteProcess(strCmd.c_str(), strArg.c_str());

            return nRet;
        }
    }
}