#ifndef KAZE_RI_SLC_NODE_H
#define KAZE_RI_SLC_NODE_H

#include "ri_slc_parameters.h"
#include "count_ptr.hpp"

namespace kaze
{
    namespace ri
    {

        class ri_slc_node
        {
        public:
            typedef const char* LPCSTR;

        public:
            ri_slc_node(const std::string& type = "Unknown");
            virtual ~ri_slc_node();
            virtual const std::string& type() const;
            virtual std::shared_ptr<ri_slc_node> clone() const;

        public:
            ri_slc_node(const ri_slc_node& node);

        public:
            virtual void add_front(const auto_count_ptr<ri_slc_node>& c);
            virtual void add_back(const auto_count_ptr<ri_slc_node>& c);
            virtual void add_child(const auto_count_ptr<ri_slc_node>& c);

        public:
            virtual size_t get_children_size() const;
            virtual const std::shared_ptr<ri_slc_node>& get_child_at(size_t i) const;
            virtual std::vector<std::shared_ptr<ri_slc_node> > clone_children() const;

        public:
            virtual bool set(const char* key, const ri_slc_parameters::value_type& val);
            virtual bool set(const char* key, int val);
            virtual bool set(const char* key, float val);
            virtual bool set(const char* key, const char* val);
            virtual bool set(const char* key, const int* val, int n);
            virtual bool set(const char* key, const float* val, int n);
            virtual bool set(const char* key, const LPCSTR* val, int n);
            virtual bool set(const char* key, const std::string& val);
            virtual bool set(const char* key, const std::string* val, int n);

        public:
            virtual const ri_slc_parameters::value_type* get(const char* key) const;

        protected:
            std::string type_;
            ri_slc_parameters* params_;
            std::vector<std::shared_ptr<ri_slc_node> > children_;
        };
    }
}

#endif
