#ifndef KAZE_RI_SLC_COMPILE_H
#define KAZE_RI_SLC_COMPILE_H

namespace kaze
{
    namespace ri
    {

        int ri_slc_compile(const char* in, const char* out);
    }
}

#endif