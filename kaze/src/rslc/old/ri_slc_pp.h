#ifndef KAZE_RI_SLC_PP_H
#define KAZE_RI_SLC_PP_H

namespace kaze
{
    namespace ri
    {

        int ri_slc_pp(const char* szIn, const char* szOut);
    }
}

#endif