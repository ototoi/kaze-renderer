#ifndef KAZE_RI_RSL_CONTEXT_H
#define KAZE_RI_RSL_CONTEXT_H

#include "count_ptr.hpp"
#include "ri_slc_node.h"

namespace kaze
{
    namespace ri
    {

        class ri_rsl_context
        {
        public:
            virtual ~ri_rsl_context() {}
            virtual void set_ast_node(const auto_count_ptr<ri_slc_node>& n) { node_ = n; }
        private:
            std::shared_ptr<ri_slc_node> node_;
        };
    }
}

#endif
