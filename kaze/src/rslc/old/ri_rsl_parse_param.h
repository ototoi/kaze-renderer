    #ifndef KAZE_RI_RSL_PARSE_PARAM_H
    #define KAZE_RI_RSL_PARSE_PARAM_H

    #include <string>

    namespace kaze
    {
        namespace ri
        {

            class ri_rsl_context;
            class ri_rsl_decoder;

            typedef struct ri_rsl_parse_param
            {
                ri_rsl_context* ctx;
                ri_rsl_decoder* dec;

                std::string filename;
                int line_number;

                void scan_begin();
                void scan_end();
            } ri_rsl_parse_param;
        }
    }

    #endif
