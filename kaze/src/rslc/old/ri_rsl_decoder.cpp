#include "ri_rsl_decoder.h"

namespace kaze
{
    namespace ri
    {

        ri_rsl_decoder::ri_rsl_decoder(FILE* fp)
        {
            fp_ = fp;
        }

        ri_rsl_decoder::~ri_rsl_decoder()
        {
            ; //
        }

        int ri_rsl_decoder::read(char* buffer, unsigned int size)
        {
            return fread(buffer, 1, (size_t)size, fp_);
        }
    }
}
