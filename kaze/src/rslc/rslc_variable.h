#ifndef RSLC_VARIABLE_H
#define RSLC_VARIABLE_H

#include <vector>
#include <string>

#include "rslc_type.h"
#include "rslc_variable.h"

namespace rslc
{
    class rslc_variable
    {
    public:
        rslc_variable(const rslc_type& type, const std::string& name)
            :type_(type), name_(name)
        {}
        const rslc_type& get_type()const
        {
            return type_;
        }
        void set_type(const rslc_type& type)
        {
            type_ = type;
        }
        const std::string& get_name()const
        {
            return name_;
        }
        void set_name(const std::string& name)
        {
            name_ = name;
        }
    private:
        rslc_type type_;
        std::string name_;
    };
}

#endif