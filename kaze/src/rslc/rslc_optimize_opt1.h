#ifndef RSLC_OPTIMIZE_OPT1_H
#define RSLC_OPTIMIZE_OPT1_H

namespace rslc
{
    class rslc_context;
    class rslc_node;
    int rslc_optimize_func_args(rslc_context* ctx, rslc_node* node);
}

#endif
