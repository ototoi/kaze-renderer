#ifndef RSLC_TEXTURENAME_H
#define RSLC_TEXTURENAME_H

#include <string>

namespace rslc
{
    class rslc_texturename
    {
    public:
        explicit rslc_texturename(const std::string& v, int i)
            :v_(v), i_(i)
        {}
        const std::string& get_name()const{return v_;}
        int get_index()const{return i_;}
    private:
        std::string v_;
        int i_;
    };
}

#endif