#ifndef RSLC_NODE_PRINT_CLIKE_H
#define RSLC_NODE_PRINT_CLIKE_H

#include <iostream>

namespace rslc
{
    class rslc_node;
    void rslc_write_clike(std::ostream& os, const rslc_node* node);
}

#endif