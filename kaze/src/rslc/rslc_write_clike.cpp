#include "rslc_write_clike.h"
#include "rslc_node.h"



#define CAST(T, n) check_cast<T>(n)

namespace rslc
{
    template<class T, class B>
    T check_cast(B b)
    {
        T t = dynamic_cast<T>(b);
        if(t == NULL)
        {
            std::cerr << "error" << std::endl;
        }
        return t;
    }

    static
    std::string tabs(int n)
    {
        typedef const char* SZCS;
#if 1
        static 
        SZCS szTABLE[] = 
        {
            "",
            "    ",
            "        ",
            "            ",
            "                ",
            "                    ",
            "                        ",
            "                            ",
            "                                ",
            "                                    ",
            "                                        ",
        };

        if(n < 10)
        {
            return szTABLE[n];
        }
        else
        {
            static char buffer[512] = {};
            for(int i=0;i<n;i++)
            {
                for(int j=0;j<4;j++)
                {
                    buffer[4*i+j] = ' ';
                }
            }
            buffer[4*n] = 0;
            return buffer;
        }
#else
        static 
        SZCS szTABLE[] = 
        {
            "",
            "\t",
            "\t\t",
            "\t\t\t",
            "\t\t\t\t",
            "\t\t\t\t\t",
            "\t\t\t\t\t\t",
            "\t\t\t\t\t\t\t",
            "\t\t\t\t\t\t\t\t",
            "\t\t\t\t\t\t\t\t\t",
            "\t\t\t\t\t\t\t\t\t\t",
        };
        if(n < 10)
        {
            return szTABLE[n];
        }
        else
        {
            static char buffer[512] = {};
            for(int i=0;i<n;i++)
            {
                buffer[i] = '\t';
            }
            buffer[n] = 0;
            return buffer;
        }
#endif
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_node* node);

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_identifier_node* node)
    {
        os << tabs(nTab) << (const std::string&)(node->get_value());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_integer_node* node)
    {
        os << tabs(nTab) << (int)(node->get_value());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_float_node* node)
    {
        os << tabs(nTab) << (float)(node->get_value());
    }   

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_string_node* node)
    {
        os << tabs(nTab) << "\"" << (const std::string&)(node->get_value()) << "\"";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_texturename_node* node)
    {
        rslc_texturename tex = node->get_value();
        os << tabs(nTab) << "\"" << (const std::string&)(tex.get_name()) << "\"";
        int ch = tex.get_index();
        if(ch >= 0)
        {
            os << "[" << ch << "]";
        }
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_list_node* node)
    {
        int sz = node->get_nodes_size();
        os << tabs(nTab) << "{";
        for(int i = 0;i<sz;i++)
        {
            rslc_write(0, os, node->get_node_at(i) );
            if(i != sz-1)
            {
                os << ", ";
            }
        }
        os << "}";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_tuple_node* node)
    {
        os << tabs(nTab) << "tuple(";
        int sz = node->get_nodes_size();
        for(int i = 0;i<sz;i++)
        {
            rslc_write(0, os, node->get_node_at(i) );
            if(i != sz-1)
            {
                os << ", ";
            }
        }
        os << ")";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_type_node* node)
    {
        os << tabs(nTab);
        os << (const std::string&)((node->get_type()).get_name());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_variable_def_node* node)
    {
        std::string name = (const std::string&)node->get_name();
        os << tabs(nTab) << name;
        const rslc_node* dnode = node->get_definition_node();
        if(dnode != NULL)
        {
            os << " = ";
            rslc_write(0, os, dnode);
        }
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_variable_array_def_node* node)
    {
        std::string name = (const std::string&)node->get_name();
        int size = node->get_nodes_size();
        os << tabs(nTab) << name << "[" << size << "]";
        const rslc_node* dnode = node->get_definition_node();
        if(dnode != NULL)
        {
            os << " = ";
            rslc_write(0, os, dnode);
        }
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_type_variables_def_node* node)
    {
        const rslc_type_variables_def_node* tvnode = node;
        const rslc_type_node* tnode = CAST(const rslc_type_node*, tvnode->get_type_node());
        const rslc_list_node* vsnode = CAST(const rslc_list_node*, tvnode->get_variables_node());
        std::string type = tnode->get_type().get_name();
        os << tabs(nTab) << type << " ";
        size_t jsz = vsnode->get_nodes_size();
        for(size_t j=0;j<jsz;j++)
        {
            const rslc_node* vnode = vsnode->get_node_at(j);
            int nType = vnode->get_node_type();
            if(nType == RSLC_NODE_VARIABLE_DEF)
            {
                rslc_write(0, os, CAST(const rslc_variable_def_node*,vnode));
            }
            else if(nType == RSLC_NODE_VARIABLE_ARRAY_DEF)
            {
                rslc_write(0, os, CAST(const rslc_variable_array_def_node*,vnode));
            }
            if(j != jsz-1)
            {
                os << ", ";
            }
        }
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_variable_ref_node* node)
    {
        std::string name = (const std::string&)node->get_name();
        os << tabs(nTab) << name;
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_variable_array_ref_node* node)
    {
        std::string name = (const std::string&)node->get_name();
        os << tabs(nTab) << name << "[";
        rslc_write(0, os, node->get_index_node());
        os << "]"; 
    }

    static 
    void rslc_write_args(std::ostream& os, const rslc_list_node* node)
    {
        int sz = (int)node->get_nodes_size();
        for(int i = 0;i<sz;i++)
        {
            rslc_write(0, os, node->get_node_at(i));
            if(i != sz-1)
            {
                os << ", ";
            }
        }
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_function_call_node* node)
    {
        std::string name = (const std::string&)(node->get_name());
        os << tabs(nTab) << name << "(";
           rslc_write_args(os, (const rslc_list_node*) node->get_arguments_node() );
        os << ")";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_neg_node* node)
    {
        os << tabs(nTab);
        os << "-";
        //os << "(";
        rslc_write(0, os, node->get_rhs_node());
        //os << ")";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_not_node* node)
    {
        os << tabs(nTab);
        os << "!";
        //os << "(";
        rslc_write(0, os, node->get_rhs_node());
        //os << ")";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_add_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " + ";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_sub_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " - ";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_mul_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << "*";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_div_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << "/";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_crs_node* node)
    {
        os << tabs(nTab);
        os << "cross(";
        rslc_write(0, os, node->get_lhs_node());
        os << ", ";
        rslc_write(0, os, node->get_rhs_node());
        os << ")";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_dot_node* node)
    {
        os << tabs(nTab);
        os << "dot(";
        rslc_write(0, os, node->get_lhs_node());
        os << ", ";
        rslc_write(0, os, node->get_rhs_node());
        os << ")";
    }

    //---------------------------
    static
    void rslc_write(int nTab, std::ostream& os, const rslc_lss_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " < ";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_leq_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " <= ";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_gtr_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " > ";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_geq_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " >= ";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_eq_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " == ";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_ne_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " != ";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_and_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " && ";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_or_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " || ";
        rslc_write(0, os, node->get_rhs_node());
    }

    //---------------------------

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_assign_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " = ";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_add_assign_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " += ";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_sub_assign_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " -= ";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_mul_assign_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " *= ";
        rslc_write(0, os, node->get_rhs_node());
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_div_assign_node* node)
    {
        os << tabs(nTab);
        rslc_write(0, os, node->get_lhs_node());
        os << " /= ";
        rslc_write(0, os, node->get_rhs_node());
    }

    //---------------------------

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_cond_node* node)
    {
        os << tabs(nTab);
        //os << "(";
        rslc_write(0, os, node->get_condition_node());
        //os << ")";
        os << " ? ";
        //os << "(";
        rslc_write(0, os, node->get_expression1_node());
        //os << ")";
        os << " : ";
        //os << "(";
        rslc_write(0, os, node->get_expression2_node());
        //os << ")";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_cast_node* node)
    {
        std::string type = (const std::string&)(node->get_type_node()->get_type().get_name());
        std::string ns = node->get_namespace();
        if(ns == "")
        {
            os << tabs(nTab) << type << "(";
            rslc_write(0, os, node->get_expression_node());
            os << ")";
        }
        else
        {
            os << tabs(nTab) << type << "(" << "\"" << ns << "\"" << ", ";
            rslc_write(0, os, node->get_expression_node());
            os << ")";
        }
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_paren_node* node)
    {
        os << tabs(nTab) << "(";
        rslc_write(0, os, node->get_contents_node());
        os << ")";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_statement_node* node)
    {
        rslc_write(nTab, os, node->get_contents_node());
        os << ";\n";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_block_node* node)
    {
        os << tabs(nTab) << "{\n";
        int sz = node->get_nodes_size();
        for(int i = 0;i<sz;i++)
        {
            rslc_write(nTab+1, os, node->get_node_at(i) );
        }
        os << tabs(nTab) << "}\n";
    }
    
    //---------------------------

    static
    void rslc_write_condition(int nTab, std::ostream& os, const rslc_list_node* node)
    {
        os << tabs(nTab);
        int sz = node->get_nodes_size();
        for(int i = 0;i<sz;i++)
        {
            rslc_write(0, os, node->get_node_at(i) );
            if(i != sz-1)
            {
                os << "; ";
            }
        }
    }

    static
    void rslc_write_block(int nTab, std::ostream& os, const rslc_node* node)
    {
        if(node->get_node_type() == RSLC_NODE_BLOCK)
        {
            const rslc_block_node* l = CAST(const rslc_block_node*, node);
            os << tabs(nTab) << "{\n";
            size_t sz = l->get_nodes_size();
            for(size_t i = 0;i<sz;i++)
            {
                rslc_write(nTab+1, os, l->get_node_at(i));
            }
            os << tabs(nTab) << "}";
        }
        else
        {
            os << tabs(nTab) << "{\n";
            rslc_write(nTab+1, os, node);
            os << tabs(nTab) << "}";
        }
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_while_node* node)
    {
        os << tabs(nTab);
        os << "while" << "(";
        rslc_write_condition(0, os, node->get_condition_node());
        os << ")";
        os << "\n";
        const rslc_node* c = node->get_contents_node();
        rslc_write_block(nTab, os, c);
        os << "\n";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_for_node* node)
    {
        os << tabs(nTab);
        os << "for" << "(";
        rslc_write_condition(0, os, node->get_condition_node());
        os << ")";
        os << "\n";
        const rslc_node* c = node->get_contents_node();
        rslc_write_block(nTab, os, c);
        os << "\n";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_solar_node* node)
    {
        os << tabs(nTab);
        os << "solar" << "(";
        rslc_write_condition(0, os, node->get_condition_node());
        os << ")";
        os << "\n";
        const rslc_node* c = node->get_contents_node();
        rslc_write_block(nTab, os, c);
        os << "\n";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_illuminate_node* node)
    {
        os << tabs(nTab);
        os << "illuminate" << "(";
        rslc_write_condition(0, os, node->get_condition_node());
        os << ")";
        os << "\n";
        const rslc_node* c = node->get_contents_node();
        rslc_write_block(nTab, os, c);
        os << "\n";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_illuminance_node* node)
    {
        os << tabs(nTab);
        os << "illuminance" << "(";
        rslc_write_condition(0, os, node->get_condition_node());
        os << ")";
        os << "\n";
        const rslc_node* c = node->get_contents_node();
        rslc_write_block(nTab, os, c);
        os << "\n";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_gather_node* node)
    {
        os << tabs(nTab);
        os << "gather" << "(";
        rslc_write_condition(0, os, node->get_condition_node());
        os << ")";
        os << "\n";
        const rslc_node* c = node->get_contents_node();
        rslc_write_block(nTab, os, c);
        os << "\n";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_if_node* node)
    {
        os << tabs(nTab);
        os << "if" << "(";
        rslc_write(0, os, node->get_condition_node());
        os << ")";
        os << "\n";
        const rslc_node* c = node->get_contents_node();
        const rslc_node* e = node->get_else_node();
        if(!c && !e)
        {
            os << ";\n";
        }
        else if(c && !e)
        {
            rslc_write_block(nTab, os, c);
            os << "\n";
        }
        else
        {
            rslc_write_block(nTab, os, c);
            os << "\n";
            os << tabs(nTab) << "else\n";
            rslc_write_block(nTab, os, e);
            os << "\n";
        }
    }

    //---------------------------

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_return_node* node)
    {
        os << tabs(nTab) << "return";
        if(node->get_expression_node())
        {
            os << " ";
            rslc_write(0, os, node->get_expression_node());
        }
        os << ";\n";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_break_node* node)
    {
        os << tabs(nTab) << "break;\n";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_continue_node* node)
    {
        os << tabs(nTab) << "continue;\n";
    }

    //---------------------------

    static
    void rslc_param_print(int nTab, std::ostream& os, const rslc_list_node* pnode, bool bBr = false)
    {
        size_t sz = pnode->get_nodes_size();
        for(size_t i=0;i<sz;i++)
        {
            const rslc_type_variables_def_node* tvnode = CAST(const rslc_type_variables_def_node*, pnode->get_node_at(i));
            const rslc_type_node* tnode = CAST(const rslc_type_node*, tvnode->get_type_node());
            const rslc_list_node* vsnode = CAST(const rslc_list_node*, tvnode->get_variables_node());
            std::string type = tnode->get_type().get_name();
            os << tabs(nTab) << type << " ";
            size_t jsz = vsnode->get_nodes_size();
            for(size_t j=0;j<jsz;j++)
            {
                const rslc_node* vnode = vsnode->get_node_at(j);
                int nType = vnode->get_node_type();
                if(nType == RSLC_NODE_VARIABLE_DEF)
                {
                    const rslc_variable_def_node* vvnode = CAST(const rslc_variable_def_node*,vnode);
                    std::string name = (const std::string&)vvnode->get_name();
                    os << name;
                    const rslc_node* dnode = vvnode->get_definition_node();
                    if(dnode != NULL)
                    {
                        os << " = ";
                        rslc_write(0, os, dnode);
                    }
                }
                else if(nType == RSLC_NODE_VARIABLE_ARRAY_DEF)
                {
                    const rslc_variable_array_def_node* vvnode = CAST(const rslc_variable_array_def_node*,vnode);
                    std::string name = (const std::string&)vvnode->get_name();
                    int size = vvnode->get_size();
                    os << name << "[" << size << "]";
                    const rslc_node* dnode = vvnode->get_definition_node();
                    if(dnode != NULL)
                    {
                        os << " = ";
                        rslc_write(0, os, dnode);
                    }
                }
                if(i != sz-1)
                {
                    os << ", ";
                }
            }
            if(bBr)
            {
                os << "\n";
            }
        }
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_shader_def_node* node)
    {
        const rslc_type_node* tnode = CAST(const rslc_type_node*, node->get_type_node());
        std::string type = tnode->get_type().get_name();
        std::string name = node->get_name();
        os << tabs(nTab) << "//" << type << "\n";

        {
            const rslc_list_node* pnode = CAST(const rslc_list_node*, node->get_arguments_node());
            if(pnode->get_nodes_size())
            {
                os << tabs(nTab) << "void" << " " << name << "(";
                os << "\n";
                rslc_param_print(nTab+1, os, pnode, true);
                os << tabs(nTab) << ")" << "\n";
            }
            else
            {
                os << tabs(nTab) << "void" << " " << name << "()";
                os << "\n";
            }
        }

        os << tabs(nTab) << "{\n";

        {
            const rslc_list_node* pnode = CAST(const rslc_list_node*, node->get_contents_node());
            size_t sz = pnode->get_nodes_size();
            for(size_t i = 0;i<sz; i++)
            {
                rslc_write(nTab+1, os, pnode->get_node_at(i));
            }
        }

        os << tabs(nTab) << "}\n";
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_function_def_node* node)
    {
        const rslc_type_node* tnode = CAST(const rslc_type_node*, node->get_type_node());
        std::string type = tnode->get_type().get_name();
        std::string name = node->get_name();
        os << tabs(nTab) << "//" << "user defined function" << "\n";
        os << tabs(nTab) << type << " " << name << "(";
        
        {
            const rslc_list_node* pnode = CAST(const rslc_list_node*, node->get_arguments_node());
            rslc_param_print(0, os, pnode, false);
        }

        os << ")" << "\n";
        os << tabs(nTab) << "{\n";
            
        {
            const rslc_list_node* pnode = CAST(const rslc_list_node*, node->get_contents_node());
            size_t sz = pnode->get_nodes_size();
            for(size_t i = 0;i<sz; i++)
            {
                rslc_write(nTab+1, os, pnode->get_node_at(i));
            }
        }

        os << tabs(nTab) << "}\n";
    }

    //---------------------------

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_definitions_node* node)
    {
        os << "//---------------------------------" << "\n";
        os << "//\"Generator\" : \"KzRSLCompiler\"" << "\n";
        os << "//\"GeneratorVersion\" : " << "1.0" << "\n";
        os << "//\"CodeType\" : " << "\"clike\"" << "\n";
        os << "//\"GenerateDate\" : " << 12345678 << "\n";
        os << "//---------------------------------" << "\n";
        os << "\n";
        int sz = node->get_nodes_size();
        for(int i = 0;i<sz;i++)
        {
            rslc_write(nTab, os, node->get_node_at(i) );
            os << tabs(nTab) << "\n";
        }
    }

    static
    void rslc_write(int nTab, std::ostream& os, const rslc_node* node)
    {
        int nType = node->get_node_type();
        switch(nType)
        {
            case RSLC_NODE_IDENTIFIER:
                rslc_write(nTab, os, CAST(const rslc_identifier_node*,node));break;
            case RSLC_NODE_INTEGER:
                rslc_write(nTab, os, CAST(const rslc_integer_node*,node));break;
            case RSLC_NODE_FLOAT:
                rslc_write(nTab, os, CAST(const rslc_float_node*,node));break;
            case RSLC_NODE_STRING:
                rslc_write(nTab, os, CAST(const rslc_string_node*,node));break;
            case RSLC_NODE_TEXTURENAME:
                rslc_write(nTab, os, CAST(const rslc_texturename_node*,node));break;

            case RSLC_NODE_LIST:
                rslc_write(nTab, os, CAST(const rslc_list_node*,node));break;
            case RSLC_NODE_TUPLE:
                rslc_write(nTab, os, CAST(const rslc_tuple_node*,node));break;
            
            case RSLC_NODE_TYPE:
                rslc_write(nTab, os, CAST(const rslc_type_node*,node));break;
            case RSLC_NODE_VARIABLE_REF:
                rslc_write(nTab, os, CAST(const rslc_variable_ref_node*,node));break;
            case RSLC_NODE_VARIABLE_ARRAY_REF:
                rslc_write(nTab, os, CAST(const rslc_variable_array_ref_node*,node));break;
            case RSLC_NODE_FUNCTION_CALL:
                rslc_write(nTab, os, CAST(const rslc_function_call_node*,node));break;
            
            case RSLC_NODE_NEG:
                rslc_write(nTab, os, CAST(const rslc_neg_node*,node));break;
            case RSLC_NODE_NOT:
                rslc_write(nTab, os, CAST(const rslc_not_node*,node));break;
            case RSLC_NODE_ADD:
                rslc_write(nTab, os, CAST(const rslc_add_node*,node));break;
            case RSLC_NODE_SUB:
                rslc_write(nTab, os, CAST(const rslc_sub_node*,node));break;
            case RSLC_NODE_MUL:
                rslc_write(nTab, os, CAST(const rslc_mul_node*,node));break;
            case RSLC_NODE_DIV:
                rslc_write(nTab, os, CAST(const rslc_div_node*,node));break;
            case RSLC_NODE_CRS:
                rslc_write(nTab, os, CAST(const rslc_crs_node*,node));break;
            case RSLC_NODE_DOT:
                rslc_write(nTab, os, CAST(const rslc_dot_node*,node));break;

            case RSLC_NODE_LSS:
                rslc_write(nTab, os, CAST(const rslc_lss_node*,node));break;
            case RSLC_NODE_LEQ:
                rslc_write(nTab, os, CAST(const rslc_leq_node*,node));break;
            case RSLC_NODE_GTR:
                rslc_write(nTab, os, CAST(const rslc_gtr_node*,node));break;
            case RSLC_NODE_GEQ:
                rslc_write(nTab, os, CAST(const rslc_geq_node*,node));break;
            case RSLC_NODE_EQ:
                rslc_write(nTab, os, CAST(const rslc_eq_node*,node));break;
            case RSLC_NODE_NE:
                rslc_write(nTab, os, CAST(const rslc_ne_node*,node));break;

            case RSLC_NODE_AND:
                rslc_write(nTab, os, CAST(const rslc_and_node*,node));break;
            case RSLC_NODE_OR:
                rslc_write(nTab, os, CAST(const rslc_or_node*,node));break;

            case RSLC_NODE_ASSIGN:
                rslc_write(nTab, os, CAST(const rslc_assign_node*,node));break;
            case RSLC_NODE_ADD_ASSIGN:
                rslc_write(nTab, os, CAST(const rslc_add_assign_node*,node));break;
            case RSLC_NODE_SUB_ASSIGN:
                rslc_write(nTab, os, CAST(const rslc_sub_assign_node*,node));break;
            case RSLC_NODE_MUL_ASSIGN:
                rslc_write(nTab, os, CAST(const rslc_mul_assign_node*,node));break;
            case RSLC_NODE_DIV_ASSIGN:
                rslc_write(nTab, os, CAST(const rslc_div_assign_node*,node));break;

            case RSLC_NODE_COND:
                rslc_write(nTab, os, CAST(const rslc_cond_node*,node));break;
            case RSLC_NODE_CAST:
                rslc_write(nTab, os, CAST(const rslc_cast_node*,node));break;

            case RSLC_NODE_STATEMENT:
                rslc_write(nTab, os, CAST(const rslc_statement_node*,node));break;

            case RSLC_NODE_BLOCK:
                rslc_write(nTab, os, CAST(const rslc_block_node*,node));break;

            case RSLC_NODE_WHILE:
                rslc_write(nTab, os, CAST(const rslc_while_node*,node));break;
            case RSLC_NODE_FOR:
                rslc_write(nTab, os, CAST(const rslc_for_node*,node));break;
            case RSLC_NODE_SOLAR:
                rslc_write(nTab, os, CAST(const rslc_solar_node*,node));break;
            case RSLC_NODE_ILLUMINATE:
                rslc_write(nTab, os, CAST(const rslc_illuminate_node*,node));break;
            case RSLC_NODE_ILLUMINANCE:
                rslc_write(nTab, os, CAST(const rslc_illuminance_node*,node));break;
            case RSLC_NODE_GATHER:
                rslc_write(nTab, os, CAST(const rslc_gather_node*,node));break;

            case RSLC_NODE_IF:
                rslc_write(nTab, os, CAST(const rslc_if_node*,node));break;

            case RSLC_NODE_RETURN:
                rslc_write(nTab, os, CAST(const rslc_return_node*,node));break;
            case RSLC_NODE_BREAK:
                rslc_write(nTab, os, CAST(const rslc_break_node*,node));break;
            case RSLC_NODE_CONTINUE:
                rslc_write(nTab, os, CAST(const rslc_continue_node*,node));break;
     
            case RSLC_NODE_VARIABLE_DEF:
                rslc_write(nTab, os, CAST(const rslc_variable_def_node*,node));break;
            case RSLC_NODE_VARIABLE_ARRAY_DEF:
                rslc_write(nTab, os, CAST(const rslc_variable_array_def_node*,node));break;
            case RSLC_NODE_TYPE_VARIABLES_DEF:
                rslc_write(nTab, os, CAST(const rslc_type_variables_def_node*,node));break;

            case RSLC_NODE_FUNCTION_DEF:
                rslc_write(nTab, os, CAST(const rslc_function_def_node*,node));break;
            case RSLC_NODE_SHADER_DEF:
                rslc_write(nTab, os, CAST(const rslc_shader_def_node*,node));break;

            case RSLC_NODE_DEFINITIONS:
                rslc_write(nTab, os, CAST(const rslc_definitions_node*,node));break;

            case RSLC_NODE_PAREN:
                rslc_write(nTab, os, CAST(const rslc_paren_node*,node));break;
            
            default:
                break;
        }
    }
    
    void rslc_write_clike(std::ostream& os, const rslc_node* node)
    {
        rslc_write(0, os, node);
        os << "\n";
    }

}
