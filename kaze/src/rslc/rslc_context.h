#ifndef RSLC_CONTEXT_H
#define RSLC_CONTEXT_H

#include "rslc_node_manager.h"

namespace rslc
{
    class rslc_context 
        : public rslc_node_manager
    {
    public:
        rslc_context();
        ~rslc_context();
    public:
        void set_root_node(rslc_node* root);
        rslc_node* get_root_node()const;
        bool is_debug()const;
        void print_debug(const char* szMsg);
        void print_error(const char* szMsg);
    protected:
        rslc_node* root_;
    };
}

#endif
