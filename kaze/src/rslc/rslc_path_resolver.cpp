#include "rslc_path_resolver.h"

#ifdef _WIN32
#include <windows.h>
#else
#include <libgen.h>
#include <stdlib.h>
#endif

#ifdef __APPLE__
#include <mach-o/dyld.h>
#endif

#if defined(__linux__) || defined(FREEBSD)
#include <unistd.h>
#endif

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

namespace rslc
{
    static std::string ri_get_full_path(const std::string& path)
    {
#if defined(_WIN32)
        char szFullPath[_MAX_PATH];
        _fullpath(szFullPath, path.c_str(), _MAX_PATH);
        return szFullPath;
#else
        char szFullPath[1024];
        realpath(path.c_str(), szFullPath);
        return szFullPath;
#endif
    }

    static std::string ri_get_exec_path_()
    {
#if defined(_WIN32)
        char buffer[_MAX_PATH];
        ::GetModuleFileNameA(NULL, buffer, _MAX_PATH);
        return buffer;
#elif defined(__APPLE__)
        char path[1024] = {};
        uint32_t size = sizeof(path);
        _NSGetExecutablePath(path, &size);
        return ri_get_full_path(path);
#elif defined(__linux__)
        char path[1024] = {};
        size_t size = sizeof(path);
        readlink("/proc/self/exe", path, size);
        return path;
#elif defined(FREEBSD)
        char path[1024] = {};
        size_t size = sizeof(path);
        readlink("/proc/curproc/file", path, size);
        return path;
#else
        return "";
#endif
    }

    static std::string dirname_(const std::string& path)
    {
        return path.substr(0, path.find_last_of('/'));
    }

    static std::string ri_get_bin_path_()
    {
#ifdef _WIN32
        static std::string strExec = ri_get_exec_path_();
        char szDrive[_MAX_DRIVE];
        char szDir[_MAX_DIR];
        _splitpath(strExec.c_str(), szDrive, szDir, NULL, NULL);
        std::string strRet1;
        strRet1 += szDrive;
        strRet1 += szDir;
        return ri_get_full_path(strRet1);
#else
        static std::string strExec = ri_get_exec_path_();
        static std::string strDir = dirname_(strExec);
        std::string strRet1;
        strRet1 = strDir;
        strRet1 += "/";
        return ri_get_full_path(strRet1) + "/";
#endif
    }

    static std::string replace_ext_(const std::string& path, const std::string& ext)
    {
#ifdef _WIN32
        char szDrive[_MAX_DRIVE];
        char szDir[_MAX_DIR];
        char szFname[_MAX_FNAME];
        //char szExt[_MAX_EXT];
        _splitpath(path.c_str(), szDrive, szDir, szFname, NULL);

        std::string strRet;
        strRet += szDrive;
        strRet += szDir;
        strRet += szFname;
        strRet += ext;

        return strRet;
#else
        std::string strRet;
        strRet += path.substr(0, path.find_last_of('.') - 1);
        strRet += ext;
        return strRet;
#endif
    }

    std::string rslc_path_resolver::get_bin_path()
    {
        static std::string strRet = ri_get_bin_path_();
        return strRet;
    }

    std::string rslc_path_resolver::replace_ext(const std::string& s, const std::string& ext)
    {
        return replace_ext_(s, ext);
    }

    std::string rslc_path_resolver::get_full_path(const std::string& s)
    {
        return ri_get_full_path(s);
    }
}
