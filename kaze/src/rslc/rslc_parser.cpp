/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison LALR(1) parsers in C++

   Copyright (C) 2002, 2003, 2004, 2005, 2006 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


#include "rslc_parser.hpp"

/* User implementation prologue.  */
#line 44 "rslc_parser.yy"


#define yyerror(m) error(yylloc,m)

using namespace rslc;

static
void release(const char* v){delete[] v;v=NULL;}

static
rslc_list_node* create_list_node(rslc_context* ctx)
{
	std::shared_ptr<rslc_node> l(new rslc_list_node());
	ctx->add_node(l);
	return (rslc_list_node*)(l.get());
}

#define GET_CTX() (((rslc_parse_param*)(scanner))->ctx)
#define GET_FNAME() (((rslc_parse_param*)(scanner))->filename)
#define GET_LINE() (((rslc_parse_param*)(scanner))->line_number)
#define GET_LOC() (rslc_location(GET_FNAME(),GET_LINE(),0))
#define DEBUG(s)  (GET_CTX()->print_debug(s))
#define MAKE_LIST()  (create_list_node(GET_CTX()))



#define	YY_DECL	1

extern int _rslc_lex(yy::rslc_parser::semantic_type* yylval,
	 yy::rslc_parser::location_type* yylloc,
	 void* scanner);

static
int yylex(yy::rslc_parser::semantic_type* yylval,
	 yy::rslc_parser::location_type* yylloc,
	 void* scanner)
{
	return _rslc_lex(yylval, yylloc, scanner);
}




/* Line 317 of lalr1.cc.  */
#line 85 "rslc_parser.cpp"

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* FIXME: INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#define YYUSE(e) ((void) (e))

/* A pseudo ostream that takes yydebug_ into account.  */
# define YYCDEBUG							\
  for (bool yydebugcond_ = yydebug_; yydebugcond_; yydebugcond_ = false)	\
    (*yycdebug_)

/* Enable debugging if requested.  */
#if YYDEBUG

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)	\
do {							\
  if (yydebug_)						\
    {							\
      *yycdebug_ << Title << ' ';			\
      yy_symbol_print_ ((Type), (Value), (Location));	\
      *yycdebug_ << std::endl;				\
    }							\
} while (false)

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug_)				\
    yy_reduce_print_ (Rule);		\
} while (false)

# define YY_STACK_PRINT()		\
do {					\
  if (yydebug_)				\
    yystack_print_ ();			\
} while (false)

#else /* !YYDEBUG */

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_REDUCE_PRINT(Rule)
# define YY_STACK_PRINT()

#endif /* !YYDEBUG */

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab

namespace yy
{
#if YYERROR_VERBOSE

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
  rslc_parser::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr = "";
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              /* Fall through.  */
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }

#endif

  /// Build a parser object.
  rslc_parser::rslc_parser (void * scanner_yyarg)
    : yydebug_ (false),
      yycdebug_ (&std::cerr),
      scanner (scanner_yyarg)
  {
  }

  rslc_parser::~rslc_parser ()
  {
  }

#if YYDEBUG
  /*--------------------------------.
  | Print this symbol on YYOUTPUT.  |
  `--------------------------------*/

  inline void
  rslc_parser::yy_symbol_value_print_ (int yytype,
			   const semantic_type* yyvaluep, const location_type* yylocationp)
  {
    YYUSE (yylocationp);
    YYUSE (yyvaluep);
    switch (yytype)
      {
         default:
	  break;
      }
  }


  void
  rslc_parser::yy_symbol_print_ (int yytype,
			   const semantic_type* yyvaluep, const location_type* yylocationp)
  {
    *yycdebug_ << (yytype < yyntokens_ ? "token" : "nterm")
	       << ' ' << yytname_[yytype] << " ("
	       << *yylocationp << ": ";
    yy_symbol_value_print_ (yytype, yyvaluep, yylocationp);
    *yycdebug_ << ')';
  }
#endif /* ! YYDEBUG */

  void
  rslc_parser::yydestruct_ (const char* yymsg,
			   int yytype, semantic_type* yyvaluep, location_type* yylocationp)
  {
    YYUSE (yylocationp);
    YYUSE (yymsg);
    YYUSE (yyvaluep);

    YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

    switch (yytype)
      {
  
	default:
	  break;
      }
  }

  void
  rslc_parser::yypop_ (unsigned int n)
  {
    yystate_stack_.pop (n);
    yysemantic_stack_.pop (n);
    yylocation_stack_.pop (n);
  }

  std::ostream&
  rslc_parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  rslc_parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  rslc_parser::debug_level_type
  rslc_parser::debug_level () const
  {
    return yydebug_;
  }

  void
  rslc_parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }


  int
  rslc_parser::parse ()
  {
    /// Look-ahead and look-ahead in internal form.
    int yychar = yyempty_;
    int yytoken = 0;

    /* State.  */
    int yyn;
    int yylen = 0;
    int yystate = 0;

    /* Error handling.  */
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// Semantic value of the look-ahead.
    semantic_type yylval;
    /// Location of the look-ahead.
    location_type yylloc;
    /// The locations where the error started and ended.
    location yyerror_range[2];

    /// $$.
    semantic_type yyval;
    /// @$.
    location_type yyloc;

    int yyresult;

    YYCDEBUG << "Starting parse" << std::endl;


    /* Initialize the stacks.  The initial state will be pushed in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystate_stack_ = state_stack_type (0);
    yysemantic_stack_ = semantic_stack_type (0);
    yylocation_stack_ = location_stack_type (0);
    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yylloc);

    /* New state.  */
  yynewstate:
    yystate_stack_.push (yystate);
    YYCDEBUG << "Entering state " << yystate << std::endl;
    goto yybackup;

    /* Backup.  */
  yybackup:

    /* Try to take a decision without look-ahead.  */
    yyn = yypact_[yystate];
    if (yyn == yypact_ninf_)
      goto yydefault;

    /* Read a look-ahead token.  */
    if (yychar == yyempty_)
      {
	YYCDEBUG << "Reading a token: ";
	yychar = yylex (&yylval, &yylloc, scanner);
      }


    /* Convert token to internal form.  */
    if (yychar <= yyeof_)
      {
	yychar = yytoken = yyeof_;
	YYCDEBUG << "Now at end of input." << std::endl;
      }
    else
      {
	yytoken = yytranslate_ (yychar);
	YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
      }

    /* If the proper action on seeing token YYTOKEN is to reduce or to
       detect an error, take that action.  */
    yyn += yytoken;
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yytoken)
      goto yydefault;

    /* Reduce or error.  */
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
	if (yyn == 0 || yyn == yytable_ninf_)
	goto yyerrlab;
	yyn = -yyn;
	goto yyreduce;
      }

    /* Accept?  */
    if (yyn == yyfinal_)
      goto yyacceptlab;

    /* Shift the look-ahead token.  */
    YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

    /* Discard the token being shifted unless it is eof.  */
    if (yychar != yyeof_)
      yychar = yyempty_;

    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yylloc);

    /* Count tokens shifted since error; after three, turn off error
       status.  */
    if (yyerrstatus_)
      --yyerrstatus_;

    yystate = yyn;
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystate];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    /* If YYLEN is nonzero, implement the default value of the action:
       `$$ = $1'.  Otherwise, use the top of the stack.

       Otherwise, the following line sets YYVAL to garbage.
       This behavior is undocumented and Bison
       users should not rely upon it.  */
    if (yylen)
      yyval = yysemantic_stack_[yylen - 1];
    else
      yyval = yysemantic_stack_[0];

    {
      slice<location_type, location_stack_type> slice (yylocation_stack_, yylen);
      YYLLOC_DEFAULT (yyloc, slice, yylen);
    }
    YY_REDUCE_PRINT (yyn);
    switch (yyn)
      {
	  case 2:
#line 221 "rslc_parser.yy"
    {
			GET_CTX()->set_root_node((yysemantic_stack_[(1) - (1)].ntype));
		;}
    break;

  case 3:
#line 227 "rslc_parser.yy"
    {
            std::shared_ptr<rslc_node> node(new rslc_definitions_node());
			GET_CTX()->add_node(node);
			((rslc_list_node*)(node.get()))->add_node((yysemantic_stack_[(1) - (1)].ntype));
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 4:
#line 235 "rslc_parser.yy"
    {
            ((rslc_list_node*)((yysemantic_stack_[(2) - (1)].ntype)))->add_node((yysemantic_stack_[(2) - (2)].ntype));
			(yyval.ntype) = (yysemantic_stack_[(2) - (1)].ntype);
		;}
    break;

  case 5:
#line 243 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("shader_definition");
			}
		;}
    break;

  case 6:
#line 252 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("function_definition");
			}
		;}
    break;

  case 7:
#line 264 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(8) - (2)].ntype)))->get_value();
			rslc_node* params = (yysemantic_stack_[(8) - (4)].ntype);
			rslc_node* contents = (yysemantic_stack_[(8) - (7)].ntype);
			std::shared_ptr<rslc_node> node(new rslc_shader_def_node((yysemantic_stack_[(8) - (1)].ntype), name, params, contents));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "shader_definition:";
				s += (const std::string&)(((rslc_identifier_node*)((yysemantic_stack_[(8) - (2)].ntype)))->get_value());
				DEBUG(s.c_str());
			}
		;}
    break;

  case 8:
#line 281 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(7) - (2)].ntype)))->get_value();
			rslc_node* params = (yysemantic_stack_[(7) - (4)].ntype);
			rslc_node* contents = NULL;

			{
				std::shared_ptr<rslc_node> n(new rslc_list_node());
				n->set_location(GET_LOC());
				GET_CTX()->add_node(n);
				contents = n.get();
			}	

			std::shared_ptr<rslc_node> node(new rslc_shader_def_node((yysemantic_stack_[(7) - (1)].ntype), name, params, contents));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "shader_definition:";
				s += (const std::string&)(((rslc_identifier_node*)((yysemantic_stack_[(7) - (2)].ntype)))->get_value());
				DEBUG(s.c_str());
			}
		;}
    break;

  case 9:
#line 306 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(7) - (2)].ntype)))->get_value();
			rslc_node* params = NULL;
			rslc_node* contents = (yysemantic_stack_[(7) - (6)].ntype);

			{
				std::shared_ptr<rslc_node> n(new rslc_list_node());
				n->set_location(GET_LOC());
				GET_CTX()->add_node(n);
				params = n.get();
			}

			std::shared_ptr<rslc_node> node(new rslc_shader_def_node((yysemantic_stack_[(7) - (1)].ntype), name, params, contents));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "shader_definition:";
				s += (const std::string&)(((rslc_identifier_node*)((yysemantic_stack_[(7) - (2)].ntype)))->get_value());
				DEBUG(s.c_str());
			}
		;}
    break;

  case 10:
#line 331 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(6) - (2)].ntype)))->get_value();
			rslc_node* params = NULL;
			rslc_node* contents = NULL;

			{
				std::shared_ptr<rslc_node> n(new rslc_list_node());
				n->set_location(GET_LOC());
				GET_CTX()->add_node(n);
				params = n.get();
			}

			{
				std::shared_ptr<rslc_node> n(new rslc_list_node());
				n->set_location(GET_LOC());
				GET_CTX()->add_node(n);
				contents = n.get();
			}

			std::shared_ptr<rslc_node> node(new rslc_shader_def_node((yysemantic_stack_[(6) - (1)].ntype), name, params, contents));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "shader_definition:";
				s += (const std::string&)(((rslc_identifier_node*)((yysemantic_stack_[(6) - (2)].ntype)))->get_value());
				DEBUG(s.c_str());
			}
		;}
    break;

  case 11:
#line 366 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("light"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 12:
#line 373 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("surface"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 13:
#line 380 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("volume"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 14:
#line 387 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("displacement"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 15:
#line 394 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("transformation"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 16:
#line 401 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("imager"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 17:
#line 411 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(8) - (2)].ntype)))->get_value();
			rslc_node* params = (yysemantic_stack_[(8) - (4)].ntype);
			std::shared_ptr<rslc_node> node(new rslc_function_def_node((yysemantic_stack_[(8) - (1)].ntype), name, params, (yysemantic_stack_[(8) - (7)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
		;}
    break;

  case 18:
#line 421 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(7) - (2)].ntype)))->get_value();
			std::shared_ptr<rslc_node> pnode(new rslc_list_node());
			GET_CTX()->add_node(pnode);
			rslc_node* params = pnode.get();
			std::shared_ptr<rslc_node> node(new rslc_function_def_node((yysemantic_stack_[(7) - (1)].ntype), name, params, (yysemantic_stack_[(7) - (6)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
		;}
    break;

  case 19:
#line 436 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_list_node());
			GET_CTX()->add_node(node);
			((rslc_list_node*)(node.get()))->add_node((yysemantic_stack_[(1) - (1)].ntype));
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
		;}
    break;

  case 20:
#line 445 "rslc_parser.yy"
    {
			((rslc_list_node*)((yysemantic_stack_[(3) - (1)].ntype)))->add_node((yysemantic_stack_[(3) - (3)].ntype));
			(yyval.ntype) = (yysemantic_stack_[(3) - (1)].ntype);
			;
		;}
    break;

  case 21:
#line 451 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(2) - (1)].ntype);
		;}
    break;

  case 22:
#line 458 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 23:
#line 462 "rslc_parser.yy"
    {
			rslc_type_variables_def_node* n0 = (rslc_type_variables_def_node*)(yysemantic_stack_[(2) - (2)].ntype);
			rslc_type_node* type = n0->get_type_node();
			type->add_attribute("output");
			(yyval.ntype) = n0;
		;}
    break;

  case 24:
#line 472 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_variables_def_node(((rslc_type_node*)(yysemantic_stack_[(2) - (1)].ntype)), ((rslc_list_node*)(yysemantic_stack_[(2) - (2)].ntype))));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definitions ',' variable_definition");
			}
		;}
    break;

  case 25:
#line 486 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_list_node());
			GET_CTX()->add_node(node);
			((rslc_list_node*)(node.get()))->add_node((yysemantic_stack_[(1) - (1)].ntype));
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definitions ',' variable_definition");
			}
		;}
    break;

  case 26:
#line 499 "rslc_parser.yy"
    {
			((rslc_list_node*)((yysemantic_stack_[(3) - (1)].ntype)))->add_node((yysemantic_stack_[(3) - (3)].ntype));
			(yyval.ntype) = (yysemantic_stack_[(3) - (1)].ntype);
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definitions ',' variable_definition");
			}
		;}
    break;

  case 27:
#line 512 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(1) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node(new rslc_variable_def_node(name));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definition:identifier");
			}
		;}
    break;

  case 28:
#line 525 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(3) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node(new rslc_variable_def_node(name, (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definition:identifier '=' expression");
			}
		;}
    break;

  case 29:
#line 538 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(4) - (1)].ntype)))->get_value();
			int size = (yysemantic_stack_[(4) - (3)].itype);
			std::shared_ptr<rslc_node> node(new rslc_variable_array_def_node(name, size));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definition:identifier '[' INTEGER_LITERAL ']'");
			}
		;}
    break;

  case 30:
#line 552 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(8) - (1)].ntype)))->get_value();
			int size = (yysemantic_stack_[(8) - (3)].itype);
			std::shared_ptr<rslc_node> node(new rslc_variable_array_def_node(name, size, (yysemantic_stack_[(8) - (7)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definition:identifier '[' INTEGER_LITERAL ']' '=' '{' expression_array '}'");
			}
		;}
    break;

  case 31:
#line 566 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(7) - (1)].ntype)))->get_value();
			int size = ((rslc_list_node*)((yysemantic_stack_[(7) - (6)].ntype)))->get_nodes_size();
			std::shared_ptr<rslc_node> node(new rslc_variable_array_def_node(name, size, (yysemantic_stack_[(7) - (6)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definition:identifier '[' ']' '=' '{' expression_array '}'");
			}
		;}
    break;

  case 32:
#line 583 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_list_node());
			GET_CTX()->add_node(node);
			((rslc_list_node*)(node.get()))->add_node((yysemantic_stack_[(1) - (1)].ntype));
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
		;}
    break;

  case 33:
#line 592 "rslc_parser.yy"
    {
			((rslc_list_node*)((yysemantic_stack_[(3) - (1)].ntype)))->add_node((yysemantic_stack_[(3) - (3)].ntype));
			(yyval.ntype) = (yysemantic_stack_[(3) - (1)].ntype);
			;
		;}
    break;

  case 34:
#line 601 "rslc_parser.yy"
    {
			rslc_type_node* type = (rslc_type_node*)(yysemantic_stack_[(2) - (2)].ntype);
			type->add_attribute("uniform");
			(yyval.ntype) = type;
		;}
    break;

  case 35:
#line 607 "rslc_parser.yy"
    {
			rslc_type_node* type = (rslc_type_node*)(yysemantic_stack_[(2) - (2)].ntype);
			type->add_attribute("varying");
			(yyval.ntype) = type;
		;}
    break;

  case 36:
#line 613 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 37:
#line 620 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_list_node());
			GET_CTX()->add_node(node);
			((rslc_list_node*)(node.get()))->add_node((yysemantic_stack_[(1) - (1)].ntype));
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statements:statement");
			}
		;}
    break;

  case 38:
#line 633 "rslc_parser.yy"
    {
			((rslc_list_node*)((yysemantic_stack_[(2) - (1)].ntype)))->add_node((yysemantic_stack_[(2) - (2)].ntype));
			(yyval.ntype) = (yysemantic_stack_[(2) - (1)].ntype);
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statements:statements statement");
			}
		;}
    break;

  case 39:
#line 646 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 40:
#line 650 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 41:
#line 654 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 42:
#line 658 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 43:
#line 662 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 44:
#line 666 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 45:
#line 670 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 46:
#line 674 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 47:
#line 682 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_statement_node((yysemantic_stack_[(2) - (1)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 48:
#line 691 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_statement_node((yysemantic_stack_[(2) - (1)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 49:
#line 700 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_return_node((yysemantic_stack_[(3) - (2)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("return_statement:TOKEN_RETURN expression ';'");
			}
		;}
    break;

  case 50:
#line 715 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statement_variables_definition");
			}
		;}
    break;

  case 51:
#line 724 "rslc_parser.yy"
    {
			rslc_type_variables_def_node* n0 = (rslc_type_variables_def_node*)(yysemantic_stack_[(2) - (2)].ntype);
			rslc_type_node* type = n0->get_type_node();
			type->add_attribute("extern");
			(yyval.ntype) = (yysemantic_stack_[(2) - (2)].ntype);
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statement_variables_definition");
			}
		;}
    break;

  case 52:
#line 739 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_statement_node((yysemantic_stack_[(2) - (1)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("definition_statement:statement_variables_definition ';'");
			}
		;}
    break;

  case 53:
#line 753 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_block_node());
			GET_CTX()->add_node(node);
			{
				rslc_list_node* l = (rslc_list_node*)(yysemantic_stack_[(3) - (2)].ntype);
				size_t sz = l->get_nodes_size();
				for(size_t i = 0;i<sz;i++)
				{
					((rslc_list_node*)(node.get()))->add_node(l->get_node_at(i));
				}
			}
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("block_statement:'{' statements '}'");
			}

			GET_CTX()->remove_node((yysemantic_stack_[(3) - (2)].ntype));
		;}
    break;

  case 54:
#line 774 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_block_node());
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("block_statement:'{' '}'");
			}
		;}
    break;

  case 55:
#line 785 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_block_node());
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("block_statement:';'");
			}
		;}
    break;

  case 56:
#line 800 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 57:
#line 804 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 58:
#line 808 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 59:
#line 812 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 60:
#line 816 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 61:
#line 820 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 62:
#line 826 "rslc_parser.yy"
    {
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node((yysemantic_stack_[(3) - (2)].ntype));
			
			std::shared_ptr<rslc_node> node(new rslc_while_node(cond, (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("while_statement:TOKEN_WHILE relation statement");
			}
		;}
    break;

  case 63:
#line 840 "rslc_parser.yy"
    {
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node((yysemantic_stack_[(3) - (2)].ntype));

			std::shared_ptr<rslc_node> node(new rslc_while_node(cond, (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("while_statement:TOKEN_WHILE expression statement");
			}
		;}
    break;

  case 64:
#line 857 "rslc_parser.yy"
    {
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node((yysemantic_stack_[(9) - (3)].ntype));
			cond->add_node((yysemantic_stack_[(9) - (5)].ntype));
			cond->add_node((yysemantic_stack_[(9) - (7)].ntype));

			std::shared_ptr<rslc_node> node(new rslc_for_node(cond, (yysemantic_stack_[(9) - (9)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("for_statement:TOKEN_FOR '(' expression ';' relation ';' expression ')' statement");
			}
		;}
    break;

  case 65:
#line 873 "rslc_parser.yy"
    {
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node((yysemantic_stack_[(9) - (3)].ntype));
			cond->add_node((yysemantic_stack_[(9) - (5)].ntype));
			cond->add_node((yysemantic_stack_[(9) - (7)].ntype));

			std::shared_ptr<rslc_node> node(new rslc_for_node(cond, (yysemantic_stack_[(9) - (9)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("for_statement:TOKEN_FOR '(' expression ';' expression ';' expression ')' statement");
			}
		;}
    break;

  case 66:
#line 892 "rslc_parser.yy"
    {
			rslc_list_node* cond = MAKE_LIST();

			std::shared_ptr<rslc_node> node(new rslc_solar_node(cond, (yysemantic_stack_[(4) - (4)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("solar_statement:TOKEN_SOLAR '(' ')' statement");
			}
		;}
    break;

  case 67:
#line 905 "rslc_parser.yy"
    {
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node((yysemantic_stack_[(7) - (3)].ntype));
			cond->add_node((yysemantic_stack_[(7) - (5)].ntype));

			std::shared_ptr<rslc_node> node(new rslc_solar_node(cond, (yysemantic_stack_[(7) - (7)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("solar_statement:TOKEN_SOLAR '(' expression ',' expression ')' statement");
			}
		;}
    break;

  case 68:
#line 923 "rslc_parser.yy"
    {
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node((yysemantic_stack_[(5) - (3)].ntype));

			std::shared_ptr<rslc_node> node(new rslc_illuminate_node(cond, (yysemantic_stack_[(5) - (5)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminate_statement:TOKEN_ILLUMINATE '(' expression ')' statement");
			}
		;}
    break;

  case 69:
#line 937 "rslc_parser.yy"
    {
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node((yysemantic_stack_[(9) - (3)].ntype));
			cond->add_node((yysemantic_stack_[(9) - (5)].ntype));
			cond->add_node((yysemantic_stack_[(9) - (7)].ntype));

			std::shared_ptr<rslc_node> node(new rslc_illuminate_node(cond, (yysemantic_stack_[(9) - (9)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminate_statement:TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement");
			}
		;}
    break;

  case 70:
#line 956 "rslc_parser.yy"
    {
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node((yysemantic_stack_[(5) - (3)].ntype));

			std::shared_ptr<rslc_node> node(new rslc_illuminance_node(cond, (yysemantic_stack_[(5) - (5)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminance_statement:TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement");
			}
		;}
    break;

  case 71:
#line 970 "rslc_parser.yy"
    {
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node((yysemantic_stack_[(7) - (3)].ntype));
			cond->add_node((yysemantic_stack_[(7) - (5)].ntype));

			std::shared_ptr<rslc_node> node(new rslc_illuminance_node(cond, (yysemantic_stack_[(7) - (7)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminance_statement:TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement");
			}
		;}
    break;

  case 72:
#line 985 "rslc_parser.yy"
    {
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node((yysemantic_stack_[(9) - (3)].ntype));
			cond->add_node((yysemantic_stack_[(9) - (5)].ntype));
			cond->add_node((yysemantic_stack_[(9) - (7)].ntype));

			std::shared_ptr<rslc_node> node(new rslc_illuminance_node(cond, (yysemantic_stack_[(9) - (9)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminance_statement:TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement");
			}
		;}
    break;

  case 73:
#line 1001 "rslc_parser.yy"
    {
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node((yysemantic_stack_[(11) - (3)].ntype));
			cond->add_node((yysemantic_stack_[(11) - (5)].ntype));
			cond->add_node((yysemantic_stack_[(11) - (7)].ntype));
			cond->add_node((yysemantic_stack_[(11) - (9)].ntype));

			std::shared_ptr<rslc_node> node(new rslc_illuminance_node(cond, (yysemantic_stack_[(11) - (11)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminance_statement:TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement");
			}
		;}
    break;

  case 74:
#line 1021 "rslc_parser.yy"
    {
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node((yysemantic_stack_[(13) - (3)].ntype));
			cond->add_node((yysemantic_stack_[(13) - (5)].ntype));
			cond->add_node((yysemantic_stack_[(13) - (7)].ntype));
			cond->add_node((yysemantic_stack_[(13) - (9)].ntype));
			cond->add_node((yysemantic_stack_[(13) - (11)].ntype));

			std::shared_ptr<rslc_node> node(new rslc_gather_node(cond, (yysemantic_stack_[(13) - (13)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminance_statement:TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement");
			}
		;}
    break;

  case 75:
#line 1041 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_if_node((yysemantic_stack_[(3) - (2)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("if_statement:TOKEN_IF relation statement");
			}
		;}
    break;

  case 76:
#line 1052 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_if_node((yysemantic_stack_[(3) - (2)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("if_statement:TOKEN_IF relation statement");
			}
		;}
    break;

  case 77:
#line 1063 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_if_node((yysemantic_stack_[(5) - (2)].ntype), (yysemantic_stack_[(5) - (3)].ntype), (yysemantic_stack_[(5) - (5)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("if_statement:TOKEN_IF relation statement TOKEN_ELSE statement");
			}
		;}
    break;

  case 78:
#line 1074 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_if_node((yysemantic_stack_[(5) - (2)].ntype), (yysemantic_stack_[(5) - (3)].ntype), (yysemantic_stack_[(5) - (5)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("if_statement:TOKEN_IF expression statement TOKEN_ELSE statement");
			}
		;}
    break;

  case 79:
#line 1088 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_break_node((yysemantic_stack_[(3) - (2)].itype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 80:
#line 1094 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_break_node());
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 81:
#line 1100 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_continue_node((yysemantic_stack_[(3) - (2)].itype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 82:
#line 1106 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_continue_node());
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 83:
#line 1115 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 84:
#line 1119 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_dot_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 85:
#line 1126 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_div_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 86:
#line 1133 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_mul_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 87:
#line 1140 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_crs_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 88:
#line 1147 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_add_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 89:
#line 1153 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_sub_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 90:
#line 1160 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_neg_node((yysemantic_stack_[(2) - (2)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("expression:'-' expression");
			}
		;}
    break;

  case 91:
#line 1172 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_cond_node((yysemantic_stack_[(5) - (1)].ntype), (yysemantic_stack_[(5) - (3)].ntype), (yysemantic_stack_[(5) - (5)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("expression:relation '?' expression ':' expression");
			}
		;}
    break;

  case 92:
#line 1184 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 93:
#line 1190 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 94:
#line 1194 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 95:
#line 1198 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(1) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node(new rslc_variable_ref_node(name));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("primary:identifier");
			}
		;}
    break;

  case 96:
#line 1211 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(4) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node(new rslc_variable_array_ref_node(name, (yysemantic_stack_[(4) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("primary:identifier '[' expression ']'");
			}
		;}
    break;

  case 97:
#line 1224 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 98:
#line 1228 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 99:
#line 1232 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_paren_node((yysemantic_stack_[(3) - (2)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 100:
#line 1239 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 101:
#line 1243 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 102:
#line 1250 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_tuple_node());
			GET_CTX()->add_node(node);
			rslc_tuple_node* n0 = (rslc_tuple_node*)(node.get());
			n0->add_node((yysemantic_stack_[(7) - (2)].ntype));
			n0->add_node((yysemantic_stack_[(7) - (4)].ntype));
			n0->add_node((yysemantic_stack_[(7) - (6)].ntype));
			node->set_location(GET_LOC());
			(yyval.ntype) = n0;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("tuple3");
			}
		;}
    break;

  case 103:
#line 1269 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_tuple_node());
			GET_CTX()->add_node(node);
			rslc_tuple_node* n0 = (rslc_tuple_node*)(node.get());
			n0->add_node((yysemantic_stack_[(33) - (2)].ntype));n0->add_node((yysemantic_stack_[(33) - (4)].ntype));n0->add_node((yysemantic_stack_[(33) - (6)].ntype));n0->add_node((yysemantic_stack_[(33) - (8)].ntype));
			n0->add_node((yysemantic_stack_[(33) - (10)].ntype));n0->add_node((yysemantic_stack_[(33) - (12)].ntype));n0->add_node((yysemantic_stack_[(33) - (14)].ntype));n0->add_node((yysemantic_stack_[(33) - (16)].ntype));
			n0->add_node((yysemantic_stack_[(33) - (18)].ntype));n0->add_node((yysemantic_stack_[(33) - (20)].ntype));n0->add_node((yysemantic_stack_[(33) - (22)].ntype));n0->add_node((yysemantic_stack_[(33) - (24)].ntype));
			n0->add_node((yysemantic_stack_[(33) - (26)].ntype));n0->add_node((yysemantic_stack_[(33) - (28)].ntype));n0->add_node((yysemantic_stack_[(33) - (30)].ntype));n0->add_node((yysemantic_stack_[(33) - (32)].ntype));
			
			node->set_location(GET_LOC());
			(yyval.ntype) = n0;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("tuple16");
			}
		;}
    break;

  case 104:
#line 1290 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_paren_node((yysemantic_stack_[(3) - (2)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:'(' relation ')'");
			}
		;}
    break;

  case 105:
#line 1302 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_gtr_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:expression '>' expression");
			}
		;}
    break;

  case 106:
#line 1314 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_geq_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:expression TOKEN_GE expression");
			}
		;}
    break;

  case 107:
#line 1326 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_lss_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:expression '<' expression");
			}
		;}
    break;

  case 108:
#line 1338 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_leq_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:expression TOKEN_LE expression");
			}
		;}
    break;

  case 109:
#line 1350 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_eq_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:expression TOKEN_EQ expression");
			}
		;}
    break;

  case 110:
#line 1362 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_ne_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:expression TOKEN_NE expression");
			}
		;}
    break;

  case 111:
#line 1374 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_and_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:relation TOKEN_AND relation");
			}
		;}
    break;

  case 112:
#line 1386 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_or_node((yysemantic_stack_[(3) - (1)].ntype), (yysemantic_stack_[(3) - (3)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:relation TOKEN_OR relation");
			}
		;}
    break;

  case 113:
#line 1398 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_not_node((yysemantic_stack_[(2) - (2)].ntype)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:'!' relation");
			}
		;}
    break;

  case 114:
#line 1413 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(3) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_ref_node(name));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node* n3   = (rslc_expression_node*)(yysemantic_stack_[(3) - (3)].ntype);

			std::shared_ptr<rslc_node> node(new rslc_assign_node(n1, n3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier '=' expression");
			}
		;}
    break;

  case 115:
#line 1433 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(3) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_ref_node(name));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node* n3   = (rslc_expression_node*)(yysemantic_stack_[(3) - (3)].ntype);

			std::shared_ptr<rslc_node> node(new rslc_add_assign_node(n1, n3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier '+=' expression");
			}
		;}
    break;

  case 116:
#line 1453 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(3) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_ref_node(name));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node* n3   = (rslc_expression_node*)(yysemantic_stack_[(3) - (3)].ntype);

			std::shared_ptr<rslc_node> node(new rslc_sub_assign_node(n1, n3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier '-=' expression");
			}
		;}
    break;

  case 117:
#line 1473 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(3) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_ref_node(name));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node* n3   = (rslc_expression_node*)(yysemantic_stack_[(3) - (3)].ntype);

			std::shared_ptr<rslc_node> node(new rslc_mul_assign_node(n1, n3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier '*=' expression");
			}
		;}
    break;

  case 118:
#line 1493 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(3) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_ref_node(name));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node* n3   = (rslc_expression_node*)(yysemantic_stack_[(3) - (3)].ntype);

			std::shared_ptr<rslc_node> node(new rslc_div_assign_node(n1, n3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier '/=' expression");
			}
		;}
    break;

  case 119:
#line 1513 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(6) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_array_ref_node(name, (yysemantic_stack_[(6) - (3)].ntype)));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node*   n6 = (rslc_expression_node*)(yysemantic_stack_[(6) - (6)].ntype);

			std::shared_ptr<rslc_node> node(new rslc_assign_node(n1, n6));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier  '[' expression ']' '/=' expression");
			}
		;}
    break;

  case 120:
#line 1533 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(6) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_array_ref_node(name, (yysemantic_stack_[(6) - (3)].ntype)));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node*   n6 = (rslc_expression_node*)(yysemantic_stack_[(6) - (6)].ntype);

			std::shared_ptr<rslc_node> node(new rslc_add_assign_node(n1, n6));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier  '[' expression ']' '/=' expression");
			}
		;}
    break;

  case 121:
#line 1553 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(6) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_array_ref_node(name, (yysemantic_stack_[(6) - (3)].ntype)));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node*   n6 = (rslc_expression_node*)(yysemantic_stack_[(6) - (6)].ntype);

			std::shared_ptr<rslc_node> node(new rslc_sub_assign_node(n1, n6));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier  '[' expression ']' '/=' expression");
			}
		;}
    break;

  case 122:
#line 1573 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(6) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_array_ref_node(name, (yysemantic_stack_[(6) - (3)].ntype)));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node*   n6 = (rslc_expression_node*)(yysemantic_stack_[(6) - (6)].ntype);

			std::shared_ptr<rslc_node> node(new rslc_mul_assign_node(n1, n6));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier  '[' expression ']' '/=' expression");
			}
		;}
    break;

  case 123:
#line 1593 "rslc_parser.yy"
    {
			std::string name = ((rslc_identifier_node*)((yysemantic_stack_[(6) - (1)].ntype)))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_array_ref_node(name, (yysemantic_stack_[(6) - (3)].ntype)));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node*   n6 = (rslc_expression_node*)(yysemantic_stack_[(6) - (6)].ntype);

			std::shared_ptr<rslc_node> node(new rslc_div_assign_node(n1, n6));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier  '[' expression ']' '/=' expression");
			}
		;}
    break;

  case 124:
#line 1616 "rslc_parser.yy"
    {
			rslc_type_node* n0 = (rslc_type_node*)(yysemantic_stack_[(2) - (1)].ntype);
			rslc_expression_node* n1 = (rslc_expression_node*)(yysemantic_stack_[(2) - (2)].ntype);
			std::shared_ptr<rslc_node> node(new rslc_cast_node(n0, n1));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 125:
#line 1625 "rslc_parser.yy"
    {
			rslc_type_node* n0 = (rslc_type_node*)(yysemantic_stack_[(3) - (1)].ntype);
			std::string s1 = (yysemantic_stack_[(3) - (2)].stype);
			rslc_expression_node* n2 = (rslc_expression_node*)(yysemantic_stack_[(3) - (3)].ntype);
			std::shared_ptr<rslc_node> node(new rslc_cast_node(n0, s1, n2));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			release((yysemantic_stack_[(3) - (2)].stype));
		;}
    break;

  case 126:
#line 1639 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("float"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 127:
#line 1646 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("string"));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 128:
#line 1652 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("color"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 129:
#line 1659 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("point"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 130:
#line 1666 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("vector"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 131:
#line 1673 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("normal"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 132:
#line 1680 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("matrix"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 133:
#line 1687 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_type_node("void"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 134:
#line 1697 "rslc_parser.yy"
    {
			rslc_identifier_node* n0 = (rslc_identifier_node*)(yysemantic_stack_[(4) - (1)].ntype);
			rslc_node* n1 = (yysemantic_stack_[(4) - (3)].ntype);

			std::shared_ptr<rslc_node> node(new rslc_function_call_node(n0->get_value(), n1));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "function_name '(' function_arguments ')'";
				s += ":";
				s += n0->get_value();
				DEBUG(s.c_str());
			}
			;
			GET_CTX()->remove_node(n0);
		;}
    break;

  case 135:
#line 1717 "rslc_parser.yy"
    {
			rslc_identifier_node* n0 = (rslc_identifier_node*)(yysemantic_stack_[(3) - (1)].ntype);
			rslc_node* n1 = NULL;

			{
				std::shared_ptr<rslc_node> n(new rslc_list_node());
				GET_CTX()->add_node(n);
				n->set_location(GET_LOC());
				n1 = n.get();
			}

			std::shared_ptr<rslc_node> node(new rslc_function_call_node(n0->get_value(), n1));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "function_call:function_name '(' ')'";
				s += ":";
				s += n0->get_value();
				DEBUG(s.c_str());
			}
			;
			GET_CTX()->remove_node(n0);
		;}
    break;

  case 136:
#line 1747 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 137:
#line 1751 "rslc_parser.yy"
    {
			std::string v = "atomosphere";
			std::shared_ptr<rslc_node> node(new rslc_identifier_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 138:
#line 1759 "rslc_parser.yy"
    {
			std::string v = "displacement";
			std::shared_ptr<rslc_node> node(new rslc_identifier_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 139:
#line 1767 "rslc_parser.yy"
    {
			std::string v = "lightsource";
			std::shared_ptr<rslc_node> node(new rslc_identifier_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 140:
#line 1775 "rslc_parser.yy"
    {
			std::string v = "surface";
			std::shared_ptr<rslc_node> node(new rslc_identifier_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
		;}
    break;

  case 141:
#line 1786 "rslc_parser.yy"
    {
			std::shared_ptr<rslc_node> node(new rslc_list_node());
			((rslc_list_node*)node.get())->add_node((yysemantic_stack_[(1) - (1)].ntype));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statements:statement");
			}
		;}
    break;

  case 142:
#line 1799 "rslc_parser.yy"
    {
			((rslc_list_node*)(yysemantic_stack_[(3) - (1)].ntype))->add_node((yysemantic_stack_[(3) - (3)].ntype));
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statements:statement");
			}
		;}
    break;

  case 143:
#line 1811 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statements:statement");
			}
		;}
    break;

  case 144:
#line 1820 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statements:statement");
			}
		;}
    break;

  case 145:
#line 1832 "rslc_parser.yy"
    {
			std::string v = (yysemantic_stack_[(1) - (1)].stype);
			std::shared_ptr<rslc_node> node(new rslc_identifier_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "identifier:IDENTIFIER:";
				s += v;
				DEBUG(s.c_str());
			}
			;
			release((yysemantic_stack_[(1) - (1)].stype));
		;}
    break;

  case 146:
#line 1852 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("number:integer");
			}
		;}
    break;

  case 147:
#line 1861 "rslc_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("number:float");
			}
		;}
    break;

  case 148:
#line 1873 "rslc_parser.yy"
    {
			int v = (yysemantic_stack_[(1) - (1)].itype);
			std::shared_ptr<rslc_node> node(new rslc_integer_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("integer:INTEGER_LITERAL");
			}
		;}
    break;

  case 149:
#line 1889 "rslc_parser.yy"
    {
			float v = (yysemantic_stack_[(1) - (1)].ftype);
			std::shared_ptr<rslc_node> node(new rslc_float_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("float:FLOAT_LITERAL");
			}
		;}
    break;

  case 150:
#line 1905 "rslc_parser.yy"
    {
			std::string v = (yysemantic_stack_[(1) - (1)].stype);
			std::shared_ptr<rslc_node> node(new rslc_string_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("string:STRING_LITERAL");
			}
			;
			release((yysemantic_stack_[(1) - (1)].stype));
		;}
    break;

  case 151:
#line 1922 "rslc_parser.yy"
    {
			std::string v = (yysemantic_stack_[(4) - (1)].stype);
			int i = (yysemantic_stack_[(4) - (3)].itype);
			std::shared_ptr<rslc_node> node(new rslc_texturename_node(v, i));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			(yyval.ntype) = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("texture_name:STRING_LITERAL '[' INTEGER_LITERAL ']'");
			}
			;
			release((yysemantic_stack_[(4) - (1)].stype));
		;}
    break;


    /* Line 675 of lalr1.cc.  */
#line 2460 "rslc_parser.cpp"
	default: break;
      }
    YY_SYMBOL_PRINT ("-> $$ =", yyr1_[yyn], &yyval, &yyloc);

    yypop_ (yylen);
    yylen = 0;
    YY_STACK_PRINT ();

    yysemantic_stack_.push (yyval);
    yylocation_stack_.push (yyloc);

    /* Shift the result of the reduction.  */
    yyn = yyr1_[yyn];
    yystate = yypgoto_[yyn - yyntokens_] + yystate_stack_[0];
    if (0 <= yystate && yystate <= yylast_
	&& yycheck_[yystate] == yystate_stack_[0])
      yystate = yytable_[yystate];
    else
      yystate = yydefgoto_[yyn - yyntokens_];
    goto yynewstate;

  /*------------------------------------.
  | yyerrlab -- here on detecting error |
  `------------------------------------*/
  yyerrlab:
    /* If not already recovering from an error, report this error.  */
    if (!yyerrstatus_)
      {
	++yynerrs_;
	error (yylloc, yysyntax_error_ (yystate, yytoken));
      }

    yyerror_range[0] = yylloc;
    if (yyerrstatus_ == 3)
      {
	/* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

	if (yychar <= yyeof_)
	  {
	  /* Return failure if at end of input.  */
	  if (yychar == yyeof_)
	    YYABORT;
	  }
	else
	  {
	    yydestruct_ ("Error: discarding", yytoken, &yylval, &yylloc);
	    yychar = yyempty_;
	  }
      }

    /* Else will try to reuse look-ahead token after shifting the error
       token.  */
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;

    yyerror_range[0] = yylocation_stack_[yylen - 1];
    /* Do not reclaim the symbols of the rule which action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    yystate = yystate_stack_[0];
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;	/* Each real token shifted decrements this.  */

    for (;;)
      {
	yyn = yypact_[yystate];
	if (yyn != yypact_ninf_)
	{
	  yyn += yyterror_;
	  if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
	    {
	      yyn = yytable_[yyn];
	      if (0 < yyn)
		break;
	    }
	}

	/* Pop the current state because it cannot handle the error token.  */
	if (yystate_stack_.height () == 1)
	YYABORT;

	yyerror_range[0] = yylocation_stack_[0];
	yydestruct_ ("Error: popping",
		     yystos_[yystate],
		     &yysemantic_stack_[0], &yylocation_stack_[0]);
	yypop_ ();
	yystate = yystate_stack_[0];
	YY_STACK_PRINT ();
      }

    if (yyn == yyfinal_)
      goto yyacceptlab;

    yyerror_range[1] = yylloc;
    // Using YYLLOC is tempting, but would change the location of
    // the look-ahead.  YYLOC is available though.
    YYLLOC_DEFAULT (yyloc, (yyerror_range - 1), 2);
    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yyloc);

    /* Shift the error token.  */
    YY_SYMBOL_PRINT ("Shifting", yystos_[yyn],
		   &yysemantic_stack_[0], &yylocation_stack_[0]);

    yystate = yyn;
    goto yynewstate;

    /* Accept.  */
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    /* Abort.  */
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (yychar != yyeof_ && yychar != yyempty_)
      yydestruct_ ("Cleanup: discarding lookahead", yytoken, &yylval, &yylloc);

    /* Do not reclaim the symbols of the rule which action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (yystate_stack_.height () != 1)
      {
	yydestruct_ ("Cleanup: popping",
		   yystos_[yystate_stack_[0]],
		   &yysemantic_stack_[0],
		   &yylocation_stack_[0]);
	yypop_ ();
      }

    return yyresult;
  }

  // Generate an error message.
  std::string
  rslc_parser::yysyntax_error_ (int yystate, int tok)
  {
    std::string res;
    YYUSE (yystate);
#if YYERROR_VERBOSE
    int yyn = yypact_[yystate];
    if (yypact_ninf_ < yyn && yyn <= yylast_)
      {
	/* Start YYX at -YYN if negative to avoid negative indexes in
	   YYCHECK.  */
	int yyxbegin = yyn < 0 ? -yyn : 0;

	/* Stay within bounds of both yycheck and yytname.  */
	int yychecklim = yylast_ - yyn + 1;
	int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
	int count = 0;
	for (int x = yyxbegin; x < yyxend; ++x)
	  if (yycheck_[x + yyn] == x && x != yyterror_)
	    ++count;

	// FIXME: This method of building the message is not compatible
	// with internationalization.  It should work like yacc.c does it.
	// That is, first build a string that looks like this:
	// "syntax error, unexpected %s or %s or %s"
	// Then, invoke YY_ on this string.
	// Finally, use the string as a format to output
	// yytname_[tok], etc.
	// Until this gets fixed, this message appears in English only.
	res = "syntax error, unexpected ";
	res += yytnamerr_ (yytname_[tok]);
	if (count < 5)
	  {
	    count = 0;
	    for (int x = yyxbegin; x < yyxend; ++x)
	      if (yycheck_[x + yyn] == x && x != yyterror_)
		{
		  res += (!count++) ? ", expecting " : " or ";
		  res += yytnamerr_ (yytname_[x]);
		}
	  }
      }
    else
#endif
      res = YY_("syntax error");
    return res;
  }


  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
  const signed char rslc_parser::yypact_ninf_ = -112;
  const short int
  rslc_parser::yypact_[] =
  {
       543,  -112,  -112,  -112,  -112,  -112,  -112,  -112,  -112,  -112,
    -112,  -112,  -112,  -112,  -112,    14,   543,  -112,  -112,    19,
    -112,    19,  -112,  -112,  -112,   -58,     8,   193,   227,   339,
     339,   116,   -48,    15,  -112,  -112,    19,  -112,    -8,    21,
    -112,  -112,  -112,   809,    22,   625,    18,  -112,   -31,  1281,
      25,   116,  -112,  -112,  -112,  1353,  1353,    31,    -3,     3,
    1353,    32,    33,    38,    41,  -112,   868,  -112,  -112,  -112,
     927,  -112,  -112,  -112,  -112,    39,  -112,  -112,  -112,  -112,
    -112,  -112,  -112,  -112,  -112,  -112,  -112,    43,    45,    42,
     -21,   986,  -112,    19,  1353,    -4,  1045,  1281,  -112,  -112,
    -112,  -112,  1353,  1353,  1353,   670,  -112,  -112,  -112,   750,
    -112,  -112,  1390,  -112,   -29,  -112,  -112,  -112,  -112,   670,
     750,  1353,    48,  -112,    49,  -112,  1654,    -9,  1353,  1353,
     594,  1353,  -112,  1104,  -112,  -112,  -112,  -112,  -112,  1316,
    1353,  1353,  1353,  1353,  1353,  1353,  -112,  1163,  -112,  1836,
      47,    68,  -112,  1222,  -112,  1836,  -112,   466,    28,  1353,
    1353,  1353,  1353,  1353,  1353,  1353,  1353,  1353,  1353,  1353,
    1353,    84,  1353,  1353,  1353,    90,  1353,  1836,  1353,  -112,
    -112,  1668,  -112,  -112,  -112,   802,   861,  1281,  1097,  1156,
    -112,    51,  -112,  1836,    -6,  -112,  -112,  1836,  1836,  1836,
    1836,  1836,   114,  -112,    96,    72,  -112,  -112,  1353,  -112,
    1836,  1836,  1836,  1836,  1836,  1836,   139,   139,   -17,    78,
      78,  -112,  1281,    -7,    87,  1822,  1281,  1836,   319,  1353,
    1281,  1353,  1281,  1353,  -112,  1353,  1353,   135,  -112,  1427,
     150,    81,  1353,  1215,  -112,  1353,  -112,   150,  1682,   -18,
    -112,  1412,  -112,   920,  1696,  1444,    77,  -112,  1353,  1353,
    1353,  1353,  1353,  1353,    12,  1836,  1353,   158,  1353,  1353,
    1353,  1281,  1353,  1281,  1353,  -112,  1836,  1836,  1836,  1836,
    1836,    34,  -112,  1353,   979,  1714,  1732,  1750,  -112,  1038,
    -112,  1458,  -112,  1836,  -112,  1353,  1281,  1281,  1281,  1281,
    1353,  1353,  1472,  -112,  -112,  -112,  -112,  1768,  1486,  1353,
    1281,  1353,  1500,  -112,  1786,  1353,  1281,  1514,  -112,  1353,
    1528,  1353,  1542,  1353,  1556,  1353,  1570,  1353,  1584,  1353,
    1598,  1353,  1612,  1353,  1626,  1353,  1640,  1353,  1804,  -112
  };

  /* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
     doesn't specify something else to do.  Zero means the default is an
     error.  */
  const unsigned char
  rslc_parser::yydefact_[] =
  {
         0,   126,   128,   129,   130,   131,   132,   127,   133,    12,
      11,    13,    14,    16,    15,     0,     2,     3,     5,     0,
       6,     0,     1,     4,   145,     0,     0,     0,     0,     0,
       0,     0,     0,     0,    19,    22,     0,    36,     0,     0,
      34,    35,    23,     0,     0,    21,    24,    25,    27,     0,
       0,     0,   140,   137,   138,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   139,     0,    10,    55,    50,
       0,    37,    39,    40,    41,     0,    43,    44,    45,    56,
      57,    58,    59,    60,    61,    46,    42,     0,     0,     0,
     136,     0,    20,     0,     0,     0,     0,     0,    51,   149,
     148,   150,     0,     0,     0,     0,    83,   100,   101,     0,
      98,    92,     0,    97,    95,    93,   146,   147,    94,     0,
       0,     0,     0,    82,     0,    80,     0,     0,     0,     0,
       0,     0,    54,     0,     9,    38,    52,    47,    48,     0,
       0,     0,     0,     0,     0,     0,     8,     0,    26,    28,
       0,     0,    18,     0,    90,     0,   113,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,    76,     0,     0,     0,    75,   150,   124,     0,    63,
      62,     0,    81,    79,    49,     0,     0,     0,     0,     0,
      53,   150,   135,   143,     0,   141,   144,   114,   115,   116,
     117,   118,     0,     7,    29,     0,    17,    99,     0,   104,
     107,   105,   108,   106,   109,   110,    88,    89,    87,    85,
      86,    84,     0,   112,   111,     0,     0,   125,     0,     0,
       0,     0,     0,     0,    66,     0,     0,     0,   134,     0,
       0,     0,     0,     0,    78,     0,    77,    96,     0,     0,
      68,     0,    70,     0,     0,     0,     0,   142,     0,     0,
       0,     0,     0,     0,     0,    32,     0,    91,     0,     0,
       0,     0,     0,     0,     0,   151,   119,   120,   121,   122,
     123,     0,    31,     0,     0,     0,     0,     0,    71,     0,
      67,     0,    30,    33,   102,     0,     0,     0,     0,     0,
       0,     0,     0,    65,    64,    69,    72,     0,     0,     0,
       0,     0,     0,    73,     0,     0,     0,     0,    74,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,   103
  };

  /* YYPGOTO[NTERM-NUM].  */
  const short int
  rslc_parser::yypgoto_[] =
  {
      -112,  -112,  -112,   130,  -112,  -112,  -112,   120,   104,    30,
    -112,    57,  -111,  -112,   -43,   -46,  -112,  -112,  -112,  -112,
    -112,  -112,  -112,  -112,  -112,  -112,  -112,  -112,  -112,  -112,
    -112,   163,  -112,  -112,  -112,   -51,   348,  -112,     0,   440,
    -112,  -112,   -85,    46,  -112,  -112,  -112,  -112,  -112
  };

  /* YYDEFGOTO[NTERM-NUM].  */
  const short int
  rslc_parser::yydefgoto_[] =
  {
        -1,    15,    16,    17,    18,    19,    20,    33,    34,    69,
      46,    47,   264,    36,    70,    71,    72,    73,    74,    75,
      76,    77,    78,    79,    80,    81,    82,    83,    84,    85,
      86,   155,   106,   107,   108,   127,   110,   111,   112,   113,
      89,   194,   195,   114,   115,   116,   117,   118,   196
  };

  /* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule which
     number is the opposite.  If zero, do what YYDEFACT says.  */
  const short int rslc_parser::yytable_ninf_ = -137;
  const short int
  rslc_parser::yytable_[] =
  {
        21,   150,   122,    27,   109,   120,    96,    94,   124,   140,
     141,   142,   143,   144,    22,    43,    21,   140,   141,   142,
     143,   144,    24,   133,   135,   172,   173,    37,    37,    40,
      41,    37,  -136,   174,   172,   173,    95,   173,   178,   168,
     169,   170,   174,    37,   174,    37,   145,   269,   147,    37,
     135,    37,   156,   158,   153,    49,   238,    35,    35,   171,
     239,    42,   123,   175,   151,    25,    37,    26,   125,    28,
      37,   172,   173,   179,   180,    35,   282,    44,   283,   174,
      45,    98,    48,    50,    93,    91,    45,   135,    97,    90,
     209,    37,   121,   128,   129,    90,    37,    37,   292,   130,
     283,   135,   131,   139,   136,    37,   205,   135,   137,    37,
     138,   222,    90,   182,   183,   204,    90,   226,   237,    37,
      37,   223,   224,     1,     2,     3,     4,     5,     6,     7,
       8,    29,    30,    37,   241,   242,   170,    90,   174,    48,
     256,   234,    90,    90,   263,   275,    23,    37,    39,    92,
     148,    90,   281,    37,   257,    90,     0,     0,     0,   159,
     160,   161,   162,   163,   164,    90,    90,   165,   166,   167,
     168,   169,   170,     0,     0,     0,   244,     0,   249,    90,
     246,     0,   240,     0,   250,     0,   252,    37,   258,   259,
     260,   261,   262,    90,   167,   168,   169,   170,     0,    90,
       1,     2,     3,     4,     5,     6,     7,     8,    29,    30,
      31,   165,   166,   167,   168,   169,   170,     0,   105,   119,
       0,     0,    37,   126,     0,   288,    37,   290,     0,     0,
      37,     0,    37,    90,     1,     2,     3,     4,     5,     6,
       7,     8,    29,    30,    31,     0,     0,     0,     0,     0,
     303,   304,   305,   306,     0,    32,     0,   149,     0,     0,
       0,     0,     0,     0,   313,   154,     0,   157,    90,     0,
     318,    37,    90,    37,     0,   177,    90,     0,    90,     0,
       0,     0,     0,     0,   181,     0,     0,     0,     0,    38,
       0,   185,   186,   188,   189,     0,    37,    37,    37,    37,
       0,     0,   193,   197,   198,   199,   200,   201,   202,     0,
      37,     0,     0,     0,     0,     0,    37,    90,     0,    90,
       0,     0,   210,   211,   212,   213,   214,   215,   216,   217,
     218,   219,   220,   221,     0,     0,     0,   225,     0,   227,
       0,   228,    90,    90,    90,    90,     1,     2,     3,     4,
       5,     6,     7,     8,     0,     0,    90,     0,     0,     0,
       0,     0,    90,     0,   159,   160,   161,   162,   163,   164,
       0,   243,   165,   166,   167,   168,   169,   170,     0,     0,
       0,     0,     0,     0,     0,     0,     0,   247,     0,     0,
       0,    87,   248,     0,   251,     0,   253,    87,   254,   255,
       0,     0,   193,     0,     0,   265,     0,     0,   267,     0,
       0,     0,     0,     0,    87,     0,     0,     0,    87,     0,
       0,   276,   277,   278,   279,   280,   265,     0,     0,   284,
       0,   285,   286,   287,     0,   289,     0,   291,     0,    87,
       0,     0,     0,     0,    87,    87,   293,     0,     0,     0,
       0,     0,     0,    87,     0,     0,     0,    87,   302,     0,
       0,     0,     0,   307,   308,     0,     0,    87,    87,     0,
       0,     0,   312,     0,   314,     0,     0,     0,   317,     0,
       0,    87,   320,    88,   322,     0,   324,     0,   326,    88,
     328,     0,   330,     0,   332,    87,   334,     0,   336,     0,
     338,    87,     0,     0,     0,     0,    88,     0,     0,     0,
      88,   159,   160,   161,   162,   163,   164,     0,     0,   165,
     166,   167,   168,   169,   170,     0,     0,     0,   207,     0,
       0,    88,   208,     0,     0,    87,    88,    88,     0,     0,
       0,     0,     0,     0,     0,    88,     0,     0,     0,    88,
       1,     2,     3,     4,     5,     6,     7,     8,     0,    88,
      88,     0,     9,    10,     0,    11,    12,    13,    14,     0,
      87,     0,     0,    88,    87,     0,     0,     0,    87,     0,
      87,     0,     0,     0,     0,     0,     0,    88,     0,     0,
       0,     0,     0,    88,     0,     0,     0,    24,    99,   100,
     101,     1,     2,     3,     4,     5,     6,     7,     8,     0,
       0,     0,     0,    52,     0,    53,     0,    54,     0,    87,
       0,    87,     0,     0,     0,     0,     0,    88,     0,     0,
       0,    65,     1,     2,     3,     4,     5,     6,     7,     8,
      29,    30,    31,     0,    87,    87,    87,    87,   102,     0,
       0,     0,     0,   103,     0,   104,   187,     0,    87,     0,
       0,     0,    88,     0,    87,     0,    88,     0,     0,     0,
      88,     0,    88,    24,     0,     0,     0,     1,     2,     3,
       4,     5,     6,     7,     8,    29,    30,     0,    51,    52,
       0,    53,     0,    54,     0,     0,    55,     0,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,     0,     0,
       0,    88,     0,    88,     0,   159,   160,   161,   162,   163,
     164,     0,     0,   165,   166,   167,   168,   169,   170,     0,
       0,     0,     0,    66,     0,    68,    88,    88,    88,    88,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      88,     0,     0,    24,     0,     0,    88,     1,     2,     3,
       4,     5,     6,     7,     8,    29,    30,     0,    51,    52,
       0,    53,     0,    54,     0,     0,    55,     0,    56,    57,
      58,    59,    60,    61,    62,    63,    64,    65,     0,     0,
       0,     0,     0,   172,   173,     0,     0,     0,     0,     0,
       0,   174,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,    24,    66,     0,    68,     1,     2,     3,     4,
       5,     6,     7,     8,    29,    30,     0,    51,    52,     0,
      53,     0,    54,     0,     0,    55,     0,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,   159,   160,   161,
     162,   163,   164,     0,     0,   165,   166,   167,   168,   169,
     170,     0,     0,     0,   230,     0,     0,     0,   231,     0,
       0,    24,    66,    67,    68,     1,     2,     3,     4,     5,
       6,     7,     8,    29,    30,     0,    51,    52,     0,    53,
       0,    54,     0,     0,    55,     0,    56,    57,    58,    59,
      60,    61,    62,    63,    64,    65,   159,   160,   161,   162,
     163,   164,     0,     0,   165,   166,   167,   168,   169,   170,
       0,     0,     0,   232,     0,     0,     0,   233,     0,     0,
      24,    66,   132,    68,     1,     2,     3,     4,     5,     6,
       7,     8,    29,    30,     0,    51,    52,     0,    53,     0,
      54,     0,     0,    55,     0,    56,    57,    58,    59,    60,
      61,    62,    63,    64,    65,   159,   160,   161,   162,   163,
     164,     0,     0,   165,   166,   167,   168,   169,   170,     0,
       0,     0,   271,     0,     0,     0,   272,     0,     0,    24,
      66,   134,    68,     1,     2,     3,     4,     5,     6,     7,
       8,    29,    30,     0,    51,    52,     0,    53,     0,    54,
       0,     0,    55,     0,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,   159,   160,   161,   162,   163,   164,
       0,     0,   165,   166,   167,   168,   169,   170,     0,     0,
       0,   294,     0,     0,     0,   295,     0,     0,    24,    66,
     146,    68,     1,     2,     3,     4,     5,     6,     7,     8,
      29,    30,     0,    51,    52,     0,    53,     0,    54,     0,
       0,    55,     0,    56,    57,    58,    59,    60,    61,    62,
      63,    64,    65,   159,   160,   161,   162,   163,   164,     0,
       0,   165,   166,   167,   168,   169,   170,     0,     0,     0,
     299,     0,     0,     0,   300,     0,     0,    24,    66,   152,
      68,     1,     2,     3,     4,     5,     6,     7,     8,    29,
      30,     0,    51,    52,     0,    53,     0,    54,     0,     0,
      55,     0,    56,    57,    58,    59,    60,    61,    62,    63,
      64,    65,   159,   160,   161,   162,   163,   164,     0,     0,
     165,   166,   167,   168,   169,   170,     0,     0,     0,     0,
       0,     0,     0,   235,     0,     0,    24,    66,   190,    68,
       1,     2,     3,     4,     5,     6,     7,     8,    29,    30,
       0,    51,    52,     0,    53,     0,    54,     0,     0,    55,
       0,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,   159,   160,   161,   162,   163,   164,     0,     0,   165,
     166,   167,   168,   169,   170,     0,     0,     0,     0,     0,
       0,     0,   236,     0,     0,    24,    66,   203,    68,     1,
       2,     3,     4,     5,     6,     7,     8,    29,    30,     0,
      51,    52,     0,    53,     0,    54,     0,     0,    55,     0,
      56,    57,    58,    59,    60,    61,    62,    63,    64,    65,
     159,   160,   161,   162,   163,   164,     0,     0,   165,   166,
     167,   168,   169,   170,     0,     0,     0,     0,     0,     0,
       0,   266,     0,     0,    24,    66,   206,    68,     1,     2,
       3,     4,     5,     6,     7,     8,    29,    30,     0,    51,
      52,     0,    53,     0,    54,     0,     0,    55,     0,    56,
      57,    58,    59,    60,    61,    62,    63,    64,    65,    24,
      99,   100,   191,     1,     2,     3,     4,     5,     6,     7,
       8,     0,     0,     0,     0,    52,     0,    53,     0,    54,
       0,     0,     0,     0,    66,     0,    68,     0,     0,     0,
       0,     0,     0,    65,     0,     0,    24,    99,   100,   101,
       1,     2,     3,     4,     5,     6,     7,     8,     0,     0,
     102,     0,    52,     0,    53,   103,    54,   104,   192,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      65,     0,     0,    24,    99,   100,   176,     1,     2,     3,
       4,     5,     6,     7,     8,     0,     0,   102,     0,    52,
       0,    53,   103,    54,   104,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,    65,     0,     0,
      24,    99,   100,   191,     1,     2,     3,     4,     5,     6,
       7,     8,     0,     0,   102,     0,    52,     0,    53,   103,
      54,   104,     0,     0,     0,     0,     0,   159,   160,   161,
     162,   163,   164,     0,    65,   165,   166,   167,   168,   169,
     170,     0,     0,     0,     0,     0,     0,     0,   270,     0,
       0,   102,     0,     0,     0,     0,   103,     0,   104,   159,
     160,   161,   162,   163,   164,     0,     0,   165,   166,   167,
     168,   169,   170,   159,   160,   161,   162,   163,   164,     0,
     274,   165,   166,   167,   168,   169,   170,   159,   160,   161,
     162,   163,   164,     0,   301,   165,   166,   167,   168,   169,
     170,   159,   160,   161,   162,   163,   164,     0,   309,   165,
     166,   167,   168,   169,   170,   159,   160,   161,   162,   163,
     164,     0,   311,   165,   166,   167,   168,   169,   170,   159,
     160,   161,   162,   163,   164,     0,   315,   165,   166,   167,
     168,   169,   170,   159,   160,   161,   162,   163,   164,     0,
     319,   165,   166,   167,   168,   169,   170,   159,   160,   161,
     162,   163,   164,     0,   321,   165,   166,   167,   168,   169,
     170,   159,   160,   161,   162,   163,   164,     0,   323,   165,
     166,   167,   168,   169,   170,   159,   160,   161,   162,   163,
     164,     0,   325,   165,   166,   167,   168,   169,   170,   159,
     160,   161,   162,   163,   164,     0,   327,   165,   166,   167,
     168,   169,   170,   159,   160,   161,   162,   163,   164,     0,
     329,   165,   166,   167,   168,   169,   170,   159,   160,   161,
     162,   163,   164,     0,   331,   165,   166,   167,   168,   169,
     170,   159,   160,   161,   162,   163,   164,     0,   333,   165,
     166,   167,   168,   169,   170,   159,   160,   161,   162,   163,
     164,     0,   335,   165,   166,   167,   168,   169,   170,   159,
     160,   161,   162,   163,   164,     0,   337,   165,   166,   167,
     168,   169,   170,   159,   160,   161,   162,   163,   164,   184,
       0,   165,   166,   167,   168,   169,   170,   159,   160,   161,
     162,   163,   164,   229,     0,   165,   166,   167,   168,   169,
     170,   159,   160,   161,   162,   163,   164,   268,     0,   165,
     166,   167,   168,   169,   170,     0,     0,     0,   273,   159,
     160,   161,   162,   163,   164,     0,     0,   165,   166,   167,
     168,   169,   170,     0,     0,     0,   296,   159,   160,   161,
     162,   163,   164,     0,     0,   165,   166,   167,   168,   169,
     170,     0,     0,     0,   297,   159,   160,   161,   162,   163,
     164,     0,     0,   165,   166,   167,   168,   169,   170,     0,
       0,     0,   298,   159,   160,   161,   162,   163,   164,     0,
       0,   165,   166,   167,   168,   169,   170,     0,     0,     0,
     310,   159,   160,   161,   162,   163,   164,     0,     0,   165,
     166,   167,   168,   169,   170,     0,     0,     0,   316,   159,
     160,   161,   162,   163,   164,     0,     0,   165,   166,   167,
     168,   169,   170,     0,     0,     0,   339,   159,   160,   161,
     162,   163,   164,     0,   245,   165,   166,   167,   168,   169,
     170,   159,   160,   161,   162,   163,   164,     0,     0,   165,
     166,   167,   168,   169,   170
  };

  /* YYCHECK.  */
  const short int
  rslc_parser::yycheck_[] =
  {
         0,     5,     5,    61,    55,    56,    49,    38,     5,    38,
      39,    40,    41,    42,     0,    63,    16,    38,    39,    40,
      41,    42,     3,    66,    70,    43,    44,    27,    28,    29,
      30,    31,    61,    51,    43,    44,    67,    44,    67,    56,
      57,    58,    51,    43,    51,    45,    67,    65,    91,    49,
      96,    51,   103,   104,    97,    63,    62,    27,    28,   105,
      66,    31,    65,   109,    68,    19,    66,    21,    65,    61,
      70,    43,    44,   119,   120,    45,    64,    62,    66,    51,
      65,    51,    36,    62,    66,    63,    65,   133,    63,    43,
      62,    91,    61,    61,    61,    49,    96,    97,    64,    61,
      66,   147,    61,    61,    65,   105,    38,   153,    65,   109,
      65,    27,    66,    65,    65,    68,    70,    27,    67,   119,
     120,   172,   173,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,   133,    38,    63,    58,    91,    51,    93,
       5,   187,    96,    97,    63,    68,    16,   147,    28,    45,
      93,   105,   263,   153,   239,   109,    -1,    -1,    -1,    45,
      46,    47,    48,    49,    50,   119,   120,    53,    54,    55,
      56,    57,    58,    -1,    -1,    -1,   222,    -1,   229,   133,
     226,    -1,    68,    -1,   230,    -1,   232,   187,    38,    39,
      40,    41,    42,   147,    55,    56,    57,    58,    -1,   153,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    53,    54,    55,    56,    57,    58,    -1,    55,    56,
      -1,    -1,   222,    60,    -1,   271,   226,   273,    -1,    -1,
     230,    -1,   232,   187,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    17,    -1,    -1,    -1,    -1,    -1,
     296,   297,   298,   299,    -1,    62,    -1,    94,    -1,    -1,
      -1,    -1,    -1,    -1,   310,   102,    -1,   104,   222,    -1,
     316,   271,   226,   273,    -1,   112,   230,    -1,   232,    -1,
      -1,    -1,    -1,    -1,   121,    -1,    -1,    -1,    -1,    62,
      -1,   128,   129,   130,   131,    -1,   296,   297,   298,   299,
      -1,    -1,   139,   140,   141,   142,   143,   144,   145,    -1,
     310,    -1,    -1,    -1,    -1,    -1,   316,   271,    -1,   273,
      -1,    -1,   159,   160,   161,   162,   163,   164,   165,   166,
     167,   168,   169,   170,    -1,    -1,    -1,   174,    -1,   176,
      -1,   178,   296,   297,   298,   299,     7,     8,     9,    10,
      11,    12,    13,    14,    -1,    -1,   310,    -1,    -1,    -1,
      -1,    -1,   316,    -1,    45,    46,    47,    48,    49,    50,
      -1,   208,    53,    54,    55,    56,    57,    58,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    68,    -1,    -1,
      -1,    43,   229,    -1,   231,    -1,   233,    49,   235,   236,
      -1,    -1,   239,    -1,    -1,   242,    -1,    -1,   245,    -1,
      -1,    -1,    -1,    -1,    66,    -1,    -1,    -1,    70,    -1,
      -1,   258,   259,   260,   261,   262,   263,    -1,    -1,   266,
      -1,   268,   269,   270,    -1,   272,    -1,   274,    -1,    91,
      -1,    -1,    -1,    -1,    96,    97,   283,    -1,    -1,    -1,
      -1,    -1,    -1,   105,    -1,    -1,    -1,   109,   295,    -1,
      -1,    -1,    -1,   300,   301,    -1,    -1,   119,   120,    -1,
      -1,    -1,   309,    -1,   311,    -1,    -1,    -1,   315,    -1,
      -1,   133,   319,    43,   321,    -1,   323,    -1,   325,    49,
     327,    -1,   329,    -1,   331,   147,   333,    -1,   335,    -1,
     337,   153,    -1,    -1,    -1,    -1,    66,    -1,    -1,    -1,
      70,    45,    46,    47,    48,    49,    50,    -1,    -1,    53,
      54,    55,    56,    57,    58,    -1,    -1,    -1,    62,    -1,
      -1,    91,    66,    -1,    -1,   187,    96,    97,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   105,    -1,    -1,    -1,   109,
       7,     8,     9,    10,    11,    12,    13,    14,    -1,   119,
     120,    -1,    19,    20,    -1,    22,    23,    24,    25,    -1,
     222,    -1,    -1,   133,   226,    -1,    -1,    -1,   230,    -1,
     232,    -1,    -1,    -1,    -1,    -1,    -1,   147,    -1,    -1,
      -1,    -1,    -1,   153,    -1,    -1,    -1,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    -1,
      -1,    -1,    -1,    19,    -1,    21,    -1,    23,    -1,   271,
      -1,   273,    -1,    -1,    -1,    -1,    -1,   187,    -1,    -1,
      -1,    37,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    -1,   296,   297,   298,   299,    54,    -1,
      -1,    -1,    -1,    59,    -1,    61,    62,    -1,   310,    -1,
      -1,    -1,   222,    -1,   316,    -1,   226,    -1,    -1,    -1,
     230,    -1,   232,     3,    -1,    -1,    -1,     7,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    -1,    18,    19,
      -1,    21,    -1,    23,    -1,    -1,    26,    -1,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    -1,    -1,
      -1,   271,    -1,   273,    -1,    45,    46,    47,    48,    49,
      50,    -1,    -1,    53,    54,    55,    56,    57,    58,    -1,
      -1,    -1,    -1,    63,    -1,    65,   296,   297,   298,   299,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     310,    -1,    -1,     3,    -1,    -1,   316,     7,     8,     9,
      10,    11,    12,    13,    14,    15,    16,    -1,    18,    19,
      -1,    21,    -1,    23,    -1,    -1,    26,    -1,    28,    29,
      30,    31,    32,    33,    34,    35,    36,    37,    -1,    -1,
      -1,    -1,    -1,    43,    44,    -1,    -1,    -1,    -1,    -1,
      -1,    51,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,     3,    63,    -1,    65,     7,     8,     9,    10,
      11,    12,    13,    14,    15,    16,    -1,    18,    19,    -1,
      21,    -1,    23,    -1,    -1,    26,    -1,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    45,    46,    47,
      48,    49,    50,    -1,    -1,    53,    54,    55,    56,    57,
      58,    -1,    -1,    -1,    62,    -1,    -1,    -1,    66,    -1,
      -1,     3,    63,    64,    65,     7,     8,     9,    10,    11,
      12,    13,    14,    15,    16,    -1,    18,    19,    -1,    21,
      -1,    23,    -1,    -1,    26,    -1,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    45,    46,    47,    48,
      49,    50,    -1,    -1,    53,    54,    55,    56,    57,    58,
      -1,    -1,    -1,    62,    -1,    -1,    -1,    66,    -1,    -1,
       3,    63,    64,    65,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    -1,    18,    19,    -1,    21,    -1,
      23,    -1,    -1,    26,    -1,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    45,    46,    47,    48,    49,
      50,    -1,    -1,    53,    54,    55,    56,    57,    58,    -1,
      -1,    -1,    62,    -1,    -1,    -1,    66,    -1,    -1,     3,
      63,    64,    65,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,    -1,    18,    19,    -1,    21,    -1,    23,
      -1,    -1,    26,    -1,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    45,    46,    47,    48,    49,    50,
      -1,    -1,    53,    54,    55,    56,    57,    58,    -1,    -1,
      -1,    62,    -1,    -1,    -1,    66,    -1,    -1,     3,    63,
      64,    65,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    -1,    18,    19,    -1,    21,    -1,    23,    -1,
      -1,    26,    -1,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    45,    46,    47,    48,    49,    50,    -1,
      -1,    53,    54,    55,    56,    57,    58,    -1,    -1,    -1,
      62,    -1,    -1,    -1,    66,    -1,    -1,     3,    63,    64,
      65,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    -1,    18,    19,    -1,    21,    -1,    23,    -1,    -1,
      26,    -1,    28,    29,    30,    31,    32,    33,    34,    35,
      36,    37,    45,    46,    47,    48,    49,    50,    -1,    -1,
      53,    54,    55,    56,    57,    58,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    66,    -1,    -1,     3,    63,    64,    65,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      -1,    18,    19,    -1,    21,    -1,    23,    -1,    -1,    26,
      -1,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    45,    46,    47,    48,    49,    50,    -1,    -1,    53,
      54,    55,    56,    57,    58,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    66,    -1,    -1,     3,    63,    64,    65,     7,
       8,     9,    10,    11,    12,    13,    14,    15,    16,    -1,
      18,    19,    -1,    21,    -1,    23,    -1,    -1,    26,    -1,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      45,    46,    47,    48,    49,    50,    -1,    -1,    53,    54,
      55,    56,    57,    58,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    66,    -1,    -1,     3,    63,    64,    65,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    -1,    18,
      19,    -1,    21,    -1,    23,    -1,    -1,    26,    -1,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,     3,
       4,     5,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    -1,    -1,    -1,    -1,    19,    -1,    21,    -1,    23,
      -1,    -1,    -1,    -1,    63,    -1,    65,    -1,    -1,    -1,
      -1,    -1,    -1,    37,    -1,    -1,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    13,    14,    -1,    -1,
      54,    -1,    19,    -1,    21,    59,    23,    61,    62,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      37,    -1,    -1,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    13,    14,    -1,    -1,    54,    -1,    19,
      -1,    21,    59,    23,    61,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    37,    -1,    -1,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      13,    14,    -1,    -1,    54,    -1,    19,    -1,    21,    59,
      23,    61,    -1,    -1,    -1,    -1,    -1,    45,    46,    47,
      48,    49,    50,    -1,    37,    53,    54,    55,    56,    57,
      58,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    66,    -1,
      -1,    54,    -1,    -1,    -1,    -1,    59,    -1,    61,    45,
      46,    47,    48,    49,    50,    -1,    -1,    53,    54,    55,
      56,    57,    58,    45,    46,    47,    48,    49,    50,    -1,
      66,    53,    54,    55,    56,    57,    58,    45,    46,    47,
      48,    49,    50,    -1,    66,    53,    54,    55,    56,    57,
      58,    45,    46,    47,    48,    49,    50,    -1,    66,    53,
      54,    55,    56,    57,    58,    45,    46,    47,    48,    49,
      50,    -1,    66,    53,    54,    55,    56,    57,    58,    45,
      46,    47,    48,    49,    50,    -1,    66,    53,    54,    55,
      56,    57,    58,    45,    46,    47,    48,    49,    50,    -1,
      66,    53,    54,    55,    56,    57,    58,    45,    46,    47,
      48,    49,    50,    -1,    66,    53,    54,    55,    56,    57,
      58,    45,    46,    47,    48,    49,    50,    -1,    66,    53,
      54,    55,    56,    57,    58,    45,    46,    47,    48,    49,
      50,    -1,    66,    53,    54,    55,    56,    57,    58,    45,
      46,    47,    48,    49,    50,    -1,    66,    53,    54,    55,
      56,    57,    58,    45,    46,    47,    48,    49,    50,    -1,
      66,    53,    54,    55,    56,    57,    58,    45,    46,    47,
      48,    49,    50,    -1,    66,    53,    54,    55,    56,    57,
      58,    45,    46,    47,    48,    49,    50,    -1,    66,    53,
      54,    55,    56,    57,    58,    45,    46,    47,    48,    49,
      50,    -1,    66,    53,    54,    55,    56,    57,    58,    45,
      46,    47,    48,    49,    50,    -1,    66,    53,    54,    55,
      56,    57,    58,    45,    46,    47,    48,    49,    50,    65,
      -1,    53,    54,    55,    56,    57,    58,    45,    46,    47,
      48,    49,    50,    65,    -1,    53,    54,    55,    56,    57,
      58,    45,    46,    47,    48,    49,    50,    65,    -1,    53,
      54,    55,    56,    57,    58,    -1,    -1,    -1,    62,    45,
      46,    47,    48,    49,    50,    -1,    -1,    53,    54,    55,
      56,    57,    58,    -1,    -1,    -1,    62,    45,    46,    47,
      48,    49,    50,    -1,    -1,    53,    54,    55,    56,    57,
      58,    -1,    -1,    -1,    62,    45,    46,    47,    48,    49,
      50,    -1,    -1,    53,    54,    55,    56,    57,    58,    -1,
      -1,    -1,    62,    45,    46,    47,    48,    49,    50,    -1,
      -1,    53,    54,    55,    56,    57,    58,    -1,    -1,    -1,
      62,    45,    46,    47,    48,    49,    50,    -1,    -1,    53,
      54,    55,    56,    57,    58,    -1,    -1,    -1,    62,    45,
      46,    47,    48,    49,    50,    -1,    -1,    53,    54,    55,
      56,    57,    58,    -1,    -1,    -1,    62,    45,    46,    47,
      48,    49,    50,    -1,    52,    53,    54,    55,    56,    57,
      58,    45,    46,    47,    48,    49,    50,    -1,    -1,    53,
      54,    55,    56,    57,    58
  };

  /* STOS_[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
  const unsigned char
  rslc_parser::yystos_[] =
  {
         0,     7,     8,     9,    10,    11,    12,    13,    14,    19,
      20,    22,    23,    24,    25,    70,    71,    72,    73,    74,
      75,   107,     0,    72,     3,   112,   112,    61,    61,    15,
      16,    17,    62,    76,    77,    78,    82,   107,    62,    76,
     107,   107,    78,    63,    62,    65,    79,    80,   112,    63,
      62,    18,    19,    21,    23,    26,    28,    29,    30,    31,
      32,    33,    34,    35,    36,    37,    63,    64,    65,    78,
      83,    84,    85,    86,    87,    88,    89,    90,    91,    92,
      93,    94,    95,    96,    97,    98,    99,   105,   108,   109,
     112,    63,    77,    66,    38,    67,    83,    63,    78,     4,
       5,     6,    54,    59,    61,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   112,   113,   114,   115,   116,   100,
     104,    61,     5,    65,     5,    65,   100,   104,    61,    61,
      61,    61,    64,    83,    64,    84,    65,    65,    65,    61,
      38,    39,    40,    41,    42,    67,    64,    83,    80,   100,
       5,    68,    64,    83,   100,   100,   104,   100,   104,    45,
      46,    47,    48,    49,    50,    53,    54,    55,    56,    57,
      58,    84,    43,    44,    51,    84,     6,   100,    67,    84,
      84,   100,    65,    65,    65,   100,   100,    62,   100,   100,
      64,     6,    62,   100,   110,   111,   117,   100,   100,   100,
     100,   100,   100,    64,    68,    38,    64,    62,    66,    62,
     100,   100,   100,   100,   100,   100,   100,   100,   100,   100,
     100,   100,    27,   104,   104,   100,    27,   100,   100,    65,
      62,    66,    62,    66,    84,    66,    66,    67,    62,    66,
      68,    38,    63,   100,    84,    52,    84,    68,   100,   104,
      84,   100,    84,   100,   100,   100,     5,   111,    38,    39,
      40,    41,    42,    63,    81,   100,    66,   100,    65,    65,
      66,    62,    66,    62,    66,    68,   100,   100,   100,   100,
     100,    81,    64,    66,   100,   100,   100,   100,    84,   100,
      84,   100,    64,   100,    62,    66,    62,    62,    62,    62,
      66,    66,   100,    84,    84,    84,    84,   100,   100,    66,
      62,    66,   100,    84,   100,    66,    62,   100,    84,    66,
     100,    66,   100,    66,   100,    66,   100,    66,   100,    66,
     100,    66,   100,    66,   100,    66,   100,    66,   100,    62
  };

#if YYDEBUG
  /* TOKEN_NUMBER_[YYLEX-NUM] -- Internal symbol number corresponding
     to YYLEX-NUM.  */
  const unsigned short int
  rslc_parser::yytoken_number_[] =
  {
         0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,    61,   293,
     294,   295,   296,   297,   298,    60,    62,   299,   300,   301,
     302,    63,    58,    43,    45,    94,    47,    42,    46,    33,
     303,    40,    41,   123,   125,    59,    44,    91,    93
  };
#endif

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
  const unsigned char
  rslc_parser::yyr1_[] =
  {
         0,    69,    70,    71,    71,    72,    72,    73,    73,    73,
      73,    74,    74,    74,    74,    74,    74,    75,    75,    76,
      76,    76,    77,    77,    78,    79,    79,    80,    80,    80,
      80,    80,    81,    81,    82,    82,    82,    83,    83,    84,
      84,    84,    84,    84,    84,    84,    84,    85,    86,    87,
      88,    88,    89,    90,    90,    90,    91,    91,    91,    91,
      91,    91,    92,    92,    93,    93,    94,    94,    95,    95,
      96,    96,    96,    96,    97,    98,    98,    98,    98,    99,
      99,    99,    99,   100,   100,   100,   100,   100,   100,   100,
     100,   100,   100,   101,   101,   101,   101,   101,   101,   101,
     101,   101,   102,   103,   104,   104,   104,   104,   104,   104,
     104,   104,   104,   104,   105,   105,   105,   105,   105,   105,
     105,   105,   105,   105,   106,   106,   107,   107,   107,   107,
     107,   107,   107,   107,   108,   108,   109,   109,   109,   109,
     109,   110,   110,   111,   111,   112,   113,   113,   114,   115,
     116,   117
  };

  /* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
  const unsigned char
  rslc_parser::yyr2_[] =
  {
         0,     2,     1,     1,     2,     1,     1,     8,     7,     7,
       6,     1,     1,     1,     1,     1,     1,     8,     7,     1,
       3,     2,     1,     2,     2,     1,     3,     1,     3,     4,
       8,     7,     1,     3,     2,     2,     1,     1,     2,     1,
       1,     1,     1,     1,     1,     1,     1,     2,     2,     3,
       1,     2,     2,     3,     2,     1,     1,     1,     1,     1,
       1,     1,     3,     3,     9,     9,     4,     7,     5,     9,
       5,     7,     9,    11,    13,     3,     3,     5,     5,     3,
       2,     3,     2,     1,     3,     3,     3,     3,     3,     3,
       2,     5,     1,     1,     1,     1,     4,     1,     1,     3,
       1,     1,     7,    33,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     2,     3,     3,     3,     3,     3,     6,
       6,     6,     6,     6,     2,     3,     1,     1,     1,     1,
       1,     1,     1,     1,     4,     3,     1,     1,     1,     1,
       1,     1,     3,     1,     1,     1,     1,     1,     1,     1,
       1,     4
  };

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
  /* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
     First, the terminals, then, starting at \a yyntokens_, nonterminals.  */
  const char*
  const rslc_parser::yytname_[] =
  {
    "$end", "error", "$undefined", "IDENTIFIER", "FLOAT_LITERAL",
  "INTEGER_LITERAL", "STRING_LITERAL", "TOKEN_FLOAT", "TOKEN_COLOR",
  "TOKEN_POINT", "TOKEN_VECTOR", "TOKEN_NORMAL", "TOKEN_MATRIX",
  "TOKEN_STRING", "TOKEN_VOID", "TOKEN_UNIFORM", "TOKEN_VARYING",
  "TOKEN_OUTPUT", "TOKEN_EXTERN", "TOKEN_SURFACE", "TOKEN_LIGHT",
  "TOKEN_ATMOSPHERE", "TOKEN_VOLUME", "TOKEN_DISPLACEMENT", "TOKEN_IMAGER",
  "TOKEN_TRANSFORMATION", "TOKEN_IF", "TOKEN_ELSE", "TOKEN_WHILE",
  "TOKEN_FOR", "TOKEN_CONTINUE", "TOKEN_BREAK", "TOKEN_RETURN",
  "TOKEN_ILLUMINATE", "TOKEN_ILLUMINANCE", "TOKEN_SOLAR", "TOKEN_GATHER",
  "TOKEN_LIGHTSOURCE", "'='", "TOKEN_ADD_ASSIGN", "TOKEN_SUB_ASSIGN",
  "TOKEN_MUL_ASSIGN", "TOKEN_DIV_ASSIGN", "TOKEN_OR", "TOKEN_AND", "'<'",
  "'>'", "TOKEN_LE", "TOKEN_GE", "TOKEN_EQ", "TOKEN_NE", "'?'", "':'",
  "'+'", "'-'", "'^'", "'/'", "'*'", "'.'", "'!'", "NEG", "'('", "')'",
  "'{'", "'}'", "';'", "','", "'['", "']'", "$accept", "file",
  "definitions", "definition", "shader_definition", "shader_type",
  "function_definition", "parameters", "parameter_variables_definition",
  "type_variables_definition", "variable_definitions",
  "variable_definition", "expression_array", "variable_type", "statements",
  "statement", "assign_statement", "function_call_statement",
  "return_statement", "statement_variables_definition",
  "definition_statement", "block_statement", "loop_statement",
  "while_statement", "for_statement", "solar_statement",
  "illuminate_statement", "illuminance_statement", "gather_statement",
  "if_statement", "loop_mod_statement", "expression", "primary", "tuple3",
  "tuple16", "relation", "assign_expression", "cast_expression", "type",
  "function_call", "function_name", "function_arguments",
  "function_argument", "identifier", "number", "integer", "float",
  "string", "texture_name", 0
  };
#endif

#if YYDEBUG
  /* YYRHS -- A `-1'-separated list of the rules' RHS.  */
  const rslc_parser::rhs_number_type
  rslc_parser::yyrhs_[] =
  {
        70,     0,    -1,    71,    -1,    72,    -1,    71,    72,    -1,
      73,    -1,    75,    -1,    74,   112,    61,    76,    62,    63,
      83,    64,    -1,    74,   112,    61,    76,    62,    63,    64,
      -1,    74,   112,    61,    62,    63,    83,    64,    -1,    74,
     112,    61,    62,    63,    64,    -1,    20,    -1,    19,    -1,
      22,    -1,    23,    -1,    25,    -1,    24,    -1,   107,   112,
      61,    76,    62,    63,    83,    64,    -1,   107,   112,    61,
      62,    63,    83,    64,    -1,    77,    -1,    76,    65,    77,
      -1,    76,    65,    -1,    78,    -1,    17,    78,    -1,    82,
      79,    -1,    80,    -1,    79,    66,    80,    -1,   112,    -1,
     112,    38,   100,    -1,   112,    67,     5,    68,    -1,   112,
      67,     5,    68,    38,    63,    81,    64,    -1,   112,    67,
      68,    38,    63,    81,    64,    -1,   100,    -1,    81,    66,
     100,    -1,    15,   107,    -1,    16,   107,    -1,   107,    -1,
      84,    -1,    83,    84,    -1,    85,    -1,    86,    -1,    87,
      -1,    99,    -1,    89,    -1,    90,    -1,    91,    -1,    98,
      -1,   105,    65,    -1,   108,    65,    -1,    32,   100,    65,
      -1,    78,    -1,    18,    78,    -1,    88,    65,    -1,    63,
      83,    64,    -1,    63,    64,    -1,    65,    -1,    92,    -1,
      93,    -1,    94,    -1,    95,    -1,    96,    -1,    97,    -1,
      28,   104,    84,    -1,    28,   100,    84,    -1,    29,    61,
     100,    65,   104,    65,   100,    62,    84,    -1,    29,    61,
     100,    65,   100,    65,   100,    62,    84,    -1,    35,    61,
      62,    84,    -1,    35,    61,   100,    66,   100,    62,    84,
      -1,    33,    61,   100,    62,    84,    -1,    33,    61,   100,
      66,   100,    66,   100,    62,    84,    -1,    34,    61,   100,
      62,    84,    -1,    34,    61,   100,    66,   100,    62,    84,
      -1,    34,    61,   100,    66,   100,    66,   100,    62,    84,
      -1,    34,    61,   100,    66,   100,    66,   100,    66,   100,
      62,    84,    -1,    36,    61,   100,    66,   100,    66,   100,
      66,   100,    66,   100,    62,    84,    -1,    26,   104,    84,
      -1,    26,   100,    84,    -1,    26,   104,    84,    27,    84,
      -1,    26,   100,    84,    27,    84,    -1,    31,     5,    65,
      -1,    31,    65,    -1,    30,     5,    65,    -1,    30,    65,
      -1,   101,    -1,   100,    58,   100,    -1,   100,    56,   100,
      -1,   100,    57,   100,    -1,   100,    55,   100,    -1,   100,
      53,   100,    -1,   100,    54,   100,    -1,    54,   100,    -1,
     104,    51,   100,    52,   100,    -1,   106,    -1,   113,    -1,
     116,    -1,   112,    -1,   112,    67,   100,    68,    -1,   108,
      -1,   105,    -1,    61,   100,    62,    -1,   102,    -1,   103,
      -1,    61,   100,    66,   100,    66,   100,    62,    -1,    61,
     100,    66,   100,    66,   100,    66,   100,    66,   100,    66,
     100,    66,   100,    66,   100,    66,   100,    66,   100,    66,
     100,    66,   100,    66,   100,    66,   100,    66,   100,    66,
     100,    62,    -1,    61,   104,    62,    -1,   100,    46,   100,
      -1,   100,    48,   100,    -1,   100,    45,   100,    -1,   100,
      47,   100,    -1,   100,    49,   100,    -1,   100,    50,   100,
      -1,   104,    44,   104,    -1,   104,    43,   104,    -1,    59,
     104,    -1,   112,    38,   100,    -1,   112,    39,   100,    -1,
     112,    40,   100,    -1,   112,    41,   100,    -1,   112,    42,
     100,    -1,   112,    67,   100,    68,    38,   100,    -1,   112,
      67,   100,    68,    39,   100,    -1,   112,    67,   100,    68,
      40,   100,    -1,   112,    67,   100,    68,    41,   100,    -1,
     112,    67,   100,    68,    42,   100,    -1,   107,   100,    -1,
     107,     6,   100,    -1,     7,    -1,    13,    -1,     8,    -1,
       9,    -1,    10,    -1,    11,    -1,    12,    -1,    14,    -1,
     109,    61,   110,    62,    -1,   109,    61,    62,    -1,   112,
      -1,    21,    -1,    23,    -1,    37,    -1,    19,    -1,   111,
      -1,   110,    66,   111,    -1,   100,    -1,   117,    -1,     3,
      -1,   114,    -1,   115,    -1,     5,    -1,     4,    -1,     6,
      -1,     6,    67,     5,    68,    -1
  };

  /* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
     YYRHS.  */
  const unsigned short int
  rslc_parser::yyprhs_[] =
  {
         0,     0,     3,     5,     7,    10,    12,    14,    23,    31,
      39,    46,    48,    50,    52,    54,    56,    58,    67,    75,
      77,    81,    84,    86,    89,    92,    94,    98,   100,   104,
     109,   118,   126,   128,   132,   135,   138,   140,   142,   145,
     147,   149,   151,   153,   155,   157,   159,   161,   164,   167,
     171,   173,   176,   179,   183,   186,   188,   190,   192,   194,
     196,   198,   200,   204,   208,   218,   228,   233,   241,   247,
     257,   263,   271,   281,   293,   307,   311,   315,   321,   327,
     331,   334,   338,   341,   343,   347,   351,   355,   359,   363,
     367,   370,   376,   378,   380,   382,   384,   389,   391,   393,
     397,   399,   401,   409,   443,   447,   451,   455,   459,   463,
     467,   471,   475,   479,   482,   486,   490,   494,   498,   502,
     509,   516,   523,   530,   537,   540,   544,   546,   548,   550,
     552,   554,   556,   558,   560,   565,   569,   571,   573,   575,
     577,   579,   581,   585,   587,   589,   591,   593,   595,   597,
     599,   601
  };

  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
  const unsigned short int
  rslc_parser::yyrline_[] =
  {
         0,   220,   220,   226,   234,   242,   251,   263,   280,   305,
     330,   365,   372,   379,   386,   393,   400,   410,   420,   435,
     444,   450,   457,   461,   471,   485,   498,   511,   524,   537,
     551,   565,   582,   591,   600,   606,   612,   619,   632,   645,
     649,   653,   657,   661,   665,   669,   673,   681,   690,   699,
     714,   723,   738,   752,   773,   784,   799,   803,   807,   811,
     815,   819,   825,   839,   856,   872,   891,   904,   922,   936,
     955,   969,   984,  1000,  1020,  1040,  1051,  1062,  1073,  1087,
    1093,  1099,  1105,  1114,  1118,  1125,  1132,  1139,  1146,  1152,
    1159,  1171,  1183,  1189,  1193,  1197,  1210,  1223,  1227,  1231,
    1238,  1242,  1249,  1268,  1289,  1301,  1313,  1325,  1337,  1349,
    1361,  1373,  1385,  1397,  1412,  1432,  1452,  1472,  1492,  1512,
    1532,  1552,  1572,  1592,  1615,  1624,  1638,  1645,  1651,  1658,
    1665,  1672,  1679,  1686,  1696,  1716,  1746,  1750,  1758,  1766,
    1774,  1785,  1798,  1810,  1819,  1831,  1851,  1860,  1872,  1888,
    1904,  1921
  };

  // Print the state stack on the debug stream.
  void
  rslc_parser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (state_stack_type::const_iterator i = yystate_stack_.begin ();
	 i != yystate_stack_.end (); ++i)
      *yycdebug_ << ' ' << *i;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  rslc_parser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    /* Print the symbols being reduced, and their result.  */
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
	       << " (line " << yylno << "), ";
    /* The symbols being reduced.  */
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
		       yyrhs_[yyprhs_[yyrule] + yyi],
		       &(yysemantic_stack_[(yynrhs) - (yyi + 1)]),
		       &(yylocation_stack_[(yynrhs) - (yyi + 1)]));
  }
#endif // YYDEBUG

  /* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
  rslc_parser::token_number_type
  rslc_parser::yytranslate_ (int t)
  {
    static
    const token_number_type
    translate_table[] =
    {
           0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    59,     2,     2,     2,     2,     2,     2,
      61,    62,    57,    53,    66,    54,    58,    56,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    52,    65,
      45,    38,    46,    51,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,    67,     2,    68,    55,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    63,     2,    64,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    39,    40,    41,    42,    43,    44,    47,
      48,    49,    50,    60
    };
    if ((unsigned int) t <= yyuser_token_number_max_)
      return translate_table[t];
    else
      return yyundef_token_;
  }

  const int rslc_parser::yyeof_ = 0;
  const int rslc_parser::yylast_ = 1894;
  const int rslc_parser::yynnts_ = 49;
  const int rslc_parser::yyempty_ = -2;
  const int rslc_parser::yyfinal_ = 22;
  const int rslc_parser::yyterror_ = 1;
  const int rslc_parser::yyerrcode_ = 256;
  const int rslc_parser::yyntokens_ = 69;

  const unsigned int rslc_parser::yyuser_token_number_max_ = 303;
  const rslc_parser::token_number_type rslc_parser::yyundef_token_ = 2;

} // namespace yy

#line 1939 "rslc_parser.yy"


void yy::rslc_parser::error(const yy::rslc_parser::location_type& loc, const std::string& msg)
{
	std::string fname = GET_FNAME();
	int lineno = GET_LINE();
	fprintf(stderr, "parser error %s :%s(%d)\n", msg.c_str(), fname.c_str(), lineno);
}

extern int _rslc_lex_init(void** p);
extern int _rslc_lex_destroy(void* p);

namespace rslc
{
	int load_rsl(rslc_context* ctx, rslc_decoder* dec, const char* szFileName)
	{
		void* scanner;
		_rslc_lex_init(&scanner);

		rslc_parse_param* param = (rslc_parse_param*)(scanner);
		param->ctx = ctx;
		param->dec = dec;
		param->line_number = 1;
		param->filename = szFileName;

		yy::rslc_parser parser(scanner);

		int nRet = parser.parse();

		_rslc_lex_destroy(scanner);
		return nRet;
	}
}

