--Array
array_meta_ = {}
array_meta_.l__ = {}
array_meta_.get_at = function(self, i) return self.l__[i+1] end
array_meta_.set_at = function(self, i, x) self.l__[i+1] = x end
array_meta_.length = function(self) return #self.l__ end
array_meta_.add_at = function(self, i, x) self.l__[i+1] = self.l__[i+1] + x end
array_meta_.sub_at = function(self, i, x) self.l__[i+1] = self.l__[i+1] - x end
array_meta_.mul_at = function(self, i, x) self.l__[i+1] = self.l__[i+1] * x end
array_meta_.div_at = function(self, i, x) self.l__[i+1] = self.l__[i+1] / x end

function array(l)
    local obj = {}
    obj.l__ = l
    setmetatable(obj, {__index=array_meta_})
	return obj
end
--Array
