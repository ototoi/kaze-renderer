#include "rslc_node_manager.h"
#include <vector>
#include <map>

namespace rslc
{
    typedef rslc_node_manager::map_type map_type;

    void rslc_node_manager::add_node(const std::shared_ptr<rslc_node>& node)
    {
        if(node.get())
        {
            rslc_node* ptr = node.get();
            nodes_.insert(std::make_pair(ptr, node));
        }
    }

    void rslc_node_manager::remove_node(rslc_node* node)
    {
        map_type::const_iterator it = nodes_.find(node);
        if(it != nodes_.end())
        {
            nodes_.erase(it);
        }
    }

    std::shared_ptr<rslc_node> rslc_node_manager::find_node(rslc_node* node)const
    {
        map_type::const_iterator it = nodes_.find(node);
        if(it != nodes_.end())
        {
            return it->second;
        }
        else
        {
            return std::shared_ptr<rslc_node>();
        }
    }
}
