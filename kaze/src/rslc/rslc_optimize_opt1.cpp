#include "rslc_optimize_opt1.h"
#include "rslc_context.h"
#include "rslc_node.h"
#include "rslc_node_walker.h"

#include <iostream>
#include <vector>
#include <cassert>

#define CAST(T, n) check_cast<T>(n)

namespace rslc
{
    template<class T, class B>
    T check_cast(B b)
    {
        T t = dynamic_cast<T>(b);
        return t;
    }

namespace
{
    static
    void rslc_params_flatten(rslc_context* ctx, rslc_list_node* node)
    {
        const std::vector<rslc_node*>& vec = node->get_nodes();

        std::vector<rslc_node*> nvec;
        size_t sz = vec.size();
        for(size_t i=0;i<sz;i++)
        {
            rslc_type_variables_def_node* tvnode = CAST(rslc_type_variables_def_node*, vec[i]);
            rslc_list_node* vsnode = CAST(rslc_list_node*, tvnode->get_variables_node());
            size_t jsz = vsnode->get_nodes_size();
            if(jsz == 1)
            {
                nvec.push_back(vec[i]);
            }
            else
            {
                for(size_t j=0;j<jsz;j++)
                {
                    rslc_node* vnode = vsnode->get_node_at(j);

                    std::shared_ptr<rslc_node> t_(new rslc_type_node(*tvnode->get_type_node()));
                    ctx->add_node(t_);
                    rslc_type_node* t = CAST(rslc_type_node*, t_.get());

                    if(vnode->get_node_type() == RSLC_NODE_VARIABLE_ARRAY_REF)
                    {
                        t->set_array(true);
                    }

                    std::shared_ptr<rslc_node> v_(new rslc_list_node());
                    ctx->add_node(v_);
                    rslc_list_node* v = CAST(rslc_list_node*, v_.get());
                    v->add_node(vnode);



                    std::shared_ptr<rslc_node> tv_(new rslc_type_variables_def_node(t, v));
                    ctx->add_node(tv_);
                    nvec.push_back(tv_.get());
                }
            }
        }
        node->clear_nodes();
        sz = nvec.size();
        for(size_t i=0;i<sz;i++)
        {
            node->add_node(nvec[i]);
        }
    }

    static
    std::vector<rslc_node*> get_variable_defs(rslc_context* ctx, rslc_node* node)
    {
        std::vector<rslc_node*> vv;
        if(node->get_node_type() == RSLC_NODE_STATEMENT)
        {
            rslc_statement_node* pnode = CAST(rslc_statement_node*, node);
            if(pnode->get_contents_node()->get_node_type() == RSLC_NODE_TYPE_VARIABLES_DEF)
            {
                rslc_type_variables_def_node* tvnode = CAST(rslc_type_variables_def_node*, pnode->get_contents_node());
                rslc_list_node* vsnode = CAST(rslc_list_node*, tvnode->get_variables_node());
                size_t jsz = vsnode->get_nodes_size();
                if(jsz > 1)
                {
                    for(size_t j=0;j<jsz;j++)
                    {
                        rslc_node* vnode = vsnode->get_node_at(j);

                        std::shared_ptr<rslc_node> t_(new rslc_type_node(*tvnode->get_type_node()));
                        ctx->add_node(t_);
                        rslc_type_node* t = CAST(rslc_type_node*, t_.get());

                        if(vnode->get_node_type() == RSLC_NODE_VARIABLE_ARRAY_REF)
                        {
                            t->set_array(true);
                        }

                        std::shared_ptr<rslc_node> v_(new rslc_list_node());
                        ctx->add_node(v_);
                        rslc_list_node* v = CAST(rslc_list_node*, v_.get());
                        v->add_node(vnode);

                        std::shared_ptr<rslc_node> tv_(new rslc_type_variables_def_node(t, v));
                        ctx->add_node(tv_);

                        std::shared_ptr<rslc_node> pv_(new rslc_statement_node(tv_.get()));
                        ctx->add_node(pv_);

                        vv.push_back(pv_.get());
                    }
                }
            }
        }
        return vv;
    }

    static
    void rslc_variables_flatten(rslc_context* ctx, rslc_list_node* node)
    {
        const std::vector<rslc_node*>& vec = node->get_nodes();

        std::vector<rslc_node*> nvec;
        size_t sz = vec.size();
        for(size_t i=0;i<sz;i++)
        {
            std::vector<rslc_node*> vv = get_variable_defs(ctx, vec[i]);
            if(!vv.empty())
            {
                for(int j=0;j<vv.size();j++)
                {
                    nvec.push_back(vv[j]);
                }
            }
            else
            {
                nvec.push_back(vec[i]);
            }
        }
        node->clear_nodes();
        sz = nvec.size();
        for(size_t i=0;i<sz;i++)
        {
            node->add_node(nvec[i]);
        }
    }

    class func_args_walker : public rslc_node_walker
    {
    public:
        func_args_walker(rslc_context* ctx)
            :ctx_(ctx)
        {}
        int walk(rslc_node* node)
        {
            return rslc_node_walker::walk(node);
        }
        int walk(rslc_function_def_node* node)
        {
            rslc_params_flatten(ctx_, (rslc_list_node*)node->get_arguments_node());
            rslc_variables_flatten(ctx_, (rslc_list_node*)node->get_contents_node());
            return rslc_node_walker::walk(node);
        }
        int walk(rslc_shader_def_node* node)
        {
            rslc_params_flatten(ctx_, (rslc_list_node*)node->get_arguments_node());
            rslc_variables_flatten(ctx_, (rslc_list_node*)node->get_contents_node());
            return rslc_node_walker::walk(node);
        }

        int walk(rslc_block_node* node)
        {
            rslc_variables_flatten(ctx_, (rslc_list_node*)node);
            return rslc_node_walker::walk(node);
        }
    protected:
        rslc_context* ctx_;
    };

    class condition_stat_walker : public rslc_node_walker
    {
    public:
        condition_stat_walker(rslc_context* ctx)
            :ctx_(ctx)
        {}
        int walk(rslc_node* node)
        {
            return rslc_node_walker::walk(node);
        }
        int walk(rslc_while_node* node)
        {
            rslc_list_node* cond = CAST(rslc_list_node*, node->get_condition_node());
            if(cond && cond->get_nodes_size() == 1)
            {
                rslc_node* n = cond->get_node_at(0); 
                if(n->get_node_type() == RSLC_NODE_PAREN)
                {
                    rslc_paren_node* p = CAST(rslc_paren_node*, n);
                    cond->clear_nodes();
                    cond->add_node(p->get_contents_node());
                    ctx_->remove_node(p);
                }
            }
            return rslc_node_walker::walk(node);
        }
        int walk(rslc_if_node* node)
        {
            rslc_node* cond = node->get_condition_node();
            if(cond->get_node_type() == RSLC_NODE_PAREN)
            {
                rslc_paren_node* p = CAST(rslc_paren_node*, cond);
                node->set_condition_node(p->get_contents_node());
                ctx_->remove_node(p);
            }
            return rslc_node_walker::walk(node);
        }
    protected:
        rslc_context* ctx_;
    };
}


    int rslc_optimize_func_args(rslc_context* ctx, rslc_node* node)
    {
        {
            func_args_walker w(ctx);
            int nRet = w.walk(node);
            if(nRet != 0)return nRet;
        }
        {
            condition_stat_walker w(ctx);
            int nRet = w.walk(node);
            if(nRet != 0)return nRet;
        }
        return 0;
    }
}
