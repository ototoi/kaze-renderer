#ifndef RSLC_TYPE_H
#define RSLC_TYPE_H

#include <vector>
#include <string>

namespace rslc
{
    class rslc_type
    {
    public:
        rslc_type(const std::string& name)
            :name_(name), is_array_(false)
        {}
        const std::string& get_name()const
        {
            return name_;
        }
        void set_name(const std::string& name)
        {
            name_ = name;
        }
        void add_attribute(const std::string& attr)
        {
            if(!find_attribute(attr))
            {
                attrs_.push_back(attr);
            }
        }
        const std::vector<std::string>& get_attributes()const
        {
            return attrs_;
        }
        bool find_attribute(const std::string& attr)const
        {
            if(std::find(attrs_.begin(), attrs_.end(), attr) != attrs_.end())
            {
                return true;
            }
            return false;
        }
        void set_array(bool bArray)
        {
            is_array_ = bArray;
        }
        bool is_array()const
        {
            return is_array_;
        }

        bool equal(const rslc_type& rhs)const 
        {
            return (name_ == rhs.name_) && (is_array_ && rhs.is_array_);
        }
    private:
        std::string name_;
        bool is_array_;
        std::vector<std::string> attrs_;
    };

    inline bool operator==(const rslc_type& lhs, const rslc_type& rhs)
    {
        return lhs.equal(rhs);
    }
}

#endif
