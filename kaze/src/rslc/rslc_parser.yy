%skeleton "lalr1.cc"
%define "parser_class_name" "rslc_parser"
%defines

%{
#ifdef _WIN32
#pragma warning(disable : 4786)
#pragma warning(disable : 4102)
#pragma warning(disable : 4996)
#pragma warning(disable : 4065)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define YYDEBUG 1
#define YYERROR_VERBOSE 1

#include <algorithm>
#include <iterator>
#include "rslc_parse_param.h"
#include "rslc_context.h"

%}

%union{
	int itype;
	float ftype;
	char ctype;
	char* stype;
	rslc::rslc_node* ntype;
}

%pure_parser

// The parsing context.
%parse-param { void * scanner }
%lex-param   { void * scanner }

%locations
// %debug
%error-verbose

%{

#define yyerror(m) error(yylloc,m)

using namespace rslc;

static
void release(const char* v){delete[] v;v=NULL;}

static
rslc_list_node* create_list_node(rslc_context* ctx)
{
	std::shared_ptr<rslc_node> l(new rslc_list_node());
	ctx->add_node(l);
	return (rslc_list_node*)(l.get());
}

#define GET_CTX() (((rslc_parse_param*)(scanner))->ctx)
#define GET_FNAME() (((rslc_parse_param*)(scanner))->filename)
#define GET_LINE() (((rslc_parse_param*)(scanner))->line_number)
#define GET_LOC() (rslc_location(GET_FNAME(),GET_LINE(),0))
#define DEBUG(s)  (GET_CTX()->print_debug(s))
#define MAKE_LIST()  (create_list_node(GET_CTX()))



#define	YY_DECL	1

extern int _rslc_lex(yy::rslc_parser::semantic_type* yylval,
	 yy::rslc_parser::location_type* yylloc,
	 void* scanner);

static
int yylex(yy::rslc_parser::semantic_type* yylval,
	 yy::rslc_parser::location_type* yylloc,
	 void* scanner)
{
	return _rslc_lex(yylval, yylloc, scanner);
}


%}

/*tokens*/

%token <stype>	IDENTIFIER

%token <ftype>	FLOAT_LITERAL
%token <itype>	INTEGER_LITERAL
%token <stype>	STRING_LITERAL

/*types*/
%token <ctype> TOKEN_FLOAT
%token <ctype> TOKEN_COLOR
%token <ctype> TOKEN_POINT
%token <ctype> TOKEN_VECTOR
%token <ctype> TOKEN_NORMAL
%token <ctype> TOKEN_MATRIX
%token <ctype> TOKEN_STRING
%token <ctype> TOKEN_VOID

/*strage class*/
%token <ctype> TOKEN_UNIFORM
%token <ctype> TOKEN_VARYING

%token <ctype> TOKEN_OUTPUT
%token <ctype> TOKEN_EXTERN

/*shader type*/
%token <ctype> TOKEN_SURFACE
%token <ctype> TOKEN_LIGHT
%token <ctype> TOKEN_ATMOSPHERE
%token <ctype> TOKEN_VOLUME
%token <ctype> TOKEN_DISPLACEMENT
%token <ctype> TOKEN_IMAGER
%token <ctype> TOKEN_TRANSFORMATION

/*block statement constructs*/
%token <ctype> TOKEN_IF
%token <ctype> TOKEN_ELSE
%token <ctype> TOKEN_WHILE
%token <ctype> TOKEN_FOR
%token <ctype> TOKEN_CONTINUE
%token <ctype> TOKEN_BREAK
%token <ctype> TOKEN_RETURN

%token <ctype> TOKEN_ILLUMINATE
%token <ctype> TOKEN_ILLUMINANCE
%token <ctype> TOKEN_SOLAR
%token <ctype> TOKEN_GATHER

%token <ctype> TOKEN_LIGHTSOURCE

/* NOTE: These are priorities in ascending precedence, operators on the same line have the same precedence. */
%right <ctype> '='
%right <ctype> TOKEN_ADD_ASSIGN
%right <ctype> TOKEN_SUB_ASSIGN
%right <ctype> TOKEN_MUL_ASSIGN
%right <ctype> TOKEN_DIV_ASSIGN

%left <ctype> TOKEN_OR
%left <ctype> TOKEN_AND

%left <ctype>	'<'
%left <ctype>	'>'
%left <ctype>	TOKEN_LE
%left <ctype>	TOKEN_GE
%left <ctype>	TOKEN_EQ
%left <ctype>	TOKEN_NE

%right <ctype>	'?' ':'

%left <ctype>	'+' '-'
%left <ctype>	'^'
%left <ctype>	'/' '*'
%left <ctype>	'.'
%right <ctype>	'!' NEG
%left <ctype>	'(' ')'

%type  <ntype>	definitions
%type  <ntype>	definition
%type  <ntype>	shader_definition
%type  <ntype>	function_definition
%type  <ntype>	shader_type
%type  <ntype>	parameters
%type  <ntype>	parameter_variables_definition
%type  <ntype>	statement_variables_definition
%type  <ntype>	type_variables_definition
%type  <ntype>	variable_type
%type  <ntype>	variable_definitions
%type  <ntype>	variable_definition
%type  <ntype>	expression_array
%type  <ntype>	type
%type  <ntype>	statements
%type  <ntype>	statement

%type  <ntype>	function_call_statement
%type  <ntype>	assign_statement
%type  <ntype>	definition_statement
%type  <ntype>	return_statement
%type  <ntype>	block_statement
%type  <ntype>	if_statement
%type  <ntype>	loop_statement
%type  <ntype>	while_statement
%type  <ntype>	for_statement
%type  <ntype>	solar_statement
%type  <ntype>	illuminate_statement
%type  <ntype>	illuminance_statement
%type  <ntype>  gather_statement
%type  <ntype>	loop_mod_statement


%type  <ntype>	expression
%type  <ntype>	primary
%type  <ntype>	tuple3
%type  <ntype>	tuple16
%type  <ntype>	relation
%type  <ntype>	assign_expression
%type  <ntype>	cast_expression

%type  <ntype>  function_call
%type  <ntype>  function_name
%type  <ntype>	function_arguments
%type  <ntype>	function_argument

%type  <ntype>	identifier
%type  <ntype>	number
%type  <ntype>	integer
%type  <ntype>	float
%type  <ntype>	string
%type  <ntype>	texture_name

%start file
%%

file
	:	definitions
		{
			GET_CTX()->set_root_node($1);
		}

definitions
	:	definition
		{
            std::shared_ptr<rslc_node> node(new rslc_definitions_node());
			GET_CTX()->add_node(node);
			((rslc_list_node*)(node.get()))->add_node($1);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	definitions definition
		{
            ((rslc_list_node*)($1))->add_node($2);
			$$ = $1;
		}
	;

definition
	:	shader_definition
		{
			$$ = $1;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("shader_definition");
			}
		}
	|	function_definition
		{
			$$ = $1;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("function_definition");
			}
		}
	;

shader_definition
	:	shader_type identifier '(' parameters ')' '{' statements '}'
		{
			std::string name = ((rslc_identifier_node*)($2))->get_value();
			rslc_node* params = $4;
			rslc_node* contents = $7;
			std::shared_ptr<rslc_node> node(new rslc_shader_def_node($1, name, params, contents));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "shader_definition:";
				s += (const std::string&)(((rslc_identifier_node*)($2))->get_value());
				DEBUG(s.c_str());
			}
		}
	|	shader_type identifier '(' parameters ')' '{' '}'
		{
			std::string name = ((rslc_identifier_node*)($2))->get_value();
			rslc_node* params = $4;
			rslc_node* contents = NULL;

			{
				std::shared_ptr<rslc_node> n(new rslc_list_node());
				n->set_location(GET_LOC());
				GET_CTX()->add_node(n);
				contents = n.get();
			}	

			std::shared_ptr<rslc_node> node(new rslc_shader_def_node($1, name, params, contents));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "shader_definition:";
				s += (const std::string&)(((rslc_identifier_node*)($2))->get_value());
				DEBUG(s.c_str());
			}
		}
	|	shader_type identifier '(' ')' '{' statements '}'
		{
			std::string name = ((rslc_identifier_node*)($2))->get_value();
			rslc_node* params = NULL;
			rslc_node* contents = $6;

			{
				std::shared_ptr<rslc_node> n(new rslc_list_node());
				n->set_location(GET_LOC());
				GET_CTX()->add_node(n);
				params = n.get();
			}

			std::shared_ptr<rslc_node> node(new rslc_shader_def_node($1, name, params, contents));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "shader_definition:";
				s += (const std::string&)(((rslc_identifier_node*)($2))->get_value());
				DEBUG(s.c_str());
			}
		}
	|	shader_type identifier '(' ')' '{' '}'
		{
			std::string name = ((rslc_identifier_node*)($2))->get_value();
			rslc_node* params = NULL;
			rslc_node* contents = NULL;

			{
				std::shared_ptr<rslc_node> n(new rslc_list_node());
				n->set_location(GET_LOC());
				GET_CTX()->add_node(n);
				params = n.get();
			}

			{
				std::shared_ptr<rslc_node> n(new rslc_list_node());
				n->set_location(GET_LOC());
				GET_CTX()->add_node(n);
				contents = n.get();
			}

			std::shared_ptr<rslc_node> node(new rslc_shader_def_node($1, name, params, contents));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "shader_definition:";
				s += (const std::string&)(((rslc_identifier_node*)($2))->get_value());
				DEBUG(s.c_str());
			}
		}
	;

shader_type
	:	TOKEN_LIGHT
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("light"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_SURFACE
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("surface"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_VOLUME	
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("volume"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_DISPLACEMENT
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("displacement"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_TRANSFORMATION
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("transformation"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_IMAGER	
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("imager"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	;

function_definition
	:	type identifier '(' parameters ')' '{' statements '}'
		{
			std::string name = ((rslc_identifier_node*)($2))->get_value();
			rslc_node* params = $4;
			std::shared_ptr<rslc_node> node(new rslc_function_def_node($1, name, params, $7));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
		}
	|	type identifier '(' ')' '{' statements '}'
		{
			std::string name = ((rslc_identifier_node*)($2))->get_value();
			std::shared_ptr<rslc_node> pnode(new rslc_list_node());
			GET_CTX()->add_node(pnode);
			rslc_node* params = pnode.get();
			std::shared_ptr<rslc_node> node(new rslc_function_def_node($1, name, params, $6));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
		}
	;

parameters
	:	parameter_variables_definition
		{
			std::shared_ptr<rslc_node> node(new rslc_list_node());
			GET_CTX()->add_node(node);
			((rslc_list_node*)(node.get()))->add_node($1);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
		}
	|	parameters ';' parameter_variables_definition
		{
			((rslc_list_node*)($1))->add_node($3);
			$$ = $1;
			;
		}
	|	parameters ';'
		{
			$$ = $1;
		}
	;

parameter_variables_definition
	:	type_variables_definition
		{
			$$ = $1;
		}
	|	TOKEN_OUTPUT type_variables_definition
		{
			rslc_type_variables_def_node* n0 = (rslc_type_variables_def_node*)$2;
			rslc_type_node* type = n0->get_type_node();
			type->add_attribute("output");
			$$ = n0;
		}
	;

type_variables_definition
	:	variable_type variable_definitions
		{
			std::shared_ptr<rslc_node> node(new rslc_type_variables_def_node(((rslc_type_node*)$1), ((rslc_list_node*)$2)));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definitions ',' variable_definition");
			}
		}
	;

variable_definitions
	:	variable_definition
		{
			std::shared_ptr<rslc_node> node(new rslc_list_node());
			GET_CTX()->add_node(node);
			((rslc_list_node*)(node.get()))->add_node($1);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definitions ',' variable_definition");
			}
		}
	|	variable_definitions ',' variable_definition
		{
			((rslc_list_node*)($1))->add_node($3);
			$$ = $1;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definitions ',' variable_definition");
			}
		}
	;

variable_definition
    :	identifier
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node(new rslc_variable_def_node(name));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definition:identifier");
			}
		}
	|	identifier '=' expression
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node(new rslc_variable_def_node(name, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definition:identifier '=' expression");
			}
		}
	|	identifier '[' INTEGER_LITERAL ']'
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			int size = $3;
			std::shared_ptr<rslc_node> node(new rslc_variable_array_def_node(name, size));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definition:identifier '[' INTEGER_LITERAL ']'");
			}
		}
	|	identifier '[' INTEGER_LITERAL ']' '=' '{' expression_array '}'
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			int size = $3;
			std::shared_ptr<rslc_node> node(new rslc_variable_array_def_node(name, size, $7));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definition:identifier '[' INTEGER_LITERAL ']' '=' '{' expression_array '}'");
			}
		}
	|	identifier '[' ']' '=' '{' expression_array '}'
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			int size = ((rslc_list_node*)($6))->get_nodes_size();
			std::shared_ptr<rslc_node> node(new rslc_variable_array_def_node(name, size, $6));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("variable_definition:identifier '[' ']' '=' '{' expression_array '}'");
			}
		}
	;

expression_array
	:	expression			
		{
			std::shared_ptr<rslc_node> node(new rslc_list_node());
			GET_CTX()->add_node(node);
			((rslc_list_node*)(node.get()))->add_node($1);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
		}
	|	expression_array ',' expression
		{
			((rslc_list_node*)($1))->add_node($3);
			$$ = $1;
			;
		}
	;

variable_type
	:	TOKEN_UNIFORM type
		{
			rslc_type_node* type = (rslc_type_node*)$2;
			type->add_attribute("uniform");
			$$ = type;
		}
    |	TOKEN_VARYING type
		{
			rslc_type_node* type = (rslc_type_node*)$2;
			type->add_attribute("varying");
			$$ = type;
		}
	|	type
		{
			$$ = $1;
		}
	;

statements
	:	statement
		{
			std::shared_ptr<rslc_node> node(new rslc_list_node());
			GET_CTX()->add_node(node);
			((rslc_list_node*)(node.get()))->add_node($1);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statements:statement");
			}
		}
	|	statements statement
		{
			((rslc_list_node*)($1))->add_node($2);
			$$ = $1;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statements:statements statement");
			}
		}
	;

statement
	:	assign_statement
		{
			$$ = $1;
		}
	|	function_call_statement
		{
			$$ = $1;
		}
	|	return_statement
		{
			$$ = $1;
		}
	|	loop_mod_statement
		{
			$$ = $1;
		}
	|	definition_statement
		{
			$$ = $1;
		}
	|	block_statement
		{
			$$ = $1;
		}
	|	loop_statement
		{
			$$ = $1;
		}
	|	if_statement
		{
			$$ = $1;
		}
	;


assign_statement
	:	assign_expression ';'
		{
			std::shared_ptr<rslc_node> node(new rslc_statement_node($1));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	;

function_call_statement
	:	function_call ';'
		{
			std::shared_ptr<rslc_node> node(new rslc_statement_node($1));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	;

return_statement
	:	TOKEN_RETURN expression ';'
		{
			std::shared_ptr<rslc_node> node(new rslc_return_node($2));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("return_statement:TOKEN_RETURN expression ';'");
			}
		}
	;

statement_variables_definition
	:	type_variables_definition
		{
			$$ = $1;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statement_variables_definition");
			}
		}
	|	TOKEN_EXTERN type_variables_definition
		{
			rslc_type_variables_def_node* n0 = (rslc_type_variables_def_node*)$2;
			rslc_type_node* type = n0->get_type_node();
			type->add_attribute("extern");
			$$ = $2;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statement_variables_definition");
			}
		}
	;

definition_statement
	:	statement_variables_definition ';'
		{
			std::shared_ptr<rslc_node> node(new rslc_statement_node($1));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("definition_statement:statement_variables_definition ';'");
			}
		}
	;

block_statement
	:	'{' statements '}'
		{
			std::shared_ptr<rslc_node> node(new rslc_block_node());
			GET_CTX()->add_node(node);
			{
				rslc_list_node* l = (rslc_list_node*)$2;
				size_t sz = l->get_nodes_size();
				for(size_t i = 0;i<sz;i++)
				{
					((rslc_list_node*)(node.get()))->add_node(l->get_node_at(i));
				}
			}
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("block_statement:'{' statements '}'");
			}

			GET_CTX()->remove_node($2);
		}
	|	'{' '}'
		{
			std::shared_ptr<rslc_node> node(new rslc_block_node());
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("block_statement:'{' '}'");
			}
		}
	|	';'
		{
			std::shared_ptr<rslc_node> node(new rslc_block_node());
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("block_statement:';'");
			}
		}
	;


loop_statement
	:	while_statement
		{
			$$ = $1;
		}
	|	for_statement
		{
			$$ = $1;
		}
	|	solar_statement
		{
			$$ = $1;
		}
	|	illuminate_statement
		{
			$$ = $1;
		}
	|	illuminance_statement
		{
			$$ = $1;
		}
	|	gather_statement
		{
			$$ = $1;
		}

while_statement
	:	TOKEN_WHILE relation statement
		{
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node($2);
			
			std::shared_ptr<rslc_node> node(new rslc_while_node(cond, $3));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("while_statement:TOKEN_WHILE relation statement");
			}
		}
	|	TOKEN_WHILE expression statement
		{
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node($2);

			std::shared_ptr<rslc_node> node(new rslc_while_node(cond, $3));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("while_statement:TOKEN_WHILE expression statement");
			}
		}
	;

for_statement
	:	TOKEN_FOR '(' expression ';' relation ';' expression ')' statement
		{
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node($3);
			cond->add_node($5);
			cond->add_node($7);

			std::shared_ptr<rslc_node> node(new rslc_for_node(cond, $9));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("for_statement:TOKEN_FOR '(' expression ';' relation ';' expression ')' statement");
			}
		}
	|	TOKEN_FOR '(' expression ';' expression ';' expression ')' statement
		{
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node($3);
			cond->add_node($5);
			cond->add_node($7);

			std::shared_ptr<rslc_node> node(new rslc_for_node(cond, $9));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("for_statement:TOKEN_FOR '(' expression ';' expression ';' expression ')' statement");
			}
		}
	;

solar_statement
	:	TOKEN_SOLAR '(' ')' statement
		{
			rslc_list_node* cond = MAKE_LIST();

			std::shared_ptr<rslc_node> node(new rslc_solar_node(cond, $4));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("solar_statement:TOKEN_SOLAR '(' ')' statement");
			}
		}
	|	TOKEN_SOLAR '(' expression ',' expression ')' statement
		{
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node($3);
			cond->add_node($5);

			std::shared_ptr<rslc_node> node(new rslc_solar_node(cond, $7));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("solar_statement:TOKEN_SOLAR '(' expression ',' expression ')' statement");
			}
		}
	;

illuminate_statement
	:	TOKEN_ILLUMINATE '(' expression ')' statement
		{
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node($3);

			std::shared_ptr<rslc_node> node(new rslc_illuminate_node(cond, $5));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminate_statement:TOKEN_ILLUMINATE '(' expression ')' statement");
			}
		}
	|	TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement
		{
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node($3);
			cond->add_node($5);
			cond->add_node($7);

			std::shared_ptr<rslc_node> node(new rslc_illuminate_node(cond, $9));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminate_statement:TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement");
			}
		}
	;

illuminance_statement
	:	TOKEN_ILLUMINANCE '(' expression ')' statement
		{
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node($3);

			std::shared_ptr<rslc_node> node(new rslc_illuminance_node(cond, $5));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminance_statement:TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement");
			}
		}
	|	TOKEN_ILLUMINANCE '(' expression ',' expression ')' statement
		{
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node($3);
			cond->add_node($5);

			std::shared_ptr<rslc_node> node(new rslc_illuminance_node(cond, $7));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminance_statement:TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement");
			}
		}
	|	TOKEN_ILLUMINANCE '(' expression ',' expression ',' expression ')' statement
		{
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node($3);
			cond->add_node($5);
			cond->add_node($7);

			std::shared_ptr<rslc_node> node(new rslc_illuminance_node(cond, $9));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminance_statement:TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement");
			}
		}
	|	TOKEN_ILLUMINANCE '(' expression ',' expression ',' expression ',' expression ')' statement
		{
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node($3);
			cond->add_node($5);
			cond->add_node($7);
			cond->add_node($9);

			std::shared_ptr<rslc_node> node(new rslc_illuminance_node(cond, $11));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminance_statement:TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement");
			}
		}
	;

gather_statement
	:	TOKEN_GATHER '(' expression ',' expression ',' expression ',' expression ',' expression ')' statement
		{
			rslc_list_node* cond = MAKE_LIST();
			cond->add_node($3);
			cond->add_node($5);
			cond->add_node($7);
			cond->add_node($9);
			cond->add_node($11);

			std::shared_ptr<rslc_node> node(new rslc_gather_node(cond, $13));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("illuminance_statement:TOKEN_ILLUMINATE '(' expression ',' expression ',' expression ')' statement");
			}
		}

if_statement
	: TOKEN_IF relation statement
		{
			std::shared_ptr<rslc_node> node(new rslc_if_node($2, $3));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("if_statement:TOKEN_IF relation statement");
			}
		}
	| TOKEN_IF expression statement
		{
			std::shared_ptr<rslc_node> node(new rslc_if_node($2, $3));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("if_statement:TOKEN_IF relation statement");
			}
		}
	| TOKEN_IF relation statement TOKEN_ELSE statement
		{
			std::shared_ptr<rslc_node> node(new rslc_if_node($2, $3, $5));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("if_statement:TOKEN_IF relation statement TOKEN_ELSE statement");
			}
		}
	| TOKEN_IF expression statement TOKEN_ELSE statement
		{
			std::shared_ptr<rslc_node> node(new rslc_if_node($2, $3, $5));
			GET_CTX()->add_node(node);
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("if_statement:TOKEN_IF expression statement TOKEN_ELSE statement");
			}
		}
	;

loop_mod_statement
	:	TOKEN_BREAK INTEGER_LITERAL ';'
		{
			std::shared_ptr<rslc_node> node(new rslc_break_node($2));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_BREAK ';'
		{
			std::shared_ptr<rslc_node> node(new rslc_break_node());
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_CONTINUE INTEGER_LITERAL ';'
		{
			std::shared_ptr<rslc_node> node(new rslc_continue_node($2));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_CONTINUE ';'
		{
			std::shared_ptr<rslc_node> node(new rslc_continue_node());
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	;

expression
	:	primary
		{
			$$ = $1;
		}
	|	expression '.' expression
		{
			std::shared_ptr<rslc_node> node(new rslc_dot_node($1, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	expression '/' expression
		{
			std::shared_ptr<rslc_node> node(new rslc_div_node($1, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	expression '*' expression
		{
			std::shared_ptr<rslc_node> node(new rslc_mul_node($1, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	expression '^' expression
		{
			std::shared_ptr<rslc_node> node(new rslc_crs_node($1, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	expression '+' expression
		{
			std::shared_ptr<rslc_node> node(new rslc_add_node($1, $3));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	expression '-' expression
		{
			std::shared_ptr<rslc_node> node(new rslc_sub_node($1, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	'-' expression	%prec NEG
		{
			std::shared_ptr<rslc_node> node(new rslc_neg_node($2));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("expression:'-' expression");
			}
		}
	|	relation '?' expression ':' expression
		{
			std::shared_ptr<rslc_node> node(new rslc_cond_node($1, $3, $5));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("expression:relation '?' expression ':' expression");
			}
		}
	|	cast_expression %prec '('
		{
			$$ = $1;
		}

primary
	:	number
		{
			$$ = $1;
		}
	|	string
		{
			$$ = $1;
		}
	|	identifier
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node(new rslc_variable_ref_node(name));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("primary:identifier");
			}
		}
	|	identifier '[' expression ']'
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node(new rslc_variable_array_ref_node(name, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("primary:identifier '[' expression ']'");
			}
		}
	|	function_call
		{
			$$ = $1;
		}
	|	assign_expression
		{
			$$ = $1;
		}
	|	'(' expression ')'
		{
			std::shared_ptr<rslc_node> node(new rslc_paren_node($2));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	tuple3
		{
			$$ = $1;
		}
	|	tuple16
		{
			$$ = $1;
		}
	;

tuple3
	:	'(' expression ',' expression ',' expression ')'
		{
			std::shared_ptr<rslc_node> node(new rslc_tuple_node());
			GET_CTX()->add_node(node);
			rslc_tuple_node* n0 = (rslc_tuple_node*)(node.get());
			n0->add_node($2);
			n0->add_node($4);
			n0->add_node($6);
			node->set_location(GET_LOC());
			$$ = n0;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("tuple3");
			}
		}
	;

tuple16
	:	'(' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ',' expression ')'
		{
			std::shared_ptr<rslc_node> node(new rslc_tuple_node());
			GET_CTX()->add_node(node);
			rslc_tuple_node* n0 = (rslc_tuple_node*)(node.get());
			n0->add_node($2);n0->add_node($4);n0->add_node($6);n0->add_node($8);
			n0->add_node($10);n0->add_node($12);n0->add_node($14);n0->add_node($16);
			n0->add_node($18);n0->add_node($20);n0->add_node($22);n0->add_node($24);
			n0->add_node($26);n0->add_node($28);n0->add_node($30);n0->add_node($32);
			
			node->set_location(GET_LOC());
			$$ = n0;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("tuple16");
			}
		}
	;

relation
	:	'(' relation ')'
		{
			std::shared_ptr<rslc_node> node(new rslc_paren_node($2));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:'(' relation ')'");
			}
		}
	|	expression '>' expression
		{
			std::shared_ptr<rslc_node> node(new rslc_gtr_node($1, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:expression '>' expression");
			}
		}
	|	expression TOKEN_GE expression
		{
			std::shared_ptr<rslc_node> node(new rslc_geq_node($1, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:expression TOKEN_GE expression");
			}
		}
	|	expression '<' expression
		{
			std::shared_ptr<rslc_node> node(new rslc_lss_node($1, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:expression '<' expression");
			}
		}
	|	expression TOKEN_LE expression
		{
			std::shared_ptr<rslc_node> node(new rslc_leq_node($1, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:expression TOKEN_LE expression");
			}
		}
	|	expression TOKEN_EQ expression
		{
			std::shared_ptr<rslc_node> node(new rslc_eq_node($1, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:expression TOKEN_EQ expression");
			}
		}
	|	expression TOKEN_NE expression
		{
			std::shared_ptr<rslc_node> node(new rslc_ne_node($1, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:expression TOKEN_NE expression");
			}
		}
	|	relation TOKEN_AND relation
		{
			std::shared_ptr<rslc_node> node(new rslc_and_node($1, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:relation TOKEN_AND relation");
			}
		}
	|	relation TOKEN_OR relation
		{
			std::shared_ptr<rslc_node> node(new rslc_or_node($1, $3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:relation TOKEN_OR relation");
			}
		}
	|	'!' relation		
		{
			std::shared_ptr<rslc_node> node(new rslc_not_node($2));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("relation:'!' relation");
			}
		}
	;

assign_expression
	:	identifier '=' expression
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_ref_node(name));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node* n3   = (rslc_expression_node*)$3;

			std::shared_ptr<rslc_node> node(new rslc_assign_node(n1, n3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier '=' expression");
			}
		}
	|	identifier TOKEN_ADD_ASSIGN expression
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_ref_node(name));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node* n3   = (rslc_expression_node*)$3;

			std::shared_ptr<rslc_node> node(new rslc_add_assign_node(n1, n3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier '+=' expression");
			}
		}
	|	identifier TOKEN_SUB_ASSIGN expression
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_ref_node(name));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node* n3   = (rslc_expression_node*)$3;

			std::shared_ptr<rslc_node> node(new rslc_sub_assign_node(n1, n3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier '-=' expression");
			}
		}
	|	identifier TOKEN_MUL_ASSIGN expression
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_ref_node(name));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node* n3   = (rslc_expression_node*)$3;

			std::shared_ptr<rslc_node> node(new rslc_mul_assign_node(n1, n3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier '*=' expression");
			}
		}
	|	identifier TOKEN_DIV_ASSIGN expression
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_ref_node(name));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node* n3   = (rslc_expression_node*)$3;

			std::shared_ptr<rslc_node> node(new rslc_div_assign_node(n1, n3));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier '/=' expression");
			}
		}
	|	identifier '[' expression ']' '=' expression
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_array_ref_node(name, $3));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node*   n6 = (rslc_expression_node*)$6;

			std::shared_ptr<rslc_node> node(new rslc_assign_node(n1, n6));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier  '[' expression ']' '/=' expression");
			}
		}
	|	identifier  '[' expression ']' TOKEN_ADD_ASSIGN expression
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_array_ref_node(name, $3));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node*   n6 = (rslc_expression_node*)$6;

			std::shared_ptr<rslc_node> node(new rslc_add_assign_node(n1, n6));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier  '[' expression ']' '/=' expression");
			}
		}
	|	identifier  '[' expression ']' TOKEN_SUB_ASSIGN expression
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_array_ref_node(name, $3));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node*   n6 = (rslc_expression_node*)$6;

			std::shared_ptr<rslc_node> node(new rslc_sub_assign_node(n1, n6));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier  '[' expression ']' '/=' expression");
			}
		}
	|	identifier  '[' expression ']' TOKEN_MUL_ASSIGN expression
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_array_ref_node(name, $3));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node*   n6 = (rslc_expression_node*)$6;

			std::shared_ptr<rslc_node> node(new rslc_mul_assign_node(n1, n6));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier  '[' expression ']' '/=' expression");
			}
		}
	|	identifier  '[' expression ']' TOKEN_DIV_ASSIGN expression
		{
			std::string name = ((rslc_identifier_node*)($1))->get_value();
			std::shared_ptr<rslc_node> node1(new rslc_variable_array_ref_node(name, $3));
			GET_CTX()->add_node(node1);
			node1->set_location(GET_LOC());

			rslc_variable_ref_node* n1 = (rslc_variable_ref_node*)node1.get();
			rslc_expression_node*   n6 = (rslc_expression_node*)$6;

			std::shared_ptr<rslc_node> node(new rslc_div_assign_node(n1, n6));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("assign_expression:identifier  '[' expression ']' '/=' expression");
			}
		}
	;

cast_expression
	:	type expression
		{
			rslc_type_node* n0 = (rslc_type_node*)$1;
			rslc_expression_node* n1 = (rslc_expression_node*)$2;
			std::shared_ptr<rslc_node> node(new rslc_cast_node(n0, n1));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	type STRING_LITERAL expression
		{
			rslc_type_node* n0 = (rslc_type_node*)$1;
			std::string s1 = $2;
			rslc_expression_node* n2 = (rslc_expression_node*)$3;
			std::shared_ptr<rslc_node> node(new rslc_cast_node(n0, s1, n2));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			release($2);
		}
	;

type
	:	TOKEN_FLOAT
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("float"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_STRING
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("string"));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_COLOR
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("color"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_POINT
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("point"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_VECTOR
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("vector"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_NORMAL
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("normal"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_MATRIX
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("matrix"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_VOID
		{
			std::shared_ptr<rslc_node> node(new rslc_type_node("void"));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	;

function_call
	:	function_name '(' function_arguments ')'
		{
			rslc_identifier_node* n0 = (rslc_identifier_node*)$1;
			rslc_node* n1 = $3;

			std::shared_ptr<rslc_node> node(new rslc_function_call_node(n0->get_value(), n1));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "function_name '(' function_arguments ')'";
				s += ":";
				s += n0->get_value();
				DEBUG(s.c_str());
			}
			;
			GET_CTX()->remove_node(n0);
		}
	|	function_name '(' ')'
		{
			rslc_identifier_node* n0 = (rslc_identifier_node*)$1;
			rslc_node* n1 = NULL;

			{
				std::shared_ptr<rslc_node> n(new rslc_list_node());
				GET_CTX()->add_node(n);
				n->set_location(GET_LOC());
				n1 = n.get();
			}

			std::shared_ptr<rslc_node> node(new rslc_function_call_node(n0->get_value(), n1));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "function_call:function_name '(' ')'";
				s += ":";
				s += n0->get_value();
				DEBUG(s.c_str());
			}
			;
			GET_CTX()->remove_node(n0);
		}
	;

function_name
	:	identifier
		{
			$$ = $1;
		}
	|	TOKEN_ATMOSPHERE
		{
			std::string v = "atomosphere";
			std::shared_ptr<rslc_node> node(new rslc_identifier_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_DISPLACEMENT
		{
			std::string v = "displacement";
			std::shared_ptr<rslc_node> node(new rslc_identifier_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_LIGHTSOURCE
		{
			std::string v = "lightsource";
			std::shared_ptr<rslc_node> node(new rslc_identifier_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	|	TOKEN_SURFACE
		{
			std::string v = "surface";
			std::shared_ptr<rslc_node> node(new rslc_identifier_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
		}
	;

function_arguments
	:	function_argument			
		{
			std::shared_ptr<rslc_node> node(new rslc_list_node());
			((rslc_list_node*)node.get())->add_node($1);
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statements:statement");
			}
		}
	|	function_arguments ',' function_argument
		{
			((rslc_list_node*)$1)->add_node($3);
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statements:statement");
			}
		}
	;

function_argument
	:	expression
		{
			$$ = $1;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statements:statement");
			}
		}
	|	texture_name
		{
			$$ = $1;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("statements:statement");
			}
		}
	;

identifier
	:	IDENTIFIER
		{
			std::string v = $1;
			std::shared_ptr<rslc_node> node(new rslc_identifier_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				std::string s = "identifier:IDENTIFIER:";
				s += v;
				DEBUG(s.c_str());
			}
			;
			release($1);
		}
	;

number
	:	integer
		{
			$$ = $1;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("number:integer");
			}
		}
	|	float
		{
			$$ = $1;
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("number:float");
			}
		}
	;

integer
	:	INTEGER_LITERAL
		{
			int v = $1;
			std::shared_ptr<rslc_node> node(new rslc_integer_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("integer:INTEGER_LITERAL");
			}
		}
	;

float 
	:	FLOAT_LITERAL
		{
			float v = $1;
			std::shared_ptr<rslc_node> node(new rslc_float_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("float:FLOAT_LITERAL");
			}
		}
	;

string
	:	STRING_LITERAL
		{
			std::string v = $1;
			std::shared_ptr<rslc_node> node(new rslc_string_node(v));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("string:STRING_LITERAL");
			}
			;
			release($1);
		}

texture_name
	:	STRING_LITERAL '[' INTEGER_LITERAL ']'
		{
			std::string v = $1;
			int i = $3;
			std::shared_ptr<rslc_node> node(new rslc_texturename_node(v, i));
			GET_CTX()->add_node(node);
			node->set_location(GET_LOC());
			$$ = node.get();
			;
			if(GET_CTX()->is_debug())
			{
				DEBUG("texture_name:STRING_LITERAL '[' INTEGER_LITERAL ']'");
			}
			;
			release($1);
		}
	;

%%

void yy::rslc_parser::error(const yy::rslc_parser::location_type& loc, const std::string& msg)
{
	std::string fname = GET_FNAME();
	int lineno = GET_LINE();
	fprintf(stderr, "parser error %s :%s(%d)\n", msg.c_str(), fname.c_str(), lineno);
}

extern int _rslc_lex_init(void** p);
extern int _rslc_lex_destroy(void* p);

namespace rslc
{
	int load_rsl(rslc_context* ctx, rslc_decoder* dec, const char* szFileName)
	{
		void* scanner;
		_rslc_lex_init(&scanner);

		rslc_parse_param* param = (rslc_parse_param*)(scanner);
		param->ctx = ctx;
		param->dec = dec;
		param->line_number = 1;
		param->filename = szFileName;

		yy::rslc_parser parser(scanner);

		int nRet = parser.parse();

		_rslc_lex_destroy(scanner);
		return nRet;
	}
}
