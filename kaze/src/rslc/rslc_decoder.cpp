#include "rslc_decoder.h"

namespace rslc
{
    rslc_decoder::rslc_decoder(FILE* fp)
    {
        fp_ = fp;
    }

    rslc_decoder::~rslc_decoder()
    {
        ; //
    }

    int rslc_decoder::read(char* buffer, unsigned int size)
    {
        return fread(buffer, 1, (size_t)size, fp_);
    }
}

