#ifndef RSLC_PATH_RESOLVER_H
#define RSLC_PATH_RESOLVER_H

#include <string>

namespace rslc
{
    class rslc_path_resolver
    {
    public:
        static std::string get_bin_path();
        static std::string replace_ext(const std::string& s, const std::string& ext);
        static std::string get_full_path(const std::string& s);
    };
}

#endif
