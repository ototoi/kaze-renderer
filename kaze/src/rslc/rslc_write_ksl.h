#ifndef RSLC_WRITE_KSL_H
#define RSLC_WRITE_KSL_H

#include <iostream>

namespace rslc
{
    class rslc_node;
    int rslc_write_ksl(std::ostream& os, const rslc_node* node);
}

#endif