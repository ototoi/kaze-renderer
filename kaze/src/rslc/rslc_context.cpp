#include "rslc_context.h"

namespace rslc
{
    rslc_context::rslc_context()
    {
        root_ = NULL;
    }

    rslc_context::~rslc_context()
    {
        ;//
    }

    void rslc_context::set_root_node(rslc_node* root)
    {
        root_ = root;
    }

    rslc_node* rslc_context::get_root_node()const
    {
        return root_;
    }

    bool rslc_context::is_debug()const
    {
        return false;
    }

    void rslc_context::print_debug(const char* szMsg)
    {
        fprintf(stderr, "%s\n", szMsg);
    }

    void rslc_context::print_error(const char* szMsg)
    {
        fprintf(stderr, "%s\n", szMsg);
    }
}