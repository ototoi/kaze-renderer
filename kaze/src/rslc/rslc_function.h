#ifndef RSLC_FUNCTION_H
#define RSLC_FUNCTION_H

#include "rslc_type.h"
#include "rslc_variable.h"

namespace rslc
{
    class rslc_function
    {
    public:
        rslc_function(const rslc_type& type, const std::string& name, const std::vector<rslc_variable>& args)
            :type_(type), name_(name), args_(args)
        {}
    public:
        const rslc_type& get_type()const
        {
            return type_;
        }
        const std::string& get_name()const
        {
            return name_;
        }
        const std::vector<rslc_variable>& get_arguments()const
        {
            return args_;
        }
    private:
        rslc_type type_;
        std::string name_;
        std::vector<rslc_variable> args_;
    };
}

#endif