#include "ri_convert_texture.h"
#include "ri_make_texture_helper.h"
#include "memory_image.hpp"
#include "image_interpolator.hpp"
#include "unit_image_texture.hpp"
#include "transform_matrix.h"
#include "values.h"

#include <string>
#include <vector>
#include <algorithm>

#include <stdlib.h>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include "image/hdr_io.h"
#include "image/png_io.h"

namespace kaze
{
    namespace ri
    {
        static bool CopyFile(const char* src, const char* dst)
        {
#ifdef _WIN32
            BOOL bRet = ::CopyFileA(src, dst, FALSE);
            return (bRet) ? true : false;
#else
            char buffer[1024] = {};
            snprintf(buffer, 1024, "cp \"%s\" \"%s\"", src, dst);
            ::system(buffer);
#endif
            return true;
        }

        static real clamp(real x, real a = 0, real b = 255)
        {
            return floor(std::min(std::max(a, x), b));
        }
        static void CreateBuffer(std::vector<unsigned char>& buffer, const std::shared_ptr<image<color4> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            size_t total = width * height * 4;
            buffer.resize(total);
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    color4 c = img->get(x, y);
                    unsigned char R = (unsigned char)(((int)clamp(c[0] * 255)) & 0xFF);
                    unsigned char G = (unsigned char)(((int)clamp(c[1] * 255)) & 0xFF);
                    unsigned char B = (unsigned char)(((int)clamp(c[2] * 255)) & 0xFF);
                    unsigned char A = (unsigned char)(((int)clamp(c[3] * 255)) & 0xFF);

                    size_t idx = (y * width + x);
                    buffer[4 * idx + 0] = R;
                    buffer[4 * idx + 1] = G;
                    buffer[4 * idx + 2] = B;
                    buffer[4 * idx + 3] = A;
                }
            }
        }

        int ri_convert_texture(const char* pic, int n, const char* tokens[], const char* params[])
        {
            std::string strOut = pic;
            strOut += ".png";
            std::shared_ptr<image<color4> > img = get_image_from_file(pic);
            if (!img.get()) return -1;
            std::vector<unsigned char> buffer;
            CreateBuffer(buffer, img);
            bool bRet = WritePNGRGBAImage(strOut.c_str(), &buffer[0], img->get_width(), img->get_height());
            return bRet ? 0 : -1;
        }

        static std::shared_ptr<image<color4> > DownSampleImage(const std::shared_ptr<image<color4> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            int nw = width / 2;
            int nh = height;
            std::shared_ptr<image<color4> > outImg(new memory_image<color4>(nw, nh));
            for (int y = 0; y < nh; y++)
            {
                for (int x = 0; x < nw; x++)
                {
                    color4 c0 = img->get(2 * x + 0, y);
                    color4 c1 = img->get(2 * x + 1, y);
                    color4 cc = (c0 + c1) * 0.5;
                    outImg->set(x, y, cc);
                }
            }
            return outImg;
        }

        static void CreateMip_(std::vector<std::shared_ptr<image<color4> > >& images, const std::shared_ptr<image<color4> >& img)
        {
            images.push_back(img);
            int width = img->get_width();
            if (width > 1)
            {
                std::shared_ptr<image<color4> > img2 = DownSampleImage(img);
                CreateMip_(images, img2);
            }
        }

        static int GetResizeSize(int width)
        {
            int r = 1;
            while (r < width)
            {
                r = r << 1;
            }
            return r;
        }

        static void CopyImage(std::shared_ptr<image<color4> >& outImg, const std::shared_ptr<image<color4> >& inImg)
        {
            std::shared_ptr<image_texture<color4> > tex(
                new unit_image_texture<color4>(
                    inImg,
                    new repeat_corresponder(),
                    new clamp_corresponder(),
                    new bilinear_image_interpolator<color4>()));
            int w = outImg->get_width();
            int h = outImg->get_height();
            for (int y = 0; y < h; y++)
            {
                real yy = (y + 0.5) / h;
                for (int x = 0; x < w; x++)
                {
                    real xx = (x + 0.5) / w;
                    color4 c = tex->get(xx, yy);
                    outImg->set(x, y, c);
                }
            }
        }

        static void CreateMip(std::vector<std::shared_ptr<image<color4> > >& images, const std::shared_ptr<image<color4> >& img)
        {
            int w = img->get_width();
            int h = img->get_height();
            int nw = GetResizeSize(w);
            int nh = nw / 2;
            if (w == nw && h == nh)
            {
                CreateMip_(images, img);
            }
            else
            {
                std::shared_ptr<image<color4> > outImg(new memory_image<color4>(nw, nh));
                CopyImage(outImg, img);
                CreateMip_(images, outImg);
            }
        }

        static std::string I2S(int i)
        {
            char buffer[64];
            sprintf(buffer, "%d", i);
            return buffer;
        }

        static real log2(real x)
        {
            static const real iLOG2 = 1.0 / log(2.0);
            return log(x) * iLOG2;
        }

        static color4 Sample(const std::vector<std::shared_ptr<image_texture<color4> > >& textures, real level, real u, real v)
        {
            size_t sz = textures.size();
            if (level <= 0) return textures[0]->get(u, v);
            if (level >= (sz - 1)) return textures[sz - 1]->get(u, v);
            int lv0 = (int)floor(level);
            int lv1 = lv0 + 1;
            real t = level - lv0;

            color4 c0 = textures[lv0]->get(u, v);
            color4 c1 = textures[lv1]->get(u, v);

            return (1 - t) * c0 + t * c1;
        }

        static color4 Sample(const std::vector<std::shared_ptr<image_texture<color4> > >& textures, const vector3& dir)
        {
            real u = (-atan2(dir[1], dir[0]) / values::pi() + 1) / 2;
            real v = (-asin(dir[2]) / values::pi() + 1) / 2;

            real Rx = sqrt(1 - dir[2] * dir[2]);
            real level = -log2(Rx);
            level = std::max<real>(0, std::min<real>(level, (real)(textures.size() - 1)));
            //real cc = level / (real)(textures.size()-1);
            //return color4(cc,cc,cc,1);
            return Sample(textures, level, u, v);
        }

        inline vector3 MulNormal(const matrix4& m, const vector3& v)
        {
            return vector3(
                m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
                m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
                m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
        }

        static void SampleCube(std::shared_ptr<image<color4> >& img, const std::vector<std::shared_ptr<image_texture<color4> > >& textures, const matrix4& mat)
        {
            //matrix4 imat = ~mat;
            int w = img->get_width();
            int h = img->get_height();
            for (int y = 0; y < h; y++)
            {
                real yy = (y + 0.5) / h;
                for (int x = 0; x < w; x++)
                {
                    real xx = (x + 0.5) / w;
                    real xxx = 2 * xx - 1;
                    real yyy = -(2 * yy - 1);
                    vector3 dir = mat * normalize(vector3(xxx, yyy, 1));
                    color4 c = Sample(textures, normalize(dir));
                    img->set(x, y, c);
                }
            }
        }

        static std::string GetExt(const std::string& str)
        {
#ifdef _WIN32
            char ext[_MAX_EXT] = {};
            _splitpath(str.c_str(), NULL, NULL, NULL, ext);
            return ext;
#else
            std::string::size_type i = str.find_last_of('.');
            if (i != std::string::npos)
            {
                return str.substr(i);
            }
            else
            {
                return "";
            }
#endif
        }

        static bool WriteImageFile(const char* in, const char* out, const std::shared_ptr<image<color4> >& img)
        {
            std::string inExt = GetExt(in);
            std::string outExt = GetExt(out);
            if (inExt == outExt)
            {
                CopyFile(in, out);
            }

            if (outExt == ".png")
            {
                std::vector<unsigned char> buffer;
                CreateBuffer(buffer, img);
                return WritePNGRGBAImage(out, &buffer[0], img->get_width(), img->get_height());
            }
            else if (outExt == ".hdr")
            {
                return WriteHDRColor4Image(out, img);
            }
            return false;
        }

        int ri_latl2cube(
            const char* pic,
            const char* px, const char* nx, const char* py, const char* ny, const char* pz, const char* nz,
            int n, const char* tokens[], const char* params[])
        {
            std::shared_ptr<image<color4> > img = get_image_from_file(pic);
            if (!img.get()) return -1;

            std::vector<std::shared_ptr<image<color4> > > images;
            CreateMip(images, img);
            std::vector<std::shared_ptr<image_texture<color4> > > textures;
            for (int i = 0; i < images.size(); i++)
            {
                std::shared_ptr<image_texture<color4> > tex(
                    new unit_image_texture<color4>(
                        images[i],
                        new repeat_corresponder(),
                        new clamp_corresponder(),
                        new bilinear_image_interpolator<color4>()));
                textures.push_back(tex);
            }

            int w = images[0]->get_height() / 2;
            int h = w;
            std::vector<std::shared_ptr<image<color4> > > outImages;
            for (int i = 0; i < 6; i++)
            {
                outImages.push_back(std::shared_ptr<image<color4> >(new memory_image<color4>(w, h)));
            }

            static const matrix4 matrices[] = {
                camera2world(vector3(0, 0, 0), vector3(1, 0, 0), vector3(0, 0, 1)),
                camera2world(vector3(0, 0, 0), vector3(-1, 0, 0), vector3(0, 0, 1)),
                camera2world(vector3(0, 0, 0), vector3(0, 1, 0), vector3(0, 0, 1)),
                camera2world(vector3(0, 0, 0), vector3(0, -1, 0), vector3(0, 0, 1)),
                camera2world(vector3(0, 0, 0), vector3(0, 0, 1), vector3(1, 0, 0)),
                camera2world(vector3(0, 0, 0), vector3(0, 0, -1), vector3(-1, 0, 0)),
            };

            for (int i = 0; i < 6; i++)
            {
                SampleCube(outImages[i], textures, matrices[i]);
            }

            std::string outFiles[] =
                {
                    px,
                    nx,
                    py,
                    ny,
                    pz,
                    nz,
                };

            for (int i = 0; i < 6; i++)
            {
                if (!WriteImageFile(pic, outFiles[i].c_str(), outImages[i])) return -1;
            }

            return 0;
        }
    }
}
