#include "gl_create_context.h"

#define PROJECT_NAME TEXT("_GL")

#if defined(_WIN32)

static HWND wnd = NULL;
static HDC dc = NULL;
static HGLRC rc = NULL;

GLboolean gl_create_context(int* pixelformat)
{
    WNDCLASS wc;
    PIXELFORMATDESCRIPTOR pfd;
    /* register window class */
    ZeroMemory(&wc, sizeof(WNDCLASS));
    wc.hInstance = GetModuleHandle(NULL);
    wc.lpfnWndProc = DefWindowProc;
    wc.lpszClassName = PROJECT_NAME;
    if (0 == RegisterClass(&wc)) return GL_TRUE;
    /* create window */
    wnd = CreateWindow(PROJECT_NAME, PROJECT_NAME, 0, CW_USEDEFAULT, CW_USEDEFAULT, CW_USEDEFAULT,
                       CW_USEDEFAULT, NULL, NULL, GetModuleHandle(NULL), NULL);
    if (NULL == wnd) return GL_TRUE;
    /* get the device context */
    dc = GetDC(wnd);
    if (NULL == dc) return GL_TRUE;
    /* find pixel format */
    ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));
    if (*pixelformat == -1) /* find default */
    {
        pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
        pfd.nVersion = 1;
        pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL;
        *pixelformat = ChoosePixelFormat(dc, &pfd);
        if (*pixelformat == 0) return GL_TRUE;
    }
    /* set the pixel format for the dc */
    if (FALSE == SetPixelFormat(dc, *pixelformat, &pfd)) return GL_TRUE;
    /* create rendering context */
    rc = wglCreateContext(dc);
    if (NULL == rc) return GL_TRUE;
    if (FALSE == wglMakeCurrent(dc, rc)) return GL_TRUE;
    return GL_FALSE;
}

void gl_destroy_context()
{
    if (NULL != rc) wglMakeCurrent(NULL, NULL);
    if (NULL != rc) wglDeleteContext(rc);
    if (NULL != wnd && NULL != dc) ReleaseDC(wnd, dc);
    if (NULL != wnd) DestroyWindow(wnd);
    UnregisterClass(PROJECT_NAME, GetModuleHandle(NULL));
}

/* ------------------------------------------------------------------------ */

#elif defined(__APPLE__) && !defined(GLEW_APPLE_GLX)

#include <AGL/agl.h>
#include <OpenGL/gl.h>
#include <OpenGL/OpenGL.h>

#if 0
static AGLContext ctx, octx;

GLboolean gl_create_context ()
{

  int attrib[] = { AGL_RGBA, AGL_NONE };
  AGLPixelFormat pf;
  /*int major, minor;
  SetPortWindowPort(wnd);
  aglGetVersion(&major, &minor);
  fprintf(stderr, "GL %d.%d\n", major, minor);*/
  pf = aglChoosePixelFormat(NULL, 0, attrib);
  if (NULL == pf) return GL_TRUE;
  ctx = aglCreateContext(pf, NULL);
  if (NULL == ctx || AGL_NO_ERROR != aglGetError()) return GL_TRUE;
  aglDestroyPixelFormat(pf);
  /*aglSetDrawable(ctx, GetWindowPort(wnd));*/
  octx = aglGetCurrentContext();
  if (GL_FALSE == aglSetCurrentContext(ctx)) return GL_TRUE;
  /* Needed for Regal on the Mac */
#if defined(GLEW_REGAL) && defined(__APPLE__)
  RegalMakeCurrent(octx);
#endif
  
  return GL_FALSE;
}

void gl_destroy_context ()
{
  aglSetCurrentContext(octx);
  if (NULL != ctx) aglDestroyContext(ctx);
}

#else

static CGLContextObj ctx, octx;

GLboolean gl_create_context()
{

    CGLPixelFormatAttribute attribs[13] = {
        kCGLPFAOpenGLProfile, (CGLPixelFormatAttribute)kCGLOGLPVersion_3_2_Core, // This sets the context to 3.2
        kCGLPFAColorSize, (CGLPixelFormatAttribute)24,
        kCGLPFAAlphaSize, (CGLPixelFormatAttribute)8,
        kCGLPFAAccelerated,
        kCGLPFADoubleBuffer,
        kCGLPFASampleBuffers, (CGLPixelFormatAttribute)1,
        kCGLPFASamples, (CGLPixelFormatAttribute)4,
        (CGLPixelFormatAttribute)0};

    CGLPixelFormatObj pix;
    GLint npix;
    CGLChoosePixelFormat(attribs, &pix, &npix);

    octx = CGLGetCurrentContext();

    CGLContextObj ctx;
    CGLCreateContext(pix, 0, &ctx);

    CGLSetCurrentContext(ctx);
    CGLLockContext(ctx);

#if defined(GLEW_REGAL) && defined(__APPLE__)
    RegalMakeCurrent(octx);
#endif

    return GL_FALSE;
}

void gl_destroy_context()
{
    CGLSetCurrentContext(octx);
    if (NULL != ctx) CGLDestroyContext(ctx);
}

#endif

/* ------------------------------------------------------------------------ */

#else /* __UNIX || (__APPLE__ && GLEW_APPLE_GLX) */

static Display* dpy = NULL;
static XVisualInfo* vi = NULL;
static XVisualInfo* vis = NULL;
static GLXContext ctx = NULL;
static Window wnd = 0;
static Colormap cmap = 0;

GLboolean gl_create_context(const char* display, int* visual)
{
    int attrib[] = {GLX_RGBA, GLX_DOUBLEBUFFER, None};
    int erb, evb;
    XSetWindowAttributes swa;
    /* open display */
    dpy = XOpenDisplay(display);
    if (NULL == dpy) return GL_TRUE;
    /* query for glx */
    if (!glXQueryExtension(dpy, &erb, &evb)) return GL_TRUE;
    /* choose visual */
    if (*visual == -1)
    {
        vi = glXChooseVisual(dpy, DefaultScreen(dpy), attrib);
        if (NULL == vi) return GL_TRUE;
        *visual = (int)XVisualIDFromVisual(vi->visual);
    }
    else
    {
        int n_vis, i;
        vis = XGetVisualInfo(dpy, 0, NULL, &n_vis);
        for (i = 0; i < n_vis; i++)
        {
            if ((int)XVisualIDFromVisual(vis[i].visual) == *visual)
                vi = &vis[i];
        }
        if (vi == NULL) return GL_TRUE;
    }
    /* create context */
    ctx = glXCreateContext(dpy, vi, None, True);
    if (NULL == ctx) return GL_TRUE;
    /* create window */
    /*wnd = XCreateSimpleWindow(dpy, RootWindow(dpy, vi->screen), 0, 0, 1, 1, 1, 0, 0);*/
    cmap = XCreateColormap(dpy, RootWindow(dpy, vi->screen), vi->visual, AllocNone);
    swa.border_pixel = 0;
    swa.colormap = cmap;
    wnd = XCreateWindow(dpy, RootWindow(dpy, vi->screen),
                        0, 0, 1, 1, 0, vi->depth, InputOutput, vi->visual,
                        CWBorderPixel | CWColormap, &swa);
    /* make context current */
    if (!glXMakeCurrent(dpy, wnd, ctx)) return GL_TRUE;
    return GL_FALSE;
}

void gl_destroy_context()
{
    if (NULL != dpy && NULL != ctx) glXDestroyContext(dpy, ctx);
    if (NULL != dpy && 0 != wnd) XDestroyWindow(dpy, wnd);
    if (NULL != dpy && 0 != cmap) XFreeColormap(dpy, cmap);
    if (NULL != vis)
        XFree(vis);
    else if (NULL != vi)
        XFree(vi);
    if (NULL != dpy) XCloseDisplay(dpy);
}

#endif /* __UNIX || (__APPLE__ && GLEW_APPLE_GLX) */
