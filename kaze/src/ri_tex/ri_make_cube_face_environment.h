#ifndef KAZE_RI_MAKE_CUBE_FACE_ENVIRONMENT_H
#define KAZE_RI_MAKE_CUBE_FACE_ENVIRONMENT_H

namespace kaze
{
    namespace ri
    {

        int ri_make_cube_face_environment(
            const char* px, const char* nx, const char* py, const char* ny, const char* pz, const char* nz,
            const char* tex, float fov, const char* filterfunc, float swidth, float twidth);
        int ri_make_cube_face_environment(
            const char* px, const char* nx, const char* py, const char* ny, const char* pz, const char* nz,
            const char* tex, float fov, const char* filterfunc, float swidth, float twidth,
            int n, const char* tokens[], const char* params[]);

        //char *px, char *nx, char *py, char *ny, char *pz, char *nz, char *tex, RtFloat fov, RtToken filterfunc, RtFloat swidth, RtFloat twidth
    }
}

#endif