#ifndef KAZE_RI_MAKE_LAT_LONG_ENVIRONMENT_H
#define KAZE_RI_MAKE_LAT_LONG_ENVIRONMENT_H

namespace kaze
{
    namespace ri
    {

        int ri_make_lat_long_environment(const char* pic, const char* tex, const char* filterfunc, float swidth, float twidth);
        int ri_make_lat_long_environment(const char* pic, const char* tex, const char* filterfunc, float swidth, float twidth, int n, const char* tokens[], const char* params[]);
    }
}

#endif