#ifndef _GL_CREATE_CONTEXT_H_
#define _GL_CREATE_CONTEXT_H_

#include <GL/glew.h>
#if defined(_WIN32)
#include <GL/wglew.h>
#elif !defined(__APPLE__) || defined(GLEW_APPLE_GLX)
#include <GL/glxew.h>
#endif

#if defined(_WIN32)
GLboolean gl_create_context(int* pixelformat);
#elif defined(__APPLE__) && !defined(GLEW_APPLE_GLX)
GLboolean gl_create_context();
#else
GLboolean gl_create_context(const char* display, int* visual);
#endif

void gl_destroy_context();

#endif