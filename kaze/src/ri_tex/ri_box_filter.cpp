#include "ri_box_filter.h"
#include "ri_tex_gl_context.h"

#include <stdio.h>
#include <memory>

#include <gl_tex/gl_box_filter_pass.h>

#include "memory_image.hpp"
#include "color.h"

#include "filter/corresponder.h"
#include "graphics/image/corresponded_image.hpp"
#include "graphics/image/image_interpolator.hpp"
#include "graphics/texture/image_texture/image_texture.hpp"
#include "graphics/texture/image_texture/unit_image_texture.hpp"

#include "aligned_allocator.hpp"

using namespace gl::tex;

namespace kaze
{
    namespace ri
    {
        enum
        {
            WRAP_REPEAT = 0,
            WRAP_CLAMP,
            WRAP_MIRROR,
            WRAP_BORDER,
        };

        struct TokenValueValue
        {
            const char* token;
            int value1;
            int value2;
        };

        static TokenValueValue WRAP_TYPES[] =
            {
                {"repeat", WRAP_REPEAT, GL_REPEAT},
                {"clamp", WRAP_CLAMP, GL_CLAMP_TO_EDGE},
                {"mirror", WRAP_MIRROR, GL_MIRRORED_REPEAT},
                {"border", WRAP_BORDER, GL_CLAMP_TO_BORDER},
                {NULL, 0},
        };

        static int ToGLWrap(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (strcmp(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].value2;
                i++;
            }
            return GL_CLAMP_TO_EDGE;
        }

        static int ToWrapType(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (strcmp(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].value1;
                i++;
            }
            return WRAP_REPEAT;
        }

        static std::shared_ptr<image<color4> > FilterImageGL(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<gl::gl_texture_2D> texInput(new gl::gl_texture_2D());
            std::shared_ptr<gl::gl_texture_2D> texOutput(new gl::gl_texture_2D());
            {
                std::vector<float> fBuffer(4 * width * height);
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        size_t index = y * width + x;
                        color4 c = img->get(x, y);
                        fBuffer[4 * index + 0] = (float)c[0];
                        fBuffer[4 * index + 1] = (float)c[1];
                        fBuffer[4 * index + 2] = (float)c[2];
                        fBuffer[4 * index + 3] = (float)c[3];
                    }
                }

                texInput->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, &fBuffer[0]);
            }
            {
                texInput->bind();
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);  //not biliear
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);  //
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, ToGLWrap(swrap)); //TODO
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, ToGLWrap(twrap)); //TODO
                texInput->unbind();
            }
            texOutput->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, 0);
            {
                texOutput->bind();
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);  //not biliear
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);  //
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, ToGLWrap(swrap)); //TODO
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, ToGLWrap(twrap)); //TODO
                texOutput->unbind();
            }
            //
            std::vector<std::shared_ptr<gl_pass> > passList;
            passList.push_back(std::shared_ptr<gl_pass>(new gl_box_filter_pass(width, height, texInput, texOutput, (int)swidth, (int)twidth)));
            //
            for (size_t i = 0; i < passList.size(); i++)
            {
                passList[i]->run();
            }
            //
            std::shared_ptr<image<color4> > outImg;
            std::vector<float> fBuffer(4 * width * height, 0.0f);
            texOutput->bind();
            ::glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &fBuffer[0]);
            texOutput->unbind();
            outImg.reset(new memory_image<color4>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    size_t index = y * width + x;
                    float r = fBuffer[4 * index + 0];
                    float g = fBuffer[4 * index + 1];
                    float b = fBuffer[4 * index + 2];
                    float a = fBuffer[4 * index + 3];
                    outImg->set(x, y, color4(r, g, b, a));
                }
            }
            return outImg;
        }

        static std::shared_ptr<image<color4> > FilterImage(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth)
        {
            int WW = ToWrapType(swrap);
            int HW = ToWrapType(twrap);

            std::shared_ptr<corresponder> corx(new powerof2_clamp_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            switch (WW)
            {
            case WRAP_REPEAT:
                corx.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                corx.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                corx.reset(new powerof2_mirror_corresponder());
                break;
            }

            switch (HW)
            {
            case WRAP_REPEAT:
                cory.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                cory.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                cory.reset(new powerof2_mirror_corresponder());
                break;
            }

            int width = img->get_width();
            int height = img->get_height();

            std::shared_ptr<image<color4> > outImg(new memory_image<color4>(width, height));

            std::shared_ptr<image<color4> > corImg(
                new corresponded_image<color4>(
                    img,
                    corx,
                    cory));

            int sf = (int)ceil(swidth) / 2;
            int tf = (int)ceil(twidth) / 2;

            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    color4 col = color4(0, 0, 0, 0);
                    for (int t = -tf; t <= +tf; t++)
                    {
                        for (int s = -sf; s <= +sf; s++)
                        {
                            col += corImg->get(x + s, y + t);
                        }
                    }

                    col /= ((2 * sf + 1) * (2 * tf + 1));
                    outImg->set(x, y, col);
                }
            }

            return outImg;
        }

        static color4 Conv(const color4f& c)
        {
            return color4(c[0], c[1], c[2], c[3]);
        }
        static color4f Conv(const color4& c)
        {
            return color4f(c[0], c[1], c[2], c[3]);
        }

        static std::shared_ptr<image<color4f> > ConvertImage(const std::shared_ptr<image<color4> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<image<color4f> > outImg(new memory_image<color4f>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    outImg->set(x, y, Conv(img->get(x, y)));
                }
            }
            return outImg;
        }

        static std::shared_ptr<image<color4> > ConvertImage(const std::shared_ptr<image<color4f> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<image<color4> > outImg(new memory_image<color4>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    outImg->set(x, y, Conv(img->get(x, y)));
                }
            }
            return outImg;
        }

        static float getWeight(float r, float x)
        {
            //r = 1.5
            //x = 0, 1, 2
            //w = 1, 1, 0
            //r = 2.2
            //x = 0, 1, 2, 3
            //w = 1, 1, 0.7, 0
            if (x + 0.5f <= r)
                return 1;
            else
                return std::max<int>(0, 1 - (x + 0.5 - r));
        }

        typedef std::vector<float, aligned_allocator<float, 32> > alvector;

        static std::shared_ptr<image<color4f> > FilterImageOpt(
            const std::shared_ptr<image<color4f> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth)
        {
            int WW = ToWrapType(swrap);
            int HW = ToWrapType(twrap);

            std::shared_ptr<corresponder> corx(new powerof2_clamp_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            switch (WW)
            {
            case WRAP_REPEAT:
                corx.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                corx.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                corx.reset(new powerof2_mirror_corresponder());
                break;
            }

            switch (HW)
            {
            case WRAP_REPEAT:
                cory.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                cory.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                cory.reset(new powerof2_mirror_corresponder());
                break;
            }

            int width = img->get_width();
            int height = img->get_height();

            int sf = (int)ceil(swidth) / 2;
            int tf = (int)ceil(twidth) / 2;

            std::shared_ptr<image<color4f> > outImg(new memory_image<color4f>(width, height));
            std::shared_ptr<image<color4f> > tmpImg(new memory_image<color4f>(width, height));

            {
                std::shared_ptr<image<color4f> > corImg(
                    new corresponded_image<color4f>(
                        img,
                        corx,
                        cory));

                alvector weights(2 * sf + 1);
                float ww = 0.0f;
                for (int s = -sf; s <= +sf; s++)
                {
                    float w = getWeight(swidth * 0.5f, s);
                    ww += w;
                    weights[s + sf] = w;
                }
                ww = 1.0f / ww;
                for (int s = -sf; s <= +sf; s++)
                {
                    weights[s + sf] *= ww;
                }

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        color4f col = color4f(0, 0, 0, 0);
                        for (int s = -sf; s <= +sf; s++)
                        {
                            col += weights[s + sf] * corImg->get(x + s, y);
                        }
                        tmpImg->set(x, y, col);
                    }
                }
            }

            {
                std::shared_ptr<image<color4f> > corImg(
                    new corresponded_image<color4f>(
                        tmpImg,
                        corx,
                        cory));

                alvector weights(2 * tf + 1);
                float ww = 0.0f;
                for (int t = -tf; t <= +tf; t++)
                {
                    float w = getWeight(twidth * 0.5f, t);
                    ww += w;
                    weights[t + tf] = w;
                }
                ww = 1.0f / ww;
                for (int t = -tf; t <= +tf; t++)
                {
                    weights[t + tf] *= ww;
                }

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        color4f col = color4f(0, 0, 0, 0);
                        for (int t = -tf; t <= +tf; t++)
                        {
                            col += weights[t + tf] * corImg->get(x, y + t);
                        }
                        outImg->set(x, y, col);
                    }
                }
            }

            return outImg;
        }

        std::shared_ptr<image<color4> > ri_box_filter(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth)
        {
            if (ri_tex_get_gl_context())
            {
                return FilterImageGL(img, swrap, twrap, swidth, twidth);
            }
            else
            {
                return ConvertImage(FilterImageOpt(ConvertImage(img), swrap, twrap, swidth, twidth));
            }
        }
    }
}
