#include "ri_tex_gl_context.h"
#include "gl_create_context.h"

#include <GL/glew.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory>

namespace kaze
{
    namespace ri
    {
        namespace
        {

            static int init()
            {
                int visual = -1;
#if defined(_WIN32)
                if (GL_TRUE == gl_create_context(&visual))
#elif defined(__APPLE__)
                if (GL_TRUE == gl_create_context())
#else
                if (GL_TRUE == gl_create_context(display, &visual))
#endif
                {
                    gl_destroy_context();
                    return -1;
                }

                glewExperimental = GL_TRUE; //><

                int err = glewInit();
                if (GLEW_OK != err)
                {
                    fprintf(stderr, "Error [main]: glewInit failed: %s\n", glewGetErrorString(err));
                    gl_destroy_context();
                    return -1;
                }

                return 0;
            }

            static int finalize()
            {
                gl_destroy_context();
                return 0;
            }

            class GLContext
            {
            public:
                GLContext()
                {
                    inited_ = (0 == init());
                }

                ~GLContext()
                {
                    if (inited_)
                    {
                        finalize();
                    }
                }

                bool is_valid(float version) const
                {
                    if (inited_)
                    {
                        const GLubyte* strVersion = ::glGetString(GL_VERSION);
                        double fVersion = atof((const char*)strVersion);

                        if (version <= fVersion)
                        {
                            return true;
                        }
                    }
                    return inited_;
                }

            private:
                bool inited_;
            };

            static std::unique_ptr<GLContext> g_ContextPtr;
            static bool bEnableGL = true;
        }

        bool ri_tex_init_gl_context()
        {
            if (!g_ContextPtr.get())
            {
                g_ContextPtr.reset(new GLContext());
            }
            return g_ContextPtr->is_valid(1.1f);
        }

        bool ri_tex_get_gl_context(float version)
        {
            if (!g_ContextPtr.get())
            {
                g_ContextPtr.reset(new GLContext());
            }
            return bEnableGL && g_ContextPtr->is_valid(version);
        }

        bool ri_tex_set_enable_gl_context(bool bEnable)
        {
            bEnableGL = bEnable;
            return true;
        }
    }
}