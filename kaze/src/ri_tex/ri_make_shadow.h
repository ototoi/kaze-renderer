#ifndef KAZE_RI_MAKE_SHADOW_H
#define KAZE_RI_MAKE_SHADOW_H

namespace kaze
{
    namespace ri
    {

        int ri_make_shadow(const char* pic, const char* tex);
    }
}

#endif