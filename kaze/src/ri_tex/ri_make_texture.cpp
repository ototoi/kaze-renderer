#include "ri_make_texture.h"
#include "ri_make_texture_helper.h"

#include <string>
#include <vector>
#include <algorithm>
#include <fstream>

#include <stdlib.h>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include "image/png_io.h"
#include "image/kxf_io.h"

#include "memory_image.hpp"

#include "ri_box_filter.h"
//#include "ri_triangle_filter.h"
#include "ri_gauss_filter.h"
#include "ri_nlmeans_filter.h"
#include "ri_waifu2x_filter.h"

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif

#include "filter/corresponder.h"
#include "graphics/image/image_interpolator.hpp"
#include "graphics/texture/image_texture/image_texture.hpp"
#include "graphics/texture/image_texture/unit_image_texture.hpp"

#include "logger.h"
#include "timer.h"

#if 1
#include <picojson.h>

namespace kaze
{
    namespace ri
    {
         class _time_reporter
        {
        public:
            _time_reporter(const std::string& s)
            {
                str_ = s;
                t1.start();
            }
            ~_time_reporter()
            {
                t1.end();
                //printf("%s time : %d msec\n",str_.c_str(), t1.msec());
            }
        private:
            std::string str_;
            timer t1;
        };

        static bool IsExistFile(const char* szFilePath)
        {
#ifdef _WIN32
            struct _stat s;
            int nRet = _stat(szFilePath, &s);
            return (nRet == 0);
#else
            struct stat s;
            int nRet = stat(szFilePath, &s);
            return (nRet == 0);
#endif
        }

        static size_t GetLastModifiedTime(const char* szFilePath)
        {
#ifdef _WIN32
            struct _stat s;
            int nRet = _stat(szFilePath, &s);
            return s.st_mtime;
#else
            struct stat s;
            int nRet = stat(szFilePath, &s);
            return s.st_mtime;
#endif
        }

        static std::string GetExt(const std::string& str)
        {
#ifdef _WIN32
            char ext[_MAX_EXT] = {};
            _splitpath(str.c_str(), NULL, NULL, NULL, ext);
            return ext;
#else
            std::string::size_type i = str.find_last_of('.');
            if (i != std::string::npos)
            {
                return str.substr(i);
            }
            else
            {
                return "";
            }
#endif
        }

        static std::string GetFileName(const std::string& path)
        {
            #ifndef _WIN32
                std::string::size_type f0 = path.find_last_of('/');
                std::string::size_type f1 = path.find_last_of('\\');
                if(f0 != std::string::npos && f1 != std::string::npos)
                {
                    return path.substr(std::max(f0, f1)+1);
                }
                else if(f0 != std::string::npos)
                {
                    return path.substr(f0+1);
                }
                else if(f1 != std::string::npos)
                {
                    return path.substr(f1+1);
                }
                else
                {
                    return "";
                }
            #else
                return path.substr(path.find_last_of('/')+1);
            #endif
        }

        int SaveImageMetaFile(
            const char* szTexFile,
            const char* swrap, const char* twrap, const char* filter, float swidth, float twidth, int width, int height
        )
        {
            std::string str_meta = std::string(szTexFile) + ".meta";

            picojson::object obj;
            obj["api"] = picojson::value("meta");
            obj["version"] = picojson::value("1.0.0");
            {
                picojson::object img_info;
                img_info["path"]   = picojson::value(GetFileName(szTexFile));//todo 
                img_info["width"]  = picojson::value((double)width); 
                img_info["height"] = picojson::value((double)height);
                img_info["dpp"] = picojson::value((double)32);

                obj["image"] = picojson::value(img_info);
            }
            {
                picojson::object filter_info;
                filter_info["swrap"]  = picojson::value(std::string(swrap));//todo 
                filter_info["twrap"]  = picojson::value(std::string(twrap)); 
                filter_info["filter"] = picojson::value(std::string(filter));
                filter_info["swidth"] = picojson::value(swidth);
                filter_info["twidth"] = picojson::value(twidth);
                filter_info["width"]  = picojson::value((double)width); 
                filter_info["height"] = picojson::value((double)height);
                
                obj["filter"] = picojson::value(filter_info);
            }

            std::ofstream ofs(str_meta);
            if(!ofs)return -1;
            //std::cout << str_meta << std::endl;

            picojson::value val(obj);
            ofs << val;
            ofs.flush();

            return 0;
        }

        static bool IsEqual(double x, double y)
        {
            return std::fabs(x-y) < 1e-4;
        }

        static bool CheckMetaFile(const char* szTexFile, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth, int width, int height)
        {
            if(!IsExistFile(szTexFile))return false;
            std::string str_meta = std::string(szTexFile) + ".meta";
            if(!IsExistFile(str_meta.c_str()))return false;

            std::ifstream ifs(str_meta);
            if(!ifs)return -1;

            picojson::value val;
            //ifs >> val;
            std::string err = picojson::parse(val, ifs);
            if(val.is<picojson::null>())return false;

            picojson::object& obj = val.get<picojson::object>();
            if(obj["version"].get<std::string>() != "1.0.0")return false;

            picojson::object& filter_info = obj["filter"].get<picojson::object>();
            
            if(filter_info["swrap"].get<std::string>() != std::string(swrap))return false;
            if(filter_info["twrap"].get<std::string>() != std::string(twrap))return false;
            if(filter_info["filter"].get<std::string>() != std::string(filter))return false;
            if(!IsEqual(filter_info["swidth"].get<double>(), swidth))return false;
            if(!IsEqual(filter_info["twidth"].get<double>(), twidth))return false;

            if(width != 0 && height != 0)
            {
                if(int(filter_info["width"].get<double>())  != width)return false; 
                if(int(filter_info["height"].get<double>()) != height)return false;
            }
            
            return true;
        }
    }
}
#endif

namespace kaze
{
    namespace ri
    {
        enum
        {
            WRAP_REPEAT = 0,
            WRAP_CLAMP,
            WRAP_MIRROR,
            WRAP_BORDER,
        };

        enum
        {
            FILTER_BOX = 0,
            FILTER_TRIANGLE,
            FILTER_MITCHELL,
            FILTER_GAUSS,
            FILTER_SINC,
            FILTER_NL_MEANS,
            FILTER_NL_WAIFU2X,
        };

        enum
        {
            RESIZE_NONE = 0,
            RESIZE_UP,
            RESIZE_DOWN,
            RESIZE_ROUND,
            RESIZE_UP_M,
            RESIZE_DOWN_M,
            RESIZE_ROUND_M,
        };

        struct TokenValue
        {
            const char* token;
            int value;
        };

        struct TokenTokenValue
        {
            const char* token;
            const char* token2;
            int value;
        };

        static TokenTokenValue WRAP_TYPES[] =
            {
                {"repeat", "repeat", WRAP_REPEAT},
                {"periodic", "repeat", WRAP_REPEAT},
                {"wrap", "repeat", WRAP_REPEAT}, //GL_REPEAT
                {"GL_REPEAT", "repeat", WRAP_REPEAT},

                {"clamp", "clamp", WRAP_CLAMP},
                {"clamp_to_edge", "clamp", WRAP_CLAMP}, //GL_CLAMP_TO_EDGE
                {"GL_CLAMP", "clamp", WRAP_CLAMP},      //?
                {"GL_CLAMP_TO_EDGE", "clamp", WRAP_CLAMP},

                {"mirror", "mirror", WRAP_MIRROR}, //GL_MIRRORED_REPEAT
                {"GL_MIRRORED_REPEAT", "mirror", WRAP_MIRROR},

                {"border", "border", WRAP_BORDER},
                {"clamp_to_border", "border", WRAP_BORDER},
                {"black", "border", WRAP_BORDER},
                {"white", "border", WRAP_BORDER}, //GL_CLAMP_TO_BORDER
                {"GL_CLAMP_TO_BORDER", "border", WRAP_BORDER},

                {NULL, "", 0},
        };

        static TokenTokenValue FILTER_TYPES[] =
            {
                {"box", "box", FILTER_BOX},
                {"point", "box", FILTER_BOX},
                {"mean", "box", FILTER_BOX},
                {"triangle", "triangle", FILTER_TRIANGLE},
                {"tent", "triangle", FILTER_TRIANGLE},
                {"mitchel", "mitchell", FILTER_MITCHELL},
                {"mitchell", "mitchell", FILTER_MITCHELL},
                {"gauss", "gauss", FILTER_GAUSS},
                {"gaussian", "gauss", FILTER_GAUSS},
                {"gaussian-soft", "gauss", FILTER_GAUSS},
                {"sinc", "sinc", FILTER_SINC},
                {"nl-means", "nlmeans", FILTER_NL_MEANS},
                {"nl_means", "nlmeans", FILTER_NL_MEANS},
                {"NLMeans", "nlmeans", FILTER_NL_MEANS},
                {"waifu2x", "waifu2x", FILTER_NL_WAIFU2X},
                {NULL, "", 0},
        };

        //gaussian-soft, catmull-rom, mitchell, cubic, lanczos, blackman-harris, and bessel

        static TokenValue RESIZE_TYPES[] =
            {
                {"none", RESIZE_NONE},
                {"up", RESIZE_UP},
                {"down", RESIZE_DOWN},
                {"round", RESIZE_ROUND},
                {"up-", RESIZE_UP_M},
                {"down-", RESIZE_DOWN_M},
                {"round-", RESIZE_ROUND_M},
                {NULL, 0},
        };

#if _WIN32
#defin CMPCASE _stricmp
#else
#define CMPCASE strcasecmp
#endif

        static const char* ToInternalWrap(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (CMPCASE(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].token2;
                i++;
            }
            return "repeat";
        }

        static const char* ToInternalFilter(const char* szFilter)
        {
            int i = 0;
            while (FILTER_TYPES[i].token)
            {
                if (CMPCASE(szFilter, FILTER_TYPES[i].token) == 0) return FILTER_TYPES[i].token2;
                i++;
            }
            return szFilter;
        }

        static int ToWrapType(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (strcmp(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].value;
                i++;
            }
            return WRAP_REPEAT;
        }

        static int ToResizeType(const char* szFilter)
        {
            int i = 0;
            while (RESIZE_TYPES[i].token)
            {
                if (strcmp(szFilter, RESIZE_TYPES[i].token) == 0) return RESIZE_TYPES[i].value;
                i++;
            }
            return RESIZE_NONE;
        }

        static std::shared_ptr<image<color4> > GetImageFromFile(const char* szPicFile)
        {
            return get_image_from_file(szPicFile);
        }

        static bool CheckKTXHeader(const char* szTexFile, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth, int width, int height)
        {
            kxf_header header;
            if (read_kxf_header(szTexFile, &header) != 0) return false;
            if (std::string(header.swrap) != swrap) return false;
            if (std::string(header.twrap) != twrap) return false;
            if (std::string(header.filter) != filter) return false;
            if (header.swidth != swidth) return false;
            if (header.twidth != twidth) return false;
            if (header.width != width) return false;
            if (header.height != height) return false;
            return true;
        }

        static int GetReizeWidth(int width, int R)
        {
            switch (R)
            {
            case RESIZE_NONE:
                return width;
            case RESIZE_UP:
            case RESIZE_UP_M:
            {
                int r = 1;
                while (r < width)
                {
                    r = r << 1;
                }
                return r;
            }
            break;
            case RESIZE_DOWN:
            case RESIZE_DOWN_M:
            {
                int r = 1;
                while ((r << 1) < width)
                {
                    r = r << 1;
                }
                return r;
            }
            break;
            case RESIZE_ROUND:
            case RESIZE_ROUND_M:
            {
                int u = GetReizeWidth(width, RESIZE_UP);
                int d = GetReizeWidth(width, RESIZE_DOWN);
                return (u - width) < (width - d) ? u : d;
            }
            break;
            }
            return GetReizeWidth(width, RESIZE_UP);
        }

        static bool IsLongestResize(int R)
        {
            return (R == RESIZE_UP || R == RESIZE_DOWN || R == RESIZE_ROUND);
        }

        static std::shared_ptr<image<color4> > ResizeImage(const std::shared_ptr<image<color4> >& img, int WR, int HR, int WW, int HW)
        {
            int width = img->get_width();
            int height = img->get_height();

            int r_width = GetReizeWidth(width, WR);
            int r_height = GetReizeWidth(height, HR);
            if (IsLongestResize(WR) || IsLongestResize(HR))
            {
                r_width = r_height = std::max(r_width, r_height);
            }

//--------------------------------------------------------
#ifdef _DEBUG
            printf("%d, %d\n", WR, HR);
            printf("width = %d, height = %d\n", width, height);
            printf("width = %d, height = %d\n", r_width, r_height);
#endif
            //--------------------------------------------------------
            if (width == r_width && height == r_height) return img;

            std::shared_ptr<image<color4> > out(new memory_image<color4>(r_width, r_height));

            std::shared_ptr<corresponder> corx(new powerof2_clamp_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            switch (WW)
            {
            case WRAP_REPEAT:
                corx.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                corx.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                corx.reset(new powerof2_mirror_corresponder());
                break;
            }

            switch (HW)
            {
            case WRAP_REPEAT:
                cory.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                cory.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                cory.reset(new powerof2_mirror_corresponder());
                break;
            }

            std::shared_ptr<unit_image_texture<color4> > tex(
                new unit_image_texture<color4>(
                    img,
                    corx,
                    cory,
                    new bilinear_image_interpolator<color4>()));

            for (int y = 0; y < r_height; y++)
            {
                real yy = (y + 0.5) / r_height;
                for (int x = 0; x < r_width; x++)
                {
                    real xx = (x + 0.5) / r_width;
                    out->set(x, y, tex->get(xx, yy));
                }
            }

            return out;
        }

        static std::shared_ptr<image<color4> > ResizeImage(
            const std::shared_ptr<image<color4> >& img,
            const char* szWResize = "up", const char* szHResize = "up",
            const char* szWWrap = "repeat", const char* szHWrap = "repeat")
        {
            int WR = ToResizeType(szWResize);
            int HR = ToResizeType(szHResize);
            int WW = ToWrapType(szWWrap);
            int HW = ToWrapType(szHWrap);
            return ResizeImage(img, WR, HR, WW, HW);
        }

        static std::shared_ptr<image<color4> > FilterImage(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            const char* filter, float swidth, float twidth,
            int n, const char* tokens[], const char* params[])
        {
            std::string filterType = ToInternalFilter(filter);
            if (filterType == "box")
            {
                return ri_box_filter(img, swrap, twrap, swidth, twidth);
            }
            //else if(filterType == "triangle")
            //{
            //    return ri_triangle_filter(img, swrap, twrap, swidth, twidth);
            //}
            else if (filterType == "mitchell")
            {
                return ri_gauss_filter(img, swrap, twrap, swidth, twidth);
            }
            else if (filterType == "gauss")
            {
                return ri_gauss_filter(img, swrap, twrap, swidth, twidth);
            }
            else if (filterType == "waifu2x")
            {
                std::string modelName = "noise2_model";
                for (int i = 0; i < n; i++)
                {
                    if (strcmp(tokens[i], "model") == 0)
                    {
                        if (strcmp(params[i] , "") != 0)
                        {
                            modelName = params[i];
                        }
                    }
                }

                return ri_waifu2x_filter(img, swrap, twrap, swidth, twidth, modelName.c_str());
            }
            else if (filterType == "nlmeans")
            {
                return ri_nlmeans_filter(img, swrap, twrap, swidth, twidth);
            }

            return img;
        }

        static int ConvertTexture(const char* szPicFile, const char* szTexFile, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth, int n, const char* tokens[], const char* params[], bool bForce = false)
        {
            timer t1;

            _time_reporter tr1("ConvertTexture");

            bool bMipmap = false;
            bool bDebug = false;
            std::string strSresize = "up-";
            std::string strTresize = "up-";

            for (int i = 0; i < n; i++)
            {
                if (strcmp(tokens[i], "mipmap") == 0)
                {
                    bMipmap = (atoi(params[i])) ? true : false;
                }
            }

            std::string strExt = GetExt(szTexFile);
            if (strExt != ".kxf")
            {
                bMipmap = false;
            }

            for (int i = 0; i < n; i++)
            {
                if (strcmp(tokens[i], "sresize") == 0)
                {
                    strSresize = params[i];
                }
                else if (strcmp(tokens[i], "tresize") == 0)
                {
                    strTresize = params[i];
                }
                else if (strcmp(tokens[i], "resize") == 0)
                {
                    strSresize = strTresize = params[i];
                }
                else if (strcmp(tokens[i], "force") == 0)
                {
                    bForce = (atoi(params[i])) ? true : false;
                }
                else if (strcmp(tokens[i], "debug") == 0)
                {
                    bDebug = (atoi(params[i])) ? true : false;
                }
            }

            if (bMipmap)
            {
                if (strSresize == "none" || strTresize == "none")
                {
                    strSresize = "up";
                    strTresize = "up";
                }
            }

#ifdef _DEBUG
            bDebug = true;
#endif
            //bForce = true;
            //bDebug = true;

            const char* sresize = strSresize.c_str();
            const char* tresize = strTresize.c_str();

            //int WW = ToWrapType(swrap);
            //int HW = ToWrapType(twrap);
            //printf("CheckMetaFile:...\n");
            if (!bForce)
            {
                _time_reporter t2("Check Meta File");
                std::string str_meta = std::string(szTexFile) + ".meta";
                if(IsExistFile(str_meta.c_str()))
                {
                    size_t mpic = GetLastModifiedTime(szPicFile);
                    size_t mmta = GetLastModifiedTime(str_meta.c_str());
                    if(mpic <= mmta)
                    {
                        //printf("....1\n");
                        int width = 0;
                        int height = 0;
                        if(CheckMetaFile(szTexFile, swrap, twrap, filter, swidth, twidth, width, height))
                        {
                            //printf("CheckMetaFile:SKIP1\n");
                            return 0;
                        }
                    }
                }
                bForce = true;
            }

            t1.start();
            std::shared_ptr<image<color4> > img = GetImageFromFile(szPicFile);
            if (!img.get()) return -1;
            t1.end();

            {
                int WR = ToResizeType(sresize);
                int HR = ToResizeType(tresize);
                int width = GetReizeWidth(img->get_width(), WR);
                int height = GetReizeWidth(img->get_height(), HR);
                if (bMipmap || IsLongestResize(WR) || IsLongestResize(HR))
                {
                    width = height = std::max(width, height);
                }
                if (!bForce && strExt == ".kxf" && CheckKTXHeader(szTexFile, swrap, twrap, filter, swidth, twidth, width, height))
                {
                    return 0;
                }
                else
                {
                    t1.start();
                    img = FilterImage(img, swrap, twrap, filter, swidth, twidth, n, tokens, params);
                    t1.end();
                    //printf("filter time:%d msec.\n", t1.msec());
                    img = ResizeImage(img, sresize, tresize, swrap, twrap);
                    std::vector<std::shared_ptr<image<color4> > > vecImg;
                    if (bMipmap)
                    {
                        create_mipmap_images(vecImg, img, swrap, twrap);
                    }
                    else
                    {
                        vecImg.push_back(img);
                    }
                    if (bDebug)
                    {
                        save_debug_images(szTexFile, vecImg);
                    }
                    if (strExt == ".kxf")
                    {
                        int nRet = 0;
                        nRet = save_to_kxf(szTexFile, vecImg, swrap, twrap, filter, swidth, twidth);
                        if(nRet != 0)return nRet;
                        nRet = SaveImageMetaFile(szTexFile, swrap, twrap, filter, swidth, twidth, img->get_width(), img->get_height());
                        return nRet;
                    }
                    else
                    {
                        int nRet = 0;
                        nRet = save_image_to_files(szTexFile, vecImg);
                        if(nRet != 0)return nRet;
                        nRet = SaveImageMetaFile(szTexFile, swrap, twrap, filter, swidth, twidth, img->get_width(), img->get_height());
                        return nRet;
                    }
                }
            }
        }

        static int MakeTexture(const char* pic, const char* tex, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth, int n, const char* tokens[], const char* params[])
        {
            _time_reporter tr("total");

            int nRet = 0;
            if (IsExistFile(pic)) nRet |= 1;
            if (IsExistFile(tex)) nRet |= 2;

            switch (nRet)
            {
            case 0:
                return -1;
            case 1:
                return ConvertTexture(pic, tex, swrap, twrap, filter, swidth, twidth, n, tokens, params, true);
            case 2:
                return -1;
            case 3:
            {
                size_t mpic = GetLastModifiedTime(pic);
                size_t mtex = GetLastModifiedTime(tex);
                bool bForce = mpic >= mtex;
                return ConvertTexture(pic, tex, swrap, twrap, filter, swidth, twidth, n, tokens, params, bForce);
            }
            }

            return -1;
        }

        int ri_make_texture(const char* pic, const char* tex, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth)
        {
            return ri_make_texture(pic, tex, swrap, twrap, filter, swidth, twidth, 0, 0, 0);
        }

        int ri_make_texture(const char* pic, const char* tex, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth, int n, const char* tokens[], const char* params[])
        {
            std::string strSwrap = ToInternalWrap(swrap);
            std::string strTwrap = ToInternalWrap(twrap);
            std::string strFilter = ToInternalFilter(filter);

            return MakeTexture(pic, tex, strSwrap.c_str(), strTwrap.c_str(), strFilter.c_str(), swidth, twidth, n, tokens, params);
        }
    }
}
