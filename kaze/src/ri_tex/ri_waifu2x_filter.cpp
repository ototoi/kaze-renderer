#include "ri_waifu2x_filter.h"
#include "ri_tex_gl_context.h"
#include "ri_make_texture_helper.h"

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif

#include <stdio.h>
#include <memory>
#include <vector>
#include <memory_image.hpp>

#include <gl_tex/gl_YUV_filter_pass.h>
#include <gl_tex/gl_waifu2x_filter_pass.h>

#include <color.h>
#include <filter/corresponder.h>
#include <graphics/image/image_interpolator.hpp>
#include <graphics/image/corresponded_image.hpp>
#include <graphics/texture/image_texture/image_texture.hpp>
#include <graphics/texture/image_texture/unit_image_texture.hpp>


#include "timer.h"
#include <map>

#include "waifu2x/Model.h"

#include "aligned_allocator.hpp"

#include <xmmintrin.h>
#include <immintrin.h>

#ifdef __APPLE__
#include <dispatch/dispatch.h>
#endif

#ifdef _WIN32
#include <windows.h>
#else
#include <libgen.h>
#include <stdlib.h>
#endif

#ifdef __APPLE__
#include <mach-o/dyld.h>
#endif

#if defined(__linux__) || defined(FREEBSD)
#include <unistd.h>
#endif

using namespace gl::tex;

namespace kaze
{
    namespace ri
    {

        enum
        {
            WRAP_REPEAT = 0,
            WRAP_CLAMP,
            WRAP_MIRROR,
            WRAP_BORDER,
        };

        struct TokenValueValue
        {
            const char* token;
            int value1;
            int value2;
        };

        static TokenValueValue WRAP_TYPES[] =
            {
                {"repeat", WRAP_REPEAT, GL_REPEAT},
                {"clamp", WRAP_CLAMP, GL_CLAMP_TO_EDGE},
                {"mirror", WRAP_MIRROR, GL_MIRRORED_REPEAT},
                {"border", WRAP_BORDER, GL_CLAMP_TO_BORDER},
                {NULL, 0},
        };

        static int ToGLWrap(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (strcmp(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].value2;
                i++;
            }
            return GL_CLAMP_TO_EDGE;
        }

        static int ToWrapType(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (strcmp(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].value1;
                i++;
            }
            return WRAP_REPEAT;
        }

        class Waifu2xStatistic
        {
        private:
            Waifu2xStatistic() {}
        public:
            static Waifu2xStatistic& GetInstance()
            {
                static Waifu2xStatistic mgr;
                return mgr;
            }

        public:
            void SetTime(const std::string& key, float time)
            {
                times_[key] = time;
            }
            void AddTime(const std::string& key, float time)
            {
                times_[key] += time;
            }
            float GetTime(const std::string& key)
            {
                return times_[key];
            }

        protected:
            std::map<std::string, float> times_;
        };

        class Waifu2xModels
        {
        public:
            Waifu2xModels(const std::string& strModelPath, bool bBinary = false)
            {
                if (!bBinary)
                {
                    waifu2x::ModelBuilder::LoadModelFromJSONObject(models, strModelPath.c_str());
                }
                else
                {
                    waifu2x::ModelBuilder::LoadModelFromBinObject(models, strModelPath.c_str());
                }
            }
            const std::vector<std::shared_ptr<waifu2x::Model> >& GetModels() const { return models; }
        private:
            std::vector<std::shared_ptr<waifu2x::Model> > models;
        };

        static std::string ri_get_full_path(const std::string& path)
        {
#if defined(_WIN32)
            char szFullPath[_MAX_PATH];
            _fullpath(szFullPath, path.c_str(), _MAX_PATH);
            return szFullPath;
#else
            char szFullPath[1024];
            realpath(path.c_str(), szFullPath);
            return szFullPath;
#endif
        }

        static std::string ri_get_exec_path_()
        {
#if defined(_WIN32)
            char buffer[_MAX_PATH];
            ::GetModuleFileNameA(NULL, buffer, _MAX_PATH);
            return buffer;
#elif defined(__APPLE__)
            char path[1024] = {};
            uint32_t size = sizeof(path);
            _NSGetExecutablePath(path, &size);
            return ri_get_full_path(path);
#elif defined(__linux__)
            char path[1024] = {};
            size_t size = sizeof(path);
            readlink("/proc/self/exe", path, size);
            return path;
#elif defined(FREEBSD)
            char path[1024] = {};
            size_t size = sizeof(path);
            readlink("/proc/curproc/file", path, size);
            return path;
#else
            return "";
#endif
        }

        static std::string dirname_(const std::string& path)
        {
            return path.substr(0, path.find_last_of('/'));
        }

        static bool IsExistFile(const char* szFilePath)
        {
#ifdef _WIN32
            struct _stat s;
            int nRet = _stat(szFilePath, &s);
            return (nRet == 0);
#else
            struct stat s;
            int nRet = stat(szFilePath, &s);
            return (nRet == 0);
#endif
        }

        class Waifu2xModelsManager
        {
        public:
            typedef std::map<std::string, std::shared_ptr<Waifu2xModels> > map_type;
            typedef map_type::value_type value_type;

            static Waifu2xModelsManager& GetInstance()
            {
                static Waifu2xModelsManager mgr;
                return mgr;
            }

        public:
            const std::vector<std::shared_ptr<waifu2x::Model> >& GetModels() const
            {
                return this->GetModels(currentKey);
            }
            const std::vector<std::shared_ptr<waifu2x::Model> >& GetModels(const std::string& key) const
            {
                currentKey = key;
                typedef map_type::iterator iterator;
                typedef map_type::const_iterator const_iterator;
                const_iterator iter = modelsMapPtr.find(key);
                if (iter != modelsMapPtr.end())
                {
                    return iter->second->GetModels();
                }
                else
                {
                    std::string strModelBinPath = GetModelPath() + "/" + key + ".bin";
                    std::string strModelJsonPath = GetModelPath() + "/" + key + ".json";
                    std::string strModelPath = strModelJsonPath;
                    bool bBinary = false;
                    if (IsExistFile(strModelBinPath.c_str()))
                    {
                        bBinary = true;
                        strModelPath = strModelBinPath;
                    }
                    std::shared_ptr<Waifu2xModels> models(new Waifu2xModels(strModelPath, bBinary));
                    std::pair<iterator, bool> iter2 = modelsMapPtr.insert(value_type(key, models));
                    return iter2.first->second->GetModels();
                }
            }

        private:
            Waifu2xModelsManager()
            {
                currentKey = "noise2_model";
                if (0)
                {
                    {
                        std::string strModelPath = GetModelPath() + "/" + "noise1_model" + ".json";
                    }
                    {
                        std::string strModelPath = GetModelPath() + "/" + "noise2_model" + ".json";
                        modelsMapPtr.insert(value_type("noise2_model", std::shared_ptr<Waifu2xModels>(new Waifu2xModels(strModelPath))));
                    }
                    {
                        std::string strModelPath = GetModelPath() + "/" + "rgb/noise2_model" + ".json";
                        modelsMapPtr.insert(value_type("rgb/noise2_model", std::shared_ptr<Waifu2xModels>(new Waifu2xModels(strModelPath))));
                    }
                }
            }

            std::string GetModelPath() const
            {
#ifdef _WIN32
                static std::string strExec = ri_get_exec_path_();
                char szDrive[_MAX_DRIVE];
                char szDir[_MAX_DIR];
                _splitpath(strExec.c_str(), szDrive, szDir, NULL, NULL);
                std::string strRet1;
                strRet1 += szDrive;
                strRet1 += szDir;
                strRet1 += "../others/models/";
                return ri_get_full_path(strRet1);
#else
                static std::string strExec = ri_get_full_path(ri_get_exec_path_());
                static std::string strDir = dirname_(strExec);
                std::string strRet1;
                strRet1 = strDir + "/";
                strRet1 += "../others/models/";
                return ri_get_full_path(strRet1) + "/";
#endif
            }

        protected:
            mutable std::map<std::string, std::shared_ptr<Waifu2xModels> > modelsMapPtr;
            mutable std::string currentKey;
        };

        static std::shared_ptr<image<color4> > FilterImageGL(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth,
            const char* modelName)
        {
            Waifu2xModelsManager& mgr = Waifu2xModelsManager::GetInstance();
            const std::vector<std::shared_ptr<waifu2x::Model> >& models = mgr.GetModels(modelName);

            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<gl::gl_texture_2D> texInput(new gl::gl_texture_2D());
            std::shared_ptr<gl::gl_texture_2D> texOutput(new gl::gl_texture_2D());
            {
                std::vector<float> fBuffer(4 * width * height);
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        size_t index = y * width + x;
                        color4 c = img->get(x, y);
                        fBuffer[4 * index + 0] = (float)c[0];
                        fBuffer[4 * index + 1] = (float)c[1];
                        fBuffer[4 * index + 2] = (float)c[2];
                        fBuffer[4 * index + 3] = (float)c[3];
                    }
                }

                texInput->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, &fBuffer[0]);
            }
            {
                texInput->bind();
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);  //not biliear
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);  //
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, ToGLWrap(swrap)); //TODO
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, ToGLWrap(twrap)); //TODO
                texInput->unbind();
            }
            texOutput->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, 0);
            {
                texOutput->bind();
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);  //not biliear
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);  //
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, ToGLWrap(swrap)); //TODO
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, ToGLWrap(twrap)); //TODO
                texOutput->unbind();
            }

            //
            std::vector<std::shared_ptr<gl_pass> > passList;
            passList.push_back(std::shared_ptr<gl_pass>(new gl_waifu2x_filter_pass(width, height, texInput, texOutput, models)));
            //
            for (size_t i = 0; i < passList.size(); i++)
            {
                passList[i]->run();
            }
            //

            std::shared_ptr<image<color4> > outImg;
            std::vector<float> fBuffer(4 * width * height, 0.0f);
            texOutput->bind();
            ::glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &fBuffer[0]);
            texOutput->unbind();

            outImg.reset(new memory_image<color4>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    size_t index = y * width + x;
                    float r = fBuffer[4 * index + 0];
                    float g = fBuffer[4 * index + 1];
                    float b = fBuffer[4 * index + 2];
                    float a = fBuffer[4 * index + 3];
                    outImg->set(x, y, color4(r, g, b, a));
                }
            }

            return outImg;
        }

        static std::shared_ptr<image<color4> > ClipImage(
            std::shared_ptr<image<color4> >& img,
            int x0, int x1, int y0, int y1)
        {
            int w = x1 - x0;
            int h = y1 - y0;
            std::shared_ptr<image<color4> > outImage(new memory_image<color4>(w, h));
            for (int y = y0; y < y1; y++)
            {
                for (int x = x0; x < x1; x++)
                {
                    color4 c = img->get(x, y);
                    outImage->set(x - x0, y - y0, c);
                }
            }
            return outImage;
        }

        static std::shared_ptr<image<color4> > GetWrapedImage(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap)
        {
            int WW = ToWrapType(swrap);
            int HW = ToWrapType(twrap);

            std::shared_ptr<corresponder> corx(new powerof2_clamp_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            switch (WW)
            {
            case WRAP_REPEAT:
                corx.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                corx.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                corx.reset(new powerof2_mirror_corresponder());
                break;
            }

            switch (HW)
            {
            case WRAP_REPEAT:
                cory.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                cory.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                cory.reset(new powerof2_mirror_corresponder());
                break;
            }

            std::shared_ptr<image<color4> > outImage(
                new corresponded_image<color4>(
                    img,
                    corx,
                    cory));
            return outImage;
        }

        static std::shared_ptr<image<color4> > FilterImageGLTiled(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth,
            const char* modelName)
        {
            static const int TILE_WIDTH = 256;
            Waifu2xModelsManager& mgr = Waifu2xModelsManager::GetInstance();
            const std::vector<std::shared_ptr<waifu2x::Model> >& models = mgr.GetModels(modelName);
            int MODEL_SIZE = (int)(models.size());
            int TILE_MARGIN = 1 * MODEL_SIZE;
            int BLOCK = TILE_WIDTH - 2 * TILE_MARGIN;

            int width = img->get_width();
            int height = img->get_height();

            int bw = (int)ceil(float(width) / BLOCK);
            int bh = (int)ceil(float(height) / BLOCK);

            std::shared_ptr<image<color4> > wrapedImage = GetWrapedImage(img, swrap, twrap);

            std::shared_ptr<image<color4> > outImage(new memory_image<color4>(width, height));

            for (int by = 0; by < bh; by++)
            {
                int y0 = by * BLOCK;
                int y1 = std::min<int>(y0 + BLOCK, height);
                int hh = y1 - y0;
                for (int bx = 0; bx < bw; bx++)
                {
                    int x0 = bx * BLOCK;
                    int x1 = std::min<int>(x0 + BLOCK, width);
                    int ww = x1 - x0;
                    std::shared_ptr<image<color4> > tileInImg = ClipImage(
                        wrapedImage,
                        x0 - TILE_MARGIN, x1 + TILE_MARGIN,
                        y0 - TILE_MARGIN, y1 + TILE_MARGIN);

                    std::shared_ptr<image<color4> > tileOutImg = FilterImageGL(
                        tileInImg,
                        swrap, twrap,
                        swidth, twidth,
                        modelName);

                    for (int yy = 0; yy < hh; yy++)
                    {
                        int yyy = y0 + yy;
                        if (height <= yyy) continue;
                        for (int xx = 0; xx < ww; xx++)
                        {
                            int xxx = x0 + xx;
                            if (width <= xxx) continue;
                            color4 c = tileOutImg->get(xx + TILE_MARGIN, yy + TILE_MARGIN);
                            outImage->set(xxx, yyy, c);
                        }
                    }
                }
            }
            return outImage;
        }
        //-------------------------------------
        static color4 Conv(const color4f& c)
        {
            return color4(c[0], c[1], c[2], c[3]);
        }
        static color4f Conv(const color4& c)
        {
            return color4f(c[0], c[1], c[2], c[3]);
        }

        static std::shared_ptr<image<color4f> > ConvertImage(const std::shared_ptr<image<color4> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<image<color4f> > outImg(new memory_image<color4f>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    outImg->set(x, y, Conv(img->get(x, y)));
                }
            }
            return outImg;
        }

        static std::shared_ptr<image<color4> > ConvertImage(const std::shared_ptr<image<color4f> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<image<color4> > outImg(new memory_image<color4>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    outImg->set(x, y, Conv(img->get(x, y)));
                }
            }
            return outImg;
        }

        static color4f RGB2YUV(const color4f& c)
        {
            color3f cc = color3f(c[0], c[1], c[2]);
            float Y = dot(color3f(0.299, 0.587, 0.114), cc);
            float U = dot(color3f(-0.147, -0.289, 0.436), cc);
            float V = dot(color3f(0.615, -0.515, -0.100), cc);
            float A = c[3];
            return color4f(Y, U, V, A);
        }

        static color4f YUV2RGB(const color4f& c)
        {
            color3f cc = color3f(c[0], c[1], c[2]);
            float R = dot(color3f(1.000, 0.000, 1.140), cc);
            float G = dot(color3f(1.000, -0.395, -0.580), cc);
            float B = dot(color3f(1.000, 2.032, 0.000), cc);
            float A = c[3];
            return color4f(R, G, B, A);
        }

        static std::shared_ptr<image<color4f> > GetYUVImage(const std::shared_ptr<image<color4f> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<image<color4f> > outImg(new memory_image<color4f>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    outImg->set(x, y, RGB2YUV(img->get(x, y)));
                }
            }
            return outImg;
        }

        static std::shared_ptr<image<color4f> > GetRGBImage(const std::shared_ptr<image<color4f> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<image<color4f> > outImg(new memory_image<color4f>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    outImg->set(x, y, YUV2RGB(img->get(x, y)));
                }
            }
            return outImg;
        }

        static std::shared_ptr<image<color4f> > CompositeImage(
            const std::shared_ptr<image<float> >& img1,
            const std::shared_ptr<image<float> >& img2,
            const std::shared_ptr<image<float> >& img3,
            const std::shared_ptr<image<float> >& img4)
        {
            int width = img1->get_width();
            int height = img1->get_height();
            std::shared_ptr<image<color4f> > outImg(new memory_image<color4f>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    float f1 = img1->get(x, y);
                    float f2 = img2->get(x, y);
                    float f3 = img3->get(x, y);
                    float f4 = img4->get(x, y);
                    outImg->set(x, y, color4f(f1, f2, f3, f4));
                }
            }
            return outImg;
        }

        static std::shared_ptr<image<float> > SplitImage(const std::shared_ptr<image<color4f> >& img, int ch)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<image<float> > outImg(new memory_image<float>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    outImg->set(x, y, img->get(x, y)[ch]);
                }
            }
            return outImg;
        }

        typedef std::vector<float, aligned_allocator<float, 32> > alvector;

        static std::shared_ptr<image<float> > GetOutputPlane(
            const std::vector<std::shared_ptr<image<float> > >& inputPlanes,
            const std::vector<std::shared_ptr<waifu2x::Mat> >& inputWeights,
            float bias)
        {
            int width = inputPlanes[0]->get_width();
            int height = inputPlanes[0]->get_height();
            int nInputPlanes = inputPlanes.size();

            std::vector<const memory_image<float>*> inputs(nInputPlanes);
            for (int i = 0; i < nInputPlanes; i++)
            {
                inputs[i] = dynamic_cast<const memory_image<float>*>(inputPlanes[i].get());
            }

            __m256 wx[9 * 128] = {};
            for (int i = 0; i < nInputPlanes; i++)
            {
                float w[9] = {};
                memcpy(w, inputWeights[i]->GetPtr(), sizeof(float) * 9);
                wx[9 * i + 0] = _mm256_broadcast_ss(w + 0);
                wx[9 * i + 1] = _mm256_broadcast_ss(w + 1);
                wx[9 * i + 2] = _mm256_broadcast_ss(w + 2);
                wx[9 * i + 3] = _mm256_broadcast_ss(w + 3);
                wx[9 * i + 4] = _mm256_broadcast_ss(w + 4);
                wx[9 * i + 5] = _mm256_broadcast_ss(w + 5);
                wx[9 * i + 6] = _mm256_broadcast_ss(w + 6);
                wx[9 * i + 7] = _mm256_broadcast_ss(w + 7);
                wx[9 * i + 8] = _mm256_broadcast_ss(w + 8);
            }

            int bufsz = (int)ceil(float(width) / 8) * 8;
            int blk = bufsz / 8;

            alvector v0(bufsz);
            alvector v1(bufsz);
            alvector v2(bufsz);
            alvector v3(bufsz);
            alvector v4(bufsz);
            alvector v5(bufsz);
            alvector v6(bufsz);
            alvector v7(bufsz);
            alvector v8(bufsz);
            alvector vo(bufsz);

            float* f0 = &v0[0];
            float* f1 = &v1[0];
            float* f2 = &v2[0];
            float* f3 = &v3[0];
            float* f4 = &v4[0];
            float* f5 = &v5[0];
            float* f6 = &v6[0];
            float* f7 = &v7[0];
            float* f8 = &v8[0];
            float* fo = &vo[0];

            memset(f0, 0, sizeof(float) * bufsz);
            memset(f1, 0, sizeof(float) * bufsz);
            memset(f2, 0, sizeof(float) * bufsz);
            memset(f3, 0, sizeof(float) * bufsz);
            memset(f4, 0, sizeof(float) * bufsz);
            memset(f5, 0, sizeof(float) * bufsz);
            memset(f6, 0, sizeof(float) * bufsz);
            memset(f7, 0, sizeof(float) * bufsz);
            memset(f8, 0, sizeof(float) * bufsz);
            memset(fo, 0, sizeof(float) * bufsz);

            float coef[] = {bias, 0.1f};
            __m256 mbias = _mm256_broadcast_ss(coef + 0);
            __m256 m01 = _mm256_broadcast_ss(coef + 1);

            std::unique_ptr<memory_image<float> > outImg(new memory_image<float>(width, height));
            float* outPtr = outImg->get_ptr();

            for (int y = 1; y < height - 1; y++)
            {
                memset(fo, 0, sizeof(float) * bufsz);
                for (int i = 0; i < nInputPlanes; i++)
                {
                    const float* imgPtr = inputs[i]->get_ptr();

                    memcpy(f0, imgPtr + (y - 1) * width + (+0), sizeof(float) * (width - 0));
                    memcpy(f1, imgPtr + (y - 1) * width + (+1), sizeof(float) * (width - 1));
                    memcpy(f2, imgPtr + (y - 1) * width + (+2), sizeof(float) * (width - 2));
                    memcpy(f3, imgPtr + (y + 0) * width + (+0), sizeof(float) * (width - 0));
                    memcpy(f4, imgPtr + (y + 0) * width + (+1), sizeof(float) * (width - 1));
                    memcpy(f5, imgPtr + (y + 0) * width + (+2), sizeof(float) * (width - 2));
                    memcpy(f6, imgPtr + (y + 1) * width + (+0), sizeof(float) * (width - 0));
                    memcpy(f7, imgPtr + (y + 1) * width + (+1), sizeof(float) * (width - 1));
                    memcpy(f8, imgPtr + (y + 1) * width + (+2), sizeof(float) * (width - 2));

                    __m256 w0 = wx[9 * i + 0];
                    __m256 w1 = wx[9 * i + 1];
                    __m256 w2 = wx[9 * i + 2];
                    __m256 w3 = wx[9 * i + 3];
                    __m256 w4 = wx[9 * i + 4];
                    __m256 w5 = wx[9 * i + 5];
                    __m256 w6 = wx[9 * i + 6];
                    __m256 w7 = wx[9 * i + 7];
                    __m256 w8 = wx[9 * i + 8];

                    for (int b = 0; b < blk; b++)
                    {
                        __m256 s = _mm256_load_ps(fo + 8 * b);
                        s = _mm256_fmadd_ps(w0, _mm256_load_ps(f0 + 8 * b), s);
                        s = _mm256_fmadd_ps(w1, _mm256_load_ps(f1 + 8 * b), s);
                        s = _mm256_fmadd_ps(w2, _mm256_load_ps(f2 + 8 * b), s);
                        s = _mm256_fmadd_ps(w3, _mm256_load_ps(f3 + 8 * b), s);
                        s = _mm256_fmadd_ps(w4, _mm256_load_ps(f4 + 8 * b), s);
                        s = _mm256_fmadd_ps(w5, _mm256_load_ps(f5 + 8 * b), s);
                        s = _mm256_fmadd_ps(w6, _mm256_load_ps(f6 + 8 * b), s);
                        s = _mm256_fmadd_ps(w7, _mm256_load_ps(f7 + 8 * b), s);
                        s = _mm256_fmadd_ps(w8, _mm256_load_ps(f8 + 8 * b), s);
                        _mm256_store_ps(fo + 8 * b, s);
                    }
                }

                for (int b = 0; b < blk; b++)
                {
                    __m256 s = _mm256_load_ps(fo + 8 * b);
                    // Leaky ReLU Process
                    s = _mm256_add_ps(s, mbias);                                                                           //s += bias;
                    s = _mm256_max_ps(s, _mm256_setzero_ps()) + _mm256_mul_ps(_mm256_min_ps(s, _mm256_setzero_ps()), m01); //s = max(s, 0.0f) + min(s, 0.0f) * 0.1f;
                    _mm256_store_ps(fo + 8 * b, s);
                }

                float* line = outPtr + y * width;
                memcpy(line + 1, fo, sizeof(float) * (width - 2));
            }

            return std::shared_ptr<image<float> >(outImg.release());
        }

        template <class T>
        void FilterImageFromModel(
            std::vector<std::shared_ptr<image<T> > >& outputPlanes,
            const std::vector<std::shared_ptr<image<T> > >& inputPlanes,
            const std::shared_ptr<waifu2x::Model>& model)
        {
            int nOutputPlanes = model->GetOutputPlanes();
            int nInputPlanes = model->GetInputPlanes();
            const std::vector<std::shared_ptr<waifu2x::Mat> >& weights = model->GetWeights();
            const std::vector<float>& biases = model->GetBiases();
            timer t1;
            for (int o = 0; o < nOutputPlanes; o++)
            {
                std::vector<std::shared_ptr<waifu2x::Mat> > inputWeights;
                float bias = biases[o];
                for (int i = 0; i < nInputPlanes; i++)
                {
                    inputWeights.push_back(weights[o * nInputPlanes + i]);
                }
                t1.start();
                outputPlanes.push_back(GetOutputPlane(inputPlanes, inputWeights, bias));
                t1.end();
                Waifu2xStatistic::GetInstance().AddTime("GetOutputPlane", t1.msec());
            }
        }

        template <class T>
        std::shared_ptr<image<T> > ExtendImage(
            const std::shared_ptr<image<T> >& img,
            const char* swrap, const char* twrap,
            int nModels)
        {
            int WW = ToWrapType(swrap);
            int HW = ToWrapType(twrap);

            std::shared_ptr<corresponder> corx(new powerof2_clamp_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            switch (WW)
            {
            case WRAP_REPEAT:
                corx.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                corx.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                corx.reset(new powerof2_mirror_corresponder());
                break;
            }

            switch (HW)
            {
            case WRAP_REPEAT:
                cory.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                cory.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                cory.reset(new powerof2_mirror_corresponder());
                break;
            }

            int width = img->get_width();
            int height = img->get_height();

            std::shared_ptr<image<T> > corImg(
                new corresponded_image<T>(
                    img,
                    corx,
                    cory));

            int widthExt = width + 2 * nModels; //(int)std::ceil(float(width+2*nModels)/8)*8;
            int heightExt = height + 2 * nModels;

            std::shared_ptr<image<T> > outImg(new memory_image<T>(widthExt, heightExt));
            for (int y = 0; y < heightExt; y++)
            {
                for (int x = 0; x < widthExt; x++)
                {
                    outImg->set(x, y, corImg->get(x - nModels, y - nModels));
                }
            }
            return outImg;
        }

        template <class T>
        std::shared_ptr<image<T> > ClipImage(
            const std::shared_ptr<image<T> >& img,
            int x0, int x1, int y0, int y1)
        {
            int w = x1 - x0;
            int h = y1 - y0;
            std::shared_ptr<image<T> > outImage(new memory_image<T>(w, h));
            for (int y = y0; y < y1; y++)
            {
                for (int x = x0; x < x1; x++)
                {
                    outImage->set(x - x0, y - y0, img->get(x, y));
                }
            }
            return outImage;
        }

        template <class T>
        std::shared_ptr<image<T> > GetWrapedImage(
            const std::shared_ptr<image<T> >& img,
            const char* swrap, const char* twrap)
        {
            int WW = ToWrapType(swrap);
            int HW = ToWrapType(twrap);

            std::shared_ptr<corresponder> corx(new powerof2_clamp_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            switch (WW)
            {
            case WRAP_REPEAT:
                corx.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                corx.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                corx.reset(new powerof2_mirror_corresponder());
                break;
            }

            switch (HW)
            {
            case WRAP_REPEAT:
                cory.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                cory.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                cory.reset(new powerof2_mirror_corresponder());
                break;
            }

            std::shared_ptr<image<T> > outImage(
                new corresponded_image<T>(
                    img,
                    corx,
                    cory));
            return outImage;
        }

        static std::shared_ptr<image<color4> > GrayscaleImage(const std::shared_ptr<image<float> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<image<color4> > outImg(new memory_image<color4>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    float c = img->get(x, y);
                    outImg->set(x, y, color4(c, c, c, 1));
                }
            }
            return outImg;
        }

        static void WriteTempImage(const std::shared_ptr<image<float> >& img, const char* szPath)
        {
            save_image_to_file(szPath, GrayscaleImage(img));
        }

        static void WriteTempImages(const std::vector<std::shared_ptr<image<float> > >& outputPlanes, const char* szPrefix)
        {
            char buffer[512];
            size_t sz = outputPlanes.size();
            for (size_t i = 0; i < sz; i++)
            {
                sprintf(buffer, "%s_i%2lu", szPrefix, i);
                WriteTempImage(outputPlanes[i], buffer);
            }
        }

        static std::shared_ptr<image<float> > FilterImageFromModels(
            const std::shared_ptr<image<float> >& img,
            const char* swrap, const char* twrap,
            const char* modelName)
        {
            Waifu2xModelsManager& mgr = Waifu2xModelsManager::GetInstance();
            const std::vector<std::shared_ptr<waifu2x::Model> >& models = mgr.GetModels(modelName);
            int msz = (int)models.size();

            int width = img->get_width();
            int height = img->get_height();

            std::shared_ptr<image<float> > extImg = ExtendImage(img, swrap, twrap, msz);

            std::vector<std::shared_ptr<image<float> > > inputPlanes;
            std::vector<std::shared_ptr<image<float> > > outputPlanes;
            inputPlanes.push_back(extImg);

            timer t1;
            for (int i = 0; i < msz; i++)
            {
                t1.start();
                FilterImageFromModel(outputPlanes, inputPlanes, models[i]);
                t1.end();
                Waifu2xStatistic::GetInstance().AddTime("FilterImageFromModel", t1.msec());

#if 0
                char buffer[512];
                sprintf(buffer, "%s_o%d", modelName, i);
                WriteTempImages(outputPlanes, buffer);
#endif

                if (i != ((int)msz - 1))
                {
                    inputPlanes.swap(outputPlanes);
                    outputPlanes.clear();
                }
            }
            return ClipImage(outputPlanes.back(), msz, msz + width, msz, msz + height);
        }

        static std::shared_ptr<image<float> > FilterImageFromModelsTiled(
            const std::shared_ptr<image<float> >& img,
            const char* swrap, const char* twrap,
            const char* modelName)
        {
#ifndef __APPLE__
            return FilterImageFromModels(img, swrap, twrap, modelName);
#else
            int width = img->get_width();
            int height = img->get_height();

            int TILE_WIDTH = std::max<int>(width / 2, height / 2);
            Waifu2xModelsManager& mgr = Waifu2xModelsManager::GetInstance();
            const std::vector<std::shared_ptr<waifu2x::Model> >& models = mgr.GetModels(modelName);
            int MODEL_SIZE = (int)(models.size());
            int TILE_MARGIN = 1 * MODEL_SIZE;
            int BLOCK = TILE_WIDTH - 2 * TILE_MARGIN;

            int bw = (int)ceil(float(width) / BLOCK);
            int bh = (int)ceil(float(height) / BLOCK);

            std::shared_ptr<image<float> > wrapedImage = GetWrapedImage(img, swrap, twrap);

            std::shared_ptr<image<float> > outImage(new memory_image<float>(width, height));

            dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t g = dispatch_group_create();

            for (int by = 0; by < bh; by++)
            {
                for (int bx = 0; bx < bw; bx++)
                {
                    dispatch_group_async(g, q,
                                         ^{
                                           int y0 = by * BLOCK;
                                           int y1 = std::min<int>(y0 + BLOCK, height);
                                           int hh = y1 - y0;

                                           int x0 = bx * BLOCK;
                                           int x1 = std::min<int>(x0 + BLOCK, width);
                                           int ww = x1 - x0;
                                           std::shared_ptr<image<float> > tileInImg = ClipImage(
                                               wrapedImage,
                                               x0 - TILE_MARGIN, x1 + TILE_MARGIN,
                                               y0 - TILE_MARGIN, y1 + TILE_MARGIN);

                                           std::shared_ptr<image<float> > tileOutImg = FilterImageFromModels(
                                               tileInImg,
                                               swrap, twrap,
                                               modelName);

                                           for (int yy = 0; yy < hh; yy++)
                                           {
                                               int yyy = y0 + yy;
                                               if (height <= yyy) continue;
                                               for (int xx = 0; xx < ww; xx++)
                                               {
                                                   int xxx = x0 + xx;
                                                   if (width <= xxx) continue;
                                                   float c = tileOutImg->get(xx + TILE_MARGIN, yy + TILE_MARGIN);
                                                   outImage->set(xxx, yyy, c);
                                               }
                                           }
                                         });
                }
            }

            dispatch_group_wait(g, DISPATCH_TIME_FOREVER);
            dispatch_release(g);

            return outImage;
#endif
        }

        static std::shared_ptr<image<color4f> > FilterImage(
            const std::shared_ptr<image<color4f> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth,
            const char* modelName)
        {
            std::shared_ptr<image<color4f> > YUVImg = GetYUVImage(img);
            std::shared_ptr<image<color4f> > RGBImg = GetRGBImage(
                CompositeImage(
                    FilterImageFromModelsTiled(SplitImage(YUVImg, 0), swrap, twrap, modelName),
                    //FilterImageFromModels(SplitImage(YUVImg, 0), swrap, twrap, modelName),
                    SplitImage(YUVImg, 1),
                    SplitImage(YUVImg, 2),
                    SplitImage(YUVImg, 3)));
            return RGBImg;
        }

        static std::shared_ptr<image<color4f> > FilterImageFromModelsRGB(
            const std::shared_ptr<image<color4f> >& img,
            const char* swrap, const char* twrap,
            const char* modelName)
        {
            Waifu2xModelsManager& mgr = Waifu2xModelsManager::GetInstance();
            const std::vector<std::shared_ptr<waifu2x::Model> >& models = mgr.GetModels(modelName);
            int msz = (int)models.size();

            int width = img->get_width();
            int height = img->get_height();

            std::shared_ptr<image<color4f> > extImg = ExtendImage(img, swrap, twrap, msz);
            std::shared_ptr<image<float> > inRImg = SplitImage(extImg, 0);
            std::shared_ptr<image<float> > inGImg = SplitImage(extImg, 1);
            std::shared_ptr<image<float> > inBImg = SplitImage(extImg, 2);
            std::shared_ptr<image<float> > inAImg = SplitImage(extImg, 3);

            std::vector<std::shared_ptr<image<float> > > inputPlanes;
            std::vector<std::shared_ptr<image<float> > > outputPlanes;
            inputPlanes.push_back(inRImg);
            inputPlanes.push_back(inGImg);
            inputPlanes.push_back(inBImg);

            for (int i = 0; i < (int)models.size(); i++)
            {
                FilterImageFromModel(outputPlanes, inputPlanes, models[i]);
                if (i != ((int)models.size() - 1))
                {
                    inputPlanes.swap(outputPlanes);
                    outputPlanes.clear();
                }
            }

            std::shared_ptr<image<float> > outRImg = outputPlanes[0];
            std::shared_ptr<image<float> > outGImg = outputPlanes[1];
            std::shared_ptr<image<float> > outBImg = outputPlanes[2];

            std::shared_ptr<image<color4f> > outRGBAImg = CompositeImage(outRImg, outGImg, outBImg, inAImg);

            return ClipImage(outRGBAImg, msz, msz + width, msz, msz + height);
        }

        static std::shared_ptr<image<color4f> > FilterImageFromModelsRGBTiled(
            const std::shared_ptr<image<color4f> >& img,
            const char* swrap, const char* twrap,
            const char* modelName)
        {
#ifndef __APPLE__
            return FilterImageFromModelsRGB(img, swrap, twrap, modelName);
#else
            int width = img->get_width();
            int height = img->get_height();

            int TILE_WIDTH = std::max<int>(width / 2, height / 2);
            Waifu2xModelsManager& mgr = Waifu2xModelsManager::GetInstance();
            const std::vector<std::shared_ptr<waifu2x::Model> >& models = mgr.GetModels();
            int MODEL_SIZE = (int)(models.size());
            int TILE_MARGIN = 1 * MODEL_SIZE;
            int BLOCK = TILE_WIDTH - 2 * TILE_MARGIN;

            int bw = (int)ceil(float(width) / BLOCK);
            int bh = (int)ceil(float(height) / BLOCK);

            std::shared_ptr<image<color4f> > wrapedImage = GetWrapedImage(img, swrap, twrap);

            std::shared_ptr<image<color4f> > outImage(new memory_image<color4f>(width, height));

            dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
            dispatch_group_t g = dispatch_group_create();

            for (int by = 0; by < bh; by++)
            {
                for (int bx = 0; bx < bw; bx++)
                {
                    dispatch_group_async(g, q,
                                         ^{
                                           int y0 = by * BLOCK;
                                           int y1 = std::min<int>(y0 + BLOCK, height);
                                           int hh = y1 - y0;

                                           int x0 = bx * BLOCK;
                                           int x1 = std::min<int>(x0 + BLOCK, width);
                                           int ww = x1 - x0;
                                           std::shared_ptr<image<color4f> > tileInImg = ClipImage(
                                               wrapedImage,
                                               x0 - TILE_MARGIN, x1 + TILE_MARGIN,
                                               y0 - TILE_MARGIN, y1 + TILE_MARGIN);

                                           std::shared_ptr<image<color4f> > tileOutImg = FilterImageFromModelsRGB(
                                               tileInImg,
                                               swrap, twrap,
                                               modelName);

                                           for (int yy = 0; yy < hh; yy++)
                                           {
                                               int yyy = y0 + yy;
                                               if (height <= yyy) continue;
                                               for (int xx = 0; xx < ww; xx++)
                                               {
                                                   int xxx = x0 + xx;
                                                   if (width <= xxx) continue;
                                                   outImage->set(xxx, yyy, tileOutImg->get(xx + TILE_MARGIN, yy + TILE_MARGIN));
                                               }
                                           }
                                         });
                }
            }

            dispatch_group_wait(g, DISPATCH_TIME_FOREVER);
            dispatch_release(g);

            return outImage;
#endif
        }

        static std::shared_ptr<image<color4f> > FilterImageRGB(
            const std::shared_ptr<image<color4f> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth,
            const char* modelName)
        {
            return FilterImageFromModelsRGBTiled(img, swrap, twrap, modelName);
        }

        std::shared_ptr<image<color4> > ri_waifu2x_filter_(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth,
            const char* modelName)
        {
            if (ri_tex_get_gl_context())
            {
                Waifu2xModelsManager& mgr = Waifu2xModelsManager::GetInstance();
                const std::vector<std::shared_ptr<waifu2x::Model> >& models = mgr.GetModels(modelName);
                return FilterImageGLTiled(img, swrap, twrap, swidth, twidth, modelName);
            }
            else
            {
                Waifu2xModelsManager& mgr = Waifu2xModelsManager::GetInstance();
                const std::vector<std::shared_ptr<waifu2x::Model> >& models = mgr.GetModels(modelName);
                if (models[0]->GetInputPlanes() == 1)
                {
                    return ConvertImage(FilterImage(ConvertImage(img), swrap, twrap, swidth, twidth, modelName));
                }
                else
                {
                    return ConvertImage(FilterImageRGB(ConvertImage(img), swrap, twrap, swidth, twidth, modelName));
                }
            }
        }

        static std::shared_ptr<image<color4> > Convert2x(const std::shared_ptr<image<color4> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<image<color4> > outImg(new memory_image<color4>(width * 2, height * 2));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    color4 c = img->get(x, y);
                    outImg->set(2 * x + 0, 2 * y + 0, c);
                    outImg->set(2 * x + 1, 2 * y + 0, c);
                    outImg->set(2 * x + 0, 2 * y + 1, c);
                    outImg->set(2 * x + 1, 2 * y + 1, c);
                }
            }
            return outImg;
        }

        std::shared_ptr<image<color4> > ri_waifu2x_filter(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth,
            const char* modelName)
        {
            if (strstr(modelName, "2.0x") != NULL)
            {
                return ri_waifu2x_filter_(Convert2x(img), swrap, twrap, swidth, twidth, modelName);
            }
            else
            {
                return ri_waifu2x_filter_(img, swrap, twrap, swidth, twidth, modelName);
            }
        }
    }
}
