#include "ri_nlmeans_filter.h"
#include "ri_tex_gl_context.h"

#include <stdio.h>
#include <memory>
#include <vector>
#include <memory_image.hpp>

#include <gl_tex/gl_nlmeans_filter_pass.h>

#include <color.h>
#include <filter/corresponder.h>
#include <graphics/image/image_interpolator.hpp>
#include <graphics/image/corresponded_image.hpp>
#include <graphics/texture/image_texture/image_texture.hpp>
#include <graphics/texture/image_texture/unit_image_texture.hpp>

using namespace gl::tex;

namespace kaze
{
    namespace ri
    {
        enum
        {
            WRAP_REPEAT = 0,
            WRAP_CLAMP,
            WRAP_MIRROR,
            WRAP_BORDER,
        };

        struct TokenValueValue
        {
            const char* token;
            int value1;
            int value2;
        };

        static TokenValueValue WRAP_TYPES[] =
        {
                {"repeat", WRAP_REPEAT, GL_REPEAT},
                {"clamp", WRAP_CLAMP, GL_CLAMP_TO_EDGE},
                {"mirror", WRAP_MIRROR, GL_MIRRORED_REPEAT},
                {"border", WRAP_BORDER, GL_CLAMP_TO_BORDER},
                {NULL, 0},
        };

        static int ToGLWrap(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (strcmp(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].value2;
                i++;
            }
            return GL_CLAMP_TO_EDGE;
        }

        static int ToWrapType(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (strcmp(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].value1;
                i++;
            }
            return WRAP_REPEAT;
        }

        static std::shared_ptr<image<color4> > FilterImageGL(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<gl::gl_texture_2D> texInput(new gl::gl_texture_2D());
            std::shared_ptr<gl::gl_texture_2D> texOutput(new gl::gl_texture_2D());
            {
                std::vector<float> fBuffer(4 * width * height);
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        size_t index = y * width + x;
                        color4 c = img->get(x, y);
                        fBuffer[4 * index + 0] = (float)c[0];
                        fBuffer[4 * index + 1] = (float)c[1];
                        fBuffer[4 * index + 2] = (float)c[2];
                        fBuffer[4 * index + 3] = (float)c[3];
                    }
                }

                texInput->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, &fBuffer[0]);
            }
            {
                texInput->bind();
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);  //not biliear
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);  //
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, ToGLWrap(swrap)); //TODO
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, ToGLWrap(twrap)); //TODO
                texInput->unbind();
            }
            texOutput->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, 0);

            //
            std::vector<std::shared_ptr<gl_pass> > passList;
            passList.push_back(std::shared_ptr<gl_pass>(new gl_nlmeans_filter_pass(width, height, texInput, texOutput, (int)swidth, (int)twidth)));
            //
            for (size_t i = 0; i < passList.size(); i++)
            {
                passList[i]->run();
            }
            //

            std::shared_ptr<image<color4> > outImg;
            std::vector<float> fBuffer(4 * width * height, 0.0f);
            texOutput->bind();
            ::glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &fBuffer[0]);
            outImg.reset(new memory_image<color4>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    size_t index = y * width + x;
                    float r = fBuffer[4 * index + 0];
                    float g = fBuffer[4 * index + 1];
                    float b = fBuffer[4 * index + 2];
                    float a = fBuffer[4 * index + 3];
                    outImg->set(x, y, color4(r, g, b, a));
                }
            }
            texOutput->unbind();

            return outImg;
        }

        static std::shared_ptr<image<color4> > ClipImage(
            std::shared_ptr<image<color4> >& img,
            int x0, int x1, int y0, int y1)
        {
            int w = x1 - x0;
            int h = y1 - y0;
            std::shared_ptr<image<color4> > outImage(new memory_image<color4>(w, h));
            for (int y = y0; y < y1; y++)
            {
                for (int x = x0; x < x1; x++)
                {
                    color4 c = img->get(x, y);
                    outImage->set(x - x0, y - y0, c);
                }
            }
            return outImage;
        }

        static std::shared_ptr<image<color4> > GetWrapedImage(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap)
        {
            int WW = ToWrapType(swrap);
            int HW = ToWrapType(twrap);

            std::shared_ptr<corresponder> corx(new powerof2_clamp_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            switch (WW)
            {
            case WRAP_REPEAT:
                corx.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                corx.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                corx.reset(new powerof2_mirror_corresponder());
                break;
            }

            switch (HW)
            {
            case WRAP_REPEAT:
                cory.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                cory.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                cory.reset(new powerof2_mirror_corresponder());
                break;
            }

            std::shared_ptr<image<color4> > outImage(
                new corresponded_image<color4>(
                    img,
                    corx,
                    cory));
            return outImage;
        }

        static std::shared_ptr<image<color4> > FilterImageGLTiled(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth)
        {
            static const int TILE_WIDTH = 256;
            static const int TILE_MARGIN = 8;
            static const int BLOCK = TILE_WIDTH - 2 * TILE_MARGIN;

            int width = img->get_width();
            int height = img->get_height();

            int bw = (int)ceil(float(width) / BLOCK);
            int bh = (int)ceil(float(height) / BLOCK);

            std::shared_ptr<image<color4> > wrapedImage = GetWrapedImage(img, swrap, twrap);

            std::shared_ptr<image<color4> > outImage(new memory_image<color4>(width, height));

            for (int by = 0; by < bh; by++)
            {
                int y0 = by * BLOCK;
                int y1 = std::min<int>(y0 + BLOCK, height);
                int hh = y1 - y0;
                for (int bx = 0; bx < bw; bx++)
                {
                    int x0 = bx * BLOCK;
                    int x1 = std::min<int>(x0 + BLOCK, width);
                    int ww = x1 - x0;
                    std::shared_ptr<image<color4> > tileInImg = ClipImage(
                        wrapedImage,
                        x0 - TILE_MARGIN, x1 + TILE_MARGIN,
                        y0 - TILE_MARGIN, y1 + TILE_MARGIN);

                    std::shared_ptr<image<color4> > tileOutImg = FilterImageGL(
                        tileInImg,
                        swrap, twrap,
                        swidth, twidth);

                    for (int yy = 0; yy < hh; yy++)
                    {
                        int yyy = y0 + yy;
                        if (height <= yyy) continue;
                        for (int xx = 0; xx < ww; xx++)
                        {
                            int xxx = x0 + xx;
                            if (width <= xxx) continue;
                            color4 c = tileOutImg->get(xx + TILE_MARGIN, yy + TILE_MARGIN);
                            outImage->set(xxx, yyy, c);
                        }
                    }
                }
            }
            return outImage;
        }

        static std::shared_ptr<image<color4> > FilterImage(
            const std::shared_ptr<image<color4> >& inImg,
            const char* swrap, const char* twrap,
            float swidth, float twidth)
        {
            int WW = ToWrapType(swrap);
            int HW = ToWrapType(twrap);

            std::shared_ptr<corresponder> corx(new powerof2_clamp_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            switch (WW)
            {
            case WRAP_REPEAT:
                corx.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                corx.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                corx.reset(new powerof2_mirror_corresponder());
                break;
            }

            switch (HW)
            {
            case WRAP_REPEAT:
                cory.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                cory.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                cory.reset(new powerof2_mirror_corresponder());
                break;
            }

            std::shared_ptr<unit_image_texture<color4> > tex(
                new unit_image_texture<color4>(
                    inImg,
                    corx,
                    cory,
                    new bilinear_image_interpolator<color4>()));

            int sf = (int)ceil(swidth) / 2;
            int tf = (int)ceil(twidth) / 2;
            int width = inImg->get_width();
            int height = inImg->get_height();
            std::shared_ptr<image<color4> > outImg(new memory_image<color4>(width, height));

            for (int y = 0; y < height; y++)
            {
                real yy = y + 0.5;
                for (int x = 0; x < width; x++)
                {
                    real xx = x + 0.5;
                    color4 col = color4(0, 0, 0, 0);
                    for (int s = -sf; s <= +sf; s++)
                    {
                        for (int t = -tf; t <= +tf; t++)
                        {
                            real u = (xx + s) / width;
                            real v = (yy + t) / height;
                            col += tex->get(u, v);
                        }
                    }

                    col /= ((2 * sf + 1) * (2 * tf + 1));
                    outImg->set(x, y, col);
                }
            }

            return outImg;
        }

        std::shared_ptr<image<color4> > ri_nlmeans_filter(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth)
        {
            if (ri_tex_get_gl_context())
            {
                return FilterImageGLTiled(img, swrap, twrap, swidth, twidth);
            }
            else
            {
                return FilterImage(img, swrap, twrap, swidth, twidth);
            }
        }
    }
}
