#include "ri_make_cube_face_environment.h"
#include "ri_make_texture_helper.h"

#include "ri_make_texture.h"

#include <string>
#include <vector>
#include <algorithm>

#include <stdlib.h>

#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include "image/png_io.h"
#include "image/kxf_io.h"

#include "memory_image.hpp"

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif

#include "filter/corresponder.h"
#include "graphics/image/image_interpolator.hpp"
#include "graphics/texture/image_texture/image_texture.hpp"
#include "graphics/texture/image_texture/unit_image_texture.hpp"

#include "logger.h"

namespace kaze
{
    namespace ri
    {
        enum
        {
            WRAP_REPEAT = 0,
            WRAP_CLAMP,
            WRAP_MIRROR,
            WRAP_BORDER,
        };

        enum
        {
            FILTER_BOX = 0,
            FILTER_TRIANGLE,
            FILTER_MITCHELL,
            FILTER_GAUSS,
            FILTER_SINC,
        };

        enum
        {
            RESIZE_NONE = 0,
            RESIZE_UP,
            RESIZE_DOWN,
            RESIZE_ROUND,
            RESIZE_UP_M,
            RESIZE_DOWN_M,
            RESIZE_ROUND_M,
        };

        struct TokenValue
        {
            const char* token;
            int value;
        };

        struct TokenTokenValue
        {
            const char* token;
            const char* token2;
            int value;
        };

        static TokenTokenValue WRAP_TYPES[] =
            {
                {"repeat", "repeat", WRAP_REPEAT},
                {"periodic", "repeat", WRAP_REPEAT},
                {"wrap", "repeat", WRAP_REPEAT}, //GL_REPEAT
                {"GL_REPEAT", "repeat", WRAP_REPEAT},

                {"clamp", "clamp", WRAP_CLAMP},
                {"clamp_to_edge", "clamp", WRAP_CLAMP}, //GL_CLAMP_TO_EDGE
                {"GL_CLAMP", "clamp", WRAP_CLAMP},      //?
                {"GL_CLAMP_TO_EDGE", "clamp", WRAP_CLAMP},

                {"mirror", "mirror", WRAP_MIRROR}, //GL_MIRRORED_REPEAT
                {"GL_MIRRORED_REPEAT", "mirror", WRAP_MIRROR},

                {"border", "border", WRAP_BORDER},
                {"clamp_to_border", "border", WRAP_BORDER},
                {"black", "border", WRAP_BORDER},
                {"white", "border", WRAP_BORDER}, //GL_CLAMP_TO_BORDER
                {"GL_CLAMP_TO_BORDER", "border", WRAP_BORDER},

                {NULL, "", 0},
        };

        static TokenTokenValue FILTER_TYPES[] =
            {
                {"box", "box", FILTER_BOX},
                {"point", "box", FILTER_BOX},
                {"mean", "box", FILTER_BOX},
                {"triangle", "triangle", FILTER_TRIANGLE},
                {"tent", "triangle", FILTER_TRIANGLE},
                {"mitchel", "mitchell", FILTER_MITCHELL},
                {"mitchell", "mitchell", FILTER_MITCHELL},
                {"gauss", "gauss", FILTER_GAUSS},
                {"gaussian", "gauss", FILTER_GAUSS},
                {"gaussian-soft", "gauss", FILTER_GAUSS},
                {"sinc", "sinc", FILTER_SINC},
                {NULL, "", 0},
        };

        //gaussian-soft, catmull-rom, mitchell, cubic, lanczos, blackman-harris, and bessel

        static TokenValue RESIZE_TYPES[] =
            {
                {"none", RESIZE_NONE},
                {"up", RESIZE_UP},
                {"down", RESIZE_DOWN},
                {"round", RESIZE_ROUND},
                {"up-", RESIZE_UP_M},
                {"down-", RESIZE_DOWN_M},
                {"round-", RESIZE_ROUND_M},
                {NULL, 0},
        };

        static const char* ToInternalWrap(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (strcmp(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].token2;
                i++;
            }
            return "repeat";
        }

        static const char* ToInternalFilter(const char* szFilter)
        {
            int i = 0;
            while (FILTER_TYPES[i].token)
            {
                if (strcmp(szFilter, FILTER_TYPES[i].token) == 0) return FILTER_TYPES[i].token2;
                i++;
            }
            return "mitchell";
        }

        static int ToWrapType(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (strcmp(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].value;
                i++;
            }
            return WRAP_REPEAT;
        }

        static int ToResizeType(const char* szFilter)
        {
            int i = 0;
            while (RESIZE_TYPES[i].token)
            {
                if (strcmp(szFilter, RESIZE_TYPES[i].token) == 0) return RESIZE_TYPES[i].value;
                i++;
            }
            return RESIZE_NONE;
        }

        static std::shared_ptr<image<color4> > GetImageFromFile(const char* szPicFile)
        {
            return get_image_from_file(szPicFile);
        }

        static bool CheckKTXHeader(const char* szTexFile, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth, int width, int height)
        {
            kxf_header header;
            if (read_kxf_header(szTexFile, &header) != 0) return false;
            if (std::string(header.swrap) != swrap) return false;
            if (std::string(header.twrap) != twrap) return false;
            if (std::string(header.filter) != filter) return false;
            if (header.swidth != swidth) return false;
            if (header.twidth != twidth) return false;
            if (header.width != width) return false;
            if (header.height != height) return false;

            return true;
        }

        static int GetReizeWidth(int width, int R)
        {
            switch (R)
            {
            case RESIZE_NONE:
                return width;
            case RESIZE_UP:
            case RESIZE_UP_M:
            {
                int r = 1;
                while (r < width)
                {
                    r = r << 1;
                }
                return r;
            }
            break;
            case RESIZE_DOWN:
            case RESIZE_DOWN_M:
            {
                int r = 1;
                while ((r << 1) < width)
                {
                    r = r << 1;
                }
                return r;
            }
            break;
            case RESIZE_ROUND:
            case RESIZE_ROUND_M:
            {
                int u = GetReizeWidth(width, RESIZE_UP);
                int d = GetReizeWidth(width, RESIZE_DOWN);
                return (u - width) < (width - d) ? u : d;
            }
            break;
            }
            return GetReizeWidth(width, RESIZE_UP);
        }

        static bool IsLongestResize(int R)
        {
            return (R == RESIZE_UP || R == RESIZE_DOWN || R == RESIZE_ROUND);
        }

        static std::shared_ptr<image<color4> > ResizeImage(const std::shared_ptr<image<color4> >& img, int WR, int HR, int WW, int HW)
        {
            int width = img->get_width();
            int height = img->get_height();

            int r_width = GetReizeWidth(width, WR);
            int r_height = GetReizeWidth(height, HR);
            if (IsLongestResize(WR) || IsLongestResize(HR))
            {
                r_width = r_height = std::max(r_width, r_height);
            }

//--------------------------------------------------------
#ifdef _DEBUG
            printf("%d, %d\n", WR, HR);
            printf("width = %d, height = %d\n", width, height);
            printf("width = %d, height = %d\n", r_width, r_height);
#endif
            //--------------------------------------------------------
            if (width == r_width && height == r_height) return img;

            std::shared_ptr<image<color4> > out(new memory_image<color4>(r_width, r_height));

            std::shared_ptr<corresponder> corx(new powerof2_clamp_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            switch (WW)
            {
            case WRAP_REPEAT:
                corx.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                corx.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                corx.reset(new powerof2_mirror_corresponder());
                break;
            }

            switch (HW)
            {
            case WRAP_REPEAT:
                cory.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                cory.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                cory.reset(new powerof2_mirror_corresponder());
                break;
            }

            std::shared_ptr<unit_image_texture<color4> > tex(
                new unit_image_texture<color4>(
                    img,
                    corx,
                    cory,
                    new bilinear_image_interpolator<color4>()));

            for (int y = 0; y < r_height; y++)
            {
                real yy = (y + 0.5) / r_height;
                for (int x = 0; x < r_width; x++)
                {
                    real xx = (x + 0.5) / r_width;
                    out->set(x, y, tex->get(xx, yy));
                }
            }

            return out;
        }

        static std::shared_ptr<image<color4> > ResizeImage(
            const std::shared_ptr<image<color4> >& img,
            const char* szWResize = "up", const char* szHResize = "up",
            const char* szWWrap = "repeat", const char* szHWrap = "repeat")
        {
            int WR = ToResizeType(szWResize);
            int HR = ToResizeType(szHResize);
            int WW = ToWrapType(szWWrap);
            int HW = ToWrapType(szHWrap);
            return ResizeImage(img, WR, HR, WW, HW);
        }

        static int ConvertTexture(
            const char* px, const char* nx, const char* py, const char* ny, const char* pz, const char* nz,
            const char* tex,
            const char* filter, float swidth, float twidth, int n, const char* tokens[], const char* params[], bool bForce = false)
        {
            const char* swrap = "clamp";
            const char* twrap = "clamp";

            const char* files[] = {px, nx, py, ny, pz, nz};

            std::vector<std::shared_ptr<image<color4> > > vecImg;

            for (int i = 0; i < 6; i++)
            {
                std::shared_ptr<image<color4> > img = GetImageFromFile(files[i]);
                if (!img.get()) return -1;
                vecImg.push_back(img);
            }

            bool bMipmap = true;
            bool bDebug = false;
            std::string strSresize = "up-";
            std::string strTresize = "up-";
            for (int i = 0; i < n; i++)
            {
                if (strcmp(tokens[i], "sresize") == 0)
                {
                    strSresize = params[i];
                }
                else if (strcmp(tokens[i], "tresize") == 0)
                {
                    strTresize = params[i];
                }
                else if (strcmp(tokens[i], "resize") == 0)
                {
                    strSresize = strTresize = params[i];
                }
                else if (strcmp(tokens[i], "force") == 0)
                {
                    bForce = (atoi(params[i])) ? true : false;
                }
                else if (strcmp(tokens[i], "debug") == 0)
                {
                    bDebug = (atoi(params[i])) ? true : false;
                }
                else if (strcmp(tokens[i], "mipmap") == 0)
                {
                    bMipmap = (atoi(params[i])) ? true : false;
                }
            }

#ifdef _DEBUG
            bDebug = true;
#endif
            //bForce = true;
            //bDebug = true;

            const char* sresize = strSresize.c_str();
            const char* tresize = strTresize.c_str();

            int WW = ToWrapType(swrap);
            int HW = ToWrapType(twrap);

            if (bForce)
            {
                for (int i = 0; i < 6; i++)
                {
                    vecImg[i] = ResizeImage(vecImg[i], sresize, tresize, swrap, twrap);
                }

                if (bDebug)
                {
                    save_debug_images(tex, vecImg);
                }
                return save_to_kxf_cube(tex, vecImg, swrap, twrap, filter, swidth, twidth);
            }
            else
            {
                int WR = ToResizeType(sresize);
                int HR = ToResizeType(tresize);
                int width = GetReizeWidth(vecImg[0]->get_width(), WR);
                int height = GetReizeWidth(vecImg[0]->get_height(), HR);
                if (IsLongestResize(WR) || IsLongestResize(HR))
                {
                    width = height = std::max(width, height);
                }
                if (CheckKTXHeader(tex, swrap, twrap, filter, swidth, twidth, width, height))
                {
                    return 0;
                }
                else
                {
                    for (int i = 0; i < 6; i++)
                    {
                        vecImg[i] = ResizeImage(vecImg[i], sresize, tresize, swrap, twrap);
                    }

                    if (bDebug)
                    {
                        save_debug_images(tex, vecImg);
                    }
                    return save_to_kxf_cube(tex, vecImg, swrap, twrap, filter, swidth, twidth);
                }
            }
        }

        static bool IsExistFile(const char* szFilePath)
        {
#ifdef _WIN32
            struct _stat s;
            int nRet = _stat(szFilePath, &s);
            return (nRet == 0);
#else
            struct stat s;
            int nRet = stat(szFilePath, &s);
            return (nRet == 0);
#endif
        }

        static bool IsExistFile(const char* szFiles[], int n)
        {
            for (int i = 0; i < n; i++)
            {
                if (!IsExistFile(szFiles[i])) return false;
            }
            return true;
        }

        static size_t GetLastModifiedTime(const char* szFilePath)
        {
#ifdef _WIN32
            struct _stat s;
            int nRet = _stat(szFilePath, &s);
            return s.st_mtime;
#else
            struct stat s;
            int nRet = stat(szFilePath, &s);
            return s.st_mtime;
#endif
        }

        static size_t GetLastModifiedTime(const char* szFiles[], int n)
        {
            size_t t = GetLastModifiedTime(szFiles[0]);
            for (int i = 1; i < n; i++)
            {
                t = std::max(t, GetLastModifiedTime(szFiles[i]));
            }
            return t;
        }

        static int MakeCube(
            const char* px, const char* nx, const char* py, const char* ny, const char* pz, const char* nz,
            const char* tex,
            const char* filter, float swidth, float twidth, int n, const char* tokens[], const char* params[])
        {

            const char* files[] = {px, nx, py, ny, pz, nz};
            int nRet = 0;
            if (IsExistFile(files, 6)) nRet |= 1;
            if (IsExistFile(tex)) nRet |= 2;

            switch (nRet)
            {
            case 0:
                return -1;
            case 1:
                return ConvertTexture(px, nx, py, ny, pz, nz, tex, filter, swidth, twidth, n, tokens, params, true);
            case 2:
                return -1;
            case 3:
            {
                size_t mpic = GetLastModifiedTime(files, 6);
                size_t mtex = GetLastModifiedTime(tex);
                bool bForce = mpic >= mtex;
                return ConvertTexture(px, nx, py, ny, pz, nz, tex, filter, swidth, twidth, n, tokens, params, bForce);
            }
            }
            return -1;
        }

        int ri_make_cube_face_environment(
            const char* px, const char* nx, const char* py, const char* ny, const char* pz, const char* nz,
            const char* tex, float fov, const char* filterfunc, float swidth, float twidth)
        {
            return ri_make_cube_face_environment(px, nx, py, ny, pz, nz, tex, fov, filterfunc, swidth, twidth, 0, 0, 0);
        }

        int ri_make_cube_face_environment(
            const char* px, const char* nx, const char* py, const char* ny, const char* pz, const char* nz,
            const char* tex, float fov, const char* filter, float swidth, float twidth,
            int n, const char* tokens[], const char* params[])
        {
            std::string strFilter = ToInternalFilter(filter);

            return MakeCube(px, nx, py, ny, pz, nz, tex, strFilter.c_str(), swidth, twidth, n, tokens, params);
        }
    }
}
