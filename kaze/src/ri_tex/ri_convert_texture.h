#ifndef KAZE_RI_CONVERT_TEXTURE_H
#define KAZE_RI_CONVERT_TEXTURE_H

namespace kaze
{
    namespace ri
    {
        int ri_convert_texture(const char* pic, int n, const char* tokens[], const char* params[]);

        int ri_latl2cube(
            const char* pic,
            const char* px, const char* nx, const char* py, const char* ny, const char* pz, const char* nz,
            int n, const char* tokens[], const char* params[]);
    }
}

#endif
