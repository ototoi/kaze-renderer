#ifndef KAZE_RI_FILTER_IMAGE_GAUSS_H
#define KAZE_RI_FILTER_IMAGE_GAUSS_H

#include "count_ptr.hpp"
#include "image.hpp"

namespace kaze
{
    namespace ri
    {

        std::shared_ptr<image<color4> > ri_gauss_filter(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth);
    }
}

#endif