#ifndef KAZE_RI_MAKE_TEXTURE_H
#define KAZE_RI_MAKE_TEXTURE_H

namespace kaze
{
    namespace ri
    {
        int ri_make_texture(const char* pic, const char* tex, const char* swrap, const char* twrap, const char* filterfunc, float swidth, float twidth);
        int ri_make_texture(const char* pic, const char* tex, const char* swrap, const char* twrap, const char* filterfunc, float swidth, float twidth, int n, const char* tokens[], const char* params[]);
    }
}

#endif