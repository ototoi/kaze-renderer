#include "ri_make_texture_helper.h"

#include "image/bmp_io.h"
#include "image/png_io.h"
#include "image/tiff_io.h"
#include "image/jpeg_io.h"
#include "image/tga_io.h"
#include "image/exr_io.h"
#include "image/hdr_io.h"
#include "image/dds_io.h"

#include "image/kxf_io.h"

#include "memory_image.hpp"

#include "filter/corresponder.h"
#include "graphics/image/image_interpolator.hpp"
#include "graphics/texture/image_texture/image_texture.hpp"
#include "graphics/texture/image_texture/image_texture.hpp"
#include "graphics/texture/image_texture/unit_image_texture.hpp"

#ifdef _WIN32
#define strcasecmp(A, B) stricmp(A, B)
#endif

namespace kaze
{
    namespace ri
    {
        namespace
        {
            enum
            {
                WRAP_REPEAT = 0,
                WRAP_CLAMP,
                WRAP_MIRROR,
                WRAP_BORDER,
            };

            enum
            {
                FILTER_BOX = 0,
                FILTER_TRIANGLE,
                FILTER_MITCHELL,
                FILTER_GAUSS,
                FILTER_SINC,
            };

            enum
            {
                RESIZE_NONE = 0,
                RESIZE_UP,
                RESIZE_DOWN,
                RESIZE_ROUND,
                RESIZE_UP_M,
                RESIZE_DOWN_M,
                RESIZE_ROUND_M,
            };

            struct TokenValue
            {
                const char* token;
                int value;
            };

            struct TokenTokenValue
            {
                const char* token;
                const char* token2;
                int value;
            };

            static TokenTokenValue WRAP_TYPES[] =
                {
                    {"repeat", "repeat", WRAP_REPEAT},
                    {"periodic", "repeat", WRAP_REPEAT},
                    {"wrap", "repeat", WRAP_REPEAT}, //GL_REPEAT
                    {"GL_REPEAT", "repeat", WRAP_REPEAT},

                    {"clamp", "clamp", WRAP_CLAMP},
                    {"clamp_to_edge", "clamp", WRAP_CLAMP}, //GL_CLAMP_TO_EDGE
                    {"GL_CLAMP", "clamp", WRAP_CLAMP},      //?
                    {"GL_CLAMP_TO_EDGE", "clamp", WRAP_CLAMP},

                    {"mirror", "mirror", WRAP_MIRROR}, //GL_MIRRORED_REPEAT
                    {"GL_MIRRORED_REPEAT", "mirror", WRAP_MIRROR},

                    {"border", "border", WRAP_BORDER},
                    {"clamp_to_border", "border", WRAP_BORDER},
                    {"black", "border", WRAP_BORDER},
                    {"white", "border", WRAP_BORDER}, //GL_CLAMP_TO_BORDER
                    {"GL_CLAMP_TO_BORDER", "border", WRAP_BORDER},

                    {NULL, "", 0},
            };

            static TokenTokenValue FILTER_TYPES[] =
                {
                    {"box", "box", FILTER_BOX},
                    {"point", "box", FILTER_BOX},
                    {"mean", "box", FILTER_BOX},
                    {"triangle", "triangle", FILTER_TRIANGLE},
                    {"tent", "triangle", FILTER_TRIANGLE},
                    {"mitchel", "mitchell", FILTER_MITCHELL},
                    {"mitchell", "mitchell", FILTER_MITCHELL},
                    {"gauss", "gauss", FILTER_GAUSS},
                    {"gaussian", "gauss", FILTER_GAUSS},
                    {"gaussian-soft", "gauss", FILTER_GAUSS},
                    {"sinc", "sinc", FILTER_SINC},
                    {NULL, "", 0},
            };

            //gaussian-soft, catmull-rom, mitchell, cubic, lanczos, blackman-harris, and bessel

            static TokenValue RESIZE_TYPES[] =
                {
                    {"none", RESIZE_NONE},
                    {"up", RESIZE_UP},
                    {"down", RESIZE_DOWN},
                    {"round", RESIZE_ROUND},
                    {"up-", RESIZE_UP_M},
                    {"down-", RESIZE_DOWN_M},
                    {"round-", RESIZE_ROUND_M},
                    {NULL, 0},
            };
        }
        static const char* ToInternalWrap(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (strcmp(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].token2;
                i++;
            }
            return "repeat";
        }

        static const char* ToInternalFilter(const char* szFilter)
        {
            int i = 0;
            while (FILTER_TYPES[i].token)
            {
                if (strcmp(szFilter, FILTER_TYPES[i].token) == 0) return FILTER_TYPES[i].token2;
                i++;
            }
            return "mitchell";
        }

        static int ToWrapType(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (strcmp(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].value;
                i++;
            }
            return WRAP_REPEAT;
        }

        static int ToResizeType(const char* szFilter)
        {
            int i = 0;
            while (RESIZE_TYPES[i].token)
            {
                if (strcmp(szFilter, RESIZE_TYPES[i].token) == 0) return RESIZE_TYPES[i].value;
                i++;
            }
            return RESIZE_NONE;
        }

        static std::string GetExt(const std::string& str)
        {
#ifdef _WIN32
            char ext[_MAX_EXT] = {};
            _splitpath(str.c_str(), NULL, NULL, NULL, ext);
            return ext;
#else
            std::string::size_type i = str.find_last_of('.');
            if (i != std::string::npos)
            {
                return str.substr(i);
            }
            else
            {
                return "";
            }
#endif
        }

        static std::string GetNoExt(const std::string& str)
        {
#ifdef _WIN32
            char szDrive[_MAX_DRIVE];
            char szDir[_MAX_PATH];
            char szFname[_MAX_FNAME];

            _splitpath(str.c_str(), szDrive, szDir, szFname, NULL);

            std::string strPath;
            strPath += szDrive;
            strPath += szDir;
            strPath += szFname;
            return strPath;
#else
            std::string::size_type i = str.find_last_of('.');
            if (i != std::string::npos)
            {
                return str.substr(0, i);
            }
            else
            {
                return str;
            }
#endif
        }

        static std::shared_ptr<image<color4> > ConvertImage(const std::shared_ptr<image<color3> >& img)
        {
            if (!img.get()) return std::shared_ptr<image<color4> >();

            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<image<color4> > out(new memory_image<color4>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    color3 c = img->get(x, y);
                    out->set(x, y, color4(c[0], c[1], c[2], 1));
                }
            }
            return out;
        }

        std::shared_ptr<image<color4> > get_image_from_file(const char* szPicFile)
        {
            std::string strExt = GetExt(szPicFile);
            if (strExt == ".tif" || strExt == ".tiff")
            {
                return ReadTiffColor4Image(szPicFile);
            }
            else if (strExt == ".bmp")
            {
                return ConvertImage(ReadBMPColor3Image(szPicFile));
            }
            else if (strExt == ".png")
            {
                return ReadPNGColor4Image(szPicFile);
            }
            else if (strExt == ".jpg" || strExt == ".jpeg")
            {
                return ReadJPEGColor4Image(szPicFile);
            }
            else if (strExt == ".tga")
            {
                return ConvertImage(ReadTGAColor3Image(szPicFile));
            }
            else if (strExt == ".exr")
            {
                return ReadEXRColor4Image(szPicFile);
            }
            else if (strExt == ".hdr")
            {
                return ReadHDRColor4Image(szPicFile);
            }
            else if (strcasecmp(strExt.c_str(), ".dds") == 0)
            {
                return ReadDDSColor4Image(szPicFile);
            }
            return ReadTiffColor4Image(szPicFile);
        }

        static real clamp(real x, real a = 0, real b = 255)
        {
            return floor(std::min(std::max(a, x), b));
        }

        static void CreateBuffer(std::vector<unsigned char>& buffer, const std::shared_ptr<image<color4> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            size_t total = width * height * 4;
            buffer.resize(total);
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    color4 c = img->get(x, y);
                    unsigned char R = (unsigned char)(((int)clamp(c[0] * 255)) & 0xFF);
                    unsigned char G = (unsigned char)(((int)clamp(c[1] * 255)) & 0xFF);
                    unsigned char B = (unsigned char)(((int)clamp(c[2] * 255)) & 0xFF);
                    unsigned char A = (unsigned char)(((int)clamp(c[3] * 255)) & 0xFF);

                    size_t idx = (y * width + x);
                    buffer[4 * idx + 0] = R;
                    buffer[4 * idx + 1] = G;
                    buffer[4 * idx + 2] = B;
                    buffer[4 * idx + 3] = A;
                }
            }
        }

        int save_image_to_file(const char* szTexFile, const std::shared_ptr<image<color4> >& img)
        {
            std::string strExt = GetExt(szTexFile);
            if (strExt == ".tif" || strExt == ".tiff")
            {
                std::vector<unsigned char> buffer;
                CreateBuffer(buffer, img);
                return (WriteTiffRGBAImage(szTexFile, &buffer[0], img->get_width(), img->get_height()) ? 0 : -1);
            }
            else if (strExt == ".bmp")
            {
                std::vector<unsigned char> buffer;
                CreateBuffer(buffer, img);
                return (WriteBMPRGBAImage(szTexFile, &buffer[0], img->get_width(), img->get_height()) ? 0 : -1);
            }
            else if (strExt == ".png")
            {
                std::vector<unsigned char> buffer;
                CreateBuffer(buffer, img);
                return (WritePNGRGBAImage(szTexFile, &buffer[0], img->get_width(), img->get_height()) ? 0 : -1);
            }
            else if (strExt == ".hdr")
            {
                return (WriteHDRColor4Image(szTexFile, img) ? 0 : -1);
            }
            return -1;
        }

        int save_image_to_files(const char* szTexFile, const std::vector<std::shared_ptr<image<color4> > >& vecImg)
        {
            std::string strExt = GetExt(szTexFile);
            //if(strExt == ".kxf")
            //{
            //
            //}
            //else
            {
                std::string strNoExt = GetNoExt(szTexFile);
                size_t sz = vecImg.size();
                if (sz == 1)
                {
                    int nRet = save_image_to_file(szTexFile, vecImg[0]);
                    if (nRet != 0) return nRet;
                }
                else
                {
                    for (size_t i = 0; i < sz; i++)
                    {
                        char buf[16] = {};
                        sprintf(buf, "_%02d", (int)i);

                        std::string strPath;
                        strPath += strNoExt;
                        strPath += buf;
                        strPath += strExt;
                        int nRet = save_image_to_file(strPath.c_str(), vecImg[i]);
                        if (nRet != 0) return nRet;
                    }
                }
            }
            return 0;
        }

        int save_debug_images(const char* szTexFile, const std::vector<std::shared_ptr<image<color4> > >& vecImg)
        {
            size_t sz = vecImg.size();
            for (size_t i = 0; i < sz; i++)
            {
                int width = vecImg[i]->get_width();
                int height = vecImg[i]->get_height();

                char buf[16] = {};
                sprintf(buf, "%02d", i);

                std::string strOut = szTexFile;
                strOut += ".debug";
                strOut += buf;
                strOut += ".png";

                std::vector<unsigned char> buffer;
                CreateBuffer(buffer, vecImg[i]);
                WritePNGRGBAImage(strOut.c_str(), &buffer[0], width, height);
            }
            return 0;
        }

        int save_to_png(const char* szTexFile, const std::shared_ptr<image<color4> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::vector<unsigned char> buffer;
            CreateBuffer(buffer, img);
            bool bRet = WritePNGRGBAImage(szTexFile, &buffer[0], width, height);
            if (bRet)
                return 0;
            else
                return -1;
        }

        static void CreateMips(std::vector<std::shared_ptr<image<color4> > >& vecImg, const std::shared_ptr<image<color4> >& img, int WW, int HW)
        {
            vecImg.push_back(img);
            int width = img->get_width();
            int height = img->get_height();
            if (width == 1 && height == 1) return;
            int r_width = width >> 1;
            int r_height = height >> 1;
            if (r_width == 0) r_width = 1;
            if (r_height == 0) r_height = 1;

            std::shared_ptr<corresponder> corx(new powerof2_clamp_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            switch (WW)
            {
            case WRAP_REPEAT:
                corx.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                corx.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                corx.reset(new powerof2_mirror_corresponder());
                break;
            }

            switch (HW)
            {
            case WRAP_REPEAT:
                cory.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                cory.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                cory.reset(new powerof2_mirror_corresponder());
                break;
            }

            std::shared_ptr<unit_image_texture<color4> > tex(
                new unit_image_texture<color4>(
                    img,
                    corx,
                    cory,
                    new bilinear_image_interpolator<color4>()));

            std::shared_ptr<image<color4> > out(new memory_image<color4>(r_width, r_height));
            for (int y = 0; y < r_height; y++)
            {
                real yy = (y + 0.5) / r_height;
                for (int x = 0; x < r_width; x++)
                {
                    real xx = (x + 0.5) / r_width;
                    out->set(x, y, tex->get(xx, yy));
                }
            }
            CreateMips(vecImg, out, WW, HW);
        }

        void create_mipmap_images(std::vector<std::shared_ptr<image<color4> > >& vecImg, const std::shared_ptr<image<color4> >& img, const char* swrap, const char* twrap)
        {
            int SW = ToWrapType(swrap);
            int TW = ToWrapType(twrap);
            CreateMips(vecImg, img, SW, TW);
        }

        void create_cubemap_names(
            const std::string& str,
            std::string& px, std::string& nx,
            std::string& py, std::string& ny,
            std::string& pz, std::string& nz)
        {
            std::string::size_type i = str.find_first_of('.');
            if (i != std::string::npos)
            {
                std::string a = str.substr(0, i);
                std::string b = str.substr(i);
                px = a + "_px" + b;
                nx = a + "_nx" + b;
                py = a + "_py" + b;
                ny = a + "_ny" + b;
                pz = a + "_pz" + b;
                nz = a + "_nz" + b;
            }
            else
            {
                px = str + "_px";
                nx = str + "_nx";
                py = str + "_py";
                ny = str + "_ny";
                pz = str + "_pz";
                nz = str + "_nz";
            }
        }
    }
}
