#include "ri_gauss_filter.h"
#include "ri_tex_gl_context.h"

#include <stdio.h>
#include <memory>
#include <vector>
#include <memory_image.hpp>

#include <gl_tex/gl_gauss_filter_pass.h>

#include <color.h>
#include <filter/corresponder.h>
#include <graphics/image/image_interpolator.hpp>
#include <graphics/image/corresponded_image.hpp>
#include <graphics/texture/image_texture/image_texture.hpp>
#include <graphics/texture/image_texture/unit_image_texture.hpp>

#include <aligned_allocator.hpp>

#include <xmmintrin.h>
#include <immintrin.h>

using namespace gl::tex;

namespace kaze
{
    namespace ri
    {
        enum
        {
            WRAP_REPEAT = 0,
            WRAP_CLAMP,
            WRAP_MIRROR,
            WRAP_BORDER,
        };

        struct TokenValueValue
        {
            const char* token;
            int value1;
            int value2;
        };

        static TokenValueValue WRAP_TYPES[] =
            {
                {"repeat", WRAP_REPEAT, GL_REPEAT},
                {"clamp", WRAP_CLAMP, GL_CLAMP_TO_EDGE},
                {"mirror", WRAP_MIRROR, GL_MIRRORED_REPEAT},
                {"border", WRAP_BORDER, GL_CLAMP_TO_BORDER},
                {NULL, 0},
        };

        static int ToGLWrap(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (strcmp(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].value2;
                i++;
            }
            return GL_CLAMP_TO_EDGE;
        }

        static int ToWrapType(const char* szFilter)
        {
            int i = 0;
            while (WRAP_TYPES[i].token)
            {
                if (strcmp(szFilter, WRAP_TYPES[i].token) == 0) return WRAP_TYPES[i].value1;
                i++;
            }
            return WRAP_REPEAT;
        }

        static std::shared_ptr<image<color4> > FilterImageGL(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<gl::gl_texture_2D> texInput(new gl::gl_texture_2D());
            std::shared_ptr<gl::gl_texture_2D> texOutput(new gl::gl_texture_2D());
            {
                std::vector<float> fBuffer(4 * width * height);
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        size_t index = y * width + x;
                        color4 c = img->get(x, y);
                        fBuffer[4 * index + 0] = (float)c[0];
                        fBuffer[4 * index + 1] = (float)c[1];
                        fBuffer[4 * index + 2] = (float)c[2];
                        fBuffer[4 * index + 3] = (float)c[3];
                    }
                }

                texInput->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, &fBuffer[0]);
            }
            {
                texInput->bind();
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);  //not biliear
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);  //
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, ToGLWrap(swrap)); //TODO
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, ToGLWrap(twrap)); //TODO
                texInput->unbind();
            }
            texOutput->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, 0);

            //
            std::vector<std::shared_ptr<gl_pass> > passList;
            {
                std::shared_ptr<gl::gl_texture_2D> texTmp(new gl::gl_texture_2D());
                texTmp->bind();
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);  //not biliear
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);  //
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, ToGLWrap(swrap)); //TODO
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, ToGLWrap(twrap)); //TODO
                texTmp->unbind();
                texTmp->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, 0);

                passList.push_back(std::shared_ptr<gl_pass>(new gl_gauss_filter_pass(width, height, texInput, texTmp, swidth, twidth, 0)));
                passList.push_back(std::shared_ptr<gl_pass>(new gl_gauss_filter_pass(width, height, texTmp, texOutput, (int)swidth, (int)twidth, 1)));
            }
            //
            for (size_t i = 0; i < passList.size(); i++)
            {
                passList[i]->run();
            }
            //

            std::shared_ptr<image<color4> > outImg;
            std::vector<float> fBuffer(4 * width * height, 0.0f);
            texOutput->bind();
            ::glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &fBuffer[0]);
            texOutput->unbind();

            outImg.reset(new memory_image<color4>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    size_t index = y * width + x;
                    float r = fBuffer[4 * index + 0];
                    float g = fBuffer[4 * index + 1];
                    float b = fBuffer[4 * index + 2];
                    float a = fBuffer[4 * index + 3];
                    outImg->set(x, y, color4(r, g, b, a));
                }
            }
            
            return outImg;
        }

        //--------------------------------------------------

        static real getWeight(real x, real s)
        {
            return std::max<real>(0, exp(-(x * x) / (2 * s * s)));
        }

        static std::shared_ptr<image<color4> > FilterImage(
            const std::shared_ptr<image<color4> >& inImg,
            const char* swrap, const char* twrap,
            float swidth, float twidth)
        {
            float sigma = 0.3f;
            int WW = ToWrapType(swrap);
            int HW = ToWrapType(twrap);

            std::shared_ptr<corresponder> corx(new powerof2_clamp_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            switch (WW)
            {
            case WRAP_REPEAT:
                corx.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                corx.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                corx.reset(new powerof2_mirror_corresponder());
                break;
            }

            switch (HW)
            {
            case WRAP_REPEAT:
                cory.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                cory.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                cory.reset(new powerof2_mirror_corresponder());
                break;
            }

            int width = inImg->get_width();
            int height = inImg->get_height();

            std::shared_ptr<image<color4> > outImg(new memory_image<color4>(width, height));
            std::shared_ptr<image<color4> > tmpImg(new memory_image<color4>(width, height));

            int sf = ((int)ceil(swidth)) / 2;
            int tf = ((int)ceil(twidth)) / 2;

            //first pass
            {
                std::shared_ptr<unit_image_texture<color4> > tex(
                    new unit_image_texture<color4>(
                        inImg,
                        corx,
                        cory,
                        new nearest_image_interpolator<color4>()));

                for (int y = 0; y < height; y++)
                {
                    real yy = y + 0.5;
                    for (int x = 0; x < width; x++)
                    {
                        real xx = x + 0.5;
                        color4 col = color4(0, 0, 0, 0);
                        real ww = 0;
                        for (int s = -sf; s <= +sf; s++)
                        {
                            real w = getWeight(s / (swidth * 0.5), sigma);
                            ww += w;

                            real u = (xx + s) / width;
                            real v = (yy) / height;
                            col += w * tex->get(u, v);
                        }
                        col /= ww;
                        tmpImg->set(x, y, col);
                    }
                }
            }

            //second pass
            {
                std::shared_ptr<unit_image_texture<color4> > tex(
                    new unit_image_texture<color4>(
                        tmpImg,
                        corx,
                        cory,
                        new nearest_image_interpolator<color4>()));

                for (int y = 0; y < height; y++)
                {
                    real yy = y + 0.5;
                    for (int x = 0; x < width; x++)
                    {
                        real xx = x + 0.5;
                        color4 col = color4(0, 0, 0, 0);
                        real ww = 0;
                        for (int t = -tf; t <= +tf; t++)
                        {
                            real w = getWeight(t / (twidth * 0.5), sigma);

                            ww += w;

                            real u = (xx) / width;
                            real v = (yy + t) / height;
                            col += w * tex->get(u, v);
                        }
                        col /= ww;
                        outImg->set(x, y, col);
                    }
                }
            }
            return outImg;
        }

        //-----------------------------------------
        static color4 Conv(const color4f& c)
        {
            return color4(c[0], c[1], c[2], c[3]);
        }
        static color4f Conv(const color4& c)
        {
            return color4f(c[0], c[1], c[2], c[3]);
        }

        static inline __m256 Sum(const __m256& a, const __m256& b)
        {
            __m256 sum = _mm256_hadd_ps(a, b);
            sum = _mm256_hadd_ps(sum, sum);
            __m256 rsum = _mm256_permute2f128_ps(sum, sum, 0 << 4 | 1);
            sum = _mm256_unpacklo_ps(sum, rsum);
            sum = _mm256_hadd_ps(sum, sum);
            return sum;
        }

        static inline __m256 Sum(const __m256& sum)
        {
            return Sum(sum, sum);
        }

        static std::shared_ptr<image<color4f> > ConvertImage(const std::shared_ptr<image<color4> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<image<color4f> > outImg(new memory_image<color4f>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    outImg->set(x, y, Conv(img->get(x, y)));
                }
            }
            return outImg;
        }

        static std::shared_ptr<image<color4> > ConvertImage(const std::shared_ptr<image<color4f> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();
            std::shared_ptr<image<color4> > outImg(new memory_image<color4>(width, height));
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    outImg->set(x, y, Conv(img->get(x, y)));
                }
            }
            return outImg;
        }

        namespace
        {
            class AVXImage : public image<float>
            {
            public:
                AVXImage(int width, int height)
                    : width_(width), height_(height)
                {
                    mem_ = (float*)malloc(width_ * height_ * sizeof(float));
                    memset(mem_, 0, width_ * height_ * sizeof(float));
                }
                ~AVXImage()
                {
                    free(mem_);
                }
                int get_width() const { return width_; }
                int get_height() const { return height_; }
                float get(int x, int y) const { return mem_[y * width_ + x]; }
                void set(int x, int y, const float& c) { mem_[y * width_ + x] = c; }

                const float* get_ptr(int x, int y) const { return &mem_[y * width_ + x]; }
            private:
                int width_;
                int height_;
                float* mem_;
            };
        }

        typedef std::vector<float, aligned_allocator<float, 32> > alvector;

        static std::shared_ptr<image<color4f> > FilterImageOpt(
            const std::shared_ptr<image<color4f> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth)
        {
            float sigma = 0.3f;
            int WW = ToWrapType(swrap);
            int HW = ToWrapType(twrap);

            std::shared_ptr<corresponder> corx(new powerof2_clamp_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            switch (WW)
            {
            case WRAP_REPEAT:
                corx.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                corx.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                corx.reset(new powerof2_mirror_corresponder());
                break;
            }

            switch (HW)
            {
            case WRAP_REPEAT:
                cory.reset(new powerof2_repeat_corresponder());
                break;
            case WRAP_CLAMP:
            case WRAP_BORDER:
                cory.reset(new powerof2_clamp_corresponder());
                break;
            case WRAP_MIRROR:
                cory.reset(new powerof2_mirror_corresponder());
                break;
            }

            int width = img->get_width();
            int height = img->get_height();

            std::shared_ptr<image<color4f> > outImg(new memory_image<color4f>(width, height));
            std::shared_ptr<image<color4f> > tmpImg(new memory_image<color4f>(width, height));

            int sf = ((int)ceil(swidth)) / 2;
            int tf = ((int)ceil(twidth)) / 2;

            //first pass
            {
                std::shared_ptr<AVXImage> rImg(new AVXImage(width + 2 * sf, height));
                std::shared_ptr<AVXImage> gImg(new AVXImage(width + 2 * sf, height));
                std::shared_ptr<AVXImage> bImg(new AVXImage(width + 2 * sf, height));
                std::shared_ptr<AVXImage> aImg(new AVXImage(width + 2 * sf, height));
                {
                    std::shared_ptr<image<color4f> > corImg(
                        new corresponded_image<color4f>(
                            img,
                            corx,
                            cory));
                    for (int y = 0; y < height; y++)
                    {
                        for (int x = -sf; x < width + sf; x++)
                        {
                            color4f c = corImg->get(x, y);
                            rImg->set(x + sf, y, c[0]);
                            gImg->set(x + sf, y, c[1]);
                            bImg->set(x + sf, y, c[2]);
                            aImg->set(x + sf, y, c[3]);
                        }
                    }
                }

                int wsz = 2 * sf + 1;
                std::vector<float> weights(wsz);
                {
                    float ww = 0.0f;
                    for (int i = 0; i < wsz; i++)
                    {
                        float w = (float)getWeight((i - sf) / (swidth * 0.5), sigma);
                        ww += w;
                        weights[i] = w;
                    }
                    float iw = 1.0f / ww;
                    for (int i = 0; i < wsz; i++)
                    {
                        weights[i] *= iw;
                    }
                }

                int bufsz = (int)ceil(float(wsz) / 8) * 8;

                alvector rv(bufsz);
                alvector gv(bufsz);
                alvector bv(bufsz);
                alvector av(bufsz);
                alvector wv(bufsz);

                float* r = &rv[0];
                float* g = &gv[0];
                float* b = &bv[0];
                float* a = &av[0];
                float* w = &wv[0];
                memset(r, 0, sizeof(float) * bufsz);
                memset(g, 0, sizeof(float) * bufsz);
                memset(b, 0, sizeof(float) * bufsz);
                memset(a, 0, sizeof(float) * bufsz);
                memcpy(w, &weights[0], sizeof(float) * wsz);

                int block = (int)ceil(float(wsz) / 8);

                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        memcpy(r, rImg->get_ptr(x, y), sizeof(float) * wsz);
                        memcpy(g, gImg->get_ptr(x, y), sizeof(float) * wsz);
                        memcpy(b, bImg->get_ptr(x, y), sizeof(float) * wsz);
                        memcpy(a, aImg->get_ptr(x, y), sizeof(float) * wsz);

                        __m256 rx = _mm256_setzero_ps();
                        __m256 gx = _mm256_setzero_ps();
                        __m256 bx = _mm256_setzero_ps();
                        __m256 ax = _mm256_setzero_ps();
                        for (int i = 0; i < block; i++)
                        {
                            __m256 wx = _mm256_load_ps(w + 8 * i);
                            rx = _mm256_fmadd_ps(wx, _mm256_load_ps(r + 8 * i), rx);
                            gx = _mm256_fmadd_ps(wx, _mm256_load_ps(g + 8 * i), gx);
                            bx = _mm256_fmadd_ps(wx, _mm256_load_ps(b + 8 * i), bx);
                            ax = _mm256_fmadd_ps(wx, _mm256_load_ps(a + 8 * i), ax);
                        }
                        rx = Sum(rx);
                        gx = Sum(gx);
                        bx = Sum(bx);
                        ax = Sum(ax);
                        float rr = ((float*)(&rx))[0];
                        float gg = ((float*)(&gx))[0];
                        float bb = ((float*)(&bx))[0];
                        float aa = ((float*)(&ax))[0];
                        tmpImg->set(x, y, color4f(rr, gg, bb, aa));
                    }
                }
            }

            //second pass
            {
                std::shared_ptr<AVXImage> rImg(new AVXImage(height + 2 * tf, width));
                std::shared_ptr<AVXImage> gImg(new AVXImage(height + 2 * tf, width));
                std::shared_ptr<AVXImage> bImg(new AVXImage(height + 2 * tf, width));
                std::shared_ptr<AVXImage> aImg(new AVXImage(height + 2 * tf, width));
                {
                    std::shared_ptr<image<color4f> > corImg(
                        new corresponded_image<color4f>(
                            tmpImg,
                            corx,
                            cory));
                    for (int y = -tf; y < height + tf; y++)
                    {
                        for (int x = 0; x < width; x++)
                        {
                            color4f c = corImg->get(x, y);
                            rImg->set(y + tf, x, c[0]);
                            gImg->set(y + tf, x, c[1]);
                            bImg->set(y + tf, x, c[2]);
                            aImg->set(y + tf, x, c[3]);
                        }
                    }
                }

                int wsz = 2 * tf + 1;
                std::vector<float> weights(wsz);
                {
                    float ww = 0.0f;
                    for (int i = 0; i < wsz; i++)
                    {
                        float w = (float)getWeight((i - tf) / (twidth * 0.5), sigma);
                        ww += w;
                        weights[i] = w;
                    }
                    float iw = 1.0f / ww;
                    for (int i = 0; i < wsz; i++)
                    {
                        weights[i] *= iw;
                    }
                }

                int bufsz = (int)ceil(float(wsz) / 8) * 8;

                alvector rv(bufsz);
                alvector gv(bufsz);
                alvector bv(bufsz);
                alvector av(bufsz);
                alvector wv(bufsz);

                float* r = &rv[0];
                float* g = &gv[0];
                float* b = &bv[0];
                float* a = &av[0];
                float* w = &wv[0];
                memset(r, 0, sizeof(float) * bufsz);
                memset(g, 0, sizeof(float) * bufsz);
                memset(b, 0, sizeof(float) * bufsz);
                memset(a, 0, sizeof(float) * bufsz);
                memcpy(w, &weights[0], sizeof(float) * wsz);

                int block = (int)ceil(float(wsz) / 8);

                for (int x = 0; x < width; x++)
                {
                    for (int y = 0; y < height; y++)
                    {
                        memcpy(r, rImg->get_ptr(y, x), sizeof(float) * wsz);
                        memcpy(g, gImg->get_ptr(y, x), sizeof(float) * wsz);
                        memcpy(b, bImg->get_ptr(y, x), sizeof(float) * wsz);
                        memcpy(a, aImg->get_ptr(y, x), sizeof(float) * wsz);

                        __m256 rx = _mm256_setzero_ps();
                        __m256 gx = _mm256_setzero_ps();
                        __m256 bx = _mm256_setzero_ps();
                        __m256 ax = _mm256_setzero_ps();
                        for (int i = 0; i < block; i++)
                        {
                            __m256 wx = _mm256_load_ps(w + 8 * i);
                            rx = _mm256_fmadd_ps(wx, _mm256_load_ps(r + 8 * i), rx);
                            gx = _mm256_fmadd_ps(wx, _mm256_load_ps(g + 8 * i), gx);
                            bx = _mm256_fmadd_ps(wx, _mm256_load_ps(b + 8 * i), bx);
                            ax = _mm256_fmadd_ps(wx, _mm256_load_ps(a + 8 * i), ax);
                        }
                        rx = Sum(rx);
                        gx = Sum(gx);
                        bx = Sum(bx);
                        ax = Sum(ax);
                        float rr = ((float*)(&rx))[0];
                        float gg = ((float*)(&gx))[0];
                        float bb = ((float*)(&bx))[0];
                        float aa = ((float*)(&ax))[0];
                        outImg->set(x, y, color4f(rr, gg, bb, aa));
                    }
                }
            }
            return outImg;
        }

        std::shared_ptr<image<color4> > ri_gauss_filter(
            const std::shared_ptr<image<color4> >& img,
            const char* swrap, const char* twrap,
            float swidth, float twidth)
        {
            if (ri_tex_get_gl_context())
            {
                return FilterImageGL(img, swrap, twrap, swidth, twidth);
            }
            else
            {
                //return FilterImage(img, swrap, twrap, swidth, twidth);
                return ConvertImage(FilterImageOpt(ConvertImage(img), swrap, twrap, swidth, twidth));
            }
        }
    }
}
