#ifndef KAZE_RI_TEX_GL_CONTEXT_H
#define KAZE_RI_TEX_GL_CONTEXT_H

namespace kaze
{
    namespace ri
    {

        bool ri_tex_init_gl_context();
        bool ri_tex_get_gl_context(float version = 4.0);
        bool ri_tex_set_enable_gl_context(bool bEnable);
    }
}

#endif