#include "ri_make_shadow.h"
#include "image/zfile.h"
#include "ri_file_util.h"

#include <string>
#include <utility>
#include <algorithm>

#ifdef _WIN32
#include <windows.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#endif

using namespace ri;

namespace kaze
{
    namespace ri
    {
        static std::string GetTmpPath()
        {
            char buffer[512];
            get_temppath_nodelete(buffer, 512, "ZF"); //
            //_snprintf(buffer,512, "E:/kaze/kaze/src/ri/RIBs/Procedurals/RAT_%d.txt", rand());
            return buffer;
        }

        static bool CopyFile(const char* src, const char* dst)
        {
#ifdef _WIN32
            BOOL bRet = ::CopyFileA(src, dst, FALSE);
            return (bRet) ? true : false;
#else
            char buffer[1024] = {};
            snprintf(buffer, 1024, "cp \"%s\" \"%s\"", src, dst);
            ::system(buffer);
#endif
            return true;
        }

        static double FetchZ(int x, int y, const std::vector<double>& z, int w, int h)
        {
            if (x < 0) x = 0;
            if (w <= x) x = w - 1;
            if (y < 0) y = 0;
            if (h <= y) y = h - 1;
            return z[w * y + x];
        }

        static double ZMax(double zs[9])
        {
            double z = zs[0];
            z = std::max<double>(z, zs[1]);
            z = std::max<double>(z, zs[2]);
            z = std::max<double>(z, zs[3]);
            z = std::max<double>(z, zs[4]);
            z = std::max<double>(z, zs[5]);
            z = std::max<double>(z, zs[6]);
            z = std::max<double>(z, zs[7]);
            z = std::max<double>(z, zs[8]);
            return z;
        }

        static double ZMid(double zs[9])
        {
            std::sort(zs, zs + 9);

            return zs[4];
        }

        static double ZAvg(double zs[9])
        {
            double z = 0;
            z += zs[0];
            z += zs[1];
            z += zs[2];
            z += zs[3];
            z += zs[4];
            z += zs[5];
            z += zs[6];
            z += zs[7];
            z += zs[8];
            z /= 9;
            return z;
        }

        static bool ZFilter(zfile_image& img)
        {
            int w = img.get_width();
            int h = img.get_height();

            const std::vector<double>& in = img.get_buffer_raw();
            std::vector<double> out(w * h);

            double zs[9];
            for (int y = 0; y < h; y++)
            {
                for (int x = 0; x < w; x++)
                {
                    zs[0] = FetchZ(x - 1, y - 1, in, w, h);
                    zs[1] = FetchZ(x - 1, y + 0, in, w, h);
                    zs[2] = FetchZ(x - 1, y + 1, in, w, h);
                    zs[3] = FetchZ(x + 0, y - 1, in, w, h);
                    zs[4] = FetchZ(x + 0, y + 0, in, w, h);
                    zs[5] = FetchZ(x + 0, y + 1, in, w, h);
                    zs[6] = FetchZ(x + 1, y - 1, in, w, h);
                    zs[7] = FetchZ(x + 1, y + 0, in, w, h);
                    zs[8] = FetchZ(x + 1, y + 1, in, w, h);

                    double z = ZMax(zs);

                    out[y * w + x] = z;
                }
            }

            img.set_buffer_raw(out);
            return true;
        }

        static bool MakeShadow(const char* pic, const char* tex)
        {
            zfile_image img;
            if (!img.load(pic)) return false;
            if (!ZFilter(img)) return false;
            if (!img.save(tex)) return false;
            return true;
        }

        int ri_make_shadow(const char* pic, const char* tex)
        {
            if (std::string(pic) != std::string(tex))
            {
                if (!MakeShadow(pic, tex)) return -1;
            }
            else
            {
                std::string tmp = GetTmpPath();
                if (!MakeShadow(pic, tmp.c_str()))
                {
                    delete_file(tmp.c_str());
                    return -1;
                }
                if (!CopyFile(tmp.c_str(), tex))
                {
                    delete_file(tmp.c_str());
                    return -1;
                }
                delete_file(tmp.c_str());
            }
            return 0;
        }
    }
}
