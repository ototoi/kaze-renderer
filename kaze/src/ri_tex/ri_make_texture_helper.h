#ifndef KAZE_RI_MAKE_TEXTURE_HELPER_H
#define KAZE_RI_MAKE_TEXTURE_HELPER_H

#include "image.hpp"
#include "count_ptr.hpp"
#include <vector>

namespace kaze
{
    namespace ri
    {
        std::shared_ptr<image<color4> > get_image_from_file(const char* szPicFile);
        int save_image_to_file(const char* szTexFile, const std::shared_ptr<image<color4> >& img);
        int save_image_to_files(const char* szTexFile, const std::vector<std::shared_ptr<image<color4> > >& vecImg);
        int save_debug_images(const char* szTexFile, const std::vector<std::shared_ptr<image<color4> > >& vecImg);
        int save_to_png(const char* szTexFile, const std::shared_ptr<image<color4> >& img);

        void create_mipmap_images(std::vector<std::shared_ptr<image<color4> > >& vecImg, const std::shared_ptr<image<color4> >& img, const char* swrap, const char* twrap);
        void create_cubemap_names(
            const std::string& pic,
            std::string& px, std::string& nx,
            std::string& py, std::string& ny,
            std::string& pz, std::string& nz);
    }
}

#endif
