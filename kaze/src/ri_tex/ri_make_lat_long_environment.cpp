#define _USE_MATH_DEFINES
#include <math.h>

#include "ri_make_lat_long_environment.h"
#include "ri_make_texture_helper.h"

#include <stdio.h>
#include <stdlib.h>

#include <string>
#include <vector>
#include <algorithm>

//#ifdef _WIN32
//#include <windows.h>
//#endif

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif

#include "image/png_io.h"

#include "image/kxf_io.h"
#include "memory_image.hpp"
#include "filter/corresponder.h"
#include "graphics/image/image_interpolator.hpp"
#include "graphics/texture/image_texture/image_texture.hpp"
#include "graphics/texture/image_texture/unit_image_texture.hpp"

namespace kaze
{
    namespace ri
    {

        enum
        {
            FILTER_BOX = 0,
            FILTER_TRIANGLE,
            FILTER_MITCHELL,
            FILTER_GAUSS,
            FILTER_SINC,
        };

        enum
        {
            RESIZE_NONE = 0,
            RESIZE_UP,
            RESIZE_DOWN,
            RESIZE_ROUND,
            RESIZE_UP_M,
            RESIZE_DOWN_M,
            RESIZE_ROUND_M,
        };

        struct TokenValue
        {
            const char* token;
            int value;
        };

        struct TokenTokenValue
        {
            const char* token;
            const char* token2;
            int value;
        };

        static TokenTokenValue FILTER_TYPES[] =
            {
                {"box", "box", FILTER_BOX},
                {"point", "box", FILTER_BOX},
                {"mean", "box", FILTER_BOX},
                {"triangle", "triangle", FILTER_TRIANGLE},
                {"tent", "triangle", FILTER_TRIANGLE},
                {"mitchel", "mitchell", FILTER_MITCHELL},
                {"mitchell", "mitchell", FILTER_MITCHELL},
                {"gauss", "gauss", FILTER_GAUSS},
                {"gaussian", "gauss", FILTER_GAUSS},
                {"gaussian-soft", "gauss", FILTER_GAUSS},
                {"sinc", "sinc", FILTER_SINC},
                {NULL, "", 0},
        };

        static TokenValue RESIZE_TYPES[] =
            {
                {"none", RESIZE_NONE},
                {"up", RESIZE_UP},
                {"down", RESIZE_DOWN},
                {"round", RESIZE_ROUND},
                {"up-", RESIZE_UP_M},
                {"down-", RESIZE_DOWN_M},
                {"round-", RESIZE_ROUND_M},
                {NULL, 0},
        };

        static const char* ToInternalFilter(const char* szFilter)
        {
            int i = 0;
            while (FILTER_TYPES[i].token)
            {
                if (strcmp(szFilter, FILTER_TYPES[i].token) == 0) return FILTER_TYPES[i].token2;
                i++;
            }
            return "mitchell";
        }

        static int ToResizeType(const char* szFilter)
        {
            int i = 0;
            while (RESIZE_TYPES[i].token)
            {
                if (strcmp(szFilter, RESIZE_TYPES[i].token) == 0) return RESIZE_TYPES[i].value;
                i++;
            }
            return RESIZE_NONE;
        }

        static int GetReizeWidth(int width, int R)
        {
            switch (R)
            {
            case RESIZE_NONE:
                return width;
            case RESIZE_UP:
            case RESIZE_UP_M:
            {
                int r = 1;
                while (r < width)
                {
                    r = r << 1;
                }
                return r;
            }
            break;
            case RESIZE_DOWN:
            case RESIZE_DOWN_M:
            {
                int r = 1;
                while ((r << 1) < width)
                {
                    r = r << 1;
                }
                return r;
            }
            break;
            case RESIZE_ROUND:
            case RESIZE_ROUND_M:
            {
                int u = GetReizeWidth(width, RESIZE_UP);
                int d = GetReizeWidth(width, RESIZE_DOWN);
                return (u - width) < (width - d) ? u : d;
            }
            break;
            }
            return GetReizeWidth(width, RESIZE_UP);
        }

        static bool IsLongestResize(int R)
        {
            return (R == RESIZE_UP || R == RESIZE_DOWN || R == RESIZE_ROUND);
        }

        static std::shared_ptr<image<color4> > ResizeImage(const std::shared_ptr<image<color4> >& img, int WR, int HR)
        {
            int width = img->get_width();
            int height = img->get_height();

            int r_width = GetReizeWidth(width, WR);
            int r_height = GetReizeWidth(height, HR);
            if (IsLongestResize(WR))
            {
                r_width = 2 * r_height;
            }

//--------------------------------------------------------
#ifdef _DEBUG
            printf("%d, %d\n", WR, HR);
            printf("width = %d, height = %d\n", width, height);
            printf("width = %d, height = %d\n", r_width, r_height);
#endif
            //--------------------------------------------------------
            if (width == r_width && height == r_height) return img;

            std::shared_ptr<image<color4> > out(new memory_image<color4>(r_width, r_height));

            std::shared_ptr<corresponder> corx(new powerof2_repeat_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            std::shared_ptr<unit_image_texture<color4> > tex(
                new unit_image_texture<color4>(
                    img,
                    corx,
                    cory,
                    new bilinear_image_interpolator<color4>()));

            for (int y = 0; y < r_height; y++)
            {
                real yy = (y + 0.5) / r_height;
                for (int x = 0; x < r_width; x++)
                {
                    real xx = (x + 0.5) / r_width;
                    out->set(x, y, tex->get(xx, yy));
                }
            }

            return out;
        }

        static std::shared_ptr<image<color4> > ResizeImage(
            const std::shared_ptr<image<color4> >& img,
            const char* szWResize = "up", const char* szHResize = "up")
        {
            int WR = ToResizeType(szWResize);
            int HR = ToResizeType(szHResize);
            return ResizeImage(img, WR, HR);
        }

        static std::shared_ptr<image<color4> > GetImageFromFile(const char* szPicFile)
        {
            return get_image_from_file(szPicFile);
        }

        static bool CheckKTXHeader(const char* szTexFile, const char* swrap, const char* twrap, const char* filter, float swidth, float twidth, int width, int height)
        {
            kxf_header header;
            if (read_kxf_header(szTexFile, &header) != 0) return false;
            if (std::string(header.swrap) != swrap) return false;
            if (std::string(header.twrap) != twrap) return false;
            if (std::string(header.filter) != filter) return false;
            if (header.swidth != swidth) return false;
            if (header.twidth != twidth) return false;
            if (header.width != width) return false;
            if (header.height != height) return false;

            return true;
        }

        static void CreateRips(std::vector<std::shared_ptr<image<color4> > >& vecImg, const std::shared_ptr<image<color4> >& img)
        {
            vecImg.push_back(img);
            int width = img->get_width();
            int height = img->get_height();
            if (width == 1) return;
            int r_width = width >> 1;
            int r_height = height;
            if (r_width == 0) r_width = 1;

            std::shared_ptr<corresponder> corx(new powerof2_repeat_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            std::shared_ptr<unit_image_texture<color4> > tex(
                new unit_image_texture<color4>(
                    img,
                    corx,
                    cory,
                    new bilinear_image_interpolator<color4>()));

            std::shared_ptr<image<color4> > out(new memory_image<color4>(r_width, r_height));
            for (int y = 0; y < r_height; y++)
            {
                real yy = (y + 0.5) / r_height;
                for (int x = 0; x < r_width; x++)
                {
                    real xx = (x + 0.5) / r_width;
                    out->set(x, y, tex->get(xx, yy));
                }
            }
            CreateRips(vecImg, out);
        }

        static inline double log2(double x)
        {
            using namespace std;
            static const double LOG2 = log(2.0);
            return log(x) / LOG2;
        }

        static std::shared_ptr<image<color4> > FilterLatLong(const std::shared_ptr<image<color4> >& img)
        {
            int width = img->get_width();
            int height = img->get_height();

            std::vector<std::shared_ptr<image<color4> > > vecImg;
            CreateRips(vecImg, img);

            std::shared_ptr<corresponder> corx(new powerof2_repeat_corresponder());
            std::shared_ptr<corresponder> cory(new powerof2_clamp_corresponder());

            std::vector<std::shared_ptr<unit_image_texture<color4> > > vecTex;
            for (size_t i = 0; i < vecImg.size(); i++)
            {
                std::shared_ptr<unit_image_texture<color4> > tex(
                    new unit_image_texture<color4>(
                        vecImg[i],
                        corx,
                        cory,
                        new bilinear_image_interpolator<color4>()));
                vecTex.push_back(tex);
            }

            std::shared_ptr<image<color4> > out(new memory_image<color4>(width, height));
            for (int y = 0; y < height; y++)
            {
                real yy = (y + 0.5) / height;
                real phi = fabs(2 * yy - 1) * M_PI * 0.5;
                real ring = cos(phi);

                real s = log2(1.0 / ring); //1 -> 0

                real level = std::min(s, (real)(vecTex.size() - 1));

                int level0 = (int)floor(level);
                int level1 = level0 + 1;

                real a = level - level0;

                for (int x = 0; x < width; x++)
                {
                    real xx = (x + 0.5) / width;
                    if (a <= 0.0)
                    {
                        color4 c = vecTex[level0]->get(xx, yy);
                        out->set(x, y, c);
                    }
                    else
                    {
                        color4 c0 = vecTex[level0]->get(xx, yy);
                        color4 c1 = vecTex[level1]->get(xx, yy);
                        color4 c = c0 * (1.0 - a) + c1 * a;
                        out->set(x, y, c);
                    }
                }
            }

            return out;
        }

        static int ConvertTexture(const char* szPicFile, const char* szTexFile, const char* filter, float swidth, float twidth, int n, const char* tokens[], const char* params[], bool bForce = false)
        {
            std::shared_ptr<image<color4> > img = GetImageFromFile(szPicFile);
            if (!img.get()) return -1;
            bool bMipmap = true;
            bool bDebug = false;

            std::string strSresize = "up";
            std::string strTresize = "up";
            for (int i = 0; i < n; i++)
            {
                if (strcmp(tokens[i], "sresize") == 0)
                {
                    strSresize = params[i];
                }
                else if (strcmp(tokens[i], "tresize") == 0)
                {
                    strTresize = params[i];
                }
                else if (strcmp(tokens[i], "resize") == 0)
                {
                    strSresize = strTresize = params[i];
                }
                else if (strcmp(tokens[i], "force") == 0)
                {
                    bForce = (atoi(params[i])) ? true : false;
                }
                else if (strcmp(tokens[i], "debug") == 0)
                {
                    bDebug = (atoi(params[i])) ? true : false;
                }
            }

#ifdef _DEBUG
            bDebug = true;
            bForce = true;
#endif
            //bDebug = true;
            //bForce = true;

            const char* sresize = strSresize.c_str();
            const char* tresize = strTresize.c_str();

            if (bForce)
            {
                img = ResizeImage(img, sresize, tresize);
                img = FilterLatLong(img);
                std::vector<std::shared_ptr<image<color4> > > vecImg;
                vecImg.push_back(img);

                if (bDebug)
                {
                    save_debug_images(szTexFile, vecImg);
                }

                return save_to_kxf_latlong(szTexFile, img, filter, swidth, twidth);
            }
            else
            {
                int WR = ToResizeType(sresize);
                int HR = ToResizeType(tresize);
                int width = GetReizeWidth(img->get_width(), WR);
                int height = GetReizeWidth(img->get_height(), HR);
                if (IsLongestResize(WR))
                {
                    width = 2 * height;
                }
                if (CheckKTXHeader(szTexFile, "repeat", "clamp", filter, swidth, twidth, width, height))
                {
                    return 0;
                }
                else
                {
                    img = ResizeImage(img, sresize, tresize);
                    img = FilterLatLong(img);
                    std::vector<std::shared_ptr<image<color4> > > vecImg;
                    vecImg.push_back(img);

                    if (bDebug)
                    {
                        save_debug_images(szTexFile, vecImg);
                    }
                    return save_to_kxf_latlong(szTexFile, img, filter, swidth, twidth);
                }
            }
        }

        static bool IsExistFile(const char* szFilePath)
        {
#ifdef _WIN32
            struct _stat s;
            int nRet = _stat(szFilePath, &s);
            return (nRet == 0);
#else
            struct stat s;
            int nRet = stat(szFilePath, &s);
            return (nRet == 0);
#endif
        }

        static size_t GetLastModifiedTime(const char* szFilePath)
        {
#ifdef _WIN32
            struct _stat s;
            int nRet = _stat(szFilePath, &s);
            return s.st_mtime;
#else
            struct stat s;
            int nRet = stat(szFilePath, &s);
            return s.st_mtime;
#endif
        }

        static int MakeTexture(const char* pic, const char* tex, const char* filter, float swidth, float twidth, int n, const char* tokens[], const char* params[])
        {
            int nRet = 0;
            if (IsExistFile(pic)) nRet |= 1;
            if (IsExistFile(tex)) nRet |= 2;

            switch (nRet)
            {
            case 0:
                return -1;
            case 1:
                return ConvertTexture(pic, tex, filter, swidth, twidth, n, tokens, params, true);
            case 2:
                return -1;
            case 3:
            {
                size_t mpic = GetLastModifiedTime(pic);
                size_t mtex = GetLastModifiedTime(tex);
                bool bForce = mpic >= mtex;
                return ConvertTexture(pic, tex, filter, swidth, twidth, n, tokens, params, bForce);
            }
            }
            return -1;
        }

        int ri_make_lat_long_environment(const char* pic, const char* tex, const char* filterfunc, float swidth, float twidth)
        {
            return ri_make_lat_long_environment(pic, tex, filterfunc, swidth, twidth, 0, 0, 0);
        }

        int ri_make_lat_long_environment(const char* pic, const char* tex, const char* filter, float swidth, float twidth, int n, const char* tokens[], const char* params[])
        {
            std::string strFilter = ToInternalFilter(filter);
            return MakeTexture(pic, tex, strFilter.c_str(), swidth, twidth, n, tokens, params);
        }
    }
}
