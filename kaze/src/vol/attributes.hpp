#pragma once
#ifndef VOL_ATTRIBUTES_HPP
#define VOL_ATTRIBUTES_HPP

#include <vector>
#include <map>
#include <string>
#include <atomic>

namespace vol
{
    enum ELEMENT_TYPE
    {
        ELEMENT_TYPE_INTEGER,
        ELEMENT_TYPE_FLOAT,
        ELEMENT_TYPE_DOUBLE,
        ELEMENT_TYPE_STRING
    };

    inline ELEMENT_TYPE get_element_type_(int)
    {
        return ELEMENT_TYPE_INTEGER;
    }

    inline ELEMENT_TYPE get_element_type_(float)
    {
        return ELEMENT_TYPE_FLOAT;
    }

    inline ELEMENT_TYPE get_element_type_(double)
    {
        return ELEMENT_TYPE_DOUBLE;
    }

    inline ELEMENT_TYPE get_element_type_(const std::string&)
    {
        return ELEMENT_TYPE_STRING;
    }

    class attributes_buffer
    {
    public:
        virtual ~attributes_buffer(){}
        virtual void*  get_ptr()const = 0;
        virtual size_t get_size()const = 0;
        virtual ELEMENT_TYPE get_element_type()const = 0;
    };

    template<class T>
    class attributes_buffer_imp : public attributes_buffer
    {
    public:
        attributes_buffer_imp(size_t sz)
        {
            buffer_.resize(sz);
        }
        virtual void* get_ptr()const
        {
            return (void*)&buffer_[0];
        }
        virtual size_t get_size()const
        {
            return buffer_.size();
        }
        virtual ELEMENT_TYPE get_element_type()const
        {
            return get_element_type_( T() );
        }
        T& operator[](size_t i){return buffer_[i];}
        const T& operator[](size_t i)const{return buffer_[i];}
        size_t size()const{return buffer_.size();}
    private:
        std::vector<T> buffer_;
    };

    class attributes_item
    {
    public:
        explicit attributes_item(const int v)
        {
            size_t sz = 1;
            buffer_.reset( new attributes_buffer_imp<int>(sz) );
            memcpy(buffer_->get_ptr(), &v, sizeof(int)*sz);
            elem_size_ = 1;
        }
        explicit attributes_item(const float v)
        {
            size_t sz = 1;
            buffer_.reset( new attributes_buffer_imp<float>(sz) );
            memcpy(buffer_->get_ptr(), &v, sizeof(float)*sz);
            elem_size_ = 1;
        }
        explicit attributes_item(const double v)
        {
            size_t sz = 1;
            buffer_.reset( new attributes_buffer_imp<double>(sz) );
            memcpy(buffer_->get_ptr(), &v, sizeof(double)*sz);
            elem_size_ = 1;
        }
        explicit attributes_item(const std::string& v)
        {
            size_t sz = 1;
            buffer_.reset( new attributes_buffer_imp<std::string>(sz) );
            attributes_buffer_imp<std::string>& imp = (attributes_buffer_imp<std::string>&)(*buffer_);
            imp[0] = v;
            elem_size_ = 1;
        }
        template<class T, size_t Sz>
        explicit attributes_item(const vector<T, Sz>& v)
        {
            size_t sz = 1;
            buffer_.reset( new attributes_buffer_imp<T>(sz*Sz) );
            memcpy(buffer_->get_ptr(), &v, sizeof(T)*sz*Sz);
            elem_size_ = Sz;
        }
        explicit attributes_item(const std::vector< int >& vec)
        {
            size_t sz = vec.size();
            buffer_.reset( new attributes_buffer_imp<int>(sz) );
            memcpy(buffer_->get_ptr(), &vec[0], sizeof(int)*sz);
            elem_size_ = 1;
        }
        explicit attributes_item(const std::vector< float >& vec)
        {
            size_t sz = vec.size();
            buffer_.reset( new attributes_buffer_imp<float>(sz) );
            memcpy(buffer_->get_ptr(), &vec[0], sizeof(float)*sz);
            elem_size_ = 1;
        }
        explicit attributes_item(const std::vector< double >& vec)
        {
            size_t sz = vec.size();
            buffer_.reset( new attributes_buffer_imp<double>(sz) );
            memcpy(buffer_->get_ptr(), &vec[0], sizeof(double)*sz);
            elem_size_ = 1;
        }
        explicit attributes_item(const std::vector< std::string >& vec)
        {
            size_t sz = vec.size();
            buffer_.reset( new attributes_buffer_imp<std::string>(sz) );
            attributes_buffer_imp<std::string>& imp = (attributes_buffer_imp<std::string>&)(*buffer_);
            for(size_t i = 0;i <imp.size();i++)
            {
                imp[i] = vec[i];
            }
            elem_size_ = 1;
        }
        template<size_t Sz>
        explicit attributes_item(const std::vector< vector<float, Sz> >& vec)
        {
            size_t sz = vec.size();
            elem_size_ = Sz;
            buffer_.reset( new attributes_buffer_imp<float>(sz*Sz) );
            memcpy(buffer_->get_ptr(), &vec[0], sizeof(float)*sz*Sz);
        }
        template<size_t Sz>
        explicit attributes_item(const std::vector< vector<double, Sz> >& vec)
        {
            size_t sz = vec.size();
            elem_size_ = Sz;
            buffer_.reset( new attributes_buffer_imp<double>(sz*Sz) );
            memcpy(buffer_->get_ptr(), &vec[0], sizeof(double)*sz*Sz);
        }
        size_t element_size()const{return elem_size_;}
        size_t get_element_size()const{return elem_size_;}
        ELEMENT_TYPE get_element_type()const{return buffer_->get_element_type();}
        size_t buffer_size()const{return buffer_->get_size();}
    public:
        template<class T>
        T as(size_t i, const T& def = T())const
        {
            if(i < buffer_size()/element_size())
            {
                const T* buf = reinterpret_cast<const T*>(buffer_->get_ptr());
                return buf[i];
            }
            return def;
        }

        std::shared_ptr<attributes_item> clone_at(size_t i)const
        {
            ELEMENT_TYPE et = buffer_->get_element_type();
            switch(et)
            {
                case ELEMENT_TYPE_INTEGER:
                {
                    const int* buffer = reinterpret_cast<const int*>(this->buffer_->get_ptr());
                    std::unique_ptr<attributes_buffer> t( new attributes_buffer_imp<int>(elem_size_) );
                    memcpy(t->get_ptr(), buffer+i*elem_size_, sizeof(int)*elem_size_);
                    return std::shared_ptr<attributes_item>( new attributes_item(t, elem_size_) );
                }
                break;
                case ELEMENT_TYPE_FLOAT:
                {
                    const float* buffer = reinterpret_cast<const float*>(this->buffer_->get_ptr());
                    std::unique_ptr<attributes_buffer> t( new attributes_buffer_imp<float>(elem_size_) );
                    memcpy(t->get_ptr(), buffer+i*elem_size_, sizeof(float)*elem_size_);
                    return std::shared_ptr<attributes_item>( new attributes_item(t, elem_size_) );
                }
                break;
                case ELEMENT_TYPE_DOUBLE:
                {
                    const double* buffer = reinterpret_cast<const double*>(this->buffer_->get_ptr());
                    std::unique_ptr<attributes_buffer> t( new attributes_buffer_imp<double>(elem_size_) );
                    memcpy(t->get_ptr(), buffer+i*elem_size_, sizeof(double)*elem_size_);
                    return std::shared_ptr<attributes_item>( new attributes_item(t, elem_size_) );
                }
                break;
                case ELEMENT_TYPE_STRING:
                {
                    const attributes_buffer_imp<std::string>* buffer = reinterpret_cast< const attributes_buffer_imp<std::string>* >(this->buffer_.get());
                    std::unique_ptr< attributes_buffer_imp<std::string> > t( new attributes_buffer_imp<std::string>(1) );
                    (*t)[0] = (*buffer)[i];
                    return std::shared_ptr<attributes_item>( new attributes_item(t.release(), elem_size_) );
                }
                break;
            }
            return std::shared_ptr<attributes_item>();
        }
    protected:
        attributes_item(attributes_buffer* buffer, int elem_size)
        {
            buffer_.reset(buffer);
            elem_size_ = elem_size;
        }
        attributes_item(std::unique_ptr<attributes_buffer>& buffer, int elem_size)
        {
            buffer_.reset(buffer.release());
            elem_size_ = elem_size;
        }
    protected:
        std::unique_ptr<attributes_buffer> buffer_;
        int elem_size_;
    };

    class attributes_table
    {
    public:
        typedef std::map<std::string, std::shared_ptr<attributes_item> > map_type;
    public:
        class vertex_iterator
        {
        public:
            vertex_iterator(attributes_table* tbl, size_t i, size_t vsz)
            {
                tbl_ = tbl;
                i_ = i;
                vsz_ = vsz;
            }
            vertex_iterator operator++(int)
            { 
                vertex_iterator tmp(tbl_, i_, vsz_);
                i_++;
                return tmp;
            }
            vertex_iterator& operator++()
            {
                i_++;
                return *this;
            }

            bool equal(const vertex_iterator& rhs)const
            {
                return (this->tbl_ == rhs.tbl_) && (this->i_ == rhs.i_) && (this->vsz_ == rhs.vsz_);
            }
     
            template<class T>
            T as(const std::string& key, const T& def = T())const
            {
                const attributes_item* item = tbl_->get(key);
                if(item)
                {
                    return item->as<T>(i_, def);
                }
                return def;
            }
            const attributes_table* get_attributes_table()const{return tbl_;}

            size_t num_iterate()const{return i_;}
            size_t index()const{return i_;}
        private:
            attributes_table* tbl_;
            size_t i_;
            size_t vsz_;
        };
    public:
        vertex_iterator begin_vertex_attributes(size_t vsz)
        {
            return vertex_iterator(this, 0, vsz);
        }
        vertex_iterator end_vertex_attributes(size_t vsz)
        {
            return vertex_iterator(this, vsz, vsz);
        }
    public:
        attributes_table()
        {}
    public:
        const attributes_item* get(const std::string& key)const
        {
            map_type::const_iterator it = m_.find(key);
            if(it != m_.end())
            {
                return it->second.get();
            }
            return NULL;
        }
        template<class T>
        T get(const std::string& key, const T& def = T())const
        {
            const attributes_item* item = this->get(key);
            if(item)
            {
                return item->as<T>(0, def);
            }
            return def;
        }
    public:
        void set(const std::string& key, int v)
        {
            m_[key] = std::shared_ptr<attributes_item> ( new attributes_item(v) );
        }
        void set(const std::string& key, float v)
        {
            m_[key] = std::shared_ptr<attributes_item> ( new attributes_item(v) );
        }
        void set(const std::string& key, double v)
        {
            m_[key] = std::shared_ptr<attributes_item> ( new attributes_item(v) );
        }
        void set(const std::string& key, const std::string& v)
        {
            m_[key] = std::shared_ptr<attributes_item> ( new attributes_item(v) );
        }
        void set(const std::string& key, const vector<float, 3>& v)
        {
            m_[key] = std::shared_ptr<attributes_item> ( new attributes_item(v) );
        }
        void set(const std::string& key, const vector<double, 3>& v)
        {
            m_[key] = std::shared_ptr<attributes_item> ( new attributes_item(v) );
        }
        void set(const std::string& key, const std::vector< int >& vec)
        {
            m_[key] = std::shared_ptr<attributes_item> ( new attributes_item(vec) );
        }
        void set(const std::string& key, const std::vector< float >& vec)
        {
            m_[key] = std::shared_ptr<attributes_item> ( new attributes_item(vec) );
        }
        void set(const std::string& key, const std::vector< double >& vec)
        {
            m_[key] = std::shared_ptr<attributes_item> ( new attributes_item(vec) );
        }
        void set(const std::string& key, const std::vector< std::string >& vec)
        {
            m_[key] = std::shared_ptr<attributes_item> ( new attributes_item(vec) );
        }
        void set(const std::string& key, const std::vector< vector<float, 3> >& vec)
        {
            m_[key] = std::shared_ptr<attributes_item> ( new attributes_item(vec) );
        }
        void set(const std::string& key, const std::vector< vector<double, 3> >& vec)
        {
            m_[key] = std::shared_ptr<attributes_item> ( new attributes_item(vec) );
        }
    public:
        void update(const vertex_iterator& it)
        {
            size_t index = it.index();
            const attributes_table* other = it.get_attributes_table();
            for(map_type::const_iterator it = other->m_.begin(); it != other->m_.end(); it++)
            {
                const std::string& key = it->first;
                m_[key] = it->second->clone_at(index);
            }
        }
    public:
        std::map<std::string, std::shared_ptr<attributes_item> > m_;
    };

    typedef attributes_table attributes;

    inline bool operator==(const attributes::vertex_iterator& a, const attributes::vertex_iterator& b)
    {
        return a.equal(b);
    }
    inline bool operator!=(const attributes::vertex_iterator& a, const attributes::vertex_iterator& b)
    {
        return !a.equal(b);
    }

}

#endif
