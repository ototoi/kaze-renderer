#pragma once
#ifndef VOL_VECTOR_MATH_HPP
#define VOL_VECTOR_MATH_HPP

#include "vector.hpp"
#include <algorithm>

namespace vol
{
    template<class T>
    inline T pi()
    {
        return T(3.14159265358979323846);
    }

    template<class T, size_t Sz>
    vector<T, Sz> fabs(const vector<T, Sz>& rhs)
    {
        vector<T, Sz> tmp;
        for(size_t i = 0; i < Sz; i++)
        {
            tmp[i] = std::fabs(rhs[i]);
        }
        return tmp;
    }

    template<class T, size_t Sz>
    vector<T, Sz> floor(const vector<T, Sz>& rhs)
    {
        vector<T, Sz> tmp;
        for(size_t i = 0; i < Sz; i++)
        {
            tmp[i] = std::floor(rhs[i]);
        }
        return tmp;
    }
    template<class T, size_t Sz>
    vector<T, Sz> ceil(const vector<T, Sz>& rhs)
    {
        vector<T, Sz> tmp;
        for(size_t i = 0; i < Sz; i++)
        {
            tmp[i] = std::ceil(rhs[i]);
        }
        return tmp;
    }

    template<class T, size_t Sz>
    T min(const vector<T, Sz>& rhs)
    {
        return *std::min_element(rhs.begin(), rhs.end());
    }

    template<class T, size_t Sz>
    T max(const vector<T, Sz>& rhs)
    {
        return *std::max_element(rhs.begin(), rhs.end());
    }

    template<class T, size_t Sz>
    vector<T, Sz> clamp(const vector<T, Sz>& rhs, const T a, const T b)
    {
        vector<T, Sz> tmp;
        for(size_t i = 0; i < Sz; i++)
        {
            tmp[i] = std::max(a, std::min(rhs[i],b));
        }
        return tmp;
    }

    template<class T, size_t Sz>
    vector<T, Sz> clamp(const vector<T, Sz>& rhs, const vector<T, Sz>& a, const vector<T, Sz>& b)
    {
        vector<T, Sz> tmp;
        for(size_t i = 0; i < Sz; i++)
        {
            tmp[i] = std::max(a[i], std::min(rhs[i], b[i]));
        }
        return tmp;
    }


}

#endif