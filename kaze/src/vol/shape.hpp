#pragma once
#ifndef VOL_SHAPE_HPP
#define VOL_SHAPE_HPP

#include <vector>
#include <memory>

#include "vector.hpp"
#include "interval_detector.hpp"
#include "oobb_interval_detector.hpp"
#include "sphere_interval_detector.hpp"
#include "grid_interval_detector.hpp"

namespace vol
{
    template<class T>
    class shape : public interval_detector<T>
    {
    public:
        virtual ~shape(){}
        virtual void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const = 0;
    };

    template<class T>
    class sphere_shape : public shape<T>
    {
    public:
        typedef T value_type;
        typedef matrix<T, 4, 4> matrix_type;
        typedef vector<T, 3> vector_type;
    public:
        sphere_shape(const vector_type& center, const T radius)
        {
            detector_.reset( new sphere_interval_detector<T>(center, radius) );
        }
    public:
        virtual void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const
        {
            detector_->detect(out_intervals, rays, in_intervals);
        }
    protected:
        std::unique_ptr< sphere_interval_detector<T> > detector_;
    };

    template<class T>
    class box_shape : public shape<T>
    {
    public:
        typedef T value_type;
        typedef matrix<T, 4, 4> matrix_type;
        typedef vector<T, 3> vector_type;
    public:
        box_shape(const vector_type& min, const vector_type& max, const matrix_type& mat)
        {
            typedef matrix_generator<T, 4, 4> mat4_gen;
            vector_type width = max - min;
            vector_type center = T(0.5) * (max + min);
            matrix_type m = mat * mat4_gen::translation(center[0], center[1], center[2]);
            vol::oobb<T> box(width, m);
            detector_.reset( new oobb_interval_detector<T>(box) );
        }
    public:
        virtual void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const
        {
            detector_->detect(out_intervals, rays, in_intervals);
        }
    protected:
        std::unique_ptr< oobb_interval_detector<T> > detector_;
    };

    template<class T>
    class grid_shape : public shape<T>
    {
    public:
        typedef T value_type;
        typedef matrix<T, 4, 4> matrix_type;
        typedef vector<T, 3> vector_type;
    public:
        grid_shape(const vector_type& min, const vector_type& max, const matrix_type& mat, const int div[3])
        {
            typedef matrix_generator<T, 4, 4> mat4_gen;
            vector_type width = max - min;
            vector_type center = T(0.5) * (max + min);
            matrix_type m = mat * mat4_gen::translation(center[0], center[1], center[2]);
            vol::grid<T> box(width, m, div);
            detector_.reset( new grid_interval_detector<T>(box) );
        }

        grid_shape(const grid<T>& box)
        {
            detector_.reset( new grid_interval_detector<T>(box) );
        }
    public:
        virtual void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const
        {
            detector_->detect(out_intervals, rays, in_intervals);
        }
    protected:
        std::unique_ptr< grid_interval_detector<T> > detector_;
    };

    template<class T>
    class shape_builder
    {
    public:
        typedef T value_type;
        typedef matrix<T, 4, 4> matrix_type;
        typedef vector<T, 3> vector_type;
    public:
        std::shared_ptr< shape<T> > build()
        {
            return std::shared_ptr< shape<T> >( new sphere_shape<T>(vector_type(0,0,0), T(1)));
        }
        std::shared_ptr< shape<T> > build_sphere(const vector_type& center, const T radius)
        {
            return std::shared_ptr< shape<T> >( new sphere_shape<T>(center, radius));
        }
        std::shared_ptr< shape<T> > build_box(const vector_type& min, const vector_type& max, const matrix_type& mat)
        {
            return std::shared_ptr< shape<T> >( new box_shape<T>(min, max, mat));
        }
        std::shared_ptr< shape<T> > build_grid(const grid<T>& box)
        {
            return std::shared_ptr< shape<T> >( new grid_shape<T>(box) );
        }
    };
}

#endif
