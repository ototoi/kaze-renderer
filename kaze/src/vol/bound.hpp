#pragma once
#ifndef VOL_BOUND_H
#define VOL_BOUND_H

#include "vector.hpp"

namespace vol
{
    template<class T>
    class bound
    {
    public:
        typedef vector<T, 3> vector_type;
    public:
        bound(const vector_type& min, const vector_type& vector_type)
            :min_(min), max_(max)
        {}
        const vector_type& get_min()const{return min_;}
        const vector_type& get_max()const{return max_;}
    private:
        vector_type min_;
        vector_type max_;
    };
}

#endif
