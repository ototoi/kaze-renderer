#pragma once
#ifndef VOL_GRID_INTERVAL_DETECTOR_CPU_HPP
#define VOL_GRID_INTERVAL_DETECTOR_CPU_HPP

#include <vector>
#include <algorithm>

#include "vector.hpp"
#include "matrix.hpp"
#include "oobb.hpp"
#include "grid.hpp"
#include "interval_detector.hpp"

namespace vol
{
    template< class T >
    class grid_interval_detector_CPU
        : public interval_detector<T>
    {
    public:
        typedef vector<T, 3> vector_type;
        typedef matrix<T, 4, 4> matrix_type;
        typedef grid<T> this_type;
    public:
        grid_interval_detector_CPU(const grid<T>& box)
            :box_(box)
        {
            ;//
        }
    public:
        virtual void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const
        {
            assert(rays.size() == out_intervals.size());
            assert(rays.size() == in_intervals.size());
            size_t sz = rays.size();
            for(size_t i = 0; i < sz; i++)
            {
                detect(out_intervals[i], box_, rays[i], in_intervals[i]);
            }
        }
    protected:
        static inline void detect
        (
                  intervals<T>& out_intervals,
            const grid<T>& box,
            const ray<T>& r,
            const intervals<T>& in_intervals
        )
        {
            typedef typename grid<T>::vector_type vector_type;
            typedef typename grid<T>::matrix_type matrix_type;
            const matrix_type& imat = box.get_inversed_matrix();
            vector_type org = mul_p(imat, vector_type(r.org()));
            vector_type dir = mul_v(imat, vector_type(r.dir()));
            vector_type width = T(0.5) * box.get_extent();
            vector_type min = -width;
            vector_type max = +width;
            const int* div = box.get_divisions();
            
            T rng[2];
            if(aabb_intersect(rng, aabb<T>(min, max), ray<T>(&org[0], &dir[0]), in_intervals[0][0], in_intervals[0][1]))
            {
                out_intervals.add(in_intervals[0]);
                out_intervals.add(interval<T>(rng[0], rng[1]));

                assert(rng[0] < rng[1]);
                //if(rng[0] > rng[1])std::swap(rng[0], rng[1]);
                std::vector<T> dists;

                vector_type p0 = org + rng[0] * dir;
                vector_type n0 = ((max - p0) / (max - min)) * vector_type(div[0], div[1], div[2]);
                vector_type p1 = org + rng[1] * dir;
                vector_type n1 = ((max - p1) / (max - min)) * vector_type(div[0], div[1], div[2]);

                vector_type delta = (max - min) / vector_type(div[0], div[1], div[2]);
                int i0[3] = {0,0,0};
                int i1[3] = {div[0], div[1], div[2]};
                int dd[3] = {1,1,1};
            
                for(int j=0; j<3; j++)
                {
                    if(0 < dir[j])
                    {
                        i0[j] = std::min(std::max(0, (int)ceil (n0[j])), div[j]);
                        i1[j] = std::min(std::max(0, (int)floor(n1[j])), div[j]);
                        dd[j] = 1;
                        
                        if(!(i0[j] <= i1[j]))
                        {
                            dd[j] = 0;
                        }

                    }
                    else if(0 > dir[j])
                    {
                        i0[j] = std::min(std::max(0, (int)floor(n0[j])), div[j]);
                        i1[j] = std::min(std::max(0, (int)ceil (n1[j])), div[j]);
                        dd[j] = -1;
                        if(!(i0[j] >= i1[j]))
                        {
                            dd[j] = 0;
                        }
                    }
                    else
                    {
                        dd[j] = 0;
                    }
                }

                for(int j=0; j<3; j++)
                {
                    if( dd[j] != 0 )
                    {
                        for(int i = i0[j]; i != i1[j]; i+= dd[j])
                        {
                            float pj = min[j] + i * delta[j];
                            float t =  (pj - org[j]) / dir[j];//pj = org[j] + t * dir[j]
                            if(rng[0] < t && t < rng[1])
                            {
                                dists.push_back(t);
                            }
                        }
                    }
                }

                if(!dists.empty())
                {
                    std::sort(dists.begin(), dists.end());
                    dists.erase(std::unique(dists.begin(), dists.end()), dists.end());
                    for(int i = 0; i < (int)dists.size()-1; i++)
                    {
                        out_intervals.add(interval<T>(dists[i], dists[i+1]));
                    }
                }
            }
            else
            {
                out_intervals.add(in_intervals[0]);                                        
            }
        }

        static inline vector<float, 3> mul_v(const matrix<float, 4, 4>& m, const vector<float, 3>& v)
        {
            return vector<float, 3>(
                m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2],
                m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2],
                m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2]);
        }
        static inline vector<float, 3> mul_p(const matrix<float, 4, 4>& m, const vector<float, 3>& v)
        {
            float iR = float(1) / (m[3][0] * v[0] + m[3][1] * v[1] + m[3][2] * v[2] + m[3][3]);
            return vector<float, 3>(
                (m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2] + m[0][3]) * iR,
                (m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2] + m[1][3]) * iR,
                (m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2] + m[2][3]) * iR);
        }

        static
        inline T clamp(T x)
        {
            return std::max(T(0), std::min(x, T(1)));
        }

        static
        inline vector_type clamp(const vector_type& c)
        {
            return vector_type(clamp(c[0]), clamp(c[1]), clamp(c[2]));
        }
    protected:
        grid<T> box_;
    };
}

#endif