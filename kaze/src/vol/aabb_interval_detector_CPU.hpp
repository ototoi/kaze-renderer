#ifndef VOL_AABB_INTERVAL_DETECTOR_CPU_HPP
#define VOL_AABB_INTERVAL_DETECTOR_CPU_HPP

#include <vector>
#include <list>
#include <iostream>
#include <sstream>
#include <cassert>

#include "vector.hpp"
#include "matrix.hpp"
#include "image.hpp"
#include "aabb.hpp"
#include "box_intersection.hpp"

namespace vol
{
    template<class T>
    class aabb_interval_detector_CPU : public interval_detector<T>
    {
    public:
        explicit aabb_interval_detector_CPU(const aabb<T>& box)
            :box_(box)
        {}
    public:
        inline void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const
        {
            assert(rays.size() == out_intervals.size());
            assert(rays.size() == in_intervals.size());
            
            size_t sz = rays.size();
            for(size_t i = 0; i < sz; i++)
            {
                detect(out_intervals[i], box_, rays[i], in_intervals[i]);
            }
        }
    protected:
        static inline void detect
        (
                  intervals<T>& out_intervals,
            const aabb<T>& box,
            const ray<T>& ray,
            const intervals<T>& in_intervals
        )
        {
            T rng[2];
            if(aabb_intersect(rng, box, ray, in_intervals[0][0], in_intervals[0][1]))
            {
                out_intervals.add(in_intervals[0]);
                out_intervals.add(interval<T>(rng[0], rng[1]));
            }
            else
            {
                out_intervals.add(in_intervals[0]);                                        
            }
        }
    protected:
        aabb<T> box_;
    };
}

#endif
