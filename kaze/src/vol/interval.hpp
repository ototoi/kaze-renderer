#pragma once
#ifndef VOL_INTERVAL_HPP
#define VOL_INTERVAL_HPP

#include <vector>
#include <list>
#include <iostream>
#include <sstream>

namespace vol
{
    template <class T>
    class interval
    {
    public:
        interval()
        {
            t_[0] = T(0);
            t_[1] = T(0);
        }
        interval(T a, T b)
        {
            t_[0] = a;
            t_[1] = b;
        }
        T& operator[](int i)     {return t_[i];}
        const T& operator[](int i)const{return t_[i];}
        T& front(){return t_[0];}
        T& back(){return t_[1];}
        const T& front()const{return t_[0];}
        const T& back()const{return t_[1];}
    protected:
        T t_[2];
    };

    template<class T>
    class intervals
    {
    public:
        typedef interval<T> value_type;
        typedef intervals<T> this_type;
    public:
        intervals(){} 
        intervals(const T a, const T b)
        {
            ranges_.push_back(value_type(a, b));
        }
        intervals(const value_type& rng)
        {
            ranges_.push_back(rng);
        }
        intervals(const this_type& rhs)
        {
            ranges_.assign(rhs.ranges_.begin(), rhs.ranges_.end());
        }
        bool equals(const this_type& rhs)const
        {
            return ranges_ == rhs.ranges_;
        }
        this_type& add(const T a, const T b)
        {
            return add(value_type(a, b));
        }
        this_type& add(const this_type& rhs)
        {
            ranges_.insert(ranges_.end(), rhs.ranges_.begin(), rhs.ranges_.end());
            return *this;
        }
        this_type& operator+=(const this_type& rhs)
        {
            return this->add(rhs);
        }
        bool empty()const
        {
            return ranges_.empty();
        }
        size_t size()const
        {
            return ranges_.size();
        }
        value_type& operator[](size_t i)
        {
            return ranges_[i];
        }
        const value_type& operator[](size_t i)const
        {
            return ranges_[i];
        }
        value_type& front()
        {
            return ranges_.front();
        }
        const value_type& front()const
        {
            return ranges_.front();
        }
        value_type& back()
        {
            return ranges_.back();
        }
        const value_type& back()const
        {
            return ranges_.back();
        }
    public:
        this_type& flatten()
        {
            if(!ranges_.empty())
            {
                std::vector<T> values(ranges_.size()*2);
                for(size_t i = 0;i<ranges_.size();i++)
                {
                    values[2*i+0] = ranges_[i].front();
                    values[2*i+1] = ranges_[i].back();
                }
                std::sort(values.begin(), values.end());
                values.erase(std::unique(values.begin(), values.end()), values.end());
                std::vector<value_type> rvals(values.size()-1);
                for(size_t i = 0;i<values.size()-1;i++)
                {
                    rvals[i] = value_type(values[i], values[i+1]);
                }
                ranges_.swap(rvals);
            }
            return *this;
        }
    protected:
        std::vector<value_type> ranges_; 
    };

    template<class T>
    bool operator==(const intervals<T>& lhs, const intervals<T>& rhs)
    {
        return lhs.equals(rhs);
    }

    template <typename T, typename _CharT, class _Traits>
    std::basic_ostream<_CharT, _Traits>& operator<<(std::basic_ostream<_CharT, _Traits>& os, const intervals<T>& rhs)
    {
        std::basic_ostringstream<_CharT, _Traits> s;
        s.flags(os.flags());
        s.imbue(os.getloc());
        s.precision(os.precision());
        for (size_t i = 0; i < rhs.size(); i++)
        {
            s <<  "(" << rhs[i].front() << ", " << rhs[i].back() << ")";
            if (i != rhs.size() - 1)
            {
                s << ",";
            }
        }
        return os << s.str();
    }
}

#endif
