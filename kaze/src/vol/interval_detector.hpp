#pragma once
#ifndef VOL_INTERVAL_DETECTOR_HPP
#define VOL_INTERVAL_DETECTOR_HPP

#include <vector>

#include "ray.hpp"
#include "interval.hpp"
#include "image.hpp"

namespace vol
{
    template<class T>
    class interval_detector
    {
    public:
        virtual ~interval_detector(){}
        virtual void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const = 0;
    };
}

#endif
