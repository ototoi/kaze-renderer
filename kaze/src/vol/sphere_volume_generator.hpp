#pragma once
#ifndef VOL_SPHERE_VOLUME_GENERATOR_HPP
#define VOL_SPHERE_VOLUME_GENERATOR_HPP

#include <vector>
#include <memory>

#include "vector.hpp"
#include "matrix.hpp"
#include "oobb.hpp"
#include "volume.hpp"
#include "voxel_volume.hpp"
#include "attributes.hpp"
#include "parameterized_volume.hpp"
#include "vector_math.hpp"
#include "bound.hpp"

namespace vol
{
    template<class T>
    struct rasterization_state
    {
        typedef vector<T, 3> vector_type;
        vector_type szP;
        vector_type wsVoxelSize;
    };

    template<class T>
    struct rasterization_sample
    {
        typedef vector<X, 3> vector_type;
        T value;
        vector_type wsVelosity;
    };

    template<class T>
    class sphere_volume_generator
    {
    public:
        typedef vector<T,3> vector_type;
        typedef vector_type Vector;
    public:
        virtual void generate(
            std::shared_ptr< parameterized_volume<T> >& vox,
            const attributes& global_data,
            const attributes::vertex_iterator& v0, const attributes::vertex_iterator& v1
        )
        const
        {
            const T dTime = global_data.get<T>("dTime", T(0));

            attributes pt;
            pt.set("P", vector_type(0,0,0));
            pt.set("v", vector_type(0,0,0));
            pt.set("radius", T(0.1));
            pt.set("density", T(1));
            pt.set("antialiased", 1);

            for(attributes::vertex_iterator it = v0; it != v1; ++it)
            {
                pt.update(it);
                const T density  = pt.get<T>("density");
                const T wsRadius = pt.get<T>("radius");
                const Vector wsCenter = pt.get<Vector>("P");
                const Vector wsVelocity = pt.get<Vector>("v");
                const int antialiased = pt.get<int>("antialiased");

                Vector wsVoxelSize = vox->get_voxel_size(wsCenter);//
                T relSize = wsRadius / vol::min(wsVoxelSize);
                if(relSize > 1)
                {
                    //write as a voxel density!
                    T filterWidth = wsVoxelSize.length();
                    vol::bound<T> vsBound = getSphereBound(*vox, wsCenter, wsRadius + filterWidth * T(0.5)); //calc a bounds in local voxel space.//indices ? 
                    //rasterize(*vox, vsBound, density);
                }
                else
                {
                    //write as a point or a line.
                    volume<T>& vol = *vox->get_density_buffer();
                    T voxelVolume  = std::fabs(wsVoxelSize[0] * wsVoxelSize[1] * wsVoxelSize[2]);
                    //std::cout << wsVoxelSize << std::endl;
                    T voxelDensity = std::min(density / voxelVolume * sphereVolume(wsRadius), T(1));
                    if(wsVelocity.length() <= T(0))
                    {
                        Vector vsP = VoxelSpace(vol, vox->sample(wsCenter));
                        if(antialiased)
                        {
                            write_point_AA(vol, vsP, voxelDensity);
                        }
                        else
                        {
                            write_point   (vol, vsP, voxelDensity);
                        }
                    }
                    else
                    {
                        Vector wsEnd = wsCenter + wsVelocity * dTime;
                        Vector vsP0 = VoxelSpace(vol, vox->sample(wsCenter));
                        Vector vsP1 = VoxelSpace(vol, vox->sample(wsEnd));
                        if(antialiased)
                        {
                            write_line_AA(vol, vsP0, vsP1, voxelDensity);
                        }
                        else
                        {
                            write_line   (vol, vsP0, vsP1, voxelDensity);
                        }
                    }
                }
            }
        }
    public:
        static bound getSphereBound(const parameterized_volume<T>& vox, const Vector& wsCenter, const T r)
        {
            Vector points[8] = 
            {
                VoxelSpace(vol, vox->sample(wsCenter + wsCenter(-r, -r, -r))),
                VoxelSpace(vol, vox->sample(wsCenter + wsCenter(-r, -r, +r))),
                VoxelSpace(vol, vox->sample(wsCenter + wsCenter(-r, +r, -r))),
                VoxelSpace(vol, vox->sample(wsCenter + wsCenter(-r, +r, +r))),
                VoxelSpace(vol, vox->sample(wsCenter + wsCenter(+r, -r, -r))),
                VoxelSpace(vol, vox->sample(wsCenter + wsCenter(+r, -r, +r))),
                VoxelSpace(vol, vox->sample(wsCenter + wsCenter(+r, +r, -r))),
                VoxelSpace(vol, vox->sample(wsCenter + wsCenter(+r, +r, +r)))
            };
            Vector min = VoxelSpace(vol, vox->sample(wsCenter));
            Vector max = min;
            for(int i = 0; i < 8; i++)
            {
                for(int j = 0; j < 3; j++)
                {
                    min[j] = std::min(min[j], points[i][j]);
                    max[j] = std::max(max[j], points[i][j]);
                }
            }
            return bound(min, max);
        }
        static void rasterize(volume<T>& vox, const bound& bnd, const T density)
        {
            
        }
    protected:
        static inline Vector VoxelSpace(const volume<T>& vol, const Vector& p)
        {
            int w = vol.get_width();
            int h = vol.get_height();
            int d = vol.get_depth();
            return p * Vector(w,h,d);
        }
        static inline T sphereVolume(const T r)
        {
            return T(4.0 / 3.0) * vol::pi<T>() *r*r*r;
        }
    };

    template<class T, class X>
    inline void write_point_AA(volume<T>& vol, const vector<X, 3>& p, const T value)
    {
        typedef vector<X,3>   Vf;

        int w = vol.get_width();
        int h = vol.get_height();
        int d = vol.get_depth();

        Vf pp = p - Vf(0.5, 0.5, 0.5);
        Vf pi = vol::floor(pp);

        Vf uvw1 = pp - pi;
        Vf uvw0 = Vf(1,1,1) - uvw1;

        int x0 = (int)pi[0];
        int y0 = (int)pi[1];
        int z0 = (int)pi[2];
        int x1 = x0+1;
        int y1 = y0+1;
        int z1 = z0+1;

        if(0 <= x0 && x0 <= w-1)
        {
            if(0 <= y0 && y0 <= h-1)
            {
                if(0 <= z0 && z0 <= d-1)
                {
                    vol.at(x0, y0, z0) += value * (uvw0[0] * uvw0[1] * uvw0[2]);
                }
                if(0 <= z1 && z1 <= d-1)
                {
                    vol.at(x0, y0, z1) += value * (uvw0[0] * uvw0[1] * uvw1[2]);
                }
            }
            if(0 <= y1 && y1 <= h-1)
            {
                if(0 <= z0 && z0 <= d-1)
                {
                    vol.at(x0, y1, z0) += value * (uvw0[0] * uvw1[1] * uvw0[2]);
                }
                if(0 <= z1 && z1 <= d-1)
                {
                    vol.at(x0, y1, z1) += value * (uvw0[0] * uvw1[1] * uvw1[2]);
                }
            }
        }

        if(0 <= x1 && x1 <= w-1)
        {
            if(0 <= y0 && y0 <= h-1)
            {
                if(0 <= z0 && z0 <= d-1)
                {
                    vol.at(x1, y0, z0) += value * (uvw1[0] * uvw0[1] * uvw0[2]);
                }
                if(0 <= z1 && z1 <= d-1)
                {
                    vol.at(x1, y0, z1) += value * (uvw1[0] * uvw0[1] * uvw1[2]);
                }
            }
            if(0 <= y1 && y1 <= h-1)
            {
                if(0 <= z0 && z0 <= d-1)
                {
                    vol.at(x1, y1, z0) += value * (uvw1[0] * uvw1[1] * uvw0[2]);
                }
                if(0 <= z1 && z1 <= d-1)
                {
                    vol.at(x1, y1, z1) += value * (uvw1[0] * uvw1[1] * uvw1[2]);
                }
            }
        }
    }

    template<class T, class X>
    inline void write_point  (volume<T>& vol, const vector<X, 3>& p, const T value)
    {
        typedef vector<X,3>   Vf;

        int w = vol.get_width();
        int h = vol.get_height();
        int d = vol.get_depth();

        int x = (int)std::floor(p[0]);
        int y = (int)std::floor(p[1]);
        int z = (int)std::floor(p[2]);
        if(x < 0 || w <= x)return;
        if(y < 0 || h <= y)return;
        if(z < 0 || d <= z)return;

        vol.at(x, y, z) += value;
    }

    template<class T, class X>
    inline void write_line_AA(volume<T>& vol, const vector<X, 3>& a, const vector<X, 3>& b, const T value)
    {
        typedef vector<T,3>   Vf;

        int w = vol.get_width();
        int h = vol.get_height();
        int d = vol.get_depth();

        Vf vsLine = b - a;
        size_t numSamples = std::max<size_t>(2, (size_t)std::ceil(vsLine.length()*T(1.01)));
        X sampleValue = value / X(numSamples);
        for(size_t i = 0; i < numSamples; i++)
        {
            X frac = X(i) /(numSamples-1);
            Vf vsP = a + frac * vsLine;
            write_point_AA(vol, vsP, sampleValue);
        }
    }

    template<class T, class X>
    inline void write_line  (volume<T>& vol, const vector<X, 3>& a, const vector<X, 3>& b, const T value)
    {
        typedef vector<T,3>   Vf;

        int w = vol.get_width();
        int h = vol.get_height();
        int d = vol.get_depth();

        Vf vsLine = b - a;
        size_t numSamples = std::max<size_t>(2, (size_t)std::ceil(vsLine.length()*T(1.01)));
        X sampleValue = value / X(numSamples);
        for(size_t i = 0; i < numSamples; i++)
        {
            X frac = X(i) /(numSamples-1);
            Vf vsP = a + frac * vsLine;
            write_point(vol, vsP, sampleValue);
        }
    }

}

#endif
