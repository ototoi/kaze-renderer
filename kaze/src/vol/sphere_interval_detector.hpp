#pragma once
#ifndef VOL_SPHERE_INTERVAL_DETECTOR_HPP
#define VOL_SPHERE_INTERVAL_DETECTOR_HPP

#include <vector>

#include "vector.hpp"
#include "matrix.hpp"
#include "oobb.hpp"
#include "interval_detector.hpp"

namespace vol
{
    template<class T>
    class sphere_interval_detector 
        : public interval_detector<T>
    {
    public:
        typedef vector<T, 3> vector_type;
    public:
        sphere_interval_detector(const vector_type& center, const T radius)
            :center_(center), radius_(radius)
        {}
    public:
        virtual void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const
        {
            assert(rays.size() == out_intervals.size());
            assert(rays.size() == in_intervals.size());
            
            size_t sz = rays.size();
            for(size_t i = 0; i < sz; i++)
            {
                detect(out_intervals[i], center_, radius_, rays[i], in_intervals[i]);
            }
        }
    protected:
        static inline void detect
        (
                  intervals<T>& out_intervals,
            const vector_type& center, const T radius,
            const ray<T>& ray,
            const intervals<T>& in_intervals
        )
        {
            T rng[2];
            if(sphere_intersect(rng, center, radius, ray, in_intervals[0][0], in_intervals[0][1]))
            {
                out_intervals.add(in_intervals[0]);
                out_intervals.add(interval<T>(rng[0], rng[1]));
            }
            else
            {
                out_intervals.add(in_intervals[0]);                                        
            }
        }
    protected:
        static inline bool sphere_intersect(T rng[2], const vector_type& center, const T radius, const ray<T>& r, const T t0, const T t1)
        {
            vector_type org(r.org());
            vector_type dir(r.dir());
            vector_type rs = org - center;

            T B = dot(rs, dir);
            T C = dot(rs, rs) - radius * radius;
            T D = B * B - C;

            if (D > T(0))
            {
                T tt0 = -B - std::sqrt(D);
                T tt1 = -B + std::sqrt(D);
                rng[0] = t0;
                rng[1] = t1;
                if ((t0 < tt0) && (tt0 < t1))
                {
                    rng[0] = tt0;
                }
                if ((t0 < tt1) && (tt1 < t1))
                {
                    rng[1] = tt1;
                }
                return true;
            }
            return false;
        }
    protected:
        vector_type center_;
        T radius_;
    };
}

#endif
