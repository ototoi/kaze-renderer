#ifndef VOL_OOBB_INTERVAL_DETECTOR_GL_HPP
#define VOL_OOBB_INTERVAL_DETECTOR_GL_HPP

#include <vector>
#include <list>
#include <iostream>
#include <sstream>
#include <cassert>

#include "vector.hpp"
#include "matrix.hpp"
#include "oobb.hpp"
#include "image.hpp"

#ifdef VOL_USE_GL
#include <gl/gl_shader.h>
#include <gl/gl_program.h>
#include <gl/gl_texture.h>
#include <gl/gl_uniform_buffer.h>
#include <gl/gl_frame_buffer.h>
#include <gl/gl_vertex_buffer.h>
#include <gl/gl_vertex_array.h>

namespace vol
{
    template<class T>
    class oobb_interval_detector_GL : public interval_detector<T>
    {
    public:
        explicit oobb_interval_detector_GL(const oobb<T>& box)
        {
            this->initialize();
        }
    public:
        static
        inline get_width_size(int w)
        {
            if(w <= 256)return 256;
            if(w <= 512)return 512;
            else        return 1024;
        }
        inline void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const
        {
            assert(rays.size() == out_intervals.size());
            assert(rays.size() == in_intervals.size());
            
            using namespace gl;

            int size    = (int)rays.size();
            int width   = get_width_size(size);
            int height  = (int)std::ceil((float)(size) / width);

            std::vector<float> interval_buffer(4*width*height, 0.0f);
            std::vector<float> org_buffer(4*width*height, 0.0f);
            std::vector<float> dir_buffer(4*width*height, 0.0f);
            convert_rays(org_buffer, dir_buffer, rays, in_intervals[0][0], in_intervals[0][1]);
            {
                //Lock GL
                std::unique_ptr<gl_frame_buffer> frame( new gl_frame_buffer() );
                std::unique_ptr<gl_texture_2D> interval( new gl_texture_2D() );
                std::unique_ptr<gl_texture_2D> org( new gl_texture_2D() );
                std::unique_ptr<gl_texture_2D> dir( new gl_texture_2D() );
                set_texture(interval.get(), &interval_buffer[0], width, height);
                set_texture(org.get(), &org_buffer[0], width, height);
                set_texture(dir.get(), &dir_buffer[0], width, height);

                frame->bind();
                frame->attach_color(interval->get_handle(), 0);
                frame->unbind();

                this->detect(frame.get(), org.get(), dir.get(), width, height);

                interval->bind();
                ::glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &interval_buffer[0]);
                interval->unbind();
                //Unlock GL
            }

            for(size_t i = 0; i < in_intervals.size(); i++)
            {
                out_intervals[i].add(in_intervals[i]);
                float f0 = interval_buffer[4*i+0];
                float f1 = interval_buffer[4*i+1];
                if(f0 > 0 && f1 > 0)
                {
                    out_intervals[i].add(interval<T>(T(f0), T(f1)));
                }
            }
        }

        inline void detect(
                  gl::gl_frame_buffer* frame,
            const gl::gl_texture* org,
            const gl::gl_texture* dir,
            int width,
            int height
        )const
        {
            frame->bind();
                ::glDisable(GL_DEPTH_TEST);
                ::glDepthMask(GL_FALSE);
                
                ::glViewport(0, 0, width, height);
                ::glClearColor(0, 0, 0, 1);
                ::glClear(GL_COLOR_BUFFER_BIT);

                ::glUseProgram(prog_->get_handle());
                    //this->SetUniformParameters();
                    ::glBindVertexArray(vao_->get_handle());
                        ::glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                    ::glBindVertexArray(0u);
                ::glUseProgram(0u);

                ::glFlush();
            frame->unbind();
        }
    protected:
        static inline void set_texture(gl::gl_texture_2D* tex, const float* f, int width, int height)
        {
            tex->set_image(GL_RGBA16F, width, height, GL_RGBA, GL_FLOAT, f);
            tex->bind();
            ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);  //not biliear
            ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);  //
            ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //TODO
            ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //TODO
            tex->unbind();
        }

        static inline void convert_rays(
            std::vector<float>& org_buffer,
            std::vector<float>& dir_buffer,
            const std::vector< ray<T> >& rays,
            const float t0,
            const float t1
        )
        {
            int sz = rays.size();
            for(size_t i = 0; i < sz; i++)
            {
                size_t index = i;
                const ray<T>& ray = rays[i];
                org_buffer[4*index+0] = (float)ray.org()[0];
                org_buffer[4*index+1] = (float)ray.org()[1];
                org_buffer[4*index+2] = (float)ray.org()[2];
                org_buffer[4*index+3] = (float)t0;
                dir_buffer[4*index+0] = (float)ray.dir()[0];
                dir_buffer[4*index+1] = (float)ray.dir()[1];
                dir_buffer[4*index+2] = (float)ray.dir()[2];
                dir_buffer[4*index+3] = (float)t1;
            }
        }
    protected:
        void initialize()
        {
            initialize_program();
            initialize_vertices();
        }

        void initialize_program()
        {
            static const char* szVS =
                "#version 410\n"
                "in vec3 pos;\n"
                "in vec2 uv;\n"
                "out vec3 fpos;\n"
                "out vec2 fuv;\n"
                "void main()\n"
                "{\n"
                "    fpos = pos;\n"
                "    fuv  = uv;\n"
                "    gl_Position = vec4(fpos,1);\n"
                "}\n";
            static const char* szFS = 
                "#version 410\n"
                "in vec3 fpos;\n"
                "in vec2 fuv;\n"
                "out vec4 col;\n"
                "uniform texture2D orgs;\n"
                "uniform texture2D dirs;\n"
                "void main()\n"
                "{\n"
                "    vec4 org_ = texture(orgs, fuv);\n"
                "    vec4 dir_ = texture(dirs, fuv);\n"
                "    vec3 org = org_.xyz;\n"
                "    vec3 dir = dir_.xyz;\n"
                "    float t0 = org_.w;\n"
                "    float t1 = dir_.w;\n"
                "    col = vec4(0);\n"
                "}\n";
        }

        void initialize_vertices()
        {
            static const float posData[] = {
                -1.0f, -1.0f, 0.0f, //0
                -1.0f, +1.0f, 0.0f, //1
                +1.0f, -1.0f, 0.0f, //2
                +1.0f, +1.0f, 0.0f, //3
            };
            static const float uvData[] = {
                0.0f, 0.0f,
                0.0f, 1.0f,
                1.0f, 0.0f,
                1.0f, 1.0f,
            };

            vbo_p_ = std::unique_ptr<gl::gl_vertex_buffer> (new gl::gl_vertex_buffer());
            vbo_c_ = std::unique_ptr<gl::gl_vertex_buffer> (new gl::gl_vertex_buffer());
            vao_   = std::unique_ptr<gl::gl_vertex_array>  (new gl::gl_vertex_array());
            {
                {
                    ::glBindBuffer(GL_ARRAY_BUFFER, vbo_p_->get_handle());
                    ::glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(float), posData, GL_STATIC_DRAW);
                }
                {
                    ::glBindBuffer(GL_ARRAY_BUFFER, vbo_c_->get_handle());
                    ::glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(float), uvData, GL_STATIC_DRAW);
                }
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                ::glBindVertexArray(vao_->get_handle());
                {
                    GLuint index = prog_->get_attrib_location("pos");
                    ::glBindBuffer(GL_ARRAY_BUFFER, vbo_p_->get_handle());
                    ::glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                {
                    GLuint index = prog_->get_attrib_location("uv");
                    ::glBindBuffer(GL_ARRAY_BUFFER, vbo_c_->get_handle());
                    ::glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
                ::glBindVertexArray(0u);
            }
        }
    protected:
        std::unique_ptr<gl::gl_program> prog_;
        std::unique_ptr<gl::gl_vertex_array> vao_;
        std::unique_ptr<gl::gl_vertex_buffer> vbo_p_;
        std::unique_ptr<gl::gl_vertex_buffer> vbo_c_;
        std::unique_ptr<gl::gl_uniform_buffer> oobb_;
    };
}

#else
namespace vol
{
    template<class T>
    class oobb_interval_detector_GL
    {
    public:
        oobb_interval_detector_GL(const oobb<T>& oobb)
        {}
    public:
        inline void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const
        {
            out_intervals = in_intervals;
        }
    };
}

#endif
#endif
