#pragma once
#ifndef VOL_OOBB_HPP
#define VOL_OOBB_HPP

#include "vector.hpp"
#include "matrix.hpp"

namespace vol
{
    template<class T>
    class oobb
    {
    public:
        typedef vector<T, 3> vector_type;
        typedef matrix<T, 4, 4> matrix_type;
        typedef oobb<T> this_type;
    public:
        oobb(const vector_type& extent, const matrix_type& world)
            :extent_(extent), world_(world)
        {
            iworld_ = ~world_;
        }
        const matrix_type& get_matrix()const{return world_;}
        const matrix_type& get_inversed_matrix()const{return iworld_;} 
        const vector_type& get_extent()const{return extent_;}
    protected:
        vector_type extent_;
        matrix_type world_;
        matrix_type iworld_;
    };

    //template<class T>
    //using OOBB = oobb<T>;
}

#endif
