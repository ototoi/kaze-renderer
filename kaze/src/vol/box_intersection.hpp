#pragma once
#ifndef VOL_BOX_INTERSECTION_HPP
#define VOL_BOX_INTERSECTION_HPP

#include "ray.hpp"
#include "aabb.hpp"
#include "oobb.hpp"

#include <limits>

namespace vol
{
    namespace box_intersection
    {
        namespace detail
        {
            template<class T>
            inline vector<T, 3> mul_v(const matrix<T, 4, 4>& m, const vector<T, 3>& v)
            {
                return vector<T, 3>(
                    m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2],
                    m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2],
                    m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2]);
            }

            template<class T>
            inline vector<T, 3> mul_p(const matrix<T, 4, 4>& m, const vector<T, 3>& v)
            {
                T iR = T(1) / (m[3][0] * v[0] + m[3][1] * v[1] + m[3][2] * v[2] + m[3][3]);
                return vector<T, 3>(
                    (m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2] + m[0][3]) * iR,
                    (m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2] + m[1][3]) * iR,
                    (m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2] + m[2][3]) * iR);
            }

            template<class T>
            inline vector<T, 3> inverse_vec3(const T x[3])
            {
                static const T EPSILON = std::numeric_limits<T>::epsilon();
                static const T INF = std::numeric_limits<T>::infinity();
                T tmp[3] = 
                {
                    (fabs(x[0]) > EPSILON) ? (T(1) / x[0]) : ((x[0] < 0) ? -INF : +INF),
                    (fabs(x[1]) > EPSILON) ? (T(1) / x[1]) : ((x[1] < 0) ? -INF : +INF),
                    (fabs(x[2]) > EPSILON) ? (T(1) / x[2]) : ((x[2] < 0) ? -INF : +INF)
                };
                return vector<T, 3>(tmp);
            }
        }
    }

    template<class T>
    inline bool aabb_intersect(T rng[2], const aabb<T>& aabb, const ray<T>& r, const T t0, const T t1)
    { 
        using namespace box_intersection::detail;
        typedef vector<T, 3> vector_type;

        int sign[3] = { 
            (r.dir()[0] < T(0)) ? 1 : 0,
            (r.dir()[1] < T(0)) ? 1 : 0,
            (r.dir()[2] < T(0)) ? 1 : 0
        };
        vector_type box[2] = {aabb.get_min(), aabb.get_max()};
        vector_type org(r.org());
        vector_type idir(inverse_vec3(r.dir()));

        T tmin = t0;
        T tmax = t1;
        for (int i = 0; i < 3; i++)
        {
            tmin = std::max<T>(tmin, (box[sign[i]][i] - org[i]) * idir[i]);
            tmax = std::min<T>(tmax, (box[1 - sign[i]][i] - org[i]) * idir[i]);
        }
        if(tmin <= tmax)
        {
            rng[0] = tmin;
            rng[1] = tmax;
            return true;
        }
        else
        {
            return false;
        }
    }

    template<class T>
    inline bool oobb_intersect(T rng[2], const oobb<T>& box, const ray<T>& r, const T t0, const T t1)
    { 
        using namespace box_intersection::detail;
        typedef typename oobb<T>::vector_type vector_type;
        typedef typename oobb<T>::matrix_type matrix_type;
        const matrix_type& imat = box.get_inversed_matrix();
        vector_type org = mul_p(imat, vector_type(r.org()));
        vector_type dir = mul_v(imat, vector_type(r.dir()));
        assert( fabs(length(dir)-T(1)) < T(1e-3) );
        vector_type width = T(0.5) * box.get_width();
        return aabb_intersect(rng, aabb<T>(-width, +width), ray<T>(&org[0], &dir[0]), t0, t1);
    }
}

#endif
