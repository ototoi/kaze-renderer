#pragma once
#ifndef VOL_BOX_INTERSECTION_SIMD_HPP
#define VOL_BOX_INTERSECTION_SIMD_HPP

#include "ray.hpp"
#include "aabb.hpp"
#include "oobb.hpp"
#include "SIMD.hpp"

#include <limits>

#ifdef VOL_USE_SIMD

namespace vol
{
    static inline void test_AABB(
        __m128& omin, 
        __m128& omax,
        const __m128 min[3],       //4boxes : min-max[2] of xyz[3] of boxes[4]
        const __m128 max[3],       //
        const __m128 org[3],       //ray origin
        const __m128 idir[3],      //ray inveresed direction
        __m128 tmin, __m128 tmax   //ray range tmin-tmax
        )
    {
        // x coordinate
        tmin = _mm_max_ps(
            tmin,
            _mm_mul_ps(_mm_sub_ps(min[0], org[0]), idir[0]));
        tmax = _mm_min_ps(
            tmax,
            _mm_mul_ps(_mm_sub_ps(max[0], org[0]), idir[0]));

        // y coordinate
        tmin = _mm_max_ps(
            tmin,
            _mm_mul_ps(_mm_sub_ps(min[1], org[1]), idir[1]));
        tmax = _mm_min_ps(
            tmax,
            _mm_mul_ps(_mm_sub_ps(max[1], org[1]), idir[1]));

        // z coordinate
        tmin = _mm_max_ps(
            tmin,
            _mm_mul_ps(_mm_sub_ps(min[2], org[2]), idir[2]));
        tmax = _mm_min_ps(
            tmax,
            _mm_mul_ps(_mm_sub_ps(max[2], org[2]), idir[2]));

        omin = tmin;
        omax = tmax;
    }

    inline void aabb_intersect_SIMD4(
              float* s0,       //size
              float* s1,       //size
        const float* min,      //3
        const float* max,      //3
        const float* org_x,    //size
        const float* org_y,    //size
        const float* org_z,    //size
        const float* idir_x,   //size
        const float* idir_y,   //size
        const float* idir_z,   //size
        const float* t0,       //size
        const float* t1,       //size
        size_t size
    )
    { 
        __m128 bboxes[2][3];
        for(int i = 0; i < 3; i++)
        {
            bboxes[0][i]  = _mm_set1_ps(min[i]);
            bboxes[1][i]  = _mm_set1_ps(max[i]);
        }

        size_t sz4 = size / 4;
        for(size_t i = 0; i < sz4; i++)
        {
            __m128 org[3], idir[3];
            org[0] = _mm_load_ps(org_x + 4*i);
            org[1] = _mm_load_ps(org_y + 4*i);
            org[2] = _mm_load_ps(org_z + 4*i);
            idir[0] = _mm_load_ps(idir_x + 4*i);
            idir[1] = _mm_load_ps(idir_y + 4*i);
            idir[2] = _mm_load_ps(idir_z + 4*i);
            __m128 tmin = _mm_load_ps(t0 + 4*i);
            __m128 tmax = _mm_load_ps(t1 + 4*i);

            alignas(32) float kmin[3][4] = {};
            alignas(32) float kmax[3][4] = {};
            for(int j = 0; j < 4; j++)
            {
                for(int k = 0; k < 3; k++)
                {
                    kmin[k][j] = min[k];
                    kmax[k][j] = max[k];
                }
                if(idir_x[4*i+j] < 0)
                {
                    std::swap(kmin[0][j], kmax[0][j]);
                }
                if(idir_y[4*i+j] < 0)
                {
                    std::swap(kmin[1][j], kmax[1][j]);
                }
                if(idir_z[4*i+j] < 0)
                {
                    std::swap(kmin[2][j], kmax[2][j]);
                }
            }
            __m128 mmin[3], mmax[3];
            for(int j = 0; j < 3; j++)
            {
                mmin[j] = _mm_load_ps(kmin[j]);
                mmax[j] = _mm_load_ps(kmax[j]);
            }
            __m128 ms0, ms1;
            test_AABB(ms0, ms1, mmin, mmax, org, idir, tmin, tmax);
            _mm_store_ps(s0 + 4*i, ms0);
            _mm_store_ps(s1 + 4*i, ms1);
        }
    }

    static inline void test_AABB(
        __m256& omin, 
        __m256& omax,
        const __m256 min[3],       //4boxes : min-max[2] of xyz[3] of boxes[4]
        const __m256 max[3],       //
        const __m256 org[3],       //ray origin
        const __m256 idir[3],      //ray inveresed direction
        __m256 tmin, __m256 tmax   //ray range tmin-tmax
        )
    {
        // x coordinate
        tmin = _mm256_max_ps(
            tmin,
            _mm256_mul_ps(_mm256_sub_ps(min[0], org[0]), idir[0]));
        tmax = _mm256_min_ps(
            tmax,
            _mm256_mul_ps(_mm256_sub_ps(max[0], org[0]), idir[0]));

        // y coordinate
        tmin = _mm256_max_ps(
            tmin,
            _mm256_mul_ps(_mm256_sub_ps(min[1], org[1]), idir[1]));
        tmax = _mm256_min_ps(
            tmax,
            _mm256_mul_ps(_mm256_sub_ps(max[1], org[1]), idir[1]));

        // z coordinate
        tmin = _mm256_max_ps(
            tmin,
            _mm256_mul_ps(_mm256_sub_ps(min[2], org[2]), idir[2]));
        tmax = _mm256_min_ps(
            tmax,
            _mm256_mul_ps(_mm256_sub_ps(max[2], org[2]), idir[2]));

        omin = tmin;
        omax = tmax;
    }

    inline void aabb_intersect_SIMD8(
              float* s0,       //size
              float* s1,       //size
        const float* min,      //3
        const float* max,      //3
        const float* org_x,    //size
        const float* org_y,    //size
        const float* org_z,    //size
        const float* idir_x,   //size
        const float* idir_y,   //size
        const float* idir_z,   //size
        const float* t0,       //size
        const float* t1,       //size
        size_t size
    )
    { 
        __m256 bboxes[2][3];
        for(int i = 0; i < 3; i++)
        {
            bboxes[0][i]  = _mm256_set1_ps(min[i]);
            bboxes[1][i]  = _mm256_set1_ps(max[i]);
        }

        size_t sz4 = size / 8;
        for(size_t i = 0; i < sz4; i++)
        {
            __m256 org[3], idir[3];
            org[0] = _mm256_load_ps(org_x + 8*i);
            org[1] = _mm256_load_ps(org_y + 8*i);
            org[2] = _mm256_load_ps(org_z + 8*i);
            idir[0] = _mm256_load_ps(idir_x + 8*i);
            idir[1] = _mm256_load_ps(idir_y + 8*i);
            idir[2] = _mm256_load_ps(idir_z + 8*i);
            __m256 tmin = _mm256_load_ps(t0 + 8*i);
            __m256 tmax = _mm256_load_ps(t1 + 8*i);

            alignas(32) float kmin[3][8] = {};
            alignas(32) float kmax[3][8] = {};
            for(int j = 0; j < 8; j++)
            {
                for(int k = 0; k < 3; k++)
                {
                    kmin[k][j] = min[k];
                    kmax[k][j] = max[k];
                }
                if(idir_x[8*i+j] < 0)
                {
                    std::swap(kmin[0][j], kmax[0][j]);
                }
                if(idir_y[8*i+j] < 0)
                {
                    std::swap(kmin[1][j], kmax[1][j]);
                }
                if(idir_z[8*i+j] < 0)
                {
                    std::swap(kmin[2][j], kmax[2][j]);
                }
            }
            __m256 mmin[3], mmax[3];
            for(int j = 0; j < 3; j++)
            {
                mmin[j] = _mm256_load_ps(kmin[j]);
                mmax[j] = _mm256_load_ps(kmax[j]);
            }
            __m256 ms0, ms1;
            test_AABB(ms0, ms1, mmin, mmax, org, idir, tmin, tmax);
            _mm256_store_ps(s0 + 8*i, ms0);
            _mm256_store_ps(s1 + 8*i, ms1);
        }
    }
}

#endif

#endif
