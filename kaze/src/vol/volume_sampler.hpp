#pragma once
#ifndef VOL_VOLUME_SAMPLER_HPP
#define VOL_VOLUME_SAMPLER_HPP

#include <cmath>

#include "volume.hpp"

namespace vol
{
    template <class T, class X>
    inline T sum_z(
        const X wz[4],
        const int x, const int y, const int dz[4],
        const volume<T>& vox)
    {
        return wz[0] * vox.get(x, y, dz[0]) +
               wz[1] * vox.get(x, y, dz[1]) +
               wz[2] * vox.get(x, y, dz[2]) +
               wz[3] * vox.get(x, y, dz[3]);
    }

    template <class T, class X>
    inline T sum_y(
        const X wy[4], const X wz[4],
        const int x, const int dy[4], const int dz[4],
        const volume<T>& vox)
    {
        return wy[0] * sum_z(wz, x, dy[0], dz, vox) +
               wy[1] * sum_z(wz, x, dy[1], dz, vox) +
               wy[2] * sum_z(wz, x, dy[2], dz, vox) +
               wy[3] * sum_z(wz, x, dy[3], dz, vox);
    }

    template <class T, class X>
    T sum_x(
        const X wx[4], const X wy[4], const X wz[4],
        const int dx[4], const int dy[4], const int dz[4],
        const volume<T>& vox)
    {
        return wx[0] * sum_y(wy, wz, dx[0], dy, dz, vox) +
               wx[1] * sum_y(wy, wz, dx[1], dy, dz, vox) +
               wx[2] * sum_y(wy, wz, dx[2], dy, dz, vox) +
               wx[3] * sum_y(wy, wz, dx[3], dy, dz, vox);
    }

    template <class T, class X, class F>
    T interp_y(const X y, const X z, const T p[4][4])
    {
        T tmp[4] = {
            F::static_get(z,p[0]),
            F::static_get(z,p[1]),
            F::static_get(z,p[2]),
            F::static_get(z,p[3])
        };
        return F::static_get(y, tmp);
    }

    template <class T, class X, class F>
    T interp_x(const X x, const X y, const X z, const T p[4][4][4])
    {
        T tmp[4] = {
            interp_y<T,X,F>(y,z,p[0]),
            interp_y<T,X,F>(y,z,p[1]),
            interp_y<T,X,F>(y,z,p[2]),
            interp_y<T,X,F>(y,z,p[3])
        };
        return F::static_get(x, tmp);
    }

    template <class T, class X, class F, class C>
    T c1_interpolation(const volume<T>& vox, const X x, const X y, const X z)
    {
        int ww = vox.get_width();
        int hh = vox.get_height();
        int dd = vox.get_depth();

        X rx = x * ww;
        X ry = y * hh;
        X rz = z * dd;

        rx -= X(0.5);
        ry -= X(0.5);
        rz -= X(0.5);

        X ix = std::floor(rx);
        X iy = std::floor(ry);
        X iz = std::floor(rz);

        X u = rx - ix;
        X v = ry - iy;
        X w = rz - iz;

        X iu = X(1) - u;
        X iv = X(1) - v;
        X iw = X(1) - w;

        int xx = (int)ix;
        int yy = (int)iy;
        int zz = (int)iz;
        int x0 = C::static_get(xx+0, ww);
        int y0 = C::static_get(yy+0, hh);
        int z0 = C::static_get(zz+0, dd);
        int x1 = C::static_get(xx+1, ww);
        int y1 = C::static_get(yy+1, hh);
        int z1 = C::static_get(zz+1, dd);

        u = F::static_get(u);
        v = F::static_get(v);
        w = F::static_get(w);

        iu = F::static_get(iu);
        iv = F::static_get(iv);
        iw = F::static_get(iw);

        return ( u *  v *  w) * vox.get(x0, y0, z0) +
               (iu *  v *  w) * vox.get(x1, y0, z0) +
               ( u * iv *  w) * vox.get(x0, y1, z0) +
               (iu * iv *  w) * vox.get(x1, y1, z0) +
               ( u *  v * iw) * vox.get(x0, y0, z1) +
               (iu *  v * iw) * vox.get(x1, y0, z1) +
               ( u * iv * iw) * vox.get(x0, y1, z1) +
               (iu * iv * iw) * vox.get(x1, y1, z1);
    }

    template <class T, class X, class F, class C>
    T c2_interpolation(const volume<T>& vox, const X x, const X y, const X z)
    {
        int ww = vox.get_width();
        int hh = vox.get_height();
        int dd = vox.get_depth();

        X rx = x * ww;
        X ry = y * hh;
        X rz = z * dd;

        rx -= X(0.5);
        ry -= X(0.5);
        rz -= X(0.5);

        X ix = std::floor(rx);
        X iy = std::floor(ry);
        X iz = std::floor(rz);

        int xx = (int)ix;
        int yy = (int)iy;
        int zz = (int)iz;

        int xd[4];
        int yd[4];
        int zd[4];
        for (int i = 0; i < 4; i++)
        {
            xd[i] = C::static_get(xx - 1 + i, ww);
            yd[i] = C::static_get(yy - 1 + i, hh);
            zd[i] = C::static_get(zz - 1 + i, dd);
        }

        X weight_x[4];
        X weight_y[4];
        X weight_z[4];

        for (int i = 0; i < 4; i++)
        {
            int p = i - 1;
            X sx = xx + p;
            X sy = yy + p;
            X sz = zz + p;
            X wx = std::fabs(rx - sx);
            X wy = std::fabs(ry - sy);
            X wz = std::fabs(rz - sz);
            wx = F::static_get(wx);
            wy = F::static_get(wy);
            wz = F::static_get(wz);
            weight_x[i] = wx;
            weight_y[i] = wy;
            weight_z[i] = wz;
        }

        return sum_x(weight_x, weight_y, weight_z, xd, yd, zd, vox);
    }

    template <class T, class X, class F, class C>
    T c2k_interpolation(const volume<T>& vox, const X x, const X y, const X z)
    {
        int ww = vox.get_width();
        int hh = vox.get_height();
        int dd = vox.get_depth();

        X rx = x * ww;
        X ry = y * hh;
        X rz = z * dd;

        rx -= X(0.5);
        ry -= X(0.5);
        rz -= X(0.5);

        X ix = std::floor(rx);
        X iy = std::floor(ry);
        X iz = std::floor(rz);

        X u = rx - ix;
        X v = ry - iy;
        X w = rz - iz;

        int xx = (int)ix;
        int yy = (int)iy;
        int zz = (int)iz;

        T p[4][4][4];
        for(int i = 0; i < 4; i++)
        {
            int ii = C::static_get(xx - 1 + i, ww);
            for(int j = 0; j < 4; j++)
            {
                int jj = C::static_get(yy - 1 + j, hh);
                for(int k = 0; k < 4; k++)
                {
                    int kk = C::static_get(zz - 1 + k, dd);
                    p[i][j][k] = vox.get(ii, jj, kk);
                }
            }
        }

        return interp_x<T, X, F>(u, v, w, p);
    }

    template<class T>
    class triangle_filter
    {
    public:
        static inline T static_get(const T x){return std::max<T>(T(0), T(1) - x);}
    };

    template<class T>
    class f3_filter
    {
    public:
        static inline T static_get(const T x)
        {
            return func(clamp(x));
        }
    protected:
        static inline T func(const T x)
        {
            return (T(2) * x - T(3)) * x * x + T(1);
        }
        static inline T clamp(const T x)
        {
            return std::max(T(0), std::min(x, T(1)));
        }
    };

    template <class T>
    using hermit_filter = f3_filter<T>;

    template<class T>
    class cubic_filter
    {
    public:
        static inline T static_get(const T x)
        {
            return (x <= T(0)) ? (T(1)) : ( (x < T(1)) ? (get_1st_cubic(x)) : ((x < T(2)) ? (get_2nd_cubic(x)) : T(0) ) );
        }
    protected:
        static T get_1st_cubic(T x)
        {
            //(x^3-x^2-x)+(-x^2+x+1)->x^3-2*x^2+1
            //return (x-1)*(x*x-x-1);
            static const T c1 = T(-2);
            static const T c0 = T( 1);
            
            return x * x * (x + c1) + c0;
        }
        static T get_2nd_cubic(T x)
        {
            //4-8*x+5*x^2-x^3
            //return -(x-1)*(x-2)*(x-2);
            static const T c3 = T(-1);
            static const T c2 = T( 5);
            static const T c1 = T(-8);
            static const T c0 = T( 4);

            return x * (x * (x * c3 + c2) + c1) + c0;
        }
    };

    template<class T>
    class mitchell_filter
    {
    public:
        static inline T static_get(const T x)
        {
            return (x <= T(0)) ? (T(1)) : ( (x < T(1)) ? (get_1st_mitchell(x)) : ((x < T(2)) ? (get_2nd_mitchell(x)) : T(0) ) );
        }
    protected:
        static T get_1st_mitchell(T x)
        {
            //7/6*x^3-2*x^2+8/9
            //(7/6+x-2)*(x^2)+8/9
            static const T c3 = 7.0 / 6.0;
            static const T c2 = -2.0;
            //static const real c1 = 0;
            static const T c0 = 8.0 / 9.0;

            return x * (x * (x * c3 + c2)) + c0;
        }
        static T get_2nd_mitchell(T x)
        {
            //2*x^2-10/3*x-7/18*x^3+16/9
            //-7/18*x^3 + 2*x^2 -10/3*x + 16/9
            static const T c3 = -7.0 / 18.0;
            static const T c2 = 2.0;
            static const T c1 = -10.0 / 3.0;
            static const T c0 = 16.0 / 9.0;

            return x * (x * (x * c3 + c2) + c1) + c0;
        }
    };

    template<class T, class X>
    inline T monotonic_cubic(
        const T& f1, const T& f2, const T& f3, const T& f4,
        const X t1, const X t2, const X t3)
    {
        T d_k  = T(0.5) * (f3 - f1);
        T d_k1 = T(0.5) * (f4 - f2);
        T delta_k = f3 - f2;

        if (delta_k == T(0))
        {
            d_k  = T(0);
            d_k1 = T(0);
        }
        
        T a0 = f2;
        T a1 = d_k;
        T a2 = (T(3) * delta_k) - (T(2) * d_k) - d_k1;
        T a3 = d_k + d_k1 - (T(2) * delta_k);
        //X t1 = t;
        //X t2 = t1 * t1;
        //X t3 = t2 * t1;
        return a3 * t3 + a2 * t2 + a1 * t1 + a0;
    }

    template<class T, class X>
    inline vector<T, 3> monotonic_cubic(
        const vector<T, 3>& f1, const vector<T, 3>& f2, const vector<T, 3>& f3, const vector<T, 3>& f4,
        const X t1, const X t2, const X t3
    )
    {
        return vector<T, 3>( 
            monotonic_cubic(f1[0], f2[0], f3[0], f4[0], t1, t2, t3),
            monotonic_cubic(f1[1], f2[1], f3[1], f4[1], t1, t2, t3),
            monotonic_cubic(f1[2], f2[2], f3[2], f4[2], t1, t2, t3)
        );
    }

    template<class T, class X>
    inline vector<T, 4> monotonic_cubic(
        const vector<T, 4>& f1, const vector<T, 4>& f2, const vector<T, 4>& f3, const vector<T, 4>& f4,
        const X t1, const X t2, const X t3
    )
    {
        return vector<T, 4>( 
            monotonic_cubic(f1[0], f2[0], f3[0], f4[0], t1, t2, t3),
            monotonic_cubic(f1[1], f2[1], f3[1], f4[1], t1, t2, t3),
            monotonic_cubic(f1[2], f2[2], f3[2], f4[2], t1, t2, t3),
            monotonic_cubic(f1[3], f2[3], f3[3], f4[3], t1, t2, t3)
        );
    }

    template<class T, class X>
    class monotonic_cubic_filter
    {
    public:
        static inline T static_get(const X t, const T p[4])
        {
            X t1 = t;
            X t2 = t1 * t1;
            X t3 = t2 * t1;
            return monotonic_cubic(p[0], p[1], p[2], p[3], t1, t2, t3);
        }
    };

    class clamp_corresponder
    {
    public:
        static inline int static_get(int x, int w){return std::max(0, std::min(x, w-1));}
    };

    template<class T, class X>
    inline T sample_nearest_neighbor(const volume<T>& vox, const X x, const X y, const X z)
    {
        int ww = vox.get_width();
        int hh = vox.get_height();
        int dd = vox.get_depth();
        int xx = (int)std::floor(x*ww);
        int yy = (int)std::floor(y*hh);
        int zz = (int)std::floor(z*dd);
        xx = clamp_corresponder::static_get(xx, ww);
        yy = clamp_corresponder::static_get(yy, hh);
        zz = clamp_corresponder::static_get(zz, dd);
        return vox.get(xx, yy, zz);
    }

    template<class T, class X>
    inline T sample_linear(const volume<T>& vox, const X x, const X y, const X z)
    {
        return c1_interpolation<T, X, triangle_filter<X>, clamp_corresponder>(vox, x, y, z);
    }

    template<class T, class X>
    inline T sample_hermit  (const volume<T>& vox, const X x, const X y, const X z)
    {
        return c1_interpolation<T, X, hermit_filter<X>, clamp_corresponder>(vox, x, y, z);
    }
    
    template<class T, class X>
    inline T sample_cubic(const volume<T>& vox, const X x, const X y, const X z)
    {
        return c2_interpolation<T, X, cubic_filter<X>, clamp_corresponder>(vox, x, y, z);
    }

    template<class T, class X>
    inline T sample_monotonic_cubic(const volume<T>& vox, const X x, const X y, const X z)
    {
        return c2k_interpolation<T, X, monotonic_cubic_filter<T, X>, clamp_corresponder>(vox, x, y, z);
    }

    template<class T, class X>
    inline T sample_mitchell(const volume<T>& vox, const X x, const X y, const X z)
    {
        return c2_interpolation<T, X, mitchell_filter<X>, clamp_corresponder>(vox, x, y, z);
    }
}

#endif