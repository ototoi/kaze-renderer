#pragma once
#ifndef VOL_COORD_MAPPER_HPP
#define VOL_COORD_MAPPER_HPP

#include <vector>

#include "vector.hpp"

namespace vol
{
    template<class T>
    class coord_mapper
    {
    public:
        typedef vector<T, 3> vector_type;
    public:
        virtual ~coord_mapper(){}
        virtual vector_type world_to_local(const vector_type& p)const = 0;
    };

    template<class T>
    class matrix_coord_mapper : public coord_mapper<T>
    {
    public:
        typedef vector<T, 3> vector_type;
        typedef matrix<T, 4, 4> matrix_type;
    public:
        matrix_coord_mapper(const matrix_type& mat)
        {
            mat_ = mat;
        }
        virtual vector_type world_to_local(const vector_type& p)const
        {
            return p;
        }
    protected:
        matrix_type mat_;
    };

}

#endif
