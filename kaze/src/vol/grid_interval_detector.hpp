#pragma once
#ifndef VOL_GRID_INTERVAL_DETECTOR_HPP
#define VOL_GRID_INTERVAL_DETECTOR_HPP

#include <vector>

#include "vector.hpp"
#include "matrix.hpp"
#include "oobb.hpp"
#include "grid.hpp"
#include "interval_detector.hpp"
#include "grid_interval_detector_CPU.hpp"

namespace vol
{
    template< class T >
    class grid_interval_detector 
        : public interval_detector<T>
    {
    public:
        grid_interval_detector(const grid<T>& box)
        {
            strategy_.reset( new grid_interval_detector_CPU<T>(box) );
        }
    public:
        virtual void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const
        {
            strategy_->detect(out_intervals, rays, in_intervals);
        }
    protected:
        std::unique_ptr< interval_detector<T> > strategy_;
    };
}

#endif