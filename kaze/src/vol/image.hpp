#pragma once
#ifndef VOL_IMAGE_HPP
#define VOL_IMAGE_HPP

#include <vector>

namespace vol
{
    template<class T>
    class image
    {
    public:
        image(int width, int height)
            :width_(width), height_(height)
        {
            values_.resize(width*height);
        }
        int get_width()const {return width_;}
        int get_height()const{return height_;}
        T& at(int x, int y){return values_[y*width_+x];}
        const T& at(int x, int y)const{return values_[y*width_+x];}

        int size()const{return values_.size();}
        T& operator[](size_t i){return values_[i];}
        const T& operator[](size_t i)const{return values_[i];}
    public:
        int width_;
        int height_;
        std::vector<T> values_;
    };
}

#endif
