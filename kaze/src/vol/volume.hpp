#pragma once
#ifndef VOL_VOLUME_HPP
#define VOL_VOLUME_HPP

#include <vector>

namespace vol
{
    template<class T>
    class volume
    {
    public:
        virtual ~volume(){}
        virtual int get_width()const = 0;
        virtual int get_height()const = 0;
        virtual int get_depth()const = 0;
        virtual T& at(int x, int y, int z) = 0;
        virtual const T& at(int x, int y, int z)const = 0;
        //virtual size_t size()const = 0;
        //virtual T& operator[](size_t i) = 0;
        //virtual const T& operator[](size_t i)const = 0;
    public:
        virtual const T& get(int x, int y, int z)const{return this->at(x, y, z);}
        virtual void set(int x, int y, int z, const T& v){this->at(x,y,z) = v;}
    };
}

#endif
