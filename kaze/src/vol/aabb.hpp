#pragma once
#ifndef VOL_AABB_HPP
#define VOL_AABB_HPP

#include "vector.hpp"

namespace vol
{
    template<class T>
    class aabb
    {
    public:
        typedef vector<T, 3> point_type;
        typedef aabb<T> this_type;
    public:
        aabb(const point_type& min, const point_type& max)
            :min_(min), max_(max)
        {}
        const point_type& get_min()const{return min_;}
        const point_type& get_max()const{return max_;}
    protected:
        point_type min_;
        point_type max_;
    };

    //template<class T>
    //using AABB = aabb<T>;
}

#endif
