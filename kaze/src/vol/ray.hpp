#pragma once
#ifndef VOL_RAY_HPP
#define VOL_RAY_HPP

#include <vector>
#include <list>
#include <iostream>
#include <sstream>
#include <limits>

#include "vector.hpp"

namespace vol
{
    template<class T>
    class ray
    {
    public:
        typedef ray<T> this_type;
    public:
        ray(){}
        ray(const T org[3], const T dir[3])
        {
            std::copy(org, org+3, org_);
            std::copy(dir, dir+3, dir_);
        }
        const T* org()const{return org_;}
        const T* dir()const{return dir_;}
        const T* get_org()const{return org_;}
        const T* get_dir()const{return dir_;}
        void set_org(const T* org)
        {
            for(int  i = 0; i < 3; i++)
            {
                org_[i] = org[i];
            }
        }
        void set_dir(const T* dir) 
        {
            for(int  i = 0; i < 3; i++)
            {
                dir_[i] = dir[i];
            }
        }
    private:
        T org_[3];
        T dir_[3];
    };
}
#endif
