#pragma once
#ifndef VOL_VECTOR_HPP
#define VOL_VECTOR_HPP

#include <tempest/vector.hpp>
#include <tempest/vector2.hpp>
#include <tempest/vector3.hpp>
#include <tempest/vector4.hpp>

namespace vol
{
    template<class T, size_t Sz>
    using vector = tempest::vector<T, Sz>;
}

#endif
