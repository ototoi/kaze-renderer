#pragma once
#ifndef VOL_PARAMETERIZED_VOLUME_HPP
#define VOL_PARAMETERIZED_VOLUME_HPP

#include <vector>
#include <string>
#include <limits>

#include "volume.hpp"
#include "voxel_volume.hpp"
#include "grid.hpp"
#include "coord_mapper.hpp"
#include "vector_math.hpp"

namespace vol
{
    template<class T>
    class parameterized_volume 
        : public volume<T>,
          public grid<T>
    {
    public:
        typedef std::map<std::string, std::shared_ptr<voxel_volume<T> > > map_type;
        typedef vector<T, 3> vector_type;
        typedef matrix<T, 4, 4> matrix_type; 
    public:
        parameterized_volume(const vector_type& width, const matrix_type& world, const int div[3])
            :grid<T>(width, world, div)
        {
            vox_ = std::shared_ptr<voxel_volume<T> >( new voxel_volume<T>(div) );
            m_["density"] = vox_;
            cm_.reset( new matrix_coord_mapper<T>(this->get_inversed_matrix() ) );
        }
        std::vector<std::string> get_keys()const
        {
            typedef typename map_type::const_iterator const_iterator;
            std::vector<std::string> vec;
            for(const_iterator it = m_.begin(); it != m_.end(); it++)
            {
                vec.push_back(it->first);
            }
            return vec;
        } 
        std::shared_ptr<voxel_volume<T> >& get_density_buffer()
        {
            return vox_;
        }
        const std::shared_ptr<voxel_volume<T> >& get_density_buffer()const
        {
            return vox_;
        }
    public:
        const std::shared_ptr< coord_mapper<T> > get_coord_mapper()const
        {
            return cm_;
        }

        vector_type get_voxel_size(const vector_type& p)
        {
            const int* div = this->get_divisions();
            vector_type pp = this->sample(p) * vector_type(div[0], div[1], div[2]);
            vector_type p0 = vol::floor(pp);
            vector_type p1 = p0 + vector_type(1,0,0);
            vector_type p2 = p0 + vector_type(0,1,0);
            vector_type p3 = p0 + vector_type(0,0,1);
            p0 = ((p0 / vector_type(div[0], div[1], div[2])) - vector_type(0.5,0.5,0.5)) * this->get_extent();
            p1 = ((p1 / vector_type(div[0], div[1], div[2])) - vector_type(0.5,0.5,0.5)) * this->get_extent();
            p2 = ((p2 / vector_type(div[0], div[1], div[2])) - vector_type(0.5,0.5,0.5)) * this->get_extent();
            p3 = ((p3 / vector_type(div[0], div[1], div[2])) - vector_type(0.5,0.5,0.5)) * this->get_extent();
            p0 = this->get_matrix() * p0;
            p1 = this->get_matrix() * p1;
            p2 = this->get_matrix() * p2;
            p3 = this->get_matrix() * p3;
            return vector_type(length(p1 - p0), length(p2 - p0), length(p3 - p0));
        }

    public:
        virtual int get_width()const {return vox_->get_width();}
        virtual int get_height()const{return vox_->get_height();}
        virtual int get_depth()const {return vox_->get_depth();}
        virtual T& at(int x, int y, int z){return vox_->at(x, y, z);}
        virtual const T& at(int x, int y, int z)const{return vox_->at(x, y, z);}
        virtual size_t size()const{return vox_->size();}
        virtual T& operator[](size_t i){return (*vox_)[i];}
        virtual const T& operator[](size_t i)const{return (*vox_)[i];}
    protected:
        std::shared_ptr<voxel_volume<T> > vox_;
        map_type m_;
        std::shared_ptr< coord_mapper<T> > cm_;
    };

}

#endif
