#pragma once
#ifndef VOL_GRID_HPP
#define VOL_GRID_HPP

#include <vector>

#include "oobb.hpp"

namespace vol
{
    template <class T>
    class grid : public oobb<T>
    {
    public:
        typedef vector<T, 3> vector_type;
        typedef matrix<T, 4, 4> matrix_type;
        typedef grid<T> this_type;
    public:
        grid(const vector_type& width, const matrix_type& world)
            :oobb<T>(width, world)
        {
            div_[0] = 1;
            div_[1] = 1;
            div_[2] = 1;
        }
        grid(const vector_type& width, const matrix_type& world, const int div[3])
            :oobb<T>(width, world)
        {
            div_[0] = div[0];
            div_[1] = div[1];
            div_[2] = div[2];
        }
        const int* get_divisions()const
        {
            return div_;
        }
        size_t size()const
        {
            return div_[0]*div_[1]*div_[2];
        }

        vector_type sample(const vector_type& p)const
        {
            vector_type pp = this->get_inversed_matrix() * p;
            vector_type wid = T(0.5) * this->get_extent();
            vector_type min = -wid;
            vector_type max = +wid;
            T x = (max[0] - pp[0]) / (max[0] - min[0]);
            T y = (max[1] - pp[1]) / (max[1] - min[1]);
            T z = (max[2] - pp[2]) / (max[2] - min[2]);
            return vector_type(x, y, z);
        }
    protected:
        int div_[3];
    };
}

#endif
