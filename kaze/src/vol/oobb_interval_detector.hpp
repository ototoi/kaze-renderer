#pragma once
#ifndef VOL_OOBB_INTERVAL_DETECTOR_HPP
#define VOL_OOBB_INTERVAL_DETECTOR_HPP

#include <vector>

#include "vector.hpp"
#include "matrix.hpp"
#include "oobb.hpp"
#include "interval_detector.hpp"
#include "oobb_interval_detector_CPU.hpp"
#include "oobb_interval_detector_GL.hpp"

namespace vol
{
    template<
        class T, 
        class Sterategy = oobb_interval_detector_CPU<T> 
    >
    class oobb_interval_detector 
        : public interval_detector<T>
    {
    public:
        oobb_interval_detector(const oobb<T>& box)
        {
            strategy_.reset( new Sterategy(box) );
        }
    public:
        virtual void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const
        {
            strategy_->detect(out_intervals, rays, in_intervals);
        }
    protected:
        std::unique_ptr< interval_detector<T> > strategy_;
    };
}

#endif
