#ifndef VOL_OOBB_INTERVAL_DETECTOR_CPU_HPP
#define VOL_OOBB_INTERVAL_DETECTOR_CPU_HPP

#include <vector>
#include <list>
#include <iostream>
#include <sstream>
#include <cassert>
#include <thread>

#include "vector.hpp"
#include "matrix.hpp"
#include "image.hpp"
#include "oobb.hpp"
#include "box_intersection.hpp"
#include "box_intersection_SIMD.hpp"

namespace vol
{
    template<class T>
    class oobb_interval_detector_CPU : public interval_detector<T>
    {
    public:
        explicit oobb_interval_detector_CPU(const oobb<T>& box)
            :box_(box)
        {}
    public:
        inline void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const
        {
            assert(rays.size() == out_intervals.size());
            assert(rays.size() == in_intervals.size());
            size_t sz = rays.size();
            for(size_t i = 0; i < sz; i++)
            {
                detect(out_intervals[i], box_, rays[i], in_intervals[i]);
            }
        }
    protected:
        static inline void detect
        (
                  intervals<T>& out_intervals,
            const oobb<T>& box,
            const ray<T>& ray,
            const intervals<T>& in_intervals
        )
        {
            T rng[2];
            if(oobb_intersect(rng, box, ray, in_intervals[0][0], in_intervals[0][1]))
            {
                out_intervals.add(in_intervals[0]);
                out_intervals.add(interval<T>(rng[0], rng[1]));
            }
            else
            {
                out_intervals.add(in_intervals[0]);                                        
            }
        }
    protected:
        oobb<T> box_;
    };

#if defined( VOL_USE_SIMD )
    template<>
    class oobb_interval_detector_CPU<float> : public interval_detector<float>
    {
    public:
        explicit oobb_interval_detector_CPU(const oobb<float>& box)
            :box_(box)
        {}
    public:
        inline void detect(
            std::vector< intervals<float> >& out_intervals,
            const std::vector< ray<float> >& rays,
            const std::vector< intervals<float> >& in_intervals
        )const
        {
            assert(rays.size() == out_intervals.size());
            assert(rays.size() == in_intervals.size());
            typedef typename oobb<float>::vector_type vector_type;
            typedef typename oobb<float>::matrix_type matrix_type;
            const matrix_type& imat = box_.get_inversed_matrix();
            vector_type width = 0.5f * box_.get_extent();
            vector_type min = -width;
            vector_type max = +width;

            int threadnum = std::thread::hardware_concurrency();
            size_t size = rays.size();
            size_t sz = size;
            if(sz & 7)
            {
                int k = sz & 7;
                sz += 8 - k;
            }
            std::vector<size_t> offsets(threadnum+1);
            {
                size_t blocks = sz / 8;
                size_t dsz = (size_t)blocks/threadnum;
                offsets[0] = 0;
                offsets[threadnum] = sz;
                for(int i = 1; i < threadnum; i++)
                {
                    offsets[i] = offsets[i-1] + dsz * 8;
                }
                offsets[threadnum] = sz;
            }

            float* orgs_x = (float*)alloc_(sizeof(float)*sz);
            float* orgs_y = (float*)alloc_(sizeof(float)*sz);
            float* orgs_z = (float*)alloc_(sizeof(float)*sz);
            float* dirs_x = (float*)alloc_(sizeof(float)*sz);
            float* dirs_y = (float*)alloc_(sizeof(float)*sz);
            float* dirs_z = (float*)alloc_(sizeof(float)*sz);
            float* t0 = (float*)alloc_(sizeof(float)*sz);
            float* t1 = (float*)alloc_(sizeof(float)*sz);
            float* s0 = (float*)alloc_(sizeof(float)*sz);
            float* s1 = (float*)alloc_(sizeof(float)*sz);

            for(size_t i = 0; i < size; i++)
            {
                const ray<float>& r = rays[i];
                vector_type org = mul_p(imat, vector_type(r.org()));
                vector_type dir = mul_v(imat, vector_type(r.dir()));

                orgs_x[i] = org[0];
                orgs_y[i] = org[1];
                orgs_z[i] = org[2];
                dirs_x[i] = 1.0f/dir[0];
                dirs_y[i] = 1.0f/dir[1];
                dirs_z[i] = 1.0f/dir[2];

                t0[i] = in_intervals[i].front()[0];
                t1[i] = in_intervals[i].back() [1];
            }
            std::thread t[threadnum];
            for(int i = 0; i < threadnum; i++)
            {
                auto f = [&] (int i) 
                { 
                    size_t offset = offsets[i];
                    size_t ssz = offsets[i+1] - offsets[i];
                    aabb_intersect_SIMD8(
                        s0 + offset, s1 + offset, 
                        &min[0], &max[0], 
                        orgs_x + offset, orgs_y + offset, orgs_z + offset,
                        dirs_x + offset, dirs_y + offset, dirs_z + offset,
                        t0 + offset, t1 + offset,
                        ssz
                    );
                };
                t[i] = std::thread(f, i);
            }
            for (int i = 0; i < threadnum; ++i) 
            {
                t[i].join();
            }

            for(size_t i = 0; i < size; i++)
            {
                if(s0[i] < s1[i])
                {
                    out_intervals[i].add(in_intervals[i][0]);
                    out_intervals[i].add(interval<float>(s0[i], s1[i]));
                }
                else
                {
                    out_intervals[i].add(in_intervals[i][0]);
                }
            }

            free_(orgs_x);
            free_(orgs_y);
            free_(orgs_z);
            free_(dirs_x);
            free_(dirs_y);
            free_(dirs_z);

            free_(t0);
            free_(t1);
            free_(s0);
            free_(s1);
        }
    protected:
        static inline float* alloc_(size_t sz)
        {
            return (float*)vol::aligned_malloc(sz, 32);
        }
        static inline void free_(void* p)
        {
            vol::aligned_free(p);
        }
        static inline vector<float, 3> mul_v(const matrix<float, 4, 4>& m, const vector<float, 3>& v)
        {
            return vector<float, 3>(
                m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2],
                m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2],
                m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2]);
        }
        static inline vector<float, 3> mul_p(const matrix<float, 4, 4>& m, const vector<float, 3>& v)
        {
            float iR = float(1) / (m[3][0] * v[0] + m[3][1] * v[1] + m[3][2] * v[2] + m[3][3]);
            return vector<float, 3>(
                (m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2] + m[0][3]) * iR,
                (m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2] + m[1][3]) * iR,
                (m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2] + m[2][3]) * iR);
        }
    protected:
        oobb<float> box_;
    };
#endif
}

#endif
