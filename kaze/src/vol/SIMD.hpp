#pragma once
#ifndef VOL_SIMD_HPP
#define VOL_SIMD_HPP

#ifdef _MSC_VER
    #ifdef __AVX__
    #include <immintrin.h>
    #define VOL_USE_SIMD 1
    #endif
#elif defined(__GNUC__) || defined(__APPLE__)
    #ifdef __AVX__
    #include <immintrin.h>
    #define VOL_USE_SIMD 1
    #endif
#endif

#include <stdlib.h>
#if !defined(__APPLE__) && !defined(__OpenBSD__)
#include <malloc.h> // for _alloca, memalign
#endif
#if !defined(WIN32) && !defined(__APPLE__) && !defined(__OpenBSD__)
#include <alloca.h>
#endif

namespace vol
{
#ifdef _WIN32
    static inline void* aligned_malloc(size_t sz, size_t al)
    {
        return ::_aligned_malloc(sz, al);
    }

    static inline void aligned_free(void* p)
    {
        ::_aligned_free(p);
    }
#elif defined(__APPLE__)
    static inline void* aligned_malloc(size_t sz, size_t al)
    {
        void* p = NULL;
        posix_memalign(&p, al, sz);
        return p;
    }
    static inline void aligned_free(void* p)
    {
        free(p);
    }
#elif defined(__unix__) || defined(__GNUC__)
    static inline void* aligned_malloc(size_t sz, size_t al)
    {
        void* p = NULL;
        posix_memalign(&p, al, sz);
        return p;
    }
    static inline void aligned_free(void* p)
    {
        free(p);
    }
#endif

}

#endif
