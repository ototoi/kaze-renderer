#pragma once
#ifndef VOL_MATRIX_HPP
#define VOL_MATRIX_HPP

#include <tempest/matrix.hpp>
#include <tempest/matrix2.hpp>
#include <tempest/matrix3.hpp>
#include <tempest/matrix4.hpp>
#include <tempest/matrix_functions.hpp>
#include <tempest/quaternion.hpp>
#include <tempest/matrix_generator.hpp>
#include <tempest/transform.hpp>

namespace vol
{
    template<class T, size_t Sz1, size_t Sz2 = Sz1>
    using matrix = tempest::matrix<T, Sz1, Sz2>;

    template <class T>
    using quaternion = tempest::quaternion<T>;

    template <class T, size_t Sz1, size_t Sz2 = Sz1>
    using matrix_generator = tempest::matrix_generator< tempest::matrix<T, Sz1, Sz2> >;

    template <class T>
    using matrix2_generator = tempest::matrix_generator< tempest::matrix<T, 2, 2> >;

    template <class T>
    using matrix3_generator = tempest::matrix_generator< tempest::matrix<T, 3, 3> >;

    template <class T>
    using matrix4_generator = tempest::matrix_generator< tempest::matrix<T, 4, 4> >;
}

#endif