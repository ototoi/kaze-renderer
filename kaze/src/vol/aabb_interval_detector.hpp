#ifndef VOL_AABB_INTERVAL_DETECTOR_HPP
#define VOL_AABB_INTERVAL_DETECTOR_HPP

#include <vector>

#include "vector.hpp"
#include "matrix.hpp"
#include "aabb.hpp"
#include "interval_detector.hpp"
#include "aabb_interval_detector_CPU.hpp"
//#include "aabb_interval_detector_GL.hpp"

namespace vol
{
    template<
        class T, 
        class Sterategy = aabb_interval_detector_CPU<T> 
    >
    class aabb_interval_detector 
        : public interval_detector<T>
    {
    public:
        aabb_interval_detector(const aabb<T>& box)
        {
            strategy_.reset( new Sterategy(box) );
        }
    public:
        virtual void detect(
            std::vector< intervals<T> >& out_intervals,
            const std::vector< ray<T> >& rays,
            const std::vector< intervals<T> >& in_intervals
        )const
        {
            strategy_->detect(out_intervals, rays, in_intervals);
        }
    protected:
        std::unique_ptr< interval_detector<T> > strategy_;
    };
}

#endif