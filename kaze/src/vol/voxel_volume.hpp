#pragma once
#ifndef VOL_VOXEL_VOLUME_HPP
#define VOL_VOXEL_VOLUME_HPP

#include <vector>

#include "volume.hpp"

namespace vol
{
    template<class T>
    class voxel_volume : public volume<T>
    {
    public:
        voxel_volume(int w, int h, int d)
        {
            div_[0] = w;
            div_[1] = h;
            div_[2] = d;
            buffer_.resize(size());
        }
        voxel_volume(const int div[3])
        {
            div_[0] = div[0];
            div_[1] = div[1];
            div_[2] = div[2];
            buffer_.resize(size());
        }
        virtual int get_width()const {return div_[0];}
        virtual int get_height()const{return div_[1];}
        virtual int get_depth()const {return div_[2];}
        virtual T& at(int x, int y, int z){return buffer_[index(x,y,z)];}
        virtual const T& at(int x, int y, int z)const{return buffer_[index(x,y,z)];}
        virtual size_t size()const{return div_[0]*div_[1]*div_[2];}
        virtual T& operator[](size_t i){return buffer_[i];}
        virtual const T& operator[](size_t i)const{return buffer_[i];}
    protected:
        inline size_t index(int x, int y, int z)const
        {
            int w = div_[0];
            int h = div_[1];
            int d = div_[2];
            return (size_t)(x + (w * (y + h * (z))));
        }
    protected:
        int div_[3];
        std::vector<T> buffer_;
    };
}

#endif
