#ifndef GALE_TYPES_H
#define GALE_TYPES_H

#include <cstddef>

#include <tempest/vector.hpp>
#include <tempest/vector2.hpp>
#include <tempest/vector3.hpp>
#include <tempest/vector4.hpp>
#include <tempest/matrix.hpp>
#include <tempest/matrix2.hpp>
#include <tempest/matrix3.hpp>
#include <tempest/matrix4.hpp>
#include <tempest/transform.hpp>
#include <tempest/matrix_generator.hpp>
#include <tempest/quaternion.hpp>

namespace gale
{
    typedef tempest::vector<float, 2> vector2;
    typedef tempest::vector<float, 3> vector3;
    typedef tempest::vector<float, 4> vector4;
    typedef tempest::vector<float, 2> vector2f;
    typedef tempest::vector<float, 3> vector3f;
    typedef tempest::vector<float, 4> vector4f;
    typedef tempest::vector<double, 2> vector2d;
    typedef tempest::vector<double, 3> vector3d;
    typedef tempest::vector<double, 4> vector4d;

    typedef tempest::vector<float, 2> color2;
    typedef tempest::vector<float, 3> color3;
    typedef tempest::vector<float, 4> color4;
    typedef tempest::vector<float, 2> color2f;
    typedef tempest::vector<float, 3> color3f;
    typedef tempest::vector<float, 4> color4f;
    typedef tempest::vector<double, 2> color2d;
    typedef tempest::vector<double, 3> color3d;
    typedef tempest::vector<double, 4> color4d;


    typedef tempest::matrix<float, 2> matrix2;
    typedef tempest::matrix<float, 3> matrix3;
    typedef tempest::matrix<float, 4> matrix4;
    typedef tempest::matrix<float, 2> matrix2f;
    typedef tempest::matrix<float, 3> matrix3f;
    typedef tempest::matrix<float, 4> matrix4f;

    typedef tempest::quaternion<float> quaternion;
    typedef tempest::quaternion<float> quaternionf;
    typedef tempest::quaternion<double> quaterniond;

    typedef tempest::matrix_generator<matrix2> mat2_gen;
    typedef tempest::matrix_generator<matrix3> mat3_gen;
    typedef tempest::matrix_generator<matrix4> mat4_gen;
}

#endif
