#include "image_texture.h"
#include <vector>

#ifdef _WIN32
#include <shlobj.h>
#include <gdiplus.h>
#include <Gdiplusinit.h>
#include <windows.h>

#pragma comment(lib, "shlwapi.lib")
#pragma comment(lib, "gdiplus.lib")

#else
#include <jpeglib.h>
#endif

namespace gale
{
    struct img_buffer
    {
        int width;
        int height;
        std::vector<unsigned char> buffer;
    };
    
    static
    int ReadJpegImage(img_buffer& img, const char*szFilePath)
    {
        FILE* fp = fopen(szFilePath, "rb");
        if (!fp)return -1;

        struct jpeg_decompress_struct cinfo;
        struct jpeg_error_mgr jerr;

        cinfo.err = jpeg_std_error(&jerr);
        jpeg_create_decompress(&cinfo);
        jpeg_stdio_src(&cinfo, fp);
        jpeg_read_header(&cinfo, TRUE);

        jpeg_start_decompress(&cinfo);

        JSAMPARRAY buffer = (JSAMPARRAY)malloc(sizeof(JSAMPROW) * cinfo.output_height);
        for (int i = 0; i < cinfo.output_height; ++i)
        {
            buffer[i] = (JSAMPROW)calloc(sizeof(JSAMPLE), cinfo.output_width * cinfo.output_components);
        }

        while (cinfo.output_scanline < cinfo.output_height)
        {
            jpeg_read_scanlines(&cinfo,
                                buffer + cinfo.output_scanline,
                                cinfo.output_height - cinfo.output_scanline);
        }

        jpeg_finish_decompress(&cinfo);
        jpeg_destroy_decompress(&cinfo);

        fclose(fp);

        int width = cinfo.output_width;
        int height = cinfo.output_height;

        if (width * height)
        {
            img.width = width;
            img.height = height;
            img.buffer.resize(width*height*4);
            memset(&img.buffer[0],255,sizeof(unsigned char)*width*height*4);
            
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    int index = y*width+x;
                    unsigned char R = buffer[y][3 * x + 0];
                    unsigned char G = buffer[y][3 * x + 1];
                    unsigned char B = buffer[y][3 * x + 2];

                    img.buffer[4*index+0] = R;
                    img.buffer[4*index+1] = G;
                    img.buffer[4*index+2] = B;
                    img.buffer[4*index+3] = 255;
                }
            }
        }
        else
        {
            return -1;
        }

        for (int i = 0; i < cinfo.output_height; ++i)
        {
            free(buffer[i]);
        }
        free(buffer);

        return 0;
    }
    
    image_texture::image_texture(float r, float g, float b, float a)
    {
        static const int width = 4;
        static const int height = 4;

        tex_ = std::shared_ptr<gl::gl_texture_2D>(new gl::gl_texture_2D());
        {
            int sz = width*height;
            std::vector<float> color(sz*4);
            for(int i = 0;i<sz;i++)
            {
                color[4*i+0] = r;
                color[4*i+1] = g;
                color[4*i+2] = b;
                color[4*i+3] = a;
            }
            tex_->set_image(GL_RGBA16F, width, height, GL_RGBA, GL_FLOAT, &color[0]);
        }
        {
            tex_->bind();
            ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);  //not biliear
            ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);  //
            ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //TODO
            ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //TODO

            tex_->unbind();
        }
        width_ = width;
        height_ = height;
    }
    image_texture::image_texture(const char* szPath)
    {
        tex_ = std::shared_ptr<gl::gl_texture_2D>(new gl::gl_texture_2D());
        {
            img_buffer img;
            int nRet = ReadJpegImage(img, szPath);
            if(nRet == 0)
            {
                int width = img.width;
                int height= img.height;
                int sz = width*height;
                tex_->set_image(GL_RGBA8, width, height, GL_RGBA, GL_UNSIGNED_BYTE, &(img.buffer[0]));
                width_ = width;
                height_ = height;
            }
        }
        {
            tex_->bind();
            ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);  //
            ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);  //
            ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //TODO
            ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //TODO
            
            float aniso = 0.0;
            ::glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &aniso);
            ::glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, aniso); 
            
            tex_->unbind();
        }            
    }
    image_texture::~image_texture()
    {
        
    }
    GLuint image_texture::get_handle()const
    {
        return tex_->get_handle();   
    }
    int image_texture::get_width()const
    {
        return width_;
    }
    int image_texture::get_height()const
    {
        return height_;
    }
}
