#include "fw_renderer.h"
#include "values.h"
#include "render_node.h"
#include "material.h"
#include "shader.h"

#include "gl/gl_frame_buffer.h"
#include "gl/gl_texture.h"
#include "gl/gl_program.h"
#include "gl/gl_vertex_buffer.h"
#include "gl/gl_vertex_array.h"



namespace gale
{
    struct rnode_tmp
    {
        float depth;
        const render_node* node;
    };

    struct node_sorter_d
    {
        bool operator()(const rnode_tmp& a, const rnode_tmp& b)
        {
            return a.depth < b.depth;
        }
    };
    
    struct node_sorter_t
    {
        bool operator()(const rnode_tmp& a, const rnode_tmp& b)
        {
            return a.depth > b.depth;
        }
    };

    class fw_render_context : public render_context
    {
    public:
        void set_world_to_camera(const matrix4& m){w2c_ = m;}
        void set_camera_to_clip (const matrix4& m){c2c_ = m;}
        void set_line_mode(bool b){is_line_ = b;}
    public:
        virtual matrix4 get_world_to_camera()const{return w2c_;}
        virtual matrix4 get_camera_to_clip ()const{return c2c_;}
        virtual bool    is_line_mode()const{return is_line_;}
    private:
        matrix4 w2c_;
        matrix4 c2c_;
        bool is_line_;
    };

    class fw_render_target : public render_target
    {
    public:
        fw_render_target(int width, int height)
        {
            initialize(width, height);
        }
        virtual GLuint get_handle()const{return framebuffer_->get_handle();}
        virtual void bind() const{framebuffer_->bind();}
        virtual void unbind() const{framebuffer_->unbind();}
    public:
        void refine(int width, int height)
        {
            if(width_ != width || height_ != height)
            {
                initialize(width, height);
            }
        }
        void initialize(int width, int height)
        {
            framebuffer_ = std::unique_ptr<gl::gl_frame_buffer>(new gl::gl_frame_buffer());
            {
                color_ = std::unique_ptr<gl::gl_texture_2D>(new gl::gl_texture_2D());
                color_->set_image(GL_RGBA16F, width, height, GL_RGBA, GL_FLOAT, NULL);
                //color_->set_image(GL_RGBA8, width, height, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
                {
                    color_->bind();
                    ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);   //not biliear
                    ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);   //
                    ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //TODO
                    ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //TODO
                    color_->unbind();
                }
            }
            {
                depth_ = std::unique_ptr<gl::gl_texture_2D>(new gl::gl_texture_2D());
                depth_->set_image(GL_DEPTH_COMPONENT32F, width, height, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
                {
                    depth_->bind();
                    ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);   //not biliear
                    ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);   //
                    ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //TODO
                    ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //TODO
                    depth_->unbind();
                }
            }
            framebuffer_->bind();
            framebuffer_->attach_color(color_->get_handle(), 0);
            framebuffer_->attach_depth(depth_->get_handle());
            framebuffer_->unbind();
            width_  = width;
            height_ = height;
        }
        GLuint get_color_texture_handle(int n)const{return color_->get_handle();}
    protected:
        std::unique_ptr<gl::gl_frame_buffer> framebuffer_;
        std::unique_ptr<gl::gl_texture_2D>  color_;
        std::unique_ptr<gl::gl_texture_2D>  depth_;
        int width_;
        int height_;
    };

    static
    std::string GetFSString()
    {
        static const char* STR =
        "#version 410\n"
        "\n"
        "in vec2 vs_uv;\n"
        "\n"
        "out vec4 color;\n"
        "\n"
        "uniform sampler2D tex;\n"
        "\n"
        "void main(void) {\n"
        "  color = texture(tex, vs_uv);\n"
        "}\n"
        "\n";
        return STR;
    }
    static
    std::string GetVSString()
    {
        static const char* STR =
        "#version 410\n"
        "\n"
        "layout (location = 0) in vec3 pos;\n"
        "layout (location = 2) in vec2 uv;\n"
        "\n"
        "out vec2 vs_uv;\n"
        "void main(void) {\n"
        "  vs_uv = uv;\n"
        "  gl_Position = vec4(pos,1);\n"
        "}\n"
        "\n";
        return STR;
    }

    static
    std::shared_ptr<gl::gl_program> CreateProgram()
    {
        std::shared_ptr<gl::gl_shader> fs(new gl::gl_shader(GL_FRAGMENT_SHADER));
        bool bFS = fs->compile_from_string(GetFSString());
        std::shared_ptr<gl::gl_shader> vs(new gl::gl_shader(GL_VERTEX_SHADER));
        bool bVS = vs->compile_from_string(GetVSString());
        if(!bFS || !bVS)return std::shared_ptr<gl::gl_program>();
        std::shared_ptr<gl::gl_program> prog(new gl::gl_program());
        prog->add_shader(vs);
        prog->add_shader(fs);
        bool bLink = prog->link();
        if(!bLink)return std::shared_ptr<gl::gl_program>();
        return prog;
    }

    class fw_copy_shader : public shader
    {
    public: 
        fw_copy_shader()
        {
            prog_ = CreateProgram();
        }
        virtual GLuint get_handle()const
        {
            return prog_->get_handle();
        }
    private:
        std::shared_ptr< gl::gl_program > prog_;
    };

    static 
    std::shared_ptr< shader > GetShader()
    {
        static std::shared_ptr< shader > s = std::shared_ptr< shader >(new fw_copy_shader());
        return s;
    }

    class fw_copy_material : public material
    {
    public:
        fw_copy_material()
        {
            shader_ = GetShader();
            pos_vbo_ = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            uv_vbo_  = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            vao_     = std::shared_ptr<gl::gl_vertex_array> (new gl::gl_vertex_array());
            {
                static float posData[] = {
                    -1.0f, -1.0f, 0.0f, //0
                    -1.0f, +1.0f, 0.0f, //1
                    +1.0f, -1.0f, 0.0f, //2
                    +1.0f, +1.0f, 0.0f, //3
                };
                static float uvData[] = {
                    0.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 0.0f,
                    1.0f, 1.0f,
                };

                ::glBindBuffer(GL_ARRAY_BUFFER, pos_vbo_->get_handle());
                ::glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(float), posData, GL_STATIC_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0);

                ::glBindBuffer(GL_ARRAY_BUFFER, uv_vbo_->get_handle());
                ::glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(float), uvData, GL_STATIC_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0);
            }
            {
                GLuint handle = vao_->get_handle();
                ::glBindVertexArray(handle);
                {
                    GLuint index = ::glGetAttribLocation(get_shader()->get_handle(), "pos");
                    ::glBindBuffer(GL_ARRAY_BUFFER, pos_vbo_->get_handle());
                    ::glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                {
                    GLuint index = ::glGetAttribLocation(get_shader()->get_handle(), "uv");
                    ::glBindBuffer(GL_ARRAY_BUFFER, uv_vbo_->get_handle());
                    ::glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                {
                    ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
                }
                ::glBindVertexArray(0u);
            }
        }
        virtual const shader* get_shader()const{ return shader_.get(); }
        virtual GLuint get_vao_handle()const{ return vao_->get_handle(); }
    private: 
        std::shared_ptr< shader > shader_;
        std::shared_ptr<gl::gl_vertex_array>  vao_; 
        std::shared_ptr<gl::gl_vertex_buffer> pos_vbo_;
        std::shared_ptr<gl::gl_vertex_buffer> uv_vbo_; 
    };
    
    fw_renderer::fw_renderer(int width, int height)
    {
        rt_ = std::shared_ptr<render_target>(new fw_render_target(width, height));
        mt_ = std::shared_ptr<material>(new fw_copy_material());
        width_ = width;
        height_ = height;
    }

    void fw_renderer::render(
            const std::vector< std::shared_ptr<geometry> >& geos,
            const std::vector<std::shared_ptr<light> >& lights,
            const std::shared_ptr<camera>& cam)
    {
        std::vector<const render_node*>& rnodes = rnodes_;

        fw_render_context ctx;
        ctx.set_world_to_camera(cam->get_world_to_camera()*m_);
        ctx.set_camera_to_clip (cam->get_camera_to_clip());
        ctx.set_line_mode      (is_line_);

        fw_render_target* rt = (fw_render_target*)(rt_.get());

        int nMulti = 1;
        if(is_multi_ && !is_line_)nMulti=2;
        rt->refine(nMulti*width_, nMulti*height_);


        rnodes.clear();
        for(size_t i = 0;i<geos.size();i++)
        {
            geos[i]->register_node(&ctx, rnodes);
        }

        if(!rnodes.empty())
        {
            size_t sz = rnodes.size();
            
            static std::vector<rnode_tmp> rnode_d;     
            static std::vector<rnode_tmp> rnode_t;      
       
            if(!is_additive_)
            {
                rnode_d.clear();
                rnode_t.clear();
                matrix4 m0 = cam->get_camera_to_clip()*cam->get_world_to_camera()*m_;    
                for(size_t i = 0; i < sz;i++)
                {
                    rnode_tmp tmp;
                    matrix4 m = m0*rnodes[i]->get_local_to_world();
                    vector3 p = m*rnodes[i]->get_origin();
                    tmp.depth = p[2];
                    tmp.node = rnodes[i];
                    if(rnodes[i]->is_opacity())
                    {
                        rnode_d.push_back(tmp); 
                    }
                    else
                    {
                        rnode_t.push_back(tmp);
                    }
                }
                rnodes.clear();
                {
                    static node_sorter_d ns;
                    std::sort(rnode_d.begin(), rnode_d.end(), ns);
                }
                {
                    static node_sorter_t ns;
                    std::sort(rnode_t.begin(), rnode_t.end(), ns);
                }
                for(int i=0;i<rnode_d.size();i++)
                {
                    rnodes.push_back(rnode_d[i].node);    
                }
                for(int i=0;i<rnode_t.size();i++)
                {
                    rnodes.push_back(rnode_t[i].node);    
                }
            }

            {
                #define MAX_LIGHT_EACH_OBJECT 4
                #define MAX_LIGHT_RECEIVABLE_OBJECT 100

                struct LightData
                {
                    int type;               //spot or point or direction
                    int has_shadow;         //shadow texture_unit;
                    float inner_angle;      //
                    float outer_angle;      //
                    float origin[4];        //xyz
                    float direction[4];     //
                    float color [4];        //
                };

                struct LightContainer
                {
                    int counts    [MAX_LIGHT_RECEIVABLE_OBJECT];                                                        //
                    LightData data[MAX_LIGHT_EACH_OBJECT*MAX_LIGHT_RECEIVABLE_OBJECT];      //
                };

                for(int i = 0;i<lights.size();i++)
                {
                    const light* l = lights[i].get();
                    if(l->get_light_type() == light::SPOT) //l->type == light::LIGHT_TYPE::SPOT
                    {
                        LightData data;
                        data.type = 1;
                        data.has_shadow = -1;
                        data.inner_angle = 1;
                    }
                }
            }
            
            if(!ctx.is_line_mode())
                rt_->bind();

            glClearColor(0,0,0,0);
            glDepthMask(GL_TRUE);
            glEnable   (GL_DEPTH_TEST);
            glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            
            
            glViewport (0, 0, nMulti*width_, nMulti*height_);
            
            //glEnable(GL_LINE_SMOOTH);
            //glLineWidth(8);

            const material* prev_mtl = NULL;
            const shader*   prev_shd = NULL;
            for(size_t i = 0; i < rnodes.size();i++)
            {
                const render_node* node = rnodes[i];
                const material* mtl = node->get_material();
                if(node && mtl)
                {
                    bool isDepth = node->is_opacity();
                    if(isDepth && !is_additive_)
                    {
                        glDepthMask(GL_TRUE);
                        glEnable   (GL_DEPTH_TEST); // enable depth-testing
                        glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
                        
                        glDisable(GL_BLEND);
                        //glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE);
                    }
                    else
                    {                        
                        glEnable   (GL_DEPTH_TEST); // enable depth-testing
                        glDepthFunc(GL_LEQUAL);
                        
                        glEnable(GL_BLEND);
                        if(!is_additive_)
                        {
                            glDepthMask(GL_TRUE);
                            glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ONE);
                        }
                        else
                        {
                            glDepthMask(GL_FALSE);
                            glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE, GL_ONE, GL_ONE);
                        }
                    }
                    
                    const shader* shd = mtl->get_shader();
                    if(shd)
                    {
                        if(prev_shd != shd)
                        {
                            ::glUseProgram(shd->get_handle());
                        }
                        
                        {
                            matrix4 m = node->get_local_to_world();
                            m.transpose();
                            GLint loc = ::glGetUniformLocation(shd->get_handle(), "local2world");
                            ::glUniformMatrix4fv(loc, 1, GL_FALSE, &m[0][0]);
                        }
                        
                        ctx.set_world_to_camera(cam->get_world_to_camera()*m_);   

                        mtl->bind_uniforms(&ctx);
                        
                        ::glBindVertexArray(node->get_vertex_array_handle());
                        if(ctx.is_line_mode())
                        {
                            ::glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
                        }
                        else
                        {
                            ::glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
                        }
                        //::glDrawArrays(GL_TRIANGLES, 0, node->get_vertex_count());
                        node->draw();
                    }
                }
            }
            ::glBindVertexArray(0u);
            ::glUseProgram(0u);
            
            glDepthMask(GL_FALSE);
            glDisable  (GL_DEPTH_TEST); // enable depth-testing

            if(!ctx.is_line_mode())
                rt_->unbind();

            
            if(!ctx.is_line_mode())
            {
                
                ::glClearColor(0, 0, 0, 0);
                ::glClear(GL_COLOR_BUFFER_BIT);
                ::glViewport (0, 0, width_, height_);
                ::glDisable(GL_DEPTH_TEST);
                ::glDisable(GL_LIGHTING);
                ::glDepthMask(GL_FALSE);
                ::glDisable(GL_BLEND);
                ::glBlendFunc(GL_ONE, GL_ZERO);

                GLuint shader_prog = mt_->get_shader()->get_handle();
                ::glUseProgram(shader_prog);

                //this->SetUniformParameters();
                GLuint tex_handle = ((const fw_render_target*)(rt_.get()))->get_color_texture_handle(0);
                GLuint nLoc = ::glGetUniformLocation(shader_prog, "tex");
                ::glActiveTexture(GL_TEXTURE0 + 0);
                ::glBindTexture(GL_TEXTURE_2D, tex_handle);
                ::glUniform1i(nLoc, 0);

                GLuint vaoHandle = ((const fw_copy_material*)mt_.get())->get_vao_handle();

                ::glBindVertexArray(vaoHandle);
                ::glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                ::glBindVertexArray(0);

                ::glUseProgram(0);

                ::glFlush();
            }
        }
    }

}
