#ifndef _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES
#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES 1
#endif

#include "logger.h"
#include <string>
#include <memory>

#include <stdio.h>

#include <stdarg.h>

#if defined(_MSC_VER)
#if (_MSC_VER < 1400) //msc_ver < vc2005
#define USE_KZVSNPRINTF()                          \
    _vsnprintf(buffer, sizeof(buffer), str, list); \
    buffer[sizeof(buffer) - 1] = 0
#else
#define USE_KZVSNPRINTF()                                                \
    _vsnprintf_s(buffer, sizeof(buffer), sizeof(buffer) - 1, str, list); \
    buffer[sizeof(buffer) - 1] = 0
#endif
#else
#define USE_KZVSNPRINTF()                         \
    vsnprintf(buffer, sizeof(buffer), str, list); \
    buffer[sizeof(buffer) - 1] = 0
#endif

namespace gale
{
    void multi_logger::add(logger* lgr)
    {
        lgrlist_.push_back(lgr);
    }

    void multi_logger::print(const char* cstr)
    {
        std::size_t sz = lgrlist_.size();

        for (std::size_t i = 0; i < sz; i++)
        {
            lgrlist_[i]->print(cstr);
        }
    }

    multi_logger::~multi_logger()
    {
        std::size_t sz = lgrlist_.size();

        for (std::size_t i = 0; i < sz; i++)
        {
            delete lgrlist_[i];
        }
    }

    //--------------------------------------------------------------------------------

    logger_set::~logger_set()
    {
        std::size_t sz = lgrlist_.size();

        for (std::size_t i = 0; i < sz; i++)
        {
            delete lgrlist_[i].p_logger;
        }
    }

    void logger_set::add(const char* key, logger* lgr)
    {
        LnK tmp;
        tmp.key = key;
        tmp.p_logger = lgr;
        lgrlist_.push_back(tmp);
    }

    void logger_set::erase(const char* key)
    {
        std::string skey(key);
        std::vector<LnK> tmp;
        std::size_t sz = lgrlist_.size();
        for (std::size_t i = 0; i < sz; i++)
        {
            if (lgrlist_[i].key != skey)
            {
                tmp.push_back(lgrlist_[i]);
            }
            else
            {
                delete lgrlist_[i].p_logger;
            }
        }

        lgrlist_.swap(tmp);
    }

    void logger_set::print(const char* cstr)
    {
        std::size_t sz = lgrlist_.size();

        for (std::size_t i = 0; i < sz; i++)
        {
            lgrlist_[i].p_logger->print(cstr);
        }
    }

    static std::unique_ptr<logger_set> g_plogger(new logger_set());
    static std::unique_ptr<logger_set> g_plogger_debug(new logger_set());
    static std::unique_ptr<logger_set> g_plogger_progress(new logger_set());

    logger* get_logger() { return g_plogger.get(); }
    logger* get_logger_debug() { return g_plogger_debug.get(); }
    logger* get_logger_progress() { return g_plogger_progress.get(); }

    void set_logger(const char* key, logger* lgr)
    {
        if (lgr)
        {
            g_plogger->add(key, lgr);
        }
        else
        {
            g_plogger->erase(key);
        }
    }
    void set_logger_debug(const char* key, logger* lgr)
    {
        if (lgr)
        {
            g_plogger_debug->add(key, lgr);
        }
        else
        {
            g_plogger_debug->erase(key);
        }
    }
    void set_logger_progress(const char* key, logger* lgr)
    {
        if (lgr)
        {
            g_plogger_progress->add(key, lgr);
        }
        else
        {
            g_plogger_progress->erase(key);
        }
    }

    void print_log(const char* str, ...)
    {
        char buffer[1024];
        using namespace std;
        va_list list;
        va_start(list, str);

        USE_KZVSNPRINTF();

        va_end(list);
        get_logger()->print(buffer);
    };

    void print_debug(const char* str, ...)
    {
#ifndef KAZE_NDEBUG
        char buffer[1024];
        using namespace std;
        va_list list;
        va_start(list, str);

        USE_KZVSNPRINTF();

        va_end(list);
        get_logger_debug()->print(buffer);
#endif //KAZE_NDEBUG
    };

    void print_progress(const char* str, ...)
    {
        char buffer[1024];
        using namespace std;
        va_list list;
        va_start(list, str);

        USE_KZVSNPRINTF();

        va_end(list);

        std::string s;
        //s += "                                          \r";
        s += buffer;
        s += "                                          \r";

        get_logger_progress()->print(s.c_str());
    };
}
