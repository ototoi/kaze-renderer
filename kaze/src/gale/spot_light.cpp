#include "spot_light.h"
#include "render_node.h"
#include "shader.h"
#include "material.h"
#include "logger.h"
#include "values.h"

#include "gl/gl_program.h"
#include "gl/gl_vertex_buffer.h"
#include "gl/gl_vertex_array.h"

namespace gale
{
    static
    std::string GetFSString()
    {
        // "  color = vec4(vec3(0.5)*(normalize(vs_normal)+vec3(1)),1);\n"
        // "  color = vec4(vs_uv,0.01,1);\n"
        static const char* STR =
        "#version 410\n"
        "\n"
        "in vec3 vs_position;\n"
        "in vec3 vs_normal;\n"
        "in vec2 vs_uv;\n"
        "\n"
        "out vec4 color;\n"
        "\n"
        "uniform mat4 local2world;\n"
        "uniform mat4 world2camera;\n"
        "uniform mat4 camera2clip;\n"
        "\n"
        "void main(void) {\n"
        "  vec3 n = normalize(vs_normal);\n"
        "  color = vec4(vec3(0.5)*(n+vec3(1)),1);\n"
        "}\n"
        "\n";
        return STR;
    }
    static
    std::string GetVSString()
    {
        static const char* STR =
        "#version 410\n"
        "\n"
        "layout (location = 0) in vec3 position;\n"
        "\n"
        "out vec3 vs_position;\n"
        "out vec3 vs_normal;\n"
        "out vec2 vs_uv;\n"
        "\n"
        "uniform mat4 local2world;\n"
        "uniform mat4 world2camera;\n"
        "uniform mat4 camera2clip;\n"
        "\n"
        "vec3 tovec3(vec4 v){return v.xyz/v.w;}\n"
        "void main(void) {\n"
        "  mat4 nmat = transpose(inverse(world2camera*local2world));\n"
        "  vs_position = tovec3(world2camera*local2world * vec4(position,1));\n"
        "  vec3 n = normalize((nmat * vec4(normal,0)).xyz);\n"
        "  if(dot(normalize(vs_position),n)>0)n*=-1;"
        "  vs_normal = n;\n"
        "  vs_uv = uv;\n"
        "  gl_Position = camera2clip * world2camera * local2world * vec4(position,1);\n"
        "}\n"
        "\n";
        return STR;
    }

    static
    std::shared_ptr<gl::gl_program> CreateProgram()
    {
        std::shared_ptr<gl::gl_shader> fs(new gl::gl_shader(GL_FRAGMENT_SHADER));
        bool bFS = fs->compile_from_string(GetFSString());
        std::shared_ptr<gl::gl_shader> vs(new gl::gl_shader(GL_VERTEX_SHADER));
        bool bVS = vs->compile_from_string(GetVSString());
        if(!bFS || !bVS)return std::shared_ptr<gl::gl_program>();
        std::shared_ptr<gl::gl_program> prog(new gl::gl_program());
        prog->add_shader(vs);
        prog->add_shader(fs);
        bool bLink = prog->link();
        if(!bLink)return std::shared_ptr<gl::gl_program>();
        return prog;
    }

    class spot_light_shader : public shader
    {
    public:
        spot_light_shader()
        {
            prog_ =  CreateProgram();
        }
        virtual GLuint get_handle()const
        {
            return prog_->get_handle();
        }
    private:
        std::shared_ptr<gl::gl_program> prog_;
    };

    static
    std::shared_ptr<shader> CreateShader()
    {
        return std::shared_ptr<shader>(new spot_light_shader());
    }

    class spot_light_material : public material
    {
    public:
        spot_light_material()
        {
            shader_ = CreateShader();
        }
        virtual const shader* get_shader()const
        {
            return shader_.get();
        }
    private:
        std::shared_ptr<shader> shader_;
    };

    static
    std::shared_ptr<material> CreateMaterial()
    {
        static std::shared_ptr<material> mtl(new spot_light_material());
        return mtl;
    }

    class spot_light_render_node : public render_node
    {
    public:
        spot_light_render_node()
        {
            ;//
        }
        spot_light_render_node
        (
            const float* vertices, int vsz,
            const int*   indices,  int isz,
            const matrix4& mat
        )
        {
            vsz_ = vsz;
            isz_ = isz;
            l2w_ = mat;

            vector3 org = vector3(0,0,0);
            for(int i=0;i<vsz/3;i++)
            {
                org += vector3(vertices + 3*i);
            }
            org *= (3.0/vsz);
            org_ = org;

            //mtl_ = CreateMaterial();

            position_vbo_ = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            index_vbo_    = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            vao_ = std::shared_ptr<gl::gl_vertex_array>(new gl::gl_vertex_array());

            {
                GLuint handle = position_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*vsz, vertices, GL_STATIC_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                GLuint handle = index_vbo_->get_handle();
                ::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*isz, indices, GL_STATIC_DRAW);
                ::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0u);
            }
            {
                GLuint handle = vao_->get_handle();
                ::glBindVertexArray(handle);
                {
                    GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "position");
                    ::glBindBuffer(GL_ARRAY_BUFFER, position_vbo_->get_handle());
                    ::glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                {
                    GLuint handle = index_vbo_->get_handle();
                    ::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle);
                }
                {
                    ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
                }
                ::glBindVertexArray(0u);
            }
        }
        vector3 get_origin()const{return org_;}
        matrix4 get_local_to_world()const{return l2w_;}
        const material* get_material()const{return mtl_.get();}
        GLuint  get_vertex_array_handle()const{return vao_->get_handle();}

        GLuint  get_vertices_handle()const{return position_vbo_->get_handle();}
        GLsizei get_vertices_count ()const{return vsz_/3;}
        GLuint  get_indices_handle ()const{return index_vbo_->get_handle();}
        GLsizei get_indices_count  ()const{return isz_;}
        void    draw               ()const{::glDrawElements(GL_TRIANGLES, isz_, GL_UNSIGNED_INT, 0 );}
    private:
        std::shared_ptr<material> mtl_;
        std::shared_ptr<gl::gl_vertex_buffer> position_vbo_;
        std::shared_ptr<gl::gl_vertex_buffer> index_vbo_;
        std::shared_ptr<gl::gl_vertex_array>  vao_;
        int vsz_;
        int isz_;
        vector3 org_;
        matrix4 l2w_;
    };
    
    static
    void CreateMesh(
        std::vector<vector3>& positins, std::vector<int>& indices,
        const vector3& from, const vector3& to, float inner, float outer
    )
    {
        static const int DIV = 32;
       
        float delta_phi = 2 * values::pi() / DIV;
        float phi_length = cos(0.5f * delta_phi);
        float alpha = 1.0f / phi_length;
        float angle = std::min<float>(outer, values::radians(189));
        float len   = cos(0.5f*angle)/sin(0.5f*angle);
        angle = 2*atan2(alpha, len);
        angle = std::min<float>(angle, values::radians(189));
        /*
        |
        |
      1 |
        |
        |_______ sin(angle/2)
        */
        positins.resize(DIV+1);
        positins[0] = vector3(0,0,0);
        float l = sin(0.5f*angle);
        for(int i = 0;i<DIV;i++)        
        {
            positins[i+1] = l*vector3(cos(i*delta_phi),sin(i*delta_phi), 1);
        }
    }

    static
    const float* Copy(float f[3], const vector3& v)
    {
        f[0]=v[0];
        f[1]=v[1];
        f[2]=v[2];
        return f;
    }

    spot_light::spot_light(const vector3& from, const vector3& to, float inner, float outer, const matrix4& mat)
        :node_(NULL)
    {
        float tmp[16] = {};
        params_.set("from", Copy(tmp, from), 3);
        params_.set("to",   Copy(tmp, to), 3);
        params_.set("inner", inner);
        params_.set("outer", outer);
        params_.set("matrix", &mat[0][0], 16);
        color3 col = color3(1,1,1);
        params_.set("color", Copy(tmp, col), 3);
        this->initialize();
    }
    spot_light::~spot_light()
    {
        if(node_)
        {
            delete node_;
        }
    }
    const float* spot_light::get_float(const char* szKey)const
    {
        const parameters::value_type* p_val = params_.get(szKey);
        if(p_val && p_val->nType == parameters::TYPE_FLOAT)
        {
            return &p_val->float_values[0];
        }
        return NULL;
    }
    void spot_light::initialize()
    {
        if(!node_)
        {
            node_ = new spot_light_render_node();
        }
    }
    void spot_light::update()
    {
        if(!node_)
        {
            this->initialize();
        }
    }
    void spot_light::register_node(const render_context* ctx, std::vector<const render_node*> & rnodes)const
    {
        if(node_)
        {
            rnodes.push_back(node_);
        }
    }
}
