#include "perspective_camera.h"
#include <cmath>

namespace gale
{
    using namespace std;

    static
    matrix4 camera2clip_GL(float l, float r, float b, float t, float n, float f)
    {
        return matrix4(
            (2*n)/(r-l), 0,            -(r+l)/(r-l), 0,
            0,           (2*n)/(t-b),  -(t+b)/(t-b), 0,
            0,           0,            (f+n)/(f-n),  -(2*f*n)/(f-n),
            0,           0,            1,            0);
        //n: n*(f+n)/(f-n) + (-(2*f*n)/(f-n))   / n
        //    (f+n -2*f)/ (f-n) 
        //    (n-f)/(f-n)
        //    -1
        //f: f*(f+n)/(f-n) + (-(2*f*n)/(f-n))   / f
        //    (f+n -(2*n)) /(f-n) 
        //    (f-n)/(f-n)
        //    +1
        //l,0,n:  (l*(2*n)/(r-l) - n*(r+l)/(r-l) )/ n
        //     (2l - (r+l))/(r-l)
        // -1
    }

    static
    matrix4 camera2clip_GL(float angle, float aspect, float n, float f)
    {
        //(-1..+1), (-1..+1), (-1..+1)
        float h = std::tan(angle / 2);
        float w = aspect * h;
        float l = -w * n;
        float r = +w * n;
        float b = -h * n;
        float t = +h * n;
        return camera2clip_GL(l,r,b,t,n,f);
    }

    static
    matrix4 camera2clip_GL(float angle, float aspect, float ll, float rr, float bb, float tt, float n, float f)
    {
        //(-1..+1), (-1..+1), (-1..+1)
        float h = std::tan(angle / 2);
        float w = aspect * h;
        float l = -w * n * (ll/-aspect);//float l = h * n * ll;
        float r = +w * n * (rr/+aspect);//float r = h * n * rr;
        float b = -h * n * (bb/-1);     //float b = h * n * bb;
        float t = +h * n * (tt/+1);     //float t = h * n * tt;
        return camera2clip_GL(l,r,b,t,n,f);
    }

    perspective_camera::perspective_camera(const matrix4& mat, float angle, float aspect, float l, float r, float b, float t, float n, float f)
    {
        params_.set("angle",  angle);
        params_.set("aspect", aspect);
        params_.set("l", l);
        params_.set("r", r);
        params_.set("b", b); 
        params_.set("t", t);
        params_.set("n", n); 
        params_.set("f", f);
        matrix4 w2c = mat;
        params_.set("w2c", &w2c[0][0], 16);

        vector4 orgw = ~w2c * vector4(0,0,0,1);
        vector3 org = vector3(orgw[0],orgw[1],orgw[2]) * (1.0f / orgw[3]);
        params_.set("org", &org[0], 3);

        params_.set("pan_x", 0.0f);
        params_.set("pan_y", 0.0f);

        initialize();
    }

    void perspective_camera::initialize()
    {
        matrix4 c2c = camera2clip_GL(get_angle(), get_aspect(), get_l(), get_r(), get_b(), get_t(), get_near(), get_far());
        params_.set("c2c", &c2c[0][0], 16);
    }

    matrix4 perspective_camera::get_world_to_camera()const
    {
        const float* f = &(params_.get("w2c")->float_values[0]);
        return matrix4(f);
    }

    matrix4 perspective_camera::get_camera_to_clip()const
    {
        const float* f = &(params_.get("c2c")->float_values[0]);
        return get_pan()*matrix4(f);
    }

    matrix4 perspective_camera::get_pan()const
    {
        float px = params_.get("pan_x")->float_values[0];
        float py = params_.get("pan_y")->float_values[0];
        return mat4_gen::translation(px, py, 0);
    }

    void perspective_camera::set_angle(float f)
    {
        params_.set("angle", f);
        initialize();
    }

    void perspective_camera::set_pan  (float x, float y)
    {
        params_.set("pan_x", x);
        params_.set("pan_y", y);
        initialize();
    }

    float perspective_camera::get_angle()const
    {
        return params_.get("angle")->float_values[0];
    }

    float perspective_camera::get_aspect()const
    {
        return params_.get("aspect")->float_values[0];
    }

    float perspective_camera::get_l()const
    {
        return params_.get("l")->float_values[0];
    }
    float perspective_camera::get_r()const
    {
        return params_.get("r")->float_values[0];
    }
    float perspective_camera::get_b()const
    {
        return params_.get("b")->float_values[0];
    }
    float perspective_camera::get_t()const
    {
        return params_.get("t")->float_values[0];
    }

    float perspective_camera::get_near()const
    {
        return params_.get("n")->float_values[0];
    }

    float perspective_camera::get_far()const
    {
        return params_.get("f")->float_values[0];
    }

    vector3 perspective_camera::get_origin()const
    {
        const float* f = &(params_.get("org")->float_values[0]);
        return vector3(f);
    }

}
