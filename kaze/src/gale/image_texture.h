#ifndef GALE_IMAGE_TEXTURE_H
#define GALE_IMAGE_TEXTURE_H

#include "texture.h"

namespace gale
{
    class image_texture : public texture
    {
    public:
        image_texture(float r, float g, float b, float a = 1);
        image_texture(const char* szPath);
        ~image_texture();
        virtual GLuint get_handle()const;
        virtual int get_width()const;
        virtual int get_height()const;
    private:
        std::shared_ptr<gl::gl_texture_2D> tex_;
        int width_;
        int height_;
    };
}

#endif