#include "points_geometry.h"
#include "test0_geometry.h"
#include "render_node.h"
#include "shader.h"
#include "basic_material.h"
#include "logger.h"

#include "gl/gl_program.h"
#include "gl/gl_vertex_buffer.h"
#include "gl/gl_vertex_array.h"

namespace gale
{
    static
    std::string GetFSString()
    {
        // "  color = vec4(vec3(0.5)*(normalize(vs_normal)+vec3(1)),1);\n"
        // "  color = vec4(vs_uv,0.01,1);\n"
        static const char* STR =
        "#version 410\n"
        "\n"
        "in vec2 vs_uv;\n"
        "in vec3 vs_color;\n"
        "\n"
        "out vec4 color;\n"
        "\n"
        "uniform mat4 local2world;\n"
        "uniform mat4 world2camera;\n"
        "uniform mat4 camera2clip;\n"
        "\n"
        "void main(void) {\n"
        "  vec2 t = 2.0*vs_uv-vec2(1);\n"
        "  float r = sqrt(dot(t,t));\n"
        "  if(r>1)discard;\n"
        "  float th = 0.9;\n"
        "  float a = (r > th) ? 1.0-(1.0/(1.0-th))*(r-th):1.0;\n"
        "  color = vec4(vs_color,0.8*a);\n"
        "}\n"
        "\n";
        return STR;
    }
    static
    std::string GetVSString()
    {
        static const char* STR =
        "#version 410\n"
        "\n"
        "layout (location = 0) in vec3 sprite_vertex;\n"
        "layout (location = 1) in vec3 position;\n"
        "layout (location = 2) in vec2 width;\n"
        "layout (location = 3) in vec3 color;\n"
        "\n"
        "out vec2 vs_uv;\n"
        "out vec3 vs_color;\n"
        "\n"
        "uniform mat4 local2world;\n"
        "uniform mat4 world2camera;\n"
        "uniform mat4 camera2clip;\n"
        "\n"
        "vec3 tovec3(vec4 v){return v.xyz/v.w;}\n"
        "void main(void) {\n"
        "  vec4 pos        = local2world*vec4(position,1);\n"
        "  vec4 poscamera  = world2camera * pos + vec4(sprite_vertex.xy * width.xy,0,0);\n"
        "  vs_uv = sprite_vertex.xy + vec2(0.5);\n"
        "  vs_color = color;\n"
        "  gl_Position = camera2clip * poscamera;\n"
        "}\n"
        "\n";
        return STR;
    }

    static
    std::shared_ptr<gl::gl_program> CreateProgram()
    {
        std::shared_ptr<gl::gl_shader> fs(new gl::gl_shader(GL_FRAGMENT_SHADER));
        bool bFS = fs->compile_from_string(GetFSString());
        std::shared_ptr<gl::gl_shader> vs(new gl::gl_shader(GL_VERTEX_SHADER));
        bool bVS = vs->compile_from_string(GetVSString());
        if(!bFS || !bVS)return std::shared_ptr<gl::gl_program>();
        std::shared_ptr<gl::gl_program> prog(new gl::gl_program());
        prog->add_shader(vs);
        prog->add_shader(fs);
        bool bLink = prog->link();
        if(!bLink)return std::shared_ptr<gl::gl_program>();
        return prog;
    }

    class points_shader : public shader
    {
    public:
        points_shader()
        {
            prog_ =  CreateProgram();
        }
        virtual GLuint get_handle()const
        {
            return prog_->get_handle();
        }
    private:
        std::shared_ptr<gl::gl_program> prog_;
    };

    static
    std::shared_ptr<shader> CreateShader()
    {
        return std::shared_ptr<shader>(new points_shader());
    }

    class points_material : public basic_material
    {
    public:
        points_material()
        {
            shader_ = CreateShader();
        }
        virtual const shader* get_shader()const
        {
            return shader_.get();
        }
    private:
        std::shared_ptr<shader> shader_;
    };

    static
    std::shared_ptr<material> CreateMaterial()
    {
        return std::shared_ptr<material>(new points_material());
    }

    class container_points_render_node : public render_node
    {
    public:
        container_points_render_node
        (
            int nverts,
            const float* positions,     //vec3
            const float* widths,        //vec2
            const float* colors,     //vec3
            const matrix4& mat
        )
        {
            nverts_ = nverts;
            l2w_ = mat;

            vector3 org = vector3(0,0,0);
            for(int i=0;i<nverts;i++)
            {
                org += vector3(positions + 3*i);
            }
            org *= 1.0f/nverts;
            org_ = org;

            mtl_ = CreateMaterial();

            sprite_vbo_   = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            position_vbo_ = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            width_vbo_    = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            color_vbo_   = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            
            vao_ = std::shared_ptr<gl::gl_vertex_array>(new gl::gl_vertex_array());
            
            {
                static const GLfloat g_vertex_buffer_data[] = {
                    -0.5f, -0.5f, 0.0f,
                    0.5f, -0.5f, 0.0f,
                    -0.5f, 0.5f, 0.0f,
                    0.5f, 0.5f, 0.0f,
                };
                
                GLuint handle = sprite_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*4*3, g_vertex_buffer_data, GL_STATIC_DRAW);
                //::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }

            {
                GLuint handle = position_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*nverts*3, positions, GL_STREAM_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                GLuint handle = width_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*nverts*2, widths,    GL_STREAM_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                GLuint handle = color_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*nverts*3, colors, GL_DYNAMIC_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                GLuint handle = vao_->get_handle();
                ::glBindVertexArray(handle);
                
                {
                    GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "sprite_vertex");
                    ::glBindBuffer(GL_ARRAY_BUFFER, sprite_vbo_->get_handle());
                    ::glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                
                {
                    GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "position");
                    ::glBindBuffer(GL_ARRAY_BUFFER, position_vbo_->get_handle());
                    ::glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                
                {
                    GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "width");
                    ::glBindBuffer(GL_ARRAY_BUFFER, width_vbo_->get_handle());
                    ::glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                
                {
                    GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "color");
                    ::glBindBuffer(GL_ARRAY_BUFFER, color_vbo_->get_handle());
                    ::glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                
                {
                    ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
                }
                ::glBindVertexArray(0u);
            }
        }
        vector3 get_origin()const{return org_;}
        bool    is_opacity()const{return false;}
        matrix4 get_local_to_world()const{return l2w_;}
        const material* get_material()const{return mtl_.get();}
        GLuint  get_vertex_array_handle()const{return vao_->get_handle();}

        GLuint  get_vertices_handle()const{return position_vbo_->get_handle();}
        GLsizei get_vertices_count ()const{return nverts_;}
        void    draw               ()const
        {
            ::glVertexAttribDivisor(0, 0); // 粒子の頂点：同じ4頂点を使いまわすので->0
            ::glVertexAttribDivisor(1, 1); // 位置：四角形ごとに一つ（中心）->1
            ::glVertexAttribDivisor(2, 1); // 幅：四角形ごとに一つ->1
            ::glVertexAttribDivisor(3, 1); // 色：四角形ごとに一つ->1

            ::glDrawArraysInstanced(GL_TRIANGLE_STRIP, 0, 4, nverts_);
        }
    public:
        void refine(
            int nverts,
            const float* positions,     //vec3
            const float* widths,        //vec2
            const float* colors,     //vec3
            const matrix4& mat
        )
        {
            {
                GLuint handle = position_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*nverts*3, positions, GL_STREAM_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                GLuint handle = width_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*nverts*2, widths,    GL_STREAM_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                GLuint handle = color_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*nverts*3, colors, GL_STREAM_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            nverts_ = nverts;
            l2w_ = mat;
        }
        
    private:
        std::shared_ptr<material> mtl_;
        std::shared_ptr<gl::gl_vertex_buffer> sprite_vbo_;
        std::shared_ptr<gl::gl_vertex_buffer> position_vbo_;
        std::shared_ptr<gl::gl_vertex_buffer> width_vbo_;
        std::shared_ptr<gl::gl_vertex_buffer> color_vbo_;
        std::shared_ptr<gl::gl_vertex_array>  vao_;
        int nverts_;
        vector3 org_;
        matrix4 l2w_;
    };

    points_geometry::points_geometry(
        const std::vector<vector3>& positions,
        const std::vector<vector2>& widths,
        const std::vector<color3>& colors,
        const matrix4& mat)
        :sort_mode_(SORT_OWN), blend_mode_(BLEND_ALPHA), positions_(positions), widths_(widths), colors_(colors), mat_(mat)
    {
        this->initialize();
    }
    points_geometry::~points_geometry()
    {
        if(!nodes_.empty())
        {
            size_t sz = nodes_.size();
            for(size_t i = 0;i<sz;i++)
            {
                delete nodes_[i];
            }
            nodes_.clear();
        }
    }

    void points_geometry::initialize()
    {
        if(nodes_.empty())
        {
            const float* positions = (const float*)(&positions_[0][0]);
            const float* width     = (const float*)(&widths_[0][0]);
            const float* colors    = (const float*)(&colors_[0][0]);
            int vsz = (int)positions_.size();

            nodes_.push_back(new container_points_render_node(vsz, positions, width, colors, mat_));
        }
    }

    void points_geometry::update()
    {
        this->initialize();
    }

    namespace 
    {    
        struct particle
        {
            float depth;
            vector3 pos;
            vector2 width;
            color3  color;
        };
        
        struct particle_sorter
        {
            bool operator()(const particle& a, const particle& b)const
            {
                return a.depth > b.depth;
            }    
        };
        
        static 
        void SortParticle(
            std::vector<vector3>& positions,
            std::vector<vector2>& widths,
            std::vector<color3>& colors,
            const matrix4& mat
        )
        {
            static particle_sorter sorter;
            
            size_t sz = positions.size();
            std::vector<particle> pars(sz);
            for(size_t i = 0;i<sz;i++)
            {
                pars[i].pos = positions[i];
                pars[i].width = widths[i];
                pars[i].color = colors[i];
                pars[i].depth = (mat*positions[i])[2];
            }
            std::stable_sort(pars.begin(), pars.end(), sorter);
            for(size_t i = 0;i<sz;i++)
            {
                positions[i] = pars[i].pos;
                widths[i] = pars[i].width;
                colors[i] = pars[i].color;
            }
        }
    }
    
    typedef render_node* PCRNODE;
    void points_geometry::register_node(const render_context* ctx, std::vector<const render_node*>& rnodes)const
    {
        if(!nodes_.empty())
        {
            bool is_sort = (this->sort_mode_ != points_geometry::SORT_NONE);
            
            if(is_sort)
            {
                matrix4 mat = ctx->get_world_to_camera() * mat_;
                SortParticle(positions_, widths_, colors_, mat);
            }
            
            size_t sz = nodes_.size();
            const PCRNODE* nodes = &nodes_[0];
            for(size_t i = 0;i<sz;i++)
            {
                container_points_render_node* node = (container_points_render_node*)nodes[i];
                if(is_sort)
                {
                    const float* p = (const float*)(&positions_[0][0]);
                    const float* w = (const float*)(&widths_[0][0]);
                    const float* c = (const float*)(&colors_[0][0]);
                    int vsz = (int)positions_.size();
                    node->refine(vsz, p, w, c, mat_);
                }
                rnodes.push_back(node);
            }
        }
    }
}
