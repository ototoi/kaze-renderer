#ifndef GALE_MATERIAL_H
#define GALE_MATERIAL_H

#include "shader.h"
#include "texture.h"
#include "render_context.h"

namespace gale
{
    class shader;
    class material
    {
    public:
        virtual ~material(){}
        virtual const shader* get_shader()const = 0;
        virtual void set_texture(const char* key, const std::shared_ptr<texture>& tex){}
        virtual void bind_uniforms(const render_context* ctx)const{}
    };
}

#endif
