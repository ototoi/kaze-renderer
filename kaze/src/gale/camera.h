#ifndef GALE_CAMERA_H
#define GALE_CAMERA_H

#include "types.h"

namespace gale
{
    class camera
    {
    public:
        virtual ~camera(){}
        virtual matrix4 get_world_to_camera()const = 0;
        virtual matrix4 get_camera_to_clip ()const = 0;
    };
}

#endif
