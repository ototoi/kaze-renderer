#ifndef GALE_SMESH_GEOMETRY_H
#define GALE_SMESH_GEOMETRY_H

#include <vector>
#include "geometry.h"
#include "material.h"
#include "basic_material.h"

namespace gale
{
    class smesh_render_node;
    
    class smesh_material : public basic_material
    {
    public:
        smesh_material();
    };
    
    class smesh_geometry : public geometry
    {
    public:
        smesh_geometry(
            const std::vector<vector3>& vertices,
            const std::vector<vector3>& normals,
            const std::vector<vector2>& uvs,
            const std::vector<int>& indices, const matrix4& mat);
        smesh_geometry(
            const std::vector<vector3>& vertices,
            const std::vector<vector3>& normals,
            const std::vector<vector3>& colors,
            const std::vector<vector2>& uvs,
            const std::vector<int>& indices, const matrix4& mat);
        ~smesh_geometry();
        void set_material(const std::shared_ptr<material>& mtl);
    public:
        virtual void update();
        virtual void register_node(const render_context* ctx, std::vector<const render_node*>& rnodes)const;
    protected:
        void initialize();
    private:
        std::vector<vector3> vertices_;
        std::vector<vector3> normals_;
        std::vector<vector3> colors_;
        std::vector<vector2> uvs_;
        std::vector<int>     indices_;
        matrix4 mat_;
        smesh_render_node*   node_;
    };
}

#endif
