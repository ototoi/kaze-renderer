#ifndef GALE_CURVES_GEOMETRY_H
#define GALE_CURVES_GEOMETRY_H

#include <vector>
#include "geometry.h"

namespace gale
{
    class curves_geometry : public geometry
    {
    public:
        struct curve_vertex
        {
            vector3 position;
            vector3 normal;
            float width;
            color3 color;
        };
        struct curve_strand
        {
            std::vector<curve_vertex> vertices;
            bool has_normal;
        };
    public:
        curves_geometry(
            const std::vector< std::shared_ptr<curve_strand> >& strands,
            const matrix4& mat
        );
        ~curves_geometry();
    public:
        virtual void update();
        virtual void register_node(const render_context* ctx, std::vector<const render_node*>& rnodes)const;
    public:
        void initialize();
    private:
        std::vector< std::shared_ptr<curve_strand> > strands_;
        matrix4 mat_;
        std::vector<render_node*>   nodes_;
    };
}

#endif
