#ifndef GALE_TEXTURE_H
#define GALE_TEXTURE_H

#include "types.h"
#include "gl/gl_texture.h"

namespace gale
{
    class texture
    {
    public:
        virtual ~texture(){}
        virtual GLuint get_handle()const = 0;
    };
}

#endif
