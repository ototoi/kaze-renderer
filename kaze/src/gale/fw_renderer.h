#ifndef GALE_FW_RENDERER_H
#define GALE_FW_RENDERER_H

#include <vector>
#include "geometry.h"
#include "light.h"
#include "camera.h"
#include "render_target.h"

namespace gale
{
    class render_node;
    class fw_renderer
    {
    public: 
        fw_renderer(int width, int height);
        void render(
            const std::vector< std::shared_ptr<geometry> >& geos,
            const std::vector<std::shared_ptr<light> >& lights,
            const std::shared_ptr<camera>& cam);
    public: 
        void set_rotation(const matrix4& m){m_ = m;};
        void set_additive(bool b){is_additive_=b;}
        void set_line(bool b){is_line_=b;}
        void set_multi(bool b){is_multi_=b;}
    private: 
        std::shared_ptr<render_target> rt_;
        std::shared_ptr<material> mt_;
        std::vector<const render_node*> rnodes_;
        bool is_additive_;
        bool is_line_;
        bool is_multi_;
        matrix4 m_;
        int width_;
        int height_;
    };

}

#endif