#ifndef GALE_POINTS_GEOMETRY_H
#define GALE_POINTS_GEOMETRY_H

#include <vector>
#include "geometry.h"

namespace gale
{
    class points_geometry : public geometry
    {
    public:
        enum SortMode
        {
            SORT_NONE = 0,
            SORT_OWN  = 1,
            SORT_WHOLE = 2 
        };
        enum BlendMode
        {
            BLEND_NONE  = 0,
            BLEND_ALPHA = 1,
            BLEND_ADDITIVE = 2    
        };
    public:
        points_geometry(
            const std::vector<vector3>& positions,
            const std::vector<vector2>& widths,
            const std::vector<color3>&  colors,
            const matrix4& mat);
        ~points_geometry();
    public:
        virtual void update();
        virtual void register_node(const render_context* ctx, std::vector<const render_node*>& rnodes)const;
    public:
        void set_sort_mode (SortMode mode) {sort_mode_ = mode;}
        void set_blend_mode(BlendMode mode){blend_mode_ = mode;}
    protected:
        void initialize();
    private:
        int sort_mode_;     //
        int blend_mode_;    //
        mutable std::vector<vector3> positions_;
        mutable std::vector<vector2> widths_;
        mutable std::vector<color3>  colors_;
        matrix4 mat_;
        std::vector<render_node*> nodes_;
    };
}

#endif
