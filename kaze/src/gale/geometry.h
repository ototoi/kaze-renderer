#ifndef GALE_GEOMETRY_H
#define GALE_GEOMETRY_H

#include <vector>
#include "types.h"
#include "render_node.h"
#include "render_context.h"

namespace gale
{
    class render_node;
    class render_context;
    class geometry
    {
    public:
        virtual ~geometry(){}
        virtual void update    () = 0;
        virtual void register_node(const render_context* ctx, std::vector<const render_node*> & rnodes)const = 0;
    };
}

#endif
