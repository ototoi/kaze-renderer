#ifndef GALE_PERSPECTIVE_CAMERA_H
#define GALE_PERSPECTIVE_CAMERA_H

#include "camera.h"
#include "parameters.h"

namespace gale
{
    class perspective_camera : public camera
    {
    public:
        perspective_camera(
            const matrix4& mat, 
            float angle, float aspect,
            float l, float r, float b, float t, float n, float f);
        virtual matrix4 get_world_to_camera()const;
        virtual matrix4 get_camera_to_clip()const;
        virtual matrix4 get_pan()const;
    public:
        void set_angle(float f);
        
        void set_pan  (float x, float y); 
        float get_angle()const;
        float get_aspect()const;

        float get_l()const;
        float get_r()const;
        float get_b()const;
        float get_t()const;

        float get_near()const;
        float get_far()const;
        vector3 get_origin()const;
    protected:
        void initialize();
    private:
        parameters params_;
    };
}

#endif
