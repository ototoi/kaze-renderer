#ifndef GALE_VALUES_H
#define GALE_VALUES_H

#ifdef min
#undef min
#endif
#ifdef max
#undef max
#endif

#ifdef far
#undef far
#endif

#ifdef near
#undef near
#endif

#ifdef FAR
#undef FAR
#endif

#ifdef NEAR
#undef NEAR
#endif

#include <cstdlib>
#include <limits>

#include "types.h"

namespace gale
{
    namespace values
    {
        inline float infinity() { return std::numeric_limits<float>::infinity(); }
        inline float maximum() { return std::numeric_limits<float>::max(); }
        inline float far() { return std::numeric_limits<float>::max() * float(1e-3); } //far != max
        inline float epsilon() { return std::numeric_limits<float>::epsilon(); }
        inline float near() { return std::numeric_limits<float>::epsilon() * float(1e+3); }
        inline float pi() { return (float)3.14159265358979323846; }
        inline float e() { return (float)2.71828183; }
        inline float less1() { return (float)1 - std::numeric_limits<float>::epsilon(); }

        inline float radians(float x) { return x * pi() / (float)180.0; }
        inline float degrees(float x) { return x * (float)180 / pi(); }

        inline size_t invalid_size() { return std::numeric_limits<size_t>::max(); }
    }
}

#endif
