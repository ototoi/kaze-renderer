#ifndef GALE_SHADER_H
#define GALE_SHADER_H

#include "types.h"
#include "gl/gl_program.h"

namespace gale
{
    class shader
    {
    public:
        virtual ~shader(){}
        virtual GLuint get_handle()const = 0;
    };
}

#endif
