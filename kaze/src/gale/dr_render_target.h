#ifndef GALE_DR_RENDER_TARGET_H
#define GALE_DR_RENDER_TARGET_H

#include "render_target.h"
#include "gl/gl_frame_buffer.h"
#include "gl/gl_texture.h"

namespace gale
{
    class dr_render_target : public render_target
    {
    public: 
        static const int CHANNELS = 5;
    public:
        dr_render_target(int width, int height);
        ~dr_render_target();
        virtual GLuint get_handle()const;
        virtual void bind() const;
        virtual void unbind() const;
    public: 
        int get_channels()const{return CHANNELS;}
    protected:
        std::shared_ptr<gl::gl_frame_buffer> framebuffer_;
        std::shared_ptr<gl::gl_texture_2D>  depth_;
        std::shared_ptr<gl::gl_texture_2D>  colors_[CHANNELS];
    };
}

#endif