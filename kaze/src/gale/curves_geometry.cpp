#include "curves_geometry.h"
#include "render_node.h"
#include "shader.h"
#include "basic_material.h"
#include "logger.h"

#include "gl/gl_program.h"
#include "gl/gl_vertex_buffer.h"
#include "gl/gl_vertex_array.h"

namespace gale
{
    static
    std::string GetFSString()
    {
        // "  color = vec4(vec3(0.5)*(normalize(vs_normal)+vec3(1)),1);\n"
        // "  color = vec4(vs_uv,0.01,1);\n"
        // "  color = vec4(vec3(0.5)*(n+vec3(1)),1);\n"
        static const char* STR =
        "#version 410\n"
        "\n"
        "in vec3 vs_position;\n"
        "in vec3 vs_normal;\n"
        "in vec3 vs_color;\n"
        "in vec2 vs_uv;\n"
        "\n"
        "out vec4 color;\n"
        "\n"
        "uniform mat4 local2world;\n"
        "uniform mat4 world2camera;\n"
        "uniform mat4 camera2clip;\n"
        "\n"
        "void main(void) {\n"
        "  color = vec4(vs_color,1);\n"
        "}\n"
        "\n";
        return STR;
    }
    // "  mat4 invModelMat = inverse(modelMat);\n"
    //  "  float l = inversesqrt(dot2( tovec3(invModelMat*vec4(edge,0,1)) - tovec3(invModelMat*vec4(0,0,0,1)) ));\n"
    static
    std::string GetVSString()
    {
        static const char* STR =
        "#version 410\n"
        "\n"
        "layout (location = 0) in vec4 prev_pos;\n"
        "layout (location = 1) in vec4 curr_pos;\n"
        "layout (location = 2) in vec4 next_pos;\n"
        "layout (location = 3) in vec3 color;\n"
        "layout (location = 4) in vec2 uv;\n"
        "\n"
        "out vec3 vs_position;\n"
        "out vec3 vs_normal;\n"
        "out vec3 vs_color;\n"
        "out vec2 vs_uv;\n"
        "\n"
        "uniform mat4 local2world;\n"
        "uniform mat4 world2camera;\n"
        "uniform mat4 camera2clip;\n"
        "\n"
        "vec3 tovec3(vec4 v){return v.xyz/v.w;}\n"
        "float dot2(vec3 x){return dot(x,x);}\n"
        "void main(void) {\n"
        "  mat4 modelMat = world2camera * local2world;\n"
        "  vec3 prev = tovec3(modelMat * vec4(prev_pos.xyz,1));\n"
        "  vec3 curr = tovec3(modelMat * vec4(curr_pos.xyz,1));\n"
        "  vec3 next = tovec3(modelMat * vec4(next_pos.xyz,1));\n"
        "  vec2 e1 = normalize(curr.xy - prev.xy);\n"
        "  vec2 e2 = normalize(next.xy - curr.xy);\n"
        "  vec2 ex = normalize(e1 + e2);\n"
        "  vec2 edge = ex.yx * vec2(1, -1);\n"
        "  vec3 edgeLocal  = normalize((transpose(modelMat)*vec4(edge,0,0)).xyz);\n"
        "  vec2 edgeCamera = (modelMat*vec4(edgeLocal,0)).xy;\n"
        "  vec3 pos = curr + curr_pos.w * vec3(edgeCamera, 0);\n"
        "  vs_position = pos;\n"
        "  vs_normal = vec3(1,1,-1);\n"
        "  vs_color  = color*uv.y;\n"
        "  vs_uv = uv;\n"
        "  gl_Position = camera2clip * vec4(pos,1);\n"
        "}\n"
        "\n";
        return STR;
    }

    static
    std::shared_ptr<gl::gl_program> CreateProgram()
    {
        std::shared_ptr<gl::gl_shader> fs(new gl::gl_shader(GL_FRAGMENT_SHADER));
        bool bFS = fs->compile_from_string(GetFSString());
        std::shared_ptr<gl::gl_shader> vs(new gl::gl_shader(GL_VERTEX_SHADER));
        bool bVS = vs->compile_from_string(GetVSString());
        if(!bFS || !bVS)return std::shared_ptr<gl::gl_program>();
        std::shared_ptr<gl::gl_program> prog(new gl::gl_program());
        prog->add_shader(vs);
        prog->add_shader(fs);
        bool bLink = prog->link();
        if(!bLink)return std::shared_ptr<gl::gl_program>();
        return prog;
    }

    class curves_shader : public shader
    {
    public:
        curves_shader()
        {
            prog_ =  CreateProgram();
        }
        virtual GLuint get_handle()const
        {
            return prog_->get_handle();
        }
    private:
        std::shared_ptr<gl::gl_program> prog_;
    };

    static
    std::shared_ptr<shader> CreateShader()
    {
        static std::shared_ptr<shader> s = std::shared_ptr<shader>(new curves_shader());
        return s;
    }

    typedef basic_material curves_material;

    typedef curves_geometry::curve_vertex curve_vertex;
    typedef curves_geometry::curve_strand curve_strand;

    class curve_render_node : public render_node
    {
    public:
        curve_render_node
        (
            const std::shared_ptr<curve_strand>& strand,
            const std::shared_ptr<material>& mtl,
            const matrix4& mat
        )
        {
            mtl_ = mtl;
            l2w_ = mat;
            org_ = strand->vertices[0].position;
            
            position_vbo_ = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            color_vbo_    = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            uv_vbo_       = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            vao_ = std::shared_ptr<gl::gl_vertex_array>(new gl::gl_vertex_array());
            
            size_t vsz = strand->vertices.size();
            vsz_ = 2 * vsz;
            std::vector<curve_vertex> points(vsz+2);
            size_t psz = points.size();
            for(size_t i = 0;i<vsz;i++)
            {
                points[i+1] = strand->vertices[i];
            }
            {
                points[0] = points[1];
                vector3 p0 = points[1].position;
                vector3 p1 = points[2].position;
                points[0].position = 2.0f * p0 - p1;
            }
            {
                points[psz-1] = points[psz-2];
                vector3 p0 = points[psz-1].position;
                vector3 p1 = points[psz-2].position;
                points[vsz].position = 2.0f * p0 - p1;
            }
            
            std::vector<float> pos_buffer(2*4*psz);
            for(size_t i = 0;i<psz;i++)
            {
                int offset = 8*i;
                pos_buffer[offset+0] = points[i].position[0];
                pos_buffer[offset+1] = points[i].position[1];
                pos_buffer[offset+2] = points[i].position[2];
                pos_buffer[offset+3] = +0.5f * points[i].width;
                pos_buffer[offset+4] = points[i].position[0];
                pos_buffer[offset+5] = points[i].position[1];
                pos_buffer[offset+6] = points[i].position[2];
                pos_buffer[offset+7] = -0.5f * points[i].width;
                /*
                if((i & 3) == 0)
                {
                    pos_buffer[offset+3] = 0;
                    pos_buffer[offset+7] = 0;
                }
                */
            }
            
            std::vector<float> col_buffer(2*3*vsz);
            for(size_t i = 0;i<vsz;i++)
            {
                color3 c = strand->vertices[i].color;
                col_buffer[6*i+0] = c[0];
                col_buffer[6*i+1] = c[1];
                col_buffer[6*i+2] = c[2];
                col_buffer[6*i+3] = c[0];
                col_buffer[6*i+4] = c[1];
                col_buffer[6*i+5] = c[2];
            }
            
            std::vector<float> uv_buffer(2*2*vsz);
            float delta = 1.0f/(vsz-1);
            for(size_t i = 0;i<vsz;i++)
            {
                uv_buffer[4*i+0] = 0;
                uv_buffer[4*i+1] = delta * i;
                uv_buffer[4*i+2] = 1;
                uv_buffer[4*i+3] = delta * i;
            }

            {
                GLuint handle = position_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*2*4*psz, &pos_buffer[0], GL_STATIC_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            
            {
                GLuint handle = color_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*2*3*vsz, &col_buffer[0], GL_STATIC_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            
             {
                GLuint handle = uv_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*2*2*vsz, &uv_buffer[0], GL_STATIC_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }


            {
                GLuint handle = vao_->get_handle();
                ::glBindVertexArray(handle);
                
                size_t offset = 0;
                {
                    ::glBindBuffer(GL_ARRAY_BUFFER, position_vbo_->get_handle());
                    {  
                        GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "prev_pos");
                        ::glEnableVertexAttribArray(index);
                        ::glVertexAttribPointer(index, 4, GL_FLOAT, GL_FALSE, sizeof(float)*4, (const void*)offset);  
                    }
                    offset += sizeof(float)*8;
                    {
                        GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "curr_pos");
                        ::glEnableVertexAttribArray(index);
                        ::glVertexAttribPointer(index, 4, GL_FLOAT, GL_FALSE, sizeof(float)*4, (const void*)offset);
                    }
                    offset += sizeof(float)*8;
                    {
                        GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "next_pos");
                        ::glEnableVertexAttribArray(index);
                        ::glVertexAttribPointer(index, 4, GL_FLOAT, GL_FALSE, sizeof(float)*4, (const void*)offset);
                    }
                }
                
                {
                    ::glBindBuffer(GL_ARRAY_BUFFER, color_vbo_->get_handle());
                    GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "color");
                    ::glEnableVertexAttribArray(index);
                    ::glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, NULL); 
                }
                
                {
                    ::glBindBuffer(GL_ARRAY_BUFFER, uv_vbo_->get_handle());
                    GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "uv");
                    ::glEnableVertexAttribArray(index);
                    ::glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, 0, NULL); 
                }
                
                {
                    ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
                }
                
                ::glBindVertexArray(0u);
            }
        }
        vector3 get_origin()const{return org_;}
        bool    is_opacity()const{return true;}
        matrix4 get_local_to_world()const{return l2w_;}
        const material* get_material()const{return mtl_.get();}
        GLuint  get_vertex_array_handle()const{return vao_->get_handle();}

        GLuint  get_vertices_handle()const{return position_vbo_->get_handle();}
        GLsizei get_vertices_count ()const{return vsz_;}
        void    draw               ()const
        {
            ::glDrawArrays(GL_TRIANGLE_STRIP, 0, vsz_);
        }
    private:
        std::shared_ptr<material> mtl_;
        std::shared_ptr<gl::gl_vertex_buffer> position_vbo_;
        std::shared_ptr<gl::gl_vertex_buffer> color_vbo_;
        std::shared_ptr<gl::gl_vertex_buffer> uv_vbo_;
        std::shared_ptr<gl::gl_vertex_array>  vao_;
        size_t vsz_;
        vector3 org_;
        matrix4 l2w_;
    };

    curves_geometry::curves_geometry(
            const std::vector< std::shared_ptr<curve_strand> >& strands,
            const matrix4& mat
        )
        :strands_(strands), mat_(mat)
    {
        this->initialize();
    }
    
    curves_geometry::~curves_geometry()
    {
        if(!nodes_.empty())
        {
            size_t sz = strands_.size();
            for(size_t i = 0;i < sz; i++)
            {
                delete nodes_[i];
            }
            nodes_.clear();
        }
    }

    void curves_geometry::initialize()
    {
        if(nodes_.empty())
        {
            std::shared_ptr<material> mtl = std::shared_ptr<material>(new curves_material(CreateShader()));
            size_t sz = strands_.size();
            for(size_t i = 0;i < sz; i++)
            {
                if(strands_[i].get())
                {
                    nodes_.push_back(new curve_render_node(strands_[i], mtl, mat_));   
                } 
            }
        }
    }

    void curves_geometry::update()
    {
        this->initialize();
    }
    
    typedef render_node* PCRNODE;
    void curves_geometry::register_node(const render_context* ctx, std::vector<const render_node*>& rnodes)const
    {
        if(!nodes_.empty())
        {
            size_t sz = nodes_.size();
            const PCRNODE* nodes = &nodes_[0];
            for(size_t i = 0;i<sz;i++)
            {
                rnodes.push_back(nodes[i]);
            }
        }
    }
}
