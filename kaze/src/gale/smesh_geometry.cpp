#include "smesh_geometry.h"
#include "render_node.h"
#include "shader.h"
#include "material.h"
#include "logger.h"

#include "image_texture.h"

#include "gl/gl_program.h"
#include "gl/gl_vertex_buffer.h"
#include "gl/gl_vertex_array.h"

#include <string.h>

namespace gale
{
    static
    std::string GetFSString()
    {
        // "  color = vec4(vec3(0.5)*(normalize(vs_normal)+vec3(1)),1);\n"
        // "  color = vec4(vs_uv,0.01,1);\n"
        // "  color = vec4(vec3(0.5)*(n+vec3(1)),1);\n"
        static const char* STR =
        "#version 410\n"
        "\n"
        "in vec3 vs_position;\n"
        "in vec3 vs_normal;\n"
        "in vec3 vs_color;\n"
        "in vec2 vs_uv;\n"
        "\n"
        "out vec4 color;\n"
        "\n"
        "uniform sampler2D albedo_tex;\n"
        "uniform sampler2D alpha_tex;\n"
        "\n"
        "void main(void) {\n"
        "  vec3 n = normalize(vs_normal);\n"
        "  if(dot(normalize(vs_position),n)>0)n*=-1;"
        "  vec3 rgb = dot(n,vec3(0,0,-1))*texture(albedo_tex, vs_uv).rgb;\n"
        "  float a  = texture(alpha_tex,  vs_uv).r;\n"
        "  if(a < 0.01)discard;\n"
        "  color = vec4(vs_color,1)*vec4(rgb,a);\n"
        "}\n"
        "\n";
        return STR;
    }
    static
    std::string GetVSString()
    {
        static const char* STR =
        "#version 410\n"
        "\n"
        "layout (location = 0) in vec3 position;\n"
        "layout (location = 1) in vec3 normal;\n"
        "layout (location = 2) in vec3 color;\n"
        "layout (location = 3) in vec2 uv;\n"
        "\n"
        "out vec3 vs_position;\n"
        "out vec3 vs_normal;\n"
        "out vec3 vs_color;\n"
        "out vec2 vs_uv;\n"
        "\n"
        "uniform mat4 local2world;\n"
        "uniform mat4 world2camera;\n"
        "uniform mat4 camera2clip;\n"
        "\n"
        "vec3 tovec3(vec4 v){return v.xyz/v.w;}\n"
        "void main(void) {\n"
        "  mat4 nmat = transpose(inverse(world2camera*local2world));\n"
        "  vs_position = tovec3(world2camera*local2world * vec4(position,1));\n"
        "  vec3 n = normalize((nmat * vec4(normal,0)).xyz);\n"
        "  vs_normal = n;\n"
        "  vs_color  = color;\n"
        "  vs_uv = uv;\n"
        "  gl_Position = camera2clip * world2camera * local2world * vec4(position,1);\n"
        "}\n"
        "\n";
        return STR;
    }

    static
    std::shared_ptr<gl::gl_program> CreateProgram()
    {
        std::shared_ptr<gl::gl_shader> fs(new gl::gl_shader(GL_FRAGMENT_SHADER));
        bool bFS = fs->compile_from_string(GetFSString());
        std::shared_ptr<gl::gl_shader> vs(new gl::gl_shader(GL_VERTEX_SHADER));
        bool bVS = vs->compile_from_string(GetVSString());
        if(!bFS || !bVS)return std::shared_ptr<gl::gl_program>();
        std::shared_ptr<gl::gl_program> prog(new gl::gl_program());
        prog->add_shader(vs);
        prog->add_shader(fs);
        bool bLink = prog->link();
        if(!bLink)return std::shared_ptr<gl::gl_program>();
        return prog;
    }

    class smesh_shader : public shader
    {
    public:
        smesh_shader()
        {
            prog_ =  CreateProgram();
        }
        virtual GLuint get_handle()const
        {
            return prog_->get_handle();
        }
    private:
        std::shared_ptr<gl::gl_program> prog_;
    };

    static
    std::shared_ptr<shader> CreateShader()
    {
        static std::shared_ptr<shader> sShader = std::shared_ptr<shader>(new smesh_shader());
        return sShader;
    }

    smesh_material::smesh_material()
    {
        static std::shared_ptr<gale::texture> white_tex(new gale::image_texture(1,1,1,1));
        this->shader_ = CreateShader();
        this->set_texture("albedo_tex", white_tex);
        this->set_texture("alpha_tex",  white_tex);
    }

    static
    std::shared_ptr<material> CreateMaterial()
    {
        return std::shared_ptr<material>(new smesh_material());
    }

    class smesh_vbo_set
    {
    public:
        smesh_vbo_set
        (
            int nverts,
            const float* vertices,
            const float* normals,
            const float* colors,
            const float* uvs,
            int nindices,
            const int*   indices,
            const matrix4& mat
        )
        {
            nverts_ = nverts;
            nindices_ = nindices;
            l2w_ = mat;

            vector3 org = vector3(0,0,0);
            for(int i=0;i<nverts;i++)
            {
                org += vector3(vertices + 3*i);
            }
            org *= (1.0/nverts);
            org_ = org;

            position_vbo_ = std::unique_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            normal_vbo_   = std::unique_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            color_vbo_    = std::unique_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            uv_vbo_       = std::unique_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            index_vbo_    = std::unique_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            {
                GLuint handle = position_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*nverts*3, vertices, GL_STATIC_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                GLuint handle = normal_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*nverts*3, normals, GL_STATIC_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                GLuint handle = color_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*nverts*3, colors, GL_STATIC_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                GLuint handle = uv_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(float)*nverts*2, uvs, GL_STATIC_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                GLuint handle = index_vbo_->get_handle();
                ::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*nindices, indices, GL_STATIC_DRAW);
                ::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0u);
            }
        }
        std::unique_ptr<gl::gl_vertex_buffer> position_vbo_;
        std::unique_ptr<gl::gl_vertex_buffer> normal_vbo_;
        std::unique_ptr<gl::gl_vertex_buffer> color_vbo_;
        std::unique_ptr<gl::gl_vertex_buffer> uv_vbo_;
        std::unique_ptr<gl::gl_vertex_buffer> index_vbo_;
        int nverts_;
        int nindices_;
        vector3 org_;
        matrix4 l2w_;
    };

    class smesh_render_node : public render_node
    {
    public:
        smesh_render_node(const std::shared_ptr<smesh_vbo_set>& vbo_set)
            :vbo_set_(vbo_set)
        {
            mtl_ = CreateMaterial();
            vao_ = std::shared_ptr<gl::gl_vertex_array>(new gl::gl_vertex_array());
            {
                GLuint handle = vao_->get_handle();
                ::glBindVertexArray(handle);
                {
                    GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "position");
                    ::glBindBuffer(GL_ARRAY_BUFFER, vbo_set_->position_vbo_->get_handle());
                    ::glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                {
                    GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "normal");
                    ::glBindBuffer(GL_ARRAY_BUFFER, vbo_set_->normal_vbo_->get_handle());
                    ::glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                {
                    GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "color");
                    ::glBindBuffer(GL_ARRAY_BUFFER, vbo_set_->color_vbo_->get_handle());
                    ::glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                {
                    GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "uv");
                    ::glBindBuffer(GL_ARRAY_BUFFER, vbo_set_->uv_vbo_->get_handle());
                    ::glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                {
                    GLuint handle = vbo_set_->index_vbo_->get_handle();
                    ::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle);
                }
                {
                    ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
                }
                ::glBindVertexArray(0u);
            }
        }
        vector3 get_origin()const{return vbo_set_->org_;}
        bool is_opacity()const{return ((const smesh_material*)(mtl_.get()))->is_opacity();}
        matrix4 get_local_to_world()const{return vbo_set_->l2w_;}
        const material* get_material()const{return mtl_.get();}
        GLuint  get_vertex_array_handle()const{return vao_->get_handle();}

        GLuint  get_vertices_handle()const{return vbo_set_->position_vbo_->get_handle();}
        GLsizei get_vertices_count ()const{return vbo_set_->nverts_;}
        GLuint  get_indices_handle ()const{return vbo_set_->index_vbo_->get_handle();}
        GLsizei get_indices_count  ()const{return vbo_set_->nindices_;}
        void    draw               ()const{::glDrawElements(GL_TRIANGLES, vbo_set_->nindices_, GL_UNSIGNED_INT, 0 );}
    public:
        void set_material(const std::shared_ptr<material>& mtl){mtl_ = mtl;}
    private:
        std::shared_ptr<material> mtl_;
        std::shared_ptr<smesh_vbo_set> vbo_set_;
        std::shared_ptr<gl::gl_vertex_array>  vao_;
    };

    smesh_geometry::smesh_geometry(
        const std::vector<vector3>& vertices,
        const std::vector<vector3>& normals,
        const std::vector<vector2>& uvs,
        const std::vector<int>& indices,
        const matrix4& mat)
        :vertices_(vertices), normals_(normals), uvs_(uvs), indices_(indices), mat_(mat), node_(NULL)
    {
        size_t vsz = vertices.size();
        colors_.resize(vsz);
        for(size_t i=0;i<vsz;i++)
        {
            colors_[i] = color3(1,1,1);    
        }
        this->initialize();
    }
    
    smesh_geometry::smesh_geometry(
        const std::vector<vector3>& vertices,
        const std::vector<vector3>& normals,
        const std::vector<vector3>& colors, 
        const std::vector<vector2>& uvs,
        const std::vector<int>& indices,
        const matrix4& mat)
        :vertices_(vertices), normals_(normals), colors_(colors), uvs_(uvs), indices_(indices), mat_(mat), node_(NULL)
    {
        this->initialize();
    }
    
    smesh_geometry::~smesh_geometry()
    {
        if(node_)
        {
            delete node_;
        }
    }
    
    void smesh_geometry::set_material(const std::shared_ptr<material>& mtl)
    {
        if(node_)
        {
            node_->set_material(mtl);
        }
    }

    void smesh_geometry::initialize()
    {
        if(!node_)
        {
            const float* vertices = (const float*)(&vertices_[0][0]);
            const float* normals  = (const float*)(&normals_[0][0]);
            const float* colors   = (const float*)(&colors_[0][0]);
            const float* uvs      = (const float*)(&uvs_[0][0]);
            const int*   indices  = &indices_[0];
            int nverts   = (int)vertices_.size();
            int nindices = (int)indices_.size();
            std::shared_ptr<smesh_vbo_set> vbo_set = std::shared_ptr<smesh_vbo_set>(new smesh_vbo_set(nverts, vertices, normals, colors, uvs, nindices, indices, mat_));
            node_ = new smesh_render_node(vbo_set);
        }
    }

    void smesh_geometry::update()
    {
        if(!node_)
        {
            this->initialize();
        }
    }
    void smesh_geometry::register_node(const render_context* ctx, std::vector<const render_node*>& rnodes)const
    {
        if(node_)
        {
            rnodes.push_back(node_);
        }
    }
}
