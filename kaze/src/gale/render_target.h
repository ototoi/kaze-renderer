#ifndef GALE_RENDER_TARGET_H
#define GALE_RENDER_TARGET_H

#include "render_node.h"

namespace gale
{
    class render_target
    {
    public: 
        virtual ~render_target(){}
        virtual GLuint get_handle()const = 0;
        virtual void bind() const = 0;
        virtual void unbind() const = 0;
    };
}

#endif