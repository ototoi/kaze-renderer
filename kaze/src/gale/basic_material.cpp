#include "basic_material.h"

namespace gale
{
    basic_material::basic_material()
        : is_opacity_(true)
    {
        ; //
    }

    basic_material::basic_material(const std::shared_ptr<shader>& shd)
        : shader_(shd), is_opacity_(true)
    {
        ; //
    }

    const shader* basic_material::get_shader() const
    {
        return shader_.get();
    }

    typedef basic_material::uniform_value uniform_value;
    namespace
    {
        enum
        {
            PARAM_INTEGER = 1,
            PARAM_INTEGER_ARRAY,
            PARAM_FLOAT,
            PATAM_FLOAT_ARRAY,
            PARAM_MATRIX,
            PARAM_TEXTURE_2D
        };
    }

    void basic_material::set_integer(const char* key, int v)
    {
        int nFound = find_param(key);
        if (nFound < 0)
        {
            GLuint handle = this->get_shader()->get_handle();
            uniform_value univ;
            univ.type = PARAM_INTEGER;
            univ.key = key;
            GLint nLoc = ::glGetUniformLocation(handle, key);
            univ.location = (int)nLoc;
            univ.ivalues.push_back(v);
            uniforms_.push_back(univ);
        }
        else
        {
            if (uniforms_[nFound].type == PARAM_TEXTURE_2D)
            {
                size_t usz = uniforms_.size();
                int cnt = 0;
                for (size_t i = 0; i < usz; i++)
                {
                    if (i != nFound)
                    {
                        if (uniforms_[i].type == PARAM_TEXTURE_2D)
                        {
                            uniforms_[i].unit = cnt;
                            cnt++;
                        }
                    }
                }
                textures_.erase(textures_.begin() + uniforms_[nFound].unit);
            }
            GLuint handle = this->get_shader()->get_handle();
            uniforms_[nFound].type = PARAM_INTEGER;
            uniforms_[nFound].key = key;
            GLint nLoc = ::glGetUniformLocation(handle, key);
            uniforms_[nFound].location = (int)nLoc;
            uniforms_[nFound].ivalues.push_back(v);
        }
    }
    void basic_material::set_float(const char* key, float v)
    {
        int nFound = find_param(key);
        if (nFound < 0)
        {
            GLuint handle = this->get_shader()->get_handle();
            uniform_value univ;
            univ.type = PARAM_FLOAT;
            univ.key = key;
            GLint nLoc = ::glGetUniformLocation(handle, key);
            univ.location = (int)nLoc;
            univ.fvalues.push_back(v);
            uniforms_.push_back(univ);
        }
        else
        {
            if (uniforms_[nFound].type == PARAM_TEXTURE_2D)
            {
                size_t usz = uniforms_.size();
                int cnt = 0;
                for (size_t i = 0; i < usz; i++)
                {
                    if (i != nFound)
                    {
                        if (uniforms_[i].type == PARAM_TEXTURE_2D)
                        {
                            uniforms_[i].unit = cnt;
                            cnt++;
                        }
                    }
                }
                textures_.erase(textures_.begin() + uniforms_[nFound].unit);
            }
            GLuint handle = this->get_shader()->get_handle();
            uniforms_[nFound].type = PARAM_FLOAT;
            uniforms_[nFound].key = key;
            GLint nLoc = ::glGetUniformLocation(handle, key);
            uniforms_[nFound].location = (int)nLoc;
            uniforms_[nFound].fvalues.push_back(v);
        }
    }
    void basic_material::set_texture(const char* key, const std::shared_ptr<texture>& tex)
    {
        int nFound = find_param(key);
        if (nFound < 0)
        {
            GLuint handle = this->get_shader()->get_handle();
            uniform_value univ;
            univ.type = PARAM_TEXTURE_2D;
            univ.key = key;
            GLint nLoc = ::glGetUniformLocation(handle, key);
            univ.location = (int)nLoc;
            univ.ivalues.push_back((int)textures_.size());
            uniforms_.push_back(univ);
            textures_.push_back(tex);
        }
        else
        {
            GLuint handle = this->get_shader()->get_handle();
            uniforms_[nFound].type = PARAM_TEXTURE_2D;
            uniforms_[nFound].key = key;
            GLint nLoc = ::glGetUniformLocation(handle, key);
            uniforms_[nFound].location = (int)nLoc;
            //univ.ivalues.push_back((int)textures_.size());
            textures_[uniforms_[nFound].ivalues[0]] = tex;
        }
    }

    void basic_material::set_matrix(const char* key, const float mat[])
    {
        ; //TODO
    }

    void basic_material::bind_uniforms(const render_context* ctx) const
    {
        size_t sz = uniforms_.size();
        for (size_t i = 0; i < sz; i++)
        {
            int type = uniforms_[i].type;
            GLuint loc = uniforms_[i].location;
            if (type == PARAM_INTEGER)
            {
                ::glUniform1i(loc, uniforms_[i].ivalues[0]);
            }
            else if (type == PARAM_FLOAT)
            {
                ::glUniform1f(loc, uniforms_[i].fvalues[0]);
            }
            else if (type == PARAM_TEXTURE_2D)
            {
                GLuint unit = uniforms_[i].ivalues[0];
                const std::shared_ptr<texture>& tex = textures_[unit];
                GLuint tex_handle = tex->get_handle();
                ::glActiveTexture(GL_TEXTURE0 + unit);
                ::glBindTexture(GL_TEXTURE_2D, tex_handle);
                ::glUniform1i(loc, unit);
            }
        }

        {
            GLuint handle = this->get_shader()->get_handle();
            {
                matrix4 m = ctx->get_world_to_camera();
                m.transpose();
                GLint loc = ::glGetUniformLocation(handle, "world2camera");
                ::glUniformMatrix4fv(loc, 1, GL_FALSE, &m[0][0]);
            }
            {
                matrix4 m = ctx->get_camera_to_clip();
                m.transpose();
                GLint loc = ::glGetUniformLocation(handle, "camera2clip");
                ::glUniformMatrix4fv(loc, 1, GL_FALSE, &m[0][0]);
            }
        }
    }

    int basic_material::find_param(const char* key) const
    {
        size_t sz = uniforms_.size();
        for (size_t i = 0; i < sz; i++)
        {
            if (uniforms_[i].key == key) return i;
        }
        return -1;
    }
}