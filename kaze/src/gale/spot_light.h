#ifndef GALE_SPOT_LIGHT_H
#define GALE_SPOT_LIGHT_H

#include "light.h"
#include "parameters.h"

namespace gale
{
    class spot_light : public light
    {
    public:
        spot_light(
            const vector3& from,
            const vector3& to,
            float inner, float outer, 
            const matrix4& mat
        );
        ~spot_light();
        virtual int get_light_type()const{return SPOT;}
        virtual void update();
        virtual void register_node(const render_context* ctx,  std::vector<const render_node*> & rnodes)const;
    public:
        const float* get_float(const char* szKey)const;
    protected:
        void initialize();
    protected:
        parameters params_;
        render_node* node_;
    };
}

#endif
