#ifndef GALE_BASIC_MATERIAL_H
#define GALE_BASIC_MATERIAL_H

#include <vector>
#include <memory>
#include "material.h"

namespace gale
{
    class basic_material : public material
    {
    public:
        struct uniform_value
        {
            int type;
            std::string key;
            int location;
            int unit;
            std::vector<int>   ivalues;
            std::vector<float> fvalues;
        };
    public:
        basic_material();
        basic_material(const std::shared_ptr<shader>& shd);
        virtual const shader* get_shader()const;
        void set_integer(const char* key, int v);
        void set_float  (const char* key, float v);
        void set_texture(const char* key, const std::shared_ptr<texture>& tex);
        void set_matrix (const char* key, const float mat[]);
        void bind_uniforms(const render_context* ctx)const; 
        void set_opacity(bool b){is_opacity_ = b;}
        bool is_opacity()const{return is_opacity_;}
    protected:
        int find_param(const char* key)const;
    protected:
        std::shared_ptr<shader> shader_;
        std::vector<uniform_value> uniforms_;
        std::vector< std::shared_ptr<texture> > textures_;
        bool is_opacity_;
    };
}
#endif