#include <GL/glew.h>

#if defined(_WIN32)
#include <GL/gl.h>
#include <GL/glut.h>
#elif defined(__APPLE__)
#include <OpenGL/gl.h>
#include <OpenGL/OpenGL.h>
#include <GLUT/glut.h>
#endif

#include <GLFW/glfw3.h>

#include "gale.h"
#include "render_node.h"
#include "material.h"
#include "shader.h"
#include "test0_geometry.h"
#include "points_geometry.h"
#include "fw_renderer.h"
#include "gl_info.h"
#include "values.h"
#include "perspective_camera.h"

namespace gale
{
    static bool is_sort = true;
    static bool is_additive = false;
    static bool is_line = false;
    static bool is_multi = false;

    int g_mbutton[3] = {0, 0, 0},
        g_running = 1;

    float g_rotate[2] = {0, 0},
          g_dolly = 0,
          g_pan[2] = {0, 0},
          g_center[3] = {0, 0, 0},
          g_size = 0;

    int g_prev_x = 0,
        g_prev_y = 0;

    int g_width = 1024,
        g_height = 1024;

    struct gale_context
    {
        GLFWwindow* window;
    };

    static gale_context g_ctx;

    static void
    mouse(GLFWwindow*, int button, int state, int /* mods */)
    {
        if (button < 3)
        {
            g_mbutton[button] = (state == GLFW_PRESS);
        }
    }

    static void
    motion(GLFWwindow*, double dx, double dy)
    {

        int x = (int)dx, y = (int)dy;

        if (g_mbutton[0] && !g_mbutton[1] && !g_mbutton[2])
        {
            // orbit
            g_rotate[0] += x - g_prev_x;
            g_rotate[1] += y - g_prev_y;
        }
        else if (!g_mbutton[0] && !g_mbutton[1] && g_mbutton[2])
        {
            // pan
            g_pan[0] -= ((float)(x - g_prev_x)) / g_width;
            g_pan[1] += ((float)(y - g_prev_y)) / g_height;
        }
        else if ((g_mbutton[0] && !g_mbutton[1] && g_mbutton[2]) or
                 (!g_mbutton[0] && g_mbutton[1] && !g_mbutton[2]))
        {
            // dolly
            g_dolly += ((float)(x - g_prev_x));
            g_dolly = std::max<float>(-g_width, g_dolly);
            g_dolly = std::min<float>(+g_width, g_dolly);
            //if(g_dolly <= 0.01) g_dolly = 0.01f;
        }

        g_prev_x = x;
        g_prev_y = y;
    }
    /*
    static void
    reshape(GLFWwindow *, int width, int height) {

        g_width = width;
        g_height = height;

        int windowWidth = g_width, windowHeight = g_height;

        // window size might not match framebuffer size on a high DPI display
        glfwGetWindowSize(g_window, &windowWidth, &windowHeight);

        //g_hud.Rebuild(windowWidth, windowHeight, width, height);
    }
*/
    static void keyboard(GLFWwindow* window, int key, int scancode, int action, int mods)
    {
        if (key == GLFW_KEY_P && action == GLFW_PRESS)
            is_sort = !is_sort;

        if (key == GLFW_KEY_A && action == GLFW_PRESS)
            is_additive = !is_additive;

        if (key == GLFW_KEY_L && action == GLFW_PRESS)
            is_line = !is_line;

        if (key == GLFW_KEY_M && action == GLFW_PRESS)
            is_multi = !is_multi;
    }

    static inline vector3 MulNormal(const matrix4& m, const vector3& v)
    {
        return vector3(
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
    }

    static matrix4 get_matrix(const std::shared_ptr<camera>& cam)
    {
        perspective_camera* pcam = dynamic_cast<perspective_camera*>(cam.get());
        if (pcam)
        {
            static float ang_org = pcam->get_angle();
            float angle = pcam->get_angle();
            float aspect = pcam->get_aspect();
            float rx = angle * aspect * g_rotate[1] / g_width;
            float ry = angle * g_rotate[0] / g_height;
            float rz = 0;

            float angle2 = ang_org + g_dolly / g_width;
            angle2 = std::max(angle2, values::radians(1));
            pcam->set_angle(angle2);

            matrix4 local = pcam->get_world_to_camera();
            vector3 xPivot = normalize(MulNormal(~local, vector3(1, 0, 0)));
            vector3 yPivot = normalize(MulNormal(~local, vector3(0, 1, 0)));
            vector3 zPivot = normalize(MulNormal(~local, vector3(0, 0, 1)));
            vector3 org = pcam->get_origin();

            pcam->set_pan(-2 * g_pan[0], -2 * g_pan[1]);

            quaternion qx = pivot_angle(xPivot, rx);
            quaternion qy = pivot_angle(yPivot, ry);
            quaternion qz = pivot_angle(yPivot, rz);
            matrix4 m = mat4_gen::identity();
            m = mat4_gen::translation(-org[0], -org[1], -org[2]) * m;
            m = mat4_gen::convert(qz * qy * qx) * m;
            m = mat4_gen::translation(+org[0], +org[1], +org[2]) * m;
            //m = mat4_gen::rotation_x(values::radians(g_rotate[1])) * m;
            //m = mat4_gen::rotation_y(values::radians(g_rotate[0])) * m;
            //m = mat4_gen::translation(-pan[0], -pan[1], 0) * m;
            //m = mat4_gen::scaling(g_dolly, g_dolly, g_dolly) * m;

            return m;
        }
        else
        {
            return mat4_gen::identity();
        }
    }

    class n_render_context : public render_context
    {
    public:
        void set_world_to_camera(const matrix4& m) { w2c_ = m; }
        void set_camera_to_clip(const matrix4& m) { c2c_ = m; }
        void set_line_mode(bool b) { is_line_ = b; }
    public:
        virtual matrix4 get_world_to_camera() const { return w2c_; }
        virtual matrix4 get_camera_to_clip() const { return c2c_; }
        virtual bool is_line_mode() const { return is_line_; }
    private:
        matrix4 w2c_;
        matrix4 c2c_;
        bool is_line_;
    };

    static void Update(
        const std::vector<std::shared_ptr<geometry> >& geos,
        const std::vector<std::shared_ptr<gale::light> >& lights)
    {
        {
            size_t sz = geos.size();
            for (size_t i = 0; i < sz; i++)
            {
                geos[i]->update();
            }
        }
        //if(is_sort)
        {
            size_t sz = geos.size();
            for (size_t i = 0; i < sz; i++)
            {
                geometry* pGeo = geos[i].get();
                points_geometry* pPoints = dynamic_cast<points_geometry*>(pGeo);
                if (pPoints != NULL)
                {
                    if (is_sort)
                    {
                        pPoints->set_sort_mode(points_geometry::SORT_OWN);
                    }
                    else
                    {
                        pPoints->set_sort_mode(points_geometry::SORT_NONE);
                    }
                }
            }

            n_render_context ctx;
            std::vector<const render_node*> nodes;
            for (size_t i = 0; i < sz; i++)
            {
                geometry* pGeo = geos[i].get();
                pGeo->register_node(&ctx, nodes);
            }
            if (nodes.size())
            {
                vector3 p = vector3(0, 0, 0);
                for (size_t i = 0; i < nodes.size(); i++)
                {
                    p += nodes[0]->get_origin();
                }
                p /= (float)nodes.size();
                g_center[0] = p[0];
                g_center[1] = p[1];
                g_center[2] = p[2];
            }
        }

        {
            size_t sz = lights.size();
            for (size_t i = 0; i < sz; i++)
            {
                lights[i]->update();
            }
        }
    }

    gale_context* initialize_gale()
    {
        GLFWwindow* window;

        /* Initialize the library */
        if (!glfwInit())
            return NULL;

#ifdef __APPLE__
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif
        glfwWindowHint(GLFW_DEPTH_BITS, 32);

        /* Create a windowed mode window and its OpenGL context */
        window = glfwCreateWindow(640, 480, "Gale - 0 fps", NULL, NULL);
        if (!window)
        {
            glfwTerminate();
            return NULL;
        }

        /* Make the window's context current */
        glfwMakeContextCurrent(window);

        glewExperimental = GL_TRUE; //><
        glewInit();

        //const GLubyte* version = glGetString(GL_VERSION);
        //printf("version:%s\n", version);
        glfwSetCursorPosCallback(window, motion);
        glfwSetMouseButtonCallback(window, mouse);
        glfwSetKeyCallback(window, keyboard);

        g_ctx.window = window;
        return &g_ctx;
    }

    void render_gale(
        gale_context* ctx,
        int width, int height,
        const std::vector<std::shared_ptr<geometry> >& geos,
        const std::vector<std::shared_ptr<gale::light> >& lights,
        const std::shared_ptr<camera>& cam)
    {
        //gl_info::get_instance()->print();
        GLFWwindow* window = ctx->window;
        glfwSetWindowSize(window, width, height);
        g_width = width;
        g_height = height;

        fw_renderer renderer(width, height);

        int frame_count = 0;
        double prevTime = glfwGetTime();
        double savedTime = prevTime;

        /* Loop until the user closes the window */
        while (!glfwWindowShouldClose(window))
        {
            matrix4 m = get_matrix(cam);
            Update(geos, lights);
            renderer.set_rotation(m);
            renderer.set_additive(is_additive);
            renderer.set_line(is_line);
            renderer.set_multi(is_multi);

            /* Render here */
            renderer.render(geos, lights, cam);

            frame_count++;
            double currTime = glfwGetTime();
            if ((currTime - savedTime) >= 1.0)
            {
                int fps = (int)floor(frame_count / (currTime - savedTime));
                frame_count = 0;
                savedTime = currTime;

                char buffer[64];
                sprintf(buffer, "Gale - %d fps", fps);
                glfwSetWindowTitle(window, buffer);
            }
            double deltaTime = currTime - prevTime;
            prevTime = currTime;
            //tRot += 5 * deltaTime;

            /* Poll for and process events */
            glfwPollEvents();

            /* Swap front and back buffers */
            glfwSwapBuffers(window);
        }

        glfwTerminate();
    }

    void destroy_gale(gale_context* ctx)
    {
        ; //
    }
}
