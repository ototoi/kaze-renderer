#ifndef GALE_RENDER_NODE_H
#define GALE_RENDER_NODE_H

#include "types.h"
#include "gl/gl_vertex_array.h"

namespace gale
{
    class material;
    class render_node
    {
    public:
        virtual ~render_node(){}
        virtual int     get_layer ()const{return 1;}
        virtual vector3 get_origin()const{return vector3(0,0,0);}
        virtual bool    is_opacity()const{return true;}
        virtual bool    is_receive_light()const{return true;}
        virtual matrix4 get_local_to_world()const = 0;
        virtual const material* get_material()const{return NULL;}
        virtual GLuint  get_vertex_array_handle()const{return 0;}
        virtual GLuint  get_vertices_handle()const{return 0;}
        virtual GLsizei get_vertices_count ()const{return 0;}
        virtual GLuint  get_indices_handle ()const{return 0;}
        virtual GLsizei get_indices_count  ()const{return 0;}
        virtual void    draw()const{;}
    };
}

#endif
