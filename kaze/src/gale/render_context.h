#ifndef GALE_RENDER_CONTEXT_H
#define GALE_RENDER_CONTEXT_H

#include "types.h"

namespace gale
{
    class render_context
    {
    public:
        virtual ~render_context(){}
        virtual matrix4 get_world_to_camera()const = 0;
        virtual matrix4 get_camera_to_clip ()const = 0;
        virtual bool    is_line_mode()const{return false;}
    };
}

#endif