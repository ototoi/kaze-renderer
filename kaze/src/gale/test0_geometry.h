#ifndef GALE_TEST0_GEOMETRY_H
#define GALE_TEST0_GEOMETRY_H

#include <vector>
#include "geometry.h"

namespace gale
{
    class test0_render_node;
    class test0_geometry : public geometry
    {
    public:
        test0_geometry();
        ~test0_geometry();
    public:
        virtual void update();
        virtual void register_node(const render_context* ctx, std::vector<const render_node*>& rnodes)const;
    protected:
        void initialize();
    private:
        test0_render_node* node_;
        double prevTime;
    };
}

#endif
