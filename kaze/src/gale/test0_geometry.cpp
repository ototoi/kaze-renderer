#include "test0_geometry.h"
#include "render_node.h"
#include "shader.h"
#include "material.h"
#include "logger.h"

#include "gl/gl_program.h"
#include "gl/gl_vertex_buffer.h"
#include "gl/gl_vertex_array.h"

#include <GLFW/glfw3.h>

#include <memory>

namespace gale
{
    static
    std::string GetFSString()
    {
        static const char* STR =
        "#version 410\n"
        "\n"
        "in vec4 vs_color;\n"
        "out vec4 color;\n"
        "\n"
        "void main(void) {\n"
        "  color = vs_color;\n"
        "}\n"
        "\n";
        return STR;
    }
    static
    std::string GetVSString()
    {
        static const char* STR =
        "#version 410\n"
        "\n"
        "layout (location = 0) in vec4 position;\n"
        "layout (location = 1) in vec4 color;\n"
        "\n"
        "out vec4 vs_color;\n"
        "\n"
        "uniform mat4 local2world;\n"
        "\n"
        "void main(void) {\n"
        "  vec4 p = position;\n"
        "  gl_Position = local2world * p;\n"
        "  vs_color = color;\n"
        "}\n"
        "\n";
        return STR;
    }

    static
    std::shared_ptr<gl::gl_program> CreateProgram()
    {
        std::shared_ptr<gl::gl_shader> fs(new gl::gl_shader(GL_FRAGMENT_SHADER));
        bool bFS = fs->compile_from_string(GetFSString());
        std::shared_ptr<gl::gl_shader> vs(new gl::gl_shader(GL_VERTEX_SHADER));
        bool bVS = vs->compile_from_string(GetVSString());
        if(!bFS || !bVS)return std::shared_ptr<gl::gl_program>();
        std::shared_ptr<gl::gl_program> prog(new gl::gl_program());
        prog->add_shader(vs);
        prog->add_shader(fs);
        bool bLink = prog->link();
        if(!bLink)return std::shared_ptr<gl::gl_program>();
        return prog;
    }

    class test0_shader : public shader
    {
    public:
        test0_shader()
        {
            prog_ =  CreateProgram();
        }
        virtual GLuint get_handle()const
        {
            return prog_->get_handle();
        }
    private:
        std::shared_ptr<gl::gl_program> prog_;
    };

    static
    std::shared_ptr<shader> CreateShader()
    {
        return std::shared_ptr<shader>(new test0_shader());
    }

    class test0_material : public material
    {
    public:
        test0_material()
        {
            shader_ = CreateShader();
        }
        virtual const shader* get_shader()const
        {
            return shader_.get();
        }
    private:
        std::shared_ptr<shader> shader_;
    };

    static
    std::shared_ptr<material> CreateMaterial()
    {
        return std::shared_ptr<material>(new test0_material());
    }

    class test0_render_node : public render_node
    {
    public:
        test0_render_node()
        {
            mtl_ = CreateMaterial();
            position_vbo_ = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            color_vbo_    = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            index_vbo_    = std::shared_ptr<gl::gl_vertex_buffer>(new gl::gl_vertex_buffer());
            vao_ = std::shared_ptr<gl::gl_vertex_array>(new gl::gl_vertex_array());

            {
                static const GLfloat triangle_vertices[4*3] = {
                    0.25, -0.25,  0.5, 1.0,
                    -0.25, -0.25,  0.5, 1.0,
                    0.0,   0.25,  0.5, 1.0
                };
                GLuint handle = position_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(triangle_vertices), triangle_vertices, GL_STATIC_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                static const GLfloat triangle_colors[4*3] = {
                    1.0, 0.0, 0.0, 1.0,
                    0.0, 1.0, 0.0, 1.0,
                    0.0, 0.0, 1.0, 1.0
                };
                GLuint handle = color_vbo_->get_handle();
                ::glBindBuffer(GL_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ARRAY_BUFFER, sizeof(triangle_colors), triangle_colors, GL_STATIC_DRAW);
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                static const GLuint indices[] = {0,1,2};
                GLuint handle = index_vbo_->get_handle();
                ::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle);
                ::glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);
                ::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0u);
            }
            {
                GLuint handle = vao_->get_handle();
                ::glBindVertexArray(handle);
                {
                    GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "position");
                    ::glBindBuffer(GL_ARRAY_BUFFER, position_vbo_->get_handle());
                    ::glVertexAttribPointer(index, 4, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                {
                    GLuint index = ::glGetAttribLocation(mtl_->get_shader()->get_handle(), "color");
                    ::glBindBuffer(GL_ARRAY_BUFFER, color_vbo_->get_handle());
                    ::glVertexAttribPointer(index, 4, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                {
                    GLuint handle = index_vbo_->get_handle();
                    ::glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle);
                }
                {
                    ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
                }
                ::glBindVertexArray(0u);
            }

            {
                mat_ = mat4_gen::identity();
            }
        }
        matrix4 get_local_to_world()const{return mat_;}
        const material* get_material()const{return mtl_.get();}
        GLuint  get_vertex_array_handle()const{return vao_->get_handle();}

        GLuint  get_vertices_handle()const{return position_vbo_->get_handle();}
        GLsizei get_vertices_count ()const{return 3;}
        GLuint  get_indices_handle ()const{return index_vbo_->get_handle();}
        GLsizei get_indices_count  ()const{return 3;}
        void    draw               ()const{::glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, 0 );}
    public:
        void set_matrix(const matrix4& m)
        {
            mat_ = m;
        }
        void mul_matrix(const matrix4& m)
        {
            mat_ = m * mat_;
        }
    private:
        std::shared_ptr<material> mtl_;
        std::shared_ptr<gl::gl_vertex_buffer> position_vbo_;
        std::shared_ptr<gl::gl_vertex_buffer> color_vbo_;
        std::shared_ptr<gl::gl_vertex_buffer> index_vbo_;
        std::shared_ptr<gl::gl_vertex_array>  vao_;

        matrix4 mat_;
    };

    test0_geometry::test0_geometry()
        :node_(NULL)
    {
        this->initialize();
    }

    test0_geometry::~test0_geometry()
    {
        if(node_)
        {
            delete node_;
        }
    }

    void test0_geometry::initialize()
    {
        if(!node_)
        {
            node_ = new test0_render_node();
        }
        prevTime = glfwGetTime();
    }

    static
    inline float pi() { return (float)3.14159265358979323846; }

    void test0_geometry::update()
    {
        if(!node_)
        {
            this->initialize();
        }

        double currTime = glfwGetTime();
        double deltaTime = currTime - prevTime;
        if(deltaTime > 1.0/60.0)
        {
            double deltaR = deltaTime * 30;
            matrix4 mat = mat4_gen::rotation_y(deltaR/180.0*pi());
            node_->mul_matrix(mat);
            prevTime = currTime;
        }
    }

    void test0_geometry::register_node(const render_context* ctx, std::vector<const render_node*>& rnodes)const
    {
        rnodes.push_back(node_);
    }

}
