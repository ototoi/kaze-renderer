#ifndef GALE_LIGHT_H
#define GALE_LIGHT_H

#include <vector>
#include "types.h"
#include "render_node.h"
#include "render_context.h"

namespace gale
{
    class light
    {
    public:
        enum LIGHT_TYPE
        {
            POINT,
            SPOT,
            DIRECTIONAL
        };
    public:
        virtual ~light(){}
        virtual int get_light_type()const = 0;
        virtual void update() = 0;
        virtual void register_node(const render_context* ctx, std::vector<const render_node*> & rnodes)const = 0;
    };
}

#endif
