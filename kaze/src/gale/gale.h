#ifndef GALE_H
#define GALE_H

#include <vector>
#include <memory>
#include "geometry.h"
#include "light.h"
#include "camera.h"

namespace gale
{
    struct gale_context;
    gale_context* initialize_gale();
    void render_gale(gale_context* ctx,
        int width, int height,
        const std::vector<std::shared_ptr<geometry> >& geos,
        const std::vector<std::shared_ptr<gale::light> >& lights,
        const std::shared_ptr<camera>& cam
    );
    void destroy_gale(gale_context* ctx);
}

#endif
