#include "parameters.h"

namespace gale
{
    typedef parameters::value_type value_type;

    parameters::parameters() { ; }
    parameters::~parameters()
    {
        map_type::iterator i = map_.begin();
        map_type::iterator end = map_.end();
        for (; i != end; i++)
        {
            delete i->second;
        }
    }
    parameters::parameters(const parameters& rhs)
    {
        map_type::const_iterator i = rhs.map_.begin();
        map_type::const_iterator end = rhs.map_.end();
        for (; i != end; i++)
        {
            map_[i->first] = new value_type(*(i->second));
        }
    }

    parameters& parameters::operator=(const parameters& rhs)
    {
        parameters tmp(rhs);
        swap(tmp);
        return *this;
    }

    bool parameters::set(const char* key, const value_type& val)
    {
        std::string skey(key);
        map_.insert(map_type::value_type(skey, new value_type(val)));
        return true;
    }

    bool parameters::set(const char* key, int val)
    {
        std::string skey(key);
        map_type::iterator i = map_.find(skey);
        if (i != map_.end())
        {
            if (i->second->nType == TYPE_INTEGER)
            {
                i->second->integer_values.clear();
                i->second->integer_values.push_back(val);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            map_.insert(map_type::value_type(skey, new value_type(val)));
            return true;
        }
    }

    bool parameters::set(const char* key, float val)
    {
        std::string skey(key);
        map_type::iterator i = map_.find(skey);
        if (i != map_.end())
        {
            if (i->second->nType == TYPE_FLOAT)
            {
                i->second->float_values.clear();
                i->second->float_values.push_back(val);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            map_.insert(map_type::value_type(skey, new value_type(val)));
            return true;
        }
    }

    bool parameters::set(const char* key, const char* val)
    {
        std::string skey(key);
        std::string sval(val);
        map_type::iterator i = map_.find(skey);
        if (i != map_.end())
        {
            if (i->second->nType == TYPE_STRING)
            {
                i->second->string_values.clear();
                i->second->string_values.push_back(sval);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            map_.insert(map_type::value_type(skey, new value_type(val)));
            return true;
        }
    }

    bool parameters::set(const char* key, const int* val, int n)
    {
        std::string skey(key);
        map_type::iterator i = map_.find(skey);
        if (i != map_.end())
        {
            if (i->second->nType == TYPE_INTEGER)
            {
                i->second->integer_values.clear();
                i->second->integer_values.assign(val, val + n);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            map_.insert(map_type::value_type(skey, new value_type(val, n)));
            return true;
        }
    }
    bool parameters::set(const char* key, const float* val, int n)
    {
        std::string skey(key);
        map_type::iterator i = map_.find(skey);
        if (i != map_.end())
        {
            if (i->second->nType == TYPE_FLOAT)
            {
                i->second->float_values.clear();
                i->second->float_values.assign(val, val + n);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            map_.insert(map_type::value_type(skey, new value_type(val, n)));
            return true;
        }
    }
    bool parameters::set(const char* key, const LPCSTR* val, int n)
    {
        std::string skey(key);
        map_type::iterator i = map_.find(skey);
        if (i != map_.end())
        {
            if (i->second->nType == TYPE_STRING)
            {
                i->second->string_values.clear();
                i->second->string_values.assign(val, val + n);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            map_.insert(map_type::value_type(skey, new value_type(val, n)));
            return true;
        }
    }

    bool parameters::set(const char* key, const std::string& val)
    {
        std::string skey(key);
        map_type::iterator i = map_.find(skey);
        if (i != map_.end())
        {
            if (i->second->nType == TYPE_STRING)
            {
                i->second->string_values.clear();
                i->second->string_values.push_back(val);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            map_.insert(map_type::value_type(skey, new value_type(val)));
            return true;
        }
    }

    bool parameters::set(const char* key, const std::string* val, int n)
    {
        std::string skey(key);
        map_type::iterator i = map_.find(skey);
        if (i != map_.end())
        {
            if (i->second->nType == TYPE_STRING)
            {
                i->second->string_values.clear();
                i->second->string_values.assign(val, val + n);
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            map_.insert(map_type::value_type(skey, new value_type(val, n)));
            return true;
        }
    }

    const value_type* parameters::get(const char* key) const
    {
        std::string skey(key);
        map_type::const_iterator i = map_.find(skey);
        if (i != map_.end())
        {
            return i->second;
        }
        else
        {
            return NULL;
        }
    }

    void parameters::swap(parameters& rhs) { map_.swap(rhs.map_); }
}
