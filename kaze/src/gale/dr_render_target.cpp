#include "dr_render_target.h"

namespace gale
{
    static const int CHANNELS = dr_render_target::CHANNELS;
    /*
     * ch0:[px   ]:[py   ]:[pz   ]:[N/A  ]      #posistion(world)-alpha
     * ch1:[nx   ]:[ny   ]:[nz   ]:[light]      #normal(world)   -lightNO.
     * ch2:[al-r ]:[al-g ]:[al-b ]:[alpha]      #albedo
     * ch3:[para1]:[para2]:[para3]:[para4]      #params
     * ch4:[lr   ]:[lg   ]:[lb   ]:[ld   ]      #light
     */
    dr_render_target::dr_render_target(int width, int height)
    {
        framebuffer_ = std::shared_ptr<gl::gl_frame_buffer>(new gl::gl_frame_buffer());
        for(int i = 0;i<CHANNELS;i++)
        {
            colors_[i] = std::shared_ptr<gl::gl_texture_2D>(new gl::gl_texture_2D());
            colors_[i]->set_image(GL_RGBA16F, width, height, GL_RGBA, GL_FLOAT, NULL);
            {
                colors_[i]->bind();
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);   //not biliear
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);   //
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //TODO
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //TODO
                colors_[i]->unbind();
            }
        }
        {
            depth_ = std::shared_ptr<gl::gl_texture_2D>(new gl::gl_texture_2D());
            depth_->set_image(GL_DEPTH_COMPONENT32F, width, height, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
            {
                depth_->bind();
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);   //not biliear
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);   //
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //TODO
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //TODO
                depth_->unbind();
            }
        }
        framebuffer_->bind();
        for(int i = 0;i<CHANNELS;i++)
        {
            framebuffer_->attach_color(colors_[i]->get_handle(), i);
        }
        framebuffer_->attach_depth(depth_->get_handle());
        framebuffer_->unbind();
    }
    dr_render_target::~dr_render_target()
    {
        ;
    }
    GLuint dr_render_target::get_handle()const
    {
        return framebuffer_->get_handle();
    }
    void dr_render_target::bind() const
    {
        framebuffer_->bind();
    }
    void dr_render_target::unbind() const
    {
        framebuffer_->unbind();
    }
}
