#ifndef GALE_GL_INFO_H
#define GALE_GL_INFO_H

#include <map>
#include <string>
#include "parameters.h"

namespace gale
{
    class gl_info
    {
    public:
        gl_info();
        void print()const;
        static gl_info* get_instance();
    private: 
        parameters params_;
    };
}

#endif