#ifndef GL_TEX_WAIFU2X_FILTER_H
#define GL_TEX_WAIFU2X_FILTER_H

#include "gl_image_effect_pass.h"
#include <waifu2x/Model.h>

#include <vector>
#include <memory>

namespace gl
{
    namespace tex
    {

        class gl_waifu2x_filter_pass_imp;
        class gl_waifu2x_filter_pass : public gl_pass
        {
        public:
            gl_waifu2x_filter_pass(
                int width, int height,
                std::shared_ptr<gl::gl_texture_2D>& inTex,
                std::shared_ptr<gl::gl_texture_2D>& outTex,
                const std::vector<std::shared_ptr<waifu2x::Model> >& models);
            ~gl_waifu2x_filter_pass();

        public:
            virtual int run();

        protected:
            gl_waifu2x_filter_pass_imp* imp_;
        };
    }
}

#endif
