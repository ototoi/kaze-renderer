#ifndef GL_TEX_IMAGE_EFFECT_PASS_H
#define GL_TEX_IMAGE_EFFECT_PASS_H

#include <GL/glew.h>

#include <gl/gl_texture.h>
#include <gl/gl_program.h>
#include <gl/gl_frame_buffer.h>
#include <gl/gl_vertex_buffer.h>
#include <gl/gl_vertex_array.h>

#include "gl_pass.h"

namespace gl
{
    namespace tex
    {
        class gl_image_effect_pass : public gl_pass
        {
        public:
            gl_image_effect_pass(
                int width,
                int height,
                const std::shared_ptr<gl::gl_program>& prog,
                const std::shared_ptr<gl::gl_texture_2D>& outTex
            );
            int run();
        public:
            virtual void set_uniforms() = 0;
        public:
            static std::string get_shaders_path();
        protected:
            int width_;
            int height_;
            std::shared_ptr<gl::gl_program>        prog_;
            std::shared_ptr<gl::gl_texture_2D>     outTex_;
            std::shared_ptr<gl::gl_frame_buffer>   fbo_;
            std::shared_ptr<gl::gl_vertex_buffer>  vbo_p_;
            std::shared_ptr<gl::gl_vertex_buffer>  vbo_c_;
            std::shared_ptr<gl::gl_vertex_array>   vao_;
        };
    }
}

#endif