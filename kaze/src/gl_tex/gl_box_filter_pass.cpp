#include "gl_box_filter_pass.h"
#include "gl_box_filter_pass.inc"

namespace gl
{
    namespace tex
    {
        static
        std::shared_ptr<gl::gl_program> CreateProgram()
        {
            std::string strVS = szVS;
            std::string strFS = szFS;
            std::shared_ptr<gl::gl_program> prog(new gl::gl_program());
            std::shared_ptr<gl::gl_shader> vsh(new gl::gl_shader(GL_VERTEX_SHADER));
            std::shared_ptr<gl::gl_shader> fsh(new gl::gl_shader(GL_FRAGMENT_SHADER));
            if (!vsh->compile_from_string(strVS)) return std::shared_ptr<gl::gl_program>();
            if (!fsh->compile_from_string(strFS)) return std::shared_ptr<gl::gl_program>();
            prog->add_shader(vsh->get_handle());
            prog->add_shader(fsh->get_handle());
            prog->link();
            return prog;
        }

        gl_box_filter_pass::gl_box_filter_pass(
            int width, int height,
            std::shared_ptr<gl::gl_texture_2D>& inTex,
            std::shared_ptr<gl::gl_texture_2D>& outTex,
            int swidth,
            int twidth) : 
                gl_image_effect_pass(width, height, CreateProgram(), outTex),
                inTex_(inTex)
        {
            swidth_ = (swidth / 2) * 2 + 1;
            twidth_ = (twidth / 2) * 2 + 1; 
        }

        void gl_box_filter_pass::set_uniforms()
        {
            std::shared_ptr<gl::gl_program>& prog = prog_;
            int width = width_;
            int height = height_;
            GLuint nInTexID = inTex_->get_handle();

            GLuint nlocTex = 0;
            GLuint nlocWidth = 0;
            GLuint nlocHeight = 0;
            GLuint nlocSWidth = 0;
            GLuint nlocTWidth = 0;

            nlocTex = prog->get_uniform_location("tex");
            nlocWidth = prog->get_uniform_location("width");
            nlocHeight = prog->get_uniform_location("height");
            nlocSWidth = prog->get_uniform_location("swidth");
            nlocTWidth = prog->get_uniform_location("twidth");

            ::glActiveTexture(GL_TEXTURE0 + 0);
            ::glBindTexture(GL_TEXTURE_2D, nInTexID);

            prog->set_uniform(nlocTex, 0);
            prog->set_uniform(nlocWidth, width);
            prog->set_uniform(nlocHeight, height);
            prog->set_uniform(nlocSWidth, swidth_);
            prog->set_uniform(nlocTWidth, twidth_);
        }
    }
}