#include "gl_YUV_filter_pass.h"
#include "gl_YUV_filter_pass.inc"

namespace gl
{
    namespace tex
    {
        static
        std::shared_ptr<gl::gl_program> CreateProgram(const std::string& shaderName)
        {
            std::string strVS = szVS;
            std::string strFS;
            if(shaderName == "RGB2YUV")
            {
                strFS = szFS_RGB2YUV;
            }
            else if(shaderName == "YUV2RGB")
            {
                strFS = szFS_YUV2RGB;
            }
            else
            {
                strFS = szFS_YUVComposite;
            }

            std::shared_ptr<gl::gl_program> prog(new gl::gl_program());
            std::shared_ptr<gl::gl_shader> vsh(new gl::gl_shader(GL_VERTEX_SHADER));
            std::shared_ptr<gl::gl_shader> fsh(new gl::gl_shader(GL_FRAGMENT_SHADER));
            if (!vsh->compile_from_string(strVS)) return std::shared_ptr<gl::gl_program>();
            if (!fsh->compile_from_string(strFS)) return std::shared_ptr<gl::gl_program>();
            prog->add_shader(vsh->get_handle());
            prog->add_shader(fsh->get_handle());
            prog->link();
            return prog;
        }

        gl_YUV_base_pass::gl_YUV_base_pass(
            int width, int height,
            std::shared_ptr<gl::gl_texture_2D>& inTex,
            std::shared_ptr<gl::gl_texture_2D>& outTex,
            const std::string& shaderName) : 
                gl_image_effect_pass(width, height, CreateProgram(shaderName), outTex),
                inTex_(inTex)
        {
            ;//
        }

        void gl_YUV_base_pass::set_uniforms()
        {
            std::shared_ptr<gl::gl_program>& prog = prog_;
            GLuint nInTexID = inTex_->get_handle();

            GLuint nlocTex = 0;

            nlocTex = prog->get_uniform_location("tex");

            ::glActiveTexture(GL_TEXTURE0 + 0);
            ::glBindTexture(GL_TEXTURE_2D, nInTexID);

            prog->set_uniform(nlocTex, 0);
        }

        gl_RGB2YUV_pass::gl_RGB2YUV_pass(
            int width, int height,
            std::shared_ptr<gl::gl_texture_2D>& inTex,
            std::shared_ptr<gl::gl_texture_2D>& outTex)
            : gl_YUV_base_pass(width, height, inTex, outTex, "RGB2YUV")
        {
            ;
        }

        gl_YUV2RGB_pass::gl_YUV2RGB_pass(
            int width, int height,
            std::shared_ptr<gl::gl_texture_2D>& inTex,
            std::shared_ptr<gl::gl_texture_2D>& outTex)
            : gl_YUV_base_pass(width, height, inTex, outTex, "YUV2RGB")
        {
            ;
        }
    }
}