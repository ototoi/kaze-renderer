#ifndef GL_TEX_PASS_H
#define GL_TEX_PASS_H

namespace gl
{
    namespace tex
    {
        class gl_pass
        {
        public:
            virtual ~gl_pass() {}
            virtual int run() = 0;
        };
    }
}

#endif