#ifndef GL_TEX_BOX_FILTER_BOX_H
#define GL_TEX_BOX_FILTER_BOX_H

#include "gl_image_effect_pass.h"

namespace gl
{
    namespace tex
    {
        class gl_box_filter_pass : public gl_image_effect_pass
        {
        public:
            gl_box_filter_pass(
                int width, int height,
                std::shared_ptr<gl::gl_texture_2D>& inTex,
                std::shared_ptr<gl::gl_texture_2D>& outTex,
                int swidth,
                int twidth);
        public:
            virtual void set_uniforms();
        protected:
            std::shared_ptr<gl::gl_texture_2D> inTex_;
            int swidth_;
            int twidth_;
        };
    }
}

#endif
