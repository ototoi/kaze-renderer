#ifndef GL_TEX_GAUSS_FILTER_BOX_H
#define GL_TEX_GAUSS_FILTER_BOX_H

#include "gl_image_effect_pass.h"

namespace gl
{
    namespace tex
    {
        class gl_gauss_filter_pass : public gl_image_effect_pass
        {
        public:
            gl_gauss_filter_pass(
                int width, int height,
                std::shared_ptr<gl::gl_texture_2D>& inTex,
                std::shared_ptr<gl::gl_texture_2D>& outTex,
                float swidth,
                float twidth,
                int phase);

        public:
            virtual void set_uniforms();
        protected:
            std::shared_ptr<gl::gl_texture_2D> inTex_;
            float swidth_;
            float twidth_;
        };
    }
}

#endif
