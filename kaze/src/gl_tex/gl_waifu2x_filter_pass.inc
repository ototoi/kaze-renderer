static const char* szFS_waifu2x =
"\n"
"in vec3 fpos;\n"
"in vec2 fuv;\n"
"\n"
"layout(location = 0) out float out_color;\n"
"\n"
"uniform sampler2D tex;\n"
"uniform float bias;\n"
"uniform sampler2DArray inputTextures;\n"
"uniform vec3 weightMatrix[3 * 128];\n"
"\n"
"void main()\n"
"{\n"
"#if 1\n"
"	// Convolution Process\n"
"	highp float s = 0.0;\n"
"	for (int i = 0; i < NUM_INPUT_PLANES; i++) {\n"
"		highp vec3 t0, t1, t2;\n"
"		vec3 uvt = vec3(fuv, i);\n"
"		t0 = vec3(textureOffset(inputTextures, uvt, ivec2(-1, -1)).r,\n"
"		          textureOffset(inputTextures, uvt, ivec2( 0, -1)).r,\n"
"		          textureOffset(inputTextures, uvt, ivec2( 1, -1)).r);\n"
"		t1 = vec3(textureOffset(inputTextures, uvt, ivec2(-1,  0)).r,\n"
"		          texture      (inputTextures, uvt               ).r,\n"
"		          textureOffset(inputTextures, uvt, ivec2( 1,  0)).r);\n"
"		t2 = vec3(textureOffset(inputTextures, uvt, ivec2(-1,  1)).r,\n"
"		          textureOffset(inputTextures, uvt, ivec2( 0,  1)).r,\n"
"		          textureOffset(inputTextures, uvt, ivec2( 1,  1)).r);\n"
"\n"
"		s += dot(t0, weightMatrix[i * 3 + 0]) +\n"
"	         dot(t1, weightMatrix[i * 3 + 1]) +\n"
"	         dot(t2, weightMatrix[i * 3 + 2]);\n"
"	}\n"
"	// Leaky ReLU Process\n"
"	s += bias;\n"
"	s = max(s, 0) + min(s, 0) * 0.1;\n"
"	//out_color = vec4(s, 0, 0, 1);\n"
"    out_color = s;\n"
"#else\n"
"    out_color = texture(inputTextures, vec3(fuv, 0));\n"
"#endif\n"
"}\n";

static const char* szFS_Array2Tex3 =
"#version 410\n"
"\n"
"in vec3 fpos;\n"
"in vec2 fuv;\n"
"\n"
"layout(location = 0) out vec4 out_color;\n"
"\n"
"uniform sampler2DArray tex;\n"
"uniform sampler2D alphaTex;\n"
"\n"
"void main()\n"
"{\n"
"    float r = texture(tex, vec3(fuv,0)).r;\n"
"    float g = texture(tex, vec3(fuv,1)).r;\n"
"    float b = texture(tex, vec3(fuv,2)).r;\n"
"    float a = texture(alphaTex, fuv).a;\n"
"    out_color = vec4(r, g, b, a);\n"
"}\n";

#if 0
static const char* szVS =
"#version 410\n"
"\n"
"in vec3 pos;\n"
"in vec2 uv;\n"
"\n"
"out vec3 fpos;\n"
"out vec2 fuv;\n"
"\n"
"void main()\n"
"{\n"
"	fpos = pos;\n"
"	fuv  = uv;\n"
"	gl_Position = vec4(fpos,1);\n"
"}\n";
#endif
