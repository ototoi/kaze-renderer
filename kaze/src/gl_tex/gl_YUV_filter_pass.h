#ifndef GL_TEX_YUV_PASS_H
#define GL_TEX_YUV_PASS_H

#include "gl_image_effect_pass.h"

namespace gl
{
    namespace tex
    {

        class gl_YUV_base_pass : public gl_image_effect_pass
        {
        public:
            gl_YUV_base_pass(
                int width, int height,
                std::shared_ptr<gl::gl_texture_2D>& inTex,
                std::shared_ptr<gl::gl_texture_2D>& outTex,
                const std::string& shaderName);
        public:
            virtual void set_uniforms();
        protected:
            std::shared_ptr<gl::gl_texture_2D> inTex_;
        };

        class gl_RGB2YUV_pass : public gl_YUV_base_pass
        {
        public:
            gl_RGB2YUV_pass(
                int width, int height,
                std::shared_ptr<gl::gl_texture_2D>& inTex,
                std::shared_ptr<gl::gl_texture_2D>& outTex);
        };

        class gl_YUV2RGB_pass : public gl_YUV_base_pass
        {
        public:
            gl_YUV2RGB_pass(
                int width, int height,
                std::shared_ptr<gl::gl_texture_2D>& inTex,
                std::shared_ptr<gl::gl_texture_2D>& outTex);
        };
    }
}

#endif
