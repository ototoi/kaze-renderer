#include "gl_waifu2x_filter_pass.h"
#include "gl_waifu2x_filter_pass.inc"
#include "gl_YUV_filter_pass.inc"

#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <cassert>

namespace gl
{
    namespace tex
    {

        using namespace gl;
        using namespace waifu2x;

        namespace
        {
            class Texture2DArrayManager
            {
            public:
                Texture2DArrayManager(int width, int height)
                {
                    textures_[0] = std::shared_ptr<gl::gl_texture_2DArray>(new gl_texture_2DArray());
                    textures_[1] = std::shared_ptr<gl::gl_texture_2DArray>(new gl_texture_2DArray());
                    for (int i = 0; i < 2; i++)
                    {
                        textures_[i]->bind();
                        ::glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER, GL_NEAREST);   //not biliear
                        ::glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER, GL_NEAREST);   //
                        ::glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //TODO
                        ::glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //TODO
                        textures_[i]->unbind();
                        //textures_[i]->set_image(GL_RGBA32F, width, height, 128, 0, GL_RGBA, GL_FLOAT, 0);
                        textures_[i]->set_image(GL_R32F, width, height, 128, 0, GL_RED, GL_FLOAT, 0);
                    }
                }
                std::shared_ptr<gl::gl_texture_2DArray>& GetInputTextures(int pass)
                {
                    int n = pass & 1;
                    return textures_[n];
                }
                const std::shared_ptr<gl::gl_texture_2DArray>& GetInputTextures(int pass) const
                {
                    int n = pass & 1;
                    return textures_[n];
                }
                std::shared_ptr<gl::gl_texture_2DArray>& GetOutputTextures(int pass)
                {
                    int n = (pass + 1) & 1;
                    return textures_[n];
                }
                const std::shared_ptr<gl::gl_texture_2DArray>& GetOutputTextures(int pass) const
                {
                    int n = (pass + 1) & 1;
                    return textures_[n];
                }

            private:
                std::shared_ptr<gl::gl_texture_2DArray> textures_[2];
            };

            template <class T>
            class SingletonManager
            {
            public:
                typedef std::map<std::string, std::shared_ptr<T> > map_type;

            public:
                static SingletonManager& GetInstance()
                {
                    static SingletonManager mgr;
                    return mgr;
                }

                const std::shared_ptr<T>& Get(const std::string& key) const
                {
                    static const std::shared_ptr<T> nil_;
                    typename map_type::const_iterator it = map_.find(key);
                    if (it != map_.end())
                    {
                        return it->second;
                    }
                    else
                    {
                        return nil_;
                    }
                }
                void Set(const std::string& key, const std::shared_ptr<T>& val)
                {
                    map_[key] = val;
                }

            protected:
                SingletonManager() {}
            protected:
                std::map<std::string, std::shared_ptr<T> > map_;
            };

            typedef SingletonManager<gl::gl_program> ProgramManager;
            typedef SingletonManager<gl::gl_frame_buffer> FrameBufferManager;
        }

        static std::string GetShaderCode(const char* szFilename, int nOutputPlanes, int nInputPlanes)
        {
            std::stringstream ifs(szFilename);
            std::stringstream ss;
            ss << "#version 410\n";
            ss << "#define NUM_OUTPUT_PLANES " << nOutputPlanes << "\n";
            ss << "#define NUM_INPUT_PLANES " << nInputPlanes << "\n";

            std::string strLine = "";
            while (getline(ifs, strLine))
            {
                ss << strLine << "\n";
            }
            return ss.str();
        }

        class Waifu2xImageFilterPass : public gl_pass
        {
        protected:
            void CreateVertex()
            {
                static const float posData[] = {
                    -1.0f, -1.0f, 0.0f, //0
                    -1.0f, +1.0f, 0.0f, //1
                    +1.0f, -1.0f, 0.0f, //2
                    +1.0f, +1.0f, 0.0f, //3
                };
                static const float uvData[] = {
                    0.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 0.0f,
                    1.0f, 1.0f,
                };
                GLuint vboHandles[2] = {};
                ::glGenBuffers(2, vboHandles);
                GLuint posBufferHandle = vboHandles[0];
                GLuint uvBufferHandle = vboHandles[1];

                ::glBindBuffer(GL_ARRAY_BUFFER, posBufferHandle);
                ::glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(float), posData, GL_STATIC_DRAW);

                ::glBindBuffer(GL_ARRAY_BUFFER, uvBufferHandle);
                ::glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(float), uvData, GL_STATIC_DRAW);

                ::glBindBuffer(GL_ARRAY_BUFFER, 0);

                memcpy(vboHandles_, vboHandles, sizeof(GLuint) * 2);

                std::shared_ptr<gl::gl_program>& prog = prog_;
                //GLuint shader_hanfle = prog->get_handle();
                //::glUseProgram(shader_hanfle);

                GLuint nlocPosition = prog->get_attrib_location("pos");
                GLuint nlocCoord = prog->get_attrib_location("uv");

                //printf("__%d, %d__\n", nlocPosition, nlocCoord);

                GLuint vaoHandle;
                ::glGenVertexArrays(1, &vaoHandle);

                ::glBindVertexArray(vaoHandle);
                ::glEnableVertexAttribArray(nlocPosition);
                ::glEnableVertexAttribArray(nlocCoord);

                ::glBindBuffer(GL_ARRAY_BUFFER, posBufferHandle);
                ::glVertexAttribPointer(nlocPosition, 3, GL_FLOAT, GL_FALSE, 0, NULL);

                ::glBindBuffer(GL_ARRAY_BUFFER, uvBufferHandle);
                ::glVertexAttribPointer(nlocCoord, 2, GL_FLOAT, GL_FALSE, 0, NULL);

                //::glDisableVertexAttribArray(nlocCoord);
                //::glDisableVertexAttribArray(nlocPosition);
                ::glBindVertexArray(0);

                vaoHandle_ = vaoHandle;
            }

            void DestroyVertex()
            {
                ::glGenVertexArrays(1, &vaoHandle_);
                ::glDeleteBuffers(2, vboHandles_);
            }

        protected:
            std::shared_ptr<gl::gl_program> prog_;
            std::shared_ptr<gl::gl_frame_buffer> fbo_;
            GLuint vboHandles_[2];
            GLuint vaoHandle_;
        };

        class Waifu2xModelPass : public Waifu2xImageFilterPass
        {
        public:
            Waifu2xModelPass(
                int width, int height,
                const std::shared_ptr<gl::gl_texture_2DArray>& inTex,
                const std::shared_ptr<gl::gl_texture_2DArray>& outTex,
                const std::shared_ptr<waifu2x::Model>& model,
                int pass)
                : width_(width), height_(height),
                  inTex_(inTex), outTex_(outTex),
                  model_(model)
            {
                static const std::string shaderName_ = "Waifu2x:";
                char buffer[512] = {};
                sprintf(buffer, "%p", model.get());
                std::string shaderName = shaderName_ + buffer;

                const std::shared_ptr<gl::gl_program>& findProg = ProgramManager::GetInstance().Get(shaderName);
                if (findProg.get())
                {
                    prog_ = findProg;
                }
                else
                {
                    std::shared_ptr<gl::gl_program> prog = CreateProgram(model);
                    ProgramManager::GetInstance().Set(shaderName, prog);
                    prog_ = prog;
                }
                const std::shared_ptr<gl::gl_frame_buffer>& findFBO = FrameBufferManager::GetInstance().Get("Waifu2x");
                if (findFBO.get())
                {
                    fbo_ = findFBO;
                }
                else
                {
                    std::shared_ptr<gl::gl_frame_buffer> fbo = std::shared_ptr<gl::gl_frame_buffer>(new gl::gl_frame_buffer());
                    FrameBufferManager::GetInstance().Set("Waifu2x", fbo);
                    fbo_ = fbo;
                }
                CreateVertex();
            }

            ~Waifu2xModelPass()
            {
                DestroyVertex();
            }

            int run()
            {
                int width = width_;
                int height = height_;
                const std::shared_ptr<gl::gl_texture_2DArray>& inputTexArray = inTex_;
                const std::shared_ptr<gl::gl_texture_2DArray>& outputTexArray = outTex_;
                const std::shared_ptr<waifu2x::Model>& model = model_;
                const std::shared_ptr<gl::gl_program>& prog = prog_;

                int nOutputPlanes = model->GetOutputPlanes();
                int nInputPlanes = model->GetInputPlanes();

                GLuint shader_prog = prog->get_handle();
                ::glUseProgram(shader_prog);

                GLuint nlocTex = prog->get_uniform_location("inputTextures");
                GLuint nlocBias = prog->get_uniform_location("bias");
                GLuint nLocWeights = prog->get_uniform_location("weightMatrix");

                const std::vector<std::shared_ptr<Mat> >& weights = model->GetWeights();
                const std::vector<float>& biases = model->GetBiases();

                float vWeightMatrices[3 * 3 * 128] = {};

                fbo_->bind();
                ::glBindVertexArray(vaoHandle_);
                for (int o = 0; o < nOutputPlanes; o++)
                {
                    ::glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, outputTexArray->get_handle(), 0, o);

                    ::glViewport(0, 0, width, height);

                    ::glActiveTexture(GL_TEXTURE0 + 0);
                    ::glBindTexture(GL_TEXTURE_2D_ARRAY, inputTexArray->get_handle());
                    prog->set_uniform(nlocTex, 0);
                    prog->set_uniform(nlocBias, biases[o]);

                    for (int i = 0; i < nInputPlanes; i++)
                    {
                        memcpy(&vWeightMatrices[i * 3 * 3], weights[o * nInputPlanes + i]->GetPtr(), 3 * 3 * sizeof(float));
                    }
                    glUniform3fv(nLocWeights, 3 * nInputPlanes, vWeightMatrices);
                    //prog->set_uniform(nLocWeights, 3 * 3 * nInputPlanes, vWeightMatrices);

                    ::glDisable(GL_BLEND);
                    ::glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                }
                ::glBindVertexArray(0);

                ::glBindTexture(GL_TEXTURE_2D_ARRAY, 0);
                ::glBindTexture(GL_TEXTURE_2D, 0);

                ::glUseProgram(0);

                fbo_->unbind();
                return 0;
            }

        protected:
            static std::shared_ptr<gl::gl_program> CreateProgram(const std::shared_ptr<waifu2x::Model>& model)
            {
                std::shared_ptr<gl::gl_program> prog(new gl::gl_program());
                std::shared_ptr<gl::gl_shader> vsh(new gl::gl_shader(GL_VERTEX_SHADER));
                std::shared_ptr<gl::gl_shader> fsh(new gl::gl_shader(GL_FRAGMENT_SHADER));

                int nOutputPlanes = model->GetOutputPlanes();
                int nInputPlanes = model->GetInputPlanes();

                std::string strVs = szVS;//GetShaderCode(szVS, nOutputPlanes, nInputPlanes);
                std::string strFs = GetShaderCode(szFS_waifu2x, nOutputPlanes, nInputPlanes);
                if (strVs == "") return prog;
                if (strFs == "") return prog;

                if (!vsh->compile_from_string(strVs)) return prog;
                if (!fsh->compile_from_string(strFs)) return prog;
                prog->add_shader(vsh->get_handle());
                prog->add_shader(fsh->get_handle());
                prog->link();

                return prog;
            }

        protected:
            int width_;
            int height_;
            std::shared_ptr<gl::gl_texture_2DArray> inTex_;
            std::shared_ptr<gl::gl_texture_2DArray> outTex_;
            std::shared_ptr<waifu2x::Model> model_;
        };

        class YUVBasePass : public Waifu2xImageFilterPass
        {
        public:
            YUVBasePass(
                int width, int height,
                const std::shared_ptr<gl::gl_texture_2D>& inTex,
                const std::shared_ptr<gl::gl_texture_2D>& outTex,
                const std::string& shaderName)
                : width_(width), height_(height),
                  inTex_(inTex), outTex_(outTex)
            {
                const std::shared_ptr<gl::gl_program>& findProg = ProgramManager::GetInstance().Get(shaderName);
                if (findProg.get())
                {
                    prog_ = findProg;
                }
                else
                {
                    std::shared_ptr<gl::gl_program> prog = CreateProgram(shaderName);
                    ProgramManager::GetInstance().Set(shaderName, prog);
                    prog_ = prog;
                }
                const std::shared_ptr<gl::gl_frame_buffer>& findFBO = FrameBufferManager::GetInstance().Get("Waifu2x");
                if (findFBO.get())
                {
                    fbo_ = findFBO;
                }
                else
                {
                    std::shared_ptr<gl::gl_frame_buffer> fbo = std::shared_ptr<gl::gl_frame_buffer>(new gl::gl_frame_buffer());
                    FrameBufferManager::GetInstance().Set("Waifu2x", fbo);
                    fbo_ = fbo;
                }
                CreateVertex();
            }
            ~YUVBasePass()
            {
                DestroyVertex();
            }

            int run()
            {
                int width = width_;
                int height = height_;
                fbo_->bind();
                fbo_->attach_color(outTex_->get_handle(), 0);
                ::glViewport(0, 0, width, height);

                std::shared_ptr<gl::gl_program>& prog = this->prog_;
                GLuint shader_hanfle = prog->get_handle();
                ::glUseProgram(shader_hanfle);

                {
                    GLuint nInTexID = inTex_->get_handle();
                    GLuint nlocTex = prog->get_uniform_location("tex");

                    ::glActiveTexture(GL_TEXTURE0 + 0);
                    ::glBindTexture(GL_TEXTURE_2D, nInTexID);
                    prog->set_uniform(nlocTex, 0);
                }

                ::glClearColor(0, 0, 0, 1);
                ::glClear(GL_COLOR_BUFFER_BIT);

                ::glBindVertexArray(vaoHandle_);
                ::glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                ::glBindVertexArray(0);

                ::glUseProgram(0);
                fbo_->unbind();
                return 0;
            }

        public:
            static std::shared_ptr<gl::gl_program> CreateProgram(const std::string& shaderName)
            {
                std::string strVS = szVS;
                std::string strFS;
                if(shaderName == "RGB2YUV")
                {
                    strFS = szFS_RGB2YUV;
                }
                else if(shaderName == "YUV2RGB")
                {
                    strFS = szFS_YUV2RGB;
                }
                else if(shaderName == "Array2Tex3")
                {
                    strFS = szFS_Array2Tex3;
                }
                else
                {
                    strFS = szFS_YUVComposite;
                }

                std::shared_ptr<gl::gl_program> prog(new gl::gl_program());
                std::shared_ptr<gl::gl_shader> vsh(new gl::gl_shader(GL_VERTEX_SHADER));
                std::shared_ptr<gl::gl_shader> fsh(new gl::gl_shader(GL_FRAGMENT_SHADER));
                if (!vsh->compile_from_string(strVS)) return std::shared_ptr<gl::gl_program>();
                if (!fsh->compile_from_string(strFS)) return std::shared_ptr<gl::gl_program>();
                prog->add_shader(vsh->get_handle());
                prog->add_shader(fsh->get_handle());
                prog->link();
                return prog;
            }

        protected:
            int width_;
            int height_;
            std::shared_ptr<gl::gl_texture_2D> inTex_;
            std::shared_ptr<gl::gl_texture_2D> outTex_;
        };

        class RGB2YUVPass : public YUVBasePass
        {
        public:
            RGB2YUVPass(
                int width, int height,
                const std::shared_ptr<gl::gl_texture_2D>& inTex,
                const std::shared_ptr<gl::gl_texture_2D>& outTex)
                : YUVBasePass(width, height, inTex, outTex, "RGB2YUV")
            {
            }
        };

        class YUV2RGBPass : public YUVBasePass
        {
        public:
            YUV2RGBPass(
                int width, int height,
                const std::shared_ptr<gl::gl_texture_2D>& inTex,
                const std::shared_ptr<gl::gl_texture_2D>& outTex)
                : YUVBasePass(width, height, inTex, outTex, "YUV2RGB")
            {
            }
        };

        //
        class YUVCompositePass : public Waifu2xImageFilterPass
        {
        public:
            YUVCompositePass(
                int width, int height,
                const std::shared_ptr<gl::gl_texture_2DArray>& YTex,
                const std::shared_ptr<gl::gl_texture_2D>& UVTex,
                const std::shared_ptr<gl::gl_texture_2D>& outTex)
                : width_(width), height_(height),
                  YTex_(YTex), UVTex_(UVTex), outTex_(outTex)
            {
                const std::shared_ptr<gl::gl_program>& findProg = ProgramManager::GetInstance().Get("YUVComposite");
                if (findProg.get())
                {
                    prog_ = findProg;
                }
                else
                {
                    std::shared_ptr<gl::gl_program> prog = YUVBasePass::CreateProgram("YUVComposite");
                    ProgramManager::GetInstance().Set("YUVComposite", prog);
                    prog_ = prog;
                }
                const std::shared_ptr<gl::gl_frame_buffer>& findFBO = FrameBufferManager::GetInstance().Get("Waifu2x");
                if (findFBO.get())
                {
                    fbo_ = findFBO;
                }
                else
                {
                    std::shared_ptr<gl::gl_frame_buffer> fbo = std::shared_ptr<gl::gl_frame_buffer>(new gl::gl_frame_buffer());
                    FrameBufferManager::GetInstance().Set("Waifu2x", fbo);
                    fbo_ = fbo;
                }
                CreateVertex();
            }
            ~YUVCompositePass()
            {
                DestroyVertex();
            }
            int run()
            {
                int width = width_;
                int height = height_;
                fbo_->bind();
                fbo_->attach_color(outTex_->get_handle(), 0);
                ::glViewport(0, 0, width, height);

                std::shared_ptr<gl::gl_program>& prog = this->prog_;
                GLuint shader_hanfle = prog->get_handle();
                ::glUseProgram(shader_hanfle);

                {
                    GLuint nYTexID = YTex_->get_handle();
                    GLuint nUVTexID = UVTex_->get_handle();

                    GLuint nlocYTex = prog->get_uniform_location("YTex");
                    GLuint nlocUVTex = prog->get_uniform_location("UVTex");

                    ::glActiveTexture(GL_TEXTURE0 + 0);
                    ::glBindTexture(GL_TEXTURE_2D_ARRAY, nYTexID);
                    prog->set_uniform(nlocYTex, 0);

                    ::glActiveTexture(GL_TEXTURE0 + 1);
                    ::glBindTexture(GL_TEXTURE_2D, nUVTexID);
                    prog->set_uniform(nlocUVTex, 1);
                }

                ::glClearColor(0, 0, 0, 1);
                ::glClear(GL_COLOR_BUFFER_BIT);

                ::glBindVertexArray(vaoHandle_);
                ::glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                ::glBindVertexArray(0);

                ::glUseProgram(0);
                fbo_->unbind();
                return 0;
            }
        protected:
            int width_;
            int height_;
            std::shared_ptr<gl::gl_texture_2DArray> YTex_;
            std::shared_ptr<gl::gl_texture_2D> UVTex_;
            std::shared_ptr<gl::gl_texture_2D> outTex_;
        };

        class Tex2ArrayPass : public gl_pass
        {
        public:
            Tex2ArrayPass(
                int width,
                int height,
                int ch,
                const std::shared_ptr<gl::gl_texture_2D>& inTex,
                const std::shared_ptr<gl::gl_texture_2DArray>& outTex)
                : width_(width),
                  height_(height),
                  ch_(ch),
                  inTex_(inTex),
                  outTex_(outTex)
            {
            }
            int run()
            {
                int width = width_;
                int height = height_;
                std::shared_ptr<gl::gl_texture_2D>& inTex = inTex_;
                std::shared_ptr<gl::gl_texture_2DArray>& outTexArray = outTex_;
                std::vector<float> fBuffer(4 * width * height, 1.0f);
                inTex->bind();
                ::glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &fBuffer[0]);
                inTex->unbind();

                int ch = ch_;
                int offset = width * height;
                std::vector<float> fBuffer2(ch * width * height, 1.0f);
                for (int k = 0; k < width * height; k++)
                {
                    for (int c = 0; c < ch; c++)
                    {
                        fBuffer2[c * offset + k] = fBuffer[4 * k + c];
                    }
                }

                outTexArray->bind();
                ::glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, width, height, ch, GL_RED, GL_FLOAT, &fBuffer2[0]);
                outTexArray->unbind();
                return 0;
            }

        protected:
            int width_;
            int height_;
            int ch_;
            std::shared_ptr<gl::gl_texture_2D> inTex_;
            std::shared_ptr<gl::gl_texture_2DArray> outTex_;
        };

        class Array2Tex3Pass : public Waifu2xImageFilterPass
        {
        public:
            Array2Tex3Pass(
                int width,
                int height,
                const std::shared_ptr<gl::gl_texture_2DArray>& inTex,
                const std::shared_ptr<gl::gl_texture_2D>& aTex,
                const std::shared_ptr<gl::gl_texture_2D>& outTex)
                : width_(width),
                  height_(height),
                  inTex_(inTex),
                  aTex_(aTex),
                  outTex_(outTex)
            {
                const std::shared_ptr<gl::gl_program>& findProg = ProgramManager::GetInstance().Get("Array2Tex3");
                if (findProg.get())
                {
                    prog_ = findProg;
                }
                else
                {
                    std::shared_ptr<gl::gl_program> prog = YUVBasePass::CreateProgram("Array2Tex3");
                    ProgramManager::GetInstance().Set("Array2Tex3", prog);
                    prog_ = prog;
                }
                const std::shared_ptr<gl::gl_frame_buffer>& findFBO = FrameBufferManager::GetInstance().Get("Waifu2x");
                if (findFBO.get())
                {
                    fbo_ = findFBO;
                }
                else
                {
                    std::shared_ptr<gl::gl_frame_buffer> fbo = std::shared_ptr<gl::gl_frame_buffer>(new gl::gl_frame_buffer());
                    FrameBufferManager::GetInstance().Set("Waifu2x", fbo);
                    fbo_ = fbo;
                }
                CreateVertex();
            }
            ~Array2Tex3Pass()
            {
                DestroyVertex();
            }
            int run()
            {
                int width = width_;
                int height = height_;
                fbo_->bind();
                fbo_->attach_color(outTex_->get_handle(), 0);
                ::glViewport(0, 0, width, height);

                std::shared_ptr<gl::gl_program>& prog = this->prog_;
                GLuint shader_hanfle = prog->get_handle();
                ::glUseProgram(shader_hanfle);

                {
                    GLuint nInTexID = inTex_->get_handle();
                    GLuint nAlphaTexID = aTex_->get_handle();

                    GLuint nlocInTex = prog->get_uniform_location("tex");
                    GLuint nlocAlphaTex = prog->get_uniform_location("alphaTex");

                    ::glActiveTexture(GL_TEXTURE0 + 0);
                    ::glBindTexture(GL_TEXTURE_2D_ARRAY, nInTexID);
                    prog->set_uniform(nlocInTex, 0);

                    ::glActiveTexture(GL_TEXTURE0 + 1);
                    ::glBindTexture(GL_TEXTURE_2D, nAlphaTexID);
                    prog->set_uniform(nlocAlphaTex, 1);
                }

                ::glClearColor(0, 0, 0, 1);
                ::glClear(GL_COLOR_BUFFER_BIT);

                ::glBindVertexArray(vaoHandle_);
                ::glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                ::glBindVertexArray(0);

                ::glUseProgram(0);
                fbo_->unbind();
                return 0;
            }
        protected:
            int width_;
            int height_;
            std::shared_ptr<gl::gl_texture_2DArray> inTex_;
            std::shared_ptr<gl::gl_texture_2D> aTex_;
            std::shared_ptr<gl::gl_texture_2D> outTex_;
        };

        static std::vector<std::shared_ptr<gl_pass> > InitializeModelPasses(
            int width, int height,
            std::shared_ptr<gl::gl_texture_2D>& inTex,
            std::shared_ptr<gl::gl_texture_2D>& outTex,
            const std::vector<std::shared_ptr<waifu2x::Model> >& models)
        {
            int modelSize = (int)models.size();
            int ch = models[0]->GetInputPlanes();

            std::shared_ptr<Texture2DArrayManager> arrayMgr(new Texture2DArrayManager(width, height));

            std::vector<std::shared_ptr<gl_pass> > passList;

            {
                std::shared_ptr<gl_pass> pass(new Tex2ArrayPass(width, height, ch, inTex, arrayMgr->GetInputTextures(0)));
                passList.push_back(pass);
            }
            for (int i = 0; i < modelSize; i++)
            {
                std::shared_ptr<gl_pass> pass(new Waifu2xModelPass(width, height, arrayMgr->GetInputTextures(i), arrayMgr->GetOutputTextures(i), models[i], i));
                passList.push_back(pass);
            }

            if (ch == 1)
            {
                std::shared_ptr<gl_pass> pass(new YUVCompositePass(width, height, arrayMgr->GetOutputTextures(modelSize - 1), inTex, outTex));
                passList.push_back(pass);
            }
            else if (ch == 3)
            {
                std::shared_ptr<gl_pass> pass(new Array2Tex3Pass(width, height, arrayMgr->GetOutputTextures(modelSize - 1), inTex, outTex));
                passList.push_back(pass);
            }
            return passList;
        }

        static std::vector<std::shared_ptr<gl_pass> > InitializeModelPasses3(
            int width, int height,
            std::shared_ptr<gl::gl_texture_2D>& inTex,
            std::shared_ptr<gl::gl_texture_2D>& outTex,
            const std::vector<std::shared_ptr<waifu2x::Model> >& models)
        {
            return InitializeModelPasses(width, height, inTex, outTex, models);
        }

        static std::vector<std::shared_ptr<gl_pass> > InitializeModelPasses1(
            int width, int height,
            std::shared_ptr<gl::gl_texture_2D>& inTex,
            std::shared_ptr<gl::gl_texture_2D>& outTex,
            const std::vector<std::shared_ptr<waifu2x::Model> >& models)
        {
            std::vector<std::shared_ptr<gl_pass> > tmpList;

            std::shared_ptr<gl::gl_texture_2D> tmpTex1(new gl::gl_texture_2D());
            std::shared_ptr<gl::gl_texture_2D> tmpTex2(new gl::gl_texture_2D());
            {
                tmpTex1->bind();
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);   //not biliear
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);   //
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //TODO
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //TODO
                tmpTex1->unbind();
                tmpTex1->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, 0);

                tmpTex2->bind();
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);   //not biliear
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);   //
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //TODO
                ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //TODO
                tmpTex2->unbind();
                tmpTex2->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, 0);
            }
            {
                {
                    std::shared_ptr<gl_pass> pass(new RGB2YUVPass(width, height, inTex, tmpTex1));
                    tmpList.push_back(pass);
                }
                {
                    std::vector<std::shared_ptr<gl_pass> > mList = InitializeModelPasses(width, height, tmpTex1, tmpTex2, models);
                    for (int i = 0; i < mList.size(); i++)
                    {
                        tmpList.push_back(mList[i]);
                    }
                }
                {
                    std::shared_ptr<gl_pass> pass(new YUV2RGBPass(width, height, tmpTex2, outTex));
                    tmpList.push_back(pass);
                }
            }
            return tmpList;
        }

        static std::vector<std::shared_ptr<gl_pass> > InitializePasses(
            int width, int height,
            std::shared_ptr<gl::gl_texture_2D>& inTex,
            std::shared_ptr<gl::gl_texture_2D>& outTex,
            const std::vector<std::shared_ptr<waifu2x::Model> >& models)
        {
            int modelSize = (int)models.size();
            int ch = models[0]->GetInputPlanes();
            if (ch == 1)
            {
                return InitializeModelPasses1(width, height, inTex, outTex, models);
            }
            else if (ch == 3)
            {
                return InitializeModelPasses3(width, height, inTex, outTex, models);
            }
            else
            {
                std::vector<std::shared_ptr<gl_pass> > tmp;
                return tmp;
            }
        }

        class gl_waifu2x_filter_pass_imp
        {
        public:
            gl_waifu2x_filter_pass_imp(
                int width, int height,
                std::shared_ptr<gl::gl_texture_2D>& inTex,
                std::shared_ptr<gl::gl_texture_2D>& outTex,
                const std::vector<std::shared_ptr<waifu2x::Model> >& models)
            {
                passList_ = InitializePasses(width, height, inTex, outTex, models);
            }

            int run()
            {
                int nRet = 0;
                for (size_t i = 0; i < passList_.size(); i++)
                {
                    if (passList_[i]->run() != 0)
                    {
                        nRet = -1;
                    }
                }
                return nRet;
            }

        protected:
            std::vector<std::shared_ptr<gl_pass> > passList_;
        };

        gl_waifu2x_filter_pass::gl_waifu2x_filter_pass(
            int width, int height,
            std::shared_ptr<gl::gl_texture_2D>& inTex,
            std::shared_ptr<gl::gl_texture_2D>& outTex,
            const std::vector<std::shared_ptr<waifu2x::Model> >& models)
        {
            imp_ = new gl_waifu2x_filter_pass_imp(width, height, inTex, outTex, models);
        }

        gl_waifu2x_filter_pass::~gl_waifu2x_filter_pass()
        {
            delete imp_;
        }

        int gl_waifu2x_filter_pass::run()
        {
            return imp_->run();
        }
    }
}