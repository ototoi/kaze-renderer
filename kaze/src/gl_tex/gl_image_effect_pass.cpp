#include <memory>
#include "gl_image_effect_pass.h"

#ifdef _WIN32
#include <windows.h>
#else
#include <libgen.h>
#include <stdlib.h>
#endif

#ifdef __APPLE__
#include <mach-o/dyld.h>
#endif

#if defined(__linux__) || defined(FREEBSD)
#include <unistd.h>
#endif

namespace gl
{
    namespace tex
    {

        static std::string ri_get_full_path(const std::string& path)
        {
#if defined(_WIN32)
            char szFullPath[_MAX_PATH];
            _fullpath(szFullPath, path.c_str(), _MAX_PATH);
            return szFullPath;
#else
            char szFullPath[1024];
            realpath(path.c_str(), szFullPath);
            return szFullPath;
#endif
        }

        static std::string ri_get_exec_path_()
        {
#if defined(_WIN32)
            char buffer[_MAX_PATH];
            ::GetModuleFileNameA(NULL, buffer, _MAX_PATH);
            return buffer;
#elif defined(__APPLE__)
            char path[1024] = {};
            uint32_t size = sizeof(path);
            _NSGetExecutablePath(path, &size);
            return ri_get_full_path(path);
#elif defined(__linux__)
            char path[1024] = {};
            size_t size = sizeof(path);
            readlink("/proc/self/exe", path, size);
            return path;
#elif defined(FREEBSD)
            char path[1024] = {};
            size_t size = sizeof(path);
            readlink("/proc/curproc/file", path, size);
            return path;
#else
            return "";
#endif
        }

        static std::string dirname_(const std::string& path)
        {
            return path.substr(0, path.find_last_of('/'));
        }

        std::string gl_image_effect_pass::get_shaders_path()
        {
#ifdef _WIN32
            static std::string strExec = ri_get_exec_path_();
            char szDrive[_MAX_DRIVE];
            char szDir[_MAX_DIR];
            _splitpath(strExec.c_str(), szDrive, szDir, NULL, NULL);
            std::string strRet1;
            strRet1 += szDrive;
            strRet1 += szDir;
            strRet1 += "../shaders/";
            return ri_get_full_path(strRet1);
#else
            static std::string strExec = ri_get_full_path(ri_get_exec_path_());
            static std::string strDir = dirname_(strExec);
            std::string strRet1;
            strRet1 = strDir + "/";
            strRet1 += "../shaders/";
            return ri_get_full_path(strRet1) + "/";
#endif
        }

        gl_image_effect_pass::gl_image_effect_pass(
            int width,
            int height,
            const std::shared_ptr<gl::gl_program>& prog,
            const std::shared_ptr<gl::gl_texture_2D>& outTex
        )
            :width_(width), height_(height), prog_(prog), outTex_(outTex)
        {
            fbo_   = std::shared_ptr<gl::gl_frame_buffer>  (new gl::gl_frame_buffer());
            vbo_p_ = std::shared_ptr<gl::gl_vertex_buffer> (new gl::gl_vertex_buffer());
            vbo_c_ = std::shared_ptr<gl::gl_vertex_buffer> (new gl::gl_vertex_buffer());
            vao_   = std::shared_ptr<gl::gl_vertex_array>  (new gl::gl_vertex_array());

            {
                fbo_->bind();
                fbo_->attach_color(outTex_->get_handle(), 0);
                fbo_->unbind();
            }

            {
                static const float posData[] = {
                    -1.0f, -1.0f, 0.0f, //0
                    -1.0f, +1.0f, 0.0f, //1
                    +1.0f, -1.0f, 0.0f, //2
                    +1.0f, +1.0f, 0.0f, //3
                };
                static const float uvData[] = {
                    0.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 0.0f,
                    1.0f, 1.0f,
                };
                {
                    ::glBindBuffer(GL_ARRAY_BUFFER, vbo_p_->get_handle());
                    ::glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(float), posData, GL_STATIC_DRAW);
                }
                {
                    ::glBindBuffer(GL_ARRAY_BUFFER, vbo_c_->get_handle());
                    ::glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(float), uvData, GL_STATIC_DRAW);
                }
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
            }
            {
                ::glBindVertexArray(vao_->get_handle());
                {
                    GLuint index = prog->get_attrib_location("pos");
                    ::glBindBuffer(GL_ARRAY_BUFFER, vbo_p_->get_handle());
                    ::glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                {
                    GLuint index = prog->get_attrib_location("uv");
                    ::glBindBuffer(GL_ARRAY_BUFFER, vbo_c_->get_handle());
                    ::glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, 0, NULL);
                    ::glEnableVertexAttribArray(index);
                }
                ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
                ::glBindVertexArray(0u);
            }
        }

        int gl_image_effect_pass::run()
        {
            if(!prog_.get())return -1;
            if(!fbo_.get())return -1;
            if(!vao_.get())return -1;

            int width  = width_;
            int height = height_;      
            {
                fbo_->bind();
                    ::glDisable(GL_DEPTH_TEST);
                    ::glDisable(GL_LIGHTING);
                    ::glDepthMask(GL_FALSE);
                    
                    ::glViewport(0, 0, width, height);
                    ::glClearColor(0, 0, 0, 1);
                    ::glClear(GL_COLOR_BUFFER_BIT);

                    ::glUseProgram(prog_->get_handle());
                        this->set_uniforms();
                        ::glBindVertexArray(vao_->get_handle());
                            ::glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                        ::glBindVertexArray(0u);
                    ::glUseProgram(0u);

                    ::glFlush();
                fbo_->unbind();
            }
            return 0;
        }
    }
}