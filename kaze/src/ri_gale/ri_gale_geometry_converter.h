#ifndef RI_GALE_GEOMETRY_CONVERTER_H
#define RI_GALE_GEOMETRY_CONVERTER_H

#include "ri_geometry.h"
#include "gale/geometry.h"

namespace ri
{
    class ri_gale_geometry_converter
    {
    public:
        void convert(std::vector< std::shared_ptr<gale::geometry> >& geos, const ri_geometry* pGeo)const;
    };
}

#endif
