#include "ri_gale_light_converter.h"
#include "ri_shader.h"
#include "gale/light.h"
#include "gale/spot_light.h"
#include "gale/values.h"
#include <string.h>

namespace ri
{
    using namespace gale;

    static gale::matrix4 ConvertMatrix(const ri_transform& trns)
    {
        ri_transform s(trns);
        s.transpose();
        const float* f = s.get();
        gale::matrix4 tmp(f);
        return tmp;
    }

    static void GetValue(std::vector<int>& iValue, const ri_light_source* pLight, const char* key)
    {
        const ri_parameters::value_type* pv = pLight->get_parameter(key);
        if (pv)
        {
            if (pv->nType == ri_parameters::TYPE_INTEGER)
            {
                iValue = pv->integer_values;
            }
            else
            {
                iValue.clear();
            }
        }
        else
        {
            iValue.clear();
        }
    }

    static void GetValue(std::vector<float>& fValue, const ri_light_source* pLight, const char* key)
    {
        const ri_parameters::value_type* pv = pLight->get_parameter(key);
        if (pv)
        {
            if (pv->nType == ri_parameters::TYPE_FLOAT)
            {
                fValue = pv->float_values;
            }
            else
            {
                fValue.clear();
            }
        }
        else
        {
            fValue.clear();
        }
    }

    static void GetValue(std::vector<std::string>& sValue, const ri_light_source* pLight, const char* key)
    {
        const ri_parameters::value_type* pv = pLight->get_parameter(key);
        if (pv)
        {
            if (pv->nType == ri_parameters::TYPE_STRING)
            {
                sValue = pv->string_values;
            }
            else
            {
                sValue.clear();
            }
        }
        else
        {
            sValue.clear();
        }
    }

    static
    std::shared_ptr<gale::light> ConvertSpotLight(const ri_light_source* pLight)
    {
        matrix4 mat = ConvertMatrix(pLight->get_transform());

        float intensity = 1.0;
        vector3 from = vector3(0, 0, 0); //shader->current=object->world
        vector3 to = vector3(0, 0, 1);   //shader->current=object->world
        color3 lightcolor = color3(1, 1, 1);

        float coneangle = values::radians(30.0);
        float conedeltaangle = values::radians(5.0);
        //float beamdistribution = 2.0;

        std::vector<int> iValue;
        std::vector<float> fValue;
        std::vector<std::string> sValue;

        GetValue(fValue, pLight, "intensity");
        if (fValue.size() == 1)
        {
            intensity = fValue[0];
        }
        GetValue(fValue, pLight, "lightcolor");
        if (fValue.size() == 3)
        {
            lightcolor = vector3(fValue[0], fValue[1], fValue[2]);
        }
        GetValue(fValue, pLight, "from");
        if (fValue.size() == 3)
        {
            from = vector3(fValue[0], fValue[1], fValue[2]);
        }
        GetValue(fValue, pLight, "to");
        if (fValue.size() == 3)
        {
            to = vector3(fValue[0], fValue[1], fValue[2]);
        }
        GetValue(fValue, pLight, "coneangle");
        if (fValue.size() == 1)
        {
            coneangle = fValue[0];
        }
        GetValue(fValue, pLight, "conedeltaangle");
        if (fValue.size() == 1)
        {
            conedeltaangle = fValue[0];
        }

        return std::shared_ptr<gale::light>(new gale::spot_light(from, to, coneangle, coneangle+conedeltaangle, mat));
    }

    std::shared_ptr<gale::light> ri_gale_light_converter::convert(const ri_light_source* pLight)const
    {
        if(strstr(pLight->get_name().c_str(), "spot"))
        {
            return ConvertSpotLight(pLight);
        }
        return std::shared_ptr<gale::light>();
    }
}
