#include "ri_gale_task_converter.h"
#include "ri_gale_render_task.h"

#include <memory>

using namespace ri;

namespace ri
{
    static
    std::shared_ptr<ri_task> ConvertTask(const ri_frame_task* task)
    {
        const ri_options& opts = task->get_options();
        std::string strHiderType;
        opts.get("Hider", "type", strHiderType);
        if (strHiderType == "gale" || strHiderType == "gpu")
        {
            return std::shared_ptr<ri_task>(new ri_gale_render_task(task));
        }
        return std::shared_ptr<ri_task>();
    }
    
    namespace
    {
        class ri_wrapper_task : public ri_task
        {
        public:
            ri_wrapper_task(const ri_task* task)
                :task_(task)
            {}
            virtual std::string type() const { return task_->type(); };
            virtual int run() { return const_cast<ri_task*>(task_)->run(); }
        private:
            const ri_task* task_;
        };
    }

    std::shared_ptr<ri_task> ri_gale_task_converter::convert(const ri_task* task)
    {
        if (task->type() == "Frame")
        {
            return ConvertTask(static_cast<const ri_frame_task*>(task));
        }
        else
        {
            return std::shared_ptr<ri_task>(new ri_wrapper_task(task));
        }
    }
}
