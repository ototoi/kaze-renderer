#ifndef RI_GALE_CAMERA_CONVERTER_H
#define RI_GALE_CAMERA_CONVERTER_H

#include "ri_camera_options.h"
#include "gale/camera.h"

namespace ri
{
    class ri_gale_camera_converter
    {
    public:
        std::shared_ptr<gale::camera> convert(const ri_camera_options* pCmr)const;
    };
}

#endif
