#ifndef RI_GALE_RENDER_TASK_H
#define RI_GALE_RENDER_TASK_H

#include "ri_frame_task.h"

namespace ri
{
    class ri_gale_render_task : public ri_task
    {
    public:
        ri_gale_render_task(const ri_frame_task* task);
        ~ri_gale_render_task();
        virtual std::string type() const { return "GaleRender"; }
        virtual int run();

    protected:
        const ri_frame_task* task_;
    };
}

#endif
