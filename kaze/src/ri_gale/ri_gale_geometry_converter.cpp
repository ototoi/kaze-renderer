#include "ri_gale_geometry_converter.h"

#include "ri_shader.h"
#include "ri_geometry.h"
#include "ri_geometry_converter.h"
#include "ri_path_resolver.h"

#include "gale/gale.h"
#include "gale/geometry.h"
#include "gale/smesh_geometry.h"
#include "gale/points_geometry.h"
#include "gale/curves_geometry.h"
#include "gale/light.h"
#include "gale/spot_light.h"
#include "gale/bezier.h"
#include "gale/values.h"
#include "gale/image_texture.h"

#include <iostream>
#include <memory>
#include <vector>
#include <deque>

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif

namespace ri
{
    using namespace gale::values;

    static gale::matrix4 ConvertMatrix(const ri_transform& trns)
    {
        ri_transform s(trns);
        s.transpose();
        const float* f = s.get();
        gale::matrix4 tmp(f);
        return tmp;
    }
    
    static gale::color3 GetCs(const ri_attributes* pAttr)
    {
        std::vector<float> fv;
        gale::color3 Cs(1, 1, 1);
        pAttr->get("Color", "color", fv);
        if (fv.size() == 3)
        {
            Cs[0] = fv[0];
            Cs[1] = fv[1];
            Cs[2] = fv[2];
        }
        return Cs;
    }

    static inline float z2v(float x)
    {
        return asin(2.0f * (x - 0.5f)) / pi() + 0.5f;
    }
    
    static
    float Repeat(float x)
    {
        if(x < 0)x = x - floor(x);
        if(x > 1)x = x - floor(x);
        return x;
    }
    
    static
    gale::vector2 Repeat(const gale::vector2& uvs)
    {
        return gale::vector2(Repeat(uvs[0]),Repeat(uvs[1]));
    }
    
    
    static
    void RepeatUV(std::vector<gale::vector2>& uvs)
    {
        size_t sz = uvs.size();
        for(size_t i = 0;i<sz;i++)
        {
            uvs[i] = Repeat(uvs[i]);
        }
    }
    
    static
    void BoundingBoxHelper(
        std::vector<gale::vector3>& vertices,
        std::vector<gale::vector3>& normals,
        std::vector<gale::vector2>& uvs,
        std::vector<int>& indices,
        const float P[],
        const float N[]
    )
    {
        static const float _UV[] = 
        {
            0,0,
            1,0,
            1,1,
            0,1
        };
        int offset = vertices.size();
        for(int i = 0;i<4;i++)
        {
            vertices.push_back(gale::vector3(P+3*i));
            uvs.push_back(gale::vector2(_UV+2*i));
            normals.push_back (gale::vector3(N));
        }
        indices.push_back(offset + 0);
        indices.push_back(offset + 1);
        indices.push_back(offset + 2);
        indices.push_back(offset + 0);
        indices.push_back(offset + 2);
        indices.push_back(offset + 3);
    }
    
    static
    std::shared_ptr<gale::geometry> BoundingBoxGeometry(
        const std::vector<gale::vector3>& points,
        gale::matrix4 mat
    ) 
    {
        static const float FAR = gale::values::far();
        gale::vector3 min = gale::vector3(+FAR,+FAR,+FAR);
        gale::vector3 max = gale::vector3(-FAR,-FAR,-FAR);
        size_t sz = points.size();
        for(size_t i=0;i<sz;i++)
        {
            for(int j=0;j<3;j++)
            {
                min[j] = std::min<float>(min[j], points[i][j]);
                max[j] = std::max<float>(max[j], points[i][j]);
            }
        }
        
        std::vector<gale::vector3> vertices;
        std::vector<gale::vector3> normals;
        std::vector<gale::vector2> uvs;
        std::vector<int> indices;
        {//-X
            float P[3*4] = 
            {
                 min[0], min[1], min[2],
                 min[0], max[1], min[2], 
                 min[0], max[1], max[2],
                 min[0], min[1], max[2],
            };
            float N[3] = {-1,0,0};
            BoundingBoxHelper(vertices, normals, uvs, indices, P, N);
        }
        {//+X
            float P[3*4] = 
            {
                 max[0], max[1], min[2],
                 max[0], min[1], min[2], 
                 max[0], min[1], max[2],
                 max[0], max[1], max[2],
            };
            float N[3] = {+1,0,0};
            BoundingBoxHelper(vertices, normals, uvs, indices, P, N);
        }
        {//-Y
            float P[3*4] = 
            {
                 max[0], min[1], min[2],
                 min[0], min[1], min[2], 
                 min[0], min[1], max[2],
                 max[0], min[1], max[2],
            };
            float N[3] = {0,-1,0};
            BoundingBoxHelper(vertices, normals, uvs, indices, P, N);
        }
        {//+Y
            float P[3*4] = 
            {
                 min[0], max[1], min[2],
                 max[0], max[1], min[2], 
                 max[0], max[1], max[2],
                 min[0], max[1], max[2],
            };
            float N[3] = {0,+1,0};
            BoundingBoxHelper(vertices, normals, uvs, indices, P, N);
        }
        {//-Z
            float P[3*4] = 
            {
                 max[0], min[1], min[2],
                 min[0], min[1], min[2], 
                 min[0], max[1], min[2],
                 max[0], max[1], min[2],
            };
            float N[3] = {0,0,-1};
            BoundingBoxHelper(vertices, normals, uvs, indices, P, N);
        }
        {//+Z
            float P[3*4] = 
            {
                 min[0], min[1], max[2],
                 min[0], max[1], max[2], 
                 max[0], max[1], max[2],
                 max[0], min[1], max[2],
            };
            float N[3] = {0,0,+1};
            BoundingBoxHelper(vertices, normals, uvs, indices, P, N);
        }
        
        return std::shared_ptr<gale::geometry>(new gale::smesh_geometry(vertices, normals, uvs, indices, mat));
    }
    
    static
    std::shared_ptr<gale::geometry> ConvertGeometry(const ri_sphere_geometry* pGeo)
    {
        static const int DIVU = 32;
        static const int DIVV = 16;

        gale::matrix4 mat = ConvertMatrix(pGeo->get_transform());

        float radius = pGeo->get_radius();
        float zmin = pGeo->get_zmin();
        float zmax = pGeo->get_zmax();
        float tmax = pGeo->get_tmax();

        std::deque<float> us;
        std::deque<float> vs;

        float du = 1.0f / DIVU;
        float dv = 1.0f / DIVV;

        if(tmax == 0 || zmin == zmax)
        {
            return std::shared_ptr<gale::geometry>();
        }

        bool uInverse = false;
        float umin = 0;
        float umax = 1;
        if(tmax >= 0)
        {
            tmax = std::min<float>(tmax, 360.0f);

            umin = 0;
            umax = tmax / 360.0f;
        }
        else
        {
            tmax = std::max<float>(-360.0f, tmax);

            umin = 1.0f + tmax / 360.0f;
            umax = 1.0f;
            uInverse = true;
        }
        for(int i = 0; i < DIVU+1; i++)
        {
            float u = i * du;
            if(umin <= u && u <= umax)
            {
                us.push_back(u);
            }
        }
        if(us.size() < 1)
        {
            us.push_front(umin);
            us.push_back(umax);
        }
        else
        {
            if(us.back() != umax)
            {
                us.push_back(umax);
            }
        }

        bool vInverse = false;
        if(zmax < zmin)
        {
            std::swap(zmin, zmax);
            vInverse = true;
        }
        float vmin = 0;
        float vmax = 1;
        if (-radius < zmin || zmax < +radius)
        {
            vmin = 0.5 * (1 + zmin / radius);
            vmax = 0.5 * (1 + zmax / radius);
            if (vmin > vmax) std::swap(vmin, vmax);
            vmin = std::max(0.0f, vmin);
            vmax = std::min(1.0f, vmax);
            vmin = z2v(vmin);
            vmax = z2v(vmax);
        }

        if(vmin >= vmax)
        {
            return std::shared_ptr<gale::geometry>();
        }

        for(int i = 0;i<DIVV+1;i++)
        {
            float v = i * dv;
            if(vmin <= v && v <= vmax)
            {
                vs.push_back(v);
            }
        }
        if(vs.size() < 1)
        {
            vs.push_front(vmin);
            vs.push_back(vmax);
        }
        else
        {
            if(vs.front() != vmin)
            {
                vs.push_front(vmin);
            }
            if(vs.back() != vmax)
            {
                vs.push_back(vmax);
            }
        }

        std::vector<gale::vector2> uvs;
        std::vector<gale::vector3> vertices;
        std::vector<gale::vector3> normals;
        for(int j = 0;j<vs.size();j++)
        {
            float v = vs[j];
            float theta = (2 * v - 1) * pi() * 0.5f;
            for(int i = 0;i<us.size();i++)
            {
                float u = us[i];
                float phi = u * 2 * pi();

                float l = fabs(cos(theta));
                float x = l*cos(phi);
                float y = l*sin(phi);
                float z = sin(theta);

                float uu = float(i) / (us.size()-1);
                float vv = float(j) / (vs.size()-1);
                if(uInverse)uu = 1.0f - uu;
                if(vInverse)vv = 1.0f - vv;
                gale::vector2 s = gale::vector2(uu, vv);
                gale::vector3 n = normalize(gale::vector3(x,y,z));
                gale::vector3 p = n*radius;
                uvs.push_back(s);
                normals.push_back(n);
                vertices.push_back(p);
            }
        }
        int W = us.size();
        //int H = vs.size();
        std::vector<int> indices;
        for(int j = 0;j<vs.size()-1;j++)
        {
            for(int i = 0;i<us.size()-1;i++)
            {
                int idx0 = j*W+i;
                int idx1 = j*W+i+1;
                int idx2 = (j+1)*W+i+1;
                int idx3 = (j+1)*W+i;

                indices.push_back(idx0);indices.push_back(idx2);indices.push_back(idx1);
                indices.push_back(idx0);indices.push_back(idx3);indices.push_back(idx2);
            }
        }

        return std::shared_ptr<gale::geometry>(new gale::smesh_geometry(vertices, normals, uvs, indices, mat));
    }

    static
    std::shared_ptr<gale::geometry> ConvertGeometryDisk(float height, float radius, float tmax, gale::matrix4 mat, int DIVU)
    {
        std::deque<float> us;
        //std::deque<float> vs;

        float du = 1.0f / DIVU;
        //float dv = 1.0f / DIVV;

        if(tmax == 0)
        {
            return std::shared_ptr<gale::geometry>();
        }

        bool uInverse = false;
        float umin = 0;
        float umax = 1;
        if(tmax >= 0)
        {
            tmax = std::min<float>(tmax, 360.0f);

            umin = 0;
            umax = tmax / 360.0f;
        }
        else
        {
            tmax = std::max<float>(-360.0f, tmax);

            umin = 1.0f + tmax / 360.0f;
            umax = 1.0f;
            uInverse = true;
        }
        for(int i = 0; i < DIVU+1; i++)
        {
            float u = i * du;
            if(umin <= u && u <= umax)
            {
                us.push_back(u);
            }
        }
        if(us.size() < 1)
        {
            us.push_front(umin);
            us.push_back(umax);
        }
        else
        {
            if(us.back() != umax)
            {
                us.push_back(umax);
            }
        }

        std::vector<gale::vector2> uvs;
        std::vector<gale::vector3> vertices;
        std::vector<gale::vector3> normals;
        //for(int j = 0;j<2;j++)
        {
            float v = 0;
            for(int i = 0;i<us.size();i++)
            {
                float u = us[i];
                float phi = u * 2 * pi();

                float r = radius*(1-v);
                float x = r*cos(phi);
                float y = r*sin(phi);
                float z = height;

                float uu = float(i) / (us.size()-1);
                float vv = 0;
                if(uInverse)uu = 1.0f - uu;

                gale::vector2 s = gale::vector2(uu, vv);
                gale::vector3 n = normalize(gale::vector3(0,0,1));
                gale::vector3 p = gale::vector3(x,y,z);
                uvs.push_back(s);
                normals.push_back(n);
                vertices.push_back(p);
            }
        }
        {
            for(int i = 0;i<us.size()-1;i++)
            {
                float u = us[i];

                float x = 0;
                float y = 0;
                float z = height;

                float uu = float(i+0.5f) / (us.size()-1);
                float vv = 1;
                if(uInverse)uu = 1.0f - uu;

                gale::vector2 s = gale::vector2(uu, vv);
                gale::vector3 n = normalize(gale::vector3(0,0,1));
                gale::vector3 p = gale::vector3(x,y,z);
                uvs.push_back(s);
                normals.push_back(n);
                vertices.push_back(p);
            }
        }

        int W = us.size();
        //int H = vs.size();
        std::vector<int> indices;

        for(int i = 0;i<us.size()-1;i++)
        {
            int idx0 = i;
            int idx1 = i+1;
            int idx2 = W+i;

            indices.push_back(idx0);indices.push_back(idx2);indices.push_back(idx1);
        }

        return std::shared_ptr<gale::geometry>(new gale::smesh_geometry(vertices, normals, uvs, indices, mat));
    }

    static
    std::shared_ptr<gale::geometry> ConvertGeometry(const ri_cone_geometry* pGeo)
    {
        static const int DIVU = 32;
        //static const int DIVV = 16;

        gale::matrix4 mat = ConvertMatrix(pGeo->get_transform());

        float height = pGeo->get_height();
        float radius = fabs(pGeo->get_radius());
        float tmax = pGeo->get_tmax();

        std::deque<float> us;
        std::deque<float> vs;

        float du = 1.0f / DIVU;

        if(tmax == 0)
        {
            return std::shared_ptr<gale::geometry>();
        }

        if(fabs(height) <= 1e-6)
        {
            return ConvertGeometryDisk(0, radius, tmax, mat, DIVU);
        }

        bool uInverse = false;
        float umin = 0;
        float umax = 1;
        if(tmax >= 0)
        {
            tmax = std::min<float>(tmax, 360.0f);

            umin = 0;
            umax = tmax / 360.0f;
        }
        else
        {
            tmax = std::max<float>(-360.0f, tmax);

            umin = 1.0f + tmax / 360.0f;
            umax = 1.0f;
            uInverse = true;
        }
        for(int i = 0; i < DIVU+1; i++)
        {
            float u = i * du;
            if(umin <= u && u <= umax)
            {
                us.push_back(u);
            }
        }
        if(us.size() < 1)
        {
            us.push_front(umin);
            us.push_back(umax);
        }
        else
        {
            if(us.back() != umax)
            {
                us.push_back(umax);
            }
        }

        std::vector<gale::vector2> uvs;
        std::vector<gale::vector3> vertices;
        std::vector<gale::vector3> normals;

        {
            for(int i = 0;i<us.size();i++)
            {
                float u = us[i];
                float phi = u * 2 * pi();

                float r = radius;
                float x = cos(phi);
                float y = sin(phi);
                float z = 0;

                float uu = float(i) / (us.size()-1);
                float vv = 0;
                if(uInverse)uu = 1.0f - uu;
                //if(vInverse)vv = 1.0f - vv;
                gale::vector2 s = gale::vector2(uu, vv);
                gale::vector3 n = normalize(gale::vector3(x,y,r/height));
                gale::vector3 p = gale::vector3(r*x,r*y,z);
                uvs.push_back(s);
                normals.push_back(n);
                vertices.push_back(p);
            }
        }
        {
            for(int i = 0;i<us.size()-1;i++)
            {
                float u = us[i];

                float x = 0;
                float y = 0;
                float z = height;

                float uu = float(i+0.5f) / (us.size()-1);
                float vv = 1;
                if(uInverse)uu = 1.0f - uu;
                //if(vInverse)vv = 1.0f - vv;
                gale::vector2 s = gale::vector2(uu, vv);
                gale::vector3 n = normalize(gale::vector3(x,y,z));//TODO
                gale::vector3 p = gale::vector3(x,y,z);
                uvs.push_back(s);
                normals.push_back(n);
                vertices.push_back(p);
            }
        }

        int W = us.size();
        //int H = vs.size();
        std::vector<int> indices;

        for(int i = 0;i<us.size()-1;i++)
        {
            int idx0 = i;
            int idx1 = i+1;
            int idx2 = W+i;

            indices.push_back(idx0);indices.push_back(idx2);indices.push_back(idx1);
        }

        return std::shared_ptr<gale::geometry>(new gale::smesh_geometry(vertices, normals, uvs, indices, mat));
    }

    static
    std::shared_ptr<gale::geometry> ConvertGeometry(const ri_cylinder_geometry* pGeo)
    {
        static const int DIVU = 32;

        gale::matrix4 mat = ConvertMatrix(pGeo->get_transform());

        float radius = pGeo->get_radius();
        float zmin = pGeo->get_zmin();
        float zmax = pGeo->get_zmax();
        float tmax = pGeo->get_tmax();

        std::deque<float> us;
        //std::deque<float> vs;

        float du = 1.0f / DIVU;
        //float dv = 1.0f / DIVV;

        if(tmax == 0 || zmin == zmax)
        {
            return std::shared_ptr<gale::geometry>();
        }

        bool uInverse = false;
        float umin = 0;
        float umax = 1;
        if(tmax >= 0)
        {
            tmax = std::min<float>(tmax, 360.0f);

            umin = 0;
            umax = tmax / 360.0f;
        }
        else
        {
            tmax = std::max<float>(-360.0f, tmax);

            umin = 1.0f + tmax / 360.0f;
            umax = 1.0f;
            uInverse = true;
        }
        for(int i = 0; i < DIVU+1; i++)
        {
            float u = i * du;
            if(umin <= u && u <= umax)
            {
                us.push_back(u);
            }
        }
        if(us.size() < 1)
        {
            us.push_front(umin);
            us.push_back(umax);
        }
        else
        {
            if(us.back() != umax)
            {
                us.push_back(umax);
            }
        }

        bool vInverse = false;
        if(zmax < zmin)
        {
            std::swap(zmin, zmax);
            vInverse = true;
        }
        //float vmin = 0;
        //float vmax = 1;

        std::vector<gale::vector2> uvs;
        std::vector<gale::vector3> vertices;
        std::vector<gale::vector3> normals;
        for(int j = 0;j<2;j++)
        {
            float v = (float)j;
            for(int i = 0;i<us.size();i++)
            {
                float u = us[i];
                float phi = u * 2 * pi();

                float r = radius;
                float x = r*cos(phi);
                float y = r*sin(phi);
                float z = zmin + (zmax - zmin)*v;

                float uu = float(i) / (us.size()-1);
                float vv = float(j);
                if(uInverse)uu = 1.0f - uu;
                if(vInverse)vv = 1.0f - vv;

                gale::vector2 s = gale::vector2(uu, vv);
                gale::vector3 n = normalize(gale::vector3(x,y,0));
                gale::vector3 p = gale::vector3(x,y,z);
                uvs.push_back(s);
                normals.push_back(n);
                vertices.push_back(p);
            }
        }

        int W = us.size();
        //int H = vs.size();
        std::vector<int> indices;
        for(int j = 0;j<1;j++)
        {
            for(int i = 0;i<us.size()-1;i++)
            {
                int idx0 = j*W+i;
                int idx1 = j*W+i+1;
                int idx2 = (j+1)*W+i+1;
                int idx3 = (j+1)*W+i;

                indices.push_back(idx0);indices.push_back(idx2);indices.push_back(idx1);
                indices.push_back(idx0);indices.push_back(idx3);indices.push_back(idx2);
            }
        }

        return std::shared_ptr<gale::geometry>(new gale::smesh_geometry(vertices, normals, uvs, indices, mat));
    }

    static
    std::shared_ptr<gale::geometry> ConvertGeometry(const ri_disk_geometry* pGeo)
    {
        static const int DIVU = 32;

        gale::matrix4 mat = ConvertMatrix(pGeo->get_transform());

        float height = pGeo->get_height();
        float radius = pGeo->get_radius();
        float tmax = pGeo->get_tmax();

        return ConvertGeometryDisk(height, radius, tmax, mat, DIVU);
    }

    static
    std::shared_ptr<gale::geometry> ConvertGeometry(const ri_torus_geometry* pGeo)
    {
        static const int DIVU = 32;

        gale::matrix4 mat = ConvertMatrix(pGeo->get_transform());

        float majrad = pGeo->get_majrad();
        float minrad = pGeo->get_minrad();
        float phimin = pGeo->get_phimin();
        float phimax = pGeo->get_phimax();
        float tmax   = pGeo->get_tmax();

        return std::shared_ptr<gale::geometry>();
    }

    static
    std::shared_ptr<gale::geometry> ConvertGeometry(const ri_hyperboloid_geometry* pGeo)
    {
        static const int DIVU = 32;

        gale::matrix4 mat = ConvertMatrix(pGeo->get_transform());



        return std::shared_ptr<gale::geometry>();
    }

    static
    std::shared_ptr<gale::geometry> ConvertGeometry(const ri_paraboloid_geometry* pGeo)
    {
        static const int DIVU = 32;

        gale::matrix4 mat = ConvertMatrix(pGeo->get_transform());

        float rmax = pGeo->get_rmax();
        float zmin = pGeo->get_zmin();
        float zmax = pGeo->get_zmax();
        float tmax = pGeo->get_tmax();

        return std::shared_ptr<gale::geometry>();
    }
    
    static const std::string* GetString(const ri_shader* pShader, const char* key)
    {
        const ri_parameters::value_type* p_param = pShader->get_parameter(key);
        if (p_param && p_param->nType == ri_parameters::TYPE_STRING)
        {
            return &(p_param->string_values[0]);
        }
        return NULL;
    }

    static const float* GetFloat(const ri_shader* pShader, const char* key)
    {
        const ri_parameters::value_type* p_param = pShader->get_parameter(key);
        if (p_param && p_param->nType == ri_parameters::TYPE_FLOAT)
        {
            return &(p_param->float_values[0]);
        }
        return NULL;
    }
    
    static bool IsExistFile(const char* szFilePath)
    {
#ifdef _WIN32
        struct _stat s;
        int nRet = _stat(szFilePath, &s);
        return (nRet == 0);
#else
        struct stat s;
        int nRet = stat(szFilePath, &s);
        return (nRet == 0);
#endif
    }
    
    static std::string GetTexturePath(const ri_shader* pShader, const char* szName)
    {
        std::string name = szName;
        ri_path_resolver pr(pShader->get_options());
        std::vector<std::string> texture_paths = pr.get_textures_paths();
        std::vector<std::string> paths;
        for (size_t i = 0; i < texture_paths.size(); i++)
        {
            paths.push_back(texture_paths[i] + name);
        }

        for (int i = 0; i < paths.size(); i++)
        {
            //std::string mrky = paths[i] + ".mrky";
            if (IsExistFile(paths[i].c_str()))
            {
                return paths[i];
            }
        }
        return "";
    }
    
    static 
    std::shared_ptr<gale::material> ConvertMeshMaterial(const ri_attributes* pAttr)
    {
        std::unique_ptr<gale::smesh_material> mtl = std::unique_ptr<gale::smesh_material>(new gale::smesh_material());
        const ri_shader* pShader = pAttr->get_suraface();
        if(pShader && pShader->get_name() == "MQMaterial")
        {
            bool isTransparency = false;
            {
                const float* f = GetFloat(pShader, "Alpha");
                if(f)
                {
                    mtl->set_texture("alpha_tex", std::shared_ptr<gale::texture>(new gale::image_texture(*f,*f,*f,*f)));   
                    if(*f < 1.0)
                    {
                        isTransparency = true;
                    }
                }
            }
            {
                const std::string* s = GetString(pShader, "TextureName");
                if(s)
                {
                    std::string strPath = GetTexturePath(pShader, s->c_str());
                    if(!strPath.empty())
                    {
                        mtl->set_texture("albedo_tex", std::shared_ptr<gale::texture>(new gale::image_texture(strPath.c_str())));
                    }
                }
            }
            {
                const std::string* s = GetString(pShader, "AlphaName");
                if(s && !s->empty() && *s == "tex_eyes2_t.jpg")
                {
                    std::string strPath = GetTexturePath(pShader, s->c_str());
                    if(!strPath.empty())
                    {
                        mtl->set_texture("alpha_tex", std::shared_ptr<gale::texture>(new gale::image_texture(strPath.c_str())));
                        isTransparency = true;
                    }
                }
            }
            mtl->set_opacity(!isTransparency);
        }
        return std::shared_ptr<gale::material>(mtl.release());
    }

#define PRE_TRANS 0
    static
    void ConvertGeometry(std::vector< std::shared_ptr<gale::geometry> >& geos, const ri_points_triangle_polygons_geometry* pGeo)
    {
        gale::matrix4 mat = ConvertMatrix(pGeo->get_transform());

        int nfaces = pGeo->get_nfaces();
        const int* verts = pGeo->get_verts();
        int nverts = pGeo->get_nverts();
        const float* P = pGeo->get_P();
        const float* N = pGeo->get_N();
        const float* Np = pGeo->get_Np();
        const float* Nf = pGeo->get_Nf();
        const float* Cs = pGeo->get_Cs();

        const float* st  = pGeo->get_st();
        const float* stf = pGeo->get_stf();

        if(!(Nf || Np || stf))
        {
            size_t vsz = nverts;
            size_t isz = nfaces*3;

            std::vector<gale::vector3> vertices(vsz);
            for(size_t i = 0;i<vsz;i++)
            {
                vertices[i] = gale::vector3(P+3*i);
            }
            std::vector<gale::vector3> normals(vsz);
            if(N)
            {
                for(size_t i = 0;i<vsz;i++)
                {
                    normals[i] = gale::vector3(N+3*i);
                }
            }
            else
            {
                for(size_t i = 0;i<vsz;i++)
                {
                    normals[i] = gale::vector3(0,0,1);
                }
            }
            
            std::vector<gale::vector3> colors(vsz);
            if(Cs)
            {
                for(size_t i = 0;i<vsz;i++)
                {
                    colors[i] = gale::vector3(Cs+3*i);
                }
            }
            else
            {
                gale::vector3 c = GetCs(&(pGeo->get_attributes()));
                for(size_t i = 0;i<vsz;i++)
                {
                    colors[i] = c;
                }
            }
            
            std::vector<gale::vector2> uvs(vsz);
            if(st)
            {
                for(size_t i = 0;i<vsz;i++)
                {
                    uvs[i] = gale::vector2(st+2*i);
                }
                RepeatUV(uvs);
            }
            else
            {
                for(size_t i = 0;i<vsz;i++)
                {
                    uvs[i] = gale::vector2(0,0);
                }
            }

            std::vector<int> indices(isz);
            for(size_t i = 0;i<isz/3;i++)
            {
                indices[3*i+0] = verts[3*i+0];
                indices[3*i+1] = verts[3*i+2];
                indices[3*i+2] = verts[3*i+1];
            }
            
#if PRE_TRANS
            {
                for(size_t i = 0;i<vsz;i++)
                {
                    vertices[i] = mat * vertices[i];
                }
                mat = gale::mat4_gen::identity();
            }
#endif
            std::unique_ptr<gale::smesh_geometry> ap(new gale::smesh_geometry(vertices, normals, colors, uvs, indices, mat));
            std::shared_ptr<gale::material> mtl = ConvertMeshMaterial(&pGeo->get_attributes());
            ap->set_material(mtl);
            geos.push_back(std::shared_ptr<gale::geometry>(ap.release()));
        }
        else
        {
            size_t vsz = nfaces*3;
            size_t isz = nfaces*3;

            std::vector<gale::vector3> vertices(vsz);
            for(size_t i = 0;i<vsz;i++)
            {
                vertices[i] = gale::vector3(P+3*verts[i]);
            }
            std::vector<gale::vector3> normals(vsz);
            
            if(Nf)
            {
                for(size_t i = 0;i<vsz;i++)
                {
                    normals[i] = gale::vector3(Nf+3*i);
                }
            }
            else if(N)
            {
                for(size_t i = 0;i<vsz;i++)
                {
                    normals[i] = gale::vector3(N+3*verts[i]);
                }
            }
            else if(Np)
            {
                for(size_t i = 0;i<nfaces;i++)
                {
                    gale::vector3 n = gale::vector3(Np+3*i);
                    normals[3*i+0] = n;
                    normals[3*i+1] = n;
                    normals[3*i+2] = n;
                }
            }
            else
            {
                for(size_t i = 0;i<vsz;i++)
                {
                    normals[i] = gale::vector3(0,0,1);
                }
            }
            
            std::vector<gale::vector3> colors(vsz);
            if(Cs)
            {
                for(size_t i = 0;i<vsz;i++)
                {
                    colors[i] = gale::vector3(Cs+3*verts[i]);
                }
            }
            else
            {
                gale::vector3 c = GetCs(&(pGeo->get_attributes()));
                for(size_t i = 0;i<vsz;i++)
                {
                    colors[i] = c;
                }
            }

            std::vector<gale::vector2> uvs(vsz);
            
            if(stf)
            {
                for(size_t i = 0;i<vsz;i++)
                {
                    uvs[i] = gale::vector2(stf+2*i);
                }
                RepeatUV(uvs);
            }
            else
            {
                for(size_t i = 0;i<vsz;i++)
                {
                    uvs[i] = gale::vector2(0,0);
                }
            }

            std::vector<int> indices(isz);
            for(size_t i = 0;i<isz/3;i++)
            {
                indices[3*i+0] = 3*i+0;
                indices[3*i+1] = 3*i+2;
                indices[3*i+2] = 3*i+1;
            }
#if PRE_TRANS
            {
                for(size_t i = 0;i<vsz;i++)
                {
                    vertices[i] = mat * vertices[i];
                }
                mat = gale::mat4_gen::identity();
            }
#endif
            std::unique_ptr<gale::smesh_geometry> ap(new gale::smesh_geometry(vertices, normals, colors, uvs, indices, mat));
            std::shared_ptr<gale::material> mtl = ConvertMeshMaterial(&pGeo->get_attributes());
            ap->set_material(mtl);
            geos.push_back(std::shared_ptr<gale::geometry>(ap.release()));
            //geos.push_back(BoundingBoxGeometry(vertices, mat));//
        }
    }

    static
    std::shared_ptr<gale::geometry> ConvertGeometry(const ri_bezier_patch_mesh_geometry* pGeo)
    {
        gale::matrix4 mat = ConvertMatrix(pGeo->get_transform());

        int nu = pGeo->get_nu();
        int nv = pGeo->get_nv();
        int uorder = pGeo->get_uorder();
        int vorder = pGeo->get_vorder();
        const float* P = pGeo->get_P();

        int DIVU = nu*4;
        int DIVV = nv*4;

        int nPu = (nu - 1) / (uorder -1);
        int nPv = (nv - 1) / (vorder -1);

        int nnu = nPu*DIVU+1;
        int nnv = nPv*DIVV+1;

        std::vector<gale::vector2> uvs(nnu*nnv);
        std::vector<gale::vector3> vertices(nnu*nnv);
        std::vector<gale::vector3> normals(nnu*nnv);
        for(int jj = 0;jj<nPv;jj++)
        {
            for(int ii = 0;ii<nPu;ii++)
            {
                std::vector<gale::vector3> PP(uorder*vorder);

                for(int j = 0;j<vorder;j++)
                {
                    for(int i = 0;i<uorder;i++)
                    {
                        int iii = ii*(uorder-1)+i;
                        int jjj = jj*(vorder-1)+j;
                        int index = jjj*nu+iii;
                        PP[j*uorder+i] = gale::vector3(P + 3*index);
                    }
                }

                for(int j = 0;j<DIVV+1;j++)
                {
                    float v = float(j) / DIVV;
                    for(int i = 0;i<DIVU+1;i++)
                    {
                        float u = float(i) / DIVU;

                        int iii = ii*DIVU+i;
                        int jjj = jj*DIVV+j;
                        int index = jjj * nnu + iii;

                        float uu = (float(ii) + u)/ nPu;
                        float vv = (float(jj) + v)/ nPv;

                        vertices[index] = gale::bezier_evaluate(&PP[0], uorder, vorder, u, v);
                        normals [index] = gale::bezier_evaluate_normal(&PP[0], uorder, vorder, u, v);
                        uvs     [index] = gale::vector2(uu,vv);//

                        if(length(normals [index]) <= 0)
                        {
                            gale::vector3 n = gale::bezier_evaluate_normal(&PP[0], uorder, vorder, u+0.01f, v+0.01f); 
                                            + gale::bezier_evaluate_normal(&PP[0], uorder, vorder, u-0.01f, v-0.01f);  
                            normals [index] = normalize(n);
                        }
                    }
                }
            }
        }

        int W = nnu;
        //int H = vs.size();
        std::vector<int> indices;
        for(int j = 0;j<nnv-1;j++)
        {
            for(int i = 0;i<nnu-1;i++)
            {
                int idx0 = j*W+i;
                int idx1 = j*W+i+1;
                int idx2 = (j+1)*W+i+1;
                int idx3 = (j+1)*W+i;

                indices.push_back(idx0);indices.push_back(idx2);indices.push_back(idx1);
                indices.push_back(idx0);indices.push_back(idx3);indices.push_back(idx2);
            }
        }

        return std::shared_ptr<gale::geometry>(new gale::smesh_geometry(vertices, normals, uvs, indices, mat));
    }
    
    static
    std::shared_ptr<gale::geometry> ConvertGeometry(const ri_points_geometry* pGeo)
    {
        gale::matrix4 mat = ConvertMatrix(pGeo->get_transform());
        
        int nverts = pGeo->get_nverts();
        const float* P = pGeo->get_P();
        const float* width = pGeo->get_width();
        const float* Cs = pGeo->get_Cs();
        const float* Os = pGeo->get_Os();
        float constantwidth = pGeo->get_constantwidth();
        
        if(!P)return std::shared_ptr<gale::geometry>();
        
        std::vector<gale::vector3> positions(nverts);
        std::vector<gale::vector2> widths   (nverts);
        std::vector<gale::color3>  colors   (nverts);
        for(size_t i = 0;i<nverts;i++)
        {
            positions[i] = gale::vector3(P + 3 * i);
        }
        if(width)
        {
            for(size_t i = 0;i<nverts;i++)
            {
                float w = width[i];
                widths[i] = gale::vector2(w,w);
            }
        }
        else
        {
            float w = constantwidth;
            for(size_t i = 0;i<nverts;i++)
            {
                widths[i] = gale::vector2(w,w);
            }
        }
        
        if(Cs)
        {
            for(size_t i = 0;i<nverts;i++)
            {
                colors[i] = gale::vector3(Cs + 3 * i);
            }
        }
        else
        {
            for(size_t i = 0;i<nverts;i++)
            {
                colors[i] = gale::vector3(1,1,1);
            }
        }
        
        return std::shared_ptr<gale::geometry>(new gale::points_geometry(positions, widths, colors, mat));
    }

    static
    void ConvertGeometry(std::vector< std::shared_ptr<gale::geometry> >& geos, const ri_geometry* pGeo);
    
    static
    void ConvertGeometry(std::vector< std::shared_ptr<gale::geometry> >& geos, const ri_procedural_evaluation_geometry* pGeo)
    {
         for (size_t i = 0; i < pGeo->get_size(); i++)
         {
             ConvertGeometry(geos, pGeo->get_at(i));
         }
    }
    
    static
    std::shared_ptr<gale::geometry> ConvertGeometry(const ri_bezier_curves_geometry* pGeo)
    {
        static const int DIVV = 8;
        
        typedef gale::curves_geometry::curve_vertex curve_vertex;
        typedef gale::curves_geometry::curve_strand curve_strand;
        
        gale::matrix4 mat = ConvertMatrix(pGeo->get_transform());
        
        int ncurves = pGeo->get_ncurves();
        const int* nvertices = pGeo->get_nvertices();
        int order = pGeo->get_order();

        const float* P = pGeo->get_P();
        const float* N = pGeo->get_N();

        const float* width = pGeo->get_width();
        const float* Cs = pGeo->get_Cs();
        const float* Os = pGeo->get_Os();

        const float* widthv = pGeo->get_widthv();
        const float* Csv = pGeo->get_Csv();
        const float* Osv = pGeo->get_Osv();

        const float* Nv = pGeo->get_Nv();

        float constantwidth = pGeo->get_constantwidth();
        const float* constantnormal = pGeo->get_constantnormal();
        
        bool has_normal = (N || Nv);
        
        std::vector< std::shared_ptr<curve_strand> > strands(ncurves);
        int nv_offset = 0;
        int ns_offset = 0;
        for(int i = 0;i<ncurves;i++)
        {
            strands[i] = std::shared_ptr<curve_strand>(new curve_strand());
            int nverts    = nvertices[i];
            int nsegments = (nverts-1) / (order-1);//(7-1)/(4-1)->2
            strands[i]->vertices.resize(nverts);
            strands[i]->has_normal = has_normal;
            for(int j = 0;j<nverts;j++)
            {
                strands[i]->vertices[j].normal = gale::vector3(0,0,-1);
                strands[i]->vertices[j].width = constantwidth;
                strands[i]->vertices[j].color = gale::vector3(1,1,1);
            }
            for(int j = 0;j<nverts;j++)
            {
                int index = nv_offset + j;
                strands[i]->vertices[j].position = gale::vector3(P + 3 * index);
                if(Nv)
                {
                    strands[i]->vertices[j].normal = gale::vector3(Nv+ 3 * index);
                }
                if(widthv)
                {
                    strands[i]->vertices[j].width = widthv[index];
                }
                if(Csv)
                {
                    strands[i]->vertices[j].color = gale::vector3(Csv + 3 * index);
                }
            }
            float delta = 1.0f / (order - 1);
            for(int j = 0;j<nsegments;j++)
            {
                int i0 = ns_offset + j;
                int i1 = i0 + 1;
                if(N)
                {
                    gale::vector3 n0 = gale::vector3(N + 3 * i0);
                    gale::vector3 n1 = gale::vector3(N + 3 * i1);
                    for(int k = 0;k<order;k++)
                    {
                        float t = delta*k;
                        strands[i]->vertices[(order - 1) * j + k].normal = normalize((1-t)*n0 + t*n1);   
                    }
                }
                if(width)
                {
                    for(int k = 0;k<order;k++)
                    {
                        float t = delta*k;
                        strands[i]->vertices[(order - 1) * j + k].width = (1-t)*width[i0] + t*width[i1];   
                    }
                }
                if(Cs)
                {
                    gale::vector3 c0 = gale::vector3(Cs + 3 * i0);
                    gale::vector3 c1 = gale::vector3(Cs + 3 * i1);
                    for(int k = 0;k<order;k++)
                    {
                        float t = delta*k;
                        strands[i]->vertices[(order - 1) * j + k].color = (1-t)*c0 + t*c1;   
                    }
                }
            }
            nv_offset += nverts;
            ns_offset += nsegments;
        }
        
        for(int i = 0;i<ncurves;i++)
        {
            std::shared_ptr<curve_strand> input  = strands[i];
            std::shared_ptr<curve_strand> output = std::shared_ptr<curve_strand>(new curve_strand());
            int nverts = input->vertices.size();
            int nsegments = (nverts-1) / (order-1);//(7-1)/(4-1)->2
            output->vertices.resize(nsegments*DIVV+1);
            for(int j = 0;j<nsegments;j++)
            {
                std::vector<gale::vector3> PP(order);
                std::vector<gale::vector3> NN(order);
                std::vector<float> WW(order);
                std::vector<gale::vector3> CC(order);
                for(int k = 0;k<order;k++)
                {
                    PP[k] = input->vertices[j*(order-1)+k].position;
                    NN[k] = input->vertices[j*(order-1)+k].normal;
                    WW[k] = input->vertices[j*(order-1)+k].width;
                    CC[k] = input->vertices[j*(order-1)+k].color; 
                }
                for(int k = 0;k<DIVV+1;k++)
                {
                    float t = float(k)/DIVV;
                    gale::vector3 p = gale::bezier_evaluate(&PP[0], order, t);
                    gale::vector3 n = normalize(gale::bezier_evaluate(&NN[0], order, t));
                    float w = gale::bezier_evaluate(&WW[0], order, t);
                    gale::vector3 c = gale::bezier_evaluate(&CC[0], order, t);
                    
                    curve_vertex pt;
                    pt.position = p;
                    pt.normal = n;
                    pt.width = w;
                    pt.color = c;
                    output->vertices[j*DIVV+k] = pt; 
                }
            }
            output->has_normal = input->has_normal;
            strands[i] = output;
        }
        /*
        for(int i = 0;i<ncurves;i++)
        {
            size_t sz = strands[i]->vertices.size();
            printf("%d\n",(int)sz);
        }
        */
        
        return std::shared_ptr<gale::geometry>(new gale::curves_geometry(strands, mat));
    }

    static
    void ConvertGeometry(std::vector< std::shared_ptr<gale::geometry> >& geos, const ri_geometry* pGeo)
    {
        ri_geometry_converter geo_converter;
        pGeo = geo_converter.convert(pGeo);
        int typeN = pGeo->typeN();
        switch (typeN)
        {
        case RI_GEOMETRY_SPHERE:
            geos.push_back(ConvertGeometry(dynamic_cast<const ri_sphere_geometry*>(pGeo)));
            break;
        case RI_GEOMETRY_CONE:
            geos.push_back(ConvertGeometry(dynamic_cast<const ri_cone_geometry*>(pGeo)));
            break;
        case RI_GEOMETRY_CYLINDER:
            geos.push_back(ConvertGeometry(dynamic_cast<const ri_cylinder_geometry*>(pGeo)));
            break;
        //case RI_GEOMETRY_HYPERBOLOID:
        //    geos.push_back(ConvertGeometry(dynamic_cast<const ri_hyperboloid_geometry*>(pGeo)));
        //    break;
        //case RI_GEOMETRY_PARABOLOID:
        //    geos.push_back(ConvertGeometry(dynamic_cast<const ri_paraboloid_geometry*>(pGeo)));
        //    break;
        //case RI_GEOMETRY_DISK:
        //    geos.push_back(ConvertGeometry(dynamic_cast<const ri_disk_geometry*>(pGeo)));
        //    break;
        //case RI_GEOMETRY_TORUS:
        //    geos.push_back(ConvertGeometry(dynamic_cast<const ri_torus_geometry*>(pGeo)));
        //    break;
        case RI_GEOMETRY_POINTS_TRIANGLE_POLYGONS:
            ConvertGeometry(geos, dynamic_cast<const ri_points_triangle_polygons_geometry*>(pGeo));
            break;
        case RI_GEOMETRY_BEZIER_PATCH_MESH:
            geos.push_back(ConvertGeometry(dynamic_cast<const ri_bezier_patch_mesh_geometry*>(pGeo)));
            break;
        case RI_GEOMETRY_POINTS:
            geos.push_back(ConvertGeometry(dynamic_cast<const ri_points_geometry*>(pGeo)));
            break;
        case RI_GEOMETRY_RROCEDURAL_EVALUATION:
            ConvertGeometry(geos, dynamic_cast<const ri_procedural_evaluation_geometry*>(pGeo));
            break;
        case RI_GEOMETRY_BEZIER_CURVES:
            geos.push_back(ConvertGeometry(dynamic_cast<const ri_bezier_curves_geometry*>(pGeo)));
            break;
        }
    }

    void ri_gale_geometry_converter::convert(std::vector< std::shared_ptr<gale::geometry> >& geos, const ri_geometry* pGeo)const
    {
        ConvertGeometry(geos, pGeo);
    }
}
