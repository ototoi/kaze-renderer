#include "ri_gale_camera_converter.h"
#include "gale/perspective_camera.h"
#include "gale/values.h"
#include "ri.h"

namespace ri
{
    static gale::matrix4 ConvertMatrix(const ri_transform& trns)
    {
        ri_transform s(trns);
        s.transpose();
        const float* f = s.get();
        gale::matrix4 tmp(f);
        return tmp;
    }

    static
    std::shared_ptr<gale::camera> ConvertCamera(const ri_camera_options* pCmr)
    {
        int nType = pCmr->get_camera_type();
        if (nType == ri_camera_options::RI_CAMERA_PERSPECTIVE)
        {
            float fov = pCmr->get_fov();
            float angle = fov;
            float aspect = pCmr->get_frame_aspect_ratio();
            float left = -aspect;
            float right = +aspect;
            float bottom = -1;
            float top = +1;
            pCmr->get_screen_window(left, right, bottom, top);

            float Zn = std::max(1.0f,     (float)RI_EPSILON);
            float Zf = std::min(50000.0f, (float)RI_INFINITY);
            pCmr->get_clipping(Zn, Zf);
            Zn = std::max(1.0f,    Zn);
            Zf = std::min(50000.0f, Zf);

            angle = gale::values::radians(angle);
            gale::matrix4 mat = ConvertMatrix(pCmr->get_world_to_camera());
            return std::shared_ptr<gale::camera>(new gale::perspective_camera(mat, angle, aspect, left, right, bottom, top, Zn, Zf));
        }
        return std::shared_ptr<gale::camera>();
    }

    std::shared_ptr<gale::camera> ri_gale_camera_converter::convert(const ri_camera_options* pCmr)const
    {
        return ConvertCamera(pCmr);
    }
}
