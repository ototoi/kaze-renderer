#ifndef RI_GALE_LIGHT_CONVERTER_H
#define RI_GALE_LIGHT_CONVERTER_H

#include "ri_light_source.h"
#include "gale/light.h"

namespace ri
{
    class ri_gale_light_converter
    {
    public:
        std::shared_ptr<gale::light> convert(const ri_light_source* pLight)const;
    };
}

#endif
