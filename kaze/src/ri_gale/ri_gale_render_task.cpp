#include "ri_gale_render_task.h"
#include "ri_gale_geometry_converter.h"
#include "ri_gale_light_converter.h"
#include "ri_gale_camera_converter.h"
#include "ri_frame_task.h"
#include "ri_camera_options.h"

#include "logger.h"
#include "ri_sequence_task.h"
#include "gale/gale.h"

#include <iostream>


namespace ri
{
    using namespace gale;
    
    ri_gale_render_task::ri_gale_render_task(const ri_frame_task* task)
        : task_(task)
    {
    }

    ri_gale_render_task::~ri_gale_render_task()
    {
    }

    static int render(const ri_frame_task* task)
    {
        const ri_options& opts = task->get_options();
        const ri_camera_options* pCmr = NULL;
        size_t csz = opts.get_camera_size();
        if (csz)
        {
            pCmr = opts.get_camera_at(csz - 1);
        }
        if (!pCmr) return -1;

        int xres, yres;
        float aspect;
        pCmr->get_format(xres, yres, aspect);

        size_t sz = task->get_geometry_size();
        if (sz)
        {
            gale::gale_context* ctx = gale::initialize_gale();
            
            std::vector<const ri_light_source*> light_sources;
            ri_gale_geometry_converter geo_converter;
            std::vector< std::shared_ptr<gale::geometry> > geos;
            for(size_t i = 0;i<sz;i++)
            {
                const ri_geometry* pGeo = task->get_geomoetry_at(i);
                geo_converter.convert(geos, pGeo);
                const ri_attributes& attr = pGeo->get_attributes();
                std::vector<const ri_light_source*> lv;
                attr.get_lightsource(lv);
                if(!lv.empty())
                {
                    light_sources.insert(light_sources.end(), lv.begin(), lv.end());
                }
            }

            if(!light_sources.empty())
            {
                std::sort(light_sources.begin(), light_sources.end());
                light_sources.erase(std::unique(light_sources.begin(), light_sources.end()), light_sources.end());
            }
            size_t lsz = light_sources.size();
            ri_gale_light_converter light_converter;
            std::vector< std::shared_ptr<gale::light> > lights;
            for(size_t i = 0;i<lsz;i++)
            {
                const ri_light_source* pLight = light_sources[i];
                std::shared_ptr<gale::light> lgt(light_converter.convert(pLight));
                if(lgt.get())
                {
                    lights.push_back(lgt);
                }
            }

            ri_gale_camera_converter cam_converter;
            std::shared_ptr<gale::camera> cam(cam_converter.convert(pCmr));

            if(cam.get() && geos.size())
            {
                gale::render_gale(ctx, xres, yres, geos, lights, cam);
            }
            gale::destroy_gale(ctx);
            
            return 0;
        }
        return -1;
    }

    int ri_gale_render_task::run()
    {
        return render(task_);
    }
}
