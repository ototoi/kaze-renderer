#include <memory>
#include "types.h"
#include "ri_context.h"
#include "ri_composite_context.h"
#include "ri_stdout_context.h"
#include "ri_frame_context.h"
#include "ri_task.h"
#include "ri_read_archive.h"
#include "ri_kaze_task_converter.h"
#include "ri_gale_task_converter.h"

#include "logger.h"
#include "stdout_logger.h"
#include "text_logger.h"
#include "ri_path_resolver.h"

#define KZRIRND_VERSION_MAJOR 1
#define KZRIRND_VERSION_MINOR 2

struct command_option
{
    const char* option;
    int nargs;
};

struct command_output
{
    std::string option;
    std::vector<std::string> values;
};

class command
{
public:
    command(int argc, char** argv, const command_option* inputs)
    {
        {
            int i = 0;
            while (inputs[i].option)
            {
                inputs_.push_back(inputs[i]);
                i++;
            }
        }
        {
            for (int i = 1; i < argc; i++)
            {
                std::string arg = argv[i];
                int kFind = -1;
                for (int j = 0; j < inputs_.size(); j++)
                {
                    if (
                        arg == "-" + std::string(inputs_[j].option) ||
                        arg == "/" + std::string(inputs_[j].option))
                    {
                        kFind = j;
                        break;
                    }
                }
                if (kFind < 0)
                {
                    others_.push_back(arg);
                }
                else
                {
                    command_output out;
                    out.option = inputs_[kFind].option;
                    int ii = i;
                    if (inputs_[kFind].nargs > 0)
                    {
                        int maxarg = std::min(argc, ii + 1 + inputs_[kFind].nargs);
                        for (int k = ii + 1; k < maxarg; k++)
                        {
                            out.values.push_back(argv[k]);
                            i++;
                        }
                    }
                    outputs_.push_back(out);
                }
            }
        }
    }

    const std::vector<command_option>& inputs() const { return inputs_; }
    const std::vector<command_output>& outouts() const { return outputs_; }
    const std::vector<std::string>& others() const { return others_; }
private:
    std::vector<command_option> inputs_;
    std::vector<command_output> outputs_;
    std::vector<std::string> others_;
};

#include <iostream>

int main(int argc, char** argv)
{
    using namespace kaze;
    using namespace ri;

    static command_option inputs[] = {
        {"f", 1}, //
        {"q", 0}, //
        {"d", 0},
        {"t", 1},
        {"version", 0},
        {"v", 0},
        {"log", 0},
        {"help", 0},
        {"h", 0},

        {"newer", 0},
        {"verbose", 0},

        {"debug", 0},

        {"force", 0},

        {"gpu", 0},

        {NULL, 0}};
        
    int nRet = 0;

    command cmd(argc, argv, inputs);

    bool bQuiet = false;
    bool bForceDisplay = false;
    bool bDebug = false;
    bool bForce = false;
    bool bVersion = false;
    bool bHelp = false;
    bool bVerbose = false;
    bool bLog = false;
    bool bGPU = false;

    const std::vector<command_output> outputs = cmd.outouts();

    for (size_t i = 0; i < outputs.size(); i++)
    {
        if (outputs[i].option == "q")
        {
            bQuiet = true;
        }
        else if (outputs[i].option == "d")
        {
            bForceDisplay = true;
        }
        else if (outputs[i].option == "debug")
        {
            bDebug = true;
        }
        else if (outputs[i].option == "force")
        {
            bForce = false;
        }
        else if (outputs[i].option == "v" || outputs[i].option == "version")
        {
            bVersion = true;
        }
        else if (outputs[i].option == "h" || outputs[i].option == "help")
        {
            bHelp = true;
        }
        else if (outputs[i].option == "verbose")
        {
            bVerbose = true;
        }
        else if (outputs[i].option == "log")
        {
            bLog = true;
        }
        else if (outputs[i].option == "gpu")
        {
            bGPU = true;
        }
    }

    if (bVersion)
    {
        printf("%s version %d.%d\n", argv[0], KZRIRND_VERSION_MAJOR, KZRIRND_VERSION_MINOR);
    }

    if (bHelp)
    {
        printf("HELP! > <\n");
    }

    const std::vector<std::string> others = cmd.others();
    if (others.size() < 1)
    {
        printf("%s \"%s\"\n", argv[0], "*.rib");
        return -1;
    }

    if (bDebug)
    {
        bForceDisplay = true;
        bVerbose = true;
    }

    if (!bQuiet)
    {
        set_logger("text", new stdout_logger());
        set_logger_progress("text", new stdout_logger());
        if (bDebug)
        {
            set_logger_debug("text", new stdout_logger());
        }
    }

    std::string pluginpath = ri_path_resolver::get_plugin_path();

    for (int k = 0; k < others.size(); k++)
    {
        std::string strFilePath = others[k];
        if (bLog)
        {
            set_logger("log", new text_logger((strFilePath + ".log").c_str()));
        }

        std::unique_ptr<ri_composite_context> ctx(new ri_composite_context());
        ctx->add(new ri_frame_context());
        if (bVerbose)
        {
            ctx->add(new ri_stdout_context());
        }

        ri_frame_context* frm = (ri_frame_context*)ctx->get_at(0);
        if (bForceDisplay)
        {
            frm->RiDisplayV((char*)strFilePath.c_str(), "framebuffer-", "rgb", 0, NULL, NULL);
        }

        std::string oldPath = ri_read_archive_set_path(strFilePath.c_str());
        std::string filename = ri_read_archive_get_filename(strFilePath.c_str());

        int nRet2 = ri_read_archive(filename.c_str(), ctx.get());

        if (nRet2 == 0)
        {
            ri::ri_kaze_task_converter kaze_cvtr;
            ri::ri_gale_task_converter gale_cvter;
            size_t sz = frm->get_task_size();
            for (size_t i = 0; i < sz; i++)
            {
                if(bGPU)
                {
                    const ri_frame_task* ft = dynamic_cast<const ri_frame_task*>(frm->get_task_at(i));
                    if(ft)
                    {
                        ri_options& opts = (ri_options&)ft->get_options();
                        opts.set("Hider", "type", "gpu");
                    }
                }

                std::shared_ptr<ri::ri_task> task1 = kaze_cvtr.convert(frm->get_task_at(i));
                
                if(task1.get())
                {
                    task1->run();
                }
                else
                {
                    std::shared_ptr<ri::ri_task> task2 = gale_cvter.convert(frm->get_task_at(i));
                    if(task2.get())
                    {
                        task2->run();
                    }
                }
            }
        }
        else
        {
            nRet = -1;
        }

        ri_read_archive_set_path(oldPath.c_str());

        if (bLog)
        {
            set_logger("log", NULL);
        }
    }

    return nRet;
}
