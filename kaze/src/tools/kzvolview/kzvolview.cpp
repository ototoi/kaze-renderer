#include "volume_interval.hpp"
#include "volume_ray.hpp"
#include "volume_image.hpp"
#include "volume_oobb_interval_detector.hpp"

#include "types.h"

using namespace std;
using namespace kaze;
int main()
{
    volume_intervals<float> a(1,2);
    cout << a << endl;
    volume_intervals<float> b(0.5, 4);
    cout << b << endl;
    a += b;
    cout << a << endl;
    a.add(1,4);
    cout << a << endl;
    a.add(0,10);
    cout << a << endl;
    a.flatten();
    cout << a << endl;
    volume_intervals<float> vals = a;
    cout << vals << endl;

    volume_vector<float, 3> width(1,2,3);
    volume_matrix<float, 4, 4> mat = tempest::matrix_generator< volume_matrix<float, 4, 4> >::identity();
    volume_oobb<float> oobb(width, mat);
    typedef volume_oobb_interval_detector_GL<float> ST;
    
    volume_oobb_interval_detector<float> det(oobb);

    volume_image< volume_intervals<float> > intervals(10,10);
    volume_image< volume_ray<float> > rays(10,10);
    det.detect(intervals, rays);
    
    /*
    for(int i=0;i<intervals.size();i++)
    {
        intervals[i].flatten();
        cout << intervals[i] << endl;
    }
    */
    return 0;
}