#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>

#include "ri_make_texture.h"
#include "ri_make_shadow.h"
#include "ri_make_lat_long_environment.h"
#include "ri_make_cube_face_environment.h"
#include "ri_convert_texture.h"
#include "ri_make_texture_helper.h"
#include "ri_tex_gl_context.h"

#include <vector>
#include <string>
#include <algorithm>

#define KZRITEX_VERSION_MAJOR 1
#define KZRITEX_VERSION_MINOR 2

struct command_option
{
    const char* option;
    int nargs;
};

struct command_output
{
    std::string option;
    std::vector<std::string> values;
};

static std::string cleanstr(const char* str)
{
#if _WIN32
    if (*str == 63)
        return ++str;
    else
        return str;
#else
    return str;
#endif
}

class command
{
public:
    command(int argc, char** argv, const command_option* inputs)
    {
        {
            int i = 0;
            while (inputs[i].option)
            {
                inputs_.push_back(inputs[i]);
                i++;
            }
        }
        {
            for (int i = 1; i < argc; i++)
            {
                std::string arg = cleanstr(argv[i]);
                int kFind = -1;
                for (int j = 0; j < inputs_.size(); j++)
                {
                    if (
                        arg == "-" + std::string(inputs_[j].option) ||
                        arg == "/" + std::string(inputs_[j].option))
                    {
                        kFind = j;
                        break;
                    }
                }
                if (kFind < 0)
                {
                    others_.push_back(arg);
                }
                else
                {
                    command_output out;
                    out.option = inputs_[kFind].option;
                    int ii = i;
                    if (inputs_[kFind].nargs > 0)
                    {
                        int maxarg = std::min(argc, ii + 1 + inputs_[kFind].nargs);
                        for (int k = ii + 1; k < maxarg; k++)
                        {
                            out.values.push_back(argv[k]);
                            i++;
                        }
                    }
                    outputs_.push_back(out);
                }
            }
        }
    }

    const std::vector<command_option>& inputs() const { return inputs_; }
    const std::vector<command_output>& outouts() const { return outputs_; }
    const std::vector<std::string>& others() const { return others_; }
private:
    std::vector<command_option> inputs_;
    std::vector<command_output> outputs_;
    std::vector<std::string> others_;
};

static bool Str2Bool(const std::string& s)
{
    if (s == "true") return true;
    if (s == "True") return true;
    if (s == "false") return false;
    if (s == "False") return false;
    int nRet = atoi(s.c_str());
    if (nRet) return true;
    return false;
}

int main(int argc, char** argv)
{
    using namespace kaze::ri;

    static command_option inputs[] = {
        {"shadow", 0},
        {"envlatl", 0},
        {"envcube", 0},
        {"minmaxshadow", 0},
        {"pyramidshadow", 0},
        {"penvcube", 0},
        {"latl2cube", 0},

        {"shbands", 1},
        {"fov", 1},

        {"mode", 1},
        {"smode", 1},
        {"tmode", 1},

        {"wrap", 1},
        {"swrap", 1},
        {"twrap", 1},

        {"ch", 1},

        {"short", 0},
        {"float", 0},
        {"word", 0},
        {"byte", 0},

        {"pattern", 1},

        {"format", 1},
        {"memory", 1},
        {"compression", 1},

        {"resize", 1},
        {"sresize", 1},
        {"tresize", 1},

        {"extraargs", 1},

        {"filter", 1},
        {"mipfilter", 1},
        {"depthfilter", 1},

        {"blur", 1},
        {"sblur", 1},
        {"tblur", 1},

        {"filterwidth", 1},
        {"sfilterwidth", 1},
        {"tfilterwidth", 1},

        {"version", 0},
        {"v", 0},

        {"help", 0},
        {"h", 0},

        {"newer", 0},
        {"verbose", 0},
        {"diffconv", 3},

        {"debug", 0},

        {"force", 0},

        {"nomipmap", 0},

        {"convert", 0},

        {"gl", 1},

        {"model", 1},

        {NULL, 0}};

    int nRet = 0;

    command cmd(argc, argv, inputs);

    std::string swrap = "repeat";
    std::string twrap = "repeat";

    std::string filter = "box";
    float swidth = 1;
    float twidth = 1;

    std::string texmode = "texture";

    std::string sresize = "none";
    std::string tresize = "none";

    float fov = 30.0f;

    std::string unit = "short";

    std::string debug = "0";
    std::string force = "0";

    std::string mipmap = "1";

    bool bVersion = false;
    bool bHelp = false;
    bool bOpenGL = false;

    std::string modelName = "noise2_model";

    const std::vector<command_output> outputs = cmd.outouts();

    for (size_t i = 0; i < outputs.size(); i++)
    {
        if (outputs[i].option == "shadow")
        {
            texmode = outputs[i].option;
        }
        else if (outputs[i].option == "envlatl")
        {
            texmode = outputs[i].option;
            sresize = "up";
            tresize = "up";
        }
        else if (outputs[i].option == "envcube")
        {
            texmode = outputs[i].option;
        }
        else if (outputs[i].option == "convert")
        {
            texmode = outputs[i].option;
        }
        else if (outputs[i].option == "latl2cube")
        {
            texmode = outputs[i].option;
        }
    }

    for (size_t i = 0; i < outputs.size(); i++)
    {
        if (outputs[i].option == "shadow")
        {
            texmode = outputs[i].option;
        }
        else if (outputs[i].option == "envlatl")
        {
            texmode = outputs[i].option;
        }
        else if (outputs[i].option == "envcube")
        {
            texmode = outputs[i].option;
        }
        else if (outputs[i].option == "convert")
        {
            texmode = outputs[i].option;
        }
        else if (outputs[i].option == "minmaxshadow")
        {
            texmode = outputs[i].option;
        }
        else if (outputs[i].option == "pyramidshadow")
        {
            texmode = outputs[i].option;
        }
        else if (outputs[i].option == "latl2cube")
        {
            texmode = outputs[i].option;
        }
        else if (outputs[i].option == "fov")
        {
            fov = (float)atof(outputs[i].values[0].c_str());
        }
        else if (outputs[i].option == "short")
        {
            unit = outputs[i].option;
        }
        else if (outputs[i].option == "float")
        {
            unit = outputs[i].option;
        }
        else if (outputs[i].option == "word")
        {
            unit = outputs[i].option;
        }
        else if (outputs[i].option == "byte")
        {
            unit = outputs[i].option;
        }
        else if (outputs[i].option == "mode" || outputs[i].option == "wrap")
        {
            swrap = twrap = outputs[i].values[0];
        }
        else if (outputs[i].option == "smode" || outputs[i].option == "swrap")
        {
            swrap = outputs[i].values[0];
        }
        else if (outputs[i].option == "tmode" || outputs[i].option == "twrap")
        {
            twrap = outputs[i].values[0];
        }
        else if (outputs[i].option == "filter")
        {
            filter = outputs[i].values[0];
        }
        else if (outputs[i].option == "filterwidth")
        {
            swidth = twidth = (float)atof(outputs[i].values[0].c_str());
        }
        else if (outputs[i].option == "sfilterwidth")
        {
            swidth = (float)atof(outputs[i].values[0].c_str());
        }
        else if (outputs[i].option == "tfilterwidth")
        {
            twidth = (float)atof(outputs[i].values[0].c_str());
        }
        else if (outputs[i].option == "resize")
        {
            sresize = tresize = outputs[i].values[0];
        }
        else if (outputs[i].option == "sresize")
        {
            sresize = outputs[i].values[0];
        }
        else if (outputs[i].option == "tresize")
        {
            tresize = outputs[i].values[0];
        }
        else if (outputs[i].option == "debug")
        {
            debug = "1";
        }
        else if (outputs[i].option == "force")
        {
            force = "1";
        }
        else if (outputs[i].option == "v" || outputs[i].option == "version")
        {
            bVersion = true;
        }
        else if (outputs[i].option == "h" || outputs[i].option == "help")
        {
            bHelp = true;
        }
        else if (outputs[i].option == "nomipmap")
        {
            mipmap = "0";
        }
        else if (outputs[i].option == "gl")
        {
            bOpenGL = true;
            if(outputs[i].values.size())
            {
                bOpenGL = Str2Bool(outputs[i].values[0]);
            }
        }
        else if (outputs[i].option == "model")
        {
            modelName = outputs[i].values[0];
        }
    }

    bool bIsCreate = ri_tex_init_gl_context();
    ri_tex_set_enable_gl_context(bOpenGL);

    if (bVersion)
    {
        printf("%s version %d.%d\n", argv[0], KZRITEX_VERSION_MAJOR, KZRITEX_VERSION_MINOR);
    }

    if (bHelp)
    {
        printf("HELP! > <\n");
    }

    if (texmode == "texture")
    {
        const std::vector<std::string>& others = cmd.others();
        if (others.size() == 1)
        {
            texmode = "convert";
        }
    }

    if (texmode == "convert")
    {
        const std::vector<std::string>& others = cmd.others();
        if (others.size() < 1)
        {
            printf("%s -convert \"%s\" \"%s\"\n", argv[0], "pic", "tex");
            return -1;
        }
        std::string pic = others[0];
        return ri_convert_texture(pic.c_str(), 0, NULL, NULL);
    }
    else if (texmode == "texture")
    {
        const std::vector<std::string>& others = cmd.others();
        if (others.size() < 2)
        {
            printf("%s \"%s\" \"%s\"\n", argv[0], "pic", "tex");
            return -1;
        }
        std::string pic = others[0];
        std::string tex = others[1];

        if (others.size() > 2)
        {
            swrap = others[2];
        }
        if (others.size() > 3)
        {
            twrap = others[3];
        }
        if (others.size() > 4)
        {
            filter = others[4];
        }
        if (others.size() > 5)
        {
            swidth = (float)atof(others[5].c_str());
        }
        if (others.size() > 6)
        {
            twidth = (float)atof(others[6].c_str());
        }
        const char* tokens[] = {"sresize", "tresize", "debug", "force", "mipmap", "model"};
        const char* params[] = {sresize.c_str(), tresize.c_str(), debug.c_str(), force.c_str(), mipmap.c_str(), modelName.c_str()};
        nRet = ri_make_texture(pic.c_str(), tex.c_str(), swrap.c_str(), twrap.c_str(), filter.c_str(), swidth, twidth, 6, tokens, params);
    }
    else if (texmode == "shadow")
    {
        const std::vector<std::string>& others = cmd.others();
        if (others.size() < 2)
        {
            printf("%s -shadow \"%s\" \"%s\"\n", argv[0], "pic", "tex");
            return -1;
        }
        std::string pic = others[0];
        std::string tex = others[1];
        nRet = ri_make_shadow(pic.c_str(), tex.c_str());
    }
    else if (texmode == "envlatl")
    {
        const std::vector<std::string>& others = cmd.others();
        if (others.size() < 2)
        {
            printf("%s -envlatl \"%s\" \"%s\"\n", argv[0], "pic", "tex");
            return -1;
        }
        std::string pic = others[0];
        std::string tex = others[1];
        const char* tokens[] = {"sresize", "tresize", "debug", "force"};
        const char* params[] = {sresize.c_str(), tresize.c_str(), debug.c_str(), force.c_str()};
        nRet = ri_make_lat_long_environment(pic.c_str(), tex.c_str(), filter.c_str(), swidth, twidth, 4, tokens, params);
    }
    else if (texmode == "envcube")
    {
        const std::vector<std::string> others = cmd.others();
        if (others.size() < 7)
        {
            printf("%s -envcube \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\"\n", argv[0], "px", "nx", "py", "ny", "pz", "nz", "tex");
            return -1;
        }
        std::string px = others[0];
        std::string nx = others[1];
        std::string py = others[2];
        std::string ny = others[3];
        std::string pz = others[4];
        std::string nz = others[5];

        std::string tex = others[6];

        const char* tokens[] = {"sresize", "tresize", "debug", "force"};
        const char* params[] = {sresize.c_str(), tresize.c_str(), debug.c_str(), force.c_str()};
        nRet = ri_make_cube_face_environment(px.c_str(), nx.c_str(), py.c_str(), ny.c_str(), pz.c_str(), nz.c_str(), tex.c_str(), fov, filter.c_str(), swidth, twidth, 4, tokens, params);
    }
    else if (texmode == "latl2cube")
    {
        const std::vector<std::string> others = cmd.others();

        std::string pic;
        std::string px;
        std::string nx;
        std::string py;
        std::string ny;
        std::string pz;
        std::string nz;
        if (others.size() == 1)
        {
            pic = others[0];
            create_cubemap_names(pic, px, nx, py, ny, pz, nz);
        }
        else if (others.size() == 2)
        {
            pic = others[0];
            std::string strOut = others[1];
            create_cubemap_names(strOut, px, nx, py, ny, pz, nz);
        }
        else if (others.size() < 7)
        {
            printf("%s -latl2cube \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\" \"%s\"\n", argv[0], "pic", "px", "nx", "py", "ny", "pz", "nz");
            return -1;
        }
        else
        {
            pic = others[0];
            px = others[1];
            nx = others[2];
            py = others[3];
            ny = others[4];
            pz = others[5];
            nz = others[6];
        }
        const char* tokens[] = {"sresize", "tresize", "debug", "force"};
        const char* params[] = {sresize.c_str(), tresize.c_str(), debug.c_str(), force.c_str()};
        nRet = ri_latl2cube(pic.c_str(), px.c_str(), nx.c_str(), py.c_str(), ny.c_str(), pz.c_str(), nz.c_str(), 4, tokens, params);
    }

    return nRet;
}
