#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>

#include <iostream>
#include <fstream>
#include <sstream>

#include <vector>
#include <string>
#include <map>
#include <algorithm>

#include "context.h"

#define KZRISLPP_VERSION_MAJOR 2
#define KZRISLPP_VERSION_MINOR 0

struct command_option
{
    const char* option;
    int nargs;
    const char* description;
};

struct command_output
{
    std::string option;
    std::vector<std::string> values;
};

static std::string cleanstr(const char* str)
{
#if _WIN32
    if (*str == 63)
        return ++str;
    else
        return str;
#else
    return str;
#endif
}

class command
{
public:
    command(int argc, char** argv, const command_option* inputs)
    {
        {
            int i = 0;
            while (inputs[i].option)
            {
                inputs_.push_back(inputs[i]);
                i++;
            }
        }
        {
            for (int i = 1; i < argc; i++)
            {
                std::string arg = cleanstr(argv[i]);
                bool bContinues = false;
                int kFind = -1;
                for (int j = 0; j < inputs_.size(); j++)
                {
                    std::string opt1 = "-" + std::string(inputs_[j].option);
                    std::string opt2 = "/" + std::string(inputs_[j].option);
                    if (arg ==  opt1 || arg == opt2)
                    {
                        kFind = j;
                        break;
                    }
                    else
                    {
                        size_t sLen = opt1.size();
                        if(
                            (arg.size() > sLen) && 
                            ((memcmp(arg.c_str(), opt1.c_str(), sLen) == 0))
                        )
                        {
                            kFind = j;
                            bContinues = true;
                            break;
                        }
                    }
                }
                if (kFind < 0)
                {
                    others_.push_back(arg);
                }
                else
                {
                    command_output out;
                    out.option = inputs_[kFind].option;
                    if(!bContinues)
                    {
                        int ii = i;
                        if (inputs_[kFind].nargs > 0)
                        {
                            int maxarg = std::min(argc, ii + 1 + inputs_[kFind].nargs);
                            for (int k = ii + 1; k < maxarg; k++)
                            {
                                out.values.push_back(argv[k]);
                                i++;
                            }
                        }
                    }
                    else
                    {
                        std::size_t sLen = 1 + strlen(inputs_[kFind].option);
                        std::string value = arg.substr(sLen);
                        out.values.push_back(value);
                    }
                    outputs_.push_back(out);
                }
            }
        }
    }

    const std::vector<command_option>& inputs() const { return inputs_; }
    const std::vector<command_output>& outouts() const { return outputs_; }
    const std::vector<std::string>& others() const { return others_; }
private:
    std::vector<command_option> inputs_;
    std::vector<command_output> outputs_;
    std::vector<std::string> others_;
};

static std::pair<std::string,std::string> split_key_value(const std::string& keyvalue)
{
    std::string key = "";
    std::string value = "1";
    std::istringstream iss(keyvalue);
    std::string tmp;
    std::vector<std::string> res;
    while(std::getline(iss, tmp, '='))
    {
        res.push_back(tmp);
    }
    if(res.size() >= 1)key = res[0];
    if(res.size() >= 2)value = res[1];

    return std::make_pair(key, value);
}

static
int preprocess
(
    std::shared_ptr<slpp::input_stream>& is, 
    std::shared_ptr<slpp::output_stream>& os,
    const slpp::context_options& options
)
{
    using namespace slpp;
    using namespace std;

    context ctx(options, is, os);
    return ctx.run();
}

int main(int argc, char** argv)
{
    //using namespace kaze::ri;

    static command_option inputs[] =
    {
        {"D", 1, NULL},                 //

        {"U", 1, NULL},                 //
        {"undef", 0, NULL},             //

        {"CC", 0, NULL},                //
        {"C", 0, NULL},                 //Do not discard comments
        

        {"l", 1, NULL},                 //Add the directory
        {"o", 1, NULL},                 //Write output to file

        {"I", 1, NULL},                 //

        {"MF", 1, NULL},
        {"M", 0, NULL},
        {"MM", 0, NULL},

        {"x", 1, NULL},                 //

        {"include", 1, NULL},           //

        {"P", 0, NULL},                 //
        

        {"version", 0, NULL},
        {"v", 0, NULL},

        {"help", 0, NULL},
        {"h", 0, NULL},

        {NULL, 0, NULL}
    };

    int nRet = 0;

    command cmd(argc, argv, inputs);

    bool bVersion = false;
    bool bHelp = false;
    bool bDiscardComments = true;

    const std::vector<command_output> outputs = cmd.outouts();

    for (size_t i = 0; i < outputs.size(); i++)
    {
        if (outputs[i].option == "v" || outputs[i].option == "version")
        {
            bVersion = true;
        }
        else if (outputs[i].option == "h" || outputs[i].option == "help")
        {
            bHelp = true;
        }
    }

    if (bHelp)
    {
        printf("HELP! > <\n");
        return 0;
    }

    if (bVersion)
    {
        printf("%s version %d.%d\n", argv[0], KZRISLPP_VERSION_MAJOR, KZRISLPP_VERSION_MINOR);
    }

    std::string out_path = "";
    std::map<std::string, std::pair<std::string,std::string> > define_macro_list;
    std::vector< std::string > undef_macro_list;

    std::vector< std::string > include_dirs;
    for (size_t i = 0; i < outputs.size(); i++)
    {
        if (outputs[i].option == "D")
        {
            std::string macro = outputs[i].values[0];
            std::pair<std::string,std::string> key_value = split_key_value(macro);
            //printf("%s:%s\n", key_value.first.c_str(), key_value.second.c_str());
            if(key_value.first != "")
            {
                define_macro_list[key_value.first] = key_value;
            }
        }
        else if (outputs[i].option == "U")
        {
            std::string macro = outputs[i].values[0];
            define_macro_list.erase(macro);
            undef_macro_list.push_back(macro);
        }
        else if (outputs[i].option == "o")
        {
            std::string filepath = outputs[i].values[0];
            std::ofstream ofs(filepath);
            if(ofs)
            {
                out_path = filepath;
            }
        }
        else if (outputs[i].option == "C" || outputs[i].option == "CC")
        {
            bDiscardComments = false;
        }
        else if (outputs[i].option == "I")
        {
            if(!outputs[i].values[0].empty())
            {
                include_dirs.push_back(outputs[i].values[0]);
            }
        }
    }

    {
        using namespace slpp;
        using namespace std;

        std::shared_ptr<slpp::input_stream> is( new slpp::console_input_stream(std::cin) );
        std::shared_ptr<slpp::output_stream> os( new slpp::console_output_stream(std::cout) );
        
        if(out_path != "")
        {
            os = std::shared_ptr<slpp::output_stream>( new slpp::file_output_stream(out_path.c_str()) );
        }

        const std::vector<std::string>& others = cmd.others();
        if (others.size() >= 1)
        {
            is = std::shared_ptr<slpp::input_stream>( new slpp::file_input_stream(others[0].c_str()) );
        }

        context_options options;
        options.is_output_comment = !bDiscardComments;
        options.include_dirs = include_dirs;

        nRet |= preprocess(is, os, options);
    }

    return nRet;
}
