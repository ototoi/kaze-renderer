#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include <algorithm>
#include <memory>

#include <rslc/rslc_context.h>
#include <rslc/rslc_compiler.h>
#include <rslc/rslc_path_resolver.h>
#include <slpp/context.h>

#define KZRISLC_VERSION_MAJOR 0
#define KZRISLC_VERSION_MINOR 1

struct command_option
{
    const char* option;
    int nargs;
};

struct command_output
{
    std::string option;
    std::vector<std::string> values;
};

class command
{
public:
    command(int argc, char** argv, const command_option* inputs)
    {
        {
            int i = 0;
            while (inputs[i].option)
            {
                inputs_.push_back(inputs[i]);
                i++;
            }
        }
        {
            for (int i = 1; i < argc; i++)
            {
                std::string arg = argv[i];
                int kFind = -1;
                for (int j = 0; j < inputs_.size(); j++)
                {
                    if (
                        arg == "-" + std::string(inputs_[j].option) ||
                        arg == "/" + std::string(inputs_[j].option))
                    {
                        kFind = j;
                        break;
                    }
                }
                if (kFind < 0)
                {
                    others_.push_back(arg);
                }
                else
                {
                    command_output out;
                    out.option = inputs_[kFind].option;
                    int ii = i;
                    if (inputs_[kFind].nargs > 0)
                    {
                        int maxarg = std::min(argc, ii + 1 + inputs_[kFind].nargs);
                        for (int k = ii + 1; k < maxarg; k++)
                        {
                            out.values.push_back(argv[k]);
                            i++;
                        }
                    }
                    outputs_.push_back(out);
                }
            }
        }
    }

    const std::vector<command_option>& inputs() const { return inputs_; }
    const std::vector<command_output>& outouts() const { return outputs_; }
    const std::vector<std::string>& others() const { return others_; }
private:
    std::vector<command_option> inputs_;
    std::vector<command_output> outputs_;
    std::vector<std::string> others_;
};

static
int preprocess
(
    std::shared_ptr<slpp::input_stream>& is, 
    std::shared_ptr<slpp::output_stream>& os,
    const slpp::context_options& options
)
{
    using namespace slpp;
    using namespace std;

    context ctx(options, is, os);
    return ctx.run();
}

int main(int argc, char** argv)
{
    using namespace rslc;

    static command_option inputs[] = {
        {"q", 0},
        {"o", 1},

        {"O", 0},
        {"O0", 0},
        {"O1", 0},
        {"O2", 0},
        {"O3", 0},
        {"Os", 0},
        {"Ox", 0},
        {"version", 0},
        {"v", 0},
        {"log", 0},
        {"help", 0},
        {"h", 0},

        {"verbose", 0},

        {"debug", 0},

        {"force", 0},

        {"E", 0},

        {NULL, 0}};

    int nRet = 0;

    command cmd(argc, argv, inputs);

    bool bQuiet = false;
    bool bDebug = false;
    bool bForce = false;
    bool bVersion = false;
    bool bHelp = false;
    bool bVerbose = false;
    bool bLog = false;
    bool bCPPOnly = false;

    std::string strOutput = "";

    const std::vector<command_output> outputs = cmd.outouts();

    for (size_t i = 0; i < outputs.size(); i++)
    {
        if (outputs[i].option == "q")
        {
            bQuiet = true;
        }
        else if (outputs[i].option == "debug")
        {
            bDebug = true;
        }
        else if (outputs[i].option == "force")
        {
            bForce = false;
        }
        else if (outputs[i].option == "v" || outputs[i].option == "version")
        {
            bVersion = true;
        }
        else if (outputs[i].option == "h" || outputs[i].option == "help")
        {
            bHelp = true;
        }
        else if (outputs[i].option == "verbose")
        {
            bVerbose = true;
        }
        else if (outputs[i].option == "log")
        {
            bLog = true;
        }
        if (outputs[i].option == "o")
        {
            if (outputs[i].values.size() > 0)
            {
                strOutput = outputs[i].values[0];
            }
        }
        if (outputs[i].option == "E")
        {
            bCPPOnly = true;
        }
    }

    if (bVersion)
    {
        printf("%s version %d.%d\n", argv[0], KZRISLC_VERSION_MAJOR, KZRISLC_VERSION_MINOR);
    }

    if (bHelp)
    {
        printf("HELP! > <\n");
    }

    if (bDebug)
    {
        bVerbose = true;
    }

    if (!bQuiet)
    {
        ;//
    }

    const std::vector<std::string> others = cmd.others();
    if (others.size() < 1)
    {
        printf("%s \"%s\"\n", argv[0], "*.sl");
        return -1;
    }

    for (int k = 0; k < others.size(); k++)
    {
        std::string strInputPath = others[k];
        if (bLog)
        {
            //set_logger("log", new text_logger((strInputPath+".log").c_str()));
        }

        std::string strOutputPath = "";
        if (strOutput != "")
        {
            strOutputPath = strOutput;
        }
        else
        {
            strOutputPath = rslc_path_resolver::replace_ext(strInputPath, ".ksl");
        }

        if (bCPPOnly)
        {
            using namespace slpp;
            using namespace std;

            std::shared_ptr<slpp::input_stream> is( new slpp::console_input_stream(std::cin) );
            std::shared_ptr<slpp::output_stream> os( new slpp::console_output_stream(std::cout) );
            
            if(strOutputPath != "")
            {
                os = std::shared_ptr<slpp::output_stream>( new slpp::file_output_stream(strOutputPath.c_str()) );
            }

            const std::vector<std::string>& others = cmd.others();
            if (others.size() >= 1)
            {
                is = std::shared_ptr<slpp::input_stream>( new slpp::file_input_stream(others[0].c_str()) );
            }

            context_options options;
            options.is_output_comment = false;
            //options.include_dirs = include_dirs;

            return preprocess(is, os, options);
        }
        else
        {
            for(int i=0;i<others.size();i++)
            {
                std::string inStr = others[i];
                std::string outStr = others[i] + ".t";
                {
                    using namespace slpp;
                    using namespace std;

                    std::shared_ptr<slpp::input_stream> is( new slpp::file_input_stream(inStr.c_str()) );
                    std::shared_ptr<slpp::output_stream> os( new slpp::file_output_stream(outStr.c_str()) );
                    
                    context_options options;
                    options.is_output_comment = false;
                    //options.include_dirs = include_dirs;

                    preprocess(is, os, options);
                }
                {
                    rslc_compiler com;
                    int nRet = com.compile(outStr.c_str());
                }
            }
            return 0;
        }
    }

    return nRet;
}
