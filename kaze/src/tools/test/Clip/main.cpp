#include <stdio.h>
#include <vector>
#include "tempest/vector.hpp"
#include "tempest/vector2.hpp"
#include "tempest/vector3.hpp"
#include "tempest/vector4.hpp"

typedef tempest::vector<float, 2> vector2;
typedef tempest::vector<float, 3> vector3;

struct polygon
{
    std::vector<vector2> positions;
};

static
int CreateRandomPolygons(std::vector<polygon>& polygons, int n)
{
    polygons.resize(n);
    for(int i=0;i<n;i++)
    {
        std::vector<vector2> positions(4);
        for(int j=0;j<4;j++)
        {
            //positions[j] = 
        }

        polygon& pol = polygons[i];
        pol.positions.swap(positions);
    }
}
//https://www.youtube.com/watch?v=Euuw72Ymu0M
static
int ClipPolygons(std::vector<polygon>& polygons)
{
    //sutherland hodgeman polygon clipping
    return 0;
}

int main()
{
    std::vector<polygon> polygons;
    CreateRandomPolygons(polygons, 1000);
    ClipPolygons(polygons);
    return 0;
}