#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include "volvis2kzv.h"
#include "voxel/volvis_voxel_loader.h"
#include "voxel/kzv.h"
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

using namespace kaze;

static int WriteKZV(const char* szOutName, int dim[3], const unsigned char* ptr)
{
    kzv_header header;
    strcpy(header.MAGIC, "KZV");
    header.version = 1;
    memcpy(header.dim, dim, sizeof(int) * 3);
    FILE* fp = fopen(szOutName, "wb");
    if (fp == 0) return -1;

    int w = dim[0];
    int h = dim[1];
    int d = dim[2];
    int sz = w * h * d;

    fwrite(&header, sizeof(kzv_header), 1, fp);
    fwrite(ptr, 1, sizeof(unsigned char) * sz, fp);

    fclose(fp);
    return 0;
}

int volvis2kzv(const char* szInName, const char* szOutName, int dim[3])
{
    volvis_voxel_loader vvl(szInName, dim);

    int w = vvl.dim(0);
    int h = vvl.dim(1);
    int d = vvl.dim(2);

    int sz = w * h * d;
    if (sz == 0) return -1;

    return WriteKZV(szOutName, dim, vvl.get_ptr());
}
