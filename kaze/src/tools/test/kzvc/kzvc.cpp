#include <stdio.h>
#include <string>
#include <vector>
#include <iostream>
#include <algorithm>

#include "volvis2kzv.h"
#include "kzv2mesh.h"

#define VERSION_MAJOR 1
#define VERSION_MINOR 0

struct command_option
{
    const char* option;
    int nargs;
};

struct command_output
{
    std::string option;
    std::vector<std::string> values;
};

class command
{
public:
    command(int argc, char** argv, const command_option* inputs)
    {
        {
            int i = 0;
            while (inputs[i].option)
            {
                inputs_.push_back(inputs[i]);
                i++;
            }
        }
        {
            for (int i = 1; i < argc; i++)
            {
                std::string arg = argv[i];
                int kFind = -1;
                for (int j = 0; j < inputs_.size(); j++)
                {
                    if (
                        arg == "-" + std::string(inputs_[j].option) ||
                        arg == "/" + std::string(inputs_[j].option))
                    {
                        kFind = j;
                        break;
                    }
                }
                if (kFind < 0)
                {
                    others_.push_back(arg);
                }
                else
                {
                    command_output out;
                    out.option = inputs_[kFind].option;
                    int ii = i;
                    if (inputs_[kFind].nargs > 0)
                    {
                        int maxarg = std::min(argc, ii + 1 + inputs_[kFind].nargs);
                        for (int k = ii + 1; k < maxarg; k++)
                        {
                            out.values.push_back(argv[k]);
                            i++;
                        }
                    }
                    outputs_.push_back(out);
                }
            }
        }
    }

    const std::vector<command_option>& inputs() const { return inputs_; }
    const std::vector<command_output>& outouts() const { return outputs_; }
    const std::vector<std::string>& others() const { return others_; }
private:
    std::vector<command_option> inputs_;
    std::vector<command_output> outputs_;
    std::vector<std::string> others_;
};

int main(int argc, char** argv)
{
    static command_option inputs[] = {
        {"v", 0},
        {"log", 0},
        {"help", 0},
        {"h", 0},
        {"dim", 3},
        {"dimension", 3},
        {"debug", 0},
        {"threshold", 1},
        {"dcf", 0},

        {NULL, 0}};

    int nRet = 0;

    command cmd(argc, argv, inputs);

    bool bQuiet = false;
    bool bForceDisplay = false;
    bool bDebug = false;
    bool bForce = false;
    bool bVersion = false;
    bool bHelp = false;
    bool bVerbose = false;
    bool bLog = false;
    bool bDcf = false;

    int mode = 0;

    int dim[3] = {1, 1, 1};
    double threshold = 0.5;

    const std::vector<command_output> outputs = cmd.outouts();

    for (size_t i = 0; i < outputs.size(); i++)
    {
        if (outputs[i].option == "debug")
        {
            bDebug = true;
        }
        else if (outputs[i].option == "v" || outputs[i].option == "version")
        {
            bVersion = true;
        }
        else if (outputs[i].option == "h" || outputs[i].option == "help")
        {
            bHelp = true;
        }
        else if (outputs[i].option == "log")
        {
            bLog = true;
        }
        else if (outputs[i].option == "dim" || outputs[i].option == "dimension")
        {
            dim[0] = atoi(outputs[i].values[0].c_str());
            dim[1] = atoi(outputs[i].values[1].c_str());
            dim[2] = atoi(outputs[i].values[2].c_str());
            mode = 1;
        }
        else if (outputs[i].option == "threshold")
        {
            threshold = atof(outputs[i].values[0].c_str());
            mode = 2;
        }
        else if (outputs[i].option == "dcf")
        {
            bDcf = true;
            mode = 3;
        }
    }

    if (bVersion)
    {
        printf("%s version %d.%d\n", argv[0], VERSION_MAJOR, VERSION_MINOR);
    }

    if (bHelp)
    {
        printf("HELP! > <\n");
    }

    const std::vector<std::string> others = cmd.others();
    if (others.size() < 1)
    {
        printf("%s \"%s\"\n", argv[0], "*.xxx");
        return -1;
    }

    if (mode == 1)
    { //convert volvis to kzv
        printf("convert volvis to kzv...\n");

        std::string srcFile = others[0];
        std::string dstFile;
        if (others.size() >= 2)
        {
            dstFile = others[1];
        }
        else
        {
            dstFile = srcFile + ".kzv";
        }
        return volvis2kzv(srcFile.c_str(), dstFile.c_str(), dim);
    }
    else if (mode == 2)
    { //convert kzv to obj
        printf("convert kzv to obj...\n");
        std::string srcFile = others[0];
        std::string dstFile;
        if (others.size() >= 2)
        {
            dstFile = others[1];
        }
        else
        {
            dstFile = srcFile + ".obj";
        }
        return kzv2mesh(srcFile.c_str(), dstFile.c_str(), threshold);
    }
    else if (mode == 3)
    { //convert dcf to obj
        /*
        printf("convert dcf to obj...\n");
        std::string srcFile = others[0];
        std::string dstFile;
        if (others.size() >= 2)
        {
            dstFile = others[1];
        }
        else
        {
            dstFile = srcFile + ".obj";
        }
        return dcf2mesh(srcFile.c_str(), dstFile.c_str());
        */
    }

    return 0;
}
