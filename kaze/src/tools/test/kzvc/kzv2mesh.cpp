#ifdef _MSC_VER
#pragma warning(disable : 4996)
#endif

#include "kzv2mesh.h"
#include "voxel/kzv.h"
#include "types.h"
#include "volume.hpp"
#include "evaluator/mc_volume_evaluator.h"
#include "evaluator/voxel_mc_volume_evaluator.h"
#include "extractor/mc_mesh_extractor.h"
//#include "extractor/emc_mesh_extractor.h"
//#include "extractor/tmp_dc_mesh_extractor.h"
//#include "extractor/dmc_mesh_extractor.h"
//#include "extractor/parallel_mesh_extractor.h"
//#include "io/mc_obj_io.h"

#include <stdio.h>
#include <vector>
#include <memory>

namespace kaze
{
    namespace
    {

        class kzv_volume : public volume<real>
        {
        public:
            kzv_volume(const char* szFile, real th)
                : th_(th)
            {
                dim_[0] = 0;
                dim_[1] = 0;
                dim_[2] = 0;

                FILE* fp = fopen(szFile, "rb");
                if (fp == 0) return;
                kzv_header header;
                fread(&header, sizeof(kzv_header), 1, fp);
                memcpy(dim_, header.dim, sizeof(int) * 3);
                int w = get_width();
                int h = get_height();
                int d = get_depth();
                int sz = w * h * d;
                data_.resize(sz);
                fread(&data_[0], sizeof(unsigned char), sz, fp);
                fclose(fp);
            }
            real get(int x, int y, int z) const
            {
                int w = get_width();
                int h = get_height();
                int d = get_depth();
                if (x < 0 || w <= x) return -th_;
                if (y < 0 || h <= y) return -th_;
                if (z < 0 || d <= z) return -th_;

                size_t idx = (size_t)(x + (w * (y + h * (z))));
                return real(data_[idx] / 255.0) - th_;
            }
            int get_width() const { return dim_[0]; }
            int get_height() const { return dim_[1]; }
            int get_depth() const { return dim_[2]; }

            bool is_vaild() const
            {
                int w = get_width();
                int h = get_height();
                int d = get_depth();
                if (w * h * d) return true;
                return false;
            }

        protected:
            int dim_[3];
            std::vector<unsigned char> data_;
            real th_;
        };
    }
}

int kzv2mesh(const char* szInName, const char* szOutName, double threshold)
{
    using namespace kaze;
    using namespace mc;

    std::unique_ptr<kzv_volume> ap(new kzv_volume(szInName, threshold));
    if (ap->is_vaild())
    {
        std::shared_ptr<volume<real> > vol(ap.release());
#if 0
		int dim[] =
		{
			256,//vol->get_width(),
			256,//vol->get_height(),
			256,//vol->get_depth()
		};
		std::shared_ptr<mc_volume_evaluator> ve( new offset_blob_mc_volume_evaluator(std::shared_ptr<blob_mc_volume_evaluator>(new impl_cube_blob_mc_volume_evaluator() ), 0.5) );
		/*
		std::vector<std::shared_ptr<blob_mc_volume_evaluator> > mv;
		mv.push_back(std::shared_ptr<blob_mc_volume_evaluator>(new impl_cube_blob_mc_volume_evaluator() ));
		mv.push_back(std::shared_ptr<blob_mc_volume_evaluator>(new sphere_blob_mc_volume_evaluator(vector3(0,0,0),1) ) );

		std::shared_ptr<mc_volume_evaluator> ve(
			new offset_blob_mc_volume_evaluator(
				std::shared_ptr<blob_mc_volume_evaluator>(new sub_blob_mc_volume_evaluator( mv ) ),
			 	threshold
			)
		);
		*/
#else
        int dim[] =
            {
                vol->get_width(),
                vol->get_height(),
                vol->get_depth()};
        std::shared_ptr<mc_volume_evaluator> ve(new voxel_mc_volume_evaluator(vol, new bilinear_volume_interpolator<real>()));
#endif

        mc_volume_evaluator::bound b = ve->get_bound();

        mc_mesh_extractor gen(b.min, b.max, dim);
        //emc_mesh_extractor gen(b.min, b.max, dim, values::radians(5));//6
        //dmc_mesh_extractor gen(b.min, b.max, dim, values::radians(20));//6
        //parallel_mesh_extractor gen(b.min, b.max, dim);//2
        //tmp_dc_mesh_extractor gen(b.min, b.max, dim); //2

        mc_mesh mesh;
        if (gen.run(mesh, *ve))
        {
            //save_to_obj(szOutName, mesh);
            return 0;
        }
    }
    return -1;
}

/*
int dcf2mesh(const char* szInName, const char* szOutName)
{
    using namespace kaze;
    using namespace mc;
    int dim[] = {10, 10, 10};
    vector3 min = vector3(-1, -1, -1);
    vector3 max = vector3(+1, +1, +1);

    tmp_dc_mesh_extractor gen(min, max, dim); //2

    mc_mesh mesh;
    if (gen.run(mesh, szInName))
    {
        save_to_obj(szOutName, mesh);
        return 0;
    }

    return -1;
}
*/
