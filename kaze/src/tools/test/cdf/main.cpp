#include "bit_vector_rs.hpp"

int main()
{
    int width = 10000;
    kaze::bit_vector_rs sem(width);
    for (int i = 0; i < width; i++)
    {
        sem.set(i, 1);
    }

    return 0;
}

urllib3