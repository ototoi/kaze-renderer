#include "types.h"
#include "values.h"
#include "svd.h"

#include <iostream>

using namespace kaze;

namespace kaze
{
    static svd::matrix Conv(const matrix3& m)
    {
        svd::matrix mm(3, 3);
        for (int j = 0; j < 3; j++)
        {
            for (int i = 0; i < 3; i++)
            {
                mm[j][i] = m[j][i];
            }
        }
        return mm;
    }
    static matrix3 Conv(const svd::matrix& m)
    {
        matrix3 mm;
        for (int j = 0; j < 3; j++)
        {
            for (int i = 0; i < 3; i++)
            {
                mm[j][i] = m[j][i];
            }
        }
        return mm;
    }
    static svd::vector Conv(const vector3& v)
    {
        svd::vector r(3);
        for (int i = 0; i < 3; i++)
            r[i] = v[i];
        return r;
    }
    static vector3 Conv(const svd::vector& v)
    {
        vector3 r;
        for (int i = 0; i < 3; i++)
            r[i] = v[i];
        return r;
    }

    /*
    static
    void matInverse ( const real mat[][3], real rvalue[][3] )
    {
        svd::matrix A(3,3);
        for(int j=0;j<3;j++){
            for(int i=0;i<3;i++){
                A[j][i] = mat[j][i];
            }    
        }
        svd::matrix U(3, 3);
        svd::vector S(3);
        svd::matrix V(3, 3);
    
        decomp(A, U, S, V);
        
        //V.transpose();
        svd::matrix SS(3,3);
        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                if(i == j)
                {
                    SS(i,j) =  1.0/S[i];  
                }
                else
                {
                    SS(i,j) = 0;
                }
            }
        }
        U.transpose();
        
        svd::matrix R(3,3);
        R = V * SS * U;                
        for(int j=0;j<3;j++){
            for(int i=0;i<3;i++){
                rvalue[j][i] = R[j][i];
            }    
        }
    }
    */

    static void matInverse(const svd::matrix& A, svd::matrix& R)
    {
        svd::matrix U(3, 3);
        svd::vector S(3);
        svd::matrix V(3, 3);

        decomp(A, U, S, V);

        //V.transpose();
        svd::matrix SS(3, 3);
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                if (i == j)
                {
                    SS(i, j) = 1.0 / S(i);
                }
                else
                {
                    SS(i, j) = 0;
                }
            }
        }
        U.transpose();
        //svd::matrix R(3,3);
        R = V * SS * U;
    }
    //U S V^t * x = b
    //x = ~M b
    //x = ~V ~S ~Ub
}

int main()
{
    using namespace std;
    using namespace kaze;
    using namespace kaze::svd;
    matrix3 m = mat3_gen::scaling(2, 2, 1) * mat3_gen::rotation_z(values::radians(30));
    vector3 v1 = vector3(0, 1, 0);
    vector3 v2 = m * v1;

    svd::matrix sm = Conv(m);
    svd::matrix ism(3, 3);
    matInverse(sm, ism);

    matrix3 mm = Conv(sm);
    matrix3 imm = Conv(ism);

    //{
    svd::matrix U(3, 3);
    svd::vector S(3);
    svd::matrix V(3, 3);

    svd::decomp(sm, U, S, V);

    svd::vector xx(3);
    backsub(U, S, V, Conv(v2), xx);
    vector3 v1x = Conv(xx);
    //}

    cout << v1 << ": m*v =" << m * v1 << endl;
    cout << v2 << ": ~m*v =" << ~m * v2 << endl;

    cout << v1x << ": mm*v =" << mm * v1 << endl;
    cout << v1x << ": imm*v =" << imm * v2 << endl;

    cout << "-------" << endl;
    cout << m << endl;
    cout << mm << endl;
    cout << ~m << endl;
    cout << imm << endl;
    cout << "-------" << endl;
    cout << m << endl;
    cout << Conv(sm) << endl;
    svd::matrix tm = sm;
    tm.transpose();
    cout << transpose(m) << endl;
    cout << Conv(tm) << endl;

    return 0;
}