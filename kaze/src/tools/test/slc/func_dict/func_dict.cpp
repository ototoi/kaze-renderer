#include "ri_slc_function_dictionary.h"

int main()
{
    using namespace kaze::ri;

    ri_slc_function_dictionary dict;

    dict.add("color sin(integer a, color b, color c)");
    dict.add("color cos(color a, color b, color c)");
    dict.add("vector sin(vector a, color b)");
    dict.add("vector sin(vector a, color b)");
    dict.add("vector sin(matrix a, color b)");
    dict.add("vector sin(string a, color b)");
    dict.add("vector sin(vector a, color b)");
    dict.add("void sin(vector a, color b)");

    dict.dump();

    return 0;
}
