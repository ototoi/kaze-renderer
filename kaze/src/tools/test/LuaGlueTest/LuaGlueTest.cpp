#ifdef __cplusplus
extern "C" {
#endif
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#ifdef __cplusplus
}
#endif

#ifndef LUA_UNSIGNED
#define LUA_UNSIGNED unsigned char
#endif

#define LUAGLUE_TYPECHECK 1

#include <LuaGlue/LuaGlue.h>

class Foo
{
public:
    Foo(int i) { printf("ctor! %i\n", i); }
    ~Foo() {}

    int abc(int a, int b, int c)
    {
        printf("%i:%i:%i\n", a, b, c);
        return 143;
    }
    static void aaa() { printf("aaa!\n"); }
};

class Bar
{
public:
    Bar(int i) { printf("ctor! %i\n", i); }
    ~Bar() {}

    int abc(int a, int b, int c)
    {
        printf("%i:%i:%i\n", 100 * a, b, c);
        return 143;
    }
    static void aaa() { printf("aaa!\n"); }
};

class Vec3
{
public:
    Vec3()
    {
        a = 0;
        b = 0;
        c = 0;
    }
    Vec3(float _a, float _b, float _c) : a(_a), b(_b), c(_c) {}
    void print() const { printf("%f:%f:%f\n", a, b, c); }
    Vec3 add(const Vec3& v)
    {
        a += v.a;
        b += v.b;
        c += v.c;
        return *this;
    }

protected:
    float a, b, c;
};

int main(int, char**)
{
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
    LuaGlue state(L);

    state
    .Class<Foo>("Foo")
        .ctor<int>("new")
        .method("abc", &Foo::abc)
        .method("aaa", &Foo::aaa)
        .constants({{"ONE", 1}, {"TWO", 2.0}, {"THREE", "three"}})
    .end();

    state
    .Class<Bar>("Bar")
        .ctor<int>("new")
        .method("abc", &Bar::abc)
        .method("aaa", &Bar::aaa)
        .constants({{"ONE", 1}, {"TWO", 2.0}, {"THREE", "three"}})
    .end();

    state.Class<Vec3>("Vec3").ctor<float, float, float>("new").method("add", &Vec3::add).method("print", &Vec3::print).end();

    state.glue();

    //if(!state.doFile("foo.lua"))
    if (LUA_OK != luaL_dofile(L, "foo.lua"))
    {
        printf("failed to dofile: foo.lua\n");
        printf("err: %s\n", state.lastError().c_str());
    }

    state.open(NULL);

    lua_close(L);

    return 0;
}