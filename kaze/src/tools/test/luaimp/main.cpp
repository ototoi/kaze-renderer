#ifdef __cplusplus
extern "C" {
#endif
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#ifdef __cplusplus
}
#endif

extern "C" int zzz()
{
    return 0;
}

int main()
{
    lua_State* L = luaL_newstate();
    luaL_openlibs(L);

    if (LUA_OK != luaL_dofile(L, "foo.lua"))
    {
        printf("failed to dofile: foo.lua\n");
        printf("err: %s\n", "state.lastError().c_str()");
    }

    lua_close(L);

    return 0;
}