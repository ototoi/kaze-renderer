#ifndef API_H
#define API_H

#ifdef __cplusplus
extern "C" {
#endif
    typedef int (*function_api_t)(int x, int y, int z);
    //extern int function_api(int x, int y, int z);
    extern function_api_t function_api = 0;
    extern function_api_t* function_api_p;

#ifdef __cplusplus
}
#endif

#endif