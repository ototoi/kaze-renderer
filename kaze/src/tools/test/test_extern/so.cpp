#include "api.h"

#ifdef __cplusplus
extern "C" {
#endif
    function_api_t* function_api_p = 0;
    extern int function_imp()
    {
        if(function_api_p)
        {
            function_api_t f = *function_api_p;
            return (f ? (f(1,2,3)) : 0);
        }
        return 0;
    }
    extern int function_imp2()
    {
        return 34567;
    }
#ifdef __cplusplus
}
#endif