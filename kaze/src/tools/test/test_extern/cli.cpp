#include <stdio.h>
#include <dlfcn.h>
#include "api.h"

typedef int (*function_imp_t)();

int api_cli(int x, int y, int z)
{
    return x+y*z;
}

int main()
{
    void* handle = dlopen("so.so", RTLD_LAZY);
    function_api_t* fapi_so_p = (function_api_t*)dlsym(handle, "function_api_p");
    printf("fapi_so:%p\n", fapi_so_p);

    *fapi_so_p = api_cli;
    function_imp_t fimp_so = (function_imp_t)dlsym(handle, "function_imp");
    printf("fimp_so:%p\n", fimp_so);

    int nRet = fimp_so();

    printf("%d\n", nRet);


    return 0;
}
