#include <string>
#include <vector>
#include <stdio.h>

#if defined(__APPLE__)
#if defined(OSD_USES_GLEW)
#include <GL/glew.h>
#else
#include <OpenGL/gl3.h>
#endif
#define GLFW_INCLUDE_GL3
#define GLFW_NO_GLU
#else
#include <stdlib.h>
#include <GL/glew.h>
#if defined(WIN32)
#include <GL/wglew.h>
#endif
#endif

#include <GLFW/glfw3.h>

GLFWwindow* g_window = 0;

// GUI variables
//int   g_displayStyle = Scene::SHADED, //Scene::PATCH_TYPE,

int g_drawBVH = false,
    g_blockFill = true,
    g_mbutton[3] = {0, 0, 0},
    g_partitioning = 1,
    g_running = 1;

float g_rotate[2] = {0, 0},
      g_dolly = 5,
      g_pan[2] = {0, 0},
      g_center[3] = {0, 0, 0},
      g_size = 0;

int g_prev_x = 0,
    g_prev_y = 0;

int g_frameBufferWidth = 1024,
    g_frameBufferHeight = 1024;

int g_width = 1024,
    g_height = 1024;
std::vector<float> g_image;
int g_step = 8;
int g_stepIndex = 0;

int g_level = 2;
int g_preTess = 0;
int g_preTessLevel = 1;
int g_intersectKernel = 1;
int g_watertight = 1;
int g_cropUV = 0;
int g_bezierClip = 1;
int g_debug = 0;
float g_debugScale = 0.01f;
int g_debugScope[2] = {g_width / 2, g_height / 2};
float g_uvMargin = 0.0f;

int g_epsLevel = 4;  //4->16
int g_maxLevel = 16; //10->32
int g_minLeafPrimitives = 2;
int g_useTriangle = 0;

int g_useRayDiffEpsilon = 1;
int g_conservativeTest = 1;
int g_directBilinear = 0;
int g_useSingleCreasePatch = 1;

int g_animate = 0;
int g_frame = 0;

struct Transform
{
    float ModelViewMatrix[16];
    float ProjectionMatrix[16];
    float ModelViewProjectionMatrix[16];
} g_transformData;

GLuint g_vao = 0;
GLuint g_vaoBVH = 0;
GLuint g_vbo = 0;
GLuint g_programSimpleFill = 0;
GLuint g_programBlockFill = 0;
GLuint g_programBVH = 0;
GLuint g_programDebug = 0;

float g_eye[] = {0, 0, 5, 1};
float g_lookat[] = {0, 0, 0, 1};
float g_up[] = {0, 1, 0, 0};

//------------------------------------------------------------------------------

void windowClose(GLFWwindow*)
{
    g_running = false;
}

static void
motion(GLFWwindow*, double dx, double dy)
{
}

static void
mouse(GLFWwindow*, int button, int state, int mods)
{
}

static void
reshape(GLFWwindow*, int width, int height)
{
}

static void
keyboardChar(GLFWwindow*, unsigned int codepoint)
{
}

static void
keyboard(GLFWwindow*, int key, int /* scancode */, int event, int /* mods */)
{
}

static GLuint
compileProgram(const char* vs, const char* gs, const char* fs)
{
    /*
    GLuint vertexShader = compileShader(GL_VERTEX_SHADER, vs);
    GLuint geometryShader = gs ? compileShader(GL_GEOMETRY_SHADER, gs) : 0;
    GLuint fragmentShader = compileShader(GL_FRAGMENT_SHADER, fs);
    GLuint program = glCreateProgram();
    glAttachShader(program, vertexShader);
    glAttachShader(program, fragmentShader);
    if (geometryShader) glAttachShader(program, geometryShader);
    glLinkProgram(program);
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);
    if (geometryShader) glDeleteShader(geometryShader);
    return program;
    */
    return 0;
}

static void
initGL()
{
    glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glCullFace(GL_BACK);
    glEnable(GL_CULL_FACE);

    glGenVertexArrays(1, &g_vao);
    glGenVertexArrays(1, &g_vaoBVH);

    glGenBuffers(1, &g_vbo);
    glBindBuffer(GL_ARRAY_BUFFER, g_vbo);
    static float pos[] = {-1, -1,
                          1, -1,
                          -1, 1,
                          1, 1,
                          0.5, -1,
                          1, -1,
                          0.5, -0.5,
                          1, -0.5};
    glBufferData(GL_ARRAY_BUFFER, sizeof(pos), pos, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    /*
    g_programBlockFill = compileProgram(s_VS, NULL, s_FS);
    g_programSimpleFill = compileProgram(s_VS, NULL, s0_FS);
    g_programBVH = compileProgram(s_VS_BVH, s_GS_BVH, s_FS_BVH);
    g_programDebug = compileProgram(s_VS_Debug, NULL, s_FS);
    */
}

static void
idle()
{
}

static void
setGLCoreProfile()
{
#define glfwOpenWindowHint glfwWindowHint
#define GLFW_OPENGL_VERSION_MAJOR GLFW_CONTEXT_VERSION_MAJOR
#define GLFW_OPENGL_VERSION_MINOR GLFW_CONTEXT_VERSION_MINOR

    glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#if not defined(__APPLE__)
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 4);
#ifdef OPENSUBDIV_HAS_GLSL_COMPUTE
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 3);
#else
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 2);
#endif

#else
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 3);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 2);
#endif
    glfwOpenWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
}

static void
uninitGL()
{

    glDeleteVertexArrays(1, &g_vao);
    glDeleteVertexArrays(1, &g_vaoBVH);
    glDeleteBuffers(1, &g_vbo);

    //delete g_computeContext;
}

int main(int argc, char** argv)
{
    std::string str;
    std::string envmapFile;
    for (int i = 1; i < argc; ++i)
    {
        ; //
    }

    if (!glfwInit())
    {
        printf("Failed to initialize GLFW\n");
        return 1;
    }

    static const char windowTitle[] = "test";

#define CORE_PROFILE
#ifdef CORE_PROFILE
    setGLCoreProfile();
#endif

    if (!(g_window = glfwCreateWindow(g_width, g_height, windowTitle, NULL, NULL)))
    {
        printf("Failed to open window.\n");
        glfwTerminate();
        return 1;
    }
    glfwMakeContextCurrent(g_window);

    glfwGetFramebufferSize(g_window, &g_frameBufferWidth, &g_frameBufferHeight);
    glfwSetFramebufferSizeCallback(g_window, reshape);

    glfwSetCharCallback(g_window, keyboardChar);
    glfwSetKeyCallback(g_window, keyboard);
    glfwSetCursorPosCallback(g_window, motion);
    glfwSetMouseButtonCallback(g_window, mouse);
    glfwSetWindowCloseCallback(g_window, windowClose);

#if defined(OSD_USES_GLEW)
#ifdef CORE_PROFILE
    // this is the only way to initialize glew correctly under core profile context.
    glewExperimental = true;
#endif
    if (GLenum r = glewInit() != GLEW_OK)
    {
        printf("Failed to initialize glew. Error = %s\n", glewGetErrorString(r));
        exit(1);
    }
#ifdef CORE_PROFILE
    // clear GL errors which was generated during glewInit()
    glGetError();
#endif
#endif

    initGL();

    glfwSwapInterval(0);

    g_image.resize(g_width * g_height * 4);
    //g_scene.SetShadeMode((Scene::ShadeMode)g_displayStyle);

    while (g_running)
    {
        idle();

        glfwPollEvents();

        if (not g_animate and g_stepIndex == 0) glfwWaitEvents();
    }

    uninitGL();
    glfwTerminate();
}