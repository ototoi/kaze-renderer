#include <GL/glew.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#ifdef _WIN32
#include <windows.h>
#else
#include <libgen.h>
#include <stdlib.h>
#endif

#ifdef __APPLE__
#include <mach-o/dyld.h>
#endif

#if defined(__linux__) || defined(FREEBSD)
#include <unistd.h>
#endif

#include "memory_image.hpp"

#include "image/bmp_io.h"
#include "image/png_io.h"
#include "image/tiff_io.h"
#include "image/jpeg_io.h"
#include "image/tga_io.h"

#include "gl_texture.h"
#include "gl_shader.h"
#include "gl_program.h"
#include "gl_frame_buffer.h"

namespace kaze
{
    namespace lf
    {

        static std::string ri_get_full_path(const std::string& path)
        {
#if defined(_WIN32)
            char szFullPath[_MAX_PATH];
            _fullpath(szFullPath, path.c_str(), _MAX_PATH);
            return szFullPath;
#else
            char szFullPath[1024];
            realpath(path.c_str(), szFullPath);
            return szFullPath;
#endif
        }

        static std::string ri_get_exec_path_()
        {
#if defined(_WIN32)
            char buffer[_MAX_PATH];
            ::GetModuleFileNameA(NULL, buffer, _MAX_PATH);
            return buffer;
#elif defined(__APPLE__)
            char path[1024] = {};
            uint32_t size = sizeof(path);
            _NSGetExecutablePath(path, &size);
            return ri_get_full_path(path);
#elif defined(__linux__)
            char path[1024] = {};
            size_t size = sizeof(path);
            readlink("/proc/self/exe", path, size);
            return path;
#elif defined(FREEBSD)
            char path[1024] = {};
            size_t size = sizeof(path);
            readlink("/proc/curproc/file", path, size);
            return path;
#else
            return "";
#endif
        }

        static std::string dirname_(const std::string& path)
        {
            return path.substr(0, path.find_last_of('/'));
        }

        static std::string ri_get_shaders_path()
        {
#ifdef _WIN32
            static std::string strExec = ri_get_exec_path_();
            char szDrive[_MAX_DRIVE];
            char szDir[_MAX_DIR];
            _splitpath(strExec.c_str(), szDrive, szDir, NULL, NULL);
            std::string strRet1;
            strRet1 += szDrive;
            strRet1 += szDir;
            strRet1 += "../shaders/";
            return ri_get_full_path(strRet1);
#else
            static std::string strExec = ri_get_full_path(ri_get_exec_path_());
            static std::string strDir = dirname_(strExec);
            std::string strRet1;
            strRet1 = strDir + "/";
            strRet1 += "../shaders/";
            return ri_get_full_path(strRet1) + "/";
#endif
        }

        static int WriteTiffImage(const char* szTexFile, const std::shared_ptr<image<color4> >& img)
        {
            std::string strFile = szTexFile;

            int width = img->get_width();
            int height = img->get_height();
            std::vector<unsigned char> buffer(width * height * 4);
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    size_t index = y * width + x;
                    buffer[4 * index + 0] = (unsigned char)(255 * (img->get(x, y)[0]));
                    buffer[4 * index + 1] = (unsigned char)(255 * (img->get(x, y)[1]));
                    buffer[4 * index + 2] = (unsigned char)(255 * (img->get(x, y)[2]));
                    buffer[4 * index + 3] = (unsigned char)(255 * (img->get(x, y)[3]));
                }
            }

            WriteTiffRGBAImage(strFile.c_str(), &buffer[0], width, height);

            return 0;
        }

        int DrawImage(
            std::shared_ptr<image<color4> >& outImg)
        {
            int width = outImg->get_width();
            int height = outImg->get_height();

            const GLubyte* strVersion = ::glGetString(GL_VERSION);
            double fVersion = atof((const char*)strVersion);
            if (fVersion < 4) return -1;

            std::shared_ptr<gl::gl_texture_2D> texOutput(new gl::gl_texture_2D());
            texOutput->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, 0);
            //-----------------------------------------------------------------------------------------------
            std::shared_ptr<gl::gl_frame_buffer> fboPtr(new gl::gl_frame_buffer());
            {
                fboPtr->bind();
                fboPtr->attach_color(texOutput, 0);
                //::glFramebufferRenderbufferEXT( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT,
                //                          GL_RENDERBUFFER, nDepthBufID );
                //GLenum status = fboPtr->check_status();
                fboPtr->unbind();
            }
            //-----------------------------------------------------------------------------------------------
            {
                std::string strFolder = ri_get_shaders_path() + "glsl/";
                std::shared_ptr<gl::gl_program> prog(new gl::gl_program());
                std::shared_ptr<gl::gl_shader> vsh(new gl::gl_shader(GL_VERTEX_SHADER));
                std::shared_ptr<gl::gl_shader> fsh(new gl::gl_shader(GL_FRAGMENT_SHADER));
                //if(!vsh->compile_from_file((strFolder+"draw_iris.vs").c_str()))return -1;
                //if(!fsh->compile_from_file((strFolder+"draw_iris.fs").c_str()))return -1;
                if (!vsh->compile_from_file((strFolder + "draw_streak.vs").c_str())) return -1;
                if (!fsh->compile_from_file((strFolder + "draw_streak.fs").c_str())) return -1;
                prog->add_shader(vsh->get_handle());
                prog->add_shader(fsh->get_handle());
                prog->link();

                fboPtr->bind();
                ::glDisable(GL_DEPTH_TEST);
                ::glDisable(GL_LIGHTING);
                ::glDepthMask(GL_FALSE);

                float posData[] = {
                    -1.0f, -1.0f, 0.0f, //0
                    -1.0f, +1.0f, 0.0f, //1
                    +1.0f, -1.0f, 0.0f, //2
                    +1.0f, +1.0f, 0.0f, //3
                };
                float uvData[] = {
                    0.0f, 0.0f,
                    0.0f, 1.0f,
                    1.0f, 0.0f,
                    1.0f, 1.0f,
                };
                GLuint vboHandles[2];
                ::glGenBuffers(2, vboHandles);
                GLuint posBufferHandle = vboHandles[0];
                GLuint uvBufferHandle = vboHandles[1];

                ::glBindBuffer(GL_ARRAY_BUFFER, posBufferHandle);
                ::glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(float), posData, GL_STATIC_DRAW);

                ::glBindBuffer(GL_ARRAY_BUFFER, uvBufferHandle);
                ::glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(float), uvData, GL_STATIC_DRAW);

                ::glViewport(0, 0, width, height);

                GLuint nlocPosition = 0;
                GLuint nlocCoord = 0;

                GLuint shader_prog = prog->get_handle();
                ::glUseProgram(shader_prog);

                nlocPosition = prog->get_attrib_location("pos");
                nlocCoord = prog->get_attrib_location("uv");
                /*
                if(0){
                    GLuint nlocTex      = 0;
            		GLuint nlocWidth    = 0;
            		GLuint nlocHeight   = 0;
                    GLuint nlocSigma    = 0;
                    
                    nlocTex      = prog->get_uniform_location("tex");
            		nlocWidth    = prog->get_uniform_location("width");
            		nlocHeight   = prog->get_uniform_location("height");
                    nlocSigma    = prog->get_uniform_location("sigma");
            
                    ::glActiveTexture(GL_TEXTURE0 + 0);
                    ::glBindTexture(GL_TEXTURE_2D, nInTexID);
            
                    prog->set_uniform(nlocTex, 0);
            		prog->set_uniform(nlocWidth,  width);
            		prog->set_uniform(nlocHeight, height);               
                }
 */

                GLuint vaoHandle;
                ::glGenVertexArrays(1, &vaoHandle);
                ::glBindVertexArray(vaoHandle);

                ::glEnableVertexAttribArray(nlocPosition);
                ::glEnableVertexAttribArray(nlocCoord);

                ::glBindBuffer(GL_ARRAY_BUFFER, posBufferHandle);
                ::glVertexAttribPointer(nlocPosition, 3, GL_FLOAT, GL_FALSE, 0, NULL);

                ::glBindBuffer(GL_ARRAY_BUFFER, uvBufferHandle);
                ::glVertexAttribPointer(nlocCoord, 2, GL_FLOAT, GL_FALSE, 0, NULL);

                ::glClearColor(0, 0, 0, 1);
                ::glClear(GL_COLOR_BUFFER_BIT);

                ::glBindVertexArray(vaoHandle);
                ::glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                ::glBindVertexArray(0);

                glDeleteVertexArrays(1, &vaoHandle);

                ::glDisableVertexAttribArray(nlocCoord);
                ::glDisableVertexAttribArray(nlocPosition);

                ::glBindTexture(GL_TEXTURE_2D, 0);

                ::glUseProgram(0);

                ::glFlush();
                fboPtr->unbind();
            }
            //-----------------------------------------------------------------------------------------------
            {
                std::vector<float> fBuffer(4 * width * height, 0.0f);
                texOutput->bind();
                ::glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &fBuffer[0]);
                outImg.reset(new memory_image<color4>(width, height));
                for (int y = 0; y < height; y++)
                {
                    for (int x = 0; x < width; x++)
                    {
                        size_t index = y * width + x;
                        float r = fBuffer[4 * index + 0];
                        float g = fBuffer[4 * index + 1];
                        float b = fBuffer[4 * index + 2];
                        float a = fBuffer[4 * index + 3];
                        outImg->set(x, y, color4(r, g, b, a));
                    }
                }
                texOutput->unbind();
            }
            //-----------------------------------------------------------------------------------------------
            return 0;
        }

        int DebugDrawParticles(int argc, char** argv)
        {
            std::shared_ptr<image<color4> > outImg(new memory_image<color4>(512, 512));
            printf("a\n");
            int nRet = DrawImage(outImg);
            printf("c\n");
            WriteTiffImage("test.tiff", outImg);
            return nRet;
        }
    }
}
