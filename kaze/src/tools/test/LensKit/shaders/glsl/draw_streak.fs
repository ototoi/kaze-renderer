#version 410

in vec3 fpos;
in vec2 fuv;

layout(location = 0) out vec4 out_color;

const float thickness     = 0.01;
const float symmetry      = 0.1;
const float fanEnds       = 0.4;
const float fanFeathering = 0.1;

const float glowRadius = 1.0;
const float frontGain = 1.0;
const float gammaV = 1.0;
const float backGain = 1.0; 

const vec3  col = vec3(1.0,0.0,0.0);

const float PI = 3.14159265358979323846264;

float attenuationOfLight(float map, float radius, float frontGain, float gamma, float backGain)
{
    return max(0,1-map);
}

float getShape(float xx, float yy)
{
    float coordW = xx;
    float coordH = yy;
    
    if(coordW > 0.0f){
        coordW = coordW / symmetry;
    }

    coordH /= thickness;
    coordW = abs(coordW);

    //
    float wb = step(atan(coordH,coordW)/PI*2.0f , atan(fanEnds/1.0f)/PI*2.0f);

    //
    float shape = coordH/fanEnds*wb*step(0.0f, thickness) + (coordH-coordW*(fanEnds-1.0f))*(1-wb);

    //
    float circleMap = sqrt(coordW * coordW + coordH * coordH);    //
    float streakMap = sqrt(coordW * coordW + shape * shape);      //

    //
    float map = circleMap*fanFeathering + streakMap*(1-fanFeathering);

    //
    float color = attenuationOfLight
            (
                map,
                glowRadius,
                frontGain,
                gammaV,
                backGain
            );
                
    //
    float result = max(0.0f, color);
    
    //
    return result;
}

void main()
{
    float xx = 2.0 * fuv.x - 1.0;
    float yy = 2.0 * fuv.y - 1.0;
    
    out_color = vec4(col * getShape(xx, yy),1);
}