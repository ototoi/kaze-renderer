#version 410

in vec3 fpos;
in vec2 fuv;

layout(location = 0) out vec4 out_color;

const int   blades           = 8;
const float roundness        = 0.2;
const float notching         = 0.01;
const float notching_pos     = 0.85;

const vec3  col = vec3(1.0,0.0,0.0);

const float PI = 3.14159265358979323846264;

float getShape(float xx, float yy)
{
    float deltaAngle  = 2*PI/blades;
    float deltaCos    = cos(0.5*deltaAngle);
    
    float rotMap    = (atan(xx,yy) + PI)/(2*PI);
    float rotMapFun = rotMap * blades - floor(rotMap * blades);
    
    //circle
    float circleShape = sqrt(xx * xx + yy * yy);
    
    //polygon
    float polygonShape = circleShape * cos(deltaAngle * (abs(rotMapFun - 0.5))) / deltaCos;
    
    //roundness
    float roundnessShape = polygonShape + (circleShape - polygonShape) * roundness;
    
    //notch
    float lv = (roundnessShape - rotMapFun / notching_pos * notching * roundnessShape);
    float rv = (roundnessShape - (1 - (rotMapFun-notching_pos)/(1-notching_pos)) * notching * roundnessShape);
    
    float shape = step(rotMapFun, notching_pos) * lv + step(notching_pos, rotMapFun) * rv;
    
    //
    float brigtness = min(shape,1);
    
    return brigtness * (1 -step(0.8, brigtness));
}

void main()
{
    float xx = 2.0 * fuv.x - 1.0;
    float yy = 2.0 * fuv.y - 1.0;
    
    out_color = vec4(col * getShape(xx, yy),1);
}