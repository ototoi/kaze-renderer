#ifndef KAZE_LF_DEBUG_DRAW_H
#define KAZE_LF_DEBUG_DRAW_H

namespace kaze
{
    namespace lf
    {

        int DebugDrawParticles(int argc, char** argv);
    }
}

#endif