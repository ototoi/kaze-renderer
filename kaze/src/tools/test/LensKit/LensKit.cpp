#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <string>

#include <GL/glew.h>
#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <OpenGL/glu.h>
#else
#include <GL/gl.h>
#include <GL/glu.h>
#endif

#include "gl_create_context.h"
#include "lf_debug_draw.h"

static int init()
{
    int visual = -1;
#if defined(_WIN32)
    if (GL_TRUE == gl_create_context(&visual))
#elif defined(__APPLE__)
    if (GL_TRUE == gl_create_context())
#else
    if (GL_TRUE == gl_create_context(display, &visual))
#endif
    {
        gl_destroy_context();
        return -1;
    }

    glewExperimental = GL_TRUE; //><

    int err = glewInit();
    if (GLEW_OK != err)
    {
        fprintf(stderr, "Error [main]: glewInit failed: %s\n", glewGetErrorString(err));
        gl_destroy_context();
        return -1;
    }

    return 0;
}

static int finalize()
{
    gl_destroy_context();
    return 0;
}

int main(int argc, char** argv)
{
    using namespace kaze::lf;

    int nRet = 0;

    if (init() != 0)
    {
        return -1;
    }

    nRet = DebugDrawParticles(argc, argv);

    finalize();
    return nRet;
}
