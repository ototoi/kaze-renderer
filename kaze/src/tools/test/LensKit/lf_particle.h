#ifndef KAZE_LF_PARTICLE_H
#define KAZE_LF_PARTICLE_H

namespace kaze
{
    namespace lf
    {

        class particle
        {
        public:
            enum
            {
                TYPE_ALPHA,
                TYPE_ADD,
            };

        public:
            virtual ~particle() {} //
            virtual int get_type() const { return TYPE_ADD; }
        };
    }
}

#endif