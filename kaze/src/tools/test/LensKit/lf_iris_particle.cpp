#include "lf_iris_particle.h"

namespace kaze
{
    namespace lf
    {

        iris_particle::iris_particle(
            int blades,
            int roundness,
            double notching,
            double notching_pos,
            const vector3& col,
            const matrix4& mat) : blades_(blades),
                                  roundness_(roundness),
                                  notching_(notching),
                                  notching_pos_(notching_pos),
                                  col_(col),
                                  mat_(mat)
        {
            ; //
        }

        void iris_particle_creator::create(std::vector<std::shared_ptr<particle> >& particles) const
        {
            int blades = 6;
            double roundness = 0.5;
            double notching = 0.5;
            double notching_pos = 0.1;
            vector3 col = vector3(1, 1, 1);
            matrix4 mat = mat4_gen::identity();

            for (int i = 0; i < 1; i++)
            {
                std::shared_ptr<particle> particle(new iris_particle(blades, roundness, notching, notching_pos, col, mat));
                particles.push_back(particle);
            }
        }
    }
}
