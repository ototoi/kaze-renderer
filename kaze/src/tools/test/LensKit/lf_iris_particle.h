#ifndef KAZE_LF_IRIS_PARTCLE_H
#define KAZE_LF_IRIS_PARTCLE_H

#include <vector>
#include <memory>
#include "types.h"

#include "lf_particle.h"

namespace kaze
{
    namespace lf
    {
        /*
        int    blades;      //1..32
        double roundness;   //0..1
        double notching;    //0..1
        double notching_pos //0..1
        */
        class iris_particle : public particle
        {
        public:
            iris_particle(
                int blades,
                int roundness,
                double notching,
                double notching_pos,
                const vector3& col,
                const matrix4& mat);

        private:
            int blades_;          //1..32
            double roundness_;    //0..1
            double notching_;     //0..1
            double notching_pos_; //0..1
            vector3 col_;
            matrix4 mat_;
        };

        class iris_particle_creator
        {
        public:
            void create(std::vector<std::shared_ptr<particle> >& particles) const;
        };
    }
}

#endif
