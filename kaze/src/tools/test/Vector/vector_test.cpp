#include <tempest/vector.hpp>
#include <tempest/vector2.hpp>
#include <tempest/vector3.hpp>
#include <tempest/vector4.hpp>
#include <tempest/matrix.hpp>
#include <tempest/matrix2.hpp>
#include <tempest/matrix3.hpp>
#include <tempest/matrix4.hpp>
#include <tempest/transform.hpp>
#include <tempest/matrix_generator.hpp>
#include <tempest/quaternion.hpp>
#include <tempest/et_vector.hpp>

#include <iostream>

typedef tempest::vector<double,3> vec3;


int main()
{
    using namespace std;
    using namespace tempest; 

    vec3 v1 = vec3(1,2,3);
    vec3 v2 = vec3(4,5,6);

    vec3 v3 = vec3(0,0,0);
    v3 << (et(v1) + et(v2) * 2.0 );

    std::cout << v1 << std::endl;
    std::cout << v2 << std::endl;
    std::cout << v3 << std::endl;
    std::cout << 2.0 << std::endl;
    std::cout << (et(v1) / (et(v2) * 2.0)) << std::endl;
    std::cout << (et(v1) / et(v2) * 2.0) << std::endl;
    std::cout << (2.0*et(v1) - et(v2)) << std::endl;

    std::cout << (et(v1) * 2.0 * et(v2)) << std::endl;

    return 0;
}
