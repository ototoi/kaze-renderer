#ifndef RSLC_FS_CONVERT_H
#define RSLC_FS_CONVERT_H

namespace rslc
{
    int rslc_fs_convert(const char* szInput, const char* szOutPut);
}

#endif