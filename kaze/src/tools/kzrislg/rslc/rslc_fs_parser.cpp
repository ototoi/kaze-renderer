/* A Bison parser, made by GNU Bison 2.3.  */

/* Skeleton implementation for Bison LALR(1) parsers in C++

   Copyright (C) 2002, 2003, 2004, 2005, 2006 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 51 Franklin Street, Fifth Floor,
   Boston, MA 02110-1301, USA.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */


#include "rslc_fs_parser.hpp"

/* User implementation prologue.  */
#line 48 "rslc/rslc_fs_parser.yy"


#define yyerror(m) error(yylloc,m)

using namespace rslc;


#define GET_CTX() (((rslc_fs_parse_param*)(scanner))->ctx)
#define GET_LINE() (((rslc_fs_parse_param*)(scanner))->line_number)
#define GET_FNAME() (((rslc_fs_parse_param*)(scanner))->filename)
#define PRINT(s) printf(s)

typedef char* LPCSTR;

static
void release_value(LPCSTR v){delete[] v;v=NULL;}



#define	YY_DECL											\
	int						\
	yylex(yy::rslc_fs_parser::semantic_type* yylval,		\
		 yy::rslc_fs_parser::location_type* yylloc,		\
		 void* scanner)

YY_DECL;




/* Line 317 of lalr1.cc.  */
#line 72 "rslc/rslc_fs_parser.cpp"

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* FIXME: INFRINGES ON USER NAME SPACE */
#   define YY_(msgid) dgettext ("bison-runtime", msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(msgid) msgid
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#define YYUSE(e) ((void) (e))

/* A pseudo ostream that takes yydebug_ into account.  */
# define YYCDEBUG							\
  for (bool yydebugcond_ = yydebug_; yydebugcond_; yydebugcond_ = false)	\
    (*yycdebug_)

/* Enable debugging if requested.  */
#if YYDEBUG

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)	\
do {							\
  if (yydebug_)						\
    {							\
      *yycdebug_ << Title << ' ';			\
      yy_symbol_print_ ((Type), (Value), (Location));	\
      *yycdebug_ << std::endl;				\
    }							\
} while (false)

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug_)				\
    yy_reduce_print_ (Rule);		\
} while (false)

# define YY_STACK_PRINT()		\
do {					\
  if (yydebug_)				\
    yystack_print_ ();			\
} while (false)

#else /* !YYDEBUG */

# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_REDUCE_PRINT(Rule)
# define YY_STACK_PRINT()

#endif /* !YYDEBUG */

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrorlab

namespace yy
{
#if YYERROR_VERBOSE

  /* Return YYSTR after stripping away unnecessary quotes and
     backslashes, so that it's suitable for yyerror.  The heuristic is
     that double-quoting is unnecessary unless the string contains an
     apostrophe, a comma, or backslash (other than backslash-backslash).
     YYSTR is taken from yytname.  */
  std::string
  rslc_fs_parser::yytnamerr_ (const char *yystr)
  {
    if (*yystr == '"')
      {
        std::string yyr = "";
        char const *yyp = yystr;

        for (;;)
          switch (*++yyp)
            {
            case '\'':
            case ',':
              goto do_not_strip_quotes;

            case '\\':
              if (*++yyp != '\\')
                goto do_not_strip_quotes;
              /* Fall through.  */
            default:
              yyr += *yyp;
              break;

            case '"':
              return yyr;
            }
      do_not_strip_quotes: ;
      }

    return yystr;
  }

#endif

  /// Build a parser object.
  rslc_fs_parser::rslc_fs_parser (void * scanner_yyarg)
    : yydebug_ (false),
      yycdebug_ (&std::cerr),
      scanner (scanner_yyarg)
  {
  }

  rslc_fs_parser::~rslc_fs_parser ()
  {
  }

#if YYDEBUG
  /*--------------------------------.
  | Print this symbol on YYOUTPUT.  |
  `--------------------------------*/

  inline void
  rslc_fs_parser::yy_symbol_value_print_ (int yytype,
			   const semantic_type* yyvaluep, const location_type* yylocationp)
  {
    YYUSE (yylocationp);
    YYUSE (yyvaluep);
    switch (yytype)
      {
         default:
	  break;
      }
  }


  void
  rslc_fs_parser::yy_symbol_print_ (int yytype,
			   const semantic_type* yyvaluep, const location_type* yylocationp)
  {
    *yycdebug_ << (yytype < yyntokens_ ? "token" : "nterm")
	       << ' ' << yytname_[yytype] << " ("
	       << *yylocationp << ": ";
    yy_symbol_value_print_ (yytype, yyvaluep, yylocationp);
    *yycdebug_ << ')';
  }
#endif /* ! YYDEBUG */

  void
  rslc_fs_parser::yydestruct_ (const char* yymsg,
			   int yytype, semantic_type* yyvaluep, location_type* yylocationp)
  {
    YYUSE (yylocationp);
    YYUSE (yymsg);
    YYUSE (yyvaluep);

    YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

    switch (yytype)
      {
  
	default:
	  break;
      }
  }

  void
  rslc_fs_parser::yypop_ (unsigned int n)
  {
    yystate_stack_.pop (n);
    yysemantic_stack_.pop (n);
    yylocation_stack_.pop (n);
  }

  std::ostream&
  rslc_fs_parser::debug_stream () const
  {
    return *yycdebug_;
  }

  void
  rslc_fs_parser::set_debug_stream (std::ostream& o)
  {
    yycdebug_ = &o;
  }


  rslc_fs_parser::debug_level_type
  rslc_fs_parser::debug_level () const
  {
    return yydebug_;
  }

  void
  rslc_fs_parser::set_debug_level (debug_level_type l)
  {
    yydebug_ = l;
  }


  int
  rslc_fs_parser::parse ()
  {
    /// Look-ahead and look-ahead in internal form.
    int yychar = yyempty_;
    int yytoken = 0;

    /* State.  */
    int yyn;
    int yylen = 0;
    int yystate = 0;

    /* Error handling.  */
    int yynerrs_ = 0;
    int yyerrstatus_ = 0;

    /// Semantic value of the look-ahead.
    semantic_type yylval;
    /// Location of the look-ahead.
    location_type yylloc;
    /// The locations where the error started and ended.
    location yyerror_range[2];

    /// $$.
    semantic_type yyval;
    /// @$.
    location_type yyloc;

    int yyresult;

    YYCDEBUG << "Starting parse" << std::endl;


    /* Initialize the stacks.  The initial state will be pushed in
       yynewstate, since the latter expects the semantical and the
       location values to have been already stored, initialize these
       stacks with a primary value.  */
    yystate_stack_ = state_stack_type (0);
    yysemantic_stack_ = semantic_stack_type (0);
    yylocation_stack_ = location_stack_type (0);
    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yylloc);

    /* New state.  */
  yynewstate:
    yystate_stack_.push (yystate);
    YYCDEBUG << "Entering state " << yystate << std::endl;
    goto yybackup;

    /* Backup.  */
  yybackup:

    /* Try to take a decision without look-ahead.  */
    yyn = yypact_[yystate];
    if (yyn == yypact_ninf_)
      goto yydefault;

    /* Read a look-ahead token.  */
    if (yychar == yyempty_)
      {
	YYCDEBUG << "Reading a token: ";
	yychar = yylex (&yylval, &yylloc, scanner);
      }


    /* Convert token to internal form.  */
    if (yychar <= yyeof_)
      {
	yychar = yytoken = yyeof_;
	YYCDEBUG << "Now at end of input." << std::endl;
      }
    else
      {
	yytoken = yytranslate_ (yychar);
	YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
      }

    /* If the proper action on seeing token YYTOKEN is to reduce or to
       detect an error, take that action.  */
    yyn += yytoken;
    if (yyn < 0 || yylast_ < yyn || yycheck_[yyn] != yytoken)
      goto yydefault;

    /* Reduce or error.  */
    yyn = yytable_[yyn];
    if (yyn <= 0)
      {
	if (yyn == 0 || yyn == yytable_ninf_)
	goto yyerrlab;
	yyn = -yyn;
	goto yyreduce;
      }

    /* Accept?  */
    if (yyn == yyfinal_)
      goto yyacceptlab;

    /* Shift the look-ahead token.  */
    YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

    /* Discard the token being shifted unless it is eof.  */
    if (yychar != yyeof_)
      yychar = yyempty_;

    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yylloc);

    /* Count tokens shifted since error; after three, turn off error
       status.  */
    if (yyerrstatus_)
      --yyerrstatus_;

    yystate = yyn;
    goto yynewstate;

  /*-----------------------------------------------------------.
  | yydefault -- do the default action for the current state.  |
  `-----------------------------------------------------------*/
  yydefault:
    yyn = yydefact_[yystate];
    if (yyn == 0)
      goto yyerrlab;
    goto yyreduce;

  /*-----------------------------.
  | yyreduce -- Do a reduction.  |
  `-----------------------------*/
  yyreduce:
    yylen = yyr2_[yyn];
    /* If YYLEN is nonzero, implement the default value of the action:
       `$$ = $1'.  Otherwise, use the top of the stack.

       Otherwise, the following line sets YYVAL to garbage.
       This behavior is undocumented and Bison
       users should not rely upon it.  */
    if (yylen)
      yyval = yysemantic_stack_[yylen - 1];
    else
      yyval = yysemantic_stack_[0];

    {
      slice<location_type, location_stack_type> slice (yylocation_stack_, yylen);
      YYLLOC_DEFAULT (yyloc, slice, yylen);
    }
    YY_REDUCE_PRINT (yyn);
    switch (yyn)
      {
	  case 2:
#line 184 "rslc/rslc_fs_parser.yy"
    {
			GET_CTX()->set_final_node((yysemantic_stack_[(1) - (1)].ntype));
		;}
    break;

  case 3:
#line 190 "rslc/rslc_fs_parser.yy"
    {
			rslc_fs_function_node* n1 = (rslc_fs_function_node*)(yysemantic_stack_[(1) - (1)].ntype);
            std::shared_ptr<rslc_fs_node> node(new rslc_fs_function_list_node());
			GET_CTX()->add_node(node);
			rslc_fs_function_list_node* n = (rslc_fs_function_list_node*)node.get();
			n->add(n1);
			(yyval.ntype) = n;
		;}
    break;

  case 4:
#line 199 "rslc/rslc_fs_parser.yy"
    {
			rslc_fs_function_list_node* n1 = (rslc_fs_function_list_node*)(yysemantic_stack_[(2) - (1)].ntype);
            rslc_fs_function_node* n2 = (rslc_fs_function_node*)(yysemantic_stack_[(2) - (2)].ntype);
			n1->add(n2);
			(yyval.ntype) = n1;
		;}
    break;

  case 5:
#line 209 "rslc/rslc_fs_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 6:
#line 216 "rslc/rslc_fs_parser.yy"
    {
			rslc_fs_type_node*          n1 = (rslc_fs_type_node*)(yysemantic_stack_[(6) - (1)].ntype);
			rslc_fs_variable_list_node* n4 = (rslc_fs_variable_list_node*)(yysemantic_stack_[(6) - (4)].ntype);

			rslc_fs_type       type = n1->get_type();
			rslc_fs_identifier name = (yysemantic_stack_[(6) - (2)].stype);
			const std::vector<rslc_fs_variable_list>& args = n4->get_list();
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_function_node(type, name, args));
			GET_CTX()->add_node(node);
			
			(yyval.ntype) = node.get();

			release_value((yysemantic_stack_[(6) - (2)].stype));
		;}
    break;

  case 7:
#line 231 "rslc/rslc_fs_parser.yy"
    {
			rslc_fs_type_node*        n1 = (rslc_fs_type_node*)(yysemantic_stack_[(5) - (1)].ntype);
			rslc_fs_type       type = n1->get_type();
			rslc_fs_identifier name = (yysemantic_stack_[(5) - (2)].stype);
			std::vector<rslc_fs_variable_list> args; // dummy
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_function_node(type, name, args));
			GET_CTX()->add_node(node);
			
			(yyval.ntype) = node.get();

			release_value((yysemantic_stack_[(5) - (2)].stype));
		;}
    break;

  case 8:
#line 247 "rslc/rslc_fs_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 9:
#line 251 "rslc/rslc_fs_parser.yy"
    {
			rslc_fs_variable_list_node* n1 = (rslc_fs_variable_list_node*)(yysemantic_stack_[(3) - (1)].ntype);
			rslc_fs_variable_list_node* n3 = (rslc_fs_variable_list_node*)(yysemantic_stack_[(3) - (3)].ntype);
			n1->add(n3);
			n3->clear();
			(yyval.ntype) = n1;
		;}
    break;

  case 10:
#line 259 "rslc/rslc_fs_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(2) - (1)].ntype);
		;}
    break;

  case 11:
#line 266 "rslc/rslc_fs_parser.yy"
    {
			rslc_fs_type_node*          n1 = (rslc_fs_type_node*)(yysemantic_stack_[(2) - (1)].ntype);
			rslc_fs_variable_list_node* n2 = (rslc_fs_variable_list_node*)(yysemantic_stack_[(2) - (2)].ntype);

			std::shared_ptr<rslc_fs_node> node(new rslc_fs_variable_list_node());
			GET_CTX()->add_node(node);
			rslc_fs_variable_list_node* n = (rslc_fs_variable_list_node*)node.get();
			rslc_fs_type type = n1->get_type();
			type.is_output = false;
			n->add_type(type);
			n->add(n2);
			n2->clear();
			(yyval.ntype) = n;
		;}
    break;

  case 12:
#line 281 "rslc/rslc_fs_parser.yy"
    {
			rslc_fs_type_node*          n2 = (rslc_fs_type_node*)(yysemantic_stack_[(3) - (2)].ntype);
			rslc_fs_variable_list_node* n3 = (rslc_fs_variable_list_node*)(yysemantic_stack_[(3) - (3)].ntype);

			std::shared_ptr<rslc_fs_node> node(new rslc_fs_variable_list_node());
			GET_CTX()->add_node(node);
			rslc_fs_variable_list_node* n = (rslc_fs_variable_list_node*)node.get();
			rslc_fs_type type = n2->get_type();
			type.is_output = true;
			n->add_type(type);
			n->add(n3);
			n3->clear();
			(yyval.ntype) = n;
		;}
    break;

  case 13:
#line 299 "rslc/rslc_fs_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(2) - (2)].ntype);
		;}
    break;

  case 14:
#line 303 "rslc/rslc_fs_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(2) - (2)].ntype);
		;}
    break;

  case 15:
#line 307 "rslc/rslc_fs_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 16:
#line 314 "rslc/rslc_fs_parser.yy"
    {
			(yyval.ntype) = (yysemantic_stack_[(1) - (1)].ntype);
		;}
    break;

  case 17:
#line 321 "rslc/rslc_fs_parser.yy"
    {
			rslc_fs_variable_node* n1 = (rslc_fs_variable_node*)(yysemantic_stack_[(1) - (1)].ntype);

			std::shared_ptr<rslc_fs_node> node(new rslc_fs_variable_list_node());
			GET_CTX()->add_node(node);
			rslc_fs_variable_list_node* n = (rslc_fs_variable_list_node*)node.get();
			n->add_name(n1->get_name());

			(yyval.ntype) = n;
		;}
    break;

  case 18:
#line 332 "rslc/rslc_fs_parser.yy"
    {
			rslc_fs_variable_list_node* n1 = (rslc_fs_variable_list_node*)(yysemantic_stack_[(3) - (1)].ntype);
			rslc_fs_variable_node*      n3 = (rslc_fs_variable_node*)(yysemantic_stack_[(3) - (3)].ntype);

			n1->add_name(n3->get_name());

			(yyval.ntype) = n1;
		;}
    break;

  case 19:
#line 341 "rslc/rslc_fs_parser.yy"
    {
			rslc_fs_variable_list_node* n1 = (rslc_fs_variable_list_node*)(yysemantic_stack_[(4) - (1)].ntype);
			rslc_fs_type_node*          n3 = (rslc_fs_type_node*)(yysemantic_stack_[(4) - (3)].ntype);
			rslc_fs_variable_node*      n4 = (rslc_fs_variable_node*)(yysemantic_stack_[(4) - (4)].ntype);

			n1->add_type_name(n3->get_type(), n4->get_name());

			(yyval.ntype) = n1;
		;}
    break;

  case 20:
#line 351 "rslc/rslc_fs_parser.yy"
    {
			rslc_fs_variable_list_node* n1 = (rslc_fs_variable_list_node*)(yysemantic_stack_[(5) - (1)].ntype);
			rslc_fs_type_node*          n4 = (rslc_fs_type_node*)(yysemantic_stack_[(5) - (4)].ntype);
			rslc_fs_variable_node*      n5 = (rslc_fs_variable_node*)(yysemantic_stack_[(5) - (5)].ntype);

			n1->add_type_name(n4->get_type(), n5->get_name());

			(yyval.ntype) = n1;
		;}
    break;

  case 21:
#line 364 "rslc/rslc_fs_parser.yy"
    {
			std::string s = (yysemantic_stack_[(1) - (1)].stype);
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_variable_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
			release_value((yysemantic_stack_[(1) - (1)].stype));
		;}
    break;

  case 22:
#line 372 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "...";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_variable_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 23:
#line 382 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "float";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 24:
#line 389 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "string";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 25:
#line 396 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "color";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 26:
#line 403 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "point";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 27:
#line 410 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "vector";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 28:
#line 417 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "normal";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 29:
#line 424 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "matrix";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 30:
#line 431 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "void";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 31:
#line 438 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "array<float>";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 32:
#line 445 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "array<float>";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 33:
#line 452 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "array_string";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 34:
#line 459 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "array<string>";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 35:
#line 466 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "type";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;

  case 36:
#line 473 "rslc/rslc_fs_parser.yy"
    {
			std::string s = "ptype";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			(yyval.ntype) = node.get();
		;}
    break;


    /* Line 675 of lalr1.cc.  */
#line 790 "rslc/rslc_fs_parser.cpp"
	default: break;
      }
    YY_SYMBOL_PRINT ("-> $$ =", yyr1_[yyn], &yyval, &yyloc);

    yypop_ (yylen);
    yylen = 0;
    YY_STACK_PRINT ();

    yysemantic_stack_.push (yyval);
    yylocation_stack_.push (yyloc);

    /* Shift the result of the reduction.  */
    yyn = yyr1_[yyn];
    yystate = yypgoto_[yyn - yyntokens_] + yystate_stack_[0];
    if (0 <= yystate && yystate <= yylast_
	&& yycheck_[yystate] == yystate_stack_[0])
      yystate = yytable_[yystate];
    else
      yystate = yydefgoto_[yyn - yyntokens_];
    goto yynewstate;

  /*------------------------------------.
  | yyerrlab -- here on detecting error |
  `------------------------------------*/
  yyerrlab:
    /* If not already recovering from an error, report this error.  */
    if (!yyerrstatus_)
      {
	++yynerrs_;
	error (yylloc, yysyntax_error_ (yystate, yytoken));
      }

    yyerror_range[0] = yylloc;
    if (yyerrstatus_ == 3)
      {
	/* If just tried and failed to reuse look-ahead token after an
	 error, discard it.  */

	if (yychar <= yyeof_)
	  {
	  /* Return failure if at end of input.  */
	  if (yychar == yyeof_)
	    YYABORT;
	  }
	else
	  {
	    yydestruct_ ("Error: discarding", yytoken, &yylval, &yylloc);
	    yychar = yyempty_;
	  }
      }

    /* Else will try to reuse look-ahead token after shifting the error
       token.  */
    goto yyerrlab1;


  /*---------------------------------------------------.
  | yyerrorlab -- error raised explicitly by YYERROR.  |
  `---------------------------------------------------*/
  yyerrorlab:

    /* Pacify compilers like GCC when the user code never invokes
       YYERROR and the label yyerrorlab therefore never appears in user
       code.  */
    if (false)
      goto yyerrorlab;

    yyerror_range[0] = yylocation_stack_[yylen - 1];
    /* Do not reclaim the symbols of the rule which action triggered
       this YYERROR.  */
    yypop_ (yylen);
    yylen = 0;
    yystate = yystate_stack_[0];
    goto yyerrlab1;

  /*-------------------------------------------------------------.
  | yyerrlab1 -- common code for both syntax error and YYERROR.  |
  `-------------------------------------------------------------*/
  yyerrlab1:
    yyerrstatus_ = 3;	/* Each real token shifted decrements this.  */

    for (;;)
      {
	yyn = yypact_[yystate];
	if (yyn != yypact_ninf_)
	{
	  yyn += yyterror_;
	  if (0 <= yyn && yyn <= yylast_ && yycheck_[yyn] == yyterror_)
	    {
	      yyn = yytable_[yyn];
	      if (0 < yyn)
		break;
	    }
	}

	/* Pop the current state because it cannot handle the error token.  */
	if (yystate_stack_.height () == 1)
	YYABORT;

	yyerror_range[0] = yylocation_stack_[0];
	yydestruct_ ("Error: popping",
		     yystos_[yystate],
		     &yysemantic_stack_[0], &yylocation_stack_[0]);
	yypop_ ();
	yystate = yystate_stack_[0];
	YY_STACK_PRINT ();
      }

    if (yyn == yyfinal_)
      goto yyacceptlab;

    yyerror_range[1] = yylloc;
    // Using YYLLOC is tempting, but would change the location of
    // the look-ahead.  YYLOC is available though.
    YYLLOC_DEFAULT (yyloc, (yyerror_range - 1), 2);
    yysemantic_stack_.push (yylval);
    yylocation_stack_.push (yyloc);

    /* Shift the error token.  */
    YY_SYMBOL_PRINT ("Shifting", yystos_[yyn],
		   &yysemantic_stack_[0], &yylocation_stack_[0]);

    yystate = yyn;
    goto yynewstate;

    /* Accept.  */
  yyacceptlab:
    yyresult = 0;
    goto yyreturn;

    /* Abort.  */
  yyabortlab:
    yyresult = 1;
    goto yyreturn;

  yyreturn:
    if (yychar != yyeof_ && yychar != yyempty_)
      yydestruct_ ("Cleanup: discarding lookahead", yytoken, &yylval, &yylloc);

    /* Do not reclaim the symbols of the rule which action triggered
       this YYABORT or YYACCEPT.  */
    yypop_ (yylen);
    while (yystate_stack_.height () != 1)
      {
	yydestruct_ ("Cleanup: popping",
		   yystos_[yystate_stack_[0]],
		   &yysemantic_stack_[0],
		   &yylocation_stack_[0]);
	yypop_ ();
      }

    return yyresult;
  }

  // Generate an error message.
  std::string
  rslc_fs_parser::yysyntax_error_ (int yystate, int tok)
  {
    std::string res;
    YYUSE (yystate);
#if YYERROR_VERBOSE
    int yyn = yypact_[yystate];
    if (yypact_ninf_ < yyn && yyn <= yylast_)
      {
	/* Start YYX at -YYN if negative to avoid negative indexes in
	   YYCHECK.  */
	int yyxbegin = yyn < 0 ? -yyn : 0;

	/* Stay within bounds of both yycheck and yytname.  */
	int yychecklim = yylast_ - yyn + 1;
	int yyxend = yychecklim < yyntokens_ ? yychecklim : yyntokens_;
	int count = 0;
	for (int x = yyxbegin; x < yyxend; ++x)
	  if (yycheck_[x + yyn] == x && x != yyterror_)
	    ++count;

	// FIXME: This method of building the message is not compatible
	// with internationalization.  It should work like yacc.c does it.
	// That is, first build a string that looks like this:
	// "syntax error, unexpected %s or %s or %s"
	// Then, invoke YY_ on this string.
	// Finally, use the string as a format to output
	// yytname_[tok], etc.
	// Until this gets fixed, this message appears in English only.
	res = "syntax error, unexpected ";
	res += yytnamerr_ (yytname_[tok]);
	if (count < 5)
	  {
	    count = 0;
	    for (int x = yyxbegin; x < yyxend; ++x)
	      if (yycheck_[x + yyn] == x && x != yyterror_)
		{
		  res += (!count++) ? ", expecting " : " or ";
		  res += yytnamerr_ (yytname_[x]);
		}
	  }
      }
    else
#endif
      res = YY_("syntax error");
    return res;
  }


  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
  const signed char rslc_fs_parser::yypact_ninf_ = -47;
  const signed char
  rslc_fs_parser::yypact_[] =
  {
        97,   -47,   -47,   -47,   -47,   -47,   -47,   -47,   -47,   -47,
     -47,   -32,   -47,   -47,    24,    97,   -47,   -47,    27,   -47,
      16,   -47,   -47,   -43,   -24,   -22,    -4,   -47,   -47,    97,
      97,    81,   -41,   -46,   -47,    15,   -47,   -47,   -47,    15,
     -47,   -40,    64,   -47,   -47,   -38,   -47,   -38,   -47,   -47,
      41,    81,    15,   -47,    15,   -47,   -47
  };

  /* YYDEFACT[S] -- default rule to reduce with in state S when YYTABLE
     doesn't specify something else to do.  Zero means the default is an
     error.  */
  const unsigned char
  rslc_fs_parser::yydefact_[] =
  {
         0,    23,    25,    26,    27,    28,    29,    24,    30,    31,
      33,     0,    35,    36,     0,     2,     3,     5,     0,    16,
       0,     1,     4,     0,     0,     0,     0,    32,    34,     0,
       0,     0,     0,     0,     8,     0,    15,    13,    14,     0,
       7,     0,    10,    21,    22,    11,    17,    12,     6,     9,
       0,     0,     0,    18,     0,    19,    20
  };

  /* YYPGOTO[NTERM-NUM].  */
  const signed char
  rslc_fs_parser::yypgoto_[] =
  {
       -47,   -47,   -47,    25,   -47,   -47,    -3,   -14,   -47,     2,
     -31,    13
  };

  /* YYDEFGOTO[NTERM-NUM].  */
  const signed char
  rslc_fs_parser::yydefgoto_[] =
  {
        -1,    14,    15,    16,    17,    33,    34,    35,    18,    45,
      46,    36
  };

  /* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule which
     number is the opposite.  If zero, do what YYDEFACT says.  */
  const signed char rslc_fs_parser::yytable_ninf_ = -1;
  const unsigned char
  rslc_fs_parser::yytable_[] =
  {
         1,     2,     3,     4,     5,     6,     7,     8,     9,    10,
      11,    12,    13,    19,    29,    30,    31,    39,    43,    53,
      24,    55,    20,    56,    21,    41,    25,    26,    19,    42,
      23,    27,    44,    28,    40,    48,    52,    54,    50,    49,
      22,    47,    37,    38,    43,     1,     2,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    44,    29,
      30,    51,     0,     0,     0,     0,     0,    32,     1,     2,
       3,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      13,     0,    29,    30,    31,     1,     2,     3,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    13,     0,    29,
      30,     1,     2,     3,     4,     5,     6,     7,     8,     9,
      10,    11,    12,    13
  };

  /* YYCHECK.  */
  const signed char
  rslc_fs_parser::yycheck_[] =
  {
         4,     5,     6,     7,     8,     9,    10,    11,    12,    13,
      14,    15,    16,     0,    18,    19,    20,    31,     3,    50,
       4,    52,    54,    54,     0,    71,    10,    70,    15,    75,
       3,    55,    17,    55,    75,    75,    50,    51,    76,    42,
      15,    39,    29,    30,     3,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    17,    18,
      19,    20,    -1,    -1,    -1,    -1,    -1,    71,     4,     5,
       6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
      16,    -1,    18,    19,    20,     4,     5,     6,     7,     8,
       9,    10,    11,    12,    13,    14,    15,    16,    -1,    18,
      19,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    16
  };

  /* STOS_[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
  const unsigned char
  rslc_fs_parser::yystos_[] =
  {
         0,     4,     5,     6,     7,     8,     9,    10,    11,    12,
      13,    14,    15,    16,    78,    79,    80,    81,    85,    88,
      54,     0,    80,     3,     4,    10,    70,    55,    55,    18,
      19,    20,    71,    82,    83,    84,    88,    88,    88,    84,
      75,    71,    75,     3,    17,    86,    87,    86,    75,    83,
      76,    20,    84,    87,    84,    87,    87
  };

#if YYDEBUG
  /* TOKEN_NUMBER_[YYLEX-NUM] -- Internal symbol number corresponding
     to YYLEX-NUM.  */
  const unsigned short int
  rslc_fs_parser::yytoken_number_[] =
  {
         0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,    61,   302,   303,
     304,   305,   306,   307,    60,    62,   308,   309,   310,   311,
      63,    58,    43,    45,    94,    47,    42,    46,    33,   312,
      40,    41,   313,   314,   315,    59,    44
  };
#endif

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
  const unsigned char
  rslc_fs_parser::yyr1_[] =
  {
         0,    77,    78,    79,    79,    80,    81,    81,    82,    82,
      82,    83,    83,    84,    84,    84,    85,    86,    86,    86,
      86,    87,    87,    88,    88,    88,    88,    88,    88,    88,
      88,    88,    88,    88,    88,    88,    88
  };

  /* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
  const unsigned char
  rslc_fs_parser::yyr2_[] =
  {
         0,     2,     1,     1,     2,     1,     6,     5,     1,     3,
       2,     2,     3,     2,     2,     1,     1,     1,     3,     4,
       5,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     4,     1,     4,     1,     1
  };

#if YYDEBUG || YYERROR_VERBOSE || YYTOKEN_TABLE
  /* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
     First, the terminals, then, starting at \a yyntokens_, nonterminals.  */
  const char*
  const rslc_fs_parser::yytname_[] =
  {
    "$end", "error", "$undefined", "IDENTIFIER", "TOKEN_FLOAT",
  "TOKEN_COLOR", "TOKEN_POINT", "TOKEN_VECTOR", "TOKEN_NORMAL",
  "TOKEN_MATRIX", "TOKEN_STRING", "TOKEN_VOID", "TOKEN_FARRAY",
  "TOKEN_SARRAY", "TOKEN_ARRAY", "TOKEN_TYPE", "TOKEN_PTYPE",
  "TOKEN_TRIPLEDOT", "TOKEN_UNIFORM", "TOKEN_VARYING", "TOKEN_OUTPUT",
  "TOKEN_EXTERN", "TOKEN_SURFACE", "TOKEN_LIGHT", "TOKEN_ATMOSPHERE",
  "TOKEN_VOLUME", "TOKEN_DISPLACEMENT", "TOKEN_IMAGER",
  "TOKEN_TRANSFORMATION", "TOKEN_IF", "TOKEN_ELSE", "TOKEN_WHILE",
  "TOKEN_FOR", "TOKEN_CONTINUE", "TOKEN_BREAK", "TOKEN_RETURN",
  "TOKEN_ILLUMINATE", "TOKEN_ILLUMINANCE", "TOKEN_SOLAR", "TOKEN_GATHER",
  "TOKEN_OCCLUSION", "TOKEN_LIGHTSOURCE", "TOKEN_INCIDENT",
  "TOKEN_OPPOSITE", "TOKEN_ATTRIBUTE", "TOKEN_OPTION",
  "TOKEN_RENDERERINFO", "'='", "TOKEN_ADD_ASSIGN", "TOKEN_SUB_ASSIGN",
  "TOKEN_MUL_ASSIGN", "TOKEN_DIV_ASSIGN", "TOKEN_OR", "TOKEN_AND", "'<'",
  "'>'", "TOKEN_LE", "TOKEN_GE", "TOKEN_EQ", "TOKEN_NE", "'?'", "':'",
  "'+'", "'-'", "'^'", "'/'", "'*'", "'.'", "'!'", "NEG", "'('", "')'",
  "FLOAT_LITERAL", "INTEGER_LITERAL", "STRING_LITERAL", "';'", "','",
  "$accept", "file", "definition_list", "definition",
  "function_definition", "argument_definition_list",
  "argument_variable_definition_list", "argument_type", "function_type",
  "variable_name_list", "variable_name", "type", 0
  };
#endif

#if YYDEBUG
  /* YYRHS -- A `-1'-separated list of the rules' RHS.  */
  const rslc_fs_parser::rhs_number_type
  rslc_fs_parser::yyrhs_[] =
  {
        78,     0,    -1,    79,    -1,    80,    -1,    79,    80,    -1,
      81,    -1,    85,     3,    70,    82,    71,    75,    -1,    85,
       3,    70,    71,    75,    -1,    83,    -1,    82,    75,    83,
      -1,    82,    75,    -1,    84,    86,    -1,    20,    84,    86,
      -1,    18,    88,    -1,    19,    88,    -1,    88,    -1,    88,
      -1,    87,    -1,    86,    76,    87,    -1,    86,    76,    84,
      87,    -1,    86,    76,    20,    84,    87,    -1,     3,    -1,
      17,    -1,     4,    -1,    10,    -1,     5,    -1,     6,    -1,
       7,    -1,     8,    -1,     9,    -1,    11,    -1,    12,    -1,
      14,    54,     4,    55,    -1,    13,    -1,    14,    54,    10,
      55,    -1,    15,    -1,    16,    -1
  };

  /* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
     YYRHS.  */
  const unsigned char
  rslc_fs_parser::yyprhs_[] =
  {
         0,     0,     3,     5,     7,    10,    12,    19,    25,    27,
      31,    34,    37,    41,    44,    47,    49,    51,    53,    57,
      62,    68,    70,    72,    74,    76,    78,    80,    82,    84,
      86,    88,    90,    95,    97,   102,   104
  };

  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
  const unsigned short int
  rslc_fs_parser::yyrline_[] =
  {
         0,   183,   183,   189,   198,   208,   215,   230,   246,   250,
     258,   265,   280,   298,   302,   306,   313,   320,   331,   340,
     350,   363,   371,   381,   388,   395,   402,   409,   416,   423,
     430,   437,   444,   451,   458,   465,   472
  };

  // Print the state stack on the debug stream.
  void
  rslc_fs_parser::yystack_print_ ()
  {
    *yycdebug_ << "Stack now";
    for (state_stack_type::const_iterator i = yystate_stack_.begin ();
	 i != yystate_stack_.end (); ++i)
      *yycdebug_ << ' ' << *i;
    *yycdebug_ << std::endl;
  }

  // Report on the debug stream that the rule \a yyrule is going to be reduced.
  void
  rslc_fs_parser::yy_reduce_print_ (int yyrule)
  {
    unsigned int yylno = yyrline_[yyrule];
    int yynrhs = yyr2_[yyrule];
    /* Print the symbols being reduced, and their result.  */
    *yycdebug_ << "Reducing stack by rule " << yyrule - 1
	       << " (line " << yylno << "), ";
    /* The symbols being reduced.  */
    for (int yyi = 0; yyi < yynrhs; yyi++)
      YY_SYMBOL_PRINT ("   $" << yyi + 1 << " =",
		       yyrhs_[yyprhs_[yyrule] + yyi],
		       &(yysemantic_stack_[(yynrhs) - (yyi + 1)]),
		       &(yylocation_stack_[(yynrhs) - (yyi + 1)]));
  }
#endif // YYDEBUG

  /* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
  rslc_fs_parser::token_number_type
  rslc_fs_parser::yytranslate_ (int t)
  {
    static
    const token_number_type
    translate_table[] =
    {
           0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,    68,     2,     2,     2,     2,     2,     2,
      70,    71,    66,    62,    76,    63,    67,    65,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,    61,    75,
      54,    47,    55,    60,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,    64,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    48,    49,    50,    51,    52,    53,    56,    57,
      58,    59,    69,    72,    73,    74
    };
    if ((unsigned int) t <= yyuser_token_number_max_)
      return translate_table[t];
    else
      return yyundef_token_;
  }

  const int rslc_fs_parser::yyeof_ = 0;
  const int rslc_fs_parser::yylast_ = 113;
  const int rslc_fs_parser::yynnts_ = 12;
  const int rslc_fs_parser::yyempty_ = -2;
  const int rslc_fs_parser::yyfinal_ = 21;
  const int rslc_fs_parser::yyterror_ = 1;
  const int rslc_fs_parser::yyerrcode_ = 256;
  const int rslc_fs_parser::yyntokens_ = 77;

  const unsigned int rslc_fs_parser::yyuser_token_number_max_ = 315;
  const rslc_fs_parser::token_number_type rslc_fs_parser::yyundef_token_ = 2;

} // namespace yy

#line 481 "rslc/rslc_fs_parser.yy"


void yy::rslc_fs_parser::error (const location_type& loc, const std::string& msg)
{
	std::string fname = GET_FNAME();
	int lineno = GET_LINE();
	fprintf(stderr, "parser error %s :%s(%d)\n", msg.c_str(), fname.c_str(), lineno);
}


extern int yylex_init(void** p);
extern int yylex_destroy(void* p);

namespace rslc
{
	int rslc_fs_load(FILE* in, rslc_fs_context* ctx)
	{
		int nRet = 0;
		void* scanner;
		yylex_init(&scanner);

		rslc_fs_decoder dec(in);

		rslc_fs_parse_param* param = (rslc_fs_parse_param*)(scanner);
		param->ctx = ctx;
		param->dec = &dec;
		param->line_number = 1;
		param->filename = ctx->get_file_name();

		yy::rslc_fs_parser parser(scanner);

		nRet = parser.parse();
		
		yylex_destroy(scanner);

		return nRet;
	}
}

