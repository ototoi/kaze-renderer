%skeleton "lalr1.cc"
%define "parser_class_name" "rslc_fs_parser"
%defines

%{
#ifdef _WIN32
#pragma warning(disable : 4786)
#pragma warning(disable : 4102)
#pragma warning(disable : 4996)
#pragma warning(disable : 4065)
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define YYDEBUG 1
#define YYERROR_VERBOSE 1

#include <algorithm>
#include <iterator>
#include <memory>

#include "rslc_fs_parse_param.h"
#include "rslc_fs_context.h"
#include "rslc_fs_node.h"

%}

%union{
	int itype;
	float ftype;
	char ctype;
	char* stype;
	rslc::rslc_fs_node* ntype;
}

%pure_parser


// The parsing context.
%parse-param { void * scanner }
%lex-param   { void * scanner }

%locations
// %debug
%error-verbose

%{

#define yyerror(m) error(yylloc,m)

using namespace rslc;


#define GET_CTX() (((rslc_fs_parse_param*)(scanner))->ctx)
#define GET_LINE() (((rslc_fs_parse_param*)(scanner))->line_number)
#define GET_FNAME() (((rslc_fs_parse_param*)(scanner))->filename)
#define PRINT(s) printf(s)

typedef char* LPCSTR;

static
void release_value(LPCSTR v){delete[] v;v=NULL;}



#define	YY_DECL											\
	int						\
	yylex(yy::rslc_fs_parser::semantic_type* yylval,		\
		 yy::rslc_fs_parser::location_type* yylloc,		\
		 void* scanner)

YY_DECL;


%}

/*tokens*/

%token <stype>	IDENTIFIER

/*types*/
%token <ctype> TOKEN_FLOAT
%token <ctype> TOKEN_COLOR
%token <ctype> TOKEN_POINT
%token <ctype> TOKEN_VECTOR
%token <ctype> TOKEN_NORMAL
%token <ctype> TOKEN_MATRIX
%token <ctype> TOKEN_STRING
%token <ctype> TOKEN_VOID
%token <ctype> TOKEN_FARRAY
%token <ctype> TOKEN_SARRAY
%token <ctype> TOKEN_ARRAY
%token <ctype> TOKEN_TYPE
%token <ctype> TOKEN_PTYPE

%token <ctype> TOKEN_TRIPLEDOT

/*strage class*/
%token <ctype> TOKEN_UNIFORM
%token <ctype> TOKEN_VARYING

%token <ctype> TOKEN_OUTPUT
%token <ctype> TOKEN_EXTERN

/*shader type*/
%token <ctype> TOKEN_SURFACE
%token <ctype> TOKEN_LIGHT
%token <ctype> TOKEN_ATMOSPHERE
%token <ctype> TOKEN_VOLUME
%token <ctype> TOKEN_DISPLACEMENT
%token <ctype> TOKEN_IMAGER
%token <ctype> TOKEN_TRANSFORMATION

/*block statement constructs*/
%token <ctype> TOKEN_IF
%token <ctype> TOKEN_ELSE
%token <ctype> TOKEN_WHILE
%token <ctype> TOKEN_FOR
%token <ctype> TOKEN_CONTINUE
%token <ctype> TOKEN_BREAK
%token <ctype> TOKEN_RETURN

%token <ctype> TOKEN_ILLUMINATE
%token <ctype> TOKEN_ILLUMINANCE
%token <ctype> TOKEN_SOLAR
%token <ctype> TOKEN_GATHER
%token <ctype> TOKEN_OCCLUSION

%token <ctype> TOKEN_LIGHTSOURCE
%token <ctype> TOKEN_INCIDENT
%token <ctype> TOKEN_OPPOSITE
%token <ctype> TOKEN_ATTRIBUTE
%token <ctype> TOKEN_OPTION
%token <ctype> TOKEN_RENDERERINFO


/* NOTE: These are priorities in ascending precedence, operators on the same line have the same precedence. */
%right <ctype> '='
%right <ctype> TOKEN_ADD_ASSIGN
%right <ctype> TOKEN_SUB_ASSIGN
%right <ctype> TOKEN_MUL_ASSIGN
%right <ctype> TOKEN_DIV_ASSIGN

%left <ctype> TOKEN_OR
%left <ctype> TOKEN_AND

%left <ctype>	'<'
%left <ctype>	'>'
%left <ctype>	TOKEN_LE
%left <ctype>	TOKEN_GE
%left <ctype>	TOKEN_EQ
%left <ctype>	TOKEN_NE

%right <ctype>	'?' ':'

%left <ctype>	'+' '-'
%left <ctype>	'^'
%left <ctype>	'/' '*'
%left <ctype>	'.'
%right <ctype>	'!' NEG
%left <ctype>	'(' ')'

%token <ftype>	FLOAT_LITERAL
%token <itype>	INTEGER_LITERAL
%token <stype>	STRING_LITERAL

%type  <ntype>	definition_list
%type  <ntype>	definition
%type  <ntype>	function_definition
%type  <ntype>	function_type
%type  <ntype>	argument_definition_list
%type  <ntype>	argument_variable_definition_list
%type  <ntype>	argument_type
%type  <ntype>	variable_name_list
%type  <ntype>	variable_name
%type  <ntype>	type

%start file
%%

file
	: definition_list
		{
			GET_CTX()->set_final_node($1);
		}

definition_list
	: definition
		{
			rslc_fs_function_node* n1 = (rslc_fs_function_node*)$1;
            std::shared_ptr<rslc_fs_node> node(new rslc_fs_function_list_node());
			GET_CTX()->add_node(node);
			rslc_fs_function_list_node* n = (rslc_fs_function_list_node*)node.get();
			n->add(n1);
			$$ = n;
		}
	| definition_list definition
		{
			rslc_fs_function_list_node* n1 = (rslc_fs_function_list_node*)$1;
            rslc_fs_function_node* n2 = (rslc_fs_function_node*)$2;
			n1->add(n2);
			$$ = n1;
		}
	;

definition
	:	function_definition
		{
			$$ = $1;
		}
	;

function_definition
	:	function_type IDENTIFIER '(' argument_definition_list ')' ';'
		{
			rslc_fs_type_node*          n1 = (rslc_fs_type_node*)$1;
			rslc_fs_variable_list_node* n4 = (rslc_fs_variable_list_node*)$4;

			rslc_fs_type       type = n1->get_type();
			rslc_fs_identifier name = $2;
			const std::vector<rslc_fs_variable_list>& args = n4->get_list();
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_function_node(type, name, args));
			GET_CTX()->add_node(node);
			
			$$ = node.get();

			release_value($2);
		}
	|	function_type IDENTIFIER '(' ')' ';'
		{
			rslc_fs_type_node*        n1 = (rslc_fs_type_node*)$1;
			rslc_fs_type       type = n1->get_type();
			rslc_fs_identifier name = $2;
			std::vector<rslc_fs_variable_list> args; // dummy
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_function_node(type, name, args));
			GET_CTX()->add_node(node);
			
			$$ = node.get();

			release_value($2);
		}
	;

argument_definition_list
	:	argument_variable_definition_list
		{
			$$ = $1;
		}
	|	argument_definition_list ';' argument_variable_definition_list
		{
			rslc_fs_variable_list_node* n1 = (rslc_fs_variable_list_node*)$1;
			rslc_fs_variable_list_node* n3 = (rslc_fs_variable_list_node*)$3;
			n1->add(n3);
			n3->clear();
			$$ = n1;
		}
	|	argument_definition_list ';'
		{
			$$ = $1;
		}
	;

argument_variable_definition_list
	:	argument_type variable_name_list
		{
			rslc_fs_type_node*          n1 = (rslc_fs_type_node*)$1;
			rslc_fs_variable_list_node* n2 = (rslc_fs_variable_list_node*)$2;

			std::shared_ptr<rslc_fs_node> node(new rslc_fs_variable_list_node());
			GET_CTX()->add_node(node);
			rslc_fs_variable_list_node* n = (rslc_fs_variable_list_node*)node.get();
			rslc_fs_type type = n1->get_type();
			type.is_output = false;
			n->add_type(type);
			n->add(n2);
			n2->clear();
			$$ = n;
		}
	|	TOKEN_OUTPUT argument_type variable_name_list
		{
			rslc_fs_type_node*          n2 = (rslc_fs_type_node*)$2;
			rslc_fs_variable_list_node* n3 = (rslc_fs_variable_list_node*)$3;

			std::shared_ptr<rslc_fs_node> node(new rslc_fs_variable_list_node());
			GET_CTX()->add_node(node);
			rslc_fs_variable_list_node* n = (rslc_fs_variable_list_node*)node.get();
			rslc_fs_type type = n2->get_type();
			type.is_output = true;
			n->add_type(type);
			n->add(n3);
			n3->clear();
			$$ = n;
		}
	;

argument_type
	:	TOKEN_UNIFORM type
		{
			$$ = $2;
		}
    |	TOKEN_VARYING type
		{
			$$ = $2;
		}
	|	type
		{
			$$ = $1;
		}
	;

function_type
	:	type
		{
			$$ = $1;
		}
	;

variable_name_list
	:	variable_name		
		{
			rslc_fs_variable_node* n1 = (rslc_fs_variable_node*)$1;

			std::shared_ptr<rslc_fs_node> node(new rslc_fs_variable_list_node());
			GET_CTX()->add_node(node);
			rslc_fs_variable_list_node* n = (rslc_fs_variable_list_node*)node.get();
			n->add_name(n1->get_name());

			$$ = n;
		}
	|	variable_name_list ',' variable_name
		{
			rslc_fs_variable_list_node* n1 = (rslc_fs_variable_list_node*)$1;
			rslc_fs_variable_node*      n3 = (rslc_fs_variable_node*)$3;

			n1->add_name(n3->get_name());

			$$ = n1;
		}
	|	variable_name_list ',' argument_type variable_name
		{
			rslc_fs_variable_list_node* n1 = (rslc_fs_variable_list_node*)$1;
			rslc_fs_type_node*          n3 = (rslc_fs_type_node*)$3;
			rslc_fs_variable_node*      n4 = (rslc_fs_variable_node*)$4;

			n1->add_type_name(n3->get_type(), n4->get_name());

			$$ = n1;
		}
	|	variable_name_list ',' TOKEN_OUTPUT argument_type variable_name
		{
			rslc_fs_variable_list_node* n1 = (rslc_fs_variable_list_node*)$1;
			rslc_fs_type_node*          n4 = (rslc_fs_type_node*)$4;
			rslc_fs_variable_node*      n5 = (rslc_fs_variable_node*)$5;

			n1->add_type_name(n4->get_type(), n5->get_name());

			$$ = n1;
		}
	;

variable_name
    :	IDENTIFIER
		{
			std::string s = $1;
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_variable_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
			release_value($1);
		}
	|	TOKEN_TRIPLEDOT
		{
			std::string s = "...";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_variable_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	;

type
	:	TOKEN_FLOAT
		{
			std::string s = "float";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_STRING
		{
			std::string s = "string";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_COLOR
		{
			std::string s = "color";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_POINT
		{
			std::string s = "point";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_VECTOR
		{
			std::string s = "vector";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_NORMAL
		{
			std::string s = "normal";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_MATRIX
		{
			std::string s = "matrix";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_VOID
		{
			std::string s = "void";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_FARRAY
		{
			std::string s = "array<float>";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_ARRAY '<' TOKEN_FLOAT '>'
		{
			std::string s = "array<float>";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_SARRAY
		{
			std::string s = "array_string";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_ARRAY '<' TOKEN_STRING '>'
		{
			std::string s = "array<string>";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_TYPE
		{
			std::string s = "type";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	|	TOKEN_PTYPE
		{
			std::string s = "ptype";
			std::shared_ptr<rslc_fs_node> node(new rslc_fs_type_node(s));
			GET_CTX()->add_node(node);
			$$ = node.get();
		}
	;

%%

void yy::rslc_fs_parser::error (const location_type& loc, const std::string& msg)
{
	std::string fname = GET_FNAME();
	int lineno = GET_LINE();
	fprintf(stderr, "parser error %s :%s(%d)\n", msg.c_str(), fname.c_str(), lineno);
}


extern int yylex_init(void** p);
extern int yylex_destroy(void* p);

namespace rslc
{
	int rslc_fs_load(FILE* in, rslc_fs_context* ctx)
	{
		int nRet = 0;
		void* scanner;
		yylex_init(&scanner);

		rslc_fs_decoder dec(in);

		rslc_fs_parse_param* param = (rslc_fs_parse_param*)(scanner);
		param->ctx = ctx;
		param->dec = &dec;
		param->line_number = 1;
		param->filename = ctx->get_file_name();

		yy::rslc_fs_parser parser(scanner);

		nRet = parser.parse();
		
		yylex_destroy(scanner);

		return nRet;
	}
}
