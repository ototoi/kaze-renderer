#ifndef rslc_fs_CONTEXT_H
#define rslc_fs_CONTEXT_H

#include <string>
#include <vector>
#include <iostream>

#include "rslc_fs_node.h"

namespace rslc
{
    class rslc_fs_context
    {
    public:
        rslc_fs_context()
            :fnode_(NULL)
        {
            ;//
        }
    public:
        void set_final_node(const rslc_fs_node* n)
        {
            fnode_ = n;
        }
        const rslc_fs_node* get_final_node()
        {
            return fnode_;
        }
    public:
        void add_node(const std::shared_ptr<rslc_fs_node>& node)
        {
            nodes_.push_back(node);
        }
    public:
        void set_file_name(const std::string& s)
        {
            filename_ = s;
        }
        const std::string& get_file_name()const
        {
            return filename_;
        }
    private:
        std::vector< std::shared_ptr<rslc_fs_node> > nodes_;
        const rslc_fs_node* fnode_;
        std::string filename_;
    };
}

#endif