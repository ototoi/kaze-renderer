#ifndef RSLC_FS_TYPES_H
#define RSLC_FS_TYPES_H

#include <string>
#include <vector>

namespace rslc
{
    typedef std::string rslc_fs_identifier;

    struct rslc_fs_type
    {
        rslc_fs_identifier name;
        bool is_output;
    };

    struct rslc_fs_variable_list
    {
        rslc_fs_type type;
        std::vector<rslc_fs_identifier> names;
    };

    struct rslc_fs_function
    {
        rslc_fs_function(){}
        rslc_fs_function(
            const rslc_fs_type& t,
            const rslc_fs_identifier& n,
            const std::vector<rslc_fs_variable_list>& a
        ):type(t), name(n), args(a)
        {}
        rslc_fs_type type;
        rslc_fs_identifier name;
        std::vector<rslc_fs_variable_list> args;
    };
}

#endif 