#ifndef rslc_fs_PARSE_PARAM_H
#define rslc_fs_PARSE_PARAM_H

#include <string>
#include "rslc_decoder.h"
#include "rslc_fs_context.h"

namespace rslc
{
    typedef rslc_decoder rslc_fs_decoder;

    typedef struct rslc_fs_parse_param
    {
        rslc_fs_context* ctx;
        rslc_fs_decoder* dec;

        std::string filename;
        int line_number;
    } rslc_fs_parse_param;
}

#endif
