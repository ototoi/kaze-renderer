#include "rslc_fs_convert.h"
#include "rslc_fs_context.h"
#include <stdio.h>
#include <vector>
#include <unordered_set>
#include <sstream>

//(1) , or ;
//(2) Expand args ?
  //(3) output -> &
//(4) vector, point, normal -> float3 ... and then merge
//(5) matrix -> float4x4
//(6) type -> float, point, matrix
//(7) ptype ->
//(8) ... -> array<type>
//(9) mangling ex. float sin(float x) -> float sin_f1_f1(float x)

//a) nakid
//b) C like
//c) hlsl like
//d) ksl
//e) ksl implementation code

namespace rslc
{
    using namespace std;

    static
    std::string ConvertToString(const std::vector<rslc_fs_variable_list>& args, bool bCLike = false)
    {
        std::string s;
        for(size_t i=0;i<args.size();i++)
        {
            const rslc_fs_variable_list& val = args[i];
            std::string type = val.type.name;
            s += type;
            s += " ";
            for(size_t j = 0;j<val.names.size();j++)
            {
                s += val.names[j];
                if(j+1 != val.names.size())
                {
                    s += ", ";
                }
            }
            if(i+1 != args.size())
            {
                if(!bCLike)
                {
                    s += "; ";
                }
                else
                {
                    s += ", ";
                }
            }
        }
        return s;
    }

    static
    std::vector<rslc_fs_variable_list> ExpandArgs(const std::vector<rslc_fs_variable_list>& args)
    {
        std::vector<rslc_fs_variable_list> list;
        for(size_t i=0;i<args.size();i++)
        {
            const rslc_fs_variable_list& val = args[i];
            rslc_fs_type type = val.type;
            if(val.names.size() != 1)
            {
                for(size_t j = 0;j<val.names.size();j++)
                {
                    rslc_fs_variable_list v;
                    v.type = type;
                    v.names.push_back(val.names[j]);
                    list.push_back(v);
                }
            }
            else
            {
                list.push_back(val);
            }       
        }
        return list;
    }

    static 
    bool IsIncludeType(const std::vector<rslc_fs_variable_list>& args, const std::string& type)
    {
        for(size_t i=0;i<args.size();i++)
        {
            if(args[i].type.name == type)
            {
                return true;
            }
        }
        return false;
    }

    static
    rslc_fs_function ReplaceType(const rslc_fs_function& func, const std::string& before, const std::string& after)
    {
        rslc_fs_function f = func;
        if(f.type.name == before)
        {
            f.type.name = after;
        }
        for(size_t i =0;i<f.args.size();i++)
        {
            rslc_fs_variable_list& ff = f.args[i];
            if(ff.type.name == before)
            {
                ff.type.name = after;
            }
        }
        return f;
    }

    static
    std::vector<rslc_fs_function> ExpandType(const std::vector<rslc_fs_function>& funcs)
    {
        std::vector<rslc_fs_function> list;
        for(size_t i=0;i<funcs.size();i++)
        {
            if(funcs[i].type.name == "type" || IsIncludeType(funcs[i].args, "type"))
            {
                list.push_back(ReplaceType(funcs[i], "type", "float"));
                list.push_back(ReplaceType(funcs[i], "type", "point"));
                list.push_back(ReplaceType(funcs[i], "type", "vector"));
                list.push_back(ReplaceType(funcs[i], "type", "normal"));
            }
            else if(funcs[i].type.name == "ptype" || IsIncludeType(funcs[i].args, "ptype"))
            {
                list.push_back(ReplaceType(funcs[i], "ptype", "point"));
                list.push_back(ReplaceType(funcs[i], "ptype", "vector"));
                list.push_back(ReplaceType(funcs[i], "ptype", "normal"));
            }
            else
            {
                list.push_back(funcs[i]);
            }
        }
        return list;
    }

    static
    std::vector<rslc_fs_function> ReplaceType(const std::vector<rslc_fs_function>& funcs)
    {
        typedef const char* LPCSTR;
        static const LPCSTR KEYS[] = 
        {
            "color",
            "normal",
            "point",
            NULL
        };
        std::vector<rslc_fs_function> list;
        for(size_t i=0;i<funcs.size();i++)
        {
            rslc_fs_function func = funcs[i];
            int j=0;
            while(KEYS[j])
            {
                if(func.type.name == KEYS[j] || IsIncludeType(func.args, KEYS[j]))
                {
                    func = ReplaceType(func, KEYS[j], "vector");
                }
                j++;
            }
            list.push_back(func);
        }
        return list;
    }

    static
    std::vector<rslc_fs_function> ExpandArgs(const std::vector<rslc_fs_function>& funcs)
    {
        std::vector<rslc_fs_function> list = funcs;
        for(size_t i=0;i<list.size();i++)
        {
            list[i].args = ExpandArgs(list[i].args);
        }
        return list;
    }

    static
    std::string FunctionKey(const rslc_fs_function& func)
    {
        std::string s;
        s += func.type.name;
        s += " ";
        s += func.name;
        s += " ";
        for(int i=0;i<func.args.size();i++)
        {
            s += func.args[i].type.name;
            s += " ";
        }
        return s;
    }

    static
    std::vector<rslc_fs_function> MergeFunction(const std::vector<rslc_fs_function>& funcs)
    {
        std::unordered_set<std::string> s;
        std::vector<rslc_fs_function> list;
        for(size_t i=0;i<funcs.size();i++)
        {
            std::string key = FunctionKey(funcs[i]);
            if(s.find(key) == s.end())
            {
                list.push_back(funcs[i]);
                s.insert(key);
            }
        }
        return list;
    }

    struct TokenToken 
    {
        const char* key;
        const char* value;
    };

    static const TokenToken MANGKING[] = 
    {
        {"void", "z0"},
        {"float", "f1"},
        {"vector", "f3"},
        {"matrix", "m4"},
        {"int", "i1"},
        {"string", "sx"},
        {"bound", "f6"},
        {"array_float", "fa"},
        {"array<float>", "fa"},
        {"array_string", "sa"},
        {"array<string>", "sa"},
        {NULL, NULL}
    };

    static
    std::string ManglingType(const std::string& type)
    {
        int i=0;
        while(MANGKING[i].key)
        {
            if(strcmp(MANGKING[i].key, type.c_str())==0)
            {
                return MANGKING[i].value;
            }
            i++;
        }
        return type;
    }

    static
    std::string GetManglingName(const rslc_fs_function& func)
    {
        std::string s;
        s += func.name;
        s += "_";
        s += ManglingType(func.type.name);
        s += "_";
        if(func.args.empty())
        {
            s += ManglingType("void"); 
        }
        else
        {
            for(int i=0;i<func.args.size();i++)
            {
                s += ManglingType(func.args[i].type.name);
            }
        }
        return s;
    }

    int rslc_fs_load(FILE* in, rslc_fs_context* ctx);
    int rslc_fs_convert(const char* szInput, const char* szOutPut)
    {
        int nRet = 0;
        rslc_fs_context ctx;
        ctx.set_file_name(szInput);
        {
            FILE* fp = fopen(szInput, "rt");
            if(fp == NULL)return -1;
            nRet = rslc_fs_load(fp, &ctx);
            fclose(fp);
        }

        if(nRet == 0)
        {
            FILE* fp = NULL;
            bool bFile = false;
            if(strlen(szOutPut) > 0)
            {
                fp = fopen(szOutPut, "wt");
                if(fp == NULL)return -1;
                bFile = true;
            }
            else
            {
                fp = stdout;
            }
            
            const rslc_fs_function_list_node* list = dynamic_cast<const rslc_fs_function_list_node*>(ctx.get_final_node());
            if(list != NULL)
            {
                std::vector<rslc_fs_function> funcs;
                for(size_t i = 0;i<list->size();i++)
                {
                    const rslc_fs_function_node* func = list->at(i);
                    funcs.push_back(func->get_function());
                }

                funcs = ExpandType (funcs);
                funcs = ReplaceType(funcs);
                funcs = ExpandArgs (funcs);
                funcs = MergeFunction(funcs);
#if 1
                /*
                for(size_t i = 0;i<funcs.size();i++)
                {
                    std::string type = funcs[i].type.name;
                    std::string name = funcs[i].name;
                    std::string args = ConvertToString(funcs[i].args, true);
                    std::string mname = GetManglingName(funcs[i]);
                    fprintf(fp, "{\"%s\", \"%s\"},\n", name.c_str(), mname.c_str());
                    //fprintf(fp, "%s %s (%s);\t\t\t//%s\n", type.c_str(), name.c_str(), args.c_str(), GetManglingName(funcs[i]).c_str());
                }
                */

                /*
                {
                    std::vector<rslc_variable> vars;
                    vars.push_back(rslc_variable(rslc_type("vec3"), "x"));
                    rslc_function fun(rslc_type("float"), "xcomp", vars);
                    std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );
                    sm->add_symbol(s);
                }
                */

                for(size_t i = 0;i<funcs.size();i++)
                {
                    std::string type = funcs[i].type.name;
                    std::string name = funcs[i].name;
                    std::stringstream ss;
                    ss << "{" << "\n";
                    ss << "    " << "std::vector<rslc_variable> vars;" << "\n";
                    for(int j=0;j<funcs[i].args.size();j++)
                    {
                        ss << "    " << "vars.push_back(rslc_variable(rslc_type(\"";
                        ss << funcs[i].args[j].type.name << "\"), ";
                        ss << "\"" << "x" << j << "\"" << "));\n";
                    }
                    fprintf(fp, "%s", ss.str().c_str() );
                    fprintf(fp, "    rslc_function fun(rslc_type(\"%s\"), \"%s\", vars);\n", type.c_str(), name.c_str()  );
                    fprintf(fp, "    %s", "std::shared_ptr<rslc_symbol> s( new rslc_symbol(fun) );\n" );
                    fprintf(fp, "    %s", "sm->add_symbol(s);\n" );
                    fprintf(fp, "%s", "}\n" );

                    //fprintf(fp, )
                    //fprintf(fp, "{\"%s\", \"%s\"},\n", name.c_str(), mname.c_str());
                    //fprintf(fp, "%s %s (%s);\t\t\t//%s\n", type.c_str(), name.c_str(), args.c_str(), GetManglingName(funcs[i]).c_str());
                }

#else
                {
                    for(size_t i = 0;i<funcs.size();i++)
                    {
                        std::string type = funcs[i].type.name;
                        std::string name = funcs[i].name;
                        std::string args = ConvertToString(funcs[i].args, true);
                        std::string mname = GetManglingName(funcs[i]);

                        fprintf(fp, "//%s %s(%s);\n", type.c_str(), name.c_str(), args.c_str());
                        fprintf(fp, "static int ksl_shader_input_%s(lua_State* L)\n", mname.c_str());
                        fprintf(fp, "{\n");
                        fprintf(fp, "    const ksl_shader_input* self = (const ksl_shader_input*)lua_touserdata(L, 1);\n");
                        for(int j=0;j<funcs[i].args.size();j++)
                        {
                            std::string type = funcs[i].args[j].type.name;
                            if(type == "float")
                            {
                                fprintf(fp, "    const double       a%d = lua_tonumber(L, %d);\n", j+1, j+2);
                            }
                            else if(type == "vector")
                            {
                                fprintf(fp, "    const ksl_vector&  a%d = *((const ksl_vector*)lua_touserdata(L, %d));\n", j+1, j+2);
                            }
                            else if(type == "string")
                            {
                                fprintf(fp, "    const std::string  a%d = lua_tostring(L, %d);\n", j+1, j+2);//todo
                            }
                            else if(type == "matrix")
                            {
                                fprintf(fp, "    const ksl_matrix&  a%d = *((const ksl_matrix*)lua_touserdata(L, %d));\n", j+1, j+2);//todo
                            }
                            else if(type == "array<float>")
                            {
                                fprintf(fp, "    int sz = lua_rawlen(L, %d);\n", j+2);
                                fprintf(fp, "    std::vector<double> a%d(sz);\n", j+1);
                                fprintf(fp, "    for(int i_=1;i_<=sz;i_++)\n");
                                fprintf(fp, "    {\n");
                                fprintf(fp, "        lua_pushinteger(L, i_);\n");
                                fprintf(fp, "        lua_gettable(L, %d);\n", j+2);
                                fprintf(fp, "        a%d[i_-1]=lua_tonumber(L, -1);\n", j+1);
                                fprintf(fp, "        lua_pop(L, 1);\n");
                                fprintf(fp, "    }\n");
                            }
                            else if(type == "array<string>")
                            {
                                fprintf(fp, "    int sz = lua_rawlen(L, %d);\n", j+2);
                                fprintf(fp, "    std::vector<std::string> a%d(sz);\n", j+1);
                                fprintf(fp, "    for(int i_=1;i_<=sz;i_++)\n");
                                fprintf(fp, "    {\n");
                                fprintf(fp, "        lua_pushinteger(L, i_);\n");
                                fprintf(fp, "        lua_gettable(L, %d);\n", j+2);
                                fprintf(fp, "        a%d[i_-1]=lua_tostring(L, -1);\n", j+1);
                                fprintf(fp, "        lua_pop(L, 1);\n");
                                fprintf(fp, "    }\n");
                            }
                        }
                        {
                            std::string type = funcs[i].type.name;
                            if(type == "float")
                            {
                                fprintf(fp, "    const double       r1 = self->%s(", name.c_str());
                            }
                            else if(type == "vector")
                            {
                                fprintf(fp, "    const ksl_vector   r1 = self->%s(", name.c_str());
                            }
                            else if(type == "string")
                            {
                                fprintf(fp, "    const std::string  r1 = self->%s(", name.c_str());
                            }
                            else if(type == "matrix")
                            {
                                fprintf(fp, "    const ksl_matrix   r1 = self->%s(", name.c_str());
                            }
                            else if(type == "void")
                            {
                                fprintf(fp, "    self->%s(", name.c_str());
                            }

                            {
                                for(int j=0;j<funcs[i].args.size();j++)
                                {
                                    fprintf(fp, "a%d", j+1);
                                    if(j+1!=funcs[i].args.size())
                                    {
                                        fprintf(fp, ", ");
                                    }
                                }
                                fprintf(fp, ");\n");
                            }
                        }
                        {
                            std::string type = funcs[i].type.name;
                            if(type == "float")
                            {
                                fprintf(fp, "    lua_pushnumber(L, r1);\n");
                                fprintf(fp, "    return 1;\n");
                            }
                            else if(type == "vector")
                            {
                                fprintf(fp, "    lua_getglobal(L, \"vector_from_ptr\");\n");
                                fprintf(fp, "    lua_pushlightuserdata(L, (void*)&r1);\n");
                                fprintf(fp, "    lua_pcall(L, 1, 1, 0);\n");
                                fprintf(fp, "    return 1;\n");
                            }
                            else if(type == "string")
                            {
                                fprintf(fp, "    lua_pushstring(L, r1.c_str());\n");
                                fprintf(fp, "    return 1;\n");
                            }
                            else if(type == "matrix")
                            {
                                //fprintf(fp, "    lua_pushstring(L, r1.c_str());\n");
                                //fprintf(fp, "    return 1;\n");
                                fprintf(fp, "    return 0;\n");
                            }
                            else if(type == "void")
                            {
                                fprintf(fp, "    return 0;\n");
                            }
                            else
                            {
                                fprintf(fp, "    return 0;\n");
                            }
                        }
                        
                        fprintf(fp, "}\n");
                        fprintf(fp, "\n");
                    }
                }
#endif
            }
            if(bFile)
            {
                fclose(fp);
            }
        }
        
        return nRet;
    }
}
