#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include <algorithm>

#include "rslc_fs_convert.h"

#define VERSION_MAJOR 0
#define VERSION_MINOR 1

struct command_option
{
    const char* option;
    int nargs;
};

struct command_output
{
    std::string option;
    std::vector<std::string> values;
};


class command
{
public:
    command(int argc, char** argv, const command_option* inputs)
    {
        {
            int i = 0;
            while (inputs[i].option)
            {
                inputs_.push_back(inputs[i]);
                i++;
            }
        }
        {
            for (int i = 1; i < argc; i++)
            {
                std::string arg = argv[i];
                int kFind = -1;
                for (int j = 0; j < inputs_.size(); j++)
                {
                    if (
                        arg == "-" + std::string(inputs_[j].option) ||
                        arg == "/" + std::string(inputs_[j].option))
                    {
                        kFind = j;
                        break;
                    }
                }
                if (kFind < 0)
                {
                    others_.push_back(arg);
                }
                else
                {
                    command_output out;
                    out.option = inputs_[kFind].option;
                    int ii = i;
                    if (inputs_[kFind].nargs > 0)
                    {
                        int maxarg = std::min(argc, ii + 1 + inputs_[kFind].nargs);
                        for (int k = ii + 1; k < maxarg; k++)
                        {
                            out.values.push_back(argv[k]);
                            i++;
                        }
                    }
                    outputs_.push_back(out);
                }
            }
        }
    }

    const std::vector<command_option>& inputs() const { return inputs_; }
    const std::vector<command_output>& outouts() const { return outputs_; }
    const std::vector<std::string>& others() const { return others_; }
private:
    std::vector<command_option> inputs_;
    std::vector<command_output> outputs_;
    std::vector<std::string> others_;
};

int main(int argc, char** argv)
{
    using namespace rslc;

    static command_option inputs[] = {
        {"o", 1},
        {"version", 0},
        {"v", 0},
        {"log", 0},
        {"help", 0},
        {"h", 0},

        {"verbose", 0},
        {"debug", 0},
        {"c_like", 0},

        {NULL, 0}};

    int nRet = 0;

    command cmd(argc, argv, inputs);

    bool bQuiet = false;
    bool bDebug = false;
    bool bVersion = false;
    bool bHelp = false;
    bool bVerbose = false;
    bool bLog = false;

    std::string strOutput = "";

    const std::vector<command_output> outputs = cmd.outouts();

    for (size_t i = 0; i < outputs.size(); i++)
    {
        if (outputs[i].option == "debug")
        {
            bDebug = true;
        }
        else if (outputs[i].option == "v" || outputs[i].option == "version")
        {
            bVersion = true;
        }
        else if (outputs[i].option == "h" || outputs[i].option == "help")
        {
            bHelp = true;
        }
        else if (outputs[i].option == "verbose")
        {
            bVerbose = true;
        }
        else if (outputs[i].option == "log")
        {
            bLog = true;
        }
        if (outputs[i].option == "o")
        {
            if (outputs[i].values.size() > 0)
            {
                strOutput = outputs[i].values[0];
            }
        }
    }

    if (bVersion)
    {
        printf("%s version %d.%d\n", argv[0], VERSION_MAJOR, VERSION_MINOR);
    }

    if (bHelp)
    {
        printf("HELP! > <\n");
    }

    if (bDebug)
    {
        bVerbose = true;
    }

    if (!bQuiet)
    {
        //
    }

    const std::vector<std::string> others = cmd.others();
    if (others.size() < 1)
    {
        printf("%s \"%s\"\n", argv[0], "*.sl");
        return -1;
    }
    std::string strInputPath = "";
    std::string strOutputPath = "";
    if (strOutput != "")
    {
        strOutputPath = strOutput;
    }
    for (int k = 0; k < others.size(); k++)
    {
        strInputPath = others[k];
    }

    if(!strInputPath.empty())
    {
        return rslc_fs_convert(strInputPath.c_str(), strOutputPath.c_str());
    }

    return nRet;
}
