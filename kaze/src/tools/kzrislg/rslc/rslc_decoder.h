#ifndef rslc_DECODER_H
#define rslc_DECODER_H

#include <cstdio>
#include <string>

namespace rslc
{
    class rslc_decoder
    {
    public:
        rslc_decoder(FILE* fp);
        ~rslc_decoder();
        int read(char* buffer, unsigned int size);
    private:
        FILE* fp_;
    };
}

#endif