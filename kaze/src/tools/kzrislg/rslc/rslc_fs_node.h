#ifndef RSLC_FS_NODE_H
#define RSLC_FS_NODE_H

#include "rslc_node.h"
#include "rslc_fs_types.h"
#include <string>
#include <vector>

namespace rslc
{
    class rslc_fs_node : public rslc_node
    {
        ;
    };

    class rslc_fs_identfier_node : public rslc_fs_node
    {
    public:
        explicit rslc_fs_identfier_node(const rslc_fs_identifier& name):name_(name){}
        const rslc_fs_identifier& get_name()const{return name_;}
    protected:
        rslc_fs_identifier name_;
    };

    typedef rslc_fs_identfier_node rslc_fs_variable_node;

    class rslc_fs_type_node : public rslc_fs_node
    {
    public:
        explicit rslc_fs_type_node(const std::string& name)
        {
            type_.name = name;
            type_.is_output = false;
        }
        explicit rslc_fs_type_node(const rslc_fs_type& type):type_(type){}
        const rslc_fs_type& get_type()const{return type_;}
    protected:
        rslc_fs_type type_;
    };

    class rslc_fs_variable_list_node : public rslc_fs_node
    {
    public:
        rslc_fs_variable_list_node(){}
        
        void add_type(const rslc_fs_type& type)
        {
            rslc_fs_variable_list val;
            val.type = type;
            l_.push_back(val);
        }

        void add_name(const rslc_fs_identifier& name)
        {
            if(!l_.empty())    
            {
                l_.back().names.push_back(name);
            }
            else
            {
                rslc_fs_variable_list val;
                val.names.push_back(name);
                l_.push_back(val);
            }
        }

        void add_type_name(const rslc_fs_type& type, const rslc_fs_identifier& name)
        {
            rslc_fs_variable_list val;
            val.type = type;
            val.names.push_back(name);
            l_.push_back(val);
        }

        void add(const rslc_fs_variable_list_node* other)
        {
            if(l_.empty())
            {
                l_ = other->l_;
            }
            else
            {
                if(!other->l_.empty())
                {
                    rslc_fs_variable_list& v1 = l_.back();
                    const rslc_fs_variable_list& v2 = other->l_.front();
                    if(
                        !v1.type.name.empty() && v1.names.empty() &&
                        v2.type.name.empty() && !v2.names.empty()
                    )
                    {
                        v1.names = v2.names;
                        for(size_t i=1;i<other->l_.size();i++)
                        {
                            l_.push_back(other->l_[i]);
                        }
                    }
                    else
                    {
                        for(size_t i=0;i<other->l_.size();i++)
                        {
                            l_.push_back(other->l_[i]);
                        }
                    }
                }
            }
        }
        void clear()
        {
            l_.clear();
        }
        const std::vector<rslc_fs_variable_list>& get_list()const{return l_;}
    protected:
        std::vector<rslc_fs_variable_list> l_;
    };

    class rslc_fs_function_node : public rslc_fs_node
    {
    public:
        rslc_fs_function_node
        (
            const rslc_fs_type& type, 
            const rslc_fs_identifier& name,
            const std::vector<rslc_fs_variable_list>& args
        ):func_(type, name, args)
        {
            ;
        }
        const rslc_fs_type& get_type()const{return func_.type;}
        const rslc_fs_identifier& get_name()const{return func_.name;}
        const std::vector<rslc_fs_variable_list>& get_args()const{return func_.args;}

        const rslc_fs_function& get_function()const{return func_;}
    protected:
        rslc_fs_function func_;
    };

    class rslc_fs_function_list_node : public rslc_fs_node
    {
    public:
        rslc_fs_function_list_node(){}
        void add(const rslc_fs_function_node* f){funcs_.push_back(f);}
        size_t size()const{return funcs_.size();}
        const rslc_fs_function_node* at(size_t i)const{return funcs_[i];}
    protected:
        std::vector<const rslc_fs_function_node*> funcs_;
    };

}

#endif