#ifndef KAZE_SOCKET_IO_H
#define KAZE_SOCKET_IO_H

#ifdef _WIN32
#include <winsock2.h>
#else
typedef int SOCKET;
#endif
#include <string>

    class socket_io
    {

    public:
        typedef enum
        {
            SOCKET_STATE_INIT,
            SOCKET_STATE_CONNECTED,
            SOCKET_STATE_CLOSED,
            SOCKET_STATE_INVALID
        } SocketState;

    public:
        socket_io();
        socket_io(SOCKET fd);
        ~socket_io();

        bool connect(const char* addrString, int port);
        bool bind(int port);
#ifndef _WIN32
        bool connect(const char* filename);
        bool bind(const char* filename);
#endif
        SOCKET accept(void);
        bool send(const void* buf, size_t len, int flags);
        bool recv(void* buf, size_t len, int flags);
        bool close();
        bool select();
        SOCKET get_fd(void) { return m_fd; }

    private:
        SOCKET m_fd;              ///< Socket descriptor
        int m_port;               ///< Port number
        std::string m_addrString; ///< Address name

        SocketState m_state; ///< The state of the socket object.
    };
#endif
