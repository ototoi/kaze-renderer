#include <gl/gl_texture.h>
#include <gl/gl_shader.h>
#include <gl/gl_program.h>
#include <gl/gl_vertex_buffer.h>
#include <gl/gl_vertex_array.h>

#include <GLFW/glfw3.h>
#include <stdlib.h>
#include <stdio.h>

#include <thread>
#include <atomic>
#include <cstdint>
#include <vector>
#include <string>

#include "socket_io.h"



//----------------------------------
//from lucille
#define DEFAULT_PORT 21264 /* change as you like. */
#define MAXPACKETS 32 * 32
#define DEFAULT_APP "rockenfield"
#define LOCALADDR "127.0.0.1"

/* Commands sent to the server */
#define COMMAND_NEW 0
#define COMMAND_FINISH 1
#define COMMAND_PIXEL 2

/* Commands sent from the server */
#define COMMAND_CANCEL 10 // Cancel(e.g. The window is closed.)

/* Commands for ack */
#define COMMAND_FINISH_ACK 21
//----------------------------------

#define DEFAULT_FILE "/tmp/12348383.zzz"

namespace
{
    struct pixel_packet
    {
        uint32_t x;
        uint32_t y;
        float col[4];
    };

    struct image_info
    {
        uint32_t width;
        uint32_t height;
    };

    struct command_buffer
    {
        void open(int w, int h)
        {
            std::lock_guard<std::mutex> lock(this->mtx);
            this->width = w;
            this->height = h;
            buffer.resize(w*h*4);
            bDirty = true;
            //printf("%d, %d\n", width, height);
        }
        void close()
        {
            std::lock_guard<std::mutex> lock(this->mtx);
            bDirty = true;
            bClose = true;
            print("close\n");
        }
        void print(const std::string& s)
        {
            std::lock_guard<std::mutex> lock(this->mtx_print);
            //printf("%s", s.c_str());
        } 
        void add_pixels(std::vector<pixel_packet>& pixels)
        {
            std::lock_guard<std::mutex> lock(this->mtx);
            int w = this->width;
            for(size_t i = 0; i < pixels.size(); i++)
            {
                int x = pixels[i].x;
                int y = pixels[i].y;
                size_t index = y*w+x;
                if(index < buffer.size()/4)
                {
                    buffer[4*index+0] = pixels[i].col[0];
                    buffer[4*index+1] = pixels[i].col[1];
                    buffer[4*index+2] = pixels[i].col[2];
                    buffer[4*index+3] = pixels[i].col[3];
                }
            }
            bDirty = true;
            print("pixels\n");
        }
        GLFWwindow* window;
        int width;
        int height;
        bool bDirty;
        bool bClose;
        std::vector<float> buffer;
        std::mutex mtx;
        std::mutex mtx_print;
    };
}


static
void receive_command(command_buffer* data)
{
    socket_io s;
    bool bContinue = true;
    if(s.bind(DEFAULT_FILE))
    {
        if(data->bClose)
        {
            bContinue = false;
        }
    }
    else
    {
        data->bClose = true;
        bContinue = false;
    }

    while(bContinue)
    {
        if(data->bClose)
        {
            bContinue = false;
        }
        {
            if(s.select())
            {
                SOCKET r_fd = 0; 
                if((r_fd = s.accept()))
                {
                    socket_io s2(r_fd);
                    data->print("accept\n");
                    {
                        uint32_t com = 0;
                        s2.recv(&com, sizeof(uint32_t), 0);
                        switch(com)
                        {
                            case COMMAND_NEW:
                            {
                                uint32_t len = 0;
                                image_info info;
                                s2.recv(&len,  sizeof(uint32_t), 0);
                                if(len == sizeof(image_info))
                                {
                                    s2.recv(&info, len, 0);
                                }
                                else
                                {
                                    char buffer[512];
                                    s2.recv(buffer, len, 0);
                                    info = *(reinterpret_cast<const image_info*>(buffer));
                                }
                                data->open(info.width, info.height);
                            }
                        }
                    }

                    while(bContinue)
                    {
                        if(data->bClose)
                        {
                            bContinue = false;
                        }
                        else
                        {
                            if(s2.select())
                            {
                                uint32_t com = 0;
                                s2.recv(&com, sizeof(uint32_t), 0);
                                switch(com)
                                {
                                    case COMMAND_FINISH:
                                    {
                                        uint32_t com2 = COMMAND_FINISH_ACK;
                                        s2.send(&com2,  sizeof(uint32_t), 0);
                                        data->close();
                                        bContinue = false;
                                    }
                                    break;
                                    case COMMAND_PIXEL:
                                    {
                                        uint32_t len = 0;
                                        std::vector<pixel_packet> pixels;
                                        s2.recv(&len,  sizeof(uint32_t), 0);
                                        pixels.resize(len/sizeof(pixel_packet));
                                        s2.recv(&pixels[0], len, 0);
                                        data->add_pixels(pixels);
                                    }
                                    break;
                                }
                            }
                        }
                        std::this_thread::yield();
                    }
                    s2.close();
                }
            }
        }
        std::this_thread::yield();
    }
    s.close();
}

static const char* vertex_shader_text =
    "#version 410\n"
    "in vec3 pos;\n"
    "in vec2 uv;\n"
    "out vec3 fpos;\n"
    "out vec2 fuv;\n"
    "void main()\n"
    "{\n"
    "	fpos = pos;\n"
    "	fuv  = uv;\n"
    "	gl_Position = vec4(fpos,1);\n"
    "}\n";
static const char* fragment_shader_text =
    "#version 410\n"
    "uniform sampler2D tex;\n"
    "in vec3 fpos;\n"
    "in vec2 fuv;\n"
    "out vec4 out_color;\n"
    "void main()\n"
    "{\n"
    "    out_color = texture(tex, fuv);\n"
    "}\n";
static void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}

int main(int argc, char** argv)
{
    int width = 640;
    int height = 480;

    command_buffer cb;
    cb.width = width;
    cb.height = height;
    cb.buffer.resize(width * height * 4);
    cb.bDirty = false;
    cb.bClose = false;

    std::thread t(receive_command, &cb);

    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
        exit(EXIT_FAILURE);
    //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    
    GLFWwindow* window = glfwCreateWindow(width, height, "kzrfview", NULL, NULL);
    if (!window)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }
    glfwSetKeyCallback(window, key_callback);
    glfwMakeContextCurrent(window);
    //gladLoadGLLoader((GLADloadproc) glfwGetProcAddress);
    glfwSwapInterval(1);
    // NOTE: OpenGL error checks have been omitted for brevity


    // this is the only way to initialize glew correctly under core profile context.
    glewExperimental = true;
    if (GLenum r = glewInit() != GLEW_OK)
    {
        printf("Failed to initialize glew. Error = %s\n", glewGetErrorString(r));
        exit(1);
    }

    std::shared_ptr<gl::gl_program> prog(new gl::gl_program());
    {
        std::string strVS = vertex_shader_text;
        std::string strFS = fragment_shader_text;

        std::shared_ptr<gl::gl_shader> vsh(new gl::gl_shader(GL_VERTEX_SHADER));
        std::shared_ptr<gl::gl_shader> fsh(new gl::gl_shader(GL_FRAGMENT_SHADER));
        if(!vsh->compile_from_string(strVS))
        {
            printf("vsh\n");
        }
        if(!fsh->compile_from_string(strFS))
        {
            printf("fsh\n");
        }
        prog->add_shader(vsh->get_handle());
        prog->add_shader(fsh->get_handle());
        prog->link();
    }
    std::shared_ptr<gl::gl_vertex_buffer> vbo_p(new gl::gl_vertex_buffer());
    std::shared_ptr<gl::gl_vertex_buffer> vbo_c(new gl::gl_vertex_buffer());
    std::shared_ptr<gl::gl_vertex_array>  vao  (new gl::gl_vertex_array());

    {
        static const float posData[] = {
            -1.0f, -1.0f, 0.0f, //0
            -1.0f, +1.0f, 0.0f, //1
            +1.0f, -1.0f, 0.0f, //2
            +1.0f, +1.0f, 0.0f, //3
        };
        static const float uvData[] = {
            0.0f, 1.0f,
            0.0f, 0.0f,
            1.0f, 1.0f,
            1.0f, 0.0f,
        };
        {
            ::glBindBuffer(GL_ARRAY_BUFFER, vbo_p->get_handle());
            ::glBufferData(GL_ARRAY_BUFFER, 3 * 4 * sizeof(float), posData, GL_STATIC_DRAW);
        }
        {
            ::glBindBuffer(GL_ARRAY_BUFFER, vbo_c->get_handle());
            ::glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(float), uvData, GL_STATIC_DRAW);
        }
        ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
    }
    {
        ::glBindVertexArray(vao->get_handle());
        {
            GLuint index = prog->get_attrib_location("pos");
            ::glBindBuffer(GL_ARRAY_BUFFER, vbo_p->get_handle());
            ::glVertexAttribPointer(index, 3, GL_FLOAT, GL_FALSE, 0, NULL);
            ::glEnableVertexAttribArray(index);
        }
        {
            GLuint index = prog->get_attrib_location("uv");
            ::glBindBuffer(GL_ARRAY_BUFFER, vbo_c->get_handle());
            ::glVertexAttribPointer(index, 2, GL_FLOAT, GL_FALSE, 0, NULL);
            ::glEnableVertexAttribArray(index);
        }
        ::glBindBuffer(GL_ARRAY_BUFFER, 0u);
        ::glBindVertexArray(0u);
    }

    std::shared_ptr<gl::gl_texture_2D> tex( new gl::gl_texture_2D() );
    tex->bind();
    ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);  //not biliear
    ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);  //
    ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //TODO
    ::glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //TODO
    tex->unbind();
    tex->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, 0);

    int bResize = 0;

    while (!glfwWindowShouldClose(window))
    {
        int width, height;
        if(cb.bDirty)
        {
            std::lock_guard<std::mutex> lock(cb.mtx);
            width = cb.width;
            height = cb.height;
            {
                if(bResize == 0)
                {
                    glfwSetWindowSize(window, width, height);
                    bResize++;
                }
            }

            tex->set_image(GL_RGBA32F, width, height, GL_RGBA, GL_FLOAT, &(cb.buffer[0]));
            cb.bDirty = false;
        }
        
        glfwGetFramebufferSize(window, &width, &height);
        {
            ::glDisable(GL_DEPTH_TEST);
            ::glDisable(GL_LIGHTING);
            ::glDepthMask(GL_FALSE);
            
            ::glViewport(0, 0, width, height);
            ::glClearColor(0, 0, 0, 1);
            ::glClear(GL_COLOR_BUFFER_BIT);

            ::glUseProgram(prog->get_handle());
                {
                    GLuint nInTexID = tex->get_handle();
                    GLuint nlocTex = prog->get_uniform_location("tex");
                    ::glActiveTexture(GL_TEXTURE0 + 0);
                    ::glBindTexture(GL_TEXTURE_2D, nInTexID);
                    prog->set_uniform(nlocTex, 0);
                }
                ::glBindVertexArray(vao->get_handle());
                    ::glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
                ::glBindVertexArray(0u);
            ::glUseProgram(0u);

            ::glFlush();
        }


        if(cb.bClose)
        {
            glfwSetWindowShouldClose(window, GLFW_TRUE);
            cb.bClose = false;
            cb.bDirty = false;
        }
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    cb.bClose = true;
    
    t.join();

    glfwDestroyWindow(window);
    glfwTerminate();
    exit(EXIT_SUCCESS);
}