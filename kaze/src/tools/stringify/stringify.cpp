#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

std::string stringify(const std::string& line)
{

    bool inconstant = false;

    std::stringstream s;
    for (int i = 0; i < (int)line.size(); ++i)
    {

        // escape double quotes
        if (line[i] == '"')
        {
            s << '\\';
            inconstant = inconstant ? false : true;
        }

        // escape backslash
        if (inconstant && line[i] == '\\')
            s << '\\';

        s << line[i];
    }

    return s.str();
}

std::string trim_right(const std::string& line)
{
    std::string s = line;
    while(!s.empty())
    {
        if(isspace(s.back()))
        {
            s.pop_back();
        }
        else
        {
            break;
        }
    }
    return s;
}

int main(int argc, const char** argv)
{

    if (argc != 3)
    {
        std::cerr << "Usage: " << argv[0] << " input-file output-file" << std::endl;
        return 1;
    }

    std::ifstream input;
    input.open(argv[1]);
    if (!input.is_open())
    {
        std::cerr << "Can not read from: " << argv[1] << std::endl;
        return 1;
    }

    std::ofstream output;
    output.open(argv[2]);
    if (!output.is_open())
    {
        std::cerr << "Can not write to: " << argv[2] << std::endl;
        return 1;
    }

    std::string line;

    while (!input.eof())
    {
        std::getline(input, line);
        if(!line.empty())
        {
            output << "\"" << stringify(trim_right(line)) << "\\n\"" << std::endl;
        }
    }

    return 0;
}
