#include <stdio.h>
#include <stdlib.h>

namespace kaze
{
    //Lucas-Kanade
    //BlockMatch
    static vector3 OptocalFlowBlockMatch(int x, int y, const texture& PyramidTex)
    {
        int offsetX = 0;
        int offsetY = 0;
        {
            float uv_x = (int(x / 2) + 0.5) * pDx * 2;
            float uv_y = (int(y / 2) + 0.5) * pDy * 2;
            vector4 col = texture(PyramidTex, vec2(uv_x, uv_y));
            offsetX = -int(col.r + eps) * 2;
            offsetY = -int(col.g + eps) * 2;
        }

        {
            for (int p = -Rs + offsetX; p <= Rs + offsetX; p++)
            {
                float SAD = 0.0;
                bool MIN_check = true;

                // ブロック
                for (int j = -blockSize; j <= blockSize; j++)
                {
                    for (int i = -blockSize; i <= blockSize; i++)
                    {
                        SAD += length(getCurrentColor(x + i, y + j) - getPrevColor(x + i + p, y + j + q));

                        // 今調べている所のマッチ度が現在の最良値を下回っていた場合は処理を抜ける
                        if (SAD > MIN_SAD)
                        {
                            MIN_check = false;
                            break;
                        }
                    }
                    if (MIN_check == false) break;
                }
                if (MIN_check == true)
                {
                    if (SAD < MIN_SAD || ((SAD == MIN_SAD) && dis_compare(p, q, MIN_p, MIN_q)))
                    {
                        MIN_SAD = SAD;
                        MIN_p = p;
                        MIN_q = q;
                    }
                }
            }
        }
        return vector4(float(-MIN_p), float(-MIN_q), 0.0, 1.0);
    }

    vector4 UV_errorcheck()
    {
        vec2 average = vec2(0.0, 0.0);
        vec2 variance = vec2(0.0, 0.0);
        int n = 0;

        for (int l = -echeckWidth; l <= echeckWidth; l++)
        {
            for (int k = -echeckWidth; k <= echeckWidth; k++)
            {
                vec2 lookupUV = ioTexUV + vec2(k * pDx, l * pDy);
                if (lookupUV.x > 0.0 && lookupUV.x < 1.0 && lookupUV.y > 0.0 && lookupUV.y < 1.0)
                {
                    vector4 valueCol = texture(prevTex, lookupUV);
                    average += vec2(valueCol.x, valueCol.y);
                    variance += vec2(valueCol.x * valueCol.x, valueCol.y * valueCol.y);
                    ++n;
                }
            }
        }
        average /= n;
        variance /= n;
        variance = vec2(variance.x - average.x * average.x, variance.y - average.y * average.y);

        vector4 valueCol = texture2D(prevTex, ioTexUV);
        vec2 value = vec2(valueCol.x, valueCol.y);
        vec2 diff = value - average;

        if (diff.x * diff.x > variance.x)
        {
            value.x = (average.x * n - value.x) / (n - 1);
        }

        if (diff.y * diff.y > variance.y)
        {
            value.y = (average.y * n - value.y) / (n - 1);
        }

        return vector4(value.x, value.y, 0.0, 1.0);
    }

    vector4 UV_smooth()
    {
        vector4 result;
        int n = 0;
        for (int l = -smoothWidth; l <= smoothWidth; l++)
        {
            for (int k = -smoothWidth; k <= smoothWidth; k++)
            {
                vec2 lookupUV = ioTexUV + vec2(k * pDx, l * pDy);
                if (lookupUV.x > 0.0 && lookupUV.x < 1.0 && lookupUV.y > 0.0 && lookupUV.y < 1.0)
                {
                    result += texture(prevTex, lookupUV);
                    n++;
                }
            }
        }
        result /= n;
        return result;
    }
}