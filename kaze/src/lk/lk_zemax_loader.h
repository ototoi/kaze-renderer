#ifndef LK_ZEMAX_LOADER_H
#define LK_ZEMAX_LOADER_H

#include <iostream>
#include "lk_surface.h"
#include "lk_surface_list.h"

namespace lk
{
    class lk_zemax_loader
    {
    public:
        static int run(lk_surface_list* ll, std::istream& is);
    };
}

#endif
