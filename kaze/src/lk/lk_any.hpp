#ifndef LK_ANY_HPP
#define LK_ANY_HPP

#include <vector>

namespace lk
{
    class lk_any_value_base
    {
    public:
        virtual ~lk_any_value_base() {}
        virtual lk_any_value_base* clone() const = 0;
    };

    template <class T>
    class lk_any_value : public lk_any_value_base
    {
    public:
        lk_any_value(const T& x)
        {
            values_.push_back(x);
        }
        lk_any_value(const std::vector<T>& values)
            : values_(values)
        {
            ;
        }
        std::vector<T>& get() { return values_; }
        const std::vector<T>& get() const { return values_; }

        lk_any_value_base* clone() const
        {
            return new lk_any_value<T>(values_);
        }
    private:
        std::vector<T> values_;
    };

    class lk_any
    {
    public:
        lk_any()
        {
            base_ = NULL;
        }
        template <class X>
        lk_any(const X& x)
        {
            base_ = new lk_any_value<X>(x);
        }
        template <class X>
        lk_any(const std::vector<X>& x)
        {
            base_ = new lk_any_value<X>(x);
        }
        lk_any(const lk_any& other)
        {
            base_ = other.base_->clone();
        }
        ~lk_any()
        {
            if (base_) delete base_;
        }
        lk_any& operator=(const lk_any& other)
        {
            if (base_) delete base_;
            base_ = other.base_->clone();
            return *this;
        }
        template <class X>
        lk_any& operator=(const X& x)
        {
            if (base_) delete base_;
            base_ = new lk_any_value<X>(x);
            return *this;
        }
        template <class X>
        lk_any& operator=(const std::vector<X>& x)
        {
            if (base_) delete base_;
            base_ = new lk_any_value<X>(x);
            return *this;
        }

        lk_any_value_base* get_ptr() { return base_; }
        const lk_any_value_base* get_ptr() const { return base_; }

        template <class X>
        X cast() const
        {
            return static_cast<const lk_any_value<X>*>(base_)->get()[0];
        }

        bool empty() const
        {
            return base_ == NULL;
        }

    private:
        lk_any_value_base* base_;
    };

    template <class X>
    X lk_any_cast(const lk_any& a)
    {
        return a.cast<X>();
    }
}


#endif