#include "lk_surface_list.h"

namespace lk
{
    void lk_surface_list::add_surface(const std::shared_ptr<lk_surface>& suf)
    {
        surfaces_.push_back(suf);
    }

    size_t lk_surface_list::get_surface_size() const
    {
        return surfaces_.size();
    }

    std::shared_ptr<lk_surface>& lk_surface_list::get_surface_at(size_t i)
    {
        return surfaces_[i];
    }

    const std::shared_ptr<lk_surface>& lk_surface_list::get_surface_at(size_t i) const
    {
        return surfaces_[i];
    }

    const std::vector<std::shared_ptr<lk_surface> >& lk_surface_list::get_surfaces() const
    {
        return surfaces_;
    }
}

