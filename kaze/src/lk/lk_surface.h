#ifndef LK_SURFACE_H
#define LK_SURFACE_H

namespace lk
{
    typedef enum
    {
        SURFACE_NONE,
        SURFACE_AIR,                //空気
        SURFACE_APERTURE_WINGS,     //羽絞り
        SURFACE_APERTURE_CIRCLE,    //円形絞り
        SURFACE_LENS_SPHERICAL,     //球面レンズ
        SURFACE_LENS_CYLINDERCAL_X, //シリンドリカルレンズ
        SURFACE_LENS_CYLINDERCAL_Y, //シリンドリカルレンズ
    } lk_surface_type;

    //-------------------------------------------------------

    ///
    /// Surface base class.
    ///
    class lk_surface
    {
    public:
        virtual ~lk_surface() {}
        virtual lk_surface_type get_type() const = 0;
        virtual bool is_aperture() const = 0;
        virtual bool is_lens() const = 0;
    };

    //-------------------------------------------------------

    ///
    /// Aperture Surface
    ///
    class lk_aperture_surface : public lk_surface
    {
    public:
        virtual bool is_aperture() const { return true; }
        virtual bool is_lens() const { return false; }
    };

    ///
    /// Lens Surface
    ///
    class lk_lens_surface : public lk_surface
    {
    public:
        virtual bool is_aperture() const { return false; }
        virtual bool is_lens() const { return true; }
    };

    //-------------------------------------------------------

    ///
    /// Circle Aperture Surface
    ///
    class lk_circle_aperture_surface : public lk_aperture_surface
    {
    public:
        lk_surface_type get_type() const { return SURFACE_APERTURE_CIRCLE; }
    public:
        double get_diameter() const { return diameter_; }
        void set_diameter(double diameter) { diameter_ = diameter; }
    private:
        double diameter_; //直径
    };

    //-------------------------------------------------------

    ///
    /// Spherical Lens Surface
    ///
    class lk_spherical_lens_surface : public lk_lens_surface
    {
    public:
        lk_surface_type get_type() const { return SURFACE_LENS_SPHERICAL; }
    public:
        double get_radius() const { return radius_; }
        void set_radius(double radius) { radius_ = radius; }

        double get_thickness() const { return thickness_; }
        void set_thickness(double thickness) { thickness_ = thickness; }

        double get_ior() const { return ior_; }
        void set_ior(double ior) { ior_ = ior; }
    private:
        //double diameter_;   //直径
        double radius_; //球面半径
        double thickness_;
        double ior_;
    };
}


#endif
