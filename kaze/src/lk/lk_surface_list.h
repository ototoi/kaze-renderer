#ifndef LK_SURFACE_LIST_H
#define LK_SURFACE_LIST_H

#include <vector>
#include <memory>
#include "lk_surface.h"

namespace lk
{
    class lk_surface_list
    {
    public:
        void add_surface(const std::shared_ptr<lk_surface>& suf);
        size_t get_surface_size() const;
        std::shared_ptr<lk_surface>& get_surface_at(size_t i);
        const std::shared_ptr<lk_surface>& get_surface_at(size_t i) const;
        const std::vector<std::shared_ptr<lk_surface> >& get_surfaces() const;
    protected:
        std::vector<std::shared_ptr<lk_surface> > surfaces_;
    };
}


#endif