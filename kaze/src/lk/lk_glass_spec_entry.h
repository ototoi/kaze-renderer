#ifndef LK_GLASS_SPEC_ENTRY_H
#define LK_GLASS_SPEC_ENTRY_H

#include <map>
#include "lk_any.hpp"

namespace lk
{
    class lk_glass_spec_entry
    {
    public:
        virtual ~lk_glass_spec_entry() {}
    protected:
        std::map<std::string, lk_any> map_;
    };
}

#endif