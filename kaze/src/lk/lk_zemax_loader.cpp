#include "lk_zemax_loader.h"
#include <iostream>
#include <sstream>
#include <string>
#include <string.h>

namespace lk
{
    ///static function
    int lk_zemax_loader::run(lk_surface_list* ll, std::istream& is)
    {
        std::string line;
        while (std::getline(is, line))
        {
            std::stringstream ss(line);
            std::string token;
            ss >> token;

            bool isAperture = false;
            bool isLens = false;
            int nBlade = 0;

            if (token[0] == 'S' || token[0] == 'I' || token[0] == 'Y')
            {
                if (strcmp(token.c_str(), "S") == 0)
                {
                    isLens = true;
                }
                else if (strcmp(token.c_str(), "SZ") == 0)
                {
                    isLens = true;
                }
                else if (strcmp(token.c_str(), "Y") == 0)
                {
                    isLens = true;
                }
                else if (strcmp(token.c_str(), "I") == 0)
                {
                    isAperture = true;
                }
                else if ((token[0] == 'I') && (token.size() >= 2))
                {
                    isAperture = true;
                    nBlade = atoi(&(token[1]));
                }
            }
            else if (token[0] == 'B')
            {
                //back focus
                ss >> token;
                float fBackFocus = atof(token.c_str());
            }
            else if (token[0] == 'C')
            {
                //imagesensor
                ss >> token;
                float fIOR = atof(token.c_str());
            }
        }
        return 0;
    }
}


#ifdef LK_UNIT_TEST
int main()
{
    return 0;
}
#endif
