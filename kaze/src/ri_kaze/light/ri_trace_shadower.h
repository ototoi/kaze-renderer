#ifndef KAZE_RI_TRACE_SHADOWER_H
#define KAZE_RI_TRACE_SHADOWER_H

#include "shadower.h"
#include "tracer.h"
#include "count_ptr.hpp"

namespace kaze
{

    class ri_trace_shadower : public shadower
    {
    public:
        ri_trace_shadower(const auto_count_ptr<tracer>& trc);
        ~ri_trace_shadower();
        void cast(aggregator<lightpath>* lps, const lightpath& lp) const;

    protected:
        std::shared_ptr<tracer> tracer_;
    };
}

#endif