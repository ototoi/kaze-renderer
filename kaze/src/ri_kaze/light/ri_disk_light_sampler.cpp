#include "ri_disk_light_sampler.h"
#include "disk_light_sampler.h"
#include <math.h>

namespace kaze
{
    static light_sampler* CreateSampler(real height, real radius, real tmax, const matrix4& mat, int num_samples)
    {
        real umin = 0.0;
        real umax = tmax / 360.0;
        real vmin = 0.0;
        real vmax = 1.0;

        int nReverse = 0;
        if (tmax < 0)
        {
            umin = (360.0 + tmax) / 360.0;
            umax = 1.0;
            nReverse = 1 - nReverse;
        }

        if (umin > umax)
        {
            std::swap(umin, umax);
        }

        vector3 o = mat * vector3(0, 0, height);
        vector3 x = mat * vector3(radius, 0, 0) - o;
        vector3 y = mat * vector3(0, radius, 0) - o;
        return new disk_light_sampler(o, x, y, umin, umax, num_samples);
    }

    ri_disk_light_sampler::ri_disk_light_sampler(real height, real radius, real tmax, const matrix4& mat, int num_samples)
    {
        inter_.reset(CreateSampler(height, radius, tmax, mat, num_samples));
    }

    ri_disk_light_sampler::~ri_disk_light_sampler()
    {
        ; //
    }

    int ri_disk_light_sampler::sample(std::vector<vector3>& samples, const sufflight& sl) const
    {
        return inter_->sample(samples, sl);
    }
}