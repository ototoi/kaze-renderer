#include "ri_distantlight_light.h"

/*
light
distantlight ( float intensity = 1;
	       color lightcolor = 1;
	       point from = point "shader" (0,0,0);
	       point to = point "shader" (0,0,1); )
{
  solar (to-from, 0)
      Cl = intensity * lightcolor;
}
*/

namespace kaze
{
    namespace
    {
        class _null_shadower : public shadower
        {
        public:
            void cast(aggregator<lightpath>* lps, const lightpath& lp) const
            {
                lps->add(lp);
            }
        };
    }

    static vector3 faceforward(const vector3& N, const vector3& I)
    {
        if (dot(I, N) > 0)
        {
            return -N;
        }
        else
        {
            return N;
        }
    }

    ri_distantlight_light::ri_distantlight_light(
        real intensity, const color3& lightcolor,
        const vector3& from, const vector3& to) : intensity_(intensity), lightcolor_(lightcolor), from_(from), to_(to)
    {
        from_ = from_;
        to_ = to_;

        shdr_.reset(new _null_shadower());
    }

    ri_distantlight_light::~ri_distantlight_light()
    {
    }

    void ri_distantlight_light::set_shadower(const auto_count_ptr<shadower>& shdr)
    {
        shdr_ = shdr;
    }

    void ri_distantlight_light::cast(aggregator<lightpath>* lps, const sufflight& sl) const
    {
        vector3 from = from_;
        vector3 to = to_;
        vector3 N = normalize(faceforward(sl.geometric(), sl.direction()));
        vector3 P = sl.position() + (N * 1e-3);

        vector3 L = to - from;
        from = P - L;

        real intensity = intensity_;
        color3 lightcolor = lightcolor_;
        color3 Cl = intensity * lightcolor;
        shdr_->cast(lps, lightpath(from, P, spectrum(Cl)));
    }
}
