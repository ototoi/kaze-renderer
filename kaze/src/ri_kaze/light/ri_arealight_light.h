#ifndef KAZE_RI_AREALIGHT_LIGHT_H
#define KAZE_RI_AREALIGHT_LIGHT_H

#include "light.h"
#include "light_sampler.h"
#include <vector>

namespace kaze
{

    class ri_arealight_light : public light
    {
    public:
        ri_arealight_light(
            real intensity = real(1.0), 
            const color3& lightcolor = color3(1, 1, 1), 
            const std::shared_ptr<light_sampler>& smp = std::shared_ptr<light_sampler>()
        );
        ~ri_arealight_light();

    public:
        void cast(aggregator<lightpath>* lps, const sufflight& sl) const;

    private:
        real intensity_;
        color3 lightcolor_;
        std::shared_ptr<light_sampler> smp_;
    };
}

#endif
