#ifndef KAZE_RI_DISTANTLIGHT_LIGHT_H
#define KAZE_RI_DISTANTLIGHT_LIGHT_H

#include "light.h"
#include "count_ptr.hpp"
#include "shadower.h"

namespace kaze
{

    class ri_distantlight_light : public light
    {
    public:
        ri_distantlight_light(real intensity, const color3& lightcolor,
                              const vector3& from, const vector3& to);
        ~ri_distantlight_light();
        void set_shadower(const auto_count_ptr<shadower>& shdr);

    public:
        void cast(aggregator<lightpath>* lps, const sufflight& sl) const;

    protected:
        real intensity_;
        color3 lightcolor_;
        vector3 from_;
        vector3 to_;
        std::shared_ptr<shadower> shdr_;
    };
}

#endif
