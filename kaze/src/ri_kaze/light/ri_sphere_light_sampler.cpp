#include "ri_sphere_light_sampler.h"
#include "disk_light_sampler.h"
#include <math.h>

namespace kaze
{
    static light_sampler* CreateSampler(real radius, real zmin, real zmax, real tmax, const matrix4& mat, int num_samples)
    {
        real umin = 0.0;
        real umax = tmax / 360.0;
        real vmin = 0.0;
        real vmax = 1.0;

        int nReverse = 0;
        if (tmax < 0)
        {
            umin = (360.0 + tmax) / 360.0;
            umax = 1.0;
            nReverse = 1 - nReverse;
        }

        if (umin > umax)
        {
            std::swap(umin, umax);
        }

        return 0;
    }

    ri_sphere_light_sampler::ri_sphere_light_sampler(real radius, real zmin, real zmax, real tmax, const matrix4& mat, int num_samples)
    {
        inter_.reset(CreateSampler(radius, zmin, zmax, tmax, mat, num_samples));
    }

    ri_sphere_light_sampler::~ri_sphere_light_sampler()
    {
        ; //
    }

    int ri_sphere_light_sampler::sample(std::vector<vector3>& samples, const sufflight& sl) const
    {
        return inter_->sample(samples, sl);
    }
}