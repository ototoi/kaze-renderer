#ifndef KAZE_RI_SPOTLIGHT_LIGHT_H
#define KAZE_RI_SPOTLIGHT_LIGHT_H

#include "light.h"
#include "count_ptr.hpp"
#include "shadower.h"

namespace kaze
{

    class ri_spotlight_light : public light
    {
    public:
        ri_spotlight_light(
            real intensity, const color3& lightcolor,
            const vector3& from, const vector3& to,
            real coneangle, real conedeltaangle, real beamdistribution);
        ~ri_spotlight_light();
        void set_shadower(const auto_count_ptr<shadower>& shdr);

    public:
        void cast(aggregator<lightpath>* lps, const sufflight& sl) const;

    protected:
        real intensity_;
        color3 lightcolor_;
        vector3 from_;
        vector3 to_;
        real coneangle_;
        real conedeltaangle_;
        real beamdistribution_;
        std::shared_ptr<shadower> shdr_;
    };
}
#endif
