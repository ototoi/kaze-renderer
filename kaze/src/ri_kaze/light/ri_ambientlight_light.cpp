#include "ri_ambientlight_light.h"

/*
light
ambientlight
 ( float intensity = 1;
	       color lightcolor = 1;)
{
      Cl = intensity * lightcolor;
}
*/

namespace kaze
{

    ri_ambientlight_light::ri_ambientlight_light(real intensity, const color3& lightcolor)
        : intensity_(intensity), lightcolor_(lightcolor)
    {
    }

    ri_ambientlight_light::~ri_ambientlight_light()
    {
    }

    void ri_ambientlight_light::cast(aggregator<lightpath>* lps, const sufflight& sl) const
    {
#if 0
        real intensity = intensity_;
        color3 lightcolor = lightcolor_;

		vector3 p = sl.position();
		//vector3 g = sl.geometric();
		vector3 n = sl.normal();
		if(dot(sl.direction(),n)>0){
			n = -n;
		}
        
		vector3 from = p+n;
		vector3 to   = p;
        color3 Cl = intensity*lightcolor;
		lps->add(lightpath(from, to, spectrum(Cl)));
#endif
        real intensity = intensity_;
        color3 lightcolor = lightcolor_;

        vector3 p = sl.position();
        vector3 from = p;
        vector3 to = p;
        color3 Cl = intensity * lightcolor;
        lps->add(lightpath(from, to, spectrum(Cl)));
    }
}
