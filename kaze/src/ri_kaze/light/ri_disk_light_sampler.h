#ifndef KAZE_RI_DISK_LIGHT_SAMPLER_H
#define KAZE_RI_DISK_LIGHT_SAMPLER_H

#include "light_sampler.h"

namespace kaze
{

    class ri_disk_light_sampler : public light_sampler
    {
    public:
        ri_disk_light_sampler(real height, real radius, real tmax, const matrix4& mat, int nsamples = 1);
        ~ri_disk_light_sampler();
        int sample(std::vector<vector3>& samples, const sufflight& sl) const;

    protected:
        std::shared_ptr<light_sampler> inter_;
    };
}

#endif