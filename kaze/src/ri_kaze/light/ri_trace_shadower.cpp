#include "ri_trace_shadower.h"

#include <vector>

namespace kaze
{
    static const real EPSILON = 1e-5;

    ri_trace_shadower::ri_trace_shadower(const auto_count_ptr<tracer>& trc)
    {
        if (trc.get())
        {
            tracer_ = trc;
        }
    }

    ri_trace_shadower::~ri_trace_shadower()
    {
        ; //
    }

    void ri_trace_shadower::cast(aggregator<lightpath>* lps, const lightpath& lp) const
    {
        vector3 from = lp.from();
        vector3 to = lp.to();
        vector3 dir = to - from;
        real l = length(dir);

        if (l > 1e-8)
        {
            dir *= (1.0 / l);
            ray r(from + EPSILON * dir, dir);
            mediation md;
            if (!(tracer_.get()) || !tracer_->trace(r, 0, l - EPSILON, md))
            {
                lps->add(lp);
            }
        }
    }
}
