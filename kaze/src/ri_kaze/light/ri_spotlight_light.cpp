#include "ri_spotlight_light.h"

#if 0
/*
light
spotlight ( float intensity = 1;
	    color lightcolor = 1;
	    point from = point "shader" (0,0,0);
	    point to = point "shader" (0,0,1);
	    float coneangle = radians(30);
	    float conedeltaangle = radians(5);
	    float beamdistribution = 2; )
{
  float atten, cosangle;
  uniform point A = normalize(to-from);

  illuminate (from, A, coneangle) {
      cosangle = (L . A) / length(L);
      atten = pow (cosangle, beamdistribution) / (L . L);
      atten *= smoothstep (cos(coneangle), cos(coneangle-conedeltaangle),
			   cosangle);
      Cl = atten * intensity * lightcolor ;
    }
}
*/
#endif

namespace kaze
{
    namespace
    {
        /*
class shadower{
	public:
		virtual ~shadower(){}
		virtual void cast(aggregator<lightpath>* lps, const lightpath& lp)const = 0;
		virtual void cast(aggregator<lightpath>* lps, const enumerator<lightpath>& enlps)const;
	};

*/

        class _null_shadower : public shadower
        {
        public:
            void cast(aggregator<lightpath>* lps, const lightpath& lp) const
            {
                lps->add(lp);
            }
        };
    }

    static vector3 faceforward(const vector3& N, const vector3& I)
    {
        if (dot(I, N) > 0)
        {
            return -N;
        }
        else
        {
            return N;
        }
    }

    ri_spotlight_light::ri_spotlight_light(
        real intensity, const color3& lightcolor,
        const vector3& from, const vector3& to,
        real coneangle, real conedeltaangle, real beamdistribution) : intensity_(intensity), lightcolor_(lightcolor),
                                                                      from_(from), to_(to),
                                                                      coneangle_(coneangle), conedeltaangle_(conedeltaangle), beamdistribution_(beamdistribution)
    {
        from_ = from_;
        to_ = to_;

        shdr_.reset(new _null_shadower());
    }

    ri_spotlight_light::~ri_spotlight_light()
    {
        ;
    }

    void ri_spotlight_light::set_shadower(const auto_count_ptr<shadower>& shdr)
    {
        shdr_ = shdr;
    }

    static real smoothstep(real min, real max, real x)
    {
        if (x <= min) return 0;
        if (max <= x) return 1;
        x = (x - min) / (max - min);
        return (x * x * (real(3.0) - (real(2.0) * x)));
    }

    void ri_spotlight_light::cast(aggregator<lightpath>* lps, const sufflight& sl) const
    {
        vector3 from = from_;
        vector3 to = to_;
        vector3 N = normalize(faceforward(sl.geometric(), sl.direction()));

        vector3 P = sl.position() + (N * 1e-3);
        vector3 L = P - from;

        color3 lightcolor = lightcolor_;
        real intensity = intensity_;
        real coneangle = coneangle_;
        real conedeltaangle = conedeltaangle_;
        real beamdistribution = beamdistribution_;

        vector3 A = (to - from) / length(to - from);
        real cosoutside = cos(coneangle);
        real cosinside = cos(coneangle - conedeltaangle);
        if (dot(L, A) > 0)
        {
            real cosangle = dot(L, A) / length(L);
            if (cosoutside <= cosangle)
            {
                real atten = pow(cosangle, beamdistribution) / dot(L, L);
                atten *= smoothstep(cosoutside, cosinside, cosangle);
                color3 Cl = atten * intensity * lightcolor;

                shdr_->cast(lps, lightpath(from, P, spectrum(Cl)));
            }
        }
    }
}
