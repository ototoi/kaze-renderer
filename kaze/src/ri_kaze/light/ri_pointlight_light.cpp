#include "ri_pointlight_light.h"

/*
light
pointlight (float intensity = 1;
	    color lightcolor = 1;
	    point from = point "shader" (0,0,0);)
{
  illuminate (from)
      Cl = intensity * lightcolor / (L . L);
}
*/

namespace kaze
{

    ri_pointlight_light::ri_pointlight_light(real intensity, const color3& lightcolor, const vector3& from)
        : intensity_(intensity), lightcolor_(lightcolor),
          from_(from)
    {
        ; //
    }

    ri_pointlight_light::~ri_pointlight_light()
    {
    }

    /*
    static
    bool illuminate(const vector3& N, const vector3& L)
    {
        return dot(N, L)<0;
    }
    */

    void ri_pointlight_light::cast(aggregator<lightpath>* lps, const sufflight& sl) const
    {
        color3 lightcolor = lightcolor_;
        real intensity = intensity_;
        vector3 from = from_;
        vector3 P = sl.position();
        vector3 L = P - from;

        vector3 N = sl.normal();
        if (dot(sl.direction(), N) > 0)
        {
            N = -N;
        }

        if (1)
        {
            color3 Cl = intensity * lightcolor / dot(L, L);
            lps->add(lightpath(from, P, spectrum(Cl)));
        }
    }
}
