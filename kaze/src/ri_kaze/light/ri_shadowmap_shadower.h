#ifndef KAZE_RI_SHADOWMAP_SHADOWER_H
#define KAZE_RI_SHADOWMAP_SHADOWER_H

#include "shadower.h"

namespace kaze
{

    class ri_shadowmap_shadower : public shadower
    {
    public:
        ri_shadowmap_shadower(const char* szFilePath, int samples = 16, real width = 1, real blur = 0, real bias = 0);
        ~ri_shadowmap_shadower();
        void cast(aggregator<lightpath>* lps, const lightpath& lp) const;

    protected:
        std::shared_ptr<shadower> shdr_;
    };
}

#endif
