#include "ri_arealight_light.h"

/*
light arealight(
	float intensity = 1;
	color lightcolor = 1; )
{
    illuminate( P, N, PI/2 )
	{
		Cl = ( intensity / (L.L) ) * transmission( P, Ps ) * lightcolor;
    }
}
*/

namespace kaze
{

    ri_arealight_light::ri_arealight_light(
        real intensity, 
        const color3& lightcolor,
        const std::shared_ptr<light_sampler>& smp
    )
        : intensity_(intensity), lightcolor_(lightcolor), smp_(smp)
    {
    }

    ri_arealight_light::~ri_arealight_light()
    {
    }

    void ri_arealight_light::cast(aggregator<lightpath>* lps, const sufflight& sl) const
    {
        color3 lightcolor = lightcolor_;
        real intensity = intensity_;

        vector3 P = sl.position();
        vector3 N = sl.normal();
        if (dot(sl.direction(), N) > 0)
        {
            N = -N;
        }
        std::vector<vector3> samples;
        if(smp_.get())
        {
            smp_->sample(samples, sl);
        }

        size_t nsamples = samples.size();
        if (nsamples)
        {
            std::vector<real> weights(nsamples);

            for (size_t i = 0; i < nsamples; i++)
            {
                vector3 from = samples[i];
                vector3 L = from - P;
                real w = 0;
                if (L.sqr_length() > 0)
                {
                    L = normalize(L);
                    w = std::max<real>(0, dot(L, N));
                }
                weights[i] = w;
            }

            real total = 0;
            for (size_t i = 0; i < nsamples; i++)
            {
                total += weights[i];
            }
            total = 1.0 / total;
            for (size_t i = 0; i < nsamples; i++)
            {
                weights[i] *= total;
            }

            for (size_t i = 0; i < nsamples; i++)
            {
                vector3 from = samples[i];
                vector3 L = P - from;
                if (weights[i] > 0)
                {
                    color3 Cl = weights[i] * intensity * lightcolor / dot(L, L);
                    lps->add(lightpath(from, P, spectrum(Cl)));
                }
            }
        }
    }
}
