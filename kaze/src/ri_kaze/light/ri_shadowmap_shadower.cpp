#include "ri_shadowmap_shadower.h"
#include "zfile.h"

#include "random.h"
#include "LDS_sampler.h"

namespace kaze
{

    static real rand01()
    {
        static const real c = 1.0 / (real(std::numeric_limits<rand_t>::max()) + 1);
        return real(xor128()) * c;
    }

    class ri_shadowmap_shadower_imp : public shadower
    {
    public:
        ri_shadowmap_shadower_imp(
            const char* szFilePath,
            int samples = 16,
            real blur_width = 1,
            real blur = 0,
            real bias = 0)
            : strFilePath_(szFilePath), samples_(samples), blur_width_(blur_width), blur_(blur), bias_(bias)
        {
            width_ = 0;
            height_ = 0;
            mat_ = mat4_gen::identity();

            zfile_image zimg;
            if (zimg.load(szFilePath))
            {
                width_ = zimg.get_width();
                height_ = zimg.get_height();
                buffer_.assign(zimg.get_buffer(), zimg.get_buffer() + width_ * height_);
                mat_ = matrix4(zimg.get_w2s());
            }
        }
        void cast(aggregator<lightpath>* lps, const lightpath& lp) const
        {
            if (width_ && height_)
            {
                vector3 from = lp.from();
                vector3 to = lp.to();
                vector3 n = (from - to);
                real l = length(n);
                if (l != 0)
                {
                    n = n * (1e-8 / l);
                    vector3 p = mat_ * (lp.to() + n);
                    int nPass = 0;
                    int nTest = 0;
                    this->test(nPass, nTest, p);
                    if (nPass == 0)
                    {
                        /*
						if(quad_out(p)){
							lps->add(lightpath(lp.from(),lp.to(), convert(color3(0,1,0)) ));
						}else{
							lps->add(lightpath(lp.from(),lp.to(), convert(color3(0,1,1)) ));
						}
						*/
                    }
                    else if (nPass == nTest)
                    {
                        lps->add(lp);
                    }
                    else
                    {
                        lps->add(lightpath(lp.from(), lp.to(), real(nPass) / nTest * lp.power()));
                    }
                }
                else
                {
                    lps->add(lp);
                }
            }
            else
            {
                lps->add(lp);
            }
        }

        vector3 rand_vector() const
        {
#if 0
			real x,y,l;
			do{
				x = 2*rand01()-1;
				y = 2*rand01()-1;
				l = x*x+y*y;
			}while(1<l);
			//l = 1.0/sqrt(l);
			return vector3(x,y,0);
#else

            real x, y, l;

            do
            {
                vector2 v = halton_sampler::get_instace().get2();
                x = 2 * v[0] - 1;
                y = 2 * v[1] - 1;
                l = x * x + y * y;
            } while (1 < l);
            //l = 1.0/sqrt(l);
            return vector3(x, y, 0);

#endif
        }

        void test(int& nPass, int& nTest, const vector3& p) const
        {
            if (blur_ > 0)
            {
                int total = samples_;
                int pass = 0;
                for (int j = 0; j < total; j++)
                {

                    vector3 q = p + blur_width_ * blur_ * rand_vector();
                    real xx = q[0];
                    real yy = q[1];
                    real zz = q[2];

                    if (!this->test(xx, yy, zz))
                    { //no hit
                        pass++;
                    }
                }
                nPass = pass;
                nTest = total;
            }
            else
            {
                int total = 1;
                int pass = 0;
                for (int j = 0; j < total; j++)
                {
                    real xx = p[0];
                    real yy = p[1];
                    real zz = p[2];

                    if (!this->test(xx, yy, zz))
                    { //no hit
                        pass++;
                    }
                }
                nPass = pass;
                nTest = total;
            }
        }

        bool quad_out(const vector3& p) const
        {
            int ww = width_;
            int hh = height_;

            real x = p[0];
            real y = p[1];
            real z = p[2];
            int ix = (int)(x * ww);
            int iy = (int)(hh - y * hh);
            if (ix < 0 || width_ <= ix) return true;
            if (iy < 0 || height_ <= iy) return true;
            return false;
        }

        bool test(real x, real y, real z) const
        {
            int ww = width_;
            int hh = height_;

            int ix = (int)(x * ww);
            int iy = (int)(hh - y * hh);

            int nPass = 0;
            if (test(ix - 1, iy - 1, z)) nPass++;
            if (test(ix, iy - 1, z)) nPass++;
            if (test(ix + 1, iy - 1, z)) nPass++;

            if (test(ix - 1, iy, z)) nPass++;
            if (test(ix, iy, z)) nPass++;
            if (test(ix + 1, iy, z)) nPass++;

            if (test(ix - 1, iy + 1, z)) nPass++;
            if (test(ix, iy + 1, z)) nPass++;
            if (test(ix + 1, iy + 1, z)) nPass++;

            if (nPass == 9)
                return true;
            else
                return false;
        }

        bool test(int x, int y, real z) const
        {
            if (x < 0 || width_ <= x) return true;
            if (y < 0 || height_ <= y) return true;

            real zz = buffer_[y * width_ + x];

            if (z > zz + bias_)
            { //no pass
                return true;
            }
            else
            { //pass
                return false;
            }
        }

    protected:
        std::string strFilePath_;
        int samples_;
        real blur_width_;
        real blur_;
        real bias_;
        std::vector<double> buffer_;
        int width_;
        int height_;
        matrix4 mat_;
    };

    ri_shadowmap_shadower::ri_shadowmap_shadower(const char* szFilePath, int samples, real width, real blur, real bias)
    {
        shdr_.reset(new ri_shadowmap_shadower_imp(szFilePath, samples, width, blur, bias));
    }

    ri_shadowmap_shadower::~ri_shadowmap_shadower()
    {
        ;
    }

    void ri_shadowmap_shadower::cast(aggregator<lightpath>* lps, const lightpath& lp) const
    {
        shdr_->cast(lps, lp);
    }
}
