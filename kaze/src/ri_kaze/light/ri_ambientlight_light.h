#ifndef KAZE_RI_AMBIENTLIGHT_LIGHT_H
#define KAZE_RI_AMBIENTLIGHT_LIGHT_H

#include "light.h"

namespace kaze
{

    class ri_ambientlight_light : public light
    {
    public:
        ri_ambientlight_light(real intensity, const color3& lightcolor);
        ~ri_ambientlight_light();

    public:
        void cast(aggregator<lightpath>* lps, const sufflight& sl) const;

    private:
        real intensity_;
        color3 lightcolor_;
    };
}

#endif
