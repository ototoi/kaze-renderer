#ifndef KAZE_RI_POINTLIGHT_LIGHT_H
#define KAZE_RI_POINTLIGHT_LIGHT_H

#include "light.h"

namespace kaze
{

    class ri_pointlight_light : public light
    {
    public:
        ri_pointlight_light(real intensity, const color3& lightcolor, const vector3& from);
        ~ri_pointlight_light();

    public:
        void cast(aggregator<lightpath>* lps, const sufflight& sl) const;

    protected:
        real intensity_;
        color3 lightcolor_;
        vector3 from_;
    };
}

#endif
