#ifndef KAZE_RI_TEXTURE_H
#define KAZE_RI_TEXTURE_H

#include "texture.hpp"
#include "color.h"
#include "count_ptr.hpp"
#include <vector>

namespace kaze
{

    class ri_uniform_float1_texture : public texture<real>
    {
    public:
        ri_uniform_float1_texture(const float C[]);
        ri_uniform_float1_texture(real c);

    public:
        real get(const sufflight& suf) const;
        real get(real u) const;
        real get(real u, real v) const;
        real get(real u, real v, real w) const;

    private:
        real c_;
    };

    class ri_uniform_float2_texture : public texture<vector2>
    {
    public:
        ri_uniform_float2_texture(const float C[]);
        ri_uniform_float2_texture(vector2& c);

    public:
        vector2 get(const sufflight& suf) const;
        vector2 get(real u) const;
        vector2 get(real u, real v) const;
        vector2 get(real u, real v, real w) const;

    private:
        vector2 c_;
    };

    class ri_uniform_float3_texture : public texture<vector3>
    {
    public:
        ri_uniform_float3_texture(const float C[]);
        ri_uniform_float3_texture(vector3& c);

    public:
        vector3 get(const sufflight& suf) const;
        vector3 get(real u) const;
        vector3 get(real u, real v) const;
        vector3 get(real u, real v, real w) const;

    private:
        vector3 c_;
    };

    class ri_indexed_uniform_float1_texture : public texture<real>
    {
    public:
        ri_indexed_uniform_float1_texture(const float C[], size_t nface);
        ri_indexed_uniform_float1_texture(size_t nface, const float C[]);
        ri_indexed_uniform_float1_texture(const std::vector<float>& c);

    public:
        real get(const sufflight& suf) const;
        real get(real u) const;
        real get(real u, real v) const;
        real get(real u, real v, real w) const;

    private:
        std::vector<float> c_;
    };

    class ri_indexed_uniform_float2_texture : public texture<vector2>
    {
    public:
        ri_indexed_uniform_float2_texture(const float C[], size_t nface);
        ri_indexed_uniform_float2_texture(size_t nface, const float C[]);
        ri_indexed_uniform_float2_texture(const std::vector<vector2f>& c);

    public:
        vector2 get(const sufflight& suf) const;
        vector2 get(real u) const;
        vector2 get(real u, real v) const;
        vector2 get(real u, real v, real w) const;

    private:
        std::vector<vector2f> c_;
    };

    class ri_indexed_uniform_float3_texture : public texture<vector3>
    {
    public:
        ri_indexed_uniform_float3_texture(const float C[], size_t nface);
        ri_indexed_uniform_float3_texture(size_t nface, const float C[]);
        ri_indexed_uniform_float3_texture(const std::vector<vector3f>& c);

    public:
        vector3 get(const sufflight& suf) const;
        vector3 get(real u) const;
        vector3 get(real u, real v) const;
        vector3 get(real u, real v, real w) const;

    private:
        std::vector<vector3f> c_;
    };

    class ri_bind_col1_texture : public texture<vector3>
    {
    public:
        vector3 get(const sufflight& suf) const;
        vector3 get(real u) const;
        vector3 get(real u, real v) const;
        vector3 get(real u, real v, real w) const;
    };

    class ri_bind_col2_texture : public texture<vector3>
    {
    public:
        vector3 get(const sufflight& suf) const;
        vector3 get(real u) const;
        vector3 get(real u, real v) const;
        vector3 get(real u, real v, real w) const;
    };

    class ri_bind_col3_texture : public texture<vector3>
    {
    public:
        vector3 get(const sufflight& suf) const;
        vector3 get(real u) const;
        vector3 get(real u, real v) const;
        vector3 get(real u, real v, real w) const;
    };

    class ri_composite_float2_texture : public texture<vector2>
    {
    public:
        ri_composite_float2_texture(const auto_count_ptr<texture<real> >& a, const auto_count_ptr<texture<real> >& b);

    public:
        vector2 get(const sufflight& suf) const;

    protected:
        std::shared_ptr<texture<real> > a_;
        std::shared_ptr<texture<real> > b_;
    };

    class ri_modify_st_texture : public texture<vector2>
    {
    public:
        ri_modify_st_texture(const float tc[8], const auto_count_ptr<texture<vector2> >& st);
    public:
        vector2 get(const sufflight& suf) const;

    protected:
        float tc_[8];
        std::shared_ptr<texture<vector2> > st_;
    };

    class ri_param_col2_texture : public texture<vector2>
    {
    public:
        vector2 get(const sufflight& suf) const;
    };

    //-------------------------------------------------------
    //quadrics
    typedef ri_uniform_float1_texture ri_quadric_uniform_float1_texture;
    class ri_quadric_varying_float1_texture : public texture<real>
    {
    public:
        ri_quadric_varying_float1_texture(const float C[]);

    public:
        real get(const sufflight& suf) const;
        real get(real u, real v) const;

    protected:
        float c_[4];
    };
    typedef ri_quadric_varying_float1_texture ri_quadric_vertex_float1_texture;
    typedef ri_uniform_float2_texture ri_quadric_uniform_float2_texture;
    class ri_quadric_varying_float2_texture : public texture<vector2>
    {
    public:
        ri_quadric_varying_float2_texture(const float C[]);

    public:
        vector2 get(const sufflight& suf) const;
        vector2 get(real u, real v) const;

    protected:
        color2f c_[4];
    };
    typedef ri_quadric_varying_float2_texture ri_quadric_vertex_float2_texture;
    typedef ri_uniform_float3_texture ri_quadric_uniform_float3_texture;
    class ri_quadric_varying_float3_texture : public texture<vector3>
    {
    public:
        ri_quadric_varying_float3_texture(const float C[]);

    public:
        vector3 get(const sufflight& suf) const;
        vector3 get(real u, real v) const;

    protected:
        color3f c_[4];
    };
    typedef ri_quadric_varying_float3_texture ri_quadric_vertex_float3_texture;
    //-------------------------------------------------------
    //ri_polygon
    typedef ri_indexed_uniform_float1_texture ri_points_triangle_polygons_uniform_float1_texture; //uniform
    class ri_points_triangle_polygons_vertex_float1_texture : public texture<real>
    {
    public:
        ri_points_triangle_polygons_vertex_float1_texture(int nfaces, const int verts[], int nverts, const float C[]);

    public:
        real get(const sufflight& suf) const;

    protected:
        std::vector<int> indices_;
        std::vector<float> v_;
    };
    typedef ri_points_triangle_polygons_vertex_float1_texture ri_points_triangle_polygons_varying_float1_texture;

    class ri_points_triangle_polygons_facevarying_float1_texture : public texture<real>
    {
    public:
        ri_points_triangle_polygons_facevarying_float1_texture(int nfaces, const float C[]);

    public:
        real get(const sufflight& suf) const;

    protected:
        std::vector<float> v_;
    };

    typedef ri_indexed_uniform_float2_texture ri_points_triangle_polygons_uniform_float2_texture; //uniform
    class ri_points_triangle_polygons_vertex_float2_texture : public texture<vector2>
    {
    public:
        ri_points_triangle_polygons_vertex_float2_texture(int nfaces, const int verts[], int nverts, const float C[]);

    public:
        vector2 get(const sufflight& suf) const;

    protected:
        std::vector<int> indices_;
        std::vector<vector2f> v_;
    };
    typedef ri_points_triangle_polygons_vertex_float2_texture ri_points_triangle_polygons_varying_float2_texture;

    class ri_points_triangle_polygons_facevarying_float2_texture : public texture<vector2>
    {
    public:
        ri_points_triangle_polygons_facevarying_float2_texture(int nfaces, const float C[]);

    public:
        vector2 get(const sufflight& suf) const;

    protected:
        std::vector<vector2f> v_;
    };

    typedef ri_indexed_uniform_float3_texture ri_points_triangle_polygons_uniform_float3_texture; //uniform
    class ri_points_triangle_polygons_vertex_float3_texture : public texture<vector3>
    {
    public:
        ri_points_triangle_polygons_vertex_float3_texture(int nfaces, const int verts[], int nverts, const float C[]);

    public:
        vector3 get(const sufflight& suf) const;

    protected:
        std::vector<int> indices_;
        std::vector<vector3f> v_;
    };
    typedef ri_points_triangle_polygons_vertex_float3_texture ri_points_triangle_polygons_varying_float3_texture;

    class ri_points_triangle_polygons_facevarying_float3_texture : public texture<vector3>
    {
    public:
        ri_points_triangle_polygons_facevarying_float3_texture(int nfaces, const float C[]);

    public:
        vector3 get(const sufflight& suf) const;

    protected:
        std::vector<vector3f> v_;
    };

    //-------------------------------------------------------
    //ri_patch
    //ri_patch_mesh
    //ri_bezier_patch_mesh
    class ri_bezier_patch_mesh_uniform_float1_texture : public texture<real>
    {
    public:
        ri_bezier_patch_mesh_uniform_float1_texture(int nu, int uorder, int nv, int vorder, const float P[]);

    public:
        real get(const sufflight& suf) const;

    protected:
        int nu_;
        int uorder_;
        int nv_;
        int vorder_;
        int nPu_;
        int nPv_;
        std::vector<float> v_;
    };

    class ri_bezier_patch_mesh_varying_float1_texture : public texture<real>
    {
    public:
        ri_bezier_patch_mesh_varying_float1_texture(int nu, int uorder, int nv, int vorder, const float P[]);

    public:
        real get(const sufflight& suf) const;

    protected:
        int nu_;
        int uorder_;
        int nv_;
        int vorder_;
        int nPu_;
        int nPv_;
        std::vector<float> v_;
    };

    class ri_bezier_patch_mesh_vertex_float1_texture : public texture<real>
    {
    public:
        ri_bezier_patch_mesh_vertex_float1_texture(int nu, int uorder, int nv, int vorder, const float P[]);

    public:
        real get(const sufflight& suf) const;

    protected:
        int nu_;
        int uorder_;
        int nv_;
        int vorder_;
        int nPu_;
        int nPv_;
        std::vector<float> v_;
    };

    class ri_bezier_patch_mesh_uniform_float2_texture : public texture<vector2>
    {
    public:
        ri_bezier_patch_mesh_uniform_float2_texture(int nu, int uorder, int nv, int vorder, const float P[]);

    public:
        vector2 get(const sufflight& suf) const;

    protected:
        int nu_;
        int uorder_;
        int nv_;
        int vorder_;
        int nPu_;
        int nPv_;
        std::vector<vector2f> v_;
    };

    class ri_bezier_patch_mesh_varying_float2_texture : public texture<vector2>
    {
    public:
        ri_bezier_patch_mesh_varying_float2_texture(int nu, int uorder, int nv, int vorder, const float P[]);

    public:
        vector2 get(const sufflight& suf) const;

    protected:
        int nu_;
        int uorder_;
        int nv_;
        int vorder_;
        int nPu_;
        int nPv_;
        std::vector<vector2f> v_;
    };

    class ri_bezier_patch_mesh_vertex_float2_texture : public texture<vector2>
    {
    public:
        ri_bezier_patch_mesh_vertex_float2_texture(int nu, int uorder, int nv, int vorder, const float P[]);

    public:
        vector2 get(const sufflight& suf) const;

    protected:
        int nu_;
        int uorder_;
        int nv_;
        int vorder_;
        int nPu_;
        int nPv_;
        std::vector<vector2f> v_;
    };

    class ri_bezier_patch_mesh_uniform_float3_texture : public texture<vector3>
    {
    public:
        ri_bezier_patch_mesh_uniform_float3_texture(int nu, int uorder, int nv, int vorder, const float P[]);

    public:
        vector3 get(const sufflight& suf) const;

    protected:
        int nu_;
        int uorder_;
        int nv_;
        int vorder_;
        int nPu_;
        int nPv_;
        std::vector<vector3f> v_;
    };

    class ri_bezier_patch_mesh_varying_float3_texture : public texture<vector3>
    {
    public:
        ri_bezier_patch_mesh_varying_float3_texture(int nu, int uorder, int nv, int vorder, const float P[]);

    public:
        vector3 get(const sufflight& suf) const;

    protected:
        int nu_;
        int uorder_;
        int nv_;
        int vorder_;
        int nPu_;
        int nPv_;
        std::vector<vector3f> v_;
    };

    class ri_bezier_patch_mesh_vertex_float3_texture : public texture<vector3>
    {
    public:
        ri_bezier_patch_mesh_vertex_float3_texture(int nu, int uorder, int nv, int vorder, const float P[]);

    public:
        vector3 get(const sufflight& suf) const;

    protected:
        int nu_;
        int uorder_;
        int nv_;
        int vorder_;
        int nPu_;
        int nPv_;
        std::vector<vector3f> v_;
    };

    //-------------------------------------------------------
    //ri_bezier_curves
    typedef ri_bind_col1_texture ri_bezier_curves_Cs_texture;
    typedef ri_bind_col2_texture ri_bezier_curves_Os_texture;

    //-------------------------------------------------------
    //ri_points
    typedef ri_bind_col1_texture ri_points_Cs_texture;
    typedef ri_bind_col2_texture ri_points_Os_texture;
}

#endif