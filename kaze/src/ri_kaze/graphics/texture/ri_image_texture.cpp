#include "ri_image_texture.h"

#include "vipmap_image_texture.hpp"
#include "unit_image_texture.hpp"
#include "cubemap_texture.hpp"
#include "latlong_texture.hpp"

#include "image/bmp_io.h"
#include "image/png_io.h"
#include "image/tiff_io.h"
#include "image/jpeg_io.h"

#include "image/kxf_io.h"

namespace kaze
{

    static std::string GetExt(const std::string& str)
    {
#ifdef _WIN32
        char ext[_MAX_EXT] = {};
        _splitpath(str.c_str(), NULL, NULL, NULL, ext);
        return ext;
#else
        std::string::size_type i = str.find_last_of('.');
        if (i != std::string::npos)
        {
            return str.substr(i);
        }
        else
        {
            return "";
        }
#endif
    }

    static std::shared_ptr<image<color3> > GetImageFromFile(const char* szPicFile)
    {
        std::string strExt = GetExt(szPicFile);
        if (strExt == ".tif" || strExt == ".tiff")
        {
            return ReadTiffColor3Image(szPicFile);
        }
        else if (strExt == ".bmp")
        {
            return ReadBMPColor3Image(szPicFile);
        }
        else if (strExt == ".png")
        {
            return ReadPNGColor3Image(szPicFile);
        }
        else if (strExt == ".jpg" || strExt == ".jpeg")
        {
            return ReadJPEGColor3Image(szPicFile);
        }
        return std::shared_ptr<image<color3> >();
    }

    class ri_float3_image_texture_imp
    {
    public:
        ri_float3_image_texture_imp(const char* szImageFile);
        vector3 get(const sufflight& suf) const;

    protected:
        std::shared_ptr<texture<color3> > vip_;
    };

    static inline bool is_powerof2(unsigned int x)
    {
        return ((x - 1) & x) == 0;
    }

    static std::shared_ptr<image_interpolator<color3> > GetInterpolator(const char* filter)
    {
        if (strcmp(filter, "box") == 0) return std::shared_ptr<image_interpolator<color3> >(new nearest_image_interpolator<color3>());
        if (strcmp(filter, "triangle") == 0) return std::shared_ptr<image_interpolator<color3> >(new bilinear_image_interpolator<color3>());
        if (strcmp(filter, "mitchell") == 0) return std::shared_ptr<image_interpolator<color3> >(new mitchell_image_interpolator<color3>());
        if (strcmp(filter, "gauss") == 0) return std::shared_ptr<image_interpolator<color3> >(new gaussian_image_interpolator<color3>());
        if (strcmp(filter, "sinc") == 0) return std::shared_ptr<image_interpolator<color3> >(new lanczos_image_interpolator<color3>());

        return std::shared_ptr<image_interpolator<color3> >(new mitchell_image_interpolator<color3>());
    }

    static std::shared_ptr<corresponder> GetCorresponder(const char* wrap)
    {
        if (strcmp(wrap, "repeat") == 0) return std::shared_ptr<corresponder>(new powerof2_repeat_corresponder());
        if (strcmp(wrap, "clamp") == 0) return std::shared_ptr<corresponder>(new powerof2_clamp_corresponder());
        if (strcmp(wrap, "mirror") == 0) return std::shared_ptr<corresponder>(new powerof2_mirror_corresponder());

        if (strcmp(wrap, "border") == 0) return std::shared_ptr<corresponder>(new powerof2_clamp_corresponder());

        return std::shared_ptr<corresponder>(new powerof2_repeat_corresponder());
    }

    template <class T>
    class border_texture : public texture<T>
    {
    public:
        border_texture(const T& c, int nFlag, const auto_count_ptr<texture<T> >& tex)
            : c_(c), nFlag_(nFlag), tex_(tex)
        {
        }

        T get(const sufflight& sl) const
        {
            vector3 stw = sl.get_stw();
            real s = stw[0];
            real t = stw[1];
            int n = 0;
            if ((nFlag_ & 1) && (s < 0 || 1 < s)) n |= 1;
            if ((nFlag_ & 2) && (t < 0 || 1 < t)) n |= 2;
            switch (n)
            {
            case 0:
                return tex_->get(sl);
            case 1:
                return c_;
            case 2:
                return c_;
            default:
                return c_;
            }
        }

    private:
        T c_;
        int nFlag_;
        std::shared_ptr<texture<T> > tex_;
    };

    static std::shared_ptr<texture<color3> > AddBorder(const auto_count_ptr<texture<color3> >& img, const char* swrap, const char* twrap)
    {
        int n = 0;
        if (strcmp(swrap, "border") == 0) n |= 1;
        if (strcmp(twrap, "border") == 0) n |= 2;
        if (n) return std::shared_ptr<texture<color3> >(new border_texture<color3>(color3(0, 0, 0), n, img));

        return img;
    }

    ri_float3_image_texture_imp::ri_float3_image_texture_imp(const char* szImageFile)
    {
        kxf_header header;
        int nRet = read_kxf_header(szImageFile, &header);

        if (nRet == 0)
        {
            if (strcmp(header.projection, "cube") == 0)
            {
                std::vector<std::shared_ptr<image<color3> > > vecImg;
                nRet = load_from_kxf(szImageFile, vecImg);
                if (nRet != 0) return;

                std::shared_ptr<image<color3> >& img = vecImg[0];
                std::shared_ptr<corresponder> corx = GetCorresponder("clamp");
                std::shared_ptr<corresponder> cory = GetCorresponder("clamp");
                if (!is_powerof2(img->get_width()))
                {
                    corx.reset(corx->clone_general());
                }
                if (!is_powerof2(img->get_height()))
                {
                    cory.reset(cory->clone_general());
                }
                std::shared_ptr<image_interpolator<color3> > inp = GetInterpolator(header.filter);

                std::vector<std::shared_ptr<texture<color3> > > vecTex;
                for (int i = 0; i < vecImg.size(); i++)
                {
                    vecTex.push_back(std::shared_ptr<texture<color3> >(new unit_image_texture<color3>(vecImg[i], corx, cory, inp)));
                }

                vip_.reset(new cubemap_texture<color3>(vecTex));
            }
            else if (strcmp(header.projection, "latlong") == 0)
            {
                std::vector<std::shared_ptr<image<color3> > > vecImg;
                nRet = load_from_kxf(szImageFile, vecImg);
                if (nRet != 0) return;

                std::shared_ptr<image<color3> >& img = vecImg[0];
                std::shared_ptr<corresponder> corx = GetCorresponder("repeat");
                std::shared_ptr<corresponder> cory = GetCorresponder("clamp");
                if (!is_powerof2(img->get_width()))
                {
                    corx.reset(corx->clone_general());
                }
                if (!is_powerof2(img->get_height()))
                {
                    cory.reset(cory->clone_general());
                }
                std::shared_ptr<image_interpolator<color3> > inp = GetInterpolator(header.filter);

                vip_.reset(new latlong_texture<color3>(new unit_image_texture<color3>(vecImg[0], corx, cory, inp)));
            }
            else
            {
                std::vector<std::shared_ptr<image<color3> > > vecImg;
                nRet = load_from_kxf(szImageFile, vecImg);
                if (nRet != 0) return;

                std::shared_ptr<image<color3> >& img = vecImg[0];
                std::shared_ptr<corresponder> corx = GetCorresponder(header.swrap);
                std::shared_ptr<corresponder> cory = GetCorresponder(header.twrap);
                if (!is_powerof2(img->get_width()))
                {
                    corx.reset(corx->clone_general());
                }
                if (!is_powerof2(img->get_height()))
                {
                    cory.reset(cory->clone_general());
                }

                std::shared_ptr<image_interpolator<color3> > inp = GetInterpolator(header.filter);
                if (vecImg.size() == 1)
                {
                    vip_ = AddBorder(new unit_image_texture<color3>(vecImg[0], corx, cory, inp), header.swrap, header.twrap);
                }
                else
                {
                    std::reverse(vecImg.begin(), vecImg.end()); //><
                    std::shared_ptr<mipmap_image<color3> > mip(new mipmap_image<color3>(vecImg));
                    vip_ = AddBorder(new vipmap_image_texture<color3>(mip, corx, cory, inp), header.swrap, header.twrap);
                }
            }
        }
        else
        {
            std::shared_ptr<image<color3> > img = GetImageFromFile(szImageFile);
            if (img.get())
            {
                vip_.reset(new unit_image_texture<color3>(img));
            }
        }
    }

    vector3 ri_float3_image_texture_imp::get(const sufflight& suf) const
    {
        if (vip_.get()) return vip_->get(suf);
        return vector3(0, 0, 0);
    }

    ri_float3_image_texture::ri_float3_image_texture(const char* szImageFile)
    {
        imp_ = new ri_float3_image_texture_imp(szImageFile);
    }
    ri_float3_image_texture::~ri_float3_image_texture()
    {
        delete imp_;
    }
    vector3 ri_float3_image_texture::get(const sufflight& suf) const
    {
        return imp_->get(suf);
    }
}