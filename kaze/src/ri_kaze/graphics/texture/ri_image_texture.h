#ifndef KAZE_RI_IMAGE_TEXTURE_H
#define KAZE_RI_IMAGE_TEXTURE_H

#include "texture.hpp"

namespace kaze
{

    class ri_float3_image_texture_imp;
    class ri_float3_image_texture : public texture<vector3>
    {
    public:
        ri_float3_image_texture(const char* szImageFile);
        ~ri_float3_image_texture();
        vector3 get(const sufflight& suf) const;

    private:
        ri_float3_image_texture_imp* imp_;
    };
}

#endif