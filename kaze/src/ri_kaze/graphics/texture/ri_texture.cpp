#include "ri_texture.h"
#include "bezier.h"

#include <vector>
#include <iostream>

namespace kaze
{

    static inline real f2c(double c)
    {
        return c;
    }

    static inline real f2c(float c)
    {
        return c;
    }

    static inline vector2 f2c(const vector2d& c)
    {
        return vector2(c[0], c[1]);
    }

    static inline vector2 f2c(const vector2f& c)
    {
        return vector2(c[0], c[1]);
    }

    static inline vector3 f2c(const vector3d& c)
    {
        return vector3(c[0], c[1], c[2]);
    }

    static inline vector3 f2c(const vector3f& c)
    {
        return vector3(c[0], c[1], c[2]);
    }

    static inline float c2f(double c)
    {
        return (float)c;
    }

    static inline float c2f(float c)
    {
        return c;
    }

    static inline vector2f c2f(const vector2d& c)
    {
        return vector2f((float)c[0], (float)c[1]);
    }

    static inline vector2f c2f(const vector2f& c)
    {
        return vector2f(c[0], c[1]);
    }

    static inline vector3f c2f(const vector3d& c)
    {
        return vector3f((float)c[0], (float)c[1], (float)c[2]);
    }

    static inline vector3f c2f(const vector3f& c)
    {
        return vector3f(c[0], c[1], c[2]);
    }

    template <int Sz>
    struct float_type
    {
        typedef float type;
    };
    template <>
    struct float_type<2>
    {
        typedef vector2f type;
    };
    template <>
    struct float_type<3>
    {
        typedef vector3f type;
    };

    template <int Sz>
    struct return_type
    {
        typedef real type;
    };
    template <>
    struct return_type<2>
    {
        typedef vector2 type;
    };
    template <>
    struct return_type<3>
    {
        typedef vector3 type;
    };

    ri_uniform_float1_texture::ri_uniform_float1_texture(const float C[]) : c_(C[0]) {}
    ri_uniform_float1_texture::ri_uniform_float1_texture(real c) : c_(c) {}

    real ri_uniform_float1_texture::get(const sufflight& suf) const
    {
        return c_;
    }
    real ri_uniform_float1_texture::get(real u) const
    {
        return c_;
    }
    real ri_uniform_float1_texture::get(real u, real v) const
    {
        return c_;
    }
    real ri_uniform_float1_texture::get(real u, real v, real w) const
    {
        return c_;
    }

    ri_uniform_float2_texture::ri_uniform_float2_texture(const float C[]) : c_(C) {}
    ri_uniform_float2_texture::ri_uniform_float2_texture(vector2& c) : c_(c) {}

    vector2 ri_uniform_float2_texture::get(const sufflight& suf) const
    {
        return c_;
    }
    vector2 ri_uniform_float2_texture::get(real u) const
    {
        return c_;
    }
    vector2 ri_uniform_float2_texture::get(real u, real v) const
    {
        return c_;
    }
    vector2 ri_uniform_float2_texture::get(real u, real v, real w) const
    {
        return c_;
    }

    ri_uniform_float3_texture::ri_uniform_float3_texture(const float C[]) : c_(C) {}
    ri_uniform_float3_texture::ri_uniform_float3_texture(vector3& c) : c_(c) {}

    vector3 ri_uniform_float3_texture::get(const sufflight& suf) const
    {
        return c_;
    }
    vector3 ri_uniform_float3_texture::get(real u) const
    {
        return c_;
    }
    vector3 ri_uniform_float3_texture::get(real u, real v) const
    {
        return c_;
    }
    vector3 ri_uniform_float3_texture::get(real u, real v, real w) const
    {
        return c_;
    }
    //-----------------------------------------------------------------------------
    ri_indexed_uniform_float1_texture::ri_indexed_uniform_float1_texture(const float C[], size_t nface)
    {
        c_.resize(nface);
        for (size_t i = 0; i < nface; i++)
        {
            c_[i] = C[i];
        }
    }
    ri_indexed_uniform_float1_texture::ri_indexed_uniform_float1_texture(size_t nface, const float C[])
    {
        c_.resize(nface);
        for (size_t i = 0; i < nface; i++)
        {
            c_[i] = C[i];
        }
    }
    ri_indexed_uniform_float1_texture::ri_indexed_uniform_float1_texture(const std::vector<float>& c)
        : c_(c)
    {
        ; //
    }

    real ri_indexed_uniform_float1_texture::get(const sufflight& suf) const
    {
        return f2c(c_[suf.get_index()]);
    }
    real ri_indexed_uniform_float1_texture::get(real u) const
    {
        return real(0);
    }
    real ri_indexed_uniform_float1_texture::get(real u, real v) const
    {
        return real(0);
    }
    real ri_indexed_uniform_float1_texture::get(real u, real v, real w) const
    {
        return real(0);
    }
    //-----------------------------------------------------------------------------
    ri_indexed_uniform_float2_texture::ri_indexed_uniform_float2_texture(const float C[], size_t nface)
    {
        c_.resize(nface);
        for (size_t i = 0; i < nface; i++)
        {
            c_[i] = vector2f(C + 2 * i);
        }
    }
    ri_indexed_uniform_float2_texture::ri_indexed_uniform_float2_texture(size_t nface, const float C[])
    {
        c_.resize(nface);
        for (size_t i = 0; i < nface; i++)
        {
            c_[i] = vector2f(C + 2 * i);
        }
    }
    ri_indexed_uniform_float2_texture::ri_indexed_uniform_float2_texture(const std::vector<vector2f>& c)
        : c_(c)
    {
        ; //
    }

    vector2 ri_indexed_uniform_float2_texture::get(const sufflight& suf) const
    {
        return f2c(c_[suf.get_index()]);
    }
    vector2 ri_indexed_uniform_float2_texture::get(real u) const
    {
        return vector2(0, 0);
    }
    vector2 ri_indexed_uniform_float2_texture::get(real u, real v) const
    {
        return vector2(0, 0);
    }
    vector2 ri_indexed_uniform_float2_texture::get(real u, real v, real w) const
    {
        return vector2(0, 0);
    }
    //-----------------------------------------------------------------------------
    ri_indexed_uniform_float3_texture::ri_indexed_uniform_float3_texture(const float C[], size_t nface)
    {
        c_.resize(nface);
        for (size_t i = 0; i < nface; i++)
        {
            c_[i] = vector3f(C + 3 * i);
        }
    }
    ri_indexed_uniform_float3_texture::ri_indexed_uniform_float3_texture(size_t nface, const float C[])
    {
        c_.resize(nface);
        for (size_t i = 0; i < nface; i++)
        {
            c_[i] = vector3f(C + 3 * i);
        }
    }
    ri_indexed_uniform_float3_texture::ri_indexed_uniform_float3_texture(const std::vector<vector3f>& c)
        : c_(c)
    {
        ; //
    }

    vector3 ri_indexed_uniform_float3_texture::get(const sufflight& suf) const
    {
        return f2c(c_[suf.get_index()]);
    }
    vector3 ri_indexed_uniform_float3_texture::get(real u) const
    {
        return vector3(0, 0, 0);
    }
    vector3 ri_indexed_uniform_float3_texture::get(real u, real v) const
    {
        return vector3(0, 0, 0);
    }
    vector3 ri_indexed_uniform_float3_texture::get(real u, real v, real w) const
    {
        return vector3(0, 0, 0);
    }

    //-----------------------------------------------------------------------------
    vector3 ri_bind_col1_texture::get(const sufflight& suf) const
    {
        return suf.get_color1();
    }
    vector3 ri_bind_col1_texture::get(real u) const
    {
        return color_black();
    }
    vector3 ri_bind_col1_texture::get(real u, real v) const
    {
        return color_black();
    }
    vector3 ri_bind_col1_texture::get(real u, real v, real w) const
    {
        return color_black();
    }
    //-----------------------------------------------------------------------------
    vector3 ri_bind_col2_texture::get(const sufflight& suf) const
    {
        return suf.get_col2();
    }
    vector3 ri_bind_col2_texture::get(real u) const
    {
        return color_black();
    }
    vector3 ri_bind_col2_texture::get(real u, real v) const
    {
        return color_black();
    }
    vector3 ri_bind_col2_texture::get(real u, real v, real w) const
    {
        return color_black();
    }
    //-----------------------------------------------------------------------------
    vector3 ri_bind_col3_texture::get(const sufflight& suf) const
    {
        return suf.get_col3();
    }
    vector3 ri_bind_col3_texture::get(real u) const
    {
        return color_black();
    }
    vector3 ri_bind_col3_texture::get(real u, real v) const
    {
        return color_black();
    }
    vector3 ri_bind_col3_texture::get(real u, real v, real w) const
    {
        return color_black();
    }
    //-----------------------------------------------------------------------------
    ri_composite_float2_texture::ri_composite_float2_texture(const auto_count_ptr<texture<real> >& a, const auto_count_ptr<texture<real> >& b)
        : a_(a), b_(b)
    {
        ;
    }
    vector2 ri_composite_float2_texture::get(const sufflight& suf) const
    {
        return vector2(a_->get(suf), b_->get(suf));
    }
    //-----------------------------------------------------------------------------
    ri_modify_st_texture::ri_modify_st_texture(const float tc[8], const auto_count_ptr<texture<vector2> >& st)
        : st_(st)
    {
        memcpy(tc_, tc, sizeof(float) * 8);
    }

    static inline vector2 L(const vector2& a, const vector2& b, real t) { return a * (1 - t) + b * t; }
    vector2 ri_modify_st_texture::get(const sufflight& suf) const
    {
        vector2 st = st_->get(suf);
        real s = st[0];
        real t = st[1];
        return L(L(vector2(tc_[0], tc_[1]), vector2(tc_[2], tc_[3]), s), L(vector2(tc_[4], tc_[5]), vector2(tc_[6], tc_[7]), s), t);
    }

    //-----------------------------------------------------------------------------
    vector2 ri_param_col2_texture::get(const sufflight& suf) const
    {
        vector3 stw = suf.get_param();
        return vector2(stw[0], stw[1]);
    }

    //-----------------------------------------------------------------------------
    ri_quadric_varying_float1_texture::ri_quadric_varying_float1_texture(const float C[])
    {
        for (int i = 0; i < 4; i++)
        {
            c_[i] = C[i];
        }
    }

    real ri_quadric_varying_float1_texture::get(const sufflight& suf) const
    {
        vector3 uvw = suf.get_param();
        real u = uvw[0];
        real v = uvw[1];
        return this->get(u, v);
    }
    real ri_quadric_varying_float1_texture::get(real u, real v) const
    {
        return f2c(bezier_evaluate(c_, 2, 2, (float)u, (float)v));
    }

    ri_quadric_varying_float2_texture::ri_quadric_varying_float2_texture(const float C[])
    {
        for (int i = 0; i < 4; i++)
        {
            c_[i] = vector2f(C + 2 * i);
        }
    }

    vector2 ri_quadric_varying_float2_texture::get(const sufflight& suf) const
    {
        vector3 uvw = suf.get_param();
        real u = uvw[0];
        real v = uvw[1];
        return this->get(u, v);
    }
    vector2 ri_quadric_varying_float2_texture::get(real u, real v) const
    {
        return f2c(bezier_evaluate(c_, 2, 2, (float)u, (float)v));
    }

    ri_quadric_varying_float3_texture::ri_quadric_varying_float3_texture(const float C[])
    {
        for (int i = 0; i < 4; i++)
        {
            c_[i] = vector3f(C + 3 * i);
        }
    }

    vector3 ri_quadric_varying_float3_texture::get(const sufflight& suf) const
    {
        vector3 uvw = suf.get_param();
        real u = uvw[0];
        real v = uvw[1];
        return this->get(u, v);
    }
    vector3 ri_quadric_varying_float3_texture::get(real u, real v) const
    {
        return f2c(bezier_evaluate(c_, 2, 2, (float)u, (float)v));
    }

    //-----------------------------------------------------------------------------
    /*
	template<int Sz>
	class ri_points_triangle_polygons_vertex_texture:public texture<typename return_type<Sz>::type>
	{
	public:
		typedef typename float_type<Sz>::type ftype;
		typedef typename return_type<Sz>::type rtype;
	public:
		ri_points_triangle_polygons_vertex_texture(int nfaces, const int verts[], int nverts, const float C[])
		{
			indices_.resize(3*nfaces);
			for(int i=0;i<3*nfaces;i++){
				indices_[i] = verts[i];
			}
			v_.resize(nverts);
			for(int i=0;i<nverts;i++){
				v_[i] = ftype(C+Sz*i);
			}
		}
		rtype get(const sufflight& suf)const
		{
			size_t i = suf.get_index();
			int i0 = indices_[3*i+0];
			int i1 = indices_[3*i+1];
			int i2 = indices_[3*i+2];
			ftype c0 = v_[i0];
			ftype c1 = v_[i1];
			ftype c2 = v_[i2];
			vector3 uvw = suf.get_param();
			float u = (float)uvw[0];
			float v = (float)uvw[1];
			float w = (float)(1-u-v);

			return f2c(w*c0 + u*c1 + v*c2);
		}
	protected:
		std::vector<int> indices_;
		std::vector<ftype> v_;
	};

	typedef ri_points_triangle_polygons_vertex_texture<2> ri_points_triangle_polygons_vertex_float2_texture;
	*/
    ri_points_triangle_polygons_vertex_float1_texture::ri_points_triangle_polygons_vertex_float1_texture(int nfaces, const int verts[], int nverts, const float C[])
    {
        indices_.resize(3 * nfaces);
        for (int i = 0; i < 3 * nfaces; i++)
        {
            indices_[i] = verts[i];
        }
        v_.resize(nverts);
        for (int i = 0; i < nverts; i++)
        {
            v_[i] = C[i];
        }
    }

    real ri_points_triangle_polygons_vertex_float1_texture::get(const sufflight& suf) const
    {
        size_t i = suf.get_index();
        int i0 = indices_[3 * i + 0];
        int i1 = indices_[3 * i + 1];
        int i2 = indices_[3 * i + 2];
        float c0 = v_[i0];
        float c1 = v_[i1];
        float c2 = v_[i2];
        vector3 uvw = suf.get_param();
        float u = (float)uvw[0];
        float v = (float)uvw[1];
        float w = (float)(1 - u - v);

        return f2c(w * c0 + u * c1 + v * c2);
    }

    ri_points_triangle_polygons_facevarying_float1_texture::ri_points_triangle_polygons_facevarying_float1_texture(int nfaces, const float C[])
    {
        v_.resize(3 * nfaces);
        for (int i = 0; i < 3 * nfaces; i++)
        {
            v_[i] = C[i];
        }
    }

    real ri_points_triangle_polygons_facevarying_float1_texture::get(const sufflight& suf) const
    {
        size_t i = suf.get_index();

        size_t i0 = 3 * i + 0;
        size_t i1 = 3 * i + 1;
        size_t i2 = 3 * i + 2;

        float c0 = v_[i0];
        float c1 = v_[i1];
        float c2 = v_[i2];
        vector3 uvw = suf.get_param();
        float u = (float)uvw[0];
        float v = (float)uvw[1];
        float w = (float)(1 - u - v);

        return f2c(w * c0 + u * c1 + v * c2);
    }

    ri_points_triangle_polygons_vertex_float2_texture::ri_points_triangle_polygons_vertex_float2_texture(int nfaces, const int verts[], int nverts, const float C[])
    {
        indices_.resize(3 * nfaces);
        for (int i = 0; i < 3 * nfaces; i++)
        {
            indices_[i] = verts[i];
        }
        v_.resize(nverts);
        for (int i = 0; i < nverts; i++)
        {
            v_[i] = vector2f(C + 2 * i);
        }
    }

    vector2 ri_points_triangle_polygons_vertex_float2_texture::get(const sufflight& suf) const
    {
        size_t i = suf.get_index();
        int i0 = indices_[3 * i + 0];
        int i1 = indices_[3 * i + 1];
        int i2 = indices_[3 * i + 2];
        vector2f c0 = v_[i0];
        vector2f c1 = v_[i1];
        vector2f c2 = v_[i2];
        vector3 uvw = suf.get_param();
        float u = (float)uvw[0];
        float v = (float)uvw[1];
        float w = (float)(1 - u - v);

        return f2c(w * c0 + u * c1 + v * c2);
    }

    ri_points_triangle_polygons_facevarying_float2_texture::ri_points_triangle_polygons_facevarying_float2_texture(int nfaces, const float C[])
    {
        v_.resize(3 * nfaces);
        for (int i = 0; i < 3 * nfaces; i++)
        {
            v_[i] = vector2f(C + 2 * i);
        }
    }

    vector2 ri_points_triangle_polygons_facevarying_float2_texture::get(const sufflight& suf) const
    {
        size_t i = suf.get_index();

        size_t i0 = 3 * i + 0;
        size_t i1 = 3 * i + 1;
        size_t i2 = 3 * i + 2;

        vector2f c0 = v_[i0];
        vector2f c1 = v_[i1];
        vector2f c2 = v_[i2];
        vector3 uvw = suf.get_param();
        float u = (float)uvw[0];
        float v = (float)uvw[1];
        float w = (float)(1 - u - v);

        return f2c(w * c0 + u * c1 + v * c2);
    }
    //-----------------------------------------------------------------------------

    ri_points_triangle_polygons_vertex_float3_texture::ri_points_triangle_polygons_vertex_float3_texture(int nfaces, const int verts[], int nverts, const float C[])
    {
        indices_.resize(3 * nfaces);
        for (int i = 0; i < 3 * nfaces; i++)
        {
            indices_[i] = verts[i];
        }
        v_.resize(nverts);
        for (int i = 0; i < nverts; i++)
        {
            v_[i] = vector3f(C + 3 * i);
        }
    }

    vector3 ri_points_triangle_polygons_vertex_float3_texture::get(const sufflight& suf) const
    {
        size_t i = suf.get_index();
        int i0 = indices_[3 * i + 0];
        int i1 = indices_[3 * i + 1];
        int i2 = indices_[3 * i + 2];
        vector3f c0 = v_[i0];
        vector3f c1 = v_[i1];
        vector3f c2 = v_[i2];
        vector3 uvw = suf.get_param();
        float u = (float)uvw[0];
        float v = (float)uvw[1];
        float w = (float)(1 - u - v);

        return f2c(w * c0 + u * c1 + v * c2);
    }

    ri_points_triangle_polygons_facevarying_float3_texture::ri_points_triangle_polygons_facevarying_float3_texture(int nfaces, const float C[])
    {
        v_.resize(3 * nfaces);
        for (int i = 0; i < 3 * nfaces; i++)
        {
            v_[i] = vector3f(C + 3 * i);
        }
    }

    vector3 ri_points_triangle_polygons_facevarying_float3_texture::get(const sufflight& suf) const
    {
        size_t i = suf.get_index();

        size_t i0 = 3 * i + 0;
        size_t i1 = 3 * i + 1;
        size_t i2 = 3 * i + 2;

        vector3f c0 = v_[i0];
        vector3f c1 = v_[i1];
        vector3f c2 = v_[i2];
        vector3 uvw = suf.get_param();
        float u = (float)uvw[0];
        float v = (float)uvw[1];
        float w = (float)(1 - u - v);

        return f2c(w * c0 + u * c1 + v * c2);
    }
    //-----------------------------------------------------------------------------
    ri_bezier_patch_mesh_uniform_float1_texture::ri_bezier_patch_mesh_uniform_float1_texture(int nu, int uorder, int nv, int vorder, const float P[])
        : nu_(nu), uorder_(uorder), nv_(nv), vorder_(vorder)
    {
        int nPu = (nu - 1) / (uorder - 1);
        int nPv = (nv - 1) / (vorder - 1);

        nPu_ = nPu;
        nPv_ = nPv;

        size_t sz = nPu * nPv;
        v_.resize(sz);
        for (size_t i = 0; i < sz; i++)
        {
            v_[i] = P[i];
        }
    }

    real ri_bezier_patch_mesh_uniform_float1_texture::get(const sufflight& suf) const
    {
        int nu = nu_;
        int nv = nv_;
        int uorder = uorder_;
        int vorder = vorder_;
        int nPu = nPu_;
        int nPv = nPv_;

        vector3 uvw = suf.get_param();
        float u = (float)uvw[0] * nPu;
        float v = (float)uvw[1] * nPv;

        int uu = (int)floor(u);
        int vv = (int)floor(v);

        return f2c(v_[vv * nPu + uu]);
    }

    ri_bezier_patch_mesh_varying_float1_texture::ri_bezier_patch_mesh_varying_float1_texture(int nu, int uorder, int nv, int vorder, const float P[])
        : nu_(nu), uorder_(uorder), nv_(nv), vorder_(vorder)
    {
        int nPu = (nu - 1) / (uorder - 1);
        int nPv = (nv - 1) / (vorder - 1);

        nPu_ = nPu;
        nPv_ = nPv;

        size_t sz = (nPu + 1) * (nPv + 1);
        v_.resize(sz);
        for (size_t i = 0; i < sz; i++)
        {
            v_[i] = P[i];
        }
    }

    real ri_bezier_patch_mesh_varying_float1_texture::get(const sufflight& suf) const
    {
        int nu = nu_;
        int nv = nv_;
        int uorder = uorder_;
        int vorder = vorder_;
        int nPu = nPu_;
        int nPv = nPv_;

        vector3 uvw = suf.get_param();
        float u = (float)uvw[0] * nPu;
        float v = (float)uvw[1] * nPv;

        int uu = (int)floor(u);
        int vv = (int)floor(v);
        u = u - uu;
        v = v - vv;
        if (uu >= nPu)
        {
            uu = nPu - 1;
            u = 1;
        }
        if (vv >= nPv)
        {
            vv = nPv - 1;
            v = 1;
        }

        float tmp[4];
        tmp[0] = v_[(vv + 0) * (nPu + 1) + uu];
        tmp[1] = v_[(vv + 0) * (nPu + 1) + uu + 1];
        tmp[1] = v_[(vv + 1) * (nPu + 1) + uu];
        tmp[1] = v_[(vv + 1) * (nPu + 1) + uu + 1];

        return f2c(bezier_evaluate(&tmp[0], 2, 2, u, v));
    }

    ri_bezier_patch_mesh_vertex_float1_texture::ri_bezier_patch_mesh_vertex_float1_texture(int nu, int uorder, int nv, int vorder, const float P[])
        : nu_(nu), uorder_(uorder), nv_(nv), vorder_(vorder)
    {
        int nPu = (nu - 1) / (uorder - 1);
        int nPv = (nv - 1) / (vorder - 1);

        nPu_ = nPu;
        nPv_ = nPv;

        size_t sz = nu * nv;
        v_.resize(sz);
        for (size_t i = 0; i < sz; i++)
            v_[i] = (float)P[i];
    }

    real ri_bezier_patch_mesh_vertex_float1_texture::get(const sufflight& suf) const
    {
        int nu = nu_;
        int nv = nv_;
        int uorder = uorder_;
        int vorder = vorder_;
        int nPu = nPu_;
        int nPv = nPv_;

        vector3 uvw = suf.get_param();
        float u = (float)uvw[0] * nPu;
        float v = (float)uvw[1] * nPv;

        int uu = (int)floor(u);
        int vv = (int)floor(v);
        u = u - uu;
        v = v - vv;
        if (uu >= nPu)
        {
            uu = nPu - 1;
            u = 1;
        }
        if (vv >= nPv)
        {
            vv = nPv - 1;
            v = 1;
        }

        int i0 = uu * (uorder - 1);
        int j0 = vv * (vorder - 1);

        std::vector<float> tmp(uorder * vorder);
        for (int j = 0; j < vorder; j++)
        {
            for (int i = 0; i < uorder; i++)
            {
                tmp[j * uorder + i] = v_[(j0 + j) * nu + i0 + i];
            }
        }

        return f2c(bezier_evaluate(&tmp[0], uorder, vorder, u, v));
    }
    //-----------------------------------------------------------------------------

    ri_bezier_patch_mesh_uniform_float2_texture::ri_bezier_patch_mesh_uniform_float2_texture(int nu, int uorder, int nv, int vorder, const float P[])
        : nu_(nu), uorder_(uorder), nv_(nv), vorder_(vorder)
    {
        int nPu = (nu - 1) / (uorder - 1);
        int nPv = (nv - 1) / (vorder - 1);

        nPu_ = nPu;
        nPv_ = nPv;

        size_t sz = nPu * nPv;
        v_.resize(sz);
        for (size_t i = 0; i < sz; i++)
        {
            v_[i] = vector2f(P + 2 * i);
        }
    }

    vector2 ri_bezier_patch_mesh_uniform_float2_texture::get(const sufflight& suf) const
    {
        int nu = nu_;
        int nv = nv_;
        int uorder = uorder_;
        int vorder = vorder_;
        int nPu = nPu_;
        int nPv = nPv_;

        vector3 uvw = suf.get_param();
        float u = (float)uvw[0] * nPu;
        float v = (float)uvw[1] * nPv;

        int uu = (int)floor(u);
        int vv = (int)floor(v);

        return f2c(v_[vv * nPu + uu]);
    }

    ri_bezier_patch_mesh_varying_float2_texture::ri_bezier_patch_mesh_varying_float2_texture(int nu, int uorder, int nv, int vorder, const float P[])
        : nu_(nu), uorder_(uorder), nv_(nv), vorder_(vorder)
    {
        int nPu = (nu - 1) / (uorder - 1);
        int nPv = (nv - 1) / (vorder - 1);

        nPu_ = nPu;
        nPv_ = nPv;

        size_t sz = (nPu + 1) * (nPv + 1);
        v_.resize(sz);
        for (size_t i = 0; i < sz; i++)
        {
            v_[i] = vector2f(P + 2 * i);
        }
    }

    vector2 ri_bezier_patch_mesh_varying_float2_texture::get(const sufflight& suf) const
    {
        int nu = nu_;
        int nv = nv_;
        int uorder = uorder_;
        int vorder = vorder_;
        int nPu = nPu_;
        int nPv = nPv_;

        vector3 uvw = suf.get_param();
        float u = (float)uvw[0] * nPu;
        float v = (float)uvw[1] * nPv;

        int uu = (int)floor(u);
        int vv = (int)floor(v);
        u = u - uu;
        v = v - vv;
        if (uu >= nPu)
        {
            uu = nPu - 1;
            u = 1;
        }
        if (vv >= nPv)
        {
            vv = nPv - 1;
            v = 1;
        }

        vector2f tmp[4];
        tmp[0] = v_[(vv + 0) * (nPu + 1) + uu];
        tmp[1] = v_[(vv + 0) * (nPu + 1) + uu + 1];
        tmp[2] = v_[(vv + 1) * (nPu + 1) + uu];
        tmp[2] = v_[(vv + 1) * (nPu + 1) + uu + 1];

        return f2c(bezier_evaluate(&tmp[0], 2, 2, u, v));
    }

    ri_bezier_patch_mesh_vertex_float2_texture::ri_bezier_patch_mesh_vertex_float2_texture(int nu, int uorder, int nv, int vorder, const float P[])
        : nu_(nu), uorder_(uorder), nv_(nv), vorder_(vorder)
    {
        nPu_ = (nu - 1) / (uorder - 1);
        nPv_ = (nv - 1) / (vorder - 1);

        size_t sz = nu * nv;
        v_.resize(sz);
        for (size_t i = 0; i < sz; i++)
        {
            v_[i] = vector2f(P + 2 * i);
        }
    }

    vector2 ri_bezier_patch_mesh_vertex_float2_texture::get(const sufflight& suf) const
    {
        int nu = nu_;
        int nv = nv_;
        int uorder = uorder_;
        int vorder = vorder_;
        int nPu = nPu_;
        int nPv = nPv_;

        vector3 uvw = suf.get_param();
        float u = (float)uvw[0] * nPu;
        float v = (float)uvw[1] * nPv;

        int uu = (int)floor(u);
        int vv = (int)floor(v);
        u = u - uu;
        v = v - vv;
        if (uu >= nPu)
        {
            uu = nPu - 1;
            u = 1;
        }
        if (vv >= nPv)
        {
            vv = nPv - 1;
            v = 1;
        }

        int i0 = uu * (uorder - 1);
        int j0 = vv * (vorder - 1);

        int sz = uorder * vorder;
        if (sz <= 16)
        {
            vector2f tmp[16]; //
            for (int j = 0; j < vorder; j++)
            {
                for (int i = 0; i < uorder; i++)
                {
                    tmp[j * uorder + i] = v_[(j0 + j) * nu + i0 + i];
                }
            }

            return f2c(bezier_evaluate(&tmp[0], uorder, vorder, u, v));
        }
        else
        {
            std::vector<vector2f> tmp(sz);
            for (int j = 0; j < vorder; j++)
            {
                for (int i = 0; i < uorder; i++)
                {
                    tmp[j * uorder + i] = v_[(j0 + j) * nu + i0 + i];
                }
            }

            return f2c(bezier_evaluate(&tmp[0], uorder, vorder, u, v));
        }
    }

    ri_bezier_patch_mesh_uniform_float3_texture::ri_bezier_patch_mesh_uniform_float3_texture(int nu, int uorder, int nv, int vorder, const float P[])
        : nu_(nu), uorder_(uorder), nv_(nv), vorder_(vorder)
    {
        int nPu = (nu - 1) / (uorder - 1);
        int nPv = (nv - 1) / (vorder - 1);

        nPu_ = nPu;
        nPv_ = nPv;

        size_t sz = nPu * nPv;
        v_.resize(sz);
        for (size_t i = 0; i < sz; i++)
        {
            v_[i] = vector3f(P + 3 * i);
        }
    }

    vector3 ri_bezier_patch_mesh_uniform_float3_texture::get(const sufflight& suf) const
    {
        int nu = nu_;
        int nv = nv_;
        int uorder = uorder_;
        int vorder = vorder_;
        int nPu = nPu_;
        int nPv = nPv_;

        vector3 uvw = suf.get_param();
        float u = (float)uvw[0] * nPu;
        float v = (float)uvw[1] * nPv;

        int uu = (int)floor(u);
        int vv = (int)floor(v);

        return f2c(v_[vv * nPu + uu]);
    }

    ri_bezier_patch_mesh_varying_float3_texture::ri_bezier_patch_mesh_varying_float3_texture(int nu, int uorder, int nv, int vorder, const float P[])
        : nu_(nu), uorder_(uorder), nv_(nv), vorder_(vorder)
    {
        int nPu = (nu - 1) / (uorder - 1);
        int nPv = (nv - 1) / (vorder - 1);

        nPu_ = nPu;
        nPv_ = nPv;

        size_t sz = (nPu + 1) * (nPv + 1);
        v_.resize(sz);
        for (size_t i = 0; i < sz; i++)
        {
            v_[i] = vector3f(P + 3 * i);
        }
    }

    vector3 ri_bezier_patch_mesh_varying_float3_texture::get(const sufflight& suf) const
    {
        int nu = nu_;
        int nv = nv_;
        int uorder = uorder_;
        int vorder = vorder_;
        int nPu = nPu_;
        int nPv = nPv_;

        vector3 uvw = suf.get_param();
        float u = (float)uvw[0] * nPu;
        float v = (float)uvw[1] * nPv;

        int uu = (int)floor(u);
        int vv = (int)floor(v);
        u = u - uu;
        v = v - vv;
        if (uu >= nPu)
        {
            uu = nPu - 1;
            u = 1;
        }
        if (vv >= nPv)
        {
            vv = nPv - 1;
            v = 1;
        }

        vector3f tmp[4];
        tmp[0] = v_[(vv + 0) * (nPu + 1) + uu];
        tmp[1] = v_[(vv + 0) * (nPu + 1) + uu + 1];
        tmp[2] = v_[(vv + 1) * (nPu + 1) + uu];
        tmp[3] = v_[(vv + 1) * (nPu + 1) + uu + 1];

        return f2c(bezier_evaluate(&tmp[0], 2, 2, u, v));
    }

    ri_bezier_patch_mesh_vertex_float3_texture::ri_bezier_patch_mesh_vertex_float3_texture(int nu, int uorder, int nv, int vorder, const float P[])
        : nu_(nu), uorder_(uorder), nv_(nv), vorder_(vorder)
    {
        nPu_ = (nu - 1) / (uorder - 1);
        nPv_ = (nv - 1) / (vorder - 1);

        size_t sz = nu * nv;
        v_.resize(sz);
        for (size_t i = 0; i < sz; i++)
        {
            v_[i] = vector3f(P + 3 * i);
        }
    }

    vector3 ri_bezier_patch_mesh_vertex_float3_texture::get(const sufflight& suf) const
    {
        int nu = nu_;
        int nv = nv_;
        int uorder = uorder_;
        int vorder = vorder_;
        int nPu = nPu_;
        int nPv = nPv_;

        vector3 uvw = suf.get_param();
        float u = (float)uvw[0] * nPu;
        float v = (float)uvw[1] * nPv;

        int uu = (int)floor(u);
        int vv = (int)floor(v);
        u = u - uu;
        v = v - vv;
        if (uu >= nPu)
        {
            uu = nPu - 1;
            u = 1;
        }
        if (vv >= nPv)
        {
            vv = nPv - 1;
            v = 1;
        }

        int i0 = uu * (uorder - 1);
        int j0 = vv * (vorder - 1);

        int sz = uorder * vorder;
        if (sz <= 16)
        {
            vector3f tmp[16]; //
            for (int j = 0; j < vorder; j++)
            {
                for (int i = 0; i < uorder; i++)
                {
                    tmp[j * uorder + i] = v_[(j0 + j) * nu + i0 + i];
                }
            }

            return f2c(bezier_evaluate(&tmp[0], uorder, vorder, u, v));
        }
        else
        {
            std::vector<vector3f> tmp(sz);
            for (int j = 0; j < vorder; j++)
            {
                for (int i = 0; i < uorder; i++)
                {
                    tmp[j * uorder + i] = v_[(j0 + j) * nu + i0 + i];
                }
            }

            return f2c(bezier_evaluate(&tmp[0], uorder, vorder, u, v));
        }
    }

    //--------------------------------------------------------------------------------
}