#include "ri_ray_tracer.h"

#include "values.h"
#include "color.h"
#include "shader.h"
#include "integrator.h"

#include "test_info.h"

#include <algorithm> //max
#include <cmath>

#include <iostream>

#include "intersection.h"
#include "basic_collection.hpp"

#include "light_integrator.h"

#include "spectrum.h"
#include "spectrum_component.h"

namespace kaze
{
    static inline void finalize(test_info* info, const ray& r, const mediation& media)
    {
        static const real ZERO[3] = {0, 0, 0};

        if (info->p_intersection)
        {
            info->p_intersection->finalize(info, r, info->distance);
            if (memcmp(&(info->geometric), ZERO, sizeof(real) * 3) == 0)
            {
                info->geometric = info->normal;
            }
        }
    }

    static bool test_opaque(const color3& Oi)
    {
        return (Oi[0] >= 1.0) && (Oi[1] >= 1.0) && (Oi[2] >= 1.0);
    }

    ri_ray_tracer::ri_ray_tracer()
    {
        ; //
    }

    ri_ray_tracer::~ri_ray_tracer()
    {
        ; //
    }

    static color3 limit(const color3& c)
    {
        real r = std::min<real>(c[0], 1);
        real g = std::min<real>(c[1], 1);
        real b = std::min<real>(c[2], 1);
        return color3(r, g, b);
    }

    bool ri_ray_tracer::trace(spectrum* spec, const ray& r, real tmin, real tmax, const mediation& media) const
    {
        tmin = std::max<real>(tmin, tmin + 1e-3);

        test_info info;
        if (inter_.get() && inter_->test(&info, r, tmax))
        {
            finalize(&info, r, media);

            const shader* pShader = info.get_shader();
            const surface_shader* ps = (pShader) ? pShader->get_surface_shader() : NULL;

            if (ps)
            {
                sufflight sl(
                    r.origin(), r.direction(),
                    info.get_position(),
                    info.get_geometric(),
                    info.get_normal(),
                    info.get_tangent(),
                    info.get_binormal(),
                    info.get_coord());

                sl.set_attr(info.i_attr);
                sl.set_index(info.index);

                sl.set_color1(info.col1);
                sl.set_color2(info.col2);
                sl.set_color3(info.col3);

                basic_collection<radiance> rs;
                spectrum spc = ps->shade(rs, sl, *this, media);
                color3 Ci = get_Ci(spc);
                color3 Oi = get_Oi(spc);
                bool bOpaque = test_opaque(Oi);
                if (!bOpaque)
                {
                    ray r2(info.get_position() + r.direction() * tmin, r.direction(), r.time());
                    spectrum spc2;
                    if (this->trace(&spc2, r2, tmin, tmax, media.next_t()))
                    {
                        color3 Ci2 = get_Ci(spc2);
                        color3 Oi2 = get_Oi(spc2);

                        color3 Oi3 = limit(Oi + Oi2);
                        color3 Ci3 = Ci + (color3(1, 1, 1) - Oi) * Ci2;

                        *spec = spectrum(new CiOi_spectrum_component(Ci3, Oi3));
                    }
                    else
                    {
                        *spec = spc;
                    }
                }
                else
                {
                    *spec = spc;
                }
            }
            else
            {
                *spec = spectrum_black();
            }
            return true;
        }
        return false;
    }

    bool ri_ray_tracer::trace(const ray& r, real tmin, real tmax, const mediation& media) const
    {
        if (inter_.get() && inter_->test(r, tmax))
        {
            return true;
        }
        return false;
    }

    void ri_ray_tracer::set_intersection(const auto_count_ptr<intersection>& inter)
    {
        inter_ = inter;
    }
}