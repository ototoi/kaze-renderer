#ifndef KAZE_RI_RAY_TRACER_H
#define KAZE_RI_RAY_TRACER_H

#include "tracer.h"
#include "count_ptr.hpp"
#include "intersection.h"

namespace kaze
{

    class ri_ray_tracer : public tracer
    {
    public:
        ri_ray_tracer();
        ~ri_ray_tracer();

    public:
        virtual bool trace(spectrum* spec, const ray& r, real tmin, real tmax, const mediation& media) const;
        virtual bool trace(const ray& r, real tmin, real tmax, const mediation& media) const;

    public:
        void set_intersection(const auto_count_ptr<intersection>& inter);

    protected:
        std::shared_ptr<intersection> inter_;
    };
}

#endif