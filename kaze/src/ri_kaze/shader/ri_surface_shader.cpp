#include "ri_surface_shader.h"

#include "basic_collection.hpp"
#include "constant_texture.hpp"
#include "aggregator.hpp"
#include "spectrum_component.h"

#include "tracer.h"

#include "shader_evaluator.h"
#include "null_shader_evaluator.h"

#include <vector>
#include <map>
#include <string>
#include <unordered_map>

namespace kaze
{
    using namespace rsle;
    using namespace kaze;
    using namespace std;

    typedef unordered_map<std::string, std::shared_ptr<texture<real> > > v1_map;
    typedef unordered_map<std::string, std::shared_ptr<texture<color2> > > v2_map;
    typedef unordered_map<std::string, std::shared_ptr<texture<color3> > > v3_map;
    typedef unordered_map<std::string, std::shared_ptr<texture<std::string> > > s_map;
    typedef unordered_map<std::string, matrix4> mat_map;

    namespace
    {

        class constant_string_texture : public texture<std::string>
        {
        public:
            constant_string_texture(const std::string& str) : str_(str) {}
            std::string get(const sufflight& suf) const
            {
                return str_;
            }

        private:
            std::string str_;
        };

        static vector2 v3v2(const vector3& v)
        {
            return vector2(v[0], v[1]);
        }

        static vector3 v2v3(const vector2& v, real x = 0)
        {
            return vector3(v[0], v[1], x);
        }

        struct LC
        {
            vector3 L;
            color3 C;
        };

        class LCVector : public aggregator<lightpath>
        {
        public:
            void add(const lightpath& rhs)
            {
                LC t;
                t.L = rhs.from() - rhs.to(); //P->Lorg
                t.C = convert(rhs.power());

                if (t.L.sqr_length() > 0)
                {
                    v_.push_back(t);
                }
                else
                {
                    a_.push_back(t.C);
                }
            }
            const std::vector<LC>& get_vector() const { return v_; }
            const std::vector<color3>& get_ambient() const { return a_; }
        protected:
            std::vector<LC> v_;
            std::vector<color3> a_;
        };

        class ri_attr_shader_output : public shader_output
        {
        public:
            ri_attr_shader_output() : Ci_(color3(1, 1, 1)), Oi_(color3(1, 1, 1)) {}
            ri_attr_shader_output(const color3& Ci, const color3& Oi) : Ci_(Ci), Oi_(Oi) {}
        public:
            void Ci(const color3& c) { Ci_ = c; }
            void Oi(const color3& c) { Oi_ = c; }
            void env(const color3& c) { env_ = c; }
            void set_f3(const char* key, const vector3& v) {}
            void set_f2(const char* key, const vector2& v) {}
            void set_f1(const char* key, real f) {}
        public:
            const color3& Ci() const { return Ci_; }
            const color3& Oi() const { return Oi_; }
            const color3& env() const { return env_; }
        private:
            color3 Ci_;
            color3 Oi_;
            color3 env_;
        };

        class ri_attr_shader_input : public shader_input
        {
        public:
            ri_attr_shader_input(
                const sufflight& sl,
                const v1_map& tx1,
                const v2_map& tx2,
                const v3_map& tx3,
                const s_map& txs,
                const mat_map& mats,
                const std::vector<LC>& lv,
                const std::vector<color3>& la,
                const tracer* trc) : sl_(sl), tx1_(tx1), tx2_(tx2), tx3_(tx3), txs_(txs), mats_(mats), lv_(lv), la_(la), trc_(trc) {}
        public:
            vector3 P() const { return sl_.position(); }
            vector3 N() const { return sl_.normal(); }
            vector3 Ng() const { return sl_.geometric(); }
            vector3 E() const { return sl_.origin(); }
            vector3 I() const { return sl_.direction(); }
            vector3 dPdu() const { return sl_.dPdu(); }
            vector3 dPdv() const { return sl_.dPdv(); }

            vector3 uvw() const { return sl_.get_uvw(); }
            vector2 uv() const { return v3v2(uvw()); }

            vector3 stw() const
            {
                v3_map::const_iterator it = tx3_.find("stw");
                if (it != tx3_.end())
                {
                    return it->second->get(sl_);
                }
                else
                {
                    v2_map::const_iterator it = tx2_.find("st");
                    if (it != tx2_.end())
                    {
                        return v2v3(it->second->get(sl_), sl_.get_stw()[2]);
                    }
                    else
                    {
                        return sl_.get_stw();
                    }
                }
            }

            vector2 st() const
            {
                v2_map::const_iterator it = tx2_.find("st");
                if (it != tx2_.end())
                {
                    return it->second->get(sl_);
                }
                else
                {
                    v3_map::const_iterator it = tx3_.find("stw");
                    if (it != tx3_.end())
                    {
                        return v3v2(it->second->get(sl_));
                    }
                    else
                    {
                        return v3v2(sl_.get_stw());
                    }
                }
            }

            real s() const
            {
                vector2 st = this->st();
                return st[0];
            }

            real t() const
            {
                vector2 st = this->st();
                return st[1];
            }

            vector3 Cs() const { return get_f3("Cs"); }
            vector3 Os() const { return get_f3("Os"); }
            real get_f1(const char* key, real def) const
            {
                v1_map::const_iterator it = tx1_.find(key);
                if (it != tx1_.end())
                {
                    return it->second->get(sl_);
                }
                else
                {
                    return def;
                }
            }
            vector2 get_f2(const char* key, const vector2& def) const
            {
                v2_map::const_iterator it = tx2_.find(key);
                if (it != tx2_.end())
                {
                    return it->second->get(sl_);
                }
                else
                {
                    return def;
                }
            }
            vector3 get_f3(const char* key) const { return shader_input::get_f3(key); }
            vector3 get_f3(const char* key, const vector3& def) const
            {
                v3_map::const_iterator it = tx3_.find(key);
                if (it != tx3_.end())
                {
                    return it->second->get(sl_);
                }
                else
                {
                    return def;
                }
            }
            std::string get_string(const char* key) const
            {
                s_map::const_iterator it = txs_.find(key);
                if (it != txs_.end())
                {
                    return it->second->get(sl_);
                }
                else
                {
                    return "";
                }
            }
            std::string get_string(const char* key, const std::string& def) const
            {
                s_map::const_iterator it = txs_.find(key);
                if (it != txs_.end())
                {
                    return it->second->get(sl_);
                }
                else
                {
                    return def;
                }
            }

            bool has_key(const char* key) const { return true; } //
            size_t illuminance_size() const { return lv_.size(); }
            vector3 illuminance_L(size_t i) const { return lv_[i].L; }
            vector3 illuminance_Cl(size_t i) const { return lv_[i].C; }
            vector3 ambient() const
            {
                color3 c(0, 0, 0);
                size_t sz = la_.size();
                for (size_t i = 0; i < sz; i++)
                {
                    c += la_[i];
                }
                return c;
            }
            real texture_f1(const char* key, real u, real v) const
            {
                sufflight tsl(sl_);
                tsl.set_st(vector3(u, v, 0));
                v1_map::const_iterator it = tx1_.find(key);
                if (it != tx1_.end())
                {
                    return it->second->get(tsl);
                }
                else
                {
                    return 0;
                }
            }

            vector2 texture_f2(const char* key, real u, real v) const
            {
                sufflight tsl(sl_);
                tsl.set_st(vector3(u, v, 0));
                v2_map::const_iterator it = tx2_.find(key);
                if (it != tx2_.end())
                {
                    return it->second->get(tsl);
                }
                else
                {
                    return vector2(0, 0);
                }
            }

            vector3 texture_f3(const char* key, real u, real v) const
            {
                sufflight tsl(sl_);
                tsl.set_st(vector3(u, v, 0));
                v3_map::const_iterator it = tx3_.find(key);
                if (it != tx3_.end())
                {
                    return it->second->get(tsl);
                }
                else
                {
                    return vector3(0, 0, 0);
                }
            }

            matrix4 get_matrix(const char* key) const
            {
                mat_map::const_iterator it = mats_.find(key);
                if (it != mats_.end())
                {
                    return it->second;
                }
                else
                {
                    return mat4_gen::identity();
                }
            }

            //virtual vector3 indirectdiffuse(const vector3& P, const vector3& N, double samples)const{return vector3(0,0,1);}
            real occlusion(const vector3& P, const vector3& N, double samples) const
            {
                if (trc_)
                {
                    mediation md;

                    int nTest = 0;
                    int nSamples = (int)ceil(samples);
                    for (int i = 0; i < nSamples; i++)
                    {
                        vector3 R = N; //TODO:
                        ray r(P, R);
                        if (!trc_->trace(r, md))
                        {
                            nTest++;
                        }
                    }
                    return real(nTest) / nSamples;
                }
                else
                {
                    return 0;
                }
            }

            real trace_f1(const vector3& P, const vector3& R) const
            {
                if (trc_)
                {
                    mediation md;
                    spectrum spc;
                    ray r(P, R);
                    if (trc_->trace(&spc, r, md))
                    {
                        return spc.intensity();
                    }
                    else
                    {
                        return 0;
                    }
                }
                else
                {
                    return 0;
                }
            }

            vector3 trace_f3(const vector3& P, const vector3& R) const
            {
                if (trc_)
                {
                    mediation md;
                    spectrum spc;
                    ray r(P, R);
                    if (trc_->trace(&spc, r, 1e-3, 1e+8, md))
                    {
                        return spc.to_rgb();
                    }
                    else
                    {
                        return vector3(0, 0, 0);
                    }
                }
                else
                {
                    return vector3(0, 0, 0);
                }
            }

            vector3 transmission(const vector3& Psrc, const vector3& Pdst) const
            {
                /*
                    if(trc_)
                    {
                        vector3 L = Pdst-Psrc;
                        real l = L.length();
                        if(l>0)
                        {
                            vector3 R = L/l;
                            ray r(Psrc, R); 
                            basic_aggregator<const transmitter*> trans;
                            trc_->get_transmitter(r, l);

                            spuctrum spc = spectrum_white();
                            size_t sz = trans.size();
                            for(size_t i = 0:i<sz;i++)
                            {
                                spc *= trans[i]->cast(R, 0, l);
                            }
                            return spc.to_rgb();
                        }
                    else
                    {
                        return vector3(1,1,1);
                    }
                */
                return vector3(0, 0, 0);
            }

            vector3 environment_f3(const char* texturename, const vector3& V) const
            {
                if (strcmp(texturename, "raytrace") == 0)
                {
                    vector3 P_ = P();
                    vector3 Nf = faceforward(normalize(N()), I());
                    P_ += 1e-3 * Nf; /**/
                    return trace_f3(P_, V);
                }
                else
                {
                    sufflight tsl(sl_);
                    tsl.set_direction(V);
                    v3_map::const_iterator it = tx3_.find(texturename);
                    if (it != tx3_.end())
                    {
                        return it->second->get(tsl);
                    }
                    else
                    {
                        return vector3(0, 0, 0);
                    }
                }
            }

        protected:
            sufflight sl_;
            const v1_map& tx1_;
            const v2_map& tx2_;
            const v3_map& tx3_;
            const s_map& txs_;
            const mat_map& mats_;
            const std::vector<LC>& lv_;
            const std::vector<color3>& la_;
            const tracer* trc_;
        };
    }

    class ri_surface_shader_imp
    {
    public:
        ri_surface_shader_imp()
        {
            init();
        }

    protected:
        void init()
        {
            tex3_["Cs"] = std::shared_ptr<texture<color3> >(new constant_texture<color3>(color3(1, 1, 1)));
            tex3_["Os"] = std::shared_ptr<texture<color3> >(new constant_texture<color3>(color3(1, 1, 1)));
            eval_.reset(new null_shader_evaluator());
        }

    public:
        spectrum shade(const enumerator<radiance>& rads, const sufflight& sl, const mediation& md) const
        {
            LCVector lv;
            size_t sz = lights_.size();
            for (size_t i = 0; i < sz; i++)
            {
                lights_[i]->cast(&lv, sl);
            }

            ri_attr_shader_input in(sl, tex1_, tex2_, tex3_, texs_, mats_, lv.get_vector(), lv.get_ambient(), trc_.get());
            ri_attr_shader_output out;

            eval_->evaluate(&out, &in);
            return spectrum(new CiOi_spectrum_component(out.Ci(), out.Oi()));
        }

    public:
        void add_light(const auto_count_ptr<light>& l)
        {
            lights_.push_back(l);
        }

        void add_texture(const char* key, const auto_count_ptr<texture<real> >& tex)
        {
            tex1_[key] = tex;
        }

        void add_texture(const char* key, const auto_count_ptr<texture<color2> >& tex)
        {
            tex2_[key] = tex;
        }

        void add_texture(const char* key, const auto_count_ptr<texture<color3> >& tex)
        {
            tex3_[key] = tex;
        }

        void add_texture(const char* key, const auto_count_ptr<texture<std::string> >& tex)
        {
            texs_[key] = tex;
        }

        void add_string(const char* key, const char* str)
        {
            texs_[key] = std::shared_ptr<texture<std::string> >(new constant_string_texture(str));
        }

        void add_matrix(const char* key, const matrix4& mat)
        {
            mats_[key] = mat;
        }

        void set_evaluator(const auto_count_ptr<shader_evaluator>& eval)
        {
            eval_ = eval;
        }

        void set_tracer(const auto_count_ptr<tracer>& trc)
        {
            trc_ = trc;
        }

    protected:
        std::vector<std::shared_ptr<light> > lights_;
        v1_map tex1_;
        v2_map tex2_;
        v3_map tex3_;
        s_map texs_;
        mat_map mats_;

        std::shared_ptr<tracer> trc_;

        std::shared_ptr<shader_evaluator> eval_;
    };

    //----------------------------------------------------------------------------------------------------

    //----------------------------------------------------------------------------------------------------

    ri_surface_shader::ri_surface_shader()
    {
        imp_ = new ri_surface_shader_imp();
    }

    ri_surface_shader::~ri_surface_shader()
    {
        delete imp_;
    }

    spectrum ri_surface_shader::shade(const enumerator<radiance>& rads, const sufflight& sl, const mediation& md) const
    {
        return imp_->shade(rads, sl, md);
    }

    //----------------------------------------------------------------------------------------------------
    void ri_surface_shader::add_texture(const char* key, const auto_count_ptr<texture<real> >& tex)
    {
        imp_->add_texture(key, tex);
    }

    void ri_surface_shader::add_texture(const char* key, const auto_count_ptr<texture<color2> >& tex)
    {
        imp_->add_texture(key, tex);
    }

    void ri_surface_shader::add_texture(const char* key, const auto_count_ptr<texture<color3> >& tex)
    {
        imp_->add_texture(key, tex);
    }

    void ri_surface_shader::add_texture(const char* key, const auto_count_ptr<texture<std::string> >& tex)
    {
        imp_->add_texture(key, tex);
    }

    void ri_surface_shader::add_string(const char* key, const char* str)
    {
        imp_->add_string(key, str);
    }

    void ri_surface_shader::add_matrix(const char* key, const matrix4& mat)
    {
        imp_->add_matrix(key, mat);
    }

    void ri_surface_shader::add_light(const auto_count_ptr<light>& l)
    {
        imp_->add_light(l);
    }

    void ri_surface_shader::set_evaluator(const auto_count_ptr<shader_evaluator>& eval)
    {
        imp_->set_evaluator(eval);
    }

    void ri_surface_shader::set_tracer(const auto_count_ptr<tracer>& trc)
    {
        imp_->set_tracer(trc);
    }
}