#ifndef KAZE_RI_SURFACE_SHADER_H
#define KAZE_RI_SURFACE_SHADER_H

#include "shader.h"
#include "light.h"
#include "texture.hpp"
#include "count_ptr.hpp"

#include "shader_evaluator.h"

namespace kaze
{

    class ri_surface_shader_imp;
    class ri_surface_shader : public surface_shader
    {
    public:
        spectrum shade(const enumerator<radiance>& rads, const sufflight& sl, const mediation& md) const;

    public:
        void add_texture(const char* key, const auto_count_ptr<texture<real> >& tex);
        void add_texture(const char* key, const auto_count_ptr<texture<vector2> >& tex);
        void add_texture(const char* key, const auto_count_ptr<texture<vector3> >& tex);
        void add_texture(const char* key, const auto_count_ptr<texture<std::string> >& tex);
        void add_string(const char* key, const char* str);
        void add_matrix(const char* key, const matrix4& mat);
        void add_light(const auto_count_ptr<light>& l);
        void set_evaluator(const auto_count_ptr<rsle::shader_evaluator>& eval);
        void set_tracer(const auto_count_ptr<tracer>& trc);

    public:
        ri_surface_shader();
        ~ri_surface_shader();

    protected:
        ri_surface_shader_imp* imp_;
    };
}
#endif
