#ifndef RI_POLYGONIZE_BLOBBY_H
#define RI_POLYGONIZE_BLOBBY_H

#include "ri_blobby.h"
#include "ri_transform.h"
#include "ri_camera_options.h"
#include <vector>
#include <map>

namespace ri
{

    struct ri_blobby_polygon
    {
        std::vector<int> indices;
        std::vector<float> vertices;
        std::vector<float> normals;
        ri_classifier cls;
        ri_parameters params;
    };

    std::shared_ptr<ri_blobby_polygon>
    ri_polygonize_blobby(const ri_blobby* blobby,
                         const ri_transform& trans, const ri_camera_options& cam,
                         float threthold);
}

#endif
