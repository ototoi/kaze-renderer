#define _USE_MATH_DEFINES
#include <math.h>
#include <iostream>

#include "ri_polygonize_blobby.h"

//#include "evaluator/mc_volume_evaluator.h"
#include "extractor/mc_mesh_extractor.h"
//#include "extractor/parallel_mesh_extractor.h"
//#include "extractor/tmp_mc_mesh_extractor.h"
//#include "extractor/tmp_dc_mesh_extractor.h"
#include "mc_node_to_evaluator.h"
//#include "io/mc_vnd_io.h"
//#include "io/mc_obj_io.h"
#include "mc_volume_node.h"
#include "mc_node_to_evaluator.h"
#include "ri_path_resolver.h"
#include "transform_matrix.h"
#include "values.h"

#define POLYGON_OUTPUT 0

#define POLYGONIZE_RESOLUTION (2.0 / 1.0)

//#include "logger.h"

namespace ri
{
    using namespace kaze;

    typedef mc::mc_attribute_key mc_attribute_key;
    typedef mc::mc_mesh mc_mesh;
    typedef mc::mc_bound mc_bound;
    typedef mc::mc_volume_node mc_volume_node;
    typedef mc::mc_volume_evaluator mc_volume_evaluator;
    typedef mc::mc_mesh_extractor mc_mesh_extractor;

    static matrix4 ConvertMatrix(const float m[])
    {
        matrix4 mm(m);
        return transpose(mm);
    }

    static matrix4 ConvertMatrix(const ri_transform& trns)
    {
        ri_transform s(trns);
        s.transpose();
        const float* f = s.get();
        matrix4 tmp(f);
        return tmp;
    }

    static std::string GetFileName(const std::string& str)
    {
        //#ifdef _WIN32
        //          char fname[_MAX_FNAME] = {};
        //          _splitpath(str.c_str(), NULL, NULL, fname, NULL);
        //          return fname;
        //#else
        char buffer[1024] = {};
        strcpy(buffer, str.c_str());
        char* fname_ext = strrchr(buffer, '\\');
        if (fname_ext == NULL)
        {
            fname_ext = strrchr(buffer, '/');
        }
        if (fname_ext)
            fname_ext++;
        else
            fname_ext = buffer;
        char* extension = strrchr(fname_ext, '.');
        if (extension)
        {
            *extension = '\0';
        }

        return fname_ext;
        //#endif
    }

    static void ConvertAttributeValues(std::vector<real>& val,
                                       const std::vector<float>& values)
    {
        size_t sz = values.size();
        val.resize(sz);
        for (size_t i = 0; i < sz; i++)
        {
            val[i] = (real)values[i];
        }
    }

    static void ConvertAttributeValues(std::vector<float>& val,
                                       const std::vector<real>& values)
    {
        size_t sz = values.size();
        val.resize(sz);
        for (size_t i = 0; i < sz; i++)
        {
            val[i] = (float)values[i];
        }
    }

    static std::shared_ptr<mc_volume_node>
    ConvertBlobbyToNode(const ri_blobby* blobby);

    static std::vector<std::shared_ptr<mc_volume_node> >
    ConvertBlobbyToNodes(const std::vector<std::shared_ptr<ri_blobby> >& c)
    {
        std::vector<std::shared_ptr<mc_volume_node> > cld;
        for (size_t i = 0; i < c.size(); i++)
        {
            std::shared_ptr<mc_volume_node> k = ConvertBlobbyToNode(c[i].get());
            if (k.get())
            {
                cld.push_back(k);
            }
        }
        return cld;
    }

    static void SetAttributes(std::shared_ptr<mc_volume_node>& node,
                              const ri_blobby* blobby)
    {
        const ri_base_leaf_blobby* b =
            dynamic_cast<const ri_base_leaf_blobby*>(blobby);
        const ri_classifier& cls = b->get_classifier();
        const ri_parameters& params = b->get_parameters();
        for (size_t i = 0; i < cls.size(); i++)
        {
            mc_attribute_key attr(cls[i].get_name(), cls[i].get_size());
            const ri_parameters::value_type* p_param = params.get(cls[i].get_name());
            if (p_param && p_param->nType == ri_parameters::TYPE_FLOAT)
            {
                std::vector<real> values;
                ConvertAttributeValues(values, p_param->float_values);
                node->set_attribute(attr, values);
            }
        }
    }

    static std::shared_ptr<mc_volume_node>
    ConvertBlobbyToNode(const ri_blobby* blobby)
    {
        using namespace mc;
        int type = blobby->typeN();
        switch (type)
        {
        case RI_BLOBBY_ADD:
        {
            const ri_add_blobby* s = dynamic_cast<const ri_add_blobby*>(blobby);
            std::vector<std::shared_ptr<mc_volume_node> > cld =
                ConvertBlobbyToNodes(s->get_children());
            if (!cld.empty())
            {
                std::shared_ptr<mc_volume_node> node =
                    std::shared_ptr<mc_volume_node>(new add_mc_volume_node());
                node->get_children() = cld;
                return node;
            }
        }
        break;
        case RI_BLOBBY_MUL:
        {
            const ri_mul_blobby* s = dynamic_cast<const ri_mul_blobby*>(blobby);
            std::vector<std::shared_ptr<mc_volume_node> > cld =
                ConvertBlobbyToNodes(s->get_children());
            if (!cld.empty())
            {
                std::shared_ptr<mc_volume_node> node =
                    std::shared_ptr<mc_volume_node>(new mul_mc_volume_node());
                node->get_children() = cld;
                return node;
            }
        }
        break;
        case RI_BLOBBY_MIN:
        {
            const ri_min_blobby* s = dynamic_cast<const ri_min_blobby*>(blobby);
            std::vector<std::shared_ptr<mc_volume_node> > cld =
                ConvertBlobbyToNodes(s->get_children());
            if (!cld.empty())
            {
                std::shared_ptr<mc_volume_node> node =
                    std::shared_ptr<mc_volume_node>(new min_mc_volume_node());
                node->get_children() = cld;
                return node;
            }
        }
        break;
        case RI_BLOBBY_MAX:
        {
            const ri_max_blobby* s = dynamic_cast<const ri_max_blobby*>(blobby);
            std::vector<std::shared_ptr<mc_volume_node> > cld =
                ConvertBlobbyToNodes(s->get_children());
            if (!cld.empty())
            {
                std::shared_ptr<mc_volume_node> node =
                    std::shared_ptr<mc_volume_node>(new max_mc_volume_node());
                node->get_children() = cld;
                return node;
            }
        }
        break;
        case RI_BLOBBY_SUB:
        {
            const ri_sub_blobby* s = dynamic_cast<const ri_sub_blobby*>(blobby);
            std::vector<std::shared_ptr<mc_volume_node> > cld =
                ConvertBlobbyToNodes(s->get_children());
            if (!cld.empty())
            {
                std::shared_ptr<mc_volume_node> node =
                    std::shared_ptr<mc_volume_node>(new sub_mc_volume_node());
                node->get_children() = cld;
                return node;
            }
        }
        break;
        case RI_BLOBBY_DIV:
        {
            const ri_div_blobby* s = dynamic_cast<const ri_div_blobby*>(blobby);
            std::vector<std::shared_ptr<mc_volume_node> > cld =
                ConvertBlobbyToNodes(s->get_children());
            if (!cld.empty())
            {
                std::shared_ptr<mc_volume_node> node =
                    std::shared_ptr<mc_volume_node>(new div_mc_volume_node());
                node->get_children() = cld;
                return node;
            }
        }
        break;
        case RI_BLOBBY_NEG:
        {
            const ri_neg_blobby* s = dynamic_cast<const ri_neg_blobby*>(blobby);
            std::vector<std::shared_ptr<mc_volume_node> > cld =
                ConvertBlobbyToNodes(s->get_children());
            if (!cld.empty())
            {
                std::shared_ptr<mc_volume_node> node =
                    std::shared_ptr<mc_volume_node>(new neg_mc_volume_node());
                node->get_children() = cld;
                return node;
            }
        }
        break;
        case RI_BLOBBY_INV:
        {
            const ri_inv_blobby* s = dynamic_cast<const ri_inv_blobby*>(blobby);
            std::vector<std::shared_ptr<mc_volume_node> > cld =
                ConvertBlobbyToNodes(s->get_children());
            if (!cld.empty())
            {
                std::shared_ptr<mc_volume_node> node =
                    std::shared_ptr<mc_volume_node>(new inv_mc_volume_node());
                node->get_children() = cld;
                return node;
            }
        }
        break;
        case RI_BLOBBY_FMA:
        {
            ; //
        }
        break;
        case RI_BLOBBY_IDENTITY:
        case RI_BLOBBY_CONSTANT:
        case RI_BLOBBY_VALUE:
        {
#ifdef _DEBUG
            assert(0);
#endif
        }
        break;
        case RI_BLOBBY_ELLIPSOID:
        {
            const ri_ellipsoid_blobby* s =
                dynamic_cast<const ri_ellipsoid_blobby*>(blobby);
            matrix4 mat = ConvertMatrix(s->get_mat());
            std::shared_ptr<mc_volume_node> node =
                std::shared_ptr<mc_volume_node>(new ellipsoid_mc_volume_node(mat));
            SetAttributes(node, blobby);
            return node;
        }
        break;
        case RI_BLOBBY_SPHERE:
        {
            const ri_sphere_blobby* s =
                dynamic_cast<const ri_sphere_blobby*>(blobby);
            const float* p = s->get_position();
            float r = s->get_radius();
            std::shared_ptr<mc_volume_node> node =
                std::shared_ptr<mc_volume_node>(new sphere_mc_volume_node(vector3(p), r));
            SetAttributes(node, blobby);
            return node;
        }
        break;
        case RI_BLOBBY_SEGMENT:
        {
            const ri_segment_blobby* s =
                dynamic_cast<const ri_segment_blobby*>(blobby);
            const float* p0 = s->get_position0();
            const float* p1 = s->get_position1();
            matrix4 mat = ConvertMatrix(s->get_mat());
            float r = s->get_radius();
            std::shared_ptr<mc_volume_node> node = std::shared_ptr<mc_volume_node>(
                new segment_mc_volume_node(vector3(p0), vector3(p1), r, mat));
            SetAttributes(node, blobby);
            return node;
        }
        break;
        case RI_BLOBBY_IMPLICIT:
        {
            const ri_implicit_field_blobby* s =
                dynamic_cast<const ri_implicit_field_blobby*>(blobby);
            std::string strFName = GetFileName(s->get_name());
            if (strFName == "segblob" || strFName == "impl_segblob")
            {
                const std::vector<float>& f = s->get_floats();
                const float* p0 = &f[0];
                const float* p1 = &f[4];
                float r0 = f[3];
                float r1 = f[7];
                std::shared_ptr<mc_volume_node> node = std::shared_ptr<mc_volume_node>(
                    new impl_segment_mc_volume_node(vector3(p0), r0, vector3(p1), r1));
                SetAttributes(node, blobby);
                return node;
            }
            else if (strFName == "cube" || strFName == "impl_cube")
            {
                std::shared_ptr<mc_volume_node> node =
                    std::shared_ptr<mc_volume_node>(new impl_cube_mc_volume_node());
                SetAttributes(node, blobby);
                return node;
            }
            return std::shared_ptr<mc_volume_node>();
        }
        break;
        }

        return std::shared_ptr<mc_volume_node>();
    }

    static std::shared_ptr<mc_volume_node>
    ConvertBlobbyToNode(const ri_blobby* blobby, const matrix4& mat)
    {
        return std::shared_ptr<mc_volume_node>(
            new mc::transform_mc_volume_node(ConvertBlobbyToNode(blobby), mat));
    }

    static std::shared_ptr<mc_volume_node>
    ConvertBlobbyToNode(const ri_blobby* blobby, const matrix4& mat,
                        float threthold)
    {
        std::shared_ptr<mc_volume_node> node(new mc::offset_mc_volume_node(threthold));
        node->get_children().push_back(ConvertBlobbyToNode(blobby, mat));
        return node;
    }

    static std::shared_ptr<mc_volume_evaluator>
    ConvertBlobbyToEvaluator(const ri_blobby* blobby, const matrix4& mat,
                             float threthold)
    {
        return mc::mc_node_to_evaluator(ConvertBlobbyToNode(blobby, mat, threthold));
    }

    static void GetLeafBlobby(std::vector<const ri_blobby*>& leafs,
                              const ri_blobby* blobby)
    {
        if (blobby->is_branch())
        {
            const ri_base_branch_blobby* b =
                dynamic_cast<const ri_base_branch_blobby*>(blobby);
            const std::vector<std::shared_ptr<ri_blobby> >& cld = b->get_children();
            for (size_t i = 0; i < cld.size(); i++)
            {
                GetLeafBlobby(leafs, cld[i].get());
            }
        }
        else if (blobby->is_leaf())
        {
            leafs.push_back(blobby);
        }
    }

    static void GetAttributes(ri_classifier& cls,
                              const ri_blobby* blobby)
    {
        std::vector<const ri_blobby*> leafs;
        GetLeafBlobby(leafs, blobby);
        typedef std::map<std::string, ri_classifier::param_data> map_type;
        map_type m;
        for (size_t i = 0; i < leafs.size(); i++)
        {
            const ri_base_leaf_blobby* b = dynamic_cast<const ri_base_leaf_blobby*>(leafs[i]);
            const ri_classifier& bcls = b->get_classifier();
            for (int j = 0; j < bcls.size(); j++)
            {
                std::string name = bcls[j].get_name();
                m.insert(map_type::value_type(name, bcls[j]));
            }
        }
        for (map_type::const_iterator it = m.begin(); it != m.end(); it++)
        {
            cls.add(it->second);
        }
    }

    static inline vector3 MulNormal(const matrix4& m, const vector3& v)
    {
        return vector3(m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
                       m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
                       m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
    }

    static void TransformVertices(mc_mesh& m, const matrix4& l2w)
    {
        matrix4 w2l = ~l2w;
        if (m.get_vertices().size() != 0)
        {
            size_t sz = m.get_vertices().size();
            std::vector<mc::real>& vertices = m.get_vertices();
            for (size_t i = 0; i < sz; i++)
            {
                vector3 p = vector3(&vertices[3*i]);
                p = l2w * p;
                vertices[3*i+0] = p[0];
                vertices[3*i+1] = p[1];
                vertices[3*i+2] = p[2];
            }
            sz = m.get_normals().size();
            std::vector<mc::real>& normals = m.get_normals();
            for (size_t i = 0; i < sz; i++)
            {
                vector3 p = vector3(&normals[3*i]);
                p = normalize(MulNormal(w2l, p));
                normals[3*i+0] = p[0];
                normals[3*i+1] = p[1];
                normals[3*i+2] = p[2];
            }
        }
    }

    /*
        static void RecalcNormals(mc_mesh& mesh, const mc_volume_evaluator& e)
        {
            if (mesh.vertices.size() != 0)
            {
                size_t sz = mesh.vertices.size();
                mesh.normals.resize(sz);
                for (size_t i = 0; i < sz; i++)
                {
                    vector3 p = mesh.vertices[i];
                    mesh.normals[i] = normalize(-e.get_gradient(p));
                    //if (mesh.normals[i][2]>0)mesh.normals[i] *= -1;
                }
            }
        }
        */

    static void CreateAttributes(mc_mesh& mesh, const mc_volume_evaluator& e,
                                 const std::vector<mc_attribute_key>& attrbutes)
    {
        size_t sz = mesh.get_vertices().size() / 3;
        if (sz != 0)
        {
            
            for (size_t j = 0; j < attrbutes.size(); j++)
            {
                std::vector<real> values(sz * attrbutes[j].count);
                for (size_t i = 0; i < sz; i++)
                {
                    mc::real x = mesh.get_vertices()[3*i+0];
                    mc::real y = mesh.get_vertices()[3*i+1];
                    mc::real z = mesh.get_vertices()[3*i+2];
                    mc::vector3 p =  mc::vector3(x,y,z);
                    std::vector<mc::real> value;
                    e.get_attribute(p, attrbutes[j], value);
                    for (int k = 0; k < attrbutes[j].count; k++)
                    {
                        values[i * attrbutes[j].count + k] = value[k];
                    }
                }

                mesh.set_attribute(attrbutes[j], values);
            }
        }
    }

    static bool GenerateMesh(mc_mesh& mesh, const std::shared_ptr<mc_volume_node>& node,
                             int dim[3])
    {
        using namespace mc;
        /*
          parallel_mesh_extractor gen(vector3(0,0,0), vector3(1,1,1), dim);
          if (gen.run(mesh, node)) {
              return true;
          }
          */
        static const real EPS = 1e-3;
        vector3 min = vector3(0 - EPS, 0 - EPS, 0 - EPS);
        vector3 max = vector3(1 + EPS, 1 + EPS, 1 + EPS);

        // tmp_dc_mesh_extractor gen(vector3(0, 0, 0), vector3(1, 1, 1), dim);
        // parallel_mesh_extractor gen(min, max, dim);
        mc_mesh_extractor gen(min, max, dim);
        std::shared_ptr<mc_volume_evaluator> e = mc::mc_node_to_evaluator(node, min, max);
        if (e.get())
        {
            if (gen.run(mesh, *e))
            {
                return true;
            }
        }
        return false;
    }

    std::shared_ptr<ri_blobby_polygon>
    ri_polygonize_blobby(const ri_blobby* blobby,
                         const ri_transform& trns, const ri_camera_options& cam,
                         float threthold)
    {
        int width = 640;
        int height = 480;
        float paspect = 1;
        cam.get_format(width, height, paspect);
        width = abs(width);
        height = abs(height);
        paspect = fabs(paspect);

        int nType = cam.get_camera_type();

        float fov = cam.get_fov();
        float angle = fov;
        float aspect = cam.get_frame_aspect_ratio();
        float left = -aspect;
        float right = +aspect;
        float bottom = -1.0f;
        float top = +1.0f;
        float near = (float)RI_EPSILON;
        float far = (float)RI_INFINITY;
        cam.get_screen_window(left, right, bottom, top);
        cam.get_clipping(near, far);

        matrix4 w2c = ConvertMatrix(cam.get_world_to_camera());
        matrix4 l2w = ConvertMatrix(trns);

        std::shared_ptr<mc_volume_evaluator> came =
            ConvertBlobbyToEvaluator(blobby, w2c * l2w, threthold);
        mc_bound bb = came->get_bound();

        if (bb.min[2] >= bb.max[2])
            return std::shared_ptr<ri_blobby_polygon>();

        near = std::max<real>(near, bb.min[2]);
        far = std::min<real>(far, bb.max[2]);

        int depth = std::max<int>(width, height);

        matrix4 c2c;
        if (nType == ri_camera_options::RI_CAMERA_PERSPECTIVE)
        {
            c2c = camera2clip_01(values::radians(angle), aspect, near, far);

            real tn = std::tan(values::radians(angle) * 0.5);
            real fh = 2 * tn * far;
            real fw = fh * aspect;
            real fd = std::fabs(far - near);
            depth =
                std::max<int>((int)ceil(width * fd / fw), (int)ceil(height * fd / fh));
        }
        else
        {
            c2c = camera2clip_01(left, right, bottom, top, near, far);

            real fw = std::fabs(right - left);
            real fh = std::fabs(top - bottom);
            real fd = std::fabs(far - near);
            depth =
                std::max<int>((int)ceil(width * fd / fw), (int)ceil(height * fd / fh));
        }

        depth = std::max<int>(depth, 8);

        // int dim[] = {width*1.3, height*1.3, depth*1.3};
        int dim[] = {width * POLYGONIZE_RESOLUTION, height * POLYGONIZE_RESOLUTION,
                     depth * POLYGONIZE_RESOLUTION};

        std::shared_ptr<mc_volume_node> node =
            ConvertBlobbyToNode(blobby, c2c * w2c * l2w, threthold);
        // save_to_vnd(std::cout, *node);

        /*
          std::shared_ptr<mc_volume_node> node2 = mc_node_add_bvh(node);
          save_to_vnd(std::cout, *node2);

          std::shared_ptr<mc_volume_node> node3 = mc_node_remove_bvh(node2,
            vector3(0,0,0), vector3(1,1,1));
          save_to_vnd(std::cout, *node3);
          */

        mc_mesh msh;
        if (GenerateMesh(msh, node, dim))
        {
            std::vector<mc_attribute_key> attrbutes;
            ri_classifier cls(0);
            GetAttributes(cls, blobby);
            for (size_t i = 0; i < cls.size(); i++)
            {
                mc_attribute_key attr(cls[i].get_name(), cls[i].get_size());
                attrbutes.push_back(attr);
            }
            std::shared_ptr<mc_volume_evaluator> ve =
                ConvertBlobbyToEvaluator(blobby, c2c * w2c * l2w, threthold);
            CreateAttributes(msh, *ve, attrbutes);
            // RecalcNormals(msh, *ve);
            size_t isz = msh.get_indices().size();
            size_t vsz = msh.get_vertices().size();
            size_t nsz = msh.get_normals().size();

            /*
            if (nsz == 0)
            {
                RecalcNormals(msh, *ve);
                nsz = msh.normals.size();
            }
            */
            matrix4 im = ~(c2c * w2c);
            TransformVertices(msh, im);

#if POLYGON_OUTPUT
            static int count = 0;
            std::string strDump =
                ri_path_resolver::get_rib_path() + char('0' + count) + ".dump.obj";
            count++;

// save_to_obj(strDump.c_str(), msh);
#endif

            std::shared_ptr<ri_blobby_polygon> ap(new ri_blobby_polygon());
            ap->cls = ri_classifier(0);

            ap->indices.resize(isz);
            const std::vector<size_t>& indices = msh.get_indices();
            for (size_t i = 0; i < isz; i++)
            {
                ap->indices[i] = (int)indices[i];
            }
            ap->vertices.resize(vsz);
            const std::vector<mc::real>& vertices = msh.get_vertices();
            for (size_t i = 0; i < vsz; i++)
            {
                ap->vertices[i] = (float)vertices[i];
            }

            if (nsz)
            {
                ap->normals.resize(nsz);
                const std::vector<mc::real>& normals = msh.get_normals();
                for (size_t i = 0; i < nsz; i++)
                {
                    ap->normals[i] = (float)normals[i];
                }
            }

            if (cls.size())
            {
                for (size_t i = 0; i < cls.size(); i++)
                {
                    const std::vector<mc::real>& attr_val = msh.get_attribute(attrbutes[i]);
                    if (attr_val.size())
                    {
                        std::vector<float> values;
                        ConvertAttributeValues(values, attr_val);

                        ri_classifier::param_data data = cls[i];
                        data.class_ = ri_classifier::VERTEX;
                        ap->cls.add(data);
                        ap->params.set(cls[i].get_name(), &values[0], (int)values.size());
                    }
                }
            }

            return ap;
        }

        return std::shared_ptr<ri_blobby_polygon>();
    }
}
