#ifndef KAZE_RI_TEAPOT_INTERSECTION_H
#define KAZE_RI_TEAPOT_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class ri_teapot_intersection : public bounded_intersection
    {
    public:
        ri_teapot_intersection(const matrix4& mat);
        ~ri_teapot_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> inter_;
    };
}
#endif
