#ifndef KAZE_RI_MOTION_PARABOLOID_INTERSECTION_H
#define KAZE_RI_MOTION_PARABOLOID_INTERSECTION_H

#include "bounded_intersection.h"
#include <vector>

namespace kaze
{

    class ri_motion_paraboloid_intersection : public bounded_intersection
    {
    public:
        struct ri_motion_sequence
        {
            real t;
            real rmax;
            real zmin;
            real zmax;
            real tmax;
        };
        typedef ri_motion_sequence sequence_t;

    public:
        ri_motion_paraboloid_intersection(const std::vector<ri_motion_sequence>& seq, const matrix4& mat);
        ~ri_motion_paraboloid_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> inter_;
    };
}

#endif