#ifndef KAZE_RI_NU_PATCH_INTERSECTION_H
#define KAZE_RI_NU_PATCH_INTERSECTION_H

#include "bounded_intersection.h"
#include "nurbs_curve.hpp"
#include <vector>

namespace kaze
{

    typedef nurbs_curve<vector2> ri_nu_patch_trim_curve;
    typedef std::vector<ri_nu_patch_trim_curve> ri_nu_patch_trim_loop;

    class ri_nu_patch_intersection : public bounded_intersection
    {
    public:
        ri_nu_patch_intersection(
            int nu, int uorder, const float uknot[], float umin, float umax,
            int nv, int vorder, const float vknot[], float vmin, float vmax,
            const float Pw[],
            const std::vector<ri_nu_patch_trim_loop>& outers,
            const std::vector<ri_nu_patch_trim_loop>& inners,
            const matrix4& mat);
        ~ri_nu_patch_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> inter_;
    };
}

#endif
