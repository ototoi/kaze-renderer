#include "ri_points_intersection.h"
#include "test_info.h"
#include "points/points_intersection.h"
#include "points/sphere_points_container.h"
#include "points/disk_points_container.h"
#include "transformed_intersection.h"

#include <vector>
#include <memory>
#include <string>

namespace kaze
{

    ri_points_intersection::ri_points_intersection(
        int nverts,
        const float P[], const float width[], const float Cs[], const float Os[], float constantwidth,
        const float N[], const float constantnormal[], const char* type,
        const matrix4& mat)
    {
        std::string strType;
        if (type)
        {
            strType = type;
        }

        std::vector<vector3f> Pr(nverts);
        std::vector<float> Rr(nverts);
        std::vector<vector3f> C1r(nverts);
        std::vector<vector3f> C2r(nverts);
        std::vector<vector3f> Nr;
        for (int i = 0; i < nverts; i++)
        {
            Pr[i] = vector3f((float)P[3 * i + 0], (float)P[3 * i + 1], (float)P[3 * i + 2]);
        }

        if (width)
        {
            for (int i = 0; i < nverts; i++)
            {
                Rr[i] = (float)width[i] * 0.5f;
            }
        }
        else
        {
            for (int i = 0; i < nverts; i++)
            {
                Rr[i] = constantwidth * 0.5f;
            }
        }

        if (Cs)
        {
            for (int i = 0; i < nverts; i++)
            {
                C1r[i] = vector3f((float)Cs[3 * i + 0], (float)Cs[3 * i + 1], (float)Cs[3 * i + 2]);
            }
        }
        else
        {
            for (int i = 0; i < nverts; i++)
            {
                C1r[i] = vector3f((float)1, (float)1, (float)1);
            }
        }

        if (N)
        {
            Nr.resize(nverts);
            for (int i = 0; i < nverts; i++)
            {
                Nr[i] = vector3f((float)N[3 * i + 0], (float)N[3 * i + 1], (float)N[3 * i + 2]);
            }
        }
        else if (constantnormal)
        {
            Nr.push_back(vector3f((float)constantnormal[0], (float)constantnormal[1], (float)constantnormal[2]));
        }

        if (strType == "sphere")
        {
            inter_.reset(
                new m4_transformed_intersection(
                    new points_intersection(new sphere_points_container(Pr, Rr, C1r, C2r)), mat));
        }
        else
        {
            //disk
            //particle
            //patch
            assert(strType != "blobby");
            if (!Nr.empty())
            {
                inter_.reset(
                    new m4_transformed_intersection(
                        new points_intersection(new normal_disk_points_container(Pr, Rr, Nr, C1r, C2r)), mat));
            }
            else
            {
                inter_.reset(
                    new m4_transformed_intersection(
                        new points_intersection(new disk_points_container(Pr, Rr, C1r, C2r)), mat));
            }
        }
    }

    ri_points_intersection::~ri_points_intersection()
    {
    }

    bool ri_points_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_points_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void ri_points_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        /**/
    }

    vector3 ri_points_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_points_intersection::max() const
    {
        return inter_->max();
    }
}