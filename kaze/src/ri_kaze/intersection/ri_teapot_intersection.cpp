#include "ri_teapot_intersection.h"
#include "test_info.h"
#include "teapot_intersection.h"

namespace kaze
{

    ri_teapot_intersection::ri_teapot_intersection(const matrix4& mat)
    {
        m4_transformer tr(mat);
        inter_.reset(
            new teapot_intersection(tr));
    }

    ri_teapot_intersection::~ri_teapot_intersection()
    {
        ;
    }

    bool ri_teapot_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_teapot_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void ri_teapot_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        /**/
    }

    vector3 ri_teapot_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_teapot_intersection::max() const
    {
        return inter_->max();
    }
}