#include "ri_motion_intersection.h"
#include "intersection_ex.h"
#include "transformed_intersection.h"
#include "test_info.h"
#include "proxy_intersection.h"
#include "values.h"

namespace kaze
{
    namespace
    {

        typedef ri_motion_intersection::sequence_t sequence_t;

        class ri_motion_intersection_imp : public bounded_intersection
        {
        public:
            ri_motion_intersection_imp(std::vector<sequence_t> seq, const auto_count_ptr<intersection>& inter);
            ~ri_motion_intersection_imp();

        public:
            bool test(const ray& r, real dist) const;
            bool test(test_info* info, const ray& r, real dist) const;
            void finalize(test_info* info, const ray& r, real dist) const;
            vector3 min() const;
            vector3 max() const;

        private:
            std::vector<sequence_t> seq_;
            std::shared_ptr<intersection> inter_;
            vector3 min_;
            vector3 max_;
        };

        static int GetIndex(const std::vector<sequence_t>& seq, real t)
        {
            size_t sz = seq.size();
            if (t < seq[0].t) return 0;
            for (size_t i = 0; i < sz - 1; i++)
            {
                if (seq[i].t <= t && t < seq[i + 1].t) return (int)i;
            }
            return (int)sz - 2;
        }

        static matrix4 GetMotionMatrix(real t, const std::vector<sequence_t>& seq)
        {
            int idx = GetIndex(seq, t);
            real a = (t - seq[idx].t) / (seq[idx + 1].t - seq[idx].t);
            return (1 - a) * seq[idx].mat + a * seq[idx + 1].mat;
        }

        static std::vector<sequence_t> RecalcSequence(const std::vector<sequence_t>& seq)
        {
            size_t sz = seq.size();
            real dt = 1;
            for (size_t i = 0; i < sz - 1; i++)
            {
                dt = std::min<real>(dt, seq[i + 1].t - seq[i].t);
            }
            int div = std::min<int>(std::max<int>((int)ceil(1.0 / dt) + 1, 2), 64);
            std::vector<sequence_t> tseq;
            for (int i = 0; i < div; i++)
            {
                real t = real(i) / (div - 1);
                matrix4 mat = GetMotionMatrix(t, seq);

                sequence_t tmp;
                tmp.t = t;
                tmp.mat = mat;
                tseq.push_back(tmp);
            }

            return tseq;
        }

        static vector3 min__(const vector3& a, const vector3& b)
        {
            return vector3(
                std::min<real>(a[0], b[0]),
                std::min<real>(a[1], b[1]),
                std::min<real>(a[2], b[2]));
        }

        static vector3 max__(const vector3& a, const vector3& b)
        {
            return vector3(
                std::max<real>(a[0], b[0]),
                std::max<real>(a[1], b[1]),
                std::max<real>(a[2], b[2]));
        }

        ri_motion_intersection_imp::ri_motion_intersection_imp(std::vector<sequence_t> seq, const auto_count_ptr<intersection>& inter)
            : inter_(inter)
        {
            seq_ = seq = RecalcSequence(seq);

            std::vector<std::shared_ptr<intersection> > inters;
            for (size_t i = 0; i < seq.size(); i++)
            {
                inters.push_back(std::shared_ptr<intersection>(new m4_transformed_intersection(inter, seq[i].mat)));
            }

            static const real FAR = values::far();

            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            for (size_t i = 0; i < inters.size() - 1; i++)
            {
                vector3 cmin = min__(inters[i]->min(), inters[i + 1]->min());
                vector3 cmax = max__(inters[i]->max(), inters[i + 1]->max());

                vector3 cnt = (cmin + cmax) * 0.5;
                vector3 ext = (cmax - cmin) * 0.5;

                cmin = cnt - ext * 2.0;
                cmax = cnt + ext * 2.0;

                for (int j = 0; j < 3; j++)
                {
                    if (min[j] > cmin[j]) min[j] = cmin[j];
                    if (max[j] < cmax[j]) max[j] = cmax[j];
                }
            }

            min_ = min;
            max_ = max;
        }

        ri_motion_intersection_imp::~ri_motion_intersection_imp()
        {
            ; //
        }

        bool ri_motion_intersection_imp::test(const ray& r, real dist) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, dist))
            {
                real tmin = std::max<real>(rng.tmin, 0);
                real tmax = std::min<real>(rng.tmax, dist);

                {
                    real t = r.time();
                    matrix4 mat = GetMotionMatrix(t, seq_);
                    m4_transformed_intersection intr(new proxy_intersection(inter_.get()), mat);

                    return intr.test(r, tmin, tmax);
                }
            }
            return false;
        }

        bool ri_motion_intersection_imp::test(test_info* info, const ray& r, real dist) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, dist))
            {
                real tmin = std::max<real>(rng.tmin, 0);
                real tmax = std::min<real>(rng.tmax, dist);

                {
                    real t = r.time();
                    matrix4 mat = GetMotionMatrix(t, seq_);

                    m4_transformed_intersection intr(new proxy_intersection(inter_.get()), mat);

                    if (intr.test(info, r, tmin, tmax))
                    {
                        if (info->p_intersection)
                        {
                            info->p_intersection->finalize(info, r, info->distance);
                        }
                        info->p_intersection = this;
                        return true;
                    }
                }
            }
            return false;
        }

        void ri_motion_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
        {
            //
            //info->position = r.origin()+info->distance*r.direction();
        }

        vector3 ri_motion_intersection_imp::min() const
        {
            return min_;
        }

        vector3 ri_motion_intersection_imp::max() const
        {
            return max_;
        }
    }

    ri_motion_intersection::ri_motion_intersection(const std::vector<ri_motion_sequence>& seq, const auto_count_ptr<intersection>& inter)
    {
        inter_.reset(new ri_motion_intersection_imp(seq, inter));
    }

    ri_motion_intersection::~ri_motion_intersection()
    {
        ; //
    }

    bool ri_motion_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_motion_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void ri_motion_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        inter_->finalize(info, r, dist);
    }

    vector3 ri_motion_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_motion_intersection::max() const
    {
        return inter_->max();
    }
}
