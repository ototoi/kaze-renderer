#ifndef KAZE_RI_BLOBBY_INTERSECTION_H
#define KAZE_RI_BLOBBY_INTERSECTION_H

#include "bounded_intersection.h"
#include <vector>

namespace kaze
{
    class ri_blobby_intersection : public bounded_intersection
    {
    public:
        ri_blobby_intersection();
        //ri_blobby_intersection(const std::shared_ptr<blobby_node>& node, const matrix4& mat);
        ~ri_blobby_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> inter_;
    };
}

#endif
