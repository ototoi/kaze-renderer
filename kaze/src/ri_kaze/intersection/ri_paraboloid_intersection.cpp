#include "ri_paraboloid_intersection.h"
#include "test_info.h"
#include "paraboloid_intersection.h"
#include "transformed_intersection.h"
#include "count_ptr.hpp"

#include <vector>
#include <memory>

namespace kaze
{
    namespace
    {

        class uv_correct_intersection : public bounded_intersection
        {
        public:
            uv_correct_intersection(const auto_count_ptr<intersection>& inter, real umin = 0, real umax = 1, real vmin = 0, real vmax = 1, int nReverse = 0)
                : inter_(inter), umin_(umin), umax_(umax), vmin_(vmin), vmax_(vmax), nReverse_(nReverse) {}
        public:
            bool test(const ray& r, real dist) const
            {
                return inter_->test(r, dist);
            }

            bool test(test_info* info, const ray& r, real dist) const
            {
                if (inter_->test(info, r, dist))
                {
                    if (info->p_intersection)
                    {
                        info->p_intersection->finalize(info, r, info->distance);

                        real u = info->coord[0];
                        real v = info->coord[1];

                        u = (u - umin_) / (umax_ - umin_);
                        v = (v - vmin_) / (vmax_ - vmin_);

                        u = std::max<real>(0, std::min<real>(u, 1));
                        v = std::max<real>(0, std::min<real>(v, 1));

                        if (nReverse_ & 1) u = 1.0 - u;
                        if (nReverse_ & 2) v = 1.0 - v;

                        info->coord[0] = u;
                        info->coord[1] = v;

                        info->p_intersection = this;
                        return true;
                    }
                }
                return false;
            }

            void finalize(test_info* info, const ray& r, real dist) const
            {
                ;
            }

            vector3 min() const
            {
                return inter_->min();
            }

            vector3 max() const
            {
                return inter_->max();
            }

        private:
            std::shared_ptr<intersection> inter_;
            real umin_;
            real umax_;
            real vmin_;
            real vmax_;
            int nReverse_;
        };
    }

    ri_paraboloid_intersection::ri_paraboloid_intersection(real rmax, real zmin, real zmax, real tmax, const matrix4& mat)
    {
        real umin = 0.0;
        real umax = tmax / 360.0;
        real vmin = 0.0;
        real vmax = 1.0;

        int nReverse = 0;
        if (tmax < 0)
        {
            umin = (360.0 + tmax) / 360.0;
            umax = 1.0;
            nReverse = 1 - nReverse;
        }

        if (umin > umax)
        {
            std::swap(umin, umax);
        }

        inter_.reset(
            new uv_correct_intersection(
                new m4_transformed_intersection(
                    new paraboloid_intersection(rmax, zmin, zmax, umax), mat),
                umin,
                umax,
                vmin,
                vmax,
                nReverse));
    }

    ri_paraboloid_intersection::~ri_paraboloid_intersection()
    {
        ;
    }

    bool ri_paraboloid_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_paraboloid_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void ri_paraboloid_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        /**/
    }

    vector3 ri_paraboloid_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_paraboloid_intersection::max() const
    {
        return inter_->max();
    }
}
