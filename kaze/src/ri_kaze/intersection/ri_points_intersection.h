#ifndef KAZE_RI_POINTS_INTERSECTION_H
#define KAZE_RI_POINTS_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class ri_points_intersection : public bounded_intersection
    {
    public:
        ri_points_intersection(int nverts,
                               const float P[], const float width[], const float Cs[], const float Os[], float constantwidth,
                               const float N[], const float constantnormal[], const char* type,
                               const matrix4& mat);
        ~ri_points_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> inter_;
    };
}

#endif