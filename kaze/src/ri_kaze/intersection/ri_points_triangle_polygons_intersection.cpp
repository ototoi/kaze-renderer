#include "ri_points_triangle_polygons_intersection.h"
#include "test_info.h"
#include "mesh_loader.h"
#include "mesh_saver.h"
#include "fast_mesh_intersection.h"
#include "values.h"
#include "logger.h"

namespace kaze
{
    namespace
    {

        static void copy_vector(double out[2], const vector2& in)
        {
            out[0] = in[0];
            out[1] = in[1];
        }

        static void copy_vector(double out[3], const vector3& in)
        {
            out[0] = in[0];
            out[1] = in[1];
            out[2] = in[2];
        }

        static inline vector3 mul(const matrix4& m, const vector3& v)
        {
            return m * v;
        }

        static inline vector3 mul_normal(const matrix4& m, const vector3& v)
        {
            return vector3(
                m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
                m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
                m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
        }

        class ri_polygon_mesh_loader : public mesh_loader
        {
        public:
            ri_polygon_mesh_loader(int nfaces, const int verts[], int nverts, const float P[], const float N[], const float Np[], const float Nf[], const matrix4& mat)
                : nfaces_(nfaces), verts_(verts), nverts_(nverts), P_(P), N_(N), Np_(Np), Nf_(Nf), mat_(mat)
            {
                imat_ = ~mat;
            }

        public:
            bool get_vertices(size_t& sz) const
            {
                sz = nverts_;
                return true;
            }
            bool get_faces(size_t& sz) const
            {
                sz = nfaces_;
                return true;
            }
            bool get_vertex(size_t i, io_mesh_vertex* v) const
            {
                vector3 p = mat_ * vector3(P_ + 3 * i);
                copy_vector(v->pos, p);
                return true;
            }
            bool get_face(size_t i, io_mesh_face* f) const
            {
                size_t i0 = verts_[3 * i + 0];
                size_t i1 = verts_[3 * i + 1];
                size_t i2 = verts_[3 * i + 2];

                f->i0 = i0;
                f->i1 = i1;
                f->i2 = i2;

                if (Nf_)
                {
                    size_t ni0 = 3 * i + 0;
                    size_t ni1 = 3 * i + 1;
                    size_t ni2 = 3 * i + 2;

                    vector3 n0 = normalize(mul_normal(imat_, normalize(vector3(Nf_ + 3 * ni0))));
                    vector3 n1 = normalize(mul_normal(imat_, normalize(vector3(Nf_ + 3 * ni1))));
                    vector3 n2 = normalize(mul_normal(imat_, normalize(vector3(Nf_ + 3 * ni2))));
                    copy_vector(f->n0, n0);
                    copy_vector(f->n1, n1);
                    copy_vector(f->n2, n2);
                }
                else if (N_)
                {
                    vector3 n0 = normalize(mul_normal(imat_, normalize(vector3(N_ + 3 * i0))));
                    vector3 n1 = normalize(mul_normal(imat_, normalize(vector3(N_ + 3 * i1))));
                    vector3 n2 = normalize(mul_normal(imat_, normalize(vector3(N_ + 3 * i2))));
                    copy_vector(f->n0, n0);
                    copy_vector(f->n1, n1);
                    copy_vector(f->n2, n2);
                }
                else if (Np_)
                {
                    vector3 n = normalize(mul_normal(imat_, normalize(vector3(Np_ + 3 * i))));
                    copy_vector(f->n0, n);
                    copy_vector(f->n1, n);
                    copy_vector(f->n2, n);
                }
                else
                {
                    vector3 p0 = mul(mat_, vector3(P_ + 3 * i0));
                    vector3 p1 = mul(mat_, vector3(P_ + 3 * i1));
                    vector3 p2 = mul(mat_, vector3(P_ + 3 * i2));
                    vector3 e1 = p1 - p0;
                    vector3 e2 = p2 - p0;
                    vector3 n = normalize(cross(e1, e2));
                    copy_vector(f->n0, n);
                    copy_vector(f->n1, n);
                    copy_vector(f->n2, n);
                }
                copy_vector(f->uv0, vector2(0, 0));
                copy_vector(f->uv1, vector2(1, 0));
                copy_vector(f->uv2, vector2(0, 1));

                f->i_shader = i; //index of face

                return true;
            }

        private:
            int nfaces_;
            const int* verts_;
            int nverts_;
            const float* P_;
            const float* N_;
            const float* Np_;
            const float* Nf_; //face varying
            matrix4 mat_;
            matrix4 imat_;
        };
    }

    ri_points_triangle_polygons_intersection::ri_points_triangle_polygons_intersection(
        int nfaces, const int verts[], int nverts,
        const float P[], const float N[], const float Np[], const float Nf[], const matrix4& mat)
    {
        ri_polygon_mesh_loader ml(nfaces, verts, nverts, P, N, Np, Nf, mat);
        parameter_map pm;
        if (N || Np || Nf)
        {
            pm.set_int("CALC_NORMALS", 0);
        }
        else
        {
            pm.set_int("CALC_NORMALS", 1);
        }
        if (nfaces <= 16)
        {
            pm.set("MESH_ACCELERATOR", "exhibited");
        }
        else
        {
            pm.set("MESH_ACCELERATOR", "mtbvh");
        }
        pm.set_int("SINGLE_SIDE", 1);

        inter_.reset(
            new fast_mesh_intersection(ml, pm));
    }

    ri_points_triangle_polygons_intersection::ri_points_triangle_polygons_intersection(
        int nfaces, const int verts[], int nverts,
        const float P[], const float N[], const float Np[], const float Nf[], float smooth_angle, const matrix4& mat)
    {
        ri_polygon_mesh_loader ml(nfaces, verts, nverts, P, N, Np, Nf, mat);
        parameter_map pm;
        if (N || Np || Nf)
        {
            pm.set_int("CALC_NORMALS", 0);
        }
        else
        {
            pm.set_int("CALC_NORMALS", 1);
            pm.set_double("SMOOTH_ANGLE", smooth_angle);
        }
        if (nfaces <= 16)
        {
            pm.set("MESH_ACCELERATOR", "exhibited");
        }
        else
        {
            pm.set("MESH_ACCELERATOR", "plqbvh");
        }

        inter_.reset(
            new fast_mesh_intersection(ml, pm));
    }

    ri_points_triangle_polygons_intersection::~ri_points_triangle_polygons_intersection()
    {
    }

    bool ri_points_triangle_polygons_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_points_triangle_polygons_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void ri_points_triangle_polygons_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        /**/
    }

    vector3 ri_points_triangle_polygons_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_points_triangle_polygons_intersection::max() const
    {
        return inter_->max();
    }
}
