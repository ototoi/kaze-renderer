#ifndef KAZE_RI_MOTION_INTERSECTION_H
#define KAZE_RI_MOTION_INTERSECTION_H

#include "bounded_intersection.h"
#include "count_ptr.hpp"
#include <vector>

namespace kaze
{

    class ri_motion_intersection : public bounded_intersection
    {
    public:
        struct ri_motion_sequence
        {
            real t;
            matrix4 mat;
        };
        typedef ri_motion_sequence sequence_t;

    public:
        ri_motion_intersection(const std::vector<ri_motion_sequence>& seq, const auto_count_ptr<intersection>& inter);
        ~ri_motion_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> inter_;
    };
}

#endif