#include "ri_motion_points_triangle_polygons_intersection.h"
#include "test_info.h"
#include "intersection_ex.h"
#include "composite/bvh_composite_intersection.h"
#include "mesh_face.h"
#include <memory>

namespace kaze
{
    namespace
    {

        typedef ri_motion_points_triangle_polygons_intersection::sequence_t sequence_t;

        static inline vector3 mul(const matrix4& m, const vector3& v)
        {
            return m * v;
        }

        static inline vector3 mul_normal(const matrix4& m, const vector3& v)
        {
            return vector3(
                m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
                m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
                m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
        }

        //--------------------------------------------------

        class ri_motion_triangle : public bounded_intersection
        {
        public:
            ri_motion_triangle(const vector3* P[6], const vector3* N[6], size_t i);
            bool test(const ray& r, real dist) const;
            bool test(test_info* info, const ray& r, real dist) const;
            void finalize(test_info* info, const ray& r, real dist) const;
            vector3 min() const;
            vector3 max() const;

        private:
            const vector3* P_[6];
            const vector3* N_[6];
            size_t i_;
            vector3 min_;
            vector3 max_;
        };

        ri_motion_triangle::ri_motion_triangle(const vector3* P[6], const vector3* N[6], size_t i)
        {
            memcpy(P_, P, sizeof(const vector3*) * 6);
            memcpy(N_, N, sizeof(const vector3*) * 6);
            i_ = i;
            vector3 min;
            vector3 max;
            min = max = *P[0];
            for (int i = 1; i < 6; i++)
            {
                vector3 p = *P[i];
                for (int j = 0; j < 3; j++)
                {
                    min[j] = std::min(min[j], p[j]);
                    max[j] = std::max(max[j], p[j]);
                }
            }
            min_ = min;
            max_ = max;
        }

        bool ri_motion_triangle::test(const ray& r, real dist) const
        {
            real t = r.time();
            vector3 P[3];
            for (int i = 0; i < 3; i++)
            {
                P[i] = (1 - t) * (*P_[0 + i]) + t * (*P_[3 + i]);
            }
            mesh_face mf;
            mf.p_p0 = P + 0;
            mf.p_p1 = P + 1;
            mf.p_p2 = P + 2;
            return mf.test_DS(r, dist);
        }

        bool ri_motion_triangle::test(test_info* info, const ray& r, real dist) const
        {
            real t = r.time();
            vector3 P[3];
            vector3 N[3];
            for (int i = 0; i < 3; i++)
            {
                P[i] = (1 - t) * (*P_[0 + i]) + t * (*P_[3 + i]);
                N[i] = normalize((1 - t) * (*N_[0 + i]) + t * (*N_[3 + i]));
            }
            mesh_face mf;
            mf.p_p0 = P + 0;
            mf.p_p1 = P + 1;
            mf.p_p2 = P + 2;
            mf.n0 = N[0];
            mf.n1 = N[1];
            mf.n2 = N[2];
            mf.uv0 = vector2(0, 0);
            mf.uv1 = vector2(1, 0);
            mf.uv2 = vector2(0, 1);
            if (mf.test_DS(info, r, dist))
            {
                this->finalize(info, r, info->distance);
                return true;
            }
            return false;
        }

        void ri_motion_triangle::finalize(test_info* info, const ray& r, real dist) const
        {
            assert(reinterpret_cast<mesh_face_freearea_struct*>(info->freearea)->face);
            mesh_face_freearea_struct* fsp = reinterpret_cast<mesh_face_freearea_struct*>(info->freearea);
            const mesh_face* p_face = fsp->face;
            p_face->finalize(info, r, dist);
            info->index = i_;
        }

        vector3 ri_motion_triangle::min() const
        {
            return min_;
        }

        vector3 ri_motion_triangle::max() const
        {
            return max_;
        }

        //--------------------------------------------------------------------------------

        struct sequence_x
        {
            real t;
            std::vector<vector3> P;
            std::vector<vector3> N;
        };

        class ri_motion_points_triangle_polygons_intersection_imp : public bounded_intersection
        {
        public:
            ri_motion_points_triangle_polygons_intersection_imp(int nfaces, const int verts[], int nverts, std::vector<sequence_t> seq, const matrix4& mat);
            ~ri_motion_points_triangle_polygons_intersection_imp();

        public:
            bool test(const ray& r, real dist) const;
            bool test(test_info* info, const ray& r, real dist) const;
            void finalize(test_info* info, const ray& r, real dist) const;
            vector3 min() const;
            vector3 max() const;

        private:
            std::vector<sequence_x> seq_;
            std::vector<std::shared_ptr<intersection> > inters_;
            vector3 min_;
            vector3 max_;
        };

        static int GetIndex(const std::vector<sequence_x>& seq, real t)
        {
            size_t sz = seq.size();
            if (t < seq[0].t) return 0;
            for (size_t i = 0; i < sz - 1; i++)
            {
                if (seq[i].t <= t && t < seq[i + 1].t) return (int)i;
            }
            return (int)sz - 2;
        }

        ri_motion_points_triangle_polygons_intersection_imp::ri_motion_points_triangle_polygons_intersection_imp(
            int nfaces, const int verts[], int nverts,
            const std::vector<sequence_t> seq, const matrix4& mat)
        {
            size_t ssz = seq.size();
            assert(ssz >= 2);
            seq_.resize(ssz);
            inters_.resize(ssz - 1);

            std::vector<int> nNormalTypes(ssz);
            matrix4 imat = ~mat;
            for (size_t i = 0; i < ssz; i++)
            {
                seq_[i].t = seq[i].t;
                size_t psz = seq[i].P.size() / 3;
                seq_[i].P.resize(psz);
                for (size_t j = 0; j < psz; j++)
                {
                    vector3 v = vector3(seq[i].P[3 * j + 0], seq[i].P[3 * j + 1], seq[i].P[3 * j + 2]);
                    seq_[i].P[j] = mul(mat, v);
                }

                int nNormalType = 0;
                if (seq[i].N.size())
                { //vertex
                    nNormalType = 0;
                    size_t nsz = psz;
                    assert(nsz == (seq[i].N.size() / 3));
                    seq_[i].N.resize(nsz);
                    for (size_t j = 0; j < nsz; j++)
                    {
                        vector3 v = normalize(vector3(seq[i].N[3 * j + 0], seq[i].N[3 * j + 1], seq[i].N[3 * j + 2]));
                        seq_[i].N[j] = normalize(mul_normal(imat, v));
                    }
                }
                else if (seq[i].Nf.size())
                { //facevarying
                    nNormalType = 1;
                    size_t nsz = nfaces * 3;
                    assert(nsz == (seq[i].Nf.size() / 3));
                    seq_[i].N.resize(nsz);
                    for (size_t j = 0; j < nsz; j++)
                    {
                        vector3 v = normalize(vector3(seq[i].Nf[3 * j + 0], seq[i].Nf[3 * j + 1], seq[i].Nf[3 * j + 2]));
                        seq_[i].N[j] = normalize(mul_normal(imat, v));
                    }
                }
                else if (seq[i].Np.size())
                {
                    nNormalType = 2;
                    size_t nsz = nfaces;
                    assert(nsz == (seq[i].Np.size() / 3));
                    seq_[i].N.resize(nsz);
                    for (size_t j = 0; j < nsz; j++)
                    {
                        vector3 v = normalize(vector3(seq[i].Np[3 * j + 0], seq[i].Np[3 * j + 1], seq[i].Np[3 * j + 2]));
                        seq_[i].N[j] = normalize(mul_normal(imat, v));
                    }
                }
                else
                {
                    nNormalType = 3;
                    size_t nsz = nfaces;
                    seq_[i].N.resize(nsz);
                    for (size_t j = 0; j < nsz; j++)
                    {
                        size_t i0 = verts[3 * j + 0];
                        size_t i1 = verts[3 * j + 1];
                        size_t i2 = verts[3 * j + 2];

                        vector3 p0 = seq_[i].P[i0];
                        vector3 p1 = seq_[i].P[i1];
                        vector3 p2 = seq_[i].P[i2];
                        vector3 e1 = p1 - p0;
                        vector3 e2 = p2 - p0;
                        vector3 n = normalize(cross(e1, e2));
                        seq_[i].N[j] = n;
                    }
                }
                nNormalTypes[i] = nNormalType;
            }

            for (size_t i = 0; i < ssz - 1; i++)
            {
                std::unique_ptr<bvh_composite_intersection> bvh(new bvh_composite_intersection());

                for (size_t j = 0; j < nfaces; j++)
                {
                    const vector3* P[6];
                    const vector3* N[6];

                    size_t i0 = verts[3 * j + 0];
                    size_t i1 = verts[3 * j + 1];
                    size_t i2 = verts[3 * j + 2];

                    P[0] = &seq_[i + 0].P[i0];
                    P[1] = &seq_[i + 0].P[i1];
                    P[2] = &seq_[i + 0].P[i2];
                    P[3] = &seq_[i + 1].P[i0];
                    P[4] = &seq_[i + 1].P[i1];
                    P[5] = &seq_[i + 1].P[i2];

                    for (int k = 0; k < 2; k++)
                    {
                        int nNormalType = nNormalTypes[i];
                        if (nNormalType == 0)
                        {
                            N[3 * k + 0] = &seq_[i + k].N[i0];
                            N[3 * k + 1] = &seq_[i + k].N[i1];
                            N[3 * k + 2] = &seq_[i + k].N[i2];
                        }
                        else if (nNormalType == 1)
                        {
                            N[3 * k + 0] = &seq_[i + k].N[3 * j + 0];
                            N[3 * k + 1] = &seq_[i + k].N[3 * j + 1];
                            N[3 * k + 2] = &seq_[i + k].N[3 * j + 2];
                        }
                        else if (nNormalType == 2 || nNormalType == 3)
                        {
                            N[3 * k + 0] = &seq_[i + k].N[j];
                            N[3 * k + 1] = &seq_[i + k].N[j];
                            N[3 * k + 2] = &seq_[i + k].N[j];
                        }
                    }

                    bvh->add(new ri_motion_triangle(P, N, j));
                }
                bvh->construct();
                inters_[i].reset(bvh.release());
            }

            {
                vector3 min = inters_[0]->min();
                vector3 max = inters_[0]->max();
                for (size_t i = 1; i < ssz - 1; i++)
                {
                    vector3 cmin = inters_[i]->min();
                    vector3 cmax = inters_[i]->max();
                    for (int j = 0; j < 3; j++)
                    {
                        min[j] = std::min(min[j], cmin[j]);
                        max[j] = std::max(max[j], cmax[j]);
                    }
                }
                min_ = min;
                max_ = max;
            }
        }

        ri_motion_points_triangle_polygons_intersection_imp::~ri_motion_points_triangle_polygons_intersection_imp()
        {
            ;
        }

        bool ri_motion_points_triangle_polygons_intersection_imp::test(const ray& r, real dist) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, dist))
            {
                real t1 = r.time();
                int i = GetIndex(seq_, t1);
                real t2 = (t1 - seq_[i + 0].t) / (seq_[i + 1].t - seq_[i + 0].t);
                if (0 <= t2 && t2 <= 1)
                {
                    ray r2 = ray(r.origin(), r.direction(), t2);
                    return inters_[i]->test(r2, dist);
                }
            }
            return false;
        }

        bool ri_motion_points_triangle_polygons_intersection_imp::test(test_info* info, const ray& r, real dist) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, dist))
            {
                real t1 = r.time();
                int i = GetIndex(seq_, t1);
                real t2 = (t1 - seq_[i + 0].t) / (seq_[i + 1].t - seq_[i + 0].t);
                if (0 <= t2 && t2 <= 1)
                {
                    ray r2 = ray(r.origin(), r.direction(), t2);
                    if (inters_[i]->test(info, r2, dist))
                    {
                        info->p_intersection = this;
                        return true;
                    }
                }
            }
            return false;
        }

        void ri_motion_points_triangle_polygons_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
        {
            ; //
        }

        vector3 ri_motion_points_triangle_polygons_intersection_imp::min() const
        {
            return min_;
        }

        vector3 ri_motion_points_triangle_polygons_intersection_imp::max() const
        {
            return max_;
        }
    }

    ri_motion_points_triangle_polygons_intersection::ri_motion_points_triangle_polygons_intersection(
        int nfaces, const int verts[], int nverts,
        const std::vector<sequence_t>& seq, const matrix4& mat)
    {
        inter_.reset(new ri_motion_points_triangle_polygons_intersection_imp(nfaces, verts, nverts, seq, mat));
    }

    ri_motion_points_triangle_polygons_intersection::~ri_motion_points_triangle_polygons_intersection()
    {
        ; //
    }

    bool ri_motion_points_triangle_polygons_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_motion_points_triangle_polygons_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void ri_motion_points_triangle_polygons_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        inter_->finalize(info, r, dist);
    }

    vector3 ri_motion_points_triangle_polygons_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_motion_points_triangle_polygons_intersection::max() const
    {
        return inter_->max();
    }
}
