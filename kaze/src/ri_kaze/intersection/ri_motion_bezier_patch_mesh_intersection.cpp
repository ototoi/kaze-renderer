#include "ri_motion_bezier_patch_mesh_intersection.h"
#include "test_info.h"
#include "intersection_ex.h"
#include "patch/bezier_patch_intersection.h"
#include "bvh_composite_intersection.h"
#include <memory>

namespace kaze
{
    namespace
    {
        typedef ri_motion_bezier_patch_mesh_intersection::sequence_t sequence_t;

        class ri_motion_bezier_patch_intersection : public bounded_intersection
        {
        public:
            ri_motion_bezier_patch_intersection(int nu, int nv, const std::vector<const vector3*>& P, real u0, real u1, real v0, real v1)
                : nu_(nu), nv_(nv), P_(P), u0_(u0), u1_(u1), v0_(v0), v1_(v1)
            {
                vector3 min, max;
                min = max = *P[0];
                for (size_t i = 1; i < P.size(); i++)
                {
                    vector3 p = *P[i];
                    for (int j = 0; j < 3; j++)
                    {
                        min[j] = std::min(min[j], p[j]);
                        max[j] = std::max(max[j], p[j]);
                    }
                }
                min_ = min;
                max_ = max;
            }
            bool test(const ray& r, real dist) const
            {
                int sz = nu_ * nv_;
                real t = r.time();
                real s = 1 - t;
                std::vector<vector3> P(sz);
                for (int i = 0; i < sz; i++)
                {
                    P[i] = s * (*(P_[0 * sz + i])) + t * (*(P_[1 * sz + i]));
                }
                bezier_patch_intersection inter(bezier_patch<vector3>(nu_, nv_, P), u0_, u1_, v0_, v1_);
                return inter.test(r, dist);
            }
            bool test(test_info* info, const ray& r, real dist) const
            {
                int sz = nu_ * nv_;
                real t = r.time();
                real s = 1 - t;
                std::vector<vector3> P(sz);
                for (int i = 0; i < sz; i++)
                {
                    P[i] = s * (*(P_[0 * sz + i])) + t * (*(P_[1 * sz + i]));
                }
                bezier_patch_intersection inter(bezier_patch<vector3>(nu_, nv_, P), u0_, u1_, v0_, v1_);
                if (inter.test(info, r, dist))
                {
                    this->finalize(info, r, info->distance);
                    return true;
                }
                return false;
            }
            void finalize(test_info* info, const ray& r, real dist) const
            {
                if (info->p_intersection)
                {
                    info->p_intersection->finalize(info, r, dist);
                    info->p_intersection = NULL;
                }
            }
            vector3 min() const
            {
                return min_;
            }
            vector3 max() const
            {
                return max_;
            }

        private:
            int nu_;
            int nv_;
            std::vector<const vector3*> P_;
            real u0_;
            real u1_;
            real v0_;
            real v1_;
            vector3 min_;
            vector3 max_;
        };

        struct sequence_x
        {
            real t;
            std::vector<vector3> P;
        };

        static int GetIndex(const std::vector<sequence_x>& seq, real t)
        {
            size_t sz = seq.size();
            if (t < seq[0].t) return 0;
            for (size_t i = 0; i < sz - 1; i++)
            {
                if (seq[i].t <= t && t < seq[i + 1].t) return (int)i;
            }
            return (int)sz - 2;
        }

        static std::shared_ptr<intersection> CreateBezierPatch(
            int nu, int uorder,
            int nv, int vorder,
            const vector3 P0[],
            const vector3 P1[])
        {
            std::unique_ptr<bvh_composite_intersection> bvh(new bvh_composite_intersection());
            int nPu = (nu - 1) / (uorder - 1);
            int nPv = (nv - 1) / (vorder - 1);

            size_t psz = uorder * vorder;
            for (int jj = 0; jj < nPv; jj++)
            {
                for (int ii = 0; ii < nPu; ii++)
                {

                    std::vector<const vector3*> points(psz * 2);
                    int i0 = ii * (uorder - 1);
                    int j0 = jj * (vorder - 1);
                    for (int j = 0; j < vorder; j++)
                    {
                        for (int i = 0; i < uorder; i++)
                        {
                            int ix = i0 + i;
                            int jx = j0 + j;
                            int index = jx * nu + ix;

                            const vector3* p0 = P0 + index;
                            const vector3* p1 = P1 + index;

                            points[j * uorder + i + psz * 0] = p0;
                            points[j * uorder + i + psz * 1] = p1;
                        }
                    }

                    real u0 = real(ii) / nPu;
                    real u1 = real(ii + 1) / nPu;
                    real v0 = real(jj) / nPv;
                    real v1 = real(jj + 1) / nPv;

                    bvh->add(new ri_motion_bezier_patch_intersection(uorder, vorder, points, u0, u1, v0, v1));
                }
            }
            bvh->construct();
            return std::shared_ptr<intersection>(bvh.release());
        }

        class ri_motion_bezier_patch_mesh_intersection_imp : public bounded_intersection
        {
        public:
            ri_motion_bezier_patch_mesh_intersection_imp(
                int nu, int uorder,
                int nv, int vorder,
                const std::vector<sequence_t>& seq,
                const matrix4& mat);
            ~ri_motion_bezier_patch_mesh_intersection_imp();

        public:
            bool test(const ray& r, real dist) const;
            bool test(test_info* info, const ray& r, real dist) const;
            void finalize(test_info* info, const ray& r, real dist) const;
            vector3 min() const;
            vector3 max() const;

        private:
            std::vector<sequence_x> seq_;
            std::vector<std::shared_ptr<intersection> > inters_;
            vector3 min_;
            vector3 max_;
        };

        ri_motion_bezier_patch_mesh_intersection_imp::ri_motion_bezier_patch_mesh_intersection_imp(
            int nu, int uorder,
            int nv, int vorder,
            const std::vector<sequence_t>& seq,
            const matrix4& mat)
        {
            size_t ssz = seq.size();
            seq_.resize(ssz);
            for (size_t i = 0; i < ssz; i++)
            {
                seq_[i].t = seq[i].t;
                size_t psz = seq[i].P.size() / 3;
                seq_[i].P.resize(psz);
                for (size_t j = 0; j < psz; j++)
                {
                    seq_[i].P[j] = mat * vector3(seq[i].P[3 * j + 0], seq[i].P[3 * j + 1], seq[i].P[3 * j + 2]);
                }
            }
            inters_.resize(ssz - 1);
            for (size_t i = 0; i < ssz - 1; i++)
            {
                inters_[i] = CreateBezierPatch(nu, uorder, nv, vorder, &(seq_[i].P[0]), &(seq_[i + 1].P[0]));
            }

            {
                vector3 min = inters_[0]->min();
                vector3 max = inters_[0]->max();
                for (size_t i = 1; i < ssz - 1; i++)
                {
                    vector3 cmin = inters_[i]->min();
                    vector3 cmax = inters_[i]->max();
                    for (int j = 0; j < 3; j++)
                    {
                        min[j] = std::min(min[j], cmin[j]);
                        max[j] = std::max(max[j], cmax[j]);
                    }
                }
                min_ = min;
                max_ = max;
            }
        }

        ri_motion_bezier_patch_mesh_intersection_imp::~ri_motion_bezier_patch_mesh_intersection_imp()
        {
            ; //
        }

        bool ri_motion_bezier_patch_mesh_intersection_imp::test(const ray& r, real dist) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, dist))
            {
                real t1 = r.time();
                int i = GetIndex(seq_, t1);
                real t2 = (t1 - seq_[i + 0].t) / (seq_[i + 1].t - seq_[i + 0].t);
                if (0 <= t2 && t2 <= 1)
                {
                    ray r2 = ray(r.origin(), r.direction(), t2);
                    return inters_[i]->test(r2, dist);
                }
            }
            return false;
        }
        bool ri_motion_bezier_patch_mesh_intersection_imp::test(test_info* info, const ray& r, real dist) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, dist))
            {
                real t1 = r.time();
                int i = GetIndex(seq_, t1);
                real t2 = (t1 - seq_[i + 0].t) / (seq_[i + 1].t - seq_[i + 0].t);
                if (0 <= t2 && t2 <= 1)
                {
                    ray r2 = ray(r.origin(), r.direction(), t2);
                    if (inters_[i]->test(info, r2, dist))
                    {
                        info->p_intersection = this;
                        return true;
                    }
                }
            }
            return false;
        }

        void ri_motion_bezier_patch_mesh_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
        {
            ; //
        }
        vector3 ri_motion_bezier_patch_mesh_intersection_imp::min() const
        {
            return min_;
        }
        vector3 ri_motion_bezier_patch_mesh_intersection_imp::max() const
        {
            return max_;
        }
    }

    ri_motion_bezier_patch_mesh_intersection::ri_motion_bezier_patch_mesh_intersection(
        int nu, int uorder,
        int nv, int vorder,
        const std::vector<sequence_t>& seq,
        const matrix4& mat)
    {
        inter_.reset(new ri_motion_bezier_patch_mesh_intersection_imp(nu, uorder, nv, vorder, seq, mat));
    }

    ri_motion_bezier_patch_mesh_intersection::~ri_motion_bezier_patch_mesh_intersection()
    {
        ; //
    }

    bool ri_motion_bezier_patch_mesh_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }
    bool ri_motion_bezier_patch_mesh_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }
    void ri_motion_bezier_patch_mesh_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }
    vector3 ri_motion_bezier_patch_mesh_intersection::min() const
    {
        return inter_->min();
    }
    vector3 ri_motion_bezier_patch_mesh_intersection::max() const
    {
        return inter_->max();
    }
}
