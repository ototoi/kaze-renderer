#pragma warning(disable : 4996)

#include "ri_file_mesh_intersection.h"
#include "fast_mesh_intersection.h"

#include <string>

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif

#include "mesh/ply/ply_mesh_loader.h"
#include "mesh/obj/obj_mesh_loader.h"
#include "mesh/transformed_mesh_loader.h"

namespace kaze
{
    namespace
    {
        class dummy_intersection : public intersection
        {
        public:
            bool test(const ray& r, real dist) const { return false; }
            bool test(test_info* info, const ray& r, real dist) const { return false; }
            void finalize(test_info* info, const ray& r, real dist) const {}
            vector3 min() const { return vector3(0, 0, 0); }
            vector3 max() const { return vector3(0, 0, 0); }
        };
    }

    static std::string GetExt(const std::string& str)
    {
#ifdef _WIN32
        char ext[_MAX_EXT] = {};
        _splitpath(str.c_str(), NULL, NULL, NULL, ext);
        return ext;
#else
        std::string::size_type i = str.find_last_of('.');
        if (i != std::string::npos)
        {
            return str.substr(i);
        }
        else
        {
            return "";
        }
#endif
    }

    static bool IsExistFile(const char* szFilePath)
    {
#ifdef _WIN32
        struct _stat s;
        int nRet = _stat(szFilePath, &s);
        return (nRet == 0);
#else
        struct stat s;
        int nRet = stat(szFilePath, &s);
        return (nRet == 0);
#endif
    }

    bool ri_file_mesh_intersection::is_valid(const char* szFilePath)
    {
        if (!IsExistFile(szFilePath)) return false;
        std::string strExt = GetExt(szFilePath);
        if (strExt == ".ply")
        {
            return true;
        }
        else if (strExt == ".obj")
        {
            return true;
        }
        return false;
    }

    ri_file_mesh_intersection::ri_file_mesh_intersection(
        const char* type,
        const char* path,
        const matrix4& mat)
    {
        if (is_valid(path))
        {
            parameter_map param;
            std::shared_ptr<mesh_loader> ml;

            std::string strExt = GetExt(path);
            if (strExt == ".ply")
            {
                ml.reset(new transformed_mesh_loader(new ply_mesh_loader(path), mat));
            }
            else if (strExt == ".obj")
            {
                ml.reset(new transformed_mesh_loader(new obj_mesh_loader(path), mat));
            }

            if (ml.get())
            {
                inter_.reset(new fast_mesh_intersection(*ml, param));
            }
        }

        if (!inter_.get())
        {
            inter_.reset(new dummy_intersection());
        }
    }

    ri_file_mesh_intersection::~ri_file_mesh_intersection()
    {
        ; //
    }

    bool ri_file_mesh_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_file_mesh_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }
    void ri_file_mesh_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }

    vector3 ri_file_mesh_intersection::min() const
    {
        return inter_->min();
    }
    vector3 ri_file_mesh_intersection::max() const
    {
        return inter_->max();
    }
}