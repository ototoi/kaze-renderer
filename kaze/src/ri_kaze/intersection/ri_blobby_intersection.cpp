#include "ri_blobby_intersection.h"
//#include "blobby_intersection.h"
#include "transformed_intersection.h"

namespace kaze
{
    /*
    ri_blobby_intersection::ri_blobby_intersection(const std::shared_ptr<blobby_node>& node, const matrix4& mat)
    {
        inter_.reset(
            new m4_transformed_intersection(
                new blobby_intersection(node),
                mat
            )
        );
    }
    */

    ri_blobby_intersection::ri_blobby_intersection()
    {
        ;//
    }

    ri_blobby_intersection::~ri_blobby_intersection()
    {
        ;
    }

    bool ri_blobby_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_blobby_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void ri_blobby_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ;
    }

    vector3 ri_blobby_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_blobby_intersection::max() const
    {
        return inter_->max();
    }
}
