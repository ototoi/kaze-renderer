#ifndef KAZE_RI_BEZIER_CURVES_INTERSECTION_H
#define KAZE_RI_BEZIER_CURVES_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class ri_bezier_curves_intersection : public bounded_intersection
    {
    public:
        ri_bezier_curves_intersection(
            int ncurves, const int nvertices[], int order,
            const float P[], const float N[],
            const float width[], const float Cs[], const float Os[],
            const float widthv[], const float Csv[], const float Osv[],
            const float Nv[], //
            float constantwidth, const float constantnormal[],
            const matrix4& mat);
        ~ri_bezier_curves_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> inter_;
    };
}

#endif