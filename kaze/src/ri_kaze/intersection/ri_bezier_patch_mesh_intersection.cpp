#include "ri_bezier_patch_mesh_intersection.h"
#include "test_info.h"

#include "patch/bezier_patch_intersection.h"
#include "bvh_composite_intersection.h"

#include <vector>
#include <memory>

namespace kaze
{

    static std::shared_ptr<intersection> CreateBezierPatch(int nu, int uorder, int nv, int vorder, const float P[], const matrix4& mat)
    {
        std::unique_ptr<bvh_composite_intersection> bvh(new bvh_composite_intersection());
        int nPu = (nu - 1) / (uorder - 1);
        int nPv = (nv - 1) / (vorder - 1);
        for (int jj = 0; jj < nPv; jj++)
        {
            for (int ii = 0; ii < nPu; ii++)
            {
                std::vector<vector3> points(uorder * vorder);
                int i0 = ii * (uorder - 1);
                int j0 = jj * (vorder - 1);
                for (int j = 0; j < vorder; j++)
                {
                    for (int i = 0; i < uorder; i++)
                    {
                        int ix = i0 + i;
                        int jx = j0 + j;
                        int index = jx * nu + ix;
                        float x = P[3 * index + 0];
                        float y = P[3 * index + 1];
                        float z = P[3 * index + 2];
                        points[j * uorder + i] = mat * vector3(x, y, z);
                    }
                }

                real u0 = real(ii) / nPu;
                real u1 = real(ii + 1) / nPu;
                real v0 = real(jj) / nPv;
                real v1 = real(jj + 1) / nPv;

                bezier_patch<vector3> bz(uorder, vorder, points);
                bvh->add(new bezier_patch_intersection(bz, u0, u1, v0, v1));
            }
        }
        bvh->construct();
        return std::shared_ptr<intersection>(bvh.release());
    }

    ri_bezier_patch_mesh_intersection::ri_bezier_patch_mesh_intersection(int nu, int uorder, int nv, int vorder, const float P[], const matrix4& mat)
    {
        inter_ = CreateBezierPatch(nu, uorder, nv, vorder, P, mat);
    }

    ri_bezier_patch_mesh_intersection::~ri_bezier_patch_mesh_intersection()
    {
        ; //
    }

    bool ri_bezier_patch_mesh_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_bezier_patch_mesh_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void ri_bezier_patch_mesh_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        /**/
    }

    vector3 ri_bezier_patch_mesh_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_bezier_patch_mesh_intersection::max() const
    {
        return inter_->max();
    }
}