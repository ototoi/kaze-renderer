#ifndef KAZE_RI_POINTS_TRIANGLE_POLYGONS_INTERSECTION_H
#define KAZE_RI_POINTS_TRIANGLE_POLYGONS_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{

    class ri_points_triangle_polygons_intersection : public bounded_intersection
    {
    public:
        ri_points_triangle_polygons_intersection(
            int nfaces, const int verts[], int nverts,
            const float P[], const float N[], const float Np[], const float Nf[], const matrix4& mat);
        ri_points_triangle_polygons_intersection(
            int nfaces, const int verts[], int nverts,
            const float P[], const float N[], const float Np[], const float Nf[], float smooth_angle, const matrix4& mat);
        ~ri_points_triangle_polygons_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> inter_;
    };
}

#endif
