#ifndef KAZE_RI_MOTION_BEZIER_PATCH_MESH_INTERSECTION_H
#define KAZE_RI_MOTION_BEZIER_PATCH_MESH_INTERSECTION_H

#include "bounded_intersection.h"
#include <vector>

namespace kaze
{

    class ri_motion_bezier_patch_mesh_intersection : public bounded_intersection
    {
    public:
        struct ri_motion_sequence
        {
            real t;
            std::vector<float> P;
        };
        typedef ri_motion_sequence sequence_t;

    public:
        ri_motion_bezier_patch_mesh_intersection(
            int nu, int uorder,
            int nv, int vorder,
            const std::vector<ri_motion_sequence>& seq,
            const matrix4& mat);
        ~ri_motion_bezier_patch_mesh_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> inter_;
    };
}

#endif