#include "ri_subdivision_mesh_intersection.h"
//#include "subdivision/cc_subdivision_mesh_intersection.h"
#include <vector>
#include <memory>

namespace kaze
{

    ri_subdivision_mesh_intersection::ri_subdivision_mesh_intersection(
        const char* type,
        int npolys, const int nverts[], const int verts[],
        const float P[], const matrix4& mat)
    {
        std::vector<int> polys(npolys);

        size_t isz = 0;
        for (size_t i = 0; i < npolys; i++)
        {
            polys[i] = nverts[i];
            isz += nverts[i];
        }
        size_t max_index = 0;
        ;
        std::vector<size_t> indices(isz);
        for (size_t i = 0; i < isz; i++)
        {
            indices[i] = (size_t)verts[i];
            max_index = std::max<size_t>(max_index, indices[i]);
        }

        size_t vsz = max_index + 1;
        std::vector<vector3> vertices(vsz);
        for (size_t i = 0; i < vsz; i++)
        {
            vertices[i] = mat * vector3(P + 3 * i);
        }

        std::vector<vector2> uvs(isz);
        for (size_t i = 0; i < isz; i++)
        {
            uvs[i] = vector2(0, 0); //TODO
        }
/*
        std::unique_ptr<cc_subdivision_mesh_intersection> ap(
            new cc_subdivision_mesh_intersection(
                polys,
                indices,
                vertices,
                uvs));
*/
        //inter_.reset(ap.release());
    }

    ri_subdivision_mesh_intersection::~ri_subdivision_mesh_intersection()
    {
    }

    bool ri_subdivision_mesh_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }
    bool ri_subdivision_mesh_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }
    void ri_subdivision_mesh_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }

    vector3 ri_subdivision_mesh_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_subdivision_mesh_intersection::max() const
    {
        return inter_->max();
    }
}
