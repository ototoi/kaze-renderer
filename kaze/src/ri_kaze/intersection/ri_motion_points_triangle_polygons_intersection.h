#ifndef KAZE_RI_MOTION_POINTS_TRIANGLE_POLYGONS_INTERSECTION_H
#define KAZE_RI_MOTION_POINTS_TRIANGLE_POLYGONS_INTERSECTION_H

#include "bounded_intersection.h"
#include <vector>

namespace kaze
{

    class ri_motion_points_triangle_polygons_intersection : public bounded_intersection
    {
    public:
        struct ri_motion_sequence
        {
            real t;
            std::vector<float> P;
            std::vector<float> N;
            std::vector<float> Np;
            std::vector<float> Nf;
        };
        typedef ri_motion_sequence sequence_t;

    public:
        ri_motion_points_triangle_polygons_intersection(
            int nfaces, const int verts[], int nverts,
            const std::vector<ri_motion_sequence>& seq, const matrix4& mat);
        ~ri_motion_points_triangle_polygons_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> inter_;
    };
}

#endif