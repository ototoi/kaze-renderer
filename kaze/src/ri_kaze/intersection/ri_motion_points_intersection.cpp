#include "ri_motion_points_intersection.h"
#include "test_info.h"
#include "intersection_ex.h"

#include "curves/curve_strand.h"
#include "curves/motion_points_curve_strand.h"
#include "curves/plqbvh_curves_accelerator.h"
#include "curves/curve_test_info.h"

#include "logger.h"
#include "timer.h"

#include "transformed_intersection.h"

#include <vector>

#if 0
#define PRINTLOG print_log
#else
static void print_dummy(const char* str, ...) {}
#define PRINTLOG print_dummy
#endif

namespace kaze
{
    namespace
    {

        struct curve_segment
        {
            const curve_strand* p_strand;
            int nseg;
        };

        typedef ri_motion_points_intersection::sequence_t sequence_t;

        class ri_motion_points_intersection_imp : public bounded_intersection
        {
        public:
            ri_motion_points_intersection_imp(int nverts, std::vector<sequence_t> seq, const char* type);
            ~ri_motion_points_intersection_imp();

        public:
            bool test(const ray& r, real dist) const;
            bool test(test_info* info, const ray& r, real dist) const;
            void finalize(test_info* info, const ray& r, real dist) const;
            vector3 min() const;
            vector3 max() const;

        protected:
            void initialize();

        private:
            curves_accelerator* root_;
            std::vector<const curve_strand*> strands_;
        };

        static int GetIndex(const std::vector<sequence_t>& seq, real t)
        {
            size_t sz = seq.size();
            if (t < seq[0].t) return 0;
            for (size_t i = 0; i < sz - 1; i++)
            {
                if (seq[i].t <= t && t < seq[i + 1].t) return (int)i;
            }
            return (int)sz - 2;
        }

        static std::vector<float> GetLinear(const std::vector<float>& v0, const std::vector<float>& v1, float a, bool bNormalize = false)
        {
            size_t sz = v0.size();
            std::vector<float> out(sz);
            for (size_t i = 0; i < sz; i++)
            {
                out[i] = (1 - a) * v0[i] + (a)*v1[i];
            }
            if (sz && bNormalize)
            {
                size_t tsz = sz / 3;
                for (size_t i = 0; i < tsz; i++)
                {
                    vector3f n = normalize(vector3f(&out[3 * i]));
                    out[3 * i + 0] = n[0];
                    out[3 * i + 1] = n[1];
                    out[3 * i + 2] = n[2];
                }
            }
            return out;
        }

        static sequence_t GetLinear(const sequence_t& s0, const sequence_t& s1, float a)
        {
            sequence_t tmp;

            tmp.t = (1 - a) * s0.t + (a)*s1.t;
            tmp.P = GetLinear(s0.P, s1.P, a);
            tmp.width = GetLinear(s0.width, s1.width, a);
            tmp.N = GetLinear(s0.N, s1.N, a, true);
            tmp.Cs = GetLinear(s0.Cs, s1.Cs, a);
            tmp.Os = GetLinear(s0.Os, s1.Os, a);
            tmp.constantwidth = (1 - a) * s0.constantwidth + (a)*s1.constantwidth;
            tmp.constantnormal = GetLinear(s0.constantnormal, s1.constantnormal, a, true);

            return tmp;
        }

        static sequence_t GetMotion(real t, const std::vector<sequence_t>& seq)
        {
            int idx = GetIndex(seq, t);
            float a = (float)((t - seq[idx].t) / (seq[idx + 1].t - seq[idx].t));
            return GetLinear(seq[idx], seq[idx + 1], a);
        }

        static std::vector<sequence_t> RecalcSequence(const std::vector<sequence_t>& seq)
        {
            size_t sz = seq.size();
            real dt = 1;
            for (size_t i = 0; i < sz - 1; i++)
            {
                dt = std::min<real>(dt, seq[i + 1].t - seq[i].t);
            }
            int div = std::min<int>(std::max<int>((int)ceil(1.0 / dt) + 1, 2), 64);
            std::vector<sequence_t> tseq;
            for (int i = 0; i < div; i++)
            {
                real t = real(i) / (div - 1);
                tseq.push_back(GetMotion(t, seq));
            }

            return tseq;
        }

        ri_motion_points_intersection_imp::ri_motion_points_intersection_imp(int nverts, const std::vector<sequence_t> seq, const char* type)
            : root_(0)
        {
            std::string strType;
            if (type) strType = type;
            std::vector<sequence_t> tseq = RecalcSequence(seq);

            size_t psz = nverts;
            size_t tsz = tseq.size();

            strands_.reserve(psz);

            std::vector<float> times(tsz);
            for (size_t k = 0; k < tsz; k++)
            {
                times[k] = (float)tseq[k].t;
            }

            for (size_t p = 0; p < psz; p++)
            {
                std::vector<vector3f> P(tsz);
                std::vector<float> R(tsz);
                std::vector<vector3f> N(tsz);
                std::vector<vector3f> C1(tsz);
                std::vector<vector3f> C2(tsz);

                bool bNormal = false;

                for (size_t k = 0; k < tsz; k++)
                {
                    P[k] = vector3f(tseq[k].P[3 * p + 0], tseq[k].P[3 * p + 1], tseq[k].P[3 * p + 2]);
                    if (!tseq[k].width.empty())
                    {
                        R[k] = tseq[k].width[k] * 0.5f;
                    }
                    else
                    {
                        R[k] = tseq[k].constantwidth * 0.5f;
                    }
                    if (!tseq[k].N.empty())
                    {
                        N[k] = vector3f(tseq[k].N[3 * p + 0], tseq[k].N[3 * p + 1], tseq[k].N[3 * p + 2]);
                        bNormal = true;
                    }
                    else
                    {
                        if (!tseq[k].constantnormal.empty())
                        {
                            N[k] = vector3f(tseq[k].constantnormal[0], tseq[k].constantnormal[1], tseq[k].constantnormal[2]);
                            bNormal = true;
                        }
                    }
                    if (!tseq[k].Cs.empty())
                    {
                        C1[k] = vector3f(tseq[k].Cs[3 * p + 0], tseq[k].Cs[3 * p + 1], tseq[k].Cs[3 * p + 2]);
                    }
                    else
                    {
                        C1[k] = color3f(1, 1, 1);
                    }
                    if (!tseq[k].Os.empty())
                    {
                        C2[k] = vector3f(tseq[k].Os[3 * p + 0], tseq[k].Os[3 * p + 1], tseq[k].Os[3 * p + 2]);
                    }
                    else
                    {
                        C2[k] = color3f(1, 1, 1);
                    }
                }

                if (strType == "sphere")
                {
                    strands_.push_back(new sphere_motion_points_curve_strand(times, P, R, C1, C2, p));
                }
                else
                {
                    if (bNormal)
                    {
                        strands_.push_back(new normal_disk_motion_points_curve_strand(times, P, R, N, C1, C2, p));
                    }
                    else
                    {
                        strands_.push_back(new disk_motion_points_curve_strand(times, P, R, C1, C2, p));
                    }
                }
            }
            initialize();
        }

        ri_motion_points_intersection_imp::~ri_motion_points_intersection_imp()
        {
            delete root_;
            size_t sz = strands_.size();
            for (size_t i = 0; i < sz; i++)
            {
                delete strands_[i];
            }
        }

        void ri_motion_points_intersection_imp::initialize()
        {
            timer t;
            PRINTLOG("ri_motion_points_intersection:construct tree start ...\n");
            t.start();
            root_ = new plqbvh_curves_accelerator(strands_);
            t.end();
            PRINTLOG("ri_motion_points_intersection:construct tree end %d ms\n", t.msec());
        }

        bool ri_motion_points_intersection_imp::test(const ray& r, real dist) const
        {
            return root_->test(r, 0, dist);
        }

        bool ri_motion_points_intersection_imp::test(test_info* info, const ray& r, real dist) const
        {
            curve_test_info cinfo;
            cinfo.p_strand = NULL;
            if (root_->test(&cinfo, r, 0, dist))
            {
                if (cinfo.p_strand)
                {
                    cinfo.p_strand->finalize(cinfo.nseg, &cinfo, r, 0, cinfo.t);

                    vector3 N = vector3(cinfo.normal);
                    vector3 U = vector3(cinfo.tangent);
                    vector3 V = vector3(cinfo.binormal);
                    vector3 G = vector3(cinfo.geometric);
                    info->distance = cinfo.t;
                    info->position = vector3(cinfo.position);
                    info->normal = N;
                    info->geometric = G;
                    info->tangent = U;
                    info->binormal = V;
                    info->coord = vector3(cinfo.u, cinfo.v, 0);
                    info->col1 = vector3(cinfo.col1);
                    info->col2 = vector3(cinfo.col2);
                    info->index = cinfo.index;

                    return true;
                }
            }
            return false;
        }

        void ri_motion_points_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
        {
            //;
        }

        vector3 ri_motion_points_intersection_imp::min() const
        {
            return root_->min();
        }

        vector3 ri_motion_points_intersection_imp::max() const
        {
            return root_->max();
        }
    }

    ri_motion_points_intersection::ri_motion_points_intersection(int nverts, const std::vector<sequence_t>& seq, const char* type, const matrix4& mat)
    {
        inter_.reset(
            new m4_transformed_intersection(
                new ri_motion_points_intersection_imp(nverts, seq, type), mat));
    }

    ri_motion_points_intersection::~ri_motion_points_intersection()
    {
        ; //
    }

    bool ri_motion_points_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_motion_points_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void ri_motion_points_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        inter_->finalize(info, r, dist);
    }

    vector3 ri_motion_points_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_motion_points_intersection::max() const
    {
        return inter_->max();
    }
}