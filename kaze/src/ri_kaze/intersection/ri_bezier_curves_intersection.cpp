#include "ri_bezier_curves_intersection.h"
#include "curves/curves_intersection.h"
#include "transformed_intersection.h"

namespace kaze
{
    namespace
    {

        static void CopyVector(float P[3], const vector3& p)
        {
            P[0] = (float)p[0];
            P[1] = (float)p[1];
            P[2] = (float)p[2];
        }

        class ri_bezier_curves_loader : public bezier_curves_loader
        {
        public:
            ri_bezier_curves_loader(
                int ncurves, const int nvertices[], int order,
                const float P[], const float N[],
                const float width[], const float Cs[], const float Os[],
                const float widthv[], const float Csv[], const float Osv[],
                const float Nv[],
                float constantwidth, const float constantnormal[])
                : ncurves_(ncurves), nvertices_(nvertices), order_(order),
                  P_(P),
                  N_(N), width_(width), Cs_(Cs), Os_(Os),
                  Nv_(Nv), widthv_(widthv), Csv_(Csv), Osv_(Osv),

                  constantwidth_(constantwidth), constantnormal_(constantnormal)
            {
                i_ = 0;
                offset_s_ = 0;
                offset_v_ = 0;
            }
            ~ri_bezier_curves_loader()
            {
                ;
            }

        public:
            bool get_strands(size_t& sz) const
            {
                sz = ncurves_;
                return true;
            }
            bool get_strand(size_t i, io_bezier_curve_strand* str) const
            {
                int order = order_;
                if (i != i_)
                {
                    i_ = 0;
                    offset_s_ = 0;
                    offset_v_ = 0;
                    for (i_ = 0; i_ < i; i_++)
                    {
                        int nv = nvertices_[i_];
                        int ns = (nv - 1) / (order - 1); //(7-1)/(4-1)=2
                        offset_s_ += ns + 1;
                        offset_v_ += nv;
                    }
                }
                int nv = nvertices_[i_];
                int ns = (nv - 1) / (order - 1); //

                str->points.resize(nv);
                for (int j = 0; j < nv; j++)
                {
                    str->points[j].radius = constantwidth_ * 0.5f;
                    str->points[j].color1[0] = 1.f;
                    str->points[j].color1[1] = 1.f;
                    str->points[j].color1[2] = 1.f;
                    str->points[j].color2[0] = 1.f;
                    str->points[j].color2[1] = 1.f;
                    str->points[j].color2[2] = 1.f;
                    str->points[j].color3[0] = 1.f;
                    str->points[j].color3[1] = 1.f;
                    str->points[j].color3[2] = 1.f;
                }

                for (int j = 0; j < nv; j++)
                {
                    if (P_)
                    {
                        str->points[j].position[0] = P_[3 * (offset_v_ + j) + 0];
                        str->points[j].position[1] = P_[3 * (offset_v_ + j) + 1];
                        str->points[j].position[2] = P_[3 * (offset_v_ + j) + 2];
                    }
                    if (widthv_)
                    {
                        str->points[j].radius = widthv_[offset_v_ + j] * 0.5f;
                    }
                    if (Csv_)
                    {
                        str->points[j].color1[0] = Csv_[3 * (offset_v_ + j) + 0];
                        str->points[j].color1[1] = Csv_[3 * (offset_v_ + j) + 1];
                        str->points[j].color1[2] = Csv_[3 * (offset_v_ + j) + 2];
                    }
                    if (Osv_)
                    {
                        str->points[j].color2[0] = Osv_[3 * (offset_v_ + j) + 0];
                        str->points[j].color2[1] = Osv_[3 * (offset_v_ + j) + 1];
                        str->points[j].color2[2] = Osv_[3 * (offset_v_ + j) + 2];
                    }

                    if (Nv_)
                    {
                        str->points[j].normal[0] = Nv_[3 * (offset_v_ + j) + 0];
                        str->points[j].normal[1] = Nv_[3 * (offset_v_ + j) + 1];
                        str->points[j].normal[2] = Nv_[3 * (offset_v_ + j) + 2];
                    }
                }

                real delta = 1.0 / (order - 1);
                for (int j = 0; j < ns; j++)
                {
                    if (width_)
                    {
                        real r0 = width_[offset_s_ + j] * 0.5;
                        real r1 = width_[offset_s_ + j + 1] * 0.5;
                        for (int k = 0; k < order; k++)
                        {
                            real t = k * delta; //0,1/3,2/3,1
                            str->points[(order - 1) * j + k].radius = (float)(r0 * (1 - t) + r1 * t);
                        }
                    }

                    if (Cs_)
                    {
                        color3 c0 = color3(Cs_[3 * (offset_s_ + j) + 0], Cs_[3 * (offset_s_ + j) + 1], Cs_[3 * (offset_s_ + j) + 2]);
                        color3 c1 = color3(Cs_[3 * (offset_s_ + 1 + j) + 0], Cs_[3 * (offset_s_ + 1 + j) + 1], Cs_[3 * (offset_s_ + 1 + j) + 2]);

                        for (int k = 0; k < order; k++)
                        {
                            real t = k * delta; //0,1/3,2/3,1
                            CopyVector(str->points[(order - 1) * j + k].color1, (c0 * (1 - t) + c1 * t));
                        }
                    }

                    if (Os_)
                    {
                        color3 c0 = color3(Os_[3 * (offset_s_ + j) + 0], Os_[3 * (offset_s_ + j) + 1], Os_[3 * (offset_s_ + j) + 2]);
                        color3 c1 = color3(Os_[3 * (offset_s_ + 1 + j) + 0], Os_[3 * (offset_s_ + 1 + j) + 1], Os_[3 * (offset_s_ + 1 + j) + 2]);

                        for (int k = 0; k < order; k++)
                        {
                            real t = k * delta; //0,1/3,2/3,1
                            CopyVector(str->points[(order - 1) * j + k].color2, (c0 * (1 - t) + c1 * t));
                        }
                    }

                    if (N_)
                    {
                        vector3 n0 = vector3(N_[3 * (offset_s_ + j) + 0], N_[3 * (offset_s_ + j) + 1], N_[3 * (offset_s_ + j) + 2]);
                        vector3 n1 = vector3(N_[3 * (offset_s_ + 1 + j) + 0], N_[3 * (offset_s_ + 1 + j) + 1], N_[3 * (offset_s_ + 1 + j) + 2]);

                        for (int k = 0; k < order; k++)
                        {
                            real t = k * delta; //0,1/3,2/3,1
                            CopyVector(str->points[(order - 1) * j + k].normal, normalize(n0 * (1 - t) + n1 * t));
                        }
                    }
                    else if (Nv_)
                    {
                        ;
                    }
                    else if (constantnormal_)
                    {
                        vector3 n = normalize(vector3(constantnormal_));
                        for (int k = 0; k < order; k++)
                        {
                            CopyVector(str->points[(order - 1) * j + k].normal, n);
                        }
                    }
                    else
                    {
                        vector3 n = normalize(vector3(0, 0, 1));
                        for (int k = 0; k < order; k++)
                        {
                            CopyVector(str->points[(order - 1) * j + k].normal, n);
                        }
                    }
                }

                if (N_ || Nv_ || constantnormal_)
                {
                    str->has_normal = true;
                }
                else
                {
                    str->has_normal = false;
                }

                str->order = order;
                i_++;
                offset_s_ += ns + 1;
                offset_v_ += nv;

                return true;
            }

        private:
            int ncurves_;
            const int* nvertices_;
            int order_;
            const float* P_;

            const float* N_;     //varying
            const float* width_; //varying
            const float* Cs_;    //varying
            const float* Os_;    //varying

            const float* Nv_;     //vertex
            const float* widthv_; //vertex
            const float* Csv_;    //vertex
            const float* Osv_;    //vertex

            const float* constantnormal_;

            float constantwidth_;
            mutable size_t i_;
            mutable size_t offset_s_; //segment
            mutable size_t offset_v_; //
        };
    }

    ri_bezier_curves_intersection::ri_bezier_curves_intersection(
        int ncurves, const int nvertices[], int order,
        const float P[], const float N[],
        const float width[], const float Cs[], const float Os[],
        const float widthv[], const float Csv[], const float Osv[],
        const float Nv[], //
        float constantwidth, const float constantnormal[],
        const matrix4& mat)
    {
        parameter_map pm;

        std::shared_ptr<bezier_curves_loader> loader(
            new ri_bezier_curves_loader(
                ncurves, nvertices, order,
                P, N,
                width, Cs, Os,
                widthv, Csv, Osv,
                Nv,
                constantwidth, constantnormal));
        inter_.reset(
            new m4_transformed_intersection(
                new curves_intersection(*loader, pm), mat));
    }
    ri_bezier_curves_intersection::~ri_bezier_curves_intersection()
    {
        ;
    }

    bool ri_bezier_curves_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_bezier_curves_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void ri_bezier_curves_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        inter_->finalize(info, r, dist);
    }

    vector3 ri_bezier_curves_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_bezier_curves_intersection::max() const
    {
        return inter_->max();
    }
}
