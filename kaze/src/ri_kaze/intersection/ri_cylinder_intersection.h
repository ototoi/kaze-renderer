#ifndef KAZE_RI_CYLINDER_INTERSECTION_H
#define KAZE_RI_CYLINDER_INTERSECTION_H

#include "bounded_intersection.h"

namespace kaze
{
    class ri_cylinder_intersection : public bounded_intersection
    {
    public:
        ri_cylinder_intersection(real radius, real zmin, real zmax, real tmax, const matrix4& mat);
        ~ri_cylinder_intersection();

    public:
        bool test(const ray& r, real dist) const;
        bool test(test_info* info, const ray& r, real dist) const;
        void finalize(test_info* info, const ray& r, real dist) const;
        vector3 min() const;
        vector3 max() const;

    private:
        std::shared_ptr<intersection> inter_;
    };
}
#endif
