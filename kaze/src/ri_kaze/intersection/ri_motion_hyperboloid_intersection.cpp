#include "ri_motion_hyperboloid_intersection.h"
#include "test_info.h"
#include "ri_hyperboloid_intersection.h"
#include "intersection_ex.h"

namespace kaze
{
    namespace
    {

        typedef ri_motion_hyperboloid_intersection::sequence_t sequence_t;

        class ri_motion_hyperboloid_intersection_imp : public bounded_intersection
        {
        public:
            ri_motion_hyperboloid_intersection_imp(std::vector<sequence_t> seq, const matrix4& mat);
            ~ri_motion_hyperboloid_intersection_imp();

        public:
            bool test(const ray& r, real dist) const;
            bool test(test_info* info, const ray& r, real dist) const;
            void finalize(test_info* info, const ray& r, real dist) const;
            vector3 min() const;
            vector3 max() const;

        private:
            std::vector<sequence_t> seq_;
            matrix4 mat_;
            vector3 min_;
            vector3 max_;
        };

        static int GetIndex(const std::vector<sequence_t>& seq, real t)
        {
            size_t sz = seq.size();
            if (t < seq[0].t) return 0;
            for (size_t i = 0; i < sz - 1; i++)
            {
                if (seq[i].t <= t && t < seq[i + 1].t) return (int)i;
            }
            return (int)sz - 2;
        }

        static sequence_t GetLinear(const sequence_t& s0, const sequence_t& s1, real a)
        {
            sequence_t tmp;
            tmp.t = (1 - a) * s0.t + (a)*s1.t;
            tmp.point1 = (1 - a) * s0.point1 + (a)*s1.point1;
            tmp.point2 = (1 - a) * s0.point2 + (a)*s1.point2;
            tmp.tmax = (1 - a) * s0.tmax + (a)*s1.tmax;

            return tmp;
        }

        static sequence_t GetMotion(real t, const std::vector<sequence_t>& seq)
        {
            int idx = GetIndex(seq, t);
            real a = (t - seq[idx].t) / (seq[idx + 1].t - seq[idx].t);
            return GetLinear(seq[idx], seq[idx + 1], a);
        }

        static std::vector<sequence_t> RecalcSequence(const std::vector<sequence_t>& seq)
        {
            size_t sz = seq.size();
            real dt = 1;
            for (size_t i = 0; i < sz - 1; i++)
            {
                dt = std::min<real>(dt, seq[i + 1].t - seq[i].t);
            }
            int div = std::min<int>(std::max<int>((int)ceil(1.0 / dt) + 1, 2), 64);
            std::vector<sequence_t> tseq;
            for (int i = 0; i < div; i++)
            {
                real t = real(i) / (div - 1);
                tseq.push_back(GetMotion(t, seq));
            }

            return tseq;
        }

        static vector3 min__(const vector3& a, const vector3& b)
        {
            return vector3(
                std::min<real>(a[0], b[0]),
                std::min<real>(a[1], b[1]),
                std::min<real>(a[2], b[2]));
        }

        static vector3 max__(const vector3& a, const vector3& b)
        {
            return vector3(
                std::max<real>(a[0], b[0]),
                std::max<real>(a[1], b[1]),
                std::max<real>(a[2], b[2]));
        }

        ri_motion_hyperboloid_intersection_imp::ri_motion_hyperboloid_intersection_imp(const std::vector<sequence_t> seq, const matrix4& mat)
            : mat_(mat)
        {
            seq_ = RecalcSequence(seq);

            std::vector<std::shared_ptr<intersection> > inters;
            for (size_t i = 0; i < seq_.size(); i++)
            {
                inters.push_back(
                    std::shared_ptr<intersection>(
                        new ri_hyperboloid_intersection(seq_[i].point1, seq_[i].point2, seq_[i].tmax, mat)));
            }

            static const real FAR = values::far();

            vector3 min = vector3(+FAR, +FAR, +FAR);
            vector3 max = vector3(-FAR, -FAR, -FAR);
            for (size_t i = 0; i < inters.size() - 1; i++)
            {
                vector3 cmin = min__(inters[i]->min(), inters[i + 1]->min());
                vector3 cmax = max__(inters[i]->max(), inters[i + 1]->max());

                vector3 cnt = (cmin + cmax) * 0.5;
                vector3 ext = (cmax - cmin) * 0.5;

                cmin = cnt - ext;
                cmax = cnt + ext;

                for (int j = 0; j < 3; j++)
                {
                    if (min[j] > cmin[j]) min[j] = cmin[j];
                    if (max[j] < cmax[j]) max[j] = cmax[j];
                }
            }

            min_ = min;
            max_ = max;
        }

        ri_motion_hyperboloid_intersection_imp::~ri_motion_hyperboloid_intersection_imp()
        {
            ; //
        }

        bool ri_motion_hyperboloid_intersection_imp::test(const ray& r, real dist) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, dist))
            {
                //real tmin = std::max<real>(rng.tmin, 0);
                real tmax = std::min<real>(rng.tmax, dist);

                {
                    real t = r.time();
                    sequence_t s = GetMotion(t, seq_);
                    ri_hyperboloid_intersection intr(s.point1, s.point2, s.tmax, mat_);

                    return intr.test(r, tmax);
                }
            }
            return false;
        }

        bool ri_motion_hyperboloid_intersection_imp::test(test_info* info, const ray& r, real dist) const
        {
            range_AABB rng;
            if (test_AABB(&rng, min_, max_, r, dist))
            {
                //real tmin = std::max<real>(rng.tmin, 0);
                real tmax = std::min<real>(rng.tmax, dist);

                {
                    real t = r.time();
                    sequence_t s = GetMotion(t, seq_);
                    ri_hyperboloid_intersection intr(s.point1, s.point2, s.tmax, mat_);

                    if (intr.test(info, r, tmax))
                    {
                        if (info->p_intersection)
                        {
                            info->p_intersection->finalize(info, r, info->distance);
                        }
                        info->p_intersection = this;
                        return true;
                    }
                }
            }
            return false;
        }

        void ri_motion_hyperboloid_intersection_imp::finalize(test_info* info, const ray& r, real dist) const
        {
            //
            //info->position = r.origin()+info->distance*r.direction();
        }

        vector3 ri_motion_hyperboloid_intersection_imp::min() const
        {
            return min_;
        }

        vector3 ri_motion_hyperboloid_intersection_imp::max() const
        {
            return max_;
        }
    }

    ri_motion_hyperboloid_intersection::ri_motion_hyperboloid_intersection(const std::vector<sequence_t>& seq, const matrix4& mat)
    {
        inter_.reset(new ri_motion_hyperboloid_intersection_imp(seq, mat));
    }

    ri_motion_hyperboloid_intersection::~ri_motion_hyperboloid_intersection()
    {
        ; //
    }

    bool ri_motion_hyperboloid_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_motion_hyperboloid_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void ri_motion_hyperboloid_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        inter_->finalize(info, r, dist);
    }

    vector3 ri_motion_hyperboloid_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_motion_hyperboloid_intersection::max() const
    {
        return inter_->max();
    }
}