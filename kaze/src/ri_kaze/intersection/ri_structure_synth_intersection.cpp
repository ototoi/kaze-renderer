#pragma warning(disable : 4996)

#include "ri_structure_synth_intersection.h"

#include <string>

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif

namespace kaze
{
    namespace
    {
        class dummy_intersection : public intersection
        {
        public:
            bool test(const ray& r, real dist) const { return false; }
            bool test(test_info* info, const ray& r, real dist) const { return false; }
            void finalize(test_info* info, const ray& r, real dist) const {}
            vector3 min() const { return vector3(0, 0, 0); }
            vector3 max() const { return vector3(0, 0, 0); }
        };
    }

    static bool IsExistFile(const char* szFilePath)
    {
#ifdef _WIN32
        struct _stat s;
        int nRet = _stat(szFilePath, &s);
        return (nRet == 0);
#else
        struct stat s;
        int nRet = stat(szFilePath, &s);
        return (nRet == 0);
#endif
    }

    bool ri_structure_synth_intersection::is_valid(const char* szFilePath)
    {
        if (!IsExistFile(szFilePath)) return false;
        return true;
    }

    ri_structure_synth_intersection::ri_structure_synth_intersection(
        const char* path,
        const matrix4& mat)
    {
        if (is_valid(path))
        {
        }

        if (!inter_.get())
        {
            inter_.reset(new dummy_intersection());
        }
    }

    ri_structure_synth_intersection::~ri_structure_synth_intersection()
    {
        ; //
    }

    bool ri_structure_synth_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_structure_synth_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }
    void ri_structure_synth_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        ; //
    }

    vector3 ri_structure_synth_intersection::min() const
    {
        return inter_->min();
    }
    vector3 ri_structure_synth_intersection::max() const
    {
        return inter_->max();
    }
}
