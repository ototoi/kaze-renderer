#include "ri_nu_patch_intersection.h"
#include "test_info.h"

#include "patch/trimmed_nurbs_patch_intersection.h"

#include <vector>
#include <memory>

namespace kaze
{

    ri_nu_patch_intersection::ri_nu_patch_intersection(
        int nu, int uorder, const float uknot[], float umin, float umax,
        int nv, int vorder, const float vknot[], float vmin, float vmax,
        const float Pw[],
        const std::vector<ri_nu_patch_trim_loop>& outers,
        const std::vector<ri_nu_patch_trim_loop>& inners,
        const matrix4& mat)
    {
        int nuk = nu + uorder;
        int nvk = nv + vorder;

        std::vector<real> ruknots(nuk);
        std::vector<real> rvknots(nvk);
        for (int i = 0; i < nuk; i++)
        {
            ruknots[i] = (real)uknot[i];
        }
        for (int i = 0; i < nvk; i++)
        {
            rvknots[i] = (real)vknot[i];
        }
        size_t sz = nu * nv;
        std::vector<vector3> cp(nu * nv);
        std::vector<real> w(nu * nv);
        for (int i = 0; i < sz; i++)
        {
            cp[i] = vector3(Pw[4 * i + 0], Pw[4 * i + 1], Pw[4 * i + 2]);
            w[i] = real(Pw[4 * i + 3]);
        }
        for (int i = 0; i < sz; i++)
        {
            cp[i] = mat * cp[i];
        }

        std::shared_ptr<intersection> nrbs(
            new trimmed_nurbs_patch_intersection(
                nurbs_patch<vector3>(nu, nv, cp, w, ruknots, rvknots),
                outers, inners));

        inter_ = nrbs;
    }

    ri_nu_patch_intersection::~ri_nu_patch_intersection()
    {
        ;
    }

    bool ri_nu_patch_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_nu_patch_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void ri_nu_patch_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        /**/
    }

    vector3 ri_nu_patch_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_nu_patch_intersection::max() const
    {
        return inter_->max();
    }
}
