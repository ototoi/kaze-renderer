#include "ri_torus_intersection.h"
#include "torus_intersection.h"
#include "test_info.h"
#include "transformed_intersection.h"
#include "exhibited_composite_intersection.h"
#include "count_ptr.hpp"
#include <memory>

namespace kaze
{
    namespace
    {

        class uv_correct_intersection : public bounded_intersection
        {
        public:
            uv_correct_intersection(
                const auto_count_ptr<intersection>& inter,
                real umin = 0, real umax = 1, real vmin = 0, real vmax = 1,
                real cu = 1, real cv = 1, real du = 0, real dv = 0, int nReverse = 0)
                : inter_(inter), umin_(umin), umax_(umax), vmin_(vmin), vmax_(vmax), cu_(cu), cv_(cv), du_(du), dv_(dv), nReverse_(nReverse) {}
        public:
            bool test(const ray& r, real dist) const
            {
                return inter_->test(r, dist);
            }

            bool test(test_info* info, const ray& r, real dist) const
            {
                if (inter_->test(info, r, dist))
                {
                    if (info->p_intersection)
                    {
                        info->p_intersection->finalize(info, r, info->distance);

                        real u = info->coord[0];
                        real v = info->coord[1];

                        u = (u - umin_) / (umax_ - umin_);
                        v = (v - vmin_) / (vmax_ - vmin_);

                        u = std::max<real>(0, std::min<real>(u, 1));
                        v = std::max<real>(0, std::min<real>(v, 1));

                        if (nReverse_ & 1) u = 1.0 - u;
                        if (nReverse_ & 2) v = 1.0 - v;

                        u = cu_ * u + du_;
                        v = cv_ * v + dv_;

                        info->coord[0] = u;
                        info->coord[1] = v;

                        info->p_intersection = this;
                        return true;
                    }
                }
                return false;
            }

            void finalize(test_info* info, const ray& r, real dist) const
            {
                ;
            }

            vector3 min() const
            {
                return inter_->min();
            }

            vector3 max() const
            {
                return inter_->max();
            }

        private:
            std::shared_ptr<intersection> inter_;
            real umin_;
            real umax_;
            real vmin_;
            real vmax_;
            real cu_;
            real cv_;
            real du_;
            real dv_;
            int nReverse_;
        };

        static int Reverse2(int x)
        {
            if (x & 0x2)
            {
                return x & ~0x2;
            }
            else
            {
                return x | 0x2;
            }
        }

        static real AngleMax(real x)
        {
            if (x < -360)
            {
                return -360;
            }
            else if (360 < x)
            {
                return 360;
            }
            else
            {
                return x;
            }
        }

        struct torus_param
        {
            torus_param() : umin(0), umax(1), vmin(0), vmax(1), cu(1), cv(1), du(0), dv(0), nReverse(0) {}
            torus_param(real a, real b, real c, real d, int n) : umin(a), umax(b), vmin(c), vmax(d), cu(1), cv(1), du(0), dv(0), nReverse(n) {}
            torus_param(real a, real b, real c, real d, real e, real f, real g, real h, int n) : umin(a), umax(b), vmin(c), vmax(d), cu(e), cv(f), du(g), dv(h), nReverse(n) {}
            real umin;
            real umax;
            real vmin;
            real vmax;
            real cu;
            real cv;
            real du;
            real dv;
            int nReverse;
        };
    }

    ri_torus_intersection::ri_torus_intersection(real majrad, real minrad, real phimin, real phimax, real tmax, const matrix4& mat)
    {
        phimin = AngleMax(phimin);
        phimax = AngleMax(phimax);
        tmax = AngleMax(tmax);

        real umin = 0;
        real umax = tmax / 360.0;

        int nReverse = 0;
        if (tmax < 0)
        {
            umin = (360.0 + tmax) / 360.0;
            umax = 1.0;
            nReverse = 1 - nReverse;
        }

        if (umin > umax)
        {
            std::swap(umin, umax);
        }

        std::vector<torus_param> params;

        if (phimin >= 0 && phimax >= 0)
        {
            if (phimin < phimax)
            {
                real vmin = phimin / 360.0;
                real vmax = phimax / 360.0;
                params.push_back(torus_param(umin, umax, vmin, vmax, nReverse));
            }
            else
            {
                real vmin = phimax / 360.0;
                real vmax = phimin / 360.0;
                params.push_back(torus_param(umin, umax, vmin, vmax, Reverse2(nReverse)));
            }
        }
        else if (phimin < 0 && phimax >= 0)
        {
            real total = phimax - phimin;
            {
                real vmin = (360.0 + phimin) / 360.0;
                real vmax = 1.0;
                real cu = 1;
                real cv = fabs(phimin) / total;
                real du = 0;
                real dv = 0;
                params.push_back(torus_param(umin, umax, vmin, vmax, cu, cv, du, dv, nReverse));
            }
            {
                real vmin = 0;
                real vmax = phimax / 360.0;
                real cu = 1;
                real cv = fabs(phimax) / total;
                real du = 0;
                real dv = fabs(phimin) / total;
                params.push_back(torus_param(umin, umax, vmin, vmax, cu, cv, du, dv, nReverse));
            }
        }
        else if (phimin >= 0 && phimax < 0)
        {
            real total = phimin - phimax;
            {
                real vmin = (360.0 + phimax) / 360.0;
                real vmax = 1.0;
                real cu = 1;
                real cv = fabs(phimax) / total;
                real du = 0;
                real dv = fabs(phimin) / total;
                params.push_back(torus_param(umin, umax, vmin, vmax, cu, cv, du, dv, Reverse2(nReverse)));
            }
            {
                real vmin = 0;
                real vmax = phimin / 360.0;
                real cu = 1;
                real cv = fabs(phimin) / total;
                real du = 0;
                real dv = 0;
                params.push_back(torus_param(umin, umax, vmin, vmax, cu, cv, du, dv, Reverse2(nReverse)));
            }
        }
        else
        { //phimin<0 && phimax<0
            if (phimin < phimax)
            {
                //-300 -60
                //60 300
                real vmin = (360.0 + phimin) / 360.0;
                real vmax = (360.0 + phimax) / 360.0;
                params.push_back(torus_param(umin, umax, vmin, vmax, nReverse));
            }
            else
            { //phimax<phimin
                //-60 -300
                //300 60
                real vmin = (360.0 + phimax) / 360.0;
                real vmax = (360.0 + phimin) / 360.0;
                params.push_back(torus_param(umin, umax, vmin, vmax, Reverse2(nReverse)));
            }
        }

        std::unique_ptr<exhibited_composite_intersection> cinter(new exhibited_composite_intersection());
        for (int i = 0; i < params.size(); i++)
        {
            real umin = params[i].umin;
            real umax = params[i].umax;
            real vmin = params[i].vmin;
            real vmax = params[i].vmax;
            real cu = params[i].cu;
            real cv = params[i].cv;
            real du = params[i].du;
            real dv = params[i].dv;
            int nReverse = params[i].nReverse;

            cinter->add(
                new uv_correct_intersection(
                    new m4_transformed_intersection(
                        new torus_intersection(vector3(0, 0, 0), majrad, minrad, umin, umax, vmin, vmax), mat),
                    umin,
                    umax,
                    vmin,
                    vmax,
                    cu,
                    cv,
                    du,
                    dv,
                    nReverse));
        }

        cinter->construct();

        inter_.reset(cinter.release());
    }

    ri_torus_intersection::~ri_torus_intersection()
    {
        ;
    }

    bool ri_torus_intersection::test(const ray& r, real dist) const
    {
        return inter_->test(r, dist);
    }

    bool ri_torus_intersection::test(test_info* info, const ray& r, real dist) const
    {
        return inter_->test(info, r, dist);
    }

    void ri_torus_intersection::finalize(test_info* info, const ray& r, real dist) const
    {
        /**/
    }

    vector3 ri_torus_intersection::min() const
    {
        return inter_->min();
    }

    vector3 ri_torus_intersection::max() const
    {
        return inter_->max();
    }
}
