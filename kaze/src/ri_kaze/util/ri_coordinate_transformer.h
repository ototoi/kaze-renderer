#ifndef RI_COORDINATE_TRANSFORMER_H
#define RI_COORDINATE_TRANSFORMER_H

#include "types.h"
#include <string>
#include <vector>
#include <map>

namespace ri
{
    class ri_camera_options;
    class ri_transform;
    class ri_attributes;
    class ri_coordinate_system;

    class ri_coordinate_transformer
    {
    public:
        ri_coordinate_transformer();
        kaze::matrix4 get_matrix(const char* from, const char* to) const;
        void get_matrices(std::vector<std::string>& names, std::vector<kaze::matrix4>& mats,
                          const char* current = "world") const;

    public:
        kaze::vector3 transform(const char* from, const char* to, const kaze::vector3& v) const;
        kaze::vector3 vtransform(const char* from, const char* to, const kaze::vector3& v) const;
        kaze::vector3 ntransform(const char* from, const char* to, const kaze::vector3& v) const;

    public:
        void set_camera_to_world(const kaze::matrix4& mat);
        void set_world_to_camera(const kaze::matrix4& mat);
        void set_object_to_world(const kaze::matrix4& mat);
        void set_world_to_object(const kaze::matrix4& mat);
        void set_shader_to_world(const kaze::matrix4& mat);
        void set_world_to_shader(const kaze::matrix4& mat);

    public:
        void set_camera_matrices(const ri_camera_options* pCmr);
        void set_attributes(const ri_attributes* pAttr);
        void set(const ri_coordinate_system* pCoords);

    protected:
        int get_coordinates_type(const char* name) const;

        kaze::matrix4 get_user_matrix(const char* name) const;

    protected:
        kaze::matrix4 object_to_world_;
        kaze::matrix4 world_to_object_;
        kaze::matrix4 shader_to_world_;
        kaze::matrix4 world_to_shader_;
        kaze::matrix4 world_to_camera_;
        kaze::matrix4 camera_to_world_;
        kaze::matrix4 camera_to_screen_;
        kaze::matrix4 screen_to_camera_;
        kaze::matrix4 NDC_to_screen_;
        kaze::matrix4 screen_to_NDC_;
        kaze::matrix4 screen_to_raster_;
        kaze::matrix4 raster_to_screen_;
        std::map<std::string, kaze::matrix4> users_;
    };
}

#endif
