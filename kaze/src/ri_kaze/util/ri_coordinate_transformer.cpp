#include "ri.h"
#include "ri_coordinate_transformer.h"

#include "ri_camera_options.h"
#include "ri_attributes.h"
#include "ri_coordinate_system.h"

namespace ri
{
    using namespace kaze;
    
    enum
    {
        RI_COORDINATE_SYSTEM_RASTER = 0,
        RI_COORDINATE_SYSTEM_NDC = 1,
        RI_COORDINATE_SYSTEM_SCREEN = 2,
        RI_COORDINATE_SYSTEM_CAMERA = 3,
        RI_COORDINATE_SYSTEM_WORLD = 4,
        RI_COORDINATE_SYSTEM_OBJECT = 5,
        RI_COORDINATE_SYSTEM_SHADER = 6,
        RI_COORDINATE_SYSTEM_USER = 7
    };

    // RI_RASTER, RI_SCREEN, RI_CAMERA, RI_WORLD, RI_OBJECT

    static matrix4 ConvertMatrix(const ri_transform& trns)
    {
        ri_transform s(trns);
        s.transpose();
        const float* f = s.get();
        matrix4 tmp(f);
        return tmp;
    }

    static inline vector3 mul_n(const matrix4& m, const vector3& v)
    {
        return vector3(m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
                       m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
                       m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
    }

    static inline vector3 mul_v(const matrix4& m, const vector3& v)
    {
        return vector3(m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2],
                       m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2],
                       m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2]);
    }

    static inline vector3 mul_p(const matrix4& m, const vector3& v)
    {
        real iR =
            real(1) / (m[3][0] * v[0] + m[3][1] * v[1] + m[3][2] * v[2] + m[3][3]);
        return vector3(
            (m[0][0] * v[0] + m[0][1] * v[1] + m[0][2] * v[2] + m[0][3]) * iR,
            (m[1][0] * v[0] + m[1][1] * v[1] + m[1][2] * v[2] + m[1][3]) * iR,
            (m[2][0] * v[0] + m[2][1] * v[1] + m[2][2] * v[2] + m[2][3]) * iR);
    }

    static matrix4 GetIdentity()
    {
        return matrix4(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1);
    }

    ri_coordinate_transformer::ri_coordinate_transformer()
    {
        world_to_camera_ = GetIdentity();
        camera_to_world_ = GetIdentity();
        object_to_world_ = GetIdentity();
        world_to_object_ = GetIdentity();
        camera_to_screen_ = GetIdentity();
        screen_to_camera_ = GetIdentity();

        screen_to_NDC_ = GetIdentity();
        NDC_to_screen_ = GetIdentity();

        screen_to_raster_ = GetIdentity();
        raster_to_screen_ = GetIdentity();
    }

    vector3 ri_coordinate_transformer::transform(const char* from, const char* to,
                                                 const vector3& v) const
    {
        matrix4 mat = get_matrix(from, to);
        return mul_p(mat, v);
    }

    vector3 ri_coordinate_transformer::vtransform(const char* from, const char* to,
                                                  const vector3& v) const
    {
        matrix4 mat = get_matrix(from, to);
        return mul_v(mat, v);
    }

    vector3 ri_coordinate_transformer::ntransform(const char* from, const char* to,
                                                  const vector3& v) const
    {
        matrix4 mat = get_matrix(from, to);
        return mul_n(~mat, v);
    }

    namespace
    {
        struct T2I
        {
            const char* name;
            int num;
        };

        static T2I COORDINATES[] = {
            {RI_RASTER, RI_COORDINATE_SYSTEM_RASTER},
            {"NDC", RI_COORDINATE_SYSTEM_NDC},
            {RI_SCREEN, RI_COORDINATE_SYSTEM_SCREEN},
            {RI_CAMERA, RI_COORDINATE_SYSTEM_CAMERA},
            {RI_WORLD, RI_COORDINATE_SYSTEM_WORLD},
            {RI_OBJECT, RI_COORDINATE_SYSTEM_OBJECT},
            {"shader", RI_COORDINATE_SYSTEM_SHADER}, // object
            {NULL, -1},
        };

        static int GetPreDefinedCoordinates(const char* name)
        {
            int i = 0;
            while (COORDINATES[i].name)
            {
                if (strcmp(COORDINATES[i].name, name) == 0)
                    return COORDINATES[i].num;
                i++;
            }
            return -1;
        }
    }

    int ri_coordinate_transformer::get_coordinates_type(const char* name) const
    {
        if (users_.find(name) != users_.end())
        {
            return RI_COORDINATE_SYSTEM_USER;
        }
        else
        {
            return GetPreDefinedCoordinates(name);
        }
    }

    matrix4 ri_coordinate_transformer::get_user_matrix(const char* name) const
    {
        std::map<std::string, matrix4>::const_iterator it = users_.find(name);
        if (it != users_.end())
        {
            return it->second;
        }
        else
        {
            return GetIdentity();
        }
    }

    matrix4 ri_coordinate_transformer::get_matrix(const char* from,
                                                  const char* to) const
    {
        int nFrom = get_coordinates_type(from);
        int nTo = get_coordinates_type(to);

        if (nFrom < 0)
            return GetIdentity();
        if (nTo < 0)
            return GetIdentity();

        if (nFrom == nTo)
        {
            if (nFrom != RI_COORDINATE_SYSTEM_USER)
            {
                return GetIdentity();
            }
        }

        switch (nFrom)
        {
        case RI_COORDINATE_SYSTEM_RASTER:
        {
            switch (nTo)
            {
            // case RI_COORDINATE_SYSTEM_RASTER:
            case RI_COORDINATE_SYSTEM_NDC:
                return screen_to_NDC_ * get_matrix(from, RI_SCREEN);
            case RI_COORDINATE_SYSTEM_SCREEN:
                return raster_to_screen_;
            case RI_COORDINATE_SYSTEM_CAMERA:
                return screen_to_camera_ * get_matrix(from, RI_SCREEN);
            case RI_COORDINATE_SYSTEM_WORLD:
                return camera_to_world_ * get_matrix(from, RI_CAMERA);
            case RI_COORDINATE_SYSTEM_OBJECT:
                return world_to_object_ * get_matrix(from, RI_WORLD);
            case RI_COORDINATE_SYSTEM_SHADER:
                return world_to_shader_ * get_matrix(from, RI_WORLD);
            case RI_COORDINATE_SYSTEM_USER:
            {
                matrix4 user_to_world = get_user_matrix(to); // user2world
                matrix4 world_to_user = ~user_to_world;

                return world_to_user * get_matrix(from, RI_WORLD);
            }
            }
        }
        break;
        case RI_COORDINATE_SYSTEM_NDC:
        {
            switch (nTo)
            {
            case RI_COORDINATE_SYSTEM_RASTER:
                return screen_to_raster_ * get_matrix(from, RI_SCREEN);
            // case RI_COORDINATE_SYSTEM_NDC:
            case RI_COORDINATE_SYSTEM_SCREEN:
                return NDC_to_screen_;
            case RI_COORDINATE_SYSTEM_CAMERA:
                return screen_to_camera_ * get_matrix(from, RI_SCREEN);
            case RI_COORDINATE_SYSTEM_WORLD:
                return camera_to_world_ * get_matrix(from, RI_CAMERA);
            case RI_COORDINATE_SYSTEM_OBJECT:
                return world_to_object_ * get_matrix(from, RI_WORLD);
            case RI_COORDINATE_SYSTEM_SHADER:
                return world_to_shader_ * get_matrix(from, RI_WORLD);
            case RI_COORDINATE_SYSTEM_USER:
            {
                matrix4 user_to_world = get_user_matrix(to); // user2world
                matrix4 world_to_user = ~user_to_world;

                return world_to_user * get_matrix(from, RI_WORLD);
            }
            }
        }
        break;
        case RI_COORDINATE_SYSTEM_SCREEN: // screen
        {
            switch (nTo)
            {
            case RI_COORDINATE_SYSTEM_RASTER:
                return screen_to_raster_; // screen
            case RI_COORDINATE_SYSTEM_NDC:
                return screen_to_NDC_; // NDC
            // case RI_COORDINATE_SYSTEM_SCREEN:
            case RI_COORDINATE_SYSTEM_CAMERA:
                return screen_to_camera_;
            case RI_COORDINATE_SYSTEM_WORLD:
                return camera_to_world_ * get_matrix(from, RI_CAMERA);
            case RI_COORDINATE_SYSTEM_OBJECT:
                return world_to_object_ * get_matrix(from, RI_WORLD);
            case RI_COORDINATE_SYSTEM_SHADER:
                return world_to_shader_ * get_matrix(from, RI_WORLD);
            case RI_COORDINATE_SYSTEM_USER:
            {
                matrix4 user_to_world = get_user_matrix(to); // user2world
                matrix4 world_to_user = ~user_to_world;

                return world_to_user * get_matrix(from, RI_WORLD);
            }
            }
        }
        break;
        case RI_COORDINATE_SYSTEM_CAMERA: // camera
        {
            switch (nTo)
            {
            case RI_COORDINATE_SYSTEM_RASTER:
                return screen_to_raster_ * get_matrix(from, RI_SCREEN);
            case RI_COORDINATE_SYSTEM_NDC:
                return screen_to_NDC_ * get_matrix(from, RI_SCREEN); // NDC
            case RI_COORDINATE_SYSTEM_SCREEN:
                return camera_to_screen_; // screen
            // case RI_COORDINATE_SYSTEM_CAMERA:return
            case RI_COORDINATE_SYSTEM_WORLD:
                return camera_to_world_;
            case RI_COORDINATE_SYSTEM_OBJECT:
                return world_to_object_ * camera_to_world_; // c->w->object
            case RI_COORDINATE_SYSTEM_SHADER:
                return world_to_shader_ * camera_to_world_;
            case RI_COORDINATE_SYSTEM_USER:
            {
                matrix4 user_to_world = get_user_matrix(to); // user2world
                matrix4 world_to_user = ~user_to_world;

                return world_to_user * camera_to_world_;
            }
            }
        }
        break;
        case RI_COORDINATE_SYSTEM_WORLD: // world
        {
            switch (nTo)
            {
            case RI_COORDINATE_SYSTEM_RASTER:
                return screen_to_raster_ * get_matrix(from, RI_SCREEN);
            case RI_COORDINATE_SYSTEM_NDC:
                return screen_to_NDC_ * get_matrix(from, RI_SCREEN); // NDC
            case RI_COORDINATE_SYSTEM_SCREEN:
                return camera_to_screen_ * get_matrix(from, RI_CAMERA); // screen
            case RI_COORDINATE_SYSTEM_CAMERA:
                return world_to_camera_;
            // case RI_COORDINATE_SYSTEM_WORLD
            case RI_COORDINATE_SYSTEM_OBJECT:
                return world_to_object_;
            case RI_COORDINATE_SYSTEM_SHADER:
                return world_to_shader_;
            case RI_COORDINATE_SYSTEM_USER:
            {
                matrix4 user_to_world = get_user_matrix(to); // user2world
                matrix4 world_to_user = ~user_to_world;

                return world_to_user;
            }
            }
        }
        break;
        case RI_COORDINATE_SYSTEM_OBJECT: // object
        {
            switch (nTo)
            {
            case RI_COORDINATE_SYSTEM_RASTER:
                return screen_to_raster_ * get_matrix(from, RI_SCREEN);
            case RI_COORDINATE_SYSTEM_NDC:
                return screen_to_NDC_ * get_matrix(from, RI_SCREEN); // NDC
            case RI_COORDINATE_SYSTEM_SCREEN:
                return camera_to_screen_ * get_matrix(from, RI_CAMERA); // screen
            case RI_COORDINATE_SYSTEM_CAMERA:
                return world_to_camera_ * object_to_world_; // camera
            case RI_COORDINATE_SYSTEM_WORLD:
                return object_to_world_; // world
            // case RI_COORDINATE_SYSTEM_OBJECT:
            case RI_COORDINATE_SYSTEM_SHADER:
                return world_to_shader_ * object_to_world_;
            case RI_COORDINATE_SYSTEM_USER:
            {
                matrix4 user_to_world = get_user_matrix(to); // user2world
                matrix4 world_to_user = ~user_to_world;

                return world_to_user * object_to_world_;
            }
            }
        }
        case RI_COORDINATE_SYSTEM_SHADER: // shader
        {
            switch (nTo)
            {
            case RI_COORDINATE_SYSTEM_RASTER:
                return screen_to_raster_ * get_matrix(from, RI_SCREEN);
            case RI_COORDINATE_SYSTEM_NDC:
                return screen_to_NDC_ * get_matrix(from, RI_SCREEN); // NDC
            case RI_COORDINATE_SYSTEM_SCREEN:
                return camera_to_screen_ * get_matrix(from, RI_CAMERA); // screen
            case RI_COORDINATE_SYSTEM_CAMERA:
                return world_to_camera_ * shader_to_world_; // camera
            case RI_COORDINATE_SYSTEM_WORLD:
                return shader_to_world_; // world
            case RI_COORDINATE_SYSTEM_OBJECT:
                return world_to_object_ * shader_to_world_;
            // case RI_COORDINATE_SYSTEM_SHADER:
            case RI_COORDINATE_SYSTEM_USER:
            {
                matrix4 user_to_world = get_user_matrix(to); // user2world
                matrix4 world_to_user = ~user_to_world;

                return world_to_user * object_to_world_;
            }
            }
        }
        case RI_COORDINATE_SYSTEM_USER: // user
        {
            matrix4 user_to_world = get_user_matrix(from); // user2world
            // matrix4 world_to_user = ~user_to_world;

            switch (nTo)
            {
            case RI_COORDINATE_SYSTEM_RASTER:
                return screen_to_raster_ * get_matrix(from, RI_SCREEN);
            case RI_COORDINATE_SYSTEM_NDC:
                return screen_to_NDC_ * get_matrix(from, RI_SCREEN); // NDC
            case RI_COORDINATE_SYSTEM_SCREEN:
                return camera_to_screen_ * get_matrix(from, RI_CAMERA); // screen
            case RI_COORDINATE_SYSTEM_CAMERA:
                return world_to_camera_ * user_to_world; // camera
            case RI_COORDINATE_SYSTEM_WORLD:
                return user_to_world; // world
            case RI_COORDINATE_SYSTEM_OBJECT:
                return world_to_object_ * user_to_world; // object
            case RI_COORDINATE_SYSTEM_SHADER:
                return world_to_shader_ * user_to_world;
            case RI_COORDINATE_SYSTEM_USER:
            {
                if (strcmp(from, to) == 0)
                    return GetIdentity();
                matrix4 user_to_world2 = get_user_matrix(to); // user2world
                matrix4 world_to_user2 = ~user_to_world2;
                return world_to_user2 * user_to_world;
            }
            }
        }
        }

        return GetIdentity();
    }

    typedef std::map<std::string, matrix4> map_type;
    typedef map_type::const_iterator citer;
    void ri_coordinate_transformer::get_matrices(std::vector<std::string>& names,
                                                 std::vector<matrix4>& mats,
                                                 const char* current) const
    {
        int nCurrent = get_coordinates_type(current);
        if (nCurrent < 0)
            return;
        int i = 0;
        while (COORDINATES[i].name)
        {
            if (COORDINATES[i].num != nCurrent)
            {
                names.push_back(COORDINATES[i].name);
                mats.push_back(get_matrix(current, COORDINATES[i].name));
            }
            i++;
        }
        for (citer it = users_.begin(); it != users_.end(); it++)
        {
            names.push_back(it->first);
            mats.push_back(get_matrix(current, it->first.c_str()));
        }
    }

    void ri_coordinate_transformer::set_camera_to_world(const matrix4& mat)
    {
        camera_to_world_ = mat;
        world_to_camera_ = ~mat;
    }

    void ri_coordinate_transformer::set_world_to_camera(const matrix4& mat)
    {
        world_to_camera_ = mat;
        camera_to_world_ = ~mat;
    }

    void ri_coordinate_transformer::set_object_to_world(const matrix4& mat)
    {
        object_to_world_ = mat;
        world_to_object_ = ~mat;
    }

    void ri_coordinate_transformer::set_world_to_object(const matrix4& mat)
    {
        world_to_object_ = mat;
        object_to_world_ = ~mat;
    }

    void ri_coordinate_transformer::set_shader_to_world(const matrix4& mat)
    {
        shader_to_world_ = mat;
        world_to_shader_ = ~mat;
    }

    void ri_coordinate_transformer::set_world_to_shader(const matrix4& mat)
    {
        world_to_shader_ = mat;
        shader_to_world_ = ~mat;
    }

    void ri_coordinate_transformer::set_camera_matrices(
        const ri_camera_options* pCmr)
    {
        camera_to_screen_ = GetIdentity();
        screen_to_camera_ = GetIdentity();
        NDC_to_screen_ = GetIdentity();
        screen_to_NDC_ = GetIdentity();
        screen_to_raster_ = GetIdentity();
        raster_to_screen_ = GetIdentity();

        matrix4 mat = ConvertMatrix(pCmr->get_world_to_camera());
        set_world_to_camera(mat);
    }

    void ri_coordinate_transformer::set_attributes(const ri_attributes* pAttr)
    {
        std::vector<const ri_coordinate_system*> coords;
        pAttr->get_coordinate_system(coords);
        size_t sz = coords.size();
        for (size_t i = 0; i < sz; i++)
        {
            this->set(coords[i]);
        }
    }

    void ri_coordinate_transformer::set(const ri_coordinate_system* pCoords)
    {
        std::string name = pCoords->get_name();
        matrix4 mat = ConvertMatrix(pCoords->get_transform());
        users_[name] = mat; //
    }
}
