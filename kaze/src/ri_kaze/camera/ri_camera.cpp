#define _USE_MATH_DEFINES
#include <math.h>

#include "ri_camera.h"
#include "ray_shooter.h"
#include "types.h"
#include "values.h"

#include <memory>
#include <iostream>

namespace kaze
{

    typedef ri_camera::sequence_t sequence_t;

    static int GetIndex(const std::vector<sequence_t>& seq, real t)
    {
        size_t sz = seq.size();
        if (t < seq[0].t) return 0;
        for (size_t i = 0; i < sz - 1; i++)
        {
            if (seq[i].t <= t && t < seq[i + 1].t) return (int)i;
        }
        return (int)sz - 2;
    }

    static sequence_t GetLinear(const sequence_t& s0, const sequence_t& s1, real a)
    {
        sequence_t tmp;
        tmp.t = (1 - a) * s0.t + (a)*s1.t;
        tmp.mat = (1 - a) * s0.mat + (a)*s1.mat;
        return tmp;
    }

    static sequence_t GetMotion(real t, const std::vector<sequence_t>& seq)
    {
        int idx = GetIndex(seq, t);
        real a = (t - seq[idx].t) / (seq[idx + 1].t - seq[idx].t);
        return GetLinear(seq[idx], seq[idx + 1], a);
    }

    class ri_perspective_ray_shooter : public ray_shooter
    {
    public:
        ri_perspective_ray_shooter(const vector3& org, const vector3& x, const vector3& y, const vector3& z, real angle, real aspect);

    public:
        void init(const vector3& org, const vector3& x, const vector3& y, const vector3& z, real angle, real aspect);

    public:
        ray shoot(real i, real j) const;
        void shoot_ray(real i, real j, vector3& org, vector3& dir) const;

    public:
        vector3 origin() const { return from_; }
        vector3 x() const { return axis_x_; }
        vector3 y() const { return axis_y_; }
        vector3 z() const { return axis_z_; }
    private:
        vector3 from_;
        vector3 axis_x_;
        vector3 axis_y_;
        vector3 axis_z_;

        real cof_x_;
        real cof_y_;
    };

    class ri_orthogonal_ray_shooter : public ray_shooter
    {
    public:
        ri_orthogonal_ray_shooter(const vector3& org, const vector3& x, const vector3& y, const vector3& z, real width, real height);

    public:
        void init(const vector3& org, const vector3& x, const vector3& y, const vector3& z, real width, real height);

    public:
        ray shoot(real i, real j) const;

    public:
        vector3 origin() const { return from_; }
        vector3 x() const { return axis_x_; }
        vector3 y() const { return axis_y_; }
        vector3 z() const { return axis_z_; }
    private:
        vector3 from_;
        vector3 axis_x_;
        vector3 axis_y_;
        vector3 axis_z_;
    };

    //---------------------------------------------------------------------------------------
    ri_perspective_ray_shooter::ri_perspective_ray_shooter(const vector3& org, const vector3& x, const vector3& y, const vector3& z, real angle, real aspect)
    {
        init(org, x, y, z, angle, aspect);
    }

    void ri_perspective_ray_shooter::init(const vector3& org, const vector3& x, const vector3& y, const vector3& z, real angle, real aspect)
    {
        //left hand
        from_ = org;
        axis_z_ = z;
        axis_y_ = y;
        axis_x_ = x;

        //tangent_ = std::tan(angle*0.5);
        //aspect_ = aspect;
        real tangent = std::tan(angle * 0.5);
        cof_x_ = aspect * tangent;
        cof_y_ = tangent;
    }

    void ri_perspective_ray_shooter::shoot_ray(
        real i, real j,
        vector3& org, vector3& dir) const
    {
        real WW = cof_x_ * i;
        real HH = cof_y_ * j;

        org = from_;
        dir = normalize(WW * axis_x_ + HH * axis_y_ + axis_z_);
    }

    ray ri_perspective_ray_shooter::shoot(real i, real j) const
    {
        vector3 org;
        vector3 dir;
        this->shoot_ray(i, j, org, dir);
        return ray(org, dir);
    }

    ri_orthogonal_ray_shooter::ri_orthogonal_ray_shooter(const vector3& org, const vector3& x, const vector3& y, const vector3& z, real width, real height)
    {
        init(org, x, y, z, width, height);
    }

    void ri_orthogonal_ray_shooter::init(const vector3& org, const vector3& x, const vector3& y, const vector3& z, real width, real height)
    {
        //left hand
        from_ = org;
        axis_z_ = z;
        axis_y_ = y * (height / 2);
        axis_x_ = x * (width / 2);
    }

    ray ri_orthogonal_ray_shooter::shoot(real i, real j) const
    {
        //assert(is_valid_range(i,j));

        return ray(from_ + (i * axis_x_) + (j * axis_y_), axis_z_);
    }

    static ri_perspective_ray_shooter* CreateRayShooter(const matrix4& mat, real angle, real aspect)
    {
        vector3 org = mat * vector3(0, 0, 0);

        vector3 x = mat * vector3(1, 0, 0) - org;
        vector3 y = mat * vector3(0, 1, 0) - org;
        vector3 z = mat * vector3(0, 0, 1) - org;

        x = normalize(x);
        y = normalize(y);
        z = normalize(z);

        angle = values::radians(angle); //degree->radians
        return new ri_perspective_ray_shooter(org, x, y, z, angle, aspect);
    }

    ri_perspective_camera::ri_perspective_camera(const matrix4& mat, real angle, real aspect)
    {
        l_ = 0;
        r_ = 1;
        b_ = 0;
        t_ = 1;
        focaldistance_ = 0;
        lensradius_ = 0;
        rs_ = CreateRayShooter(mat, angle, aspect);
        angle_ = angle;
        aspect_ = aspect;
    }

    ri_perspective_camera::ri_perspective_camera(const matrix4& mat, real angle, real aspect, real l, real r, real b, real t)
    {
        real ll = -aspect;
        real rr = +aspect;
        real bb = -1.0f;
        real tt = +1.0f;

        real lx = (l - ll) / (rr - ll);
        real rx = (r - ll) / (rr - ll);
        real bx = (b - bb) / (tt - bb);
        real tx = (t - bb) / (tt - bb);

        if (bx > tx) std::swap(bx, tx);

        l_ = lx;
        r_ = rx;
        b_ = bx;
        t_ = tx;
        focaldistance_ = 0;
        lensradius_ = 0;

        rs_ = CreateRayShooter(mat, angle, aspect);
        angle_ = angle;
        aspect_ = aspect;
    }

    ri_perspective_camera::ri_perspective_camera(const std::vector<sequence_t>& seq, real angle, real aspect)
        : seq_(seq)
    {
        l_ = 0;
        r_ = 1;
        b_ = 0;
        t_ = 1;
        focaldistance_ = 0;
        lensradius_ = 0;

        rs_ = 0;
        angle_ = angle;
        aspect_ = aspect;
    }

    ri_perspective_camera::ri_perspective_camera(const std::vector<sequence_t>& seq, real angle, real aspect, real l, real r, real b, real t)
        : seq_(seq)
    {
        real ll = -aspect;
        real rr = +aspect;
        real bb = -1.0f;
        real tt = +1.0f;

        real lx = (l - ll) / (rr - ll);
        real rx = (r - ll) / (rr - ll);
        real bx = (b - bb) / (tt - bb);
        real tx = (t - bb) / (tt - bb);

        if (bx > tx) std::swap(bx, tx);

        l_ = lx;
        r_ = rx;
        b_ = bx;
        t_ = tx;
        focaldistance_ = 0;
        lensradius_ = 0;

        rs_ = 0;
        angle_ = angle;
        aspect_ = aspect;
    }

    ri_perspective_camera::~ri_perspective_camera()
    {
        if (rs_) delete rs_;
    }

    ray ri_perspective_camera::shoot(real i, real j, real t) const
    {
        i = (i + 1) * 0.5; //0..1
        j = (j + 1) * 0.5; //0..1

        real ix = i * (r_ - l_) + l_;
        real jx = j * (t_ - b_) + b_;

        ix = 2 * ix - 1;
        jx = 2 * jx - 1;

        if (rs_)
        {
            return rs_->shoot(ix, jx);
        }
        else
        {
            matrix4 mat = GetMotion(t, seq_).mat;
            std::unique_ptr<ri_perspective_ray_shooter> rs(CreateRayShooter(mat, angle_, aspect_));
            return rs->shoot(ix, jx);
        }
    }

    ray ri_perspective_camera::shoot(real i, real j) const
    {
        return shoot(i, j, 0);
    }

    static vector2 SampleDisk(real u, real v)
    {
        real r = sqrt(u);
        real theta = 2.0 * M_PI * v;
        real x = r * cos(theta);
        real y = r * sin(theta);
        return vector2(x, y);
    }

    static vector2 SampleIris(real u, real v)
    {
        static int blades = 5;
        static real delta = 2.0 * M_PI / blades;

        real r = sqrt(u);
        real t = 2.0 * M_PI * v;

        int nBlade = (int)floor(blades * v);
        real fRate = (blades * v - nBlade);

        real rr = cos(delta * (0.5 - fabs(fRate - 0.5)));

        real x = r * rr * cos(t);
        real y = r * rr * sin(t);
        return vector2(x, y);
    }

    ray ri_perspective_camera::shoot(const pixel_sample& sample, int width, int height) const
    {
        real xx = sample.x + sample.dx;
        real yy = sample.y + sample.dy;
        real i = xx / width;
        real j = 1 - (yy / height);

        real ix = i * (r_ - l_) + l_;
        real jx = j * (t_ - b_) + b_;

        ix = 2 * ix - 1;
        jx = 2 * jx - 1;

        real tm = sample.time;

        std::unique_ptr<ri_perspective_ray_shooter> ap;
        const ri_perspective_ray_shooter* rs = 0;
        if (rs_)
        {
            rs = static_cast<const ri_perspective_ray_shooter*>(rs_);
        }
        else
        {
            matrix4 mat = GetMotion(tm, seq_).mat;
            ap.reset(CreateRayShooter(mat, angle_, aspect_));
            rs = ap.get();
        }

        vector3 org;
        vector3 dir;
        rs->shoot_ray(ix, jx, org, dir);

        if (focaldistance_ > 0 && lensradius_ > 0)
        {
            real dz = dot(rs->z(), dir);
            real t = focaldistance_ / dz;
            real u = sample.u;
            real v = sample.v;
            vector2 s = SampleIris(u, v);
            real lu = lensradius_ * s[0];
            real lv = lensradius_ * s[1];

            vector3 focusP = org + t * dir;
            org = org + rs->x() * lu + rs->y() * lv;
            dir = normalize(focusP - org);
            return ray(org, dir, tm);
        }
        else
        {
            return ray(org, dir, tm);
        }
    }

    void ri_perspective_camera::set_depth_of_field(real fstop, real focallength, real focaldistance)
    {
        real lensdiameter = focallength / fstop;
        real lensradius = lensdiameter * 0.5;
        focaldistance_ = focaldistance;
        lensradius_ = lensradius;
    }

    //---------------------------------------------------

    ri_orthogonal_camera::ri_orthogonal_camera(const matrix4& mat, real width, real height)
    {
        vector3 org = mat * vector3(0, 0, 0);

        vector3 x = mat * vector3(1, 0, 0) - org;
        vector3 y = mat * vector3(0, 1, 0) - org;
        vector3 z = mat * vector3(0, 0, 1) - org;

        rs_ = new ri_orthogonal_ray_shooter(org, x, y, z, width, height);
    }
    ri_orthogonal_camera::~ri_orthogonal_camera()
    {
        delete rs_;
    }
    ray ri_orthogonal_camera::shoot(real i, real j) const
    {
        return rs_->shoot(i, j);
    }
}
