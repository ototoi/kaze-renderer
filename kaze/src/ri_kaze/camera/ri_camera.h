#ifndef KAZE_RI_CAMERA_H
#define KAZE_RI_CAMERA_H

#include "camera.h"
#include <vector>

namespace kaze
{

    class ri_camera : public camera
    {
    public:
        struct ri_motion_sequence
        {
            real t;
            matrix4 mat;
        };
        typedef ri_motion_sequence sequence_t;
    };

    class ri_perspective_camera : public ri_camera
    {
    public:
        ri_perspective_camera(const matrix4& mat, real angle, real aspect);
        ri_perspective_camera(const matrix4& mat, real angle, real aspect, real left, real right, real bottom, real top);

        ri_perspective_camera(const std::vector<sequence_t>& seq, real angle, real aspect);
        ri_perspective_camera(const std::vector<sequence_t>& seq, real angle, real aspect, real left, real right, real bottom, real top);

        ~ri_perspective_camera();

    public:
        void set_depth_of_field(real fstop, real focallength, real focaldistance);

    public:
        ray shoot(real i, real j) const;
        ray shoot(const pixel_sample& sample, int width, int height) const;

    public:
        ray shoot(real i, real j, real t) const;

    protected:
        std::vector<sequence_t> seq_;
        real l_;
        real r_;
        real b_;
        real t_;
        real focaldistance_;
        real lensradius_;
        ray_shooter* rs_;
        real angle_;
        real aspect_;
    };

    class ri_orthogonal_camera : public ri_camera
    {
    public:
        ri_orthogonal_camera(const matrix4& mat, real width, real height);
        ~ri_orthogonal_camera();

    public:
        ray shoot(real i, real j) const;

    protected:
        ray_shooter* rs_;
    };
}

#endif