#ifndef RI_RAYTRACE_RENDER_TASK_H
#define RI_RAYTRACE_RENDER_TASK_H

#include "ri_task.h"

namespace ri
{

    class ri_frame_task;

    class ri_raytrace_render_task : public ri_task
    {
    public:
        ri_raytrace_render_task(const ri_frame_task* task);
        ~ri_raytrace_render_task();
        virtual std::string type() const { return "RaytraceRender"; }
        virtual int run();

    protected:
        const ri_frame_task* task_;
    };
}

#endif
