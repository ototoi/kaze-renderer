#include "ri_autoshadows_task.h"

#include "ri_geometry.h"
#include "ri_z_render_task.h"
#include "ri_camera.h"
#include "light_transform_matrix.h"
#include "ri_light_source.h"
#include "ri_make_texture_task.h"
#include "ri_sequence_task.h"
#include "ri_zfile_render_frame.h"
#include "ri_zframebuffer_render_frame.h"
#include "ri_framebuffer_render_frame.h"
#include "ri_intersection_converter.h"
#include "ri_classifier.h"
#include "z_trace_render_task.h"
#include "count_ptr.hpp"
#include "ri_file_util.h"

#include "timer.h"

#ifdef _WIN32
#include <windows.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#else

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#endif

#include <string.h>
#include <memory>

namespace ri
{
    using namespace kaze;

    static std::string MakeFullPath(const char* szFilePath)
    {
#ifdef _WIN32
        char szFullPath[_MAX_PATH] = "";
        _fullpath(szFullPath, szFilePath, _MAX_PATH);
        return szFullPath;
#else
        char szFullPath[512];
        realpath(szFilePath, szFullPath);
        return szFullPath;
#endif
    }

    namespace
    {

        class ri_z_trace_task : public ri_task
        {
        public:
            ri_z_trace_task(z_trace_render_task* task) : task_(task) {}
            ~ri_z_trace_task() { delete task_; }
            std::string type() const { return "__z_trace_task"; }
            int run() { return task_->run(); }

        private:
            z_trace_render_task* task_;
        };

        class remove_task : public ri_task
        {
        public:
            remove_task(const char* szpath) { path_ = MakeFullPath(szpath); }
            std::string type() const { return "__remove_task"; }
            int run()
            {
                delete_file(path_.c_str());
                return 0;
            }

        private:
            std::string path_;
        };
    }

    static matrix4 ConvertMatrix(const ri_transform& trns)
    {
        ri_transform s(trns);
        s.transpose();
        const float* f = s.get();
        matrix4 tmp(f);
        return tmp;
    }

    static vector3 GetVector(const ri_light_source* l, const char* key,
                             const vector3& def)
    {
        const ri_parameters::value_type* val = l->get_parameter(key);
        if (val)
        {
            if (val->float_values.size() == 3)
            {
                return vector3(&val->float_values[0]);
            }
        }
        return def;
    }

    static bool GetFloat(const ri_light_source* l, const char* key, float& f)
    {
        const ri_parameters::value_type* val = l->get_parameter(key);
        if (val)
        {
            if (val->float_values.size() == 1)
            {
                f = val->float_values[0];
                return true;
            }
        }
        return false;
    }

    static matrix4 GetClipMatrix(const ri_light_source* l)
    {
        float coneangle = 0.5;
        GetFloat(l, "coneangle", coneangle);
        float angle = coneangle * 2;
        float near = (float)RI_EPSILON;
        float far = (float)RI_INFINITY;

        vector3 from = GetVector(l, "from", vector3(0, 0, 0));
        vector3 to = GetVector(l, "to", vector3(0, 0, 1));
        matrix4 mat = ConvertMatrix(l->get_transform());

        matrix4 w2c = spot_light_world2clip(mat * from, mat * to, near, far, angle);
        return w2c;
    }

    static void ConvertMatrix(double mat[16], const matrix4& m)
    {
        for (int j = 0; j < 4; j++)
        {
            for (int i = 0; i < 4; i++)
            {
                mat[4 * j + i] = (double)m[j][i];
            }
        }
    }

    static std::shared_ptr<ri_task> CreateTask(const ri_frame_task* task)
    {
        typedef std::set<const ri_light_source*> light_set;

        light_set lights;
        size_t sz = task->get_geometry_size();
        for (size_t i = 0; i < sz; i++)
        {
            const ri_geometry* pGeo = task->get_geomoetry_at(i);
            const ri_attributes& attr = pGeo->get_attributes();
            std::vector<const ri_light_source*> lv;
            attr.get_lightsource(lv);
            if (!lv.empty())
            {
                lights.insert(lv.begin(), lv.end());
            }
        }

        const ri_camera_options* target = NULL;
        {
            const ri_options& opt = task->get_options();
            size_t csz = opt.get_camera_size();
            if (csz)
                target = opt.get_camera_at(csz - 1);
        }

        if (lights.empty())
            return std::shared_ptr<ri_task>();

        std::vector<const ri_light_source*> lightv;
        for (light_set::const_iterator i = lights.begin(); i != lights.end(); i++)
        {
            const ri_light_source* l = *i;
            const ri_attributes& attr = l->get_attributes();
            std::string shadowmapname;
            bool bAutoShadow = attr.get("autoshadows", "shadowmapname", shadowmapname);
            if (bAutoShadow)
            {
                lightv.push_back(l);
            }
        }

        if (lightv.empty())
            return std::shared_ptr<ri_task>();

        std::unique_ptr<ri_intersection_converter> cvtr(
            new ri_intersection_converter(target));

        std::vector<std::shared_ptr<ri_task> > tasks;
        for (size_t j = 0; j < lightv.size(); j++)
        {
            // print_log("autoshadows find!\n");
            const ri_light_source* l = lightv[j];
            const ri_attributes& attr = l->get_attributes();
            bool bAutoShadow = true;
            std::string shadowmapname;
            int res = 600;
            float coneangle = 0.5;
            bAutoShadow &= attr.get("autoshadows", "shadowmapname", shadowmapname);
            bAutoShadow &= GetFloat(l, "coneangle", coneangle);
            if (bAutoShadow)
            {
                attr.get("autoshadows", "res", res);
                matrix4 clip = GetClipMatrix(l);
                std::unique_ptr<z_trace_render_task> rdr(
                    new z_trace_render_task(res, res, clip));
                {
                    size_t sz = task->get_geometry_size();
                    for (size_t i = 0; i < sz; i++)
                    {
                        const ri_geometry* pGeo = task->get_geomoetry_at(i);
                        std::shared_ptr<intersection> inter(cvtr->convert(pGeo));
                        if (inter.get())
                        {
                            rdr->add(inter);
                        }
                    }
                }

                vector3 from = GetVector(l, "from", vector3(0, 0, 0));
                vector3 to = GetVector(l, "to", vector3(0, 0, 1));
                matrix4 mat = ConvertMatrix(l->get_transform());
                matrix4 w2c = spot_light_world2camera(mat * from, mat * to);
                matrix4 c2w = ~w2c;

                // coneangle is radians
                rdr->add(
                    new ri_perspective_camera(c2w, values::degrees(2 * coneangle), 1));

                double w2c_[16];
                double w2s_[16];
                ConvertMatrix(w2c_, w2c);  // world2camera
                ConvertMatrix(w2s_, clip); // world2clip

                std::string zfilename = shadowmapname + ".z";

                rdr->add(new ri_zfile_render_frame(zfilename.c_str(), "zfile", "z", res,
                                                   res, w2c_, w2s_));
                // rdr->add_render_frame(new
                // ri_zframebuffer_render_frame(shadowmapname.c_str(), "zfile", "z", res,
                // res));

                ri_classifier cls;
                ri_options opt;

                std::unique_ptr<ri_sequence_task> seq(new ri_sequence_task());
                seq->add_task(std::shared_ptr<ri_task>(new ri_z_trace_task(rdr.release())));
                seq->add_task(std::shared_ptr<ri_task>(new ri_make_shadow_task(
                    &cls, opt, (RtToken)zfilename.c_str(), (RtToken)shadowmapname.c_str(),
                    0, NULL, NULL)));
                seq->add_task(std::shared_ptr<ri_task>(new remove_task(zfilename.c_str())));
                tasks.push_back(std::shared_ptr<ri_task>(seq.release()));
            }
        }

        std::unique_ptr<ri_sequence_task> seq(new ri_sequence_task());
        for (size_t i = 0; i < tasks.size(); i++)
        {
            seq->add_task(tasks[i]);
        }
        return std::shared_ptr<ri_task>(seq.release());
    }

    ri_autoshadows_task::ri_autoshadows_task(const ri_frame_task* task)
    {
        task_ = CreateTask(task);
    }

    int ri_autoshadows_task::run()
    {
        if (task_.get())
            return task_->run();
        return 0;
    }
}
