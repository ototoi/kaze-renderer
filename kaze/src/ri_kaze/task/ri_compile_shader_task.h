#ifndef RI_COMPILE_SHADER_TASK_H
#define RI_COMPILE_SHADER_TASK_H

#include "ri_object.h"
#include "ri_task.h"
#include "ri_classifier.h"
#include "ri_parameters.h"

namespace ri
{
    class ri_compile_shader_task : public ri_task
    {
    public:
        ri_compile_shader_task(const char* szSLPath, const char* szKSLPath);
        std::string type() const { return "CompileShader"; }
        int run();

    protected:
        ri_classifier clsf_;
        ri_parameters params_;
    };
}

#endif
