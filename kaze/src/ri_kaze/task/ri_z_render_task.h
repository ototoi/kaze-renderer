#ifndef RI_Z_RENDER_TASK_H
#define RI_Z_RENDER_TASK_H

#include "ri_task.h"
#include "ri_frame_task.h"
#include <vector>

namespace ri
{
    class ri_z_render_task : public ri_task
    {
    public:
        ri_z_render_task(const ri_frame_task* task);
        virtual std::string type() const { return "ZRender"; }
        virtual int run();

    protected:
        std::shared_ptr<ri_task> task_;
    };
}

#endif
