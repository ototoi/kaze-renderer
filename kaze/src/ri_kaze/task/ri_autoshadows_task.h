#ifndef RI_AUTOSHADOWS_TASK_H
#define RI_AUTOSHADOWS_TASK_H

#include "ri_task.h"
#include "ri_frame_task.h"

namespace ri
{

    class ri_autoshadows_task : public ri_task
    {
    public:
        ri_autoshadows_task(const ri_frame_task* task);

    public:
        std::string type() const { return "Autoshadows"; }
        int run();

    protected:
        std::shared_ptr<ri_task> task_;
    };
}

#endif
