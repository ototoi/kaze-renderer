#include "ri_compile_shader_task.h"
#include "ri_path_resolver.h"

#include "logger.h"

#include <limits.h>
#include <stdlib.h>

#ifndef _WIN32
#include <errno.h>
#endif

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif

#include <thread>
#include <mutex>

namespace ri
{
    using namespace kaze;

    static std::string MakeFullPath(const char* szFilePath)
    {
#ifdef _WIN32
        char szFullPath[_MAX_PATH] = "";
        _fullpath(szFullPath, szFilePath, _MAX_PATH);
        return szFullPath;
#else
        char szFullPath[512];
        realpath(szFilePath, szFullPath);
        return szFullPath;
#endif
    }

#ifdef _WIN32
    static int ExecuteProcess(const char* szCmd, const char* szArg)
    {
        PROCESS_INFORMATION pi;
        STARTUPINFOA si;

        ZeroMemory(&si, sizeof(si));
        si.cb = sizeof(si);

        si.cb = sizeof(STARTUPINFO);
        si.dwFlags = STARTF_USESHOWWINDOW;
        si.wShowWindow = SW_SHOWNORMAL;

#if 0
            int nWINDOW = CREATE_NEW_CONSOLE;
#else
        int nWINDOW = CREATE_NO_WINDOW;
#endif
        std::string strCmdLine;
        strCmdLine += szCmd;
        strCmdLine += " ";
        strCmdLine += szArg;

        std::vector<char> s_strCmdLine(strCmdLine.size() + 1);
        strcpy(&s_strCmdLine[0], strCmdLine.c_str());
        s_strCmdLine.back() = 0;

        BOOL bRet = ::CreateProcessA(NULL, (char*)(&s_strCmdLine[0]), NULL, NULL,
                                     FALSE, nWINDOW, NULL, NULL, &si, &pi);

        if (bRet)
        {
            DWORD nExitCode = 0;
            ::CloseHandle(pi.hThread);
            ::WaitForSingleObject(pi.hProcess, INFINITE);
            ::GetExitCodeProcess(pi.hProcess, &nExitCode);
            ::CloseHandle(pi.hProcess);
            return nExitCode;
        }

        if (!bRet)
            return -1;

        return 0;
    }
#else
    static int ExecuteProcess(const char* szCmd, const char* szArg)
    {
        std::string cmdLine;
        cmdLine += szCmd;
        cmdLine += " ";
        cmdLine += szArg;

        // printf("%s\n", cmdLine.c_str());
        int nRet = ::system(cmdLine.c_str());
        if (nRet == 127 || nRet == -1)
        {
            print_log("%s\n", strerror(errno));
            return -1;
        }
        return nRet;
    }
#endif

    static bool IsExistFile(const char* szFilePath)
    {
#ifdef _WIN32
        struct _stat s;
        int nRet = _stat(szFilePath, &s);
        return (nRet == 0);
#else
        struct stat s;
        int nRet = stat(szFilePath, &s);
        return (nRet == 0);
#endif
    }

    static size_t GetLastModifiedTime(const char* szFilePath)
    {
#ifdef _WIN32
        struct _stat s;
        int nRet = _stat(szFilePath, &s);
        return s.st_mtime; // mtime
#else
        struct stat s;
        int nRet = stat(szFilePath, &s);
        return s.st_mtime; // mtime
#endif
    }

    namespace
    {

        class compile_checker
        {
        public:
            bool IsCompiled(const std::string& strSLCmp, const std::string& strFullSL,
                            const std::string& strFullKSL)
            {
                bool bCompiled = GetCompiled(strSLCmp, strFullSL, strFullKSL);
                if (bCompiled)
                    return true;

                int nCase = 0;
                {

                    if (!IsExistFile(strSLCmp.c_str()))
                    {
                        nCase = -1;
                    }
                    else
                    {
                        if (IsExistFile(strFullSL.c_str()))
                            nCase |= 1;
                        if (IsExistFile(strFullKSL.c_str()))
                            nCase |= 2;
                    }
                }
                switch (nCase)
                {
                case -1:
                    return true; //><
                case 0:
                    return true; //><
                case 1:
                    return false;
                case 2:
                    return true; //><
                case 3:
                {
                    bool bRet = false;
                    size_t e = GetLastModifiedTime(strSLCmp.c_str());
                    size_t a = GetLastModifiedTime(strFullSL.c_str());
                    size_t b = GetLastModifiedTime(strFullKSL.c_str());
                    if (e < b)
                    {
                        if (a < b)
                            bRet = true;
                    }
                    return bRet;
                }
                }
                return false;
            }

            bool GetCompiled(const std::string& strSLCmp, const std::string& strFullSL,
                             const std::string& strFullKSL)
            {
                std::lock_guard<std::mutex> lck(mtx);
                std::string strKey = strSLCmp + "#" + strFullSL + "#" + strFullKSL;
                std::map<std::string, bool>::const_iterator it = compile_map_.find(strKey);
                if (it != compile_map_.end())
                {
                    return it->second;
                }
                return false;
            }

            void SetCompiled(const std::string& strSLCmp, const std::string& strFullSL,
                             const std::string& strFullKSL, bool bSuccess)
            {
                std::lock_guard<std::mutex> lck(mtx);
                std::string strKey = strSLCmp + "#" + strFullSL + "#" + strFullKSL;
                compile_map_.insert(std::make_pair(strKey, bSuccess));
            }

            static compile_checker& get_instance()
            {
                static compile_checker mc;
                return mc;
            }

        private:
            mutable std::mutex mtx;
            // std::map<std::string, int> exist_map_;
            std::map<std::string, bool> compile_map_;
        };
    }

    ri_compile_shader_task::ri_compile_shader_task(const char* szSLPath,
                                                   const char* szKSLPath)
    {
        params_.set("sl", szSLPath);
        params_.set("ksl", szKSLPath);

        std::string strFullSL = MakeFullPath(szSLPath);
        std::string strFullKSL = MakeFullPath(szKSLPath);

        params_.set("full_sl", strFullSL.c_str());
        params_.set("full_ksl", strFullKSL.c_str());
    }

    int ri_compile_shader_task::run()
    {
        ri_path_resolver pr;
        std::string strSLCmp = pr.get_bin_path() + "kzrislc";

#ifdef _WIN32
        strSLCmp += ".exe";
#endif
        std::string strFullSL = (params_.get("full_sl"))->string_values[0];
        std::string strFullKSL = (params_.get("full_ksl"))->string_values[0];

        compile_checker& mc = compile_checker::get_instance();

        bool bCompiled = mc.IsCompiled(strSLCmp, strFullSL, strFullKSL);

        if (bCompiled)
            return 0;

        std::string strCmd;
        strCmd += "\"";
        strCmd += strSLCmp;
        strCmd += "\"";

        std::string strArg;
        strArg += "\"";
        strArg += strFullSL;
        strArg += "\"";

        strArg += " ";
        strArg += "-o";

        strArg += " ";
        strArg += "\"";
        strArg += strFullKSL;
        strArg += "\"";

        int nRet = ExecuteProcess(strCmd.c_str(), strArg.c_str());

        if (nRet == 0)
        {
            mc.SetCompiled(strSLCmp, strFullSL, strFullKSL, true);
        }
        else
        {
            mc.SetCompiled(strSLCmp, strFullSL, strFullKSL, false);
        }

        return nRet;
    }
}
