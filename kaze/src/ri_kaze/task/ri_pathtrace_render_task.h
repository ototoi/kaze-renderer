#ifndef RI_PATHTRACE_RENDER_TASK_H
#define RI_PATHTRACE_RENDER_TASK_H

#include "ri_task.h"

namespace ri
{

    class ri_frame_task;

    class ri_pathtrace_render_task : public ri_task
    {
    public:
        ri_pathtrace_render_task(const ri_frame_task* task);
        ~ri_pathtrace_render_task();
        virtual std::string type() const { return "PathTraceRender"; }
        virtual int run();

    protected:
        const ri_frame_task* task_;
    };
}

#endif
