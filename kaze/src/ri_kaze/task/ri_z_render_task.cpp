#include "ri_z_render_task.h"
#include "logger.h"

#include "ri.h"
#include "ri_intersection_converter.h"
#include "z_trace_render_task.h"
#include "ri_camera_options.h"
#include "ri_camera_options_converter.h"
#include "transform_matrix.h"

#include <memory>

namespace ri
{
    using namespace kaze;
    
    namespace
    {

        class ri_z_trace_task : public ri_task
        {
        public:
            ri_z_trace_task(z_trace_render_task* task) : task_(task) {}
            ~ri_z_trace_task() { delete task_; }
            std::string type() const { return "__z_trace_task"; }
            int run() { return task_->run(); }

        private:
            z_trace_render_task* task_;
        };
    }

    static matrix4 ConvertMatrix(const ri_transform& trns)
    {
        ri_transform s(trns);
        s.transpose();
        const float* f = s.get();
        matrix4 tmp(f);
        return tmp;
    }

    static matrix4 GetClipMatrix(const ri_camera_options* pCmr)
    {
        int nType = pCmr->get_camera_type();

        float fov = pCmr->get_fov();
        float angle = fov;
        float aspect = pCmr->get_frame_aspect_ratio();
        float left = -aspect;
        float right = +aspect;
        float bottom = -1.0f;
        float top = +1.0f;
        float near = (float)RI_EPSILON;
        float far = (float)RI_INFINITY;
        pCmr->get_screen_window(left, right, bottom, top);
        pCmr->get_clipping(near, far);

        matrix4 w2c = ConvertMatrix(pCmr->get_world_to_camera()); // w2c

        if (nType == ri_camera_options::RI_CAMERA_PERSPECTIVE)
        {
            matrix4 mc2c = camera2clip_01(values::radians(angle), aspect, near, far);
            return mc2c * w2c;
        }
        else
        {
            matrix4 mc2c = camera2clip_01(left, right, bottom, top, near, far);
            return mc2c * w2c;
        }
    }

    static std::shared_ptr<ri_task> CreateTask(const ri_frame_task* task)
    {
        const ri_options& opts = task->get_options();
        const ri_camera_options* pCmr = NULL;
        size_t csz = opts.get_camera_size();
        if (csz)
        {
            pCmr = opts.get_camera_at(csz - 1);
        }
        if (!pCmr)
            return std::shared_ptr<ri_task>();

        ri_camera_options_converter c_cvtr;

        int width, height;
        float aspect;
        pCmr->get_format(width, height, aspect);
        matrix4 clip = GetClipMatrix(pCmr);

        std::shared_ptr<camera> cmr(c_cvtr.convert_camera(pCmr));
        std::shared_ptr<render_frame> rf(c_cvtr.convert_render_frame(pCmr));

        std::unique_ptr<z_trace_render_task> rdr(
            new z_trace_render_task(width, height, clip));

        std::unique_ptr<ri_intersection_converter> cvtr(
            new ri_intersection_converter(pCmr));
        size_t sz = task->get_geometry_size();
        for (size_t i = 0; i < sz; i++)
        {
            const ri_geometry* pGeo = task->get_geomoetry_at(i);
            std::shared_ptr<intersection> inter(cvtr->convert(pGeo));
            if (inter.get())
            {
                rdr->add(inter);
            }
        }

        if (cmr.get())
        {
            rdr->add(cmr);
        }

        if (rf.get())
        {
            rdr->add(rf);
        }

        return std::shared_ptr<ri_task>(new ri_z_trace_task(rdr.release()));
    }

    ri_z_render_task::ri_z_render_task(const ri_frame_task* task)
    {
        task_ = CreateTask(task);
    }

    int ri_z_render_task::run() { return task_->run(); }
}
