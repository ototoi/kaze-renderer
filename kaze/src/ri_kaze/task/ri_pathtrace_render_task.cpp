#include "ri_pathtrace_render_task.h"
#include "ri_frame_task.h"
#include "ri_camera_options.h"
#include "ri_camera_options_converter.h"

#include "logger.h"
#include "ri_sequence_task.h"
#include "grid_pixel_sampler.h"
#include "ri_intersection_converter.h"
#include "ri_shader_converter.h"
#include "ri_light_converter.h"

#include "incremental_pathtrace_render_task.h"
#include "bvh_composite_intersection.h"
#include "ri_ray_tracer.h"

#include <thread>

namespace ri
{
    using namespace kaze;

    ri_pathtrace_render_task::ri_pathtrace_render_task(const ri_frame_task* task)
        : task_(task) {}

    ri_pathtrace_render_task::~ri_pathtrace_render_task() {}

    static int render(const ri_frame_task* task)
    {
        const ri_options& opts = task->get_options();
        const ri_camera_options* pCmr = NULL;
        size_t csz = opts.get_camera_size();
        if (csz)
        {
            pCmr = opts.get_camera_at(csz - 1);
        }
        if (!pCmr)
            return -1;

        ri_camera_options_converter c_cvtr;

        std::vector<std::shared_ptr<ri_task> > tasks;

        int xres, yres;
        float aspect;
        pCmr->get_format(xres, yres, aspect);
        std::unique_ptr<incremental_pathtrace_render_task> rdr(
            new incremental_pathtrace_render_task(xres, yres));

        std::shared_ptr<camera> cmr(c_cvtr.convert_camera(pCmr));
        if (cmr.get())
        {
            rdr->add(cmr);
        }
        std::shared_ptr<render_frame> rf(c_cvtr.convert_render_frame(pCmr));
        if (rf.get())
        {
            rdr->add(rf);
        }

        int xsamples, ysamples;
        pCmr->get_pixel_samples(xsamples, ysamples);
        std::shared_ptr<pixel_sampler> ps(new grid_pixel_sampler(xsamples, ysamples));
        rdr->add(ps);

        std::shared_ptr<tracer> rt(new ri_ray_tracer());
        rdr->add(rt);

        size_t sz = task->get_geometry_size();
        if (sz)
        {
            std::unique_ptr<bvh_composite_intersection> bvh(
                new bvh_composite_intersection());
            std::shared_ptr<ri_intersection_converter> gconv(
                new ri_intersection_converter(pCmr));

            std::shared_ptr<ri_light_converter> lconv(new ri_light_converter());
            std::shared_ptr<ri_shader_converter> sconv(new ri_shader_converter());
            sconv->set_light_converter(lconv);
            sconv->set_tracer(rt);
            gconv->set_shader_converter(sconv);

            for (size_t i = 0; i < sz; i++)
            {
                const ri_geometry* pGeo = task->get_geomoetry_at(i);
                std::shared_ptr<intersection> inter(gconv->convert(pGeo));
                if (inter.get())
                {
                    bvh->add(inter);
                }
                if ((i & 63) == 0 || i == (sz - 1))
                {
                    print_progress("loading %d%%", int(100 * float(i + 1) / sz));
                }
            }
            bvh->construct();
            dynamic_cast<ri_ray_tracer*>(rt.get())->set_intersection(bvh.release());
        }

        return rdr->run();
    }

    int ri_pathtrace_render_task::run() { return render(task_); }
}
