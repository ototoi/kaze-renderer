#ifndef KAZE_RI_BMP_RENDER_FRAME_H
#define KAZE_RI_BMP_RENDER_FRAME_H

#include "ri_base_render_frame.h"
#include <vector>

namespace kaze
{

    class ri_bmp_render_frame : public ri_base_render_frame
    {
    public:
        ri_bmp_render_frame(const char* name, int width, int height);
        ri_bmp_render_frame(const char* name, const char* type, const char* mode, int width, int height);

    public:
        void begin();
        void render(int x0, int y0, int x1, int y1, const image<real>&);
        void render(int x0, int y0, int x1, int y1, const image<color3>& img);
        void render(int x0, int y0, int x1, int y1, const image<color4>& img);
        void end();

    protected:
        void set(int x, int y, const color3& col);
        void set(int x, int y, const color4& col);

    protected:
        std::vector<unsigned char> buffer_;
        std::string path_;
        int width_;
        int height_;
    };
}
#endif