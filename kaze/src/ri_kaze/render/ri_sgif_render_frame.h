#ifndef KAZE_RI_SGIF_RENDER_FRAME_H
#define KAZE_RI_SGIF_RENDER_FRAME_H

#include "ri_base_render_frame.h"

namespace kaze
{

    class ri_sgif_render_frame : public ri_base_render_frame
    {
    public:
        ri_sgif_render_frame(const char* name, const char* type, const char* mode, int width, int height);

    public:
        void begin();
        void render(int x0, int y0, int x1, int y1, const image<real>&);
        void render(int x0, int y0, int x1, int y1, const image<color3>& img);
        void render(int x0, int y0, int x1, int y1, const image<color4>& img);
        void end();
    };
}

#endif
