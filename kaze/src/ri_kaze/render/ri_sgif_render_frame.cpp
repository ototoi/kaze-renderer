#include "ri_sgif_render_frame.h"

namespace kaze
{

    ri_sgif_render_frame::ri_sgif_render_frame(const char* name, const char* type, const char* mode, int width, int height)
    {
    }

    void ri_sgif_render_frame::begin() {}
    void ri_sgif_render_frame::render(int x0, int y0, int x1, int y1, const image<real>&) {}
    void ri_sgif_render_frame::render(int x0, int y0, int x1, int y1, const image<color3>& img) {}
    void ri_sgif_render_frame::render(int x0, int y0, int x1, int y1, const image<color4>& img) {}
    void ri_sgif_render_frame::end() {}
}
