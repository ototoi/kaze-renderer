#include "ri_file_render_frame.h"
#include "ri_tiff_render_frame.h"
#include "ri_bmp_render_frame.h"
#include "ri_png_render_frame.h"

#ifdef _WIN32
#include <windows.h>
#pragma warning(disable : 4996)
#endif

namespace kaze
{

    static std::string GetExt(const std::string& str)
    {
#ifdef _WIN32
        char ext[_MAX_EXT] = {};
        _splitpath(str.c_str(), NULL, NULL, NULL, ext);
        return ext;
#else
        std::string::size_type i = str.find_last_of('.');
        if (i != std::string::npos)
        {
            return str.substr(i);
        }
        else
        {
            return "";
        }
#endif
    }

    static std::shared_ptr<render_frame> CreateRenderFrame(const char* name, const char* type, const char* mode, int width, int height)
    {
        std::string strExt = GetExt(name);
        if (strExt == ".tif" || strExt == ".tiff")
        {
            return std::shared_ptr<render_frame>(new ri_tiff_render_frame(name, type, mode, width, height));
        }
        else if (strExt == ".bmp")
        {
            return std::shared_ptr<render_frame>(new ri_bmp_render_frame(name, type, mode, width, height));
        }
        else if (strExt == ".png")
        {
            return std::shared_ptr<render_frame>(new ri_png_render_frame(name, type, mode, width, height));
        }
        return std::shared_ptr<render_frame>(new ri_tiff_render_frame(name, type, mode, width, height));
    }

    ri_file_render_frame::ri_file_render_frame(const char* name, const char* type, const char* mode, int width, int height)
    {
        rf_ = CreateRenderFrame(name, type, mode, width, height);
    }

    void ri_file_render_frame::begin()
    {
        rf_->begin();
    }

    void ri_file_render_frame::render(int x0, int y0, int x1, int y1, const image<real>& img)
    {
        rf_->render(x0, y0, x1, y1, img);
    }

    void ri_file_render_frame::render(int x0, int y0, int x1, int y1, const image<color3>& img)
    {
        rf_->render(x0, y0, x1, y1, img);
    }

    void ri_file_render_frame::render(int x0, int y0, int x1, int y1, const image<color4>& img)
    {
        rf_->render(x0, y0, x1, y1, img);
    }

    void ri_file_render_frame::end()
    {
        rf_->end();
    }
}
