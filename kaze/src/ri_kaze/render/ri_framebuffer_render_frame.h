#ifndef KAZE_RI_FRAMEBUFFER_RENDER_FRAME_H
#define KAZE_RI_FRAMEBUFFER_RENDER_FRAME_H

#include "ri_base_render_frame.h"

namespace kaze
{

    class ri_framebuffer_render_frame : public ri_base_render_frame
    {
    public:
        ri_framebuffer_render_frame(const char* name, const char* type, const char* mode, int width, int height);

    public:
        void begin();
        void render(int x0, int y0, int x1, int y1, const image<real>&);
        void render(int x0, int y0, int x1, int y1, const image<color3>&);
        void render(int x0, int y0, int x1, int y1, const image<color4>&);
        void end();

    protected:
        std::shared_ptr<render_frame> rf_;
    };
}

#endif
