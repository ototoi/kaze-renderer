#include "ri_framebuffer_render_frame.h"
#include "rfp_render_frame.h"
#include "ri_path_resolver.h"
#include "logger.h"

#ifndef _WIN32
#include <errno.h>
#endif

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif
#include <thread>

namespace kaze
{
    static const char* szFrameBufferWindow = "kzrfview";

    static std::string MakeFullPath(const char* szFilePath)
    {
#ifdef _WIN32
        char szFullPath[_MAX_PATH] = "";
        _fullpath(szFullPath, szFilePath, _MAX_PATH);
        return szFullPath;
#else
        char szFullPath[512];
        realpath(szFilePath, szFullPath);
        return szFullPath;
#endif
    }

#ifdef _WIN32
    static int ExecuteProcess(const char* szCmd, const char* szArg)
    {
        PROCESS_INFORMATION pi;
        STARTUPINFOA si;

        ZeroMemory(&si, sizeof(si));
        si.cb = sizeof(si);

        si.cb = sizeof(STARTUPINFO);
        si.dwFlags = STARTF_USESHOWWINDOW;
        si.wShowWindow = SW_SHOWNORMAL;

#if 0
            int nWINDOW = CREATE_NEW_CONSOLE;
#else
        int nWINDOW = CREATE_NO_WINDOW;
#endif
        std::string strCmdLine;
        strCmdLine += szCmd;
        strCmdLine += " ";
        strCmdLine += szArg;

        std::vector<char> s_strCmdLine(strCmdLine.size() + 1);
        strcpy(&s_strCmdLine[0], strCmdLine.c_str());
        s_strCmdLine.back() = 0;

        BOOL bRet = ::CreateProcessA(NULL, (char*)(&s_strCmdLine[0]), NULL, NULL,
                                     FALSE, nWINDOW, NULL, NULL, &si, &pi);

        /*
        if (bRet)
        {
            DWORD nExitCode = 0;
            ::CloseHandle(pi.hThread);
            ::WaitForSingleObject(pi.hProcess, INFINITE);
            ::GetExitCodeProcess(pi.hProcess, &nExitCode);
            ::CloseHandle(pi.hProcess);
            return nExitCode;
        }
        */

        if (!bRet)
            return -1;

        return 0;
    }
#else
    static int ExecuteProcess(const char* szCmd, const char* szArg)
    {
        std::string cmdLine;
        cmdLine += szCmd;
        cmdLine += " ";
        cmdLine += szArg;
        cmdLine += "&";

        // printf("%s\n", cmdLine.c_str());
        int nRet = ::system(cmdLine.c_str());
        if (nRet == 127 || nRet == -1)
        {
            print_log("%s\n", strerror(errno));
            return -1;
        }
        return nRet;
    }
#endif

    static bool IsExistFile(const char* szFilePath)
    {
#ifdef _WIN32
        struct _stat s;
        int nRet = _stat(szFilePath, &s);
        return (nRet == 0);
#else
        struct stat s;
        int nRet = stat(szFilePath, &s);
        return (nRet == 0);
#endif
    }

    ri_framebuffer_render_frame::ri_framebuffer_render_frame(const char* name, const char* type, const char* mode, int width, int height)
    {
        std::string strWindow = ri::ri_path_resolver::get_bin_path() + "/" + szFrameBufferWindow;
#ifdef _WIN32
        strWindow += ".exe";
#endif
        strWindow = MakeFullPath(strWindow.c_str());
        if(IsExistFile(strWindow.c_str()))
        {
            #ifdef _WIN32
                ;;//shoud use named pipe
            #else
                //shoud use unix domain socket
                ExecuteProcess(strWindow.c_str(), "");
                std::chrono::milliseconds dura( 100 );
                std::this_thread::sleep_for( dura );
                rf_.reset(new rfp_render_frame(width, height));
            #endif
        }
        else
        {
            rf_.reset(new rfp_render_frame(width, height));
        }
    }

    void ri_framebuffer_render_frame::begin()
    {
        if(rf_.get())
            rf_->begin();
    }

    void ri_framebuffer_render_frame::render(int x0, int y0, int x1, int y1, const image<real>& img)
    {
        if(rf_.get())
            rf_->render(x0, y0, x1, y1, img);
    }

    void ri_framebuffer_render_frame::render(int x0, int y0, int x1, int y1, const image<color3>& img)
    {
        if(rf_.get())
            rf_->render(x0, y0, x1, y1, img);
    }

    void ri_framebuffer_render_frame::render(int x0, int y0, int x1, int y1, const image<color4>& img)
    {
        if(rf_.get())
            rf_->render(x0, y0, x1, y1, img);
    }

    void ri_framebuffer_render_frame::end()
    {
        if(rf_.get())
            rf_->end();
    }
}
