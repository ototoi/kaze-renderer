#ifndef KAZE_RI_ZFILE_RENDER_FRAME_H
#define KAZE_RI_ZFILE_RENDER_FRAME_H

#include "ri_base_render_frame.h"
#include <vector>

namespace kaze
{

    class ri_zfile_render_frame : public ri_base_render_frame
    {
    public:
        ri_zfile_render_frame(const char* name, const char* type, const char* mode, int width, int height, double w2c[16], double w2s[16]);

    public:
        void begin();
        void render(int x0, int y0, int x1, int y1, const image<real>&);
        void render(int x0, int y0, int x1, int y1, const image<color3>& img);
        void render(int x0, int y0, int x1, int y1, const image<color4>& img);
        void end();

    protected:
        std::string path_;
        std::string type_;
        std::string mode_;
        std::vector<double> buffer_;
        int width_;
        int height_;
        double w2c_[16];
        double w2s_[16];
    };
}

#endif