#include "ri_window_render_frame.h"
#include "ri_framebuffer_render_frame.h"

namespace kaze
{

    ri_window_render_frame::ri_window_render_frame(const char* name, const char* type, const char* mode, int width, int height)
    {
        rf_.reset(new ri_framebuffer_render_frame(name, type, mode, width, height));
    }

    void ri_window_render_frame::begin()
    {
        rf_->begin();
    }

    void ri_window_render_frame::render(int x0, int y0, int x1, int y1, const image<real>& img)
    {
        rf_->render(x0, y0, x1, y1, img);
    }

    void ri_window_render_frame::render(int x0, int y0, int x1, int y1, const image<color3>& img)
    {
        rf_->render(x0, y0, x1, y1, img);
    }

    void ri_window_render_frame::render(int x0, int y0, int x1, int y1, const image<color4>& img)
    {
        rf_->render(x0, y0, x1, y1, img);
    }

    void ri_window_render_frame::end()
    {
        rf_->end();
    }
}
