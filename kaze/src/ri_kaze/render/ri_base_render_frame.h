#ifndef KAZE_RI_BASE_RENDER_FRAME_H
#define KAZE_RI_BASE_RENDER_FRAME_H

#include "render_frame.h"
#include <string>

namespace kaze
{

    class ri_base_render_frame : public render_frame
    {
    public:
        static std::string get_full_path(const std::string& path);
    };
}
#endif