#include "ri_zfile_render_frame.h"
#include "zfile.h"



namespace kaze
{

    ri_zfile_render_frame::ri_zfile_render_frame(const char* name, const char* type, const char* mode, int width, int height, double w2c[16], double w2s[16])
    {
        buffer_.resize(width * height);
        memset(&buffer_[0], 0, sizeof(double) * width * height);
        path_ = get_full_path(name);
        type_ = type;
        mode_ = mode;
        width_ = width;
        height_ = height;
        memcpy(w2c_, w2c, sizeof(double) * 16);
        memcpy(w2s_, w2s, sizeof(double) * 16);
    }

    void ri_zfile_render_frame::begin()
    {
        ;
    }

    void ri_zfile_render_frame::render(int x0, int y0, int x1, int y1, const image<real>& img)
    {
        for (int y = y0; y < y1; y++)
        {
            for (int x = x0; x < x1; x++)
            {
                real z = img.get(x, y);
                buffer_[y * width_ + x] = (double)z;
            }
        }
    }

    void ri_zfile_render_frame::render(int x0, int y0, int x1, int y1, const image<color3>& img)
    {
        ;
    }
    void ri_zfile_render_frame::render(int x0, int y0, int x1, int y1, const image<color4>& img)
    {
        ;
    }

    static unsigned char Convert(double x)
    {
        x *= 255;
        if (x < 0) return 0;
        if (x > 255) return 255;
        return (unsigned char)(int)floor(x);
    }

    void ri_zfile_render_frame::end()
    {
        int nRet = write_zfile_image(path_.c_str(), width_, height_, &buffer_[0], w2c_, w2s_);
        assert(nRet == 0);
    }
}