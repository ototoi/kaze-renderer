#ifndef KAZE_RI_TIFF_RENDER_FRAME_H
#define KAZE_RI_TIFF_RENDER_FRAME_H

#include "ri_base_render_frame.h"
#include <vector>
#include <string>

namespace kaze
{

    class ri_tiff_render_frame : public ri_base_render_frame
    {
    public:
        ri_tiff_render_frame(const char* name, const char* type, const char* mode, int width, int height);

    public:
        void begin();
        void render(int x0, int y0, int x1, int y1, const image<real>&);
        void render(int x0, int y0, int x1, int y1, const image<color3>& img);
        void render(int x0, int y0, int x1, int y1, const image<color4>& img);
        void end();

    protected:
        void set(int x, int y, const color3& col);
        void set(int x, int y, const color4& col);

    protected:
        std::vector<unsigned char> buffer_;
        std::string path_;
        std::string type_;
        std::string mode_;
        int width_;
        int height_;
    };
}

#endif