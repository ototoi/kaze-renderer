#include "ri_base_render_frame.h"

namespace kaze
{

    static std::string MakeFullPath(const char* szFilePath)
    {
#ifdef _WIN32
        char szFullPath[_MAX_PATH] = "";
        _fullpath(szFullPath, szFilePath, _MAX_PATH);
        return szFullPath;
#else
        char szFullPath[512];
        realpath(szFilePath, szFullPath);
        return szFullPath;
#endif
    }

    std::string ri_base_render_frame::get_full_path(const std::string& path)
    {
        return MakeFullPath(path.c_str());
    }
}