#include "ri_bmp_render_frame.h"
#include "image/bmp_io.h"
#include "color_convert.h"

namespace kaze
{

    ri_bmp_render_frame::ri_bmp_render_frame(const char* name, int width, int height)
    {
        buffer_.resize(width * height * 3);
        memset(&buffer_[0], 0, sizeof(unsigned char) * width * height * 3);
        path_ = get_full_path(name);
        width_ = width;
        height_ = height;
    }

    ri_bmp_render_frame::ri_bmp_render_frame(const char* name, const char* type, const char* mode, int width, int height)
    {
        buffer_.resize(width * height * 3);
        memset(&buffer_[0], 0, sizeof(unsigned char) * width * height * 3);
        path_ = name;
        width_ = width;
        height_ = height;
    }

    void ri_bmp_render_frame::begin() {}
    void ri_bmp_render_frame::render(int x0, int y0, int x1, int y1, const image<real>&) {}
    void ri_bmp_render_frame::render(int x0, int y0, int x1, int y1, const image<color3>& img)
    {
        int w = x1 - x0;
        int h = y1 - y0;
        for (int y = y0; y < y1; y++)
        {
            for (int x = x0; x < x1; x++)
            {
                color3 col = img.get(x, y);
                this->set(x, y, col);
            }
        }
    }

    void ri_bmp_render_frame::render(int x0, int y0, int x1, int y1, const image<color4>& img)
    {
        int w = x1 - x0;
        int h = y1 - y0;
        for (int y = y0; y < y1; y++)
        {
            for (int x = x0; x < x1; x++)
            {
                color4 col = img.get(x, y);
                this->set(x, y, col);
            }
        }
    }

    void ri_bmp_render_frame::end()
    {
        bool bRet = WriteBMPRGBAImage(path_.c_str(), &buffer_[0], width_, height_);
    }

    void ri_bmp_render_frame::set(int x, int y, const color3& col)
    {
        typedef unsigned char byte;

        int i = y * width_ + x;

        unsigned int ui = color3toRGBA(col);

        byte r = byte(0xff & (ui >> 24));
        byte g = byte(0xff & (ui >> 16));
        byte b = byte(0xff & (ui >> 8));

        buffer_[3 * i + 0] = r;
        buffer_[3 * i + 1] = g;
        buffer_[3 * i + 2] = b;
    }

    void ri_bmp_render_frame::set(int x, int y, const color4& col)
    {
        typedef unsigned char byte;

        int i = y * width_ + x;

        real a = col[3];
        vector3 out = vector3(col[0], col[1], col[2]) * a;

        unsigned int ui = color3toRGBA(out);

        byte r = byte(0xff & (ui >> 24));
        byte g = byte(0xff & (ui >> 16));
        byte b = byte(0xff & (ui >> 8));

        buffer_[3 * i + 0] = r;
        buffer_[3 * i + 1] = g;
        buffer_[3 * i + 2] = b;
    }
}