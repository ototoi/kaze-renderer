#include "ri_shader_converter.h"
#include "ri_geometry.h"
#include "ri_attributes.h"
#include "ri_path_resolver.h"
#include "ri_compile_shader_task.h"

#include "ri_texture.h"
#include "ri_image_texture.h"
#include "ri_surface_shader.h"

#include "null_shader_evaluator.h"
#include "constant_shader_evaluator.h"
#include "plastic_shader_evaluator.h"
#include "matte_shader_evaluator.h"
#include "metal_shader_evaluator.h"
#include "shinymetal_shader_evaluator.h"
#include "Marschner_shader_evaluator.h"
#include "AFMarschner_shader_evaluator.h"
#include "ksl_shader_evaluator.h"
#include "MQMaterial_shader_evaluator.h"
#include "mesured_MERL_shader_evaluator.h"

#include "ksl_shader_evaluator.h"

#include "ri_coordinate_transformer.h"

#include "proxy_tracer.h"

#ifdef _WIN32
#include <sys/types.h>
#include <sys/stat.h>
#else
#include <sys/stat.h>
#endif

#include <memory>

namespace ri
{
    using namespace rsle;
    using namespace kaze;
    
    namespace
    {
        enum
        {
            RI_SURFACESHADER_UNKNOWN = 3000,
            RI_SURFACESHADER_CONSTANT,
            RI_SURFACESHADER_MATTE,
            RI_SURFACESHADER_METAL,
            RI_SURFACESHADER_SHINYMETAL,
            RI_SURFACESHADER_PLASTIC,

            RI_SURFACESHADER_MARSCHNER,
            RI_SURFACESHADER_AFMARSCHNER,
            RI_SURFACESHADER_MQMATERIAL
        };

        struct T2I
        {
            const char* name;
            int num;
        };

        static T2I PREDEFINED_SHADERS[] =
            {
                {"constant", RI_SURFACESHADER_CONSTANT},
                {"matte", RI_SURFACESHADER_MATTE},
                {"diffsurf", RI_SURFACESHADER_MATTE},
                {"metal", RI_SURFACESHADER_METAL},
                {"shinymetal", RI_SURFACESHADER_SHINYMETAL},
                {"specsurf", RI_SURFACESHADER_SHINYMETAL},
                {"VPlastic", RI_SURFACESHADER_PLASTIC},
                {"plastic", RI_SURFACESHADER_PLASTIC},

                {"Marschner", RI_SURFACESHADER_MARSCHNER},
                {"AFMarschner", RI_SURFACESHADER_AFMARSCHNER},
                {"MQMaterial", RI_SURFACESHADER_MQMATERIAL},
                {NULL, -1},
        };

        static int GetPreDefinedShader(const char* name)
        {
            int i = 0;
            while (PREDEFINED_SHADERS[i].name)
            {
                if (strcmp(PREDEFINED_SHADERS[i].name, name) == 0) return PREDEFINED_SHADERS[i].num;
                i++;
            }
            return -1;
        }
    }

    static matrix4 ConvertMatrix(const ri_transform& trns)
    {
        ri_transform s(trns);
        s.transpose();
        const float* f = s.get();
        matrix4 tmp(f);
        return tmp;
    }

    static void GetValue(std::vector<int>& iValue, const ri_shader* pShader, const char* key)
    {
        const ri_parameters::value_type* pv = pShader->get_parameter(key);
        if (pv)
        {
            if (pv->nType == ri_parameters::TYPE_INTEGER)
            {
                iValue = pv->integer_values;
            }
            else
            {
                iValue.clear();
            }
        }
        else
        {
            iValue.clear();
        }
    }

    static void GetValue(std::vector<float>& fValue, const ri_shader* pShader, const char* key)
    {
        const ri_parameters::value_type* pv = pShader->get_parameter(key);
        if (pv)
        {
            if (pv->nType == ri_parameters::TYPE_FLOAT)
            {
                fValue = pv->float_values;
            }
            else
            {
                fValue.clear();
            }
        }
        else
        {
            fValue.clear();
        }
    }

    static void GetValue(std::vector<std::string>& sValue, const ri_shader* pShader, const char* key)
    {
        const ri_parameters::value_type* pv = pShader->get_parameter(key);
        if (pv)
        {
            if (pv->nType == ri_parameters::TYPE_STRING)
            {
                sValue = pv->string_values;
            }
            else
            {
                sValue.clear();
            }
        }
        else
        {
            sValue.clear();
        }
    }

    static bool IsFloat1(const ri_classifier::param_data& data)
    {
        int nType = data.get_type();
        switch (nType)
        {
        case ri_classifier::FLOAT:
        {
            if (data.get_size() == 1)
                return true;
            else
                return false;
        }
        }
        return false;
    }

    static bool IsFloat2(const ri_classifier::param_data& data)
    {
        int nType = data.get_type();
        switch (nType)
        {
        case ri_classifier::FLOAT:
        {
            if (data.get_size() == 2)
                return true;
            else
                return false;
        }
        }
        return false;
    }

    static bool IsFloat3(const ri_classifier::param_data& data)
    {
        int nType = data.get_type();
        switch (nType)
        {
        case ri_classifier::FLOAT:
        {
            if (data.get_size() == 3)
                return true;
            else
                return false;
        }
        case ri_classifier::COLOR:
            return true;
        case ri_classifier::VECTOR:
            return true;
        case ri_classifier::NORMAL:
            return true;
        case ri_classifier::POINT:
            return true;
        }
        return false;
    }

    static bool IsString(const ri_classifier::param_data& data)
    {
        int nType = data.get_type();
        return ri_classifier::STRING == nType;
    }

    static bool IsUniform(const ri_classifier::param_data& data)
    {
        if (data.get_class() == ri_classifier::UNIFORM)
            return true;
        else
            return false;
    }

    static bool IsVarying(const ri_classifier::param_data& data)
    {
        if (data.get_class() == ri_classifier::VARYING)
            return true;
        else
            return false;
    }

    static bool IsVertex(const ri_classifier::param_data& data)
    {
        if (data.get_class() == ri_classifier::VERTEX)
            return true;
        else
            return false;
    }

    static bool IsFaceVarying(const ri_classifier::param_data& data)
    {
        if (data.get_class() == ri_classifier::FACEVARYING)
            return true;
        else
            return false;
    }

    static const int* GetInteger(const ri_geometry* pGeo, const char* key)
    {
        const ri_parameters::value_type* p_param = pGeo->get_parameter(key);
        if (p_param && p_param->nType == ri_parameters::TYPE_INTEGER)
        {
            return &(p_param->integer_values[0]);
        }
        return NULL;
    }

    static const float* GetFloat(const ri_geometry* pGeo, const char* key)
    {
        const ri_parameters::value_type* p_param = pGeo->get_parameter(key);
        if (p_param && p_param->nType == ri_parameters::TYPE_FLOAT)
        {
            return &(p_param->float_values[0]);
        }
        return NULL;
    }

    static bool IsKey(const char* a, const char* b)
    {
        if (strcmp(a, b) == 0) return true;
        return false;
    }

    static bool IsKey(const char* const tbl[], const char* key)
    {
        int i = 0;
        while (tbl[i])
        {
            if (strcmp(key, tbl[i]) == 0) return true;
            i++;
        }
        return false;
    }

    static std::shared_ptr<shader_evaluator> ConvertConstantShader(const ri_shader* pShader)
    {
        return std::shared_ptr<shader_evaluator>(new constant_shader_evaluator());
    }

    static std::shared_ptr<shader_evaluator> ConvertPlasticShader(const ri_shader* pShader)
    {
        return std::shared_ptr<shader_evaluator>(new plastic_shader_evaluator());
    }

    static std::shared_ptr<shader_evaluator> ConvertMatteShader(const ri_shader* pShader)
    {
        return std::shared_ptr<shader_evaluator>(new matte_shader_evaluator());
    }

    static std::shared_ptr<shader_evaluator> ConvertMetalShader(const ri_shader* pShader)
    {
        return std::shared_ptr<shader_evaluator>(new metal_shader_evaluator());
    }

    static std::shared_ptr<shader_evaluator> ConvertShinyMetalShader(const ri_shader* pShader)
    {
        return std::shared_ptr<shader_evaluator>(new shinymetal_shader_evaluator());
    }

    static std::shared_ptr<shader_evaluator> ConvertMarschnerShader(const ri_shader* pShader)
    {
        return std::shared_ptr<shader_evaluator>(new Marschner_shader_evaluator());
    }

    static std::shared_ptr<shader_evaluator> ConvertAFMarschnerShader(const ri_shader* pShader)
    {
        std::unique_ptr<AFMarschner_shader_evaluator> sp(new AFMarschner_shader_evaluator());
        const ri_classifier& cls = pShader->get_classifier();

        for (int i = 0; i < cls.size(); i++)
        {
            std::vector<float> fValue;
            const ri_classifier::param_data& data = cls[i];
            if (IsFloat1(data))
            {
                GetValue(fValue, pShader, data.get_name());
                sp->set(data.get_name(), fValue[0]);
            }
            else if (IsFloat3(data))
            {
                GetValue(fValue, pShader, data.get_name());
                sp->set(data.get_name(), vector3(&fValue[0]));
            }
        }

        return std::shared_ptr<shader_evaluator>(sp.release());
    }

    static std::shared_ptr<shader_evaluator> ConvertMQMaterialShader(const ri_shader* pShader)
    {
        return std::shared_ptr<shader_evaluator>(new MQMaterial_shader_evaluator());
    }

    static bool IsExistFile(const char* szFilePath)
    {
#ifdef _WIN32
        struct _stat s;
        int nRet = _stat(szFilePath, &s);
        return (nRet == 0);
#else
        struct stat s;
        int nRet = stat(szFilePath, &s);
        return (nRet == 0);
#endif
    }

    static std::string GetKSLShader(const ri_shader* pShader)
    {
        std::string name = pShader->get_name();
        ri_path_resolver pr(pShader->get_options());
        std::vector<std::string> shader_paths = pr.get_shaders_paths();

        for (size_t i = 0; i < shader_paths.size(); i++)
        {
            std::string strSLPath = shader_paths[i] + name + ".sl";
            std::string strKSLPath = shader_paths[i] + name + ".ksl";
            if (IsExistFile(strSLPath.c_str()))
            {
                ri_compile_shader_task tsk(strSLPath.c_str(), strKSLPath.c_str());
                tsk.run();
            }

            if (IsExistFile(strKSLPath.c_str()))
            {
                return strKSLPath;
            }
        }

        return "";
    }

    bool CheckMERLExt(const std::string& name)
    {
        if (std::string::npos != name.rfind(".merl")) return true;
        if (std::string::npos != name.rfind(".binary")) return true;
        return false;
    }

    static std::string GetMERLShader(const ri_shader* pShader)
    {
        std::string name = pShader->get_name();
        ri_path_resolver pr(pShader->get_options());

        std::vector<std::string> shader_paths = pr.get_shaders_paths();
        std::vector<std::string> paths;
        for (size_t i = 0; i < shader_paths.size(); i++)
        {
            if (CheckMERLExt(name))
            {
                paths.push_back(shader_paths[i] + name);
            }
            else
            {
                paths.push_back(shader_paths[i] + name + ".merl");
                paths.push_back(shader_paths[i] + name + ".binary");
            }
        }

        for (int i = 0; i < paths.size(); i++)
        {
            if (IsExistFile(paths[i].c_str()))
            {
                return paths[i];
            }
        }
        return "";
    }

    static std::shared_ptr<shader_evaluator> ConvertShaderEvaluator(const ri_shader* pShader)
    {
        std::string strShaderPath = "";
        if ((strShaderPath = GetKSLShader(pShader)) != "")
        {
            return std::shared_ptr<shader_evaluator>(new ksl_shader_evaluator(strShaderPath.c_str()));
        }
        else if ((strShaderPath = GetMERLShader(pShader)) != "")
        {
            return std::shared_ptr<shader_evaluator>(new mesured_MERL_shader_evaluator(strShaderPath.c_str()));
        }
        else
        {
            int nShader = GetPreDefinedShader(pShader->get_name().c_str());
            if (nShader >= 0)
            {
                switch (nShader)
                {
                case RI_SURFACESHADER_CONSTANT:
                    return ConvertConstantShader(pShader);
                case RI_SURFACESHADER_MATTE:
                    return ConvertMatteShader(pShader);
                case RI_SURFACESHADER_METAL:
                    return ConvertMetalShader(pShader);
                case RI_SURFACESHADER_SHINYMETAL:
                    return ConvertShinyMetalShader(pShader);
                case RI_SURFACESHADER_PLASTIC:
                    return ConvertPlasticShader(pShader);
                case RI_SURFACESHADER_MARSCHNER:
                    return ConvertMarschnerShader(pShader);
                case RI_SURFACESHADER_AFMARSCHNER:
                    return ConvertAFMarschnerShader(pShader);
                case RI_SURFACESHADER_MQMATERIAL:
                    return ConvertMQMaterialShader(pShader);
                }
            }
        }

        return std::shared_ptr<shader_evaluator>();
    }

    /*
        INIT_PARAM("TextureCoordinates", "s1", texUnit[0]);
        INIT_PARAM("TextureCoordinates", "t1", texUnit[1]);
        INIT_PARAM("TextureCoordinates", "s2", texUnit[2]);
        INIT_PARAM("TextureCoordinates", "t2", texUnit[3]);
        INIT_PARAM("TextureCoordinates", "s3", texUnit[4]);
        INIT_PARAM("TextureCoordinates", "t3", texUnit[5]);
        INIT_PARAM("TextureCoordinates", "s4", texUnit[6]);
        INIT_PARAM("TextureCoordinates", "t4", texUnit[7]);
    */

    static bool GetTextureCoordinates(float tc[8], const ri_attributes* pAttr)
    {
        static float texUnit[] = {0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f};
        static const char* st[] = {"s1", "t1", "s2", "t2", "s3", "t3", "s4", "t4"};
        for (int i = 0; i < 8; i++)
        {
            if (!pAttr->get("TextureCoordinates", st[i], tc[i])) return false;
        }
        if (memcmp(texUnit, tc, sizeof(float) * 8) == 0) return false;

        return true;
    }

    static void SetAttributes(ri_surface_shader* shd, const ri_attributes* pAttr, const ri_points_triangle_polygons_geometry* pGeo)
    {
        static const char* IGNORE[] = {
            "P",
            "N",
            "Np",
            "s",
            "t",
            "st",
            NULL};
        int nface = pGeo->get_nfaces();
        const int* verts = pGeo->get_verts();
        int nverts = pGeo->get_nverts();

        const ri_classifier& cls = pGeo->get_classifier();

        float tc[8];
        bool bModifyTC = GetTextureCoordinates(tc, pAttr);

        std::shared_ptr<texture<real> > texs;
        std::shared_ptr<texture<real> > text;
        std::shared_ptr<texture<vector2> > texst;

        for (int i = 0; i < cls.size(); i++)
        {
            const ri_classifier::param_data& data = cls[i];
            if (IsKey("s", data.get_name()))
            {
                if (IsUniform(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        texs.reset(new ri_points_triangle_polygons_uniform_float1_texture(nface, v));
                    }
                }
                else if (IsVarying(data) || IsVertex(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        texs.reset(new ri_points_triangle_polygons_vertex_float1_texture(nface, verts, nverts, v));
                    }
                }
                else if (IsFaceVarying(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        texs.reset(new ri_points_triangle_polygons_facevarying_float1_texture(nface, v));
                    }
                }
            }
            else if (IsKey("t", data.get_name()))
            {
                if (IsUniform(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        text.reset(new ri_points_triangle_polygons_uniform_float1_texture(nface, v));
                    }
                }
                else if (IsVarying(data) || IsVertex(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        text.reset(new ri_points_triangle_polygons_vertex_float1_texture(nface, verts, nverts, v));
                    }
                }
                else if (IsFaceVarying(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        text.reset(new ri_points_triangle_polygons_facevarying_float1_texture(nface, v));
                    }
                }
            }
            else if (IsKey("st", data.get_name()))
            {
                if (IsUniform(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        texst.reset(new ri_points_triangle_polygons_uniform_float2_texture(nface, v));
                    }
                }
                else if (IsVarying(data) || IsVertex(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        texst.reset(new ri_points_triangle_polygons_vertex_float2_texture(nface, verts, nverts, v));
                    }
                }
                else if (IsFaceVarying(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        texst.reset(new ri_points_triangle_polygons_facevarying_float2_texture(nface, v));
                    }
                }
            }
        }

        if (!texst.get() && (texs.get() && text.get()))
        {
            texst.reset(new ri_composite_float2_texture(texs, text));
        }

        if (texst.get())
        {
            if (bModifyTC)
            {
                shd->add_texture("st", new ri_modify_st_texture(tc, texst));
            }
            else
            {
                shd->add_texture("st", texst);
            }
        }
        else if (bModifyTC)
        {
            shd->add_texture("st", new ri_modify_st_texture(tc, new ri_param_col2_texture()));
        }

        for (size_t i = 0; i < cls.size(); i++)
        {
            const ri_classifier::param_data& data = cls[i];
            if (IsKey(IGNORE, data.get_name())) continue;
            if (IsFloat1(data))
            {
                if (IsUniform(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_points_triangle_polygons_uniform_float1_texture(nface, v));
                    }
                }
                else if (IsVarying(data) || IsVertex(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_points_triangle_polygons_vertex_float1_texture(nface, verts, nverts, v));
                    }
                }
                else if (IsFaceVarying(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_points_triangle_polygons_facevarying_float1_texture(nface, v));
                    }
                }
            }
            else if (IsFloat2(data))
            {
                if (IsUniform(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_points_triangle_polygons_uniform_float2_texture(nface, v));
                    }
                }
                else if (IsVarying(data) || IsVertex(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_points_triangle_polygons_vertex_float2_texture(nface, verts, nverts, v));
                    }
                }
                else if (IsFaceVarying(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_points_triangle_polygons_facevarying_float2_texture(nface, v));
                    }
                }
            }
            else if (IsFloat3(data))
            {
                if (IsUniform(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_points_triangle_polygons_uniform_float3_texture(nface, v));
                    }
                }
                else if (IsVarying(data) || IsVertex(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_points_triangle_polygons_vertex_float3_texture(nface, verts, nverts, v));
                    }
                }
                else if (IsFaceVarying(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_points_triangle_polygons_facevarying_float3_texture(nface, v));
                    }
                }
            }
        }
    }

    static void SetAttributes(ri_surface_shader* shd, const ri_attributes* pAttr, const ri_bezier_patch_mesh_geometry* pGeo)
    {
        static const char* IGNORE[] = {
            "P",
            "Pw",
            "Pz",
            "s",
            "t",
            "st",
            NULL};

        int nu = pGeo->get_nu();
        int nv = pGeo->get_nv();
        int uorder = pGeo->get_uorder();
        int vorder = pGeo->get_vorder();

        const ri_classifier& cls = pGeo->get_classifier();

        float tc[8];
        bool bModifyTC = GetTextureCoordinates(tc, pAttr);

        std::shared_ptr<texture<real> > texs;
        std::shared_ptr<texture<real> > text;
        std::shared_ptr<texture<vector2> > texst;
        for (size_t i = 0; i < cls.size(); i++)
        {
            const ri_classifier::param_data& data = cls[i];
            if (IsKey("st", data.get_name()))
            {
                const float* v = GetFloat(pGeo, data.get_name());
                if (IsUniform(data))
                {
                    if (v)
                    {
                        texst.reset(new ri_bezier_patch_mesh_uniform_float2_texture(nu, uorder, nv, vorder, v));
                    }
                }
                else if (IsVarying(data))
                {
                    if (v)
                    {
                        texst.reset(new ri_bezier_patch_mesh_varying_float2_texture(nu, uorder, nv, vorder, v));
                    }
                }
                else if (IsVertex(data))
                {
                    if (v)
                    {
                        texst.reset(new ri_bezier_patch_mesh_vertex_float2_texture(nu, uorder, nv, vorder, v));
                    }
                }
            }
        }

        if (texst.get())
        {
            if (bModifyTC)
            {
                shd->add_texture("st", new ri_modify_st_texture(tc, texst));
            }
            else
            {
                shd->add_texture("st", texst);
            }
        }
        else if (bModifyTC)
        {
            shd->add_texture("st", new ri_modify_st_texture(tc, new ri_param_col2_texture()));
        }

        for (size_t i = 0; i < cls.size(); i++)
        {
            const ri_classifier::param_data& data = cls[i];
            if (IsKey(IGNORE, data.get_name())) continue;
            if (IsFloat1(data))
            {
                const float* v = GetFloat(pGeo, data.get_name());
                if (IsUniform(data))
                {
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_bezier_patch_mesh_uniform_float1_texture(nu, uorder, nv, vorder, v));
                    }
                }
                else if (IsVarying(data))
                {
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_bezier_patch_mesh_varying_float1_texture(nu, uorder, nv, vorder, v));
                    }
                }
                else if (IsVertex(data))
                {
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_bezier_patch_mesh_vertex_float1_texture(nu, uorder, nv, vorder, v));
                    }
                }
            }
            else if (IsFloat2(data))
            {
                const float* v = GetFloat(pGeo, data.get_name());
                if (IsUniform(data))
                {
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_bezier_patch_mesh_uniform_float2_texture(nu, uorder, nv, vorder, v));
                    }
                }
                else if (IsVarying(data))
                {
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_bezier_patch_mesh_varying_float2_texture(nu, uorder, nv, vorder, v));
                    }
                }
                else if (IsVertex(data))
                {
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_bezier_patch_mesh_vertex_float2_texture(nu, uorder, nv, vorder, v));
                    }
                }
            }
            else if (IsFloat3(data))
            {
                const float* v = GetFloat(pGeo, data.get_name());
                if (IsUniform(data))
                {
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_bezier_patch_mesh_uniform_float3_texture(nu, uorder, nv, vorder, v));
                    }
                }
                else if (IsVarying(data))
                {
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_bezier_patch_mesh_varying_float3_texture(nu, uorder, nv, vorder, v));
                    }
                }
                else if (IsVertex(data))
                {
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_bezier_patch_mesh_vertex_float3_texture(nu, uorder, nv, vorder, v));
                    }
                }
            }
        }
    }

    static void SetAttributes(ri_surface_shader* shd, const ri_attributes* pAttr, const ri_bezier_curves_geometry* pGeo)
    {
        static const char* IGNORE[] = {
            "Cs",
            "Os",
            "P",
            "Pw",
            "N", //for user defined normal
            "width",
            "constantwidth",
            NULL};

        const float* Cs = pGeo->get_Cs();
        const float* Os = pGeo->get_Os();

        const float* Csv = pGeo->get_Csv();
        const float* Osv = pGeo->get_Osv();

        //const float* P = pGeo->get_P();

        int ncurves = pGeo->get_ncurves();
        const int* nvertices = pGeo->get_nvertices();

        if (Cs || Csv)
        {
            shd->add_texture("Cs", new ri_bezier_curves_Cs_texture());
        }

        if (Os || Osv)
        {
            shd->add_texture("Os", new ri_bezier_curves_Os_texture());
        }
    }

    static void SetAttributes(ri_surface_shader* shd, const ri_attributes* pAttr, const ri_points_geometry* pGeo)
    {
        static const char* IGNORE[] = {
            "Cs",
            "Os",
            "P",
            "N", //for user defined normal
            "width",
            "constantwidth",
            NULL};

        const float* Cs = pGeo->get_Cs();
        const float* Os = pGeo->get_Os();

        //const float* P = pGeo->get_P();

        if (Cs)
        {
            shd->add_texture("Cs", new ri_points_Cs_texture());
        }

        if (Os)
        {
            shd->add_texture("Os", new ri_points_Os_texture());
        }

        //
    }

    static void SetAttributesQuadrics(ri_surface_shader* shd, const ri_attributes* pAttr, const ri_geometry* pGeo)
    {
        static const char* IGNORE[] = {
            "P",
            "s",
            "t",
            "st",
            NULL};

        const ri_classifier& cls = pGeo->get_classifier();

        float tc[8];
        bool bModifyTC = GetTextureCoordinates(tc, pAttr);

        //std::shared_ptr< texture<real> > texs;
        //std::shared_ptr< texture<real> > text;
        std::shared_ptr<texture<vector2> > texst;
        for (size_t i = 0; i < cls.size(); i++)
        {
            const ri_classifier::param_data& data = cls[i];
            if (IsKey("st", data.get_name()))
            {
                if (IsUniform(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        texst.reset(new ri_quadric_uniform_float2_texture(v));
                    }
                }
                else if (IsVarying(data) || IsVertex(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        texst.reset(new ri_quadric_varying_float2_texture(v));
                    }
                }
            }
        }

        if (texst.get())
        {
            if (bModifyTC)
            {
                shd->add_texture("st", new ri_modify_st_texture(tc, texst));
            }
            else
            {
                shd->add_texture("st", texst);
            }
        }
        else if (bModifyTC)
        {
            shd->add_texture("st", new ri_modify_st_texture(tc, new ri_param_col2_texture()));
        }

        for (size_t i = 0; i < cls.size(); i++)
        {
            const ri_classifier::param_data& data = cls[i];
            if (IsKey(IGNORE, data.get_name())) continue;
            if (IsFloat1(data))
            {
                if (IsUniform(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_quadric_uniform_float1_texture(v));
                    }
                }
                else if (IsVarying(data) || IsVertex(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_quadric_varying_float1_texture(v));
                    }
                }
            }
            else if (IsFloat2(data))
            {
                if (IsUniform(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_quadric_uniform_float2_texture(v));
                    }
                }
                else if (IsVarying(data) || IsVertex(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_quadric_varying_float2_texture(v));
                    }
                }
            }
            else if (IsFloat3(data))
            {
                if (IsUniform(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_quadric_uniform_float3_texture(v));
                    }
                }
                else if (IsVarying(data) || IsVertex(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_quadric_varying_float3_texture(v));
                    }
                }
            }
        }
    }

    static void SetAttributes(ri_surface_shader* shd, const ri_attributes* pAttr, const ri_blobby_mesh_geometry* pGeo)
    {
        static const char* IGNORE[] = {
            "P",
            "N",
            "Np",
            "s",
            "t",
            "st",
            NULL};

        int nface = pGeo->get_nfaces();
        const int* verts = pGeo->get_verts();
        int nverts = pGeo->get_nverts();

        const ri_classifier& cls = pGeo->get_classifier();

        for (size_t i = 0; i < cls.size(); i++)
        {
            const ri_classifier::param_data& data = cls[i];
            if (IsKey(IGNORE, data.get_name())) continue;
            if (IsFloat1(data))
            {
                if (IsVarying(data) || IsVertex(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_points_triangle_polygons_vertex_float1_texture(nface, verts, nverts, v));
                    }
                }
            }
            else if (IsFloat2(data))
            {
                if (IsVarying(data) || IsVertex(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_points_triangle_polygons_vertex_float2_texture(nface, verts, nverts, v));
                    }
                }
            }
            else if (IsFloat3(data))
            {
                if (IsVarying(data) || IsVertex(data))
                {
                    const float* v = GetFloat(pGeo, data.get_name());
                    if (v)
                    {
                        shd->add_texture(data.get_name(), new ri_points_triangle_polygons_vertex_float3_texture(nface, verts, nverts, v));
                    }
                }
            }
        }
    }

    static const std::string* GetString(const ri_shader* pShader, const char* key)
    {
        const ri_parameters::value_type* p_param = pShader->get_parameter(key);
        if (p_param && p_param->nType == ri_parameters::TYPE_STRING)
        {
            return &(p_param->string_values[0]);
        }
        return NULL;
    }

    static const float* GetFloat(const ri_shader* pShader, const char* key)
    {
        const ri_parameters::value_type* p_param = pShader->get_parameter(key);
        if (p_param && p_param->nType == ri_parameters::TYPE_FLOAT)
        {
            return &(p_param->float_values[0]);
        }
        return NULL;
    }

    static bool CheckTexExt(const std::string& name)
    {
        if (std::string::npos != name.rfind(".tex")) return true;
        return false;
    }

    static std::string GetTexturePath(const ri_shader* pShader, const char* szName)
    {
        std::string name = szName;
        ri_path_resolver pr(pShader->get_options());
        std::vector<std::string> texture_paths = pr.get_textures_paths();
        std::vector<std::string> paths;
        for (size_t i = 0; i < texture_paths.size(); i++)
        {
            paths.push_back(texture_paths[i] + name);
        }

        for (int i = 0; i < paths.size(); i++)
        {
            std::string mrky = paths[i] + ".mrky";
            if (IsExistFile(paths[i].c_str()) || IsExistFile(mrky.c_str()))
            {
                return paths[i];
            }
        }
        return "";
    }

    class f1_texture : public texture<real>
    {
    public:
        f1_texture(const auto_count_ptr<texture<color3> >& img)
            : img_(img)
        {
        }
        real get(const sufflight& suf) const
        {
            return (real)(img_->get(suf)[0]);
        }

    protected:
        std::shared_ptr<texture<color3> > img_;
    };

    void SetShaderParams(ri_shader_converter& cvtr, ri_surface_shader* shd, const ri_shader* pShader)
    {
        static const char* IGNORE[] = {
            NULL};

        const ri_classifier& cls = pShader->get_classifier();
        for (size_t i = 0; i < cls.size(); i++)
        {
            const ri_classifier::param_data& data = cls[i];
            if (IsKey(IGNORE, data.get_name())) continue;
            if (IsFloat1(data))
            {
                const float* v = GetFloat(pShader, data.get_name());
                if (v)
                {
                    shd->add_texture(data.get_name(), new ri_uniform_float1_texture(*v));
                }
            }
            else if (IsFloat2(data))
            {
                const float* v = GetFloat(pShader, data.get_name());
                if (v)
                {
                    shd->add_texture(data.get_name(), new ri_uniform_float2_texture(v));
                }
            }
            else if (IsFloat3(data))
            {
                const float* v = GetFloat(pShader, data.get_name());
                if (v)
                {
                    shd->add_texture(data.get_name(), new ri_uniform_float3_texture(v));
                }
            }
            else if (IsString(data))
            {
                const std::string* v = GetString(pShader, data.get_name());
                if (v)
                {
                    std::string s = *v;
                    shd->add_string(data.get_name(), s.c_str());
                    {
                        std::string texpath = GetTexturePath(pShader, s.c_str());
                        if (texpath != "")
                        {
                            std::shared_ptr<texture<color3> > img = cvtr.get_col3_cache(texpath);
                            if (!img.get())
                            {
                                img.reset(new ri_float3_image_texture(texpath.c_str()));
                                cvtr.set_col3_cache(texpath, img);
                            }
                            shd->add_texture(s.c_str(), img);
                            shd->add_texture(s.c_str(), new f1_texture(img));
                        }
                    }
                }
            }
        }
    }

    static void SetAttributes(ri_surface_shader* shd, const ri_attributes* pAttr, const ri_geometry* pGeo)
    {
        int nType = pGeo->typeN();
        switch (nType)
        {
        case RI_GEOMETRY_BEZIER_PATCH_MESH:
        {
            SetAttributes(shd, pAttr, dynamic_cast<const ri_bezier_patch_mesh_geometry*>(pGeo));
            break;
        }
        case RI_GEOMETRY_BEZIER_CURVES:
        {
            SetAttributes(shd, pAttr, dynamic_cast<const ri_bezier_curves_geometry*>(pGeo));
            break;
        }
        case RI_GEOMETRY_POINTS_TRIANGLE_POLYGONS:
        {
            SetAttributes(shd, pAttr, dynamic_cast<const ri_points_triangle_polygons_geometry*>(pGeo));
            break;
        }
        case RI_GEOMETRY_SPHERE:
        case RI_GEOMETRY_CONE:
        case RI_GEOMETRY_CYLINDER:
        case RI_GEOMETRY_HYPERBOLOID:
        case RI_GEOMETRY_PARABOLOID:
        case RI_GEOMETRY_DISK:
        case RI_GEOMETRY_TORUS:
        {
            SetAttributesQuadrics(shd, pAttr, pGeo);
            break;
        }
        case RI_GEOMETRY_POINTS:
        {
            SetAttributes(shd, pAttr, dynamic_cast<const ri_points_geometry*>(pGeo));
            break;
        }
        case RI_GEOMETRY_BLOBBY_MESH:
        {
            SetAttributes(shd, pAttr, dynamic_cast<const ri_blobby_mesh_geometry*>(pGeo));
            break;
        }
        }
    }

    void SetMatrices(ri_surface_shader* shd, const ri_attributes* pAttr, const ri_geometry* pGeo, const ri_camera_options* pCamera)
    {
        ri_coordinate_transformer trns;
        trns.set_object_to_world(ConvertMatrix(pGeo->get_transform()));

        const ri_shader* pShader = pAttr->get_suraface();
        if (pShader)
        { //TODO
            trns.set_shader_to_world(ConvertMatrix(pShader->get_transform()));
        }
        if (pCamera)
        {
            trns.set_camera_matrices(pCamera);
        }
        if (pAttr)
        {
            trns.set_attributes(pAttr);
        }

        std::vector<std::string> strs;
        std::vector<matrix4> mats;
        trns.get_matrices(strs, mats, "world"); //current is world
        for (size_t i = 0; i < strs.size(); i++)
        {
            shd->add_matrix(strs[i].c_str(), mats[i]);
        }
    }

    std::shared_ptr<shader> ri_shader_converter::convert_shader(const ri_attributes* pAttr, const ri_geometry* pGeo)
    {
        std::unique_ptr<ri_surface_shader> shd(new ri_surface_shader());

        std::vector<float> fv;

        color3 Cs(1, 1, 1);
        pAttr->get("Color", "color", fv);
        if (fv.size() == 3)
        {
            Cs[0] = fv[0];
            Cs[1] = fv[1];
            Cs[2] = fv[2];
        }
        shd->add_texture("Cs", new ri_uniform_float3_texture(Cs));

        color3 Os(1, 1, 1);
        pAttr->get("Opacity", "color", fv);
        if (fv.size() == 3)
        {
            Os[0] = fv[0];
            Os[1] = fv[1];
            Os[2] = fv[2];
        }
        shd->add_texture("Os", new ri_uniform_float3_texture(Os));

        if (lc_.get())
        {
            std::vector<const ri_light_source*> lv;
            pAttr->get_lightsource(lv);
            for (size_t i = 0; i < lv.size(); i++)
            {
                std::shared_ptr<light> l = lc_->convert(lv[i]);
                if (l.get())
                {
                    shd->add_light(l);
                }
            }
        }

        const ri_shader* pShader = pAttr->get_suraface();
        if (pShader)
        { //TODO
            std::shared_ptr<shader_evaluator> se = ConvertShaderEvaluator(pShader);
            if (se.get())
            {
                shd->set_evaluator(se);
            }
            SetShaderParams(*this, shd.get(), pShader);
        }
        SetAttributes(shd.get(), pAttr, pGeo);
        SetMatrices(shd.get(), pAttr, pGeo, pCamera_);

        if (trc_.get())
        {
            shd->set_tracer(new proxy_tracer(trc_.get()));
        }

        return std::shared_ptr<shader>(shd.release());
    }

    std::shared_ptr<texture<color3> > ri_shader_converter::get_col3_cache(const std::string& str)
    {
        col3_tex_cache::const_iterator it = c3_cache_.find(str);
        if (it != c3_cache_.end()) return it->second;
        return std::shared_ptr<texture<color3> >();
    }

    void ri_shader_converter::set_col3_cache(const std::string& str, const std::shared_ptr<texture<color3> >& tex)
    {
        c3_cache_.insert(col3_tex_cache::value_type(str, tex));
    }

    std::shared_ptr<shader> ri_shader_converter::convert(const ri_geometry* pGeo)
    {
        return convert(&(pGeo->get_attributes()), pGeo);
    }

    std::shared_ptr<shader> ri_shader_converter::convert(const ri_attributes* pAttr, const ri_geometry* pGeo)
    {
        return convert_shader(pAttr, pGeo);
    }

    void ri_shader_converter::set_light_converter(const std::shared_ptr<ri_light_converter>& lc)
    {
        lc_ = lc;
    }

    void ri_shader_converter::set_tracer(const std::shared_ptr<tracer>& trc)
    {
        trc_ = trc;
        if (lc_.get())
        {
            lc_->set_tracer(trc);
        }
    }
}
