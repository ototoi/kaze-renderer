#include "ri_kaze_task_converter.h"
#include "ri_sequence_task.h"
#include "ri_z_render_task.h"
#include "ri_raytrace_render_task.h"
#include "ri_pathtrace_render_task.h"

#include "ri_camera_options.h"
#include "ri_camera_options_converter.h"
#include "ri_autoshadows_task.h"

#include <memory>

using namespace ri;

namespace ri
{
    using namespace kaze;

    static matrix4 ConvertMatrix(const ri_transform& trns)
    {
        ri_transform s(trns);
        s.transpose();
        const float* f = s.get();
        matrix4 tmp(f);
        return tmp;
    }

    static
    std::shared_ptr<ri_task>
    convert_raytrace_render_task(const ri_frame_task* task)
    {
        std::unique_ptr<ri_sequence_task> seq(new ri_sequence_task());
        {
            std::shared_ptr<ri_task> ap(new ri_autoshadows_task(task));
            if (ap.get())
            {
                seq->add_task(ap);
            }
        }
        {
            std::shared_ptr<ri_task> ap(new ri_raytrace_render_task(task));
            if (ap.get())
            {
                seq->add_task(ap);
            }
        }

        return std::shared_ptr<ri_task>(seq.release());
    }

    static
    std::shared_ptr<ri_task>
    convert_pathtrace_render_task(const ri_frame_task* task)
    {
        std::unique_ptr<ri_sequence_task> seq(new ri_sequence_task());
        {
            std::shared_ptr<ri_task> ap(new ri_autoshadows_task(task));
            if (ap.get())
            {
                seq->add_task(ap);
            }
        }
        {
            std::shared_ptr<ri_task> ap(new ri_pathtrace_render_task(task));
            if (ap.get())
            {
                seq->add_task(ap);
            }
        }

        return std::shared_ptr<ri_task>(seq.release());
    }

    static
    std::shared_ptr<ri_task>
    convert_z_render_task(const ri_frame_task* task)
    {
        return std::shared_ptr<ri_task>(new ri_z_render_task(task));
    }

    static
    std::shared_ptr<ri_task>
    convert_color_render_task(const ri_frame_task* task)
    {
        const ri_options& opts = task->get_options();

        std::string strHiderType;
        opts.get("Hider", "type", strHiderType);
        std::string strIntegrationMode;
        bool bIncremental = true;
        if (strHiderType == "raytrace" || strHiderType == "pathtrace")
        {
            opts.get("Hider", "integrationmode", strIntegrationMode);
            int nIncremental = 1;
            if (opts.get("Hider", "incremental", nIncremental))
            {
                bIncremental = nIncremental != 0;
            }
        }

        if (strHiderType == "gale" || strHiderType == "gpu")
        {
            return std::shared_ptr<ri_task>();
        }
        else if (strHiderType == "pathtrace" ||
                 (strIntegrationMode == "path" && bIncremental))
        {
            return convert_pathtrace_render_task(task);
        }
        else
        {
            return convert_raytrace_render_task(task);
        }
    }

    static
    std::shared_ptr<ri_task>
    convert_frame_task(const ri_frame_task* task)
    {
        const ri_options& opts = task->get_options();
        const ri_camera_options* pCmr = NULL;
        size_t csz = opts.get_camera_size();
        if (csz)
        {
            pCmr = opts.get_camera_at(csz - 1);
        }
        if (!pCmr)
            return std::shared_ptr<ri_task>();

        ri_camera_options_converter c_cvtr;
        bool bZ = c_cvtr.require_z(pCmr);
        bool bRGB = c_cvtr.require_color(pCmr);

        if (bZ && !bRGB)
        {
            return convert_z_render_task(task);
        }
        else
        {
            return convert_color_render_task(task);
        }
    }

    namespace
    {
        class ri_wrapper_task : public ri_task
        {
        public:
            ri_wrapper_task(const ri_task* task)
                :task_(task)
            {}
            virtual std::string type() const { return task_->type(); };
            virtual int run() { return const_cast<ri_task*>(task_)->run(); }
        private:
            const ri_task* task_;
        };
    }

    std::shared_ptr<ri_task> ri_kaze_task_converter::convert(const ri_task* task)
    {
        if (task->type() == "Frame")
        {
            return convert_frame_task(static_cast<const ri_frame_task*>(task));
        }
        else
        {
            return std::shared_ptr<ri_task>(new ri_wrapper_task(task));
        }
    }
}
