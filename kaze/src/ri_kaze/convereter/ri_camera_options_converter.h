#ifndef KAZE_RI_CAMERA_OPTIONS_CONVERTER_H
#define KAZE_RI_CAMERA_OPTIONS_CONVERTER_H

#include "ri_camera_options.h"
#include "camera.h"
#include "render_frame.h"

namespace ri
{
    class ri_camera_options_converter
    {
    public:
        std::shared_ptr<kaze::camera> convert_camera(const ri_camera_options* cmr);
        std::shared_ptr<kaze::render_frame> convert_render_frame(const ri_camera_options* cmr);

    public:
        bool require_channel(const ri_camera_options* pCmr, const char* szChannel) const;
        bool require_r(const ri_camera_options* pCmr) const;
        bool require_g(const ri_camera_options* pCmr) const;
        bool require_b(const ri_camera_options* pCmr) const;
        bool require_a(const ri_camera_options* pCmr) const;
        bool require_z(const ri_camera_options* pCmr) const;
        bool require_color(const ri_camera_options* pCmr) const;
    };
}


#endif
