#ifndef RI_KAZE_TASK_CONVERTER_H
#define RI_KAZE_TASK_CONVERTER_H

#include "ri_task.h"
#include "ri_frame_task.h"

namespace ri
{
    class ri_kaze_task_converter
    {
    public:
        std::shared_ptr<ri_task> convert(const ri_task*);
    };
}

#endif
