#ifndef KAZE_RI_SHADER_CONVERTER_H
#define KAZE_RI_SHADER_CONVERTER_H

#include "shader.h"
#include "texture.hpp"
#include "ri_light_converter.h"

#ifndef KAZE_MAP_TYPE
#include "mapset_selection.h"
#endif


namespace ri
{
    class ri_geometry;
    class ri_camera_options;
    class ri_shader;
    class ri_shader_converter
    {
    public:
        std::shared_ptr<kaze::shader> convert(const ri_geometry* pGeo);
        std::shared_ptr<kaze::shader> convert(const ri_attributes* pAttr, const ri_geometry* pGeo);

        ri_shader_converter() : pCamera_(0) {}
        ri_shader_converter(const ri_camera_options* pCamera) : pCamera_(pCamera) {}
    protected:
        std::shared_ptr<kaze::shader> convert_shader(const ri_attributes* pAttr, const ri_geometry* pGeo);

    public:
        void set_light_converter(const std::shared_ptr<ri_light_converter>& lc);
        void set_tracer(const std::shared_ptr<kaze::tracer>& trc);

    public:
        std::shared_ptr<kaze::texture<kaze::color3> > get_col3_cache(const std::string& str);
        void set_col3_cache(const std::string& str, const std::shared_ptr<kaze::texture<kaze::color3> >& tex);

    protected:
        typedef KAZE_MAP_TYPE<std::string, std::shared_ptr<kaze::texture<kaze::color3> > > col3_tex_cache;
        col3_tex_cache c3_cache_;

    protected:
        std::shared_ptr<ri_light_converter> lc_;
        std::shared_ptr<kaze::tracer> trc_;
        const ri_camera_options* pCamera_;
    };
}


#endif
