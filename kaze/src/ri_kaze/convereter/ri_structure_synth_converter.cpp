#include "ri_structure_synth_converter.h"

#include "ss_decoder.h"
#include "ss_preprocessor.h"
#include "ss_evaluator.h"
#include "ss_context_loader.h"
#include "ss_geometry.h"

#include "ri_transform.h"
#include "ri_attributes.h"
#include "box_intersection.h"
#include "sphere_intersection.h"
#include "frame_box_intersection.h"
#include "triangle_intersection.h"
#include "bvh_composite_intersection.h"
#include "transformed_intersection.h"
#include "shadered_intersection.h"
#include "ri_geometry.h"

#include "ri_shader_converter.h"

#include <fstream>
#include <sstream>

namespace ri
{
    using namespace kaze;
    using namespace ss;
    
    namespace
    {
        class mshader : public surface_shader
        {
        public:
            mshader(const color3& c) : col_(c) {}
            spectrum shade(const enumerator<radiance>& rads, const sufflight& sl,
                           const mediation& md) const
            {
                // real d = dot(sl.get_normal(), vector3(0,1,0)) + 0.2;
                // d = std::max(d, 0.0);
                // printf("%f,%f,%f\n",col_[0],col_[1],col_[2]);
                // return convert(d*col_+(1-d)*color3(0,0.1,0.15));
                return convert(col_);
            }

        private:
            color3 col_;
        };
    }

    static matrix4 ConvertMatrix(const ss_attributes& trns)
    {
        ri_transform s(trns.get_transform_ptr());
        s.transpose();
        const float* f = s.get();
        matrix4 tmp(f);
        return tmp;
    }

    static std::shared_ptr<intersection> ConvertGeom(const ss_box_geometry* pGeo)
    {
        matrix4 mat = ConvertMatrix(pGeo->get_attributes());
        vector3 min = vector3(-0.5, -0.5, -0.5);
        vector3 max = vector3(+0.5, +0.5, +0.5);
        return std::shared_ptr<intersection>(
            new m4_transformed_intersection(new box_intersection(min, max), mat));
    }

    static std::shared_ptr<intersection> ConvertGeom(const ss_sphere_geometry* pGeo)
    {
        matrix4 mat = ConvertMatrix(pGeo->get_attributes());
        return std::shared_ptr<intersection>(new m4_transformed_intersection(
            new sphere_intersection(vector3(0, 0, 0), 0.5), mat));
    }

    static std::shared_ptr<intersection> ConvertGeom(const ss_grid_geometry* pGeo)
    {
        matrix4 mat = ConvertMatrix(pGeo->get_attributes());
        vector3 min = vector3(-0.5, -0.5, -0.5);
        vector3 max = vector3(+0.5, +0.5, +0.5);
        real radius = 0.05f;
        return std::shared_ptr<intersection>(new m4_transformed_intersection(
            new frame_box_intersection(min, max, radius), mat));
    }

    static std::shared_ptr<intersection> ConvertGeom(const ss_line_geometry* pGeo)
    {
        matrix4 mat = ConvertMatrix(pGeo->get_attributes());
        vector3 min = vector3(-0.5, -0.5, -0.5);
        vector3 max = vector3(+0.5, +0.5, +0.5);
        return std::shared_ptr<intersection>(
            new m4_transformed_intersection(new box_intersection(min, max), mat));
    }

    static std::shared_ptr<intersection> ConvertGeom(const ss_point_geometry* pGeo)
    {
        matrix4 mat = ConvertMatrix(pGeo->get_attributes());
        return std::shared_ptr<intersection>(new m4_transformed_intersection(
            new sphere_intersection(vector3(0, 0, 0), 0.5), mat));
    }

    static std::shared_ptr<intersection> ConvertGeom(const ss_triangle_geometry* pGeo)
    {
        matrix4 mat = ConvertMatrix(pGeo->get_attributes());
        const float* f = pGeo->get_positions();
        vector3 p0(f + 0);
        vector3 p1(f + 3);
        vector3 p2(f + 6);
        return std::shared_ptr<intersection>(new m4_transformed_intersection(
            new triangle_intersection(p0, p1, p2), mat));
    }

    static std::shared_ptr<intersection> ConvertGeom(const ss_cylinder_geometry* pGeo)
    {
        matrix4 mat = ConvertMatrix(pGeo->get_attributes());
        vector3 min = vector3(-0.5, -0.5, -0.5);
        vector3 max = vector3(+0.5, +0.5, +0.5);
        return std::shared_ptr<intersection>(
            new m4_transformed_intersection(new box_intersection(min, max), mat));
    }

    static std::shared_ptr<intersection> ConvertGeom(const ss_tube_geometry* pGeo)
    {
        matrix4 mat = ConvertMatrix(pGeo->get_attributes());
        vector3 min = vector3(-0.5, -0.5, -0.5);
        vector3 max = vector3(+0.5, +0.5, +0.5);
        return std::shared_ptr<intersection>(
            new m4_transformed_intersection(new box_intersection(min, max), mat));
    }

    static std::shared_ptr<intersection> ConvertGeom(const ss_geometry* pGeo)
    {
        int nType = pGeo->get_type();
        switch (nType)
        {
        case GEOMETRY_BOX:
            return ConvertGeom((const ss_box_geometry*)pGeo);
        case GEOMETRY_SPHERE:
            return ConvertGeom((const ss_sphere_geometry*)pGeo);
        case GEOMETRY_GRID:
            return ConvertGeom((const ss_grid_geometry*)pGeo);
        case GEOMETRY_LINE:
            return ConvertGeom((const ss_line_geometry*)pGeo);
        case GEOMETRY_POINT:
            return ConvertGeom((const ss_point_geometry*)pGeo);
        case GEOMETRY_TRIANGLE:
            return ConvertGeom((const ss_triangle_geometry*)pGeo);
        case GEOMETRY_CYLINDER:
            return ConvertGeom((const ss_cylinder_geometry*)pGeo);
        case GEOMETRY_TUBE:
            return ConvertGeom((const ss_tube_geometry*)pGeo);
        }
        return std::shared_ptr<intersection>();
    }

    static std::shared_ptr<intersection> ConvertAttr(const ri_attributes* pAttr,
                                               const ri_geometry* pGeo1,
                                               const ss_geometry* pGeo2)
    {
        static ri_shader_converter sc;
        std::shared_ptr<intersection> inter = ConvertGeom(pGeo2);
        if (inter.get())
        {
            const ss_attributes& ssattr = pGeo2->get_attributes();
            float f[4];
            memcpy(f, ssattr.get_color_ptr(), sizeof(float) * 4);
            ri_attributes attr2(*pAttr);
            attr2.set("Color", "color", f, 3);
            std::shared_ptr<shader> shd(new mshader(color3(f))); // sc.convert(&attr2, pGeo1)
            if (shd.get())
            {
                return std::shared_ptr<intersection>(new shadered_intersection(inter, shd));
            }
        }
        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_structure_synth_converter::convert(
        const ri_structure_synth_geometry* pGeo) const
    {
        const ri_attributes* pAttr = &(pGeo->get_attributes());
        std::string path = pGeo->get_full_path();

        std::ifstream is(path);
        if (!is)
            return std::shared_ptr<intersection>();

        int nRet = 0;

        std::stringstream ss;
        nRet = ss_preprocessor::run(ss, is);
        if (nRet != 0)
            return std::shared_ptr<intersection>();

        ss_context ctx;
        ss_decoder dec(ss);

        nRet = ss_context_loader::run(&ctx, &dec);
        if (nRet != 0)
            return std::shared_ptr<intersection>();

        ss_scene scn;
        nRet = ss_evaluator::run(&scn, &ctx);
        if (nRet != 0)
            return std::shared_ptr<intersection>();

        size_t sz = scn.get_geometry_size();
        if (sz != 0)
        {
            std::unique_ptr<composite_intersection> ap(new bvh_composite_intersection());
            for (size_t i = 0; i < sz; i++)
            {
                const ss_geometry* pGeo2 = scn.get_geometry_at(i);
                std::shared_ptr<intersection> inter = ConvertAttr(pAttr, pGeo, pGeo2);
                if (inter.get())
                {
                    ap->add(inter);
                }
            }
            ap->construct();
            return std::shared_ptr<intersection>(ap.release());
        }

        return std::shared_ptr<intersection>();
    }
}
