#include "ri_light_converter.h"
#include "ri_light_source.h"
#include "ri_geometry.h"

#include "ri_ambientlight_light.h"
#include "ri_distantlight_light.h"
#include "ri_pointlight_light.h"
#include "ri_spotlight_light.h"
#include "ri_arealight_light.h"

#include "ri_trace_shadower.h"
#include "ri_shadowmap_shadower.h"

#include "ri_disk_light_sampler.h"
#include "point_light_sampler.h"

#include "proxy_tracer.h"

#include "values.h"

namespace ri
{
    using namespace kaze;

    namespace
    {
        enum
        {
            RI_LIGHTSOURCE_UNKNOWN = 2000,
            RI_LIGHTSOURCE_AMBIENTLIGHT,
            RI_LIGHTSOURCE_DISTANTLIGHT,
            RI_LIGHTSOURCE_POINTLIGHT,
            RI_LIGHTSOURCE_SPOTLIGHT,
            RI_LIGHTSOURCE_SHADOWSPOT,
            RI_LIGHTSOURCE_AREALIGHT
        };

        struct T2I
        {
            const char* name;
            int num;
        };

        static T2I PREDEFINED_SHADERS[] =
            {
                {"ambientlight", RI_LIGHTSOURCE_AMBIENTLIGHT},
                {"distantlight", RI_LIGHTSOURCE_DISTANTLIGHT},
                {"pointlight", RI_LIGHTSOURCE_POINTLIGHT},
                {"spotlight", RI_LIGHTSOURCE_SPOTLIGHT},
                {"shadowspot", RI_LIGHTSOURCE_SHADOWSPOT},
                {"arealight", RI_LIGHTSOURCE_AREALIGHT},
                {NULL, -1},
        };

        static int GetPreDefinedShader(const char* name)
        {
            int i = 0;
            while (PREDEFINED_SHADERS[i].name)
            {
                if (strcmp(PREDEFINED_SHADERS[i].name, name) == 0) return PREDEFINED_SHADERS[i].num;
                i++;
            }
            return -1;
        }
    }

    static matrix4 ConvertMatrix(const ri_transform& trns)
    {
        ri_transform s(trns);
        s.transpose();
        const float* f = s.get();
        matrix4 tmp(f);
        return tmp;
    }

    static void GetValue(std::vector<int>& iValue, const ri_light_source* pLight, const char* key)
    {
        const ri_parameters::value_type* pv = pLight->get_parameter(key);
        if (pv)
        {
            if (pv->nType == ri_parameters::TYPE_INTEGER)
            {
                iValue = pv->integer_values;
            }
            else
            {
                iValue.clear();
            }
        }
        else
        {
            iValue.clear();
        }
    }

    static void GetValue(std::vector<float>& fValue, const ri_light_source* pLight, const char* key)
    {
        const ri_parameters::value_type* pv = pLight->get_parameter(key);
        if (pv)
        {
            if (pv->nType == ri_parameters::TYPE_FLOAT)
            {
                fValue = pv->float_values;
            }
            else
            {
                fValue.clear();
            }
        }
        else
        {
            fValue.clear();
        }
    }

    static void GetValue(std::vector<std::string>& sValue, const ri_light_source* pLight, const char* key)
    {
        const ri_parameters::value_type* pv = pLight->get_parameter(key);
        if (pv)
        {
            if (pv->nType == ri_parameters::TYPE_STRING)
            {
                sValue = pv->string_values;
            }
            else
            {
                sValue.clear();
            }
        }
        else
        {
            sValue.clear();
        }
    }

    struct shadow_info
    {
        bool bRaytraceShadow;
    };

    static shadow_info GetShadowInfo(const ri_light_source* pLight)
    {
        shadow_info si;
        const ri_attributes& attr = pLight->get_attributes();

        bool bRaytraceShadow = false;
        std::string strRet;
        if (attr.get("light", "shadows", strRet)) //Attribute "light" "shadows" "on"
        {
            if (strRet == "on" || strRet == "On")
            {
                bRaytraceShadow = true;
            }
        }

        std::vector<std::string> sValue;
        GetValue(sValue, pLight, "shadowname");
        if (sValue.size() == 1)
        {
            if (sValue[0] == "raytrace")
            {
                bRaytraceShadow = true;
            }
        }

        si.bRaytraceShadow = bRaytraceShadow;

        return si;
    }

    static std::shared_ptr<light> ConvertAmbientLight(const ri_light_source* pLight)
    {
        assert(pLight->get_name() == "ambientlight");

        real intensity = 1.0;
        color3 lightcolor = color3(1, 1, 1);

        std::vector<float> fValue;

        GetValue(fValue, pLight, "intensity");
        if (fValue.size() == 1)
        {
            intensity = fValue[0];
        }
        GetValue(fValue, pLight, "lightcolor");
        if (fValue.size() == 3)
        {
            lightcolor = vector3(fValue[0], fValue[1], fValue[2]);
        }

        return std::shared_ptr<light>(new ri_ambientlight_light(intensity, lightcolor));
    }

    static std::shared_ptr<light> ConvertDistantLight(const ri_light_source* pLight, const std::shared_ptr<tracer>& trc)
    {
        assert(pLight->get_name() == "distantlight");

        matrix4 mat = ConvertMatrix(pLight->get_transform());
        real intensity = 1.0;
        vector3 from = mat * vector3(0, 0, 0); //camera
        vector3 to = mat * vector3(0, 0, 1);   //camera
        color3 lightcolor = color3(1, 1, 1);

        std::vector<float> fValue;

        GetValue(fValue, pLight, "intensity");
        if (fValue.size() == 1)
        {
            intensity = fValue[0];
        }
        GetValue(fValue, pLight, "lightcolor");
        if (fValue.size() == 3)
        {
            lightcolor = vector3(fValue[0], fValue[1], fValue[2]);
        }
        GetValue(fValue, pLight, "from");
        if (fValue.size() == 3)
        {
            from = mat * vector3(fValue[0], fValue[1], fValue[2]);
        }
        GetValue(fValue, pLight, "to");
        if (fValue.size() == 3)
        {
            to = mat * vector3(fValue[0], fValue[1], fValue[2]);
        }

        std::unique_ptr<ri_distantlight_light> ap(new ri_distantlight_light(intensity, lightcolor, from, to));

        shadow_info si = GetShadowInfo(pLight);
        if (si.bRaytraceShadow)
        {
            ap->set_shadower(new ri_trace_shadower(new proxy_tracer(trc.get())));
        }

        return std::shared_ptr<light>(ap.release());
    }

    static std::shared_ptr<light> ConvertPointLight(const ri_light_source* pLight, const std::shared_ptr<tracer>& trc)
    {
        assert(pLight->get_name() == "pointlight");
        matrix4 mat = ConvertMatrix(pLight->get_transform());

        real intensity = 1.0;
        vector3 from = mat * vector3(0, 0, 0); //camera
        color3 lightcolor = color3(1, 1, 1);

        std::vector<float> fValue;

        GetValue(fValue, pLight, "intensity");
        if (fValue.size() == 1)
        {
            intensity = fValue[0];
        }
        GetValue(fValue, pLight, "lightcolor");
        if (fValue.size() == 3)
        {
            lightcolor = vector3(fValue[0], fValue[1], fValue[2]);
        }
        GetValue(fValue, pLight, "from");
        if (fValue.size() == 3)
        {
            from = mat * vector3(fValue[0], fValue[1], fValue[2]);
        }

        return std::shared_ptr<light>(new ri_pointlight_light(intensity, lightcolor, from));
    }

    static std::shared_ptr<light> ConvertSpotLight(const ri_light_source* pLight, const std::shared_ptr<tracer>& trc)
    {
        //assert(pLight->get_name() == "spotlight");
        matrix4 mat = ConvertMatrix(pLight->get_transform());

        real intensity = 1.0;
        vector3 from = mat * vector3(0, 0, 0); //shader->current=object->world
        vector3 to = mat * vector3(0, 0, 1);   //shader->current=object->world
        color3 lightcolor = color3(1, 1, 1);
        real coneangle = values::radians(30.0);
        real conedeltaangle = values::radians(5.0);
        real beamdistribution = 2.0;

        std::string shadowname = "";
        int samples = 16;
        real width = 1;
        real blur = 0;
        real bias = 0;
        int falloff = 2;

        std::vector<int> iValue;
        std::vector<float> fValue;
        std::vector<std::string> sValue;

        GetValue(fValue, pLight, "intensity");
        if (fValue.size() == 1)
        {
            intensity = fValue[0];
        }
        GetValue(fValue, pLight, "lightcolor");
        if (fValue.size() == 3)
        {
            lightcolor = vector3(fValue[0], fValue[1], fValue[2]);
        }
        GetValue(fValue, pLight, "from");
        if (fValue.size() == 3)
        {
            from = mat * vector3(fValue[0], fValue[1], fValue[2]);
        }
        GetValue(fValue, pLight, "to");
        if (fValue.size() == 3)
        {
            to = mat * vector3(fValue[0], fValue[1], fValue[2]);
        }
        GetValue(fValue, pLight, "coneangle");
        if (fValue.size() == 1)
        {
            coneangle = fValue[0];
        }
        GetValue(fValue, pLight, "conedeltaangle");
        if (fValue.size() == 1)
        {
            conedeltaangle = fValue[0];
        }

        shadow_info si = GetShadowInfo(pLight);

        if (si.bRaytraceShadow)
        {
            std::unique_ptr<ri_spotlight_light> ap(new ri_spotlight_light(intensity, lightcolor, from, to, coneangle, conedeltaangle, beamdistribution));
            ap->set_shadower(new ri_trace_shadower(new proxy_tracer(trc.get())));
            return std::shared_ptr<light>(ap.release());
        }
        else
        {
            GetValue(sValue, pLight, "shadowname");
            if (sValue.size() == 1)
            {
                shadowname = sValue[0];
            }
            GetValue(iValue, pLight, "samples");
            if (iValue.size() == 1)
            {
                samples = iValue[0];
            }
            GetValue(fValue, pLight, "width");
            if (fValue.size() == 1)
            {
                width = fValue[0];
            }
            GetValue(fValue, pLight, "blur");
            if (fValue.size() == 1)
            {
                blur = fValue[0];
            }
            GetValue(fValue, pLight, "bias");
            if (fValue.size() == 1)
            {
                bias = fValue[0];
            }
            GetValue(iValue, pLight, "falloff");
            if (iValue.size() == 1)
            {
                falloff = iValue[0];
            }
            std::unique_ptr<ri_spotlight_light> ap(new ri_spotlight_light(intensity, lightcolor, from, to, coneangle, conedeltaangle, beamdistribution));
            if (shadowname != "")
            {
                ap->set_shadower(new ri_shadowmap_shadower(shadowname.c_str(), samples, width, blur, bias));
            }
            return std::shared_ptr<light>(ap.release());
        }
    }

    //----------------------------------------------------------------------------------------------------
    static int GetSamples(const ri_geometry* pGeo)
    {
        int nsamples = 1;
        std::vector<int> nvsamples;
        pGeo->get_attributes().get("light", "nsamples", nvsamples);
        if (nvsamples.size() == 1)
        {
            nsamples = nvsamples[0];
        }
        return nsamples;
    }

    static std::shared_ptr<light_sampler> ConvertLightSampler(const ri_disk_geometry* pGeo)
    {
        int nsamples = GetSamples(pGeo);
        matrix4 mat = ConvertMatrix(pGeo->get_transform());
        float height = pGeo->get_height();
        float radius = pGeo->get_radius();
        float tmax = pGeo->get_tmax();
        return std::shared_ptr<light_sampler>(new ri_disk_light_sampler(height, radius, tmax, mat, nsamples));
    }

    static std::shared_ptr<light_sampler> ConvertLightSampler(const ri_geometry* pGeo)
    {
        int nType = pGeo->typeN();
        switch (nType)
        {
        case RI_GEOMETRY_DISK:
            return ConvertLightSampler(dynamic_cast<const ri_disk_geometry*>(pGeo));
        default:
        {
            matrix4 mat = ConvertMatrix(pGeo->get_transform());
            return std::shared_ptr<light_sampler>(new point_light_sampler(mat));
        }
        }
    }

    //----------------------------------------------------------------------------------------------------

    static std::shared_ptr<light> ConvertAreaLight(const ri_light_source* pLight, const std::shared_ptr<tracer>& trc)
    {
        assert(pLight->get_name() == "arealight");
        matrix4 mat = ConvertMatrix(pLight->get_transform());

        real intensity = 1.0;
        vector3 from = mat * vector3(0, 0, 0); //camera
        color3 lightcolor = color3(1, 1, 1);

        std::vector<float> fValue;

        GetValue(fValue, pLight, "intensity");
        if (fValue.size() == 1)
        {
            intensity = fValue[0];
        }
        GetValue(fValue, pLight, "lightcolor");
        if (fValue.size() == 3)
        {
            lightcolor = vector3(fValue[0], fValue[1], fValue[2]);
        }

        const ri_area_light_source* pAreaLight = dynamic_cast<const ri_area_light_source*>(pLight);
        if (pAreaLight)
        {
            std::vector<std::shared_ptr<light_sampler> > shapes;
            std::vector<const ri_geometry*> geos;
            pAreaLight->get_geometry(geos);
            for (size_t i = 0; i < geos.size(); i++)
            {
                std::shared_ptr<light_sampler> ls = ConvertLightSampler(geos[i]);
                if (ls.get())
                {
                    shapes.push_back(ls);
                }
            }
            if(shapes.size() == 1)
            {
                return std::shared_ptr<light>(new ri_arealight_light(intensity, lightcolor, shapes[0]));
            }
        }
   
        return std::shared_ptr<light>();
    }

    std::shared_ptr<light> ri_light_converter::convert(const ri_light_source* pLight)
    {
        return convert_cached_lightsource(pLight);
    }

    std::shared_ptr<light> ri_light_converter::convert_lightsource(const ri_light_source* pLight)
    {
        assert(pLight);

        //Find User Define Shader
        bool bFindShader = false;
        if (!bFindShader)
        {
            int nShader = GetPreDefinedShader(pLight->get_name().c_str());
            if (nShader >= 0)
            {
                switch (nShader)
                {
                case RI_LIGHTSOURCE_AMBIENTLIGHT:
                    return ConvertAmbientLight(pLight);
                case RI_LIGHTSOURCE_DISTANTLIGHT:
                    return ConvertDistantLight(pLight, trc_);
                case RI_LIGHTSOURCE_POINTLIGHT:
                    return ConvertPointLight(pLight, trc_);
                case RI_LIGHTSOURCE_SPOTLIGHT:
                    return ConvertSpotLight(pLight, trc_);
                case RI_LIGHTSOURCE_SHADOWSPOT:
                    return ConvertSpotLight(pLight, trc_);
                case RI_LIGHTSOURCE_AREALIGHT:
                    return ConvertAreaLight(pLight, trc_);
                }
            }
        }
        return std::shared_ptr<light>();
    }

    std::shared_ptr<light> ri_light_converter::convert_cached_lightsource(const ri_light_source* pLight)
    {
        map_type::iterator it = cache_.find(pLight);
        if (it != cache_.end())
        {
            return it->second;
        }
        else
        {
            std::shared_ptr<light> lgt = convert_lightsource(pLight);
            if (lgt.get())
            {
                cache_.insert(map_type::value_type(pLight, lgt));
            }
            return lgt;
        }
    }

    void ri_light_converter::set_tracer(const std::shared_ptr<tracer>& trc)
    {
        trc_ = trc;
    }
}
