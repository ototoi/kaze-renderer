#ifndef RI_STURUCTURE_SYNTH_CONVERTER_H
#define RI_STURUCTURE_SYNTH_CONVERTER_H

#include "intersection.h"
#include "ri_geometry.h"

namespace ri
{
    class ri_structure_synth_converter
    {
    public:
        std::shared_ptr<kaze::intersection>
        convert(const ri_structure_synth_geometry* pGeo) const;
    };
}

#endif
