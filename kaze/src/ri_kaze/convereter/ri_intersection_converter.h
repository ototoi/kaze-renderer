#ifndef KAZE_RI_INTERSECTION_CONVERTER_H
#define KAZE_RI_INTERSECTION_CONVERTER_H

#include "ri_geometry.h"
#include "ri_camera_options.h"
#include "intersection.h"
#include "camera.h"

#include "ri_geometry_converter.h"
#include "ri_shader_converter.h"
#include "ri_coordinate_transformer.h"

#ifndef KAZE_MAP_TYPE
#include "mapset_selection.h"
#endif

namespace ri
{

    class ri_camera_options;
    class ri_transform;

    class ri_intersection_converter
    {
    public:
        ri_intersection_converter() : pCamera_(0) {}
        ri_intersection_converter(const ri_camera_options* pCamera) : pCamera_(pCamera) {}
    public:
        std::shared_ptr<kaze::intersection> convert(const ri_geometry* pGeo);

    public:
        void set_shader_converter(const std::shared_ptr<ri_shader_converter>& ac);

    protected:
        const ri_geometry* convert_geometry_to_geometry(const ri_geometry* pGeo);

    protected:
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_geometry* pGeo);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_geometry* pGeo, const ri_transform& trns);

        std::shared_ptr<kaze::intersection> convert_geometry(const ri_polygon_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_general_polygon_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_points_polygons_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_points_general_polygons_geometry* pGeo, const ri_transform& trns);

        std::shared_ptr<kaze::intersection> convert_geometry(const ri_patch_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_patch_mesh_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_nu_patch_geometry* pGeo, const ri_transform& trns);

        std::shared_ptr<kaze::intersection> convert_geometry(const ri_sphere_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_cone_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_cylinder_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_disk_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_torus_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_hyperboloid_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_paraboloid_geometry* pGeo, const ri_transform& trns);

        std::shared_ptr<kaze::intersection> convert_geometry(const ri_blobby_geometry* pGeo, const ri_transform& trns);

        std::shared_ptr<kaze::intersection> convert_geometry(const ri_curves_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_points_geometry* pGeo, const ri_transform& trns);

        std::shared_ptr<kaze::intersection> convert_geometry(const ri_subdivision_mesh_geometry* pGeo, const ri_transform& trns);

        std::shared_ptr<kaze::intersection> convert_geometry(const ri_teapot_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_bunny_geometry* pGeo, const ri_transform& trns);

        std::shared_ptr<kaze::intersection> convert_geometry(const ri_group_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_solid_geometry* pGeo, const ri_transform& trns);

        std::shared_ptr<kaze::intersection> convert_geometry(const ri_bezier_patch_mesh_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_points_triangle_polygons_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_procedural_evaluation_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_bezier_curves_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_file_mesh_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_structure_synth_geometry* pGeo, const ri_transform& trns);

        std::shared_ptr<kaze::intersection> convert_geometry(const ri_motion_geometry* pGeo, const ri_transform& trns);
        std::shared_ptr<kaze::intersection> convert_geometry(const ri_object_instance_geometry* pGeo, const ri_transform& trns);

    protected:
        std::shared_ptr<kaze::intersection> convert_attributed_geometry(const ri_geometry* pGeo);
        std::shared_ptr<kaze::intersection> convert_attributed_geometry(const ri_attributes* pAttr, const ri_geometry* pGeo);
        std::shared_ptr<kaze::intersection> convert_attributed_geometry(const ri_group_geometry* pGeo);
        std::shared_ptr<kaze::intersection> convert_attributed_geometry(const ri_solid_geometry* pGeo);
        std::shared_ptr<kaze::intersection> convert_attributed_geometry(const ri_object_instance_geometry* pGeo);

    protected:
        std::shared_ptr<kaze::intersection> convert_cached_geometry(const ri_geometry* pGeo);

    protected:
        typedef KAZE_MAP_TYPE<const ri_geometry*, std::shared_ptr<kaze::intersection> > map_type;
        map_type cache_;

    protected:
        ri_geometry_converter ggc_;
    protected:
        std::shared_ptr<ri_shader_converter> ac_;

    protected:
        const ri_camera_options* pCamera_;
    };
}

#endif
