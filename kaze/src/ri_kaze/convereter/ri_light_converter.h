#ifndef KAZE_RI_LIGHT_CONVERTER_H
#define KAZE_RI_LIGHT_CONVERTER_H

#include "ri_light_source.h"
#include "light.h"
#include "tracer.h"
#include <map>

namespace ri
{
    class ri_light_converter
    {
    public:
        std::shared_ptr<kaze::light> convert(const ri_light_source* pLight);

    public:
        void set_tracer(const std::shared_ptr<kaze::tracer>& trc);

    protected:
        std::shared_ptr<kaze::light> convert_lightsource(const ri_light_source* pLight);
        std::shared_ptr<kaze::light> convert_cached_lightsource(const ri_light_source* pLight);

    protected:
        typedef std::map<const ri_light_source*, std::shared_ptr<kaze::light> > map_type;
        map_type cache_;

        std::shared_ptr<kaze::tracer> trc_;
    };
}

#endif
