#include "ri_camera_options_converter.h"
#include "ri_camera_options.h"
#include "ri_camera.h"

#include "ri_display.h"
#include "ri_framebuffer_render_frame.h"
#include "ri_zframebuffer_render_frame.h"
#include "ri_file_render_frame.h"
#include "ri_tiff_render_frame.h"
#include "ri_bmp_render_frame.h"
#include "ri_png_render_frame.h"
#include "ri_sgif_render_frame.h"
#include "ri_zfile_render_frame.h"
#include "ri_window_render_frame.h"
#include "composite_render_frame.h"

#include "transform_matrix.h"

#include "ri.h"

#include <memory>
#include <iostream>

namespace ri
{
    using namespace kaze;
    
    static matrix4 ConvertMatrix(const ri_transform& trns)
    {
        ri_transform s(trns);
        s.transpose();
        const float* f = s.get();
        matrix4 tmp(f);
        return tmp;
    }

    static void ConvertMatrix(double mat[16], const matrix4& m)
    {
        for (int j = 0; j < 4; j++)
        {
            for (int i = 0; i < 4; i++)
            {
                mat[4 * j + i] = (double)m[j][i];
            }
        }
    }

    std::shared_ptr<camera> ri_camera_options_converter::convert_camera(const ri_camera_options* pCmr)
    {
        int nType = pCmr->get_camera_type();
        if (nType == ri_camera_options::RI_CAMERA_PERSPECTIVE)
        {
            float fov = pCmr->get_fov();
            float angle = fov;
            float aspect = pCmr->get_frame_aspect_ratio();
            float left = -aspect;
            float right = +aspect;
            float bottom = -1;
            float top = +1;
            pCmr->get_screen_window(left, right, bottom, top);

            std::unique_ptr<ri_perspective_camera> ap;
            if (!pCmr->is_motion())
            {
                matrix4 mat = ConvertMatrix(pCmr->get_camera_to_world());
                ap.reset(new ri_perspective_camera(mat, angle, aspect, left, right, bottom, top));
            }
            else
            {
                typedef ri_perspective_camera::sequence_t sequence_t;
                size_t n = pCmr->get_motion_size();
                std::vector<sequence_t> seq(n);
                for (size_t i = 0; i < n; i++)
                {
                    seq[i].t = pCmr->get_time_at(i);
                    seq[i].mat = ConvertMatrix(pCmr->get_camera_to_world_at(i));
                }
                ap.reset(new ri_perspective_camera(seq, angle, aspect, left, right, bottom, top));
            }

            float fstop = (RtFloat)RI_INFINITY;
            float focallength = (RtFloat)RI_INFINITY;
            float focaldistance = (RtFloat)RI_INFINITY;
            bool bEnableDOF = pCmr->get_depth_of_field(fstop, focallength, focaldistance);
            if (bEnableDOF)
            {
                ap->set_depth_of_field(fstop, focallength, focaldistance);
            }

            return std::shared_ptr<camera>(ap.release());
        }
        else if (nType == ri_camera_options::RI_CAMERA_ORTHOGRAPHIC)
        {
            float aspect = pCmr->get_frame_aspect_ratio();
            float left = -aspect;
            float right = +aspect;
            float bottom = -1.0f;
            float top = +1.0f;
            pCmr->get_screen_window(left, right, bottom, top);
            float width = right - left;
            float height = top - bottom;

            matrix4 mat = ConvertMatrix(pCmr->get_camera_to_world());
            return std::shared_ptr<camera>(new ri_orthogonal_camera(mat, width, height));
        }
        return std::shared_ptr<camera>();
    }

    static std::string RemovePrefix(const std::string& str)
    {
        if (!str.empty())
        {
            if (str[0] == '+')
            {
                return str.substr(1);
            }
        }
        return str;
    }

    static std::shared_ptr<render_frame> ConvertRenderFrame(const ri_camera_options* pCmr, const ri_display* pDisp, bool bForce = false)
    {
        std::string strName = pDisp->get_name();
        std::string strType = pDisp->get_type();
        std::string strMode = pDisp->get_mode();

        strName = RemovePrefix(strName);

        int width = 640;
        int height = 480;
        float aspect = 1;
        pCmr->get_format(width, height, aspect);
        width = abs(width);
        height = abs(height);
        aspect = fabs(aspect);
        
        if (strType == RI_FILE)
        {
            return std::shared_ptr<render_frame>(new ri_file_render_frame(strName.c_str(), strType.c_str(), strMode.c_str(), width, height));
        }
        else if (strType == "tif" || strType == "tiff")
        {
            return std::shared_ptr<render_frame>(new ri_tiff_render_frame(strName.c_str(), strType.c_str(), strMode.c_str(), width, height));
        }
        else if (strType == "bmp" || strType == "bitmap")
        {
            return std::shared_ptr<render_frame>(new ri_bmp_render_frame(strName.c_str(), strType.c_str(), strMode.c_str(), width, height));
        }
        else if (strType == "png")
        {
            return std::shared_ptr<render_frame>(new ri_png_render_frame(strName.c_str(), strType.c_str(), strMode.c_str(), width, height));
        }
        else if (strType == "sgif" || strType == "sgi")
        {
            return std::shared_ptr<render_frame>(new ri_sgif_render_frame(strName.c_str(), strType.c_str(), strMode.c_str(), width, height));
        }
        else if (strType == "zfile" || strType == "z" || strType == "shadow")
        {
            double w2c[16] = {};
            double w2s[16] = {};

            int nType = pCmr->get_camera_type();

            float fov = pCmr->get_fov();
            float angle = fov;
            float aspect = pCmr->get_frame_aspect_ratio();
            float left = -aspect;
            float right = +aspect;
            float bottom = -1.0f;
            float top = +1.0f;
            float near = (float)RI_EPSILON;
            float far = (float)RI_INFINITY;
            pCmr->get_screen_window(left, right, bottom, top);
            pCmr->get_clipping(near, far);

            matrix4 mw2c = ConvertMatrix(pCmr->get_world_to_camera()); //w2c//get_world_to_camera
            ConvertMatrix(w2c, mw2c);

            if (nType == ri_camera_options::RI_CAMERA_PERSPECTIVE)
            {
                matrix4 mc2c = camera2clip_01(values::radians(angle), aspect, near, far);
                ConvertMatrix(w2s, mc2c * mw2c);
            }
            else
            {
                matrix4 mc2c = camera2clip_01(left, right, bottom, top, near, far);
                ConvertMatrix(w2s, mc2c * mw2c);
            }

            return std::shared_ptr<render_frame>(new ri_zfile_render_frame(strName.c_str(), strType.c_str(), strMode.c_str(), width, height, w2c, w2s));
        }
        else if (strType == "x11")
        {
            return std::shared_ptr<render_frame>(new ri_x11_render_frame(strName.c_str(), strType.c_str(), strMode.c_str(), width, height));
        }
        else if (strType == "windows")
        {
            return std::shared_ptr<render_frame>(new ri_windows_render_frame(strName.c_str(), strType.c_str(), strMode.c_str(), width, height));
        }
        else if (strType == RI_FRAMEBUFFER || strType == "zframebuffer" || bForce)
        {
            if (strMode == "z" || strType == "zframebuffer")
            {
                return std::shared_ptr<render_frame>(new ri_zframebuffer_render_frame(strName.c_str(), strType.c_str(), strMode.c_str(), width, height));
            }
            else
            {
                return std::shared_ptr<render_frame>(new ri_framebuffer_render_frame(strName.c_str(), strType.c_str(), strMode.c_str(), width, height));
            }
        }
        return std::shared_ptr<render_frame>();
    }

    std::shared_ptr<render_frame> ri_camera_options_converter::convert_render_frame(const ri_camera_options* pCmr)
    {
        size_t sz = pCmr->get_display_size();
        if (sz)
        {
            std::vector<std::shared_ptr<render_frame> > vFR;
            int idx = -1;
            for (size_t i = 0; i < sz; i++)
            {
                const ri_display* pDisp = pCmr->get_display_at(i);
                std::string strType = pDisp->get_type();
                if (strType == "framebuffer-")
                {
                    idx = i;
                }
                else if (strType == "framebuffer" || strType == "zframebuffer")
                {
                    idx = -1;
                }

                std::shared_ptr<render_frame> rf = ConvertRenderFrame(pCmr, pDisp);

                if (rf.get())
                {
                    vFR.push_back(rf);
                }
            }

            if (0 <= idx)
            {
                std::shared_ptr<render_frame> rf = ConvertRenderFrame(pCmr, pCmr->get_display_at(idx), true);

                if (rf.get())
                {
                    vFR.push_back(rf);
                }
            }

            if (vFR.size())
            {
                if (vFR.size() == 1)
                {
                    return vFR[0];
                }
                else
                {
                    std::unique_ptr<composite_render_frame> ap(new composite_render_frame());
                    for (size_t i = 0; i < vFR.size(); i++)
                    {
                        ap->add(vFR[i]);
                    }
                    return std::shared_ptr<render_frame>(ap.release());
                }
            }
        }
        return std::shared_ptr<render_frame>();
    }

    static bool IsContain(const char* szMode, const char* szKey)
    {
        const char* szFind = strstr(szMode, szKey);
        if (szFind)
            return true;
        else
            return false;
    }

    bool ri_camera_options_converter::require_channel(const ri_camera_options* pCmr, const char* szChannel) const
    {
        if (strcmp(szChannel, "z") == 0) return require_z(pCmr);
        size_t sz = pCmr->get_display_size();
        for (size_t i = 0; i < sz; i++)
        {
            const ri_display* pDisp = pCmr->get_display_at(i);
            std::string strType = pDisp->get_type();
            std::string strMode = pDisp->get_mode();
            if (IsContain(strMode.c_str(), szChannel))
            {
                return true;
            }
        }
        return false;
    }

    bool ri_camera_options_converter::require_r(const ri_camera_options* pCmr) const
    {
        return require_channel(pCmr, "r");
    }

    bool ri_camera_options_converter::require_g(const ri_camera_options* pCmr) const
    {
        return require_channel(pCmr, "g");
    }

    bool ri_camera_options_converter::require_b(const ri_camera_options* pCmr) const
    {
        return require_channel(pCmr, "b");
    }

    bool ri_camera_options_converter::require_a(const ri_camera_options* pCmr) const
    {
        return require_channel(pCmr, "a");
    }

    bool ri_camera_options_converter::require_z(const ri_camera_options* pCmr) const
    {
        size_t sz = pCmr->get_display_size();
        for (size_t i = 0; i < sz; i++)
        {
            const ri_display* pDisp = pCmr->get_display_at(i);
            std::string strType = pDisp->get_type();
            std::string strMode = pDisp->get_mode();
            if (strType == "zfile" || strType == "z")
            {
                return true;
            }
            if (IsContain(strMode.c_str(), "z"))
            {
                return true;
            }
        }
        return false;
    }

    bool ri_camera_options_converter::require_color(const ri_camera_options* pCmr) const
    {
        return require_r(pCmr) || require_g(pCmr) || require_b(pCmr);
    }
}
