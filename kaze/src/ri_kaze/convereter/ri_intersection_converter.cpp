#include "ri_intersection_converter.h"

#include "ri_sphere_intersection.h"
#include "ri_cone_intersection.h"
#include "ri_cylinder_intersection.h"
#include "ri_disk_intersection.h"
#include "ri_torus_intersection.h"
#include "ri_hyperboloid_intersection.h"
#include "ri_paraboloid_intersection.h"
#include "ri_points_intersection.h"
//include "ri_blobby_intersection.h"
#include "ri_subdivision_mesh_intersection.h"
#include "ri_nu_patch_intersection.h"

#include "ri_points_triangle_polygons_intersection.h"
#include "ri_bezier_patch_mesh_intersection.h"
#include "ri_bezier_curves_intersection.h"

#include "ri_file_mesh_intersection.h"
#include "ri_structure_synth_intersection.h"

#include "ri_motion_intersection.h"
#include "ri_motion_sphere_intersection.h"
#include "ri_motion_cylinder_intersection.h"
#include "ri_motion_cone_intersection.h"
#include "ri_motion_disk_intersection.h"
#include "ri_motion_paraboloid_intersection.h"
#include "ri_motion_hyperboloid_intersection.h"
#include "ri_motion_points_triangle_polygons_intersection.h"
#include "ri_motion_points_intersection.h"
#include "ri_motion_bezier_patch_mesh_intersection.h"

#include "ri_teapot_intersection.h"
#include "ri_bunny_intersection.h"

#include "ri_structure_synth_converter.h"

#include "bvh_composite_intersection.h"
#include "transformed_intersection.h"
#include "shadered_intersection.h"

//-----------------------------------
#include "ri_camera_options.h"
//-----------------------------------

#include "ri_blobby.h"
#include "ri_evaluate_blobby.h"
#include "ri_polygonize_blobby.h"

#include "ri_subdivide_mesh.h"

#include <stdio.h>
#include <memory>
#include <iostream>

namespace ri
{
    using namespace kaze;

    static matrix4 ConvertMatrix(const ri_transform& trns)
    {
        ri_transform s(trns);
        s.transpose();
        const float* f = s.get();
        matrix4 tmp(f);
        return tmp;
    }

    //--------------------------------------------------------------------------------------

    std::shared_ptr<intersection> ri_intersection_converter::convert(const ri_geometry* pGeo)
    {
        if (cache_.size() >= 256) cache_.clear();
        return convert_attributed_geometry(convert_geometry_to_geometry(pGeo));
    }
    //--------------------------------------------------------------------------------------
    const ri_geometry* ri_intersection_converter::convert_geometry_to_geometry(const ri_geometry* pGeo)
    {
        return ggc_.convert(pGeo);
    }
    //--------------------------------------------------------------------------------------
    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_geometry* pGeo)
    {
        return convert_geometry(pGeo, pGeo->get_transform());
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_geometry* pGeo, const ri_transform& trns)
    {
        if (!trns.is_motion())
        {
            int nType = pGeo->typeN();
            switch (nType)
            {
            case RI_GEOMETRY_POLYGON:
                return convert_geometry(dynamic_cast<const ri_polygon_geometry*>(pGeo), trns);
            case RI_GEOMETRY_GENERAL_POLYGON:
                return convert_geometry(dynamic_cast<const ri_general_polygon_geometry*>(pGeo), trns);
            case RI_GEOMETRY_POINTS_POLYGONS:
                return convert_geometry(dynamic_cast<const ri_points_polygons_geometry*>(pGeo), trns);
            case RI_GEOMETRY_POINTS_GENERAL_POLYGONS:
                return convert_geometry(dynamic_cast<const ri_points_general_polygons_geometry*>(pGeo), trns);
            case RI_GEOMETRY_PATCH:
                return convert_geometry(dynamic_cast<const ri_patch_geometry*>(pGeo), trns);
            case RI_GEOMETRY_PATCH_MESH:
                return convert_geometry(dynamic_cast<const ri_patch_mesh_geometry*>(pGeo), trns);
            case RI_GEOMETRY_NU_PATCH:
                return convert_geometry(dynamic_cast<const ri_nu_patch_geometry*>(pGeo), trns);
            case RI_GEOMETRY_TRIM_CURVE:
                return convert_geometry(dynamic_cast<const ri_trim_curve_geometry*>(pGeo), trns);
            case RI_GEOMETRY_SPHERE:
                return convert_geometry(dynamic_cast<const ri_sphere_geometry*>(pGeo), trns);
            case RI_GEOMETRY_CONE:
                return convert_geometry(dynamic_cast<const ri_cone_geometry*>(pGeo), trns);
            case RI_GEOMETRY_CYLINDER:
                return convert_geometry(dynamic_cast<const ri_cylinder_geometry*>(pGeo), trns);
            case RI_GEOMETRY_HYPERBOLOID:
                return convert_geometry(dynamic_cast<const ri_hyperboloid_geometry*>(pGeo), trns);
            case RI_GEOMETRY_PARABOLOID:
                return convert_geometry(dynamic_cast<const ri_paraboloid_geometry*>(pGeo), trns);
            case RI_GEOMETRY_DISK:
                return convert_geometry(dynamic_cast<const ri_disk_geometry*>(pGeo), trns);
            case RI_GEOMETRY_TORUS:
                return convert_geometry(dynamic_cast<const ri_torus_geometry*>(pGeo), trns);
            case RI_GEOMETRY_BLOBBY:
                return convert_geometry(dynamic_cast<const ri_blobby_geometry*>(pGeo), trns);
            case RI_GEOMETRY_CURVES:
                return convert_geometry(dynamic_cast<const ri_curves_geometry*>(pGeo), trns);
            case RI_GEOMETRY_POINTS:
                return convert_geometry(dynamic_cast<const ri_points_geometry*>(pGeo), trns);
            case RI_GEOMETRY_SUBDIVISION_MESH:
                return convert_geometry(dynamic_cast<const ri_subdivision_mesh_geometry*>(pGeo), trns);
            case RI_GEOMETRY_PROCEDURAL:
                return convert_geometry(dynamic_cast<const ri_procedural_geometry*>(pGeo), trns);
            case RI_GEOMETRY_GROUP:
                return convert_geometry(dynamic_cast<const ri_group_geometry*>(pGeo), trns);
            case RI_GEOMETRY_SOLID:
                return convert_geometry(dynamic_cast<const ri_solid_geometry*>(pGeo), trns);
            case RI_GEOMETRY_OBJECT_INSTANCE:
                return convert_geometry(dynamic_cast<const ri_group_geometry*>(pGeo), trns);
            case RI_GEOMETRY_BUNNY:
                return convert_geometry(dynamic_cast<const ri_bunny_geometry*>(pGeo), trns);
            case RI_GEOMETRY_TEAPOT:
                return convert_geometry(dynamic_cast<const ri_teapot_geometry*>(pGeo), trns);
            /**/
            case RI_GEOMETRY_BEZIER_PATCH_MESH:
                return convert_geometry(dynamic_cast<const ri_bezier_patch_mesh_geometry*>(pGeo), trns);
            case RI_GEOMETRY_POINTS_TRIANGLE_POLYGONS:
                return convert_geometry(dynamic_cast<const ri_points_triangle_polygons_geometry*>(pGeo), trns);
            case RI_GEOMETRY_RROCEDURAL_EVALUATION:
                return convert_geometry(dynamic_cast<const ri_procedural_evaluation_geometry*>(pGeo), trns);
            case RI_GEOMETRY_BEZIER_CURVES:
                return convert_geometry(dynamic_cast<const ri_bezier_curves_geometry*>(pGeo), trns);
            case RI_GEOMETRY_FILE_MESH:
                return convert_geometry(dynamic_cast<const ri_file_mesh_geometry*>(pGeo), trns);
            case RI_GEOMETRY_STRUCTURE_SYNTH:
                return convert_geometry(dynamic_cast<const ri_structure_synth_geometry*>(pGeo), trns);
            case RI_GEOMETRY_MOTION:
                return convert_geometry(dynamic_cast<const ri_motion_geometry*>(pGeo), trns);
            }
            return std::shared_ptr<intersection>();
        }
        else
        {
            static ri_transform trans1;
            size_t sz = trns.get_size(); //
            if (sz)
            {
                typedef ri_motion_intersection::sequence_t sequence_t;
                std::vector<sequence_t> seq;
                for (size_t i = 0; i < sz; i++)
                {
                    sequence_t tmp;
                    tmp.mat = ConvertMatrix(ri_transform(trns.get_mat_at(i)));
                    tmp.t = trns.get_time_at(i);
                    seq.push_back(tmp);
                }
                return std::shared_ptr<intersection>(new ri_motion_intersection(seq, convert_geometry(pGeo, trans1)));
            }
            return std::shared_ptr<intersection>();
        }
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_polygon_geometry* pGeo, const ri_transform& trns)
    {
        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_general_polygon_geometry* pGeo, const ri_transform& trns)
    {
        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_points_polygons_geometry* pGeo, const ri_transform& trns)
    {
        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_points_general_polygons_geometry* pGeo, const ri_transform& trns)
    {
        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_patch_geometry* pGeo, const ri_transform& trns)
    {
        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_patch_mesh_geometry* pGeo, const ri_transform& trns)
    {
        return std::shared_ptr<intersection>();
    }

    static ri_nu_patch_trim_curve GetTrimCurve(int n, int order, const float knot[], float min, float max, const float u[], const float v[], const float w[], bool bCompatibleRMAN = true)
    {
        if (min > max) std::swap(min, max);
        if (min < 0) min = 0;
        if (1 < max) max = 1;
        std::vector<vector2> uv_(n);
        std::vector<real> w_(n);
        if (bCompatibleRMAN)
        {
            for (int i = 0; i < n; i++)
            {
                uv_[i] = vector2(u[i] / w[i], v[i] / w[i]); //OH
                w_[i] = (real)w[i];
            }
        }
        else
        {
            for (int i = 0; i < n; i++)
            {
                uv_[i] = vector2(u[i], v[i]);
                w_[i] = (real)w[i];
            }
        }

        std::vector<real> knot_(n + order);
        for (int i = 0; i < n + order; i++)
        {
            knot_[i] = knot[i];
        }

        ri_nu_patch_trim_curve crv(uv_, w_, knot_);
        if (0 < min || max < 1)
        { //trim
            //crv.split();
        }
        return crv;
    }

    static void GetTrimLoops(std::vector<ri_nu_patch_trim_loop>& loops, int nloops, const int ncurves[], const int order[], const float knot[], const float min[], const float max[], const int n[], const float u[], const float v[], const float w[], bool bCompatibleRMAN = true)
    {
        int crvOffset = 0;
        int cpOffset = 0;
        int knotOffset = 0;
        for (int i = 0; i < nloops; i++)
        {
            ri_nu_patch_trim_loop loop;
            int nCurves = ncurves[i];
            for (int j = 0; j < nCurves; j++)
            {
                int n_ = n[crvOffset + j];
                int order_ = order[crvOffset + j];
                float min_ = min[crvOffset + j];
                float max_ = max[crvOffset + j];

                const float* knot_ = knot + knotOffset;
                const float* u_ = u + cpOffset;
                const float* v_ = v + cpOffset;
                const float* w_ = w + cpOffset;

                ri_nu_patch_trim_curve crv = GetTrimCurve(n_, order_, knot_, min_, max_, u_, v_, w_, bCompatibleRMAN);
                loop.push_back(crv);

                cpOffset += n_;
                knotOffset += (n_ + order_);
            }
            loops.push_back(loop);
            crvOffset += nCurves;
        }
    }

    static void GetTrimLoops(std::vector<ri_nu_patch_trim_loop>& loops, const ri_trim_curve_geometry* crv)
    {
        bool bCompatibleRMAN = true;
        if (crv->is_noweight_cp())
        {
            bCompatibleRMAN = false;
        }
        GetTrimLoops(loops, crv->get_nloops(), crv->get_ncurves(), crv->get_order(), crv->get_knot(), crv->get_min(), crv->get_max(), crv->get_n(), crv->get_u(), crv->get_v(), crv->get_w(), bCompatibleRMAN);
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_nu_patch_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        int nu = pGeo->get_nu();
        int nv = pGeo->get_nv();
        int uorder = pGeo->get_uorder();
        int vorder = pGeo->get_vorder();
        const std::vector<float>& uknot = pGeo->get_uknot();
        const std::vector<float>& vknot = pGeo->get_vknot();
        float umin = pGeo->get_umin();
        float umax = pGeo->get_umax();
        float vmin = pGeo->get_vmin();
        float vmax = pGeo->get_vmax();

        const float* Pw = pGeo->get_Pw();
        const float* P = pGeo->get_P();

        bool bCompatibleRMAN = true;
        if (pGeo->is_noweight_cp())
        {
            bCompatibleRMAN = false;
        }

        std::vector<float> Pwt;
        if (Pw)
        {
            if (bCompatibleRMAN)
            {
                Pwt.resize(nu * nv * 4);
                for (int i = 0; i < nu * nv; i++)
                {
                    float w = Pw[4 * i + 3];
                    Pwt[4 * i + 0] = Pw[4 * i + 0] / w;
                    Pwt[4 * i + 1] = Pw[4 * i + 1] / w;
                    Pwt[4 * i + 2] = Pw[4 * i + 2] / w;
                    Pwt[4 * i + 3] = w;
                }
                Pw = &Pwt[0];
            }
        }
        else if (P)
        {
            Pwt.resize(nu * nv * 4);
            for (int i = 0; i < nu * nv; i++)
            {
                Pwt[4 * i + 0] = P[3 * i + 0];
                Pwt[4 * i + 1] = P[3 * i + 1];
                Pwt[4 * i + 2] = P[3 * i + 2];
                Pwt[4 * i + 3] = 1.0f;
            }
            Pw = &Pwt[0];
        }

        std::vector<const ri_trim_curve_geometry*> lv;
        pGeo->get_attributes().get_trim_curve(lv);

        std::vector<ri_nu_patch_trim_loop> outers;
        std::vector<ri_nu_patch_trim_loop> inners;

        if (!lv.empty())
        {
            for (size_t i = 0; i < lv.size(); i++)
            {
                const ri_trim_curve_geometry* crv = lv[i];
                if (crv->is_outer())
                { //inside
                    GetTrimLoops(outers, crv);
                }
                else
                {
                    GetTrimLoops(inners, crv);
                }
            }
        }
        return std::shared_ptr<intersection>(new ri_nu_patch_intersection(nu, uorder, &uknot[0], umin, umax, nv, vorder, &vknot[0], vmin, vmax, Pw, outers, inners, mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_sphere_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        float radius = pGeo->get_radius();
        float zmin = pGeo->get_zmin();
        float zmax = pGeo->get_zmax();
        float tmax = pGeo->get_tmax();
        return std::shared_ptr<intersection>(new ri_sphere_intersection(radius, zmin, zmax, tmax, mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_cone_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        float height = pGeo->get_height();
        float radius = pGeo->get_radius();
        float tmax = pGeo->get_tmax();
        return std::shared_ptr<intersection>(new ri_cone_intersection(height, radius, tmax, mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_cylinder_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        float radius = pGeo->get_radius();
        float zmin = pGeo->get_zmin();
        float zmax = pGeo->get_zmax();
        float tmax = pGeo->get_tmax();
        return std::shared_ptr<intersection>(new ri_cylinder_intersection(radius, zmin, zmax, tmax, mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_disk_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        float height = pGeo->get_height();
        float radius = pGeo->get_radius();
        float tmax = pGeo->get_tmax();
        return std::shared_ptr<intersection>(new ri_disk_intersection(height, radius, tmax, mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_torus_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        float majrad = pGeo->get_majrad();
        float minrad = pGeo->get_minrad();
        float phimin = pGeo->get_phimin();
        float phimax = pGeo->get_phimax();
        float tmax = pGeo->get_tmax();
        return std::shared_ptr<intersection>(new ri_torus_intersection(majrad, minrad, phimin, phimax, tmax, mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_hyperboloid_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        vector3 point1(pGeo->get_point1());
        vector3 point2(pGeo->get_point2());
        float tmax = pGeo->get_tmax();
        return std::shared_ptr<intersection>(new ri_hyperboloid_intersection(point1, point2, tmax, mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_paraboloid_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        float rmax = pGeo->get_rmax();
        float zmin = pGeo->get_zmin();
        float zmax = pGeo->get_zmax();
        float tmax = pGeo->get_tmax();
        return std::shared_ptr<intersection>(new ri_paraboloid_intersection(rmax, zmin, zmax, tmax, mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_blobby_geometry* pGeo, const ri_transform& trns)
    {
        int nleaf = pGeo->get_nleaf();
        int ncode = pGeo->get_ncode();
        const int* code = pGeo->get_code();
        int nflt = pGeo->get_nflt();
        const float* flt = pGeo->get_flt();
        int nstr = pGeo->get_nstr();
        const std::string* str = pGeo->get_str();
        float threthold = pGeo->get_threthold();
        std::shared_ptr<ri_blobby> ap = ri_attributes_blobby(ri_optimize_blobby(ri_evaluate_blobby(nleaf, ncode, code, nflt, flt, nstr, str)), pGeo->get_classifier(), pGeo->get_parameters());

#if 1
        std::shared_ptr<ri_blobby_polygon> bp;

        matrix4 l2w = mat4_gen::identity();
        const ri_camera_options* pCamera = pCamera_;

        if (pCamera)
        {
            bp = ri_polygonize_blobby(ap.get(), trns, *pCamera, threthold);
        }
        else
        {
            return std::shared_ptr<intersection>();
        }

        if (bp.get() && bp->indices.size())
        {
            int nfaces = bp->indices.size() / 3;
            int nverts = bp->vertices.size() / 3;
            const int* verts = &bp->indices[0];
            const float* P = &bp->vertices[0];
            const float* N = NULL; //&bp->normals[0];
            //const float* Np = NULL;
            //const float* Nf = NULL;

            if (bp->normals.size())
            {
                N = &bp->normals[0];
            }

            //bool bSmooth = false;
            //real smooth_angle = 0;
            std::shared_ptr<intersection> inter = std::shared_ptr<intersection>(new ri_points_triangle_polygons_intersection(nfaces, verts, nverts, P, N, NULL, NULL, 180, l2w));
            if (inter.get())
            {
                if (ac_.get())
                {
                    std::unique_ptr<ri_geometry> ag(new ri_blobby_mesh_geometry(pGeo, bp->indices, bp->vertices, bp->cls, bp->params));
                    std::shared_ptr<shader> shd(ac_->convert(&ag->get_attributes(), ag.get()));
                    if (shd.get())
                    {
                        return std::shared_ptr<intersection>(new shadered_intersection(inter, shd));
                    }
                }
            }
            return inter;
        }
#else
        if(ap.get())
        {
            std::shared_ptr<blobby_node> node = ConvertBlobbyNode(ap);
            if(node.get())
            {
                matrix4 mat = ConvertMatrix(trns);
                return std::shared_ptr<intersection>( new ri_blobby_intersection(node, mat) );
            }
        }
#endif
        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_curves_geometry* pGeo, const ri_transform& trns)
    {
        return std::shared_ptr<intersection>();
    }

    static inline vector3 MulNormal(const matrix4& m, const vector3& v)
    {
        return vector3(
            m[0][0] * v[0] + m[1][0] * v[1] + m[2][0] * v[2],
            m[0][1] * v[0] + m[1][1] * v[1] + m[2][1] * v[2],
            m[0][2] * v[0] + m[1][2] * v[1] + m[2][2] * v[2]);
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_points_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        int nverts = pGeo->get_nverts();
        const float* P = pGeo->get_P();
        const float* width = pGeo->get_width();
        const float* Cs = pGeo->get_Cs();
        const float* Os = pGeo->get_Os();
        float constantwidth = pGeo->get_constantwidth();

        const float* N = pGeo->get_N();
        const float* constantnormal = pGeo->get_constantnormal();

        const char* type = pGeo->get_type();

        float camera_normal[3] = {0, 0, 1};
        if (!constantnormal)
        { //form camera
            const ri_camera_options* pCamera = pCamera_;
            if (pCamera)
            {
                matrix4 c2w = ConvertMatrix(pCamera->get_camera_to_world());
                matrix4 w2l = ~mat;
                matrix4 c2l = w2l * c2w;
                matrix4 c2ln = ~c2l;

                vector3 cd = normalize(MulNormal(c2ln, vector3(0, 0, 1))); //camera direction;
                vector3 cn = -cd;
                camera_normal[0] = (float)cn[0];
                camera_normal[1] = (float)cn[1];
                camera_normal[2] = (float)cn[2];

                constantnormal = camera_normal;
            }
        }

        return std::shared_ptr<intersection>(new ri_points_intersection(nverts, P, width, Cs, Os, constantwidth, N, constantnormal, type, mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_subdivision_mesh_geometry* pGeo, const ri_transform& trns)
    {
        std::unique_ptr<ri_points_polygons_geometry> ap(ri_subdivide_mesh(pGeo));
        const ri_geometry* pGeo2 = ggc_.convert(ap.get());
        return this->convert_geometry(pGeo2, trns);
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_teapot_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        return std::shared_ptr<intersection>(new ri_teapot_intersection(mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_bunny_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        return std::shared_ptr<intersection>(new ri_bunny_intersection(mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_group_geometry* pGeo, const ri_transform& trns)
    {
        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_solid_geometry* pGeo, const ri_transform& trns)
    {
        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_object_instance_geometry* pGeo, const ri_transform& trns)
    {
        static ri_transform trans1;

        const ri_geometry* pGeo2 = convert_geometry_to_geometry(pGeo->get_geometry());
        std::shared_ptr<intersection> inter(convert_geometry(pGeo2, trans1));
        if (inter.get())
        {
            if (ac_.get())
            {
                std::shared_ptr<shader> shd(ac_->convert(&(pGeo->get_attributes()), pGeo2));
                if (shd.get())
                {
                    matrix4 mat = ConvertMatrix(trns);
                    return std::shared_ptr<intersection>(
                        new m4_transformed_intersection(
                            new shadered_intersection(inter, shd), mat));
                }
            }
            else
            {
                matrix4 mat = ConvertMatrix(pGeo->get_transform());
                return std::shared_ptr<intersection>(
                    new m4_transformed_intersection(
                        inter, mat));
            }
        }
        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_bezier_patch_mesh_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        int nu = pGeo->get_nu();
        int nv = pGeo->get_nv();
        int uorder = pGeo->get_uorder();
        int vorder = pGeo->get_vorder();
        const float* P = pGeo->get_P();

        return std::shared_ptr<intersection>(new ri_bezier_patch_mesh_intersection(nu, uorder, nv, vorder, P, mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_points_triangle_polygons_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        int nfaces = pGeo->get_nfaces();
        const int* verts = pGeo->get_verts();
        int nverts = pGeo->get_nverts();
        const float* P = pGeo->get_P();
        const float* N = pGeo->get_N();
        const float* Np = pGeo->get_Np();
        const float* Nf = pGeo->get_Nf();

        bool bSmooth = false;
        float smooth_angle = 30.0f;
        {
            std::string type;
            if (pGeo->get_attributes().get("ShadingInterpolation", "type", type))
            {
                if (type == RI_CONSTANT)
                    bSmooth = false;
                else
                    bSmooth = true;
            }
        }
        {
            if (bSmooth)
            {
                float t = smooth_angle;
                if (pGeo->get_attributes().get("ShadingInterpolation", "smoothangle", t))
                {
                    if (0.0f <= t && t <= 360.0f)
                    {
                        smooth_angle = t;
                    }
                }
            }
            else
            {
                smooth_angle = 0;
            }
        }

        return std::shared_ptr<intersection>(new ri_points_triangle_polygons_intersection(nfaces, verts, nverts, P, N, Np, Nf, smooth_angle, mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_procedural_evaluation_geometry* pGeo, const ri_transform& trns)
    {
        size_t sz = pGeo->get_size();
        if (sz)
        {
            std::unique_ptr<composite_intersection> ap(new bvh_composite_intersection());
            for (size_t i = 0; i < pGeo->get_size(); i++)
            {
                std::shared_ptr<intersection> cld(convert_geometry(pGeo->get_at(i)));
                if (cld.get())
                {
                    ap->add(cld);
                }
            }
            ap->construct();
            return std::shared_ptr<intersection>(ap.release());
        }
        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_bezier_curves_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);
        int ncurves = pGeo->get_ncurves();
        const int* nvertices = pGeo->get_nvertices();
        int order = pGeo->get_order();

        const float* P = pGeo->get_P();
        const float* N = pGeo->get_N();

        const float* width = pGeo->get_width();
        const float* Cs = pGeo->get_Cs();
        const float* Os = pGeo->get_Os();

        const float* widthv = pGeo->get_widthv();
        const float* Csv = pGeo->get_Csv();
        const float* Osv = pGeo->get_Osv();

        const float* Nv = pGeo->get_Nv();

        float constantwidth = pGeo->get_constantwidth();
        const float* constantnormal = pGeo->get_constantnormal();

        if (!P) return std::shared_ptr<intersection>();

        float camera_normal[3] = {0, 0, 1};
        if (!constantnormal)
        { //form camera
            const ri_camera_options* pCamera = pCamera_;
            if (pCamera)
            {
                matrix4 c2w = ConvertMatrix(pCamera->get_camera_to_world());
                matrix4 w2l = ~mat;
                matrix4 c2l = w2l * c2w;
                matrix4 c2ln = ~c2l;

                vector3 cd = normalize(MulNormal(c2ln, vector3(0, 0, 1))); //camera direction;
                vector3 cn = -cd;
                camera_normal[0] = (float)cn[0];
                camera_normal[1] = (float)cn[1];
                camera_normal[2] = (float)cn[2];

                constantnormal = camera_normal;
            }
        }

        return std::shared_ptr<intersection>(
            new ri_bezier_curves_intersection(
                ncurves, nvertices, order,
                P, N, width, Cs, Os, widthv, Csv, Osv, Nv, constantwidth, constantnormal,
                mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_file_mesh_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);

        std::string type = pGeo->get_type();
        std::string path = pGeo->get_full_path();
        if (ri_file_mesh_intersection::is_valid(path.c_str()))
        {
            return std::shared_ptr<intersection>(new ri_file_mesh_intersection(type.c_str(), path.c_str(), mat));
        }
        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_structure_synth_geometry* pGeo, const ri_transform& trns)
    {
        matrix4 mat = ConvertMatrix(trns);

        std::string path = pGeo->get_full_path();
        if (ri_structure_synth_intersection::is_valid(path.c_str()))
        {
            ri_structure_synth_converter sscv;
            std::shared_ptr<intersection> inter = sscv.convert(pGeo);
            if (inter.get())
            {
                return std::shared_ptr<intersection>(new m4_transformed_intersection(
                    inter, mat));
            }
        }
        return std::shared_ptr<intersection>();
    }

    //-------------------------------------------------------------------------------------------------------------------------

    static int GetGeometryTypeN(const std::vector<const ri_geometry*>& geos)
    {
        int nType = geos[0]->typeN();
        for (size_t i = 1; i < geos.size(); i++)
        {
            if (nType != geos[i]->typeN()) return -1;
        }
        return nType;
    }

    static std::vector<float> CopyFloats(const float* P, size_t sz)
    {
        std::vector<float> tmp;
        if (!P) return tmp;
        tmp.resize(sz);
        memcpy(&tmp[0], P, sizeof(float) * sz);
        return tmp;
    }

    static std::shared_ptr<intersection> ConvertMotionSphere(const float times[], const std::vector<const ri_geometry*>& geos, const matrix4& mat)
    {
        typedef ri_motion_sphere_intersection::sequence_t sequence_t;
        std::vector<sequence_t> seq;
        for (size_t i = 0; i < geos.size(); i++)
        {
            const ri_sphere_geometry* g = dynamic_cast<const ri_sphere_geometry*>(geos[i]);
            sequence_t s;
            s.t = times[i];
            s.radius = g->get_radius();
            s.zmin = g->get_zmin();
            s.zmax = g->get_zmax();
            s.tmax = g->get_tmax();
            seq.push_back(s);
        }
        return std::shared_ptr<intersection>(new ri_motion_sphere_intersection(seq, mat));
    }

    static std::shared_ptr<intersection> ConvertMotionCylinder(const float times[], const std::vector<const ri_geometry*>& geos, const matrix4& mat)
    {
        typedef ri_motion_cylinder_intersection::sequence_t sequence_t;
        std::vector<sequence_t> seq;
        for (size_t i = 0; i < geos.size(); i++)
        {
            const ri_cylinder_geometry* g = dynamic_cast<const ri_cylinder_geometry*>(geos[i]);
            sequence_t s;
            s.t = times[i];
            s.radius = g->get_radius();
            s.zmin = g->get_zmin();
            s.zmax = g->get_zmax();
            s.tmax = g->get_tmax();
            seq.push_back(s);
        }
        return std::shared_ptr<intersection>(new ri_motion_cylinder_intersection(seq, mat));
    }

    static std::shared_ptr<intersection> ConvertMotionCone(const float times[], const std::vector<const ri_geometry*>& geos, const matrix4& mat)
    {
        typedef ri_motion_cone_intersection::sequence_t sequence_t;
        std::vector<sequence_t> seq;
        for (size_t i = 0; i < geos.size(); i++)
        {
            const ri_cone_geometry* g = dynamic_cast<const ri_cone_geometry*>(geos[i]);
            sequence_t s;
            s.t = times[i];
            s.radius = g->get_radius();
            s.height = g->get_height();
            s.radius = g->get_radius();
            s.tmax = g->get_tmax();
            seq.push_back(s);
        }
        return std::shared_ptr<intersection>(new ri_motion_cone_intersection(seq, mat));
    }

    static std::shared_ptr<intersection> ConvertMotionDisk(const float times[], const std::vector<const ri_geometry*>& geos, const matrix4& mat)
    {
        typedef ri_motion_disk_intersection::sequence_t sequence_t;
        std::vector<sequence_t> seq;
        for (size_t i = 0; i < geos.size(); i++)
        {
            const ri_disk_geometry* g = dynamic_cast<const ri_disk_geometry*>(geos[i]);
            sequence_t s;
            s.t = times[i];
            s.height = g->get_height();
            s.radius = g->get_radius();
            s.tmax = g->get_tmax();
            seq.push_back(s);
        }
        return std::shared_ptr<intersection>(new ri_motion_disk_intersection(seq, mat));
    }

    static std::shared_ptr<intersection> ConvertMotionParaboloid(const float times[], const std::vector<const ri_geometry*>& geos, const matrix4& mat)
    {
        typedef ri_motion_paraboloid_intersection::sequence_t sequence_t;
        std::vector<sequence_t> seq;
        for (size_t i = 0; i < geos.size(); i++)
        {
            const ri_paraboloid_geometry* g = dynamic_cast<const ri_paraboloid_geometry*>(geos[i]);
            sequence_t s;
            s.t = times[i];
            s.rmax = g->get_rmax();
            s.zmin = g->get_zmin();
            s.zmax = g->get_zmax();
            s.tmax = g->get_tmax();
            seq.push_back(s);
        }
        return std::shared_ptr<intersection>(new ri_motion_paraboloid_intersection(seq, mat));
    }

    static std::shared_ptr<intersection> ConvertMotionHyperboloid(const float times[], const std::vector<const ri_geometry*>& geos, const matrix4& mat)
    {
        typedef ri_motion_hyperboloid_intersection::sequence_t sequence_t;
        std::vector<sequence_t> seq;
        for (size_t i = 0; i < geos.size(); i++)
        {
            const ri_hyperboloid_geometry* g = dynamic_cast<const ri_hyperboloid_geometry*>(geos[i]);
            sequence_t s;
            s.t = times[i];
            s.point1 = vector3(g->get_point1());
            s.point2 = vector3(g->get_point2());
            s.tmax = g->get_tmax();
            seq.push_back(s);
        }
        return std::shared_ptr<intersection>(new ri_motion_hyperboloid_intersection(seq, mat));
    }

    static std::shared_ptr<intersection> ConvertMotionPointsTrianglePolygons(const float times[], const std::vector<const ri_geometry*>& geos, const matrix4& mat)
    {
        size_t gsz = geos.size();

        typedef ri_motion_points_triangle_polygons_intersection::sequence_t sequence_t;
        std::vector<sequence_t> seq;
        for (size_t i = 0; i < gsz; i++)
        {
            const ri_points_triangle_polygons_geometry* g = dynamic_cast<const ri_points_triangle_polygons_geometry*>(geos[i]);
            int nf = g->get_nfaces();
            int nv = g->get_nverts();
            sequence_t s;
            s.t = times[i];
            s.P = CopyFloats(g->get_P(), nv * 3);
            s.N = CopyFloats(g->get_N(), nv * 3);
            s.Np = CopyFloats(g->get_Np(), nf);
            s.Nf = CopyFloats(g->get_Nf(), nf * 3 * 3);
            seq.push_back(s);
        }
        { //check size
            size_t Psz = seq[0].P.size();
            size_t Nsz = seq[0].N.size();
            size_t Npsz = seq[0].Np.size();
            size_t Nfsz = seq[0].Nf.size();
            for (size_t i = 1; i < gsz; i++)
            {
                if (Psz != seq[i].P.size()) return std::shared_ptr<intersection>();
                if (Nsz != seq[i].N.size()) return std::shared_ptr<intersection>();
                if (Npsz != seq[i].Np.size()) return std::shared_ptr<intersection>();
                if (Nfsz != seq[i].Nf.size()) return std::shared_ptr<intersection>();
            }
        }
        const ri_points_triangle_polygons_geometry* g = dynamic_cast<const ri_points_triangle_polygons_geometry*>(geos[0]);
        int nfaces = g->get_nfaces();
        const int* verts = g->get_verts();
        int nverts = g->get_nverts();
        return std::shared_ptr<intersection>(new ri_motion_points_triangle_polygons_intersection(nfaces, verts, nverts, seq, mat));
    }

    std::shared_ptr<intersection> ConvertMotionPoints(const float times[], const std::vector<const ri_geometry*>& geos, const matrix4& mat, const ri_camera_options* pCamera)
    {
        size_t gsz = geos.size();

        typedef ri_motion_points_intersection::sequence_t sequence_t;

        float camera_normal[3] = {0, 0, 1};
        if (pCamera)
        {
            matrix4 c2w = ConvertMatrix(pCamera->get_camera_to_world());
            matrix4 w2l = mat;
            matrix4 c2l = w2l * c2w;
            matrix4 c2ln = ~c2l;

            vector3 cd = normalize(MulNormal(c2ln, vector3(0, 0, 1))); //camera direction;
            vector3 cn = -cd;
            camera_normal[0] = (float)cn[0];
            camera_normal[1] = (float)cn[1];
            camera_normal[2] = (float)cn[2];
        }

        std::vector<sequence_t> seq;
        for (size_t i = 0; i < gsz; i++)
        {
            const ri_points_geometry* g = dynamic_cast<const ri_points_geometry*>(geos[i]);
            int nv = g->get_nverts();
            sequence_t s;
            s.t = times[i];
            s.P = CopyFloats(g->get_P(), nv * 3);
            s.width = CopyFloats(g->get_width(), nv);
            s.N = CopyFloats(g->get_N(), nv * 3);
            s.Cs = CopyFloats(g->get_Cs(), nv * 3);
            s.Os = CopyFloats(g->get_Os(), nv * 3);
            s.constantwidth = g->get_constantwidth();

            const float* constantnormal = g->get_constantnormal();

            if (!constantnormal)
            {
                constantnormal = camera_normal;
            }

            s.constantnormal = CopyFloats(constantnormal, 3);

            seq.push_back(s);
        }
        { //check size
            /*
            size_t Psz  = seq[0].P.size();
            size_t Nsz  = seq[0].N.size();
            size_t Npsz = seq[0].Np.size();
            size_t Nfsz = seq[0].Nf.size();
            for(size_t i = 1;i<gsz;i++){
                if(Psz != seq[i].P.size())return std::shared_ptr<intersection>();
                if(Nsz != seq[i].N.size())return std::shared_ptr<intersection>();
                if(Npsz != seq[i].Np.size())return std::shared_ptr<intersection>();
                if(Nfsz != seq[i].Nf.size())return std::shared_ptr<intersection>();
            }
            */
        }
        const ri_points_geometry* g = dynamic_cast<const ri_points_geometry*>(geos[0]);
        int nverts = g->get_nverts();
        return std::shared_ptr<intersection>(new ri_motion_points_intersection(nverts, seq, g->get_type(), mat));
    }

    static std::shared_ptr<intersection> ConvertMotionBezeirPatchMesh(const float times[], const std::vector<const ri_geometry*>& geos, const matrix4& mat)
    {
        size_t gsz = geos.size();

        typedef ri_motion_bezier_patch_mesh_intersection::sequence_t sequence_t;

        int nu = 4;
        int nv = 4;
        int uorder = 4;
        int vorder = 4;
        {
            const ri_bezier_patch_mesh_geometry* g = dynamic_cast<const ri_bezier_patch_mesh_geometry*>(geos[0]);
            nu = g->get_nu();
            nv = g->get_nv();
            uorder = g->get_uorder();
            vorder = g->get_vorder();
        }

        std::vector<sequence_t> seq;
        for (size_t i = 0; i < gsz; i++)
        {
            const ri_bezier_patch_mesh_geometry* g = dynamic_cast<const ri_bezier_patch_mesh_geometry*>(geos[i]);
            sequence_t s;
            s.t = times[i];
            s.P = CopyFloats(g->get_P(), nu * nv * 3);
            seq.push_back(s);
        }

        return std::shared_ptr<intersection>(new ri_motion_bezier_patch_mesh_intersection(nu, uorder, nv, vorder, seq, mat));
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_geometry(const ri_motion_geometry* pGeo, const ri_transform& trns)
    {
        size_t n = pGeo->get_n();
        if (n)
        {
            matrix4 mat = ConvertMatrix(trns);
            std::vector<const ri_geometry*> geos(n);
            for (size_t i = 0; i < n; i++)
            {
                geos[i] = convert_geometry_to_geometry(pGeo->get_at(i));
            }

            std::shared_ptr<intersection> cld;
            int nType = GetGeometryTypeN(geos);
            switch (nType)
            {
            case RI_GEOMETRY_SPHERE:
                cld = ConvertMotionSphere(pGeo->get_times(), geos, mat);
                break;
            case RI_GEOMETRY_CYLINDER:
                cld = ConvertMotionCylinder(pGeo->get_times(), geos, mat);
                break;
            case RI_GEOMETRY_CONE:
                cld = ConvertMotionCone(pGeo->get_times(), geos, mat);
                break;
            case RI_GEOMETRY_DISK:
                cld = ConvertMotionDisk(pGeo->get_times(), geos, mat);
                break;
            case RI_GEOMETRY_PARABOLOID:
                cld = ConvertMotionParaboloid(pGeo->get_times(), geos, mat);
                break;
            case RI_GEOMETRY_HYPERBOLOID:
                cld = ConvertMotionHyperboloid(pGeo->get_times(), geos, mat);
                break;
            case RI_GEOMETRY_POINTS_TRIANGLE_POLYGONS:
                cld = ConvertMotionPointsTrianglePolygons(pGeo->get_times(), geos, mat);
                break;
            case RI_GEOMETRY_POINTS:
                cld = ConvertMotionPoints(pGeo->get_times(), geos, mat, pCamera_);
                break;
            case RI_GEOMETRY_BEZIER_PATCH_MESH:
                cld = ConvertMotionBezeirPatchMesh(pGeo->get_times(), geos, mat);
                break;
                //default:break;
            }
            return cld;
        }
        return std::shared_ptr<intersection>();
    }
    //-------------------------------------------------------------------------------------------------------------------------

    std::shared_ptr<intersection> ri_intersection_converter::convert_attributed_geometry(const ri_geometry* pGeo)
    {
        int nType = pGeo->typeN();
        switch (nType)
        {
        case RI_GEOMETRY_GROUP:
        case RI_GEOMETRY_RROCEDURAL_EVALUATION:
            return convert_attributed_geometry(dynamic_cast<const ri_group_geometry*>(pGeo));
        case RI_GEOMETRY_SOLID:
            return convert_attributed_geometry(dynamic_cast<const ri_solid_geometry*>(pGeo));
        case RI_GEOMETRY_OBJECT_INSTANCE:
            return convert_attributed_geometry(dynamic_cast<const ri_object_instance_geometry*>(pGeo));
        case RI_GEOMETRY_BLOBBY:
        case RI_GEOMETRY_STRUCTURE_SYNTH:
            return convert_geometry(pGeo);
        }
        return convert_attributed_geometry(&(pGeo->get_attributes()), pGeo);
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_attributed_geometry(const ri_attributes* pAttr, const ri_geometry* pGeo)
    {
        std::shared_ptr<intersection> inter(convert_geometry(pGeo));
        if (inter.get())
        {
            if (ac_.get())
            {
                std::shared_ptr<shader> shd(ac_->convert(pAttr, pGeo));
                if (shd.get())
                {
                    return std::shared_ptr<intersection>(new shadered_intersection(inter, shd));
                }
            }
        }
        return inter;
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_attributed_geometry(const ri_group_geometry* pGeo)
    {
        size_t gsz = pGeo->get_size();
        if (gsz > 1)
        {
            std::unique_ptr<composite_intersection> ap(new bvh_composite_intersection());
            for (size_t i = 0; i < pGeo->get_size(); i++)
            {
                std::shared_ptr<intersection> cld(convert(pGeo->get_at(i)));
                if (cld.get())
                {
                    ap->add(cld);
                }
            }
            ap->construct();
            std::shared_ptr<intersection> inter(ap.release());
            cache_.insert(map_type::value_type(pGeo, inter));
            return inter;
        }
        else if (gsz == 1)
        {
            return convert(pGeo->get_at(0));
        }
        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_attributed_geometry(const ri_solid_geometry* pGeo)
    {
        std::string operation = pGeo->get_operation();
        if (operation == "union" || operation == "primitive")
        {
            return convert_attributed_geometry(dynamic_cast<const ri_group_geometry*>(pGeo));
        }
        else
        {
            //std::cout << operation << std::endl;
        }

        return std::shared_ptr<intersection>();
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_attributed_geometry(const ri_object_instance_geometry* pGeo)
    {
        return convert_geometry(pGeo);
    }

    std::shared_ptr<intersection> ri_intersection_converter::convert_cached_geometry(const ri_geometry* pGeo)
    {
        map_type::iterator it = cache_.find(pGeo);
        if (it != cache_.end())
        {
            return it->second;
        }
        else
        {
            std::shared_ptr<intersection> inter(convert_geometry(pGeo));
            if (inter.get())
            {
                cache_.insert(map_type::value_type(pGeo, inter));
            }
            return inter;
        }
    }

    void ri_intersection_converter::set_shader_converter(const std::shared_ptr<ri_shader_converter>& ac)
    {
        ac_ = ac;
    }
}
