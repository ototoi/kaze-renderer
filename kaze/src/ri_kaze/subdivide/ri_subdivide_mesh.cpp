#include "ri_subdivide_mesh.h"
#include "osd_subdivide_mesh.h"

#include <memory>
#include <stdio.h>

namespace ri
{
    static int SumArray(const int ar[], int n)
    {
        int nRet = 0;
        for (int i = 0; i < n; i++)
        {
            nRet += ar[i];
        }
        return nRet;
    }

    static bool IsKey(const char* a, const char* b)
    {
        if (strcmp(a, b) == 0) return true;
        return false;
    }

    static bool IsKey(const char* const tbl[], const char* key)
    {
        int i = 0;
        while (tbl[i])
        {
            if (strcmp(key, tbl[i]) == 0) return true;
            i++;
        }
        return false;
    }

    static bool IsFloat(const ri_classifier::param_data& data)
    {
        int nType = data.get_type();
        switch (nType)
        {
        case ri_classifier::INTEGER:
        case ri_classifier::STRING:
            return false;
        }
        return true;
    }

    static const float* GetFloat(const ri_geometry* pGeo, const char* key)
    {
        const ri_parameters::value_type* p_param = pGeo->get_parameter(key);
        if (p_param && p_param->nType == ri_parameters::TYPE_FLOAT)
        {
            return &(p_param->float_values[0]);
        }
        return NULL;
    }

    static bool IsVarying(const ri_classifier::param_data& data)
    {
        if (data.get_class() == ri_classifier::VARYING)
            return true;
        else
            return false;
    }

    static bool IsVertex(const ri_classifier::param_data& data)
    {
        if (data.get_class() == ri_classifier::VERTEX)
            return true;
        else
            return false;
    }

    static bool IsFaceVarying(const ri_classifier::param_data& data)
    {
        if (data.get_class() == ri_classifier::FACEVARYING)
            return true;
        else
            return false;
    }

    static std::string ToS(int n)
    {
        char buffer[64];
        sprintf(buffer, "_%d", n);
        return buffer;
    }

    static void SeparateFloatValues(std::vector< std::vector<float> >& values, const std::vector<float>& value, int ch)
    {
        if(ch == 1)
        {
            values.push_back(value);
        }
        else
        {
            size_t vsz = value.size()/ch;
            for(int i = 0; i < ch; i++)
            {
                std::vector<float> out(vsz);
                for(size_t j=0;j<vsz;j++)
                {
                    out[j] = value[ch*j+i];
                }
                values.push_back(out);
            }
        }
    }

    ri_points_polygons_geometry*
    ri_subdivide_mesh(const ri_subdivision_mesh_geometry* pGeo)
    {
        int npolys = pGeo->get_npolys();
        const int* nverts = pGeo->get_nverts();
        const int* verts = pGeo->get_verts();
        const float* P = pGeo->get_P();
        if (!P)
            return NULL;
        //const float* st = pGeo->get_st();

        int ntags = pGeo->get_ntags();
        const std::string* tags = pGeo->get_tags();
        const int* nargs = pGeo->get_nargs();
        const int* intargs = pGeo->get_intargs();
        const float* floatargs = pGeo->get_floatargs();

        int isz = 0;
        int fsz = 0;
        for (int i = 0; i < ntags * 2; i++)
        {
            if (i % 2 == 0)
                isz += nargs[i];
            else
                fsz += nargs[i];
        }

        int nPts = SumArray(nverts, npolys);
        int psz = 0;
        for (int i = 0; i < nPts; i++)
        {
            if (psz < verts[i])
            {
                psz = verts[i];
            }
        }

        osd::subdivide_mesh_cage i_cage, o_cage;
        i_cage.facenums.assign(nverts, nverts + npolys);
        i_cage.indices.assign(verts, verts + nPts);
        i_cage.vertices.assign(P, P + 3 * (psz + 1));

        int ioffset = 0;
        int foffset = 0;
        for (int i = 0; i < ntags; i++)
        {
            osd::subdivide_mesh_tag tag;
            tag.name = tags[i];
            int is = nargs[2 * i + 0];
            int fs = nargs[2 * i + 1];
            for (int j = 0; j < is; j++)
            {
                tag.intargs.push_back(intargs[ioffset + j]);
            }
            for (int j = 0; j < fs; j++)
            {
                tag.floatargs.push_back(floatargs[foffset + j]);
            }
            ioffset += is;
            foffset += fs;
            i_cage.tags.push_back(tag);
        }

        std::vector<ri_classifier::param_data> vvKeys;
        std::vector<ri_classifier::param_data> fvKeys;

        {
            static const char* IGNORE[] = {
                "P",
                NULL};

            const ri_classifier& cls = pGeo->get_classifier();
            std::vector< std::vector<float> > vvDataTmp;
            std::vector< std::vector<float> > fvDataTmp;
            for(size_t i = 0;i<cls.size();i++)
            {
                const ri_classifier::param_data& data = cls[i];
                if (IsKey(IGNORE, data.get_name())) continue;
                if (!IsFloat(data)) continue;
                if (IsVarying(data)||IsVertex(data))
                {
                    vvKeys.push_back(data);
                    const std::vector<float>& value = pGeo->get_parameter(data.get_name())->float_values;
                    int ch = data.get_size();
                    std::vector< std::vector<float> > values;
                    SeparateFloatValues(values, value, ch);
                    for(int j=0;j<ch;j++)
                    {
                        std::string key = std::string(data.get_name()) + ToS(j);
                        i_cage.vvNames.push_back(key);
                        vvDataTmp.push_back(values[j]);
                    }
                } 
                else if (IsFaceVarying(data))
                {
                    fvKeys.push_back(data);
                    const std::vector<float>& value = pGeo->get_parameter(data.get_name())->float_values;
                    int ch = data.get_size();
                    std::vector< std::vector<float> > values;
                    SeparateFloatValues(values, value, ch);
                    for(int j=0;j<ch;j++)
                    {
                        std::string key = std::string(data.get_name()) + ToS(j);
                        i_cage.fvNames.push_back(key);
                        fvDataTmp.push_back(values[j]);
                    }
                }
            }

            if(vvDataTmp.size())
            {
                int ch = i_cage.vvNames.size();
                size_t vsz = vvDataTmp[0].size();
                i_cage.vvData.resize(ch*vsz);
                for(size_t i = 0;i<vsz;i++)
                {
                    for(int j = 0;j<ch;j++)
                    {
                        i_cage.vvData[ch*i + j] = vvDataTmp[j][i];
                    }
                }
            }
            if(fvDataTmp.size())
            {
                int ch = i_cage.fvNames.size();
                size_t vsz = fvDataTmp[0].size();
                i_cage.fvData.resize(ch*vsz);
                for(size_t i = 0;i<vsz;i++)
                {
                    for(int j = 0;j<ch;j++)
                    {
                        i_cage.fvData[ch*i + j] = fvDataTmp[j][i];
                    }
                }
            }
        }

        int nRet = subdivide_mesh_catmark(&i_cage, &o_cage, 3);
        if (nRet != 0)
            return NULL;

        std::vector<RtToken> tokens;
        std::vector<RtPointer> ptrs;
        std::vector< std::vector<float> > vvDataTmp;
        std::vector< std::vector<float> > fvDataTmp;

        tokens.push_back((char*)"P");
        ptrs.push_back((void*)(const float*)&(o_cage.vertices[0]));
        if (!o_cage.vvData.empty())
        {
            int total_ch = o_cage.vvNames.size();
            size_t vsz = o_cage.vvData.size() / total_ch;
            int offset_ch = 0;
            for(size_t i = 0; i<vvKeys.size(); i++)
            {
                int ch = vvKeys[i].get_size();
                std::vector<float> values(ch*vsz);
                for(size_t k = 0;k<vsz;k++)
                {
                    for(int j=0;j<ch;j++)
                    {
                        values[ch*k+j] = o_cage.vvData[total_ch*k+offset_ch+j];
                    }
                }
                vvDataTmp.push_back(values);
                offset_ch += ch;
            }
            for(size_t i = 0; i<vvKeys.size(); i++)
            {
                tokens.push_back((char*)vvKeys[i].get_name());
                ptrs.push_back((void*)(const float*)&(vvDataTmp[i][0]));
            }
        }
        if (!o_cage.fvData.empty())
        {
            int total_ch = o_cage.fvNames.size();
            size_t vsz = o_cage.fvData.size() / total_ch;
            
            int offset_ch = 0;
            for(size_t i = 0; i<fvKeys.size(); i++)
            {
                int ch = fvKeys[i].get_size();
                std::vector<float> values(ch*vsz);
                for(size_t k = 0;k<vsz;k++)
                {
                    for(int j=0;j<ch;j++)
                    {
                        values[ch*k+j] = o_cage.fvData[total_ch*k+offset_ch+j];
                    }
                }
                fvDataTmp.push_back(values);
                offset_ch += ch;
            }
            for(size_t i = 0; i<fvKeys.size(); i++)
            {
                tokens.push_back((char*)fvKeys[i].get_name());
                ptrs.push_back((void*)(const float*)&(fvDataTmp[i][0]));
            }
        }

        std::unique_ptr<ri_points_polygons_geometry> ap(new ri_points_polygons_geometry(
            &(pGeo->get_classifier()), (int)o_cage.facenums.size(), &o_cage.facenums[0],
            &o_cage.indices[0], (int)tokens.size(), &tokens[0], &ptrs[0]));
        ap->set_transform(pGeo->get_transform());
        {
            ri_attributes attr = pGeo->get_attributes();
            //std::string type = RI_SMOOTH;
            //if(!attr.get("ShadingInterpolation", "type", type))
            {
                attr.set("ShadingInterpolation", "type", RI_SMOOTH);
                attr.set("ShadingInterpolation", "smoothangle", 180.0f);
            }
            ap->set_attributes(attr);
        }
        ap->make_normals();

        return ap.release();
    }
}
