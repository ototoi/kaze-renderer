#ifndef RI_SUBDIVIDE_MESH_H
#define RI_SUBDIVIDE_MESH_H

#include "ri_geometry.h"

namespace ri
{
    ri_points_polygons_geometry*
    ri_subdivide_mesh(const ri_subdivision_mesh_geometry* pGeo);
}
#endif
