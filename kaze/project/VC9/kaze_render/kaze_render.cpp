// kaze_render.cpp : コンソール アプリケーションのエントリ ポイントを定義します。
//

#include "stdafx.h"

/*

#include "render1_test.h"
#include "render_region_test.h"
#include "mesh_accelerator_test.h"
#include "noise_texture_test.h"
#include "coorder_test.h"
#include "agree_test.h"
#include "primitive_test.h"
#include "polygon_test.h"
#include "dart_test.h"
#include "color_test.h"

#include "spline_test.h"
#include "ply2stl_test.h"
#include "hair_test.h"
#include "stream_mesh_test.h"
#include "font_test.h"
#include "blob_test.h"
#include "hilbert_test.h"

#include "sampler_test.h"
#include "patch_convert_test.h"

#include "load_rib_test.h"
#include "simple_intersection_test.h"
#include "curve_algo_test.h"
#include "curve_algo_test2.h"

#include "subdiv_test.h"

#include "lua_test.h"

*/

#include "mesh_accelerator_test.h"

#include <iostream>

int main(int argc, char* argv[])
{
	char* args[] = {"../../../../model/", "800","600"} ;
    //kaze::font_test(argc,argv);
    //return kaze::spline_test(argc,argv);
    //kaze::stream_mesh_test(argc,argv);
    //return kaze::agree_test(argc, argv);
    //return kaze::noise_texture_test(argc,argv);
    //return kaze::coorder_test(argc,argv);
    //return kaze::lua_test(argc, argv);
    //return kaze::render1_test(3,args);
    //return kaze::sampler_test(argc, argv);
    return kaze::mesh_accelerator_test(3,args);
    //return kaze::render_region_test(argc,argv);
    //return kaze::primitive_test(argc,argv);
    //return kaze::hilbert_test(argc, argv);
    //return kaze::hair_test(argc,argv);
	//return kaze::curve_algo_test(argc,argv);
	//return kaze::curve_algo_test2(argc,argv);
    //return kaze::polygon_test(argc,argv);
    //return kaze::dart_test(argc,argv);
    //return kaze::color_test(argc, argv);
    //return kaze::ply2stl_test(argc, argv);
    //return kaze::patch_convert_test(argc, argv);

    //return kaze::blob_test(argc,argv);
	//return kaze::simple_intersection_test(argc, argv);

	//return kaze::subdiv_test(argc, argv);

	//return kaze::lua_test(argc, argv);

	//return kaze::load_rib_test(argc, argv);
    return 0;
}

