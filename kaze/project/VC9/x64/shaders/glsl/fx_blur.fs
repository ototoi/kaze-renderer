#version 130

in vec3 fpos;
in vec2 fuv;

out vec4 out_color;

uniform sampler2D tex;
uniform int width;
uniform int height;

void main()
{
	float dx = 1.0/width;
	float dy = 1.0/height;
	vec3 col = vec3(0);
	col += texture2D(tex,fuv+vec2(-dx,-dy)).rgb;
	col += texture2D(tex,fuv+vec2(-dx,  0)).rgb;
	col += texture2D(tex,fuv+vec2(-dx,+dy)).rgb;
	col += texture2D(tex,fuv+vec2(  0,-dy)).rgb;
	col += texture2D(tex,fuv+vec2(  0,  0)).rgb;
	col += texture2D(tex,fuv+vec2(  0,+dy)).rgb;
	col += texture2D(tex,fuv+vec2(+dx,-dy)).rgb;
	col += texture2D(tex,fuv+vec2(+dx,  0)).rgb;
	col += texture2D(tex,fuv+vec2(+dx,+dy)).rgb;
	col *= 1.0/9.0;
	out_color = vec4(col,1);	//gl_FragColor = vec4(col,1);
}