#version 130

in vec3 pos;
in vec2 uv;

out vec3 fpos;
out vec2 fuv;

void main()
{
	fpos = pos;
	fuv  = uv;
	gl_Position = vec4(fpos,1);
}
