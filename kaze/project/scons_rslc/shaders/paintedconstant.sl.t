#line 1 "./shaders/paintedconstant.sl"




surface paintedconstant(string texturename="";)
{
  Oi = Os;

  if (texturename!="")
       Ci = Os * color texture (texturename);
  else 
       Ci = Os * Cs;
}
