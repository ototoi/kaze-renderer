#line 1 "./shaders/function_overloading_bug2644580.sl"
float foo( float a ) { return a; }
vector foo( vector a ) { return a; }
string foo( string a ) { return a; }

surface function_overloading_bug2644580()
{
	float some_float = 0;
	vector some_vector = (1,1,1);
	string some_string = "asdf";

	foo(some_float);
	foo(some_vector);
	foo(some_string);
}
