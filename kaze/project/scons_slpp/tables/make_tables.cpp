#include <stdio.h>
#include <ctype.h>

enum CHAR_MASK
{
    IS_SPACE = 1,
    IS_ALPHA = 2,
    IS_NUM = 4,
    SLPP_IS_HEX   = 8,
    SLPP_IS_OCT   = 16,
};

static int CHAR_TABLE[256] = {};

static
void make_char_mask()
{
    for(int i = 0;i<256;i++)
    {
        if(isspace((char)i))
        {
            CHAR_TABLE[i] |= IS_SPACE;
        }
        if(isalpha((char)i))
        {
            CHAR_TABLE[i] |= IS_ALPHA;
        }
        if(isdigit((char)i))
        {
            CHAR_TABLE[i] |= IS_NUM;
        }

        if(isdigit((char)i) || ('a'<=i&&i<='f') || ('A'<=i&&i<='F'))
        {
            CHAR_TABLE[i] |= SLPP_IS_HEX;
        }

        if(('0'<=i&&i<='7'))
        {
            CHAR_TABLE[i] |= SLPP_IS_OCT;
        }
    }
}

static
void print_char_mask()
{
    printf("static const int CHAR_MASK_TABLE[256] =\n");
    printf("{\n");
    for(int i = 0; i < 256; i++)
    {
        int x = i % 16;
        //int y = i / 16;
        if(x == 0)
        {
            printf("\t");
        }
        printf("%d", CHAR_TABLE[i]);
        if(i != 255)
        {
            printf(",");
        }
        if(x == 15)
        {
            printf("\n");
        }
    }
    printf("};\n");
}


int main()
{
    make_char_mask();
    print_char_mask();
    return 0;
}
