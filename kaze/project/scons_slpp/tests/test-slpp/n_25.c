 

 



 
 








extern int      strcmp( const char *, const char *);
extern size_t   strlen( const char *);
extern void     exit( int);










main( void)
{
    int     a = 1, b = 2, abc = 3, MACRO_0MACRO_1 = 2;

    fputs( "started\n", stderr);

 
    assert( (a,b - 1) == 1);

 
 
    assert( (   - a) == -1);

 
    assert( abc == 3);

 
    assert( MACRO_0MACRO_1 == 2);

 
    assert( strcmp( "ZERO_TOKEN", "ZERO_TOKEN") == 0);

    fputs( "success\n", stderr);
    return  0;
}

