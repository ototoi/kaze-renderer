 

 



 
 








extern int      strcmp( const char *, const char *);
extern size_t   strlen( const char *);
extern void     exit( int);


main( void)
{
 
%: define  stringize( a)    %: a

    fputs( "started\n", stderr);

    assert( strcmp( stringize( abc), "abc") == 0);

 
    assert( strcmp( stringize( <:), "<" ":") == 0);

    fputs( "success\n", stderr);
    return 0;
}

