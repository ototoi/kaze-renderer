 

 



 
 








extern int      strcmp( const char *, const char *);
extern size_t   strlen( const char *);
extern void     exit( int);






main( void)
{
    fputs( "started\n", stderr);

 
    assert( strcmp( "12E+EXP", "12E+EXP") == 0);

 
    assert( strcmp( ".2e-EXP", ".2e-EXP") == 0);

 
    assert( strcmp( "12+EXP", "12+1") == 0);

    fputs( "success\n", stderr);
    return  0;
}

