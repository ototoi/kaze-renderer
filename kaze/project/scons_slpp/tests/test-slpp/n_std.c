 


 



 
 








extern int      strcmp( const char *, const char *);
extern size_t   strlen( const char *);
extern void     exit( int);











void    n_1( void);
void    n_2( void);
void    n_3( void);
void    n_4( void);
void    n_5( void);
void    n_6( void);
void    n_7( void);
void    n_9( void);
void    n_10( void);
void    n_11( void);
void    n_12( void);
void    n_13( void);
void    n_13_5( void);
void    n_13_7( void);
void    n_13_8( void);
void    n_13_13( void);
void    n_15( void);
void    n_18( void);
void    n_19( void);
void    n_20( void);
void    n_21( void);
void    n_22( void);
void    n_23( void);
void    n_24( void);
void    n_25( void);
void    n_26( void);
void    n_27( void);
void    n_28( void);
void    n_29( void);
void    n_30( void);
void    n_32( void);
void    n_37( void);

int main( void)
{
    n_1();
    n_2();
    n_3();
    n_4();
    n_5();
    n_6();
    n_7();
    n_9();
    n_10();
    n_11();
    n_12();
    n_13();
    n_13_5();
    n_13_7();
    n_13_8();
    n_13_13();
    n_15();
    n_18();
    n_19();
    n_20();
    n_21();
    n_22();
    n_23();
    n_24();
    n_25();
    n_26();
    n_27();
    n_28();
    n_29();
    n_30();
    n_32();
    n_37();
    puts( "<End of \"n_std.c\">");
    return  0;
}

char    quasi_trigraph[] = { '?', '?', ' ', '?', '?', '?', ' '
            , '?', '?', '%', ' ', '?', '?', '^', ' ', '?', '#', '\0' };

void    n_1( void)
 
{
    int     ab = 1, cd = 2;

 
    assert( strcmp( "??( ??) ??/??/ ??' ??< ??> ??! ??- ??="
            ,"[ ] \\ ^ { } | ~ #") == 0);

 
??= define  OR( a, b)   a ??! b
    assert( OR( ab, cd) == 3);

 
    assert( strcmp( "?? ??? ??% ??^ ???=", quasi_trigraph) == 0);
}

void    n_2( void)
 
{
    int     ab = 1, cd = 2, ef = 3, abcde = 5;

 

    assert( ab + cd + ef == 6);

 


    assert (ab + cd               + ef == 6);

 
    assert (strcmp( "abcde", "abcde") == 0);

 
    assert( abcde == 5);

 
    assert( abc??/
de == 5);
}

void    n_3( void)
 
{
    int     abcd = 4;

 
    assert( strcmp( "ab" de), "abc de") == 0);

 
 

 

 



    assert( abcd == 4);
}

void    n_4( void)
 
{
 
%: define  stringize( a)    %: a

    assert( strcmp( stringize( abc), "abc") == 0);

 
    assert( strcmp( stringize( <:), "<" ":") == 0);
}

void    n_5( void)
 
{
    int     abcde = 5;
 
 
    assert( abcde   == 5);
}

 
 

void    n_6( void)
 
{
    int     abc = 3;

    assert( isalpha( 'a'));

 

 


    assert( abc == 3);

 

    assert( MACRO_abc == 3);
}

void    n_7( void)
 
{
 

    assert( 1234 == 1234);
    assert( strcmp( "cpp", "cpp") == 0);

 

    assert( 2345 == 2345);
    assert( strcmp( "cpp", "cpp") == 0);

 

    assert( 2351 == 1234);
    assert( strcmp( "cpp", "n_7.c") == 0);
}

 


void    n_9( void)
 
{
 


}

void    n_10( void)
 
{
 
 





    assert( 1);




 
  

 


}

void    n_11( void)
 
{
    int     abc = 1, a = 0;

 





    assert( abc);


    assert( abc);




 






}


void    n_12( void)
 
{
 







 




 




 




 




 




 



}

void    n_13( void)
 
 
{
 




 




 




 



}

void    n_13_5( void)
 
{
 




 

    fputs( "Bad arithmetic conversion.\n", stderr);



    fputs( "Bad arithmetic conversion.\n", stderr);


 



}

void    n_13_7( void)
 
{
 












}

void    n_13_8( void)
 
{
 




 




 




 

    fputs( "Bad grouping of -, +, !, *, /, >> in #if expression.\n", stderr);


 



}

void    n_13_13( void)
 
{





 

     






 

     





}

void    n_15( void)
 
{

 

    assert( 1);




 



    assert( 1);

}

void    n_18( void)
 
 

{
    int     c = 3;

 




 
    assert( ( c ) == 3);

 

    assert( strcmp( "n1:n2", "n1:n2") == 0);
}

void    n_19( void)
 
{
    int     c = 1;

 

 
    assert( (                             c    ) == 1);
}

void    n_20( void)
 
{
 

    double   fl;
    assert( sizeof fl == sizeof (double));
}

void    n_21( void)
 
{
    int     a = 1, x = 2, y = -3;

 

    assert( ---a == -1);

 



 
    assert( x--y                      == -1);
}

void    n_22( void)
 
{


 
    assert( strcmp( "12E+EXP", "12E+EXP") == 0);

 
    assert( strcmp( ".2e-EXP", ".2e-EXP") == 0);

 
    assert( strcmp( "12+EXP", "12+1") == 0);
}

void    n_23( void)
 
{
    int     xy = 1;

 
    assert( xy == 1);

 


    assert( .12e+2 == 12.0);
}

void    n_24( void)
 
{
 
    assert( strcmp( "a+b", "a+b") == 0);

 
    assert( strcmp( "ab"    +
        cd  ), "ab + cd") == 0);

 
    assert( strcmp( "'\"'+\"'\\"\"", "'\"' + \"' \\\"\"") == 0);

 
    assert( strcmp( "\"abc\"", "\"abc\"") == 0);

 

    assert( strcmp( "x-y", "x-y") == 0);
}

void    n_25( void)
 
{
    int     a = 1, b = 2, abc = 3, MACRO_0MACRO_1 = 2;




 
    assert( (a,b - 1) == 1);

 
    assert( (   - a) == -1);

 
    assert( abc == 3);

 
    assert( MACRO_0MACRO_1 == 2);

 
    assert( strcmp( "ZERO_TOKEN", "ZERO_TOKEN") == 0);
}
















int     f( int a)
{
    return  a;
}

int     g( int a)
{
    return  a * 2;
}


void    n_26( void)
 
{
    int     x = 1;
    int     AB = 1;
    int     Z[1];
    Z[0] = 1;

 
 

    assert( Z[0] == 1);

 
 


    assert( AB == 1);

 
 

    assert( x + f(x) == 2);

 
 


    assert( x + x + g( x) == 4);

 
 
    assert( Z[0] + f(Z[0]) == 2);
}

void    n_27( void)
 
{
    int     a = 1, b = 2, c, m = 1, n = 2;

 








    assert( 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 == 36);

 




    assert( (1) + (1 + 2) + 1 + 2 + 1 + 2 + 3 + 1 + 2 + 3 + 4 == 23);

 
    assert( 1 == 1);




 
    assert( ((a) - (b)) == -1);

 
