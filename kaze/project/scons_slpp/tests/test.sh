for file in `\find ./test-c -maxdepth 1 -type f`; do
    faname_ext="${file##*/}"
    ../bin/slpp $file > ./test-slpp/$faname_ext
done

for file in `\find ./test-c -maxdepth 1 -type f`; do
    faname_ext="${file##*/}"
    gcc -E -P $file > ./test-gcc/$faname_ext
done