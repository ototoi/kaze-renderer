

extern int strcmp( const char *, const char *);
extern size_t strlen( const char *);
extern void exit( int);

char quasi_trigraph[] = { '?', '?', ' ', '?', '?', '?', ' ', '?', '?', '%',
            ' ', '?', '?', '^', ' ', '?', '#', '\0' };

main( void)
{
    int ab = 1, cd = 2;

    fputs( "started\n", stderr);


    assert( strcmp( "??( ??) ??/??/ ??' ??< ??> ??! ??- ??="
            ,"[ ] \\ ^ { } | ~ #") == 0);


??= define OR( a, b) a ??! b
    assert( OR( ab, cd) == 3);


    assert( strcmp( "?? ??? ??% ??^ ???=", quasi_trigraph) == 0);

    fputs( "success\n", stderr);
    return 0;
}
