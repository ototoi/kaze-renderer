extern int strcmp( const char *, const char *);
extern size_t strlen( const char *);
extern void exit( int);
void n_1( void);
void n_2( void);
void n_3( void);
void n_4( void);
void n_5( void);
void n_6( void);
void n_7( void);
void n_9( void);
void n_10( void);
void n_11( void);
void n_12( void);
void n_13( void);
void n_13_5( void);
void n_13_7( void);
void n_13_8( void);
void n_13_13( void);
void n_15( void);
void n_18( void);
void n_19( void);
void n_20( void);
void n_21( void);
void n_22( void);
void n_23( void);
void n_24( void);
void n_25( void);
void n_26( void);
void n_27( void);
void n_28( void);
void n_29( void);
void n_30( void);
void n_32( void);
void n_37( void);

int main( void)
{
    n_1();
    n_2();
    n_3();
    n_4();
    n_5();
    n_6();
    n_7();
    n_9();
    n_10();
    n_11();
    n_12();
    n_13();
    n_13_5();
    n_13_7();
    n_13_8();
    n_13_13();
    n_15();
    n_18();
    n_19();
    n_20();
    n_21();
    n_22();
    n_23();
    n_24();
    n_25();
    n_26();
    n_27();
    n_28();
    n_29();
    n_30();
    n_32();
    n_37();
    puts( "<End of \"n_std.c\">");
    return 0;
}

char quasi_trigraph[] = { '?', '?', ' ', '?', '?', '?', ' '
            , '?', '?', '%', ' ', '?', '?', '^', ' ', '?', '#', '\0' };

void n_1( void)

{
    int ab = 1, cd = 2;


    assert( strcmp( "??( ??) ??/??/ ??' ??< ??> ??! ??- ??="
            ,"[ ] \\ ^ { } | ~ #") == 0);


??= define OR( a, b) a ??! b
    assert( OR( ab, cd) == 3);


    assert( strcmp( "?? ??? ??% ??^ ???=", quasi_trigraph) == 0);
}

void n_2( void)

{
    int ab = 1, cd = 2, ef = 3, abcde = 5;





    assert( ab + cd + ef == 6);
    assert (ab + cd + ef == 6);


    assert (strcmp( "abcde", "abcde") == 0);



    assert( abcde == 5);



    assert( abc??/
de == 5);
}

void n_3( void)

{
    int abcd = 4;


    assert( strcmp( "abc de", "abc de") == 0);
    assert( abcd == 4);
}

void n_4( void)

{



    assert( strcmp( "abc", "abc") == 0);


    assert( strcmp( "<:", "<" ":") == 0);
}

void n_5( void)



{
    int abcde = 5;


    assert( abcde == 5);
}





typedef signed char __int8_t;



typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;
typedef long long __int64_t;
typedef unsigned long long __uint64_t;

typedef long __darwin_intptr_t;
typedef unsigned int __darwin_natural_t;
typedef int __darwin_ct_rune_t;





typedef union {
 char __mbstate8[128];
 long long _mbstateL;
} __mbstate_t;

typedef __mbstate_t __darwin_mbstate_t;


typedef long int __darwin_ptrdiff_t;







typedef long unsigned int __darwin_size_t;





typedef __builtin_va_list __darwin_va_list;





typedef int __darwin_wchar_t;




typedef __darwin_wchar_t __darwin_rune_t;


typedef int __darwin_wint_t;




typedef unsigned long __darwin_clock_t;
typedef __uint32_t __darwin_socklen_t;
typedef long __darwin_ssize_t;
typedef long __darwin_time_t;
typedef __int64_t __darwin_blkcnt_t;
typedef __int32_t __darwin_blksize_t;
typedef __int32_t __darwin_dev_t;
typedef unsigned int __darwin_fsblkcnt_t;
typedef unsigned int __darwin_fsfilcnt_t;
typedef __uint32_t __darwin_gid_t;
typedef __uint32_t __darwin_id_t;
typedef __uint64_t __darwin_ino64_t;

typedef __darwin_ino64_t __darwin_ino_t;



typedef __darwin_natural_t __darwin_mach_port_name_t;
typedef __darwin_mach_port_name_t __darwin_mach_port_t;
typedef __uint16_t __darwin_mode_t;
typedef __int64_t __darwin_off_t;
typedef __int32_t __darwin_pid_t;
typedef __uint32_t __darwin_sigset_t;
typedef __int32_t __darwin_suseconds_t;
typedef __uint32_t __darwin_uid_t;
typedef __uint32_t __darwin_useconds_t;
typedef unsigned char __darwin_uuid_t[16];
typedef char __darwin_uuid_string_t[37];


struct __darwin_pthread_handler_rec {
 void (*__routine)(void *);
 void *__arg;
 struct __darwin_pthread_handler_rec *__next;
};

struct _opaque_pthread_attr_t {
 long __sig;
 char __opaque[56];
};

struct _opaque_pthread_cond_t {
 long __sig;
 char __opaque[40];
};

struct _opaque_pthread_condattr_t {
 long __sig;
 char __opaque[8];
};

struct _opaque_pthread_mutex_t {
 long __sig;
 char __opaque[56];
};

struct _opaque_pthread_mutexattr_t {
 long __sig;
 char __opaque[8];
};

struct _opaque_pthread_once_t {
 long __sig;
 char __opaque[8];
};

struct _opaque_pthread_rwlock_t {
 long __sig;
 char __opaque[192];
};

struct _opaque_pthread_rwlockattr_t {
 long __sig;
 char __opaque[16];
};

struct _opaque_pthread_t {
 long __sig;
 struct __darwin_pthread_handler_rec *__cleanup_stack;
 char __opaque[8176];
};

typedef struct _opaque_pthread_attr_t __darwin_pthread_attr_t;
typedef struct _opaque_pthread_cond_t __darwin_pthread_cond_t;
typedef struct _opaque_pthread_condattr_t __darwin_pthread_condattr_t;
typedef unsigned long __darwin_pthread_key_t;
typedef struct _opaque_pthread_mutex_t __darwin_pthread_mutex_t;
typedef struct _opaque_pthread_mutexattr_t __darwin_pthread_mutexattr_t;
typedef struct _opaque_pthread_once_t __darwin_pthread_once_t;
typedef struct _opaque_pthread_rwlock_t __darwin_pthread_rwlock_t;
typedef struct _opaque_pthread_rwlockattr_t __darwin_pthread_rwlockattr_t;
typedef struct _opaque_pthread_t *__darwin_pthread_t;
typedef int __darwin_nl_item;
typedef int __darwin_wctrans_t;

typedef __uint32_t __darwin_wctype_t;



typedef __darwin_size_t size_t;
typedef __darwin_ct_rune_t ct_rune_t;
typedef __darwin_rune_t rune_t;
typedef __darwin_wchar_t wchar_t;
typedef __darwin_wint_t wint_t;
typedef struct {
 __darwin_rune_t __min;
 __darwin_rune_t __max;
 __darwin_rune_t __map;
 __uint32_t *__types;
} _RuneEntry;

typedef struct {
 int __nranges;
 _RuneEntry *__ranges;
} _RuneRange;

typedef struct {
 char __name[14];
 __uint32_t __mask;
} _RuneCharClass;

typedef struct {
 char __magic[8];
 char __encoding[32];

 __darwin_rune_t (*__sgetrune)(const char *, __darwin_size_t, char const **);
 int (*__sputrune)(__darwin_rune_t, char *, __darwin_size_t, char **);
 __darwin_rune_t __invalid_rune;

 __uint32_t __runetype[(1 <<8 )];
 __darwin_rune_t __maplower[(1 <<8 )];
 __darwin_rune_t __mapupper[(1 <<8 )];






 _RuneRange __runetype_ext;
 _RuneRange __maplower_ext;
 _RuneRange __mapupper_ext;

 void *__variable;
 int __variable_len;




 int __ncharclasses;
 _RuneCharClass *__charclasses;
} _RuneLocale;




extern _RuneLocale _DefaultRuneLocale;
extern _RuneLocale *_CurrentRuneLocale;
unsigned long ___runetype(__darwin_ct_rune_t);
__darwin_ct_rune_t ___tolower(__darwin_ct_rune_t);
__darwin_ct_rune_t ___toupper(__darwin_ct_rune_t);


inline int
isascii(int _c)
{
 return ((_c & ~0x7F) == 0);
}
int __maskrune(__darwin_ct_rune_t, unsigned long);



inline int
__istype(__darwin_ct_rune_t _c, unsigned long _f)
{



 return (isascii(_c) ? !!(_DefaultRuneLocale.__runetype[_c] & _f)
  : !!__maskrune(_c, _f));

}

inline __darwin_ct_rune_t
__isctype(__darwin_ct_rune_t _c, unsigned long _f)
{



 return (_c < 0 || _c >= (1 <<8 )) ? 0 :
  !!(_DefaultRuneLocale.__runetype[_c] & _f);

}
__darwin_ct_rune_t __toupper(__darwin_ct_rune_t);
__darwin_ct_rune_t __tolower(__darwin_ct_rune_t);



inline int
__wcwidth(__darwin_ct_rune_t _c)
{
 unsigned int _x;

 if (_c == 0)
  return (0);
 _x = (unsigned int)__maskrune(_c, 0xe0000000L|0x00040000L);
 if ((_x & 0xe0000000L) != 0)
  return ((_x & 0xe0000000L) >> 30);
 return ((_x & 0x00040000L) != 0 ? 1 : -1);
}






inline int
isalnum(int _c)
{
 return (__istype(_c, 0x00000100L|0x00000400L));
}

inline int
isalpha(int _c)
{
 return (__istype(_c, 0x00000100L));
}

inline int
isblank(int _c)
{
 return (__istype(_c, 0x00020000L));
}

inline int
iscntrl(int _c)
{
 return (__istype(_c, 0x00000200L));
}


inline int
isdigit(int _c)
{
 return (__isctype(_c, 0x00000400L));
}

inline int
isgraph(int _c)
{
 return (__istype(_c, 0x00000800L));
}

inline int
islower(int _c)
{
 return (__istype(_c, 0x00001000L));
}

inline int
isprint(int _c)
{
 return (__istype(_c, 0x00040000L));
}

inline int
ispunct(int _c)
{
 return (__istype(_c, 0x00002000L));
}

inline int
isspace(int _c)
{
 return (__istype(_c, 0x00004000L));
}

inline int
isupper(int _c)
{
 return (__istype(_c, 0x00008000L));
}


inline int
isxdigit(int _c)
{
 return (__isctype(_c, 0x00010000L));
}

inline int
toascii(int _c)
{
 return (_c & 0x7F);
}

inline int
tolower(int _c)
{
        return (__tolower(_c));
}

inline int
toupper(int _c)
{
        return (__toupper(_c));
}


inline int
digittoint(int _c)
{
 return (__maskrune(_c, 0x0F));
}

inline int
ishexnumber(int _c)
{
 return (__istype(_c, 0x00010000L));
}

inline int
isideogram(int _c)
{
 return (__istype(_c, 0x00080000L));
}

inline int
isnumber(int _c)
{
 return (__istype(_c, 0x00000400L));
}

inline int
isphonogram(int _c)
{
 return (__istype(_c, 0x00200000L));
}

inline int
isrune(int _c)
{
 return (__istype(_c, 0xFFFFFFF0L));
}

inline int
isspecial(int _c)
{
 return (__istype(_c, 0x00100000L));
}


void n_6( void)

{
    int abc = 3;

    assert( isalpha( 'a'));




 assert( abc == 3);




 assert( abc == 3);
}

void n_7( void)

{
 assert( 1234 == 1234);
    assert( strcmp( "cpp", "cpp") == 0);
 assert( 2345 == 2345);
    assert( strcmp( "cpp", "cpp") == 0);
 assert( 1234 == 1234);
    assert( strcmp( "n_7.c", "n_7.c") == 0);
}

void n_9( void)

{



#pragma who knows ?
}

void n_10( void)

{







    assert( 1);
}

void n_11( void)

{
    int abc = 1, a = 0;







    assert( abc);


    assert( abc);
}








void n_12( void)

{
}

void n_13( void)
{
}

void n_13_5( void)

{
}

void n_13_7( void)

{
}

void n_13_8( void)

{
}

void n_13_13( void)

{
}

void n_15( void)

{



    assert( 1);
    assert( 1);

}

void n_18( void)




{
    int c = 3;


    assert( (1-1) == 0);





    assert( ( c ) == 3);



    assert( strcmp( "n1:n2", "n1:n2") == 0);
}

void n_19( void)

{
    int c = 1;
    assert( ( c ) == 1);
}

void n_20( void)

{


    double fl;
    assert( sizeof fl == sizeof (double));
}

void n_21( void)

{
    int a = 1, x = 2, y = -3;



    assert( - - -a == -1);






    assert( x- -y == -1);
}

void n_22( void)

{



    assert( strcmp( "12E+EXP", "12E+EXP") == 0);


    assert( strcmp( ".2e-EXP", ".2e-EXP") == 0);



    assert( strcmp( "12+1", "12+1") == 0);
}

void n_23( void)

{
    int xy = 1;


    assert( xy == 1);




    assert( .12e+2 == 12.0);
}

void n_24( void)

{

    assert( strcmp( "a+b", "a+b") == 0);



    assert( strcmp( "ab + cd", "ab + cd") == 0);




    assert( strcmp( "'\"' + \"' \\\"\"", "'\"' + \"' \\\"\"") == 0);



    assert( strcmp( "\"abc\"", "\"abc\"") == 0);





    assert( strcmp( "x-y", "x-y") == 0);
}

void n_25( void)



{
    int a = 1, b = 2, abc = 3, MACRO_0MACRO_1 = 2;






    assert( (a,b - 1) == 1);


    assert( ( - a) == -1);


    assert( abc == 3);


    assert( MACRO_0MACRO_1 == 2);


    assert( strcmp( "ZERO_TOKEN", "ZERO_TOKEN") == 0);
}
int f( int a)
{
    return a;
}

int g( int a)
{
    return a * 2;
}


void n_26( void)

{
    int x = 1;
    int AB = 1;
    int Z[1];
    Z[0] = 1;




    assert( Z[0] == 1);





    assert( AB == 1);




    assert( x + f(x) == 2);





    assert( x + x + g( x) == 4);



    assert( Z[0] + f(Z[0]) == 2);
}

void n_27( void)




{
    int a = 1, b = 2, c, m = 1, n = 2;
    assert( 1 + 2 + 3 + 4 + 5 + 6 + 7 + 8 == 36);






    assert( (1) + (1 + 2) + 1 + 2 + 1 + 2 + 3 + 1 + 2 + 3 + 4 == 23);


    assert( 1 == 1);






    assert( ((a) - (b)) == -1);


    c = (a - b);
    assert( c == -1);





    assert( n == 2);
}

void n_28( void)


{
    char * date = "Oct 25 2016";


    assert( strcmp( "n_std.c", "n_std.c") == 0);


    assert( 779 == 779);


    assert( strlen( "Oct 25 2016") == 11);
    assert( date[ 4] != '0');


    assert( strlen( "17:43:32") == 8);


    assert( 1);


    assert( 201112L >= 199409L);





{
    char * file = "./test-c/line.h";
    file += strlen( file) - 6;
    assert( 6 == 6 && strcmp( file, "line.h") == 0);
}
}

void n_29( void)

{
    int DEFINED = 1;




    assert( DEFINED == 1);



}

void n_30( void)






{



    int a = 1, b = 2, c = 3;


    assert
    (
        a + b + c





        == 6
    );
}

void n_32( void)

{
}

void n_37( void)

{






    int ABCDEFGHIJKLMNOPQRSTUVWXYZabcde = 31;
    int ABCDEFGHIJKLMNOPQRSTUVWXYZabcd_ = 30;
    int nest = 0;


    assert(
        ABCDEFGHIJKLMNOPQRSTUVWXYZabcde

        == 31);



    assert( ABCDEFGHIJKLMNOPQRSTUVWXYZabcd_ == 30);


    nest = 0;
                                nest = 8;
    assert( nest == 8);


    nest = 0;



    nest = 1;




    nest = 2;




    nest = 3;




    nest = 4;




    nest = 5;




    nest = 6;




    nest = 7;





    nest = 8;
 assert( nest == 8);






    nest = 32;

    assert( nest == 32);


    {
        char * extremely_long_string =
"123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567"







        ;
        assert( strlen( extremely_long_string) == 507);
    }


    {
    int a123456789012345678901234567890 = 123450; int b123456789012345678901234567890 = 123451; int c123456789012345678901234567890 = 123452; int d123456789012345678901234567890 = 123453; int e123456789012345678901234567890 = 123454; int f123456789012345678901234567890 = 123455; int A123456789012345678901234567890 = 123456; int B123456789012345678901234567890 = 123457; int C123456789012345678901234567890 = 123458; int D1234567890123456789012 = 123459;
        assert( a123456789012345678901234567890 == 123450
            && D1234567890123456789012 == 123459);
    }







 assert( 1);
}
