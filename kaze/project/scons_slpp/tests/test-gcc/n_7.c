

extern int strcmp( const char *, const char *);
extern size_t strlen( const char *);
extern void exit( int);

main( void)
{
    fputs( "started\n", stderr);
 assert( 1234 == 1234);
    assert( strcmp( "cpp", "cpp") == 0);
 assert( 2345 == 2345);
    assert( strcmp( "cpp", "cpp") == 0);
 assert( 1234 == 1234);
    assert( strcmp( "n_7.c", "n_7.c") == 0);

    fputs( "success\n", stderr);
    return 0;
}
