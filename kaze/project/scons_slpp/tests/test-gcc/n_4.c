

extern int strcmp( const char *, const char *);
extern size_t strlen( const char *);
extern void exit( int);

main( void)
{



    fputs( "started\n", stderr);

    assert( strcmp( "abc", "abc") == 0);


    assert( strcmp( "<:", "<" ":") == 0);

    fputs( "success\n", stderr);
    return 0;
}
