#define A
//#define B
//#undef B

#if defined(A)          //1-1
1
2
3
#elif defined(A)        //0-1
4
5
6
#elif defined(A)        //01
7
8
9
#else                   //00
q
w
e
#endif

#if defined(B)             //10
1B
2B
3B
#elif defined(B)            //10
4B
5B
6B
#elif defined(A)            //10
4B
5B
6B
#else                       //
7B
8B
9B
#endif

#define AABB A ## B
#define CCBB CC ## BB 
#define AAA(X) (defined(X))

#if AAA(AABB)
1
#else
2
#endif

#if AAA(CCBB)
3
#else
4
#endif



#line 1 "ggg.txt"
#undef __FILE__
__LINE__, __FILE__, __DATE__

