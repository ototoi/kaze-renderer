#include "test.h"
/*
 * test #include <sdf> //
 */
#define P0(x) P1(x)
#define P1(x) x ## 1
#define P2(x) #x "1"
//#define P2(x) "qwerty"
#define P3(x) P ## 2(x)
#undef P3
#define P4(x) P ## 2(x)
#define P6(x) #x "0"
#define P5(x) P ## 6(x ## 1)
#define P6(x) #x "1" 2
#define P7(x) 2
//#define P8(x ## 1) x1

#define G0 666
#define G1 1
#define G2 2
#define G3 3
#define G4 3  ##            1
#define G5 G7
#define G6 G5
#define G7 G6

#define EA G8

#define G8 G9
#define G9 EA

#define F11 1
#define F1 2
#define F111 0

#define H1 "H1" H3
#define H2 "H2" H4
#define H3 H1 H2 H3
#define H4 H2 "K"

#define I1

#define J1(x) "x + 1" x

#define K1(x,y) (x + y)

#define L1(x) (#x)
#define L2(x, y) (#x # y)
#define L3(x, y) (P1(y))

#define M(x) 1


int main()
{
    P0(4)
    P1(4)
    P2(4)
    P3(4)
    P4(4)
    P5(4)

    P8(4)

    //#1 ## 2
    G0;
    G1;
    G2;
    G3;
    G4;G4;

    G7;
    G8;
    F111;F111;F111;
    F111;

    H3;

    I1;

    J1(1234);

    K1(1   ,2   );

    G0X + 1;
    "G0X + 1;"
    G0 + 1;
    "G0 + 1;"

    L2(1,L1(2));
    L3(1, 2)

#define N1 5

#if 5<<1 == N1 || 12345678 != 0
    100;
#else
    200;
#endif

#if 0XA == 10 || true
    300;
#else
    400;
#endif

#if 010 == 8
    500;
#else
    600;
#endif

#define Z1 defined(Z0)
#define Z2 defined(Z)
#define Z3 defined(Z2)
#define Z4 0
SSSS
/*

//12345*/KKKKKK
MMMMMMMMM
Z2;
Z0;
Z3;
3456
#if Z2
111
#else
222
#endif


    return 0;
}
