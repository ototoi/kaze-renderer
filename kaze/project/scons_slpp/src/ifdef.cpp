int main()
{
#define N1 5
#define N2 6

#if 3*1-1
    100;
#else
    200;
#endif


#if 3*(1-1)
    300;
#else
    400;
#endif

#if -N1
    500;
#else
    600;
#endif

#if -true
    700;
#else
    800;
#endif

#if 10 -(5+5)
    900;
#else
    1000;
#endif

#if ((1+1) & 1) -1
    AAA;
#else
    BBB;
#endif

#if ((1+1) && 1) -1
    CCC;
#else
    DDD;
#endif

#if (3*2&2) == 2
    EEE;
#else
    FFF;
#endif

#if (2&2*3) == 2
    GGG;
#else
    HHH;
#endif

//* >> + >> & >> == >> &&

#if (4&2+3) == 4
    III;
#else
    JJJ;
#endif

#if (1<<2+1) == 8
    KKK;
#else
    LLL;
#endif

#if (1&&2==1) == 0
    MMM;
#else
    NNN;
#endif

#if (1^2)
    OOO;
#else
    PPP;
#endif

#if (7%2)
    QQQ;
#else
    RRR;
#endif

#if !0
    SSS;
#else
    TTT;
#endif

#if 0x00FF
    SSS;
#else
    TTT;
#endif

#if 010 != 8
    SSS;
#else
    TTT;
#endif



    return 0;
}
