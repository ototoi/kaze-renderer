#define MAY_0 0

#if MAY_0 ? 1 / MAY_0 : 1
1
#else
2
#endif

#if MAY_0 ? 1 / MAY_0 : 1 + 1 == 2
3
#else
4
#endif

#if !(0 ? 0 : 1 ? 0 : 1)
5
#else
6
#endif

#define F(x) (x+123)

F(1);

F(
    1
);

F
(
    2
);

#if 1
1
#else
2
#endif

int f()
{
    __FUNCTION__
}
'asdfghj'
//eee
#if 1 //eee
1
#else
2
#endif

// OpenMPの言語拡張を使用して、for文を並列実行する
#define OMP_PARALLEL_FOR _Pragma("omp parallel for")

int a[N] = {1, 2, 3, 4, 5};
  int b[N] = {1, 2, 3, 4, 5};
  int c[N] = {};
_Pragma("omp parallel for")
  // OMP_PARALLEL_FORは、 #pragma omp parallel for に展開される
  OMP_PARALLEL_FOR
  for (int i = 0; i < N; ++i) {
    c[i] = a[i] + b[i];
  }
line: __LINE__
"1 2 4 6"
__LINE__
  #pragma omp parallel for
  for (int i = 0; i < N; ++i) {
    c[i] = a[i] + b[i];
  }

  #   pragma warning (disable:1234)
  for (int i = 0; i < N; ++i) {
    c[i] = a[i] + b[i];
  }
__LINE__
  #define D(x) /*1234*/(x/*12345*/+ 1)

  D(2);

  12345

#define L(x) /*1234*/(x/*12345*/+ 1) \
    112

L(345778)

__LINE__



