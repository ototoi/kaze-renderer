#version 410

in vec3 fpos;
in vec2 fuv;

layout(location = 0) out vec4 out_color;

uniform sampler2DArray YTex;
uniform sampler2D UVTex;

void main()
{
    float Y = texture(YTex, vec3(fuv,0)).r;
    vec3 UVa = texture(UVTex, fuv).gba;
    out_color = vec4(Y, UVa);
}