#version 410

#define TE 1
#define TR (2*TE+1)

#define SE 5
#define SR (2*SE+1)

in vec3 fpos;
in vec2 fuv;

layout(location = 0) out vec4 out_color;

uniform sampler2D tex;
uniform int width;
uniform int height;
uniform float sigmaColor = 0.01;
uniform float sigmaDist = 0.3;

float getY(vec3 c)
{
    return dot(vec3(0.299,0.587,0.114),c);
}


void fetchArray(out vec4 cc[TR*TR], vec2 uv)
{
    float dx = 1.0/width;
	float dy = 1.0/height;
    for(int yy = -TE;yy<=+TE;yy++)
    {
        int sy = yy+TE;
        for(int xx = -TE;xx<=+TE;xx++)
        {
            int sx = xx+TE;
            cc[TR*sy+sx] = texture(tex,uv+vec2(-xx*dx,-yy*dy));
        }
    }
    /*
    cc[0] = texture(tex,uv+vec2(-dx,-dy));
	cc[1] = texture(tex,uv+vec2(-dx,  0));
	cc[2] = texture(tex,uv+vec2(-dx,+dy));
	cc[3] = texture(tex,uv+vec2(  0,-dy));
	cc[4] = texture(tex,uv+vec2(  0,  0));
	cc[5] = texture(tex,uv+vec2(  0,+dy));
	cc[6] = texture(tex,uv+vec2(+dx,-dy));
	cc[7] = texture(tex,uv+vec2(+dx,  0));
	cc[8] = texture(tex,uv+vec2(+dx,+dy));
    */
}

float getWeight(float x2, float s)
{
    float kk = (2*s*s);
    return exp( -max(0, x2-kk)/kk );
}

float getDiff(vec4 a[TR*TR], vec4 b[TR*TR])
{
    float f = 0;
    for(int i=0;i<(TR*TR);i++)
    {
        //vec4 dif = a[i]-b[i];
        float dif = (getY(a[i].rgb) - getY(b[i].rgb));
        float p = getWeight(dif*dif, sigmaColor);
        f += p;
    }
    return f / (TR*TR);
}

void main()
{
    float dx = 1.0/width;
	float dy = 1.0/height;
    
    vec4 ccc[TR*TR];
    fetchArray(ccc, fuv);
    float weight[SR*SR];
    for(int yy = -SE;yy<=+SE;yy++)
    {
        int sy = yy+SE;
        for(int xx = -SE;xx<=+SE;xx++)
        {
            int sx = xx+SE;
            vec4 cct[TR*TR];
            fetchArray(cct, fuv + vec2(dx*xx,dy*yy));
            weight[sy*SR+sx] = getDiff(ccc, cct) * getWeight((xx*xx+yy*yy)/((SE+0.5)*(SE+0.5) ), sigmaDist);
        }
    }
    float sum = 0;
    for(int i=0;i<(SR*SR);i++)
    {
        sum += weight[i];
    }
    if(sum == 0.0)
    {
        for(int i=0;i<(SR*SR);i++)
        {
            weight[i] = 1.0/(SR*SR);     
        }
    }
    else
    {
        float iSum = 1.0/sum;
        for(int i=0;i<(SR*SR);i++)
        {
            weight[i] *= iSum;
        }
    }
    vec4 col = vec4(0);
    for(int yy = -SE;yy <= +SE;yy++)
    {
        int sy = yy+SE;
        for(int xx = -SE;xx <= +SE;xx++)
        {
            int sx = xx+SE;
            vec4 t = texture(tex, fuv + vec2(dx*xx,dy*yy));
            col += t * weight[sy*SR+sx];
        }
    }
	out_color = col;
}