#version 410

in vec3 fpos;
in vec2 fuv;

layout(location = 0) out vec4 out_color;

uniform sampler2D tex;

vec4 RGB2YUV(vec4 c)
{
    float Y = dot(vec3( 0.299, 0.587, 0.114), c.rgb);//0.299R + 0.587G + 0.114B
    float U = dot(vec3(-0.147,-0.289, 0.436), c.rgb);//-0.169R - 0.331G + 0.500B
    float V = dot(vec3( 0.615,-0.515,-0.100), c.rgb);//0.500R - 0.419G - 0.081B
    return vec4(Y,U,V,c.a);
}

void main()
{
	out_color = RGB2YUV( texture(tex, fuv) );//texture(tex, fuv);//
}