"#version 410\n"
"\n"
"in vec3 fpos;\n"
"in vec2 fuv;\n"
"\n"
"layout(location = 0) out vec4 out_color;\n"
"\n"
"uniform sampler2D tex;\n"
"uniform int width;\n"
"uniform int height;\n"
"uniform float swidth;\n"
"uniform float twidth;\n"
"uniform float sigma = 0.3;\n"
"\n"
"float get_weight(float x)\n"
"{\n"
"    return max(0, exp(-(x*x)/(2*sigma*sigma)));\n"
"}\n"
"\n"
"void main()\n"
"{\n"
"	float dx = 1.0/width;\n"
"	float dy = 1.0/height;\n"
"\n"
"    int sf = int(ceil(swidth))/2;\n"
"    //int tf = twidth/2;\n"
"    float ww = 0;\n"
"	vec4 col = vec4(0);\n"
"    \n"
"    for (int x = -sf; x <= sf; x++) {\n"
"        float w = get_weight( float(x)/(swidth*0.5) );\n"
"        ww += w;\n"
"        col += w*texture(tex, fuv + vec2(x*dx,0) );\n"
"    }\n"
"    \n"
"    col *= 1.0/ww;\n"
"	out_color = col; //gl_FragColor = vec4(col,1);\n"
"}\n"
