#version 410

in vec3 fpos;
in vec2 fuv;

layout(location = 0) out vec4 out_color;

uniform sampler2D tex;
uniform int width;
uniform int height;
uniform float swidth;
uniform float twidth;
uniform float sigma = 0.3;

float get_weight(float x)
{
    return max(0, exp(-(x*x)/(2*sigma*sigma)));
}

void main()
{
	float dx = 1.0/width;
	float dy = 1.0/height;
    
    //int sf = swidth/2;
    int tf = int(ceil(twidth))/2;
    float ww = 0;
	vec4 col = vec4(0);
    
    for (int y = -tf; y <= tf; y++) {
        float w = get_weight( float(y)/(twidth*0.5)  );
        ww += w;
        col += w*texture(tex, fuv + vec2(0,y*dy) );
    }
    
    col *= 1.0/ww;
	out_color = col;	//gl_FragColor = vec4(col,1);
}