#version 410

in vec3 fpos;
in vec2 fuv;

layout(location = 0) out vec4 out_color;

uniform sampler2D tex;

vec4 YUV2RGB(vec4 c)
{
    float R = dot(vec3( 1.000, 0.000, 1.140), c.rgb);//1.000Y          + 1.402V
    float G = dot(vec3( 1.000,-0.395,-0.580), c.rgb);//1.000Y - 0.344U - 0.714V
    float B = dot(vec3( 1.000, 2.032, 0.000), c.rgb);//1.000Y + 1.772U
    return vec4(R,G,B,c.a);
}

void main()
{
    out_color = YUV2RGB( texture(tex, fuv) );//texture(tex, fuv);//
}