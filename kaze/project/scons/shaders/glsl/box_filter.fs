#version 410

in vec3 fpos;
in vec2 fuv;

layout(location = 0) out vec4 out_color;

uniform sampler2D tex;
uniform int width;
uniform int height;
uniform int swidth;
uniform int twidth;

void main()
{
	float dx = 1.0/width;
	float dy = 1.0/height;
    
    int sf = swidth/2;
    int tf = twidth/2;
	vec4 col = vec4(0);
    for (int y = -tf; y <= tf; y++) {
        for (int x = -sf; x <= sf; x++) {
            col += texture(tex, fuv + vec2(x*dx,y*dy) );
        }
    }
    col *= 1.0/(swidth*twidth);
	out_color = col;	//gl_FragColor = vec4(col,1);
}