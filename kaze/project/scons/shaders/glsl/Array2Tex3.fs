#version 410

in vec3 fpos;
in vec2 fuv;

layout(location = 0) out vec4 out_color;

uniform sampler2DArray tex;
uniform sampler2D alphaTex;

void main()
{
    float r = texture(tex, vec3(fuv,0)).r;
    float g = texture(tex, vec3(fuv,1)).r;
    float b = texture(tex, vec3(fuv,2)).r;
    float a = texture(alphaTex, fuv).a;
    out_color = vec4(r, g, b, a);
}