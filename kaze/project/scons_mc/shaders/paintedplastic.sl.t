#line 1 "./shaders/paintedplastic.sl"


















surface
paintedplastic ( float Ka = 1, Kd = .5, Ks = .5, roughness = .1;
		 color specularcolor = 1;
		 string texturename = ""; )
{
  normal Nf;
  vector V;
  color Ct;

  if (texturename != "")
       Ct = color texture (texturename);
  else Ct = 1;

  Nf = faceforward (normalize(N),I);
  V = -normalize(I);
  Oi = Os;
  Ci = Os * ( Cs * Ct * (Ka*ambient() + Kd*diffuse(Nf)) +
	      specularcolor * Ks*specular(Nf,V,roughness));
}
