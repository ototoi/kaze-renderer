#line 1 "./shaders/fisheye_projection.sl"








imager fisheye_projection(
		 
		string texturename = "";
		


		float thetaMax = 90;
		 
		float xzAngle = 0;
		 
		float blur = 0;
		)
{
	uniform float format[3] = {0,0,0};
	option("Format", format);
	uniform float scale = 2*radians(thetaMax);
	float x = scale*(xcomp(P)/format[0]-0.5);
	float y = -scale*(ycomp(P)/format[1]-0.5);
	




	float theta = sqrt(x*x + y*y);
	float phi = atan(y,x);
	


	float Rx = sin(theta)*cos(phi);
	float Ry = sin(theta)*sin(phi);
	float Rz = cos(theta);

	if(xzAngle != 0)
	{
		
		float Rxtmp = Rx;
		float a = radians(xzAngle);
		Rx = cos(a)*Rx + sin(a)*Rz;
		Rz = -sin(a)*Rxtmp + cos(a)*Rz;
	}

	
	color col = environment(texturename, vector(Rx,Ry,Rz), "blur", blur);

	 
	Oi = (1 - filterstep(radians(thetaMax), theta));
	Ci = Oi * col;
}
