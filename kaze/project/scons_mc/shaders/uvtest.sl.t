#line 1 "./shaders/uvtest.sl"









surface
uvtest (float Ka = 1;
         float Kd = .5;
         float Ks = .5;
         float roughness = .1;
	 color specularcolor = 1;)
{
	color c;

	point Nf = faceforward (normalize(N),I);

	c=color(u,v,0);

    Oi = Os;
    Ci = Os * ( c * (Ka*ambient() + Kd*diffuse(Nf)) +
		specularcolor * Ks*specular(Nf,-normalize(I),roughness));
}
