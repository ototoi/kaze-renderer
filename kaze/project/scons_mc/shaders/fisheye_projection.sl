/**
 * Map a environment onto coordinates of an Angular Fisheye Lens
 *
 * By default, the viewer faces in the +ve Z direction, with the +ve Y
 * direction pointing upward.
 *
 * Author: Chris Foster  [ chris42f (at) gmail (dot) com ]
 */
imager fisheye_projection(
		/* Environment maps */
		string texturename = "";
		/* Maximum angle from the image center that the fisheye lens should display.
		 * specifying 90 gives a full 180 degree view (hemisphere).
		 */
		float thetaMax = 90;
		/* Angle by which to rotate the view in the xz plane */
		float xzAngle = 0;
		/* Amount of blur to apply to the environment map */
		float blur = 0;
		)
{
	uniform float format[3] = {0,0,0};
	option("Format", format);
	uniform float scale = 2*radians(thetaMax);
	float x = scale*(xcomp(P)/format[0]-0.5);
	float y = -scale*(ycomp(P)/format[1]-0.5);
	/* (theta, phi) are spherical coordinates for a vector pointing from the
	 * viewer out into the scene and corresponding to the current position P.
	 *
	 * We need to sample the scene at these coordinates.
	 */
	float theta = sqrt(x*x + y*y);
	float phi = atan(y,x);
	/* R is the vector corrsponding to theta and phi.  We arrange things such
	 * that the camera points in the +z direction, with +y being upward.
	 */
	float Rx = sin(theta)*cos(phi);
	float Ry = sin(theta)*sin(phi);
	float Rz = cos(theta);

	if(xzAngle != 0)
	{
		// Rotate the view if necessary
		float Rxtmp = Rx;
		float a = radians(xzAngle);
		Rx = cos(a)*Rx + sin(a)*Rz;
		Rz = -sin(a)*Rxtmp + cos(a)*Rz;
	}

	// Sample the environment.
	color col = environment(texturename, vector(Rx,Ry,Rz), "blur", blur);

	/* cut the image off cleanly for angles > thetaMax */
	Oi = (1 - filterstep(radians(thetaMax), theta));
	Ci = Oi * col;
}
