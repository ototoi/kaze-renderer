import os
import fnmatch

flist = []

for root, dirs, files in os.walk(""):
    for fname in fnmatch.filter(files,"*.h**"):
        pathname =  os.path.join(root, fname)
        flist.append(pathname)

flist.sort()

for fname in flist:
    print "#include<" + fname + ">"
     

