import sys, os
from itertools import imap #for only 2.x

def iterfiles(basedir):
    for (path, dirs, files) in os.walk(basedir):
        for fn in files:
            yield os.path.join(path, fn)

if len(sys.argv) < 2:
    raise RuntimeError("No folder specified to convert.")

for fpath in iterfiles(sys.argv[1]):
    with open(fpath) as f:
        txt = "\n".join(imap(lambda line:line.rstrip("\r\n"), f))
    with open(fpath, 'wb') as f: # 'b' for windows stdio
        f.write(txt)