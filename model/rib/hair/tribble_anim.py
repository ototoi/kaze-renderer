import sys
import math

s = sys.stdin.readline()
if s=="":
    exit()
a = s.split(" ")
detail = float(a[0])
num = int(a[1])

out = sys.stdout

for i in range(num):
	x = 10.0
	y = 10.0
	theta = float(i)/ num * 2.0 * math.pi
	
	out.write('#theta = %f\n'%theta )
	
	x = math.sin(theta)*100
	y = math.cos(theta)*100

	out.write('FrameBegin %d\n'%(i+1) )
	out.write('Display "tribble100000_%05d.bmp" "file" "rgba"\n'%(i+1))
	out.write('Display "tribble100000_%05d.bmp" "framebuffer" "rgba"\n'%(i+1))

	out.write('Format 1000 1000 1\n')
	out.write('Hider "hidden" "jitter" 0\n')
	out.write('PixelSamples 1 1\n')

	out.write('Projection "perspective" "fov" 45\n')

	out.write('Translate 0 0 100\n')

	out.write('WorldBegin\n')
	out.write('LightSource "distantlight" 2 "intensity" 1.0 "from" [%f %f -100] "to" [0 0 0]\n'%(x, y) )

	out.write('TransformBegin\n')
	out.write('Scale 0.01 0.01 0.01\n')
	# #[0.519 0.325 0.125]
	out.write('Surface "AFMarschner" "uniform color colorTT" [0.719 0.325 0.125] "uniform color colorTRT" [0.619 0.325 0.125]\n')
	out.write('ReadArchive "tribbles/tribble100000.rib"\n')
	out.write('TransformEnd\n')
	out.write('WorldEnd\n')
	out.write('FrameEnd\n')
	out.write('\n\n')


