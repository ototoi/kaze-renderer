import sys, math, random

random.seed(5)
def randBetween(min, max):
    return random.random() * (max - min) + min
def length(x, y, z):
    return math.sqrt(x*x + y*y + z*z)
def normalize(x, y, z):
    len = length(x, y, z)
    return x/len, y/len, z/len
def scaleVector(x, y, z, sc):
    return x*sc, y*sc, z*sc
def cloud(radius, num, width):
    print 'Points \"P\" ['
    for n in range(num):
        x = 0
        y = 0
        z = 0
        l = 10
        while l >= 1:
            x = random.random() * 2 - 1;
            y = random.random() * 2 - 1;
            z = random.random() * 2 - 1;
            l = x*x+y*y+z*z
        #x,y,z = normalize(x, y, z)
        x,y,z = scaleVector(x, y, z, radius)
        print '%s %s %s' % (x, y, z)
    print '] \"constantwidth\" [%s]' % width
    print '\"Cs\" ['
    for n in range(num):
        r = randBetween(0, 1)
        g = randBetween(0, 1)
        b = randBetween(0, 1)
        print '%s %s %s' % (r, g, b)
    print ']'

    print '"uniform string type" "disk"'


def main():
    args = sys.stdin.readline()
    while args:
        arg = args.split()
        pixels = float(arg[0])
        rad = float(arg[1])
        num = int(arg[2])
        width = float(arg[3])

        print 'TransformBegin'
        cloud(rad, num, width)
        print 'TransformEnd'
        sys.stdout.write('\377')
        sys.stdout.flush()
        # read the next set of inputs
        args = sys.stdin.readline()

if __name__ == "__main__":
    main()
