set maxdepth 39
set colorpool randomhue
5 * { z -1.1 s 0.8 1 0.8 } L1
rule L1 { {color random} 15 * { y 1 } R1 }
rule R1 { { ry 10 x 1.1 } R1 }
rule R1 w 0.1 { R2 }
rule R2 { {} E1 { ry 10 x 1.1 h 3 } R2 }
rule R2 w 0.06 { }
rule E1 { { s 0.9 sat 0.9 h 1 } box }
rule BG { { y -1 s 100 1 100 sat 0 } box }