set maxdepth 400
Start

rule Start { 8 * { h 100 y 10 z 10 rz 30 } 1 * { } P }

rule P { Arm { y 3.5 rx 45 ry 45 } P }
rule P { Arm { y 3.5 ry -45 rz -45 } P }
rule P { Arm { y 3.5 rx -45 ry 45 } P }
rule P { Arm { y 3.5 ry -45 rz 45 } P }
rule P  w 0.06 { Dott }

rule Dott {
	{ h 100 } grid
	{ h 200 } sphere
}

rule Arm {
	{ y -0.3 } Dott
	6 * { y 0.5 } 1 * { s 0.15 0.6 0.15 h 200 } box
}